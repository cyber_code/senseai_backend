param([switch]$Elevated)
function Use-Admin 
{
  $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
  $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

if ((Use-Admin) -eq $false)
{
  if ($elevated)
  {
    # could not elevate, quit
  }
  
  else
  {  
    Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
  }
  exit
}

try 
{
  Get-NetFirewallRule -DisplayName ValidataSenseAI -ErrorAction Stop
  Write-Host "Rule found"
}
catch [Exception] 
{
  New-NetFirewallRule -DisplayName ValidataSenseAI-Inbound -Confirm -Description "ValidataSenseAI Inbound Rule for port range 9100-9105" -LocalAddress Any -LocalPort 9100-9105 -Protocol tcp -RemoteAddress Any -RemotePort Any -Direction Inbound
  New-NetFirewallRule -DisplayName ValidataSenseAI-Outbound -Confirm -Description "ValidataSenseAI Outbound Rule for port range 9100-9105" -LocalAddress Any -LocalPort 9100-9105 -Protocol tcp -RemoteAddress Any -RemotePort Any -Direction Outbound
  
  New-NetFirewallRule -DisplayName ValidataSenseAI-SQL-Inbound -Confirm -Description "ValidataSenseAI Inbound Rule for sql server port 9433" -LocalAddress Any -LocalPort 9433 -Protocol tcp -RemoteAddress Any -RemotePort Any -Direction Inbound
  New-NetFirewallRule -DisplayName ValidataSenseAI-SQL-Outbound -Confirm -Description "ValidataSenseAI Outbound Rule for sql server port 9433" -LocalAddress Any -LocalPort 9433 -Protocol tcp -RemoteAddress Any -RemotePort Any -Direction Outbound
  
  New-NetFirewallRule -DisplayName ValidataSenseAI-NuoAdmin-Inbound -Confirm -Description "ValidataSenseAI Inbound Rule for Nuo Admin port 9433" -LocalAddress Any -LocalPort 8888 -Protocol tcp -RemoteAddress Any -RemotePort Any -Direction Inbound
  New-NetFirewallRule -DisplayName ValidataSenseAI-NuoAdmin-Outbound -Confirm -Description "ValidataSenseAI Outbound Rule for Nuo Admin port 9433" -LocalAddress Any -LocalPort 8888 -Protocol tcp -RemoteAddress Any -RemotePort Any -Direction Outbound

  New-NetFirewallRule -DisplayName ValidataSenseAI-NuoDB-Inbound -Confirm -Description "ValidataSenseAI Inbound Rule for NuoDB port range 48004-48050" -LocalAddress Any -LocalPort 48004-48050 -Protocol tcp -RemoteAddress Any -RemotePort Any -Direction Inbound
  New-NetFirewallRule -DisplayName ValidataSenseAI-NuoDB-Outbound -Confirm -Description "ValidataSenseAI Outbound Rule for NuoDB port range 48004-48050" -LocalAddress Any -LocalPort 48004-48050 -Protocol tcp -RemoteAddress Any -RemotePort Any -Direction Outbound
}