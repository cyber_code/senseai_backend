$imagesToDelete = docker images --filter=reference="validatasenseai/*" -q

If (-Not $imagesToDelete) {Write-Host "Not deleting validatasenseai images as there are no validatasenseai images in the current local Docker repo."} 
Else 
{
    # Delete all containers
    Write-Host "Deleting all containers in local Docker Host"
    docker rm $(docker ps -a -q) -f
    
    # Delete all validatasenseai images
    Write-Host "Deleting validatasenseai images in local Docker repo"
    Write-Host $imagesToDelete
    docker rmi $(docker images --filter=reference="validatasenseai/*" -q) -f
}