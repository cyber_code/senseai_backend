$imagesToDelete = docker images --filter=reference="validatasenseai.azurecr.io/validatasenseai/*" -q

If (-Not $imagesToDelete) {Write-Host "Not deleting validatasenseai.azurecr.io images as there are no images in the current local Docker repo pulled from this registry."} 
Else 
{
    # Delete all containers
    Write-Host "Deleting all containers in local Docker Host"
    docker rm $(docker ps -a -q) -f
    
    # Delete all validatasenseai.azurecr.io/validatasenseai images
    Write-Host "Deleting validatasenseai.azurecr.io/validatasenseai images in local Docker repo"
    Write-Host $imagesToDelete
    docker rmi $(docker images --filter=reference="validatasenseai.azurecr.io/validatasenseai/*" -q) -f
}