﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace WebApi.Routing
{
    /// <inheritdoc />
    public class RouteProvider : IRouteProvider
    {
        /// <inheritdoc />
        public int Priority => 0;

        /// <inheritdoc />
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute(
                name: "default",
                template: "api/{controller}/{action=Index}/{id?}");
        }
    }
}