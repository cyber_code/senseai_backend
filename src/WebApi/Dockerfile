#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /src

COPY scripts scripts/

COPY src/*/*.csproj /src/csproj-files/
COPY src/*/*/*.csproj /src/csproj-files/
COPY src/*/*/*/*.csproj /src/csproj-files/

COPY . .
WORKDIR /src/src/WebApi
ARG BUILD_BUILDNUMBER
RUN echo "Building version ${BUILD_BUILDNUMBER}"
RUN dotnet publish -c Release /p:Version=${BUILD_BUILDNUMBER} -o /app

FROM build as unittest
WORKDIR /src/src/Tests/Domain.Tests

FROM build as integrationtest
WORKDIR /src/src/Tests/WebApi.IntegrationTest

FROM build AS publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "WebApi.dll"]