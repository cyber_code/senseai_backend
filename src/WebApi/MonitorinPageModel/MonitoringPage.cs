﻿using Logging.Messaging.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.MonitorinPageModel
{
    public class MonitoringPage
    {
        public MonitoringPage()
        {
            ServiceInfo = new List<ServiceInfo>();
            CommandLog = new List<CommandLog>();
            QueryLog = new List<QueryLog>();
        }

        public List<ServiceInfo> ServiceInfo { get; set; }
        public IEnumerable<CommandLog> CommandLog { get; set; }
        public IEnumerable<QueryLog> QueryLog { get; set; }
    }
}