﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.MonitorinPageModel
{
    public class ServiceInfo
    {
        public string Title { get; set; }

        public bool Active { get; set; }

        public string Info { get; set; }
    }
}