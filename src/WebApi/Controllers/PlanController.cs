﻿using Microsoft.AspNetCore.Mvc;
using Messaging;
using Commands.DesignModule.QualitySuite;
using Queries.DesignModule.QualitySuite;
using Queries.TestGenerationModule;

namespace WebApi.Controllers
{
    /// <summary>
    /// Plan controller
    /// </summary>
    [Route("api/[controller]/[action]")]
    public class PlanController : SenseAIController
    {
        private readonly IBus _bus;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bus"></param>
        public PlanController(IBus bus)
        {
            _bus = bus;
        }

        #region Test Generation

        /// <summary>
        /// Export Testcases
        /// </summary>
        /// <param name="query">Provide WorkflowId</param>
        /// <returns>Return Testcases exported in XML format</returns>
        [HttpGet]
        public IActionResult ExportTestCases([FromQuery]ExportTestCases query)
        {
            return File(_bus.Query(query).Result.Xml, "application/octet-stream", "testcases.xml");
        }

        /// <summary>
        /// Check ATS Test Cases
        /// </summary>
        /// <param name="query">Provide WorkflowId</param>
        /// <returns>Return Testcases exported in XML format</returns>
        [HttpGet]
        public IActionResult CheckATSTestCases([FromQuery]CheckATSTestCases query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Save Workflow to Quality Suite
        /// For the given workflow id saves the exports the plan to ATS
        /// </summary>
        /// <param name="command">Provide WorkflowId</param>
        /// <returns>Return number of TotlalTestCases generated</returns>
        [HttpPost]
        public IActionResult ExportWorkflowToQualitySuite(ExportWorkflowToQualitySuite command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Get list of project from ATS
        /// </summary>
        /// <returns>Return list of project from ATS</returns>
        [HttpGet]
        public IActionResult GetATSProjects()
        {
            return Ok(_bus.Query(new GetATSProject()));
        }

        /// <summary>
        /// Get list of subprojects from ATS
        /// </summary>
        /// <param name="query">Provide project id</param>
        /// <returns>List of subprojects</returns>
        [HttpGet]
        public IActionResult GetATSSubProjects([FromQuery]GetATSSubproject query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get ats systems
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetATSSystems([FromQuery]GetATSSystems query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get ats catalogs
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetATSCatalogs([FromQuery]GetATSCatalogs query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get list of products from ATS
        /// </summary>
        /// <returns>Return list of project from ATS</returns>
        [HttpGet]
        public IActionResult GetATSProducts([FromQuery]GetATSProducts query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get list of adapters from ATS
        /// </summary>
        /// <returns>Return list of adapters from ATS</returns>
        [HttpGet]
        public IActionResult GetATSAdapters([FromQuery]GetATSAdapters query)
        {
            return Ok(_bus.Query(query));
        }
        /// <summary>
        /// Get list of adapters from ATS by systems
        /// </summary>
        /// <returns>Return list of adapters from ATS</returns>
        [HttpGet]
        public IActionResult GetATSAdaptersPerSystems([FromQuery]GetATSAdaptersPerSystems query)
        {
            return Ok(_bus.Query(query));
        }
        /// <summary>
        /// Get list of features from ATS
        /// </summary>
        /// <returns>Return list of project from ATS</returns>
        [HttpGet]
        public IActionResult GetATSFeatures([FromQuery]GetATSFeatures query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get list of use cases from ATS
        /// </summary>
        /// <returns>Return list of project from ATS</returns>
        [HttpGet]
        public IActionResult GetATSUsecases([FromQuery]GetATSUsecases query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get list of requirements from ATS
        /// </summary>
        /// <returns>Return list of project from ATS</returns>
        [HttpGet]
        public IActionResult GetATSRequirements([FromQuery]GetATSRequirements query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Release Summary Report from ATS for given Release Iteration ID (TAG_UNIQUE)
        /// </summary>
        /// <param name="query">Should contain release iteration id</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetATSReleaseSummary([FromQuery]GetATSReleaseSummary query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get highlighted workflow path by workflowPathId
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetHighlightedWorkflowPath([FromQuery]GetHighlightedWorkflowPath query)
        {
            return Ok(_bus.Query(query));
        }

        #endregion Test Generation
    }
}