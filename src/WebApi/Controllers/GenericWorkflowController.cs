﻿using Commands.ProcessModule.GenericWorkflows;
using Messaging; 
using Microsoft.AspNetCore.Mvc;
using Queries.ProcessModule.GenericWorkflows; 

namespace WebApi.Controllers
{
    /// <summary>
    /// GenericWorkflow controller
    /// </summary>
    [Route("api/Process/[controller]/[action]")]
    public class GenericWorkflowController : SenseAIController
    {
        private readonly IBus _bus;

        public GenericWorkflowController(IBus bus)
        {
            _bus = bus;
        }

        /// <summary>
        /// Get the list of generic workflows under a process
        /// </summary>
        /// <param name="query">Provide the processId</param>
        /// <returns>Returns the id and title</returns>
        [HttpGet]
        public IActionResult GetGenericWorkflows([FromQuery] GetGenericWorkflows query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Return the details of generic workflow
        /// </summary>
        /// <param name="query">Provide the Id of ars workflow</param>
        /// <returns>Returns the details of generic workflow</returns>
        [HttpGet]
        public IActionResult GetGenericWorkflow([FromQuery] GetGenericWorkflow query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Load generic workflow version
        /// </summary>
        /// <param name="query">Provide GenericWorkflowId</param>
        /// <returns>Return details of aris workflow version including Title, Version</returns>
        [HttpGet]
        public IActionResult GetGenericWorkflowVersions([FromQuery]GetGenericWorkflowVersions query)
        {
            return Ok(_bus.Query(query));
        }
        /// <summary>
        /// Load generic workflow version by date
        /// </summary>
        /// <param name="query">Provide GenericWorkflowId and date</param>
        /// <returns>Return details of aris workflow version including Title, Version</returns>
        [HttpGet]
        public IActionResult GetGenericWorkflowVersionsByDate([FromQuery]GetGenericWorkflowVersionsByDate query)
        {
            return Ok(_bus.Query(query));
        }
        /// <summary>
        /// Add an generic workflow
        /// </summary>
        /// <param name="command">Provide ProcessId, title, description and designerJson </param>
        /// <returns>Retunr the Id of saved generic workflow</returns>
        [HttpPost]
        public IActionResult AddGenericWorkflow(AddGenericWorkflow command)
        {
            return Ok(_bus.Execute(command));
        } 

        /// <summary>
        /// Issue generic workflow
        /// </summary>
        /// <param name="command">Provide GenericWorkflowId and Image of generic workflow</param>
        /// <returns>Return GenericWorkflowId</returns>
        [HttpPost]
        public IActionResult IssueGenericWorkflow(IssueGenericWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Rollback generic workflow
        /// </summary>
        /// <param name="command">Provide GenericWorkflowId and Version</param>
        /// <returns>Return details of generic workflow version including Title, Description, Version, Json</returns>
        [HttpPut]
        public IActionResult RollbackGenericWorkflow(RollbackGenericWorkflowVersion command)
        {
            var result = _bus.Execute(command);
            return Ok(result);
        }

        /// <summary>
        /// Update the generic workflow
        /// </summary>
        /// <param name="command">Provide the Id and designerJson</param>
        /// <returns>Returns the saved Id </returns>
        [HttpPut]
        public IActionResult SaveGenericWorkflow(SaveGenericWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// rename the generic workflow
        /// </summary>
        /// <param name="command">Provide the Id and title</param>
        /// <returns>Returns the saved Id </returns>
        [HttpPut]
        public IActionResult RenameGenericWorkflow(RenameGenericWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Delete generic workflow
        /// </summary>
        /// <param name="command">Provide the Id</param>
        /// <returns>Returns true if action is OK.</returns>
        [HttpDelete]
        public IActionResult DeleteGenericWorkflow([FromQuery]DeleteGenericWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }
    }
}