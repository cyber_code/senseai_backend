﻿using Messaging;
using Microsoft.AspNetCore.Mvc;
using Queries.ReportModule.RequirementPrioritizationReport;
using Queries.ReportModule.TraceabilityReport;

namespace WebApi.Controllers
{
    /// <summary>
    /// ArisWorkflow controller
    /// </summary>
    [Route("api/Process/[controller]/[action]")]
    public class ReportsController : SenseAIController
    {
        private readonly IBus _bus;

        public ReportsController(IBus bus)
        {
            _bus = bus;
        }

        /// <summary>
        /// Get Traceability Report Line
        /// </summary>
        /// <param name="query">Provide the processId</param>
        /// <returns>Returns summary lines</returns>
        [HttpGet]
        public IActionResult GetTraceabilityReportLine([FromQuery]GetTraceabilityReport query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Traceability Report Detail
        /// </summary>
        /// <param name="query">Provide the processId</param>
        /// <returns>Returns list of details</returns>
        [HttpGet]
        public IActionResult GetTraceabilityReportDetail([FromQuery] GetTraceabilityReportDetail query)
        {
            return Ok(_bus.Query(query));
        }

        
        /// <summary>
        /// Get Requirement Prioritization Report
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetRequirementPrioritizationReport([FromQuery] GetRequirementPrioritizationReport query)
        {
            return Ok(_bus.Query(query));
        }

    }
}