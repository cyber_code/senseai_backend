﻿using Commands.ProcessModule.JiraIntegration;
using Messaging;
using Microsoft.AspNetCore.Mvc;
using Queries.ProcessModule.Issues;
using Queries.ProcessModule.JiraIntegration;

namespace WebApi.Controllers
{
    [Route("api/Process/[controller]/[action]")]
    public class JiraController : SenseAIController
    {
        private readonly IBus _bus;

        public JiraController(IBus bus)
        {
            _bus = bus;
        }

        #region Jira API

        /// <summary>
        /// Get Projects from Jira
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetJiraProjects([FromQuery]GetJiraProjects query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Issue Type from Jira
        /// </summary>
        /// <param name="query">Need to provide Jira Project Key</param>
        /// <returns>Return list of suggestion items</returns>
        [HttpGet]
        public IActionResult GetJiraIssueTypes([FromQuery]GetJiraIssueTypes query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Issue Type from sense.ai
        /// </summary>
        /// <param name="query">Need to provide Sense.ai subprojectId</param>
        /// <returns>Return list of suggestion items</returns>
        [HttpGet]
        public IActionResult GetSenseaiIssueTypes([FromQuery]GetSenseaiIssueTypes query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Statuses form Jira Cloud
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetIssueStatuses([FromQuery]GetJiraIssueStatuses query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Priorities from Jira Cloud
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetIssuePriorities([FromQuery]GetJiraIssuePriorities query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Resolutions from Jira Cloud
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetJiraIssueResolutions([FromQuery]GetJiraIssueResolutions query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get IssueType Fields from SenseAI by providing IssueType Id
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetIssueTypeFieldsAndAttributes([FromQuery] GetIssueTypeFieldsAndAttributes query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Requirement Fields from SenseAI
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetIssueTypeRequirementFields([FromQuery] GetIssueTypeRequirementFieldsAndAttributes query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get IssueType Fields from Jira Cloud by providing Jira IssueType Id
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetJiraIssueTypeFieldsAndAttributes([FromQuery]GetJiraIssueTypeFieldsAndAttributes query)
        {
            return Ok(_bus.Query(query != null ? query : new GetJiraIssueTypeFieldsAndAttributes()));
        }

        #region Mapping

        /// <summary>
        /// Add new JiraProjectMapping on Sense.ai database
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddJiraProjectMapping(AddJiraProjectMapping command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a JiraIssueTypeMapping icluding Attributes and AttributeValues
        /// </summary>
        /// <param name="command">Id of JiraIssueTypeMapping</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DeleteJiraIssueTypeMapping(DeleteJiraIssueTypeMapping command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Add new JiraUserMapping on Sense.ai database
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddJiraUserMapping(AddJiraUserMapping command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update JiraProjectMapping on Sense.ai database
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateJiraProjectMapping(UpdateJiraProjectMapping command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Delete JiraUserMapping on Sense.ai database
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult DeleteJiraUserMapping([FromQuery]DeleteJiraUserMapping command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Update JiraUserMapping on Sense.ai database
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateJiraUserMapping(UpdateJiraUserMapping command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Get mapping between Jira and Sense.ai project
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetJiraProjectMapping([FromQuery]GetJiraProjectMapping query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get user mapping between Jira and Sense.ai project
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetJiraUserMapping([FromQuery]GetJiraUserMapping query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Add new Jira IssueType mapping
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddJiraIssueTypeMapping(AddJiraIssueTypeMapping command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update existing Jira IssueType mapping
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateJiraIssueTypeMapping(UpdateJiraIssueTypeMapping command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Get Jira IssueType Mapping including attributes and attribute values
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetJiraIssueTypeMapping([FromQuery]GetJiraIssueTypeMapping query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Add jira credentials for specific user
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult SetJiraUserSubprojectConfigs(SetJiraUserSubprojectConfigs command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// udpate jira credentials for specific user
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateJiraUserSubprojectConfigs(UpdateJiraUserSubprojectConfigs command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// get jira credenctials
        /// </summary>
        /// <param name="query">Username and subproject</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetJiraUserSubprojectConfigs([FromQuery]GetJiraUserSubprojectConfigs query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// get jira credenctials
        /// </summary>
        /// <param name="query">Username and subproject</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult HasValidJiraCredentials([FromQuery]HasValidJiraCredentials query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Jira IssueType list by providing ProjectId from mapping table
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetJiraIssueTypeListByProjectMapping([FromQuery]GetJiraIssueTypeListByProjectMapping query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get list of users from Jira
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetJiraUsers([FromQuery]GetJiraUsers query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get details for Integration mappings
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetJiraIntegrationSummary([FromQuery]GetJiraIntegrationSummary query)
        {
            return Ok(_bus.Query(query));
        }

        [HttpGet]
        public IActionResult FirstTimeSync([FromQuery]FirstTimeSync query)
        {
            return Ok(_bus.Query(query));
        }

        [HttpPost]
        public IActionResult AddJiraWebhooks(AddJiraWebhooks command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpPut]
        public IActionResult UpdateJiraWebhooks(UpdateJiraWebhooks command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpGet]
        public IActionResult GetJiraWebhooks([FromQuery]GetJiraWebhooks query)
        {
            return Ok(_bus.Query(query));
        }

        #endregion Mapping

        #endregion Jira API
    }
}