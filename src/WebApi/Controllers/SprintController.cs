﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Commands.ProcessModule.Sprints;
using Messaging;
using Microsoft.AspNetCore.Mvc;
using Queries.ProcessModule.Processes;
using Queries.ProcessModule.Sprints;

namespace WebApi.Controllers
{
    [Route("api/Process/[controller]/[action]")]
    public class SprintController : SenseAIController
    {
        private readonly IBus _bus;

        public SprintController(IBus bus)
        {
            _bus = bus;
        }

        #region Sprint Model Implementation

        /// <summary>
        /// Add a Sprint
        /// </summary>
        /// <param name="command">Provide Title, description, start and end date</param>
        /// <returns>Return added Sprint Id</returns>
        [HttpPost]
        public IActionResult AddSprint(AddSprints command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a Sprint
        /// </summary>
        /// <param name="command">Provide Id, Title, description, start and end date</param>
        /// <returns>Return updated Sprint Id</returns>
        [HttpPut]
        public IActionResult UpdateSprint(UpdateSprints command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a Sprint
        /// </summary>
        /// <param name="command">Provide SprintId</param>
        /// <returns>Success or failure</returns>
        [HttpDelete]
        public IActionResult DeleteSprint([FromQuery]DeleteSprints command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }
        /// <summary>
        /// Start a Sprint
        /// </summary>
        /// <param name="command">Provide SprintId</param>
        /// <returns>Success or failure</returns>
        [HttpPut]
        public IActionResult StartSprint([FromQuery]StartSprints command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }
        /// <summary>
        /// End a Sprint
        /// </summary>
        /// <param name="command">Provide SprintId</param>
        /// <returns>Success or failure</returns>
        [HttpPut]
        public IActionResult EndSprint([FromQuery]EndSprints command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }
        /// <summary>
        /// Get Sprint by Id
        /// </summary>
        /// <param name="getSprint">Provide SprintId</param>
        /// <returns>Return details of Sprint</returns>
        [HttpGet]
        public IActionResult GetSprint([FromQuery]GetSprint getSprint)
        {
            return Ok(_bus.Query(getSprint));
        }
        [HttpGet]
        public IActionResult GetSprintsBySubProject([FromQuery]GetSprintsBySubProject getSprint)
        {
            return Ok(_bus.Query(getSprint));
        }
        [HttpGet]
        public IActionResult GetOpenSprintBySubProject([FromQuery]GetOpenSprintBySubProject getSprint)
        {
            return Ok(_bus.Query(getSprint));
        }
        /// <summary>
        /// Get Sprint 
        /// </summary>
        /// <returns>Return list of Sprintes with details</returns>
        [HttpGet]
        public IActionResult GetSprints()
        {
            return Ok(_bus.Query(new GetSprints()));
        }

        #endregion

        #region Sprint Issues Model Implementation

        /// <summary>
        /// Add a SprintIssue
        /// </summary>
        /// <param name="command">Provide sprintid, issueid, status</param>
        /// <returns>Return added SprintIssue Id</returns>
        [HttpPost]
        public IActionResult AddSprintIssue(AddSprintIssues command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a SprintIssue
        /// </summary>
        /// <param name="command">Provide Id, sprintid, issueid, status</param>
        /// <returns>Return updated SprintIssue Id</returns>
        [HttpPut]
        public IActionResult UpdateSprintIssue(UpdateSprintIssues command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a SprintIssue
        /// </summary>
        /// <param name="command">Provide SprintIssueId</param>
        /// <returns>Success or failure</returns>
        [HttpDelete]
        public IActionResult DeleteSprintIssue([FromQuery]DeleteSprintIssues command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get SprintIssue by SprintId
        /// </summary>
        /// <param name="getSprint">Provide SprintId</param>
        /// <returns>Return details of Sprint</returns>
        [HttpGet]
        public IActionResult GetSprintIssues([FromQuery]GetSprintIssues getSprint)
        {
            return Ok(_bus.Query(getSprint));
        }

        /// <summary>
        /// Get Sprint statistics by SprintId
        /// </summary>
        /// <param name="query">Provide SprintId</param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public IActionResult GetSprintStatistics([FromQuery]GetSprintStatistics query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }


        #endregion
    }
}