﻿using Microsoft.AspNetCore.Mvc;
using Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Commands.InsightModule.Defects;
using Queries.InsightModule;
using Application.Commands.InsightModule.ExecutionLogs;

namespace WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class InsightController : SenseAIController
    {
        private readonly IBus _bus;

        public InsightController(IBus bus)
        {
            _bus = bus;
        }

        #region Insight Implementation

        /// <summary>
        /// Tenant Info and files
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult SaveDefects([FromForm] DefectsModel command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }
        /// <summary>
        /// acceptchanges
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AcceptChanges(AcceptTypicalChangesModel command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }
        [HttpGet]
        public IActionResult GetDefectsColumnMapping()
        {
            return Ok(_bus.Query(new DefectColumnMappingModel()));
        }

        [HttpPost]
        public IActionResult UploadExecutionLogs([FromForm] ExecutionsModel command)
        {
            var commandResponse = _bus.Execute(command); 
            return Ok(commandResponse);
        }

        #endregion Insight Implementation
    }
}