﻿using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [ApiController]
    public class SenseAIController : ControllerBase
    {
        /// <summary>
        /// return status code 204 (NoContent)
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        protected IActionResult Deleted()
        {
            return NoContent();
        }

        /// <summary>
        /// return status code 200 and no content
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        protected IActionResult Updated()
        {
            return Ok();
        }
    }
}