﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Commands.DesignModule.Suggestions;
using Commands.DesignModule.WorkflowModel;
using Messaging;
using Queries.DesignModule;
using Queries.DesignModule.Suggestions;
using Queries.DesignModule.System;
using Queries.DesignModule.Systems;
using System;
using System.IO;
using Application.Commands.DesignModule.TestEngineModel;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Queries.DesignModule.TestEngine;
using Queries.ProcessModule.JiraIntegration;
using Commands.ProcessModule.JiraIntegration;

namespace WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class DesignController : SenseAIController
    {
        private readonly IBus _bus;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bus"></param>
        public DesignController(IBus bus)
        {
            _bus = bus;
        }

        #region Workflow Implementation

        /// <summary>
        /// Add New Workflow
        /// </summary>
        /// <param name="command">Provide Title, Description and FolderId</param>
        /// <returns>Return ID of added workflow </returns>
        [HttpPost]
        public IActionResult AddWorkflow(AddWorkflow command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update Workflow
        /// </summary>
        /// <param name="command">Provide Workflow Id, Json and PathJson</param>
        /// <returns>Return ID of updated workflow</returns>
        [HttpPut]
        public IActionResult SaveWorkflow(SaveWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Delete workflow
        /// </summary>
        /// <param name="command">Provide WorkflowId</param>
        [HttpDelete]
        public IActionResult DeleteWorkflow([FromQuery]DeleteWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Copy and Paste workflow
        /// </summary>
        /// <param name="command">Provide WorkflowId, FolderId and Type [Copy = 0,Cut = 1]</param>
        /// <returns>Return WorkflowId and Name of workflow</returns>
        [HttpPost]
        public IActionResult PasteWorkflow(PasteWorkflow command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Issue workflow
        /// </summary>
        /// <param name="command">Provide WorkflowId and Image of workflow</param>
        /// <returns>Return WorkflowId</returns>
        [HttpPost]
        public IActionResult IssueWorkflow(IssueWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        [HttpPost]
        public IActionResult WorkflowsVersioning(WorkflowsVersioning command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Rename Workflow Name
        /// </summary>
        /// <param name="command">Provide WorkflowId</param>
        [HttpPut]
        public IActionResult RenameWorkflow(RenameWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Load workflow
        /// </summary>
        /// <param name="workflowId">Provide WorkflowId and the new name of workflow</param>
        /// <returns>Return WorkflowId and the updated name</returns>
        [HttpGet]
        public IActionResult GetWorkflow([FromQuery]Guid workflowId)
        {
            return Ok(_bus.Query(new GetWorkflow { Id = workflowId }));
        }

        /// <summary>
        /// Load workflow by id and version
        /// </summary>
        /// <param name="workflowId">Provide WorkflowId and the new name of workflow</param>
        /// <param name="versionId">Provide WorkflowId and the new name of workflow</param>
        /// <returns>Return Workflow</returns>
        [HttpGet]
        public IActionResult GetWorkflowByVersion([FromQuery]GetWorkflowByVersion query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Load workflow by title
        /// </summary>
        /// <param name="query">Provide title of workflow</param>
        /// <returns>Return workflow details</returns>
        [HttpGet]
        public IActionResult GetWorkflowByTitle([FromQuery]GetWorkflowByTitle query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Load Workflow version
        /// </summary>
        /// <param name="query">Provide WorkflowId</param>
        /// <returns>Return details of workflow version including Title, Description, Version, Json</returns>
        [HttpGet]
        public IActionResult GetWorkflowVersions([FromQuery]GetWorkflowVersions query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Workflow Version details
        /// </summary>
        /// <param name="query">Provide WorkflowId, Date from and Date to</param>
        /// <returns>Return details of workflow version including Title, Description, Version, Json</returns>
        [HttpGet]
        public IActionResult GetWorkflowVersionsByDate([FromQuery]GetWorkflowVersionsByDate query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Workflow Version details
        /// </summary>
        /// <param name="workflowId">Provide WorkflowId</param>
        /// <param name="version">Provide Workflow Version</param>
        /// <returns>Return details of workflow version including Title, Description, Version, Json</returns>
        [HttpGet]
        public IActionResult GetWorkflowVersion([FromQuery]Guid workflowId, [FromQuery]int version)
        {
            var wfVersion = new GetWorkflowVersion();
            wfVersion.WorkflowId = workflowId;
            wfVersion.Version = version;
            return Ok(_bus.Query(wfVersion));
        }

        /// <summary>
        /// Rollback workflow
        /// </summary>
        /// <param name="command">Provide WorkflowId and Version</param>
        /// <returns>Return details of workflow version including Title, Description, Version, Json</returns>
        [HttpPut]
        public IActionResult RollbackWorkflow(RollbackWorkflowVersion command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Get suggested relationship attributes between typicals
        /// </summary>
        /// <param name="query">SourceTypicalName and TargetTypicalName</param>
        /// <returns>Return suggested relationship attributes between typicals</returns>
        [HttpGet]
        public IActionResult GetDynamicDataSuggestions([FromQuery]GetDynamicDataSuggestions query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// save suggested relationship attributes between typicals
        /// </summary>
        /// <param name="command">DynamicData suggestion object</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult SaveDynamicDataSuggestions(SaveDynamicDataSuggestions command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get previous workflow items
        /// </summary>
        /// <param name="query">Provide currentId and DesignerJson</param>
        /// <returns>Return workflowItem [id, titile and typicalName] </returns>
        [HttpPost]
        public IActionResult GetPreviousWorkflowItems(GetPreviousWorkflowItems query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Action to upload Aris XML file
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="systemId"></param>
        /// <param name="catalogId"></param>
        /// <param name="file"></param>
        [HttpPost]
        public IActionResult ImportArisWorkflow(Guid folderId, Guid systemId, Guid catalogId, IFormFile file)
        {
            string fileContent = "";
            using (StreamReader sr = new StreamReader(file.OpenReadStream()))
            {
                fileContent = sr.ReadToEnd();
            }
            var command = new ImportArisWorkflow
            {
                File = fileContent,
                FileName = file.FileName,
                FolderId = folderId,
                SystemId = systemId,
                CatalogId = catalogId
            };
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Save workflows
        /// </summary>
        /// <param name="command">Provide workflow details</param>
        /// <returns>Id of workflows</returns>
        [HttpPost]
        public IActionResult SaveComputerGeneratedWorkflows(SaveComputerGeneratedWorkflows command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Save workflows recorded on test engine
        /// </summary>
        /// <param name="command">Provide workflow details</param>
        /// <returns>Id of workflows</returns>
        [HttpPost]
        public IActionResult SaveRecordedWorkflow(SaveRecordedWorkflow command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Cancel workflows recorded on test engine
        /// </summary>
        /// <param name="command">Provide workflow details</param>
        /// <returns>Id of workflows</returns>
        [HttpPost]
        public IActionResult CancelRecordedWorkflow(CancelRecordedWorkflow command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion Workflow Implementation

        #region TreeView Implementation

        /// <summary>
        /// Get all folders by SubProjectId
        /// </summary>
        /// <param name="query">SubProjectId</param>
        /// <returns>Return list of folders. Return attribute: Id, Subproject, Title and Description</returns>
        [HttpGet]
        public IActionResult GetFolders([FromQuery]GetFolders query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all workflows by FolderId
        /// </summary>
        /// <param name="query">Provide folder id</param>
        /// <returns>Return list of workflows. Return attribute: Id, Title, Description and FolderId </returns>
        [HttpGet]
        public IActionResult GetWorkflows([FromQuery]GetWorkflows query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all workflows by keyword and SubProjectId
        /// </summary>
        /// <param name="query">Provide SubProject and Search string criteria</param>
        /// <returns>Return list of workflows. Return attribute: Id, Title, Description and FolderId</returns>
        [HttpGet]
        public IActionResult SearchWorkflows([FromQuery]SearchWorkflows query)
        {
            return Ok(_bus.Query(query));
        }

        #endregion TreeView Implementation

        #region Enquiry Actions Implementation

        /// <summary>
        /// Get Enquiry Actions
        /// </summary>
        /// <param name="getEnquiryActions">Provide Typical Name and CatalogId</param>
        /// <returns>Return list of action types</returns>
        [HttpGet]
        public IActionResult GetEnquiryActions([FromQuery]GetEnquiryActions getEnquiryActions)
        {
            return Ok(_bus.Query(getEnquiryActions));
        }

        #endregion Enquiry Actions Implementation

        #region Search Typicals and Attributes Implementation

        /// <summary>
        /// Get typicals detail
        /// </summary>
        /// <param name="query">Provide Catalog Id as parameter</param>
        /// <returns>Return title and Json details of Typicals</returns>
        [HttpGet]
        public IActionResult GetTypicals([FromQuery]GetTypicals query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Search typicals
        /// </summary>
        /// <param name="query">Provide Catalog Id and search string</param>
        /// <returns>Return title and Json details of Typicals</returns>
        [HttpGet]
        public IActionResult SearchTypicals([FromQuery]SearchTypicals query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// get typical details
        /// </summary>
        /// <param name="query">Provide Catalog Id and name and type</param>
        /// <returns>Return Id and the list of attributes</returns>
        [HttpGet]
        public IActionResult GetTypical([FromQuery]GetTypical query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        ///     Get Attributes of typical
        /// </summary>
        /// <param name="query">Provide Typical Id and search string</param>
        /// <returns>Return attribute result of typical</returns>
        [HttpGet]
        public IActionResult SearchAttributes([FromQuery]SearchAttributes query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        ///     Get attributes of typical by catalog id and typical name
        /// </summary>
        /// <param name="query">Provide Catalog Id, Typical name and search string</param>
        /// <returns>Return attribute result of typical</returns>
        [HttpGet]
        public IActionResult SearchAttributesByCatalogIdAndTypicalName([FromQuery]SearchAttributesByCatalogIdAndTypicalName query)
        {
            return Ok(_bus.Query(query));
        }

        #endregion Search Typicals and Attributes Implementation

        #region Systems Implementation

        /// <summary>
        /// Get List of all systems
        /// </summary>
        /// <returns>Return list of systems with Title and AttributeName</returns>
        [HttpGet]
        public IActionResult GetSystems()
        {
            return Ok(_bus.Query(new GetSystems()));
        }

        /// <summary>
        /// Get condition items
        /// </summary>
        /// <param name="query">Provide Adapter name as parameter</param>
        /// <returns>Return list of condition items</returns>
        [HttpGet]
        public IActionResult GetConditionItem([FromBody] GetConditionItems query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get action items
        /// </summary>
        /// <param name="query">Provide Adapter name as parameter</param>
        /// <returns>Return list of action items</returns>
        [HttpGet]
        public IActionResult GetActionItem([FromQuery] GetActionItems query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Catalog by Id
        /// </summary>
        /// <param name="catalogId">Provide catalog Id</param>
        /// <returns>Return catalog details</returns>
        [HttpGet]
        public IActionResult GetCatalog([FromQuery]Guid catalogId)
        {
            return Ok(_bus.Query(new GetCatalog { Id = catalogId }));
        }

        /// <summary>
        /// Get list of catalogs by SubProjectId
        /// </summary>
        /// <returns>Return list of catalogs. Return attribute: Id, Title, Description and Type</returns>
        [HttpGet]
        public IActionResult GetCatalogs([FromQuery]GetCatalogs query)
        {
            return Ok(_bus.Query(query));
        }

        #endregion Systems Implementation

        #region Neural Engine API

        /// <summary>
        /// Get Pre-suggestion and Post-suggestion list from Neural engine
        /// </summary>
        /// <param name="query">Need to provide currentItemId and Json designer</param>
        /// <returns>Return list of suggestion items</returns>
        [HttpPost]
        public IActionResult GetSuggestions(GetSuggestions query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get workflow paths
        /// </summary>
        /// <param name="query">Need to provide Json desinger from UI</param>
        /// <returns>Return list of workflow paths</returns>
        [HttpPost]
        public IActionResult GetPaths(GetPaths query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Post issued workflow
        /// </summary>
        /// <param name="query">WorkflowId</param>
        /// <returns>Success or failure</returns>
        [HttpPost]
        public IActionResult PostIssueWorkflow(IssueWorkflowToML query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get workflow coverage and data coverage  from Neural engine
        /// </summary>
        /// <param name="query">Need to provide currentItemId and Json designer</param>
        /// <returns>Return the workflow coverage and data coverage of that workflow</returns>
        [HttpPost]
        public IActionResult GetWorkflowCoverage(GetWorkflowCoverage query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get workflow list from Neural engine
        /// </summary>
        /// <param name="query">Need to provide TenantInfo, TypicalId, TypicalTitle and WorkflowTitle</param>
        /// <returns>Return list of workflows</returns>
        [HttpPost]
        public IActionResult GetComputerGeneratedWorkflows(GetComputerGeneratedWorkflows query)
        {
            return Ok(_bus.Query(query));
        }

        #endregion Neural Engine API

        #region TestEngineIntegration

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 200000000)]
        [RequestSizeLimit(200000000)]
        public IActionResult SaveVideo(Guid workflowId, IFormFile videoFile)
        {
            var bytes = default(byte[]);
            using (StreamReader reader = new StreamReader(videoFile.OpenReadStream()))
            using (var memstream = new MemoryStream())
            {
                reader.BaseStream.CopyTo(memstream);
                bytes = memstream.ToArray();
            }

            SaveVideo command = new SaveVideo
            {
                WorkflowId = workflowId,
                VideoFile = bytes
            };
            return Ok(_bus.Execute(command));
        }

        [HttpGet]
        public IActionResult DownloadVideo([FromQuery] PlayVideoModel command)
        {
            var commandResponse = _bus.Execute(command);

            return File(commandResponse.Result.VideoFile, "application/octet-stream", $"{ command.WorkflowId }.mp4");
        }

        [HttpGet]
        public IActionResult HasVideo([FromQuery] HasVideoModel command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion TestEngineIntegration

    }
}