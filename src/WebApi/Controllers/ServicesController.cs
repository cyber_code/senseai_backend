﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DataExtraction;
using Logging.Messaging;
using Logging.Messaging.Persistence.Model;
using Messaging;
using NeuralEngine;
using Queries.AdministrationModule.Settings;
using WebApi.MonitorinPageModel;
using WebApi.PolicyManagement.Contracts;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ServicesController : SenseAIController
    {
        private readonly IBus _bus;
        private readonly IMessagingLogger _messagingLogger;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly INeuralEngineClient _neuralEngineClient;
        private readonly IIdentityServerContract _identityServerContract;

        public ServicesController(IBus bus, IMessagingLogger messagingLogger, IDataExtractionClient dataExtractionClient, INeuralEngineClient neuralEngineClient, IIdentityServerContract identityServerContract)
        {
            _bus = bus;
            _messagingLogger = messagingLogger;
            _dataExtractionClient = dataExtractionClient;
            _neuralEngineClient = neuralEngineClient;
            _identityServerContract = identityServerContract;
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Ping([FromQuery] CheckDBConnection command)
        {
            MonitoringPage monitorinPageModel = new MonitoringPage();

            var check = _bus.Query(command).Result;

            monitorinPageModel.ServiceInfo.Add(new ServiceInfo { Title = "Sense AI web API is running", Active = true, Info = "" });
            monitorinPageModel.ServiceInfo.Add(new ServiceInfo { Title = "Sense AI DB is running", Active = check.IsConnected, Info = "" });

            var extrationTool = _dataExtractionClient.CheckService();
            foreach (var item in extrationTool)
            {
                monitorinPageModel.ServiceInfo.Add(new ServiceInfo { Title = item.Title, Active = item.Active, Info = item.Info });
            }

            var neuralEngine = _neuralEngineClient.CheckServices();
            foreach (var item in neuralEngine)
            {
                monitorinPageModel.ServiceInfo.Add(new ServiceInfo { Title = item.Title, Active = item.Active, Info = item.Info });
            }

            monitorinPageModel.CommandLog = _messagingLogger.GetCommandLogs();
            monitorinPageModel.QueryLog = _messagingLogger.GetQueryLogs();

            //var queryLogs = _messagingLogger.GetQueryLogs();

            //var identityServer = _identityServerContract.CheckService("-");

            return Ok(JsonConvert.SerializeObject(monitorinPageModel));
        }

        [HttpGet]
        public IActionResult GetSetting([FromQuery] GetSetting command)
        {
            return Ok(_bus.Query(command));
        }

        [HttpGet]
        public IActionResult GetVersion()
        {
          
            return Ok(_bus.Query(new GetVersion()));
        }

        //public class MonitoringPage
        //{
        //    public MonitoringPage()
        //    {
        //        ServiceInfo = new List<ServiceInfo>();
        //        ServiceInfo.Add(new ServiceInfo { Title = "Sence AI Api", Active = true, Info = "API URL" });

        //        CommandLog = new List<CommandLog>();
        //        CommandLog.Add(new Logging.Messaging.Persistence.Model.CommandLog
        //        {
        //            Id = 1,
        //            ExecutedBy = "Argetim Ahmeti",
        //            AppName = "App Name",
        //            CalledFrom = "CalledFrom",
        //            Name = "Name",
        //            Json = "Json",
        //            ResultName = "ResultName",
        //            ResultJson = "ResultJson",
        //            ErrorExceptionType = "ErrorExceptionType",
        //            ErrorExceptionMessage = "ErrorExceptionMessage",
        //            ErrorExceptionStackTrace = "ErrorExceptionStackTrace",
        //            ErrorExceptionJson = "ErrorExceptionJson",
        //            Started = DateTime.Now,
        //            Ended = DateTime.Now,
        //            ElapsedMilliseconds = 120,
        //            IpAddress = "IpAddress"
        //        });
        //    }

        //    //public ServiceInfo SenseAIApi { get; set; }
        //    //public ServiceInfo SenseAIDB { get; set; }
        //    //public ServiceInfo MLApi { get; set; }
        //    //public ServiceInfo MLDB { get; set; }
        //    //public ServiceInfo ExctractionApi { get; set; }
        //    //public ServiceInfo ExctractionDB { get; set; }
        //    //public ServiceInfo ISApi { get; set; }
        //    //public ServiceInfo ISDB { get; set; }
        //    //public ServiceInfo LogDb { get; set; }

        //    public List<ServiceInfo> ServiceInfo { get; set; }
        //    public List<CommandLog> CommandLog { get; set; }
        //    public List<QueryLog> QueryLog { get; set; }
        //}

        //public class ServiceInfo
        //{
        //    public string Title { get; set; }

        //    public bool Active { get; set; }

        //    public string Info { get; set; }
        //}
    }
}