﻿using Commands.AAProductBuilderModule;
using Messaging;
using Microsoft.AspNetCore.Mvc;
using Queries.AAProductBuilderModule;

namespace WebApi.Controllers
{
    /// <summary>
    /// This controller contains methods for the AAProductBuilder
    /// </summary>
    [Route("api/[controller]/[action]")]
    public class AAProductBuilderController : SenseAIController
    {
        private readonly IBus _bus;

        /// <summary>
        /// The controller cunstructor
        /// </summary>
        /// <param name="bus"></param>
        public AAProductBuilderController(IBus bus)
        {
            _bus = bus;
        }

        /// <summary>
        /// Creates the product
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateProduct(CreateProduct createProduct)
        {
            return Ok(_bus.Execute(createProduct).Result);
        }

        /// <summary>
        /// Updates the product property status
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult PropertySelected(PropertySelected propertySelected)
        {
            return Ok(_bus.Execute(propertySelected).Result);
        }

        /// <summary>
        /// Updates all product properties status
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ProductCompleted(ProductCompleted productCompleted)
        {
            return Ok(_bus.Execute(productCompleted).Result);
        }

        /// <summary>
        /// Inserts the answer
        /// </summary>
        /// <param name="questionAnswered"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult QuestionAnswered(QuestionAnswered questionAnswered)
        {
            return Ok(_bus.Execute(questionAnswered).Result);
        }

        /// <summary>
        /// Inserts the header answer
        /// </summary>
        /// <param name="headerQuestionAnswered"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult HeaderQuestionAnswered(HeaderQuestionAnswered headerQuestionAnswered)
        {
            return Ok(_bus.Execute(headerQuestionAnswered).Result);
        }

        /// <summary>
        /// Resets the session
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ResetSession(ResetSession resetSession)
        {
            return Ok(_bus.Execute(resetSession).Result);
        }

        /// <summary>
        /// Get all parameters
        /// </summary>
        /// <returns>Return list of parameters</returns>
        [HttpGet]
        public IActionResult ListParameters()
        {
            return Ok(_bus.Query(new ListParameters()).Result);
        }

        /// <summary>
        /// Get all queries
        /// </summary>
        /// <returns>Return list of queries</returns>
        [HttpGet]
        public IActionResult ListQueries()
        {
            return Ok(_bus.Query(new ListQueries()).Result);
        }

        /// <summary>
        /// Get query by QueryId
        /// </summary>
        /// <returns>Return list of products</returns>
        [HttpPost]
        public IActionResult GetQuery(GetQuery getQuery)
        {
            return Ok(_bus.Query(getQuery).Result);
        }

        /// <summary>
        /// Runs the query
        /// </summary>
        /// <param name="runQuery">Returns a list of key value results</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult RunQuery(RunQuery runQuery)
        {
            return Ok(_bus.Query(runQuery).Result);
        }

        /// <summary>
        /// Deploys the product
        /// </summary>
        [HttpPost]
        public IActionResult DeployProduct(DeployProduct deployProduct)
        {
            return Ok(_bus.Execute(deployProduct));
        }

        /// <summary>
        /// Get product header configurations
        /// </summary>
        /// <returns>Return list of product's configurations</returns>
        [HttpGet]
        public IActionResult GetNewArrangementFieldConfigs()
        {
            return Ok(_bus.Query(new GetNewArrangementFieldConfigs()));
        }

        /// <summary>
        /// Updates the field values
        /// </summary>
        /// <returns>Return list of product's configurations</returns>
        [HttpPost]
        public IActionResult UpdateFieldValues(UpdateFieldValues updateFieldValues)
        {
            return Ok(_bus.Execute(updateFieldValues));
        }

        /// <summary>
        /// Get property selected field values
        /// </summary>
        /// <returns>Return list of product's configurations</returns>
        [HttpGet]
        public IActionResult GetPropertySelectedFieldValues([FromQuery] GetPropertySelectedFieldValues query)
        {
            return Ok(_bus.Query(query));
        }
    }
}