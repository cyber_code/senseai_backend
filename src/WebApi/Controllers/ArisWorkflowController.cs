﻿using Commands.ProcessModule.ArisWorkflows;
using Messaging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Queries.ProcessModule.ArisWorkflows;
using System;
using System.IO;

namespace WebApi.Controllers
{
    /// <summary>
    /// ArisWorkflow controller
    /// </summary>
    [Route("api/Process/[controller]/[action]")]
    public class ArisWorkflowController : SenseAIController
    {
        private readonly IBus _bus;

        public ArisWorkflowController(IBus bus)
        {
            _bus = bus;
        }

        /// <summary>
        /// Get the list of aris workflows under a process
        /// </summary>
        /// <param name="query">Provide the processId</param>
        /// <returns>Returns the id and title</returns>
        [HttpGet]
        public IActionResult GetArisWorkflows([FromQuery] GetArisWorkflows query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Return the details of aris workflow
        /// </summary>
        /// <param name="query">Provide the Id of ars workflow</param>
        /// <returns>Returns the details of aris workflow</returns>
        [HttpGet]
        public IActionResult GetArisWorkflow([FromQuery] GetArisWorkflow query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all workflows by keyword and SubProjectId
        /// </summary>
        /// <param name="query">Provide SubProject and Search string criteria</param>
        /// <returns>Return list of aris workflows. Return attribute: Id, Title, Description and proceId</returns>
        [HttpGet]
        public IActionResult SearchArisWorkflows([FromQuery]SearchArisWorkflows query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Load aris workflow version
        /// </summary>
        /// <param name="query">Provide ArisWorkflowId</param>
        /// <returns>Return details of aris workflow version including Title, Version</returns>
        [HttpGet]
        public IActionResult GetArisWorkflowVersions([FromQuery]GetArisWorkflowVersions query)
        {
            return Ok(_bus.Query(query));
        }
        /// <summary>
        /// Get aris Workflow Version details
        /// </summary>
        /// <param name="query">Provide ArisWorkflowId, Date from and Date to</param>
        /// <returns>Return details of aris workflow version including Title, Description, Version, Json</returns>
        [HttpGet]
        public IActionResult GetArisWorkflowVersionsByDate([FromQuery]GetArisWorkflowVersionsByDate query)
        {
            return Ok(_bus.Query(query));
        }
        /// <summary>
        /// Add an aris workflow
        /// </summary>
        /// <param name="command">Provide ProcessId, title, description and designerJson </param>
        /// <returns>Retunr the Id of saved aris workflow</returns>
        [HttpPost]
        public IActionResult AddArisWorkflow(AddArisWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Import aris workflows to aris desinger
        /// </summary> 
        /// <param name="file">File name</param>
        /// <param name="processId">Process Id</param>
        /// <returns>returns the generated aris workflows</returns>
        [HttpPost]
        public IActionResult ImportArisWorkflow2ArisDesigner(Guid processId, IFormFile file)
        {
            string content = "";
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                content = reader.ReadToEnd();
                reader.Close();
            }
            var command = new ImportArisWorkflow2ArisDesigner
            {
                FileContent = content,
                FileName = file.FileName,
                ProcessId = processId
            };
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Import aris workflows and create process for each workflow
        /// </summary>
        /// <param name="subProjectId">Subproject Id</param>
        /// <param name="hierarchyId">Hierarchy Id</param>
        /// <param name="file">File name</param>
        /// <returns>returns the list of created process</returns>
        [HttpPost]
        public IActionResult ImportArisWorkflows(Guid subProjectId, Guid hierarchyId, IFormFile file)
        {
            string content = "";
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                content = reader.ReadToEnd();
                reader.Close();
            }
            var command = new ImportArisWorkflows
            {
                FileContent = content,
                FileName = file.FileName,
                SubProjectId = subProjectId,
                HierarchyId = hierarchyId
            };
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Export an aris workflow to workflow
        /// </summary>
        /// <param name="command">Provide subprojectId, systemId, catalogId, and arisWorkflowId</param>
        /// <returns>Returns true in case the export is OK.</returns>
        [HttpPost]
        public IActionResult ExportArisWorkflow2WorkflowDesinger([FromQuery] ExportArisWorkflow2WorkflowDesinger command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Issue aris workflow
        /// </summary>
        /// <param name="command">Provide ArisWorkflowId and Image of aris workflow</param>
        /// <returns>Return ArisWorkflowId</returns>
        [HttpPost]
        public IActionResult IssueArisWorkflow(IssueArisWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Update the aris workflow
        /// </summary>
        /// <param name="command">Provide the Id and designerJson</param>
        /// <returns>Returns the saved Id </returns>
        [HttpPut]
        public IActionResult SaveArisWorkflow(SaveArisWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// rename the aris workflow
        /// </summary>
        /// <param name="command">Provide the Id and title</param>
        /// <returns>Returns the saved Id </returns>
        [HttpPut]
        public IActionResult RenameArisWorkflow(RenameArisWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Rollback aris workflow
        /// </summary>
        /// <param name="command">Provide ArisWorkflowId and Version</param>
        /// <returns>Return details of aris workflow version including Title, Description, Version, Json</returns>
        [HttpPut]
        public IActionResult RollbackArisWorkflow(RollbackArisWorkflowVersion command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Delete aris workflow
        /// </summary>
        /// <param name="command">Provide the Id</param>
        /// <returns>Returns true if action is OK.</returns>
        [HttpDelete]
        public IActionResult DeleteArisWorkflow([FromQuery]DeleteArisWorkflow command)
        {
            return Ok(_bus.Execute(command));
        }
    }
}