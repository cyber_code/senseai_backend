﻿using Microsoft.AspNetCore.Mvc;
using Commands.DataModule.Data;
using Commands.DataModule.DataSets;
using Commands.DataModule.TestCases;
using Messaging;
using Queries.DataModule.Data;
using Queries.DataModule.Data.DataSets;
using Queries.DataModule.DataSets;
using Queries.DataModule.Visualization;
using Queries.TestGenerationModule;
using WebApi.ApiErrors;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Commands.DataModule.TestEngine;

namespace WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class DataController : SenseAIController
    {
        private readonly IBus _bus;

        public DataController(IBus bus)
        {
            _bus = bus;
        }

        #region Data Model Implementation

        /// <summary>
        /// Get All navigations
        /// </summary>
        /// <returns>Return list of Navigation (Id, Name, Url, ParentId)</returns>
        [HttpGet]
        public IActionResult GetAllNavigations([FromQuery]GetNavigations query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Navigation by Id
        /// </summary>
        /// <param name="getNavigation">Provide NavigationId</param>
        /// <returns>Return Navigation with details (Id, Name, Url, ParentId)</returns>
        [HttpGet]
        public IActionResult GetNavigation([FromQuery]GetNavigation getNavigation)
        {
            return Ok(_bus.Query(getNavigation));
        }

        /// <summary>
        /// Get Navigation by parent id
        /// </summary>
        /// <param name="getNavigation">Provide ParentId</param>
        /// <returns>Return list of Navigation (Id, Name, Url, ParentId)</returns>
        [HttpGet]
        public IActionResult GetNavigationsByParent([FromQuery]GetNavigationsByParent getNavigation)
        {
            return Ok(_bus.Query(getNavigation));
        }

        /// <summary>
        /// Get All Production Data
        /// </summary>
        /// <returns>Return list of production data (Id, TypicalId, Json)</returns>
        [HttpGet]
        public IActionResult GetAllProductionDatas()
        {
            return Ok(_bus.Query(new GetProductionDatas()));
        }

        /// <summary>
        /// Get Production Data by Id
        /// </summary>
        /// <param name="getProductionData">Provide ProducationDataId</param>
        /// <returns>Return list of production data (Id, TypicalId, Json)</returns>
        [HttpGet]
        public IActionResult GetProductionData([FromQuery]GetProductionData getProductionData)
        {
            return Ok(_bus.Query(getProductionData));
        }

        /// <summary>
        /// Add a Typical
        /// </summary>
        /// <param name="command">Provide CatalogId, Title, Json, TypicalType [Application,Enqury,Version,Association]</param>
        /// <returns>Return added TypicalId</returns>
        [HttpPost]
        public IActionResult AddTypical(AddTypical command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a Typical
        /// </summary>
        /// <param name="command">Provide TypicalId, Title, Json</param>
        /// <returns>Return updated TypicalId</returns>
        [HttpPut]
        public IActionResult UpdateTypical(UpdateTypical command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a Typical
        /// </summary>
        /// <param name="command">Provide TypcialId</param>
        /// <returns>Success or failure</returns>
        [HttpDelete]
        public IActionResult DeleteTypical([FromQuery]DeleteTypical command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Add a Typical Manually
        /// </summary>
        /// <param name="command">Provide ProjectId, SubProjectId, SystemId, Title, TenantId, TypicalType [Application,Enqury,Version,Association]</param>
        /// <returns>Return added TypicalId</returns>
        [HttpPost]
        public IActionResult AddTypicalManually(AddTypicalManually command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a Typical Manually
        /// </summary>
        /// <param name="command">Provide TypicalId, Title, IsTypicalChange</param>
        /// <returns>Return updated TypicalId</returns>
        [HttpPut]
        public IActionResult UpdateTypicalManually(UpdateTypicalManually command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a Typical Manually
        /// </summary>
        /// <param name="command">Provide Id, IsTypicalChange</param>
        /// <returns>Success or failure</returns>
        [HttpDelete]
        public IActionResult DeleteTypicalManually([FromQuery]DeleteTypicalManually command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get Typical by Id
        /// </summary>
        /// <param name="getTypical">Provide TypicalId</param>
        /// <returns>Return details of Typical</returns>
        [HttpGet]
        public IActionResult GetTypical([FromQuery]GetTypical getTypical)
        {
            return Ok(_bus.Query(getTypical));
        }

        /// <summary>
        /// Get Typical by name
        /// </summary>
        /// <param name="query">Provide Typical name</param>
        /// <returns>Return details of Typical</returns>
        [HttpGet]
        public IActionResult GetTypicalByName([FromQuery]GetTypicalByName query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Typical by type
        /// </summary>
        /// <param name="getTypical">Provide TypicalType [Application,Enqury,Version,Association]</param>
        /// <returns>Return list of typical details</returns>
        [HttpGet]
        public IActionResult GetTypicalsByType([FromQuery]GetTypicalsByType getTypical)
        {
            return Ok(_bus.Query(getTypical));
        }

        /// <summary>
        /// Add a Typical Attribute Manually
        /// </summary>
        /// <param name="command">Provide TypicalId, Name, IsMultiValue, PossibleValues, IsRequired, IsNoInput, IsNoChange, IsExternal, MinimumLength, MaximumLength, RelatedApplicationName, ExtraData, RelatedApplicationIsConfiguration</param>
        /// <returns>Return TypicalId</returns>
        [HttpPost]
        public IActionResult AddTypicalAttributeManually(AddTypicalAttributeManually command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a Typical Attribute Manually
        /// </summary>
        /// <param name="command">Provide TypicalId, Name, IsMultiValue, PossibleValues, IsRequired, IsNoInput, IsNoChange, IsExternal, MinimumLength, MaximumLength, RelatedApplicationName, ExtraData, RelatedApplicationIsConfiguration</param>
        /// <returns>Return TypicalId</returns>
        [HttpPut]
        public IActionResult UpdateTypicalAttributeManually(UpdateTypicalAttributeManually command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a Typical Attribute
        /// </summary>
        /// <param name="command">Provide TypcialId, Name</param>
        /// <returns>Success or failure</returns>
        [HttpDelete]
        public IActionResult DeleteTypicalAttributeManually([FromQuery]DeleteTypicalAttributeManually command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get Typical Attribute by Typical Id and Attribute Name
        /// </summary>
        /// <param name="getTypicalAttribute">Provide TypicalId, Name</param>
        /// <returns>Return details of Typical Attribute</returns>
        [HttpGet]
        public IActionResult GetTypicalAttribute([FromQuery]GetTypicalAttribute getTypicalAttribute)
        {
            return Ok(_bus.Query(getTypicalAttribute));
        }

        #endregion Data Model Implementation

        #region Test case generation

        /// <summary>
        /// Generate Test Cases
        /// </summary>
        /// <param name="command">Provide WorkflowId, WorkflowPathId and DataSetsJson</param>
        /// <returns>Return Id of testcase generated</returns>
        [HttpPost]
        public IActionResult GenerateTestCases(GenerateTestCases command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Generate Test Cases
        /// </summary>
        /// <param name="command">Provide WorkflowId, WorkflowPathId and DataSetsJson</param>
        /// <returns>Return Id of testcase generated</returns>
        [HttpPost]
        public IActionResult SetDataSetForPathItem(SetDataSetForPathItem command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }
       
        #endregion Test case generation
        /// <summary>
        /// Get Workflow Paths
        /// </summary>
        /// <param name="query">Provide WorkflowId</param>
        /// <returns>Return list of WorkflowPaths</returns>
        [HttpGet]
        public IActionResult GetWorkflowPaths([FromQuery] GetWorkflowPaths query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Workflow Path Items only for test steps
        /// </summary>
        /// <param name="query">Provide WorkflowPathId</param>
        /// <returns>Return WorkflowPath details (PathItemId and PathTitle)</returns>
        [HttpGet]
        public IActionResult GetWorkflowPathItems([FromQuery] GetWorkflowPathItems query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Workflow all Path Items ids
        /// </summary>
        /// <param name="query">Provide WorkflowPathId</param>
        /// <returns>Return WorkflowPath details (PathItemId and PathTitle)</returns>
        [HttpGet]
        public IActionResult GetWorkflowPathItemIds([FromQuery] GetWorkflowPathItemIds query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Test Suite
        /// </summary>
        /// <param name="query">Provide WorkflowId and WorkflowPathId</param>
        /// <returns>Return Name of TestSuite, with TestCases and TestSteps with coverage</returns>
        [HttpGet]
        public IActionResult GetTestSuite([FromQuery] GetTestSuite query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Test Suite catalogs
        /// </summary>
        /// <param name="query">Provide WorkflowId</param>
        /// <returns>Return Name of catalogs</returns>
        [HttpGet]
        public IActionResult GetTestSuiteCatalogs([FromQuery] GetTestSuiteCatalogs query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get Test Suite systems
        /// </summary>
        /// <param name="query">Provide WorkflowId</param>
        /// <returns>Return Name of catalogs</returns>
        [HttpGet]
        public IActionResult GetTestSuiteSystems([FromQuery] GetTestSuiteSystems query)
        {
            return Ok(_bus.Query(query));
        }

        #region "Data set and data set items"

        /// <summary>
        /// Add new data set
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Returns the id of new added data set</returns>
        [HttpPost]
        public IActionResult AddDataSet(AddDataSet command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Add new data set in manual way
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Returns the id of new added data set</returns>
        [HttpPost]
        public IActionResult AddManualDataSet(AddManualDataSet command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// update a manual data set by Id
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Returns the id of updated data set</returns>
        [HttpPut]
        public IActionResult UpdateManualDataSet(UpdateManualDataSet command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// update a data set by Id
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Returns the id of updated data set</returns>
        [HttpPut]
        public IActionResult UpdateDataSet(UpdateDataSet command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Delete a data set by Id
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Returns the id of updated data set</returns>
        [HttpDelete]
        public IActionResult DeleteDataSet([FromQuery]DeleteDataSet command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Update a data set item by id
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Returns the id of updated data set item</returns>
        [HttpPut]
        public IActionResult UpdateDataSetItem(UpdateDataSetItem command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// delete a data set item by id
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Returns the id of delete data set item</returns>
        [HttpDelete]
        public IActionResult DeleteDataSetItem([FromQuery]DeleteDataSetItem command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Get List of DataSet stored in DB
        /// </summary>
        /// <param name="query">Provide TypicalId</param>
        /// <returns>Return list of rows with Title and list of rows</returns>
        [HttpGet]
        public IActionResult GetDataSets([FromQuery] GetDataSets query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get DataSet stored in DB by ID
        /// </summary>
        /// <param name="query">Provide DataSetID</param>
        /// <returns>Return details with Title and list of combinations</returns>
        [HttpGet]
        public IActionResult GetDataSet([FromQuery] GetDataSet query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get List of DataSet items sugessted from AI and PI
        /// </summary>
        /// <param name="query">Provide TypicalId</param>
        /// <returns>Return list of rows with Title and list of rows</returns>
        [HttpGet]
        public IActionResult GetSuggestedDataSets([FromQuery] GetSuggestedDataSets query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get List of DataSet items sugessted from AI and PI
        /// </summary>
        /// <param name="query">Provide TypicalId</param>
        /// <returns>Return list of rows with Title and list of rows</returns>
        [HttpGet]
        public IActionResult GetSuggestedDataSets2([FromQuery] GetSuggestedDataSets query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get  DataSet items stored in DB for a data set
        /// </summary>
        /// <param name="query">Provide Data set id</param>
        /// <returns>Return list of rows with Title and list of rows</returns>
        [HttpGet]
        public IActionResult GetDataSetItem([FromQuery] GetDataSetItem query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get  DataSet items stored in DB for a list of data sets
        /// </summary>
        /// <param name="query">Provide Data set ids</param>
        /// <returns>Return list of rows with Title and list of rows</returns>
        [HttpPost]
        public IActionResult GetDataSetItems(GetDataSetItems query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get typical details by ID
        /// </summary>
        /// <param name="query">Provide the typical Id</param>
        /// <returns>Returns the details of a typical</returns>
        [HttpGet]
        public IActionResult GetTypicalAttributes([FromQuery] GetTypicalAttributes query)
        {
            return Ok(_bus.Query(query));
        }

        #endregion "Data set and data set items"

        [HttpGet]
        public IActionResult GetModelVisualization([FromQuery] GetModelVisualization query)
        {
            return Ok(_bus.Query(query));
        }
        #region Accept model changes
        /// <summary>
        /// Get nr of changes
        /// </summary>

        /// <returns>Return the number of typical changes</returns>
        [HttpGet]
        public IActionResult GetNumberOfTypicalChanges()
        {
            var response = _bus.Query(new GetTypicalChanges());
            var newresponse = new Messaging.Queries.QueryResponse<int>(response.Result.Length);
 
            return Ok(newresponse);
        }
        /// <summary>
        /// Get typical changes
        /// </summary>
        /// <returns>Typical changes</returns>
        [HttpGet]
        public IActionResult GetTypicalChanges()
        {
            return Ok(_bus.Query(new GetTypicalChanges()));
        }
        /// <summary>
        /// Get accepted typical changes
        /// </summary>
        /// <returns>Typical changes</returns>
        [HttpGet]
        public IActionResult GetAcceptedTypicalChanges()
        {
            return Ok(_bus.Query(new GetAcceptedTypicalChanges()));
        }
        [HttpGet]
        public IActionResult GetTypicalAttributeChangesById([FromQuery] GetTypicalAttributeChanges query)
        {
            return Ok(_bus.Query(query));
        }
        [HttpGet]
        public IActionResult GetAcceptedTypicalAttributeChangesById([FromQuery] GetAcceptedTypicalAttributeChanges query)
        {
            return Ok(_bus.Query(query));
        }
        /// <summary>
        /// Accept the changes to the typical
        /// </summary>
        /// <param name="command">Provide typical changes id and if the affected workflow will be marked as invalid</param>
        /// <returns>Return true or false</returns>
        [HttpPost]
        public IActionResult AcceptChanges(AcceptTypicalChanges command)
        {
            var commandResponse = _bus.Execute(command);
            
                return Ok(commandResponse);
            
        }
        /// <summary>
        /// Accept the changes to the typicals without affected workflow
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Return true or false</returns>
        [HttpPost]
        public IActionResult AcceptAllChangesWithoutWorkflow(AcceptAllTypicalChangesWithoutWorkflow command)
        {
            var commandResponse = _bus.Execute(command);
           
                return Ok(commandResponse);
           
        }
        #endregion

        /// <summary>
        /// Create a v-enquiry typical
        /// </summary>
        /// <param name="command">Provide details to save the typical</param>
        /// <returns>Return true in case everything is OK</returns>
        [HttpPost]
        public IActionResult SaveTypical(SaveTypical command)
        {
            return Ok(_bus.Execute(command));
        }
    }
}