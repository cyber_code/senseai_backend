﻿using Microsoft.AspNetCore.Mvc;
using Messaging;
using Commands.ProcessModule.Issues;
using Commands.ProcessModule.Hierarchies;
using Queries.ProcessModule.Issues;
using Queries.ProcessModule.Hierarchies;
using Commands.ProcessModule.RequirementTypes;
using Queries.ProcessModule.RequirementTypes;
using Commands.ProcessModule.RequirementPriorities;
using Queries.ProcessModule.RequirementPriorities;

namespace WebApi.Controllers
{
    [Route("api/Process/[controller]/[action]")]
    public class ConfigureController : SenseAIController
    {
        private readonly IBus _bus;

        public ConfigureController(IBus bus)
        {
            _bus = bus;
        }

        #region IssueTypes

        /// <summary>
        /// Add new IssueType
        /// </summary>
        /// <param name="command">Provide parameters Title and Description</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddIssueType(AddIssueType command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update IssueType
        /// </summary>
        /// <param name="command">Provide parameters ID, Title and Description</param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateIssueType(UpdateIssueType command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete IssueType
        /// </summary>
        /// <param name="command">Provide parameter Id</param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult DeleteIssueType([FromQuery]DeleteIssueType command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get All Issue Types
        /// </summary>
        /// <returns>Return list of issue types</returns>
        [HttpGet]
        public IActionResult GetIssueTypes([FromQuery]GetIssueTypes getIssueTypes)
        {
            return Ok(_bus.Query(getIssueTypes));
        }

        /// <summary>
        /// Get issue type by Id
        /// </summary>
        /// <param name="getIssueType">Provide IssueTypeId</param>
        /// <returns>Return issue tpye details</returns>
        [HttpGet]
        public IActionResult GetIssueType([FromQuery]GetIssueType getIssueType)
        {
            return Ok(_bus.Query(getIssueType));
        }
        /// <summary>
        /// Get All Issue Types Fields
        /// </summary>
        /// <returns>Return list of issue types Fields</returns>
        [HttpGet]
        public IActionResult GetIssueTypeFields([FromQuery]GetIssueTypesFields getIssueTypesFields)
        {
            return Ok(_bus.Query(getIssueTypesFields));
        }

        /// <summary>
        /// Get issue type Field by Id
        /// </summary>
        /// <param name="getIssueTypeFields">Provide IssueType Field Id</param>
        /// <returns>Return issue tpye Fields details</returns>
        [HttpGet]
        public IActionResult GetIssueTypeField([FromQuery]GetIssueTypeFields getIssueTypeFields)
        {
            return Ok(_bus.Query(getIssueTypeFields));
        }
        /// <summary>
        /// Get All Issue Types
        /// </summary>
        /// <returns>Return list of issue types FieldsNames</returns>
        [HttpGet]
        public IActionResult GetIssueTypeFieldsNames()
        {
            return Ok(_bus.Query(new GetIssueTypeFieldsNames()));
        }

        /// <summary>
        /// Get issue type by Id
        /// </summary>
        /// <param name="getIssueTypeFieldsName">Provide IssueTypeFieldsNameId</param>
        /// <returns>Return issue tpye FieldsNames details</returns>
        [HttpGet]
        public IActionResult GetIssueTypeFieldsName([FromQuery]GetIssueTypeFieldsName getIssueTypeFieldsName)
        {
            return Ok(_bus.Query(getIssueTypeFieldsName));
        }
        #endregion

        #region HierarchyTypes

        /// <summary>
        /// Create a new HierarchyType by giving ParentId, Title and Description
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddHierarchyType(AddHierarchyType command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a HierarchyType
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateHierarchyType(UpdateHierarchyType command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a HierarchyType
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult DeleteHierarchyType([FromQuery]DeleteHierarchyType command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get a specific HierarchyType by giving its Id
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetHierarchyType([FromQuery]GetHierarchyType query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get last hierarchy type given SubProjectId
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetLastHierarchyType([FromQuery]GetLastHierarchyType query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get HierarchyTypes by giving their ParentId
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetHierarchyTypes([FromQuery]GetHierarchyTypes query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Move HierarchyType
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult MoveHierarchyType(MoveHierarchyType command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion

        #region Requirement Types

        /// <summary>
        /// Add a requirement type to a SubProject
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddRequirementType(AddRequirementType command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a requirement type
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateRequirementType(UpdateRequirementType command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a requirement type
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult DeleteRequirementType([FromQuery] DeleteRequirementType command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get a requirement type details
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetRequirementType([FromQuery] GetRequirementType query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get all the requirement types in a SubProject
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetRequirementTypes([FromQuery] GetRequirementTypes query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        #endregion

        #region Requirement Priorities

        /// <summary>
        /// Add a requirement priority to a subproject
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddRequirementPriority(AddRequirementPriority command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a requirement priority
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateRequirementPriority(UpdateRequirementPriority command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }
        /// <summary>
        /// Update a requirement priority index
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateIndexesRequirementPriority(UpdateIndexesRequirementPriority command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }
        /// <summary>
        /// Delete a requirement priority
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult DeleteRequirementPriority([FromQuery] DeleteRequirementPriority command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get a requirement priority details
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetRequirementPriority([FromQuery] GetRequirementPriority query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get all the requirement priorities in a SubProject
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetRequirementPriorities([FromQuery] GetRequirementPriorities query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        #endregion

    }
}