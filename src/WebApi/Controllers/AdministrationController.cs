﻿using System;
using Microsoft.AspNetCore.Mvc;
using Messaging;
using Commands.AdministrationModule.Projects;
using Commands.AdministrationModule.Settings;
using Commands.AdministrationModule.SystemCatalogs;
using Commands.AdministrationModule.Systems;
using Commands.AdministrationModule.Catalogs;
using Commands.AdministrationModule.SystemTags;
using Commands.DesignModule.Folder;
using Queries.AdministrationModule.Folder;
using Queries.AdministrationModule.Projects;
using Queries.AdministrationModule.Settings;
using Queries.AdministrationModule.SystemCatalogs;
using Queries.AdministrationModule.Systems;
using Queries.AdministrationModule.SystemTags;

namespace WebApi.Controllers
{
    /// <summary>
    /// This controller contains methods for the admin
    /// </summary>
    [Route("api/[controller]/[action]")]
    public class AdministrationController : SenseAIController
    {
        private readonly IBus _bus;

        /// <summary>
        /// the controller cunstructor
        /// </summary>
        /// <param name="bus"></param>
        public AdministrationController(IBus bus)
        {
            _bus = bus;
        }

        #region Project and SubProject Implementation

        /// <summary>
        /// Add a project
        /// </summary>
        /// <param name="command">Provide Title and Description of project</param>
        /// <returns>Return id of newly created project</returns>
        [HttpPost]
        public IActionResult AddProject(AddProject command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Add a new sub-project
        /// </summary>
        /// <param name="command">Provide ProjectId, Title and Description</param>
        /// <returns>Return SubprojectId</returns>
        [HttpPost]
        public IActionResult AddSubProject(AddSubProject command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a project
        /// </summary>
        /// <param name="command">Provide Project Id</param>
        /// <returns>Return true or false</returns>
        [HttpDelete]
        public IActionResult DeleteProject([FromQuery]DeleteProject command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a sub project
        /// </summary>
        /// <param name="command">Provide SubprojectId</param>
        /// <returns>Return success or failure </returns>
        [HttpDelete]
        public IActionResult DeleteSubProject([FromQuery]DeleteSubProject command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get project by Id
        /// </summary>
        /// <param name="getProject">Provide ProjectId</param>
        /// <returns>Return project details</returns>
        [HttpGet]
        public IActionResult GetProject([FromQuery]GetProject getProject)
        {
            return Ok(_bus.Query(getProject));
        }

        /// <summary>
        /// Get All projects
        /// </summary>
        /// <returns>Return list of projects</returns>
        [HttpGet]
        public IActionResult GetProjects()
        {
            return Ok(_bus.Query(new GetProjects()));
        }

        /// <summary>
        /// Get Sub-project by Id
        /// </summary>
        /// <param name="getSubProject">Provide SubprojectId</param>
        /// <returns>Return SubProject details</returns>
        [HttpGet]
        public IActionResult GetSubProject([FromQuery]GetSubProject getSubProject)
        {
            return Ok(_bus.Query(getSubProject));
        }

        /// <summary>
        /// Get all sub projects by a project Id
        /// </summary>
        /// <param name="query">Provide ProjectId</param>
        /// <returns>Return all subproject details related to ProjectId</returns>
        [HttpGet]
        public IActionResult GetSubProjects([FromQuery]GetSubProjects query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Update a project
        /// </summary>
        /// <param name="command">Provide Id, Title and Description of project</param>
        /// <returns>Return true or false</returns>
        [HttpPut]
        public IActionResult UpdateProject(UpdateProject command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a Sub-project
        /// </summary>
        /// <param name="command">Provide SubprojectId, ProjectId, Title and Description</param>
        /// <returns>Return success or failure </returns>
        [HttpPut]
        public IActionResult UpdateSubProject(UpdateSubProject command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a Sub-project (jiraConfiguration and jiraUrl
        /// </summary>
        /// <param name="command">Provide configuration and jiraUrl</param>
        /// <returns>Return success or failure </returns>
        [HttpPut]
        public IActionResult UpdateSubProjectJiraCredentials(UpdateSubProjectJiraCredentials command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion Project and SubProject Implementation

        #region Folders

        /// <summary>
        /// Add a new folder
        /// </summary>
        /// <param name="command">Provide SubprojectId, Title and Description</param>
        /// <returns>Return FolderId</returns>
        [HttpPost]
        public IActionResult AddFolder(AddFolder command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete the folder
        /// </summary>
        /// <param name="command">Provide FolderId</param>
        /// <returns>Return true or false</returns>
        [HttpDelete]
        public IActionResult DeleteFolder([FromQuery]DeleteFolder command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get folder by id
        /// </summary>
        /// <param name="folderId">Provide FolderId</param>
        /// <returns>Return details of folder</returns>
        [HttpGet]
        public IActionResult GetFolder(Guid folderId)
        {
            return Ok(_bus.Query(new GetFolder { Id = folderId }));
        }

        /// <summary>
        /// Paste the selected folder from cut or copy
        /// </summary>
        /// <param name="command">Provide existing FolderId</param>
        /// <returns>Return new FolderId</returns>
        [HttpPost]
        public IActionResult PasteFolder(PasteFolder command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a folder
        /// </summary>
        /// <param name="command">Provide FolderId and other Folder attributes</param>
        /// <returns>Return true or false</returns>
        [HttpPut]
        public IActionResult UpdateFolder(UpdateFolder command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion Folders

        #region Settings

        /// <summary>
        /// Add a new setting
        /// </summary>
        /// <param name="command">Provide ProjectId, SubprojectId, SystemId and CatalogId</param>
        /// <returns>Return SettingId</returns>
        [HttpPost]
        public IActionResult AddSettings(AddSettings command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get Setting details
        /// </summary>
        /// <param name="command">Provide ProjectId and SubprojectId</param>
        /// <returns>Return setting details</returns>
        [HttpGet]
        public IActionResult GetSetting([FromQuery] GetSetting command)
        {
            return Ok(_bus.Query(command));
        }

        /// <summary>
        /// Update setting
        /// </summary>
        /// <param name="command">Provide SettingId</param>
        /// <returns>Return true or false</returns>
        [HttpPut]
        public IActionResult UpdateSettings(UpdateSettings command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion Settings

        #region SystemCatalogs

        /// <summary>
        ///
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddSystemCatalog(AddSystemCatalog command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpDelete]
        public IActionResult DeleteSystemCatalog([FromQuery]DeleteSystemCatalog command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpGet]
        public IActionResult GetCatalogSystem([FromQuery] GetCatalogSystem query)
        {
            return Ok(_bus.Query(query));
        }

        [HttpGet]
        public IActionResult GetSystemCatalog([FromQuery] GetSystemCatalog query)
        {
            return Ok(_bus.Query(query));
        }

        [HttpGet]
        public IActionResult GetSystemCatalogs([FromQuery] GetSystemCatalogs query)
        {
            return Ok(_bus.Query(query));
        }

        [HttpPut]
        public IActionResult UpdateSystemCatalog(UpdateSystemCatalog command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion SystemCatalogs

        #region Systems

        /// <summary>
        ///
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddSystem(AddSystem command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpDelete]
        public IActionResult DeleteSystem([FromQuery]DeleteSystem command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpGet]
        public IActionResult GetSystem([FromQuery] GetSystems query)
        {
            return Ok(_bus.Query(query));
        }

        [HttpGet]
        public IActionResult GetSystemById([FromQuery] GetSystemById query)
        {
            return Ok(_bus.Query(query));
        }

        [HttpPut]
        public IActionResult UpdateSystem(UpdateSystem command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion Systems

        #region SystemTags

        /// <summary>
        /// Create a new SystemTag by giving SystemId, Title and Description
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddSystemTag(AddSystemTag command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a SystemTag
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult DeleteSystemTag([FromQuery]DeleteSystemTag command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get a specific SystemTag
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetSystemTag([FromQuery] GetSystemTag query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get all SystemTags of a System
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetSystemTags([FromQuery] GetSystemTags query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Update a SystemTag
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateSystemTag(UpdateSystemTag command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion SystemTags

        #region Catalogs

        [HttpPost]
        public IActionResult AddProjectCatalog(AddProjectCatalog command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpPost]
        public IActionResult AddSubProjectCatalog(AddSubProjectCatalog command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpPost]
        public IActionResult AddTenantCatalog(AddTenantCatalog command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpDelete]
        public IActionResult DeleteCatalog([FromQuery]DeleteCatalog command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpGet]
        public IActionResult GetTenantCatalogs([FromQuery]GetTenantCatalogs query)
        {
            return Ok(_bus.Query(query));
        }

        [HttpPut]
        public IActionResult UpdateCatalog(UpdateCatalog command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion Catalogs

        /// <summary>
        /// Get List of all adapters
        /// </summary>
        /// <returns>Return list of adapters</returns>
        [HttpGet]
        public IActionResult GetAdapters()
        {
            return Ok(_bus.Query(new GetAdapters()));
        }
    }
}