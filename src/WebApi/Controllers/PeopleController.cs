﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commands.PeopleModule;
using Messaging;
using Microsoft.AspNetCore.Mvc;
using Queries.PeopleModule;


namespace WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class PeopleController : SenseAIController
    {
        private readonly IBus _bus;

        public PeopleController(IBus bus)
        {
            _bus = bus;
        }

        #region Role Model Implementation

        /// <summary>
        /// Add a Role
        /// </summary>
        /// <param name="command">Provide Title, description</param>
        /// <returns>Return added Role Id</returns>
        [HttpPost]
        public IActionResult AddRole(AddRole command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a Role
        /// </summary>
        /// <param name="command">Provide Id, Title, description</param>
        /// <returns>Return updated Role Id</returns>
        [HttpPut]
        public IActionResult UpdateRole(UpdateRole command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a Role
        /// </summary>
        /// <param name="command">Provide RoleId</param>
        /// <returns>Success or failure</returns>
        [HttpDelete]
        public IActionResult DeleteRole([FromQuery]DeleteRole command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }
       
        /// <summary>
        /// Get Role by Id
        /// </summary>
        /// <param name="getRole">Provide RoleId</param>
        /// <returns>Return details of Role</returns>
        [HttpGet]
        public IActionResult GetRole([FromQuery]GetRole getRole)
        {
            return Ok(_bus.Query(getRole));
        }
     
        /// <summary>
        /// Get Role
        /// </summary>
        /// <returns>Return list of Roles with details</returns>
        [HttpGet]
        public IActionResult GetRoles([FromQuery]GetRoles getRoles)
        {
            return Ok(_bus.Query(getRoles));
        }

        #endregion

        #region People Model Implementation

        /// <summary>
        /// Add a People
        /// </summary>
        /// <param name="command">Provide roleid,name, surname, address, email</param>
        /// <returns>Return added People Id</returns>
        [HttpPost]
        public IActionResult AddPeople(AddPeople command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a People
        /// </summary>
        /// <param name="command">Provide Id, roleid,name, surname, address, email</param>
        /// <returns>Return updated People Id</returns>
        [HttpPut]
        public IActionResult UpdatePeople(UpdatePeople command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a People
        /// </summary>
        /// <param name="command">Provide PeopleId</param>
        /// <returns>Success or failure</returns>
        [HttpDelete]
        public IActionResult DeletePeople([FromQuery]DeletePeople command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }



        /// <summary>
        /// Get People by Id
        /// </summary>
        /// <param name="getPeople">Provide People</param>
        /// <returns>Return details of People</returns>
        [HttpGet]
        public IActionResult GetPeople([FromQuery]GetPeople getPeople)
        {
            return Ok(_bus.Query(getPeople));
        }
        /// <summary>
        /// Get People by UserFromAdmin
        /// </summary>
        /// <param name="getPeople">Provide People</param>
        /// <returns>Return details of People</returns>
        [HttpGet]
        public IActionResult GetPeopleByUserFromAdmin([FromQuery]GetPeopleByUserFromAdmin getPeople)
        {
            return Ok(_bus.Query(getPeople));
        }
        [HttpGet]
        public IActionResult GetPeoplesByRole([FromQuery]GetPeoplesByRole getPeople)
        {
            return Ok(_bus.Query(getPeople));
        }
        /// <summary>
        /// Get People
        /// </summary>
        /// <returns>Return list of People with details</returns>
        [HttpGet]
        public IActionResult GetPeoples([FromQuery]GetPeoples getPeoples)
        {
            return Ok(_bus.Query(getPeoples));
        }
        /// <summary>
        /// Get People
        /// </summary>
        /// <returns>Return list of People with details</returns>
        [HttpPost]
        public IActionResult GetPeoplesByIds(GetPeoplesByIds getPeoplesByIds)
        {
            return Ok(_bus.Query(getPeoplesByIds));
        }
        #endregion
    }
}