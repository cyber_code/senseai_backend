﻿using Commands.ProcessModule.Hierarchies;
using Commands.ProcessModule.Issues;
using Commands.ProcessModule.JiraIntegration;
using Commands.ProcessModule.Processes;
using Commands.ProcessModule.ProcessResources;
using Commands.ProcessModule.RequirementDocumentGeneration;
using Messaging;
using Microsoft.AspNetCore.Mvc;
using Queries.ProcessModule.Hierarchies;
using Queries.ProcessModule.Issues;
using Queries.ProcessModule.JiraIntegration;
using Queries.ProcessModule.Processes;
using Queries.ProcessModule.ProcessResources;
using System.Net.Http;

namespace WebApi.Controllers
{
    [Route("api/Process/[controller]/[action]")]
    public class BrowseController : SenseAIController
    {
        private readonly IBus _bus;

        public BrowseController(IBus bus)
        {
            _bus = bus;
        }

        #region Process Model Implementation

        /// <summary>
        /// Add a Process
        /// </summary>
        /// <param name="command">Provide Title, HierarchyId</param>
        /// <returns>Return added Process Id</returns>
        [HttpPost]
        public IActionResult AddProcess(AddProcess command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a Process
        /// </summary>
        /// <param name="command">Provide Id, Title, HierarchyId</param>
        /// <returns>Return updated Process Id</returns>
        [HttpPut]
        public IActionResult UpdateProcess(UpdateProcess command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a Process
        /// </summary>
        /// <param name="command">Provide ProcessId</param>
        /// <returns>Success or failure</returns>
        [HttpDelete]
        public IActionResult DeleteProcess([FromQuery] DeleteProcess command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpPost]
        public IActionResult PasteProcess(PasteProcess command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get Process by Id
        /// </summary>
        /// <param name="getProcess">Provide ProcessId</param>
        /// <returns>Return details of Process</returns>
        [HttpGet]
        public IActionResult GetProcess([FromQuery]GetProcess getProcess)
        {
            return Ok(_bus.Query(getProcess));
        }

        /// <summary>
        /// Get Process by HierarchyId
        /// </summary>
        /// <param name="getProcess">Provide HierarchyId</param>
        /// <returns>Return list of processes with details</returns>
        [HttpGet]
        public IActionResult GetProcesses([FromQuery]GetProcesses getProcess)
        {
            return Ok(_bus.Query(getProcess));
        }

        /// <summary>
        /// Get Process  BySubProject
        /// </summary>
        /// <param name="getProcess">Provide SubProjectId</param>
        /// <returns>Return list of processes with details</returns>
        [HttpGet]
        public IActionResult GetProcessesBySubProject([FromQuery]GetProcessesBySubProject getProcess)
        {
            return Ok(_bus.Query(getProcess));
        }

        /// <summary>
        /// Get Process Statistics by ProcessId
        /// </summary>
        /// <param name="getProcessStatistics">Provide ProcessId</param>
        /// <returns>Return list of process statistics with details</returns>
        [HttpGet]
        public IActionResult GetProcessStatistics([FromQuery]GetProcessStatistics getProcessStatistics)
        {
            return Ok(_bus.Query(getProcessStatistics));
        }

        /// <summary>
        /// Renames the Title of a Process
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult RenameProcessTitle(RenameProcessTitle command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Edits the Description of a Process
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult EditProcessDescription(EditProcessDescription command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Changes the HierarchyId of a Process
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult ChangeProcessHierarchy(ChangeProcessHierarchy command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion Process Model Implementation

        #region Process Resources Implementation

        /// <summary>
        /// Add a Process Resources
        /// </summary>
        /// <param name="command">Provide FileName, ProcessId</param>
        /// <returns>Return added Process Resource Id</returns>
        [HttpPost]
        public IActionResult AddProcessResources([FromForm] AddProcessResources command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a Process Resource
        /// </summary>
        /// <param name="command">Provide Id, FileName, ProcessId</param>
        /// <returns>Return updated Process Resource Id</returns>
        [HttpPut]
        public IActionResult UpdateProcessResources([FromForm] UpdateProcessResources command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a Process Resource
        /// </summary>
        /// <param name="command">Provide Id</param>
        /// <returns>Success or failure</returns>
        [HttpDelete]
        public IActionResult DeleteProcessResources([FromQuery]DeleteProcessResourcess command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get Process Resource by Id
        /// </summary>
        /// <param name="query">Provide ProcessResourceId</param>
        /// <returns>Return details of Process</returns>
        [HttpGet]
        public IActionResult GetProcessResource([FromQuery]GetProcessResource query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Download Process Resource by Id
        /// </summary>
        /// <param name="query">Provide ProcessResourceId</param>
        /// <returns>Return details of Process</returns>
        [HttpGet]
        public IActionResult DownloadProcessResource([FromQuery]DownloadProcessResource query)
        {
            var result = _bus.Query(query).Result;
            return File(result.File, "application/octet-stream", result.FileName);
        }

        /// <summary>
        /// Get Process Resources by ProcesId
        /// </summary>
        /// <param name="query">Provide ProcesId</param>
        /// <returns>Return list of process resources with details</returns>
        [HttpGet]
        public IActionResult GetProcessResources([FromQuery]GetProcessResources query)
        {
            return Ok(_bus.Query(query));
        }

        [HttpGet]
        public IActionResult DownloadProcessResources([FromQuery]DownloadProcessResources query)
        {
            var result = _bus.Query(query).Result;

            return File(result[0].File, "application/zip", "Attachments.zip");
           return Ok(_bus.Query(query));
        }

        #endregion Process Resources Implementation

        #region Process Workflows

        /// <summary>
        /// Add new process workflow
        /// </summary>
        /// <param name="command">Provide parameters 'ProcessId', and 'WorkflowId' for adding new process workflow</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddProcessWorkflow(AddProcessWorkflow command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update process workflow
        /// </summary>
        /// <param name="command">Provide parameters 'ProcessId', and 'WorkflowId' for updating process workflow</param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateProcessWorkflow(UpdateProcessWorkflow command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete process workflow
        /// </summary>
        /// <param name="command">Provide parameter Id for delete process workflow</param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult DeleteProcessWorkflow([FromQuery]DeleteProcessWorkflow command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get process workflow by Id
        /// </summary>
        /// <param name="query">Provide parameter Id</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetProcessWorkflow([FromQuery]GetProcessWorkflow query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all process workflows by ProcessId
        /// </summary>
        /// <param name="query">Provide parameter ProcessId</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetProcessWorkflows([FromQuery]GetProcessWorkflows query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all processes workflows by SubProjectId
        /// </summary>
        /// <param name="query">Provide parameter SubProjectId</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllProcessWorkflowsBySubProject([FromQuery]GetAllProcessWorkflowsBySubProject query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all processes testcase by SubProjectId
        /// </summary>
        /// <param name="query">Provide parameter SubProjectId</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllProcessTestCasesBySubProject([FromQuery]GetAllProcessTestCasesBySubProject query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all processes open Issues by SubProjectId
        /// </summary>
        /// <param name="query">Provide parameter SubProjectId</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllProcessOpenIssuesBySubProject([FromQuery]GetAllProcessOpenIssuesBySubProject query)
        {
            return Ok(_bus.Query(query));
        }

        #endregion Process Workflows

        #region Issues Implementation

        /// <summary>
        /// Add new Issue
        /// </summary>
        /// <param name="command">Provide parameters for adding new Issue</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddIssue(AddIssue command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update Issue
        /// </summary>
        /// <param name="command">Provide parameters for update Issue</param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateIssue(UpdateIssue command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete Issue
        /// </summary>
        /// <param name="command">Provide parameter Id</param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult DeleteIssue([FromQuery]DeleteIssue command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get all issues for subproject
        /// </summary>
        /// <param name="query">Provide parameter Subproject</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetIssues([FromQuery]GetIssues query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all issues for subproject with details
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetIssuesWithDetails([FromQuery]GetIssuesWithDetails query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all issues for subproject and issue type
        /// </summary>
        /// <param name="query">Provide parameter Subproject</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetIssuesByIssueType([FromQuery]GetIssuesByIssueType query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all possible issue to be linked with a process the unlinked ones and the one that already linked to this process
        /// </summary>
        /// <param name="query">Provide parameter Subproject</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType([FromQuery]GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get all open issues for backlog
        /// </summary>
        /// <param name="query">Provide parameter Subproject id</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GetIssuesForSprintPlannerWithFilters(GetIssuesForSprintPlannerWithFilters query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Get issue by Id
        /// </summary>
        /// <param name="getIssue">Provide parameter IssueId</param>
        /// <returns>Return issue tpye details</returns>
        [HttpGet]
        public IActionResult GetIssue([FromQuery]GetIssue getIssue)
        {
            return Ok(_bus.Query(getIssue));
        }

        /// <summary>
        /// unlink and issue from the requirement
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UnlinkIssueFromProcess(UnlinkIssueFromProcess command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// link and issue from the requirement
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult LinkIssueToProcess(LinkIssueToProcess command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion Issues Implementation

        #region IssueResources

        [HttpPost]
        public IActionResult AttachIssueResource([FromForm] AttachIssueResource command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpDelete]
        public IActionResult DeleteIssueResource([FromQuery]DeleteIssueResource command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpGet]
        public IActionResult GetIssueResources([FromQuery]GetIssueResources query)
        {
            return Ok(_bus.Query(query));
        }

        /// <summary>
        /// Download Issue Resource by Id
        /// </summary>
        /// <param name="query">Provide IssueResourceId</param>
        /// <returns>Return details of Issue</returns>
        [HttpGet]
        public IActionResult DownloadIssueResource([FromQuery]DownloadIssueResource query)
        {
            var result = _bus.Query(query).Result;
            return File(result.File, "application/octet-stream", result.FileName);
        }

        #endregion IssueResources

        #region IssueComment

        [HttpPost]
        public IActionResult AddIssueComment(AddIssueComment command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpPut]
        public IActionResult EditIssueComment(EditIssueComment command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpDelete]
        public IActionResult DeleteIssueComment([FromQuery]DeleteIssueComment command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpGet]
        public IActionResult GetIssueComments([FromQuery]GetIssueComments query)
        {
            return Ok(_bus.Query(query));
        }

        #endregion IssueComment

        #region Issue Links

        /// <summary>
        /// Link two issues with a given link type (direction source -> destination)
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult LinkIssue(LinkIssue command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Unlink two issues (direction src -> destination)
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult UnlinkIssue([FromQuery] UnlinkIssue command)
        {
            return Ok(_bus.Execute(command));
        }

        /// <summary>
        /// Get all (destination) issues linked to the issue with IssueId
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetLinkedIssues([FromQuery] GetLinkedIssues query)
        {
            return Ok(_bus.Query(query));
        }

        #endregion Issue Links

        #region Hierarchies

        /// <summary>
        /// Create a new Hierarchy by giving ParentId, HierarchyTypeId, Title and Description
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddHierarchy(AddHierarchy command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update a Hierarchy
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateHierarchy(UpdateHierarchy command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete a Hierarchy
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult DeleteHierarchy([FromQuery]DeleteHierarchy command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get a specific Hierarchy by giving its Id
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetHierarchy([FromQuery]GetHierarchy query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get Hierarchies by giving their ParentId
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetHierarchies([FromQuery]GetHierarchies query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get Hierarchies by giving their SubProjectId, HierarchyTypeId and ParentId
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetHierarchiesByParent([FromQuery]GetHierarchiesByParent query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get Hierarchy Statistics by giving SubprojectId and HierarchyId
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetHierarchyStatistics([FromQuery]GetHierarchyStatistics query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get Hierarchies Statistics by giving SubprojectId
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetHierarchiesStatistics([FromQuery]GetHierarchiesStatistics query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Paste an Hierarchy object after applying Copy or Cut
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult PasteHierarchy(PasteHierarchy command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Move Hierarchy
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult MoveHierarchy(MoveHierarchy command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        [HttpGet]
        public IActionResult SearchHierarchies([FromQuery] SearchHierarchies query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Import hierarchies and processes from excel file
        /// </summary>
        /// <param name="command">SubprojectId and excel file</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ImportHierarchy([FromForm] ImportHierarchy command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        #endregion Hierarchies

        #region WorkflowIssues Implementation

        /// <summary>
        /// Add new WorkflowIssue
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddWorkflowIssue(AddWorkflowIssue command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Update existing WorkflowIssue
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateWorkflowIssue(UpdateWorkflowIssue command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete existing WorkflowIssue
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult DeleteWorkflowIssue(DeleteWorkflowIssue command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Delete all WorkflowIssue by WorkflowId and IssueId
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult UnlinkWorkflowIssue(UnlinkWorkflowIssue command)
        {
            var commandResponse = _bus.Execute(command);
            return Ok(commandResponse);
        }

        /// <summary>
        /// Get workflow issue details
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetWorkflowIssue([FromQuery]GetWorkflowIssue query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get all issues linked to a certain workflow
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetWorkflowIssues([FromQuery]GetWorkflowIssues query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get all workflows linked to a certain issue
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetIssueWorkflows([FromQuery]GetIssueWorkflows query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get all workflows linked to a certain issue in a sprint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetSprintIssuesWorkflow([FromQuery]GetSprintIssuesWorkflow query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        /// <summary>
        /// Get all testcase linked to a certain issue in a sprint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetSprintIssuesTestCase([FromQuery]GetSprintIssuesTestCase query)
        {
            var queryResponse = _bus.Query(query);
            return Ok(queryResponse);
        }

        #endregion WorkflowIssues Implementation

        #region Requirement Document Generation

        /// <summary>
        /// Generates hierarchy documentation by date
        /// </summary>
        /// <param name="command">Provide parameters HierarchyId and Date</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GenerateHierarchyDocsByDate([FromQuery]GenerateHierarchyDocsByDate command)
        {
            var response = _bus.Execute(command);
            if (response.Result != null)
                return File(response.Result.File, "application/octet-stream", response.Result.FileName);
            return Ok(response);
        }

        /// <summary>
        /// Generates hierarchy documentation from last version
        /// </summary>
        /// <param name="command">Provide parameters HierarchyId</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GenerateHierarchyDocs([FromQuery]GenerateHierarchyDocs command)
        {
            var response = _bus.Execute(command);
            if (response.Result != null)
                return File(response.Result.File, "application/octet-stream", response.Result.FileName);
            return Ok(response);
        }

        /// <summary>
        /// Generates requirement documentation by date
        /// </summary>
        /// <param name="command">Provide parameters ProcessId and Date</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GenerateRequirementDocsByDate([FromQuery]GenerateRequirementDocsByDate command)
        {
            var response = _bus.Execute(command);
            if (response.Result != null)
                return File(response.Result.File, "application/octet-stream", response.Result.FileName);
            return Ok(response);
        }

        /// <summary>
        /// Generates hierarchy documentation from last version
        /// </summary>
        /// <param name="command">Provide parameters HierarchyId</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GenerateRequirementDocs([FromQuery]GenerateRequirementDocs command)
        {
            var response = _bus.Execute(command);
            if (response.Result != null)
                return File(response.Result.File, "application/octet-stream", response.Result.FileName);
            return Ok(response);
        }

        #endregion Requirement Document Generation
    }
}