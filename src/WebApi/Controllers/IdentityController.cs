﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Commands.Identity;
using WebApi.PolicyManagement.Contracts;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    /// <summary>
    /// IdentityController
    /// </summary>
   // [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class IdentityController : SenseAIController
    {
        private readonly IIdentityServerContract _identityServer;

        #region [ Ctor ]

        /// <summary>
        /// Constructor IdentityController
        /// </summary>
        /// <param name="identityServer"></param>
        public IdentityController(IIdentityServerContract identityServer)
        {
            _identityServer = identityServer;
        }

        #endregion [ Ctor ]

        #region [ Actions ]

        /// <summary>
        /// Login action
        /// </summary>
        /// <param name="signInData">Object containin user credential</param>
        /// <returns></returns>
        [HttpPost]
        //[AllowAnonymous]
        public async Task<IActionResult> SignIn(SignIn signInData)
        {
            var response = await _identityServer.SignIn(signInData.UserName, signInData.Password);

            if (response.IsError)
            {
                return Unauthorized(response.ErrorDescription);
            }
            else
            {
                return Ok(response.Json);
            }
        }

        /// <summary>
        /// Logout action
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> SignOut()
        {
            var idToken = await HttpContext.GetTokenAsync("access_token");
            SignOut("cookie");

            return Ok("signout");
        }

        /// <summary>
        /// Call Identity Token
        /// </summary>
        /// <returns>Returns identity token</returns>
        [HttpGet]
        public async Task<IActionResult> GetIdentityToken()
        {
            var accessToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImJhZWUwZDNkNmU2OTdhYmVhMzhmYjIwNGE5OGQ1MzI4IiwidHlwIjoiSldUIn0.eyJuYmYiOjE1NjE1Nzc4NzMsImV4cCI6MTU2MTU4MTQ3MywiaXNzIjoiaHR0cDovL2lkZW50aXR5LmFwaSIsImF1ZCI6WyJodHRwOi8vaWRlbnRpdHkuYXBpL3Jlc291cmNlcyIsIlJQQVdlYkFwaSIsIlVzZXJQcm9maWxlIl0sImNsaWVudF9pZCI6IlJQQS5XZWJBcGkiLCJzdWIiOiIxIiwiYXV0aF90aW1lIjoxNTYxNTc3ODcyLCJpZHAiOiJsb2NhbCIsInVzZXJfaWQiOiIxIiwidGVuZW5hdF9pZCI6IjU0YjZiODdjLTQxY2QtNGEyMS04ZGRlLTYyODE4NjI0Njc4YiIsIm5hbWUiOiJhZG1pbiIsInByZWZlcnJlZF91c2VybmFtZSI6ImFkbWluIiwiZW1haWwiOiJtYW5hZ2VyQGdtYWlsLmNvbSIsImxpY2Vuc2VfaWQiOiI1Iiwicm9sZSI6IkFkbWluaXN0cmF0b3IsV29ya2Zsb3cgRGVzaWduZXIiLCJzY29wZSI6WyJvcGVuaWQiLCJwcm9maWxlIiwiUlBBV2ViQXBpIiwiVXNlclByb2ZpbGUiXSwiYW1yIjpbImN1c3RvbSJdfQ.S_c1FZRs_E9U26-61qWVvc0LsruEPKptwP_yIE8SHl_NnG1Rctu15-lMZhDQJzcTyIUV_W_bYd3I4TcRyL6V9rkacK7szWhZjWhG0ZlyQRoL3lhbNgeQvoO6rxZjWawpI2ilNADa9ZPzDqgzYJ0YStRqAiK-1qPqCS89AhOGCXb7Y8ZA-aI2SF0u31TKyg_XGcPFJagWgeT3NCu3HZIoe-G29n6FjIua5SIunTDEaruaw8DXOdJ-kvdLBT9XuCiVRmGng47jofIAHA1eVOnMzBLrrl_B7oL2pz6QGgL5hkDgxOeyA-dLeSzzVF1GwA-p8Sb1xwPp3s0m_XL3QkBuKw";// await HttpContext.GetTokenAsync("access_token");
            var content = await _identityServer.GetIdentity(accessToken);
            return Ok(content);
        }
        /// <summary>
        /// Call Identity Token
        /// </summary>
        /// <returns>Returns identity token</returns>
        [HttpGet]
        public async Task<IActionResult> GetAllPermissions()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            if (string.IsNullOrEmpty(accessToken))
                return BadRequest("No identity server activated");
            var content = await _identityServer.GetProfileDataAsync(accessToken);
            return Ok(content);
        }
        #endregion [ Actions ]
    }
}