﻿using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Core;
using Core.Abstractions;
using Core.Configuration;
using Logging.Messaging.Persistence;
using Persistence;
using Persistence.Internal;
using WebApi.Filters;
using WebApi.ModelBinders;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace WebApi.Extensions
{
    public static class ServicesExtensions
    {
        /// <summary>
        /// Configure Swagger
        /// </summary>
        /// <param name="services"></param>
        public static void AddMySwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                // resolve the IApiVersionDescriptionProvider service
                // note: that we have to build a temporary service provider here because one has not been created yet
                var apiVersionDescriptionProvider = EngineContext.Current.Resolve<IApiVersionDescriptionProvider>();

                // add a swagger document for each discovered API version
                // note: you might choose to skip or document deprecated API versions differently
                foreach (var description in apiVersionDescriptionProvider.ApiVersionDescriptions)
                {
                    c.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                }

                // add a custom operation filter which sets default values
                c.OperationFilter<SwaggerOperationFilter>();

                // Set the comments path for the Swagger JSON and UI.
                var basePath = AppContext.BaseDirectory;
                var xmlPath = Path.Combine(basePath, "WebApi.xml");
                c.IncludeXmlComments(xmlPath);

                //c.SwaggerDoc("v1", new Info { Title = "You api title", Version = "v1" });
                c.AddSecurityDefinition("Bearer",
                    new ApiKeyScheme
                    {
                        In = "header",
                        Description = "Please enter into field the word 'Bearer' following by space and JWT",
                        Name = "Authorization",
                        Type = "apiKey"
                    });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                { "Bearer", Enumerable.Empty<string>() },
            });
            });
        }

        /// <summary>
        /// Add Api versioning
        /// </summary>
        /// <param name="services"></param>
        public static void AddMyApiVersioning(this IServiceCollection services)
        {
            services.AddApiVersioning(options =>
            {
                options.ApiVersionReader = new HeaderApiVersionReader("api-version");
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });
        }

        /// <summary>
        /// Add and configure MVC for the application
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration"></param>
        /// <returns>A builder for configuring MVC services</returns>
        public static IMvcBuilder AddSenseAIMvc(this IServiceCollection services, SenseAIConfig configuration)
        {
            //add basic MVC feature
            var mvcBuilder = services.AddMvc();

            mvcBuilder.SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            mvcBuilder.AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            mvcBuilder.AddMvcOptions(opt =>
            {
                opt.Filters.Add(typeof(ValidateModelStateAttribute));
                opt.ModelBinderProviders.Insert(0, new QueryModelBinderProvider());
                //to uncoment for deploy
                if (configuration.EnableIdentityServer)
                {
                    var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                    opt.Filters.Add(new AuthorizeFilter(policy));
                }
            });
            mvcBuilder.AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssemblyContaining<WebApiStartup>();
            });
            return mvcBuilder;
        }

        /// <summary>
        /// Add HttpContextAccessor as a service
        /// </summary>
        /// <param name="services"></param>
        public static void AddMyHttpContextAccesor(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddIdentityServer(this IServiceCollection services, IIdentityServerConfig configuration)
        {
            // services.ConfigureApplicationCookie(options => options.LoginPath = configuration.SignInAction);
            services.AddAuthentication(configuration.AuthenticationScheme)
                .AddIdentityServerAuthentication(configuration.AuthenticationScheme, options =>
                {
                    options.Authority = configuration.Endpoint;
                    options.RequireHttpsMetadata = false;
                    options.ApiName = configuration.ClientName;
                });
        }

        /// <summary>
        /// Register base object context
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="useInMemoryDatabase"></param>
        public static void AddSenseAIObjectContext(this IServiceCollection services, bool useInMemoryDatabase)
        {
            services.AddDbContext<SenseAIObjectContext>(optionsBuilder =>
            {
                // var dataSettings = ((DataSettingsManager)services.BuildServiceProvider().GetService<IDataSettingsManager>()).DataSettings;
                var dataSettings = DataSettingsManager.Instance.DataSettings;

                if (dataSettings == null || !dataSettings.IsValid) return;

                if (useInMemoryDatabase)
                {
                    //in case we are doing integration testing we have to use InMemoryDb
                    optionsBuilder.UseLazyLoadingProxies()
                        .UseInMemoryDatabase("SenseAITestDb");
                }
                else
                {
                    // production Db
                    optionsBuilder.EnableDetailedErrors();
                    optionsBuilder.EnableSensitiveDataLogging();
                    optionsBuilder.UseLazyLoadingProxies()
                        .UseSqlServer(dataSettings.DataConnectionString,
                            options =>
                            {
                                options.MigrationsAssembly("Persistence.Migrations");
                                options.CommandTimeout(600);
                            });
                }
            });
        }

        /// <summary>
        /// Register Messaging Log Context
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="isTesting"></param>
        public static void AddMessagingLogContext(this IServiceCollection services, bool isTesting)
        {
            services.AddDbContext<MessagingLogContext>(optionsBuilder =>
            {
                //var dataSettings = ((DataSettingsManager)services.BuildServiceProvider().GetService<IDataSettingsManager>()).DataSettings;
                var dataSettings = DataSettingsManager.Instance.DataSettings;

                if (dataSettings == null || !dataSettings.IsValid) return;

                if (isTesting)
                {
                    //in case we are doing integration testing we have to use InMemoryDb
                    optionsBuilder.UseLazyLoadingProxies()
                        .UseInMemoryDatabase("SenseAITestDb");
                }
                else
                {
                    // production Db
                    optionsBuilder.UseLazyLoadingProxies()
                        .UseSqlServer(dataSettings.MessagingLogConnectionString,
                            options =>
                            {
                                options.MigrationsAssembly("Logging");
                            });
                }
            });
        }

        #region Helper

        private static Info CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new Info
            {
                Title = $"SenseAI Api {description.ApiVersion}",
                Version = $"Api Version v{description.ApiVersion.ToString()}",
                Description = "This is a demo of SenseAI Web Api",
                Contact = new Contact
                {
                    Name = "Getson Cela",
                    Email = "getson.cela@validata-software.com"
                },
                TermsOfService = "Validata",
                License = new License
                {
                    Name = "Getson Cela"
                }
            };

            if (description.IsDeprecated)
            {
                info.Description += " This Api version has been deprecated.";
            }

            return info;
        }

        #endregion Helper
    }
}