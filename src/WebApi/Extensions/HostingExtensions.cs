﻿using Microsoft.AspNetCore.Hosting;

namespace WebApi.Extensions
{
    public static class HostingExtensions
    {
        public static bool IsTesting(this IHostingEnvironment hostingEnvironement)
        {
            return hostingEnvironement.IsEnvironment("Testing");
        }
    }
}