﻿using Core;
using Core.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using WebApi.Infrastructure;
using WebApi.PolicyManagement;

namespace WebApi.Extensions
{
    /// <summary>
    /// Configure Application services
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        /// configure Swagger
        /// </summary>
        /// <param name="app"></param>
        public static void UseMySwagger(this IApplicationBuilder app)
        {
            var apiVersionDescriptionProvider = EngineContext.Current.Resolve<IApiVersionDescriptionProvider>();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(options =>
            {
                var swaggerJsonBasePath = options.RoutePrefix.IsNullOrEmpty() ? "." : "..";

                // build a swagger endpoint for each discovered API version
                foreach (var description in apiVersionDescriptionProvider.ApiVersionDescriptions)
                {
                    options.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                }
                options.InjectStylesheet($"{swaggerJsonBasePath}/css/swagger.min.css");
            });
        }

        /// <summary>
        /// Configure middleware checking whether database is installed
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public static void UseInstallUrl(this IApplicationBuilder application)
        {
            application.UseMiddleware<InstallMiddleware>();
        }

        /// <summary>
        /// configure execption handlin for api
        /// </summary>
        /// <param name="application"></param>
        public static void UseValidataExceptionHandler(this IApplicationBuilder application)
        {
            application.UseMiddleware<ErrorHandlingMiddleware>();
        }

        public static void UseValidataCors(this IApplicationBuilder application)
        {
            application.UseCors(config =>
            {
                config.AllowAnyOrigin()
                     .AllowAnyMethod()
                     .AllowAnyHeader()
                     .SetIsOriginAllowed((host) => true)
                     .AllowCredentials();
            });
        }

        /// <summary>
        /// Add the policy server claims transformation middleware to the pipeline.
        /// This middleware will turn application roles and permissions into claims and add them to the current user
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns></returns>
        public static IApplicationBuilder UsePolicyServerClaims(this IApplicationBuilder app)
        {
            return app.UseMiddleware<PolicyServerClaimsMiddleware>();
        }
    }
}