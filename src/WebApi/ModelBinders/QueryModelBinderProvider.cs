﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Core;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;

namespace WebApi.ModelBinders
{
    public class QueryModelBinderProvider : IModelBinderProvider
    {
        public QueryModelBinderProvider()
        {
        }

        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            var httpContext = EngineContext.Current.Resolve<IHttpContextAccessor>();
            var @interface = context.Metadata
                                    .ModelType
                                    .GetInterfaces()
                                    .FirstOrDefault(i => i.Name.StartsWith("IQuery"));

            if (((@interface != null && httpContext.HttpContext.Request.Method == "GET") || httpContext.HttpContext.Request.Method == "DELETE"))
            {
                //parsing string arrays into json is not possible
                //with this im skiping bindings when we have string arrays
                if (((DefaultModelMetadata)context.Metadata).Properties.Any(i => i.ModelType.ToString().Equals("System.String[]")))
                {
                    return null;
                }

                return new QueryModelBinder();
            }
            return null;
        }
    }
}