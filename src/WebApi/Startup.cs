﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Common;
using Core;
using Core.Abstractions;
using Core.Abstractions.Bindings;
using Core.Common;
using Core.Configuration;
using Core.Configuration.Bindings;
using Core.Extensions;
using WebApi.Extensions;
using WebApi.Infrastructure;
using System;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]

namespace WebApi
{
    public class Startup
    {
        #region Properties

        /// <summary>
        /// Get Configuration of the application
        /// </summary>
        public IConfigurationRoot Configuration { get; }

        #endregion Properties

        #region Ctor

        /// <inheritdoc />
        public Startup(IHostingEnvironment environment)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
        }

        #endregion Ctor

        /// <summary>
        /// Add services to the application and configure service provider
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //create, initialize and configure the engine
            var provider = services.BuildServiceProvider();
            var hostingEnvironment = provider.GetRequiredService<IHostingEnvironment>();
            var rootPath = GetRootPath(hostingEnvironment);

            #region [ SenseAI Config service ]

            //create instance of config
            var saiConfig = new SenseAIConfig(Configuration, hostingEnvironment.IsTesting());
            //and register it as a service
            services.AddSingleton(saiConfig);

            #endregion [ SenseAI Config service ]

            #region [ IdentityServer Config service ]

            var identityServerConfig = new IdentityServerConfig(Configuration);
            services.AddSingleton<IIdentityServerConfig>(identityServerConfig);

            #endregion [ IdentityServer Config service ]

            #region [ NeuralEngine Config service ]

            var neuralEngineBindingConfiguration = new NeuralEngineBindingConfiguration(Configuration);
            services.AddSingleton<INeuralEngineBindingConfiguration>(neuralEngineBindingConfiguration);

            #endregion [ NeuralEngine Config service ]

            #region [ DataExtraction Config service ]

            var dataExtractionBindingConfiguration = new DataExtractionBindingConfiguration(Configuration);
            services.AddSingleton<IDataExtractionBindingConfiguration>(dataExtractionBindingConfiguration);

            #endregion [ DataExtraction Config service ]

            #region [ QualitySuite Config service ]

            var qualitySuiteBindingConfiguration = new QualitySuiteBindingConfiguration(Configuration);
            services.AddSingleton<IQualitySuiteBindingConfiguration>(qualitySuiteBindingConfiguration);

            #endregion [ QualitySuite Config service ]

            #region [ Jira Config service ]

            var jiraBindingConfiguration = new JiraBindingConfiguration(Configuration);
            services.AddSingleton<IJiraBindingConfiguration>(jiraBindingConfiguration);

            #endregion [ Jira Config service ]

            var engine = (SenseAIWebApiEngine)EngineContext.Create<SenseAIWebApiEngine>();

            DefaultFileProvider.Instance = new SenseAIFileProvider(rootPath);
            engine.Initialize(services, identityServerConfig, DefaultFileProvider.Instance, saiConfig);
            var serviceProvider = engine.ConfigureServices(services, saiConfig);

            return serviceProvider;
        }

        /// <summary>
        /// Configure the application HTTP request pipeline
        /// </summary>
        /// <param name="application"></param>
        public void Configure(IApplicationBuilder application)
        {
            ((SenseAIWebApiEngine)EngineContext.Current).ConfigureRequestPipeline(application);
        }

        private static string GetRootPath(IHostingEnvironment hostingEnvironment)
        {
            if (hostingEnvironment == null || hostingEnvironment.ContentRootPath.IsNullOrEmpty())
            {
                return AppContext.BaseDirectory;
            }
            return hostingEnvironment.ContentRootPath;
        }
    }
}