using Autofac;
using Core.Abstractions;
using Core.Configuration;
using WebApi.Routing;

namespace WebApi.Infrastructure
{
    /// <inheritdoc />
    /// <summary>
    /// Register application services
    /// </summary>
    public sealed class DependencyRegistrar : IDependencyRegistrar
    {
        public int Order => 10;

        /// <inheritdoc />
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, SenseAIConfig config)
        {
            builder.RegisterType<RoutePublisher>().As<IRoutePublisher>().SingleInstance();
            builder.RegisterType<WorkContext>().As<IWorkContext>().InstancePerLifetimeScope();
        }
    }
}