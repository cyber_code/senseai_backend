﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Core;
using Core.Abstractions;
using Core.Configuration;
using System;
using System.Linq;

namespace WebApi.Infrastructure
{
    public class SenseAIWebApiEngine : SenseAIEngine, ISenseAIWebEngine
    {
        /// <summary>
        /// Configure HTTP request pipeline
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public void ConfigureRequestPipeline(IApplicationBuilder application)
        {
            var typeFinder = Resolve<ITypeFinder>();
            //find startup configurations provided by other assemblies
            var startupConfigurations = typeFinder.FindClassesOfType<IWebSenseAIStartup>();

            //create and sort instances of startup configurations
            var instances = startupConfigurations
                //.Where(startup => PluginManager.FindPlugin(startup)?.Installed ?? true) //ignore not installed plugins
                .Select(startup => (IWebSenseAIStartup)Activator.CreateInstance(startup))
                .OrderBy(startup => startup.Order);

            var configuration = Resolve<SenseAIConfig>();

            //configure request pipeline
            foreach (var instance in instances)
                instance.Configure(application, configuration);
        }

        protected override IServiceProvider GetServiceProvider()
        {
            var accessor = ServiceProvider.GetService<IHttpContextAccessor>();
            var context = accessor.HttpContext;
            return context?.RequestServices ?? ServiceProvider;
        }
    }
}