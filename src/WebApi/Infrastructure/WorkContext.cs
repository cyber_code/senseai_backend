﻿using Microsoft.AspNetCore.Http;
using Core.Abstractions;
using Core.Extensions;
using System.Linq;
using System.Net;
using Application.Services;
using Core.Abstractions.Caching;
using System.Net.Http.Headers;

namespace WebApi.Infrastructure
{
    public class WorkContext : IWorkContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string encryptionPass = "123456";
        private readonly IStaticCacheManager _cacheManager;
        private string _userId;
        private string _fullName;
        private string _tenenatId;
        private string accessToken;
        private string _email;
        private string _phone;
        private string _calledFrom;
        private string _ipAddress;
        private string _connectionString;
        private string _messagingLogConnectionString;
        private string _basePath;
        private IEncryptionService _encryption;
        private string _jiraUser;
        private string _jiraToken;

        public WorkContext(IHttpContextAccessor httpContextAccessor,
                           IEncryptionService encryption,
                           IStaticCacheManager cacheManager)
        {
            _httpContextAccessor = httpContextAccessor;
            _encryption = encryption;
            _cacheManager = cacheManager;
        }

        public string TenantId
        {
            get
            {
                if (_tenenatId.IsNullOrEmpty())
                {
                    _tenenatId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "tenenat_id")?.Value;
                }

                return _tenenatId;
            }
        }

        public string AccessToken
        {
            get
            {
                if (accessToken.IsNullOrEmpty())
                {
                    accessToken = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
                    //_tokeId = ((HttpRequestHeaders)_httpContextAccessor.HttpContext.Request.Headers).HeaderAuthorization.FirstOrDefault(); ;
                }

                return accessToken;
            }
        }

        public string ConnectionString
        {
            get
            {
                if (!_connectionString.IsNullOrEmpty())
                {
                    return _connectionString;
                }
                _connectionString = _cacheManager.Get(GetConnectionStringKey(), () =>
               {
                   var con = _httpContextAccessor.HttpContext
                                                 ?.User
                                                 ?.Claims
                                                 .FirstOrDefault(c => c.Type == "connection_string")?.Value;
                   if (con.IsNullOrEmpty())
                   {
                       return null;
                   }
                   return _encryption.Decrypt(con, encryptionPass);
               });

                return _connectionString;
            }
        }

        public string MessagingLogConnectionString
        {
            get
            {
                if (!_messagingLogConnectionString.IsNullOrEmpty())
                {
                    return _messagingLogConnectionString;
                }
                _messagingLogConnectionString = _cacheManager.Get(GetMessagingLogConnectionStringKey(), () =>
                {
                    var mes = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "messagelog_connection_string")?.Value;
                    if (mes.IsNullOrEmpty())
                    {
                        return null;
                    }
                    return _encryption.Decrypt(mes, encryptionPass);
                });

                return _messagingLogConnectionString;
            }
        }

        public string BasePath
        {
            get
            {
                if (_basePath.IsNullOrEmpty())
                {
                    _basePath = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "basepath")?.Value;
                }

                return _basePath;
            }
        }

        public string UserId
        {
            get
            {
                if (_userId.IsNullOrEmpty())
                {
                    _userId = _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "user_id")?.Value;
                }

                return _userId;
            }
        }

        public string FullName
        {
            get
            {
                if (_fullName.IsNullOrEmpty())
                {
                    _fullName = _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "name")?.Value;
                }

                return _fullName;
            }
        }

        public string Email
        {
            get
            {
                if (_email.IsNullOrEmpty())
                {
                    _email = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "email")?.Value;
                }

                return _email;
            }
        }

        public string Phone
        {
            get
            {
                if (_phone.IsNullOrEmpty())
                {
                    _phone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "phone")?.Value;
                }

                return _phone;
            }
        }

        public string JiraUser
        {
            get
            {
                if (_jiraUser.IsNullOrEmpty())
                {
                    _jiraUser = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "JiraUser")?.Value;
                }

                return _jiraUser;
            }
        }

        public string JiraToken
        {
            get
            {
                if (_jiraToken.IsNullOrEmpty())
                {
                    _jiraToken = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "JiraToken")?.Value;
                }

                return _jiraToken;
            }
        }

        public string CalledFrom
        {
            get
            {
                if (_calledFrom.IsNullOrEmpty())
                {
                    _calledFrom = _httpContextAccessor.HttpContext.Request.Path;
                }

                return _calledFrom;
            }
        }

        public string IpAddress
        {
            get
            {
                if (_ipAddress.IsNullOrEmpty())
                {
                    try
                    {
                        _ipAddress = _httpContextAccessor.HttpContext.Request.Host.Value;
                        string ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                        string port = _httpContextAccessor.HttpContext.Connection.RemotePort.ToString();
                        if (!string.IsNullOrEmpty(ip))
                            _ipAddress = ip;

                        if (!string.IsNullOrEmpty(ip) && !string.IsNullOrEmpty(port))
                            _ipAddress = _ipAddress + ":" + port;
                    }
                    catch (System.Exception)
                    {
                        _ipAddress = null;
                    }
                }

                return _ipAddress;
            }
        }

        private string GetConnectionStringKey()
        {
            return "Connection" + TenantId;
        }

        private string GetMessagingLogConnectionStringKey()
        {
            return "MessagingLog" + TenantId;
        }
    }
}