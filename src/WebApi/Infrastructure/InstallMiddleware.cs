﻿using Microsoft.AspNetCore.Http;
using Application.Services;
using Core;
using Core.Abstractions;
using Core.Configuration;
using Persistence;
using System.Threading.Tasks;
using System.Transactions;
using System.Linq;

namespace WebApi.Infrastructure
{
    /// <summary>
    /// Represents middleware that checks whether database is installed and redirects to installation URL in otherwise
    /// </summary>
    public class InstallMiddleware
    {
        #region Fields

        private readonly RequestDelegate _next;

        #endregion Fields

        #region Ctor

        /// <inheritdoc />
        public InstallMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        #endregion Ctor

        #region Methods

        /// <summary>
        /// Invoke middleware actions
        /// </summary>
        /// <param name="context">HTTP context</param>
        /// <returns>Task</returns>
        public async Task Invoke(HttpContext context)
        {
            //whether database is installed
            if (!DataSettingsManager.Instance.DatabaseIsInstalled)
            {
                InstallDatabase();
            }
            else if (!DataSettingsManager.Instance.DatabaseIsUpdated)
            {
                var config = EngineContext.Current.Resolve<SenseAIConfig>();
                if (config.AutoUpdate)
                {
                    UpdateDatabase();
                }
            }
            //or call the next middleware in the request pipeline
            await _next(context);
        }

        private static void UpdateDatabase()
        {
            var dataProvider = EngineContext.Current.Resolve<IDataProvider>();
            //initialize database
            dataProvider.UpdateDatabase();

            DataSettingsManager.Instance.ResetCache();
        }

        private static void InstallDatabase()
        {
            //We should access Installation service and IDataProvider through service locator becuase the DataSettings is
            //not created yet. If we put service interfaces in the constructor Autofac
            //will throw an exception because it
            //cannot find a supported database

            var dataProvider = EngineContext.Current.Resolve<IDataProvider>();
            var installService = EngineContext.Current.Resolve<IInstallationService>();

            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                //initialize database
                dataProvider.InitializeDatabase();
                installService.InstallData(DataSettingsManager.Instance.InstallSampleData || DataSettingsManager.Instance.IsTesting);
            scope.Complete();
        }

        DataSettingsManager.Instance.ResetCache();
        }

        #endregion Methods
    }
}