﻿using Microsoft.AspNetCore.Builder;
using Core.Abstractions;
using Core.Configuration;

namespace WebApi.Infrastructure
{
    /// <inheritdoc />
    /// <summary>
    /// Represents object for the configuring services and middleware on application startup
    /// </summary>
    public interface IWebSenseAIStartup : ISenseAIStartup
    {
        /// <summary>
        /// Configure the using of added middleware
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        /// <param name="configuration"></param>
        void Configure(IApplicationBuilder application, SenseAIConfig configuration);
    }
}