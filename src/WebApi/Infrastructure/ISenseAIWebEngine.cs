﻿using Microsoft.AspNetCore.Builder;
using Core.Abstractions;

namespace WebApi.Infrastructure
{
    public interface ISenseAIWebEngine : IEngine
    {
        /// <summary>
        /// Configure HTTP request pipeline
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        void ConfigureRequestPipeline(IApplicationBuilder application);
    }
}