﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // var configuration = new ConfigurationBuilder()
            //.AddCommandLine(args)
            //.Build();

            // var hostUrl = configuration["hosturl"];
            // if (string.IsNullOrEmpty(hostUrl))
            //     hostUrl = "http://0.0.0.0:5000";

            // var host = new WebHostBuilder()
            //     .UseKestrel()
            //     .UseUrls(hostUrl)   // <!-- this
            //     .UseContentRoot(Directory.GetCurrentDirectory())
            //     .UseIISIntegration()
            //     .UseStartup<Startup>()
            //     .UseConfiguration(configuration)
            //     .Build();

            // host.Run();
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}