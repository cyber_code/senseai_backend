﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Core;
using Core.Abstractions;
using Core.Configuration;
using WebApi.Extensions;
using WebApi.Infrastructure;
using WebApi.PolicyManagement.Infrastructure;
using WebApi.Routing;
using SignalR;

namespace WebApi
{
    public class WebApiStartup : IWebSenseAIStartup
    {
        public int Order => 2;

        public void ConfigureServices(IServiceCollection services, IIdentityServerConfig configuration)
        {
            //add identity server
            services.AddIdentityServer(configuration);
            services.AddValidataPolicies(configuration);
        }

        void ISenseAIStartup.ConfigureServices(IServiceCollection services, SenseAIConfig saiConfiguration)
        {
            services.AddLogging();

            //add object context
            services.AddSenseAIObjectContext(saiConfiguration.IsTesting && !saiConfiguration.TestWithRealDatabase);
            services.AddMessagingLogContext(saiConfiguration.IsTesting);

            //add EF services
            services.AddEntityFrameworkSqlServer();
            services.AddEntityFrameworkProxies();

            //add accessor to HttpContext
            services.AddMyHttpContextAccesor();
            //compression
            services.AddOptions();

            services.AddMvcCore();
            services.AddVersionedApiExplorer();

            //add and configure MVC feature
            services.AddSenseAIMvc(saiConfiguration);

            services.AddResponseCompression();

            //add options feature
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                        builder => builder
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .SetIsOriginAllowed((host) => true)
                        .AllowCredentials());
            });

            //add signalR
            services.AddSignalR(o =>
            {
                o.EnableDetailedErrors = true;
            });

            services.AddMyApiVersioning();

            if (!saiConfiguration.IsTesting)
            {
                services.AddMySwagger();
            }
        }

        void IWebSenseAIStartup.Configure(IApplicationBuilder application, SenseAIConfig configuration)
        {
            //UpdateDatabase(application);
            //compression
            if (configuration.UseResponseCompression)
            {
                //gzip by default
                application.UseResponseCompression();
            }

            application.UseValidataExceptionHandler();

            application.UseCors("CorsPolicy");

            //to uncoment for deploy
            if (configuration.EnableIdentityServer)
            {
#if DEBUG
                Microsoft.IdentityModel.Logging.IdentityModelEventSource.ShowPII = true;
#endif
                application.UseAuthentication();
                application.UsePolicyServerClaims();
            }

            application.UseSignalR(routes =>
            {
                routes.MapHub<NotificationsHub>("/api/notificationhub", options =>
                {
                    options.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransports.All;
                });
            });

            //MVC routing
            application.UseMvc(routeBuilder =>
            {
                //register all routes
                EngineContext.Current.Resolve<IRoutePublisher>().RegisterRoutes(routeBuilder);
            });

            if (configuration.EnableSwagger)
                application.UseMySwagger();

            if (!configuration.IsTesting)
            {
                application.UseInstallUrl();
            }
        }

        //private static void UpdateDatabase(IApplicationBuilder app)
        //{
        //    using (var serviceScope = app.ApplicationServices
        //        .GetRequiredService<IServiceScopeFactory>()
        //        .CreateScope())
        //    {
        //        using (var saiObjectContext = serviceScope.ServiceProvider.GetService<SenseAIObjectContext>())
        //        {
        //            saiObjectContext.Database.Migrate();
        //        }

        //        using (var messagingLogContext = serviceScope.ServiceProvider.GetService<MessagingLogContext>())
        //        {
        //            messagingLogContext.Database.Migrate();
        //        }
        //    }
        //}
    }
}