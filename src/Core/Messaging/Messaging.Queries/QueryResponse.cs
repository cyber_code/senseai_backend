﻿using System;

namespace Messaging.Queries
{
    public sealed class QueryResponse<TQueryResult>
    {
        public QueryResponse()
        {
        }

        public QueryResponse(TQueryResult result, QueryResponseMessage response = null)
        {
            Result = result;
            Response = response;
        }

        public QueryResponse(Exception exception)
        {
            Error = new QueryResponseError(exception);
            Response = new QueryResponseMessage(0, exception.Message); //MessageType 0 from  SenseAI.Domain.MessageType.Failed
        }

        public bool Successful
        {
            get => Error == null;
        }

        public bool HasResult
        {
            get => Result != null;
        }

        public TQueryResult Result { get; private set; }

        public QueryResponseError Error { get; private set; }

        public QueryResponseMessage Response { get; private set; }
    }
}