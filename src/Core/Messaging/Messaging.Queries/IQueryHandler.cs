﻿using System.Collections.Generic;

namespace Messaging.Queries
{
    public interface IQueryHandler<in TQuery, TQueryResult>
        where TQuery : IQuery<TQueryResult>
    {
        List<string> Errors { get; set; }

        QueryResponse<TQueryResult> Handle(TQuery queryObject);

        bool Validate(TQuery query);
    }
}