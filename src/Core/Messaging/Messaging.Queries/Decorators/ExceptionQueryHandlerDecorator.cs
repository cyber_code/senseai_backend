﻿using System;
using System.Collections.Generic;

namespace Messaging.Queries.Decorators
{
    public sealed class ExceptionQueryHandlerDecorator<TQuery, TQueryResult> : BaseQueryHandlerDecorator<TQuery, TQueryResult>
        where TQuery : IQuery<TQueryResult>
    {
        public ExceptionQueryHandlerDecorator(IQueryHandler<TQuery, TQueryResult> handler) : base(handler)
        {
        }

        public override List<string> Errors
        {
            get { return Handler.Errors; }
            set { Handler.Errors = value; }
        }

        public override QueryResponse<TQueryResult> Handle(TQuery queryObject)
        {
            try
            {
                return Handler.Handle(queryObject);
            }
            catch (Exception exception)
            {
                return new QueryResponse<TQueryResult>(exception);
            }
        }

        public override bool Validate(TQuery query)
        {
            return Handler.Validate(query);
        }
    }
}