﻿using System.Collections.Generic;

namespace Messaging.Queries.Decorators
{
    public abstract class BaseQueryHandlerDecorator<TQuery, TQueryResult> : IQueryHandler<TQuery, TQueryResult>
        where TQuery : IQuery<TQueryResult>
    {
        protected readonly IQueryHandler<TQuery, TQueryResult> Handler;

        protected BaseQueryHandlerDecorator(IQueryHandler<TQuery, TQueryResult> handler)
        {
            Handler = handler;
        }

        public abstract List<string> Errors { get; set; }

        public abstract QueryResponse<TQueryResult> Handle(TQuery queryObject);

        public abstract bool Validate(TQuery query);
    }
}