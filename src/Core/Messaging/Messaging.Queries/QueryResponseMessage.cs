﻿namespace Messaging.Queries
{
    public sealed class QueryResponseMessage
    {
        public QueryResponseMessage(int messageType, string message)
        {
            MessageType = messageType;
            Message = message;
        }

        public int MessageType { get; private set; }

        public string Message { get; private set; }
    }
}