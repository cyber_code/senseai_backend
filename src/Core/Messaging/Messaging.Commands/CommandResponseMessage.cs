﻿namespace Messaging.Commands
{
    public sealed class CommandResponseMessage
    {
        public CommandResponseMessage(int messageType, string message)
        {
            MessageType = messageType;
            Message = message;
        }

        public int MessageType { get; private set; }

        public string Message { get; private set; }
    }
}