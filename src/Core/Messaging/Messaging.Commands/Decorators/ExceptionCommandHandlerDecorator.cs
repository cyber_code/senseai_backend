﻿using System;
using System.Collections.Generic;

namespace Messaging.Commands.Decorators
{
    public sealed class ExceptionCommandHandlerDecorator<TCommand, TCommandResult> : BaseCommandHandlerDecorator<TCommand, TCommandResult>
        where TCommand : ICommand<TCommandResult>
    {
        public ExceptionCommandHandlerDecorator(ICommandHandler<TCommand, TCommandResult> handler) : base(handler)
        {
        }

        public override List<string> Errors
        {
            get { return Handler.Errors; }
            set { Handler.Errors = value; }
        }

        public override CommandResponse<TCommandResult> Handle(TCommand command)
        {
            try
            {
                return Handler.Handle(command);
            }
            catch (Exception exception)
            {
                return new CommandResponse<TCommandResult>(exception);
            }
        }

        public override bool Validate(TCommand command)
        {
            return Handler.Validate(command);
        }
    }
}