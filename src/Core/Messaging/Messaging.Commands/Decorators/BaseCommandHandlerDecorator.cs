﻿using System.Collections.Generic;

namespace Messaging.Commands.Decorators
{
    public abstract class BaseCommandHandlerDecorator<TCommand, TCommandResult> : ICommandHandler<TCommand, TCommandResult>
        where TCommand : ICommand<TCommandResult>
    {
        protected readonly ICommandHandler<TCommand, TCommandResult> Handler;

        protected BaseCommandHandlerDecorator(ICommandHandler<TCommand, TCommandResult> handler)
        {
            Handler = handler;
        }

        public abstract List<string> Errors { get; set; }

        public abstract CommandResponse<TCommandResult> Handle(TCommand command);

        public abstract bool Validate(TCommand command);
    }
}