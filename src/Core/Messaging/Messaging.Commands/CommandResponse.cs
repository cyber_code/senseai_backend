﻿using System;

namespace Messaging.Commands
{
    public sealed class CommandResponse<TCommandResult>
    {
        public CommandResponse(TCommandResult result, CommandResponseMessage response = null)
        {
            Result = result;
            Response = response;
        }

        public CommandResponse(Exception exception)
        {
            Error = new CommandResponseError(exception);
            Response = new CommandResponseMessage(0, exception.Message); //MessageType 0 from  SenseAI.Domain.MessageType.Failed
        }

        public bool Successful
        {
            get => Error == null;
        }

        public bool HasResult
        {
            get => Result != null;
        }

        public TCommandResult Result { get; private set; }

        public CommandResponseError Error { get; private set; }

        public CommandResponseMessage Response { get; private set; }
    }
}