﻿using System.Collections.Generic;

namespace Messaging.Commands
{
    public interface ICommandHandler<in TCommand, TCommandResult>
        where TCommand : ICommand<TCommandResult>
    {
        List<string> Errors { get; set; }

        CommandResponse<TCommandResult> Handle(TCommand command);

        bool Validate(TCommand command);
    }
}