﻿namespace Messaging.Events
{
    public sealed class EventResponseMessage
    {
        public EventResponseMessage(string message, bool isValid)
        {
            Message = message;
            IsValid = isValid;
        }

        public string Message { get; private set; }

        public bool IsValid { get; private set; }
    }
}
