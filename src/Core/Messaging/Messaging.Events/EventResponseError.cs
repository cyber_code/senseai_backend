﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messaging.Events
{
    public sealed class EventResponseError
    {
        internal EventResponseError(Exception exception)
        {
            ExceptionType = exception.GetType().Name;
            ExceptionMessage = exception.Message;
            ExceptionObject = exception;
        }

        public string ExceptionType { get; private set; }

        public string ExceptionMessage { get; private set; }

        public Exception ExceptionObject { get; private set; }
    }
}
