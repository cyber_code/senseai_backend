﻿using System.Threading.Tasks;

namespace Messaging.Events
{
    public interface IEventHandler<in TEvent, TEventResult>
        where TEvent : IEvent<TEventResult>
    {
        Task<EventResponse<TEventResult>> Handle(TEvent @event);
    }
}
