﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Messaging.Events.Decorators
{
    public abstract class BaseEventHandlerDecorator<TEvent, TEventResult> : IEventHandler<TEvent, TEventResult>
        where TEvent : IEvent<TEventResult>
    {
        protected readonly IEventHandler<TEvent, TEventResult> Handler;

        protected BaseEventHandlerDecorator(IEventHandler<TEvent, TEventResult> handler)
        {
            Handler = handler;
        }

        public abstract Task<EventResponse<TEventResult>> Handle(TEvent @event);
    }
}
