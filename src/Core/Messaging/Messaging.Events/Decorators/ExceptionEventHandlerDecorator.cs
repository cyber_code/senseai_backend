﻿using System;
using System.Threading.Tasks;

namespace Messaging.Events.Decorators
{
    public sealed class ExceptionEventHandlerDecorator<TEvent, TEventResult> : BaseEventHandlerDecorator<TEvent, TEventResult>
        where TEvent : IEvent<TEventResult>
    {
        public ExceptionEventHandlerDecorator(IEventHandler<TEvent, TEventResult> handler) : base(handler)
        {                
        }

        public override Task<EventResponse<TEventResult>> Handle(TEvent @event)
        {
            throw new NotImplementedException();
        }
    }
}
