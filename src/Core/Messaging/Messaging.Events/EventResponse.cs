﻿using System;

namespace Messaging.Events
{
    public sealed class EventResponse<TEventResult>
    {
        public EventResponse(TEventResult result, EventResponseMessage response = null)
        {
            Result = result;
            Response = response;
        }

        public EventResponse(Exception exception)
        {
            Error = new EventResponseError(exception);
        }

        public bool Successful
        {
            get => Error == null;
        }

        public bool HasResult
        {
            get => Result != null;
        }

        public TEventResult Result { get; private set; }

        public EventResponseError Error { get; private set; }

        public EventResponseMessage Response { get; private set; }
    }
}
