﻿using System;

namespace Messaging
{
    public interface IHandlerResolver
    {
        Type Get(Type type);
    }
}