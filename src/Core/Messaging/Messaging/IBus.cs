﻿using Messaging.Commands;
using Messaging.Queries;

namespace Messaging
{
    public interface IBus
    {
        CommandResponse<TCommandResult> Execute<TCommandResult>(ICommand<TCommandResult> command);

        QueryResponse<TQueryResult> Query<TQueryResult>(IQuery<TQueryResult> queryObject);
    }
}