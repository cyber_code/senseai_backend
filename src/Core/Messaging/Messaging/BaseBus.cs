﻿using Core;
using Messaging.Commands;
using Messaging.Queries;
using SenseAI.Domain;
using System;
using System.Collections.Generic;

namespace Messaging
{
    public abstract class BaseBus : IBus
    {
        private readonly HashSet<Type> _commandHandlerDecorators = new HashSet<Type>();
        private readonly HashSet<Type> _queryHandlerDecorators = new HashSet<Type>();
        private readonly IHandlerResolver _commandHandlerResolver;
        private readonly IHandlerResolver _queryHandlerResolver;

        protected BaseBus(IHandlerResolver commandHandlerResolver, IHandlerResolver queryHandlerResolver)
        {
            _commandHandlerResolver = commandHandlerResolver;
            _queryHandlerResolver = queryHandlerResolver;
        }

        public CommandResponse<TCommandResult> Execute<TCommandResult>(ICommand<TCommandResult> command)
        {
            var handlerType = _commandHandlerResolver.Get(command.GetType());

            dynamic handler = EngineContext.Current.Resolve(handlerType.GetInterfaces()[0]);
            handler.Errors = new List<string>();

            if (handler.Validate((dynamic)command))
            {
                return handler.Handle((dynamic)command);
            }
            else
            {
                string error = handler.Errors.Count > 0 ? handler.Errors[0] : "";
                return new CommandResponse<TCommandResult>(default(TCommandResult), new CommandResponseMessage((int)MessageType.Failed, error));
            }
        }

        public QueryResponse<TQueryResult> Query<TQueryResult>(IQuery<TQueryResult> queryObject)
        {
            var handlerType = _queryHandlerResolver.Get(queryObject.GetType());

            dynamic handler = EngineContext.Current.Resolve(handlerType.GetInterfaces()[0]);
            handler.Errors = new List<string>();

            if (handler.Validate((dynamic)queryObject))
            {
                return handler.Handle((dynamic)queryObject);
            }
            else
            {
                string error = handler.Errors.Count > 0 ? handler.Errors[0] : "";
                return new QueryResponse<TQueryResult>(default(TQueryResult), new QueryResponseMessage((int)MessageType.Failed, error));
            }
        }
    }
}