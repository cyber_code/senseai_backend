﻿using System;
using System.Threading.Tasks;

namespace Messaging.ExternalServices.Decorators
{
    public sealed class ExceptionServiceRequestHandlerDecorator<TServiceRequest, TServiceResponse>
          : BaseServiceRequestHandlerDecorator<TServiceRequest, TServiceResponse> 
            where TServiceRequest: IServiceRequest<TServiceResponse>
    {
        public ExceptionServiceRequestHandlerDecorator(IServiceRequestHandler<TServiceRequest, TServiceResponse> handler) : base(handler)
        {
        }

        public override async Task<ServiceResponse<TServiceResponse>> Handle(TServiceRequest request)
        {
            try
            {
                return await Handler.Handle(request);
            }
            catch (Exception exception)
            {
                return new ServiceResponse<TServiceResponse>(exception);
            }
        }

        public override bool Validate(TServiceRequest request)
        {
            return Handler.Validate(request);
        }
    }
}