﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Messaging.ExternalServices.Decorators
{
    public abstract class BaseServiceRequestHandlerDecorator<TServiceRequest, TServiceResponse> 
        : IServiceRequestHandler<TServiceRequest, TServiceResponse> 
          where TServiceRequest : IServiceRequest<TServiceResponse>
    {
        protected BaseServiceRequestHandlerDecorator(IServiceRequestHandler<TServiceRequest, TServiceResponse> handler)
        {
            Handler = handler;
        }

        protected readonly IServiceRequestHandler<TServiceRequest, TServiceResponse> Handler;

        public abstract Task<ServiceResponse<TServiceResponse>> Handle(TServiceRequest resuest);

        public abstract bool Validate(TServiceRequest resuest);
    }
}
