﻿using System.Threading.Tasks;

namespace Messaging.ExternalServices
{
    public interface IServiceRequestHandler<in TServiceRequest, TServiceResponse>
        where TServiceRequest : IServiceRequest<TServiceResponse>
    {
       // ServiceResponse<TServiceResponse> Handle(TServiceRequest request);
        Task<ServiceResponse<TServiceResponse>> Handle(TServiceRequest request);

        bool Validate(TServiceRequest request);
    }
}