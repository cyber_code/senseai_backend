﻿using System;

namespace Messaging.ExternalServices
{
    public sealed class ServiceResponse<TCommandResult>
    {
        public ServiceResponse(TCommandResult result)
        {
            Result = result;
        }

        public ServiceResponse(Exception exception)
        {
            Error = new ServiceResponseError(exception);
        }

        public bool Successful => Error == null;

        public bool HasResult => Result != null;

        public TCommandResult Result { get; private set; }

        public ServiceResponseError Error { get; private set; }
    }
}