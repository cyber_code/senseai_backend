﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.AdministrationModule.Catalogs
{
    public sealed class AddSubProjectCatalog : ICommand<AddSubProjectCatalogResult>
    {
        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int Type { get; set; }
    }

    public sealed class AddSubProjectCatalogResult
    {
        public AddSubProjectCatalogResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
