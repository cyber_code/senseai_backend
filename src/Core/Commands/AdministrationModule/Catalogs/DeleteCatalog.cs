﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.AdministrationModule.Catalogs
{
    public sealed class DeleteCatalog : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}
