﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.AdministrationModule.Catalogs
{
    public sealed class UpdateCatalog : ICommand<bool>
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int Type { get; set; }
    }
}
