﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.AdministrationModule.Catalogs
{
    public sealed class AddProjectCatalog : ICommand<AddProjectCatalogResult>
    {
        public Guid ProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int Type { get; set; }
    }

    public sealed class AddProjectCatalogResult
    {
        public AddProjectCatalogResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
