﻿using System;
using Messaging.Commands;

namespace Commands.AdministrationModule.SystemTags
{
    public sealed class UpdateSystemTag : ICommand<bool>
    {
        public Guid Id { get; set; }
        public Guid SystemId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}