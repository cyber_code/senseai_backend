﻿using System;
using Messaging.Commands;

namespace Commands.AdministrationModule.SystemTags
{
    public sealed class AddSystemTag : ICommand<AddSystemTagResult>
    {
        public Guid SystemId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }

    public sealed class AddSystemTagResult
    {
        public Guid Id { get; private set; }

        public AddSystemTagResult(Guid id)
        {
            Id = id;
        }
    }
}