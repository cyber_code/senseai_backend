﻿using System;
using Messaging.Commands;

namespace Commands.AdministrationModule.SystemTags
{
    public sealed class DeleteSystemTag : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}