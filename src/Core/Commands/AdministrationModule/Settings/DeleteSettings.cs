﻿using Messaging.Commands;
using System;

namespace Commands.AdministrationModule.Settings
{
    public sealed class DeleteSettings : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}