﻿using Messaging.Commands;
using System;

namespace Commands.AdministrationModule.Settings
{
    public sealed class AddSettings : ICommand<AddSettingsResult>
    {
        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }
    }

    public sealed class AddSettingsResult
    {
        public AddSettingsResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}