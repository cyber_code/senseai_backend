﻿using Messaging.Commands;
using System;

namespace Commands.AdministrationModule.Settings
{
    public sealed class UpdateSettings : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }
    }
}