﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.AdministrationModule.SystemCatalogs
{
    public sealed class UpdateSystemCatalog : ICommand<bool>
    {
        public Guid Id { get; set; } 

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }
    }
}