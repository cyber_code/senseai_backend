﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.AdministrationModule.SystemCatalogs
{
    public sealed class DeleteSystemCatalog : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}