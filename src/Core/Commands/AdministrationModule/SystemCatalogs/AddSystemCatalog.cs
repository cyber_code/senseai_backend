﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.AdministrationModule.SystemCatalogs
{
   public sealed class AddSystemCatalog : ICommand<AddSystemCatalogResult>
    {
        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }
    }
    public sealed class AddSystemCatalogResult
    {
        public AddSystemCatalogResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
