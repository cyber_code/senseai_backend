﻿using Messaging.Commands;
using System;

namespace Commands.AdministrationModule.Projects
{
    public sealed class UpdateSubProjectJiraCredentials : ICommand<bool>
    {
        public Guid Id { get; set; }

        public bool IsJiraIntegrationEnabled { get; set; }
    }
}