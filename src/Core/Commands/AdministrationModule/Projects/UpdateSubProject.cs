﻿using Messaging.Commands;
using System;

namespace Commands.AdministrationModule.Projects
{
    public sealed class UpdateSubProject : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}