﻿using Messaging.Commands;
using System;

namespace Commands.AdministrationModule.Projects
{
    public sealed class AddProject : ICommand<AddProjectResult>
    {
        public string Title { get; set; }

        public string Description { get; set; }
    }

    public sealed class AddProjectResult
    {
        public AddProjectResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}