﻿using Messaging.Commands;
using System;

namespace Commands.AdministrationModule.Projects
{
    public sealed class AddSubProject : ICommand<AddSubProjectResult>
    {
        public Guid ProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }

    public sealed class AddSubProjectResult
    {
        public AddSubProjectResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}