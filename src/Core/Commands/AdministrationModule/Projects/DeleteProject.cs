﻿using Messaging.Commands;
using System;

namespace Commands.AdministrationModule.Projects
{
    public sealed class DeleteProject : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}