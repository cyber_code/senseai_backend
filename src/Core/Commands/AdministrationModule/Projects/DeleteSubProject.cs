﻿using Messaging.Commands;
using System;

namespace Commands.AdministrationModule.Projects
{
    public sealed class DeleteSubProject : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}