﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.AdministrationModule.Systems
{
    public sealed class UpdateSystem : ICommand<bool>
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string AdapterName { get; set; }
    }
}