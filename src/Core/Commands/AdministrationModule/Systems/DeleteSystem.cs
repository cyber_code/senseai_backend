﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.AdministrationModule.Systems
{
   public sealed class DeleteSystem : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}