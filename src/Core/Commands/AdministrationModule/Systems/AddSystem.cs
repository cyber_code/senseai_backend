﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.AdministrationModule.Systems
{
    public sealed class AddSystem : ICommand<AddSystemResult>
    {
        public string AdapterName { get; set; }
        public string Title { get; set; }
    }
    public sealed class AddSystemResult
    {
        public AddSystemResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
