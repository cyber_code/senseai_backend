﻿using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Commands.AAProductBuilderModule
{
    public class HeaderQuestionAnswered : ICommand<bool>
    {
        public List<HeaderQuestionAnsweredAnswers> Answers { get; set; }
    }

    public class HeaderQuestionAnsweredAnswers
    {
        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }

        public string FieldId { get; set; }

        public int MultiValueIndex { get; set; }

        public string Value { get; set; }
    }

    public sealed class HeaderQuestionAnsweredNewArrangementFieldAttribute : ICloneable
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsMultiValue { get; set; }

        public string[] PossibleValues { get; set; }

        public bool IsRequired { get; set; }

        public bool IsNoInput { get; set; }

        public bool IsNoChange { get; set; }

        public bool IsExternal { get; set; }

        public int MinimumLength { get; set; }

        public int MaximumLength { get; set; }

        public string Value { get; set; }

        public int MultiValueIndex { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}