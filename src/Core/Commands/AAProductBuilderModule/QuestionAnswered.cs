﻿using System;
using Messaging.Commands;

namespace Commands.AAProductBuilderModule
{
    public class QuestionAnswered : ICommand<bool>
    {
        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }

        public string PropertyId { get; set; }

        public string FieldId { get; set; }

        public int MultiValueIndex { get; set; }

        public int SubValueIndex { get; set; }

        public string Value { get; set; }

        public string CurrencyId { get; set; }
    }
}
