﻿using Messaging.Commands;
using System;

namespace Commands.AAProductBuilderModule
{
    public sealed class DeployProduct : ICommand<bool>
    {
        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }
    }

    public sealed class Result
    {
        public bool IsDeployed { get; set; }

        public string Error { get; set; }
    }
}
