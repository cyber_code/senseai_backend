﻿using System;
using System.Collections.Generic;
using Messaging.Commands;

namespace Commands.AAProductBuilderModule
{
    public sealed class PropertySelected : ICommand<bool>
    {
        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }

        public string PropertyId { get; set; }

        public string CurrencyId { get; set; }
    }

    public class PropertySelectedConfig
    {
        public PropertySelectedProperty Property { get; set; }

        public List<PropertySelectedAttribute> Attributes { get; set; }
    }

    public class PropertySelectedProperty
    {
        public string PropertyId { get; set; }

        public string PropertyName { get; set; }

        public string CurrencyId { get; set; }
    }

    public class PropertySelectedAttribute
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Value { get; set; }

        public string IsRelativeDate { get; set; }

        public string[] RelativeFields1 { get; set; }

        public string[] RelativeFields2 { get; set; }

        public string[] PossibleValues { get; set; }

        public string RelatedApplication { get; set; }

        public string IsMultiValue { get; set; }

        public string IsRequired { get; set; }

        public string IsNoInput { get; set; }

        public string IsNoChange { get; set; }

        public string IsExternal { get; set; }

        public int MinimumLength { get; set; }

        public int MaximumLength { get; set; }

        public int? FirstGroupId { get; set; }

        public int? SecondGroupId { get; set; }

        public bool ExpandMultiValue { get; set; }

        public bool ExpandSubValue { get; set; }

        public bool ExpandSingleMultiValue { get; set; }

        public bool ExpandSingleSubValue { get; set; }

        public int MultiValueIndex { get; set; } = 1;

        public int SubValueIndex { get; set; } = 1;

        public string MvSvInd { get; set; }
    }
}