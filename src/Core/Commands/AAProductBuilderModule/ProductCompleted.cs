﻿using System;
using Messaging.Commands;

namespace Commands.AAProductBuilderModule
{
    public sealed class ProductCompleted : ICommand<bool>
    {
        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }
    }
}
