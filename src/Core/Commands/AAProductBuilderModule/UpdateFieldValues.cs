﻿using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Commands.AAProductBuilderModule
{
    public sealed class UpdateFieldValues : ICommand<bool>
    {
        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }

        public string PropertyId { get; set; }

        public string CurrencyId { get; set; }

        public List<UpdateFieldValuesFieldValues> FieldValues { get; set; }
    }

    public sealed class UpdateFieldValuesFieldValues
    {
        public string FieldId { get; set; }

        public int MultiValueIndex { get; set; }

        public int SubValueIndex { get; set; }

        public string Value { get; set; }
    }
}