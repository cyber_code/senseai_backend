﻿using Messaging.Commands;
using System;

namespace Commands.AAProductBuilderModule
{
    public sealed class ResetSession : ICommand<bool>
    {
        public Guid SessionId { get; set; }
    }
}
