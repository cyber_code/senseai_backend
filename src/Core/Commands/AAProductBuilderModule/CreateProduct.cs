﻿using System;
using System.Collections.Generic;
using Messaging.Commands;

namespace Commands.AAProductBuilderModule
{
    public sealed class CreateProduct : ICommand<bool>
    {
        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }

        public List<string> Currencies { get; set; }
    }
}