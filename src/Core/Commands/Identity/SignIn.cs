﻿using Messaging.Commands;
using System;

namespace Commands.Identity
{
    public class SignIn : ICommand<SignInResult>
    {

        #region [ Properties ]

        public string UserName { get; set; }

        public string Password { get; set; }

        public string ReturnUrl { get; set; }

        #endregion [ Properties ]
    }

    public sealed class SignInResult
    {
        public SignInResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}