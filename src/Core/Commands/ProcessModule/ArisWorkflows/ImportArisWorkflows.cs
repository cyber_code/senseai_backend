﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.ArisWorkflows
{
    public sealed class ImportArisWorkflows : ICommand<ImportArisWorkflowsResult[]>
    {
        public Guid SubProjectId { get; set; }

        public Guid HierarchyId { get; set; }

        public string FileContent { get; set; }

        public string FileName { get; set; }
    }

    public class ImportArisWorkflowsResult
    {
        public ImportArisWorkflowsResult(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }
    }
}
