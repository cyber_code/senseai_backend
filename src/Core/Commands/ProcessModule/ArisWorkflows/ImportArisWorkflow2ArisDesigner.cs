﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.ArisWorkflows
{
    public sealed class ImportArisWorkflow2ArisDesigner : ICommand<ImportArisWorkflow2ArisDesignerResult[]>
    {
        public string FileContent { get; set; }

        public string FileName { get; set; }

        public Guid ProcessId { get; set; }
    }

    public class ImportArisWorkflow2ArisDesignerResult
    {
        public ImportArisWorkflow2ArisDesignerResult(Guid id, string title, string desingerJson)
        {
            Id = id;
            Title = title;
            DesingerJson = desingerJson;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string DesingerJson { get; private set; }

    }
}
