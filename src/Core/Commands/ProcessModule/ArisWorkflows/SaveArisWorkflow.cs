﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.ArisWorkflows
{
    public sealed class SaveArisWorkflow : ICommand<SaveArisWorkflowResult>
    {
        public Guid Id { get; set; }

        public string DesignerJson { get; set; }
    }

    public sealed class SaveArisWorkflowResult
    {
        public SaveArisWorkflowResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
