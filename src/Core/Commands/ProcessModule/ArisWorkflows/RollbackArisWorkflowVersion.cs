﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.ArisWorkflows
{
    public class RollbackArisWorkflowVersion : ICommand<RollbackArisWorkflowVersionResult>
    {
        public Guid ArisWorkflowId { get; set; }

        public int Version { get; set; }
    }

    public sealed class RollbackArisWorkflowVersionResult
    {
        public RollbackArisWorkflowVersionResult(Guid arisWorkflowId, int version, string designerJson)
        {
            ArisWorkflowId = arisWorkflowId;
            Version = version;
            DesignerJson = designerJson;
        }

        public Guid ArisWorkflowId { get; private set; }

        public int Version { get; private set; }

        public string DesignerJson { get; private set; }
    }
}