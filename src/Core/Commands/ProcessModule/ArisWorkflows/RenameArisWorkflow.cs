﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.ArisWorkflows
{
    public sealed class RenameArisWorkflow : ICommand<RenameArisWorkflowResult>
    { 
        public Guid Id { get; set; }

        public string Title { get; set; }
    }

    public sealed class RenameArisWorkflowResult
    {
        public RenameArisWorkflowResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}