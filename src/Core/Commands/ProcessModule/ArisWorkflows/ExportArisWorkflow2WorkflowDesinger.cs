﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.ArisWorkflows
{
    public sealed class ExportArisWorkflow2WorkflowDesinger : ICommand<bool>
    {
        public Guid SubprojectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid ArisWorkflowId { get; set; }
    }

}
