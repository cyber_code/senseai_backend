﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.ArisWorkflows
{
    public sealed class DeleteArisWorkflow : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}
