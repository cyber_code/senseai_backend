﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.ArisWorkflows
{
    public class IssueArisWorkflow : ICommand<IssueArisWorkflowResult>
    {
        public Guid ArisWorkflowId { get; set; }

        public string ArisWorkflowImage { get; set; }

        public bool NewVersion { get; set; }
    }

    public sealed class IssueArisWorkflowResult
    {
        public IssueArisWorkflowResult(Guid arisWorkflowId)
        {
            ArisWorkflowId = arisWorkflowId; 
        }

        public Guid ArisWorkflowId { get; private set; } 
    }
}