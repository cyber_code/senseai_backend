﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.ArisWorkflows
{
    public sealed class AddArisWorkflow : ICommand<AddArisWorkflowResult>
    {
        public Guid ProcessId { get; set; }

        public string Title { get;  set; }

        public string Description { get; set; }
    }

    public sealed class AddArisWorkflowResult
    {
        public AddArisWorkflowResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
