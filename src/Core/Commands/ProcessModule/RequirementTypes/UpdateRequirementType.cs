﻿using System;
using Messaging.Commands;
using SenseAI.Domain;

namespace Commands.ProcessModule.RequirementTypes
{
    public sealed class UpdateRequirementType : ICommand<bool>
    {
        public Guid Id { get; set; }
        public Guid SubProjectId { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
    }
}