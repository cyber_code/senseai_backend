﻿using System;
using Messaging.Commands;
using SenseAI.Domain;

namespace Commands.ProcessModule.RequirementTypes
{
    public sealed class AddRequirementType : ICommand<AddRequirementTypeResult>
    {
        public Guid SubProjectId { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
    }

    public sealed class AddRequirementTypeResult
    {
        public Guid Id { get; private set; }

        public AddRequirementTypeResult(Guid id)
        {
            Id = id;
        }
    }
}