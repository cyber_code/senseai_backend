﻿using System;
using Messaging.Commands;
using SenseAI.Domain;

namespace Commands.ProcessModule.RequirementTypes
{
    public sealed class DeleteRequirementType : ICommand<bool>
    {
        public Guid Id { get; set; }
        public bool ForceDelete { get; set; }
    }
}