﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Sprints
{
    public sealed class UpdateSprintIssues : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid SprintId { get; set; }

        public Guid IssueId { get; set; }
        public int Status { get; set; }
    }
}