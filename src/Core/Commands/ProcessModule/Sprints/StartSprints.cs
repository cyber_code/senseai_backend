﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Sprints
{
    public sealed class StartSprints : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}