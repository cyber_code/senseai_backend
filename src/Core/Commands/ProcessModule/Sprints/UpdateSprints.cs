﻿using Messaging.Commands;
using SenseAI.Domain;
using System;

namespace Commands.ProcessModule.Sprints
{
    public sealed class UpdateSprints : ICommand<bool>
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public Guid SubProjectId { get; set; }
        public int Duration { get; set; }
        public DurationUnit DurationUnit { get; set; }
    }
}