﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Sprints
{
    public sealed class EndSprints : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}