﻿using Messaging.Commands;
using SenseAI.Domain;
using System;

namespace Commands.ProcessModule.Sprints

{
    public sealed class AddSprints : ICommand<AddSprintsResult>
    {
        public string Title { get; set; }

        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public Guid SubProjectId { get; set; }
        public int Duration { get; set; }
        public DurationUnit DurationUnit { get; set; }
    }

    public sealed class AddSprintsResult
    {
        public AddSprintsResult(Guid id)
        {
            Id = id;
        }
       
        public Guid Id { get; private set; }
    }
}