﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Sprints

{
    public sealed class AddSprintIssues : ICommand<AddSprintIssuesResult>
    {
        public Guid SprintId { get; set; }

        public Guid IssueId { get; set; }
        public int Status { get; set; }
    }

    public sealed class AddSprintIssuesResult
    {
        public AddSprintIssuesResult(Guid id)
        {
            Id = id;
        }
       
        public Guid Id { get; private set; }
    }
}