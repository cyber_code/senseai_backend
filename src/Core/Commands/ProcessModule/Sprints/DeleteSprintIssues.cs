﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Sprints
{
    public sealed class DeleteSprintIssues : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}