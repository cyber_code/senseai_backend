﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Processes
{
    public sealed class AddProcessWorkflow : ICommand<AddProcessWorkflowResult>
    {
        public Guid ProcessId { get; set; }

        public Guid WorkflowId { get; set; }
    }

    public sealed class AddProcessWorkflowResult
    {
        public AddProcessWorkflowResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
