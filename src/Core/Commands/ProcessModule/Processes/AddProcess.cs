﻿using System;
using Messaging.Commands;
using SenseAI.Domain;

namespace Commands.ProcessModule.Processes
{
    public sealed class AddProcess : ICommand<AddProcessResult>
    {
        public Guid SubProjectId { get; set; }

        public Guid HierarchyId { get; set; }

        public Guid CreatedId { get; set; }

        public Guid OwnerId { get; set; }

        public ProcessType ProcessType { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
        public Guid RequirementPriorityId { get; set; }
        public Guid RequirementTypeId { get; set; }

        public int ExpectedNumberOfTestCases { get; set; }

        public Guid TypicalId { get; set; }
    }

    public sealed class AddProcessResult
    {
        public Guid Id { get; private set; }

        public AddProcessResult(Guid id)
        {
            Id = id;
        }
    }
}