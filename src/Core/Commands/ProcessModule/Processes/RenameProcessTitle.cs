﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Processes
{
    public sealed class RenameProcessTitle : ICommand<bool>
    {
        public Guid ProcessId { get; set; }

        public string Title { get; set; }
    }
}