﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Processes
{
    public sealed class EditProcessDescription : ICommand<bool>
    {
        public Guid ProcessId { get; set; }

        public string Description { get; set; }
    }
}