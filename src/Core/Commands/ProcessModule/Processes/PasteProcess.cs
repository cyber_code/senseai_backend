﻿using Messaging.Commands;
using SenseAI.Domain;
using System;

namespace Commands.ProcessModule.Processes
{
    public sealed class PasteProcess : ICommand<PasteProcessResult>
    {
        public Guid ProcessId { get; set; }

        public Guid HierarchyId { get; set; }

        public PasteType Type { get; set; }
    }

    public sealed class PasteProcessResult
    {
        public PasteProcessResult(Guid processId, string processName)
        {
            ProcessId = processId;
            ProcessName = processName;
        }

        public Guid ProcessId { get; private set; }

        public string ProcessName { get; private set; }
    }
}
