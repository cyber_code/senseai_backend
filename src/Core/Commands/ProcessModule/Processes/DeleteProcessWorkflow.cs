﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Processes
{
    public sealed class DeleteProcessWorkflow : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}