﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Processes
{
    public sealed class ChangeProcessHierarchy : ICommand<bool>
    {
        public Guid ProcessId { get; set; }

        public Guid HierarchyId { get; set; }
    }
}