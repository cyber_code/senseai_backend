﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Processes
{
    public sealed class UpdateProcessWorkflow : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid ProcessId { get; set; }

        public Guid WorkflowId { get; set; }
    }
}
