﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Processes
{
    public sealed class DeleteProcess : ICommand<bool>
    {
        public Guid Id { get; set; }

        public bool ForceDelete { get; set; }
    }
}
