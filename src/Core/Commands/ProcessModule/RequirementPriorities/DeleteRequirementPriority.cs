﻿using System;
using Messaging.Commands;
using SenseAI.Domain;

namespace Commands.ProcessModule.RequirementPriorities
{
    public sealed class DeleteRequirementPriority : ICommand<bool>
    {
        public Guid Id { get; set; }
        public bool ForceDelete { get; set; }
    }
}