﻿using System;
using Messaging.Commands;
using SenseAI.Domain;

namespace Commands.ProcessModule.RequirementPriorities
{
    public sealed class AddRequirementPriority : ICommand<AddRequirementPriorityResult>
    {
        public Guid SubProjectId { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
    }

    public sealed class AddRequirementPriorityResult
    {
        public Guid Id { get; private set; }

        public AddRequirementPriorityResult(Guid id)
        {
            Id = id;
        }
    }
}