﻿using System;
using Messaging.Commands;
using SenseAI.Domain;

namespace Commands.ProcessModule.RequirementPriorities
{
    public sealed class UpdateIndexesRequirementPriority : ICommand<bool>
    {
       public RequirementPriorities[] RequirementPriorities { get; set; }
    }

    public sealed class RequirementPriorities
    {
        public Guid Id { get; set; }
        public Guid SubProjectId { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
    }
}