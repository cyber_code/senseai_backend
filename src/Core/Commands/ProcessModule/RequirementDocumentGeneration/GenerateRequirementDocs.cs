﻿using Messaging.Commands;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Commands.ProcessModule.RequirementDocumentGeneration
{
    public sealed class GenerateRequirementDocs : ICommand<GenerateRequirementDocsResult>
    {
        public Guid ProcessId { get; set; }

        public string ApiURL { get; set; }

        public string UiURL { get; set; }
    }

    public sealed class GenerateRequirementDocsResult
    {
        public GenerateRequirementDocsResult(FileStream file, string fileName)
        {
            File = file;
            FileName = fileName;
        }

        public string FileName { get; private set; }

        public FileStream File { get; private set; }
    }
}
