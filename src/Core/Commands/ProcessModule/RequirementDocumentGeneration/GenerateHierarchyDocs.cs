﻿using Messaging.Commands;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Commands.ProcessModule.RequirementDocumentGeneration
{
    public sealed class GenerateHierarchyDocs : ICommand<GenerateHierarchyDocsResult>
    {
        public Guid HierarchyId { get; set; }

        public string ApiURL { get; set; }

        public string UiURL { get; set; }
    }

    public sealed class GenerateHierarchyDocsResult
    {
        public GenerateHierarchyDocsResult(FileStream file, string fileName)
        {
            File = file;
            FileName = fileName;
        }

        public string FileName { get; private set; }

        public FileStream File { get; private set; }
    }
}
