﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public class UpdateJiraUserSubprojectConfigs : ICommand<bool>
    {
        public Guid Id { get; set; }

        public string UserId { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraUsername { get; set; }

        public string JiraToken { get; set; }
    }
}