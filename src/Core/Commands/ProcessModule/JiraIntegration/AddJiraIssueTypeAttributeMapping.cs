﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraIssueTypeAttributeMapping : ICommand<AddJiraIssueTypeAttributeMappingResult>
    {
        public Guid JiraIssueTypeMappingId { get; set; }

        public string SenseaiField { get; set; }

        public string JiraField { get; set; }

        public bool IsMandatory { get; set; }

        public int Index { get; set; }
    }

    public sealed class AddJiraIssueTypeAttributeMappingResult
    {
        public AddJiraIssueTypeAttributeMappingResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}