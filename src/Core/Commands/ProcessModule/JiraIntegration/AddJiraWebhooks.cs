﻿using Messaging.Commands;
using Newtonsoft.Json;
using System;

namespace Commands.ProcessModule.JiraIntegration
{
    public class AddJiraWebhooks : ICommand<bool>
    {
        public Guid SubProjectId { get; set; }

        public string Name { get; set; }

        public string[] Events { get; set; }

        public Filters Filters { get; set; }

        public string Url { get; set; }
    }

    public class Filters
    {
        //[JsonProperty(PropertyName = "issue-related-events-section")]
        public string IssueRelatedEventsSection { get; set; }
    }
}