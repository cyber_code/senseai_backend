﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraUserMapping : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraUserId { get; set; }

        public string SenseaiUserId { get; set; }
    }
}