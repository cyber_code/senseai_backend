﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraUserMapping : ICommand<AddJiraUserMappingResult>
    {
        public Guid SubprojectId { get; set; }

        public string JiraUserId { get; set; }

        public string SenseaiUserId { get; set; }
    }

    public class AddJiraUserMappingResult
    {
        public AddJiraUserMappingResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}