﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.JiraIntegration
{
    public class UpdateJiraWebhooks : ICommand<bool>
    {
        public string Self { get; set; }

        public Guid SubProjectId { get; set; }

        public string Name { get; set; }

        public string[] Events { get; set; }

        public Filters Filters { get; set; }

        public string Url { get; set; }

        public bool Enabled { get; set; }
    }
}