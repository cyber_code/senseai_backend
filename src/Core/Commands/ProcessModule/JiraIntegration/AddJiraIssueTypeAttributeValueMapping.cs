﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraIssueTypeAttributeValueMapping : ICommand<AddJiraIssueTypeAttributeValueMappingResult>
    {
        public Guid JiraIssueTypeAttributeMappingId { get; set; }

        public string SenseaiValue { get; set; }

        public string JiraValue { get; set; }
    }

    public sealed class AddJiraIssueTypeAttributeValueMappingResult
    {
        public AddJiraIssueTypeAttributeValueMappingResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}