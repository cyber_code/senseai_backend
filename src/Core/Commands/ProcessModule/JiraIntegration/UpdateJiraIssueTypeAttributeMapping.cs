﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraIssueTypeAttributeMapping : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid JiraIssueTypeMappingId { get; set; }

        public string SenseaiField { get; set; }

        public string JiraField { get; set; }

        public bool Required { get; set; }

        public int Index { get; set; }
    }
}