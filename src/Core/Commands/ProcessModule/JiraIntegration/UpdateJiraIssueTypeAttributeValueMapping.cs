﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraIssueTypeAttributeValueMapping : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid JiraIssueTypeAttributeMappingId { get; set; }

        public string SenseaiValue { get; set; }

        public string JiraValue { get; set; }
    }
}