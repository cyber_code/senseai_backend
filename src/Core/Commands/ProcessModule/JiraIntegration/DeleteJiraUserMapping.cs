﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class DeleteJiraUserMapping : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}