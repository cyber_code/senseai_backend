﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public class SetJiraUserSubprojectConfigs : ICommand<bool>
    {
        public string UserId { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraUsername { get; set; }

        public string JiraToken { get; set; }

        public string JiraUrl { get; set; }
    }
}