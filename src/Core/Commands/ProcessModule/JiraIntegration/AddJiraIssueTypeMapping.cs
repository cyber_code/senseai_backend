﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraIssueTypeMapping : ICommand<AddJiraIssueTypeMappingResult>
    {
        public Guid Id { get; set; }
        public Guid JiraProjectMappingId { get; set; }

        public string SenseaiIssueTypeField { get; set; }

        public string JiraIssueTypeFieldId { get; set; }

        public string JiraIssueTypeFieldName { get; set; }

        public List<AttributeMapping> AttributeMapping { get; set; }

        public bool AllowAttachments { get; set; }

        public bool isUpdate { get; set; }
    }

    public class AttributeMapping
    {
        public Guid JiraIssueTypeMappingId { get; set; }

        public string SenseaiValue { get; set; }

        public string JiraValue { get; set; }

        public bool IsMandatory { get; set; }

        public int Index { get; set; }

        public List<AttributeValuesMapping> AttributeValuesMapping { get; set; }
    }

    public class AttributeValuesMapping
    {
        public Guid AttributeMappingId { get; set; }

        public string SenseaiValue { get; set; }

        public string JiraValue { get; set; }
    }

    public class AddJiraIssueTypeMappingResult
    {
        public AddJiraIssueTypeMappingResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}