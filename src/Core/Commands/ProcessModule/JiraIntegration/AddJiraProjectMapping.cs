﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraProjectMapping : ICommand<AddJiraProjectMappingResult>
    {
        public Guid ProjectId { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraProjectKey { get; set; }

        public string JiraProjectName { get; set; }

        public bool MapHierarchy { get; set; }

        public Guid HierarchyParentId { get; set; }

        public Guid HierarchyTypeId { get; set; }
    }

    public class AddJiraProjectMappingResult
    {
        public AddJiraProjectMappingResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}