﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraIssueTypeMapping : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid JiraProjectMappingId { get; set; }

        public string SenseaiIssueTypeField { get; set; }

        public string JiraIssueTypeFieldId { get; set; }

        public string JiraIssueTypeFieldName { get; set; }

        public bool AllowAttachments { get; set; }
    }
}