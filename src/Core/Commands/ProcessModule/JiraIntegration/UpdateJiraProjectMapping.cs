﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraProjectMapping : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraProjectKey { get; set; }

        public string JiraProjectName { get; set; }

        public bool MapHierarchy { get; set; }

        public Guid HierarchyParentId { get; set; }

        public Guid HierarchyTypeId { get; set; }
    }
}