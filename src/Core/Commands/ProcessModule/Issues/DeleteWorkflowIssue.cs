﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class DeleteWorkflowIssue : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}