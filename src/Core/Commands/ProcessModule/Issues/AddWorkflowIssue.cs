﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class AddWorkflowIssue : ICommand<AddWorkflowIssueResult>
    {
        public Guid WorkflowId { get; set; }

        public Guid IssueId { get; set; }
    }

    public sealed class AddWorkflowIssueResult
    {
        public AddWorkflowIssueResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}