﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class LinkIssueToProcess : ICommand<bool>
    {
        public Guid IssueId { get; set; }
        public Guid ProcessId { get; set; }
    }
}