﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class UnlinkIssue : ICommand<bool>
    {
        public Guid SrcIssueId { get; set; }
        public Guid DstIssueId { get; set; }
    }
}
