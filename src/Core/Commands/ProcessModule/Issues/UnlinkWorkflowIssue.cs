﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class UnlinkWorkflowIssue : ICommand<bool>
    {
        public Guid WorkflowId { get; set; }
        public Guid IssueId { get; set; }
    }
}