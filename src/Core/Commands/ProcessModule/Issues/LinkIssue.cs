﻿using Messaging.Commands;
using SenseAI.Domain;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class LinkIssue : ICommand<bool>
    {
        public Guid SrcIssueId { get; set; }
        public IssueLinkType LinkType { get; set; }
        public Guid DstIssueId { get; set; }
    }
}
