﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class UnlinkIssueFromProcess : ICommand<bool>
    {
        public Guid IssueId { get; set; }
    }
}