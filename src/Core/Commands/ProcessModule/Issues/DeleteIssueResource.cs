﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.Issues
{
   public sealed class DeleteIssueResource : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}
