﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class UpdateIssueType : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Unit { get; set; }
        public bool Default { get; set; }
         
        public IssueTypeFields[] IssueTypeFields { get; set; }
    }
}