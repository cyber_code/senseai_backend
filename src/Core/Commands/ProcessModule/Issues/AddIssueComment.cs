﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.Issues
{
    public sealed class AddIssueComment : ICommand<AddIssueCommentResult>
    {
        public Guid IssueId { get;  set; }
        public string Comment { get;  set; }
        public Guid CommenterId { get;  set; }
        public Guid ParentId { get;  set; }
    }
    public sealed class AddIssueCommentResult
    {
        public AddIssueCommentResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
