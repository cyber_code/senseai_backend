﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class UpdateWorkflowIssue : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid WorkflowId { get; set; }

        public Guid IssueId { get; set; }
    }
}