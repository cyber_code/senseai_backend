﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.Issues
{
    public sealed class EditIssueComment : ICommand<bool>
    {
        public Guid Id { get; set; }
        public Guid IssueId { get; set; }
        public string Comment { get; set; }
        public Guid CommenterId { get; set; }
        public Guid ParentId { get; set; }
    }

}
