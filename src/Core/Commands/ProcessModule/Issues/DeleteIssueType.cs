﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class DeleteIssueType : ICommand<bool>
    {
        public Guid Id { get; set; }

        public bool ForceDelete { get; set; }
    }
}