﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.Issues
{
    public sealed class AddIssueType : ICommand<AddIssueTypeResult>
    { 
        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Unit { get;  set; }
        public bool Default { get;  set; }

        public IssueTypeFields[] IssueTypeFields { get; set; }
    }

    public sealed class AddIssueTypeResult
    {
        public AddIssueTypeResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }

    public sealed class IssueTypeFields
    {
        public Guid IssueTypeFieldsNameId { get;  set; }

        public bool Checked { get;  set; }
    }
}