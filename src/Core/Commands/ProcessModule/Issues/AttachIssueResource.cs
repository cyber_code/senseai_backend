﻿using Messaging.Commands;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.Issues
{
public sealed class AttachIssueResource : ICommand<AttachIssueResourceResult[]>
    { 
        public Guid IssueId { get;  set; }
        public IFormFileCollection FileCollection { get; set; }
    }
    public sealed class AttachIssueResourceResult
    {
        public AttachIssueResourceResult(Guid id)
        {
            Id = id;
        } 
        public Guid Id { get; private set; }
    }
}
