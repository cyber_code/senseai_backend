﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Hierarchies
{
    public sealed class AddHierarchyType : ICommand<AddHierarchyTypeResult>
    {
        public string ParentId { get; set; }

        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
        public bool ForceDelete { get; set; }
    }

    public sealed class AddHierarchyTypeResult
    {
        public Guid Id { get; private set; }

        public AddHierarchyTypeResult(Guid id)
        {
            Id = id;
        }
    }
}