﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Hierarchies
{
    public sealed class UpdateHierarchyType : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid ParentId { get; set; }

        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}