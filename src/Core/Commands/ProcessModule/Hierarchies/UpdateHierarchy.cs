﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Hierarchies
{
    public sealed class UpdateHierarchy : ICommand<bool>
    {
        public Guid Id { get; set; }

        public string ParentId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid HierarchyTypeId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}