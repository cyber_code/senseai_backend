﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Hierarchies
{
    public sealed class MoveHierarchyType : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}