﻿using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.ProcessModule.Hierarchies
{
    public sealed class ImportModelStructure
    {
        public ImportModelStructure(List<Hierarchy> listHierarchies, List<Process> listProcessess)
        {
            ListHierarchies = listHierarchies;
            ListProcessess = listProcessess;
        }

        public List<Hierarchy> ListHierarchies { get; set; }

        public List<Process> ListProcessess { get; set; }
    }
}