﻿using Messaging.Commands;
using SenseAI.Domain.Process.HierarchiesModel;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace Commands.ProcessModule.Hierarchies
{
    public sealed class ImportHierarchy : ICommand<bool>
    {
        public Guid SubprojectId { get; set; }

        public IFormFile File { get; set; }

        public bool ForceDelete { get; set; }
    }
}