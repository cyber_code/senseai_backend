﻿using System;
using Messaging.Commands;
using SenseAI.Domain;

namespace Commands.ProcessModule.Hierarchies
{
    public sealed class PasteHierarchy : ICommand<PasteHierarchyResult>
    {
        public Guid HierarchyId { get; set; }

        public Guid ParentId { get; set; }

        public PasteType PasteType { get; set; }
    }

    public sealed class PasteHierarchyResult
    {
        public PasteHierarchyResult(Guid hierarchyId, string title)
        {
            HierarchyId = hierarchyId;
            Title = title;
        }

        public Guid HierarchyId { get; private set; }

        public string Title { get; private set; }
    }
}