﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Hierarchies
{
    public sealed class AddHierarchy : ICommand<AddHierarchyResult>
    {
        public string ParentId { get; set; }

        public Guid SubProjectId { get; set; }

        public string HierarchyTypeId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }

    public sealed class AddHierarchyResult
    {
        public Guid Id { get; private set; }

        public AddHierarchyResult(Guid id)
        {
            Id = id;
        }
    }
}