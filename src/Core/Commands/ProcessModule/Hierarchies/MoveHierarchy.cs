﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Hierarchies
{
    public sealed class MoveHierarchy : ICommand<bool>
    {
        public Guid FromHierarchyId { get; set; }

        public Guid ToHierarchyId { get; set; }
    }
}