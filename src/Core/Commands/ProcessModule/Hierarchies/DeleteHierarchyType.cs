﻿using System;
using Messaging.Commands;

namespace Commands.ProcessModule.Hierarchies
{
    public sealed class DeleteHierarchyType : ICommand<bool>
    {
        public Guid Id { get; set; }

        public bool ForceDelete { get; set; }
    }
}