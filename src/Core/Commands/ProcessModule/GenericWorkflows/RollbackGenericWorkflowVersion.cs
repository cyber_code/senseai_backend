﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.GenericWorkflows
{
    public class RollbackGenericWorkflowVersion : ICommand<RollbackGenericWorkflowVersionResult>
    {
        public Guid GenericWorkflowId { get; set; }

        public int Version { get; set; }
    }

    public sealed class RollbackGenericWorkflowVersionResult
    {
        public RollbackGenericWorkflowVersionResult(Guid genericWorkflowId, int version, string designerJson)
        {
            GenericWorkflowId = genericWorkflowId;
            Version = version;
            DesignerJson = designerJson;
        }

        public Guid GenericWorkflowId { get; private set; }

        public int Version { get; private set; }

        public string DesignerJson { get; private set; }
    }
}