﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.GenericWorkflows
{
    public sealed class DeleteGenericWorkflow : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}
