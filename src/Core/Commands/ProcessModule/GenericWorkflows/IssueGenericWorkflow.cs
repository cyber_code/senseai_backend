﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.GenericWorkflows
{
    public class IssueGenericWorkflow : ICommand<IssueGenericWorkflowResult>
    {
        public Guid GenericWorkflowId { get; set; }

        public string GenericWorkflowImage { get; set; }

        public bool NewVersion { get; set; }
    }

    public sealed class IssueGenericWorkflowResult
    {
        public IssueGenericWorkflowResult(Guid genericWorkflowId)
        {
            GenericWorkflowId = genericWorkflowId; 
        }

        public Guid GenericWorkflowId { get; private set; } 
    }
}