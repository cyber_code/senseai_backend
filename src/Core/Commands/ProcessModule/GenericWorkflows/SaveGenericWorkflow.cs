﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.GenericWorkflows
{
    public sealed class SaveGenericWorkflow : ICommand<SaveGenericWorkflowResult>
    {
        public Guid Id { get; set; }

        public string DesignerJson { get; set; }
    }

    public sealed class SaveGenericWorkflowResult
    {
        public SaveGenericWorkflowResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
