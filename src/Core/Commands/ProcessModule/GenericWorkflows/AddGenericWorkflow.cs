﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.GenericWorkflows
{
    public sealed class AddGenericWorkflow : ICommand<AddGenericWorkflowResult>
    {
        public Guid ProcessId { get; set; }

        public string Title { get;  set; }

        public string Description { get; set; }
    }

    public sealed class AddGenericWorkflowResult
    {
        public AddGenericWorkflowResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
