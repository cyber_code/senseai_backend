﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.GenericWorkflows
{
    public sealed class RenameGenericWorkflow : ICommand<RenameGenericWorkflowResult>
    { 
        public Guid Id { get; set; }

        public string Title { get; set; }
    }

    public sealed class RenameGenericWorkflowResult
    {
        public RenameGenericWorkflowResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}