﻿using Messaging.Commands;
using System;

namespace Commands.ProcessModule.ProcessResources
{
    public sealed class DeleteProcessResourcess : ICommand<bool>
    {
        public Guid Id { get;  set; }
    }
}
