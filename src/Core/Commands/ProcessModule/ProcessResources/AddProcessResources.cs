﻿using Messaging.Commands;
using Microsoft.AspNetCore.Http;
using System; 

namespace Commands.ProcessModule.ProcessResources
{
    public sealed class AddProcessResources : ICommand<AddProcessResourcesResult[]>
    {  
        public Guid ProcessId { get; set; }
        public IFormFileCollection FileCollection { get; set; }
    }

    public sealed class AddProcessResourcesResult
    {
        public AddProcessResourcesResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
