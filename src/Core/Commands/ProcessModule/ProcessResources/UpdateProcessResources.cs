﻿using Messaging.Commands;
using Microsoft.AspNetCore.Http;
using System;

namespace Commands.ProcessModule.ProcessResources
{
    public sealed class UpdateProcessResources : ICommand<bool>
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public Guid ProcessId { get; set; }
        public IFormFile File { get; set; }
    }
}
