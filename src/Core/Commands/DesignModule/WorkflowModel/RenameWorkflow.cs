﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.WorkflowModel
{
    public sealed class RenameWorkflow : ICommand<bool>
    { 
        public string Title { get; set; }

        public Guid WorkflowId { get; set; }
    }
}