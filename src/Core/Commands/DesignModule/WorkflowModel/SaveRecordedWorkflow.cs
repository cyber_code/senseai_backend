﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.WorkflowModel
{
    public sealed class SaveRecordedWorkflow : ICommand<bool>
    {
        public Header Header { get; set; }

        public Guid WorkflowId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public WorkflowItem[] Items { get; set; }

        public WorkflowLinks[] ItemLinks { get; set; }

        public TEDataSets[] DataSet { get; set; }
    }

    public class TEDataSets
    {
        public Guid Id { get; set; }

        public TETypical Typical { get; set; }
        public TEAttributes[] Attributes { get; set; }
    }

    public class TEAttributes
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }

    public class TETypical
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }

    public sealed class Header
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public Guid FolderId { get; set; }
    }

    public sealed class WorkflowItem
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public Guid CatalogId { get; set; }

        public string CatalogName { get; set; }

        public Guid TypicalId { get; set; }

        public string TypicalName { get; set; }

        public string TypicalType { get; set; }

        public int ActionType { get; set; }

        public string Parameters { get; set; }

        public Constraints[] Constraints { get; set; }

        public Constraints[] PostConstraints { get; set; }

        public DynamicDatas[] DynamicDatas { get; set; }

        public bool IsNegativeStep { get; set; }

        public string ExpectedError { get; set; }

        public Guid SystemId { get; set; }
    }

    public class DynamicDatas
    {
        public Guid SourceWorkflowItemId { get; set; }

        public string SourceAttributeName { get; set; }

        public string TargetAttributeName { get; set; }

        public byte[] PlainTextFormula { get; set; }

        public string Path { get; set; }

        public short? UiMode { get; set; }

        public string PathName { get; set; }
    }

    public class Constraints
    {
        public string AttributeName { get; set; }

        public string Operator { get; set; }

        public string AttributeValue { get; set; }

        public DynamicDatas DynamicData { get; set; }

        public bool IsPostFilter { get; set; }
    }

    public sealed class WorkflowLinks
    {
        public Guid FromId { get; set; }

        public Guid ToId { get; set; }

        public WorkflowLinkState State { get; set; }
    }

    public enum WorkflowLinkState
    {
        None,
        Yes,
        No
    }

    public sealed class EnquiryAction
    {
        public string Action { get; set; }

        public string Type { get; set; }
    }
}
