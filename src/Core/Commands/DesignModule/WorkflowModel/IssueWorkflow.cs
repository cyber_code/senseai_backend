﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.WorkflowModel
{
    public class IssueWorkflow : ICommand<IssueWorkflowResult>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public Guid WorkflowId { get; set; }

        public string WorkflowImage { get; set; }

        public bool NewVersion { get; set; }

        public bool SkipStatus { get; set; }

        public IssueVersioning VersionStatus { get; set; }

        public int  CurrentVersionId { get; set; }

        public bool IsRollBack { get; set; }
    }

    public sealed class IssueWorkflowResult
    {
        public IssueWorkflowResult(Guid workflowId, string workflowImage)
        {
            WorkflowId = workflowId;
            WorkflowImage = workflowImage;
        }

        public Guid WorkflowId { get; private set; }

        public string WorkflowImage { get; private set; }
    }

    public enum IssueVersioning
    {
        LinkedUpagrade = 0,
        LinkedNewVersion = 1,
        NoAction = 2
    }
}