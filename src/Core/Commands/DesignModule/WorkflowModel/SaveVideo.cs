﻿using Messaging.Commands;
using System;
using System.IO;

namespace Commands.DesignModule.WorkflowModel
{
    public sealed class SaveVideo : ICommand<bool>
    {
        public Guid WorkflowId { get; set; }
        public byte [] VideoFile { get; set; }
    }
}
