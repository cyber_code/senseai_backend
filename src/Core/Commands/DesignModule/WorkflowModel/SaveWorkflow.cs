﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.WorkflowModel
{
    public sealed class SaveWorkflow : ICommand<SaveWorkflowResult>
    {
        public Guid Id { get; set; }

        public string Json { get; set; }

        public string WorkflowPathsJson { get; set; }
    }

    public sealed class SaveWorkflowResult
    {
        public SaveWorkflowResult(Guid id, string json)
        {
            Id = id;
            Json = json;
        }

        public Guid Id { get; private set; }

        public string Json { get; set; }
    }
}