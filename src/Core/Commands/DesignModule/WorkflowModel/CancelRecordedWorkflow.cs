﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.WorkflowModel
{
    public sealed class CancelRecordedWorkflow: ICommand<bool>
    {
        public Guid WorkflowId { get; set; }
    }
}
