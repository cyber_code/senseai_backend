﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.WorkflowModel
{
    public sealed class DeleteWorkflow : ICommand<bool>
    { 
        public Guid Id { get; set; }
    }
}