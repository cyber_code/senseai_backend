﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.WorkflowModel
{
    public sealed class AddWorkflow : ICommand<AddWorkflowResult>
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public Guid FolderId { get; set; }
    }

    public sealed class AddWorkflowResult
    {
        public AddWorkflowResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}