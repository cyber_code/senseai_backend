﻿using Messaging.Commands;
using System;
using SenseAI.Domain.WorkflowModel;

namespace Commands.DesignModule.WorkflowModel
{
    public sealed class ImportArisWorkflow : ICommand<bool>
    {
        public string File { get; set; }

        public string FileName { get; set; }

        public Guid FolderId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }
    }

}