﻿using SenseAI.Domain;
using Messaging.Commands;
using System;

namespace Commands.DesignModule.WorkflowModel
{
    public sealed class PasteWorkflow : ICommand<PasteWorkflowResult>
    { 
        public Guid WorkflowId { get; set; }

        public Guid FolderId { get; set; }

        public PasteType Type { get; set; }
    }

    public sealed class PasteWorkflowResult
    {
        public PasteWorkflowResult(Guid workflowId, string workflowName)
        {
            WorkflowId = workflowId;
            WorkflowName = workflowName;
        }

        public Guid WorkflowId { get; private set; }

        public string WorkflowName { get; private set; }
    }
}