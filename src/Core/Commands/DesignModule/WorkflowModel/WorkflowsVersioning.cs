﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.WorkflowModel
{
    public sealed class WorkflowsVersioning : ICommand<bool>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public bool Generate { get; set; }

        public Guid WorkflowId { get; set; }
    }
}
