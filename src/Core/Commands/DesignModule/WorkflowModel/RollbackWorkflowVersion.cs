﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.WorkflowModel
{
    public class RollbackWorkflowVersion : ICommand<RollbackWorkflowVersionResult>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public Guid WorkflowId { get; set; }

        public int Version { get; set; }

        public bool SkipStatus { get; set; }
    }

    public sealed class RollbackWorkflowVersionResult
    {
        public RollbackWorkflowVersionResult(Guid workflowId, int version, string designerJson)
        {
            WorkflowId = workflowId;
            Version = version;
            DesignerJson = designerJson;
        }

        public Guid WorkflowId { get; private set; }

        public int Version { get; private set; }

        public string DesignerJson { get; private set; }
    }
}