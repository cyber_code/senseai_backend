﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.DesignModule.Suggestions
{
    public sealed class SaveComputerGeneratedWorkflows : ICommand<SaveComputerGeneratedWorkflowsResult>
    {
        public List<ComputerGeneratedWorkflows> Workflows { get; set; }
    }

    public sealed class ComputerGeneratedWorkflows
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public Guid FolderId { get; set; }

        public string Json { get; set; }

        public string WorkflowPathsJson { get; set; }
    }

    public sealed class SaveComputerGeneratedWorkflowsResult
    {
        public SaveComputerGeneratedWorkflowsResult(List<Guid> workflowIds)
        {
            WorkflowIds = workflowIds;
        }

        public List<Guid> WorkflowIds { get; set; }
    }
}