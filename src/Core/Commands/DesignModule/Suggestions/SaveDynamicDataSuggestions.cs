﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.Suggestions
{
    public sealed class SaveDynamicDataSuggestions : ICommand<SaveDynamicDataSuggestionsResult>
    {
        public string SourceTypicalName { get;  set; }

        public string SourceAttributeName { get;  set; }

        public string TargetTypicalName { get;  set; }

        public string TargetAttributeName { get;  set; }
    }

    public sealed class SaveDynamicDataSuggestionsResult
    {
        public SaveDynamicDataSuggestionsResult(Guid id)
        {
            Id = id;
        }

        public System.Guid Id { get; private set; }
       
    }
}