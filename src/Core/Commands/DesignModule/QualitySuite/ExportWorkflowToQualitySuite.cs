﻿using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Commands.DesignModule.QualitySuite
{
    public sealed class ExportWorkflowToQualitySuite : ICommand<ExportWorkflowToQualitySuiteResult>
    {
        public Guid WorkflowId { get; set; }      

        public Guid WorkflowPathId { get; set; }

        public ulong ProjectId { get; set; }

        public ulong SubProjectId { get; set; }

        public bool MaintainManualSteps { get; set; }

        public Catalogs[] Catalogs { get; set; }

        public Systems[] Systems { get; set; }
    }

    public sealed class Catalogs
    {
        public string From { get; set; }
        public string To { get; set; }
    }

    public sealed class Systems
    {
        public string From { get; set; }
        public string To { get; set; }
    }

    public sealed class ExportWorkflowToQualitySuiteResult
    {
        public ExportWorkflowToQualitySuiteResult(int totlalTestCases, int successfullySaved, List<string> errors)
        {
            TotlalTestCases = totlalTestCases;
            SuccessfullySaved = successfullySaved;
            Errors = errors;
        }

        public int TotlalTestCases { get; set; }

        public int SuccessfullySaved { get; set; }

        public List<string> Errors { get; set; }
    }
}