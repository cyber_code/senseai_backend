﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.Folder
{
    public sealed class PasteFolder : ICommand<PastFolderResult>
    {
        public Guid FolderId { get; set; }
    }

    public sealed class PastFolderResult
    {
        public PastFolderResult(Guid folderId)
        {
            FolderId = folderId;
        }

        public Guid FolderId { get; private set; }
    }
}