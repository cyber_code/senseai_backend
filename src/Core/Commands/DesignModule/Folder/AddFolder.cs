﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.Folder
{
    public sealed class AddFolder : ICommand<AddFolderResult>
    {
        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }

    public sealed class AddFolderResult
    {
        public AddFolderResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}