﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.Folder
{
    public sealed class DeleteFolder : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}