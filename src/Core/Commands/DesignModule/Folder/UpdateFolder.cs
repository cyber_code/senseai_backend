﻿using Messaging.Commands;
using System;

namespace Commands.DesignModule.Folder
{
    public sealed class UpdateFolder : ICommand<bool>
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}