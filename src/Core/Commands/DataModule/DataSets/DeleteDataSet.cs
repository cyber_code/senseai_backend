﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.DataSets
{
    public sealed class DeleteDataSet : ICommand<DeleteDataSetResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class DeleteDataSetResult
    {
        public DeleteDataSetResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}