﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.DataSets
{
    public sealed class AddManualDataSet : ICommand<AddManualDataSetResult>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public Guid TypicalId { get; set; }

        public string TypicalName { get; set; }

        public string TypicalType { get; set; }

        public string Title { get; set; }

        public decimal Coverage { get; set; }

        public Combinations[] Combinations { get; set; }
    }

    public sealed class AddManualDataSetResult
    {
        public AddManualDataSetResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}