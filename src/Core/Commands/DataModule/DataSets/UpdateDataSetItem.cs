﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.DataSets
{
    public sealed class UpdateDataSetItem : ICommand<UpdateDataSetItemResult>
    {
        public Guid Id { get; set; }

        public Guid DataSetId { get; set; }

        public Rows Row { get; set; }
    }

    public sealed class UpdateDataSetItemResult
    {
        public UpdateDataSetItemResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}