﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.DataSets
{
    public sealed class UpdateManualDataSet : ICommand<UpdateManualDataSetResult>
    {
        public Guid Id { get; set; }

        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public Guid WorkflowId { get; set; }

        public Guid WorkflowPathId { get; set; }

        public Guid WorkflowPathItemId { get; set; }

        public Guid TypicalId { get; set; }

        public string TypicalName { get; set; }

        public string TypicalType { get; set; }

        public string Title { get; set; }

        public decimal Coverage { get; set; }

        public Combinations[] Combinations { get; set; }

        public UpdateStatus UpdateStatus { get; set; }

        public int  Index{ get; set; }
    }

    public sealed class UpdateManualDataSetResult
    {
        public UpdateManualDataSetResult(Guid id, bool isLinkedLastVersion, bool isLinkedNotLastVersion)
        {
            Id = id;
            IsLinkedLastVersion = isLinkedLastVersion;
            IsLinkedNotLastVersion = isLinkedNotLastVersion;
        }

        public Guid Id { get; private set; }

        public bool IsLinkedLastVersion { get; private set; }

        public bool IsLinkedNotLastVersion { get; private set; }
    }

    public enum UpdateStatus
    {
        NoAction = 1,
        LastVersionUpdate = 2,
        LastVersionCreateNew = 3,
        NotLastVersionCreateNew = 4
    }
}