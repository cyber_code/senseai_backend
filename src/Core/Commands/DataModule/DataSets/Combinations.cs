﻿using System.Collections.Generic;

namespace Commands.DataModule.DataSets
{
    public sealed class Combinations
    {
        public string Attribute { get; set; }

        public List<string> Values { get; set; }
    }
}