﻿using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Commands.DataModule.DataSets
{
    public sealed class AddDataSet : ICommand<AddDataSetResult>
    {
        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public Guid TypicalId { get; set; }

        public string Title { get; set; }

        public decimal Coverage { get; set; }

        public Rows[] Rows { get; set; }
    }

    public sealed class AddDataSetResult
    {
        public AddDataSetResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }

    public sealed class Rows
    {
        public List<Attributes> Attributes { get;set; }
    }

    public sealed class Attributes
    {
        public Attributes(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; private set; }

        public string Value { get; private set; }
    }
}