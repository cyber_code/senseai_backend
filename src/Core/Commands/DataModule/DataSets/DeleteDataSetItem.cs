﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.DataSets
{
    public sealed class DeleteDataSetItem : ICommand<DeleteDataSetItemResult>
    {
        public Guid Id { get; set; }

        public Guid WorkflowId { get; set; }

        public Guid WorkflowPathId { get; set; }

        public Guid WorkflowPathItemId { get; set; }

        public Guid DataSetId { get; set; }

        public DeleteStatus DeleteStatus { get; set; }

        public int Index { get; set; }
    }

    public sealed class DeleteDataSetItemResult
    {
        public DeleteDataSetItemResult(Guid id, bool isLinkedLastVersion, bool isLinkedNotLastVersion)
        {
            Id = id;
            IsLinkedLastVersion = isLinkedLastVersion;
            IsLinkedNotLastVersion = isLinkedNotLastVersion;
        }

        public bool IsLinkedLastVersion { get; private set; }

        public bool IsLinkedNotLastVersion { get; private set; }

        public Guid Id { get; private set; }
    }

    public enum DeleteStatus
    {
        NoAction = 1,
        LastVersionUpdate = 2,
        LastVersionCreateNew = 3,
        NotLastVersionCreateNew = 4
    }
}