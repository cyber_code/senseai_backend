﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.DataSets
{
    public sealed class UpdateDataSet : ICommand<UpdateDataSetResult>
    {
        public Guid Id { get; set; }

        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public Guid TypicalId { get; set; }

        public string TypicalName { get; set; }

        public string TypicalType { get; set; }

        public string Title { get; set; }

        public decimal Coverage { get; set; }

        public Rows[] Rows { get; set; }
    }

    public sealed class UpdateDataSetResult
    {
        public UpdateDataSetResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}