﻿using SenseAI.Domain;
using Messaging.Commands;
using System;

namespace Commands.DataModule.Data
{
    public sealed class AcceptAllTypicalChangesWithoutWorkflow : ICommand<AcceptAllTypicalChangesWithoutWorkflowResult>
    { 

    }

    public sealed class AcceptAllTypicalChangesWithoutWorkflowResult
    {
        public AcceptAllTypicalChangesWithoutWorkflowResult(int nr)
        {
            Nr=nr;
        }

        public int Nr { get; private set; }
    }
}