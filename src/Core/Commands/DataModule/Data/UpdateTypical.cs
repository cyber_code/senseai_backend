﻿using SenseAI.Domain;
using Messaging.Commands;
using System;

namespace Commands.DataModule.Data
{
    public sealed class UpdateTypical : ICommand<UpdateTypicalResult>
    { 
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Json { get; set; }

        public string Xml { get; set; }

        public string XmlHash { get; set; }

        public TypicalType Type { get; set; }
    }

    public sealed class UpdateTypicalResult
    {
        public UpdateTypicalResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}