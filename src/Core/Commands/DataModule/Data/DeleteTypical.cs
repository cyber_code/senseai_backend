﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.Data
{
    public sealed class DeleteTypical : ICommand<DeleteTypicalResult>
    {
        public Guid Id { get;  set; }
    }

    public sealed class DeleteTypicalResult
    {
        public DeleteTypicalResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}