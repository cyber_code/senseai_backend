﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.DataModule.Data
{
    public sealed class DeleteTypicalAttributeManually : ICommand<DeleteTypicalAttributeManuallyResult>
    {
        public Guid TypicalId { get; set; }

        public string Name { get; set; }

        public bool IsTypicalChange { get; set; }
    }

    public sealed class DeleteTypicalAttributeManuallyResult
    {
        public DeleteTypicalAttributeManuallyResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
