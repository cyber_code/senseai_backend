﻿using SenseAI.Domain;
using Messaging.Commands;
using System;

namespace Commands.DataModule.Data
{
    public sealed class AddTypical : ICommand<AddTypicalResult>
    { 
        public Guid CatalogId { get; set; }

        public string Title { get; set; }

        public string Json { get; set; }

        public string Xml { get; set; }

        public string XmlHash { get; set; }

        public TypicalType Type { get; set; }
    }

    public sealed class AddTypicalResult
    {
        public AddTypicalResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}