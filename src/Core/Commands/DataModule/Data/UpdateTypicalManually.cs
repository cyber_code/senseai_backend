﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.Data
{
    public sealed class UpdateTypicalManually : ICommand<bool>
    {
        public Guid Id { get; set; }

        public string Title { get; set; }
    }
     
}
