﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.Data
{
    public sealed class DeleteTypicalManually : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}
