﻿using Messaging.Commands;
using SenseAI.Domain;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Commands.DataModule.Data
{
    public sealed class AddTypicalManually : ICommand<AddTypicalManuallyResult>
    {
        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public string HashId
        {
            get
            {
                return BitConverter.ToString(MD5.Create()
                    .ComputeHash(UTF8Encoding.UTF8.GetBytes(
                        TenantId.ToString() + "|" +
                        ProjectId.ToString() + "|" +
                        SubProjectId.ToString() + "|" +
                        SystemId.ToString() + "|" +
                        CatalogId.ToString())))
                    .Replace("-", string.Empty);
            }
        }

        public string Title { get; set; }

        public TypicalType Type { get; set; }

        public Guid TenantId { get; set; }

        public bool HasAutomaticId { get; set; }

        public bool IsConfiguration { get; set; }        
    }

    public sealed class AddTypicalManuallyResult
    {
        public AddTypicalManuallyResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
