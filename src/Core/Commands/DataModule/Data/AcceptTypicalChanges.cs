﻿using SenseAI.Domain;
using Messaging.Commands;
using System;

namespace Commands.DataModule.Data
{
    public sealed class AcceptTypicalChanges : ICommand<AcceptTypicalChangesResult>
    { 
        public Guid[] Ids { get; set; }

        public bool MarkAsInvalid { get; set; }

       
    }

    public sealed class AcceptTypicalChangesResult
    {
        public AcceptTypicalChangesResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}