﻿using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commands.DataModule.Data
{
    public sealed class AddTypicalAttributeManually : ICommand<AddTypicalAttributeManuallyResult>
    {
        public Guid TypicalId { get; set; }

        public string Name { get; set; }

        public bool IsMultiValue { get; set; }

        public string[] PossibleValues { get; set; }

        public bool IsRequired { get; set; }

        public bool IsNoInput { get; set; }

        public bool IsNoChange { get; set; }

        public bool IsExternal { get; set; }

        public int MinimumLength { get; set; }

        public int MaximumLength { get; set; }

        public string RelatedApplicationName { get; set; }

        public string ExtraData { get; set; }

        public bool RelatedApplicationIsConfiguration { get; set; }
    }

    public sealed class AddTypicalAttributeManuallyResult
    {
        public AddTypicalAttributeManuallyResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
