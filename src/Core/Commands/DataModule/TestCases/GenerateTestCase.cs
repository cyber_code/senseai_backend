﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.TestCases
{
    public sealed class SetDataSetForPathItem : ICommand<bool>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public Guid WorkflowId { get; set; }

        public Guid WorkflowPathId { get; set; }

        public Guid WorkflowPathItemId { get; set; }

        public Guid DataSetId { get; set; }

        public bool SkipStatus { get; set; }

        public int Index { get; set; }
    }
}