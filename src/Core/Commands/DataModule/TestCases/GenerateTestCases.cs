﻿using Messaging.Commands;
using System;

namespace Commands.DataModule.TestCases
{
    public sealed class GenerateTestCases : ICommand<GenerateTestCasesResult>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public Guid WorkflowId { get; set; }

        public Guid WorkflowPathId { get; set; }
 
        public bool SkipStatus { get; set; }
    }

    public sealed class GenerateTestCasesResult
    {
        public GenerateTestCasesResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}