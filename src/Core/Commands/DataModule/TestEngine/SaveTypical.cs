﻿using Commands.DataModule.DataSets;
using Messaging.Commands;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Commands.DataModule.TestEngine
{
    public sealed class SaveTypical : ICommand<bool>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public string HashId
        {
            get
            {
                return BitConverter.ToString(MD5.Create()
                    .ComputeHash(UTF8Encoding.UTF8.GetBytes(
                        TenantId.ToString() + "|" +
                        ProjectId.ToString() + "|" +
                        SubProjectId.ToString() + "|" +
                        SystemId.ToString() + "|" +
                        CatalogId.ToString()))).Replace("-", string.Empty);
            }
        }

        public string TypicalName { get; set; }

        public int TypicalType { get; set; }

        public TEAllAttributes[] Attributes { get; set; }

    }

    public class TEAllAttributes
    {
        public string Name { get; set; }

        public bool IsMultiValue { get; set; }

        public string[] PossibleValues { get; set; }

        public bool IsRequired { get; set; }

        public bool IsNoInput { get; set; }

        public bool IsNoChange { get; set; }

        public bool IsExternal { get; set; }

        public int MinimumLength { get; set; }

        public int MaximumLength { get; set; }

        public string RelatedApplicationName { get; set; }

        public string ExtraData { get; set; }

        public bool RelatedApplicationIsConfiguration { get; set; }
    }
     
}
