﻿using Messaging.Commands;
using System;

namespace Commands.PeopleModule

{
    public sealed class AddPeople : ICommand<AddPeopleResult>
    {
        public Guid RoleId { get; set; }
        public string Name { get; set; }

        public string Surname { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int UserIdFromAdminPanel { get; set; }
        public Guid SubProjectId { get; set; }
        public string Color { get; set; }


    }

    public sealed class AddPeopleResult
    {
        public AddPeopleResult(Guid id)
        {
            Id = id;
        }
       
        public Guid Id { get; private set; }
    }
}