﻿using Messaging.Commands;
using System;

namespace Commands.PeopleModule
{
    public sealed class DeleteRole : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}