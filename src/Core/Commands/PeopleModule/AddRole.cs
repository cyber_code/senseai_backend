﻿using Messaging.Commands;
using System;

namespace Commands.PeopleModule

{
    public sealed class AddRole : ICommand<AddRoleResult>
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public Guid SubProjectId { get; set; }

    }

    public sealed class AddRoleResult
    {
        public AddRoleResult(Guid id)
        {
            Id = id;
        }
       
        public Guid Id { get; private set; }
    }
}