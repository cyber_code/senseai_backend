﻿using Messaging.Commands;
using System;

namespace Commands.PeopleModule
{
    public sealed class DeletePeople : ICommand<bool>
    {
        public Guid Id { get; set; }
    }
}