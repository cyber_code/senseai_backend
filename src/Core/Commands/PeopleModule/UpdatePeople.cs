﻿using Messaging.Commands;
using System;

namespace Commands.PeopleModule
{
    public sealed class UpdatePeople : ICommand<bool>
    {
        public Guid Id { get; set; }

        public Guid RoleId { get; set; }
        public string Name { get; set; }

        public string Surname { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int UserIdFromAdminPanel { get; set; }
        public Guid SubProjectId { get; set; }
        public string Color { get; set; }
    }
}