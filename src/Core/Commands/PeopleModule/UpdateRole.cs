﻿using Messaging.Commands;
using System;

namespace Commands.PeopleModule
{
    public sealed class UpdateRole : ICommand<bool>
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public Guid SubProjectId { get; set; }
    }
}