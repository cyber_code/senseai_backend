﻿using Messaging.Queries;
using System;

namespace Queries.ReportModule.TraceabilityReport
{
    public sealed class GetTraceabilityReportDetail : IQuery<GetTraceabilityReportDetailResult[]>
    {
        public Guid SubProjectId { get; set; }
        public Guid Id { get; set; }

        public TraceablityReportLevel Level { get; set; }

        public string[] IssueTypes { get; set; }
        //public IssueTypes[] IssueTypes { get; set; }
    }

    public sealed class GetTraceabilityReportDetailResult
    {
        public string Title { get; set; }

        public int Workflows { get; set; }

        public int TestCases { get; set; }

        public int PlannedTestCases { get; set; }

        public int OpenIssues { get; set; }

        public int FailedExecutions { get; set; }

        public int SuccessfulExecutions { get; set; }
    }
}