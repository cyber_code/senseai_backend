﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ReportModule.TraceabilityReport
{
    public sealed class GetTraceabilityReport : IQuery<GetTraceabilityReportResult>
    {
        public Guid SubProjectId { get; set; }

        public Guid? Id { get; set; }

        public TraceablityReportLevel Level { get; set; }

        public string[] IssueTypes { get; set; }
    }

    public sealed class GetTraceabilityReportResult
    {
        public TraceablityReportLevel NextLevel { get; set; }

        public GetTraceabilityReportResultLine[] Lines { get; set; }

        public GetTraceablityReportResultExecutionLine[] ExecutionLines { get; set; }

        public GetTraceablityReportResultIssuesLine[] IssuesLines { get; set; }
    }

    public sealed class GetTraceabilityReportResultLine
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public int Processes { get; set; }

        public int Workflows { get; set; }

        public int TestCases { get; set; }

        public int PlannedTestCases { get; set; }

        public int OpenIssues { get; set; }

        public int FailedExecutions { get; set; }

        public int SuccessfulExecutions { get; set; }
    }

    public sealed class GetTraceablityReportResultExecutionLine
    {
        public Guid Id { get; set; }

        public string Day { get; set; }

        public int FailedExecutions { get; set; }

        public int SuccessfulExecutions { get; set; }
    }

    public sealed class GetTraceablityReportResultIssuesLine
    {
        public Guid Id { get; set; }

        public string Date { get; set; }

        public int Status { get; set; }

        public int Count { get; set; }
    }

    public sealed class IssueTypes
    {
        public string Item { get; set; }
    }

}
