﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ReportModule.TraceabilityReport
{
    public enum TraceablityReportLevel
    {
        Hierarchy,
        Process,
        Workflow,
        End
    }
}
