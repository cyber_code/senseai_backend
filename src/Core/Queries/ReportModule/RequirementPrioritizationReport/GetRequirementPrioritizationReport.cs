﻿using Messaging.Queries;
using System;
using System.Collections.Generic;

namespace Queries.ReportModule.RequirementPrioritizationReport
{
    public sealed class GetRequirementPrioritizationReport : IQuery<GetRequirementPrioritizationReportResult>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetRequirementPrioritizationReportResult
    {
        public string[] ColumnNames { get; set; }
        public string[][] ReportData { get; set; }
    }
}
