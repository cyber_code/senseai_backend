﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public class GetWorkflowVersion : IQuery<GetWorkflowVersionResult>
    {
        public Guid WorkflowId { get; set; }

        public int Version { get; set; }
    }

    public sealed class GetWorkflowVersionResult
    {
        public GetWorkflowVersionResult(Guid workflowId, int version, string title, string description, bool isLast, string designerJson, string pathsJson)
        {
            WorkflowId = workflowId;
            Version = version;
            Title = title;
            Description = description;
            IsLast = isLast;
            DesignerJson = designerJson;
            PathsJson = pathsJson;
        }

        public Guid WorkflowId { get; private set; }

        public int Version { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public bool IsLast { get; private set; }

        public string DesignerJson { get; private set; }

        public string PathsJson { get; private set; }        
    }
}