﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public class GetWorkflowByTitle : IQuery<GetWorkflowResult>
    {

        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public string Title { get; set; }
    }
     
}