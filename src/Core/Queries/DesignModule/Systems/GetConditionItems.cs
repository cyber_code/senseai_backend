﻿using Messaging.Queries;
using System.Collections.Generic;

namespace Queries.DesignModule.Systems
{
    public sealed class GetConditionItems : IQuery<GetConditionItemsResult>
    { 
        public string AdapterName { get; set; }
    }

    public sealed class GetConditionItemsResult
    { 

        public GetConditionItemsResult(string adapterName, List<string> operators)
        {
            AdapterName = adapterName;
            Operators = operators;
        }

        public string AdapterName { get; private set; }

        public List<string> Operators { get; private set; }         
    }
}