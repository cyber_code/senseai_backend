﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule.System
{
    public sealed class GetSystems : IQuery<GetSystemsResult[]>
    {
    }

    public sealed class GetSystemsResult
    {
        public GetSystemsResult(Guid id, string title, string adapterName)
        {
            Id = id;
            Title = title;
            AdapterName = adapterName;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string AdapterName { get; private set; }
    }
}