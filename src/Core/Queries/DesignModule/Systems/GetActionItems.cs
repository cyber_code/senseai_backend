﻿using Messaging.Queries;
using System.Collections.Generic;

namespace Queries.DesignModule.Systems
{
    public sealed class GetActionItems : IQuery<GetActionItemsResult>
    {  
        public string AdapterName { get; set; }
    }

    public sealed class GetActionItemsResult
    {  
        public GetActionItemsResult(string adapterName, List<string> actionItems)
        {
            AdapterName = adapterName;
            ActionItems = actionItems;
        }

        public string AdapterName { get; private set; }

        public List<string> ActionItems { get; private set; }
    }
}