﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public sealed class GetWorkflows : IQuery<GetWorkflowsResult[]>
    {
        public Guid FolderId { get; set; }
    }

    public sealed class GetWorkflowsResult
    {
        public GetWorkflowsResult(Guid id, string title, string description, Guid folderId, int status, bool issued, bool generatedTestCase)
        {
            Id = id;
            Title = title;
            Description = description;
            FolderId = folderId;
            Status = status;
            isIssued = issued;
            generatedTestCases = generatedTestCase;

        }

        public Guid Id { get; set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid FolderId { get; private set; }
        public int Status { get; private set; }

        public bool isIssued { get; private set; }

        public bool generatedTestCases { get; private set; }
    }
}