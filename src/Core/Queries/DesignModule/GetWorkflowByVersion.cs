﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public class GetWorkflowByVersion : IQuery<GetWorkflowResult>
    {
        public Guid WorkflowId { get; set; }

        public int VersionId { get; set; }
    }
}