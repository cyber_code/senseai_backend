﻿using Messaging.Queries;
using System;
using System.Collections.Generic;

namespace Queries.DesignModule
{
    public class GetEnquiryActions : IQuery<GetEnquiryActionsResult[]>
    {
        public string TypicalName { get; set; }
        public Guid CatalogId { get; set; }
    }

    public sealed class GetEnquiryActionsResult
    {
        public string Action { get; set; }

        public string Type { get; set; }
    }
}