﻿using Messaging.Queries;
using System;
using System.Collections.Generic;

namespace Queries.DesignModule
{
    public class SearchAttributes : IQuery<AttributesResult[]>
    { 
        public string SearchString { get; set; }

        public Guid TypicalId { get; set; }
    }

    public sealed class GetAttributesResult
    {
        public GetAttributesResult(Guid id, string title, string json)
        {
            Id = Id;
            Title = title;
            Json = json;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Json { get; private set; }
    }

    public sealed class AttributesResult
    {
        public AttributesResult(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }

    public class TypicalItem
    {
        public TypicalItem(string typicalName, List<AttributesResult> attributes)
        {
            TypicalName = typicalName;
            Attributes = attributes;
        }

        public string TypicalName { get; private set; }

        public List<AttributesResult> Attributes { get; private set; }
    }
}