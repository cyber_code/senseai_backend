﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public class GetCatalog : IQuery<GetCatalogResult>
    { 
        public Guid Id { get; set; }
    }

    public sealed class GetCatalogResult
    {
        public GetCatalogResult(Guid id, Guid projectId, Guid subProjectId, string title, string description, int type)
        {
            Id = id;
            ProjectId = projectId;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
            Type = type;
        }

        public Guid Id { get; private set; }
        public Guid ProjectId { get; private set; }
        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public int Type { get; private set; }
    }
}