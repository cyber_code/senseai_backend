﻿using Messaging.Queries;

namespace Queries.DesignModule.QualitySuite
{
    public sealed class GetATSRequirements : IQuery<GetATSRequirementsResult[]>
    {
        public ulong ProjectId { get; set; }

        public ulong SubProjectId { get; set; }

        public ulong UseCaseId { get; set; }
    }

    public sealed class GetATSRequirementsResult
    {
        public GetATSRequirementsResult(string nameField, string noderefField)
        {
            NameField = nameField;
            NoderefField = noderefField;
        }

        public string NameField { get; set; }

        public string NoderefField { get; set; }
    }
}