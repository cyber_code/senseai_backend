﻿using Messaging.Queries;

namespace Queries.DesignModule.QualitySuite
{
    public sealed class GetATSFeatures : IQuery<GetATSFeaturesResult[]>
    {
        public ulong ProjectId { get; set; }

        public ulong SubProjectId { get; set; }

        public ulong ProductId { get; set; }

        public ulong FolderId { get; set; }
    }

    public sealed class GetATSFeaturesResult
    {
        public GetATSFeaturesResult(string nameField, string noderefField, string typeField)
        {
            NameField = nameField;
            NoderefField = noderefField;
            TypeField = typeField;
        }

        public string NameField { get; set; }

        public string NoderefField { get; set; }

        public string TypeField { get; set; }
    }
}