﻿using Messaging.Queries;

namespace Queries.DesignModule.QualitySuite
{
    public sealed class GetATSUsecases : IQuery<GetATSUsecasesResult[]>
    {
        public ulong ProjectId { get; set; }

        public ulong SubProjectId { get; set; }

        public ulong FeatureId { get; set; }

        public ulong FolderId { get; set; }
    }

    public sealed class GetATSUsecasesResult
    {
        public GetATSUsecasesResult(string nameField, string noderefField, string typeField)
        {
            NameField = nameField;
            NoderefField = noderefField;
            TypeField = typeField;
        }

        public string NameField { get; set; }

        public string NoderefField { get; set; }

        public string TypeField { get; set; }
    }
}