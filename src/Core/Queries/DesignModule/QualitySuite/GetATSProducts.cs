﻿using Messaging.Queries;

namespace Queries.DesignModule.QualitySuite
{
    public sealed class GetATSProducts : IQuery<GetATSProductsResult[]>
    {
        public ulong ProjectId { get; set; }

        public ulong SubProjectId { get; set; }
    }

    public sealed class GetATSProductsResult
    {
        public GetATSProductsResult(string nameField, string noderefField)
        {
            NameField = nameField;
            NoderefField = noderefField;
        }

        public string NameField { get; set; }

        public string NoderefField { get; set; }
    }
}