﻿using Messaging.Queries;

namespace Queries.DesignModule.QualitySuite
{
    public sealed class GetATSReleaseSummary : IQuery<GetATSReleaseSummaryResult>
    {
        public ulong ReleaseIterationId { get; set; }
        public ulong ProjectId { get; set; }

        public ulong SubProjectId { get; set; }
    }

    public sealed class GetATSReleaseSummaryResult
    {
        public GetATSReleaseSummaryResult(int totalTestCaseExecutions, int passedTestCaseExecutions, int outstandingDefects)
        {
            TotalTestCaseExecutions = totalTestCaseExecutions;
            PassedTestCaseExecutions = passedTestCaseExecutions;
            OutstandingDefects = outstandingDefects;
        }

        public int TotalTestCaseExecutions { get; set; }
        public int PassedTestCaseExecutions { get; set; }
        public int OutstandingDefects { get; set; }
    }
}