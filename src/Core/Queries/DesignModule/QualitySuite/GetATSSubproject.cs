﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.DesignModule.QualitySuite
{
    public sealed class GetATSSubproject : IQuery<GetATSSubprojectResult[]>
    {
        public ulong NoderefField { get; set; }
    }

    public sealed class GetATSSubprojectResult
    {
        public GetATSSubprojectResult(string nameField, string noderefField)
        {
            NameField = nameField;
            NoderefField = noderefField;
        }

        public string NameField { get; set; }

        public string NoderefField { get; set; }
    }
}