﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.DesignModule.QualitySuite
{
    public sealed class GetATSProject : IQuery<GetATSProjectResult[]>
    {
    }

    public sealed class GetATSProjectResult
    {
        public GetATSProjectResult(string nameField, string noderefField)
        {
            NameField = nameField;
            NoderefField = noderefField;
        }

        public string NameField { get; set; }

        public string NoderefField { get; set; }
    }
}