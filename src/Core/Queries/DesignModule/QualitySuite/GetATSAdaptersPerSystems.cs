﻿using Messaging.Queries;

namespace Queries.DesignModule.QualitySuite
{
    public sealed class GetATSAdaptersPerSystems : IQuery<GetATSAdaptersPerSystemsResult[]>
    {
        public ulong ProjectId { get; set; }

        public ulong SubProjectId { get; set; }
    }

    public sealed class GetATSAdaptersPerSystemsResult
    {
        public GetATSAdaptersPerSystemsResult( string noderefField, GetATSAdaptersPerSystemsResultItem[] value)
        {
            Value = value;
            NoderefField = noderefField;
        }

        public GetATSAdaptersPerSystemsResultItem[] Value { get; set; }

        public string NoderefField { get; set; }
    }
    public sealed class GetATSAdaptersPerSystemsResultItem
    {
        public GetATSAdaptersPerSystemsResultItem(string nameField, string noderefField)
        {
            NameField = nameField;
            NoderefField = noderefField;
        }

        public string NameField { get; set; }

        public string NoderefField { get; set; }
    }
}