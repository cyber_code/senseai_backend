﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule.QualitySuite
{
    public sealed class CheckATSTestCases : IQuery<bool>
    {
        public Guid WorkflowId { get; set; }

        public Guid WorkflowPathId { get; set; }

        public ulong ProjectId { get; set; }

        public ulong SubProjectId { get; set; }
    }
}
