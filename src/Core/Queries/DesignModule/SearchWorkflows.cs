﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public sealed class SearchWorkflows : IQuery<SearchWorkflowsResult[]>
    {
        public string SearchString { get; set; }

        public Guid SubProjectId { get; set; }
    }

    public sealed class SearchWorkflowsResult
    {
        public SearchWorkflowsResult(Guid id, string title, string description, Guid folderId)
        {
            Id = id;
            Title = title;
            Description = description;
            FolderId = folderId;
        }

        public Guid Id { get; set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid FolderId { get; private set; }
    }
}