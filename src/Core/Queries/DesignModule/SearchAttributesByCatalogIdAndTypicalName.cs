﻿using Messaging.Queries;
using System; 

namespace Queries.DesignModule
{
    public class SearchAttributesByCatalogIdAndTypicalName : IQuery<AttributesResult[]>
    { 
        public string SearchString { get; set; }

        public Guid CatalogId { get; set; }

        public string TypicalName { get; set; }
    }
}
