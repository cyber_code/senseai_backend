﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public sealed class SearchTypicals : IQuery<SearchTypicalsResult[]>
    {
        public string SearchString { get; set; }

        public Guid CatalogId { get; set; }
    }

    public sealed class SearchTypicalsResult
    {
        public SearchTypicalsResult(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }
    }
}