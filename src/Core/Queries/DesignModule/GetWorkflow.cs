﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public class GetWorkflow : IQuery<GetWorkflowResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetWorkflowResult
    {
        public GetWorkflowResult(Guid id, string title, string description, Guid folderId,
            string designerJson, string pathsJson, LinkedWorkflow[] linkedWorkflows, bool isParsed, int versionId)
        {
            Id = id;
            Title = title;
            Description = description;
            FolderId = folderId;
            DesignerJson = designerJson;
            PathsJson = pathsJson;
            LinkedWorkflows = linkedWorkflows;
            IsParsed = isParsed;
            VersionId = versionId;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid FolderId { get; private set; }

        public string DesignerJson { get; private set; }

        public string PathsJson { get; private set; }
        public bool IsParsed { get; private set; }

        public int VersionId { get; private set; }

        public LinkedWorkflow[] LinkedWorkflows { get; private set; }
    }

    public sealed class LinkedWorkflow
    {
        public LinkedWorkflow(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }
    }
}