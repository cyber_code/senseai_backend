﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public sealed class GetPreviousWorkflowItems : IQuery<GetPreviousWorkflowItemsResult[]>
    {
        public Guid CurrentId { get; set; }

        public Guid ParenId { get; set; }

        public string DesignerJson { get; set; }

        public Guid[] ItemIds { get; set; }
    }

    public sealed class GetPreviousWorkflowItemsResult
    {
        public GetPreviousWorkflowItemsResult(Guid id, string title, string typicalName, Guid typicalId)
        {
            Id = id;
            Title = title;
            TypicalName = typicalName;
            TypicalId = typicalId;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string TypicalName { get; private set; }

        public Guid TypicalId { get; private set; }
    }
}