﻿using Messaging.Queries;
using System;
using System.Collections.Generic;

namespace Queries.DesignModule.TestEngine
{
    public class GetTypical : IQuery<GetTypicalResult>
    {
        public Guid CatalogId { get; set; }
        public Guid SubProjectId { get; set; }
        public Guid SystemId { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
    }

    public sealed class GetTypicalResult
    {
        public GetTypicalResult(Guid id,  int type, string name, Guid catalogId, string catalogName, List<string> attributes)
        {
            Id = id;
            Type = type;
            Name = name;
            CatalogId = catalogId;
            CatalogName = catalogName;
            Attributes = attributes;
        }

        public Guid Id { get; private set; }

        public int Type { get; private set; }

        public string Name { get; private set; }

        public Guid CatalogId { get; private set; }

        public string CatalogName { get; private set; }
      

        public List<string> Attributes { get; private set; }
    }
}