﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule.Suggestions
{
    public sealed class IssueWorkflowToML : IQuery<bool>
    {
        public Guid WorkflowId { get; set; }

        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }
    }
}