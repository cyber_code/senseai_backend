﻿using System.Collections.Generic;

namespace Queries.DesignModule.Suggestions
{
    public sealed class Workflows
    {
        public Workflows(string workflowTitle, List<GetSuggestionResultWorkflowItem> workflowItem,
            List<GetSuggestionResultWorkflowItemLink> workflowItemLink,
            List<GetPathsResultWorkflowPath> workflowPath)
        {
            WorkflowTitle = workflowTitle;
            WorkflowItem = workflowItem;
            WorkflowItemLink = workflowItemLink;
            WorkflowPath = workflowPath;
        }

        public string WorkflowTitle { get; set; }

        public List<GetSuggestionResultWorkflowItem> WorkflowItem { get; set; }

        public List<GetSuggestionResultWorkflowItemLink> WorkflowItemLink { get; set; }

        public List<GetPathsResultWorkflowPath> WorkflowPath { get; set; }
    }
}