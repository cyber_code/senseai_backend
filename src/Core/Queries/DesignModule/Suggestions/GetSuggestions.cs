﻿using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using System;

namespace Queries.DesignModule.Suggestions
{
    public sealed class GetSuggestions : IQuery<GetSuggestionsResult>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid CurrentItemId { get; set; }

        public string Json { get; set; }
    }

    public sealed class GetSuggestionsResult
    {
        public GetSuggestionsResult(GetSuggestionsResultItem[] preItems, GetSuggestionsResultItem[] postItems)
        {
            PreItems = preItems;
            PostItems = postItems;
        }

        public GetSuggestionsResultItem[] PreItems { get; private set; }

        public GetSuggestionsResultItem[] PostItems { get; private set; }
    }

    public sealed class GetSuggestionsResultItem
    {
        public GetSuggestionsResultItem(GetSuggestionResultWorkflowItem workflowItem, GetSuggestionResultWorkflowItemLink workflowItemLink)
        {
            WorkflowItem = workflowItem;
            WorkflowItemLink = workflowItemLink;
        }

        public GetSuggestionResultWorkflowItem WorkflowItem { get; private set; }

        public GetSuggestionResultWorkflowItemLink WorkflowItemLink { get; private set; }
    }

    public sealed class GetSuggestionResultWorkflowItem
    {
        public GetSuggestionResultWorkflowItem(
            string title, string description,
            Guid catalogId, string catalogName, Guid typicalId,
            Guid systemId, string typicalName, string typicalType, int actionType, DynamicData[] dynamicData,
             int type, bool isNegativeStep, string expectedError
        )
        {
            Title = title;
            Description = description;
            CatalogId = catalogId;
            CatalogName = catalogName;
            TypicalId = typicalId;
            SystemId = systemId;
            TypicalName = typicalName;
            TypicalType = typicalType;
            ActionType = actionType;
            DynamicData = dynamicData;
            Type = type;
            IsNegativeStep = isNegativeStep;
            ExpectedError = expectedError;
        }

        public GetSuggestionResultWorkflowItem(Guid id,
            string title, string description,
            Guid catalogId, string catalogName, Guid typicalId,
            Guid systemId, string typicalName, string typicalType, int actionType, DynamicData[] dynamicData,
             int type, bool isNegativeStep, string expectedError
        )
        {
            Id = id;
            Title = title;
            Description = description;
            CatalogId = catalogId;
            CatalogName = catalogName;
            TypicalId = typicalId;
            SystemId = systemId;
            TypicalName = typicalName;
            TypicalType = typicalType;
            ActionType = actionType;
            DynamicData = dynamicData;
            Type = type;
            IsNegativeStep = isNegativeStep;
            ExpectedError = expectedError;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid CatalogId { get; private set; }

        public string CatalogName { get; private set; }

        public Guid TypicalId { get; private set; }

        public Guid SystemId { get; private set; }

        public string TypicalName { get; private set; }

        public string TypicalType { get; private set; }

        public int ActionType { get; private set; }

        //public string Parameters { get; private set; }
        public DynamicData[] DynamicData { get; set; }

        public int Type { get; private set; }

        public bool IsNegativeStep { get; set; }

        public string ExpectedError { get; set; }
    }

    public sealed class DynamicData
    {
        public DynamicData(Guid sourceWorkflowItemId, string sourceAttributeName, string targetAttributeName, string formulaText)
        {
            SourceWorkflowItemId = sourceWorkflowItemId;
            SourceAttributeName = sourceAttributeName;
            TargetAttributeName = targetAttributeName;
            FormulaText = formulaText;
        }

        public Guid SourceWorkflowItemId { get; set; }

        public string SourceAttributeName { get; set; }

        public string TargetAttributeName { get; set; }

        public string FormulaText { get; set; }
    }

    public sealed class GetSuggestionResultWorkflowItemLink
    {
        public GetSuggestionResultWorkflowItemLink(
            Guid sourceId, Guid targetId, WorkflowItemLinkState type
        )
        {
            SourceId = sourceId;
            TargetId = targetId;
            Type = type;
        }

        public Guid SourceId { get; private set; }

        public Guid TargetId { get; private set; }

        public WorkflowItemLinkState Type { get; private set; }
    }
}