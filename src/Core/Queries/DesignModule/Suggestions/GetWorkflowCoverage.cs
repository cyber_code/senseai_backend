﻿using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using System;

namespace Queries.DesignModule.Suggestions
{
    public sealed class GetWorkflowCoverage : IQuery<GetWorkflowCoverageResult>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid WorkflowId { get; set; }


    }

    public sealed class GetWorkflowCoverageResult
    {
        public GetWorkflowCoverageResult(Guid workflowId,decimal workflowCovarage, decimal dataCoverage, string status)
        {
            Status = status;
            WorkflowId = workflowId;
            WorkflowCoverage = workflowCovarage;
            DataCoverage = dataCoverage;
        }
        public string Status { get; private set; }
        public decimal WorkflowCoverage { get; private set; }

        public decimal DataCoverage { get; private set; }
        public Guid WorkflowId { get; private set; }
    }

   
}