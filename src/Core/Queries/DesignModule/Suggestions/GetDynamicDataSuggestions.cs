﻿using Messaging.Queries;

namespace Queries.DesignModule.Suggestions
{
    public sealed class GetDynamicDataSuggestions : IQuery<GetDynamicDataSuggestionsResult[]>
    { 
        public string SourceTypicalName { get; set; }

        public string TargetTypicalName { get; set; }
    }

    public sealed class GetDynamicDataSuggestionsResult
    {
        public GetDynamicDataSuggestionsResult(string sourceTypicalName, string sourceAttributeName, string targetTypicalName, string targetAttributeName)
        {
            SourceTypicalName = sourceTypicalName;
            SourceAttributeName = sourceAttributeName;
            TargetTypicalName = targetTypicalName;
            TargetAttributeName = targetAttributeName;
        }

        public string SourceTypicalName { get; private set; }

        public string SourceAttributeName { get; private set; }

        public string TargetTypicalName { get; private set; }

        public string TargetAttributeName { get; private set; }
    }
}