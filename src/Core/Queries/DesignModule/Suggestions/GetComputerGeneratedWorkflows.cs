﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using Queries.DesignModule.Suggestions;

namespace Queries.DesignModule.Suggestions
{
    public sealed class GetComputerGeneratedWorkflows : IQuery<GetComputerGeneratedWorkflowsResult>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public string WorkflowTitle { get; set; }

        public Guid TypicalId { get; set; }

        public string TypicalTitle { get; set; }
    }

    public sealed class GetComputerGeneratedWorkflowsResult
    {
        public GetComputerGeneratedWorkflowsResult(List<Workflows> workflows)
        {
            Workflows = workflows;
        }

        public List<Workflows> Workflows { get; set; }
    }
}