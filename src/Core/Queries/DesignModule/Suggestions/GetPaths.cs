﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule.Suggestions
{
    public sealed class GetPaths : IQuery<GetPathsResult>
    { 
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public string Json { get; set; }
    }

    public sealed class GetPathsResult
    {
        public GetPathsResult(GetPathsResultWorkflowPath[] worklfowPath)
        {
            WorkflowPath = worklfowPath;
        }

        public GetPathsResultWorkflowPath[] WorkflowPath { get; private set; }
    }

    public sealed class GetPathsResultWorkflowPath
    {
        public GetPathsResultWorkflowPath(string title, int number, string coverage, bool isBestPath, Guid[] itemIds)
        {
            Title = title;
            Number = number;
            Coverage = coverage;
            IsBestPath = isBestPath;
            ItemIds = itemIds;
        }

        public string Title { get; private set; }

        public int Number { get; private set; }

        public string Coverage { get; private set; }

        public bool IsBestPath { get; private set; }

        public Guid[] ItemIds { get; private set; }
    }
}