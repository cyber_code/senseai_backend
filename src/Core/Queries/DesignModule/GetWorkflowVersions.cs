﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public class GetWorkflowVersions : IQuery<GetWorkflowVersionsResult[]>
    {        
        public Guid WorkflowId { get; set; }
    }

    public sealed class GetWorkflowVersionsResult
    {
        public GetWorkflowVersionsResult(Guid workflowId, int version, string title)
        {
            WorkflowId = workflowId;
            Version = version;
            Title = title; 
        }

        public Guid WorkflowId { get; private set; }

        public int Version { get; private set; }

        public string Title { get; private set; }
    }
}