﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public class GetTypicals : IQuery<GetTypicalsResult[]>
    { 
        public Guid CatalogId { get; set; }
    }

    public sealed class GetTypicalsResult
    {
        public GetTypicalsResult(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }
    }
}