﻿using Messaging.Queries;
using System;

namespace Queries.DesignModule
{
    public sealed class GetCatalogs : IQuery<GetCatalogsResult[]>
    {
        public Guid ProjectId { get; set; }
        public Guid SubProjectId { get; set; }
    }
  

    public sealed class GetCatalogsResult
    {
        public GetCatalogsResult(Guid id, Guid projectId, Guid subProjectid, string title, string description, int type)
        {
            Id = id;
            ProjectId = projectId;
            SubProjectId = subProjectid;
            Title = title;
            Description = description;
            Type = type;
        }

        public Guid Id { get; private set; }
        public Guid ProjectId { get; private set; }
        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int Type { get; set; }
    }
}