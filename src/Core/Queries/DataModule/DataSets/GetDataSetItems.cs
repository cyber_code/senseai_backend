﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data.DataSets
{
    public sealed class GetDataSetItems : IQuery<GetDataSetItemsResult[]>
    {
        public Guid[] DataSetIds { get; set; }
    }

    public sealed class GetDataSetItemsResult
    {
        public GetDataSetItemsResult(Guid id, string title, object rows)
        {
            Id = id;
            Title = title;
            Rows = rows;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public object Rows { get; private set; }
    }
}