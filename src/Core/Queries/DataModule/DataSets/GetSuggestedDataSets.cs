﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data.DataSets
{
    public sealed class GetSuggestedDataSets : IQuery<GetSuggestedDataSetsResult[]>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid TypicalId { get; set; }

        public string TypicalName { get; set; }
        
        public Guid WorkflowId { get; set; }

        public Guid PathItemId { get; set; }
    }

    public sealed class GetSuggestedDataSetsResult
    {
        public GetSuggestedDataSetsResult(Guid id, string title, decimal coverage, object rows)
        {
            Id = id;
            Title = title;
            Coverage = coverage;
            Rows = rows;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public decimal Coverage { get; private set; }

        public object Rows { get; private set; }
    }
}