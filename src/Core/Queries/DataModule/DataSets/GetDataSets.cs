﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data.DataSets
{
    public sealed class GetDataSets : IQuery<GetDataSetsResult[]>
    {
        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid TypicalId { get; set; }
    }

    public sealed class GetDataSetsResult
    {
        public GetDataSetsResult(Guid id, string title, bool aiGenerated, decimal coverage, int nrRows)
        {
            Id = id;
            Title = title;
            AiGenerated = aiGenerated;
            Coverage = coverage;
            NrRows = nrRows;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public bool AiGenerated { get; private set; }

        public int NrRows { get; private set; }

        public decimal Coverage { get; private set; }
    }
}