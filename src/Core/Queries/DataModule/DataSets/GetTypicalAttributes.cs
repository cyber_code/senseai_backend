﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.DataSets
{
    public sealed class GetTypicalAttributes : IQuery<GetTypicalAttributesResult>
    {
        public Guid TypicalId { get; set; }

        public int Mandatory { get; set; }
    }

    public sealed class GetTypicalAttributesResult
    {
        public GetTypicalAttributesResult(Guid id, string title, Attributes[] attributes)
        {
            Id = id;
            Title = title;
            Attributes = attributes;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public Attributes[] Attributes { get; private set; }
    }

    public sealed class Attributes
    {
        public Attributes(string name, bool isRequired)
        {
            Name = name;
            IsRequired = isRequired;
        }

        public string Name { get; private set; }

        public bool IsRequired { get; private set; }
    }
}