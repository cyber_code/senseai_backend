﻿using Messaging.Queries;
using Queries.DataModule.DataSets;
using System;

namespace Queries.DataModule.Data.DataSets
{
    public sealed class GetDataSet : IQuery<GetDataSetResult>
    {
        public Guid Id { get; set; }

        public int Mandatory { get; set; }
    }

    public sealed class GetDataSetResult
    {
        public GetDataSetResult(Guid id, string title, Guid typicalId, decimal coverage, Combinations[] combinations, Attributes[] attributes)
        {
            Id = id;
            Title = title;
            TypicalId = typicalId;
            Coverage = coverage;
            Combinations = combinations;
            Attributes = attributes;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public Guid TypicalId { get; private set; }

        public decimal Coverage { get; private set; }

        public Combinations[] Combinations { get; private set; }

        public Attributes[] Attributes { get; private set; }
    }
}