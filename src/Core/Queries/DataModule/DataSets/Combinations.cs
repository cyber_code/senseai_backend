﻿using System.Collections.Generic;

namespace Queries.DataModule.Data.DataSets
{
    public sealed class Combinations
    {
        public string Attribute { get; set; }

        public List<string> Values { get; set; }
    }
}