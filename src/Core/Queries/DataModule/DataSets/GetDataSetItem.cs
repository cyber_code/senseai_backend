﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data.DataSets
{
    public sealed class GetDataSetItem : IQuery<GetDataSetItemResult>
    {
        public Guid DataSetId { get; set; }
    }

    public sealed class GetDataSetItemResult
    {
        public GetDataSetItemResult(Guid dataSetId, Rows[] rows)
        {
            DataSetId = dataSetId;
            Rows = rows;
        }

        public Guid DataSetId { get; private set; }

        public Rows[] Rows { get; private set; }
    }

    public sealed class Rows
    {
        public Rows(Guid id, object row)
        {
            Id = id;
            Row = row;
        }

        public Guid Id { get; private set; }

        public object Row { get; private set; }
    }
}