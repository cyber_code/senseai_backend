﻿using Messaging.Queries;

namespace Queries.DataModule.Data
{
    public class GetTypicalByName : IQuery<GetTypicalResult>
    {
        public string Name { get; set; }

        public int Type { get; set; }
    }
}