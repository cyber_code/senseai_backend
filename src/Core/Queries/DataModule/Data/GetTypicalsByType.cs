﻿using SenseAI.Domain;
using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public class GetTypicalsByType : IQuery<GetTypicalsByTypeResult[]>
    {  
        public TypicalType Type { get; set; }
    }

    public sealed class GetTypicalsByTypeResult
    {
        public GetTypicalsByTypeResult(Guid id, string title, TypicalType type)
        {
            Id = id;
            Title = title;
            Type = type;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public TypicalType Type { get; private set; }
    }
}