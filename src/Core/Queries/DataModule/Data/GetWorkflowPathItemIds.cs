﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public sealed class GetWorkflowPathItemIds : IQuery<Guid[]> 
    {
        public Guid WorkflowPathId { get; set; }
    }
}
