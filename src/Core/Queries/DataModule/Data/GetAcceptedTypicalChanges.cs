﻿using SenseAI.Domain;
using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public sealed class GetAcceptedTypicalChanges : IQuery<GetAcceptedTypicalChangesResult[]>
    {
    }

    public sealed class GetAcceptedTypicalChangesResult
    {
        public GetAcceptedTypicalChangesResult(Guid id, Guid typicalId, string typicalName)
        {
            Id = id;
            TypicalId = typicalId;
            TypicalName = typicalName;
        }

        public Guid Id { get; set; }

      

        public Guid TypicalId { get;  set; }
        public string TypicalName { get;  set; }


       
    }
}