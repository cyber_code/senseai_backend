﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public class GetNavigationsByParent : IQuery<GetNavigationsByParentResult[]>
    { 
        public Guid ParentId { get; set; }
    }

    public sealed class GetNavigationsByParentResult
    {
        public GetNavigationsByParentResult(Guid id, string name, string url, Guid? parentId)
        {
            Id = id;
            Name = name;
            Url = url;
            ParentId = parentId;
        }

        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public string Url { get; private set; }

        public Guid? ParentId { get; private set; }
    }
}