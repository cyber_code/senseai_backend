﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public sealed class GetWorkflowPathItems : IQuery<GetWorkflowPathItemsResult[]>
    {
        public Guid WorkflowPathId { get; set; }
    }

    public sealed class GetWorkflowPathItemsResult
    {
        public GetWorkflowPathItemsResult(Guid workflowId, Guid workflowPathId, Guid pathItemId, string pathItemTitle, Guid catalogId, Guid typicalId, 
            string typicalName, string typicalType, Guid dataSetId, int actionType, int index)
        {
            WorkflowId = workflowId;
            WorkflowPathId = workflowPathId;
            PathItemId = pathItemId;
            PathItemTitle = pathItemTitle;
            CatalogId = catalogId;
            TypicalId = typicalId;
            TypicalName = typicalName;
            TypicalType = typicalType;
            DataSetId = dataSetId;
            ActionType = actionType;
            TSIndex = index;
        }

        public Guid WorkflowId { get; private set; }

        public Guid WorkflowPathId { get; private set; }

        public Guid PathItemId { get; private set; }

        public string PathItemTitle { get; private set; }

        public Guid CatalogId { get; private set; }

        public Guid TypicalId { get; private set; }

        public string TypicalName { get; private set; }

        public string TypicalType { get; private set; }

        public Guid DataSetId { get; private set; }

        public int ActionType { get; private set; }

        public int TSIndex { get; private set; }
    } 
}