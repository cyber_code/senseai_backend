﻿using SenseAI.Domain;
using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public sealed class GetTypicalChanges : IQuery<GetTypicalChangesResult[]>
    {
    }

    public sealed class GetTypicalChangesResult
    {
        public GetTypicalChangesResult(Guid id, string title, TypicalType type, int status)
        {
            Id = id;
            Title = title;
            Type = type;
            Status = status;
        }

        public Guid Id { get; set; }

        public string Title { get; set; }

        public TypicalType Type { get; set; }

        public int Status { get; set; }
    }
}