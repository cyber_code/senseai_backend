﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public class GetNavigation : IQuery<GetNavigationResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetNavigationResult
    {
        public GetNavigationResult(Guid id, string name, string url, Guid? parentId)
        {
            Id = id;
            Name = name;
            Url = url;
            ParentId = parentId;
        }

        public Guid Id { get; set; }

        public string Name { get; private set; }

        public string Url { get; private set; }

        public Guid? ParentId { get; private set; }
    }
}