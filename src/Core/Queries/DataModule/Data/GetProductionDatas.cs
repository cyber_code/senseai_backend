﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public class GetProductionDatas : IQuery<GetProductionDatasResult[]>
    {
    }

    public sealed class GetProductionDatasResult
    {
        public GetProductionDatasResult(int id, Guid typicalId, string json)
        {
            Id = id;
            TypicalId = typicalId;
            Json = json;
        }

        public int Id { get; private set; }

        public Guid TypicalId { get; private set; }

        public string Json { get; private set; }
    }
}