﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public class GetProductionData : IQuery<GetProductionDataResult>
    { 
        public int Id { get; set; }
    }

    public sealed class GetProductionDataResult
    {
        public GetProductionDataResult(int id, Guid typicalId, string json)
        {
            Id = id;
            TypicalId = typicalId;
            Json = json;
        }

        public int Id { get; private set; }

        public Guid TypicalId { get; private set; }

        public string Json { get; private set; }
    }
}