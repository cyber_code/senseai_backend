﻿using SenseAI.Domain;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public sealed class GetAcceptedTypicalAttributeChanges : IQuery<GetAcceptedTypicalAttributeChangesResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetAcceptedTypicalAttributeChangesResult
    {
        public GetAcceptedTypicalAttributeChangesResult(  TypicalChangesAttributes[] newAttributes ,
         TypicalChangesAttributes[] deletedAttributes ,
         TypicalChangesAttributes[] changedAttributes//,
         //Workflow[] listOfAffectedWorkflows
         )
        {
            NewAttributes = newAttributes;
            DeletedAttributes = deletedAttributes;
            ChangedAttributes = changedAttributes;
          //  ListOfAffectedWorkflows = listOfAffectedWorkflows;
        }
        public TypicalChangesAttributes[] NewAttributes { get; set; }
        public TypicalChangesAttributes[] DeletedAttributes { get; set; }
        public TypicalChangesAttributes[] ChangedAttributes { get; set; }
       // public Workflow [] ListOfAffectedWorkflows { get; set; }
       
    }
}