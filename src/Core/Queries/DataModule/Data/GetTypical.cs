﻿using SenseAI.Domain;
using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public class GetTypical : IQuery<GetTypicalResult>
    {  
        public Guid Id { get; set; }
    }

    public sealed class GetTypicalResult
    {
        public GetTypicalResult(Guid id, string title, string json, TypicalType type)
        {
            Id = id;
            Title = title;
            Json = json;
            Type = type;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Json { get; private set; }

        public TypicalType Type { get; private set; }
    }
}