﻿using Messaging.Queries;
using System;

namespace Queries.DataModule.Data
{
    public sealed class GetWorkflowPaths : IQuery<GetWorkflowPathsResult[]>
    {
        public Guid WorkflowId { get; set; }
    }

    public sealed class GetWorkflowPathsResult
    {
        public GetWorkflowPathsResult(Guid pathId, string pathTitle, string coverage)
        {
            PathId = pathId;
            PathTitle = pathTitle;
            Coverage = coverage;
        }

        public Guid PathId { get; private set; }

        public string PathTitle { get; private set; }

        public string Coverage { get; private set; }
    }
}