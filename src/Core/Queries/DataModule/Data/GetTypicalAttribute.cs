﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.DataModule.Data
{
    public class GetTypicalAttribute : IQuery<GetTypicalAttributeResult>
    {
        public Guid TypicalId { get; set; }

        public string Name { get; set; }
    }

    public sealed class GetTypicalAttributeResult
    {
        public GetTypicalAttributeResult(Guid typicalId, string name, bool isMultiValue, string[] possibleValues, bool isRequired, bool isNoInput, bool isNoChange,
            bool isExternal, int minimumLength, int maximumLength, string relatedApplicationName, string extraData, bool relatedApplicationIsConfiguration)
        {
            TypicalId = typicalId;
            Name = name;
            IsMultiValue = isMultiValue;
            PossibleValues = possibleValues;
            IsRequired = isRequired;
            IsNoInput = isNoInput;
            IsNoChange = isNoChange;
            IsExternal = isExternal;
            MinimumLength = minimumLength;
            MaximumLength = maximumLength;
            RelatedApplicationName = relatedApplicationName;
            ExtraData = extraData;
            RelatedApplicationIsConfiguration = relatedApplicationIsConfiguration;
        }

        public Guid TypicalId { get; set; }

        public string Name { get; set; }

        public bool IsMultiValue { get; set; }

        public string[] PossibleValues { get; set; }

        public bool IsRequired { get; set; }

        public bool IsNoInput { get; set; }

        public bool IsNoChange { get; set; }

        public bool IsExternal { get; set; }

        public int MinimumLength { get; set; }

        public int MaximumLength { get; set; }

        public string RelatedApplicationName { get; set; }

        public string ExtraData { get; set; }

        public bool RelatedApplicationIsConfiguration { get; set; }
    }
}
