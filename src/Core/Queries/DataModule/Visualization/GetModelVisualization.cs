﻿using Messaging.Queries;
using System;
using System.Collections.Generic;

namespace Queries.DataModule.Visualization
{
    public sealed class GetModelVisualization : IQuery<GetModelVisualizationResult>
    {
        public string TypicalName { get; set; }

        public int Level { get; set; } 
    }

    public sealed class GetModelVisualizationResult
    {
        public GetModelVisualizationResult(List<AssossiationItems> items, List<AssossiationLinks> links)
        {
            Items = items;
            Links = links;
        }

        public List<AssossiationItems> Items { get; private set; }

        public List<AssossiationLinks> Links { get; private set; }


        // public GetModelVisualizationResult Child { get; set; }
    }

    public class AssossiationItems
    {
        public AssossiationItems(Guid key, string text, string color)
        {
            Key = key;
            Text = text;
            Color = color;
        }

        public Guid Key { get; private set; }

        public string Text { get; private set; }

        public string Color { get; private set; }
    }

    public class AssossiationLinks
    {
        public AssossiationLinks(Guid from, Guid to, string color)
        {
            From = from;
            To = to;
            Color = color;
        }

        public Guid From { get; private set; }

        public Guid To { get; private set; }

        public string Color { get; set; }
    }
}