﻿using Messaging.Queries;
using System;

namespace Queries.AdministrationModule.Projects
{
    public sealed class GetProject : IQuery<GetProjectResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetProjectResult
    {
        public GetProjectResult(Guid id, string title, string description)
        {
            Id = id;
            Title = title;
            Description = description;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }
    }
}