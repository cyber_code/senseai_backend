﻿using Messaging.Queries;
using System;

namespace Queries.AdministrationModule.Projects
{
    public sealed class GetSubProject : IQuery<GetSubProjectResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetSubProjectResult
    {
        //public GetSubProjectResult(Guid id, Guid projectId, string title, string description)
        //{
        //    Id = id;
        //    ProjectId = projectId;
        //    Title = title;
        //    Description = description;
        //}

        public GetSubProjectResult(Guid id, Guid projectId, string title, string description, string jiraLink, bool isJiraIntegrationEnabled)
        {
            Id = id;
            ProjectId = projectId;
            Title = title;
            Description = description;
            JiraLink = jiraLink;
            IsJiraIntegrationEnabled = isJiraIntegrationEnabled;
        }

        public Guid Id { get; private set; }

        public Guid ProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string JiraLink { get; private set; }

        public bool IsJiraIntegrationEnabled { get; private set; }
    }
}