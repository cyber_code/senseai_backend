﻿using Messaging.Queries;
using System;

namespace Queries.AdministrationModule.Projects
{
    public sealed class GetProjects : IQuery<GetProjectsResult[]>
    {
    }

    public sealed class GetProjectsResult
    {
        public GetProjectsResult(Guid id, string title, string description)
        {
            Id = id;
            Title = title;
            Description = description;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }
    }
}