﻿using Messaging.Queries;
using System;

namespace Queries.AdministrationModule.Projects
{
    public sealed class GetSubProjects : IQuery<GetSubProjectsResult[]>
    {
        public Guid ProjectId { get; set; }
    }

    public sealed class GetSubProjectsResult
    {
        public GetSubProjectsResult(Guid id, Guid projectId, string title, string description)
        {
            Id = id;
            ProjectId = projectId;
            Title = title;
            Description = description;
        }

        public Guid Id { get; private set; }

        public Guid ProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }
    }
}