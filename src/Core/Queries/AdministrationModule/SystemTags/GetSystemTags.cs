﻿using System;
using Messaging.Queries;

namespace Queries.AdministrationModule.SystemTags
{
    public sealed class GetSystemTags : IQuery<GetSystemTagsResult[]>
    {
        public Guid SystemId { get; set; }
    }

    public sealed class GetSystemTagsResult
    {
        public Guid Id { get; private set; }
        public Guid SystemId { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }

        public GetSystemTagsResult(Guid id, Guid systemId, string title, string description)
        {
            Id = id;
            SystemId = systemId;
            Title = title;
            Description = description;
        }
    }
}