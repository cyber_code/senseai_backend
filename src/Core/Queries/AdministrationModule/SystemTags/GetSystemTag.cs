﻿using System;
using Messaging.Queries;

namespace Queries.AdministrationModule.SystemTags
{
    public sealed class GetSystemTag : IQuery<GetSystemTagResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetSystemTagResult
    {
        public Guid Id { get; private set; }
        public Guid SystemId { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }

        public GetSystemTagResult(Guid id, Guid systemId, string title, string description)
        {
            Id = id;
            SystemId = systemId;
            Title = title;
            Description = description;
        }
    }
}