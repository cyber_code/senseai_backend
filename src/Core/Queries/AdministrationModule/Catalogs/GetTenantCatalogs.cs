﻿using Messaging.Queries;
using System;

namespace Queries.AdministrationModule.Systems
{
    public sealed class GetTenantCatalogs : IQuery<GetTenantCatalogsResult[]>
    {
        public Guid ProjectId { get; set; }
        public Guid SubprojectId { get; set; }
    }

    public sealed class GetTenantCatalogsResult
    {
        public GetTenantCatalogsResult(Guid id, Guid projectId, Guid subProjectId,
            string title, string description, string type)
        {
            Id = id;
            ProjectId = projectId;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
            Type = type;
        }

        public Guid Id { get; private set; }

        public Guid ProjectId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string Type { get; private set; }
    }
}