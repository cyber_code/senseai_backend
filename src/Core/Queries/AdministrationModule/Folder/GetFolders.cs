﻿using Messaging.Queries;
using System;

namespace Queries.AdministrationModule.Folder
{
    public class GetFolders : IQuery<GetFoldersResult[]>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetFoldersResult
    {
        public GetFoldersResult(Guid id, Guid subProjectId, string title, string description)
        {
            Id = id;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
        }

        public Guid Id { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }
    }
}