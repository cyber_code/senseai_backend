﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.AdministrationModule.Systems
{
    public sealed class GetSystems : IQuery<GetSystemsResult>
    { 
        public string Title { get; set; }
        public string AdapterName { get; set; } 
    }

    public sealed class GetSystemsResult
    {
        public GetSystemsResult(Guid id, string title, string adapterName)
        {
            Id = id;
            Title = title;
            AdapterName = adapterName;
        }

        public Guid Id { get; private set; }
        public string Title { get; private set; }
        public string AdapterName { get; private set; } 
    }
}