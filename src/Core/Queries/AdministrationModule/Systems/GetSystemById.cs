﻿using Messaging.Queries;
using System;

namespace Queries.AdministrationModule.Systems
{
    public sealed class GetSystemById : IQuery<GetSystemByIdResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetSystemByIdResult
    {
        public GetSystemByIdResult(Guid id, string title, string adapterName)
        {
            Id = id;
            Title = title;
            AdapterName = adapterName;
        }

        public Guid Id { get; private set; }
        public string Title { get; private set; }
        public string AdapterName { get; private set; } 
    }
}