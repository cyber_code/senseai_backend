﻿using Messaging.Queries;
using System;

namespace Queries.AdministrationModule.Settings
{
    public sealed class GetSetting : IQuery<GetSettingResult>
    {
        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }
    }

    public sealed class GetSettingResult
    {
        public GetSettingResult(Guid id, Guid projectId, Guid subProjectId, Guid systemId, Guid catalogId)
        {
            Id = id;
            ProjectId = projectId;
            SubProjectId = subProjectId;
            SystemId = systemId;
            CatalogId = catalogId;
        }

        public Guid Id { get; private set; }

        public Guid ProjectId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public Guid SystemId { get; private set; }

        public Guid CatalogId { get; private set; }
    }
}