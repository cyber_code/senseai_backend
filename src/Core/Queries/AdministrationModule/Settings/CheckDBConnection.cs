﻿using Messaging.Queries;
using System;

namespace Queries.AdministrationModule.Settings
{
    public sealed class CheckDBConnection : IQuery<CheckDBConnectionResult>
    {
    }

    public sealed class CheckDBConnectionResult
    {
        public CheckDBConnectionResult(bool isConnected)
        {
            IsConnected = isConnected;
        }

        public bool IsConnected { get; set; }
    }
}