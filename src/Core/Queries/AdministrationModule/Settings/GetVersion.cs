﻿using Messaging.Queries;

namespace Queries.AdministrationModule.Settings
{
    public sealed class GetVersion : IQuery<GetVersionResult>
    {
    }

    public sealed class GetVersionResult
    {
        public GetVersionResult(string version)
        {
            Version = version;
        }

        public string Version { get; set; }
    }
}