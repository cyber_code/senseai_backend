﻿using Messaging.Queries;
using System; 

namespace Queries.AdministrationModule.SystemCatalogs
{
    public sealed class GetSystemCatalogs : IQuery<GetSystemCatalogsResult []>
    {
        
        public Guid SubProjectId { get;  set; }
        public Guid SystemId { get;  set; } 
    }

    public sealed class GetSystemCatalogsResult
    {
        public GetSystemCatalogsResult(Guid id, string title, int type)
        {
            Id = id;
            Title = title;
            Type = type;
        } 
        public Guid Id { get; private set; } 
        public string Title { get; private set; }

        public int Type { get; private set; }

    }
}