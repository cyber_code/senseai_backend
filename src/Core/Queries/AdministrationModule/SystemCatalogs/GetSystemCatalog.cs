﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.AdministrationModule.SystemCatalogs
{
    public sealed class GetSystemCatalog : IQuery<GetSystemCatalogResult>
    {
        
        public Guid SubProjectId { get;  set; }
        public Guid SystemId { get;  set; }
        public Guid CatalogId { get;  set; }
    }

    public sealed class GetSystemCatalogResult
    {
        public GetSystemCatalogResult(Guid id, Guid subProjectId, Guid systemId, Guid catalogId)
        {
            Id = id;
            SubProjectId = subProjectId;
            SystemId = systemId;
            CatalogId = catalogId;
        } 
        public Guid Id { get; private set; } 
        public Guid SubProjectId { get; private set; } 
        public Guid SystemId { get; private set; } 
        public Guid CatalogId { get; private set; }
    }
}