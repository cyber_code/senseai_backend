﻿using Messaging.Queries;
using System;

namespace Queries.AdministrationModule.SystemCatalogs
{
    public sealed class GetCatalogSystem : IQuery<GetCatalogSystemResult>
    {
        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }
    }

    public sealed class GetCatalogSystemResult
    {
        public GetCatalogSystemResult(Guid id, Guid subProjectId, Guid systemId, Guid catalogId)
        {
            Id = id;
            SubProjectId = subProjectId;
            SystemId = systemId;
            CatalogId = catalogId;
        }

        public Guid Id { get; private set; }

        public Guid SubProjectId { get; private set; }

        public Guid SystemId { get; private set; }

        public Guid CatalogId { get; private set; }
    }
}