﻿using Messaging.Queries;

namespace Queries.AdministrationModule.Folder
{
    public class GetAdapters : IQuery<GetAdaptersResult[]>
    {

    }

    public sealed class GetAdaptersResult
    {
        public GetAdaptersResult(string description)
        {
            Description = description;
        }

        public string Description { get; private set; }
    }
}