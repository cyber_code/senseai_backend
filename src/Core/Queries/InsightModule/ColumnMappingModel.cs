﻿using Messaging.Queries;
using System;
using System.Collections.Generic;

namespace Queries.InsightModule
{
    public sealed class DefectColumnMappingModel : IQuery<List<string>>
    {
    }

    public sealed class ColumnMappingModelResult
    {
        public List<String> PropertyList { get; set; }
    }
}