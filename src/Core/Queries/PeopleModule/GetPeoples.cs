﻿using Messaging.Queries;
using System;

namespace Queries.PeopleModule
{
    public sealed class GetPeoples : IQuery<GetPeoplesResult[]>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetPeoplesResult
    {
        public GetPeoplesResult(Guid id, Guid roleId, string name, string surname, string email, string address, string role,int userIdFromAdminPanel, Guid subProjectId, string color)
        {
            Id = id;
            RoleId = roleId;
            Name = name;
            Surname = surname;
            Email = email;
            Address = address;
            Role = role;
            UserIdFromAdminPanel = userIdFromAdminPanel;
            SubProjectId = subProjectId;
            Color = color;
        }

        public Guid Id { get; private set; }

        public Guid RoleId { get; private set; }
        public string Name { get; private set; }

        public string Surname { get; private set; }
        public string Email { get; private set; }
        public string Address { get; private set; }
        public string Role { get; private set; }
        public int UserIdFromAdminPanel { get; private set; }
        public Guid SubProjectId { get; private set; }
        public string Color { get; private set; }
    }
}