﻿using Messaging.Queries;
using System;

namespace Queries.PeopleModule
{
    public sealed class GetPeoplesByRole : IQuery<GetPeoplesByRoleResult[]>
    {
        public Guid RoleId { get; set; }
    }

    public sealed class GetPeoplesByRoleResult
    {
        public GetPeoplesByRoleResult(Guid id, Guid roleId, string name, string surname, string email, string address,int userIdFromAdminPanel, Guid subProjectId, string color)
        {
            Id = id;
            RoleId = roleId;
            Name = name;
            Surname = surname;
            Email = email;
            Address = address;
            UserIdFromAdminPanel = userIdFromAdminPanel;
            SubProjectId = subProjectId;
            Color = color;
        }

        public Guid Id { get; private set; }

        public Guid RoleId { get; private set; }
        public string Name { get; private set; }

        public string Surname { get; private set; }
        public string Email { get; private set; }
        public string Address { get; private set; }
        public int UserIdFromAdminPanel { get; private set; }
        public Guid SubProjectId { get; private set; }
        public string Color { get; private set; }

    }
}