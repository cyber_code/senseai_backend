﻿using Messaging.Queries;
using System;

namespace Queries.PeopleModule

{
    public sealed class GetRole : IQuery<GetRoleResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetRoleResult
    {
        public GetRoleResult(Guid id, string title, string description, Guid subProjectId)
        {
            Id = id;
            Title = title;
            Description = description;
            SubProjectId = subProjectId;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid SubProjectId { get; private set; }
      
    }
}