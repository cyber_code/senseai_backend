﻿using Messaging.Queries;
using System;

namespace Queries.PeopleModule
{
    public sealed class GetRoles : IQuery<GetRolesResult[]>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetRolesResult
    {
        public GetRolesResult(Guid id, string title, string description, Guid subProjectid)
        {
            Id = id;
            Title = title;
            Description = description;
            SubProjectId = subProjectid;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid SubProjectId { get; private set; }

    }
}