﻿using Messaging.Queries;
using System;

namespace Queries.TestGenerationModule
{
    public sealed class GetHighlightedWorkflowPath : IQuery<GetHighlightedWorkflowPathResult>
    {
        public Guid WorkflowPathId { get; set; }
    }

    public sealed class GetHighlightedWorkflowPathResult
    {
        public GetHighlightedWorkflowPathResult(string desingerJson, Guid[] pathItemIds)
        {
            DesingerJson = desingerJson;
            PathItemIds = pathItemIds;
        }

        public string DesingerJson { get; private set; }

        public Guid[] PathItemIds { get; private set; }
    }
}