﻿using Messaging.Queries;
using System;

namespace Queries.TestGenerationModule
{
    public sealed class ExportTestCases : IQuery<ExportTestCasesResult>
    {
        public Guid WorkflowId { get; set; }

    }

    public sealed class ExportTestCasesResult
    {
        public ExportTestCasesResult(byte[] xml)
        {
            Xml = xml;
        }

        public byte[] Xml { get; private set; }
    }
}