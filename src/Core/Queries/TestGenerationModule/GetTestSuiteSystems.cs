﻿using Messaging.Queries;
using System;

namespace Queries.TestGenerationModule
{
    public sealed class GetTestSuiteSystems : IQuery<GetTestSuiteSystemsResult[]>
    {
        public Guid WorkflowId { get; set; }
    }

    public sealed class GetTestSuiteSystemsResult
    {
        public GetTestSuiteSystemsResult(string title)
        {
            Title = title;
        }
        public string Title { get; private set; }
    }
}