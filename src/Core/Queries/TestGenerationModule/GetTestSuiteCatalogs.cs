﻿using Messaging.Queries;
using System;

namespace Queries.TestGenerationModule
{
    public sealed class GetTestSuiteCatalogs : IQuery<GetTestSuiteCatalogsResult[]>
    {
        public Guid WorkflowId { get; set; }
    }

    public sealed class GetTestSuiteCatalogsResult
    {

        public GetTestSuiteCatalogsResult(string title, Guid catalogId)
        {
            Title = title;
            CatalogId = catalogId;
        }
        public string Title { get; private set; }

        public Guid CatalogId { get; private set; }
    }
}