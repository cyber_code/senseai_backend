﻿using Messaging.Queries;
using System;

namespace Queries.TestGenerationModule
{
    public sealed class GetTestSuite : IQuery<GetTestSuiteResult>
    {
        public Guid WorkflowId { get; set; }

        public Guid WorkflowPathId { get; set; }
    }

    public sealed class GetTestSuiteResult
    {
        public GetTestSuiteResult(string title, TestCasesResult[] testCases)
        {
            Title = title;
            TestCases = testCases;
        }

        public string Title { get; private set; }

        public TestCasesResult[] TestCases { get; private set; }
    }

    public sealed class TestCasesResult
    {
        public TestCasesResult(Guid id, string title, TestStepsResult[] testSteps)
        {
            Id = id;
            Title = title;
            TestSteps = testSteps;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public TestStepsResult[] TestSteps { get; private set; }
    }

    public class TestStepsResult
    {
        public TestStepsResult(Guid id, string title, string coverage, Guid[] dataSetIds,
            Guid typicalId, string typicalName, int actionType, int tsIndex)
        {
            Id = id;
            Title = title;
            Coverage = coverage;
            DataSetIds = dataSetIds;
            TypicalId = typicalId;
            TypicalName = typicalName;
            ActionType = actionType;
            TSIndex = tsIndex + 1;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Coverage { get; private set; }

        public Guid[] DataSetIds { get; private set; }

        public Guid TypicalId { get; private set; }

        public string TypicalName { get; private set; }

        public int ActionType { get; private set; }

        public int TSIndex { get; private set; }
    }
}