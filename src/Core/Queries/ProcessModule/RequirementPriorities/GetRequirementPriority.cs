﻿using System;
using Messaging.Queries;

namespace Queries.ProcessModule.RequirementPriorities
{
    public sealed class GetRequirementPriority : IQuery<GetRequirementPriorityResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetRequirementPriorityResult
    {
        public Guid Id { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Code { get; private set; }
        public int Index { get; private set; }
        public GetRequirementPriorityResult(Guid id, Guid subProjectId, string title, string code, int index)
        {
            Id = id;
            SubProjectId = subProjectId;
            Title = title;
            Code = code;
            Index = index;
        }
    }
}