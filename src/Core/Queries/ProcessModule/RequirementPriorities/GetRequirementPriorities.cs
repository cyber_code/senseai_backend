﻿using System;
using Messaging.Queries;

namespace Queries.ProcessModule.RequirementPriorities
{
    public sealed class GetRequirementPriorities : IQuery<GetRequirementPrioritiesResult[]>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetRequirementPrioritiesResult
    {
        public Guid Id { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Code { get; private set; }
        public int Index { get; private set; }

        public GetRequirementPrioritiesResult(Guid id, Guid subProjectId, string title, string code, int index)
        {
            Id = id;
            SubProjectId = subProjectId;
            Title = title;
            Code = code;
            Index = index;
        }
    }
}