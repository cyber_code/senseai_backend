﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraProjects : IQuery<GetJiraProjectsResult[]>
    {
        public Guid SubprojectId { get; set; }
    }

    public class GetJiraProjectsResult
    {
        public string ProjectId { get; set; }

        public string ProjectName { get; set; }
    }
}