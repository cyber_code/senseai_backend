﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetIssueTypeRequirementFieldsAndAttributes : IQuery<GetIssueTypeFieldsAndAttributesResult[]>
    {
        public Guid SubprojectId { get; set; }
    }
}