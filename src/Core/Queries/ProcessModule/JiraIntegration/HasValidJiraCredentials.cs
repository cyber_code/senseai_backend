﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.JiraIntegration
{
    public class HasValidJiraCredentials : IQuery<bool>
    {
        public Guid SubprojectId { get; set; }

        public bool SkipIsEnabled { get; set; }
    }
}