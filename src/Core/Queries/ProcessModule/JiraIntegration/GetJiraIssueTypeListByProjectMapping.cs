﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueTypeListByProjectMapping : IQuery<GetJiraIssueTypeListByProjectMappingResult[]>
    {
        public Guid JiraProjectMappingId { get; set; }
    }

    public sealed class GetJiraIssueTypeListByProjectMappingResult
    {
        public GetJiraIssueTypeListByProjectMappingResult(Guid id, Guid jiraProjectMappingId, string senseaiIssueTypeField, string senseaiIssueTypeFieldName, string jiraIssueTypeFieldId, string jiraIssueTypeFieldName)
        {
            Id = id;
            JiraProjectMappingId = jiraProjectMappingId;
            SenseaiIssueTypeField = senseaiIssueTypeField;
            SenseaiIssueTypeFieldName = senseaiIssueTypeFieldName;
            JiraIssueTypeFieldId = jiraIssueTypeFieldId;
            JiraIssueTypeFieldName = jiraIssueTypeFieldName;
        }

        public Guid Id { get; private set; }

        public Guid JiraProjectMappingId { get; private set; }

        public string SenseaiIssueTypeField { get; private set; }

        public string SenseaiIssueTypeFieldName { get; private set; }

        public string JiraIssueTypeFieldId { get; private set; }

        public string JiraIssueTypeFieldName { get; private set; }
    }
}