﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssue : IQuery<GetJiraIssueResult>
    {
        public string Key { get; set; }

        public Guid SubprojectId { get; set; }
    }

    public sealed class GetJiraIssueResult
    {
        public string IssueId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Component { get; set; }

        public string Priority { get; set; }

        public string Severity { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

        public string AssignedTo { get; set; }

        public string StartDate { get; set; }

        public string ModifiedDate { get; set; }

        public string TargetDate { get; set; }

        public string TestCaseId { get; set; }
    }
}