﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetJiraIssueTypes : IQuery<GetJiraIssueTypesResult[]>
    {
        public Guid SubprojectId { get; set; }

        public string ProjectKey { get; set; }
    }

    public class GetJiraIssueTypesResult
    {
        public string IssueTypeId { get; set; }

        public string IssueTypeName { get; set; }
    }
}