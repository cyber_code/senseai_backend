﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueTypeAttributeValueMapping : IQuery<GetJiraIssueTypeAttributeValueMappingResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetJiraIssueTypeAttributeValueMappingResult
    {
        public GetJiraIssueTypeAttributeValueMappingResult(Guid id, Guid jiraIssueTypeAttributeMappingId, string senseaiValue, string jiraValue)
        {
            Id = id;
            JiraIssueTypeAttributeMappingId = jiraIssueTypeAttributeMappingId;
            SenseaiValue = senseaiValue;
            JiraValue = jiraValue;
        }

        public Guid Id { get; private set; }

        public Guid JiraIssueTypeAttributeMappingId { get; private set; }

        public string SenseaiValue { get; private set; }

        public string JiraValue { get; private set; }
    }
}