﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueTypeMapping : IQuery<GetJiraIssueTypeMappingResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetJiraIssueTypeMappingResult
    {
        public GetJiraIssueTypeMappingResult(Guid id, Guid jiraProjectMappingId, string senseaiIssueTypeField, string jiraIssueTypeField, bool allowAttachments, List<GetAttributeMapping> getAttributeMapping)
        {
            Id = id;
            JiraProjectMappingId = jiraProjectMappingId;
            SenseaiIssueTypeField = senseaiIssueTypeField;
            JiraIssueTypeField = jiraIssueTypeField;
            GetAttributeMapping = getAttributeMapping;
            AllowAttachments = allowAttachments;
        }

        public Guid Id { get; private set; }

        public Guid JiraProjectMappingId { get; private set; }

        public string SenseaiIssueTypeField { get; private set; }

        public string JiraIssueTypeField { get; private set; }

        public bool AllowAttachments { get; private set; }

        public List<GetAttributeMapping> GetAttributeMapping { get; set; }
    }

    public class GetAttributeMapping
    {
        public Guid JiraIssueTypeMappingId { get; set; }

        public string SenseaiValue { get; set; }

        public string JiraValue { get; set; }

        public bool IsMandatory { get; set; }

        public int Index { get; set; }

        public List<GetAttributeValuesMapping> GetAttributeValuesMapping { get; set; }
    }

    public class GetAttributeValuesMapping
    {
        public Guid AttributeMappingId { get; set; }

        public string SenseaiValue { get; set; }

        public string JiraValue { get; set; }
    }
}