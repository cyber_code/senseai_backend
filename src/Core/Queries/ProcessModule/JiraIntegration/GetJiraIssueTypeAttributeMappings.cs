﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueTypeAttributeMappings : IQuery<GetJiraIssueTypeAttributeMappingsResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetJiraIssueTypeAttributeMappingsResult
    {
        public GetJiraIssueTypeAttributeMappingsResult(Guid id, Guid jiraIssueTypeMappingId, string senseaiField, string jiraField, bool required, int index)
        {
            Id = id;
            JiraIssueTypeMappingId = jiraIssueTypeMappingId;
            SenseaiField = senseaiField;
            JiraField = jiraField;
            Required = required;
            Index = index;
        }

        public Guid Id { get; private set; }

        public Guid JiraIssueTypeMappingId { get; private set; }

        public string SenseaiField { get; private set; }

        public string JiraField { get; private set; }

        public bool Required { get; private set; }

        public int Index { get; private set; }
    }
}