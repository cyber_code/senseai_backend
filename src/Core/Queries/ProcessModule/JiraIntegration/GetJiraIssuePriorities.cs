﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetJiraIssuePriorities : IQuery<GetJiraIssuePrioritiesResult[]>
    {
        public Guid SubprojectId { get; set; }
    }

    public class GetJiraIssuePrioritiesResult
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}