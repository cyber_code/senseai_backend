﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetJiraUserMapping : IQuery<GetJiraUserMappingResult[]>
    {
        public Guid SubprojectId { get; set; }
    }

    public sealed class GetJiraUserMappingResult
    {
        public GetJiraUserMappingResult(Guid id, Guid subprojectId, string jiraUserId, string jiraUserDisplayName, string senseaiUserId, string senseaiUserDisplayName)
        {
            Id = id;
            SubprojectId = subprojectId;
            JiraUserId = jiraUserId;
            JiraUserDisplayName = jiraUserDisplayName;
            SenseaiUserId = senseaiUserId;
            SenseaiUserDisplayName = senseaiUserDisplayName;
        }

        public Guid Id { get; set; }

        public Guid SubprojectId { get; private set; }

        public string JiraUserId { get; private set; }

        public string JiraUserDisplayName { get; private set; }

        public string SenseaiUserId { get; private set; }

        public string SenseaiUserDisplayName { get; private set; }
    }
}