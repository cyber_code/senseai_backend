﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetJiraIssueStatuses : IQuery<GetJiraIssueStatusesResult[]>
    {
        public Guid SubprojectId { get; set; }

        public string projectKey { get; set; }
    }

    public class GetJiraIssueStatusesResult
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}