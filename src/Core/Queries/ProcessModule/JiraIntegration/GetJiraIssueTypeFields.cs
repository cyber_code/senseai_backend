﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetJiraIssueTypeFieldsAndAttributes : IQuery<GetJiraIssueTypeFieldsAndAttributesResult[]>
    {
        public Guid SubprojectId { get; set; }

        public string IssueTypeId { get; set; }

        public string JiraProjectKey { get; set; }
    }

    public class GetJiraIssueTypeFieldsAndAttributesResult
    {
        public string FieldName { get; set; }

        public GetJiraIssueTypeFieldsInfoResultValue[] Values { get; set; }

        public bool IsMandatory { get; set; }

        public bool IsFieldWithValues
        {
            get
            {
                return Values?.Any() ?? false;
            }
        }
    }

    public class GetJiraIssueTypeFieldsInfoResultValue
    {
        public string Id { get; set; }

        public string Text { get; set; }
    }
}