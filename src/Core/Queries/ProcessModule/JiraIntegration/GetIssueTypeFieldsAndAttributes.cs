﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetIssueTypeFieldsAndAttributes : IQuery<GetIssueTypeFieldsAndAttributesResult[]>
    {
        public Guid IssueTypeId { get; set; }

        public Guid SubprojectId { get; set; }
    }

    public class GetIssueTypeFieldsAndAttributesResult
    {
        public string FieldName { get; set; }

        public GetIssueTypeFieldsInfoResultValue[] Values { get; set; }

        public bool IsMandatory { get; set; }

        public bool IsFieldWithValues
        {
            get
            {
                return Values?.Any() ?? false;
            }
        }
    }

    public class GetIssueTypeFieldsInfoResultValue
    {
        public string Id { get; set; }

        public string Text { get; set; }
    }
}