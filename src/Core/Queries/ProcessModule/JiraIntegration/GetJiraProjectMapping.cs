﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetJiraProjectMapping : IQuery<GetJiraProjectMappingResult>
    {
        public Guid SubprojectId { get; set; }
    }

    public sealed class GetJiraProjectMappingResult
    {
        public GetJiraProjectMappingResult(Guid id, Guid projectId, Guid subprojectId, string jiraProjectKey, string jiraProjectName,
            bool mapHierarchy, string hierarchyParentName, Guid hierarchyParentId, string hierarchyTypeName, Guid hierarchyTypeId)
        {
            Id = id;
            ProjectId = projectId;
            SubprojectId = subprojectId;
            JiraProjectKey = jiraProjectKey;
            JiraProjectName = jiraProjectName;
            MapHierarchy = mapHierarchy;
            HierarchyParentName = hierarchyParentName;
            HierarchyParentId = hierarchyParentId;
            HierarchyTypeName = hierarchyTypeName;
            HierarchyTypeId = hierarchyTypeId;
        }

        public Guid Id { get; set; }

        public Guid ProjectId { get; private set; }

        public Guid SubprojectId { get; private set; }

        public string JiraProjectKey { get; private set; }

        public string JiraProjectName { get; private set; }

        public bool MapHierarchy { get; set; }

        public Guid HierarchyParentId { get; set; }

        public Guid HierarchyTypeId { get; set; }

        public string HierarchyParentName { get; set; }

        public string HierarchyTypeName { get; set; }
    }
}