﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetJiraUsers : IQuery<GetJiraUsersResult[]>
    {
        public Guid SubprojectId { get; set; }
    }

    public class GetJiraUsersResult
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}