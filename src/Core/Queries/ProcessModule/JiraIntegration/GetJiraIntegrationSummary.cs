﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIntegrationSummary : IQuery<GetJiraIntegrationSummaryResult>
    {
        public Guid ProjectMappingId { get; set; }
    }

    public class GetJiraIntegrationSummaryResult
    {
        public GetJiraIntegrationSummaryResult(string jiraProjectName, string senseaiProjectName, List<IssueTypesSummary> issueTypesSyncronized, int userMappedCount)
        {
            JiraProjectName = jiraProjectName;
            SenseaiProjectName = senseaiProjectName;
            IssueTypesSyncronized = issueTypesSyncronized;
            UserMappedCount = userMappedCount;
        }

        public string JiraProjectName { get; set; }

        public string SenseaiProjectName { get; set; }

        public List<IssueTypesSummary> IssueTypesSyncronized { get; set; }

        public int UserMappedCount { get; set; }
    }

    public class IssueTypesSummary
    {
        public IssueTypesSummary(string jiraIssueTypeName, string senseaiIssueTypeName)
        {
            JiraIssueTypeName = jiraIssueTypeName;
            SenseaiIssueTypeName = senseaiIssueTypeName;
        }

        public string JiraIssueTypeName { get; set; }

        public string SenseaiIssueTypeName { get; set; }
    }
}