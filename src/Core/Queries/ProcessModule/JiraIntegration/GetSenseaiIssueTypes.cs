﻿using Messaging.Queries;
using Queries.ProcessModule.Issues;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetSenseaiIssueTypes : IQuery<GetIssueTypesResult[]>
    {
        public Guid SubProjectId { get; set; }
    }
}