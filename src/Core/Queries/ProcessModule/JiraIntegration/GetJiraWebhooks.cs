﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetJiraWebhooks : IQuery<GetJiraWebhooksResult>
    {
        public Guid subProjectId { get; set; }
    }

    public class GetJiraWebhooksResult
    {
        public string Self { get; set; }

        public string Name { get; set; }

        public string[] Events { get; set; }

        public Filters Filters { get; set; }

        public string Url { get; set; }

        public bool Enabled { get; set; }
    }

    public class Filters
    {
        //[JsonProperty(PropertyName = "issue-related-events-section")]
        public string IssueRelatedEventsSection { get; set; }
    }
}