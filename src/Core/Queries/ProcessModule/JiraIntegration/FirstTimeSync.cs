﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class FirstTimeSync : IQuery<bool>
    {
        public Guid SubprojectId { get; set; }

        public string SenseAIUser { get; set; }

        public bool Sync { get; set; }
    }
}