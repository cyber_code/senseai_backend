﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetJiraUserSubprojectConfigs : IQuery<GetJiraUserSubprojectConfigsResult>
    {
        public Guid SubprojectId { get; set; }

        public string UserId { get; set; }
    }

    public class GetJiraUserSubprojectConfigsResult
    {
        public GetJiraUserSubprojectConfigsResult()
        {
        }

        public GetJiraUserSubprojectConfigsResult(string jiraUsername, string jiraToken, Guid subprojectId, string userId, string jiraUrl)
        {
            JiraUsername = jiraUsername;
            JiraToken = jiraToken;
            SubprojectId = subprojectId;
            UserId = userId;
            JiraUrl = jiraUrl;
        }

        public string JiraUsername { get; set; }

        public string JiraToken { get; set; }

        public Guid SubprojectId { get; set; }

        public string UserId { get; set; }

        public string JiraUrl { get; set; }
    }
}