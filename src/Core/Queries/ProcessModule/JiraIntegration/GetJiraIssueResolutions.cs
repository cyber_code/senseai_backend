﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.JiraIntegration
{
    public class GetJiraIssueResolutions : IQuery<GetJiraIssueResolutionsResult[]>
    {
        public Guid SubprojectId { get; set; }
    }

    public class GetJiraIssueResolutionsResult
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}