﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.ArisWorkflows
{
    public class GetArisWorkflowVersionsByDate : IQuery<GetArisWorkflowVersionsByDateResult[]>
    { 
        public Guid ArisWorkflowId { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }
    }

    public sealed class GetArisWorkflowVersionsByDateResult
    {
        public GetArisWorkflowVersionsByDateResult(Guid arisWorkflowId, int version, string title, string description, bool isLast, string workflowImage, DateTime? dateCreated)
        {
            ArisWorkflowId = arisWorkflowId;
            Version = version;
            Title = title;
            Description = description;
            IsLast = isLast;
            WorkflowImage = workflowImage;
            DateCreated = dateCreated;
        }

        public Guid ArisWorkflowId { get; private set; }

        public int Version { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public bool IsLast { get; private set; }

        public string WorkflowImage { get; private set; }

        public DateTime? DateCreated { get; private set; }
    }
}