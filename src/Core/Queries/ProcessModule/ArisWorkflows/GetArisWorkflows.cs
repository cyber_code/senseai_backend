﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.ArisWorkflows
{
    public sealed class GetArisWorkflows : IQuery<GetArisWorkflowResult>
    {
        public Guid ProcessId { get; set; }

    }

    public sealed class GetArisWorkflowsResult
    {
        public GetArisWorkflowsResult(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public Guid Id { get; private set; }

        public string Title  { get; private set; }
    }
}
