﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.ArisWorkflows
{
    public sealed class SearchArisWorkflows : IQuery<SearchArisWorkflowsResult[]>
    {
        public string SearchString { get; set; }

        public Guid SubProjectId { get; set; }
    }

    public sealed class SearchArisWorkflowsResult
    {
        public SearchArisWorkflowsResult(Guid id, string title, string description, Guid processId)
        {
            Id = id;
            Title = title;
            Description = description;
            ProcessId = processId;
        }

        public Guid Id { get; set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid ProcessId { get; private set; }
    }
}