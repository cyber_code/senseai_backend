﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.ArisWorkflows
{
    public sealed class GetArisWorkflow : IQuery<GetArisWorkflowResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetArisWorkflowResult
    {
        public GetArisWorkflowResult(Guid id, Guid processId, string title, string description, string designerJson, bool isParsed)
        {
            Id = id;
            ProcessId = processId;
            Title = title;
            Description = description;
            DesignerJson = designerJson;
            Isparsed = isParsed;
        }

        public Guid Id { get; private set; }

        public Guid ProcessId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string DesignerJson { get; private set; }

        public bool Isparsed { get; private set; }
    }
}
