﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.ArisWorkflows
{
    public class GetArisWorkflowVersions : IQuery<GetArisWorkflowVersionsResult[]>
    {        
        public Guid ArisWorkflowId { get; set; }
    }

    public sealed class GetArisWorkflowVersionsResult
    {
        public GetArisWorkflowVersionsResult(Guid arisWorkflowId, int version, string title)
        {
            ArisWorkflowId = arisWorkflowId;
            Version = version;
            Title = title; 
        }

        public Guid ArisWorkflowId { get; private set; }

        public int Version { get; private set; }

        public string Title { get; private set; }
    }
}