﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.ProcessResources
{
    public sealed class GetProcessResources : IQuery<GetProcessResourcesResult[]>
    {
        public Guid ProcessId { get; set; }
    }
    public sealed class GetProcessResourcesResult 
    {
        public GetProcessResourcesResult(Guid id, string fileName, Guid processId)
        {
            Id = id;
            FileName = fileName;
            ProcessId = processId;
        }

        public Guid Id { get; private set; }

        public string FileName { get; private set; }

        public Guid ProcessId { get; private set; }
    }
}
