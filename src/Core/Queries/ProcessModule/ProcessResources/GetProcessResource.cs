﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.ProcessResources
{
   public sealed class GetProcessResource : IQuery<GetProcessResourceResult>
    {
        public Guid Id { get; set; }
    }
    public sealed class GetProcessResourceResult
    {
        public GetProcessResourceResult(Guid id, string fileName, Guid processId)
        {
            Id = id;
            FileName = fileName;
            ProcessId = processId;
        }

        public Guid Id { get; private set; }

        public string FileName { get; private set; }

        public Guid ProcessId { get; private set; }
    }
}
