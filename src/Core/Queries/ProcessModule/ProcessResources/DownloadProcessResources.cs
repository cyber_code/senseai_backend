﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.ProcessResources
{
   public sealed class DownloadProcessResources: IQuery<DownloadProcessResourcesResult[]>
    {
        public Guid ProcessId { get; set; }
    }
    public sealed class DownloadProcessResourcesResult
    {
        public DownloadProcessResourcesResult(Guid id, string fileName, Guid processId, byte[] file)
        {
            Id = id;
            FileName = fileName;
            ProcessId = processId;
            File = file;

        }

        public Guid Id { get; private set; }

        public string FileName { get; private set; }

        public Guid ProcessId { get; private set; }

        public byte[] File { get; private set; }
    }
}
