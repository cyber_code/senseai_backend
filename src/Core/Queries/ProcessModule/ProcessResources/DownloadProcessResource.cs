﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Queries.ProcessModule.ProcessResources
{
   public sealed class DownloadProcessResource : IQuery<DownloadProcessResourceResult>
    {
        public Guid Id { get; set; }
    }
    public sealed class DownloadProcessResourceResult
    {
        public DownloadProcessResourceResult(FileStream file, string fileName)
        {
            File = file;
            FileName = fileName;
        }

       
        public string FileName { get; private set; }

        public FileStream File { get; private set; }
    }
}
