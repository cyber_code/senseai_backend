﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetIssueTypeFieldsName : IQuery<GetIssueTypeFieldsNameResult>
    {  
        public Guid Id { get; set; }
    }

    public sealed class GetIssueTypeFieldsNameResult
    {
        public GetIssueTypeFieldsNameResult(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public Guid Id { get; private set; }

        public string Name { get; private set; }

        
    }
}