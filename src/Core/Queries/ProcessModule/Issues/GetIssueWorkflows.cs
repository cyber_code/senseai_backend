﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetIssueWorkflows : IQuery<GetIssueWorkflowsResult[]>
    {
        public Guid IssueId { get; set; }
    }

    public sealed class GetIssueWorkflowsResult
    {
        public GetIssueWorkflowsResult(Guid workflowId, string workflowTitle)
        {
            WorkflowId = workflowId;
            WorkflowTitle = workflowTitle;
        }

        public Guid WorkflowId { get; private set; }

        public string WorkflowTitle { get; private set; }
    }
}