﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetIssueTypes : IQuery<GetIssueTypesResult[]>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetIssueTypesResult
    {
        public GetIssueTypesResult(Guid id, Guid subProjectId, string title, string description, string unit, bool def)
        {
            Id = id;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
            Unit = unit;
            Default = def;
        }

        public Guid Id { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string Unit { get; private set; }
        public bool Default { get; private set; }
    }
}