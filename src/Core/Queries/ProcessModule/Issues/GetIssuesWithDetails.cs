﻿using Messaging.Queries;
using SenseAI.Domain;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetIssuesWithDetails : IQuery<GetIssuesWithDetailsResult[]>
    {

        public Guid SubProjectId { get; set; }
    }

    public sealed class GetIssuesWithDetailsResult
    {
        public GetIssuesWithDetailsResult(Guid id, Guid processId, Guid subProjectId, Guid issueTypeId, string issueTypeTitle, Guid assigneeId, string assigneeName, string title, string description, StatusType status, string statusTitle, int estimate, string reason, string implication, string comment, Guid typicalId, string attribute, bool approvedForInvestigation, InvestigationStatus investigationStatus, string investigationStatusTitle, ChangeStatus changeStatus, string changeStatusTitle, Guid reporterId, string reporterName, Guid approverId, string approverName, DateTime dateSubmitted, DateTime approvalDate, DateTime startDate, IssuePriority priority, string priorityTitle, IssueSeverity severity, string severityTitle, DateTime dueDate, TypeDefect type, string typeTitle, Guid componentId, string componentTitle, Guid? sprintId, string sprintTitle, string testCaseId)
        {
            Id = id;
            ProcessId = processId;
            SubProjectId = subProjectId;
            IssueTypeId = issueTypeId;
            IssueTypeTitle = issueTypeTitle;
            AssigneeId = assigneeId;
            AssigneeName = assigneeName;
            Title = title;
            Description = description;
            Status = status;
            StatusTitle = statusTitle;
            Estimate = estimate;
            Reason = reason;
            Implication = implication;
            Comment = comment;
            TypicalId = typicalId;
            Attribute = attribute;
            ApprovedForInvestigation = approvedForInvestigation;
            InvestigationStatus = investigationStatus;
            InvestigationStatusTitle = investigationStatusTitle;
            ChangeStatus = changeStatus;
            ChangeStatusTitle = changeStatusTitle;
            ReporterId = reporterId;
            ReporterName = reporterName;
            ApproverId = approverId;
            ApproverName = approverName;
            DateSubmitted = dateSubmitted;
            ApprovalDate = approvalDate;
            StartDate = startDate;
            Priority = priority;
            PriorityTitle = priorityTitle;
            Severity = severity;
            SeverityTitle = severityTitle;
            DueDate = dueDate;
            Type = type;
            TypeTitle = typeTitle;
            ComponentId = componentId;
            ComponentTitle = componentTitle;
            SprintId = sprintId;
            SprintTitle = sprintTitle;
            TestCaseId = testCaseId;
        }

        public Guid Id { get; private set; }

        public Guid ProcessId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public Guid IssueTypeId { get; private set; }
        public string IssueTypeTitle { get; private set; }

        public Guid AssigneeId { get; private set; }
        public string AssigneeName { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public SenseAI.Domain.StatusType Status { get; private set; }
        public string StatusTitle { get; private set; }

        public int Estimate { get; private set; }
        public string Reason { get; set; }

        public string Implication { get; set; }

        public string Comment { get; set; }

        public Guid TypicalId { get; set; }

        public string Attribute { get; set; }

        public bool ApprovedForInvestigation { get; set; }
        public InvestigationStatus InvestigationStatus { get; set; }
        public string InvestigationStatusTitle { get; set; }

        public ChangeStatus ChangeStatus { get; set; }
        public string ChangeStatusTitle { get; set; }

        public Guid ReporterId { get; set; }
        public string ReporterName { get; set; }

        public Guid ApproverId { get; set; }
        public string ApproverName { get; set; }

        public DateTime DateSubmitted { get; set; }

        public DateTime ApprovalDate { get; set; }

        public DateTime StartDate { get; set; }
        public IssuePriority Priority { get; set; }
        public string PriorityTitle { get; set; }
        public IssueSeverity Severity { get; set; }
        public string SeverityTitle { get; set; }
        public DateTime DueDate { get; set; }
        public TypeDefect Type { get; set; }
        public string TypeTitle { get; set; }
        public Guid ComponentId { get; set; }
        public string ComponentTitle { get; set; }
        public Guid? SprintId { get; set; }
        public string SprintTitle { get; set; }
        public string TestCaseId { get; set; }
    }
}