﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetWorkflowIssues : IQuery<GetWorkflowIssuesResult[]>
    {
        public Guid WorkflowId { get; set; }
    }

    public sealed class GetWorkflowIssuesResult
    {
        public GetWorkflowIssuesResult(Guid issueId, string issueTitle)
        {
            IssueId = issueId;
            IssueTitle = issueTitle;
        }

        public Guid IssueId { get; private set; }

        public string IssueTitle { get; private set; }
    }
}