﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.Issues
{
    public sealed class GetIssueResources : IQuery<GetIssueResourcesResult[]>
    {
        public Guid IssueId { get; set; }
    }
    public sealed class GetIssueResourcesResult
    {
        public GetIssueResourcesResult(Guid id, Guid issueId, string title, string path)
        {
            Id = id;
            IssueId = issueId;
            Title = title;
            Path = path;
        }

        public Guid Id { get; private set; } 
        public Guid IssueId { get; private set; } 
        public string Title { get; private set; } 
        public string Path { get; private set; }
    }
}
