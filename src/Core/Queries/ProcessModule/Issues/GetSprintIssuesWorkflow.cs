﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetSprintIssuesWorkflow : IQuery<GetSprintIssuesWorkflowResult[]>
    {
        public Guid SprintId { get; set; }
    }

    public sealed class GetSprintIssuesWorkflowResult
    {
        public GetSprintIssuesWorkflowResult(Guid id)
        {
            Id = id;

        }

        public Guid Id { get; private set; }


    }
}