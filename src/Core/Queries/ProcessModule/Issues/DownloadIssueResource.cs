﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Queries.ProcessModule.Issues
{
   public sealed class DownloadIssueResource : IQuery<DownloadIssueResourceResult>
    {
        public Guid Id { get; set; }
    }
    public sealed class DownloadIssueResourceResult
    {
        public DownloadIssueResourceResult(FileStream file, string fileName)
        {
            File = file;
            FileName = fileName;
        }

       
        public string FileName { get; private set; }

        public FileStream File { get; private set; }
    }
}
