﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.ProcessModule.Issues
{
    public sealed class GetIssueComments : IQuery<GetIssueCommentsResult[]>
    {
        public Guid IssueId { get; set; }
    }
    public sealed class GetIssueCommentsResult
    {
        public GetIssueCommentsResult(Guid id, Guid issueId, string comment, Guid commenterId, Guid parentId)
        {
            Id = id;
            IssueId = issueId;
            Comment = comment;
            CommenterId = commenterId;
            ParentId = parentId;
        }

        public Guid Id { get; private set; }
        public Guid IssueId { get; private set; }
        public string Comment { get; private set; }
        public Guid CommenterId { get; private set; }
        public Guid ParentId { get; private set; }

    }
}
