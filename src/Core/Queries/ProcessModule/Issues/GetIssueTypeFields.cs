﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetIssueTypeFields : IQuery<GetIssueTypeFieldsResult>
    {  
        public Guid Id { get; set; }
    }

    public sealed class GetIssueTypeFieldsResult
    {
        public GetIssueTypeFieldsResult(Guid id, Guid issueTypeId,Guid issueTypeFieldsNameId, bool check, string issueTypeFieldsName)
        {
            Id = id;
            IssueTypeId = issueTypeId;
            IssueTypeFieldsNameId = issueTypeFieldsNameId;
            Checked = check;
            IssueTypeFieldsName = issueTypeFieldsName;
        }

        public Guid Id { get; private set; }

        public Guid IssueTypeId { get; private set; }

        public Guid IssueTypeFieldsNameId { get; private set; }

        public string IssueTypeFieldsName { get; private set; }

        public bool Checked { get; private set; }
    }
}