﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetIssueTypesFields : IQuery<GetIssueTypesFieldsResult[]>
    {
        public Guid IssueTypeId { get; set; }
    }

    public sealed class GetIssueTypesFieldsResult
    {
        public GetIssueTypesFieldsResult(Guid id, Guid issueTypeId, Guid issueTypeFieldsNameId, bool check, string issueTypeFieldsName)
        {
            Id = id;
            IssueTypeId = issueTypeId;
            IssueTypeFieldsNameId = issueTypeFieldsNameId;
            Checked = check;
            IssueTypeFieldsName = issueTypeFieldsName;
        }

        public Guid Id { get; private set; }

        public Guid IssueTypeId { get; private set; }

        public Guid IssueTypeFieldsNameId { get; private set; }

        public bool Checked { get; private set; }
        public string IssueTypeFieldsName { get; private set; }
    }
}