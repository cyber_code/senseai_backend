﻿using Messaging.Queries;
using SenseAI.Domain;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetIssue : IQuery<GetIssueResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetIssueResult
    {
        public GetIssueResult(Guid id, Guid processId, Guid subProjectId, Guid issueTypeId, Guid assignedId, string title, string description, SenseAI.Domain.StatusType status, int estimate,string reason, string implication, string comment, Guid typicalId, string attribute, bool approvedForInvestigation, InvestigationStatus investigationStatus, ChangeStatus changeStatus, Guid reporterId, Guid approvedId, DateTime dateSubmitted, DateTime approvalDate, DateTime startDate, IssuePriority priority, IssueSeverity severity, DateTime dueDate, TypeDefect type, Guid componentId, string testCaseId)
        {
            Id = id;
            ProcessId = processId;
            SubProjectId = subProjectId;
            IssueTypeId = issueTypeId;
            AssignedId = assignedId;
            Title = title;
            Description = description;
            Status = status;
            Estimate = estimate;
            Reason = reason;
            Implication = implication;
            Comment = comment;
            TypicalId = typicalId;
            Attribute = attribute;
            ApprovedForInvestigation = approvedForInvestigation;
            InvestigationStatus = investigationStatus;
            ChangeStatus = changeStatus;
            ReporterId = reporterId;
            ApprovedId = approvedId;
            DateSubmitted = dateSubmitted;
            ApprovalDate = approvalDate;
            StartDate = startDate;
            Priority = priority;
            Severity = severity;
            DueDate = dueDate;
            Type = type;
            ComponentId = componentId;
            TestCaseId = testCaseId;
        }

        public Guid Id { get; private set; }

        public Guid ProcessId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public Guid IssueTypeId { get; private set; }

        public Guid AssignedId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public SenseAI.Domain.StatusType Status { get; private set; }

        public int Estimate { get; private set; }
        public string Reason { get; set; }

        public string Implication { get; set; }

        public string Comment { get; set; }

        public Guid TypicalId { get; set; }

        public string Attribute { get; set; }

        public bool ApprovedForInvestigation { get; set; }
        public InvestigationStatus InvestigationStatus { get; set; }

        public ChangeStatus ChangeStatus { get; set; }

        public Guid ReporterId { get; set; }

        public Guid ApprovedId { get; set; }

        public DateTime DateSubmitted { get; set; }

        public DateTime ApprovalDate { get; set; }

        public DateTime StartDate { get; set; }
        public IssuePriority Priority { get; set; }
        public IssueSeverity Severity { get; set; }
        public DateTime DueDate { get; set; }
        public TypeDefect Type { get; set; }
        public Guid ComponentId { get; set; }
        public string TestCaseId { get; set; }
    }
}