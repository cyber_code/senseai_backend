﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetIssueTypeFieldsNames : IQuery<GetIssueTypeFieldsNamesResult[]>
    {
     
    }

    public sealed class GetIssueTypeFieldsNamesResult
    {
        public GetIssueTypeFieldsNamesResult(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public Guid Id { get; private set; }

        public string Name { get; private set; }

    }
}