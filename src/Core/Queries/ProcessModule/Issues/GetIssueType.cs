﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetIssueType : IQuery<GetIssueTypeResult>
    {  
        public Guid Id { get; set; }
    }

    public sealed class GetIssueTypeResult
    {
        public GetIssueTypeResult(Guid id, Guid subProjectId, string title, string description, string unit, bool def)
        {
            Id = id;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
            Unit = unit;
            Default = def;
        }

        public Guid Id { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string Unit { get; private set; }
        public bool Default { get; private set; }
    }
}