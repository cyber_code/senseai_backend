﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetWorkflowIssue : IQuery<GetWorkflowIssueResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetWorkflowIssueResult
    {
        public GetWorkflowIssueResult(Guid id, Guid workflowId, Guid issueId)
        {
            Id = id;
            WorkflowId = workflowId;
            IssueId = issueId;
        }

        public Guid Id { get; private set; }

        public Guid WorkflowId { get; private set; }

        public Guid IssueId { get; private set; }
    }
}