﻿using Messaging.Queries;
using SenseAI.Domain;
using System;

namespace Queries.ProcessModule.Issues
{
    public sealed class GetLinkedIssues : IQuery<GetLinkedIssuesResult[]>
    {
        public Guid IssueId { get; set; }
    }

    public sealed class GetLinkedIssuesResult
    {
        public Guid IssueId { get; set; }
        public IssueLinkType LinkType { get; set; }
        public bool IsDestination { get; set; }
        public string Title { get; set; }

        public GetLinkedIssuesResult()
        {
        }

        public GetLinkedIssuesResult(Guid issueId, IssueLinkType linkType, bool isDestination, string title)
        {
            IssueId = issueId;
            LinkType = linkType;
            IsDestination = isDestination;
            Title = title;
        }
    }
}
