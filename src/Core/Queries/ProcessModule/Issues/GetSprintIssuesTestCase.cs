﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Issues
{
    public class GetSprintIssuesTestCase : IQuery<GetSprintIssuesTestCaseResult[]>
    {
        public Guid SprintId { get; set; }
    }

    public sealed class GetSprintIssuesTestCaseResult
    {
        public GetSprintIssuesTestCaseResult(long id)
        {
            Id = id;
        }

        public long Id { get; private set; }

    }
}