﻿using Messaging.Queries;
using System;
using System.Collections.Generic;

namespace Queries.ProcessModule.Sprints
{
    public sealed class GetSprintStatistics: IQuery<GetSprintStatisticsResult[]>
    {
        public Guid SprintId { get; set; }
    }
    public sealed class GetSprintStatisticsResult
    {
        public GetSprintStatisticsResult(Guid hierarchyId, Guid processId, int nrTotalIssues, int nrOpenIssues, int nrWorkflows, int nrTestCases)
        {
            HierarchyId = hierarchyId;
            ProcessId = processId;
            NrTotalIssues = nrTotalIssues;
            NrOpenIssues = nrOpenIssues;
            NrWorkflows = nrWorkflows;
            NrTestCases = nrTestCases;
        }

        public Guid HierarchyId { get; private set; }
        public Guid ProcessId { get; private set; }
        public int NrTotalIssues { get; private set; }
        public int NrOpenIssues { get; private set; }
        public int NrWorkflows { get; private set; }
        public int NrTestCases { get; private set; }
    }
    
}
