﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Sprints

{
    public sealed class GetSprintIssues : IQuery<GetSprintIssuesResult[]>
    {
        public Guid SprintId { get; set; }
    }

    public sealed class GetSprintIssuesResult
    {
        public GetSprintIssuesResult(Guid id, Guid sprintId, Guid issueId, int status, Guid processId)
        {
            Id = id;
            SprintId = sprintId;
            IssueId = issueId;
            Status = status;
            ProcessId = processId;
        }

        public Guid Id { get; private set; }
        public Guid SprintId { get; private set; }
        public Guid ProcessId { get; private set; }
        public Guid IssueId { get; private set; }
        public int Status { get;  set; }
    }
}