﻿using Messaging.Queries;
using SenseAI.Domain;
using System;

namespace Queries.ProcessModule.Sprints

{
    public sealed class GetSprint : IQuery<GetSprintResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetSprintResult
    {
        public GetSprintResult(Guid id, string title, string description, DateTime start, DateTime end, Guid subProjectId, int duration, DurationUnit durationUnit)
        {
            Id = id;
            Title = title;
            Description = description;
            Start = start;
            End = end;
            SubProjectId = subProjectId;
            Duration = duration;
            DurationUnit = durationUnit;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }
        public Guid SubProjectId { get; private set; }
        public int Duration { get; private set; }
        public DurationUnit DurationUnit { get; private set; }
    }
}