﻿using System;
using Messaging.Queries;

namespace Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchiesByParent : IQuery<GetHierarchiesByParentResult[]>
    {
        public Guid SubProjectId { get; set; }

        public string HierarchyTypeId { get; set; }

        public string ParentId { get; set; }
    }

    public sealed class GetHierarchiesByParentResult
    {
        public GetHierarchiesByParentResult(Guid id, string title, string description, Guid parentId, Guid hierarchyTypeId)
        {
            Id = id;
            Title = title;
            Description = description;
            ParentId = parentId;
            HierarchyTypeId = hierarchyTypeId;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid ParentId { get; private set; }

        public Guid HierarchyTypeId { get; private set; } 
    }
}