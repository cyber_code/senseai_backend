﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchiesStatistics : IQuery<GetHierarchiesStatisticsResult[]>
    {
        public Guid SubProjectId { get; set; }
        public Guid ParentId { get; set; }
    }

    public sealed class GetHierarchiesStatisticsResult
    {



        public GetHierarchiesStatisticsResult(int nrProcesses, int nrWorkflows, int nrTestCases, int nrTotalIssues, int nrOpenIssues,Guid hierarchyId, string title, int nrTestSteps)
        {
            NrProcesses = nrProcesses;
            NrWorkflows = nrWorkflows;
            NrTestCases = nrTestCases;
            NrTotalIssues = nrTotalIssues;
            NrOpenIssues = nrOpenIssues;
            HierarchyId = hierarchyId;
            Title = title;
            NrTestSteps = nrTestSteps;
        }


        public Guid HierarchyId { get; private set; }

        public string Title { get; private set; }

        public int NrProcesses { get; private set; }
        public int NrWorkflows { get; private set; }
        public int NrTestCases { get; private set; }
        public int NrTestSteps { get; private set; }
        public int NrTotalIssues { get; private set; }
        public int NrOpenIssues { get; private set; }

    }
}
