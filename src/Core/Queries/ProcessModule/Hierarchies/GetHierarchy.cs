﻿using System;
using Messaging.Queries;

namespace Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchy : IQuery<GetHierarchyResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetHierarchyResult
    {
        public Guid Id { get; private set; }

        public Guid ParentId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public Guid HierarchyTypeId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public GetHierarchyResult(Guid id, Guid parentId, Guid subProjectId, Guid hierarchyTypeId, string title, string description)
        {
            Id = id;
            ParentId = parentId;
            SubProjectId = subProjectId;
            HierarchyTypeId = hierarchyTypeId;
            Title = title;
            Description = description;
        }
    }
}