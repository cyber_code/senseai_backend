﻿using Messaging.Queries;
using System;


namespace Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchyStatistics: IQuery<GetHierarchyStatisticsResult>
    {
        public Guid SubProjectId { get; set; }
        public Guid HierarchyId { get; set; }
    }
    public sealed class GetHierarchyStatisticsResult
    {
        public int NrProcesses { get;  set; }
        public int NrWorkflows { get;  set; }
        public int NrTestCases { get;  set; }
        public int NrTestSteps { get;  set; }
        public int NrTotalIssues { get;  set; }
        public int NrOpenIssues { get;  set; }


        public GetHierarchyStatisticsResult(int nrProcesses, int nrWorkflows, int nrTestCases, int nrTotalIssues, int nrOpenIssues, int nrTestSteps)
        {
            NrProcesses = nrProcesses;
            NrWorkflows = nrWorkflows;
            NrTestCases = nrTestCases;
            NrTotalIssues = nrTotalIssues;
            NrOpenIssues = nrOpenIssues;
            NrTestSteps = nrTestSteps;
        }


    }
    
}
