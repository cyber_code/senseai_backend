﻿using Messaging.Queries;
using Queries.ProcessModule.Processes;
using System;

namespace Queries.ProcessModule.Hierarchies
{
    public sealed class SearchHierarchies : IQuery<SearchHierarchiesResult>
    {
        public Guid SubProjectId { get; set; }

        public Guid ParentId { get; set; }

        public string SearchCriteria { get; set; }
    }

    public sealed class SearchHierarchiesResult
    {
        public SearchHierarchiesResult(GetHierarchyResult[] listOfHierarchies, GetProcessResult[] listOfProcesses)
        {
            ListOfHierarchies = listOfHierarchies;
            ListOfProcesses = listOfProcesses;
        }

        public GetHierarchyResult[] ListOfHierarchies { get; set; }

        public GetProcessResult[] ListOfProcesses { get; set; }
    }
}