﻿using System;
using Messaging.Queries;

namespace Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchyTypes : IQuery<GetHierarchyTypesResult[]>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetHierarchyTypesResult
    {
        public Guid Id { get; private set; }

        public Guid ParentId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public GetHierarchyTypesResult(Guid id, Guid parentId, Guid subProjectId, string title, string description)
        {
            Id = id;
            ParentId = parentId;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
        }
    }
}