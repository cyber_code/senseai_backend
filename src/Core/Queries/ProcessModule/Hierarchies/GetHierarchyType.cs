﻿using System;
using Messaging.Queries;

namespace Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchyType : IQuery<GetHierarchyTypeResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetHierarchyTypeResult
    {
        public Guid Id { get; private set; }

        public Guid ParentId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public GetHierarchyTypeResult(Guid id, Guid parentId, Guid subProjectId, string title, string description)
        {
            Id = id;
            ParentId = parentId;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
        }
    }
}