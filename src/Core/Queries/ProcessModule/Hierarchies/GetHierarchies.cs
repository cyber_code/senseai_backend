﻿using System;
using Messaging.Queries;

namespace Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchies : IQuery<GetHierarchiesResult[]>
    {
        public Guid SubProjectId { get; set; }

        public Guid HierarchyTypeId { get; set; }
    }

    public sealed class GetHierarchiesResult
    {
        public Guid Id { get; private set; }

        public Guid ParentId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public Guid HierarchyTypeId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public GetHierarchiesResult(Guid id, Guid parentId, Guid subProjectId, Guid hierarchyTypeId, string title, string description)
        {
            Id = id;
            ParentId = parentId;
            SubProjectId = subProjectId;
            HierarchyTypeId = hierarchyTypeId;
            Title = title;
            Description = description;
        }
    }
}