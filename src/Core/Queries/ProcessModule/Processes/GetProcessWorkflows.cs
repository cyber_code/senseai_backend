﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Processes
{
    public sealed class GetProcessWorkflows : IQuery<GetProcessWorkflowsResult[]>
    {
        public Guid ProcessId { get; set; }
    }

    public sealed class GetProcessWorkflowsResult
    {
        public GetProcessWorkflowsResult( Guid id, Guid workflowId, string workflowTitle, string workflowDescription)
        {
            Id = id;
            WorkflowId = workflowId;
            Title = workflowTitle;
            Description = workflowDescription;
        }

        public Guid Id { get; private set; }

        public Guid WorkflowId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }
    }
}
