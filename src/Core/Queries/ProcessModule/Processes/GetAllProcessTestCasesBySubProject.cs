﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Processes
{
    public sealed class GetAllProcessTestCasesBySubProject : IQuery<GetAllProcessTestCasesBySubProjectResult[]>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetAllProcessTestCasesBySubProjectResult
    {
        public GetAllProcessTestCasesBySubProjectResult(Guid id, string title, long testcaseId)
        {
            Id = id;
            TestCaseId = testcaseId;
            Title = title;
        }
        public Guid Id { get; private set; }
        public long TestCaseId { get; private set; }

        public string Title { get; private set; }

    }
}
