﻿using System;
using Messaging.Queries;
using SenseAI.Domain;

namespace Queries.ProcessModule.Processes
{
    public class GetProcesses : IQuery<GetProcessesResult[]>
    {
        public Guid SubProjectId { get; set; }

        public Guid HierarchyId { get; set; }
    }

    public sealed class GetProcessesResult
    {
        public Guid Id { get; private set; }

        public Guid SubProjectId { get; private set; }

        public Guid HierarchyId { get; private set; }

        public Guid CreatedId { get; private set; }

        public Guid OwnerId { get; private set; }

        public ProcessType ProcessType { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }
        public Guid RequirementPriorityId { get; private set; }
        public Guid RequirementTypeId { get; private set; }
        public int ExpectedNumberOfTestCases { get; private set; }
        public Guid TypicalId { get; private set; }

        public GetProcessesResult(Guid id,
                                  Guid subProjectId,
                                  Guid hierarchyId,
                                  Guid createdId,
                                  Guid ownerId,
                                  ProcessType processType,
                                  string title,
                                  string description,
                                  Guid requirementPriorityId,
                                  Guid requirementTypeId,
                                  int expectedNumberOfTestCases,
                                  Guid typicalId)
        {
            Id = id;
            SubProjectId = subProjectId;
            HierarchyId = hierarchyId;
            CreatedId = createdId;
            OwnerId = ownerId;
            ProcessType = processType;
            Title = title;
            Description = description;
            RequirementPriorityId = requirementPriorityId;
            RequirementTypeId = requirementTypeId;
            ExpectedNumberOfTestCases = expectedNumberOfTestCases;
            TypicalId = typicalId;
        }
    }
}