﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Processes
{
    public sealed class GetAllProcessWorkflowsBySubProject : IQuery<GetAllProcessWorkflowsBySubProjectResult[]>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetAllProcessWorkflowsBySubProjectResult
    {
        public GetAllProcessWorkflowsBySubProjectResult(Guid workflowId)
        {
            WorkflowId = workflowId;
           
        }

        public Guid WorkflowId { get; private set; }


    }
}
