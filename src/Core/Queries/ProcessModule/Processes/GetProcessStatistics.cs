﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Processes
{
    public sealed class GetProcessStatistics: IQuery<GetProcessStatisticsResult>
    {
        public Guid ProcessId { get; set; }
    }
    public sealed class GetProcessStatisticsResult
    {
        public GetProcessStatisticsResult(int nrWorkflows, int nrTotalIssues, int nrOpenIssues)
        {
            NrWorkflows = nrWorkflows;
            NrTotalIssues = nrTotalIssues;
            NrOpenIssues = nrOpenIssues;
        }

        public int NrWorkflows { get; private set; }
        public int NrTotalIssues { get; private set; }
        public int NrOpenIssues { get; private set; }


    }
}
