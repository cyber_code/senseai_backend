﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Processes
{
    public sealed class GetAllProcessOpenIssuesBySubProject : IQuery<GetAllProcessOpenIssuesBySubProjectResult[]>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetAllProcessOpenIssuesBySubProjectResult
    {
        public GetAllProcessOpenIssuesBySubProjectResult(Guid id, string title, string description)
        {
            Id = id;
            Title = title;
            Description = description;
        }

        public Guid Id { get; private set; }

        public string Title { get; private set; }
        public string Description { get; private set; }

    }
}
