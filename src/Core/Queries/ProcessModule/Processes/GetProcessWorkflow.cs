﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.Processes
{
   public sealed class GetProcessWorkflow : IQuery<GetProcessWorkflowResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetProcessWorkflowResult
    {
        public GetProcessWorkflowResult(Guid id, Guid processId, Guid workflowId)
        {
            Id = id;
            ProcessId = processId;
            WorkflowId = workflowId;
        }

        public Guid Id { get; private set; }

        public Guid ProcessId { get; private set; }

        public Guid WorkflowId { get; private set; }
    }
}
