﻿using System;
using Messaging.Queries;

namespace Queries.ProcessModule.RequirementTypes
{
    public sealed class GetRequirementTypes : IQuery<GetRequirementTypesResult[]>
    {
        public Guid SubProjectId { get; set; }
    }

    public sealed class GetRequirementTypesResult
    {
        public Guid Id { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Code { get; private set; }

        public GetRequirementTypesResult(Guid id, Guid subProjectId, string title, string code)
        {
            Id = id;
            SubProjectId = subProjectId;
            Title = title;
            Code = code;
        }
    }
}