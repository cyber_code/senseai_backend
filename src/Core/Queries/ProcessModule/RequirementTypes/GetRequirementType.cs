﻿using System;
using Messaging.Queries;

namespace Queries.ProcessModule.RequirementTypes
{
    public sealed class GetRequirementType : IQuery<GetRequirementTypeResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetRequirementTypeResult
    {
        public Guid Id { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Code { get; private set; }

        public GetRequirementTypeResult(Guid id, Guid subProjectId, string title, string code)
        {
            Id = id;
            SubProjectId = subProjectId;
            Title = title;
            Code = code;
        }
    }
}