﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.GenericWorkflows
{
    public class GetGenericWorkflowVersionsByDate : IQuery<GetGenericWorkflowVersionsByDateResult[]>
    {        
        public Guid GenericWorkflowId { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }
    }

    public sealed class GetGenericWorkflowVersionsByDateResult
    {
        public GetGenericWorkflowVersionsByDateResult(Guid genericWorkflowId,  int version, string title, string description, bool isLast, string workflowImage, DateTime? dateCreated)
        {
            GenericWorkflowId = genericWorkflowId;
            Version = version;
            Title = title;
            Description = description;
            IsLast = isLast;
            WorkflowImage = workflowImage;
            DateCreated = dateCreated;
        }

        public Guid GenericWorkflowId { get; private set; }

        public int Version { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public bool IsLast { get; private set; }

        public string WorkflowImage { get; private set; }

        public DateTime? DateCreated { get; private set; }
    }
}