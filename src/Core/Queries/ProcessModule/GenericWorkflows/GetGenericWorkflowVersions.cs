﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.GenericWorkflows
{
    public class GetGenericWorkflowVersions : IQuery<GetGenericWorkflowVersionsResult[]>
    {        
        public Guid GenericWorkflowId { get; set; }
    }

    public sealed class GetGenericWorkflowVersionsResult
    {
        public GetGenericWorkflowVersionsResult(Guid genericWorkflowId, int version, string title)
        {
            GenericWorkflowId = genericWorkflowId;
            Version = version;
            Title = title; 
        }

        public Guid GenericWorkflowId { get; private set; }

        public int Version { get; private set; }

        public string Title { get; private set; }
    }
}