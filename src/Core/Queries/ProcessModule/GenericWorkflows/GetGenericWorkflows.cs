﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.GenericWorkflows
{
    public sealed class GetGenericWorkflows : IQuery<GetGenericWorkflowResult>
    {
        public Guid ProcessId { get; set; }

    }

    public sealed class GetGenericWorkflowsResult
    {
        public GetGenericWorkflowsResult(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public Guid Id { get; private set; }

        public string Title  { get; private set; }
    }
}
