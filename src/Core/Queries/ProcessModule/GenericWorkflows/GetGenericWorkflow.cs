﻿using Messaging.Queries;
using System;

namespace Queries.ProcessModule.GenericWorkflows
{
    public sealed class GetGenericWorkflow : IQuery<GetGenericWorkflowResult>
    {
        public Guid Id { get; set; }
    }

    public sealed class GetGenericWorkflowResult
    {
        public GetGenericWorkflowResult(Guid id, Guid processId, string title, string description, string designerJson)
        {
            Id = id;
            ProcessId = processId;
            Title = title;
            Description = description;
            DesignerJson = designerJson; 
        }

        public Guid Id { get; private set; }

        public Guid ProcessId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string DesignerJson { get; private set; } 
    }
}
