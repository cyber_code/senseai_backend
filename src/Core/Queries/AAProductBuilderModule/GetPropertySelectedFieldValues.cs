﻿using Messaging.Queries;
using SenseAI.Domain.AAProductionBuilderModel;
using System;
using System.Collections.Generic;

namespace Queries.AAProductBuilderModule
{
    public sealed class GetPropertySelectedFieldValues : IQuery<GetPropertySelectedFieldValuesConfig>
    {
        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }
    }

    public sealed class GetPropertySelectedFieldValuesConfig
    {
        public List<GetPropertySelectedFieldValuesHeaderFieldAttribute> HeaderFieldValueConfigs { get; set; }

        public List<PropertyFieldValueConfig> PropertyFieldValueConfigs { get; set; }
    }

    public sealed class PropertyFieldValueConfig
    {
        public GetPropertySelectedFieldValuesProperty Property { get; set; }

        public List<GetPropertySelectedFieldValuesAttribute> FieldConfigs { get; set; }

        public List<ProductGroupPropertyFieldState> FieldValues { get; set; }
    }

    public sealed class GetPropertySelectedFieldValuesProperty
    {
        public string PropertyId { get; set; }

        public string PropertyName { get; set; }

        public string CurrencyId { get; set; }
    }

    public sealed class GetPropertySelectedFieldValuesAttribute
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Value { get; set; }

        public string IsRelativeDate { get; set; }

        public string[] RelativeFields1 { get; set; }

        public string[] RelativeFields2 { get; set; }

        public string[] PossibleValues { get; set; }

        public string RelatedApplication { get; set; }

        public string IsMultiValue { get; set; }

        public string IsRequired { get; set; }

        public string IsNoInput { get; set; }

        public string IsNoChange { get; set; }

        public string IsExternal { get; set; }

        public int MinimumLength { get; set; }

        public int MaximumLength { get; set; }

        public int? FirstGroupId { get; set; }

        public int? SecondGroupId { get; set; }

        public bool ExpandMultiValue { get; set; }

        public bool ExpandSubValue { get; set; }

        public bool ExpandSingleMultiValue { get; set; }

        public bool ExpandSingleSubValue { get; set; }

        public int MultiValueIndex { get; set; } = 1;

        public int SubValueIndex { get; set; } = 1;

        public string MvSvInd { get; set; }
    }

    public sealed class GetPropertySelectedFieldValuesHeaderFieldAttribute : ICloneable
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsMultiValue { get; set; }

        public string[] PossibleValues { get; set; }

        public bool IsRequired { get; set; }

        public bool IsNoInput { get; set; }

        public bool IsNoChange { get; set; }

        public bool IsExternal { get; set; }

        public int MinimumLength { get; set; }

        public int MaximumLength { get; set; }

        public string Value { get; set; }

        public int MultiValueIndex { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}