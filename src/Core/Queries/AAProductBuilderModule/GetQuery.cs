﻿using Messaging.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Queries.AAProductBuilderModule
{
    public class GetQuery : IQuery<GetQueryResult>
    {
        public string QueryId { get; set; }
    }

    public sealed class GetQueryResult
    {
        public GetQueryResult(string id, string title, List<string> parameters)
        {
            Id = id;
            Title = title;
            Parameters = parameters;
        }

        public string Id { get; set; }

        public string Title { get; set; }

        public List<string> Parameters { get; set; }
    }
}
