﻿using Messaging.Queries;
using System.Collections.Generic;

namespace Queries.AAProductBuilderModule
{
    public class RunQuery : IQuery<RunQueryResult[]>
    {
        public string QueryId { get; set; }

        public List<Parameter> Parameters { get; set; }
    }

    public class RunQueryResult
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }

    public sealed class Parameter
    {
        public string ParameterId { get; set; }

        public string Value { get; set; }
    }
}
