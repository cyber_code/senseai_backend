﻿using Messaging.Queries;
using System;
using System.Collections.Generic;

namespace Queries.AAProductBuilderModule
{
    public class GetNewArrangementFieldConfigs : IQuery<List<GetNewArrangementFieldConfigsAttribute>>
    {
    }

    public sealed class GetNewArrangementFieldConfigsAttribute : ICloneable
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsMultiValue { get; set; }

        public string[] PossibleValues { get; set; }

        public bool IsRequired { get; set; }

        public bool IsNoInput { get; set; }

        public bool IsNoChange { get; set; }

        public bool IsExternal { get; set; }

        public int MinimumLength { get; set; }

        public int MaximumLength { get; set; }

        public string Value { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}