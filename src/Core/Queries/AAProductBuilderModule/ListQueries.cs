﻿using Messaging.Queries;
using System.Collections.Generic;

namespace Queries.AAProductBuilderModule
{
    public class ListQueries : IQuery<ListQueriesResult[]>
    {
    }

    public class ListQueriesResult
    {
        public ListQueriesResult(string id, string title, List<string> parameters)
        {
            Id = id;
            Title = title;
            Parameters = parameters;
        }

        public string Id { get; set; }

        public string Title { get; set; }

        public List<string> Parameters { get; set; }
    }
}
