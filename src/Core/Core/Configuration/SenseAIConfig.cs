using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace Core.Configuration
{
    /// <summary>
    /// Represents startup SenseAI configuration parameters
    /// </summary>
    public class SenseAIConfig : BaseConfigurationProvider
    {
        public readonly bool IsTesting;

        private DataSettings[] _dataSettings;
        private string[] _issueFieldsToExclude;
        private string[] _jiraIssueFieldsToExclude;
        private string[] _requirementFieldsToExclude;
        private string[] _peopleFields;

        private TestEngineSettings _testEngineSettings;

        private DocumentGenerationSettings _documentGenerationSettings;

        public SenseAIConfig(IConfiguration configuration, bool isTesting)
                                            : base(configuration, "AppSettings")
        {
            IsTesting = isTesting;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the full error in production environment.
        /// It's ignored (always enabled) in development environment
        /// </summary>
        public bool DisplayFullErrorStack => GetConfiguration<bool>("DisplayFullErrorStack");

        //TODO move somewhere else
        /// <summary>
        /// Gets or sets a value indicating whether we compress response
        /// </summary>
        public bool UseResponseCompression => GetConfiguration<bool>("UseResponseCompression");

        /// <summary>
        ///
        /// </summary>
        public string ProcessResourcePath => GetConfiguration<string>("ProcessResourcePath");

        /// <summary>
        /// put default values
        /// </summary>
        public DataSettings[] DataSettings => _dataSettings ?? GetDataSettings();

        /// <summary>
        /// get default values
        /// </summary>
        public string[] IssueFieldsToExclude => _issueFieldsToExclude ?? GetIssueFieldsToExclude();

        public string[] JiraIssueFieldsToExclude => _jiraIssueFieldsToExclude ?? GetJiraIssueFieldsToExclude();

        public string[] RequirementFieldsToExclude => _requirementFieldsToExclude ?? GetRequirementFieldsToExclude();

        public string[] PeopleFields => _peopleFields ?? GetPeopleField();

        /// <summary>
        /// put default values
        /// </summary>
        public TestEngineSettings TestEngineSettings => _testEngineSettings ?? GetTestEngineSettings();

        /// <summary>
        /// put default values
        /// </summary>
        public DocumentGenerationSettings DocumentGenerationSettings => _documentGenerationSettings ?? GetDocumentGenerationSettings();

        /// <summary>
        /// Gets or sets a value indicating whether we should use Redis server for caching (instead of default in-memory caching)
        /// </summary>
        public bool RedisCachingEnabled => GetConfiguration<bool>("RedisCachingEnabled");

        /// <summary>
        /// Gets or sets Redis connection string. Used when Redis caching is enabled
        /// </summary>
        public string RedisCachingConnectionString => GetConfiguration<string>("RedisCachingConnectionString");

        /// <summary>
        /// disable inserting sample data
        /// </summary>
        public bool DisableSampleDataDuringInstallation => GetConfiguration<bool>("DisableSampleDataDuringInstallation");

        /// <summary>
        /// Gets or sets a value indicating whether to use fast installation.
        /// By default this setting should always be set to "False" (only for advanced users)
        /// </summary>
        public bool UseFastInstallationService => GetConfiguration<bool>("UseFastInstallationService");

        public bool InstallSampleData => GetConfiguration<bool>("InstallSampleData");

        public bool AutoUpdate => GetConfiguration<bool>("AutoUpdate");

        /// <summary>
        /// Enable request/response logging
        /// </summary>
        public bool EnableLogging => GetConfiguration<bool>("EnableLogging");

        public bool EnableCors => GetConfiguration<bool>("EnableCors");

        public bool LogAllQueries => GetConfiguration<bool>("LogAllQueries");

        public bool EnableIdentityServer => GetConfiguration<bool>("EnableIdentityServer");

        public bool EnableNeuralEngine => GetConfiguration<bool>("EnableNeuralEngine");

        public Guid IssueTypeForRequirementId => GetConfiguration<Guid>("IssueTypeForRequirementId");

        public string RequirementTitle => GetConfiguration<string>("RequirementTitle");

        public string RequirementLinkName => GetConfiguration<string>("RequirementLinkName");

        public string IssueLinkName => GetConfiguration<string>("IssueLinkName");

        public string SaiJiraEndpoint => GetConfiguration<string>("SaiJiraEndpoint");

        public bool EnableSwagger => GetConfiguration<bool>("EnableSwagger");

        public bool EnableNeuralEnginWorkflow => GetConfiguration<bool>("EnableNeuralEnginWorkflow");

        public bool TestWithRealDatabase => GetConfiguration<bool>("TestWithRealDatabase", false);

        private DataSettings[] GetDataSettings()
        {
            var dataSettings = new List<DataSettings>();
            _configuration.GetSection(GetQualifiedSelector("DataSettings")).Bind(dataSettings);
            _dataSettings = dataSettings.ToArray();

            return _dataSettings;
        }

        private string[] GetIssueFieldsToExclude()
        {
            var issueFieldsToExclude = new List<string>();
            _configuration.GetSection(GetQualifiedSelector("IssueFieldsToExclude")).Bind(issueFieldsToExclude);
            _issueFieldsToExclude = issueFieldsToExclude.ToArray();

            return _issueFieldsToExclude;
        }

        private string[] GetJiraIssueFieldsToExclude()
        {
            var jiraIssueFieldsToExclude = new List<string>();
            _configuration.GetSection(GetQualifiedSelector("JiraIssueFieldsToExclude")).Bind(jiraIssueFieldsToExclude);
            _jiraIssueFieldsToExclude = jiraIssueFieldsToExclude.ToArray();

            return _issueFieldsToExclude;
        }

        private string[] GetRequirementFieldsToExclude()
        {
            var requirementFieldsToExclude = new List<string>();
            _configuration.GetSection(GetQualifiedSelector("RequirementFieldsToExclude")).Bind(requirementFieldsToExclude);
            _requirementFieldsToExclude = requirementFieldsToExclude.ToArray();

            return _requirementFieldsToExclude;
        }

        private string[] GetPeopleField()
        {
            var peopleFields = new List<string>();
            _configuration.GetSection(GetQualifiedSelector("PeopleFields")).Bind(peopleFields);
            _peopleFields = peopleFields.ToArray();

            return _peopleFields;
        }

        private TestEngineSettings GetTestEngineSettings()
        {
            var testEngineSettings = new TestEngineSettings();
            _configuration.GetSection(GetQualifiedSelector("TestEngineSettings")).Bind(testEngineSettings);
            _testEngineSettings = testEngineSettings;

            return _testEngineSettings;
        }

        private DocumentGenerationSettings GetDocumentGenerationSettings()
        {
            var documentGenerationSettings = new DocumentGenerationSettings();
            _configuration.GetSection(GetQualifiedSelector("DocumentGenerationSettings")).Bind(documentGenerationSettings);
            _documentGenerationSettings = documentGenerationSettings;

            return _documentGenerationSettings;
        }
    }
}