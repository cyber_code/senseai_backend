using Microsoft.Extensions.Configuration;
using Core.Abstractions.Bindings;

namespace Core.Configuration.Bindings
{
    public class BindingHeadersConfiguration : BaseConfigurationProvider, IBindingHeadersConfiguration
    {
        public BindingHeadersConfiguration(IConfiguration configuration, string prefix)
            : base(configuration, prefix)
        {
        }

        public string ContentType => GetConfiguration<string>("Content-Type");
    }
}