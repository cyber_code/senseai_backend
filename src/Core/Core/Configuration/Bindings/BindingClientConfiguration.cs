﻿using Microsoft.Extensions.Configuration;
using Core.Abstractions.Bindings;

namespace Core.Configuration.Bindings
{
    public class BindingClientConfiguration : BaseConfigurationProvider, IBindingClientConfiguration
    {
        public BindingClientConfiguration(IConfiguration configuration, string prefix)
            : base(configuration, prefix)
        {
        }

        #region [ Properties ]

        /// <summary>
        /// Gets or sets the base addres of ML server
        /// </summary>
        public string Endpoint => GetConfiguration<string>("Endpoint");

        public int Timeout => GetConfiguration<int>("Timeout");

        #endregion [ Properties ]
    }
}