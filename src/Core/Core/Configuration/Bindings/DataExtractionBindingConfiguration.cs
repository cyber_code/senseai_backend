﻿using Microsoft.Extensions.Configuration;
using Core.Abstractions.Bindings;

namespace Core.Configuration.Bindings
{
    /// <summary>
    /// Represents startup Neural Engine service configuration parameters
    /// </summary>
    public class DataExtractionBindingConfiguration : BaseConfigurationProvider, IDataExtractionBindingConfiguration
    {
        private IBindingClientConfiguration _client;
        private IBindingHeadersConfiguration _header;

        public DataExtractionBindingConfiguration(IConfiguration configuration)
            : base(configuration, "Bindings:DataExtractionBinding")
        {
        }

        #region [ Properties ]

        //public string ExcecutionLogs => _configuration["Resources:ExcecutionLogs"];

        //public string SaveDefects => _configuration["Resources:SaveDefects"];

        //public string GetColumnMapping => _configuration["Resources:GetColumnMapping"];
        //public string AcceptedTypicalChanges => _configuration["Resources:AcceptedTypicalChanges"];

        //public string CheckService => _configuration["Resources:CheckService"];

        public IBindingClientConfiguration Client => _client = _client ?? new BindingClientConfiguration(_configuration, GetQualifiedSelector("Client"));

        public IBindingHeadersConfiguration Headers => _header = _header ?? new BindingHeadersConfiguration(_configuration, GetQualifiedSelector("Headers"));

        #endregion [ Properties ]
    }
}