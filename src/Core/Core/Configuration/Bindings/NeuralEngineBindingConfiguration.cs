﻿using Microsoft.Extensions.Configuration;
using Core.Abstractions.Bindings;

namespace Core.Configuration.Bindings
{
    /// <summary>
    /// Represents startup Neural Engine service configuration parameters
    /// </summary>
    public class NeuralEngineBindingConfiguration : BaseConfigurationProvider, INeuralEngineBindingConfiguration
    {
        public NeuralEngineBindingConfiguration(IConfiguration configuration)
            : base(configuration, "Bindings:NeuralEngineBinding")
        {
        }

        #region [ Properties ]

        private IBindingClientConfiguration _client;
        private IBindingHeadersConfiguration _header;

        //public string GetPaths => GetConfiguration<string>("Resources:GetPaths");

        //public string GetDataSets => GetConfiguration<string>("Resources:GetDataSets");

        //public string GetSuggestions => GetConfiguration<string>("Resources:GetSuggestions");

        //public string PostIssueWorkflow => GetConfiguration<string>("Resources:PostIssueWorkflow");
        //public string GetWorkflowCoverage => GetConfiguration<string>("Resources:GetWorkflowCoverage");

        public IBindingClientConfiguration Client => _client = _client ?? new BindingClientConfiguration(_configuration, GetQualifiedSelector("Client"));

        public IBindingHeadersConfiguration Headers => _header = _header ?? new BindingHeadersConfiguration(_configuration, GetQualifiedSelector("Headers"));

        #endregion [ Properties ]
    }
}