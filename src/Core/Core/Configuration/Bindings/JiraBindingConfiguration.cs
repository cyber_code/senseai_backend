﻿using Microsoft.Extensions.Configuration;
using Core.Abstractions.Bindings;

namespace Core.Configuration.Bindings
{
    /// <summary>
    /// Represents startup Neural Engine service configuration parameters
    /// </summary>
    public class JiraBindingConfiguration : BaseConfigurationProvider, IJiraBindingConfiguration
    {
        public JiraBindingConfiguration(IConfiguration configuration)
            : base(configuration, "Bindings:JiraBinding")
        {
        }

        #region [ Properties ]

        private IBindingClientConfiguration _client;
        private IBindingHeadersConfiguration _header;

        public IBindingClientConfiguration Client => _client = _client ?? new BindingClientConfiguration(_configuration, GetQualifiedSelector("Client"));

        public IBindingHeadersConfiguration Headers => _header = _header ?? new BindingHeadersConfiguration(_configuration, GetQualifiedSelector("Headers"));

        #endregion [ Properties ]
    }
}