﻿using Microsoft.Extensions.Configuration;
using Core.Abstractions.Bindings;

namespace Core.Configuration.Bindings
{
    public class QualitySuiteBindingConfiguration : BaseConfigurationProvider, IQualitySuiteBindingConfiguration
    {
        private IBindingClientConfiguration _client;
        private IBindingHeadersConfiguration _header;

        public QualitySuiteBindingConfiguration(IConfiguration configuration)
            : base(configuration, "Bindings:QualitySuiteBinding")
        {
        }

        #region [ Properties ]

        public string User => GetConfiguration<string>("Client:User");

        public string Password => GetConfiguration<string>("Client:Password");

        public string DisciplineNoderef => GetConfiguration<string>("Client:DisciplineNoderef");

        public string RoleNoderef => GetConfiguration<string>("Client:RoleNoderef");

        public string LicenseType => GetConfiguration<string>("Client:LicenseType");

        public string ProjectTitle => GetConfiguration<string>("Client:ProjectTitle");

        public string SubProjectTitle => GetConfiguration<string>("Client:SubProjectTitle");

        public IBindingClientConfiguration Client => _client = _client ?? new BindingClientConfiguration(_configuration, GetQualifiedSelector("Client"));

        public IBindingHeadersConfiguration Headers => _header = _header ?? new BindingHeadersConfiguration(_configuration, GetQualifiedSelector("Headers"));

        #endregion [ Properties ]
    }
}