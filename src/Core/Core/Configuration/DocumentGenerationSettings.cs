﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Configuration
{
    public class DocumentGenerationSettings
    {
        public string DocumentsPath { get; set; }

        public string AdapterSteps { get; set; }
    }
}
