﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Core.Configuration
{
    /// <summary>
    /// Represents the data settings
    /// </summary>
    public class DataSettings
    {
        #region Properties

        /// <summary>
        /// Gets or sets a data provider
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public DataProviderType DataProvider { get; set; }

        /// <summary>
        /// the specified timeout for transactions in seconds
        /// </summary>
        public int TransactionTimeout { get; set; }

        /// <summary>
        /// Gets or sets a connection string
        /// </summary>
        public string DataConnectionString
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a connection string Tenant Id
        /// </summary>
        public string TenantId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a log connection string
        /// </summary>
        public string MessagingLogConnectionString { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the information is entered
        /// </summary>
        /// <returns></returns>
        [JsonIgnore]
        public bool IsValid
        {
            get
            {
                if (
                    DataProvider != DataProviderType.Unknown &&
                    !string.IsNullOrEmpty(DataConnectionString) &&
                    !string.IsNullOrEmpty(MessagingLogConnectionString)
                )
                {
                    return true;
                }

                return false;
            }
        }

        #endregion Properties
    }
}