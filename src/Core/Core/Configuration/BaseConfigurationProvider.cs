﻿using Microsoft.Extensions.Configuration;

namespace Core.Configuration
{
    public abstract class BaseConfigurationProvider
    {
        protected readonly IConfiguration _configuration;
        private readonly string _prefix;

        public BaseConfigurationProvider(IConfiguration configuration, string prefix)
        {
            _prefix = $"{prefix}{(prefix.EndsWith(':') ? string.Empty : ":")}";
            _configuration = configuration;
        }

        protected T GetConfiguration<T>(string selector)
        {
            return _configuration.GetValue<T>(GetQualifiedSelector(selector));
        }

        protected T GetConfiguration<T>(string selector, T defaultValue)
        {
            return _configuration.GetValue<T>(GetQualifiedSelector(selector), defaultValue);
        }

        protected virtual string GetQualifiedSelector(string path) =>  $"{_prefix}{path}";
    }
}
