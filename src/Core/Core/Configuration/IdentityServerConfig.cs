﻿using Microsoft.Extensions.Configuration;
using Core.Abstractions;

namespace Core.Configuration
{
    /// <summary>
    /// Represents startup IdentityServer configuration parameters
    /// </summary>
    public class IdentityServerConfig : BaseConfigurationProvider, IIdentityServerConfig
    {
        public IdentityServerConfig(IConfiguration configuration)
            : base(configuration, "IdentityServer")
        {
        }

        #region [ Properties ]

        /// <summary>
        /// Gets or sets the addres of identity server
        /// </summary>
        public string Endpoint => GetConfiguration<string>("Endpoint");

        /// <summary>
        /// Gets or sets the user info addres of identity
        /// </summary>
        public string UserInfoEndpoing => GetConfiguration<string>("UserInfoEndpoing");

        /// <summary>
        /// Gets or sets the identity addres
        /// </summary>
        public string IdentityEndpoint => GetConfiguration<string>("IdentityEndpoint");

        /// <summary>
        /// Gets or sets the identity authentication scheme
        /// </summary>
        public string AuthenticationScheme => GetConfiguration<string>("AuthenticationScheme");

        /// <summary>
        /// Gets or sets the client id of SenseAI web api on identity server
        /// </summary>
        public string ClientId => GetConfiguration<string>("ClientId");

        /// <summary>
        /// Gets or sets the client name of SenseAI web api on identity server
        /// </summary>
        public string ClientName => GetConfiguration<string>("ClientName");

        /// <summary>
        /// Gets or sets the client password of SenseAI web api on identity server
        /// </summary>
        public string ClientPassword => GetConfiguration<string>("ClientPassword");

        /// <summary>
        /// Gets or sets the client name of SenseAI web api on identity server
        /// </summary>
        public string Scope => GetConfiguration<string>("Scope");

        /// <summary>
        /// Gets or sets the SenseAI web api client seccret on identity server
        /// </summary>
        public string ClientSecret => GetConfiguration<string>("ClientSecret");

        /// <summary>
        /// Gets or sets the SenseAI web api client seccret on identity server
        /// </summary>
        public string SignInAction => GetConfiguration<string>("SignInAction");

        #endregion [ Properties ]
    }
}