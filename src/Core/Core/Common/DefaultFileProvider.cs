﻿using Core.Abstractions;

namespace Core.Common
{
    public static class DefaultFileProvider
    {
        /// <summary>
        /// the default instance that implements ISenseAIFileProvider
        /// </summary>
        public static ISenseAIFileProvider Instance { get; set; }
    }
}