﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Core.Abstractions;
using Core.Configuration;
using Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    /// <inheritdoc />
    /// <summary>
    /// Represents SenseAI engine
    /// </summary>
    public class SenseAIEngine : IEngine
    {
        #region Properties

        /// <summary>
        /// Gets or sets service provider
        /// </summary>
        private IServiceProvider _serviceProvider;

        /// <summary>
        /// Gets or sets identity server configuration
        /// </summary>
        private IIdentityServerConfig _identityServerConfig;

        /// <summary>
        /// Gets or sets the default file provider
        /// </summary>
        public ISenseAIFileProvider FileProvider { get; private set; }

        public SenseAIConfig Configuration { get; set; }

        public IIdentityServerConfig IdentityServerConfig { get; set; }

        #endregion Properties

        #region Utilities

        /// <summary>
        /// Get IServiceProvider
        /// </summary>
        /// <returns>IServiceProvider</returns>
        protected virtual IServiceProvider GetServiceProvider()
        {
            return ServiceProvider;
        }

        /// <summary>
        /// Register dependencies using Autofac
        /// </summary>
        /// <param name="saiConfig">Startup SenseAI configuration parameters</param>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="typeFinder">Type finder</param>
        protected virtual IServiceProvider RegisterDependencies(SenseAIConfig saiConfig, IServiceCollection services, ITypeFinder typeFinder)
        {
            var containerBuilder = new ContainerBuilder();

            //register type finder
            containerBuilder.RegisterInstance(typeFinder).As<ITypeFinder>().SingleInstance();
            containerBuilder.RegisterInstance(FileProvider).As<ISenseAIFileProvider>().SingleInstance();
            //find dependency registrars provided by other assemblies
            var dependencyRegistrars = typeFinder.FindClassesOfType<IDependencyRegistrar>();

            //create and sort instances of dependency registrars
            var instances = dependencyRegistrars
                .Select(dependencyRegistrar => (IDependencyRegistrar)Activator.CreateInstance(dependencyRegistrar))
                .OrderBy(dependencyRegistrar => dependencyRegistrar.Order);

            //register all provided dependencies
            foreach (var dependencyRegistrar in instances)
                dependencyRegistrar.Register(containerBuilder, typeFinder, saiConfig);

            //populate Autofac container builder with the set of registered service descriptors
            containerBuilder.Populate(services);

            //create service provider
            _serviceProvider = new AutofacServiceProvider(containerBuilder.Build());
            return _serviceProvider;
        }

        /// <summary>
        /// Register and configure AutoMapper
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="typeFinder">Type finder</param>
        protected virtual void RegisterMappingProfiles(IServiceCollection services, ITypeFinder typeFinder)
        {
            //find mapper configurations provided by other assemblies
            var mapperConfigurations = typeFinder.FindClassesOfType<IMapperProfile>();

            //create and sort instances of mapper configurations
            var instances = mapperConfigurations
                .Select(mapperConfiguration => (IMapperProfile)Activator.CreateInstance(mapperConfiguration))
                .OrderBy(mapperConfiguration => mapperConfiguration.Order);

            //create AutoMapper configuration
            var config = new MapperConfiguration(cfg =>
            {
                foreach (var instance in instances)
                {
                    cfg.AddProfile(instance.GetType());
                }
            });
            AutoMapperConfiguration.Init(config);
        }

        #endregion Utilities

        #region Methods

        /// <inheritdoc />
        /// <summary>
        /// Initialize engine
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="saiFileProvider"></param>
        /// <param name="saiConfig"></param>
        public void Initialize(IServiceCollection services, IIdentityServerConfig identityServerConfig, ISenseAIFileProvider saiFileProvider, SenseAIConfig saiConfig)
        {
            _identityServerConfig = identityServerConfig;
            Configuration = saiConfig;
            FileProvider = saiFileProvider;
        }

        /// <inheritdoc />
        /// <summary>
        /// Add and configure services
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration">Configuration root of the application</param>
        /// <returns>Service provider</returns>
        public IServiceProvider ConfigureServices(IServiceCollection services, SenseAIConfig configuration)
        {
            //find startup configurations provided by other assemblies
            var typeFinder = new SenseAITypeFinder(FileProvider);
            //add all services defined in Startup classes
            ConfigureStartupsServices(services, configuration, typeFinder);

            //register mapper configurations
            RegisterMappingProfiles(services, typeFinder);
            //register dependencies
            _serviceProvider = RegisterDependencies(configuration, services, typeFinder);

            return _serviceProvider;
        }

        /// <summary>
        /// Resolve dependency
        /// </summary>
        /// <typeparam name="T">Type of resolved service</typeparam>
        /// <returns>Resolved service</returns>
        public T Resolve<T>() where T : class
        {
            return (T)GetServiceProvider().GetRequiredService(typeof(T));
        }

        /// <summary>
        /// Resolve dependency
        /// </summary>
        /// <param name="type">Type of resolved service</param>
        /// <returns>Resolved service</returns>
        public object Resolve(Type type)
        {
            return GetServiceProvider().GetRequiredService(type);
        }

        /// <summary>
        /// Resolve dependencies
        /// </summary>
        /// <typeparam name="T">Type of resolved services</typeparam>
        /// <returns>Collection of resolved services</returns>
        public IEnumerable<T> ResolveAll<T>()
        {
            return (IEnumerable<T>)GetServiceProvider().GetServices(typeof(T));
        }

        /// <summary>
        /// Resolve unregistered service
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public virtual object ResolveUnregistered(Type type)
        {
            Exception innerException = null;
            foreach (var constructor in type.GetConstructors())
            {
                try
                {
                    //try to resolve constructor parameters
                    var parameters = constructor.GetParameters().Select(parameter =>
                    {
                        var service = Resolve(parameter.ParameterType);
                        if (service == null)
                            throw new SenseAIException("Unknown dependency");
                        return service;
                    });

                    //all is ok, so create instance
                    return Activator.CreateInstance(type, parameters.ToArray());
                }
                catch (Exception ex)
                {
                    innerException = ex;
                }
            }

            throw new SenseAIException("No constructor was found that had all the dependencies satisfied.", innerException);
        }

        /// <summary>
        /// create an instance per each Startup class that implements ISenseAIStartup and then call ConfigureServices
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <param name="typeFinder"></param>
        protected virtual void ConfigureStartupsServices(IServiceCollection services, SenseAIConfig configuration, ITypeFinder typeFinder)
        {
            var startupConfigurations = typeFinder.FindClassesOfType<ISenseAIStartup>();
            //create and sort instances of startup configurations
            var instances = startupConfigurations
                .Select(startup => (ISenseAIStartup)Activator.CreateInstance(startup))
                .OrderBy(startup => startup.Order);

            //configure services
            foreach (var instance in instances)
            {
                instance.ConfigureServices(services, configuration);
                instance.ConfigureServices(services, _identityServerConfig);
            }
        }

        #endregion Methods

        #region Properties

        /// <summary>
        /// Service provider
        /// </summary>
        public virtual IServiceProvider ServiceProvider => _serviceProvider;

        #endregion Properties
    }
}