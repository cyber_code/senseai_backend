﻿using System;
using System.Runtime.Serialization;

namespace Core.Exceptions
{
    [Serializable]
    public class SenseAIConflictException : SenseAIException
    {
        public SenseAIConflictException()
        {
        }

        public SenseAIConflictException(string message) : base(message)
        {
        }

        public SenseAIConflictException(string messageFormat, params object[] args) : base(messageFormat, args)
        {
        }

        public SenseAIConflictException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SenseAIConflictException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}