﻿using Core.Abstractions;

namespace Core
{
    public class AutoMapperTypeAdapterFactory : ITypeAdapterFactory
    {
        public ITypeAdapter Create()
        {
            return new AutoMapperTypeAdapter();
        }
    }
}