﻿namespace Core.Abstractions
{
    public interface ISqlExecutor
    {
        void ExecuteSqlFile(string path);
    }
}