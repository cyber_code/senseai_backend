﻿namespace Core.Abstractions
{
    public interface IIdentityServerConfig
    {
        string Endpoint { get; }

        string UserInfoEndpoing { get; }

        string IdentityEndpoint { get; }

        string AuthenticationScheme { get; }

        string ClientId { get; }

        string ClientName { get; }

        string ClientPassword { get; }

        string Scope { get; }

        string ClientSecret { get; }

        string SignInAction { get; }
    }
}