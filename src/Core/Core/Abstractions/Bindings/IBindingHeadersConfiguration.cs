﻿namespace Core.Abstractions.Bindings
{
    public interface IBindingHeadersConfiguration
    {
        string ContentType { get; }
    }
}