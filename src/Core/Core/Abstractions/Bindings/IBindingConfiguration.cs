﻿namespace Core.Abstractions.Bindings
{
    public interface IBindingConfiguration
    {
        IBindingClientConfiguration Client { get; }

        IBindingHeadersConfiguration Headers { get; }
    }
}