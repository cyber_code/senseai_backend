﻿namespace Core.Abstractions.Bindings
{
    public interface IBindingClientConfiguration
    {
        string Endpoint { get; }

        int Timeout { get; }
    }
}