﻿namespace Core.Abstractions.Bindings
{
    public interface IQualitySuiteBindingConfiguration : IBindingConfiguration
    {
        string User { get; }

        string Password { get; }

        string DisciplineNoderef { get; }

        string RoleNoderef { get; }

        string LicenseType { get; }

        string ProjectTitle { get; }

        string SubProjectTitle { get; }
    }
}