namespace Core.Abstractions.Caching
{
    public static class CachingDefaults
    {
        /// <summary>
        /// Gets the default cache time in minutes
        /// </summary>
        public static int CacheTime => 60;
    }
}
