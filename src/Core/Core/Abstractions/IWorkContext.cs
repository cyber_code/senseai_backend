﻿namespace Core.Abstractions
{
    public interface IWorkContext
    {
        string TenantId { get; }

        string UserId { get; }

        string FullName { get; }

        string Email { get; }

        string Phone { get; }

        string CalledFrom { get; }

        string IpAddress { get; }

        string ConnectionString { get; }

        string MessagingLogConnectionString { get; }

        string BasePath { get; }

        string JiraUser { get; }

        string JiraToken { get; }

        string AccessToken { get; }
    }
}