﻿using Microsoft.Extensions.DependencyInjection;
using Core.Configuration;
using System;
using System.Collections.Generic;

namespace Core.Abstractions
{
    /// <summary>
    /// Classes implementing this interface can serve as a portal for the various services composing the SenseAI engine.
    /// Edit functionality, modules and implementations access most SenseAI functionality through this interface.
    /// </summary>
    public interface IEngine
    {
        ISenseAIFileProvider FileProvider { get; }

        /// <summary>
        /// Get application configuration
        /// </summary>
        SenseAIConfig Configuration { get; }

        /// <summary>
        /// Get IS configuration
        /// </summary>
        IIdentityServerConfig IdentityServerConfig { get; }

        /// <summary>
        /// Initialize engine
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="saiFileProvider">File provider</param>
        /// <param name="saiConfig"></param>
        void Initialize(IServiceCollection services, IIdentityServerConfig identityServerConfig, ISenseAIFileProvider saiFileProvider, SenseAIConfig saiConfig);

        /// <summary>
        /// Add and configure services
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration">Configuration root of the application</param>
        /// <returns>Service provider</returns>
        IServiceProvider ConfigureServices(IServiceCollection services, SenseAIConfig configuration);

        /// <summary>
        /// Resolve dependency
        /// </summary>
        /// <typeparam name="T">Type of resolved service</typeparam>
        /// <returns>Resolved service</returns>
        T Resolve<T>() where T : class;

        /// <summary>
        /// Resolve dependency
        /// </summary>
        /// <param name="type">Type of resolved service</param>
        /// <returns>Resolved service</returns>
        object Resolve(Type type);

        /// <summary>
        /// Resolve dependencies
        /// </summary>
        /// <typeparam name="T">Type of resolved services</typeparam>
        /// <returns>Collection of resolved services</returns>
        IEnumerable<T> ResolveAll<T>();

        object ResolveUnregistered(Type type);
    }
}