﻿using Microsoft.Extensions.DependencyInjection;
using Core.Configuration;

namespace Core.Abstractions
{
    /// <summary>
    /// Represents object for the configuring services and middleware on application startup
    /// </summary>
    public interface ISenseAIStartup
    {
        /// <summary>
        /// Gets order of this startup configuration implementation
        /// </summary>
        int Order { get; }

        /// <summary>
        /// Add and configure any of the middleware
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration">Configuration root of the application</param>
        void ConfigureServices(IServiceCollection services, SenseAIConfig configuration);

        void ConfigureServices(IServiceCollection services, IIdentityServerConfig configuration);
    }
}