﻿using Messaging.Commands;
using SenseAI.Domain.AAProductionBuilderModel;

namespace Events.AAProductBuilder
{
    public sealed class QuestionAnsweredEvent : ICommand<bool>
    {
        public ProductGroupPropertyFieldState ProductGroupPropertyFieldState { get; set; }
    }
}
