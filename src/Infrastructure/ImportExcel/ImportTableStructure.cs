﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImportExcel
{
    public class ImportTableStructure
    {
        public ImportTableStructure(string field1, string field2, string field3)
        {
            Field1 = field1;
            Field2 = field2;
            Field3 = field3;
        }

        public string Field1 { get; set; }

        public string Field2 { get; set; }

        public string Field3 { get; set; }
    }
}