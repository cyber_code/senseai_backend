﻿using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using SenseAI.Domain.Process.HierarchiesModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace ImportExcel
{
    public sealed class ImportHierarchiesFromExcel : IImportHierarchy
    {
        private readonly IHierarchyRepository _hierarchyRepository;
        private readonly IHierarchyTypeRepository _hierarchyTypeRepository;

        public ImportHierarchiesFromExcel(IHierarchyRepository hierarchyRepository, IHierarchyTypeRepository hierarchyTypeRepository)
        {
            _hierarchyRepository = hierarchyRepository;
            _hierarchyTypeRepository = hierarchyTypeRepository;
        }

        public HierarchyImportModel ReadXLSXFile(Stream streamContent, bool readAllCellContent = true)
        {
            HierarchyImportModelTable hierarchyTable;
            HierarchyTypeImportModelTable hierarchyTypeTable;
            List<HierarchyImportModelTable> listHierarchyTable = new List<HierarchyImportModelTable>();
            List<HierarchyTypeImportModelTable> listHierarchyTypeTable = new List<HierarchyTypeImportModelTable>();
            //FileInfo existingFile = new FileInfo(FilePath);
            HierarchyImportModel hierarchyImportModel = new HierarchyImportModel();
            List<Hierarchy> lsthierarchy = new List<Hierarchy>();
            bool headerAdded = false;
            using (ExcelPackage package = new ExcelPackage(streamContent))
            {
                //get the first worksheet in the workbook
                foreach (var worksheet in package.Workbook.Worksheets)
                {
                    //var worksheet = package.Workbook.Worksheets[7];
                    int colCount = worksheet.Dimension.End.Column;  //get Column Count
                    int colCountHeader = 5;// worksheet.Dimension.End.Column;  //get Column Count
                    int rowCount = worksheet.Dimension.End.Row;     //get row count

                    if (string.IsNullOrWhiteSpace(worksheet.Cells[8, 2].Value?.ToString()))
                        continue;

                    //get header
                    if (!headerAdded)
                    {
                        for (int col = 2; col <= colCountHeader + 1; col++)
                        {
                            string cellHeaderValue = worksheet.Cells[7, col].Value?.ToString().Trim();
                            switch (col)
                            {
                                case 2:
                                    if (!string.IsNullOrWhiteSpace(cellHeaderValue))
                                    {
                                        hierarchyTypeTable = new HierarchyTypeImportModelTable(
                                            cellHeaderValue, "", "", "", "", 1);
                                        listHierarchyTypeTable.Add(hierarchyTypeTable);
                                    }
                                    break;

                                case 3:
                                    if (!string.IsNullOrWhiteSpace(cellHeaderValue))
                                    {
                                        hierarchyTypeTable = new HierarchyTypeImportModelTable(
                                            "", cellHeaderValue, "", "", "", 2);
                                        listHierarchyTypeTable.Add(hierarchyTypeTable);
                                    }
                                    break;

                                case 4:
                                    hierarchyTypeTable = new HierarchyTypeImportModelTable(
                                          "", "", "Use case", "", "", 3);
                                    listHierarchyTypeTable.Add(hierarchyTypeTable);
                                    break;

                                    //case 5:
                                    //    hierarchyTypeTable = new HierarchyTypeImportModelTable(
                                    //            "", "", "", "Sub Use case", "", 4);
                                    //    listHierarchyTypeTable.Add(hierarchyTypeTable);
                                    //    break;

                                    //case 6:
                                    //    hierarchyTypeTable = new HierarchyTypeImportModelTable(
                                    //            "", "", "", "", "Group use case", 5);
                                    //    listHierarchyTypeTable.Add(hierarchyTypeTable);
                                    //    break;
                            }
                        }
                        headerAdded = true;
                    }

                    //get items hierarchies
                    string hierarchyComplex3 = "";
                    string hierarchyComplex4 = "";
                    string hierarchyComplex5 = "";
                    for (int row = 8; row <= rowCount; row++)
                    {
                        if (worksheet.Cells[row, 5].Value?.ToString().Trim() == null)
                        {
                            for (int col = 2; col <= colCountHeader - 1; col++)
                            {
                                string cellValue = worksheet.Cells[row, col].Value?.ToString().Trim();
                                switch (col)
                                {
                                    case 2:
                                        if (!string.IsNullOrWhiteSpace(cellValue))
                                        {
                                            hierarchyTable =
                                                new HierarchyImportModelTable(cellValue, "", "", "", "", "", "", 1);
                                            listHierarchyTable.Add(hierarchyTable);
                                        }
                                        break;

                                    case 3:
                                        if (!string.IsNullOrWhiteSpace(cellValue))
                                        {
                                            var count = cellValue.Count(c => c == '.');
                                            if (count > 1)
                                            {
                                                hierarchyComplex3 = readAllCellContent ? cellValue.Substring(cellValue.IndexOf(' ') + 1) : cellValue;
                                                hierarchyTable =
                                                new HierarchyImportModelTable("", "", hierarchyComplex3, "", "", "", "", 3);
                                                listHierarchyTable.Add(hierarchyTable);
                                            }
                                            else
                                            {
                                                hierarchyTable =
                                                new HierarchyImportModelTable("", readAllCellContent ? cellValue.Substring(cellValue.IndexOf(' ') + 1) : cellValue, "", "", "", "", "", 2);
                                                listHierarchyTable.Add(hierarchyTable);
                                            }
                                        }
                                        break;

                                    case 4:
                                        if (!string.IsNullOrWhiteSpace(cellValue))
                                        {
                                            var count = cellValue.Count(c => c == '.');
                                            if (count == 3)
                                            {
                                                hierarchyComplex4 = $"{hierarchyComplex3} - {(readAllCellContent ? cellValue.Substring(cellValue.IndexOf(' ') + 1) : cellValue)}";
                                                var update = listHierarchyTable.FirstOrDefault(a => a.Field3 == hierarchyComplex3);
                                                if (update != null)
                                                {
                                                    update.Field3 = hierarchyComplex4;
                                                }
                                                else
                                                {
                                                    hierarchyTable =
                                                        new HierarchyImportModelTable("", "", hierarchyComplex4, "", "", "", "", 3);
                                                    listHierarchyTable.Add(hierarchyTable);
                                                }
                                            }
                                            else if (count == 2)
                                            {
                                                hierarchyComplex3 = readAllCellContent ? cellValue.Substring(cellValue.IndexOf(' ') + 1) : cellValue;
                                                hierarchyTable =
                                                    new HierarchyImportModelTable("", "", hierarchyComplex3, "", "", "", "", 3);
                                                listHierarchyTable.Add(hierarchyTable);
                                            }
                                            else
                                            {
                                                hierarchyComplex5 = $"{hierarchyComplex4} - {(readAllCellContent ? cellValue.Substring(cellValue.IndexOf(' ') + 1) : cellValue)}";
                                                var update = listHierarchyTable.FirstOrDefault(a => a.Field3 == hierarchyComplex4);
                                                if (update != null)
                                                {
                                                    update.Field3 = hierarchyComplex5;
                                                }
                                                else
                                                {
                                                    hierarchyTable =
                                                    new HierarchyImportModelTable("", "", hierarchyComplex5, "", "", "", "", 3);
                                                    listHierarchyTable.Add(hierarchyTable);
                                                }
                                            }
                                        }
                                        break;

                                    default:
                                        break;
                                }
                            }
                        }
                        else
                        {
                            string cellProcessValue = worksheet.Cells[row, colCountHeader - 1].Value?.ToString().Trim();
                            string cellProcessDescription = worksheet.Cells[row, colCountHeader].Value?.ToString().Trim();
                            hierarchyTable =
                                            new HierarchyImportModelTable("", "", "", "", "", cellProcessValue, cellProcessDescription, 6);
                            listHierarchyTable.Add(hierarchyTable);
                        }
                    }
                }
                hierarchyImportModel.HierarchyImportModelTabel = listHierarchyTable;
                hierarchyImportModel.HierarchyTypeImportModelTable = listHierarchyTypeTable;
            }
            return hierarchyImportModel;
        }

        public HierarchyImportModel ImportHierarchyFromExcelFile(Guid subProjectId, IFormFile File)
        {
            var fileContent = new StreamContent(File.OpenReadStream())
            {
                Headers =
                {
                    ContentLength = File.Length,
                    ContentType = new MediaTypeHeaderValue(File.ContentType)
                }
            };
            Stream stream = new MemoryStream();
            fileContent.CopyToAsync(stream);
            return ReadXLSXFile(stream, false);
        }
    }
}