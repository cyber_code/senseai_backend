﻿using IdentityModel.Client;
using Core.Abstractions;
using WebApi.PolicyManagement.Contracts;
using WebApi.PolicyManagement.Entities.Model;
using System.Collections.Concurrent;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.PolicyManagement.Services
{
    /// <summary>
    /// PolicyServer client
    /// </summary>
    public class PolicyServerRuntimeClient : IPolicyServerRuntimeClient
    {
        private readonly IConnectionFactory _connectionFactory;
        private static readonly ConcurrentDictionary<string, UserInfoResponse> userData = new ConcurrentDictionary<string, UserInfoResponse>();

        /// <summary>
        /// Initializes a new instance of the <see cref="PolicyServerRuntimeClient"/> class.
        /// </summary>
        /// <param name="policyService">policy service</param>
        public PolicyServerRuntimeClient(IConnectionFactory connectionFactory, IIdentityServerConfig identityServerConfig)
        {
            _connectionFactory = connectionFactory;
            IdentityServerConfig = identityServerConfig;
        }

        public IIdentityServerConfig IdentityServerConfig { get; private set; }

        [System.Obsolete]
        public async Task<TokenResponse> SignIn(string userName, string password)
        {
            var client = _connectionFactory.CreateTokenClient();
            return await client.RequestResourceOwnerPasswordAsync(userName, password, IdentityServerConfig.Scope);
        }

        [System.Obsolete]
        public async Task<bool> HasPermissionAsync(ClaimsPrincipal user, string permission)
        {
            return await GetProfileDataAsync("") != null;
        }

        [System.Obsolete]
        public async Task<PolicyResult> GetProfileDataAsync(string accessToken)
        {
            var infoclient = _connectionFactory.CreateUserInfoClient();
            if (accessToken.StartsWith("Bearer "))
            {
                accessToken = accessToken.Replace("Bearer ", string.Empty);
            }
            if (!userData.TryGetValue(accessToken, out UserInfoResponse inforesponse))
            {
                inforesponse = await infoclient.GetAsync(accessToken);
                userData[accessToken] = inforesponse;
            }
            return new PolicyResult
            {
                Roles = inforesponse.Claims.Where(claim => claim.Type.Equals("role")).Select(claim => claim.Value),
                Permissions = inforesponse.Claims.Where(claim => claim.Type.Equals("permision")).Select(claim => claim.Value),
                License = inforesponse.Claims.Where(claim => claim.Type.Equals("license_id")).FirstOrDefault().Value

            };
        }

        public async Task<string> GetIdentity(string accessToken)
        {
            return await _connectionFactory.GetIdentityBearer(accessToken);
        }
    }
}