﻿using IdentityModel.Client;
using Core.Abstractions;
using WebApi.PolicyManagement.Contracts;
using WebApi.PolicyManagement.Entities.Model;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.PolicyManagement.Services
{
    public class IdentityServerService : IIdentityServerContract
    {
        private readonly IConnectionFactory _connectionFactory;
        private readonly IIdentityServerConfig _identityServerConfig;

        public IdentityServerService(IConnectionFactory connectionFactory, IIdentityServerConfig identityServerConfig)
        {
            _connectionFactory = connectionFactory;
            _identityServerConfig = identityServerConfig;
        }

        [System.Obsolete]
        public async Task<TokenResponse> SignIn(string userName, string password)
        {
            var client = _connectionFactory.CreateTokenClient();
            return await client.RequestResourceOwnerPasswordAsync(userName, password, _identityServerConfig.Scope);
        }

        [System.Obsolete]
        public async Task<PolicyResult> GetProfileDataAsync(string accessToken)
        {
            var infoclient = _connectionFactory.CreateUserInfoClient();
            UserInfoResponse inforesponse = await infoclient.GetAsync(accessToken);

            return new PolicyResult()
            {
                Roles = inforesponse.Claims.Where(claim => claim.Type.Equals("role")).Select(claim => claim.Value),
                Permissions = inforesponse.Claims.Where(claim => claim.Type.Equals("permision")).Select(claim => claim.Value),
                License = inforesponse.Claims.Where(claim => claim.Type.Equals("license_id")).FirstOrDefault().Value

            };

            //return inforesponse.Claims.Select(claim => claim.Value);
        }

        [System.Obsolete]
        public async Task<string> GetIdentity(string accessToken)
        {
            return await _connectionFactory.GetIdentityBearer(accessToken);
        }
    }
}