﻿using WebApi.PolicyManagement.Contracts;
using WebApi.PolicyManagement.Entities.Model;
using System.Threading.Tasks;

namespace WebApi.PolicyManagement.Services
{
    ///<inheritdoc cref="IPolicyService"/>
    public class PolicyService : IPolicyService
    {
        private readonly IIdentityServerContract _identityServer;

        /// <summary>
        /// default constructor
        /// </summary>
        public PolicyService(IIdentityServerContract identityServer)
        {
            _identityServer = identityServer;
        }

        public async Task<PolicyResult> GetPolicyByAccessTokenAsync(string accessToken, string role)
        {
            return await _identityServer.GetProfileDataAsync(accessToken);
            //return new PolicyResult
            //{
            //    Roles = new[] { role },
            //    Permissions = permissions
            //};
        }
    }
}