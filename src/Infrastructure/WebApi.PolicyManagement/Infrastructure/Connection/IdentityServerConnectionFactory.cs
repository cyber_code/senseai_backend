﻿using IdentityModel.Client;
using Core.Abstractions;
using WebApi.PolicyManagement.Contracts;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WebApi.PolicyManagement.Infrastructure.Connection
{
    public class IdentityServerConnectionFactory : IConnectionFactory
    {
        private readonly IIdentityServerConfig _identityServerConfig;

        public IdentityServerConnectionFactory(IIdentityServerConfig identityServerConfig)
        {
            _identityServerConfig = identityServerConfig;
        }

        /// <summary>
        /// Factory method for creation token client connection to  Identity Server
        /// </summary>
        /// <returns><see cref="TokenClient"/> object</returns>
        [System.Obsolete]
        public TokenClient CreateTokenClient()
        {
            var dd = new DiscoveryClient(_identityServerConfig.Endpoint);
            dd.Policy.RequireHttps = false;
            var d = dd.GetAsync().Result;
            var disco = DiscoveryClient.GetAsync(_identityServerConfig.Endpoint).Result;
            return new TokenClient(d.TokenEndpoint, _identityServerConfig.ClientId, _identityServerConfig.ClientPassword.ToString());
            //return new TokenClient(disco.TokenEndpoint, _identityServerConfig.ClientId, _identityServerConfig.ClientPassword.ToString());
        }

        /// <summary>
        /// Factory method for creation user info client connection to  Identity Server
        /// </summary>
        /// <returns><see cref="UserInfoClient"/> object</returns>
        [System.Obsolete]
        public UserInfoClient CreateUserInfoClient()
        {
            return new UserInfoClient(_identityServerConfig.UserInfoEndpoing);
        }

        /// <summary>
        /// Mefod for getting the identity content
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns>returns the identity content</returns>
        public async Task<string> GetIdentityBearer(string accessToken)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(_identityServerConfig.AuthenticationScheme, accessToken);
            return await client.GetStringAsync(_identityServerConfig.IdentityEndpoint);
        }
    }
}