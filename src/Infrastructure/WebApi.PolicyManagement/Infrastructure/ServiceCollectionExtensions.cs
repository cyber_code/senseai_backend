﻿using Microsoft.Extensions.DependencyInjection;
using Core.Abstractions;
using WebApi.PolicyManagement.Contracts;
using WebApi.PolicyManagement.Infrastructure.Connection;
using WebApi.PolicyManagement.Services;

namespace WebApi.PolicyManagement.Infrastructure
{
    /// <summary>
    /// Helper class to configure DI
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds the policy server client.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns></returns>
        public static void AddValidataPolicies(this IServiceCollection services, IIdentityServerConfig configuration)
        {
            //services.AddSingleton(configuration);
            services.AddScoped<IConnectionFactory, IdentityServerConnectionFactory>();
            services.AddScoped<IIdentityServerContract, IdentityServerService>();
            services.AddScoped<IPolicyService, PolicyService>();
            services.AddTransient<IPolicyServerRuntimeClient, PolicyServerRuntimeClient>();

            var policyServerBuilder = new PolicyServerBuilder(services);
            policyServerBuilder.AddAuthorizationPermissionPolicies();
        }
    }
}