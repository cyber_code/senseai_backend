﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Core.Abstractions;
using Persistence;
using WebApi.PolicyManagement.Contracts;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace WebApi.PolicyManagement
{
    /// <summary>
    ///     Middleware to automatically turn application roles and permissions into claims
    /// </summary>
    public class PolicyServerClaimsMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Initializes a new instance of the <see cref="PolicyServerClaimsMiddleware"/> class.
        /// </summary>
        /// <param name="next">The next.</param>
        public PolicyServerClaimsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Invoke
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="client">The client.</param>
        /// <returns></returns>
        [System.Obsolete]
        public async Task Invoke(HttpContext context, IPolicyServerRuntimeClient client, IWorkContext workContext, IHostingEnvironment environment)
        {
            const string referer = "swagger";

            if (context.Authentication != null)
            {
                var claimValue = context.Authentication.HttpContext.Request.Path.Value;

                if (claimValue.ToLower().Equals(client.IdentityServerConfig.SignInAction.ToLower()))
                {
                    var id = new ClaimsIdentity("PolicyServerMiddleware", "name", "role");
                    id.AddClaims(new[] { new Claim("permission", claimValue) });
                    context.User.AddIdentity(id);
                }
                else if (context.User.Identity.IsAuthenticated)
                {
                    DataSettingsManager.Instance.SetWorkContext(workContext);
                    string accessToken = string.Empty;

                    if (context.Request.Headers is HttpRequestHeaders)
                    {
                        accessToken = ((HttpRequestHeaders)context.Request.Headers).HeaderAuthorization.FirstOrDefault();
                    }
                    else if (context.Request.Headers.Keys.Contains("Authorization"))
                    {
                        accessToken = context.Request.Headers["Authorization"];
                    }
                    // var accessToken = ((HttpRequestHeaders) context.Request.Headers).HeaderAuthorization.FirstOrDefault();
                    var policy = await client.GetProfileDataAsync(accessToken);

                    if (policy.Permissions.Any(item => item.ToLower().Equals(claimValue.ToLower())))
                    {
                        var roleClaims = policy.Roles.Select(x => new Claim("role", x));
                        var permissionClaims = policy.Permissions.Select(x => new Claim("permission", x));

                        var id = new ClaimsIdentity("PolicyServerMiddleware", "name", "role");
                        id.AddClaims(roleClaims);
                        id.AddClaims(permissionClaims);

                        context.User.AddIdentity(id);
                    }
                    else
                    {
                        context.User = null;
                    }
                }
                else if (claimValue.ToLower() == "/api/design/downloadvideo")
                {
                    var id = new ClaimsIdentity("PolicyServerMiddleware", "name", "role");
                    id.AddClaim(new Claim("api", claimValue));

                    context.User.AddIdentity(id);
                }
                else if (claimValue.ToLower() == "/api/services/getversion")
                {
                    var id = new ClaimsIdentity("PolicyServerMiddleware", "name", "role");
                    id.AddClaim(new Claim("api", claimValue));

                    context.User.AddIdentity(id);
                }
                else if (claimValue == "/api/Process/Browse/DownloadProcessResources")
                {
                    var id = new ClaimsIdentity("PolicyServerMiddleware", "name", "role");
                    id.AddClaim(new Claim("api", claimValue));

                    context.User.AddIdentity(id);
                }
                else if (IsSwaggerReferer() && environment.IsDevelopment())
                {
                    var id = new ClaimsIdentity("PolicyServerMiddleware", "name", "role");
                    id.AddClaim(new Claim(referer, claimValue));

                    context.User.AddIdentity(id);
                }

                #region [ SenseAI DB ]

                //TODO: Save claims on SenseAI DB
                //else if (context.User.Identity.IsAuthenticated)
                //{
                //    var policy = await client.EvaluateAsync(context.User);
                //    var roleClaims = policy.Roles.Select(x => new Claim("role", x));
                //    var permissionClaims = policy.Permissions.Select(x => new Claim("permission", x));
                //    var id = new ClaimsIdentity("PolicyServerMiddleware", "name", "role");
                //    id.AddClaims(roleClaims);
                //    id.AddClaims(permissionClaims);
                //    context.User.AddIdentity(id);
                //}

                #endregion [ SenseAI DB ]

            }

            await _next(context);

            bool IsSwaggerReferer() => ((HttpRequestHeaders)context.Authentication.HttpContext.Request.Headers)
                .HeaderReferer.Any(r => r.Contains(referer, System.StringComparison.OrdinalIgnoreCase));
        }
    }
}