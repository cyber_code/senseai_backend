﻿using IdentityModel.Client;
using System.Threading.Tasks;

namespace WebApi.PolicyManagement.Contracts
{
    public interface IConnectionFactory
    {
        [System.Obsolete]
        TokenClient CreateTokenClient();

        [System.Obsolete]
        UserInfoClient CreateUserInfoClient();

        Task<string> GetIdentityBearer(string accessToken);
    }
}