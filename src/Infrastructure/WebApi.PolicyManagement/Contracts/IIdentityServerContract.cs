﻿using IdentityModel.Client;
using WebApi.PolicyManagement.Entities.Model;
using System.Threading.Tasks;

namespace WebApi.PolicyManagement.Contracts
{
    public interface IIdentityServerContract : IPolicyContract
    {
        Task<TokenResponse> SignIn(string userName, string password);

        Task<PolicyResult> GetProfileDataAsync(string accessToken);

        Task<string> GetIdentity(string accessToken);
    }
}