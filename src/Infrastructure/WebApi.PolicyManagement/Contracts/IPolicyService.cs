﻿using WebApi.PolicyManagement.Entities.Model;
using System.Threading.Tasks;

namespace WebApi.PolicyManagement.Contracts
{
    /// <summary>
    /// manage policies
    /// </summary>
    public interface IPolicyService
    {
        /// <summary>
        /// get a new policy with roles and permissions per user
        /// </summary>
        /// <param name="accessToken">Authentication access token</param>
        /// /// <param name="role">Authentication user role</param>
        /// <returns></returns>
        Task<PolicyResult> GetPolicyByAccessTokenAsync(string accessToken, string role);
    }
}