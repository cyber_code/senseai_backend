﻿using IdentityModel.Client;
using Core.Abstractions;
using WebApi.PolicyManagement.Entities.Model;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.PolicyManagement.Contracts
{
    /// <summary>
    /// Interface for PolicyServer client
    /// </summary>
    public interface IPolicyServerRuntimeClient
    {
        IIdentityServerConfig IdentityServerConfig { get; }

        /// <summary>
        /// Login to IS
        /// </summary>
        /// <param name="userName">The username</param>
        /// <param name="password">The password</param>
        /// <returns></returns>
        Task<TokenResponse> SignIn(string userName, string password);

        /// <summary>
        /// Determines whether the user has a permission.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="permission">The permission.</param>
        /// <returns></returns>
        Task<bool> HasPermissionAsync(ClaimsPrincipal user, string permission);

        Task<PolicyResult> GetProfileDataAsync(string accessToken);

        Task<string> GetIdentity(string accessToken);
    }
}