﻿using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PathGenerations
{
    public sealed class GeneratePaths : IGeneratePaths
    {
        private Workflow _workflow;

        /// <summary>
        /// Generate test cases for workflow based on a path
        /// </summary>
        /// <param name="workflowPaths"></param>
        /// <returns></returns>
        public List<Path> Generate(Workflow workflow)
        {
            if (workflow == null)
                throw new ArgumentException("The workflow object is null.", "workflow");

            _workflow = workflow;
            List<Path> paths = new List<Path>();

            if (_workflow.Items.Count() == 0 || _workflow.ItemLinks.Count() == 0)
                throw new Exception("The workflow does not have any items or there is no links between items.");

            var startItem = _workflow.Items.Where(ct => ct is StartWorkflowItem &&
           (_workflow.ItemLinks.Where(il => il.ToId == ct.Id).FirstOrDefault() == null)).FirstOrDefault();

            if (startItem == null)
                throw new Exception("The workflow does not have a start item.");

            List<Guid> list = new List<Guid>();
            var ids = _workflow.ItemLinks.Where(ct => ct.FromId == startItem.Id).ToList();
            foreach (var workflowItemLink in _workflow.ItemLinks.Where(ct => ct.FromId == startItem.Id).ToList())
            {
                list = new List<Guid>();
                var workflowItem = _workflow.Items.FirstOrDefault(ct => ct.Id == workflowItemLink.ToId);
                list.Add(startItem.Id);

                ProcessGeneration(workflowItem, list, paths, ((StartWorkflowItem)startItem).AdapterName);
            }
            if (paths.Count == 0)
                throw new Exception("Paths are not generated for the WF.");
            return paths;
        }

        /// <summary>
        /// Generate the test cases
        /// </summary>
        /// <param name="workflowItem"></param>
        /// <param name="list"></param>
        /// <param name="testCases"></param>
        ///<param name="adapterName"></param>
        private void ProcessGeneration(WorkflowItem workflowItem, List<Guid> list, List<Path> paths, string adapterName)
        {
            if (workflowItem is StartWorkflowItem)
            {
                if (workflowItem.Title == "Start")
                    StartWorkflow(workflowItem, list, paths);
                else { ActionWorkflow(workflowItem, list, paths, adapterName); }
            }
            else if (workflowItem is ConditionWorkflowItem)
            {
                ConditionWorkflow(workflowItem, list, paths, adapterName);
            }
            else if (workflowItem is ActionWorkflowItem)
            {
                ActionWorkflow(workflowItem, list, paths, adapterName);
            }
            else if (workflowItem is LinkedWorkflowItem)
            {
                LinkWorkflow(workflowItem, list, paths, adapterName);
            }
            else if (workflowItem is ManualWorkflowItem)
            {
                ManualWorkflow(workflowItem, list, paths, adapterName);
            }
            else
                throw new NotImplementedException("This type is not implemented");
        }

        /// <summary>
        /// Start workflow item
        /// </summary>
        /// <param name="newWorkflowItem"></param>
        /// <param name="list"></param>
        /// <param name="testCases"></param>
        private void StartWorkflow(WorkflowItem newWorkflowItem, List<Guid> list, List<Path> paths)
        {
            var items = _workflow.ItemLinks.Where(ct => ct.FromId == newWorkflowItem.Id).ToList();
            list.Add(newWorkflowItem.Id);
            foreach (var item in items)
            {
                var workflowItem = _workflow.Items.FirstOrDefault(ct => ct.Id == item.ToId);
                ProcessGeneration(workflowItem, list, paths, workflowItem.TypicalName);
            }
        }

        /// <summary>
        /// Action workflow with no data
        /// </summary>
        /// <param name="list"></param>
        /// <param name="workflowItem"></param>
        /// <param name="testCases"></param>
        /// <param name="adapterName"></param>
        private void ActionWorkflow(WorkflowItem newWorkflowItem, List<Guid> list, List<Path> paths, string adapterName)
        {
            list.Add(newWorkflowItem.Id);
            GenerateRecursively(newWorkflowItem, list, paths, adapterName);
        }

        /// <summary>
        /// Conditition workflow item
        /// </summary>
        /// <param name="newWorkflowItem"></param>
        /// <param name="list"></param>
        /// <param name="paths"></param>
        ///<param name="adapterName"></param>
        private void ConditionWorkflow(WorkflowItem newWorkflowItem, List<Guid> list, List<Path> paths, string adapterName)
        {
            if (list.Contains(newWorkflowItem.Id))
                return;
            var items = _workflow.ItemLinks.Where(ct => ct.FromId == newWorkflowItem.Id).ToList();
            list.Add(newWorkflowItem.Id);
            if (items.Count == 0)
                GenerateRecursively(newWorkflowItem, list, paths, adapterName);
            foreach (var item in items)
            {
                var workflowItem = _workflow.Items.FirstOrDefault(ct => ct.Id == item.ToId);
                List<Guid> currentList = new List<Guid>();
                currentList.AddRange(list);
                ProcessGeneration(workflowItem, currentList, paths, adapterName);
            }
        }

        /// <summary>
        /// Action workflow with no data
        /// </summary>
        /// <param name="list"></param>
        /// <param name="workflowItem"></param>
        /// <param name="testCases"></param>
        /// <param name="adapterName"></param>
        private void LinkWorkflow(WorkflowItem newWorkflowItem, List<Guid> list, List<Path> paths, string adapterName)
        {
            list.Add(newWorkflowItem.Id);
            GenerateRecursively(newWorkflowItem, list, paths, adapterName);
        }
        private void ManualWorkflow(WorkflowItem newWorkflowItem, List<Guid> list, List<Path> paths, string adapterName)
        {
            list.Add(newWorkflowItem.Id);
            GenerateRecursively(newWorkflowItem, list, paths, adapterName);
        }

        /// <summary>
        /// Generate recursively the test cases
        /// </summary>
        /// <param name="newWorkflowItem"></param>
        /// <param name="list"></param>
        /// <param name="testCases"></param>
        /// <param name="adapterName">adapterName</param>
        private void GenerateRecursively(WorkflowItem newWorkflowItem, List<Guid> list, List<Path> paths, string adapterName)
        {
            var items = _workflow.ItemLinks.Where(ct => ct.FromId == newWorkflowItem.Id).ToList();
            if (items.Count() == 0 || (newWorkflowItem.ParenID != Guid.Empty && items.Where(ct => ct.ParenID == newWorkflowItem.ParenID).Count() == 0))
            {
                Path path = new Path(_workflow.Title + " Path " + (paths.Count + 1).ToString(), paths.Count, 0, true, list.ToArray());

                paths.Add(path);
                return;
            }

            if (items.Count() > 1)
            {
                items = items.Where(w => w.UniqueId.Contains(newWorkflowItem.UniqueId)).ToList();
            }

            foreach (var newworkflowItemLink in items)
            {
                var workflowItems = _workflow.Items.Where(ct => ct.Id == newworkflowItemLink.ToId);//
                var workflowItem = _workflow.Items.FirstOrDefault(ct => ct.Id == newworkflowItemLink.ToId);

                if (workflowItems.Count() > 1)
                {
                    if (newworkflowItemLink.UniqueId != null && workflowItems.FirstOrDefault(w => w.UniqueId.Contains(newworkflowItemLink.UniqueId)) != null)
                        workflowItem = workflowItems.FirstOrDefault(w => w.UniqueId.Contains(newworkflowItemLink.UniqueId));
                    else
                        workflowItem = workflowItems.FirstOrDefault(w => newworkflowItemLink.UniqueId.Contains(w.UniqueId));

                }
                ProcessGeneration(workflowItem, list, paths, adapterName);
            }
        }
    }
}