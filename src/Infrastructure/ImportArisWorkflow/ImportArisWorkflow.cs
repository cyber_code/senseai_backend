﻿using SenseAI.Domain.DataModel;
using SenseAI.Domain.Process.ArisWorkflowModel;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;


namespace ImportAris
{
    public sealed class ImportArisWorkflow : IImportArisWorkflow
    {
        public Workflow ArisWorkflowToWorkflow(ArisWorkflow arisWorkflow, Guid folderId, Guid systemId, Guid catalogId, string catalogTitle,
           List<Typical> typicls)
        {
            ExportArisWorkflow2Workflow arisWorkflow2Workflow = new ExportArisWorkflow2Workflow(arisWorkflow, folderId, systemId, catalogId, catalogTitle, typicls);
            return arisWorkflow2Workflow.Export(true);
        }

        public List<ArisWorkflow> XmlToArisWorkflow(string fileContent)
        {
            ExportXml2ArisWorkflow exportXml2ArisWorkflow = new ExportXml2ArisWorkflow(fileContent);
            return exportXml2ArisWorkflow.Export();
        }

        public List<Workflow> XmlToWorkflow(string fileContent, Guid folderId, Guid systemId, Guid catalogId, string catalogTitle, List<Typical> typicls)
        {
            List<Workflow> lstWorkflows = new List<Workflow>();
            ExportXml2ArisWorkflow exportXml2ArisWorkflow = new ExportXml2ArisWorkflow(fileContent);
            var lstArisWorkflow = exportXml2ArisWorkflow.Export();
            foreach (ArisWorkflow arisWorkflow in lstArisWorkflow)
            {
                var linked = arisWorkflow.Items.Where(w => w.Type == ArisWorkflowType.Process);
                foreach (var link in linked)
                {
                    if (link.ModuleId != null)
                    {
                        var linkWorkflow = lstArisWorkflow.FirstOrDefault(w => w.ModuleId == link.ModuleId);
                        if (linkWorkflow != null)
                            link.SetArisWorkflowLinkId(linkWorkflow.Id);
                    }
                }
                ExportArisWorkflow2Workflow arisWorkflow2Workflow = new ExportArisWorkflow2Workflow(arisWorkflow, folderId, systemId, catalogId, catalogTitle, typicls);
                lstWorkflows.Add(arisWorkflow2Workflow.Export());
            }
            return lstWorkflows;
        }
    }
}