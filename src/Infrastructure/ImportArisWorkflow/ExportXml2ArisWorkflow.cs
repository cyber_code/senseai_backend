﻿using ImportAris.Aris;
using SenseAI.Domain.Process.ArisWorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace ImportAris
{
    public sealed class ExportXml2ArisWorkflow
    {
        private ArisXml _arisXml;

        public ExportXml2ArisWorkflow(string fileContent)
        {
            if (string.IsNullOrEmpty(fileContent))
                throw new ArgumentException("The input file has no content.", "fileContent");
             

            ValidateXml(fileContent);

            _arisXml = ArisXml.FromXml(fileContent, true); 
        }

        public List<ArisWorkflow> Export()
        {
            var groups = GetAllGroupsContainingModels();
            List<ArisWorkflow> arisWorkflows = new List<ArisWorkflow>();
            foreach (var group in groups)
            {
                foreach (var model in GetAllModels(group))
                {
                    ArisWorkflow arisWorkflow = CreateArisWorkflowPerGroup(model, group);
                    arisWorkflows.Add(arisWorkflow);
                }
            }
            return arisWorkflows;
        }

        private ArisWorkflow CreateArisWorkflowPerGroup(Model model, Group aGroup)
        {
            var arisWorkflow = new ArisWorkflow(Guid.NewGuid(), Guid.NewGuid(), model.Name, aGroup.Name + " " + model.Name, "", model.Id);
            var type = ArisWorkflowType.Activity;
            foreach (var objectOccurence in model.ObjectOccurrences)
            {
                if (objectOccurence.InConnections.Count == 0 && objectOccurence.OutConnections.Count == 0)
                    continue;
                type = ArisWorkflowType.Activity;
                Guid arisId = Guid.NewGuid();
                if (IsStandartEpcObject(objectOccurence))
                {
                    if (objectOccurence.Type == "OT_EVT" && objectOccurence.SymbolNum == "ST_EV")
                        type = ArisWorkflowType.Event;
                    else if (objectOccurence.Type == "OT_FUNC" && objectOccurence.SymbolNum == "ST_FUNC")
                        type = ArisWorkflowType.Activity;
                    else if (objectOccurence.Type == "OT_RULE")
                        type = ArisWorkflowType.Conditional;
                    var item = new ArisWorkflowItem(arisId, type, objectOccurence.Name, objectOccurence.Description == null ? objectOccurence.Name : objectOccurence.Description, "", Guid.Empty, objectOccurence.Id);
                    item.SetPosition(objectOccurence.Position.X, objectOccurence.Position.Y);
                    arisWorkflow.AddItem(item);
                }
                else
                {
                    string moduleId = null;
                    if (objectOccurence.SymbolNum == "ST_EMPL_TYPE" && objectOccurence.Type == "OT_PERS_TYPE")
                        type = ArisWorkflowType.Role;
                    if (objectOccurence.SymbolNum == "ST_APPL_SYS_TYPE" && objectOccurence.Type == "OT_APPL_SYS_TYPE")
                        type = ArisWorkflowType.SystemType;
                    if (objectOccurence.SymbolNum == "ST_DOC" && objectOccurence.Type == "OT_INFO_CARR")
                        type = ArisWorkflowType.Resource;
                    if (objectOccurence.SymbolNum == "ST_PRCS_IF" && objectOccurence.Type == "OT_FUNC")
                    {
                        type = ArisWorkflowType.Process;
                        if (objectOccurence.AssignedModels.Count > 0)
                            moduleId = objectOccurence.AssignedModels.Select(s => s.Id).FirstOrDefault();
                    }
                    if (objectOccurence.SymbolNum == "ST_SCRN" && objectOccurence.Type == "OT_SCRN")
                    {
                        type = ArisWorkflowType.T24Screen;
                    }
                    var item = new ArisWorkflowItem(arisId, type, objectOccurence.Name, objectOccurence.Description == null ? objectOccurence.Name : objectOccurence.Description, objectOccurence.Name, Guid.Empty, objectOccurence.Id);
                    item.SetPosition(objectOccurence.Position.X, objectOccurence.Position.Y);
                    if (moduleId != null)
                        item.SetModuleId(moduleId);
                    arisWorkflow.AddItem(item);
                }
            }

            foreach (var objectOccurence in model.ObjectOccurrences)
            {
                foreach (var toConn in objectOccurence.OutConnections)
                {
                    var from = arisWorkflow.Items.FirstOrDefault(ct => ct.ArisSourceOccurenceId == toConn.SourceOccurenceId);
                    var to = arisWorkflow.Items.FirstOrDefault(ct => ct.ArisSourceOccurenceId == toConn.TargetOccurrence.Id);

                    arisWorkflow.AddItemLink(from.Id, to.Id, SenseAI.Domain.ArisWorkflowItemLinkState.No);
                }
            }

            arisWorkflow.NormalizeCoordinatesOfWorkItems();
            //arisWorkflow.GroupRoles();

            arisWorkflow.OrderByCordinateY();
            return arisWorkflow;
        }

        private List<Model> GetAllModels()
        {
            var models = from model in _arisXml.AllModels
                         where model.Type == Constants.ModelTypes.EpcDiagram
                         select model;

            return models.ToList();
        }

        private List<Group> GetAllGroupsContainingModels()
        {
            var groups = (from aGroup in _arisXml.AllGroups
                          select aGroup).ToList();

            var remainingGroups = (from aGroup in _arisXml.AllGroups
                                   select aGroup).ToList();

            var removedGroupsExternalIds = new List<string>();

            for (int i = 0; i < _arisXml.AllGroups.Count; i++)
            {
                foreach (var aGroup in remainingGroups)
                {
                    if (
                        (aGroup.Groups == null || aGroup.Groups.Count == 0 || aGroup.Groups.TrueForAll(x => removedGroupsExternalIds.Contains(x.Id))) //The current group does not contain subgroups.
                        &&
                        (aGroup.Models == null || !aGroup.Models.Exists(model => model.Type == Constants.ModelTypes.EpcDiagram)) //The current group does not contain EPC models.
                       )
                    {
                        removedGroupsExternalIds.Add(aGroup.Id);
                        groups.Remove(aGroup);
                        i++;
                    }
                }

                if (groups.Count == 0)
                {
                    break;
                }

                remainingGroups = (from aGroup in groups
                                   select aGroup).ToList();
            }

            return groups;
        }

        private List<Model> GetAllModels(Group aGroup)
        {
            var models = from model in aGroup.Models
                         where model.Type == Constants.ModelTypes.EpcDiagram
                         select model;

            return models.ToList();
        }

        private bool IsStandartEpcObject(ObjectOccurrence epcObject)
        {
            if ("OT_EVT" == epcObject.Type
                && "ST_EV" == epcObject.SymbolNum)
                return true;

            if ("OT_FUNC" == epcObject.Type
                && "ST_FUNC" == epcObject.SymbolNum)
                return true;

            if ("OT_RULE" == epcObject.Type
                && ("ST_OPR_AND_1" == epcObject.SymbolNum
                    || "ST_OPR_OR_1" == epcObject.SymbolNum
                    || "ST_OPR_XOR_1" == epcObject.SymbolNum))
                return true;

            return false;
        }

        private void ValidateXml(string xml)
        {
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xml);
            }
            catch (Exception)
            {
                throw new Exception("Xml is not valid.");
            }
        }
    }
}