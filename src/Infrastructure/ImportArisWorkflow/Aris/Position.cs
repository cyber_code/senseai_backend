﻿using System.Xml.Serialization;

namespace ImportAris.Aris
{
    public class Position
    {
        [XmlAttribute(AttributeName = "Pos.X")]
        public int X { get; set; }
     
        [XmlAttribute(AttributeName = "Pos.Y")]
        public int Y { get; set; }
    }
}