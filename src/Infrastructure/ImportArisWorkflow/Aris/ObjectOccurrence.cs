﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    [XmlType("ObjOcc")]
    public class ObjectOccurrence
    {
        [XmlIgnore]
        private ObjectDefinition definition;

        public ObjectOccurrence()
        {
            this.InConnections = new List<ConnectionOccurrence>();
        }

        [XmlIgnore]
        public List<ConnectionOccurrence> InConnections { get; internal set; } 

        [XmlElement(ElementName = "CxnOcc")]
        public List<ConnectionOccurrence> OutConnections { get; set; }

        public Size Size { get; set; }
        
        public Position Position { get; set; }
       
        [XmlElement(ElementName = "SymbolGUID")]

        public string SymbolGuid { get; set; }
        
        [XmlAttribute(AttributeName = "SymbolNum")]
        public string SymbolNum { get; set; }
        
        [XmlAttribute(AttributeName = "ObjDef.IdRef")]
        public string ObjectDefinitionId { get; set; }
        
        [XmlAttribute(AttributeName = "ObjOcc.ID")]
        public string Id { get; set; }
        
        [XmlIgnore]
        public ObjectDefinition Definition
        {
            get
            {
                return this.definition;
            }
            internal set
            {
                this.definition = value;
                if (this.definition.AllOccurrencies.Contains(this))
                    return;
                this.definition.AllOccurrencies.Add(this);
            }
        }

        [XmlIgnore]
        public List<Model> AssignedModels
        {
            get
            {
                List<Model> linkedModels = this.definition.LinkedModels;
                if (linkedModels == null)
                    return new List<Model>();
                return linkedModels.Where<Model>((Func<Model, bool>)(m => m.Id != this.Model.Id)).ToList<Model>();
            }
        }

        /// <summary>Gets the name of the occurrence</summary>
        /// <remarks>Preserves new lines!</remarks>
        [XmlIgnore]
        public string Name
        {
            get
            {
                if (this.Definition == null)
                    return string.Empty;
                return this.Definition.Name;
            }
        }

        /// <summary>Gets the description of the occurrence</summary>
        /// <remarks>Preserves new lines!</remarks>
        [XmlIgnore]
        public string Description
        {
            get
            {
                if (this.Definition == null)
                    return string.Empty;
                return this.Definition.Description;
            }
        }

        /// <summary>Gets the type of the occurrence</summary>
        [XmlIgnore]
        public string Type
        {
            get
            {
                return this.Definition.Type;
            }
        }

        /// <summary>Gets all attributes of the occurrence</summary>
        [XmlIgnore]
        public List<AttributeDefinition> AttributeDefinitions
        {
            get
            {
                return this.Definition.AttributeDefinitions;
            }
        }

        /// <summary>Gets the model in which the occurence appears</summary>
        [XmlIgnore]
        public Model Model { get; internal set; }

        /// <summary>Gets the symbol of the occurrence</summary>
        [XmlIgnore]
        public string Symbol
        {
            get
            {
                if (!string.IsNullOrEmpty(this.SymbolGuid))
                    return this.SymbolGuid;
                return this.SymbolNum;
            }
        }

        /// <summary>
        /// Returns string representation in format "ObjOcc: [Name]"
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "ObjOcc: " + (string.IsNullOrEmpty(this.Name) ? this.Id : this.Name);
        }
    }
}