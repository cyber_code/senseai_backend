﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    public sealed class Font
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "StyledElement")]
        public List<StyledElement> StyledElements { get; set; }

        public string PlainText
        {
            get
            {
                string empty = string.Empty;
                if (this.StyledElements == null)
                    return string.Empty;
                foreach (StyledElement styledElement in this.StyledElements)
                {
                    if (styledElement.Paragraph != null && styledElement.Paragraph.StyledElement != null && styledElement.Paragraph.StyledElement.PlainText != null)
                        empty += styledElement.PlainText.TextValue;
                }
                return empty;
            }
        }
    }
}
