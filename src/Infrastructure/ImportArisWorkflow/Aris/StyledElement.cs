﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    public sealed class StyledElement
    {
        [XmlElement(ElementName = "SizeElement")]
        public SizeElement SizeElement { get; set; }

        [XmlElement(ElementName = "Font")]
        public Font Font { get; set; }

        [XmlElement(ElementName = "Paragraph")]
        public Paragraph Paragraph { get; set; }

        [XmlElement(ElementName = "PlainText")]
        public PlainText PlainText { get; set; }
    }
}
