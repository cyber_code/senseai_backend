﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    [XmlType("FFTextDef")]
    public class FreeFormTextDefinition
    {
        internal const string ModelAttributeFlag = "MODELATTR";

        public FreeFormTextDefinition()
        {
            this.AttributeDefinitions = new List<AttributeDefinition>();
        }

        [XmlAttribute(AttributeName = "FFTextDef.ID")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "IsModelAttr")]
        public string IsBoundToModelAttribute { get; set; }

        [XmlElement(ElementName = "GUID")]
        public string Guid { get; set; }

        [XmlElement(ElementName = "AttrDef")]
        public List<AttributeDefinition> AttributeDefinitions { get; set; }

        [XmlIgnore]
        public string TargetModelAttribute
        {
            get
            {
                if (this.AttributeDefinitions == null)
                    return string.Empty;
                return this.AttributeDefinitions.Where<AttributeDefinition>((Func<AttributeDefinition, bool>)(att => att.Type == "AT_MODEL_AT")).Select<AttributeDefinition, string>((Func<AttributeDefinition, string>)(att => att.Value.PlainText)).FirstOrDefault<string>();
            }
        }

        [XmlIgnore]
        public string Text
        {
            get
            {
                if (this.IsBoundToModelAttribute == "MODELATTR")
                    return "##MODELATTR##";
                if (this.AttributeDefinitions == null)
                    return string.Empty;
                return this.AttributeDefinitions.Where<AttributeDefinition>((Func<AttributeDefinition, bool>)(att => att.Type == "AT_NAME")).Select<AttributeDefinition, string>((Func<AttributeDefinition, string>)(att => att.Value.FormattedText)).FirstOrDefault<string>();
            }
        }

        public override string ToString()
        {
            return "FFTextDef: " + this.Text;
        }
    }
}