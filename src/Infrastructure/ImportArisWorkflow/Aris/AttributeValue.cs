﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    [XmlType("AttrValue")]
    public class AttributeValue
    {

        [XmlAnyElement]
        public List<XmlElement> AllElements;

        [XmlAttribute(AttributeName = "LocaleId")]
        public string LocaleID { get; set; }

        [XmlText]
        public string Text { get; set; }

        public string PlainText
        {
            get
            {
                return string.Join(Environment.NewLine, this.PlainTextList.ToArray());
            }
        }

        public string FormattedText
        {
            get
            {
                return this.PlainText;
            }
        }


        public List<string> PlainTextList
        {
            get
            {
                if (this.Text != null)
                    return new List<string>() { this.Text };
                if (this.AllElements != null && this.AllElements.Count > 0)
                {
                    List<string> stringList = new List<string>();
                    XmlNodeList elementsByTagName = this.AllElements[0].GetElementsByTagName("PlainText");
                    for (int index = 0; index < elementsByTagName.Count; ++index)
                        stringList.Add(elementsByTagName[index].Attributes["TextValue"].InnerText);
                    return stringList;
                }
                return new List<string>() { "Not FOUND" };
            }
        }
    }
}