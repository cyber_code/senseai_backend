﻿using System.Xml.Serialization;

namespace ImportAris.Aris
{
    public sealed class HeaderInfo
    {
        [XmlAttribute(AttributeName = "DatabaseName")]
        public string DatabaseName { get; set; }
    }
}
