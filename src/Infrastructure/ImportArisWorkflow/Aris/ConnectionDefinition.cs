﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    [XmlType("CxnDef")]
    public class ConnectionDefinition
    {
        [XmlAttribute(AttributeName = "CxnDef.ID")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "CxnDef.Type")]
        public string Type { get; set; }

        public string GUID { get; set; }

        [XmlAttribute(AttributeName = "ToObjDef.IdRef")]
        public string TargetObjectDefintionId { get; set; }

        [XmlElement(ElementName = "AttrDef")]
        public List<AttributeDefinition> AttributeDefinitions { get; set; }

        [XmlIgnore]
        public string Complexity
        {
            get
            {
                if (this.AttributeDefinitions == null)
                    return string.Empty;
                return this.AttributeDefinitions.Where<AttributeDefinition>((Func<AttributeDefinition, bool>)(att => att.Type == "AT_CMPLXY")).Select<AttributeDefinition, string>((Func<AttributeDefinition, string>)(att => att.Value.PlainText)).FirstOrDefault<string>();
            }
        }

        [XmlIgnore]
        public ObjectDefinition TargetObject { get; set; }

        [XmlIgnore]
        public ObjectDefinition SourceObject { get; set; }


        public override string ToString()
        {
            string format = "From: {0} |To: {1}";
            string str1 = this.TargetObject != null ? this.TargetObject.ToString() : this.TargetObjectDefintionId;
            string str2 = this.SourceObject != null ? this.SourceObject.ToString() : string.Empty;
            return string.Format(format, (object)str2, (object)str1);
        }
    }
}