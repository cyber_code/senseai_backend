﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    [XmlType("ObjDef")]
    public sealed class ObjectDefinition
    {
        private const string nameAttribute = "AT_NAME";

        private const string descriptionAttribute = "AT_DESC";

        public ObjectDefinition()
        {
            this.AllOccurrencies = new List<ObjectOccurrence>();
            this.LinkedModels = new List<Model>();
            this.IncommingConnections = new List<ConnectionDefinition>();
        }

        [XmlIgnore]
        public List<ObjectOccurrence> AllOccurrencies { get; private set; }
       
        [XmlIgnore]
        public string Description
        {
            get
            {
                if (this.AttributeDefinitions == null)
                    return string.Empty;
                return this.AttributeDefinitions.Where<AttributeDefinition>((Func<AttributeDefinition, bool>)(att => att.Type == "AT_DESC")).Select<AttributeDefinition, string>((Func<AttributeDefinition, string>)(att => att.Value.FormattedText)).FirstOrDefault<string>();
            }
        }

        [XmlIgnore]
        public string Name
        {
            get
            {
                if (this.AttributeDefinitions == null)
                    return string.Empty;
                return this.AttributeDefinitions.Where<AttributeDefinition>((Func<AttributeDefinition, bool>)(att => att.Type == "AT_NAME")).Select<AttributeDefinition, string>((Func<AttributeDefinition, string>)(att => att.Value.FormattedText)).FirstOrDefault<string>();
            }
        }

        [XmlElement(ElementName = "SymbolGUID")]
        public string SymbolGUID { get; set; }
       
        [XmlAttribute("LinkedModels.IdRefs")]
        public string LinkedModelIds { get; set; }
       
        [XmlAttribute("ToCxnDefs.IdRefs")]
        public string IncomingConnectionIds { get; set; }
       
        [XmlIgnore]
        public List<ConnectionDefinition> IncommingConnections { get; set; }
       
        [XmlElement(ElementName = "CxnDef")]
        public List<ConnectionDefinition> OutgoingConnections { get; set; }
       
        [XmlAttribute(AttributeName = "SymbolNum")]
        public string Symbol { get; set; }
        
        [XmlAttribute(AttributeName = "TypeNum")]
        public string Type { get; set; }
        
        public string GUID { get; set; }
        
        [XmlAttribute(AttributeName = "ObjDef.ID")]
        public string Id { get; set; }
        
        [XmlElement(ElementName = "AttrDef")]
        public List<AttributeDefinition> AttributeDefinitions { get; set; }
        
        [XmlIgnore]
        public List<Model> LinkedModels { get; set; }


        public ObjectOccurrence GetOccurrenceInModel(Model model)
        {
            return this.AllOccurrencies.Where<ObjectOccurrence>((Func<ObjectOccurrence, bool>)(o => o.Model.Id == model.Id)).FirstOrDefault<ObjectOccurrence>();
        }

        public override string ToString()
        {
            return "ObjDef: " + (string.IsNullOrEmpty(this.Name) ? this.Id : this.Name);
        }
    }
}