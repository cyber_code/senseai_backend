﻿using System.Xml.Serialization;

namespace ImportAris.Aris
{
    public sealed class PlainText
    {
        [XmlAttribute(AttributeName = "TextValue")]
        public string TextValue { get; set; }
    }
}
