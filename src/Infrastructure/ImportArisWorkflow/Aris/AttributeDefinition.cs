﻿using System.Xml.Serialization;

namespace ImportAris.Aris
{
    [XmlType("AttrDef")]
    public class AttributeDefinition
    {
        [XmlAttribute(AttributeName = "AttrDef.Type")]
        public string Type { get; set; }

        [XmlElement(ElementName = "AttrValue")]
        public AttributeValue Value { get; set; }
    }
}