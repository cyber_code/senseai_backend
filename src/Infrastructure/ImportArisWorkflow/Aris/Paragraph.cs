﻿using System.Xml.Serialization;

namespace ImportAris.Aris
{
    public sealed  class Paragraph
    {
        [XmlAttribute(AttributeName = "Indent")]
        public string Indent { get; set; }

        [XmlElement(ElementName = "StyledElement")]
        public StyledElement StyledElement { get; set; }
    }
}
