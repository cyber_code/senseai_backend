﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    public sealed class Group
    {
        private const string nameAttribute = "AT_NAME";

        [XmlAttribute(AttributeName = "Group.ID")]
        public string Id { get; set; }

        [XmlElement(ElementName = "ObjDef")]
        public List<ObjectDefinition> ObjectDefinitions { get; set; }

        [XmlElement(ElementName = "Model")]
        public List<Model> Models { get; set; }

        [XmlElement(ElementName = "Group")]
        public List<Group> Groups { get; set; }

        [XmlElement(ElementName = "AttrDef")]
        public List<AttributeDefinition> AttributeDefinitions { get; set; }

        [XmlIgnore]
        public string Name
        {
            get
            {
                if (this.AttributeDefinitions == null)
                    return this.Id;
                string str = this.AttributeDefinitions.Where<AttributeDefinition>((Func<AttributeDefinition, bool>)(att => att.Type == "AT_NAME")).Select<AttributeDefinition, string>((Func<AttributeDefinition, string>)(att => att.Value.PlainText)).FirstOrDefault<string>();
                return string.IsNullOrEmpty(str) ? this.Id : str;
            }
        }

        public List<Model> FindModelsByType(string type)
        {
            List<Model> modelList = new List<Model>();
            if (this.Models != null)
            {
                foreach (Model model in this.Models)
                {
                    if (model.Type == type)
                        modelList.Add(model);
                }
            }
            if (this.Groups != null)
            {
                foreach (Group group in this.Groups)
                {
                    List<Model> modelsByType = group.FindModelsByType(type);
                    if (modelsByType != null)
                    {
                        foreach (Model model in modelsByType)
                            modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        public void Compile(List<ObjectDefinition> objDefs)
        {
            if (this.Models != null)
            {
                foreach (Model model in this.Models)
                {
                    if (model.ObjectOccurrences != null)
                    {
                        foreach (ObjectOccurrence objectOccurrence in model.ObjectOccurrences)
                            objectOccurrence.Definition = Group.FindObjectDefinition(objDefs, objectOccurrence.ObjectDefinitionId);
                    }
                }
            }
            if (this.Groups == null)
                return;
            foreach (Group group in this.Groups)
                group.Compile(objDefs);
        }

        private static ObjectDefinition FindObjectDefinition(
     List<ObjectDefinition> objDefs,
     string objDefID)
        {
            if (objDefs == null)
                return (ObjectDefinition)null;
            foreach (ObjectDefinition objDef in objDefs)
            {
                if (objDef.Id == objDefID)
                    return objDef;
            }
            return (ObjectDefinition)null;
        }
    }
}