﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    public class Model
    {
        private const string nameAttribute = "AT_NAME";

        [XmlAttribute(AttributeName = "Model.ID")]
        public string Id { get; set; }
     
        public string GUID { get; set; }
        
        [XmlAttribute(AttributeName = "Model.Type")]
        public string Type { get; set; }
        
        [XmlElement(ElementName = "ObjOcc")]
        public List<ObjectOccurrence> ObjectOccurrences { get; set; }
        
        [XmlElement(ElementName = "AttrDef")]
        public List<AttributeDefinition> AttributeDefinitions { get; set; }
        
        [XmlElement("FFTextOcc")]
        public List<FreeFormTextOccurrence> FreeFormTextOccurrences { get; set; }
        
        [XmlIgnore]
        public string Name
        {
            get
            {
                return this.AttributeDefinitions.Where<AttributeDefinition>((Func<AttributeDefinition, bool>)(att => att.Type == "AT_NAME")).Select<AttributeDefinition, string>((Func<AttributeDefinition, string>)(att => att.Value.PlainText)).FirstOrDefault<string>();
            }
        }

        public List<ObjectOccurrence> FindObjectOccurrencesByType(string type)
        {
            List<ObjectOccurrence> objectOccurrenceList = new List<ObjectOccurrence>();
            foreach (ObjectOccurrence objectOccurrence in this.ObjectOccurrences)
            {
                if (objectOccurrence.Symbol == type)
                    objectOccurrenceList.Add(objectOccurrence);
            }
            return objectOccurrenceList;
        }
    }
}