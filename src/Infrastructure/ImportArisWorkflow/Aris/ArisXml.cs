﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    [XmlRoot("AML")]
    public sealed class ArisXml
    {
        [XmlAttribute("Description")]
        public string Description { get; set; }

        [XmlElement("Header-Info")]
        public HeaderInfo HeaderInfo { get; set; }

        [XmlIgnore]
        public string Name
        {
            get
            {
                if (this.HeaderInfo != null)
                    return this.HeaderInfo.DatabaseName.Trim();
                return string.Empty;
            }
        }

        [XmlElement(ElementName = "Group")]
        public List<Group> Groups { get; set; }

        [XmlElement("FFTextDef")]
        public List<FreeFormTextDefinition> FreeFormTextDefinitions { get; set; }

        [XmlIgnore]
        public List<Model> AllModels { get; internal set; }

        [XmlIgnore]
        public List<Group> AllGroups { get; internal set; }

        public void Compile()
        {
            List<ObjectDefinition> objDefs = new List<ObjectDefinition>();
            foreach (Group group in this.Groups)
                objDefs.AddRange((IEnumerable<ObjectDefinition>)this.ExtractObjectDefinitions(group));
            foreach (Group group in this.Groups)
                group.Compile(objDefs);
        }

        private List<ObjectDefinition> ExtractObjectDefinitions(Group group)
        {
            List<ObjectDefinition> objectDefinitionList = new List<ObjectDefinition>();
            if (group.ObjectDefinitions != null)
            {
                foreach (ObjectDefinition objectDefinition in group.ObjectDefinitions)
                    objectDefinitionList.Add(objectDefinition);
            }
            if (group.Groups != null)
            {
                foreach (Group group1 in group.Groups)
                    objectDefinitionList.AddRange((IEnumerable<ObjectDefinition>)this.ExtractObjectDefinitions(group1));
            }
            return objectDefinitionList;
        }

        public static void PrepareInnerObjects(ArisXml arisXml)
        {
            List<Group> groupList = new List<Group>();
            Queue<Group> groupQueue = new Queue<Group>();
            foreach (Group group in arisXml.Groups)
                groupQueue.Enqueue(group);
            do
            {
                Group group1 = groupQueue.Dequeue();
                if (group1.Groups != null)
                {
                    foreach (Group group2 in group1.Groups)
                        groupQueue.Enqueue(group2);
                }
                groupList.Add(group1);
            }
            while (groupQueue.Count > 0);
            arisXml.AllGroups = groupList;
            arisXml.AllModels = arisXml.AllGroups.Where<Group>((Func<Group, bool>)(g => g.Models != null)).Select<Group, List<Model>>((Func<Group, List<Model>>)(g => g.Models.ToList<Model>())).Aggregate<List<Model>, List<Model>, List<Model>>(new List<Model>(), (Func<List<Model>, List<Model>, List<Model>>)((l, m) =>
            {
                l.AddRange((IEnumerable<Model>)m);
                return l;
            }), (Func<List<Model>, List<Model>>)(l => l));
            Dictionary<string, FreeFormTextDefinition> dictionary1 = new Dictionary<string, FreeFormTextDefinition>();
            if (arisXml.FreeFormTextDefinitions != null)
            {
                foreach (FreeFormTextDefinition formTextDefinition in arisXml.FreeFormTextDefinitions)
                    dictionary1.Add(formTextDefinition.Id, formTextDefinition);
            }
            foreach (Model allModel in arisXml.AllModels)
            {
                if (allModel.FreeFormTextOccurrences != null)
                {
                    foreach (FreeFormTextOccurrence formTextOccurrence in allModel.FreeFormTextOccurrences)
                    {
                        formTextOccurrence.Definition = dictionary1[formTextOccurrence.DefinitionId];
                        formTextOccurrence.Model = allModel;
                    }
                }
            }
            Dictionary<string, ObjectDefinition> dictionary2 = arisXml.AllGroups.Where<Group>((Func<Group, bool>)(def => def.ObjectDefinitions != null)).Select<Group, List<ObjectDefinition>>((Func<Group, List<ObjectDefinition>>)(def => def.ObjectDefinitions)).Aggregate<List<ObjectDefinition>, List<ObjectDefinition>, List<ObjectDefinition>>(new List<ObjectDefinition>(), (Func<List<ObjectDefinition>, List<ObjectDefinition>, List<ObjectDefinition>>)((l, d) =>
            {
                l.AddRange((IEnumerable<ObjectDefinition>)d);
                return l;
            }), (Func<List<ObjectDefinition>, List<ObjectDefinition>>)(l => l)).ToDictionary<ObjectDefinition, string>((Func<ObjectDefinition, string>)(d => d.Id));
            Dictionary<string, Model> dictionary3 = arisXml.AllModels.Select<Model, Model>((Func<Model, Model>)(m => m)).ToDictionary<Model, string>((Func<Model, string>)(m => m.Id));
            foreach (ObjectDefinition objectDefinition in dictionary2.Values)
            {
                if (!string.IsNullOrEmpty(objectDefinition.LinkedModelIds))
                {
                    string linkedModelIds = objectDefinition.LinkedModelIds;
                    string[] separator = new string[1] { " " };
                    foreach (string index in linkedModelIds.Split(separator, StringSplitOptions.RemoveEmptyEntries))
                    {
                        Model model = dictionary3[index];
                        objectDefinition.LinkedModels.Add(model);
                    }
                }
            }
            List<ObjectOccurrence> source = arisXml.AllModels.Where<Model>((Func<Model, bool>)(m => m.ObjectOccurrences != null)).Select<Model, List<ObjectOccurrence>>((Func<Model, List<ObjectOccurrence>>)(m => m.ObjectOccurrences)).Aggregate<List<ObjectOccurrence>, List<ObjectOccurrence>, List<ObjectOccurrence>>(new List<ObjectOccurrence>(), (Func<List<ObjectOccurrence>, List<ObjectOccurrence>, List<ObjectOccurrence>>)((l, o) =>
            {
                l.AddRange((IEnumerable<ObjectOccurrence>)o);
                return l;
            }), (Func<List<ObjectOccurrence>, List<ObjectOccurrence>>)(l => l));
            using (List<ObjectOccurrence>.Enumerator enumerator = source.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    ObjectOccurrence occ = enumerator.Current;
                    string objectDefinitionId = occ.ObjectDefinitionId;
                    occ.Definition = dictionary2[objectDefinitionId];
                    occ.Model = arisXml.AllModels.Where<Model>((Func<Model, bool>)(m => m.ObjectOccurrences != null && m.ObjectOccurrences.Contains(occ))).First<Model>();
                    if (occ.OutConnections != null)
                    {
                        foreach (ConnectionOccurrence outConnection in occ.OutConnections)
                        {
                            outConnection.SourceOccurenceId = occ.Id;
                            outConnection.SourceOccurrence = occ;
                        }
                    }
                }
            }
            Dictionary<string, ObjectOccurrence> dictionary4 = source.ToDictionary<ObjectOccurrence, string>((Func<ObjectOccurrence, string>)(o => o.Id));
            Dictionary<string, ConnectionDefinition> dictionary5 = dictionary2.Values.Where<ObjectDefinition>((Func<ObjectDefinition, bool>)(c => c.OutgoingConnections != null)).Select<ObjectDefinition, List<ConnectionDefinition>>((Func<ObjectDefinition, List<ConnectionDefinition>>)(c => c.OutgoingConnections)).Aggregate<List<ConnectionDefinition>, List<ConnectionDefinition>, List<ConnectionDefinition>>(new List<ConnectionDefinition>(), (Func<List<ConnectionDefinition>, List<ConnectionDefinition>, List<ConnectionDefinition>>)((l, d) =>
            {
                l.AddRange((IEnumerable<ConnectionDefinition>)d);
                return l;
            }), (Func<List<ConnectionDefinition>, List<ConnectionDefinition>>)(l => l)).ToDictionary<ConnectionDefinition, string>((Func<ConnectionDefinition, string>)(c => c.Id));
            foreach (ConnectionOccurrence connectionOccurrence in source.Where<ObjectOccurrence>((Func<ObjectOccurrence, bool>)(c => c.OutConnections != null)).Select<ObjectOccurrence, List<ConnectionOccurrence>>((Func<ObjectOccurrence, List<ConnectionOccurrence>>)(c => c.OutConnections)).Aggregate<List<ConnectionOccurrence>, List<ConnectionOccurrence>, List<ConnectionOccurrence>>(new List<ConnectionOccurrence>(), (Func<List<ConnectionOccurrence>, List<ConnectionOccurrence>, List<ConnectionOccurrence>>)((l, c) =>
            {
                l.AddRange((IEnumerable<ConnectionOccurrence>)c);
                return l;
            }), (Func<List<ConnectionOccurrence>, List<ConnectionOccurrence>>)(l => l)))
            {
                string connectionDefinitionId = connectionOccurrence.ConnectionDefinitionId;
                connectionOccurrence.Definition = dictionary5[connectionDefinitionId];
                connectionOccurrence.TargetOccurrence = dictionary4[connectionOccurrence.ToObjectOccurrenceId];
            }
            foreach (ObjectDefinition objectDefinition in dictionary2.Values)
            {
                if (objectDefinition.OutgoingConnections != null)
                {
                    foreach (ConnectionDefinition outgoingConnection in objectDefinition.OutgoingConnections)
                        outgoingConnection.SourceObject = objectDefinition;
                }
            }
            foreach (ObjectDefinition objectDefinition in dictionary2.Values)
            {
                string incomingConnectionIds = objectDefinition.IncomingConnectionIds;
                if (!string.IsNullOrEmpty(incomingConnectionIds))
                {
                    string str = incomingConnectionIds;
                    string[] separator = new string[1] { " " };
                    foreach (string index in str.Split(separator, StringSplitOptions.RemoveEmptyEntries))
                    {
                        ConnectionDefinition connectionDefinition = dictionary5[index];
                        connectionDefinition.TargetObject = objectDefinition;
                        objectDefinition.IncommingConnections.Add(connectionDefinition);
                    }
                }
            }
            foreach (ObjectOccurrence objectOccurrence1 in source)
            {
                if (objectOccurrence1.OutConnections != null)
                {
                    foreach (ConnectionOccurrence outConnection in objectOccurrence1.OutConnections)
                    {
                        ObjectOccurrence objectOccurrence2 = dictionary4[outConnection.ToObjectOccurrenceId];
                        objectOccurrence2.InConnections.Add(outConnection);
                        outConnection.SourceOccurrence = objectOccurrence1;
                        outConnection.TargetOccurrence = objectOccurrence2;
                    }
                }
            }
        }

        /// <summary>
        /// Removes all objects that have no connections or have connections to objects outside of the current model
        /// Feature is similar to the Reorganise functionality in ARIS Business Modeller
        /// Works only on the ARIS model loaded in memory!
        /// </summary>
        /// <param name="arisXml">ARIS Xml model that will be reorganised</param>
        /// <returns>List of reorgansation results.</returns>
        public static List<string> Reorganise(ref ArisXml arisXml)
        {
            List<string> stringList = new List<string>();
            IEnumerable<Group> groups = arisXml.Groups.Where<Group>((Func<Group, bool>)(g => g.ObjectDefinitions != null && g.ObjectDefinitions.Count > 0));
            int num1 = 0;
            int num2 = 0;
            int num3 = 0;
            int num4 = 0;
            List<ConnectionDefinition> connectionDefinitionList = new List<ConnectionDefinition>();
            foreach (Group group in groups)
            {
                List<ObjectDefinition> list1 = group.ObjectDefinitions.Where<ObjectDefinition>((Func<ObjectDefinition, bool>)(d => d.AllOccurrencies != null && d.AllOccurrencies.Count > 0)).ToList<ObjectDefinition>();
                num1 += group.ObjectDefinitions.Count;
                num2 += list1.Count;
                group.ObjectDefinitions = list1;
                foreach (ObjectDefinition objectDefinition in group.ObjectDefinitions)
                {
                    if (objectDefinition.IncommingConnections != null)
                        num3 += objectDefinition.IncommingConnections.Count;
                    if (objectDefinition.OutgoingConnections != null)
                        num3 += objectDefinition.OutgoingConnections.Count;
                    List<ConnectionOccurrence> connectionOccurrenceList1 = objectDefinition.AllOccurrencies.Where<ObjectOccurrence>((Func<ObjectOccurrence, bool>)(o => o.InConnections != null && o.InConnections.Count > 0)).Select<ObjectOccurrence, List<ConnectionOccurrence>>((Func<ObjectOccurrence, List<ConnectionOccurrence>>)(o => o.InConnections)).Aggregate<List<ConnectionOccurrence>, List<ConnectionOccurrence>>(new List<ConnectionOccurrence>(), (Func<List<ConnectionOccurrence>, List<ConnectionOccurrence>, List<ConnectionOccurrence>>)((r, o) =>
                    {
                        r.AddRange((IEnumerable<ConnectionOccurrence>)o);
                        return r;
                    }));
                    List<ConnectionOccurrence> connectionOccurrenceList2 = objectDefinition.AllOccurrencies.Where<ObjectOccurrence>((Func<ObjectOccurrence, bool>)(o => o.OutConnections != null && o.OutConnections.Count > 0)).Select<ObjectOccurrence, List<ConnectionOccurrence>>((Func<ObjectOccurrence, List<ConnectionOccurrence>>)(o => o.OutConnections)).Aggregate<List<ConnectionOccurrence>, List<ConnectionOccurrence>>(new List<ConnectionOccurrence>(), (Func<List<ConnectionOccurrence>, List<ConnectionOccurrence>, List<ConnectionOccurrence>>)((r, o) =>
                    {
                        r.AddRange((IEnumerable<ConnectionOccurrence>)o);
                        return r;
                    }));
                    HashSet<string> connDefsInUse = new HashSet<string>();
                    connectionOccurrenceList1.ForEach((Action<ConnectionOccurrence>)(c => connDefsInUse.Add(c.ConnectionDefinitionId)));
                    if (objectDefinition.IncommingConnections != null)
                    {
                        List<ConnectionDefinition> list2 = objectDefinition.IncommingConnections.Where<ConnectionDefinition>((Func<ConnectionDefinition, bool>)(c => connDefsInUse.Contains(c.Id))).ToList<ConnectionDefinition>();
                        connectionDefinitionList.AddRange((IEnumerable<ConnectionDefinition>)ArisXml.GetExtraElements<ConnectionDefinition>(list2, objectDefinition.IncommingConnections));
                        objectDefinition.IncommingConnections = list2;
                        num4 += objectDefinition.IncommingConnections.Count;
                    }
                    if (objectDefinition.OutgoingConnections != null)
                    {
                        connDefsInUse = new HashSet<string>();
                        connectionOccurrenceList2.ForEach((Action<ConnectionOccurrence>)(c => connDefsInUse.Add(c.ConnectionDefinitionId)));
                        List<ConnectionDefinition> list2 = objectDefinition.OutgoingConnections.Where<ConnectionDefinition>((Func<ConnectionDefinition, bool>)(c => connDefsInUse.Contains(c.Id))).ToList<ConnectionDefinition>();
                        connectionDefinitionList.AddRange((IEnumerable<ConnectionDefinition>)ArisXml.GetExtraElements<ConnectionDefinition>(list2, objectDefinition.OutgoingConnections.ToList<ConnectionDefinition>()));
                        objectDefinition.OutgoingConnections = list2.ToList<ConnectionDefinition>();
                        num4 += objectDefinition.OutgoingConnections.Count;
                    }
                }
            }
            stringList.Add("Reorganization result:");
            stringList.Add(string.Format("ObjectDefinitions: From {0} to {1}", (object)num1, (object)num2));
            stringList.Add(string.Format("ConnectionDefinitions: From {0} to {1}", (object)num3, (object)num4));
            return stringList;
        }

        /// <summary>
        /// Compares the content of two lists and returns the extra elements that the second one has compared to the first (etalon) one
        /// </summary>
        /// <typeparam name="T">Type of the items in the lists</typeparam>
        /// <param name="etalon">The list considered as an etalon for the comparison</param>
        /// <param name="list">The list to compare to the etalon</param>
        /// <returns>All extra elements from the second list that don't appear in the etalon</returns>
        private static List<T> GetExtraElements<T>(List<T> etalon, List<T> list)
        {
            List<T> objList = new List<T>();
            foreach (T obj in list)
            {
                if (!etalon.Contains(obj))
                    objList.Add(obj);
            }
            return objList;
        }

        public static ArisXml FromXml(string fileContent, bool doReorganize = true)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(ArisXml));
            TextReader file = new StringReader(fileContent);

            ArisXml arisXml = (ArisXml)xmlSerializer.Deserialize(file);
            arisXml.Compile();
            ArisXml.PrepareInnerObjects(arisXml);
            if (doReorganize)
                ArisXml.Reorganise(ref arisXml);

            return arisXml;
        }
    }
}
