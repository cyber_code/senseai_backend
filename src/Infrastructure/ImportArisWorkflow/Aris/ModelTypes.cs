﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImportAris.Aris
{
    public class Constants
    {
        public static class ModelTypes
        {
            //
            // Summary:
            //     Name of the 'Value Added Diagram/Chain' (VAD/VAC) model type
            public const string VadDiagram = "MT_VAL_ADD_CHN_DGM";
            //
            // Summary:
            //     Name of the 'EPC Diagram' model type
            public const string EpcDiagram = "MT_EEPC";
            //
            // Summary:
            //     Name of the 'Screen Design' model type
            public const string ScreenDesign = "MT_SCREEN_DES";
            //
            // Summary:
            //     Name of the 'Entity-Relation Diagram' model type
            public const string Eerm = "MT_EERM";
            //
            // Summary:
            //     Name of the 'Info Carrier Diagram' model type
            public const string InfoCarrierDiagram = "MT_INFO_CARR_DGM";
            //
            // Summary:
            //     Name of the 'Function Allocation Diagram' model type
            public const string FadDiagram = "MT_FUNC_ALLOC_DGM";
            //
            // Summary:
            //     Name of the 'Event Diagram' model type
            public const string EventDiagram = "MT_EV_DGM";
        }
    }
}
