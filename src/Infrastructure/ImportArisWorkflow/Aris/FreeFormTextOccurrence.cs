﻿using System;
using System.Linq;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    [XmlType("FFTextOcc")]
    public class FreeFormTextOccurrence
    {
        internal const string SymbolFlag_AddAttributeName = "ATTRNAME";

        [XmlAttribute("FFTextOcc.ID")]
        public string Id { get; set; }

        [XmlAttribute("FFTextDef.IdRef")]
        public string DefinitionId { get; set; }

        [XmlAttribute("SymbolFlag")]
        public string SymbolFlag { get; set; }

        [XmlAttribute("Alignment")]
        public string Alignment { get; set; }

        [XmlAttribute("Zorder")]
        public int ZOrder { get; set; }

        [XmlElement("Position")]
        public Position Position { get; set; }

        [XmlElement("Size")]
        public Size Size { get; set; }

        [XmlIgnore]
        public FreeFormTextDefinition Definition { get; set; }

        [XmlIgnore]
        public string Text
        {
            get
            {
                if (!(this.Definition.IsBoundToModelAttribute == "MODELATTR") || string.IsNullOrEmpty(this.Definition.TargetModelAttribute))
                    return this.Definition.Text;
                string targetAtt = this.Definition.TargetModelAttribute;
                return this.Model.AttributeDefinitions.Where<AttributeDefinition>((Func<AttributeDefinition, bool>)(att => att.Type == targetAtt)).Select<AttributeDefinition, string>((Func<AttributeDefinition, string>)(att => att.Value.FormattedText)).FirstOrDefault<string>();
            }
        }

        [XmlIgnore]
        public bool PrependTargetAttributeLabel
        {
            get
            {
                return this.SymbolFlag == "ATTRNAME";
            }
        }

        [XmlIgnore]
        public Model Model { get; internal set; }

        public override string ToString()
        {
            return "FFTextOcc: " + this.Text;
        }
    }
}