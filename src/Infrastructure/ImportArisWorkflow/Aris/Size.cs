﻿using System.Xml.Serialization;

namespace ImportAris.Aris
{
    public class Size
    {
        [XmlAttribute(AttributeName = "Size.dX")]
        public int Width { get; set; }

        [XmlAttribute(AttributeName = "Size.dY")]
        public int Height { get; set; }
    }
}