﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ImportAris.Aris
{
    [XmlType("CxnOcc")]
    public class ConnectionOccurrence
    {
        [XmlAttribute(AttributeName = "CxnOcc.ID")]
        public string Id { get; set; }
        
        [XmlAttribute(AttributeName = "CxnDef.IdRef")]
        public string ConnectionDefinitionId { get; set; }
        
        [XmlAttribute(AttributeName = "ToObjOcc.IdRef")]
        public string ToObjectOccurrenceId { get; set; }
        
        [XmlElement(ElementName = "Position")]
        public List<Position> Positions { get; set; }
        
        [XmlIgnore]
        public ConnectionDefinition Definition { get; set; }
        
        [XmlIgnore]
        public ObjectOccurrence TargetOccurrence { get; set; }
        
        [XmlIgnore]
        public string Type
        {
            get
            {
                return this.Definition.Type;
            }
        }

        [XmlIgnore]
        public string TargetObjectDefinitionId
        {
            get
            {
                return this.Definition.TargetObjectDefintionId;
            }
        }
        
        [XmlIgnore]
        public List<AttributeDefinition> AttributeDefinitions {
            get
            {
                return this.Definition != null ? this.Definition.AttributeDefinitions : new List<AttributeDefinition>();
            }
        }
        
        [XmlIgnore]
        public string SourceOccurenceId { get; set; }
        
        [XmlIgnore]
        public ObjectOccurrence SourceOccurrence { get; set; }
    }
}