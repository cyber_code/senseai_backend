﻿using System.Xml.Serialization;

namespace ImportAris.Aris
{
    public sealed class SizeElement
    {
        [XmlAttribute(AttributeName = "Value")]
        public string Value { get; set; }

        [XmlElement(ElementName = "StyledElement")]
        public StyledElement StyledElement { get; set; }
    }
}
