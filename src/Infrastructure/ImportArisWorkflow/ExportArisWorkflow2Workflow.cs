﻿using SenseAI.Domain;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.Process.ArisWorkflowModel;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImportAris
{
    public sealed class ExportArisWorkflow2Workflow
    {
        private ArisWorkflow _arisWorkflow;
        private Guid _folderId;
        private Guid _sysmteId;
        private Guid _catalogId;
        private string _catalogTitle;
        private List<Typical> _typicls;

        public ExportArisWorkflow2Workflow(ArisWorkflow arisWorkflow, Guid folderId, Guid systemId, Guid catalogId, string catalogTitle,
            List<Typical> typicls)
        {
            _arisWorkflow = arisWorkflow;
            _folderId = folderId;
            _sysmteId = systemId;
            _catalogId = catalogId;
            _catalogTitle = catalogTitle;
            _typicls = typicls;
        }

        public Workflow Export(bool newId = false)
        {
            Guid fromId = Guid.NewGuid();
            Guid wfId = _arisWorkflow.Id;
            if (newId)
                wfId = Guid.NewGuid();

            Workflow workflow = new Workflow(wfId, _arisWorkflow.Title, _arisWorkflow.Title, _folderId, true, false);
            workflow.AddStartItem(fromId, _sysmteId, "Start", "Start", _catalogId, "", "", Guid.Empty, "", "", 0, "");

            var startItems = _arisWorkflow.Items.Where(w => _arisWorkflow.ItemLinks.FirstOrDefault(w1 => w1.ToId == w.Id) == null &&
                     (w.Type != ArisWorkflowType.Role && w.Type != ArisWorkflowType.SystemType));

            foreach (var arisWorkflowItem in startItems)
            {
                GenerateRecursively(fromId, arisWorkflowItem, workflow);
            }

            return workflow;
        }

        private void GenerateRecursively(Guid fromId, ArisWorkflowItem arisWorkflowItem, Workflow workflow)
        {
            if (arisWorkflowItem == null)
                return;
            switch (arisWorkflowItem.Type)
            {
                case ArisWorkflowType.Event:
                    CreateEventItem(fromId, arisWorkflowItem, workflow);
                    break;

                case ArisWorkflowType.Activity:
                    CreateActionItem(fromId, arisWorkflowItem, workflow);
                    break;

                case ArisWorkflowType.Conditional:
                    CreateConditionalItem(fromId, arisWorkflowItem, workflow);
                    break;

                case ArisWorkflowType.Process:
                    CreateProcessItem(fromId, arisWorkflowItem, workflow);
                    break;

                default:
                    DefaultItem(fromId, arisWorkflowItem, workflow);
                    break;
            }
        }

        private void CreateEventItem(Guid fromId, ArisWorkflowItem eventArisWorkflowItem, Workflow workflow)
        {
            if (workflow.Items.FirstOrDefault(w => w.Id == eventArisWorkflowItem.Id) == null)
                workflow.AddManualItem(eventArisWorkflowItem.Id, eventArisWorkflowItem.Title, eventArisWorkflowItem.Description, Guid.Empty, "", Guid.Empty, "", "", 2, "", "");
            if (workflow.ItemLinks.FirstOrDefault(w => w.FromId == fromId && w.ToId == eventArisWorkflowItem.Id) == null)
                workflow.AddItemLink(fromId, eventArisWorkflowItem.Id, WorkflowItemLinkState.Yes);
            else return;

            var eventLinks = _arisWorkflow.ItemLinks.Where(w => w.FromId == eventArisWorkflowItem.Id);
            if (eventLinks.Count() == 0)
                return;
            foreach (var eventLink in eventLinks)
            {
                ArisWorkflowItem eventItem = _arisWorkflow.Items.FirstOrDefault(w => w.Id == eventLink.ToId);
                GenerateRecursively(eventArisWorkflowItem.Id, eventItem, workflow);
            }
        }

        private void CreateActionItem(Guid fromId, ArisWorkflowItem actionArisWorkflowItem, Workflow workflow)
        {
            var actionLinks = _arisWorkflow.ItemLinks.Where(w => w.FromId == actionArisWorkflowItem.Id);
            var actionItems = _arisWorkflow.Items.Where(w => actionLinks.FirstOrDefault(w1 => w1.ToId == w.Id) != null);
            if (actionLinks.Count() == 0 && actionItems.Count() == 0)
                return;

            if (actionItems.Count(c => c.Type == ArisWorkflowType.T24Screen) == 0)
            {
                var roleLinks = _arisWorkflow.ItemLinks.Where(w => w.ToId == actionArisWorkflowItem.Id);
                var roleItems = _arisWorkflow.Items.Where(w => roleLinks.Select(s => s.FromId).Contains(w.Id) && w.Type == ArisWorkflowType.Role);

                string parameter = string.Join(",", roleItems.Where(w => w.Type == ArisWorkflowType.Role).Select(s => s.Title).ToArray());
                if (workflow.Items.FirstOrDefault(w => w.Id == actionArisWorkflowItem.Id) == null)
                    workflow.AddManualItem(actionArisWorkflowItem.Id, actionArisWorkflowItem.Title, actionArisWorkflowItem.Description, Guid.Empty, "", Guid.Empty, "", "", 2, parameter, parameter);
                if (workflow.ItemLinks.FirstOrDefault(w => w.FromId == fromId && w.ToId == actionArisWorkflowItem.Id) == null)
                    workflow.AddItemLink(fromId, actionArisWorkflowItem.Id, WorkflowItemLinkState.Yes);
                else return;

                if (actionItems.Count(c => c.Type != ArisWorkflowType.T24Screen) == 0)
                    return;

                foreach (var actionLink in actionLinks)
                {
                    ArisWorkflowItem actionItem = _arisWorkflow.Items.FirstOrDefault(w => w.Id == actionLink.ToId);
                    GenerateRecursively(actionArisWorkflowItem.Id, actionItem, workflow);
                }
                return;
            }

            foreach (var actionItem in actionItems.Where(w => w.Type == ArisWorkflowType.T24Screen))
            {
                var action = ActionType.Input;
                if (actionItem.Title.Contains("Check") || actionItem.Title.Contains("Validate") || actionItem.Title.Contains("Confirm"))
                    action = ActionType.See;
                if (actionItem.Title.Contains("Approval") || actionItem.Title.Contains("Authorize"))
                    action = ActionType.Authorize;
                Guid typicalId = Guid.Empty;
                string typicalType = "0";
                if (_typicls.Any() && _typicls.Where(w => w.Title == actionItem.TypicalName).Any())
                {
                    var typical = _typicls.FirstOrDefault(w => w.Title == actionItem.TypicalName);
                    typicalId = typical.Id;
                    typicalType = typical.Type.ToString();
                }

                if (workflow.Items.FirstOrDefault(w => w.Id == actionItem.Id) == null)
                    workflow.AddActionItem(actionItem.Id, actionArisWorkflowItem.Title, actionArisWorkflowItem.Description, _catalogId, _catalogTitle, typicalId, actionItem.TypicalName, typicalType, (int)action, "", null, null, null, false, "");
                if (workflow.ItemLinks.FirstOrDefault(w => w.FromId == fromId && w.ToId == actionItem.Id) == null)
                    workflow.AddItemLink(fromId, actionItem.Id, WorkflowItemLinkState.Yes);
                else return;
                fromId = actionItem.Id;
            }
            if (actionItems.Count(c => c.Type != ArisWorkflowType.T24Screen) == 0)
                return;
            foreach (var actionLink in actionLinks)
            {
                ArisWorkflowItem actionItem = _arisWorkflow.Items.FirstOrDefault(w => w.Id == actionLink.ToId);
                if (actionItem.Type == ArisWorkflowType.T24Screen)
                    continue;
                GenerateRecursively(fromId, actionItem, workflow);
            }
        }

        private void CreateConditionalItem(Guid fromId, ArisWorkflowItem conditionaArisWorkflowItem, Workflow workflow)
        {
            if (workflow.Items.FirstOrDefault(w => w.Id == conditionaArisWorkflowItem.Id) == null)
                workflow.AddConditionItem(conditionaArisWorkflowItem.Id, "Conditional", "Conditional", _catalogId, "", Guid.Empty, "", "", 0, "", null);
            if (workflow.ItemLinks.FirstOrDefault(w => w.FromId == fromId && w.ToId == conditionaArisWorkflowItem.Id) == null)
                workflow.AddItemLink(fromId, conditionaArisWorkflowItem.Id, WorkflowItemLinkState.Yes);
            else return;

            var conditionalLinks = _arisWorkflow.ItemLinks.Where(w => w.FromId == conditionaArisWorkflowItem.Id);
            if (conditionalLinks.Count() == 0)
                return;
            foreach (var conditionalLink in conditionalLinks)
            {
                ArisWorkflowItem conditionalItem = _arisWorkflow.Items.FirstOrDefault(w => w.Id == conditionalLink.ToId);
                GenerateRecursively(conditionaArisWorkflowItem.Id, conditionalItem, workflow);
            }
        }

        private void CreateProcessItem(Guid fromId, ArisWorkflowItem processArisWorkflowItem, Workflow workflow)
        {
            Guid newFromId = fromId;
            if (processArisWorkflowItem.ArisWorkflowLinkId != Guid.Empty)
            {
                if (workflow.Items.FirstOrDefault(w => w.Id == processArisWorkflowItem.Id) == null)
                    workflow.AddLinkedItem(processArisWorkflowItem.Id, processArisWorkflowItem.Title, processArisWorkflowItem.Description, _catalogId, "", Guid.Empty, "", "", 0, "", processArisWorkflowItem.ArisWorkflowLinkId, 0);
                if (workflow.ItemLinks.FirstOrDefault(w => w.FromId == fromId && w.ToId == processArisWorkflowItem.Id) == null)
                    workflow.AddItemLink(fromId, processArisWorkflowItem.Id, WorkflowItemLinkState.Yes);
                else return;
                newFromId = processArisWorkflowItem.Id;
            }

            var processLinks = _arisWorkflow.ItemLinks.Where(w => w.FromId == processArisWorkflowItem.Id);
            if (processLinks.Count() == 0)
                return;
            foreach (var processLink in processLinks)
            {
                ArisWorkflowItem processItem = _arisWorkflow.Items.FirstOrDefault(w => w.Id == processLink.ToId);
                GenerateRecursively(newFromId, processItem, workflow);
            }
        }

        private void DefaultItem(Guid fromId, ArisWorkflowItem arisWorkflowItem, Workflow workflow)
        {
            var defaultLinks = _arisWorkflow.ItemLinks.Where(w => w.FromId == arisWorkflowItem.Id);
            if (defaultLinks.Count() == 0)
                return;
            foreach (var defaultLink in defaultLinks)
            {
                ArisWorkflowItem defaulttItem = _arisWorkflow.Items.FirstOrDefault(w => w.Id == defaultLink.ToId);
                GenerateRecursively(fromId, defaulttItem, workflow);
            }
        }
    }
}