﻿using SenseAI.Domain.Adapters;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain.Process.ProcessModel;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DocumentGeneration
{
    public class DocumentGenerations : IDocumentGenerations
    {
        private string _path = "";
        private string _apiURL = "";
        private string _uiURL = "";
        private string _projectTitle;
        private string _subProjectTitle;
        private string _hierarchyPath;
        private string _hierarchyPathLastPage;
        private Dictionary<Hierarchy, List<Hierarchy>> _hierarchies;
        private Dictionary<Process, List<Workflow>> _processAndWorkflows;
        private Dictionary<Guid, List<WorkflowTestCases>> _workflowTestCases;
        private Dictionary<Guid, List<WorkflowVersionPaths>> _workflowPaths;
        private Dictionary<DataSet, int> _dataSets;
        private Dictionary<Guid, string> _processImages;        
        private Dictionary<Guid, string> _workflowVideos;
        private Dictionary<Guid, int> _processResources;
        private List<AdapterSteps> _adapterSteps;
        private char[] notAllowedChars = { '*', '.', '"', ' ', '/', '\\', '[', ']', ':', ';', '|', ',' };


        public bool Generate(string projectTitle, string subProjectTitle, string path, string apiURL, string uiURL, Dictionary<Hierarchy, List<Hierarchy>> hierarchies,
            Dictionary<Process, List<Workflow>> processAndWorkflows, Dictionary<Guid, List<WorkflowVersionPaths>> workflowPaths, 
            Dictionary<Guid, List<WorkflowTestCases>> workflowTestCases, Dictionary<DataSet, int> dataSets, Dictionary<Guid, string> processImages, 
            List<AdapterSteps> adapterSteps, Dictionary<Guid, string> workflowVideos, Dictionary<Guid, int> processResources)
        {
            if (hierarchies == null || hierarchies.Keys.Count == 0)
                throw new Exception("No hierarchy is selected.");

            _processAndWorkflows = processAndWorkflows;
            _projectTitle = projectTitle;
            _projectTitle = projectTitle;
            _path = path;
            _apiURL = apiURL;
            _uiURL = uiURL;
            _subProjectTitle = subProjectTitle;
            _workflowTestCases = workflowTestCases;
            _hierarchies = hierarchies;
            _workflowPaths = workflowPaths;
            _dataSets = dataSets;
            _processImages = processImages;
            _adapterSteps = adapterSteps;
            _workflowVideos = workflowVideos;
            _processResources = processResources;

            Hierarchy firstHierarchy = hierarchies.Keys.FirstOrDefault(w => w.ParentId == Guid.Empty);
            string firstPath = _path + "\\" + new string(firstHierarchy.Title.Select(s => notAllowedChars.Contains(s) ? '_' : s).ToArray()) + "\\";
            Directory.CreateDirectory(firstPath);
            _hierarchyPath = firstHierarchy.Title + " / ";
            _hierarchyPathLastPage = firstHierarchy.HierarchyTypeTitle + ": " + firstHierarchy.Title + " / ";

            if (_hierarchies.FirstOrDefault(w1 => w1.Key.Id == firstHierarchy.Id).Value == null || (_hierarchies.FirstOrDefault(w1 => w1.Key.Id == firstHierarchy.Id).Value != null && _hierarchies.FirstOrDefault(w1 => w1.Key.Id == firstHierarchy.Id).Value.Count() == 0))
            {
                StartGeneration(firstHierarchy, firstPath);
                return true;
            }

            foreach (var hierarchy in hierarchies.FirstOrDefault(w => w.Key.Id == firstHierarchy.Id).Value)
            {
                AttachFile(hierarchy, firstPath + new string(hierarchy.Title.Select(s => notAllowedChars.Contains(s) ? '_' : s).ToArray()) + "\\");
            }

            return true;
        }

        private void AttachFile(Hierarchy newHierarchy, string path2Generate)
        {
            Directory.CreateDirectory(path2Generate);
            _hierarchyPath += newHierarchy.Title + " / ";
            _hierarchyPathLastPage += newHierarchy.HierarchyTypeTitle + ": " + newHierarchy.Title + " / ";
            if (_hierarchies.FirstOrDefault(w1 => w1.Key.Id == newHierarchy.Id).Value != null && _hierarchies.FirstOrDefault(w1 => w1.Key.Id == newHierarchy.Id).Value.Count() == 0)
            {
                StartGeneration(newHierarchy, path2Generate);
                return;
            }
            if (_hierarchies.FirstOrDefault(w => w.Key.Id == newHierarchy.Id).Value == null)
            {
                StartGeneration(newHierarchy, path2Generate);
                return;
            }

            foreach (var hierarchy in _hierarchies.FirstOrDefault(w => w.Key.Id == newHierarchy.Id).Value)
            {
                AttachFile(hierarchy, path2Generate + new string(hierarchy.Title.Select(s => notAllowedChars.Contains(s) ? '_' : s).ToArray()) + "\\");
            }
        }

        private void StartGeneration(Hierarchy hierarchy, string path)
        {
            var processes = _processAndWorkflows.Keys.Where(w => w.HierarchyId == hierarchy.Id);
            foreach (var process in processes)
            {
                var workflows = _processAndWorkflows.FirstOrDefault(w => w.Key.Id == process.Id).Value;
                var workflowIds = workflows.Select(s => s.Id).ToList();
                var workflowTestCases = _workflowTestCases.Where(w => workflowIds.Contains(w.Key)).ToDictionary(d => d.Key, d => d.Value);
                var workflowVideos = _workflowVideos.Where(w => workflowIds.Contains(w.Key)).ToDictionary(d => d.Key, d => d.Value);
                var workflowPaths = _workflowPaths.Where(w => workflowIds.Contains(w.Key)).ToDictionary(d => d.Key, d => d.Value);
                ProcessDocument processDocument = new ProcessDocument(_projectTitle, _subProjectTitle, workflows, _adapterSteps, workflowTestCases, workflowPaths, _dataSets, workflowVideos);
                processDocument.SingleProcess(process, path, _apiURL, _uiURL, _hierarchyPath, _hierarchyPathLastPage, _processImages.FirstOrDefault(w => w.Key == process.Id).Value, _processResources.FirstOrDefault(w => w.Key == process.Id).Value);
            }
        }

    }
}
