﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Rectangle = iTextSharp.text.Rectangle;
using iTextSharp.text.pdf.draw;

namespace DocumentGeneration
{
    public class ITextEvents : PdfPageEventHelper
    {
        PdfContentByte cb;

        // we will put the final number of pages in a template  
        PdfTemplate footerTemplate;

        // this is the BaseFont we are going to use for the header / footer  
        BaseFont bf = null;

        // This keeps track of the creation time  
        DateTime PrintTime = DateTime.Now;

        #region Fields  
        private string _header;

        #endregion

        #region Properties  
        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }

        protected int counter = 0;
        protected List<PageIndex> toc = new List<PageIndex>();
        public bool IsLastPage { get; set; }
        #endregion

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                footerTemplate = cb.CreateTemplate(50, 50); 
            }
            catch (DocumentException de)
            {
            }
            catch (System.IO.IOException ioe)
            {
            }
        }

        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            base.OnEndPage(writer, document);
            PdfPTable pdfPTable = new PdfPTable(2);
            PdfPTable table = new PdfPTable(1);
            table.TotalWidth = document.PageSize.Width;

            var cellFont1 = FontFactory.GetFont("Calibri", 14, iTextSharp.text.Font.NORMAL, new BaseColor(68, 114, 196));

            if (writer.PageNumber == 1)
            {
                PdfPCell cell = new PdfPCell(new Phrase("Sense AI", cellFont1));
                cell.Border = 0;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(DateTime.Now.ToString("yyy.MM.dd"), cellFont1));
                cell.Border = 0;
                table.AddCell(cell);
                table.WriteSelectedRows(0, -1, document.Left, document.Bottom + 50, writer.DirectContent);
            }
            else if (!IsLastPage && writer.PageNumber > 2)
            {
                PdfPCell cell = new PdfPCell(new Phrase(writer.PageNumber.ToString(), cellFont1));
                cell.BorderWidth = 0;
                cell.PaddingRight = 20;
                cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                table.AddCell(cell);
                table.WriteSelectedRows(-50, (int)document.PageSize.Width + 50, 0, document.Bottom + 10, writer.DirectContent);
            }
        }

        public override void OnGenericTag(PdfWriter writer, Document document, Rectangle rect, string text)
        {
            String name = "dest" + (counter++);
            int page = writer.PageNumber;
            PageIndex pageIndex = new PageIndex() { Text = text, Name = name, Page = page };
            if (toc.Find(w => w.Text == pageIndex.Text) == null)
                toc.Add(pageIndex);
            writer.DirectContent.LocalDestination(name, new PdfDestination(PdfDestination.FITH, rect.GetTop(0)));
        }

        public List<PageIndex> getTOC()
        {
            return toc;
        }
    }


    public class PageIndex
    {
        public string Text { get; set; }
        public string Name { get; set; }
        public int Page { get; set; }
    }
}
