﻿////using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
////using A = DocumentFormat.OpenXml.Drawing;
////using PIC = DocumentFormat.OpenXml.Drawing;
////using DocumentFormat.OpenXml.Office.Drawing;
////using DocumentFormat.OpenXml;
////using System;
////using Wps = DocumentFormat.OpenXml.Office2010.Word.DrawingShape;
////using System.Linq;
////using DocumentFormat.OpenXml.Packaging;
////using System.Collections.Generic;

//namespace DocumentGeneration
//{
//    public class Functions
//    //{
//    //    public static void AddImageToBody(DocumentFormat.OpenXml.Wordprocessing.Body wordDoc, string relationshipId)
//    //    {
//    //        // Define the reference of the image.
//    //        var element =
//    //             new Drawing(
//    //                 new DW.Inline(
//    //                     new DW.Extent() { Cx = 990000L, Cy = 792000L },
//    //                     new DW.EffectExtent()
//    //                     {
//    //                         LeftEdge = 0L,
//    //                         TopEdge = 0L,
//    //                         RightEdge = 0L,
//    //                         BottomEdge = 0L
//    //                     },
//    //                     new DW.DocProperties()
//    //                     {
//    //                         Id = (UInt32Value)1U,
//    //                         Name = "Picture 1"
//    //                     },
//    //                     new DW.NonVisualGraphicFrameDrawingProperties(
//    //                         new A.GraphicFrameLocks() { NoChangeAspect = true }),
//    //                     new A.Graphic(
//    //                         new A.GraphicData(
//    //                             new PIC.Picture(
//    //                                 new PIC.NonVisualPictureProperties(
//    //                                     new PIC.NonVisualDrawingProperties()
//    //                                     {
//    //                                         Id = (UInt32Value)0U,
//    //                                         Name = "New Bitmap Image.jpg"
//    //                                     },
//    //                                     new PIC.NonVisualPictureDrawingProperties()),
//    //                                 new PIC.BlipFill(
//    //                                     new A.Blip(
//    //                                         new A.BlipExtensionList(
//    //                                             new A.BlipExtension()
//    //                                             {
//    //                                                 Uri =
//    //                                                    "{28A0092B-C50C-407E-A947-70E740481C1C}"
//    //                                             })
//    //                                     )
//    //                                     {
//    //                                         Embed = relationshipId,
//    //                                         CompressionState =
//    //                                         A.BlipCompressionValues.Print
//    //                                     },
//    //                                     new A.Stretch(
//    //                                         new A.FillRectangle())),
//    //                                 new PIC.ShapeProperties(
//    //                                     new A.Transform2D(
//    //                                         new A.Offset() { X = 0L, Y = 0L },
//    //                                         new A.Extents() { Cx = 990000L, Cy = 792000L }),
//    //                                     new A.PresetGeometry(
//    //                                         new A.AdjustValueList()
//    //                                     )
//    //                                     { Preset = A.ShapeTypeValues.Rectangle }))
//    //                         )
//    //                         { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
//    //                 )
//    //                 {
//    //                     DistanceFromTop = (UInt32Value)0U,
//    //                     DistanceFromBottom = (UInt32Value)0U,
//    //                     DistanceFromLeft = (UInt32Value)0U,
//    //                     DistanceFromRight = (UInt32Value)0U,
//    //                     EditId = "50D07946"
//    //                 });

//    //        // Append the reference to body, the element should be in a Run.
//    //        wordDoc.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(element)));
//    //    }
//    //    public static Drawing AddImageToRun(string imageFileName,  string imageID)
//    //    {
//    //        try
//    //        {
//    //            string GraphicDataUri = "http://schemas.openxmlformats.org/drawingml/2006/picture";

//    //            var element =
//    //                 new Drawing(
//    //                     new DW.Inline(
//    //                         new DW.Extent() { Cx = 990000L, Cy = 792000L },

//    //                         new DW.EffectExtent()
//    //                         {
//    //                             LeftEdge = 0L,
//    //                             TopEdge = 0L,
//    //                             RightEdge = 0L,
//    //                             BottomEdge = 0L
//    //                         },
//    //                         new DW.DocProperties()
//    //                         {
//    //                             Id = (UInt32Value)1U,
//    //                             Name = imageFileName,
//    //                             Description = imageFileName
//    //                         },
//    //                         new DW.NonVisualGraphicFrameDrawingProperties(
//    //                             new A.GraphicFrameLocks() { NoChangeAspect = true }),

//    //                         new A.Graphic(
//    //                             new A.GraphicData(
//    //                                 new PIC.Picture(

//    //                                     new PIC.NonVisualPictureProperties(
//    //                                         new PIC.NonVisualDrawingProperties()
//    //                                         {
//    //                                             Id = (UInt32Value)0U,

//    //                                             Name = imageFileName
//    //                                         },
//    //                                         new PIC.NonVisualPictureDrawingProperties()),

//    //                                     new PIC.BlipFill(
//    //                                         new A.Blip()
//    //                                         {
//    //                                             Embed = imageID
//    //                                         },
//    //                                         new A.Stretch(
//    //                                             new A.FillRectangle())),
//    //                                     new PIC.ShapeProperties(
//    //                                         new A.Transform2D(
//    //                                             new A.Offset() { X = 0L, Y = 0L },

//    //                                             new A.Extents()
//    //                                             {
//    //                                                 Cx = 990000L,
//    //                                                 Cy = 792000L
//    //                                             }),

//    //                                         new A.PresetGeometry(
//    //                                             new A.AdjustValueList()
//    //                                         )
//    //                                         { Preset = A.ShapeTypeValues.Rectangle }))
//    //                             )
//    //                             { Uri = GraphicDataUri })
//    //                     )
//    //                     {
//    //                         DistanceFromTop = (UInt32Value)0U,
//    //                         DistanceFromBottom = (UInt32Value)0U,
//    //                         DistanceFromLeft = (UInt32Value)0U,
//    //                         DistanceFromRight = (UInt32Value)0U,
//    //                     });

//    //            // Append the reference to body, the element should be in a Run.
//    //            return element;
//    //        }
//    //        catch (Exception ex)
//    //        {
//    //            throw;
//    //        }
//    //        return null;
//    //    }

//    //    public static void AddImage(string relationshipId, MainDocumentPart mainDocumentPart)
//    //    {
//    //        IEnumerable<DocumentFormat.OpenXml.Office2010.Word.DrawingShape.WordprocessingShape> shapes2 =
//    //                mainDocumentPart.Document.Descendants<DocumentFormat.OpenXml.Office2010.Word.DrawingShape.WordprocessingShape>();


//    //        foreach (DocumentFormat.OpenXml.Office2010.Word.DrawingShape.WordprocessingShape sp in shapes2)
//    //        {
//    //            A.BlipFill blipFill = new A.BlipFill() { Dpi = (UInt32Value)0U, RotateWithShape = true };
//    //            A.Blip blip1 = new A.Blip() { Embed = relationshipId };

//    //            A.Stretch stretch1 = new A.Stretch();
//    //            A.FillRectangle fillRectangle1 = new A.FillRectangle()
//    //            { Left = 10000, Top = 10000, Right = 10000, Bottom = 10000 };
//    //            Wps.WordprocessingShape wordprocessingShape1 = new Wps.WordprocessingShape();

//    //            stretch1.Append(fillRectangle1);
//    //            blipFill.Append(blip1);
//    //            blipFill.Append(stretch1);
//    //            Wps.ShapeProperties shapeProperties1 = sp.Descendants<Wps.ShapeProperties>().First();
//    //            shapeProperties1.Append(blipFill);

//    //        }
//    //    }
//    }
//}
