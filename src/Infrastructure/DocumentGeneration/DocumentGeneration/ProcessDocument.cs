﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SenseAI.Domain;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain.Process.ProcessModel;
using System.IO;
using Newtonsoft.Json;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.Adapters;
using iTextSharp.text.pdf.draw;

namespace DocumentGeneration
{
    public sealed class ProcessDocument
    {
        private readonly string _projectTitle;
        private readonly string _subProjectTitle;
        private readonly List<Workflow> _workflows;
        private readonly Dictionary<Guid, List<WorkflowTestCases>> _workflowTestCases;
        private readonly Dictionary<Guid, List<WorkflowVersionPaths>> _workflowPaths;
        private readonly Dictionary<DataSet, int> _dataSets;
        private readonly List<AdapterSteps> _adapterSteps;
        private Dictionary<Guid, string> _workflowVideos;
        private int _processResources;

        private string _path = "";
        private string _apiURL = "";
        private string _uiURL = "";
        private Process _process;
        private string _hierarchyPath = "";
        private string _hierarchyPathLastPage = "";
        private string _processIamge = "";
        private int _section = 3;
        private int wfSection = 5;
        private int wfEndSection = 5;
        private int totalRuns = 0;
        PdfWriter pdfWriter;

        private ITextEvents _events;
        private List<PageIndex> _entries;
        private char[] notAllowedChars = { '*', '.', '"', ' ', '/', '\\', '[', ']', ':', ';', '|', ',' };

        /*Fonts*/
        private readonly Font fontHeading1 = FontFactory.GetFont("Calibri Light", 16, Font.NORMAL, new BaseColor(47, 84, 150));
        private readonly Font fontHeading2 = FontFactory.GetFont("Calibri Light", 13, Font.NORMAL, new BaseColor(47, 84, 150));
        private readonly Font fontTableContent = FontFactory.GetFont("Calibri", 11, Font.NORMAL, new BaseColor(0, 0, 0));
        private readonly Font fontDescription = FontFactory.GetFont("Calibri", 11, Font.NORMAL, new BaseColor(0, 0, 0));

        /*Spaces*/
        private readonly float cellPadding = 5;
        private readonly float speaceHeading1 = 20;
        private readonly float speaceHeading2 = 20;

        public ProcessDocument(string projectTitle, string subProjectTitle, List<Workflow> workflows, List<AdapterSteps> adapterSteps,
                Dictionary<Guid, List<WorkflowTestCases>> workflowTestCases, Dictionary<Guid, List<WorkflowVersionPaths>> workflowPaths,
                Dictionary<DataSet, int> dataSets, Dictionary<Guid, string> workflowVideos)
        {
            _projectTitle = projectTitle;
            _subProjectTitle = subProjectTitle;
            _workflows = workflows;
            _adapterSteps = adapterSteps;
            _workflowTestCases = workflowTestCases;
            _workflowPaths = workflowPaths;
            _dataSets = dataSets;
            _workflowVideos = workflowVideos;
        }

        public bool SingleProcess(Process process, string path, string apiURL, string uiURL, string hierarchyPath, string hierarchyPathLastPage, string processIamge, int processResources)
        {
            _process = process;
            _path = path;
            _apiURL = apiURL;
            _uiURL = uiURL;
            _hierarchyPath = hierarchyPath;
            _hierarchyPathLastPage = hierarchyPathLastPage;
            _processIamge = processIamge;
            _processResources = processResources;
            GenerateDocument();
            return false;
        }

        private void GenerateDocument()
        {
            string title = new string(_process.Title.Select(s => notAllowedChars.Contains(s) ? '_' : s).ToArray());
            string source = $"{_path}{ title } { "1.pdf"}";
            string target = $"{_path}{ title } { ".pdf"}";


            Document document = new Document(PageSize.A4, 30, 30, 30, 30);

            pdfWriter = PdfWriter.GetInstance(document, new FileStream(source, System.IO.FileMode.Create));
            _events = new ITextEvents();
            pdfWriter.PageEvent = _events;
            document.Open();

            FirstPage(document);
            document.NewPage();
            document.Add(new Paragraph("\n ", fontHeading1));
            document.NewPage();
            AddAdapteres(document);
            document.NewPage();
            AddRequirement(document);
            document.NewPage();
            AddProcessDiagram(document);
            document.NewPage();
            AddWorkflows(document);
            document.NewPage();
            AddWorkflow(document);
            document.NewPage();
            AddLastPage(document);
            _entries = _events.getTOC();
            document.Close();
            CopyDocument(source, target);
        }

        private void FirstPage(Document document)
        {
            try
            {

                var getFont1 = FontFactory.GetFont("Calibri", 12, Font.NORMAL, new BaseColor(47, 84, 150));
                var getFont3 = FontFactory.GetFont("Calibri", 12, Font.NORMAL, new BaseColor(0, 0, 0));
                var getFont2 = FontFactory.GetFont("Calibri Light", 44, Font.NORMAL, new BaseColor(68, 114, 196));

                Chunk chTitle = new Chunk(_process.Title, getFont2);
                Chunk chHierarchyPath = new Chunk(_hierarchyPath, getFont1);

                float[] widths = new float[] { 100f };
                PdfPTable table = new PdfPTable(1);
                table.WidthPercentage = 100;
                table.SetWidths(widths);

                Paragraph project = new Paragraph();
                project.Add(new Chunk("Project: ", getFont3));
                project.Add(new Chunk(_projectTitle, getFont1));
                PdfPCell cell1 = new PdfPCell(project);
                cell1.Border = 0;
                cell1.PaddingLeft = cellPadding;
                cell1.BorderColorLeft = new BaseColor(68, 114, 196);
                cell1.BorderWidthLeft = 2f;
                cell1.MinimumHeight = 30;
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table.AddCell(cell1);

                Paragraph subProject = new Paragraph();
                subProject.Add(new Chunk("Subproject: ", getFont3));
                subProject.Add(new Chunk(_subProjectTitle, getFont1));
                cell1 = new PdfPCell(subProject);
                cell1.Border = 0;
                cell1.PaddingLeft = cellPadding;
                cell1.BorderColorLeft = new BaseColor(68, 114, 196);
                cell1.BorderWidthLeft = 2f;
                cell1.MinimumHeight = 30;
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table.AddCell(cell1);


                cell1 = new PdfPCell(new Phrase(chTitle));
                cell1.Border = 0;
                cell1.BorderColorLeft = new BaseColor(68, 114, 196);
                cell1.BorderWidthLeft = 2f;
                cell1.PaddingLeft = cellPadding;
                cell1.MinimumHeight = 30;
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table.AddCell(cell1);

                cell1 = new PdfPCell(new Phrase(chHierarchyPath));
                cell1.Border = 0;
                cell1.BorderColorLeft = new BaseColor(68, 114, 196);
                cell1.BorderWidthLeft = 2f;
                cell1.PaddingLeft = cellPadding;
                cell1.MinimumHeight = 30;
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table.AddCell(cell1);

                document.Add(new Paragraph("\n \n \n \n \n \n "));
                document.Add(table);
            }
            catch (Exception ex)
            { }

        }

        private void AddTableOfContents(Document document, PdfWriter writer)
        {
            try
            {
                document.NewPage();
                _events.IsLastPage = true;

                Paragraph paragraph = new Paragraph("Table of Contents", fontHeading1);
                paragraph.SpacingAfter = speaceHeading1;
                document.Add(paragraph);

                float[] widths = new float[] { 95f, 5f };
                PdfPTable table = new PdfPTable(2);
                table.WidthPercentage = 100;

                table.SetWidths(widths);

                Chunk dottedLine = new Chunk(new DottedLineSeparator());
                dottedLine.Font = fontTableContent;
                Paragraph p;
                foreach (PageIndex pageIndex in _entries)
                {
                    string title = pageIndex.Text.Replace("\n", "");
                    string space = "";
                    if (title.Split(' ')[0].Split(".").Length == 2)
                        space = " \t ";
                    else if (title.Split(' ')[0].Split(".").Length > 2)
                        space = " \t \t \t";

                    title = space + title;
                    Chunk chunk = new Chunk(title, fontTableContent);

                    PdfAction pdfAction = PdfAction.GotoLocalPage(pageIndex.Page, new PdfDestination(PdfDestination.FIT), writer);
                    chunk.SetAction(pdfAction);

                    Paragraph element = new Paragraph();
                    element.Add(chunk);
                    element.Add(dottedLine);


                    PdfPCell pCell = new PdfPCell(element);
                    pCell.Border = 0;
                    table.AddCell(pCell);

                    chunk = new Chunk(pageIndex.Page.ToString(), fontTableContent);
                    chunk.SetAction(PdfAction.GotoLocalPage(pageIndex.Name, false));
                    pCell = new PdfPCell(new Phrase(chunk));
                    pCell.Border = 0;
                    table.AddCell(pCell);

                    pCell = new PdfPCell(new Phrase(""));
                    pCell.Border = 0;
                    pCell.Colspan = 2;
                    pCell.MinimumHeight = 5;
                    table.AddCell(pCell);
                }
                document.Add(table);
            }
            catch (Exception ex)
            { }
        }

        private void AddAdapteres(Document document)
        {
            try
            {
                string title = "1 \t \t General Guide";
                Chunk chTitle = new Chunk(title, fontHeading1);
                chTitle.SetGenericTag(title);
                Paragraph paragraph = new Paragraph(chTitle);
                paragraph.SpacingAfter = speaceHeading1;
                document.Add(paragraph);

                int subsection = 1;
                foreach (AdapterSteps adapterStep in _adapterSteps)
                {
                    string subTitle = "1." + subsection + " \t " + adapterStep.Name;
                    Chunk chSubTitle = new Chunk(subTitle, fontHeading2);
                    chSubTitle.SetGenericTag(subTitle);
                    paragraph = new Paragraph(chSubTitle);
                    paragraph.SpacingAfter = speaceHeading2;
                    document.Add(paragraph);

                    document.Add(new Paragraph("", fontHeading2));
                    float[] widths = new float[] { 30f, 70f };
                    PdfPTable table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    table.SetWidths(widths);

                    var cellFont1 = FontFactory.GetFont("Calibri", 11, Font.BOLD, new BaseColor(217, 217, 217));
                    var cellFont2 = FontFactory.GetFont("Calibri", 11, Font.ITALIC, new BaseColor(128, 128, 128));
                    foreach (Step step in adapterStep.Steps)
                    {
                        Chunk chunk = new Chunk(step.Name, cellFont1);
                        PdfPCell cell1 = new PdfPCell(new Phrase(chunk));
                        cell1.BorderColor = new BaseColor(191, 191, 191);
                        cell1.BackgroundColor = new BaseColor(31, 78, 121);
                        cell1.Padding = cellPadding;
                        cell1.MinimumHeight = 40f;
                        table.AddCell(cell1);

                        chunk = new Chunk(step.Description, cellFont2);
                        PdfPCell cell2 = new PdfPCell(new Phrase(chunk));
                        if (step.Name.ToLower() == "negative test step")
                            cell2.BackgroundColor = new BaseColor(242, 242, 242);
                        cell2.BorderColor = new BaseColor(191, 191, 191);
                        cell2.Padding = cellPadding;
                        cell2.MinimumHeight = 40f;
                        table.AddCell(cell2);
                    }
                    document.Add(table);
                }
            }
            catch (Exception ex)
            { }
        }

        private void AddRequirement(Document document)
        {
            try
            {
                string title = "2 \t \t	Requirement Overview";
                Chunk chTitle = new Chunk(title, fontHeading1);
                chTitle.SetGenericTag(title);
                Paragraph paragraph = new Paragraph(chTitle);
                paragraph.SpacingAfter = speaceHeading1;
                document.Add(paragraph);

                document.Add(new Paragraph());

                var cellFont1 = FontFactory.GetFont("Calibri", 11, Font.BOLD, new BaseColor(68, 84, 106));
                var cellFont2 = FontFactory.GetFont("Calibri", 11, Font.ITALIC, new BaseColor(68, 84, 106));

                float[] widths = new float[] { 30f, 70f };
                PdfPTable table = new PdfPTable(2);
                table.WidthPercentage = 100;
                table.SetWidths(widths);

                PdfPCell cell1 = new PdfPCell(new Phrase(new Chunk("Description: ", cellFont1)));
                cell1.Border = 0;
                cell1.Padding = cellPadding;
                cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                cell1.VerticalAlignment = PdfPCell.ALIGN_TOP;
                cell1.MinimumHeight = 25f;
                table.AddCell(cell1);

                cell1 = new PdfPCell(new Phrase(new Chunk(_process.Description + "\n \n", cellFont2)));
                cell1.Border = 0;
                cell1.Padding = cellPadding;
                cell1.MinimumHeight = 25f;
                table.AddCell(cell1);

                if (_process.Assignee != null)
                {
                    cell1 = new PdfPCell(new Phrase(new Chunk("Assignee: ", cellFont1)));
                    cell1.Border = 0;
                    cell1.Padding = cellPadding;
                    cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    cell1.VerticalAlignment = PdfPCell.ALIGN_TOP;
                    cell1.MinimumHeight = 25f;
                    table.AddCell(cell1);

                    cell1 = new PdfPCell(new Phrase(new Chunk(_process.Assignee.Name + " " + _process.Assignee.Surname, cellFont2)));
                    cell1.Border = 0;
                    cell1.Padding = cellPadding;
                    cell1.MinimumHeight = 25f;
                    table.AddCell(cell1);
                }

                if (_process.Priority != null)
                {
                    cell1 = new PdfPCell(new Phrase(new Chunk("Priority: ", cellFont1)));
                    cell1.Border = 0;
                    cell1.Padding = cellPadding;
                    cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    cell1.VerticalAlignment = PdfPCell.ALIGN_TOP;
                    cell1.MinimumHeight = 25f;
                    table.AddCell(cell1);

                    cell1 = new PdfPCell(new Phrase(new Chunk(_process.Priority.Title, cellFont2)));
                    cell1.Border = 0;
                    cell1.Padding = cellPadding;
                    cell1.MinimumHeight = 25f;
                    table.AddCell(cell1);
                }

                if (_process.Requirement != null)
                {
                    cell1 = new PdfPCell(new Phrase(new Chunk("Requirement Type: ", cellFont1)));
                    cell1.Border = 0;
                    cell1.Padding = cellPadding;
                    cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    cell1.VerticalAlignment = PdfPCell.ALIGN_TOP;
                    cell1.MinimumHeight = 25f;
                    table.AddCell(cell1);

                    cell1 = new PdfPCell(new Phrase(new Chunk(_process.Requirement.Title, cellFont2)));
                    cell1.Border = 0;
                    cell1.Padding = cellPadding;
                    cell1.MinimumHeight = 25f;
                    table.AddCell(cell1);
                }


                cell1 = new PdfPCell(new Phrase(new Chunk("Expected number of test cases: ", cellFont1)));
                cell1.Border = 0;
                cell1.Padding = cellPadding;
                cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                cell1.VerticalAlignment = PdfPCell.ALIGN_TOP;
                cell1.MinimumHeight = 25f;
                table.AddCell(cell1);

                cell1 = new PdfPCell(new Phrase(new Chunk(_process.ExpectedNumberOfTestCases.ToString(), cellFont2)));
                cell1.Border = 0;
                cell1.Padding = cellPadding;
                cell1.MinimumHeight = 25f;
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table.AddCell(cell1);

                if (_process.CoreApplication != null)
                {
                    cell1 = new PdfPCell(new Phrase(new Chunk("Core Application: ", cellFont1)));
                    cell1.Border = 0;
                    cell1.Padding = cellPadding;
                    cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    cell1.VerticalAlignment = PdfPCell.ALIGN_TOP;
                    cell1.MinimumHeight = 25f;
                    table.AddCell(cell1);

                    cell1 = new PdfPCell(new Phrase(new Chunk(_process.CoreApplication.Title, cellFont2)));
                    cell1.Border = 0;
                    cell1.Padding = cellPadding;
                    cell1.MinimumHeight = 25f;
                    table.AddCell(cell1);
                }

                if (_processResources > 0)
                {
                    cell1 = new PdfPCell(new Phrase(new Chunk("Download Attachments: ", cellFont1)));
                    cell1.Border = 0;
                    cell1.Padding = cellPadding;
                    cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    cell1.VerticalAlignment = PdfPCell.ALIGN_TOP;
                    cell1.MinimumHeight = 25f;
                    table.AddCell(cell1);
                    var underlineFont = FontFactory.GetFont("Calibri", 9, Font.UNDERLINE, new BaseColor(5, 99, 193));
                    Chunk chLink = new Chunk(" Attachments", underlineFont);
                    chLink.SetAnchor($"{_apiURL}{"Process/Browse/DownloadProcessResources?ProcessId="}{_process.Id}");


                    Paragraph element = new Paragraph();
                    element.Add(chLink);

                    PdfPCell cell = new PdfPCell(element);
                    cell.Border = 0;
                    cell.BorderColorTop = new BaseColor(217, 226, 243);
                    cell.BorderWidthTop = 0.2f;
                    cell.Padding = cellPadding;
                    cell.MinimumHeight = 25f;
                    table.AddCell(cell);
                }

                document.Add(table);
            }
            catch (Exception ex)
            { }
        }

        private void AddProcessDiagram(Document document)
        {
            try
            {
                if (string.IsNullOrEmpty(_processIamge))
                    return;
                wfSection++;
                string title = _section.ToString() + " \t \t Requirement Diagram";
                Chunk chTitle = new Chunk(title, fontHeading1);
                chTitle.SetGenericTag(title);
                Paragraph paragraph = new Paragraph(chTitle);
                paragraph.SpacingAfter = speaceHeading1;
                document.Add(paragraph);

                byte[] bytes = Convert.FromBase64String(_processIamge.Split(',')[1]);

                Image image = Image.GetInstance(new MemoryStream(bytes));
                image.Alignment = Image.ALIGN_CENTER;

                float width = image.Width;
                float height = image.Height;
                if (width > document.PageSize.Width)
                {
                    width = document.PageSize.Width - (document.LeftMargin + document.RightMargin);
                }
                if (height > document.PageSize.Height)
                    height = document.PageSize.Height - (document.BottomMargin + document.TopMargin + speaceHeading1 + 30);

                image.ScaleToFit(width, height);
                document.Add(image);

                _section++;
            }
            catch (Exception ex)
            { }
        }

        private void AddWorkflows(Document document)
        {
            try
            {
                string title = _section.ToString() + " \t \t Workflows";
                Chunk chTitle = new Chunk(title, fontHeading1);
                chTitle.SetGenericTag(title);
                Paragraph paragraph = new Paragraph(chTitle);
                paragraph.SpacingAfter = speaceHeading1;
                document.Add(paragraph);


                float[] widths = new float[] { 30f, 70f };

                var boldFont = FontFactory.GetFont("Calibri", 12, Font.BOLD, new BaseColor(68, 84, 106));
                var underlineFont = FontFactory.GetFont("Calibri", 9, Font.UNDERLINE, new BaseColor(5, 99, 193));
                int index = 1;
                foreach (Workflow workflow in _workflows)
                {
                    PdfPTable table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    table.SetWidths(widths);

                    var workflowPaths = _workflowPaths.FirstOrDefault(w => w.Key == workflow.Id).Value;
                    bool hasVideo = _workflowVideos.FirstOrDefault(w => w.Key == workflow.Id).Value != null;
                    Chunk chWorkflowTitle = new Chunk(index.ToString() + ".Workflow: " + workflow.Title, boldFont);
                    Chunk chVideo = new Chunk("", underlineFont);
                    Chunk chLink = new Chunk("\n \n Navigate to Sense.AI", underlineFont);
                    chLink.SetAnchor($"{_uiURL}{"#/workflow-designer?id="}{workflow.Id.ToString() }");

                    if (hasVideo)
                    {
                        chVideo = new Chunk("\n Download training video", underlineFont);
                        chVideo.SetAnchor($"{_apiURL}{"/Design/DownloadVideo?WorkflowId="}{workflow.Id.ToString() }");
                    }
                    Paragraph element = new Paragraph();
                    element.Add(chWorkflowTitle);
                    element.Add(chVideo);
                    element.Add(chLink);

                    PdfPCell cell1 = new PdfPCell(element);
                    cell1.Border = 0;
                    cell1.BorderColorTop = new BaseColor(217, 226, 243);
                    cell1.BorderWidthTop = 0.2f;
                    cell1.Padding = cellPadding;
                    cell1.MinimumHeight = 25f;
                    table.AddCell(cell1);

                    string paths = "Paths (0)";
                    if (workflowPaths != null && workflowPaths.Count > 0)
                        paths = $"Paths ({workflowPaths.Count()})";

                    Chunk chPathTitle = new Chunk(paths, boldFont);
                    PdfPCell cell2 = new PdfPCell(new Phrase(chPathTitle));
                    cell2.Border = 0;
                    cell2.BorderColorTop = new BaseColor(217, 226, 243);
                    cell2.BackgroundColor = new BaseColor(217, 226, 243);
                    cell2.BorderWidthTop = 0.2f;
                    cell2.Padding = cellPadding;
                    cell2.MinimumHeight = 25f;
                    cell2.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    table.AddCell(cell2);

                    cell1 = new PdfPCell(new Phrase());
                    cell1.Border = 0;
                    cell1.Colspan = 2;
                    cell1.MinimumHeight = 5f;
                    table.AddCell(cell1);

                    document.Add(table);
                    if (workflowPaths.Count > 0)
                    {
                        AddWorkflowPaths(document, cell2, workflowPaths, workflow.Id);
                    }
                    index++;
                }

                wfEndSection = pdfWriter.CurrentPageNumber;
                _section++;

            }
            catch (Exception ex)
            { }
        }

        private void AddWorkflowPaths(Document document, PdfPCell tableCell, List<WorkflowVersionPaths> workflowPaths, Guid workflowId)
        {
            try
            {
                PdfPTable pdfPTable = new PdfPTable(5);
                float[] widths = new float[] { 10f, 10f, 10f, 70f };
                PdfPTable table = new PdfPTable(4);
                table.WidthPercentage = 100;
                table.SetWidths(widths);
                var cellFont1 = FontFactory.GetFont("Calibri", 11, Font.BOLD, new BaseColor(68, 84, 106));
                var cellFont2 = FontFactory.GetFont("Calibri", 11, new BaseColor(68, 84, 106));
                var cellFont3 = FontFactory.GetFont("Calibri", 11, new BaseColor(217, 217, 217));

                PdfPCell cell = new PdfPCell(new Phrase());
                cell.BackgroundColor = new BaseColor(31, 78, 121);
                cell.BorderColor = new BaseColor(217, 226, 243);
                cell.MinimumHeight = 25f;
                cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Adapter", cellFont3));
                cell.BackgroundColor = new BaseColor(31, 78, 121);
                cell.BorderColor = new BaseColor(217, 226, 243);
                cell.MinimumHeight = 25f;
                cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Maximum number of test cases", cellFont3));
                cell.BackgroundColor = new BaseColor(31, 78, 121);
                cell.BorderColor = new BaseColor(217, 226, 243);
                cell.MinimumHeight = 25f;
                cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Steps", cellFont3));
                cell.BackgroundColor = new BaseColor(31, 78, 121);
                cell.BorderColor = new BaseColor(217, 226, 243);
                cell.MinimumHeight = 25f;
                cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                table.AddCell(cell);

                foreach (WorkflowVersionPaths workflowPath in workflowPaths.OrderBy(o => o.Index))
                {
                    var dataSetIds = workflowPath.Items.Where(w => w.ActionType == (int)ActionType.Input && w.DataSetId != Guid.Empty).Select(s => s.DataSetId).ToList();
                    Chunk chunk = new Chunk(workflowPath.Title, cellFont1);
                    chunk.SetUnderline(1, -2);
                    cell = new PdfPCell(new Phrase(chunk));
                    cell.BorderColor = new BaseColor(217, 226, 243);
                    cell.Padding = cellPadding;
                    cell.MinimumHeight = 25f;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("T24 Web", cellFont2));
                    cell.BorderColor = new BaseColor(217, 226, 243);
                    cell.Padding = cellPadding;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(GetTestRunInWorkflowPath(dataSetIds).ToString(), cellFont2)) { VerticalAlignment = PdfPCell.ALIGN_TOP, HorizontalAlignment = Element.ALIGN_CENTER };
                    cell.BorderColor = new BaseColor(217, 226, 243);
                    cell.Padding = cellPadding;
                    cell.MinimumHeight = 25f;
                    table.AddCell(cell);

                    List<string> pathItems = GetPathItems(workflowPath.Items.OrderBy(o => o.Index).ToList());

                    cell = new PdfPCell(new Phrase(new Chunk(pathItems.FirstOrDefault(), cellFont1)));
                    cell.BorderColor = new BaseColor(217, 226, 243);
                    cell.Padding = cellPadding;
                    cell.MinimumHeight = 25f;
                    table.AddCell(cell);

                    for (int i = 1; i < pathItems.Count; i++)
                    {
                        cell = new PdfPCell(new Phrase(""));
                        cell.BorderColor = new BaseColor(217, 226, 243);
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthBottom = 0;
                        cell.Colspan = 3;
                        table.AddCell(cell);

                        cell = new PdfPCell(new Phrase(new Chunk(pathItems[i], cellFont2)));
                        cell.BorderColor = new BaseColor(217, 226, 243);
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthBottom = 0;
                        cell.Padding = cellPadding;
                        cell.MinimumHeight = 25f;
                        table.AddCell(cell);
                    }

                    cell = new PdfPCell(new Phrase(""));
                    cell.BorderColor = new BaseColor(217, 226, 243);
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 4;
                    table.AddCell(cell);
                }

                cell = new PdfPCell(new Phrase());
                cell.Border = 0;
                cell.Colspan = 5;
                cell.MinimumHeight = 4f;
                cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                table.AddCell(cell);

                document.Add(table);
            }
            catch (Exception ex)
            { }
        }


        private int GetTestRunInWorkflowPath(List<Guid> dataSetIds)
        {
            if (dataSetIds == null || dataSetIds.Count == 0)
                return 0;

            var testRuns = 1;
            _dataSets.Where(w => dataSetIds.Contains(w.Key.Id)).Select(s => s.Value).ToList().ForEach(f => { testRuns = testRuns * f; });

            return testRuns;
        }

        private List<string> GetPathItems(List<WorkflowVersionPathDetails> workflowPathDetails)
        {
            List<string> result = new List<string>();

            foreach (var item in workflowPathDetails)
            {
                if (item.Title.ToLower() == "switch system")
                    continue;
                result.Add(item.Title);
            }

            return result;
        }

        private void AddWorkflow(Document document)
        {
            try
            {
                foreach (Workflow workflow in _workflows)
                {
                    string title = _section.ToString() + "	\t Detailed explanation for " + workflow.Title + " workflow";
                    Chunk chTitle = new Chunk(title, fontHeading1);
                    chTitle.SetGenericTag(title);
                    Paragraph paragraph = new Paragraph(chTitle);
                    paragraph.SpacingAfter = speaceHeading1;
                    document.Add(paragraph);

                    if (!string.IsNullOrEmpty(workflow.WorkflowImage))
                    {
                        byte[] bytes = Convert.FromBase64String(workflow.WorkflowImage.Split(',')[1]);

                        Image image = Image.GetInstance(new MemoryStream(bytes));
                        image.Alignment = Image.ALIGN_CENTER;

                        float width = image.Width;
                        float height = image.Height;
                        if (width > document.PageSize.Width)
                        {
                            width = document.PageSize.Width - (document.LeftMargin + document.RightMargin);
                        }
                        if (height > document.PageSize.Height)
                            height = document.PageSize.Height - (document.BottomMargin + document.TopMargin + speaceHeading1 + 30);
                        if (height < 490)
                            height = height - (document.BottomMargin + document.TopMargin + speaceHeading1 - 10);
                        else
                            height = height - (document.BottomMargin + document.TopMargin + speaceHeading1 + 80);

                        image.ScaleToFit(width, height);
                        document.Add(image);
                    }
                    document.NewPage();

                    AddPathSteps(document, workflow.Id, _workflowPaths.FirstOrDefault(w => w.Key == workflow.Id).Value);

                    document.NewPage();
                    _section++;
                }
            }
            catch (Exception ex)
            { }
        }

        private void AddPathSteps(Document document, Guid workflowId, List<WorkflowVersionPaths> workflowPaths)
        {
            try
            {
                int subSection = 1;
                var generatedTestCases = _workflowTestCases.FirstOrDefault(w => w.Key == workflowId).Value;

                var cellFont1 = FontFactory.GetFont("Calibri", 13, Font.BOLDITALIC, new BaseColor(217, 217, 217));
                var cellFontNegative = FontFactory.GetFont("Calibri", 13, Font.BOLDITALIC, new BaseColor(192, 0, 0));
                var cellFont2 = FontFactory.GetFont("Calibri", 11, new BaseColor(68, 84, 106));
                var cellFontDesc = FontFactory.GetFont("Calibri", 10, new BaseColor(68, 84, 106));
                var cellFont3 = FontFactory.GetFont("Calibri", 11, Font.BOLD, new BaseColor(68, 84, 106));
                var cellTypicalFont = FontFactory.GetFont("Calibri", 12, Font.BOLD, new BaseColor(68, 84, 106));
                var stepReferenceFont = FontFactory.GetFont("Calibri", 11, Font.UNDERLINE, new BaseColor(191, 143, 0));
                var stepReferenceFontRed = FontFactory.GetFont("Calibri", 11, Font.ITALIC, new BaseColor(0, 0, 0));
                var srcAttributeFont = FontFactory.GetFont("Calibri", 11, Font.ITALIC, new BaseColor(191, 143, 0));

                foreach (WorkflowVersionPaths workflowPath in workflowPaths.OrderBy(o => o.Index))
                {
                    string subTitle = _section.ToString() + "." + subSection.ToString() + " \t Items on " + workflowPath.Title + " \n \n";
                    Chunk chSubTitle = new Chunk(subTitle, fontHeading2);
                    chSubTitle.SetGenericTag(subTitle);
                    var pSubTitle = new Paragraph(chSubTitle);
                    pSubTitle.SpacingAfter = speaceHeading2;
                    document.Add(pSubTitle);

                    float[] widths = new float[] { 5f, 95f };
                    PdfPTable table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    table.SetWidths(widths);

                    foreach (var step in generatedTestCases.Where(w => w.WorkflowPathId == workflowPath.Id).OrderBy(o => o.TCIndex).ThenBy(o => o.TSIndex))
                    {
                        TestStepJson testStepJson = JsonConvert.DeserializeObject<TestStepJson>(step.TestStepJson);

                        PdfPCell cell1 = new PdfPCell(new Phrase((step.TSIndex + 1).ToString(), testStepJson.IsNegativeStep ? cellFontNegative : cellFont1));
                        cell1.Border = 0;
                        cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell1.Padding = cellPadding;
                        table.AddCell(cell1);

                        PdfPCell cell2 = new PdfPCell(new Phrase(step.TestStepTitle, testStepJson.IsNegativeStep ? cellFontNegative : cellFont1));
                        cell2.Border = 0;
                        cell2.MinimumHeight = 30;
                        cell2.BorderWidthBottom = 0.2f;
                        cell2.BorderColorBottom = new BaseColor(47, 84, 150);
                        cell2.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        cell2.BackgroundColor = new BaseColor(31, 78, 121);// new BaseColor(217, 226, 243);
                        cell1.Padding = cellPadding;
                        table.AddCell(cell2);

                        cell1 = new PdfPCell(new Phrase(new Chunk(step.TestStepType == 1 ? (testStepJson.IsNegativeStep ? " Negative Test step" : GetActionType(testStepJson.Action)) : " Manual", cellFontDesc)));
                        cell1.Border = 0;
                        cell1.Colspan = 2;
                        cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        table.AddCell(cell1);

                        cell1 = new PdfPCell(new Phrase(""));
                        cell1.Border = 0;
                        cell1.VerticalAlignment = PdfPCell.ALIGN_TOP;
                        cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        table.AddCell(cell1);

                        cell2 = new PdfPCell(new Phrase(testStepJson.Parameters, cellFont2));
                        cell2.Border = 0;

                        string description = "\n";
                        if (!string.IsNullOrEmpty(testStepJson.Description))
                            description = testStepJson.Description + " \n \n";
                        Paragraph desParagraph = new Paragraph(new Chunk(description, cellFontDesc));
                        if (testStepJson.IsNegativeStep)
                        {
                            desParagraph.Add(new Chunk("Expected Error: ", stepReferenceFontRed));
                            desParagraph.Add(new Chunk(testStepJson.ExpectedError + " \n ", cellFontDesc));
                        }
                        if (step.TestStepType == 1)
                        {
                            if (testStepJson.Action == ActionType.Input || testStepJson.Action == ActionType.Reverse ||
                                testStepJson.Action == ActionType.Delete || testStepJson.Action == ActionType.See
                                || testStepJson.Action == ActionType.Verify || testStepJson.Action == ActionType.Authorize
                                || testStepJson.Action == ActionType.See)
                            {
                                cell2.AddElement(desParagraph);

                                widths = new float[] { 30f, 70f };
                                PdfPTable tblTypicals = new PdfPTable(2);
                                tblTypicals.WidthPercentage = 100;
                                tblTypicals.SetWidths(widths);

                                if (testStepJson.Action == ActionType.Reverse ||
                                testStepJson.Action == ActionType.Delete || testStepJson.Action == ActionType.See
                                || testStepJson.Action == ActionType.Verify || testStepJson.Action == ActionType.Authorize
                                || testStepJson.Action == ActionType.See)

                                {
                                    PdfPCell pfcell1Typical = new PdfPCell(new Phrase("TYPICAL", cellTypicalFont));
                                    pfcell1Typical.MinimumHeight = 25f;
                                    pfcell1Typical.Padding = cellPadding;
                                    pfcell1Typical.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                                    pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                                    pfcell1Typical.BackgroundColor = new BaseColor(217, 226, 243);
                                    tblTypicals.AddCell(pfcell1Typical);

                                    pfcell1Typical = new PdfPCell(new Phrase("ACTION", cellTypicalFont));
                                    pfcell1Typical.MinimumHeight = 25f;
                                    pfcell1Typical.Padding = cellPadding;
                                    pfcell1Typical.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                                    pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                                    pfcell1Typical.BackgroundColor = new BaseColor(217, 226, 243);
                                    tblTypicals.AddCell(pfcell1Typical);

                                    pfcell1Typical = new PdfPCell(new Phrase(testStepJson.TypicalName.ToUpper(), cellFont3));
                                    pfcell1Typical.BorderWidth = 0.2f;
                                    pfcell1Typical.MinimumHeight = 25f;
                                    pfcell1Typical.Padding = cellPadding;
                                    pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                                    pfcell1Typical.BackgroundColor = new BaseColor(242, 242, 242);
                                    pfcell1Typical.Padding = cellPadding;
                                    tblTypicals.AddCell(pfcell1Typical);

                                    pfcell1Typical = new PdfPCell(new Phrase(testStepJson.Action.ToString(), cellFont3));
                                    pfcell1Typical.MinimumHeight = 25f;
                                    pfcell1Typical.Padding = cellPadding;
                                    pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                                    pfcell1Typical.Padding = cellPadding;
                                    tblTypicals.AddCell(pfcell1Typical);

                                    if (testStepJson.DynamicDatas.Count > 0)
                                    {
                                        pfcell1Typical = new PdfPCell(new Phrase("", cellFont3));
                                        pfcell1Typical.MinimumHeight = 5f;
                                        pfcell1Typical.Border = 0;
                                        pfcell1Typical.Colspan = 4;
                                        tblTypicals.AddCell(pfcell1Typical);
                                    }
                                }

                                if (testStepJson.Action != ActionType.Input && testStepJson.DynamicDatas.Count == 0)
                                {
                                    cell2.AddElement(tblTypicals);
                                    table.AddCell(cell2);
                                    continue;
                                }
                                string ac = "";
                                if (testStepJson.DynamicDatas.Count > 0)
                                    ac = " - Dynamic Data";

                                PdfPCell cell1Typical = new PdfPCell(new Phrase("Typical: " + testStepJson.TypicalName.ToUpper() + ac, cellTypicalFont));
                                cell1Typical.Border = 0;
                                cell1Typical.Colspan = 2;
                                cell1Typical.MinimumHeight = 25f;
                                cell1Typical.Padding = cellPadding;
                                cell1Typical.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                                cell1Typical.BorderColor = new BaseColor(217, 226, 243);
                                cell1Typical.BackgroundColor = new BaseColor(217, 226, 243);
                                tblTypicals.AddCell(cell1Typical);

                                foreach (var dynamicData in testStepJson.DynamicDatas)
                                {
                                    if (dynamicData.TestStepId == Guid.Empty)
                                        continue;

                                    PdfPCell cell2Typical = new PdfPCell(new Phrase(dynamicData.TargetAttributeName, cellFont3));
                                    cell2Typical.BorderWidth = float.Parse("0.2");
                                    cell2Typical.BackgroundColor = new BaseColor(242, 242, 242);
                                    cell2Typical.BorderColor = new BaseColor(217, 226, 243);
                                    cell2Typical.Padding = cellPadding;
                                    tblTypicals.AddCell(cell2Typical);

                                    var referenceTo = generatedTestCases.FirstOrDefault(w => w.TestStepId == dynamicData.TestStepId);
                                    Paragraph element = new Paragraph();

                                    element.Add(new Chunk($"Test case: ", stepReferenceFontRed));
                                    element.Add(new Chunk($"{ referenceTo.TestCaseTitle}", stepReferenceFont));

                                    element.Add(new Chunk($" \n \n Test step: ", stepReferenceFontRed));
                                    element.Add(new Chunk($"{ referenceTo.TestStepTitle}", stepReferenceFont));

                                    element.Add(new Chunk($" \n \n Typical name: ", stepReferenceFontRed));
                                    element.Add(new Chunk($"[{referenceTo.TypicalName.Split(',')[0]}]", stepReferenceFont));

                                    element.Add(new Chunk(" \n \n Attribute: ", stepReferenceFontRed));
                                    element.Add(new Chunk(dynamicData.SourceAttributeName + " \n ", srcAttributeFont));

                                    PdfPCell cell3Typical = new PdfPCell(element);
                                    cell3Typical.BorderColor = new BaseColor(217, 226, 243);
                                    cell3Typical.Padding = cellPadding;
                                    tblTypicals.AddCell(cell3Typical);
                                }
                                cell2.AddElement(tblTypicals);
                            }
                            else if (testStepJson.Action == ActionType.EnquiryAction || testStepJson.Action == ActionType.EnquiryResult)
                            {
                                cell2.AddElement(desParagraph);

                                if (testStepJson.Action == ActionType.EnquiryAction)
                                {
                                    widths = new float[] { 50f, 50f };
                                    PdfPTable tblTypicals = new PdfPTable(2);
                                    tblTypicals.WidthPercentage = 100;
                                    tblTypicals.SetWidths(widths);

                                    PdfPCell pfcell1Typical = new PdfPCell(new Phrase("TYPICAL", cellTypicalFont));
                                    pfcell1Typical.MinimumHeight = 25f;
                                    pfcell1Typical.Padding = cellPadding;
                                    pfcell1Typical.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                                    pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                                    pfcell1Typical.BackgroundColor = new BaseColor(217, 226, 243);
                                    tblTypicals.AddCell(pfcell1Typical);

                                    pfcell1Typical = new PdfPCell(new Phrase("ACTION", cellTypicalFont));
                                    pfcell1Typical.MinimumHeight = 25f;
                                    pfcell1Typical.Padding = cellPadding;
                                    pfcell1Typical.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                                    pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                                    pfcell1Typical.BackgroundColor = new BaseColor(217, 226, 243);
                                    tblTypicals.AddCell(pfcell1Typical);

                                    pfcell1Typical = new PdfPCell(new Phrase(testStepJson.TypicalName.ToUpper(), cellFont3));
                                    pfcell1Typical.MinimumHeight = 25f;
                                    pfcell1Typical.Padding = cellPadding;
                                    pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                                    pfcell1Typical.BackgroundColor = new BaseColor(242, 242, 243);
                                    pfcell1Typical.Padding = cellPadding;
                                    tblTypicals.AddCell(pfcell1Typical);

                                    pfcell1Typical = new PdfPCell(new Phrase(testStepJson.Parameters, cellFont3));
                                    pfcell1Typical.MinimumHeight = 25f;
                                    pfcell1Typical.Padding = cellPadding;
                                    pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                                    pfcell1Typical.Padding = cellPadding;
                                    tblTypicals.AddCell(pfcell1Typical);

                                    cell2.AddElement(tblTypicals);
                                }

                                widths = new float[] { 45f, 10f, 45f };
                                PdfPTable tblfilters = new PdfPTable(3);
                                tblfilters.WidthPercentage = 100;
                                tblfilters.SetWidths(widths);

                                PdfPCell fcell1 = new PdfPCell(new Phrase());
                                fcell1.Border = 0;

                                /*start pre filters*/
                                if (testStepJson.Filters.Count > 0)
                                {
                                    widths = new float[] { 30f, 70f };
                                    PdfPTable tblPreFilters = new PdfPTable(2);
                                    tblPreFilters.WidthPercentage = 100;
                                    tblPreFilters.SetWidths(widths);

                                    PdfPCell cell1PreFilter = new PdfPCell(new Phrase("Search criteria \n" + testStepJson.TypicalName.ToUpper().Replace(".RESULT", ""), cellTypicalFont));
                                    cell1PreFilter.Border = 0;
                                    cell1PreFilter.Colspan = 2;
                                    cell1PreFilter.MinimumHeight = 25f;
                                    cell1PreFilter.Padding = cellPadding;
                                    cell1PreFilter.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                                    cell1PreFilter.BackgroundColor = new BaseColor(242, 242, 242);
                                    cell1PreFilter.BorderColor = new BaseColor(217, 226, 243);
                                    tblPreFilters.AddCell(cell1PreFilter);

                                    foreach (var filter in testStepJson.Filters)
                                    {
                                        PdfPCell cell2PreFilter = new PdfPCell(new Phrase(filter.AttributeName, cellFont3));
                                        cell2PreFilter.BorderWidth = 0.2f;
                                        cell2PreFilter.BackgroundColor = new BaseColor(242, 242, 242);
                                        cell2PreFilter.BorderColor = new BaseColor(217, 226, 243);
                                        cell2PreFilter.Padding = cellPadding;
                                        tblPreFilters.AddCell(cell2PreFilter);

                                        Paragraph element = new Paragraph(new Chunk(filter.AttributeValue, srcAttributeFont));
                                        if (filter.DynamicData != null && filter.DynamicData.TestStepId != Guid.Empty)
                                        {
                                            var referenceTo = generatedTestCases.FirstOrDefault(w => w.TestStepId == filter.DynamicData.TestStepId);

                                            element = new Paragraph(new Chunk($" \n \n Test case: ", stepReferenceFontRed));
                                            element.Add(new Chunk($"{ referenceTo.TestCaseTitle}", stepReferenceFont));

                                            element.Add(new Chunk($" \n \n Test step: ", stepReferenceFontRed));
                                            element.Add(new Chunk($"{ referenceTo.TestStepTitle}", stepReferenceFont));

                                            element.Add(new Chunk($" \n \n Typical name: ", stepReferenceFontRed));
                                            element.Add(new Chunk($"[{referenceTo.TypicalName.Split(',')[0]}]", stepReferenceFont));

                                            element.Add(new Chunk(" \n \n Attribute: ", stepReferenceFontRed));
                                            element.Add(new Chunk(filter.DynamicData.SourceAttributeName + " \n ", srcAttributeFont));
                                        }

                                        PdfPCell cell3PreFilter = new PdfPCell(element);
                                        cell3PreFilter.BorderColor = new BaseColor(217, 226, 243);
                                        cell3PreFilter.Padding = cellPadding;
                                        cell3PreFilter.BorderWidth = 0.2f;
                                        tblPreFilters.AddCell(cell3PreFilter);
                                    }

                                    fcell1.AddElement(tblPreFilters);
                                }
                                /*end pre filters*/

                                tblfilters.AddCell(fcell1);

                                fcell1 = new PdfPCell(new Phrase());
                                fcell1.Border = 0;
                                tblfilters.AddCell(fcell1);

                                widths = new float[] { 30f, 70f };
                                PdfPTable tblPostFilters = new PdfPTable(2);
                                tblPostFilters.WidthPercentage = 100;
                                tblPostFilters.SetWidths(widths);

                                /*start post filters*/
                                if (testStepJson.PostFilters.Count > 0)
                                {
                                    PdfPCell cell1PostFilter = new PdfPCell(new Phrase("Post-Filters: " + testStepJson.TypicalName.ToUpper(), cellTypicalFont));
                                    cell1PostFilter.Border = 0;
                                    cell1PostFilter.Colspan = 2;
                                    cell1PostFilter.MinimumHeight = 25f;
                                    cell1PostFilter.Padding = cellPadding;
                                    cell1PostFilter.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                                    cell1PostFilter.BackgroundColor = new BaseColor(242, 242, 242);
                                    cell1PostFilter.BorderColor = new BaseColor(217, 226, 243);
                                    tblPostFilters.AddCell(cell1PostFilter);

                                    foreach (var filter in testStepJson.PostFilters)
                                    {
                                        PdfPCell cell2PostFilter = new PdfPCell(new Phrase(filter.AttributeName, cellFont3));
                                        cell2PostFilter.BorderWidth = 0.2f;
                                        cell2PostFilter.BackgroundColor = new BaseColor(242, 242, 242);
                                        cell2PostFilter.BorderColor = new BaseColor(217, 226, 243);
                                        cell2PostFilter.Padding = cellPadding;
                                        tblPostFilters.AddCell(cell2PostFilter);

                                        Paragraph element = new Paragraph(new Chunk(filter.AttributeValue, srcAttributeFont));
                                        if (filter.DynamicData != null && filter.DynamicData.TestStepId != Guid.Empty)
                                        {
                                            var referenceTo = generatedTestCases.FirstOrDefault(w => w.TestStepId == filter.DynamicData.TestStepId);

                                            element = new Paragraph(new Chunk($" \n \n Test case: ", stepReferenceFontRed));
                                            element.Add(new Chunk($"{ referenceTo.TestCaseTitle}", stepReferenceFont));

                                            element.Add(new Chunk($" \n \n Test step: ", stepReferenceFontRed));
                                            element.Add(new Chunk($"{ referenceTo.TestStepTitle}", stepReferenceFont));

                                            element.Add(new Chunk($" \n \n Typical name: ", stepReferenceFontRed));
                                            element.Add(new Chunk($"[{referenceTo.TypicalName.Split(',')[0]}]", stepReferenceFont));

                                            element.Add(new Chunk(" \n \n Attribute: ", stepReferenceFontRed));
                                            element.Add(new Chunk(filter.DynamicData.SourceAttributeName + " \n ", srcAttributeFont));
                                        }

                                        PdfPCell cell3PosFilter = new PdfPCell(element);
                                        cell3PosFilter.BorderColor = new BaseColor(217, 226, 243);
                                        cell3PosFilter.Padding = cellPadding;
                                        cell2PostFilter.BorderWidth = 0.2f;
                                        tblPostFilters.AddCell(cell3PosFilter);
                                    }

                                    fcell1.AddElement(tblPostFilters);
                                }
                                /*end post filters*/

                                tblfilters.AddCell(fcell1);

                                cell2.AddElement(tblfilters);
                            }

                        }
                        else if (step.TestStepType == 4)
                        {
                            cell2.AddElement(desParagraph);

                            widths = new float[] { 50f, 50f };
                            PdfPTable tblTypicals = new PdfPTable(2);
                            tblTypicals.WidthPercentage = 100;
                            tblTypicals.SetWidths(widths);

                            PdfPCell pfcell1Typical = new PdfPCell(new Phrase("ROLE", cellTypicalFont));
                            pfcell1Typical.MinimumHeight = 25f;
                            pfcell1Typical.Padding = cellPadding;
                            pfcell1Typical.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            pfcell1Typical.BackgroundColor = new BaseColor(217, 226, 243);
                            pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                            tblTypicals.AddCell(pfcell1Typical);

                            pfcell1Typical = new PdfPCell(new Phrase("ACTION", cellTypicalFont));
                            pfcell1Typical.MinimumHeight = 25f;
                            pfcell1Typical.Padding = cellPadding;
                            pfcell1Typical.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            pfcell1Typical.BackgroundColor = new BaseColor(217, 226, 243);
                            pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                            tblTypicals.AddCell(pfcell1Typical);

                            pfcell1Typical = new PdfPCell(new Phrase(testStepJson.TypicalName.ToUpper(), cellFont3));
                            pfcell1Typical.MinimumHeight = 25f;
                            pfcell1Typical.Padding = cellPadding;
                            pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                            pfcell1Typical.Padding = cellPadding;
                            tblTypicals.AddCell(pfcell1Typical);

                            pfcell1Typical = new PdfPCell(new Phrase(testStepJson.Parameters, cellFont3));
                            pfcell1Typical.MinimumHeight = 25f;
                            pfcell1Typical.Padding = cellPadding;
                            pfcell1Typical.BorderColor = new BaseColor(217, 226, 243);
                            pfcell1Typical.Padding = cellPadding;
                            tblTypicals.AddCell(pfcell1Typical);

                            cell2.AddElement(tblTypicals);

                        }
                        table.AddCell(cell2);

                        cell1 = new PdfPCell(new Phrase("\n", cellFont1));
                        cell1.Border = 0;
                        cell1.Colspan = 2;
                        cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        table.AddCell(cell1);
                    }
                    document.Add(table);
                    document.NewPage();
                    AddDataCombinations(document, workflowPath, subSection);
                    subSection++;
                }
            }
            catch (Exception ex)
            { }
        }

        private string GetActionType(ActionType actionType)
        {
            if (actionType == ActionType.Input)
                return "Application Step";
            else if (actionType == ActionType.Menu || actionType == ActionType.Tab)
                return " Navigation Step: " + actionType.ToString();
            else if (actionType == ActionType.EnquiryAction)
                return "Enquiry Action Step";
            else if (actionType == ActionType.EnquiryResult)
                return "Check Enquiry Step";
            else
                return "Action Step: " + actionType.ToString();
        }

        private void AddDataCombinations(Document document, WorkflowVersionPaths workflowPath, int subSection)
        {
            try
            {
                var pathsItems = workflowPath.Items.Where(w => w.ActionType == (int)ActionType.Input && w.DataSetId != Guid.Empty).OrderBy(o => o.Index);

                var subTitleFont = FontFactory.GetFont("Calibri Light", 13, BaseColor.BLUE);
                string subTitle = _section.ToString() + "." + subSection.ToString() + ".1 \t \t Data combinations and permutations for " + workflowPath.Title;
                Chunk chTitle = new Chunk(subTitle, fontHeading2);
                chTitle.SetGenericTag(subTitle);
                Paragraph paragraph = new Paragraph(chTitle);
                paragraph.SpacingAfter = speaceHeading2;
                document.Add(paragraph);

                float[] widths = new float[] { 30f, 70f };
                PdfPTable table = new PdfPTable(2);
                table.WidthPercentage = 100;
                table.SetWidths(widths);

                var cellFont1 = FontFactory.GetFont("Calibri", 12, Font.BOLD, new BaseColor(217, 217, 217));
                var cellFontCoverage = FontFactory.GetFont("Calibri", 12, Font.NORMAL, new BaseColor(217, 217, 217));
                var cellFont2 = FontFactory.GetFont("Calibri", 11, new BaseColor(68, 84, 106));
                var cellFont3 = FontFactory.GetFont("Calibri", 11, Font.BOLD, new BaseColor(217, 217, 217));
                var cellFont4 = FontFactory.GetFont("Calibri", 12, Font.BOLD, new BaseColor(68, 84, 106));
                var topCellFon3 = FontFactory.GetFont("Calibri", 11, Font.BOLD, new BaseColor(0, 0, 0));

                int total = 1;
                if (pathsItems.Count() == 0)
                    total = 0;

                foreach (var path in pathsItems)
                {

                    DataSet dataSet = _dataSets.FirstOrDefault(w => w.Key.Id == path.DataSetId).Key;

                    if (dataSet.Combinations == "")
                        continue;

                    int totalCombinatins = _dataSets.FirstOrDefault(w => w.Key.Id == path.DataSetId).Value;

                    paragraph = new Paragraph();

                    paragraph.Add(new Chunk(path.TypicalName + " ", cellFont1));
                    paragraph.Add(new Chunk(" with " + dataSet.Coverage + " % Coverage", cellFontCoverage));

                    PdfPCell cell1 = new PdfPCell(paragraph);
                    cell1.Border = 0;
                    cell1.Colspan = 2;
                    cell1.MinimumHeight = 30;
                    cell1.BorderColor = new BaseColor(191, 191, 191);
                    cell1.BackgroundColor = new BaseColor(31, 78, 121);
                    cell1.BorderWidth = 1f;
                    cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    cell1.Padding = cellPadding;
                    table.AddCell(cell1);


                    /*Combinations*/
                    Combinations[] combinations = JsonConvert.DeserializeObject<Combinations[]>(dataSet.Combinations);
                    foreach (Combinations combination in combinations)
                    {
                        string values = string.Join(", ", combination.Values).Trim();
                        if (values == "")
                            continue;

                        cell1 = new PdfPCell(new Phrase(combination.Attribute, cellFont4));
                        cell1.MinimumHeight = 25;
                        cell1.BorderColor = new BaseColor(31, 78, 121);
                        cell1.BackgroundColor = new BaseColor(217, 226, 243);
                        cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        cell1.Padding = cellPadding;
                        table.AddCell(cell1);

                        if (values != "" && values.LastOrDefault().ToString() == ",")
                            values = values.Substring(0, values.Length - 1);
                        PdfPCell cell2 = new PdfPCell(new Phrase(values, cellFont2));
                        cell2.BorderColor = new BaseColor(31, 78, 121);
                        cell2.MinimumHeight = 25;
                        cell2.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        cell2.Padding = cellPadding;
                        table.AddCell(cell2);
                    }

                    cell1 = new PdfPCell(new Phrase("Total Combinations: " + totalCombinatins.ToString(), cellFont3));
                    cell1.Border = 0;
                    cell1.Colspan = 2;
                    cell1.MinimumHeight = 25;
                    cell1.BorderColor = new BaseColor(31, 78, 121);
                    cell1.BackgroundColor = new BaseColor(31, 78, 121);
                    cell1.BorderWidth = 0.2f;
                    cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    cell1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    table.AddCell(cell1);

                    /*row space*/
                    cell1 = new PdfPCell(new Phrase("\n", cellFont3));
                    cell1.Border = 0;
                    cell1.Colspan = 2;
                    cell1.MinimumHeight = 20;
                    table.AddCell(cell1);
                    if (totalCombinatins > 0)
                        total *= totalCombinatins;
                }
                totalRuns += total;
                paragraph = new Paragraph(new Phrase("Total Data Combinations for " + workflowPath.Title + ": " + total.ToString() + " \n ", topCellFon3));
                document.Add(paragraph);
                document.Add(table);
            }
            catch (Exception ex)
            { }
        }

        private void AddLastPage(Document document)
        {
            try
            {
                string title = _section.ToString() + " \t	General Sense.AI Statistics ";
                Chunk chTitle = new Chunk(title, fontHeading1);
                chTitle.SetGenericTag(title);
                Paragraph paragraph = new Paragraph(chTitle);
                paragraph.SpacingAfter = speaceHeading1;
                document.Add(paragraph);

                PdfPTable pdfPTable = new PdfPTable(2);
                float[] widths = new float[] { 30f, 70f };
                PdfPTable table = new PdfPTable(2);
                table.WidthPercentage = 100;
                table.SetWidths(widths);

                var cellFont1 = FontFactory.GetFont("Calibri Light", 18, Font.BOLD, new BaseColor(217, 217, 217));
                var cellFont2 = FontFactory.GetFont("Calibri Light", 18, new BaseColor(68, 84, 106));

                /*Row project*/

                PdfPCell cell1 = new PdfPCell(new Phrase("PROJECT", cellFont1));
                cell1.Border = 0;
                cell1.MinimumHeight = 30;
                cell1.BorderColor = new BaseColor(213, 220, 228);
                cell1.BackgroundColor = new BaseColor(31, 78, 121);
                cell1.BorderWidth = float.Parse("0.5");
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell1.Padding = cellPadding;
                table.AddCell(cell1);

                PdfPCell cell2 = new PdfPCell(new Phrase(_projectTitle, cellFont2));
                cell2.MinimumHeight = 30;
                cell2.BorderColor = new BaseColor(213, 220, 228);
                cell2.BorderWidth = float.Parse("0.5");
                cell2.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell2.Padding = cellPadding;
                table.AddCell(cell2);

                /*Row sub project*/
                cell1 = new PdfPCell(new Phrase("SUBPROJECT", cellFont1));
                cell1.MinimumHeight = 30;
                cell1.BorderColor = new BaseColor(213, 220, 228);
                cell1.BackgroundColor = new BaseColor(31, 78, 121);
                cell1.BorderWidth = float.Parse("0.5");
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell1.Padding = cellPadding;
                table.AddCell(cell1);

                cell2 = new PdfPCell(new Phrase(_subProjectTitle, cellFont2));
                cell2.MinimumHeight = 30;
                cell2.BorderColor = new BaseColor(213, 220, 228);
                cell2.BorderWidth = float.Parse("0.5");
                cell2.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell2.Padding = cellPadding;
                table.AddCell(cell2);

                /*Row hierarchy*/
                string hiearchyPath = _hierarchyPathLastPage.Substring(0, _hierarchyPathLastPage.Length - 1).Trim();
                string aaaa = hiearchyPath.Substring(0, hiearchyPath.Length - 1).Trim();
                string[] hh = aaaa.Split("/");
                foreach (string s in hh)
                {
                    string[] hiearchTypeAndHier = s.Split(':');

                    cell1 = new PdfPCell(new Phrase(hiearchTypeAndHier[0], cellFont1));
                    cell1.MinimumHeight = 30;
                    cell1.BackgroundColor = new BaseColor(31, 78, 121);
                    cell1.Border = PdfPCell.NO_BORDER;
                    table.AddCell(cell1);

                    cell2 = new PdfPCell(new Phrase(hiearchTypeAndHier[1], cellFont2));
                    cell2.MinimumHeight = 30;
                    cell2.BorderColor = new BaseColor(213, 220, 228);
                    cell2.BackgroundColor = new BaseColor(255, 255, 255);
                    cell2.Border = PdfPCell.NO_BORDER;
                    cell2.Border = PdfPCell.RIGHT_BORDER;
                    cell2.BorderWidth = float.Parse("0.5");
                    table.AddCell(cell2);
                }

                /*Row systems*/
                cell1 = new PdfPCell(new Phrase("SYSTEMS", cellFont1));
                cell1.MinimumHeight = 30;
                cell1.BorderColor = new BaseColor(213, 220, 228);
                cell1.BackgroundColor = new BaseColor(31, 78, 121);
                cell1.BorderWidth = float.Parse("0.5");
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell1.Padding = cellPadding;
                table.AddCell(cell1);

                cell2 = new PdfPCell(new Phrase("T24 web", cellFont2));
                cell2.MinimumHeight = 30;
                cell2.BorderColor = new BaseColor(213, 220, 228);
                cell2.BorderWidth = float.Parse("0.5");
                cell2.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell2.Padding = cellPadding;
                table.AddCell(cell2);

                /*Row workflows*/
                cell1 = new PdfPCell(new Phrase("WORKFLOWS", cellFont1));
                cell1.MinimumHeight = 30;
                cell1.BorderColor = new BaseColor(213, 220, 228);
                cell1.BackgroundColor = new BaseColor(31, 78, 121);
                cell1.BorderWidth = float.Parse("0.5");
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell1.Padding = cellPadding;
                table.AddCell(cell1);

                cell2 = new PdfPCell(new Phrase(_workflows.Count().ToString(), cellFont2));
                cell2.MinimumHeight = 30;
                cell2.BorderColor = new BaseColor(213, 220, 228);
                cell2.BorderWidth = float.Parse("0.5");
                cell2.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell2.Padding = cellPadding;
                table.AddCell(cell2);

                /*Row paths*/
                cell1 = new PdfPCell(new Phrase("PATHS", cellFont1));
                cell1.MinimumHeight = 30;
                cell1.BorderColor = new BaseColor(213, 220, 228);
                cell1.BackgroundColor = new BaseColor(31, 78, 121);
                cell1.BorderWidth = float.Parse("0.5");
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell1.Padding = cellPadding;
                table.AddCell(cell1);

                int totalPaths = 0;
                _workflowPaths.ToList().ForEach(f => totalPaths += f.Value.GroupBy(g => g.WorkflowId).Count());
                cell2 = new PdfPCell(new Phrase(totalPaths.ToString(), cellFont2));
                cell2.MinimumHeight = 30;
                cell2.BorderColor = new BaseColor(213, 220, 228);
                cell2.BorderWidth = float.Parse("0.5");
                cell2.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell2.Padding = cellPadding;
                table.AddCell(cell2);

                /*Row test runs*/
                cell1 = new PdfPCell(new Phrase("TEST RUNS", cellFont1));
                cell1.MinimumHeight = 30;
                cell1.BorderColor = new BaseColor(213, 220, 228);
                cell1.BackgroundColor = new BaseColor(31, 78, 121);
                cell1.BorderWidth = float.Parse("0.5");
                cell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell1.Padding = cellPadding;
                table.AddCell(cell1);

                cell2 = new PdfPCell(new Phrase(totalRuns.ToString(), cellFont2));
                cell2.MinimumHeight = 30;
                cell2.BorderColor = new BaseColor(213, 220, 228);
                cell2.BorderWidth = float.Parse("0.5");
                cell2.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                cell2.Padding = cellPadding;
                table.AddCell(cell2);

                document.Add(table);
            }
            catch (Exception ex)
            { }
        }

        private void CopyDocument(string source, string target)
        {
            try
            {
                _section = 3;
                if (!string.IsNullOrEmpty(_processIamge))
                    _section = 4;
                PdfReader reader = new PdfReader(source);

                Document document = new Document(PageSize.A4, 30, 30, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(target, FileMode.Create));
                _events = new ITextEvents();
                writer.PageEvent = _events;
                document.Open();

                FirstPage(document);
                document.NewPage();
                AddTableOfContents(document, writer);
                document.NewPage();
                AddAdapteres(document);
                document.NewPage();
                AddRequirement(document);
                PdfContentByte cb = writer.DirectContent;

                for (int p = 5; p <= reader.NumberOfPages; p++)
                {
                    _events.IsLastPage = true;
                    document.SetPageSize(reader.GetPageSize(p));

                    if (p == wfSection)
                    {
                        document.NewPage();
                        p = wfEndSection;
                        AddWorkflows(document);
                        continue;
                    }
                    if (p == wfSection + 1)
                    {
                        _events.IsLastPage = false;
                    }

                    document.NewPage();
                    PdfImportedPage pageImport = writer.GetImportedPage(reader, p);

                    cb.AddTemplate(pageImport, 1f, 0, 0, 1f, 0, 0);
                }

                document.Close();
                reader.Close();
                File.Delete(source);
            }
            catch (Exception ex)
            { }
        }
    }

}