﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class workflowversions_requireid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ExecutionResults",
                table: "ExecutionResults");

            migrationBuilder.DropColumn(
                name: "RequirementId",
                table: "WorkflowVersions");

            migrationBuilder.AddColumn<decimal>(
                name: "ExecutionTestCaseId",
                table: "ExecutionResults",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExecutionResults",
                table: "ExecutionResults",
                columns: new[] { "WorkflowId", "TestCaseId", "TestCycleId", "ExecutionTestCaseId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ExecutionResults",
                table: "ExecutionResults");

            migrationBuilder.DropColumn(
                name: "ExecutionTestCaseId",
                table: "ExecutionResults");

            migrationBuilder.AddColumn<long>(
                name: "RequirementId",
                table: "WorkflowVersions",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExecutionResults",
                table: "ExecutionResults",
                columns: new[] { "WorkflowId", "TestCaseId", "TestCycleId" });
        }
    }
}
