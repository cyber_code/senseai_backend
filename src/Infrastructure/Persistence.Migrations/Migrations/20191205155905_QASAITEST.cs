﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class QASAITEST : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsJiraIntegrationEnabled",
                table: "SubProjects",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "JiraLink",
                table: "SubProjects",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsJiraIntegrationEnabled",
                table: "SubProjects");

            migrationBuilder.DropColumn(
                name: "JiraLink",
                table: "SubProjects");
        }
    }
}
