﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class removeSystemData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Systems",
                keyColumn: "Id",
                keyValue: new Guid("2e28b3db-9f0b-4229-8084-04289d910f07"));

            migrationBuilder.DeleteData(
                table: "Systems",
                keyColumn: "Id",
                keyValue: new Guid("d0e7b0df-e87f-4dbc-8fb4-419cab45d3d5"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Systems",
                columns: new[] { "Id", "AdapterName", "DateCreated", "DateModified", "Title", "UserCreated", "UserModified" },
                values: new object[,]
                {
                    { new Guid("2e28b3db-9f0b-4229-8084-04289d910f07"), "T24WebAdapter", null, null, "R17 Author System", null, null },
                    { new Guid("d0e7b0df-e87f-4dbc-8fb4-419cab45d3d5"), "T24WebAdapter", null, null, "R17 Input System", null, null }
                });
        }
    }
}
