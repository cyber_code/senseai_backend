﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class WorkflowFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isIssued",
                table: "Workflows",
                nullable: false,
                defaultValue: false);
            migrationBuilder.AddColumn<bool>(
                name: "generatedTestCases",
                table: "Workflows",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isIssued",
                table: "Workflows");
            migrationBuilder.DropColumn(
                name: "generatedTestCases",
                table: "Workflows");
        }
    }
}
