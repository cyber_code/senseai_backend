﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class AddHierarchyTypeIdandParentIdOnJiraProjectMapping : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "HierarchyParentId",
                table: "JiraProjectMapping",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "HierarchyTypeId",
                table: "JiraProjectMapping",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<bool>(
                name: "MapHierarchy",
                table: "JiraProjectMapping",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HierarchyParentId",
                table: "JiraProjectMapping");

            migrationBuilder.DropColumn(
                name: "HierarchyTypeId",
                table: "JiraProjectMapping");

            migrationBuilder.DropColumn(
                name: "MapHierarchy",
                table: "JiraProjectMapping");
        }
    }
}
