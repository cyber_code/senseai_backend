﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class status : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Sprints",
                nullable: false,
                computedColumnSql: " case when start='0001-01-01 00:00:00.0000000' then 0 when(start < GETDATE() and( [end] >= getdate() or[end] = '0001-01-01 00:00:00.0000000'))then 1 when[end] < GETDATE() then 2  end");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Sprints");
        }
    }
}
