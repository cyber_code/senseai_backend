﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class issuetypesfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.CreateTable(
                name: "IssueTypeFields",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IssueTypeId = table.Column<Guid>(nullable: false),
                    IssueTypeFieldsNameId = table.Column<Guid>(nullable: false),
                    Checked = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                  
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IssueTypeFields", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IssueTypeFields_IssueTypeFieldsNames_IssueTypeFieldsNameId",
                        column: x => x.IssueTypeFieldsNameId,
                        principalTable: "IssueTypeFieldsNames",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IssueTypeFields_IssueTypes_IssueTypeId",
                        column: x => x.IssueTypeId,
                        principalTable: "IssueTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            

            migrationBuilder.CreateIndex(
                name: "IX_IssueTypeFields_IssueTypeFieldsNameId",
                table: "IssueTypeFields",
                column: "IssueTypeFieldsNameId");

            migrationBuilder.CreateIndex(
                name: "IX_IssueTypeFields_IssueTypeId",
                table: "IssueTypeFields",
                column: "IssueTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IssueTypeFields");

           
        }
    }
}
