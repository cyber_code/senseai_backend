﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class BaseData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "QueryParameters",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "QueryParameters",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "QueryParameters",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "QueryParameters",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "Queries",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "PropertyClasses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "PropertyClasses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "PropertyClasses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "PropertyClasses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "Properties",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "Properties",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "Properties",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "Properties",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "ProductGroups",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "ProductGroups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "ProductGroups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "ProductGroups",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyStates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyStates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyStates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyStates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyFieldStates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyFieldStates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyFieldStates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyFieldStates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyClasses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyClasses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyClasses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyClasses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "Parameters",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "Parameters",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "Parameters",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "Parameters",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "Fields",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "Fields",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "Fields",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "Fields",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "TypicalChanges",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "TypicalChanges",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                table: "TypicalChanges",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                table: "TypicalChanges",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Processes",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 300,
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "AcceptedTypicalChanges",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "AcceptedTypicalChanges",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                table: "AcceptedTypicalChanges",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                table: "AcceptedTypicalChanges",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "QueryParameters");

            migrationBuilder.DropColumn(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "QueryParameters");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "QueryParameters");

            migrationBuilder.DropColumn(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "QueryParameters");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "Queries");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "PropertyClasses");

            migrationBuilder.DropColumn(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "PropertyClasses");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "PropertyClasses");

            migrationBuilder.DropColumn(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "PropertyClasses");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "Properties");

            migrationBuilder.DropColumn(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "Properties");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "Properties");

            migrationBuilder.DropColumn(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "Properties");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "ProductGroups");

            migrationBuilder.DropColumn(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "ProductGroups");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "ProductGroups");

            migrationBuilder.DropColumn(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "ProductGroups");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyStates");

            migrationBuilder.DropColumn(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyStates");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyStates");

            migrationBuilder.DropColumn(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyStates");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyFieldStates");

            migrationBuilder.DropColumn(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyFieldStates");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyFieldStates");

            migrationBuilder.DropColumn(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyFieldStates");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyClasses");

            migrationBuilder.DropColumn(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyClasses");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyClasses");

            migrationBuilder.DropColumn(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyClasses");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "Parameters");

            migrationBuilder.DropColumn(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "Parameters");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "Parameters");

            migrationBuilder.DropColumn(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "Parameters");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                schema: "AAProductBuilder",
                table: "Fields");

            migrationBuilder.DropColumn(
                name: "DateModified",
                schema: "AAProductBuilder",
                table: "Fields");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                schema: "AAProductBuilder",
                table: "Fields");

            migrationBuilder.DropColumn(
                name: "UserModified",
                schema: "AAProductBuilder",
                table: "Fields");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "TypicalChanges");

            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "TypicalChanges");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                table: "TypicalChanges");

            migrationBuilder.DropColumn(
                name: "UserModified",
                table: "TypicalChanges");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "UserModified",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "AcceptedTypicalChanges");

            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "AcceptedTypicalChanges");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                table: "AcceptedTypicalChanges");

            migrationBuilder.DropColumn(
                name: "UserModified",
                table: "AcceptedTypicalChanges");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Processes",
                maxLength: 300,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
