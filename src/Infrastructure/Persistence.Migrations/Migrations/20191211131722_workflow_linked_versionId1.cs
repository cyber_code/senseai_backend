﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class workflow_linked_versionId1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkflowVersionId",
                table: "WorkflowItems",
                newName: "WorkflowLinkVersionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkflowLinkVersionId",
                table: "WorkflowItems",
                newName: "WorkflowVersionId");
        }
    }
}
