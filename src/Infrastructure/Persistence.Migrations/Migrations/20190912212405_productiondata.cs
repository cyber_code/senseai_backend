﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class productiondata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductionData",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "UserModified",
                table: "ProductionData");

            migrationBuilder.AlterColumn<string>(
                name: "Xml",
                table: "ProductionData",
                type: "xml",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Key",
                table: "ProductionData",
                maxLength: 500,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Json",
                table: "ProductionData",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationName",
                table: "ProductionData",
                maxLength: 500,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Id1",
                table: "ProductionData", 
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductionData",
                table: "ProductionData",
                column: "Id1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductionData",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "Id1",
                table: "ProductionData");

            migrationBuilder.AlterColumn<string>(
                name: "Xml",
                table: "ProductionData",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "xml");

            migrationBuilder.AlterColumn<string>(
                name: "Key",
                table: "ProductionData",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 500);

            migrationBuilder.AlterColumn<string>(
                name: "Json",
                table: "ProductionData",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationName",
                table: "ProductionData",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 500);

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "ProductionData",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserModified",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductionData",
                table: "ProductionData",
                column: "Id");
        }
    }
}
