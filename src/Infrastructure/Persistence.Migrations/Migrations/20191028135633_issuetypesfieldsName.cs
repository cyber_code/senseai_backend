﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class issuetypesfieldsName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IssueTypeFieldsNames",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 300, nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                   
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IssueTypeFieldsNames", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "IssueTypeFieldsNames",
                columns: new[] { "Id", "DateCreated", "DateModified", "Name", "UserCreated", "UserModified" },
                values: new object[,]
                {
                    
                 
                    { new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), null, null, "Priority", null, null },
                    { new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), null, null, "Severity", null, null },
                 
                    { new Guid("68c49518-35b2-4691-9524-95be5ba93723"), null, null, "Approval Date", null, null },
                 
                    { new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), null, null, "Approved By", null, null },
                    { new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), null, null, "Identified By", null, null },
                    { new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), null, null, "Workflow", null, null },
                    { new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), null, null, "Change Status", null, null },
                    { new Guid("ada7c874-1922-44ab-981d-adf596beece2"), null, null, "Approved For Investigation", null, null },
                    { new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), null, null, "Attribute", null, null },
                    { new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), null, null, "Application", null, null },
                    { new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), null, null, "Comment", null, null },
                    { new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), null, null, "Implication", null, null },
                    { new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), null, null, "Reason", null, null },
                    { new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), null, null, "Estimate", null, null },
                    { new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), null, null, "Status", null, null },
                    { new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), null, null, "Investigation Status", null, null },
                    { new Guid("401518ad-59a4-4607-b53c-61a232adca62"), null, null, "Defect Type", null, null },

                    { new Guid("8ad0be32-31e0-43ee-8c4f-c15c145b026f"), null, null, "TestCaseId", null, null },
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IssueTypeFieldsNames");
        }
    }
}
