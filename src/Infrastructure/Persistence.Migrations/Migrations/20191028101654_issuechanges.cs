﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class issuechanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Default",
                table: "IssueTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Unit",
                table: "IssueTypes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ApprovalDate",
                table: "Issues",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "ApprovedForInvestigation",
                table: "Issues",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "ApprovedId",
                table: "Issues",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Attribute",
                table: "Issues",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ChangeStatus",
                table: "Issues",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "Issues",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ComponentId",
                table: "Issues",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateSubmitted",
                table: "Issues",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DueDate",
                table: "Issues",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Implication",
                table: "Issues",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InvestigationStatus",
                table: "Issues",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Priority",
                table: "Issues",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Reason",
                table: "Issues",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ReporterId",
                table: "Issues",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "Severity",
                table: "Issues",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "Issues",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "Issues",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "TypicalId",
                table: "Issues",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Default",
                table: "IssueTypes");

            migrationBuilder.DropColumn(
                name: "Unit",
                table: "IssueTypes");

            migrationBuilder.DropColumn(
                name: "ApprovalDate",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "ApprovedForInvestigation",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "ApprovedId",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "Attribute",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "ChangeStatus",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "Comment",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "ComponentId",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "DateSubmitted",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "DueDate",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "Implication",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "InvestigationStatus",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "Priority",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "Reason",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "ReporterId",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "Severity",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "TypicalId",
                table: "Issues");
        }
    }
}
