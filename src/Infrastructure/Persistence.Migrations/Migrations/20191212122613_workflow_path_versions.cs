﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class workflow_path_versions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkflowPaths");

            migrationBuilder.CreateTable(
                name: "WorkflowVersionPathDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    VersionId = table.Column<int>(nullable: false),
                    PathId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    ActionType = table.Column<int>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    TypicalId = table.Column<Guid>(nullable: false),
                    TypicalName = table.Column<string>(nullable: false),
                    TypicalType = table.Column<string>(nullable: false),
                    DataSetId = table.Column<Guid>(nullable: false),
                    Index = table.Column<int>(nullable: false),
                    PathItemJson = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowVersionPathDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowVersionPaths",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    VersionId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Selected = table.Column<bool>(nullable: false),
                    Coverage = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    Index = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowVersionPaths", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkflowVersionPathDetails");

            migrationBuilder.DropTable(
                name: "WorkflowVersionPaths");

            migrationBuilder.CreateTable(
                name: "WorkflowPaths",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ActionType = table.Column<int>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    Coverage = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    DataSetId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    IsValid = table.Column<bool>(nullable: false),
                    PathId = table.Column<Guid>(nullable: false),
                    PathItemId = table.Column<Guid>(nullable: false),
                    PathItemTitle = table.Column<string>(nullable: false),
                    PathItemType = table.Column<int>(nullable: false),
                    PathTitle = table.Column<string>(nullable: false),
                    Selected = table.Column<bool>(nullable: false),
                    TCIndex = table.Column<int>(nullable: false),
                    TSIndex = table.Column<int>(nullable: false),
                    TestStepJson = table.Column<string>(nullable: false),
                    TypicalId = table.Column<Guid>(nullable: false),
                    TypicalName = table.Column<string>(nullable: false),
                    TypicalType = table.Column<string>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    WorkflowId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowPaths", x => x.Id);
                });
        }
    }
}
