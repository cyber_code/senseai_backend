﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class tasktype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "IssueTypes",
                columns: new[] { "Id", "DateCreated", "DateModified", "Default", "Description", "SubProjectId", "Title", "Unit", "UserCreated", "UserModified" },
                values: new object[,]
                {
                   
                    { new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null, true, "Task QA", new Guid("42AFEB37-279E-42F4-8621-5578BAB03DE1"), "Task QA", "Days", null, null },
             
                    { new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null, true, "Task QA", new Guid("d6b208f6-4eca-4f24-a0b8-dd94462dd91a"), "Task QA", "Days", null, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.DeleteData(
                table: "IssueTypes",
                keyColumn: "Id",
                keyValue: new Guid("d498805a-a2bc-4a51-9784-da56b0828464"));

            migrationBuilder.DeleteData(
                table: "IssueTypes",
                keyColumn: "Id",
                keyValue: new Guid("d5f939b3-441b-4529-b123-65d361425dae"));

           
        }
    }
}
