﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class workflow_path_versions_pk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WorkflowVersionPaths",
                table: "WorkflowVersionPaths");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WorkflowVersionPathDetails",
                table: "WorkflowVersionPathDetails");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WorkflowVersionPaths",
                table: "WorkflowVersionPaths",
                columns: new[] { "Id", "WorkflowId", "VersionId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WorkflowVersionPathDetails",
                table: "WorkflowVersionPathDetails",
                columns: new[] { "Id", "WorkflowId", "VersionId", "PathId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WorkflowVersionPaths",
                table: "WorkflowVersionPaths");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WorkflowVersionPathDetails",
                table: "WorkflowVersionPathDetails");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WorkflowVersionPaths",
                table: "WorkflowVersionPaths",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WorkflowVersionPathDetails",
                table: "WorkflowVersionPathDetails",
                column: "Id");
        }
    }
}
