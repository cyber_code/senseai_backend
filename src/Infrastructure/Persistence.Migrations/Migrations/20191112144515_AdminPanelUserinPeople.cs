﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class AdminPanelUserinPeople : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      

            migrationBuilder.AddColumn<int>(
                name: "UserIdFromAdminPanel",
                table: "Peoples",
                nullable: true,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
       
            migrationBuilder.DropColumn(
                name: "UserIdFromAdminPanel",
                table: "Peoples");
        }
    }
}
