﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class AddColumnParentIdOnDatasetObject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ParentId",
                table: "DataSets",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "DataSets");
        }
    }
}
