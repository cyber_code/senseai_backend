﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class ats_testcases : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ATSTestCaseNoderef",
                table: "WorkflowTestCases");

            migrationBuilder.DropColumn(
                name: "ATSTestCaseUId",
                table: "WorkflowTestCases");

            migrationBuilder.DropColumn(
                name: "ATSTestStepJson",
                table: "WorkflowTestCases");

            migrationBuilder.DropColumn(
                name: "ATSTestStepNoderef",
                table: "WorkflowTestCases");

            migrationBuilder.DropColumn(
                name: "ATSTestStepUId",
                table: "WorkflowTestCases");

            migrationBuilder.CreateTable(
                name: "ATSTestCases",
                columns: table => new
                {
                    Noderef = table.Column<decimal>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    UId = table.Column<string>(nullable: false),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    WorkflowPathId = table.Column<Guid>(nullable: false),
                    VersionId = table.Column<int>(nullable: false),
                    TestCaseId = table.Column<Guid>(nullable: false),
                    DataSetId = table.Column<Guid>(nullable: false),
                    DataSetNoderef = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ATSTestCases", x => x.Noderef);
                });

            migrationBuilder.CreateTable(
                name: "ATSTestSteps",
                columns: table => new
                {
                    Noderef = table.Column<decimal>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    UId = table.Column<string>(nullable: false),
                    TestCaseNoderef = table.Column<decimal>(nullable: false),
                    TestStepId = table.Column<Guid>(nullable: false),
                    TestStepJson = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ATSTestSteps", x => x.Noderef);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ATSTestCases");

            migrationBuilder.DropTable(
                name: "ATSTestSteps");

            migrationBuilder.AddColumn<decimal>(
                name: "ATSTestCaseNoderef",
                table: "WorkflowTestCases",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "ATSTestCaseUId",
                table: "WorkflowTestCases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ATSTestStepJson",
                table: "WorkflowTestCases",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ATSTestStepNoderef",
                table: "WorkflowTestCases",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "ATSTestStepUId",
                table: "WorkflowTestCases",
                nullable: true);
        }
    }
}
