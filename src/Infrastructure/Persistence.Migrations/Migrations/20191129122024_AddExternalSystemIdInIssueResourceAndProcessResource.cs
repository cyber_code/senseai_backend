﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class AddExternalSystemIdInIssueResourceAndProcessResource : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ExternalSystemId",
                table: "ProcessResources",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExternalSystemId",
                table: "IssueResources",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExternalSystemId",
                table: "ProcessResources");

            migrationBuilder.DropColumn(
                name: "ExternalSystemId",
                table: "IssueResources");
        }
    }
}
