﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class JiraIntegrationIssueTypeChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "JiraIssueTypeField",
                table: "JiraIssueTypeMapping",
                newName: "JiraIssueTypeFieldName");

            migrationBuilder.AddColumn<string>(
                name: "JiraIssueTypeFieldId",
                table: "JiraIssueTypeMapping",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "JiraIssueTypeFieldId",
                table: "JiraIssueTypeMapping");

            migrationBuilder.RenameColumn(
                name: "JiraIssueTypeFieldName",
                table: "JiraIssueTypeMapping",
                newName: "JiraIssueTypeField");
        }
    }
}
