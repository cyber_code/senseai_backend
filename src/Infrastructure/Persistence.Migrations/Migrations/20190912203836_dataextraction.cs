﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class dataextraction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductionData_Typicals_TypicalId",
                table: "ProductionData");

            migrationBuilder.DropIndex(
                name: "IX_ProductionData_TypicalId",
                table: "ProductionData");

            migrationBuilder.RenameColumn(
                name: "TypicalId",
                table: "ProductionData",
                newName: "CatalogId");

            migrationBuilder.AddColumn<string>(
                name: "ApplicationName",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "ProductionData",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Key",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Xml",
                table: "ProductionData",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TAFJLog",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    Code = table.Column<string>(maxLength: 100, nullable: false),
                    User = table.Column<string>(maxLength: 100, nullable: false),
                    Action = table.Column<string>(maxLength: 100, nullable: false),
                    RequestType = table.Column<string>(maxLength: 100, nullable: false),
                    Command = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Data = table.Column<string>(type: "xml", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TAFJLog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ValidActivities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    ProductLine = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Json = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ValidActivities", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TAFJLog");

            migrationBuilder.DropTable(
                name: "ValidActivities");

            migrationBuilder.DropColumn(
                name: "ApplicationName",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "Key",
                table: "ProductionData");

            migrationBuilder.DropColumn(
                name: "Xml",
                table: "ProductionData");

            migrationBuilder.RenameColumn(
                name: "CatalogId",
                table: "ProductionData",
                newName: "TypicalId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionData_TypicalId",
                table: "ProductionData",
                column: "TypicalId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionData_Typicals_TypicalId",
                table: "ProductionData",
                column: "TypicalId",
                principalTable: "Typicals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
