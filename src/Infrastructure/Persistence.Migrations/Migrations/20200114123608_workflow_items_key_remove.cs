﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class workflow_items_key_remove : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WorkflowItems",
                table: "WorkflowItems");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WorkflowItems",
                table: "WorkflowItems",
                columns: new[] { "Id", "WorkflowId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WorkflowItems",
                table: "WorkflowItems");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WorkflowItems",
                table: "WorkflowItems",
                columns: new[] { "Id", "WorkflowId", "WorkflowLinkId", "WorkflowLinkVersionId" });
        }
    }
}
