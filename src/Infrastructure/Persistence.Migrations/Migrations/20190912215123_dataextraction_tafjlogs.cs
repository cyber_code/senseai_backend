﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class dataextraction_tafjlogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_TAFJLog",
                table: "TAFJLog");

            migrationBuilder.RenameTable(
                name: "TAFJLog",
                newName: "TAFJLogs");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TAFJLogs",
                table: "TAFJLogs",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_TAFJLogs",
                table: "TAFJLogs");

            migrationBuilder.RenameTable(
                name: "TAFJLogs",
                newName: "TAFJLog");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TAFJLog",
                table: "TAFJLog",
                column: "Id");
        }
    }
}
