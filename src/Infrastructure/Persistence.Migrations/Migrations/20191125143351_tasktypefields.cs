﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class tasktypefields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
          

            migrationBuilder.InsertData(
                table: "IssueTypeFields",
                columns: new[] { "Id", "Checked", "DateCreated", "DateModified", "IssueTypeFieldsNameId", "IssueTypeId", "UserCreated", "UserModified" },
                values: new object[,]
                {
                    { new Guid("0ea2c4c5-2e82-4434-9b32-5842cda9a79d"), false, null, null, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("e003ddbb-0eb4-42b5-aa51-70aee48259b3"), false, null, null, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("2127a0c5-a258-4ea3-983e-b46e8ec278dd"), false, null, null, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("2fe3ff33-fc33-42a8-b05c-87003ba8f196"), true, null, null, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("18ea5cc8-0cbb-4332-bfb1-c39e7659e191"), true, null, null, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("dc69959d-ac88-47fa-a4e6-cd990e25cf00"), true, null, null, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("3582ca56-6510-411c-8775-068f4b872c72"), false, null, null, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("0c618256-f05f-4e54-8578-45c14f7456d9"), false, null, null, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("c20f4bdf-06eb-4421-a41b-87842cdae012"), false, null, null, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("6929f729-22f6-46a3-93f6-b3374ffe35cb"), false, null, null, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("4ae6b39e-743d-4f3c-b31e-c5efa123d2f4"), false, null, null, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("146f012d-08f5-4491-ba69-ebb088470756"), false, null, null, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("c96248ab-c794-45e9-8c60-ccbaad65c459"), true, null, null, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("665c213f-5b22-4d88-8cc2-3eee7dd07231"), true, null, null, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("bf410166-637f-471c-ae22-8fe1e498d6b4"), false, null, null, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("7eea2724-2cb1-462a-9cfe-aa6f84377458"), false, null, null, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("d8a71ed8-f286-465e-a601-8dbdd9c8519a"), false, null, null, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("6ea4123c-db61-4495-be51-ddb4f61ca06e"), true, null, null, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("01e8e637-0038-4f5d-90bf-3d66c8735d50"), false, null, null, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("3a69b98d-bc42-41a4-90fc-b4d05aeece44"), false, null, null, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("3e0c7a95-f567-43c2-947e-b13ae039c5d0"), false, null, null, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("1fe90f63-a54a-4266-91c4-9ff8042a5fd1"), true, null, null, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("4dc682a7-b2d7-4f49-8df7-01ff8b937ef5"), true, null, null, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("f7c9c02c-b0dc-41ca-a5f7-6eafbfbc469a"), false, null, null, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("b94b5185-34af-4895-9c68-e8903c1ae831"), false, null, null, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("50b01b30-5a4b-48eb-a048-48d2253b48d9"), false, null, null, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("c4883bfa-b15a-404a-a115-14e17dda889a"), false, null, null, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("68edd871-0924-421e-88a7-3ad770d3e5c9"), false, null, null, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("d88bf2c7-2b61-4e11-87c2-ba60c3818833"), false, null, null, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("6c0884c0-04a7-42e3-a5af-6f5b9385eecb"), false, null, null, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("4140ec68-801e-43bf-a91b-e86c9f7194ca"), true, null, null, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("69385ffd-6636-42be-9f3c-d910e944d0d2"), true, null, null, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), null, null },
                    { new Guid("38084d62-d5e0-468d-bf14-cc1d5b84be5d"), false, null, null, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null },
                    { new Guid("c5040b29-db73-4817-b906-89a7a885cd59"), false, null, null, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), new Guid("d5f939b3-441b-4529-b123-65d361425dae"), null, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("01e8e637-0038-4f5d-90bf-3d66c8735d50"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("0c618256-f05f-4e54-8578-45c14f7456d9"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("0ea2c4c5-2e82-4434-9b32-5842cda9a79d"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("11c8c476-e8eb-4d4b-b9d2-eb61dd67fc66"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("146f012d-08f5-4491-ba69-ebb088470756"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("18ea5cc8-0cbb-4332-bfb1-c39e7659e191"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("1fe90f63-a54a-4266-91c4-9ff8042a5fd1"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("2127a0c5-a258-4ea3-983e-b46e8ec278dd"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("2fe3ff33-fc33-42a8-b05c-87003ba8f196"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("3582ca56-6510-411c-8775-068f4b872c72"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("38084d62-d5e0-468d-bf14-cc1d5b84be5d"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("3a69b98d-bc42-41a4-90fc-b4d05aeece44"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("3e0c7a95-f567-43c2-947e-b13ae039c5d0"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("4140ec68-801e-43bf-a91b-e86c9f7194ca"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("4ae6b39e-743d-4f3c-b31e-c5efa123d2f4"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("4dc682a7-b2d7-4f49-8df7-01ff8b937ef5"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("4e255d78-c23b-45d8-b6ce-ee583347960d"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("50b01b30-5a4b-48eb-a048-48d2253b48d9"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("587fd8a0-bc25-4224-9a94-1082ba77d374"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("665c213f-5b22-4d88-8cc2-3eee7dd07231"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("68edd871-0924-421e-88a7-3ad770d3e5c9"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("6929f729-22f6-46a3-93f6-b3374ffe35cb"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("69385ffd-6636-42be-9f3c-d910e944d0d2"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("6c0884c0-04a7-42e3-a5af-6f5b9385eecb"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("6ea4123c-db61-4495-be51-ddb4f61ca06e"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("7eea2724-2cb1-462a-9cfe-aa6f84377458"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("a4eef513-6189-490a-be35-a240a4caef42"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("b94b5185-34af-4895-9c68-e8903c1ae831"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("bf410166-637f-471c-ae22-8fe1e498d6b4"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("c20f4bdf-06eb-4421-a41b-87842cdae012"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("c4883bfa-b15a-404a-a115-14e17dda889a"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("c5040b29-db73-4817-b906-89a7a885cd59"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("c96248ab-c794-45e9-8c60-ccbaad65c459"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d470a479-6b33-49ea-b5af-106cee3a3aa1"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d88bf2c7-2b61-4e11-87c2-ba60c3818833"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d8a71ed8-f286-465e-a601-8dbdd9c8519a"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("db93084f-0f18-4ead-a32d-3b40cf65c36d"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("dc69959d-ac88-47fa-a4e6-cd990e25cf00"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("e003ddbb-0eb4-42b5-aa51-70aee48259b3"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("f7c9c02c-b0dc-41ca-a5f7-6eafbfbc469a"));

          
        }
    }
}
