﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class senseai_init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
             
            migrationBuilder.EnsureSchema(
                name: "AAProductBuilder");

            migrationBuilder.CreateTable(
                name: "AcceptedTypicalChanges",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    OldXml = table.Column<string>(nullable: true),
                    NewXml = table.Column<string>(nullable: true),
                    OldJson = table.Column<string>(nullable: true),
                    NewJson = table.Column<string>(nullable: true),
                    OldXmlHash = table.Column<string>(nullable: false),
                    NewXmlHash = table.Column<string>(nullable: false),
                    TypicalId = table.Column<Guid>(nullable: false),
                    TypicalName = table.Column<string>(nullable: true),
                    VersionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AcceptedTypicalChanges", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ArisWorkflowItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ArisWorkflowId = table.Column<Guid>(nullable: false),
                    ArisWorkflowLinkId = table.Column<Guid>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    TypicalName = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArisWorkflowItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ArisWorkflows",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProcessId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DesignerJson = table.Column<string>(nullable: true),
                    IsParsed = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArisWorkflows", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ArisWorkflowVersions",
                columns: table => new
                {
                    ArisWorkflowId = table.Column<Guid>(nullable: false),
                    VersionId = table.Column<int>(nullable: false),
                    ProcessId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DesignerJson = table.Column<string>(nullable: true),
                    WorkflowImage = table.Column<string>(nullable: true),
                    IsLast = table.Column<bool>(nullable: false),
                    IsParsed = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArisWorkflowVersions", x => new { x.ArisWorkflowId, x.VersionId });
                });

            migrationBuilder.CreateTable(
                name: "Catalogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProjectId = table.Column<Guid>(nullable: false),
                    SubProjectId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catalogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DataSets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SubProjectId = table.Column<Guid>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    SystemId = table.Column<Guid>(nullable: false),
                    TypicalId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Coverage = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    Combinations = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataSets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DataSetsItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DataSetId = table.Column<Guid>(nullable: false),
                    Row = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataSetsItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DynamicDataSuggestions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SourceTypicalName = table.Column<string>(nullable: true),
                    SourceTypicalAttributeName = table.Column<string>(nullable: true),
                    TargetTypicalName = table.Column<string>(nullable: true),
                    TargetTypicalAttributeName = table.Column<string>(nullable: true),
                    Frequency = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DynamicDataSuggestions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EnquiryActions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    TypicalName = table.Column<string>(nullable: false),
                    Actions = table.Column<string>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnquiryActions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeneratedTestCases",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    WorkflowPathId = table.Column<Guid>(nullable: false),
                    TestCaseId = table.Column<Guid>(nullable: false),
                    TestCaseTitle = table.Column<string>(nullable: false),
                    TCIndex = table.Column<int>(nullable: false),
                    TestStepId = table.Column<Guid>(nullable: false),
                    TestStepTitle = table.Column<string>(nullable: false),
                    TestStepJson = table.Column<string>(nullable: false),
                    TypicalName = table.Column<string>(nullable: false),
                    TypicalId = table.Column<Guid>(nullable: false),
                    TSIndex = table.Column<int>(nullable: false),
                    TestStepType = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneratedTestCases", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GenericWorkflowItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GenericWorkflowId = table.Column<Guid>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenericWorkflowItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GenericWorkflows",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProcessId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DesignerJson = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenericWorkflows", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GenericWorkflowVersions",
                columns: table => new
                {
                    GenericWorkflowId = table.Column<Guid>(nullable: false),
                    VersionId = table.Column<int>(nullable: false),
                    ProcessId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DesignerJson = table.Column<string>(nullable: true),
                    WorkflowImage = table.Column<string>(nullable: true),
                    IsLast = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenericWorkflowVersions", x => new { x.GenericWorkflowId, x.VersionId });
                });

            migrationBuilder.CreateTable(
                name: "HierarchyTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ParentId = table.Column<Guid>(nullable: false),
                    SubProjectId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 300, nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HierarchyTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Issues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProcessId = table.Column<Guid>(nullable: false),
                    SubProjectId = table.Column<Guid>(nullable: false),
                    IssueTypeId = table.Column<Guid>(nullable: false),
                    AssignedId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Status = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Issues", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IssueTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SubProjectId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IssueTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Navigations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Navigations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Processes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SubProjectId = table.Column<Guid>(nullable: false),
                    HierarchyId = table.Column<Guid>(nullable: false),
                    CreatedId = table.Column<Guid>(nullable: false),
                    OwnerId = table.Column<Guid>(nullable: false),
                    ProcessType = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 300, nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Processes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProcessResources",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FileName = table.Column<string>(maxLength: 300, nullable: false),
                    ProcessId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProcessWorkflows",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProcessId = table.Column<Guid>(nullable: false),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessWorkflows", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProjectId = table.Column<Guid>(nullable: false),
                    SubProjectId = table.Column<Guid>(nullable: false),
                    SystemId = table.Column<Guid>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SystemCatalogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SubprojectId = table.Column<Guid>(nullable: false),
                    SystemId = table.Column<Guid>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemCatalogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Systems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    AdapterName = table.Column<string>(maxLength: 300, nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Systems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypicalChanges",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Json = table.Column<string>(nullable: true),
                    Xml = table.Column<string>(nullable: true),
                    XmlHash = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypicalChanges", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypicalInstances",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TypicalId = table.Column<Guid>(nullable: false),
                    Attributes = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypicalInstances", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Typicals",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Json = table.Column<string>(nullable: true),
                    Xml = table.Column<string>(nullable: true),
                    XmlHash = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Typicals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowIssues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    IssueId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowIssues", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowPaths",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    PathId = table.Column<Guid>(nullable: false),
                    PathTitle = table.Column<string>(nullable: false),
                    TCIndex = table.Column<int>(nullable: false),
                    PathItemId = table.Column<Guid>(nullable: false),
                    PathItemTitle = table.Column<string>(nullable: false),
                    PathItemType = table.Column<int>(nullable: false),
                    ActionType = table.Column<int>(nullable: false),
                    TestStepJson = table.Column<string>(nullable: false),
                    CatalogId = table.Column<Guid>(nullable: false),
                    TypicalName = table.Column<string>(nullable: false),
                    TypicalId = table.Column<Guid>(nullable: false),
                    DataSetId = table.Column<Guid>(nullable: false),
                    Coverage = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    Selected = table.Column<bool>(nullable: false),
                    TSIndex = table.Column<int>(nullable: false),
                    IsValid = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowPaths", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowVersions",
                columns: table => new
                {
                    WorkflowId = table.Column<Guid>(nullable: false),
                    VersionId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DesignerJson = table.Column<string>(nullable: true),
                    WorkflowPathsJson = table.Column<string>(nullable: true),
                    TestCasesJson = table.Column<string>(nullable: true),
                    IsLast = table.Column<bool>(nullable: false),
                    IsSyncWithML = table.Column<bool>(nullable: false),
                    WorkflowImage = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true), 
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowVersions", x => new { x.WorkflowId, x.VersionId });
                });

            migrationBuilder.CreateTable(
                name: "Fields",
                schema: "AAProductBuilder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FieldId = table.Column<string>(nullable: true),
                    PropertyClassId = table.Column<string>(nullable: true),
                    PropertyId = table.Column<string>(nullable: true),
                    Json = table.Column<string>(nullable: true),
                    Xml = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fields", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Parameters",
                schema: "AAProductBuilder",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductGroupPropertyClasses",
                schema: "AAProductBuilder",
                columns: table => new
                {
                    ProductGroupId = table.Column<Guid>(nullable: false),
                    PropertyClassId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroupPropertyClasses", x => new { x.ProductGroupId, x.PropertyClassId });
                });

            migrationBuilder.CreateTable(
                name: "ProductGroupPropertyFieldStates",
                schema: "AAProductBuilder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<Guid>(nullable: false),
                    ProductGroupId = table.Column<Guid>(nullable: false),
                    PropertyId = table.Column<string>(nullable: true),
                    FieldId = table.Column<string>(nullable: true),
                    MultiValueIndex = table.Column<int>(nullable: false),
                    SubValueIndex = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    CurrencyId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroupPropertyFieldStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductGroupPropertyStates",
                schema: "AAProductBuilder",
                columns: table => new
                {
                    SessionId = table.Column<Guid>(nullable: false),
                    ProductGroupId = table.Column<Guid>(nullable: false),
                    PropertyId = table.Column<string>(nullable: false),
                    CurrencyId = table.Column<string>(nullable: false),
                    IsSelected = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroupPropertyStates", x => new { x.SessionId, x.ProductGroupId, x.PropertyId, x.CurrencyId });
                });

            migrationBuilder.CreateTable(
                name: "ProductGroups",
                schema: "AAProductBuilder",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Properties",
                schema: "AAProductBuilder",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    PropertyClassId = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Properties", x => new { x.Id, x.PropertyClassId });
                });

            migrationBuilder.CreateTable(
                name: "PropertyClasses",
                schema: "AAProductBuilder",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    IsCurrencySpecific = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyClasses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Queries",
                schema: "AAProductBuilder",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Queries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QueryParameters",
                schema: "AAProductBuilder",
                columns: table => new
                {
                    QueryId = table.Column<string>(nullable: false),
                    ParameterId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueryParameters", x => new { x.QueryId, x.ParameterId });
                });

            migrationBuilder.CreateTable(
                name: "Hierarchies",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ParentId = table.Column<Guid>(nullable: false),
                    SubProjectId = table.Column<Guid>(nullable: false),
                    HierarchyTypeId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 300, nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hierarchies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Hierarchies_HierarchyTypes_HierarchyTypeId",
                        column: x => x.HierarchyTypeId,
                        principalTable: "HierarchyTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubProjects",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProjectId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubProjects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubProjects_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Peoples",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Peoples", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Peoples_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SystemTags",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SystemId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 300, nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SystemTags_Systems_SystemId",
                        column: x => x.SystemId,
                        principalTable: "Systems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionData",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TypicalId = table.Column<Guid>(nullable: false),
                    Json = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductionData_Typicals_TypicalId",
                        column: x => x.TypicalId,
                        principalTable: "Typicals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Folders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SubProjectId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Folders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Folders_SubProjects_SubProjectId",
                        column: x => x.SubProjectId,
                        principalTable: "SubProjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sprints",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Start = table.Column<DateTime>(nullable: false),
                    End = table.Column<DateTime>(nullable: false),
                    SubProjectId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sprints", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sprints_SubProjects_SubProjectId",
                        column: x => x.SubProjectId,
                        principalTable: "SubProjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Workflows",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 300, nullable: false),
                    DesignerJson = table.Column<string>(nullable: true),
                    WorkflowPathsJson = table.Column<string>(nullable: true),
                    FolderId = table.Column<Guid>(nullable: false),
                    IsValid = table.Column<bool>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    IsParsed = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workflows", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Workflows_Folders_FolderId",
                        column: x => x.FolderId,
                        principalTable: "Folders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SprintIssues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SprintId = table.Column<Guid>(nullable: false),
                    IssueId = table.Column<Guid>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SprintIssues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SprintIssues_Sprints_SprintId",
                        column: x => x.SprintId,
                        principalTable: "Sprints",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    CatalogTitle = table.Column<string>(nullable: true),
                    TypicalTitle = table.Column<string>(nullable: true),
                    WorkflowLinkId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkflowItems_Workflows_WorkflowId",
                        column: x => x.WorkflowId,
                        principalTable: "Workflows",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Folders_SubProjectId",
                table: "Folders",
                column: "SubProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Hierarchies_HierarchyTypeId",
                table: "Hierarchies",
                column: "HierarchyTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Peoples_RoleId",
                table: "Peoples",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionData_TypicalId",
                table: "ProductionData",
                column: "TypicalId");

            migrationBuilder.CreateIndex(
                name: "IX_SprintIssues_SprintId",
                table: "SprintIssues",
                column: "SprintId");

            migrationBuilder.CreateIndex(
                name: "IX_Sprints_SubProjectId",
                table: "Sprints",
                column: "SubProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SubProjects_ProjectId",
                table: "SubProjects",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemTags_SystemId",
                table: "SystemTags",
                column: "SystemId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowItems_WorkflowId",
                table: "WorkflowItems",
                column: "WorkflowId");

            migrationBuilder.CreateIndex(
                name: "IX_Workflows_FolderId",
                table: "Workflows",
                column: "FolderId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupPropertyFieldStates_SessionId_ProductGroupId_PropertyId_FieldId_MultiValueIndex_SubValueIndex_CurrencyId",
                schema: "AAProductBuilder",
                table: "ProductGroupPropertyFieldStates",
                columns: new[] { "SessionId", "ProductGroupId", "PropertyId", "FieldId", "MultiValueIndex", "SubValueIndex", "CurrencyId" },
                unique: true,
                filter: "[PropertyId] IS NOT NULL AND [FieldId] IS NOT NULL AND [CurrencyId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AcceptedTypicalChanges");

            migrationBuilder.DropTable(
                name: "ArisWorkflowItems");

            migrationBuilder.DropTable(
                name: "ArisWorkflows");

            migrationBuilder.DropTable(
                name: "ArisWorkflowVersions");

            migrationBuilder.DropTable(
                name: "Catalogs");

            migrationBuilder.DropTable(
                name: "DataSets");

            migrationBuilder.DropTable(
                name: "DataSetsItems");

            migrationBuilder.DropTable(
                name: "DynamicDataSuggestions");

            migrationBuilder.DropTable(
                name: "EnquiryActions");

            migrationBuilder.DropTable(
                name: "GeneratedTestCases");

            migrationBuilder.DropTable(
                name: "GenericWorkflowItems");

            migrationBuilder.DropTable(
                name: "GenericWorkflows");

            migrationBuilder.DropTable(
                name: "GenericWorkflowVersions");

            migrationBuilder.DropTable(
                name: "Hierarchies");

            migrationBuilder.DropTable(
                name: "Issues");

            migrationBuilder.DropTable(
                name: "IssueTypes");

            migrationBuilder.DropTable(
                name: "Navigations");

            migrationBuilder.DropTable(
                name: "Peoples");

            migrationBuilder.DropTable(
                name: "Processes");

            migrationBuilder.DropTable(
                name: "ProcessResources");

            migrationBuilder.DropTable(
                name: "ProcessWorkflows");

            migrationBuilder.DropTable(
                name: "ProductionData");

            migrationBuilder.DropTable(
                name: "Settings");

            migrationBuilder.DropTable(
                name: "SprintIssues");

            migrationBuilder.DropTable(
                name: "SystemCatalogs");

            migrationBuilder.DropTable(
                name: "SystemTags");

            migrationBuilder.DropTable(
                name: "TypicalChanges");

            migrationBuilder.DropTable(
                name: "TypicalInstances");

            migrationBuilder.DropTable(
                name: "WorkflowIssues");

            migrationBuilder.DropTable(
                name: "WorkflowItems");

            migrationBuilder.DropTable(
                name: "WorkflowPaths");

            migrationBuilder.DropTable(
                name: "WorkflowVersions");

            migrationBuilder.DropTable(
                name: "Fields",
                schema: "AAProductBuilder");

            migrationBuilder.DropTable(
                name: "Parameters",
                schema: "AAProductBuilder");

            migrationBuilder.DropTable(
                name: "ProductGroupPropertyClasses",
                schema: "AAProductBuilder");

            migrationBuilder.DropTable(
                name: "ProductGroupPropertyFieldStates",
                schema: "AAProductBuilder");

            migrationBuilder.DropTable(
                name: "ProductGroupPropertyStates",
                schema: "AAProductBuilder");

            migrationBuilder.DropTable(
                name: "ProductGroups",
                schema: "AAProductBuilder");

            migrationBuilder.DropTable(
                name: "Properties",
                schema: "AAProductBuilder");

            migrationBuilder.DropTable(
                name: "PropertyClasses",
                schema: "AAProductBuilder");

            migrationBuilder.DropTable(
                name: "Queries",
                schema: "AAProductBuilder");

            migrationBuilder.DropTable(
                name: "QueryParameters",
                schema: "AAProductBuilder");

            migrationBuilder.DropTable(
                name: "HierarchyTypes");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Typicals");

            migrationBuilder.DropTable(
                name: "Sprints");

            migrationBuilder.DropTable(
                name: "Systems");

            migrationBuilder.DropTable(
                name: "Workflows");

            migrationBuilder.DropTable(
                name: "Folders");

            migrationBuilder.DropTable(
                name: "SubProjects");

            migrationBuilder.DropTable(
                name: "Projects");
        }
    }
}
