﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class issuetypedefaultdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.InsertData(
                table: "IssueTypes",
                columns: new[] { "Id", "DateCreated", "DateModified", "Default", "Description", "SubProjectId", "Title", "Unit", "UserCreated", "UserModified" },
                values: new object[,]
                {
                    { new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null, true, "Defect", new Guid("42AFEB37-279E-42F4-8621-5578BAB03DE1"), "Defect", "Story points", null, null },
                    { new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null, true, "Business Change Request", new Guid("42AFEB37-279E-42F4-8621-5578BAB03DE1"), "Business CR", "hours", null, null },
                    { new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null, true, "Technical Change Request", new Guid("42AFEB37-279E-42F4-8621-5578BAB03DE1"), "Technical CR", "hours", null, null },
                    { new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null, true, "Defect", new Guid("d6b208f6-4eca-4f24-a0b8-dd94462dd91a"), "Defect", "Story points", null, null },
                    { new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null, true, "Business Change Request", new Guid("d6b208f6-4eca-4f24-a0b8-dd94462dd91a"), "Business CR", "hours", null, null },
                    { new Guid("4a3a403a-a5f3-422d-ae5d-926570f8eb78"), null, null, true, "Technical Change Request", new Guid("d6b208f6-4eca-4f24-a0b8-dd94462dd91a"), "Technical CR", "hours", null, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "IssueTypes",
                keyColumn: "Id",
                keyValue: new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"));

            migrationBuilder.DeleteData(
                table: "IssueTypes",
                keyColumn: "Id",
                keyValue: new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"));

            migrationBuilder.DeleteData(
                table: "IssueTypes",
                keyColumn: "Id",
                keyValue: new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"));

            migrationBuilder.DeleteData(
                table: "IssueTypes",
                keyColumn: "Id",
                keyValue: new Guid("4a3a403a-a5f3-422d-ae5d-926570f8eb78"));

            migrationBuilder.DeleteData(
                table: "IssueTypes",
                keyColumn: "Id",
                keyValue: new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"));

            migrationBuilder.DeleteData(
                table: "IssueTypes",
                keyColumn: "Id",
                keyValue: new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"));

          
        }
    }
}
