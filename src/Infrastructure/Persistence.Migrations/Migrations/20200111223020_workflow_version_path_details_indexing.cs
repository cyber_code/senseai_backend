﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class workflow_version_path_details_indexing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WorkflowVersionPathDetails",
                table: "WorkflowVersionPathDetails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ExecutionResults",
                table: "ExecutionResults");

            migrationBuilder.AddColumn<int>(
                name: "VersionId",
                table: "ExecutionResults",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<ulong>(
              name: "ExecutionTestCaseId",
              table: "ExecutionResults");

            migrationBuilder.AddColumn<Guid>(
                name: "WorkflowPathId",
                table: "ExecutionResults",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<string>(
                name: "UId",
                table: "ATSTestCases",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_WorkflowVersionPathDetails",
                table: "WorkflowVersionPathDetails",
                columns: new[] { "Id", "WorkflowId", "VersionId", "PathId", "Index" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExecutionResults",
                table: "ExecutionResults",
                columns: new[] { "WorkflowId", "TestCaseId", "TestCycleId", "ExecutionTestCaseId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WorkflowVersionPathDetails",
                table: "WorkflowVersionPathDetails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ExecutionResults",
                table: "ExecutionResults");

            migrationBuilder.DropColumn(
                name: "VersionId",
                table: "ExecutionResults");

            migrationBuilder.DropColumn(
                name: "WorkflowPathId",
                table: "ExecutionResults");

            migrationBuilder.DropColumn(
             name: "ExecutionTestCaseId",
             table: "ExecutionResults");

            migrationBuilder.AlterColumn<string>(
                name: "UId",
                table: "ATSTestCases",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WorkflowVersionPathDetails",
                table: "WorkflowVersionPathDetails",
                columns: new[] { "Id", "WorkflowId", "VersionId", "PathId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExecutionResults",
                table: "ExecutionResults",
                columns: new[] { "WorkflowId", "TestCaseId", "TestCycleId", "ExecutionTestCaseId" });
        }
    }
}
