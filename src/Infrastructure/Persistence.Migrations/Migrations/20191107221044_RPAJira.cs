﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class RPAJira : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "JiraIssueTypeAttributeMapping",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    JiraIssueTypeMappingId = table.Column<Guid>(nullable: false),
                    SenseaiField = table.Column<string>(nullable: false),
                    JiraField = table.Column<string>(nullable: false),
                    Required = table.Column<bool>(nullable: false),
                    Index = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JiraIssueTypeAttributeMapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JiraIssueTypeAttributeValueMapping",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    JiraIssueTypeAttributeMappingId = table.Column<Guid>(nullable: false),
                    SenseaiValue = table.Column<string>(nullable: false),
                    JiraValue = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JiraIssueTypeAttributeValueMapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JiraIssueTypeMapping",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    JiraProjectMappingId = table.Column<Guid>(nullable: false),
                    SenseaiIssueTypeField = table.Column<string>(nullable: false),
                    JiraIssueTypeField = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JiraIssueTypeMapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JiraProjectMapping",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    ProjectId = table.Column<Guid>(nullable: false),
                    SubprojectId = table.Column<Guid>(nullable: false),
                    JiraProjectKey = table.Column<string>(nullable: true),
                    JiraProjectName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JiraProjectMapping", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JiraIssueTypeAttributeMapping");

            migrationBuilder.DropTable(
                name: "JiraIssueTypeAttributeValueMapping");

            migrationBuilder.DropTable(
                name: "JiraIssueTypeMapping");

            migrationBuilder.DropTable(
                name: "JiraProjectMapping");
        }
    }
}
