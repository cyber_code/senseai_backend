﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class issuetypefieldsdefaultdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.InsertData(
                table: "IssueTypeFields",
                columns: new[] { "Id", "Checked", "DateCreated", "DateModified", "IssueTypeFieldsNameId", "IssueTypeId", "UserCreated", "UserModified" },
                values: new object[,]
                {
                  
                    { new Guid("86a1fba7-0e37-49d5-969d-1f4fa9e39538"), true, null, null, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                   
                    { new Guid("2154ddd9-c691-4940-9a48-899704f94576"), true, null, null, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("5e8b6ce2-3fa9-47ff-bd7f-cadf8c1155b0"), false, null, null, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("8829c3ed-9523-4b61-81b7-ff7d1d65a88b"), false, null, null, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                   
                    { new Guid("47e9d0e2-4518-495a-bff6-349605cc9da8"), false, null, null, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("0cbcc218-dac0-49bf-8251-2d18a10785be"), true, null, null, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("809c378f-6487-4d1b-aca2-05a07a14182a"), true, null, null, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("530ca9ca-afed-4451-8568-fe1155315bf7"), true, null, null, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("3fae7b69-c820-46b8-9153-15bceff7f891"), true, null, null, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("60112583-1737-4211-b7d9-5c2a4a742879"), true, null, null, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("d8e29e37-c8c4-497d-9a47-2d746b1928d2"), true, null, null, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("292c3edb-7245-4ab4-a6c2-9a202bbee150"), true, null, null, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("e224e2bd-1e88-415f-84c6-765071f13ec2"), true, null, null, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("57781934-57e0-4dc3-a7a3-31fec379144d"), true, null, null, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("d31a66db-8b63-41dc-a858-d1ac719656a0"), true, null, null, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("d581d4a7-38e3-45f2-a218-6808890e71da"), false, null, null, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("f4f135ea-e9cf-49ef-bdc3-73c3178ceb33"), true, null, null, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("d3bfcf6d-1b8a-42a0-97b1-95431b6c5c83"), true, null, null, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("8427cacb-2126-47d2-bbd3-b766f2166714"), true, null, null, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("3a5611bd-8232-4939-85ac-7c9cab149130"), false, null, null, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("83c69768-cc51-4507-a3df-1ceec9a30e66"), false, null, null, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), null, null },
                    { new Guid("18dc8fc8-0669-40b4-8393-9c1b2a47800e"), true, null, null, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("66ea618e-acee-42b8-8b08-e5da6ae37390"), false, null, null, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("19282fe9-79f1-4220-b5f8-d9ca09e61d3c"), true, null, null, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("bc527ec4-2ff7-4c44-8cb2-0de4359e1e38"), true, null, null, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("da437716-021e-4604-8b8e-e54e1ca986fd"), true, null, null, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("1a0edd89-cc37-49a2-ae4e-a4082fb56b1e"), true, null, null, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("0eaaf739-ea24-4c11-8e49-f43f7ede1cb7"), true, null, null, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("65b21eeb-cbf4-4886-b238-114adcdfcb1a"), true, null, null, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("28af8461-f900-448f-ad2f-36e2bf45b93e"), true, null, null, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("e0f90e90-a8d3-4235-b627-38a156a00b53"), true, null, null, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("514e56c3-2d90-447e-b214-dd7612bef04b"), true, null, null, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("313ce7e9-1e25-4284-92b2-b274d9c7db81"), true, null, null, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("8d2f9fcb-e5fa-4a8f-acf9-ec7309b27760"), true, null, null, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("4be5a19d-45ad-4a1b-8da3-1d42c5d1ba6b"), false, null, null, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("1e24ee87-3ae1-4646-a753-2e042cbfe996"), true, null, null, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("a7233dd1-204e-42c6-b047-34a44e7f1b2f"), true, null, null, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("8ac5b26b-9410-4973-b615-58da82efa7b8"), true, null, null, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("79e572cb-a0e7-4a77-a11a-126de3a176b1"), false, null, null, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("1dea1179-a998-4253-a625-36b1b0794258"), false, null, null, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                  
                    { new Guid("ef17b9ee-17bb-497f-a549-4f00d747499c"), false, null, null, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("2e9b2380-0c07-43f2-aa87-94694b49e7b0"), true, null, null, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("6090d01c-6943-4e43-8256-635d1165b8d2"), true, null, null, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("f5f7bc84-a706-4210-8f79-4a174917c1c9"), true, null, null, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("51dbaba0-6404-45e0-a82f-46ba02ad8b86"), true, null, null, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("25a2bf4d-9de8-4608-b402-dd7d6eae9fb1"), true, null, null, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("763b1c82-3170-45bb-ab69-d685996d1518"), true, null, null, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("4dd0bf90-2382-43ab-98c0-64fd33a58133"), true, null, null, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("c3e58ce0-d60f-48e7-8a09-9b6ad4b6ae6c"), true, null, null, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                   
                    { new Guid("781af05f-0b50-4f10-bb0c-692bad230c05"), false, null, null, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("879008f5-9b00-4f5e-9fdd-0f7ecf79cdff"), true, null, null, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("6c7393c4-5ccb-4869-88b0-40bfeeb32e36"), false, null, null, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("cb6d3c11-9728-436e-b191-95b387578712"), false, null, null, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("6f790412-6ded-4cea-9fcf-8afeba2cbfd0"), true, null, null, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("d20444c1-7d31-4e2f-883b-6680f3db805d"), true, null, null, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },

                    { new Guid("17cb4a3b-6999-48f8-8bcc-251da7243408"), true, null, null, new Guid("8AD0BE32-31E0-43EE-8C4F-C15C145B026F"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("0a011999-39c4-4e8e-bcaf-ffc1a1a5ceac"), true, null, null, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("b320ffbe-e3cd-446f-a786-1a42b3ac8bec"), false, null, null, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("ec29c564-cfb5-4f5e-8c49-0fa664ee48a1"), true, null, null, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("dbc1c4b4-7e90-4158-af75-959bf82c8f6b"), true, null, null, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("20c5c1d4-244a-4ad9-a10c-112b6adf3546"), false, null, null, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("ed67efb2-5fc7-4d0c-bc74-82a1212f43ad"), false, null, null, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("bf3e4d17-ae46-43a2-a9ea-e525a2697670"), false, null, null, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("ac966c9e-d1e3-4b57-9541-ae9e10fa6e9c"), false, null, null, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("2e5e72c5-4e7a-4936-9261-976c4e4ee0ab"), false, null, null, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("38c327e2-b9f0-4ccb-b034-99ff9a4a8f65"), false, null, null, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("5d7a92a8-2ad6-4793-90fa-5cfe3e2c0fca"), false, null, null, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("8d19ab91-0c69-4f4f-8e45-f40e745c95fb"), true, null, null, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("137a43fd-8555-4650-87a2-386ad3cfb16c"), false, null, null, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("371c329a-00f6-4c14-95a4-1f98bc9b8302"), false, null, null, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("d2f08c45-a875-4946-a5f4-629bfca7e0d7"), false, null, null, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("7c3aae3e-ba04-415b-8de2-911a7a531562"), true, null, null, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("d0dc61fd-da3e-4827-99d0-2cea644fb500"), true, null, null, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },
                    { new Guid("33199222-758b-4832-ae83-38e06945f4de"), false, null, null, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), null, null },

                    { new Guid("a53f267a-3705-47f9-8fea-5c43021666e0"), true, null, null, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null },
                    { new Guid("8d44e555-60ca-4c47-9944-085865dfd21c"), true, null, null, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("33f099e1-b35e-4fb5-ac13-532e3b1ca57e"), false, null, null, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("52e64cb1-cdee-41ca-b759-462c176e6bda"), true, null, null, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("8387fabb-1a4b-4fca-aa38-6ac4c7b3b832"), true, null, null, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("f7f99f99-8958-4d3b-b7fe-b1ba13c02b6a"), true, null, null, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("b75675b3-a832-410f-87c2-de3ab190c428"), true, null, null, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("4d4e6d7d-38db-4a1b-9ea4-49e603d55dfd"), true, null, null, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("038937a0-15fc-4cb3-8269-3bc4724e6ac5"), true, null, null, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("a9865b43-9adf-4368-95d4-c712ef6be5f0"), true, null, null, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("f4d1a467-294f-47c4-a9cb-31d84ceba16c"), true, null, null, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("6c44b417-87e1-4f1b-887d-a7bf5204d691"), true, null, null, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("9050d4ed-0f66-419b-98b2-7fa0767c14f0"), false, null, null, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("967fff75-4e68-4135-8bd4-6604d500a844"), true, null, null, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("5601e3b2-d5c1-4b14-ba9c-0e84c9b86131"), true, null, null, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("9cb56f1e-1bd2-4b15-bb81-2aa7896d13b4"), false, null, null, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("1b3afeaf-3331-4de6-a7eb-1224799111c3"), true, null, null, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("746fa58d-97b5-4d58-9169-e02c65771da9"), false, null, null, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                    { new Guid("42e455bb-bb3c-4001-9955-123377786db4"), false, null, null, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), null, null },
                 
                    { new Guid("ab7797bf-3a54-46a8-86ca-c807ed18ebbf"), true, null, null, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("72c9b619-4c24-43e0-b621-4dd19141e771"), false, null, null, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("d408453f-018b-4709-9d58-8c640abbf537"), true, null, null, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("a9b7af80-3f02-4f13-ac5c-9c458d3884c7"), true, null, null, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("252a1425-d3a2-42c8-a22a-024cc428f9a7"), false, null, null, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("d6d66b16-8959-4936-8250-226eb37362f6"), false, null, null, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("aa30249f-d5c2-4440-97be-e1535dcae1d2"), false, null, null, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("faa1d640-c9b3-4287-9758-acd15af5553b"), false, null, null, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("8879b868-43ac-4398-8868-fedaef9f944c"), false, null, null, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), null, null },
                    { new Guid("fceb32e1-3680-4b31-9bf6-be5fb0b6e681"), false, null, null, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), null, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("038937a0-15fc-4cb3-8269-3bc4724e6ac5"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("07dd2f68-25e4-4e04-ad0e-f6bcb767ffe6"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("0a011999-39c4-4e8e-bcaf-ffc1a1a5ceac"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("0cbcc218-dac0-49bf-8251-2d18a10785be"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("0eaaf739-ea24-4c11-8e49-f43f7ede1cb7"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("137a43fd-8555-4650-87a2-386ad3cfb16c"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("18dc8fc8-0669-40b4-8393-9c1b2a47800e"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("19282fe9-79f1-4220-b5f8-d9ca09e61d3c"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("19d29d09-9e79-4d2e-92a7-28d8394fa20b"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("1a0edd89-cc37-49a2-ae4e-a4082fb56b1e"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("1ad7bae5-01a4-4cba-a88c-66298ec51c10"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("1b3afeaf-3331-4de6-a7eb-1224799111c3"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("1dea1179-a998-4253-a625-36b1b0794258"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("1e24ee87-3ae1-4646-a753-2e042cbfe996"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("20c5c1d4-244a-4ad9-a10c-112b6adf3546"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("2154ddd9-c691-4940-9a48-899704f94576"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("252a1425-d3a2-42c8-a22a-024cc428f9a7"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("25a2bf4d-9de8-4608-b402-dd7d6eae9fb1"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("28af8461-f900-448f-ad2f-36e2bf45b93e"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("292c3edb-7245-4ab4-a6c2-9a202bbee150"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("2cea1490-3711-446d-b2bd-0142d652a966"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("2e5e72c5-4e7a-4936-9261-976c4e4ee0ab"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("2e9b2380-0c07-43f2-aa87-94694b49e7b0"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("2f9cb319-1915-4ddd-995b-bd2236314b94"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("313ce7e9-1e25-4284-92b2-b274d9c7db81"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("33199222-758b-4832-ae83-38e06945f4de"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("33f099e1-b35e-4fb5-ac13-532e3b1ca57e"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("371c329a-00f6-4c14-95a4-1f98bc9b8302"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("38c327e2-b9f0-4ccb-b034-99ff9a4a8f65"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("392a8200-c789-41d2-94fd-e23cd27da13f"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("39f9d0b0-a781-42d3-ae66-3c7f64b2412d"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("3a5611bd-8232-4939-85ac-7c9cab149130"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("3d929874-7c12-4e5a-8032-5771a6937b8a"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("3fae7b69-c820-46b8-9153-15bceff7f891"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("42e455bb-bb3c-4001-9955-123377786db4"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("47e9d0e2-4518-495a-bff6-349605cc9da8"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("4be5a19d-45ad-4a1b-8da3-1d42c5d1ba6b"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("4d4e6d7d-38db-4a1b-9ea4-49e603d55dfd"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("4dd0bf90-2382-43ab-98c0-64fd33a58133"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("514e56c3-2d90-447e-b214-dd7612bef04b"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("51dbaba0-6404-45e0-a82f-46ba02ad8b86"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("52e64cb1-cdee-41ca-b759-462c176e6bda"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("530ca9ca-afed-4451-8568-fe1155315bf7"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("536ccddf-4541-452d-95eb-26f5465a1804"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("5601e3b2-d5c1-4b14-ba9c-0e84c9b86131"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("57781934-57e0-4dc3-a7a3-31fec379144d"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("5d7a92a8-2ad6-4793-90fa-5cfe3e2c0fca"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("5e8b6ce2-3fa9-47ff-bd7f-cadf8c1155b0"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("60112583-1737-4211-b7d9-5c2a4a742879"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("6090d01c-6943-4e43-8256-635d1165b8d2"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("619a366a-6c10-4f6d-b83a-851d47a0c55a"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("65b21eeb-cbf4-4886-b238-114adcdfcb1a"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("66ea618e-acee-42b8-8b08-e5da6ae37390"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("6c44b417-87e1-4f1b-887d-a7bf5204d691"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("6c7393c4-5ccb-4869-88b0-40bfeeb32e36"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("6f790412-6ded-4cea-9fcf-8afeba2cbfd0"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("70d3418d-b05d-47a4-b56a-f475d9e335cd"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("72c9b619-4c24-43e0-b621-4dd19141e771"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("746fa58d-97b5-4d58-9169-e02c65771da9"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("763b1c82-3170-45bb-ab69-d685996d1518"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("777a7aea-0864-4159-8960-2eafefe4d317"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("781af05f-0b50-4f10-bb0c-692bad230c05"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("79e572cb-a0e7-4a77-a11a-126de3a176b1"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("7c3aae3e-ba04-415b-8de2-911a7a531562"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("7fdd6193-76d8-400d-a71c-5c66466de041"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("809c378f-6487-4d1b-aca2-05a07a14182a"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("8387fabb-1a4b-4fca-aa38-6ac4c7b3b832"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("83c69768-cc51-4507-a3df-1ceec9a30e66"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("8427cacb-2126-47d2-bbd3-b766f2166714"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("86a1fba7-0e37-49d5-969d-1f4fa9e39538"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("879008f5-9b00-4f5e-9fdd-0f7ecf79cdff"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("8829c3ed-9523-4b61-81b7-ff7d1d65a88b"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("8879b868-43ac-4398-8868-fedaef9f944c"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("8ac5b26b-9410-4973-b615-58da82efa7b8"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("8acf4d9e-2a57-49fe-a44d-cfd7e3d9a249"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("8d19ab91-0c69-4f4f-8e45-f40e745c95fb"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("8d2f9fcb-e5fa-4a8f-acf9-ec7309b27760"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("8d44e555-60ca-4c47-9944-085865dfd21c"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("9050d4ed-0f66-419b-98b2-7fa0767c14f0"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("967fff75-4e68-4135-8bd4-6604d500a844"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("9766facd-2c26-48b4-8482-46690bd5a663"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("9a1861f4-859b-4f05-ac48-6703a7d06879"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("9cb56f1e-1bd2-4b15-bb81-2aa7896d13b4"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("a53f267a-3705-47f9-8fea-5c43021666e0"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("a7233dd1-204e-42c6-b047-34a44e7f1b2f"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("a9865b43-9adf-4368-95d4-c712ef6be5f0"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("a9b7af80-3f02-4f13-ac5c-9c458d3884c7"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("aa30249f-d5c2-4440-97be-e1535dcae1d2"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("ab7797bf-3a54-46a8-86ca-c807ed18ebbf"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("ac48d3f6-7cb3-48d5-a857-91b783705b91"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("ac966c9e-d1e3-4b57-9541-ae9e10fa6e9c"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("b320ffbe-e3cd-446f-a786-1a42b3ac8bec"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("b75675b3-a832-410f-87c2-de3ab190c428"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("bc527ec4-2ff7-4c44-8cb2-0de4359e1e38"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("be1d625b-1d60-40a4-bb8a-c559f5b492c2"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("bf3e4d17-ae46-43a2-a9ea-e525a2697670"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("c3e58ce0-d60f-48e7-8a09-9b6ad4b6ae6c"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("cb6d3c11-9728-436e-b191-95b387578712"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d0dc61fd-da3e-4827-99d0-2cea644fb500"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d20444c1-7d31-4e2f-883b-6680f3db805d"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d2f08c45-a875-4946-a5f4-629bfca7e0d7"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d31a66db-8b63-41dc-a858-d1ac719656a0"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d3bfcf6d-1b8a-42a0-97b1-95431b6c5c83"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d408453f-018b-4709-9d58-8c640abbf537"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d5362106-b867-43e1-b09c-ea12bb8c500b"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d581d4a7-38e3-45f2-a218-6808890e71da"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d6d66b16-8959-4936-8250-226eb37362f6"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d7c1d126-aff2-4a80-9d49-3f4be9eef8f9"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("d8e29e37-c8c4-497d-9a47-2d746b1928d2"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("da437716-021e-4604-8b8e-e54e1ca986fd"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("db778925-a51c-4f64-9346-6f5b1889369a"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("dbc1c4b4-7e90-4158-af75-959bf82c8f6b"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("e0f90e90-a8d3-4235-b627-38a156a00b53"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("e224e2bd-1e88-415f-84c6-765071f13ec2"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("e42d27ae-5bcb-4bfe-a8be-8ed2b4afa6ea"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("e734ef6a-dcc4-4983-adaf-fa0b255399fe"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("ec29c564-cfb5-4f5e-8c49-0fa664ee48a1"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("ed67efb2-5fc7-4d0c-bc74-82a1212f43ad"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("ef17b9ee-17bb-497f-a549-4f00d747499c"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("f4d1a467-294f-47c4-a9cb-31d84ceba16c"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("f4f135ea-e9cf-49ef-bdc3-73c3178ceb33"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("f5f7bc84-a706-4210-8f79-4a174917c1c9"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("f7f99f99-8958-4d3b-b7fe-b1ba13c02b6a"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("f8c42289-93c8-40bf-b7b6-514e804d1105"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("faa1d640-c9b3-4287-9758-acd15af5553b"));

            migrationBuilder.DeleteData(
                table: "IssueTypeFields",
                keyColumn: "Id",
                keyValue: new Guid("fceb32e1-3680-4b31-9bf6-be5fb0b6e681"));

           
        }
    }
}
