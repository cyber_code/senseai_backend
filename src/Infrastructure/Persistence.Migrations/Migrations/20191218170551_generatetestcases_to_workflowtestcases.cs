﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class generatetestcases_to_workflowtestcases : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GeneratedTestCases");

            migrationBuilder.CreateTable(
                name: "WorkflowTestCases",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    VersionId = table.Column<int>(nullable: false),
                    WorkflowPathId = table.Column<Guid>(nullable: false),
                    TestCaseId = table.Column<Guid>(nullable: false),
                    ATSTestCaseId = table.Column<decimal>(nullable: false),
                    TestCaseTitle = table.Column<string>(nullable: false),
                    TCIndex = table.Column<int>(nullable: false),
                    TestStepId = table.Column<Guid>(nullable: false),
                    ATSTestStepId = table.Column<decimal>(nullable: false),
                    TestStepTitle = table.Column<string>(nullable: false),
                    TestStepJson = table.Column<string>(nullable: false),
                    TypicalName = table.Column<string>(nullable: false),
                    TypicalId = table.Column<Guid>(nullable: false),
                    TSIndex = table.Column<int>(nullable: false),
                    TestStepType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowTestCases", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkflowTestCases");

            migrationBuilder.CreateTable(
                name: "GeneratedTestCases",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: true),
                    TCIndex = table.Column<int>(nullable: false),
                    TSIndex = table.Column<int>(nullable: false),
                    TestCaseId = table.Column<Guid>(nullable: false),
                    TestCaseTitle = table.Column<string>(nullable: false),
                    TestStepId = table.Column<Guid>(nullable: false),
                    TestStepJson = table.Column<string>(nullable: false),
                    TestStepTitle = table.Column<string>(nullable: false),
                    TestStepType = table.Column<int>(nullable: false),
                    TypicalId = table.Column<Guid>(nullable: false),
                    TypicalName = table.Column<string>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    WorkflowPathId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneratedTestCases", x => x.Id);
                });
        }
    }
}
