﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class Add_ATS_test_cases_column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TestCasesJson",
                table: "WorkflowExportedTestCases",
                newName: "SenseAITestCases");

            migrationBuilder.AddColumn<string>(
                name: "AtsTestCases",
                table: "WorkflowExportedTestCases",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AtsTestCases",
                table: "WorkflowExportedTestCases");

            migrationBuilder.RenameColumn(
                name: "SenseAITestCases",
                table: "WorkflowExportedTestCases",
                newName: "TestCasesJson");
        }
    }
}
