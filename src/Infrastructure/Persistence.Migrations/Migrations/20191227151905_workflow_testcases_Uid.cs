﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class workflow_testcases_Uid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ATSTestCaseId",
                table: "WorkflowTestCases");

            migrationBuilder.DropColumn(
                name: "ATSTestStepId",
                table: "WorkflowTestCases");

            migrationBuilder.DropColumn(
                name: "RequirementId",
                table: "Workflows");

            migrationBuilder.AddColumn<decimal>(
                name: "ATSTestCaseNoderef",
                table: "WorkflowTestCases",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "ATSTestCaseUId",
                table: "WorkflowTestCases",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ATSTestStepNoderef",
                table: "WorkflowTestCases",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "ATSTestStepUId",
                table: "WorkflowTestCases",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ATSTestCaseNoderef",
                table: "WorkflowTestCases");

            migrationBuilder.DropColumn(
                name: "ATSTestCaseUId",
                table: "WorkflowTestCases");

            migrationBuilder.DropColumn(
                name: "ATSTestStepNoderef",
                table: "WorkflowTestCases");

            migrationBuilder.DropColumn(
                name: "ATSTestStepUId",
                table: "WorkflowTestCases");

            migrationBuilder.AddColumn<decimal>(
                name: "ATSTestCaseId",
                table: "WorkflowTestCases",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ATSTestStepId",
                table: "WorkflowTestCases",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<long>(
                name: "RequirementId",
                table: "Workflows",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
