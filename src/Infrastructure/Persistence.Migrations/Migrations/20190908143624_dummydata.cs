﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class dummydata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Catalogs",
                columns: new[] { "Id", "DateCreated", "DateModified", "Description", "ProjectId", "SubProjectId", "Title", "Type", "UserCreated", "UserModified" },
                values: new object[,]
                {
                    { new Guid("764cffef-ec71-4d16-acf0-ef9f80211c1c"), null, null, "Project Resourcing", new Guid("00000000-0000-0000-0000-000000000000"), new Guid("ceb36b8c-bf93-438b-8881-38815f2bd4e2"), "ProjectResourcing", 0, null, null },
                    { new Guid("764cffef-ec71-4d16-acf0-ef9f80211c5c"), null, null, "R17 Model Bank Applications", new Guid("00000000-0000-0000-0000-000000000000"), new Guid("72998a15-d6f2-4abf-bf7b-333cde9edafd"), "R17 Model Bank Applications", 0, null, null },
                    { new Guid("6c0a8ef3-0c33-4135-b801-25a208e398bd"), null, null, "R17 Model Bank Enquiries", new Guid("00000000-0000-0000-0000-000000000000"), new Guid("d6f7b4eb-ef6d-4c40-b41a-376dc71e7782"), "R17 Model Bank Enquiries", 0, null, null }
                });

            migrationBuilder.InsertData(
                table: "DataSets",
                columns: new[] { "Id", "CatalogId", "Combinations", "Coverage", "DateCreated", "DateModified", "SubProjectId", "SystemId", "Title", "TypicalId", "UserCreated", "UserModified" },
                values: new object[,]
                {
                    { new Guid("fc3e812d-fc35-4c53-9904-fc3867cf7fc3"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c5c"), "[{\"Attribute\":\"CUSTOMER\",\"Values\":[\"4525\"]}]", 0m, null, null, new Guid("42afeb37-279e-42f4-8621-5578bab03de1"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c1c"), "ACCOUNT", new Guid("f25c75da-13f1-46e4-b015-f94036633974"), null, null },
                    { new Guid("a77e812d-ecd5-4c53-9904-0fd867cf7fc3"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c5c"), "[{\"Attribute\":\"GENDER\",\"Values\":[\"M\",\"F\"]},{\"Attribute\":\"SECTOR\",\"Values\":[\"2001\",\"1001\"]}]", 0m, null, null, new Guid("42afeb37-279e-42f4-8621-5578bab03de1"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c1c"), "CUSTOMER", new Guid("f25c75da-13f1-46e4-b015-f94036633974"), null, null }
                });

            migrationBuilder.InsertData(
                table: "DataSetsItems",
                columns: new[] { "Id", "DataSetId", "DateCreated", "DateModified", "Row", "UserCreated", "UserModified" },
                values: new object[,]
                {
                    { new Guid("2195b937-af7c-43b9-867a-fa3f205923c1"), new Guid("a77e812d-ecd5-4c53-9904-0fd867cf7fc3"), null, null, "[{ \"Name\": \"SHORT.NAME-1~1\", \"Value\": \"CM1\" }, { \"Name\": \"NAME.1-1~1\", \"Value\": \"CLIENT NAME\" }, { \"Name\": \"GENDER\", \"Value\": \"M\" }, { \"Name\": \"SECTOR\", \"Value\": \"1001\" }, { \"Name\": \"LANGUAGE\", \"Value\": \"1\" }, { \"Name\": \"NET.MONTHLY.IN\", \"Value\": \"2000\" }, { \"Name\": \"ID\", \"Value\": \"1938739\" }]", null, null },
                    { new Guid("259a3e19-cf19-40f4-9a4c-7727194f8ed5"), new Guid("a77e812d-ecd5-4c53-9904-0fd867cf7fc3"), null, null, "[{ \"Name\": \"SHORT.NAME-1~1\", \"Value\": \"CM1\" }, { \"Name\": \"NAME.1-1~1\", \"Value\": \"CLIENT NAME\" }, { \"Name\": \"GENDER\", \"Value\": \"M\" }, { \"Name\": \"SECTOR\", \"Value\": \"2001\" }, { \"Name\": \"LANGUAGE\", \"Value\": \"1\" }, { \"Name\": \"NET.MONTHLY.IN\", \"Value\": \"2000\" }, { \"Name\": \"ID\", \"Value\": \"1938739\" }]", null, null },
                    { new Guid("c27c8b6b-7cea-4bf0-b132-e78ae4697112"), new Guid("a77e812d-ecd5-4c53-9904-0fd867cf7fc3"), null, null, "[{ \"Name\": \"SHORT.NAME-1~1\", \"Value\": \"CM1\" }, { \"Name\": \"NAME.1-1~1\", \"Value\": \"CLIENT NAME\" }, { \"Name\": \"GENDER\", \"Value\": \"F\" }, { \"Name\": \"SECTOR\", \"Value\": \"1001\" }, { \"Name\": \"LANGUAGE\", \"Value\": \"1\" }, { \"Name\": \"NET.MONTHLY.IN\", \"Value\": \"2000\" }, { \"Name\": \"ID\", \"Value\": \"1938739\" }]", null, null },
                    { new Guid("bc41ed3e-207a-4115-8727-de058a3a5c07"), new Guid("a77e812d-ecd5-4c53-9904-0fd867cf7fc3"), null, null, "[{ \"Name\": \"SHORT.NAME-1~1\", \"Value\": \"CM1\" }, { \"Name\": \"NAME.1-1~1\", \"Value\": \"CLIENT NAME\" }, { \"Name\": \"GENDER\", \"Value\": \"F\" }, { \"Name\": \"SECTOR\", \"Value\": \"2001\" }, { \"Name\": \"LANGUAGE\", \"Value\": \"1\" }, { \"Name\": \"NET.MONTHLY.IN\", \"Value\": \"2000\" }, { \"Name\": \"ID\", \"Value\": \"1938739\" }]", null, null },
                    { new Guid("e0c7d70a-7cb5-4822-a0a0-75c9c8c2c979"), new Guid("fc3e812d-fc35-4c53-9904-fc3867cf7fc3"), null, null, "[{ \"Name\": \"CUSTOMER\", \"Value\": \"4525\" }]", null, null }
                });

            migrationBuilder.InsertData(
                table: "GeneratedTestCases",
                columns: new[] { "Id", "DateCreated", "DateModified", "TCIndex", "TSIndex", "TestCaseId", "TestCaseTitle", "TestStepId", "TestStepJson", "TestStepTitle", "TestStepType", "TypicalId", "TypicalName", "UserCreated", "UserModified", "WorkflowId", "WorkflowPathId" },
                values: new object[,]
                {
                    { new Guid("e7d562ff-caf6-4d54-aefb-171238a4715e"), null, null, 0, 0, new Guid("f91c4627-1b48-44ce-91fb-64413ca68395"), "Test Case Title 1", new Guid("32283980-b27d-4dd9-ab75-b617e5542954"), "Json", "Test Step Title 1", 0, new Guid("00000000-0000-0000-0000-000000000000"), "Customer", null, null, new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68390") },
                    { new Guid("4ede453c-02c1-49e4-8b97-72b656f1724a"), null, null, 1, 6, new Guid("a91c4627-1b48-44ce-91fb-64413ca68395"), "01. Customer Creation", new Guid("52415aa2-c217-4ced-a2da-2cff3eff26ed"), "{ \"title\": \"Authorise: CUSTOMER,NAU\", \"description\": \"Authorise: CUSTOMER,NAU\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"R17 214 Application\", \"typicalName\": \"CUSTOMER\", \"parameters\": \"NAU,A\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [{ \"testStepId\": \"c43859cc-874b-4404-8d73-c14950d1d555\", \"sourceAttributeName\": \"@ID\",  \"targetAttributeName\": \"Customer No\", \"plainTextFormula\": null, \"uiMode\": null }], \"dataSetIds\": [] }", "Authorise: CUSTOMER,NAU", 0, new Guid("00000000-0000-0000-0000-000000000000"), "CUSTOMER", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("1ed5af2e-493e-4087-9b1b-1ce30d5b165c"), null, null, 0, 0, new Guid("3a76727c-bb66-425c-ba1b-fd9638e15a34"), "Test Case Title 2", new Guid("9bbcb8b5-e79a-4b0e-8fdf-2badc5350369"), "Json", "Test Step Title 2", 0, new Guid("00000000-0000-0000-0000-000000000000"), "Customer", null, null, new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68390") },
                    { new Guid("38c5c322-fabe-4a76-9198-f3427060b5db"), null, null, 1, 1, new Guid("a91c4627-1b48-44ce-91fb-64413ca68395"), "01. Customer Creation", new Guid("75fc37cb-7673-48f2-86b3-7929d42a68f5"), "{ \"title\": \"Menu: Individual Customer\", \"description\": \"Menu: Individual Customer\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"ProjectResourcing\", \"typicalName\": \"NavigationInfo\", \"parameters\": \",MNU,,,,,,User Menu > Customer > Individual Customer\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [] }", "Menu: Individual Customer", 0, new Guid("00000000-0000-0000-0000-000000000000"), "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("5c013eea-bd18-4f74-8bd4-e4c18c8a15bd"), null, null, 1, 7, new Guid("a91c4627-1b48-44ce-91fb-64413ca68395"), "01. Customer Creation", new Guid("99498d53-7403-4353-a55f-22fec33dac61"), "{ \"title\": \"Menu: Sign Off\", \"description\": \"Menu: Sign Off\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"ProjectResourcing\", \"typicalName\": \"NavigationInfo\", \"parameters\": \",MNU,,,,,,Sign Off\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [] }", "Menu: Sign Off", 0, new Guid("00000000-0000-0000-0000-000000000000"), "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("8e2283ba-9499-4524-9b1d-756a96ace9ec"), null, null, 1, 2, new Guid("a91c4627-1b48-44ce-91fb-64413ca68395"), "01. Customer Creation", new Guid("c43859cc-874b-4404-8d73-c14950d1d555"), "{ \"title\": \"Create: CUSTOMER,INPUT\", \"description\": \"Create: CUSTOMER,INPUT\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"R17 214 Application\", \"typicalName\": \"CUSTOMER\", \"parameters\": \"INPUT,I\", \"typicalInstance\":null , \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [{ \"TestStepId\": \"00000000-0000-0000-0000-000000000000\", \"AttributeName\": \"MNEMONIC\", \"PlainTextFormula\": \"JE1ORU1PTklDKEdFVF9SQU5ET01fV09SRCg5KSk=\", \"UiMode\": null,\"Id\": \"b0eaa6b1-46af-4903-b307-3835a036227b\" }], \"dataSetIds\": [\"A77E812D-ECD5-4C53-9904-0FD867CF7FC3\"] }", "Create: CUSTOMER,INPUT", 0, new Guid("00000000-0000-0000-0000-000000000000"), "CUSTOMER", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("503bfbce-14aa-42b8-93a1-be87679f2677"), null, null, 1, 3, new Guid("a91c4627-1b48-44ce-91fb-64413ca68395"), "01. Customer Creation", new Guid("1802b838-e001-4623-af3e-5914374bf139"), "{ \"title\": \"Menu: Sign Off\", \"description\": \"Menu: Sign Off\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"ProjectResourcing\", \"typicalName\": \"NavigationInfo\", \"parameters\": \",MNU,,,,,,Sign Off\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [] }", "Menu: Sign Off", 0, new Guid("00000000-0000-0000-0000-000000000000"), "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("0cdbdbe7-3c52-4824-aac3-f2f541a2b52b"), null, null, 0, 0, new Guid("3a76727c-bb66-425c-ba1b-fd9638e15a34"), "Test Case Title 2", new Guid("543aa562-7794-4d2a-89ff-4a246cc044a4"), "Json", "Test Step Title 1", 0, new Guid("00000000-0000-0000-0000-000000000000"), "Customer", null, null, new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68390") },
                    { new Guid("1d4e8926-8dfc-404b-ae26-2522ea0abaad"), null, null, 0, 0, new Guid("f91c4627-1b48-44ce-91fb-64413ca68395"), "Test Case Title 1", new Guid("16989e2b-f93d-48ee-9a3f-dd72d9bc2acb"), "Json", "Test Step Title 2", 0, new Guid("00000000-0000-0000-0000-000000000000"), "Customer", null, null, new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68390") },
                    { new Guid("daee0f94-1c8c-4d6c-9755-2a37388d1d21"), null, null, 1, 5, new Guid("a91c4627-1b48-44ce-91fb-64413ca68395"), "01. Customer Creation", new Guid("bdc24f03-84c6-4268-860f-6c526597fcc1"), "{ \"title\": \"Enquiry Action: CUSTOMER.NAU.RESULT\", \"description\": \"Enquiry Action: CUSTOMER.NAU.RESULT\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"R17 214 Enquiries\", \"typicalName\": \"CUSTOMER.NAU.RESULT\", \"parameters\": \"CLICK(\\\"Authorise\\\")\", \"typicalInstance\": null, \"filters\": null, \"postFilters\":  [{ \"AttributeName\": \"Customer No\", \"OperatorName\": \"Equal\", \"AttributeValue\": null, \"DynamicData\": { \"testStepId\": \"c43859cc-874b-4404-8d73-c14950d1d555\", \"sourceAttributeName\": \"@ID\",  \"targetAttributeName\": \"Customer No\", \"plainTextFormula\": null, \"uiMode\": null } }], \"dynamicDatas\": [], \"dataSetIds\": [] }", "Enquiry Action: CUSTOMER.NAU.RESULT", 0, new Guid("00000000-0000-0000-0000-000000000000"), "CUSTOMER.NAU.RESULT", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("095cc83d-ca2d-47ff-9a7a-b313f9f1ea3f"), null, null, 2, 1, new Guid("b91c4628-1c48-44ce-91fb-64413cb68396"), "02. Account Creation", new Guid("25a583c7-d876-472c-ad69-4cdc88b5d19e"), "{ \"title\": \"Menu: Open Current Account\", \"description\": \"Menu: Open Current Account\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"ProjectResourcing\", \"typicalName\": \"NavigationInfo\", \"parameters\": \",MNU,,,,,,User Menu > Account > Open Current Account\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [] }", "Menu: Open Current Account", 0, new Guid("00000000-0000-0000-0000-000000000000"), "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("36d5524a-ce53-4564-8af5-65c071eda889"), null, null, 2, 2, new Guid("b91c4628-1c48-44ce-91fb-64413cb68396"), "02. Account Creation", new Guid("15e26906-1218-4fda-b3d6-778df2d520ba"), "{ \"title\": \"Create: ACCOUNT,CA.OPEN\", \"description\": \"Create: ACCOUNT,CA.OPEN\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"R17 214 Application\", \"typicalName\": \"ACCOUNT\", \"parameters\": \"CA.OPEN,I\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [\"FC3E812D-FC35-4C53-9904-FC3867CF7FC3\"] }", "Create: ACCOUNT,CA.OPEN", 0, new Guid("00000000-0000-0000-0000-000000000000"), "ACCOUNT", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("67173c6d-b5cc-45e5-8ab4-7345cd1023ec"), null, null, 2, 3, new Guid("b91c4628-1c48-44ce-91fb-64413cb68396"), "02. Account Creation", new Guid("debb4f15-0b9f-41c5-909a-055b36870f69"), "{ \"title\": \"Menu: Sign Off\", \"description\": \"Menu: Sign Off\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"ProjectResourcing\", \"typicalName\": \"NavigationInfo\", \"parameters\": \",MNU,,,,,,Sign Off\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [] }", "Menu: Sign Off", 0, new Guid("00000000-0000-0000-0000-000000000000"), "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("7a247ec7-3c53-4809-beb5-6a99c2fc1589"), null, null, 2, 4, new Guid("b91c4628-1c48-44ce-91fb-64413cb68396"), "02. Account Creation", new Guid("de88d07d-541a-4538-96c2-c1f0c5a45820"), "{ \"title\": \"Menu: Authorise/Delete Account\", \"description\": \"Menu: Authorise/Delete Account\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"ProjectResourcing\", \"typicalName\": \"NavigationInfo\", \"parameters\": \",MNU,,,,,,User Menu > Account > Authorise/Delete Account\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [] }", "Menu: Authorise/Delete Account", 0, new Guid("00000000-0000-0000-0000-000000000000"), "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("cee8a23f-2d3e-4bf9-b788-17bd8eb6f031"), null, null, 2, 5, new Guid("b91c4628-1c48-44ce-91fb-64413cb68396"), "02. Account Creation", new Guid("b3d8225b-42d8-49e8-87aa-f7b0625391fd"), "{ \"title\": \"Enquiry Action: ACCOUNT.NAU.RESULT\", \"description\": \"Enquiry Action: ACCOUNT.NAU.RESULT\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"R17 214 Enquiries\", \"typicalName\": \"ACCOUNT.NAU.RESULT\", \"parameters\": \"CLICK(\\\"Authorise\\\")\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [] }", "Enquiry Action: ACCOUNT.NAU.RESULT", 0, new Guid("00000000-0000-0000-0000-000000000000"), "ACCOUNT.NAU.RESULT", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("13505efa-4e74-4d06-ace1-38ec1cd3a48e"), null, null, 2, 6, new Guid("b91c4628-1c48-44ce-91fb-64413cb68396"), "02. Account Creation", new Guid("023a27a5-7d16-422a-9ddc-5f3d101f864f"), "{ \"title\": \"Authorise: ACCOUNT,AUTH\", \"description\": \"Authorise: ACCOUNT,AUTH\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"R17 214 Application\", \"typicalName\": \"ACCOUNT\", \"parameters\": \"AUTH,A\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [] }", "Authorise: ACCOUNT,AUTH", 0, new Guid("00000000-0000-0000-0000-000000000000"), "ACCOUNT", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("e113cc09-cf89-42c8-8556-ebfbffcbc9af"), null, null, 2, 7, new Guid("b91c4628-1c48-44ce-91fb-64413cb68396"), "02. Account Creation", new Guid("4ac5733e-0277-4641-a69c-4d07dd3724db"), "{ \"title\": \"Menu: Sign Off\", \"description\": \"Menu: Sign Off\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"ProjectResourcing\", \"typicalName\": \"NavigationInfo\", \"parameters\": \",MNU,,,,,,Sign Off\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [] }", "Menu: Sign Off", 0, new Guid("00000000-0000-0000-0000-000000000000"), "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") },
                    { new Guid("60f2b4dc-99b9-4f9d-9512-f13f2d50619b"), null, null, 1, 4, new Guid("a91c4627-1b48-44ce-91fb-64413ca68395"), "01. Customer Creation", new Guid("143b2b9a-0a98-46e3-8703-90427070228e"), "{ \"title\": \"Menu: Authorise/Delete Customer\", \"description\": \"Menu: Authorise/Delete Customer\", \"adapterName\": \"T24WebAdapter\", \"catalogName\": \"ProjectResourcing\", \"typicalName\": \"NavigationInfo\", \"parameters\": \",MNU,,,,,,User Menu > Customer > Authorise/Delete Customer\", \"typicalInstance\": null, \"filters\": null, \"postFilters\": null, \"dynamicDatas\": [], \"dataSetIds\": [] }", "Menu: Authorise/Delete Customer", 0, new Guid("00000000-0000-0000-0000-000000000000"), "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("f91c4627-1b48-44ce-91fb-64413ca68391") }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "DateCreated", "DateModified", "Description", "Title", "UserCreated", "UserModified" },
                values: new object[,]
                {
                    { new Guid("db783f93-8635-47ce-a27a-587d869bdef1"), null, null, "Test Strategist", "Test Strategist", null, null },
                    { new Guid("7859823c-17e2-42eb-9f1b-e2f07e2eb88f"), null, null, "Project 1 description", "Project 1", null, null }
                });

            migrationBuilder.InsertData(
                table: "Systems",
                columns: new[] { "Id", "AdapterName", "DateCreated", "DateModified", "Title", "UserCreated", "UserModified" },
                values: new object[,]
                {
                    { new Guid("2e28b3db-9f0b-4229-8084-04289d910f07"), "T24WebAdapter", null, null, "R17 Author System", null, null },
                    { new Guid("d0e7b0df-e87f-4dbc-8fb4-419cab45d3d5"), "T24WebAdapter", null, null, "R17 Input System", null, null }
                });

            migrationBuilder.InsertData(
                table: "Typicals",
                columns: new[] { "Id", "CatalogId", "DateCreated", "DateModified", "Json", "Title", "Type", "UserCreated", "UserModified", "Xml", "XmlHash" },
                values: new object[,]
                {
                    { new Guid("491942d4-bfbe-43d0-b4be-a235feaba48f"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c5c"), null, null, @"{ ""TypicalName"": ""Customer"",
                	                                  ""Attributes"": [
                                                                          { ""Name"": ""CUSTOMER.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""MNEMONIC"",      ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""FIRST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""LAST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""SHORT.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """",""Length"": ""30"" },
                		                                                  { ""Name"": ""NAME.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""STREET.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""TOWN.COUNTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RELATION.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""SECTOR"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""INDUSTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""NATIONALITY"",  ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""CUSTOMER.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RESIDENCE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""CONTACT.DATE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""REVIEW.FREQUENCY"", ""DataType"": ""string"",  ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """",  ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RECORD.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" }
                	                                                ]
                                                            }", "PREMIUM.ACCOUNT", 1, null, null, null, null },
                    { new Guid("0a3ec581-4599-4266-b86f-4aed07181775"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c5c"), null, null, @"{ ""TypicalName"": ""Customer"",
                	                                  ""Attributes"": [
                                                                          { ""Name"": ""CUSTOMER.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""MNEMONIC"",      ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""FIRST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""LAST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""SHORT.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """",""Length"": ""30"" },
                		                                                  { ""Name"": ""NAME.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""STREET.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""TOWN.COUNTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RELATION.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""SECTOR"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""INDUSTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""NATIONALITY"",  ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""CUSTOMER.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RESIDENCE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""CONTACT.DATE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""REVIEW.FREQUENCY"", ""DataType"": ""string"",  ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """",  ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RECORD.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" }
                	                                                ]
                                                            }", "INCOME.ACCOUNT", 1, null, null, null, null },
                    { new Guid("efe466a1-aaab-4b8a-a0be-60584613af6d"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c5c"), null, null, @"{ ""TypicalName"": ""Customer"",
                	                                  ""Attributes"": [
                                                                          { ""Name"": ""CUSTOMER.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""MNEMONIC"",      ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""FIRST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""LAST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""SHORT.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """",""Length"": ""30"" },
                		                                                  { ""Name"": ""NAME.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""STREET.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""TOWN.COUNTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RELATION.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""SECTOR"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""INDUSTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""NATIONALITY"",  ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""CUSTOMER.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RESIDENCE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""CONTACT.DATE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""REVIEW.FREQUENCY"", ""DataType"": ""string"",  ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """",  ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RECORD.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" }
                	                                                ]
                                                            }", "SAVINGS.ACCOUNT", 1, null, null, null, null },
                    { new Guid("6a3e4c0b-db21-4083-b4a2-7f5bc841f39d"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c5c"), null, null, @"{ ""TypicalName"": ""Customer"",
                	                                  ""Attributes"": [
                                                                          { ""Name"": ""CUSTOMER.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""MNEMONIC"",      ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""FIRST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""LAST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""SHORT.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """",""Length"": ""30"" },
                		                                                  { ""Name"": ""NAME.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""STREET.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""TOWN.COUNTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RELATION.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""SECTOR"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""INDUSTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""NATIONALITY"",  ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""CUSTOMER.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RESIDENCE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""CONTACT.DATE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""REVIEW.FREQUENCY"", ""DataType"": ""string"",  ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """",  ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RECORD.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" }
                	                                                ]
                                                            }", "ACCOUNT", 1, null, null, null, null },
                    { new Guid("859db6b6-335a-43ec-94c4-f3f0db9eb20c"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c1c"), null, null, @"{ ""TypicalName"": ""CUSTOMER.NAU.RESULT"",
                	                                     ""Attributes"": [
                                                                          { ""Name"": ""Customer No"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                                                                          { ""Name"": ""Name"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                                                                          { ""Name"": ""Relationship Officer"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                                                                          { ""Name"": ""Status"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                                                                          { ""Name"": ""Inputter"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                                                                          { ""Name"": ""ID"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" }
                                                                         ]
                                                                }", "CUSTOMER.NAU.RESULT", 4, null, null, null, null },
                    { new Guid("f25c75da-13f1-46e4-b015-f94036633974"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c5c"), null, null, @"{ ""TypicalName"": ""Customer"",
                	                                  ""Attributes"": [
                                                                          { ""Name"": ""CUSTOMER.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""MNEMONIC"",      ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""FIRST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""LAST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""SHORT.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """",""Length"": ""30"" },
                		                                                  { ""Name"": ""NAME.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""STREET.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""TOWN.COUNTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RELATION.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""SECTOR"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""INDUSTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""NATIONALITY"",  ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""CUSTOMER.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RESIDENCE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""CONTACT.DATE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""REVIEW.FREQUENCY"", ""DataType"": ""string"",  ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """",  ""MaxValue"": """", ""Length"": ""30"" },
                		                                                  { ""Name"": ""RECORD.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" }
                	                                                ]
                                                            }", "CUSTOMER", 1, null, null, null, null },
                    { new Guid("9f54584d-bec6-4a96-90be-9b01331617c6"), new Guid("764cffef-ec71-4d16-acf0-ef9f80211c1c"), null, null, "{ \"TypicalName\": \"NavigationInfo\", \"Attributes\": [] }", "NavigationInfo", 1, null, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "WorkflowVersions",
                columns: new[] { "WorkflowId", "VersionId", "DateCreated", "DateModified", "Description", "DesignerJson", "IsLast", "IsSyncWithML", "TestCasesJson", "Title", "UserCreated", "UserModified", "WorkflowImage", "WorkflowPathsJson" },
                values: new object[,]
                {
                    { new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), 0, null, null, null, null, false, false,  null, "Customer workflow", null, null, null, null },
                    { new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), 0, null, null, null, null, false, false,  null, "Customer Creation Scenario 01", null, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "SubProjects",
                columns: new[] { "Id", "DateCreated", "DateModified", "Description", "ProjectId", "Title", "UserCreated", "UserModified" },
                values: new object[] { new Guid("d6b208f6-4eca-4f24-a0b8-dd94462dd91a"), null, null, "Sub project description", new Guid("7859823c-17e2-42eb-9f1b-e2f07e2eb88f"), "Sub project", null, null });

            migrationBuilder.InsertData(
                table: "SubProjects",
                columns: new[] { "Id", "DateCreated", "DateModified", "Description", "ProjectId", "Title", "UserCreated", "UserModified" },
                values: new object[] { new Guid("42afeb37-279e-42f4-8621-5578bab03de1"), null, null, "Main Subproject", new Guid("db783f93-8635-47ce-a27a-587d869bdef1"), "Main Subproject", null, null });

            migrationBuilder.InsertData(
                table: "Folders",
                columns: new[] { "Id", "DateCreated", "DateModified", "Description", "SubProjectId", "Title", "UserCreated", "UserModified" },
                values: new object[] { new Guid("3df209a4-80de-494b-9b0d-753fd597539d"), null, null, "SenseAI Designed TC", new Guid("42afeb37-279e-42f4-8621-5578bab03de1"), "Designed Workflows", null, null });

            migrationBuilder.InsertData(
                table: "Folders",
                columns: new[] { "Id", "DateCreated", "DateModified", "Description", "SubProjectId", "Title", "UserCreated", "UserModified" },
                values: new object[] { new Guid("ca50a224-32cd-49a1-b2ef-aae1e754172d"), null, null, "SenseAI Designed TC", new Guid("42afeb37-279e-42f4-8621-5578bab03de1"), "Designed Workflows", null, null });

            migrationBuilder.InsertData(
                table: "Workflows",
                columns: new[] { "Id", "DateCreated", "DateModified", "Description", "DesignerJson", "FolderId", "IsParsed", "IsValid", "Status", "Title", "UserCreated", "UserModified", "WorkflowPathsJson" },
                values: new object[] { new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), null, null, "Add Customer", null, new Guid("3df209a4-80de-494b-9b0d-753fd597539d"), false, false,  0, "Customer workflow", null, null, null });

            migrationBuilder.InsertData(
                table: "Workflows",
                columns: new[] { "Id", "DateCreated", "DateModified", "Description", "DesignerJson", "FolderId", "IsParsed", "IsValid", "Status", "Title", "UserCreated", "UserModified", "WorkflowPathsJson" },
                values: new object[] { new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), null, null, "Customer Creation Real scenario", null, new Guid("ca50a224-32cd-49a1-b2ef-aae1e754172d"), false, false,  0, "Customer Creation Scenario 01", null, null, null });

            migrationBuilder.InsertData(
                table: "WorkflowItems",
                columns: new[] { "Id", "CatalogTitle", "DateCreated", "DateModified", "Description", "Title", "TypicalTitle", "UserCreated", "UserModified", "WorkflowId", "WorkflowLinkId" },
                values: new object[,]
                {
                    { new Guid("b00a60e1-9ca4-4ed6-8475-751aeafbb325"), "", null, null, "Customer", "Customer", "", null, null, new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("ac80da22-83ea-4f23-8130-ec2f2435080f"), "R17 214 Enquiries", null, null, "Enquiry Action: ACCOUNT.NAU.RESULT", "Enquiry Action: ACCOUNT.NAU.RESULT", "ACCOUNT.NAU.RESULT", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("df8d3ccb-574e-4498-88e7-9d3f9ad1ea8e"), "ProjectResourcing", null, null, "Menu: Authorise/Delete Account", "Menu: Authorise/Delete Account", "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("252399b0-df7e-44f7-914c-0a6da8cc54ac"), "ProjectResourcing", null, null, "Menu: Sign Off", "Menu: Sign Off", "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("1df921d2-cc1a-4539-9594-fd1d329aec5f"), "R17 214 Application", null, null, "Create: ACCOUNT,CA.OPEN", "Create: ACCOUNT,CA.OPEN", "ACCOUNT", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("13c1646d-64ae-4aee-b296-ebc2e61dca48"), "ProjectResourcing", null, null, "Menu: Open Current Account", "Menu: Open Current Account", "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("0c4d967e-37f6-475a-a732-5aa1b2ee8209"), "ProjectResourcing", null, null, "Menu: Sign Off", "Menu: Sign Off", "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("f9a9520d-c010-4581-9939-3fe8ff7abe58"), "R17 214 Application", null, null, "Authorise: CUSTOMER,NAU", "Authorise: CUSTOMER,NAU", "CUSTOMER", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("881ecd3d-ae70-4597-b5df-d95afa30ec55"), "R17 214 Enquiries", null, null, "Enquiry Action: CUSTOMER.NAU.RESULT", "Enquiry Action: CUSTOMER.NAU.RESULT", "CUSTOMER.NAU.RESULT", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("08ac5407-5f14-45cb-b602-782615fc586f"), "ProjectResourcing", null, null, "Menu: Authorise/Delete Customer", "Menu: Authorise/Delete Customer", "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("cf93cc8a-79bc-41bf-89a6-c4909fb4165e"), "ProjectResourcing", null, null, "Menu: Sign Off", "Menu: Sign Off", "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("71315187-dc78-400e-91cd-341e5e4d3985"), "R17 214 Application", null, null, "Create: CUSTOMER,INPUT", "Create: CUSTOMER,INPUT", "CUSTOMER", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("ca8a0d00-6647-40c0-871a-b72def13b466"), "ProjectResourcing", null, null, "Menu: Individual Customer", "Menu: Individual Customer", "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("0c5a4584-248b-4fa0-9536-0371d49cb87b"), "", null, null, "Close account", "Close account", "", null, null, new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("68eb08f2-cd35-40f9-83ac-786e5b45c941"), "", null, null, "Account", "Account", "", null, null, new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("7a63cc83-d485-4b07-ba7d-1262a78bcb9a"), "", null, null, "See", "See", "", null, null, new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("b5e4e820-70dc-47d7-b1a0-722abdaf3d03"), "ACCOUNT", null, null, "Authorise: ACCOUNT,AUTH", "Authorise: ACCOUNT,AUTH", "R17 214 Application", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("42bd487d-7d88-4079-a38c-cd5f10badd9c"), "ProjectResourcing", null, null, "Menu: Sign Off", "Menu: Sign Off", "NavigationInfo", null, null, new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), new Guid("00000000-0000-0000-0000-000000000000") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Catalogs",
                keyColumn: "Id",
                keyValue: new Guid("6c0a8ef3-0c33-4135-b801-25a208e398bd"));

            migrationBuilder.DeleteData(
                table: "Catalogs",
                keyColumn: "Id",
                keyValue: new Guid("764cffef-ec71-4d16-acf0-ef9f80211c1c"));

            migrationBuilder.DeleteData(
                table: "Catalogs",
                keyColumn: "Id",
                keyValue: new Guid("764cffef-ec71-4d16-acf0-ef9f80211c5c"));

            migrationBuilder.DeleteData(
                table: "DataSets",
                keyColumn: "Id",
                keyValue: new Guid("a77e812d-ecd5-4c53-9904-0fd867cf7fc3"));

            migrationBuilder.DeleteData(
                table: "DataSets",
                keyColumn: "Id",
                keyValue: new Guid("fc3e812d-fc35-4c53-9904-fc3867cf7fc3"));

            migrationBuilder.DeleteData(
                table: "DataSetsItems",
                keyColumn: "Id",
                keyValue: new Guid("2195b937-af7c-43b9-867a-fa3f205923c1"));

            migrationBuilder.DeleteData(
                table: "DataSetsItems",
                keyColumn: "Id",
                keyValue: new Guid("259a3e19-cf19-40f4-9a4c-7727194f8ed5"));

            migrationBuilder.DeleteData(
                table: "DataSetsItems",
                keyColumn: "Id",
                keyValue: new Guid("bc41ed3e-207a-4115-8727-de058a3a5c07"));

            migrationBuilder.DeleteData(
                table: "DataSetsItems",
                keyColumn: "Id",
                keyValue: new Guid("c27c8b6b-7cea-4bf0-b132-e78ae4697112"));

            migrationBuilder.DeleteData(
                table: "DataSetsItems",
                keyColumn: "Id",
                keyValue: new Guid("e0c7d70a-7cb5-4822-a0a0-75c9c8c2c979"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("095cc83d-ca2d-47ff-9a7a-b313f9f1ea3f"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("0cdbdbe7-3c52-4824-aac3-f2f541a2b52b"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("13505efa-4e74-4d06-ace1-38ec1cd3a48e"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("1d4e8926-8dfc-404b-ae26-2522ea0abaad"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("1ed5af2e-493e-4087-9b1b-1ce30d5b165c"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("36d5524a-ce53-4564-8af5-65c071eda889"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("38c5c322-fabe-4a76-9198-f3427060b5db"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("4ede453c-02c1-49e4-8b97-72b656f1724a"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("503bfbce-14aa-42b8-93a1-be87679f2677"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("5c013eea-bd18-4f74-8bd4-e4c18c8a15bd"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("60f2b4dc-99b9-4f9d-9512-f13f2d50619b"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("67173c6d-b5cc-45e5-8ab4-7345cd1023ec"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("7a247ec7-3c53-4809-beb5-6a99c2fc1589"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("8e2283ba-9499-4524-9b1d-756a96ace9ec"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("cee8a23f-2d3e-4bf9-b788-17bd8eb6f031"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("daee0f94-1c8c-4d6c-9755-2a37388d1d21"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("e113cc09-cf89-42c8-8556-ebfbffcbc9af"));

            migrationBuilder.DeleteData(
                table: "GeneratedTestCases",
                keyColumn: "Id",
                keyValue: new Guid("e7d562ff-caf6-4d54-aefb-171238a4715e"));

            migrationBuilder.DeleteData(
                table: "SubProjects",
                keyColumn: "Id",
                keyValue: new Guid("d6b208f6-4eca-4f24-a0b8-dd94462dd91a"));

            migrationBuilder.DeleteData(
                table: "Systems",
                keyColumn: "Id",
                keyValue: new Guid("2e28b3db-9f0b-4229-8084-04289d910f07"));

            migrationBuilder.DeleteData(
                table: "Systems",
                keyColumn: "Id",
                keyValue: new Guid("d0e7b0df-e87f-4dbc-8fb4-419cab45d3d5"));

            migrationBuilder.DeleteData(
                table: "Typicals",
                keyColumn: "Id",
                keyValue: new Guid("0a3ec581-4599-4266-b86f-4aed07181775"));

            migrationBuilder.DeleteData(
                table: "Typicals",
                keyColumn: "Id",
                keyValue: new Guid("491942d4-bfbe-43d0-b4be-a235feaba48f"));

            migrationBuilder.DeleteData(
                table: "Typicals",
                keyColumn: "Id",
                keyValue: new Guid("6a3e4c0b-db21-4083-b4a2-7f5bc841f39d"));

            migrationBuilder.DeleteData(
                table: "Typicals",
                keyColumn: "Id",
                keyValue: new Guid("859db6b6-335a-43ec-94c4-f3f0db9eb20c"));

            migrationBuilder.DeleteData(
                table: "Typicals",
                keyColumn: "Id",
                keyValue: new Guid("9f54584d-bec6-4a96-90be-9b01331617c6"));

            migrationBuilder.DeleteData(
                table: "Typicals",
                keyColumn: "Id",
                keyValue: new Guid("efe466a1-aaab-4b8a-a0be-60584613af6d"));

            migrationBuilder.DeleteData(
                table: "Typicals",
                keyColumn: "Id",
                keyValue: new Guid("f25c75da-13f1-46e4-b015-f94036633974"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("08ac5407-5f14-45cb-b602-782615fc586f"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("0c4d967e-37f6-475a-a732-5aa1b2ee8209"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("0c5a4584-248b-4fa0-9536-0371d49cb87b"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("13c1646d-64ae-4aee-b296-ebc2e61dca48"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("1df921d2-cc1a-4539-9594-fd1d329aec5f"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("252399b0-df7e-44f7-914c-0a6da8cc54ac"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("42bd487d-7d88-4079-a38c-cd5f10badd9c"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("68eb08f2-cd35-40f9-83ac-786e5b45c941"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("71315187-dc78-400e-91cd-341e5e4d3985"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("7a63cc83-d485-4b07-ba7d-1262a78bcb9a"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("881ecd3d-ae70-4597-b5df-d95afa30ec55"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("ac80da22-83ea-4f23-8130-ec2f2435080f"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("b00a60e1-9ca4-4ed6-8475-751aeafbb325"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("b5e4e820-70dc-47d7-b1a0-722abdaf3d03"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("ca8a0d00-6647-40c0-871a-b72def13b466"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("cf93cc8a-79bc-41bf-89a6-c4909fb4165e"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("df8d3ccb-574e-4498-88e7-9d3f9ad1ea8e"));

            migrationBuilder.DeleteData(
                table: "WorkflowItems",
                keyColumn: "Id",
                keyValue: new Guid("f9a9520d-c010-4581-9939-3fe8ff7abe58"));

            migrationBuilder.DeleteData(
                table: "WorkflowVersions",
                keyColumns: new[] { "WorkflowId", "VersionId" },
                keyValues: new object[] { new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"), 0 });

            migrationBuilder.DeleteData(
                table: "WorkflowVersions",
                keyColumns: new[] { "WorkflowId", "VersionId" },
                keyValues: new object[] { new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"), 0 });

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("7859823c-17e2-42eb-9f1b-e2f07e2eb88f"));

            migrationBuilder.DeleteData(
                table: "Workflows",
                keyColumn: "Id",
                keyValue: new Guid("677e812c-ecd8-4c53-9904-0fd867cf7ba5"));

            migrationBuilder.DeleteData(
                table: "Workflows",
                keyColumn: "Id",
                keyValue: new Guid("a4198e44-0be2-42fb-929e-150136e14a5b"));

            migrationBuilder.DeleteData(
                table: "Folders",
                keyColumn: "Id",
                keyValue: new Guid("3df209a4-80de-494b-9b0d-753fd597539d"));

            migrationBuilder.DeleteData(
                table: "Folders",
                keyColumn: "Id",
                keyValue: new Guid("ca50a224-32cd-49a1-b2ef-aae1e754172d"));

            migrationBuilder.DeleteData(
                table: "SubProjects",
                keyColumn: "Id",
                keyValue: new Guid("42afeb37-279e-42f4-8621-5578bab03de1"));

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: new Guid("db783f93-8635-47ce-a27a-587d869bdef1"));
        }
    }
}
