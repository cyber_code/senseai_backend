﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations.Migrations
{
    public partial class AddProjectSubprojectOnAtsTestCases : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "ProjectNodref",
                table: "ATSTestCases",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SubProjectNodref",
                table: "ATSTestCases",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProjectNodref",
                table: "ATSTestCases");

            migrationBuilder.DropColumn(
                name: "SubProjectNodref",
                table: "ATSTestCases");
        }
    }
}
