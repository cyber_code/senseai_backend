﻿namespace Logging.Messaging.Persistence.Configurations
{
    internal class MaximumLengthOf
    {
        public const int EXECUTED_BY = 50;
        public const int APP_NAME = 50;
        public const int NAME = 500;
        public const int RESULT_NAME = 300;
        public const int IP_ADDRESS = 20;
        public const int QUERIED_BY = 50;
    }
}