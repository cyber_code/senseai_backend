﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Logging.Messaging.Persistence.Model;

namespace Logging.Messaging.Persistence.Configurations
{
    internal class ConfigCommandLog : IEntityTypeConfiguration<CommandLog>
    {
        public void Configure(EntityTypeBuilder<CommandLog> typicalConfig)
        {
            typicalConfig.ToTable("CommandLogs", "Logs");

            typicalConfig.HasKey(o => o.Id);

            typicalConfig.Property(item => item.Id)
                .IsRequired();

            //typicalConfig.Property(item => item.ExecutedBy)
            //    .HasMaxLength(MaximumLengthOf.EXECUTED_BY)
            //    .IsRequired();

            typicalConfig.Property(item => item.AppName)
                .HasMaxLength(MaximumLengthOf.APP_NAME)
                .IsRequired();

            //typicalConfig.Property(item => item.CalledFrom)
            //    .IsRequired();

            typicalConfig.Property(item => item.Name)
                .HasMaxLength(MaximumLengthOf.NAME)
                .IsRequired();

            typicalConfig.Property(item => item.Json)
                .IsRequired();

            typicalConfig.Property(item => item.ResultName)
                .HasMaxLength(MaximumLengthOf.RESULT_NAME);

            typicalConfig.Property(item => item.Started)
                .IsRequired();

            typicalConfig.Property(item => item.Ended)
                .IsRequired();

            typicalConfig.Property(item => item.ElapsedMilliseconds)
                .IsRequired();

            typicalConfig.Property(item => item.IpAddress)
                .HasMaxLength(MaximumLengthOf.IP_ADDRESS);
        }
    }
}