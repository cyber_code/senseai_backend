﻿using Microsoft.EntityFrameworkCore;
using Logging.Messaging.Persistence.Model;
using System.Reflection;

namespace Logging.Messaging.Persistence
{
    public sealed class MessagingLogContext : DbContext
    {
        public MessagingLogContext(DbContextOptions<MessagingLogContext> options) : base(options)
        {
        }

        public DbSet<CommandLog> CommandLogs { get; set; }

        public DbSet<QueryLog> QueryLogs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}