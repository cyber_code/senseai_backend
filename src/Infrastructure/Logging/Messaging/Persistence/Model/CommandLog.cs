﻿using System;

namespace Logging.Messaging.Persistence.Model
{
    public class CommandLog
    {
        public int Id { get; set; }

        public string ExecutedBy { get; set; }

        public string AppName { get; set; }

        public string CalledFrom { get; set; }

        public string Name { get; set; }

        public string Json { get; set; }

        public string ResultName { get; set; }

        public string ResultJson { get; set; }

        public string ErrorExceptionType { get; set; }

        public string ErrorExceptionMessage { get; set; }

        public string ErrorExceptionStackTrace { get; set; }

        public string ErrorExceptionJson { get; set; }

        public DateTime Started { get; set; }

        public DateTime Ended { get; set; }

        public long ElapsedMilliseconds { get; set; }

        public string IpAddress { get; set; }
    }
}