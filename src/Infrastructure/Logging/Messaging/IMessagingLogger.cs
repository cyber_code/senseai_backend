﻿using Logging.Messaging.Persistence.Model;
using System.Collections.Generic;

namespace Logging.Messaging
{
    public interface IMessagingLogger
    {
        void Log(CommandLog log);

        void Log(QueryLog log);

        bool CheckDB();

        IEnumerable<QueryLog> GetQueryLogs();

        IEnumerable<CommandLog> GetCommandLogs();
    }
}