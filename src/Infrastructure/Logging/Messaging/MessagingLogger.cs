﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Logging.Messaging.Persistence;
using Logging.Messaging.Persistence.Model;

namespace Logging.Messaging
{
    public sealed class MessagingLogger : IMessagingLogger
    {
        private readonly MessagingLogContext _context;

        public MessagingLogger(MessagingLogContext context)
        {
            _context = context;
        }

        public void Log(CommandLog log)
        {
            _context.CommandLogs.Add(log);
            _context.SaveChanges();
        }

        public void Log(QueryLog log)
        {
            _context.QueryLogs.Add(log);
            _context.SaveChanges();
        }

        public bool CheckDB()
        {
            try
            {
                _context.Database.OpenConnection();
                _context.Database.CloseConnection();
            }
            catch (System.Data.SqlClient.SqlException)
            {
                return false;
            }

            return true;
        }

        public IEnumerable<QueryLog> GetQueryLogs()
        {
            return _context.Set<QueryLog>().OrderByDescending(i => i.Id).Take(20);
        }

        public IEnumerable<CommandLog> GetCommandLogs()
        {
            return _context.Set<CommandLog>().OrderByDescending(i => i.Id).Take(20);
        }
    }
}