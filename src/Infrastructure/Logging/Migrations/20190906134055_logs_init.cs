﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Logging.Migrations
{
    public partial class logs_init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Logs");

            migrationBuilder.CreateTable(
                name: "CommandLogs",
                schema: "Logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExecutedBy = table.Column<string>(nullable: true),
                    AppName = table.Column<string>(maxLength: 50, nullable: false),
                    CalledFrom = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 500, nullable: false),
                    Json = table.Column<string>(nullable: false),
                    ResultName = table.Column<string>(maxLength: 300, nullable: true),
                    ResultJson = table.Column<string>(nullable: true),
                    ErrorExceptionType = table.Column<string>(nullable: true),
                    ErrorExceptionMessage = table.Column<string>(nullable: true),
                    ErrorExceptionStackTrace = table.Column<string>(nullable: true),
                    ErrorExceptionJson = table.Column<string>(nullable: true),
                    Started = table.Column<DateTime>(nullable: false),
                    Ended = table.Column<DateTime>(nullable: false),
                    ElapsedMilliseconds = table.Column<long>(nullable: false),
                    IpAddress = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommandLogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QueryLogs",
                schema: "Logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QueriedBy = table.Column<string>(nullable: true),
                    AppName = table.Column<string>(maxLength: 50, nullable: false),
                    CalledFrom = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 500, nullable: false),
                    Json = table.Column<string>(nullable: false),
                    ResultName = table.Column<string>(maxLength: 300, nullable: true),
                    ResultJson = table.Column<string>(nullable: true),
                    ErrorExceptionType = table.Column<string>(nullable: true),
                    ErrorExceptionMessage = table.Column<string>(nullable: true),
                    ErrorExceptionStackTrace = table.Column<string>(nullable: true),
                    ErrorExceptionJson = table.Column<string>(nullable: true),
                    Started = table.Column<DateTime>(nullable: false),
                    Ended = table.Column<DateTime>(nullable: false),
                    ElapsedMilliseconds = table.Column<long>(nullable: false),
                    IpAddress = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueryLogs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommandLogs",
                schema: "Logs");

            migrationBuilder.DropTable(
                name: "QueryLogs",
                schema: "Logs");
        }
    }
}
