﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AdminModel
{
    [Table("Projects")]
    public class ProjectData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public virtual IReadOnlyCollection<SubProjectData> SubProjects { get; set; }
    }
}