﻿using SenseAI.Domain.AdminModel;
using Persistence.Internal;
using System;
using System.Linq;
using SenseAI.Domain.Process.ProcessModel;
using Persistence.Process.ProcessModel;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.WorkflowModel;
using Persistence.DataSetsModel;
using Persistence.Process.SprintsModel;
using Persistence.SystemCatalogModel;
using Microsoft.EntityFrameworkCore;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;

namespace Persistence.AdminModel
{
    public sealed class SubProjectRepository : ISubProjectRepository
    {
        private readonly IBaseRepository<SubProject, SubProjectData> _baseRepository;
        private readonly SenseAIObjectContext _context;
        private readonly IFolderRepository _folderRepository;
        private readonly IIssueTypeRepository _issueTypeRepository;
        private readonly IProcessRepository _processRepository;
        private readonly IDbContext _dbContext;

        public SubProjectRepository(IBaseRepository<SubProject, SubProjectData> baseRepository, SenseAIObjectContext context, IFolderRepository folderRepository, IIssueTypeRepository issueTypeRepository, IProcessRepository processRepository, IDbContext dbContext)
        {
            _baseRepository = baseRepository;
            _context = context;
            _folderRepository = folderRepository;
            _issueTypeRepository = issueTypeRepository;
            _processRepository = processRepository;
            _dbContext = dbContext;
        }

        public void Add(SubProject subProject)
        {
            _baseRepository.Add(subProject);
        }

        public void Delete(Guid id)
        {
            var subProject = _context.Set<SubProjectData>().AsNoTracking().Where(s => s.Id == id);
            if (!subProject.Any())
                throw new Exception(String.Format("Subproject with id: {0}, dosn't exist!", id.ToString()));

            var folders = _context.Set<FolderData>().AsNoTracking().Where(i => i.SubProjectId == id);
            var issuestypes = _context.Set<IssueTypeData>().AsNoTracking().Where(i => i.SubProjectId == id);
            var processes = _context.Set<ProcessData>().AsNoTracking().Where(p => p.SubProjectId == id);
            var catalogs = _context.Set<CatalogData>().AsNoTracking().Where(c => c.SubProjectId == id);
            var catalogIdList = catalogs.ToList().Select(c => c.Id);
            var dataSets = _context.Set<DataSetData>().AsNoTracking().Where(d => d.SubProjectId == id);
            var dataSetIdList = dataSets.ToList().Select(d => d.Id);

            //Delete Folders
            folders.ToList().ForEach(f => _folderRepository.Delete(f.Id));
            //delete issuetype
            issuestypes.ToList().ForEach(f => _issueTypeRepository.Delete(f.Id, true, true));
            //Delete Processes
            processes.ToList().ForEach(p => _processRepository.Delete(p.Id, true));

            //Delete TypicalInstances
            var typicals = _context.Set<TypicalData>().AsNoTracking().Where(t => catalogIdList.Contains(t.CatalogId));
            var typicalIdList = typicals.ToList().Select(t => t.Id);
            var typicalInstances = _context.Set<TypicalInstanceData>().AsNoTracking().Where(ti => typicalIdList.Contains(ti.TypicalId));
            _context.Set<TypicalInstanceData>().RemoveRange(typicalInstances);

            //Delete TypicalChanges
            var typicalChanges = _context.Set<TypicalChangesData>().AsNoTracking().Where(tc => catalogIdList.Contains(tc.CatalogId));
            _context.Set<TypicalChangesData>().RemoveRange(typicalChanges);

            //Delete EnquiryActions
            var enquiryActions = _context.Set<EnquiryActionData>().AsNoTracking().Where(e => catalogIdList.Contains(e.CatalogId));
            _context.Set<EnquiryActionData>().RemoveRange(enquiryActions);

            // Delete Navigations
            var navigations = _context.Set<NavigationData>().AsNoTracking().Where(n => catalogIdList.Contains(n.CatalogId));
            _context.Set<NavigationData>().RemoveRange(navigations);

            // Delete ProductionData
            var productionData = _context.Set<ProductionDataData>().AsNoTracking().Where(p => catalogIdList.Contains(p.CatalogId));
            _context.Set<ProductionDataData>().RemoveRange(productionData);

            // Delete Typicals
            _context.Set<TypicalData>().RemoveRange(typicals);

            // Delete Catalogs
            _context.Set<CatalogData>().RemoveRange(catalogs);

            // Delete DataSetItems
            var dataSetItems = _context.Set<DataSetItemData>().AsNoTracking().Where(dsi => dataSetIdList.Contains(dsi.DataSetId));
            _context.Set<DataSetItemData>().RemoveRange(dataSetItems);

            // Delete DataSets
            _context.Set<DataSetData>().RemoveRange(dataSets);

            // Delete Settings
            var settings = _context.Set<SettingsData>().AsNoTracking().Where(s => s.SubProjectId == id);
            _context.Set<SettingsData>().RemoveRange(settings);

            // Delete Sprints and SprintIssues
            var sprints = _context.Set<SprintsData>().AsNoTracking().Where(s => s.SubProjectId == id);
            var sprintIdList = sprints.ToList().Select(s => s.Id);
            var sprintIssues = _context.Set<SprintIssuesData>().AsNoTracking().Where(si => sprintIdList.Contains(si.SprintId));
            _context.Set<SprintIssuesData>().RemoveRange(sprintIssues);
            _context.Set<SprintsData>().RemoveRange(sprints);

            //Delete SystemCatalogs
            var systemCatalogs = _context.Set<SystemCatalogData>().AsNoTracking().Where(sc => sc.SubprojectId == id);
            _context.Set<SystemCatalogData>().RemoveRange(systemCatalogs);

            //Delete SubProject
            _baseRepository.Delete(id);
        }

        public void Update(SubProject subProject)
        {
            _baseRepository.Update(subProject);
        }

        public SubProject GetSubProjectById(Guid subProject)
        {
            var subProjectResult = (from subProj in _dbContext.Set<SubProjectData>()
                                    where subProj.Id == subProject
                                    select new SubProject(subProj.Id, subProj.ProjectId, subProj.Title,
                                    subProj.Description, subProj.IsJiraIntegrationEnabled, subProj.JiraLink)).FirstOrDefault();
            return subProjectResult;
        }
    }
}