﻿using Persistence.Internal;
using Persistence.Process.SprintsModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AdminModel
{
    [Table("SubProjects")]
    public class SubProjectData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsJiraIntegrationEnabled { get; set; }

        public string JiraLink { get; set; }

        public virtual ProjectData Project { get; set; }

        public virtual IReadOnlyCollection<FolderData> Folders { get; set; }

        public virtual IReadOnlyCollection<SprintsData> Sprints { get; set; }
    }
}