﻿using Core.Extensions;
using SenseAI.Domain.AdminModel;
using Persistence.Internal;
using System;

namespace Persistence.AdminModel
{
    public sealed class SettingsRepository : ISettingsRepository
    {
        private readonly SenseAIObjectContext _context;

        public SettingsRepository(SenseAIObjectContext context)
        {
            _context = context;
        }

        public void Add(Settings settings)
        {
            var dataItems = settings.ProjectedAs<SettingsData>();
            _context.Add(dataItems);
            _context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var data = new SettingsData
            {
                Id = id
            };
            _context.Attach(data);
            _context.Remove(data);

            _context.SaveChanges();
        }

        public void Update(Settings settings)
        {
            var dataItems = settings.ProjectedAs<SettingsData>();

            _context.Attach(dataItems);
            _context.Update(dataItems);

            _context.SaveChanges();
        }
    }
}