﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AdminModel.Configurations
{
    internal class ConfigSubProject : SenseAIEntityTypeConfiguration<SubProjectData>
    {
        public override void Configure(EntityTypeBuilder<SubProjectData> builder)
        {
            builder.ToTable(typeof(ProjectData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ProjectId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(500);

            builder.HasOne(spd => spd.Project)
                .WithMany(pd => pd.SubProjects)
                .HasForeignKey(spd => spd.ProjectId);

            base.Configure(builder);
        }
    }
}