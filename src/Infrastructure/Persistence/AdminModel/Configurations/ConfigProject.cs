﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AdminModel.Configurations
{
    internal class ConfigProject : SenseAIEntityTypeConfiguration<ProjectData>
    {
        public override void Configure(EntityTypeBuilder<ProjectData> builder)
        {
            builder.ToTable(typeof(ProjectData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(500);

            base.Configure(builder);
        }
    }
}