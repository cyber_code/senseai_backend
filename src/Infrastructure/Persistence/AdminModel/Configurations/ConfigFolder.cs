﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AdminModel.Configurations
{
    internal class ConfigFolder : SenseAIEntityTypeConfiguration<FolderData>
    {
        public override void Configure(EntityTypeBuilder<FolderData> builder)
        {
            builder.ToTable(typeof(FolderData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(500);

            builder.HasOne(fpd => fpd.SubProject)
                .WithMany(spd => spd.Folders)
                .HasForeignKey(fpd => fpd.SubProjectId);

            builder.HasMany(i => i.Workflows)
                .WithOne(i => i.Folder)
                .HasForeignKey(i => i.FolderId);

            base.Configure(builder);
        }
    }
}