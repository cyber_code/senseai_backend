﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AdminModel.Configurations
{
    internal class ConfigSettings : SenseAIEntityTypeConfiguration<SettingsData>
    {
        public override void Configure(EntityTypeBuilder<SettingsData> builder)
        {
            builder.ToTable(typeof(SettingsData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ProjectId).IsRequired();
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.SystemId).IsRequired();
            builder.Property(x => x.CatalogId).IsRequired();

            base.Configure(builder);
        }
    }
}