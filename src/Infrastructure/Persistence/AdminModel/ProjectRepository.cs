﻿using SenseAI.Domain.AdminModel;
using Persistence.Internal;
using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Persistence.AdminModel
{
    public sealed class ProjectRepository : IProjectRepository
    {
        private readonly IBaseRepository<Project, ProjectData> _baseRepository;
        private readonly ISubProjectRepository _subProjectRepository;
        private readonly SenseAIObjectContext _context;
        public ProjectRepository(IBaseRepository<Project, ProjectData> baseRepository, ISubProjectRepository subProjectRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _subProjectRepository = subProjectRepository;
            _context = context;
        }

        public void Add(Project project)
        {
            _baseRepository.Add(project);
        }

        public void Delete(Guid id)
        {
            var project = _context.Set<ProjectData>().AsNoTracking().Where(s => s.Id == id);
            var subProjects = _context.Set<SubProjectData>().AsNoTracking().Where(s => s.ProjectId == id);
            if (!project.Any())
                throw new Exception("Project dosn't exist!");
            //Delete SubProjects
            if(subProjects.Any())
                subProjects.ToList().ForEach(p => _subProjectRepository.Delete(p.Id));

            _baseRepository.Delete(id);
        }

        public void Update(Project project)
        {
            _baseRepository.Update(project);
        }
    }
}