﻿using Persistence.Internal;
using Persistence.WorkflowModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AdminModel
{
    [Table("Folders")]
    public class FolderData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid SubProjectId { get; set; }

        public string Title { get; set; } 

        public string Description { get; set; }

        public virtual SubProjectData SubProject { get; set; }

        public virtual ICollection<WorkflowData> Workflows { get; set; }
    }
}