﻿using Microsoft.EntityFrameworkCore;
using SenseAI.Domain.AdminModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using System;
using System.Linq;
using Persistence.Process.IssuesModel;

namespace Persistence.AdminModel
{
    public sealed class FolderRepository : IFolderRepository
    {
        private readonly IBaseRepository<Folder, FolderData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public FolderRepository(IBaseRepository<Folder, FolderData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(Folder folder)
        {
            _baseRepository.Add(folder);
        }

        public void Delete(Guid id)
        {
            var folder = _context.Set<FolderData>().Where(f => f.Id == id).FirstOrDefault();
            if (folder is null)
                throw new Exception(String.Format("Folder with id: {0} dosn't exist!", id.ToString()));
            //get all workflows under this folder
            var workflows = _context.Set<WorkflowData>().Include(i => i.WorkflowItems).Where(i => i.FolderId == id);
            var workflowIdList = workflows.Select(w => w.Id).ToList();
            //delete workflowItems
            foreach (var item in workflows)
            {
                _context.Set<WorkflowItemData>().RemoveRange(item.WorkflowItems);
            }
             
            //delete workflowPaths
            var workflowPaths = _context.Set<WorkflowVersionPathsData>().Where(wp => workflowIdList.Contains(wp.WorkflowId));
            _context.Set<WorkflowVersionPathsData>().RemoveRange(workflowPaths);

            //delete workflowIssues
            var workflowIssues = _context.Set<WorkflowIssueData>().Where(wi => workflowIdList.Contains(wi.WorkflowId));
            _context.Set<WorkflowIssueData>().RemoveRange(workflowIssues);

            //delete workflowVersions
            var workflowVersions = _context.Set<WorkflowVersionData>().Where(wv => workflowIdList.Contains(wv.WorkflowId));
            _context.Set<WorkflowVersionData>().RemoveRange(workflowVersions);

            //delete workflows
            _context.Set<WorkflowData>().RemoveRange(workflows);

            //delete folder 
            _context.Set<FolderData>().Remove(folder);

            _context.SaveChanges();
        }

        public void Update(Folder folder)
        {
            var item = _context.Set<FolderData>().FirstOrDefault(i => i.Id == folder.Id);

            if (item == null)
                throw new Exception("This folder does not exists");

            item.Title = folder.Title;
            item.Description = folder.Description;

            _context.SaveChanges();
        }

        public void Paste(Guid folderId)
        {
            var folder = _context.Set<FolderData>().Include(i => i.Workflows).ThenInclude(i => i.WorkflowItems).FirstOrDefault(t => t.Id == folderId);
            
            if (folder == null)
                throw new Exception("This folder does not exist");

            folder.Id = Guid.NewGuid();

            //add copied folder
            _context.Set<FolderData>().Add(folder);

            //add workflow items
            foreach (var workflow in folder.Workflows)
            {
                workflow.Id = Guid.NewGuid();
                workflow.FolderId = folder.Id;

                _context.Set<WorkflowData>().Add(workflow);

                //add workflowitems
                foreach (var workflowItem in workflow.WorkflowItems)
                {
                    workflowItem.Id = Guid.NewGuid();

                    _context.Set<WorkflowItemData>().Add(workflowItem);
                }
            }

            _context.SaveChanges();
        }

        public Guid ArisFolder(Folder folder)
        {
            var folderData = _context.Set<FolderData>().AsNoTracking().FirstOrDefault(i => i.Title == folder.Title && i.SubProjectId == folder.SubProjectId);
            var list = _context.Set<FolderData>().AsNoTracking().Where(i => i.SubProjectId == folder.SubProjectId);

            if (folderData != null)
                return folderData.Id;
            _baseRepository.Add(folder);
            list = _context.Set<FolderData>().AsNoTracking().Where(i => i.SubProjectId == folder.SubProjectId);
            return folder.Id;
        }
    }
}