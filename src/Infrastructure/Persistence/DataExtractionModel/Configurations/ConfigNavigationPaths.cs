﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.DataExtractionModel;
using Persistence.Internal;

namespace Persistence.CatalogModel.Configurations
{
    internal class ConfigNavigationPaths : SenseAIEntityTypeConfiguration<NavigationPathsData>
    {
        public override void Configure(EntityTypeBuilder<NavigationPathsData> builder)
        {
            builder.ToTable(typeof(NavigationPathsData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Path).HasColumnType("nvarchar(max)").IsRequired();

            base.Configure(builder);
        }
    }
}