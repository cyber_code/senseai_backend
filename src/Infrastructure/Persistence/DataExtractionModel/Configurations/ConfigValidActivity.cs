﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.DataExtractionModel;
using Persistence.Internal;

namespace Persistence.CatalogModel.Configurations
{
    internal class ConfigValidActivity : SenseAIEntityTypeConfiguration<ValidActivitiesData>
    {
        public override void Configure(EntityTypeBuilder<ValidActivitiesData> builder)
        {
            builder.ToTable(typeof(ValidActivitiesData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.CatalogId).IsRequired();
            builder.Property(x => x.ProductLine).HasColumnType("nvarchar(max)").IsRequired();
            builder.Property(x => x.Json).HasColumnType("nvarchar(max)").IsRequired();

            base.Configure(builder);
        }
    }
}