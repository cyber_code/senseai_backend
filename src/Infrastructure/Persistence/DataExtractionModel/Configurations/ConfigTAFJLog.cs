﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.DataExtractionModel;
using Persistence.Internal;

namespace Persistence.CatalogModel.Configurations
{
    internal class ConfigTAFJLog : SenseAIEntityTypeConfiguration<TAFJLogData>
    {
        public override void Configure(EntityTypeBuilder<TAFJLogData> builder)
        {
            builder.ToTable(typeof(TAFJLogData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Date).IsRequired();
            builder.Property(x => x.Code).HasMaxLength(100).IsRequired();
            builder.Property(x => x.User).HasMaxLength(100).IsRequired();
            builder.Property(x => x.Action).HasMaxLength(100);
            builder.Property(x => x.RequestType).HasMaxLength(100);
            builder.Property(x => x.Command).HasColumnType("nvarchar(max)");
            builder.Property(x => x.Data).HasColumnType("xml");
            builder.Property(x => x.Content).HasColumnType("nvarchar(max)");

            base.Configure(builder);
        }
    }
}