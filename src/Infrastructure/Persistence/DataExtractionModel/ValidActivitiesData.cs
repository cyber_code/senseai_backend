﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Persistence.DataExtractionModel
{
    [Table("ValidActivities")]
    public class ValidActivitiesData : BaseData, IData 
    {
        public Guid Id { get; set; }

        public Guid CatalogId { get; set; }

        public string ProductLine { get; set; } 

        public string Json { get; set; }
    }
}
