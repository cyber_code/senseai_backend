﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.DataExtractionModel
{
    [Table("NavigationPaths")]
    public class NavigationPathsData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string Path { get; set; }
    }
}
