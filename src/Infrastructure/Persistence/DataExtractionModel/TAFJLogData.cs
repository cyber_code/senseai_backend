﻿using Persistence.Internal;
using System; 
using System.ComponentModel.DataAnnotations.Schema;  

namespace Persistence.DataExtractionModel
{
    [Table("TAFJLogs")]
    public class TAFJLogData : BaseData, IData 
    {
        public Guid Id { get; set; }

        public string Type { get; set; }

        public DateTime Date { get; set; }

        public string Code { get; set; }

        public string User { get; set; }

        public string Action { get; set; }

        public string RequestType { get; set; }

        public string Command { get; set; }

        public string Data { get; set; }
         
        public string Content { get; set; }
    }
}
