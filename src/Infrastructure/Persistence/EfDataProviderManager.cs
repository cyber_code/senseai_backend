﻿using Core.Abstractions;
using Core.Configuration;
using Core.Exceptions;
using Persistence.Providers;
using System;

namespace Persistence
{
    /// <inheritdoc />
    /// <summary>
    /// Represents the Entity Framework data provider manager
    /// </summary>
    public class EfDataProviderManager : IDataProviderManager
    {
        #region Properties

        /// <inheritdoc />
        /// <summary>
        /// Gets data provider
        /// </summary>
        public IDataProvider GetDataProvider()
        {
            var providerName = DataSettingsManager.Instance.DataSettings.DataProvider;

            switch (providerName)
            {
                case DataProviderType.SqlServer:
                    return new SqlServerDataProvider();

                case DataProviderType.Oracle:
                    throw new NotImplementedException("This provider is not implemented yet!");
                default:
                    throw new SenseAIException($"Not supported data provider name: '{providerName}'");
            }
        }

        #endregion Properties
    }
}