﻿using Persistence.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AAProductBuilderModel
{
    [Table("Fields")]
    public class FieldData : BaseData
    {
        public int Id { get; set; }

        public string FieldId { get; set; }

        public string PropertyClassId { get; set; }

        public string PropertyId { get; set; }

        public string Json { get; set; }

        public string Xml { get; set; }
    }
}