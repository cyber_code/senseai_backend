﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel.Configurations
{
    internal class ConfigProductGroupPropertyFieldState : SenseAIEntityTypeWithSchemaConfiguration<ProductGroupPropertyFieldStateData>
    {
        public override void Configure(EntityTypeBuilder<ProductGroupPropertyFieldStateData> builder)
        {
            builder.ToTable(typeof(ProductGroupPropertyFieldStateData).Name);
            builder.Property(a => a.Id).ValueGeneratedOnAdd();
            builder.HasKey(x => x.Id);
            builder.HasIndex(u => new
            {
                u.SessionId,
                u.ProductGroupId,
                u.PropertyId,
                u.FieldId,
                u.MultiValueIndex,
                u.SubValueIndex,
                u.CurrencyId
            }).IsUnique();

            base.Configure(builder);
        }
    }
}