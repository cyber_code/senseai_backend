﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel.Configurations
{
    internal class ConfigField : SenseAIEntityTypeWithSchemaConfiguration<FieldData>
    {
        public override void Configure(EntityTypeBuilder<FieldData> builder)
        {
            builder.ToTable(typeof(FieldData).Name);
            builder.HasKey(x => new { x.Id });

            base.Configure(builder);
        }
    }
}
