﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel.Configurations
{
    internal class ConfigProductGroupPropertyClass : SenseAIEntityTypeWithSchemaConfiguration<ProductGroupPropertyClassData>
    {
        public override void Configure(EntityTypeBuilder<ProductGroupPropertyClassData> builder)
        {
            builder.ToTable(typeof(ProductGroupPropertyClassData).Name);
            builder.HasKey(x => new { x.ProductGroupId, x.PropertyClassId });

            base.Configure(builder);
        }
    }
}
