﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel.Configurations
{
    internal class ConfigQuery : SenseAIEntityTypeWithSchemaConfiguration<QueryData>
    {
        public override void Configure(EntityTypeBuilder<QueryData> builder)
        {
            builder.ToTable(typeof(QueryData).Name);
            builder.HasKey(x => x.Id);

            base.Configure(builder);
        }
    }
}
