﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel.Configurations
{
    public class ConfigQueryResult : SenseAIQueryTypeConfiguration<QueryResultData>
    {
        public override void Configure(QueryTypeBuilder<QueryResultData> builder)
        {
            base.Configure(builder);
        }
    }
}
