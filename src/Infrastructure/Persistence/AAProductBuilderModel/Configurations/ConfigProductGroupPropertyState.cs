﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel.Configurations
{
    internal class ConfigProductGroupPropertyState : SenseAIEntityTypeWithSchemaConfiguration<ProductGroupPropertyStateData>
    {
        public override void Configure(EntityTypeBuilder<ProductGroupPropertyStateData> builder)
        {
            builder.ToTable(typeof(ProductGroupPropertyStateData).Name);
            builder.HasKey(x => new { x.SessionId, x.ProductGroupId, x.PropertyId, x.CurrencyId });

            base.Configure(builder);
        }
    }
}
