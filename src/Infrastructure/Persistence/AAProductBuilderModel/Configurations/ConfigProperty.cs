﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel.Configurations
{
    internal class ConfigProperty : SenseAIEntityTypeWithSchemaConfiguration<PropertyData>
    {
        public override void Configure(EntityTypeBuilder<PropertyData> builder)
        {
            builder.ToTable(typeof(PropertyData).Name);
            builder.HasKey(x => new { x.Id, x.PropertyClassId });

            base.Configure(builder);
        }
    }
}
