﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel.Configurations
{
    internal class ConfigPropertyClass : SenseAIEntityTypeWithSchemaConfiguration<PropertyClassData>
    {
        public override void Configure(EntityTypeBuilder<PropertyClassData> builder)
        {
            builder.ToTable(typeof(PropertyData).Name);
            builder.HasKey(x => x.Id);

            base.Configure(builder);
        }
    }
}
