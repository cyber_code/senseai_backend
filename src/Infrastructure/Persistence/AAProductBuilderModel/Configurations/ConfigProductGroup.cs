﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;


namespace Persistence.AAProductBuilderModel.Configurations
{
    internal class ConfigProductGroup : SenseAIEntityTypeWithSchemaConfiguration<ProductGroupData>
    {
        public override void Configure(EntityTypeBuilder<ProductGroupData> builder)
        {
            builder.ToTable(typeof(ProductGroupData).Name);
            builder.HasKey(x => x.Id);

            base.Configure(builder);
        }
    }
}
