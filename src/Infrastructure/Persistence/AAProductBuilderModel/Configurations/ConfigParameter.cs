﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel.Configurations
{
    internal class ConfigParameter : SenseAIEntityTypeWithSchemaConfiguration<ParameterData>
    {
        public override void Configure(EntityTypeBuilder<ParameterData> builder)
        {
            builder.ToTable(typeof(ParameterData).Name);
            builder.HasKey(x => x.Id);

            base.Configure(builder);
        }
    }
}
