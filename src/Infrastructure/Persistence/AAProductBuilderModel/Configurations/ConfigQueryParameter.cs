﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel.Configurations
{
    internal class ConfigQueryParameter : SenseAIEntityTypeWithSchemaConfiguration<QueryParameterData>
    {
        public override void Configure(EntityTypeBuilder<QueryParameterData> builder)
        {
            builder.ToTable(typeof(QueryParameterData).Name);
            builder.HasKey(x => new { x.QueryId, x.ParameterId });

            base.Configure(builder);
        }
    }
}
