﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.AAProductBuilderModel
{
    public class QueryResultData : BaseData
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
