﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AAProductBuilderModel
{
    [Table("ProductGroupPropertyStates")]
    public class ProductGroupPropertyStateData : BaseData
    {
        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }

        public string PropertyId { get; set; }

        public string CurrencyId { get; set; }

        public bool IsSelected { get; set; }
    }
}