﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using Persistence.Internal;

namespace Persistence.AAProductBuilderModel
{
    [Table("ProductGroupPropertyFieldStates")]
    public class ProductGroupPropertyFieldStateData : BaseData
    {
        public int Id { get; set; }

        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }

        public string PropertyId { get; set; }

        public string FieldId { get; set; }

        public int MultiValueIndex { get; set; }

        public int SubValueIndex { get; set; }

        public string Value { get; set; }

        public string CurrencyId { get; set; }
    }
}