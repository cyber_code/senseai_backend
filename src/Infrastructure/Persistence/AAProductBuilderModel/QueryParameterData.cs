﻿using Persistence.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AAProductBuilderModel
{
    [Table("QueryParameters")]
    public class QueryParameterData : BaseData
    {
        public string QueryId { get; set; }

        public string ParameterId { get; set; }
    }
}
