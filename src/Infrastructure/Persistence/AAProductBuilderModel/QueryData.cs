﻿using Persistence.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AAProductBuilderModel
{
    [Table("Queries")]
    public class QueryData : BaseData
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }
    }
}
