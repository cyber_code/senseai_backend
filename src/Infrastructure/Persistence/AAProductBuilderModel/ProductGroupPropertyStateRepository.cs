﻿using SenseAI.Domain.AAProductionBuilderModel;
using Persistence.Internal;
using System;
using System.Linq;
using EFCore.BulkExtensions;
using System.Collections.Generic;

namespace Persistence.AAProductBuilderModel
{
    public class ProductGroupPropertyStateRepository : IProductGroupPropertyStateRepository
    {
        private readonly SenseAIObjectContext _saiObjectContext;

        public ProductGroupPropertyStateRepository(SenseAIObjectContext saiObjectContext)
        {
            _saiObjectContext = saiObjectContext;
        }

        public void AddBulk(IList<ProductGroupPropertyState> productGroupPropertyStates)
        {
            var productGroupPropertyStateDatas = new List<ProductGroupPropertyStateData>();

            foreach (var productGroupPropertyState in productGroupPropertyStates)
            {
                var productGroupPropertyStateData = new ProductGroupPropertyStateData
                {
                    SessionId = productGroupPropertyState.SessionId,
                    ProductGroupId = productGroupPropertyState.ProductGroupId,
                    PropertyId = productGroupPropertyState.PropertyId,
                    CurrencyId = productGroupPropertyState.CurrencyId,
                    IsSelected = productGroupPropertyState.IsSelected
                };

                productGroupPropertyStateDatas.Add(productGroupPropertyStateData);
            }

            _saiObjectContext.BulkInsert(productGroupPropertyStateDatas);
            _saiObjectContext.SaveChanges();
        }

        public void Delete(Guid sessionId)
        {
            var productGroupPropertyStates = _saiObjectContext
                                    .Set<ProductGroupPropertyStateData>()
                                    .Where(x => x.SessionId == sessionId);

            _saiObjectContext.Set<ProductGroupPropertyStateData>().RemoveRange(productGroupPropertyStates);
            _saiObjectContext.SaveChanges();
        }

        public void Update(Guid sessionId, Guid productId, string propertyId, string currencyId)
        {
            var productGroupPropertyStateData = new ProductGroupPropertyStateData
            {
                SessionId = sessionId,
                ProductGroupId = productId,
                PropertyId = propertyId,
                CurrencyId = currencyId,
                IsSelected = true
            };

            _saiObjectContext.Set<ProductGroupPropertyStateData>().Update(productGroupPropertyStateData);
            _saiObjectContext.SaveChanges();
        }

        public void UpdateAllProductGroupStatus(Guid sessionId, Guid productId)
        {
            var productPropertyStates = _saiObjectContext
                                    .Set<ProductGroupPropertyStateData>()
                                    .Where(x => x.SessionId == sessionId && x.ProductGroupId == productId)
                                    .ToList()
                                    .Select(i => { i.IsSelected = true; return i; });

            _saiObjectContext.Set<ProductGroupPropertyStateData>().UpdateRange(productPropertyStates);
            _saiObjectContext.SaveChanges();
        }
    }
}