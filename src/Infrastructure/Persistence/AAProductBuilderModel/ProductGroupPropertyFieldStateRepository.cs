﻿using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.AAProductionBuilderModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.AAProductBuilderModel
{
    public class ProductGroupPropertyFieldStateRepository : IProductGroupPropertyFieldStateRepository
    {
        private readonly SenseAIObjectContext _saiObjectContext;

        public ProductGroupPropertyFieldStateRepository(SenseAIObjectContext saiObjectContext)
        {
            _saiObjectContext = saiObjectContext;
        }

        public void Add(ProductGroupPropertyFieldState productGroupPropertyFieldState)
        {
            var productPropertyFieldStateData = new ProductGroupPropertyFieldStateData
            {
                SessionId = productGroupPropertyFieldState.SessionId,
                ProductGroupId = productGroupPropertyFieldState.ProductGroupId,
                PropertyId = productGroupPropertyFieldState.PropertyId,
                FieldId = productGroupPropertyFieldState.FieldId,
                MultiValueIndex = productGroupPropertyFieldState.MultiValueIndex,
                SubValueIndex = productGroupPropertyFieldState.SubValueIndex,
                Value = productGroupPropertyFieldState.Value,
                CurrencyId = productGroupPropertyFieldState.CurrencyId
            };

            _saiObjectContext.Add(productPropertyFieldStateData);
            _saiObjectContext.SaveChanges();
        }

        public void AddBulk(IList<ProductGroupPropertyFieldState> productGroupPropertyFieldStates)
        {
            var productGroupPropertyFieldStateData = new List<ProductGroupPropertyFieldStateData>();

            foreach (var productGroupPropertyFieldState in productGroupPropertyFieldStates)
            {
                var productPropertyFieldStateData = new ProductGroupPropertyFieldStateData
                {
                    SessionId = productGroupPropertyFieldState.SessionId,
                    ProductGroupId = productGroupPropertyFieldState.ProductGroupId,
                    PropertyId = productGroupPropertyFieldState.PropertyId,
                    FieldId = productGroupPropertyFieldState.FieldId,
                    MultiValueIndex = productGroupPropertyFieldState.MultiValueIndex,
                    SubValueIndex = productGroupPropertyFieldState.SubValueIndex,
                    Value = productGroupPropertyFieldState.Value,
                    CurrencyId = productGroupPropertyFieldState.CurrencyId
                };

                productGroupPropertyFieldStateData.Add(productPropertyFieldStateData);
            }

            _saiObjectContext.BulkInsert(productGroupPropertyFieldStateData);
            _saiObjectContext.SaveChanges();
        }

        public void Update(ProductGroupPropertyFieldState productGroupPropertyFieldState)
        {
            var productPropertyFieldStateData = new ProductGroupPropertyFieldStateData
            {
                SessionId = productGroupPropertyFieldState.SessionId,
                ProductGroupId = productGroupPropertyFieldState.ProductGroupId,
                PropertyId = productGroupPropertyFieldState.PropertyId,
                FieldId = productGroupPropertyFieldState.FieldId,
                MultiValueIndex = productGroupPropertyFieldState.MultiValueIndex,
                SubValueIndex = productGroupPropertyFieldState.SubValueIndex,
                Value = productGroupPropertyFieldState.Value,
                CurrencyId = productGroupPropertyFieldState.CurrencyId
            };

            _saiObjectContext.Update(productPropertyFieldStateData);
            _saiObjectContext.SaveChanges();
        }

        public void Delete(Guid sessionId)
        {
            var productGroupPropertyFieldStates = _saiObjectContext
                                            .Set<ProductGroupPropertyFieldStateData>()
                                            .Where(x => x.SessionId == sessionId);

            _saiObjectContext.Set<ProductGroupPropertyFieldStateData>().RemoveRange(productGroupPropertyFieldStates);
            _saiObjectContext.SaveChanges();
        }

        public void Delete(Guid sessionId, Guid productGroupId, string propertyId, string currencyId)
        {
            var productGroupPropertyFieldStates = _saiObjectContext.Set<ProductGroupPropertyFieldStateData>()
                                                                   .Where(x => x.SessionId == sessionId
                                                                               && x.ProductGroupId == productGroupId
                                                                               && x.PropertyId == propertyId
                                                                               && x.CurrencyId == currencyId);

            _saiObjectContext.Set<ProductGroupPropertyFieldStateData>().RemoveRange(productGroupPropertyFieldStates);
            _saiObjectContext.SaveChanges();
        }

        public List<ProductGroupPropertyFieldState> Get(Guid sessionId, Guid productGroupId)
        {
            return _saiObjectContext.Set<ProductGroupPropertyFieldStateData>().AsNoTracking()
                                    .Where(w => w.SessionId == sessionId
                                                && w.ProductGroupId == productGroupId)
                                    .Select(x => new ProductGroupPropertyFieldState
                                    {
                                        SessionId = x.SessionId,
                                        ProductGroupId = x.ProductGroupId,
                                        PropertyId = x.PropertyId,
                                        FieldId = x.FieldId,
                                        MultiValueIndex = x.MultiValueIndex,
                                        SubValueIndex = x.SubValueIndex,
                                        Value = x.Value,
                                        CurrencyId = x.CurrencyId
                                    })
                                    .ToList();
        }
    }
}