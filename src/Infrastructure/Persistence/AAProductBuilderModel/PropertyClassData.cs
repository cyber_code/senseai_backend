﻿using Persistence.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AAProductBuilderModel
{
    [Table("PropertyClasses")]
    public class PropertyClassData : BaseData
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public bool IsCurrencySpecific { get; set; }
    }
}
