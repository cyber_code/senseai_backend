﻿using Persistence.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AAProductBuilderModel
{
    [Table("Parameters")]
    public class ParameterData : BaseData
    {
        public string Id { get; set; }
    }
}
