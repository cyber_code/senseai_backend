﻿using Persistence.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AAProductBuilderModel
{
    [Table("Properties")]
    public class PropertyData : BaseData
    {
        public string Id { get; set; }

        public string PropertyClassId { get; set; }

        public string Description { get; set; }
    }
}
