﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AAProductBuilderModel
{
    [Table("ProductGroupPropertyClasses")]
    public class ProductGroupPropertyClassData : BaseData
    {
        public Guid ProductGroupId { get; set; }

        public string PropertyClassId { get; set; }
    }
}
