﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.AAProductBuilderModel
{
    [Table("ProductGroups")]
    public class ProductGroupData : BaseData
    {
        public Guid Id { get; set; }

        public string Title { get; set; }
    }
}
