﻿using Microsoft.EntityFrameworkCore;
using Core.Extensions;
using SenseAI.Domain.WorkflowModel;
using Persistence.Internal;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Persistence.WorkflowModel
{
    public sealed class WorkflowVersionPathsRepository : IWorkflowVersionPathsRepository
    {
        private readonly SenseAIObjectContext _context;

        public WorkflowVersionPathsRepository(SenseAIObjectContext context)
        {
            _context = context;
        }

        public void Add(WorkflowVersionPaths[] workflowPaths)
        {
            WorkflowVersionPaths versionPath = workflowPaths.FirstOrDefault();

            var previousVersion = _context.Set<WorkflowVersionPathDetailsData>().AsNoTracking().Where(
                       w => w.WorkflowId == versionPath.WorkflowId &&
                       w.VersionId == versionPath.VersionId - 1);


            var workflowVersionPathsDatas = workflowPaths.Select(s => new WorkflowVersionPaths(s.Id, s.WorkflowId, s.VersionId,
                        s.Title, s.Selected, s.Coverage, s.Index, true)).ProjectedAsCollection<WorkflowVersionPathsData>();

            var workflowVersionPathDetailsData = new List<WorkflowVersionPathDetailsData>();
            workflowPaths.ToList().ForEach(f =>
            {
                var items = f.Items.ProjectedAsCollection<WorkflowVersionPathDetailsData>();
                if (previousVersion != null && previousVersion.Count() > 0)
                {
                    items.ToList().ForEach(f1 =>
                    {
                        var item = previousVersion.FirstOrDefault(w => w.Id == f1.Id);
                        if (item != null)
                            f1.DataSetId = item.DataSetId;
                    });
                }
                workflowVersionPathDetailsData.AddRange(items);
            });

            var lastWorkflowVersionPathData = _context.Set<WorkflowVersionPathsData>().Where(w => w.WorkflowId == versionPath.WorkflowId && w.IsLast).ToList();
            lastWorkflowVersionPathData.ForEach(f => { f.IsLast = false; });

            _context.UpdateRange(lastWorkflowVersionPathData);
            _context.AddRange(workflowVersionPathsDatas);
            _context.AddRange(workflowVersionPathDetailsData);
            _context.SaveChanges();
        }

        public void Update(WorkflowVersionPaths[] workflowPaths)
        {
            WorkflowVersionPaths versionPath = workflowPaths.FirstOrDefault();

            var items2Remove = _context.Set<WorkflowVersionPathDetailsData>().Where(w => w.WorkflowId == versionPath.WorkflowId && w.VersionId == versionPath.VersionId);
            bool exported2ATS = _context.Set<WorkflowTestCasesData>().AsNoTracking().Where(w => w.WorkflowId == versionPath.WorkflowId && w.VersionId == versionPath.VersionId).Count() > 0;
            List<WorkflowVersionPathsData> paths2Remove = new List<WorkflowVersionPathsData>();
            if (!exported2ATS)
            {
                 paths2Remove = _context.Set<WorkflowVersionPathsData>().Where(w => w.WorkflowId == versionPath.WorkflowId && w.VersionId == versionPath.VersionId).ToList(); 
            }

            var items2Add = new List<WorkflowVersionPathDetailsData>();
            List<WorkflowVersionPathsData> paths2Add = new List<WorkflowVersionPathsData>();
            if (!exported2ATS)
            {
                paths2Add = workflowPaths.Select(s => new WorkflowVersionPaths(s.Id, s.WorkflowId, s.VersionId,
                            s.Title, s.Selected, s.Coverage, s.Index, true)).ProjectedAsCollection<WorkflowVersionPathsData>();
            }
            int pathIndex = 0;
            workflowPaths.ToList().ForEach(f =>
            {
                var items = f.Items.ProjectedAsCollection<WorkflowVersionPathDetailsData>();
               
                Guid pathId = Guid.Empty;

                items.ToList().OrderBy(s => s.Index).ToList().ForEach(f1 =>
                  {
                      var itemsinDb = items2Remove.Where(w => w.Id == f1.Id).ToList();
                      var item = itemsinDb.Count() > 1 ? itemsinDb[f.Index] : itemsinDb.FirstOrDefault();
                      if (item != null)
                      {
                          f1.DataSetId = item.DataSetId;
                          if (exported2ATS)
                          {
                              f1.PathId = item.PathId;
                              pathId = item.PathId;
                          }
                      }
                      items2Add.Add(f1);
                  });

                if (pathId != Guid.Empty && exported2ATS)
                {
                    items2Add.Where(w => w.PathId == f.Id).ToList().ForEach(fp => fp.PathId = pathId);
                }
                pathIndex++;
            });

            _context.Set<WorkflowVersionPathDetailsData>().RemoveRange(items2Remove);
            _context.Set<WorkflowVersionPathsData>().RemoveRange(paths2Remove);
            _context.Set<WorkflowVersionPathsData>().AddRange(paths2Add);
            _context.Set<WorkflowVersionPathDetailsData>().AddRange(items2Add);            
            _context.SaveChanges();
        }

        public WorkflowVersionPaths Get(Guid workflowId, Guid pathId)
        {
            var workflowVersionPathData = _context.Set<WorkflowVersionPathsData>().AsNoTracking()
               .FirstOrDefault(w => w.WorkflowId == workflowId && w.Id == pathId && w.Selected == true && w.IsLast);

            var workflowVersionPath = new WorkflowVersionPaths(workflowVersionPathData.Id, workflowVersionPathData.WorkflowId, workflowVersionPathData.VersionId,
                workflowVersionPathData.Title, workflowVersionPathData.Selected, workflowVersionPathData.Coverage, workflowVersionPathData.Index, workflowVersionPathData.IsLast);

            var workflowVersionPathDetailsData = _context.Set<WorkflowVersionPathDetailsData>().AsNoTracking()
               .Where(w => w.WorkflowId == workflowId && w.PathId == pathId).OrderBy(o => o.Index).ToList();

            workflowVersionPathDetailsData.ForEach(workflowPath =>
            {
                workflowVersionPath.AddItem(workflowPath.Id, workflowPath.WorkflowId, workflowPath.VersionId,
                   workflowPath.PathId, workflowPath.Title, workflowPath.Type, workflowPath.ActionType, workflowPath.CatalogId, workflowPath.TypicalId,
                   workflowPath.TypicalName, workflowPath.TypicalType, workflowPath.DataSetId, workflowPath.PathItemJson, workflowPath.Index);
            });

            return workflowVersionPath;
        }

        public List<WorkflowVersionPaths> GetVersionPaths(Guid workflowId, int versionId)
        {
            var workflowVersionPathDatas = _context.Set<WorkflowVersionPathsData>().AsNoTracking()
               .Where(w => w.WorkflowId == workflowId && w.VersionId == versionId && w.Selected == true);

            var workflowVersionPath = workflowVersionPathDatas.Select(s => new WorkflowVersionPaths(s.Id, s.WorkflowId, s.VersionId,
                s.Title, s.Selected, s.Coverage, s.Index, s.IsLast)).ToList();

            return workflowVersionPath;
        }

        public List<WorkflowVersionPaths> Get(Guid workflowId, int prevVersionId)
        {
            List<WorkflowVersionPaths> workflowVersionPaths = new List<WorkflowVersionPaths>();
            var workflowVersionPatshData = _context.Set<WorkflowVersionPathsData>().AsNoTracking()
               .Where(w => w.WorkflowId == workflowId && w.VersionId == prevVersionId).ToList();

            workflowVersionPatshData.ForEach(workflowVersionPathData =>
            {
                var workflowVersionPath = new WorkflowVersionPaths(workflowVersionPathData.Id, workflowVersionPathData.WorkflowId, workflowVersionPathData.VersionId,
               workflowVersionPathData.Title, workflowVersionPathData.Selected, workflowVersionPathData.Coverage, workflowVersionPathData.Index, workflowVersionPathData.IsLast);


                var workflowVersionPathDetailsData = _context.Set<WorkflowVersionPathDetailsData>().AsNoTracking()
                .Where(w => w.WorkflowId == workflowId && w.PathId == workflowVersionPathData.Id).ToList();

                workflowVersionPathDetailsData.ForEach(workflowPathDetail =>
                {
                    workflowVersionPath.AddItem(workflowPathDetail.Id, workflowPathDetail.WorkflowId, workflowPathDetail.VersionId,
                       workflowPathDetail.PathId, workflowPathDetail.Title, workflowPathDetail.Type, workflowPathDetail.ActionType, workflowPathDetail.CatalogId, workflowPathDetail.TypicalId,
                       workflowPathDetail.TypicalName, workflowPathDetail.TypicalType, workflowPathDetail.DataSetId, workflowPathDetail.PathItemJson, workflowPathDetail.Index);
                });

                workflowVersionPaths.Add(workflowVersionPath);
            });

            return workflowVersionPaths;
        }

        public void Update(Guid workflowId, Guid workflowPathId, Guid pathItemId, int index, Guid dataSetId)
        {
            var itemToUpdate = _context.Set<WorkflowVersionPathDetailsData>().AsNoTracking().Where(
                w => w.WorkflowId == workflowId && w.PathId == workflowPathId && w.Id == pathItemId && w.Index == index).FirstOrDefault();

            itemToUpdate.DataSetId = dataSetId;

            _context.Update(itemToUpdate);
            _context.SaveChanges();
        }

        public void UpdateOnLastVersion(Guid oldDataSetId, Guid newDataSetId)
        {
            var pathDetails = (from wp in _context.Set<WorkflowVersionPathDetailsData>().AsNoTracking()
                               join p in _context.Set<WorkflowVersionPathsData>().AsNoTracking() on wp.PathId equals p.Id
                               where wp.DataSetId == oldDataSetId && p.IsLast && wp.ActionType == (int)ActionType.Input
                               select wp).ToList();

            pathDetails.ForEach(f => { f.DataSetId = newDataSetId; });

            _context.UpdateRange(pathDetails);
            _context.SaveChanges();
        }

        public bool CheckDataSetOnLastVersions(Guid workflowId, Guid dataSetId)
        {
            return (from wp in _context.Set<WorkflowVersionPathDetailsData>().AsNoTracking()
                    join p in _context.Set<WorkflowVersionPathsData>().AsNoTracking() on wp.PathId equals p.Id
                    where wp.DataSetId == dataSetId && wp.WorkflowId != workflowId && p.IsLast && wp.ActionType == (int)ActionType.Input
                    select wp.Id).Count() > 0;
        }

        public bool CheckDataSetOnNotLastVersions(Guid workflowId, Guid dataSetId)
        {
            return (from wp in _context.Set<WorkflowVersionPathDetailsData>().AsNoTracking()
                    join p in _context.Set<WorkflowVersionPathsData>().AsNoTracking() on wp.PathId equals p.Id
                    where wp.DataSetId == dataSetId && wp.WorkflowId != workflowId && !p.IsLast && wp.ActionType == (int)ActionType.Input
                    select wp.Id).Count() > 0;
        }
    }
}