﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("WorkflowItemVersions")]
    public class WorkflowItemVersionData : BaseData
    {
        public Guid ItemId { get; set; }

        public Guid WorkflowId { get; set; }

        public int VersionId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string CatalogTitle { get; set; }

        public string TypicalTitle { get; set; }

        public virtual WorkflowData Workflow { get; set; }

        public Guid WorkflowLinkId { get; set; }

        public int WorkflowLinkVersionId { get; set; }
    }
}