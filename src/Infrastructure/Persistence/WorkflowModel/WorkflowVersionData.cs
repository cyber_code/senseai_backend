﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("WorkflowVersions")]
    public class WorkflowVersionData : BaseData
    {
        public Guid WorkflowId { get; set; }

        public int VersionId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string DesignerJson { get; set; }

        public string WorkflowPathsJson { get; set; }

        public string TestCasesJson { get; set; }

        public bool IsLast { get; set; }

        public bool IsSyncWithML { get; set; }

        public string WorkflowImage { get; set; }

        public DateTime? DateCreated { get; set; }

        public bool IsParsed { get; set; }
    }
}