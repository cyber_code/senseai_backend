﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("WorkflowTestCases")]
    public class WorkflowTestCasesData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid WorkflowId { get; set; }

        public int VersionId { get; set; }

        public Guid WorkflowPathId { get; set; }

        public Guid TestCaseId { get; set; }

        public string TestCaseTitle { get; set; }

        public int TCIndex { get; set; }

        public Guid TestStepId { get; set; }

        public string TestStepTitle { get; set; }

        public string TestStepJson { get; set; }

        public string TypicalName { get; set; }

        public Guid TypicalId { get; set; }

        public int TSIndex { get; set; }

        public int TestStepType { get; set; }
    }
}