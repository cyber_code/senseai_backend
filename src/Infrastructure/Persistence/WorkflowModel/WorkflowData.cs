﻿using Persistence.AdminModel;
using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("Workflows")]
    public class WorkflowData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string DesignerJson { get; set; }

        public string WorkflowPathsJson { get; set; }

        public Guid FolderId { get; set; }

        public virtual FolderData Folder { get; set; }

        public virtual ICollection<WorkflowItemData> WorkflowItems { get; set; }

        public bool IsValid { get; set; }

        public int Status { get; set; }

        public bool IsParsed { get; set; }

        public bool isIssued { get; set; }

        public bool generatedTestCases { get; set; }
    }
}