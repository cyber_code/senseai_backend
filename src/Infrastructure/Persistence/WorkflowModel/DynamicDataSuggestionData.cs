﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("DynamicDataSuggestions")]
    public class DynamicDataSuggestionData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string SourceTypicalName { get; set; }

        public string SourceTypicalAttributeName { get; set; }

        public string TargetTypicalName { get; set; }

        public string TargetTypicalAttributeName { get; set; }

        public int Frequency { get; set; }
    }
}