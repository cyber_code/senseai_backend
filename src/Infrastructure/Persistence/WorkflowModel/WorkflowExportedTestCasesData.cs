﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("WorkflowExportedTestCases")]
    public class WorkflowExportedTestCasesData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid WorkflowId { get; set; }

        public int VersionId { get; set; }

        public bool IsLast { get; set; }

        public string SenseAITestCases { get; set; }

        public string AtsTestCases { get; set; }
    }
}