﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("WorkflowItems")]
    public class WorkflowItemData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid WorkflowId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string CatalogTitle { get; set; }

        public string TypicalTitle { get; set; }

        public virtual WorkflowData Workflow { get; set; }

        public Guid WorkflowLinkId { get; set; }

        public int WorkflowLinkVersionId { get; set; }
    }
}