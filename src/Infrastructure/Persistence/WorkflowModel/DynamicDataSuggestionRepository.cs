﻿using Microsoft.EntityFrameworkCore;
using Core.Extensions;
using SenseAI.Domain.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using System;
using System.Linq;

namespace Persistence.WorkflowModel
{
    public sealed class DynamicDataSuggestionRepository : IDynamicDataSuggestionRepository
    {
        private readonly SenseAIObjectContext _context;

        public DynamicDataSuggestionRepository(SenseAIObjectContext context)
        {
            _context = context;
        }

        public void Add(DynamicDataSuggestion dynamicDataSuggestion)
        {
            var dynamicDataSuggestionData = new DynamicDataSuggestionData
            {
                Id = dynamicDataSuggestion.Id,
                SourceTypicalName = dynamicDataSuggestion.SourceTypicalName,
                SourceTypicalAttributeName = dynamicDataSuggestion.SourceTypicalAttributeName,
                TargetTypicalName = dynamicDataSuggestion.TargetTypicalName,
                TargetTypicalAttributeName=dynamicDataSuggestion.TargetTypicalAttributeName,
                Frequency= dynamicDataSuggestion.Frequency
            };


            _context.Add(dynamicDataSuggestionData);
            _context.SaveChanges();
        }

        public void Update(DynamicDataSuggestion dynamicDataSuggestion)
        {
            var dynamicDataSuggestionData = _context.Set<DynamicDataSuggestionData>().AsNoTracking().FirstOrDefault(t => t.Id == dynamicDataSuggestion.Id);
            if (dynamicDataSuggestionData != null)
            {
                dynamicDataSuggestionData.Frequency = dynamicDataSuggestion.Frequency;
                _context.Set<DynamicDataSuggestionData>().Update(dynamicDataSuggestionData);
            }

            _context.SaveChanges();

        }

        public DynamicDataSuggestion Get(string sourceTypicalName, string sourceTypicalAttributeName, string targetTypicalName, string targetTypicalAttributeName)
        {
            var dynamicDataSuggestionDatas = _context.Set<DynamicDataSuggestionData>().AsNoTracking()
               .Where(item => item.SourceTypicalName == sourceTypicalName && item.SourceTypicalAttributeName == sourceTypicalAttributeName && item.TargetTypicalName==targetTypicalName&& item.TargetTypicalAttributeName==targetTypicalAttributeName).ToList();
            var result = dynamicDataSuggestionDatas.Select(dynamicDataSuggestion => new DynamicDataSuggestion(dynamicDataSuggestion.Id,dynamicDataSuggestion.SourceTypicalName,dynamicDataSuggestion.SourceTypicalAttributeName,dynamicDataSuggestion.TargetTypicalName,dynamicDataSuggestion.TargetTypicalAttributeName,dynamicDataSuggestion.Frequency));

            return result.FirstOrDefault();
           
        }
    }
}