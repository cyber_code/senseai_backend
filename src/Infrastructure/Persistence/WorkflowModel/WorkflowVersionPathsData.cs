﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("WorkflowVersionPaths")]
    public class WorkflowVersionPathsData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid WorkflowId { get; set; }

        public int VersionId { get; set; }

        public string Title { get; set; }

        public bool Selected { get; set; }

        [Column(TypeName = "decimal(18,4)")]
        public decimal Coverage { get; set; }

        public int Index { get; set; }

        public bool IsLast { get; set; }
    }
}