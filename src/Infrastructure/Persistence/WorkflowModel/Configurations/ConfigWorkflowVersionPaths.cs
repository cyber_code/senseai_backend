﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;
using Persistence.WorkflowModel;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigWorkflowVersionPaths : SenseAIEntityTypeConfiguration<WorkflowVersionPathsData>
    {
        public override void Configure(EntityTypeBuilder<WorkflowVersionPathsData> builder)
        {
            builder.ToTable(typeof(WorkflowVersionPathsData).Name);
            builder.HasKey(x => new { x.Id, x.WorkflowId, x.VersionId });
            builder.Property(x => x.WorkflowId).IsRequired();
            builder.Property(x => x.VersionId).IsRequired();
            builder.Property(x => x.Title).IsRequired();
            builder.Property(x => x.Selected).IsRequired();
            builder.Property(x => x.Coverage).IsRequired();
            builder.Property(x => x.Index);

            base.Configure(builder);
        }
    }
}