﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigWorkflowItem : SenseAIEntityTypeConfiguration<WorkflowItemData>
    {
        public override void Configure(EntityTypeBuilder<WorkflowItemData> workflowItemConfig)
        {
            workflowItemConfig.HasKey(x => new { x.Id, x.WorkflowId});
            workflowItemConfig.Property(t => t.WorkflowId).IsRequired();
            workflowItemConfig.Property(t => t.WorkflowLinkId).IsRequired();
            workflowItemConfig.Property(t => t.WorkflowLinkVersionId).IsRequired();
            workflowItemConfig.Property(t => t.Title)
                            .IsRequired();

            workflowItemConfig.Property(t => t.Description)
                .IsRequired();

            base.Configure(workflowItemConfig);
        }
    }
}