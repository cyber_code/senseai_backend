﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigWorkflow : SenseAIEntityTypeConfiguration<WorkflowData>
    {
        public override void Configure(EntityTypeBuilder<WorkflowData> workflowConfig)
        {
            workflowConfig.HasKey(o => o.Id);

            workflowConfig.Property(t => t.FolderId)
                .IsRequired();

            workflowConfig.Property(t => t.Title).HasMaxLength(200)
                .IsRequired();

            workflowConfig.Property(t => t.Description).HasMaxLength(300)
                .IsRequired();

            workflowConfig.HasMany(b => b.WorkflowItems)
                .WithOne(t => t.Workflow).HasForeignKey(t => t.WorkflowId);

            base.Configure(workflowConfig);
        }
    }
}