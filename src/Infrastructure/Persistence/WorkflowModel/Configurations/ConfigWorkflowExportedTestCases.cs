﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigWorkflowExportedTestCases : SenseAIEntityTypeConfiguration<WorkflowExportedTestCasesData>
    {
        public override void Configure(EntityTypeBuilder<WorkflowExportedTestCasesData> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.WorkflowId).IsRequired();

            base.Configure(builder);
        }
    }
}