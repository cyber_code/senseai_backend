﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigGeneratedTestCases : SenseAIEntityTypeConfiguration<WorkflowTestCasesData>
    {
        public override void Configure(EntityTypeBuilder<WorkflowTestCasesData> builder)
        {
            builder.ToTable(typeof(WorkflowTestCasesData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.WorkflowId).IsRequired();
            builder.Property(x => x.WorkflowPathId).IsRequired();
            builder.Property(x => x.TestCaseId).IsRequired();
            builder.Property(x => x.TestCaseTitle).IsRequired();
            builder.Property(x => x.TCIndex).IsRequired();
            builder.Property(x => x.TestStepId).IsRequired();
            builder.Property(x => x.TestStepTitle).IsRequired();
            builder.Property(x => x.TestStepJson).IsRequired();
            builder.Property(x => x.TypicalName).IsRequired();
            builder.Property(x => x.TSIndex).IsRequired();

            base.Configure(builder);
        }
    }
}