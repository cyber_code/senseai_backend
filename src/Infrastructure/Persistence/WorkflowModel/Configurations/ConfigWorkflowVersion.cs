﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.WorkflowModel.Configurations
{
    public class ConfigWorkflowVersion : SenseAIEntityTypeConfiguration<WorkflowVersionData>
    {
        public override void Configure(EntityTypeBuilder<WorkflowVersionData> workflowVersionConfig)
        {
            workflowVersionConfig.HasKey(t => new { t.WorkflowId, t.VersionId });

            workflowVersionConfig.Property(e => e.VersionId);

            workflowVersionConfig.Property(t => t.Title)
                .IsRequired();

            base.Configure(workflowVersionConfig);
        }
    }
}