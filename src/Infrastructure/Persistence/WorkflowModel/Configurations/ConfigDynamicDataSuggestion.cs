﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigDynamicDataSuggestion : SenseAIEntityTypeConfiguration<DynamicDataSuggestionData>
    {
        public override void Configure(EntityTypeBuilder<DynamicDataSuggestionData> builder)
        {
            builder.ToTable(typeof(DynamicDataSuggestionData).Name);
            builder.HasKey(x => x.Id);

            base.Configure(builder);
        }
    }
}