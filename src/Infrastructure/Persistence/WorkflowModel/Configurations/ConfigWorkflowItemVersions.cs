﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigWorkflowItemVersions : SenseAIEntityTypeConfiguration<WorkflowItemVersionData>
    {
        public override void Configure(EntityTypeBuilder<WorkflowItemVersionData> workflowItemVersionConfig)
        {
            workflowItemVersionConfig.HasKey(t => new { t.ItemId, t.WorkflowId , t.VersionId });

            workflowItemVersionConfig.Property(t => t.WorkflowId)
                .IsRequired();

            workflowItemVersionConfig.Property(t => t.Title)
                .IsRequired();

            workflowItemVersionConfig.Property(t => t.Description)
                .IsRequired();

            base.Configure(workflowItemVersionConfig);
        }
    }
}