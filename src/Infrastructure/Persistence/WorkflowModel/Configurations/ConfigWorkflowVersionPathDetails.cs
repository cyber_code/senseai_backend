﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;
using Persistence.WorkflowModel;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigWorkflowVersionPathDetails : SenseAIEntityTypeConfiguration<WorkflowVersionPathDetailsData>
    {
        public override void Configure(EntityTypeBuilder<WorkflowVersionPathDetailsData> builder)
        {
            builder.ToTable(typeof(WorkflowVersionPathDetailsData).Name);
            builder.HasKey(x => new { x.Id, x.WorkflowId, x.VersionId, x.PathId, x.Index });
            builder.Property(x => x.WorkflowId).IsRequired();
            builder.Property(x => x.VersionId).IsRequired();
            builder.Property(x => x.PathId).IsRequired();
            builder.Property(x => x.Title).IsRequired();
            builder.Property(x => x.Type).IsRequired();
            builder.Property(x => x.ActionType).IsRequired();
            builder.Property(x => x.TypicalId).IsRequired();
            builder.Property(x => x.TypicalName).IsRequired();
            builder.Property(x => x.TypicalType).IsRequired();
            builder.Property(x => x.DataSetId).IsRequired();
            builder.Property(x => x.Index).IsRequired();
            builder.Property(x => x.PathItemJson).IsRequired();

            base.Configure(builder);
        }
    }
}