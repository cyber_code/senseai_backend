﻿using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Text;
using Persistence.Internal;

namespace Persistence.WorkflowModel
{
    public sealed class WorkflowExportedTestCasesRepository : IWorkflowExportedTestCases
    {
        private readonly SenseAIObjectContext _context;

        public WorkflowExportedTestCasesRepository(SenseAIObjectContext senseAIObjectContext)
        {
            _context = senseAIObjectContext;
        }

        public void Add(WorkflowExportedTestCases workflowExportedTestCases)
        {
            WorkflowExportedTestCasesData workflowExportedTestCasesData = new WorkflowExportedTestCasesData
            {
                Id = workflowExportedTestCases.Id,
                WorkflowId = workflowExportedTestCases.WorkflowId,
                VersionId = workflowExportedTestCases.VersionId,
                IsLast = workflowExportedTestCases.IsLast,
                SenseAITestCases = workflowExportedTestCases.SenseAITestCases,
                AtsTestCases = workflowExportedTestCases.AtsTestCases
            };

            _context.Set<WorkflowExportedTestCasesData>().Add(workflowExportedTestCasesData);
            _context.SaveChanges();
        }
    }
}
