﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("WorkflowVersionPathDetails")]
    public class WorkflowVersionPathDetailsData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid WorkflowId { get; set; }

        public int VersionId { get; set; }

        public Guid PathId { get; set; }

        public string Title { get; set; }

        public int Type { get; set; }

        public int ActionType { get; set; }
        
        public Guid CatalogId { get; set; }

        public Guid TypicalId { get; set; }

        public string TypicalName { get; set; }

        public string TypicalType { get; set; }
        
        public Guid DataSetId { get; set; }

        public int Index { get; set; }

        public string PathItemJson { get; set; }
    }
}