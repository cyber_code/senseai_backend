﻿using Microsoft.EntityFrameworkCore;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using SenseAI.Domain;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Persistence.WorkflowModel
{
    public sealed class WorkflowRepository : IWorkflowRepository
    {
        private readonly SenseAIObjectContext _context;

        public WorkflowRepository(SenseAIObjectContext context)
        {
            _context = context;
        }

        public void Add(Workflow workflow)
        {
            var workflowData = new WorkflowData
            {
                Id = workflow.Id,
                Title = workflow.Title,
                Description = workflow.Description,
                FolderId = workflow.FolderId,
                Status = (int)WorkflowStatus.ReadyToIssue,
                IsParsed = workflow.IsParsed
            };

            _context.Add(workflowData);
            _context.SaveChanges();
        }

        public void Save(Workflow workflow, string designerJson, string workflowPathsJson)
        {
            //update workflow
            var workflowData = _context.Set<WorkflowData>()
              .First(item => item.Id == workflow.Id);
            workflowData.DesignerJson = designerJson;
            workflowData.WorkflowPathsJson = workflowPathsJson;
            workflowData.IsParsed = workflow.IsParsed;
            _context.Set<WorkflowData>().Update(workflowData);

            //delete workflow items
            var workflowItems = _context.Set<WorkflowItemData>().Where(i => i.WorkflowId == workflow.Id);
            _context.Set<WorkflowItemData>().RemoveRange(workflowItems);
            _context.SaveChanges();

            var catalogIds = workflow.Items.Select(item => item.CatalogId).Distinct().ToArray();

            var catalogs = _context.Set<CatalogData>().AsNoTracking()
                .Where(catalog => catalogIds.Contains(catalog.Id))
                .ToArray();

            var typicalIds = workflow.Items.Select(item => item.TypicalId).Distinct().ToArray();

            var typicals = _context.Set<TypicalData>().AsNoTracking()
                .Where(catalog => typicalIds.Contains(catalog.Id))
                .ToArray();

            //add workflow items
            foreach (var item in workflow.Items)
            {
                var workflowItemData = new WorkflowItemData
                {
                    WorkflowId = workflow.Id,
                    Id = Guid.NewGuid(),
                    CatalogTitle = catalogs.FirstOrDefault(catalog => catalog.Id == item.CatalogId)?.Title,
                    TypicalTitle = typicals.FirstOrDefault(typical => typical.Id == item.TypicalId)?.Title,
                    Title = item.Title == null ? "" : item.Title,
                    WorkflowLinkId = (item is LinkedWorkflowItem) ? (item as LinkedWorkflowItem).WorkflowId : Guid.Empty,
                    WorkflowLinkVersionId = (item is LinkedWorkflowItem) ? (item as LinkedWorkflowItem).WorkflowVersionId : -1,
                    Description = item.Description == null ? "" : item.Description,
                };

                _context.Add(workflowItemData);
            }

            var workflowVersions = _context.Set<WorkflowVersionData>().Where(i => i.WorkflowId == workflow.Id);
            if (workflowData.IsParsed && workflowVersions != null && workflowVersions.Count() == 1 && !workflowVersions.FirstOrDefault().IsParsed)
            {
                var workflowVersion = workflowVersions.FirstOrDefault();
                workflowVersion.IsParsed = true;
                workflowVersion.DesignerJson = workflowData.DesignerJson;
                workflowVersion.WorkflowPathsJson = workflowData.WorkflowPathsJson;
                _context.Set<WorkflowVersionData>().Update(workflowVersion);
            }

            _context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            //delete all workflow Items
            var items = _context.Set<WorkflowItemData>().Where(i => i.WorkflowId == id);
            _context.Set<WorkflowItemData>().RemoveRange(items);

            //delete all generated test cases Items
            var tcgItems = _context.Set<WorkflowTestCasesData>().Where(i => i.WorkflowId == id);
            _context.Set<WorkflowTestCasesData>().RemoveRange(tcgItems);

            //delete all workflow versions
            var wfVersions = _context.Set<WorkflowVersionData>().Where(i => i.WorkflowId == id);
            _context.Set<WorkflowVersionData>().RemoveRange(wfVersions);

            //delete all generated path Items
            var wfpItems = _context.Set<WorkflowVersionPathsData>().Where(i => i.WorkflowId == id);
            _context.Set<WorkflowVersionPathsData>().RemoveRange(wfpItems);

            //delete all generated path Items details
            var wfpItemDetails = _context.Set<WorkflowVersionPathDetailsData>().Where(i => i.WorkflowId == id);
            _context.Set<WorkflowVersionPathDetailsData>().RemoveRange(wfpItemDetails);

            //delete workflow by id
            _context.Set<WorkflowData>().Remove(new WorkflowData() { Id = id });

            _context.SaveChanges();
        }

        public void Issue(Guid id, string workflowImage)
        {
            var workflowData = _context.Set<WorkflowData>().AsNoTracking()
               .FirstOrDefault(item => item.Id == id);

            var lastWorkflowVersionData = _context.Set<WorkflowVersionData>().FirstOrDefault(
                item => item.WorkflowId == id && item.IsLast
            );

            int versionId = 0;
            if (lastWorkflowVersionData != null)
            {
                lastWorkflowVersionData.IsLast = false;
                if (lastWorkflowVersionData != null)
                    versionId = _context.Set<WorkflowVersionData>()
                                        .Where(t => t.WorkflowId == id)
                                        .Max(t => t.VersionId) + 1;
            }
            else
            {
                lastWorkflowVersionData = new WorkflowVersionData { VersionId = versionId };
            }

            var workflowVersionData = new WorkflowVersionData
            {
                WorkflowId = workflowData.Id,
                VersionId = versionId,
                Title = workflowData.Title,
                Description = workflowData.Description,
                DesignerJson = workflowData.DesignerJson,
                WorkflowImage = workflowImage,
                WorkflowPathsJson = workflowData.WorkflowPathsJson,
                IsLast = true,
                IsParsed = workflowData.IsParsed,
                IsSyncWithML = false
            };

            var workflowItemData = _context.Set<WorkflowItemData>().AsNoTracking().Where(w => w.WorkflowId == id);
            var workflowVersionItemData = workflowItemData.Select(s => new WorkflowItemVersionData
            {
                ItemId = s.Id,
                WorkflowId = s.WorkflowId,
                VersionId = versionId,
                Title = s.Title,
                Description = s.Description,
                CatalogTitle = s.CatalogTitle,
                TypicalTitle = s.TypicalTitle,
                WorkflowLinkId = s.WorkflowLinkId,
                WorkflowLinkVersionId = s.WorkflowLinkVersionId
            });

            _context.Set<WorkflowVersionData>().Add(workflowVersionData);
            _context.Set<WorkflowItemVersionData>().AddRange(workflowVersionItemData);

            _context.SaveChanges();

            //add dynamic data suggestion

            var workflow = GetWorkflowByVersion(workflowVersionData.WorkflowId, versionId);



            var actionItems = workflow.Items.Where(t => t is ActionWorkflowItem);

            foreach (ActionWorkflowItem actionItem in actionItems)
            {
                var dynamicDataSuggestions = _context.Set<DynamicDataSuggestionData>().AsNoTracking().Where(item => item.TargetTypicalName == actionItem.TypicalName).ToList();
                foreach (var dynamicData in actionItem.DynamicDatas)
                {
                    var sourceWorkflowItem = actionItems.FirstOrDefault(t => t.Id == dynamicData.SourceWorkflowItemId);
                    if (sourceWorkflowItem == null)
                        continue;
                    var dynamicDataSuggestion = dynamicDataSuggestions.FirstOrDefault(item =>
                                         item.SourceTypicalName == sourceWorkflowItem.TypicalName
                                         && item.TargetTypicalName == actionItem.TypicalName
                                         && item.SourceTypicalAttributeName == dynamicData.SourceAttributeName
                                         && item.TargetTypicalAttributeName == dynamicData.TargetAttributeName);

                    if (dynamicDataSuggestion != null)
                    {
                        //update dynamicdatasuggestion frequenc
                        dynamicDataSuggestion.Frequency += 1;
                        _context.Set<DynamicDataSuggestionData>().Update(dynamicDataSuggestion);
                    }
                    else
                    {
                        //add new dynamicdatasuggestion
                        dynamicDataSuggestion = new DynamicDataSuggestionData()
                        {
                            SourceTypicalName = sourceWorkflowItem.TypicalName,
                            TargetTypicalName = actionItem.TypicalName,
                            SourceTypicalAttributeName = dynamicData.SourceAttributeName,
                            TargetTypicalAttributeName = dynamicData.TargetAttributeName,
                            Frequency = 1
                        };
                        _context.Set<DynamicDataSuggestionData>().Add(dynamicDataSuggestion);
                    }
                    _context.SaveChanges();
                    _context.Detach(dynamicDataSuggestion);
                }
            }
        }

        public void Rename(Guid workflowId, string title)
        {
            var workflow = _context.Set<WorkflowData>().AsNoTracking().FirstOrDefault(t => t.Id == workflowId);
            if (workflow != null)
            {
                workflow.Title = title;
                _context.Set<WorkflowData>().Update(workflow);
            }

            _context.SaveChanges();
        }

        public void Update(Guid workflowId, string json, bool markAsInvalid)
        {
            var workflow = _context.Set<WorkflowData>().FirstOrDefault(t => t.Id == workflowId);
            if (workflow != null)
            {
                workflow.DesignerJson = json;

                var workflowEntry = _context.Entry(workflow);
                if (markAsInvalid)
                {
                    workflow.IsValid = false;
                    workflowEntry.Property(x => x.IsValid).IsModified = true;
                }
                workflowEntry.Property(x => x.DesignerJson).IsModified = true;
                _context.SaveChanges();
            }
        }

        public Workflow Paste(Guid workflowId, Guid folderId)
        {
            //get workflow by id
            var workflow = _context.Set<WorkflowData>().Include(i => i.WorkflowItems).FirstOrDefault(t => t.Id == workflowId);

            if (workflow == null)
                throw new Exception("This workflow does not exist");

            workflow.Id = Guid.NewGuid();
            workflow.FolderId = folderId;
            workflow.Title += " - Copy";
            //add copied workflow
            _context.Set<WorkflowData>().Add(workflow);

            //add workflow items
            foreach (var item in workflow.WorkflowItems)
            {
                item.Id = Guid.NewGuid();
                item.WorkflowId = workflow.Id;

                _context.Set<WorkflowItemData>().Add(item);
            }

            _context.SaveChanges();

            return new Workflow(workflow.Id, workflow.Title, workflow.Description, workflow.FolderId, workflow.IsValid, workflow.IsParsed);
        }

        public void Cut(Guid workflowId, Guid folderId)
        {
            var workflow = _context.Set<WorkflowData>().FirstOrDefault(t => t.Id == workflowId);

            if (workflow == null)
                throw new Exception("This workflow does not exist");

            workflow.FolderId = folderId;

            _context.SaveChanges();
        }

        public Workflow GetWorkflowByVersion(Guid workflowId, int versionId)
        {
            var workflowVersionData = _context.Set<WorkflowVersionData>()
                .FirstOrDefault(w => w.WorkflowId == workflowId && w.VersionId == versionId);

            Workflow workflow = new Workflow(workflowVersionData.Title, workflowVersionData.Description, new Guid());

            if (workflowVersionData.DesignerJson != null && workflowVersionData.IsParsed)
            {
                return WorkflowFactory.Create(workflowVersionData.WorkflowId, workflowVersionData.DesignerJson);
            }
            else
            {
                workflow = WorkflowFactory.TECreate(workflowVersionData.WorkflowId, workflowVersionData.DesignerJson);
                workflow.SetWorkflowPathsJson(workflowVersionData.WorkflowPathsJson);
            }
            return workflow;
        }

        public string GetWorkflowPathsByVersion(Guid workflowId, int versionId)
        {
            var workflowVersionData = _context.Set<WorkflowVersionData>()
                .First(item => item.WorkflowId == workflowId && item.VersionId == versionId);

            return workflowVersionData.WorkflowPathsJson;
        }

        /// <summary>
        /// Update WorkflowVersion, set IsSyncWithML true
        /// </summary>
        /// <param name="id">workflowId</param>
        /// <param name="VersionId">VersionId</param>
        public void Update(Guid id, int VersionId)
        {
            var workflowVersionData = _context.Set<WorkflowVersionData>()
                .First(item => item.WorkflowId == id && item.VersionId == VersionId);
            workflowVersionData.IsSyncWithML = true;
            _context.Set<WorkflowVersionData>().Update(workflowVersionData);
            _context.SaveChanges();
        }

        public void Update(Guid id)
        {
            var workflowData = (_context.Set<WorkflowData>().AsNoTracking()
                .FirstOrDefault(item => item.Id == id));

            var workflowVersionData = (_context.Set<WorkflowVersionData>()
                .FirstOrDefault(item => item.WorkflowId == id && item.IsLast == true));
            workflowVersionData.DesignerJson = workflowData.DesignerJson;
            workflowVersionData.WorkflowPathsJson = workflowData.WorkflowPathsJson;
            _context.Set<WorkflowVersionData>().Update(workflowVersionData);
            _context.SaveChanges();
        }

        public void UpdateStatus(Guid id, WorkflowStatus status)
        {
            var workflowData = (_context.Set<WorkflowData>().FirstOrDefault(item => item.Id == id));
            workflowData.Status = (int)status;
            _context.Set<WorkflowData>().Update(workflowData);
            _context.SaveChanges();
        }

        public void SetIsIssued(Guid id)
        {
            var workflowData = (_context.Set<WorkflowData>().FirstOrDefault(item => item.Id == id));
            workflowData.isIssued = true;
            _context.Set<WorkflowData>().Update(workflowData);
            _context.SaveChanges();
        }

        public void SetIsTestCasegenerated(Guid id)
        {
            var workflowData = (_context.Set<WorkflowData>().FirstOrDefault(item => item.Id == id));
            workflowData.generatedTestCases = true;
            _context.Set<WorkflowData>().Update(workflowData);
            _context.SaveChanges();
        }

        public Workflow GetLastVersion(Guid workflowId)
        {
            var workflowVersionData = _context.Set<WorkflowVersionData>()
                .First(item => item.WorkflowId == workflowId && item.IsLast);

            Workflow workflow = null;

            if (workflowVersionData.DesignerJson != null && workflowVersionData.IsParsed)
            {
                workflow = WorkflowFactory.Create(
                       workflowVersionData.WorkflowId,
                       workflowVersionData.Title,
                       workflowVersionData.Description,
                       workflowVersionData.DesignerJson
                   );
                workflow.SetWorkflowPathsJson(workflowVersionData.WorkflowPathsJson);
            }
            else
            {
                workflow = WorkflowFactory.TECreate(workflowVersionData.WorkflowId, workflowVersionData.DesignerJson);
                workflow.SetWorkflowPathsJson(workflowVersionData.WorkflowPathsJson);
            }
            return workflow;
        }

        public string RollbackWorkflow(Guid workflowId, int versionId)
        {
            //update workflow version
            var workflowVersions = _context.Set<WorkflowVersionData>().Where(item => item.WorkflowId == workflowId && item.IsLast && item.VersionId != versionId);
            workflowVersions.ToList().ForEach(f => { f.IsLast = false; });
            _context.Set<WorkflowVersionData>().UpdateRange(workflowVersions);

            var workflowVersionData = _context.Set<WorkflowVersionData>().FirstOrDefault(item => item.WorkflowId == workflowId && item.VersionId == versionId);
            workflowVersionData.IsLast = true;
            _context.Set<WorkflowVersionData>().Update(workflowVersionData);

            //delete workflow items
            var workflowItems = _context.Set<WorkflowItemData>().Where(i => i.WorkflowId == workflowId);
            _context.Set<WorkflowItemData>().RemoveRange(workflowItems);

            var workflowItemVersions = _context.Set<WorkflowItemVersionData>().AsNoTracking().Where(item => item.WorkflowId == workflowId && item.VersionId == versionId);

            var workflowItemsFromVersion = workflowItemVersions.Select(s => new WorkflowItemData
            {
                Id = s.ItemId,
                WorkflowId = s.WorkflowId,
                Title = s.Title,
                Description = s.Description,
                CatalogTitle = s.CatalogTitle,
                TypicalTitle = s.TypicalTitle,
                WorkflowLinkId = s.WorkflowLinkId,
                WorkflowLinkVersionId = s.WorkflowLinkVersionId
            });

            _context.Set<WorkflowItemData>().AddRange(workflowItemsFromVersion);

            //Update the workflow path
            var workflowPaths = _context.Set<WorkflowVersionPathsData>().Where(item => item.WorkflowId == workflowId && item.IsLast && item.VersionId != versionId);
            workflowPaths.ToList().ForEach(f => { f.IsLast = false; });
            _context.Set<WorkflowVersionPathsData>().UpdateRange(workflowPaths);

            workflowPaths = _context.Set<WorkflowVersionPathsData>().Where(item => item.WorkflowId == workflowId && item.VersionId == versionId);
            workflowPaths.ToList().ForEach(f => { f.IsLast = true; });
            _context.Set<WorkflowVersionPathsData>().UpdateRange(workflowPaths);

            //update workflow
            var workflowData = _context.Set<WorkflowData>().FirstOrDefault(w => w.Id == workflowId);
            workflowData.DesignerJson = workflowVersionData.DesignerJson;
            workflowData.WorkflowPathsJson = workflowVersionData.WorkflowPathsJson;
            _context.Set<WorkflowData>().Update(workflowData);

            _context.SaveChanges();
            return workflowVersionData.DesignerJson;
        }

        public int GetNrVersions(Guid id)
        {
            return _context.Set<WorkflowVersionData>().AsNoTracking().Where(w => w.WorkflowId == id).Count();
        }

        public string GetWorkflowImage(Guid workflowId, int versionId)
        {
            var workflowVersion = _context.Set<WorkflowVersionData>().AsNoTracking()
             .FirstOrDefault(item => item.WorkflowId == workflowId && item.VersionId == versionId);

            return workflowVersion.WorkflowImage;
        }

        public bool IsWorkflowLinked(Guid workflowId)
        {
            var workflows = _context.Set<WorkflowItemData>().AsNoTracking()
             .Count(item => item.WorkflowLinkId == workflowId);

            return workflows > 0;
        }

        public int GetWorkflowStatus(Guid workflowId)
        {
            var workflow = _context.Set<WorkflowData>().AsNoTracking()
             .FirstOrDefault(item => item.Id == workflowId);

            return workflow.Status;
        }

        public Workflow GetWorkflowDetails(Guid workflowId)
        {
            var query = from workflow in _context.Set<WorkflowData>()
                        where workflow.Id == workflowId
                        select new Workflow(workflow.Title, workflow.Description, workflow.FolderId, workflow.IsValid, workflow.IsParsed);
            return query.FirstOrDefault();
        }
 
        public void UpgradeLinkedWorkflowVersion(Guid workflowId, int currentVersionId, int newVersionId)
        {
            /*update workflow items*/
            var workflowItems = _context.Set<WorkflowItemData>().Where(w => w.WorkflowLinkId == workflowId && w.WorkflowLinkVersionId == currentVersionId);
            workflowItems.ToList().ForEach(f => { f.WorkflowLinkVersionId = newVersionId; });

            /*update workflow items in versioning */
            var workflowVersionItems = _context.Set<WorkflowItemVersionData>().Where(w => w.WorkflowLinkId == workflowId && w.WorkflowLinkVersionId == currentVersionId);
            workflowVersionItems.ToList().ForEach(f => { f.WorkflowLinkVersionId = newVersionId; });

            /*Change the version id inside workflows*/
            var workflows = _context.Set<WorkflowData>().Where(w => workflowItems.Select(s => s.WorkflowId).Contains(w.Id)).ToList();
            workflows.ForEach(f =>
            {
                f.DesignerJson = f.DesignerJson.Replace("\"versionId\": " + currentVersionId.ToString(), "\"versionId\": " + newVersionId.ToString());
            });

            /*Change the version id inside last version of workflows*/
            var workflowVersions = _context.Set<WorkflowVersionData>().Where(w => workflowItems.Select(s => s.WorkflowId).Contains(w.WorkflowId) && w.IsLast).ToList();
            workflowVersions.ForEach(f =>
            {
                f.DesignerJson = f.DesignerJson.Replace("\"versionId\": " + currentVersionId.ToString(), "\"versionId\": " + newVersionId.ToString());
            });

            _context.SaveChanges();
        }
    }
}