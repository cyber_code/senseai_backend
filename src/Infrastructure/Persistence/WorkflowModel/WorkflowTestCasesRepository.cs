﻿using Core.Extensions;
using SenseAI.Domain.WorkflowModel;
using Persistence.Internal;
using System.Linq;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;

namespace Persistence.WorkflowModel
{
    public sealed class WorkflowTestCasesRepository : IWorkflowTestCasesRepository
    {
        private readonly IBaseRepository<WorkflowTestCases, WorkflowTestCasesData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public WorkflowTestCasesRepository(IBaseRepository<WorkflowTestCases, WorkflowTestCasesData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(WorkflowTestCases[] workflowTestCases)
        {
            var workflowTestCase = workflowTestCases.FirstOrDefault();
            var existingWorkflowTestCaseData = _context.Set<WorkflowTestCasesData>().Where(w => w.WorkflowId == workflowTestCase.WorkflowId &&
            w.WorkflowPathId == workflowTestCase.WorkflowPathId && w.VersionId == workflowTestCase.VersionId).ToList();

            var tc2Add = new List<WorkflowTestCasesData>();
            var tc2Update = new List<WorkflowTestCasesData>();
            var tcItems2Remove = new List<WorkflowTestCasesData>();

            Dictionary<Guid, Guid> mapTestCaseIds = new Dictionary<Guid, Guid>();
            workflowTestCases.ToList().ForEach(f =>
            {
                var testCasesData = existingWorkflowTestCaseData.FirstOrDefault(w => w.TestStepId == f.TestStepId && w.TCIndex == f.TCIndex);
                var item = f.ProjectedAs<WorkflowTestCasesData>();
                if (testCasesData != null)
                {
                    if (!mapTestCaseIds.ContainsKey(item.TestCaseId))
                        mapTestCaseIds.Add(item.TestCaseId, testCasesData.TestCaseId);

                    testCasesData.TestCaseTitle = item.TestCaseTitle;
                    testCasesData.TestStepTitle = item.TestStepTitle;
                    testCasesData.TypicalId = item.TypicalId;
                    testCasesData.TypicalName = item.TypicalName;
                    testCasesData.TestStepJson = item.TestStepJson;
                    testCasesData.TCIndex = item.TCIndex;
                    testCasesData.TSIndex = item.TSIndex;
                    tc2Update.Add(testCasesData);
                }
                else
                {
                    tc2Add.Add(item);
                }
            });

            tc2Add.ForEach(f =>
            {
                if (mapTestCaseIds.ContainsKey(f.TestCaseId))
                    f.TestCaseId = mapTestCaseIds.FirstOrDefault(w => w.Key == f.TestCaseId).Value;
            });

            existingWorkflowTestCaseData.ForEach(f =>
            {
                if (tc2Add.FirstOrDefault(w => w.Id == f.Id) == null && tc2Update.FirstOrDefault(w => w.Id == f.Id) == null)
                    tcItems2Remove.Add(f);
            });
            _context.Set<WorkflowTestCasesData>().RemoveRange(tcItems2Remove);
            _context.Set<WorkflowTestCasesData>().AddRange(tc2Add);
            _context.Set<WorkflowTestCasesData>().UpdateRange(tc2Update);
            _context.SaveChanges();
        }

        public void Update(WorkflowTestCases[] workflowTestCases)
        {
            var data = ProjectionsExtensionMethods.ProjectedAsCollection<WorkflowTestCasesData>(workflowTestCases);
            _context.UpdateRange(data);
            _context.SaveChanges();
        }
    }
}