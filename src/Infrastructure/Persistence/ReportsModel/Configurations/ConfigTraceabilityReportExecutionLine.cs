﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.ReportsModel.Configurations
{
    public class ConfigTraceabilityReportExecutionLine : SenseAIQueryTypeConfiguration<TraceabilityReportExecutionLine>
    {
        public override void Configure(QueryTypeBuilder<TraceabilityReportExecutionLine> builder)
        {
            base.Configure(builder);
        }
    }
}
