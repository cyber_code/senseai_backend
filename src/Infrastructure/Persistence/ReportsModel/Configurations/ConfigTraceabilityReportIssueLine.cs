﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.ReportsModel.Configurations
{
    public class ConfigTraceabilityReportIssuesLine : SenseAIQueryTypeConfiguration<TraceabilityReportIssuesLine>
    {
        public override void Configure(QueryTypeBuilder<TraceabilityReportIssuesLine> builder)
        {
            base.Configure(builder);
        }
    }
}
