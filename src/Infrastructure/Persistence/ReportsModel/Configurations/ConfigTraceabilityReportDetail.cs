﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.ReportsModel.Configurations
{
    public class ConfigTraceabilityReportDetail : SenseAIQueryTypeConfiguration<TraceabilityReportLineDetail>
    {
        public override void Configure(QueryTypeBuilder<TraceabilityReportLineDetail> builder)
        {
            base.Configure(builder);
        }
    }
}
