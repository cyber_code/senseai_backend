﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.ReportsModel.Configurations
{
    public class ConfigTraceabilityReportLine : SenseAIQueryTypeConfiguration<TraceabilityReportLine>
    {
        public override void Configure(QueryTypeBuilder<TraceabilityReportLine> builder)
        {
            base.Configure(builder);
        }
    }
}
