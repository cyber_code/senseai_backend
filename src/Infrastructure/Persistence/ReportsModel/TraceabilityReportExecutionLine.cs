﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.ReportsModel
{
    public class TraceabilityReportExecutionLine
    {
        public Guid Id { get; set; }

        public string Day { get; set; }

        public int FailedExecutions { get; set; }

        public int SuccessfulExecutions { get; set; }
    }
}
