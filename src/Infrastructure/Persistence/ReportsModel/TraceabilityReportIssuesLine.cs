﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.ReportsModel
{
    public class TraceabilityReportIssuesLine
    {
        public Guid Id { get; set; }

        public string Date { get; set; }

        public int Status { get; set; }

        public int Count { get; set; }
    }
}
