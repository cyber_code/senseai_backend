﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.ReportsModel
{
    public class TraceabilityReportLine
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public int Processes { get; set; }

        public int Workflows { get; set; }

        public int TestCases { get; set; }

        public int PlannedTestCases { get; set; }

        public int OpenIssues { get; set; }

        public int FailedExecutions { get; set; }

        public int SuccessfulExecutions { get; set; }
    }
}
