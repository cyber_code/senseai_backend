﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.SystemCatalogModel
{
    [Table("SystemCatalogs")]
    public class SystemCatalogData : BaseData, IData
    {
        //Id,SubprojectId, SystemId, CatalogId.
        public Guid Id { get; set; }
        public Guid SubprojectId { get; set; }
        public Guid SystemId { get; set; }
        public Guid CatalogId { get; set; } 

    }
}
