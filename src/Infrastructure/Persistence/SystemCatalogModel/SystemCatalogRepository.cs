﻿using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.SystemCatalogs; 
using SenseAI.Domain.SystemModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.SystemCatalogModel
{
    class SystemCatalogRepository : ISystemCatalogRepository
    {
        private readonly IBaseRepository<SystemCatalog, SystemCatalogData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public SystemCatalogRepository(IBaseRepository<SystemCatalog, SystemCatalogData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;

            _context = context;
        }

        public void Add(SystemCatalog SystemCatalog)
        {
            _baseRepository.Add(SystemCatalog);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(SystemCatalog SystemCatalog)
        {
            _baseRepository.Update(SystemCatalog);
        }

        public List<SystemCatalog> GetSystemCatalogs()
        {
            return _context.Set<SystemCatalogData>().AsNoTracking().Select(ct => new SystemCatalog(ct.Id, ct.SubprojectId, ct.SystemId, ct.CatalogId)).ToList();
        }
    }
}