﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.SystemCatalogModel.Configurations
{
    class ConfigSystemCatalog : SenseAIEntityTypeConfiguration<SystemCatalogData>
    {
        public override void Configure(EntityTypeBuilder<SystemCatalogData> builder)
        {
            builder.ToTable(typeof(SystemCatalogData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.SubprojectId).IsRequired();
            builder.Property(x => x.SystemId).IsRequired();
            builder.Property(x => x.CatalogId).IsRequired();


            base.Configure(builder);
        }
    }
}