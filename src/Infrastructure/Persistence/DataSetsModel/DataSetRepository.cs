﻿using Microsoft.EntityFrameworkCore;
using SenseAI.Domain.DataSetsModel;
using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.DataSetsModel
{
    public class DataSetRepository : IDataSetRepository
    {
        private readonly SenseAIObjectContext _context;

        public DataSetRepository(SenseAIObjectContext context)
        {
            _context = context;
        }

        public void Add(DataSet dataSet)
        {
            DataSetData dataSetData = new DataSetData
            {
                Id = dataSet.Id,
                SubProjectId = dataSet.SubProjectId,
                CatalogId = dataSet.CatalogId,
                SystemId = dataSet.SystemId,
                TypicalId = dataSet.TypicalId,
                Title = dataSet.Title,
                Coverage = dataSet.Coverage,
                Combinations = dataSet.Combinations,
                ParentId = dataSet.ParentId
            };

            _context.Add(dataSetData);

            Guid dataSetId = dataSet.Id;
            List<DataSetItemData> dataSetItemData = dataSet.DataSetItems.Select(ct => new DataSetItemData { Id = ct.Id, DataSetId = dataSetId, Row = ct.Row }).ToList();

            _context.AddRange(dataSetItemData);
            _context.SaveChanges();
        }

        public void Update(DataSet dataSet)
        {
            DataSetData dataSetData = _context.Set<DataSetData>().AsNoTracking()
              .FirstOrDefault(item => item.Id == dataSet.Id);

            dataSetData.Title = dataSet.Title;
            dataSetData.Coverage = dataSet.Coverage;
            dataSetData.Combinations = dataSet.Combinations;

            _context.Update(dataSetData);

            Guid dataSetId = dataSet.Id;

            List<DataSetItemData> dataSetItemDataOld = _context.Set<DataSetItemData>().AsNoTracking()
              .Where(item => item.DataSetId == dataSet.Id).ToList();
            _context.Set<DataSetItemData>().RemoveRange(dataSetItemDataOld);

            List<DataSetItemData> dataSetItemData = dataSet.DataSetItems.Select(ct => new DataSetItemData { DataSetId = dataSetId, Row = ct.Row }).ToList();

            _context.AddRange(dataSetItemData);
            _context.SaveChanges();
        }
        public void UpdateCombinations(Guid id, string combinations)
        {
            DataSetData dataSetData = _context.Set<DataSetData>().AsNoTracking()
              .FirstOrDefault(item => item.Id == id);

            dataSetData.Combinations = combinations;

            _context.Update(dataSetData);

            _context.SaveChanges();
        }
        public void Delete(Guid id)
        {
            List<DataSetItemData> dataSetItemData = _context.Set<DataSetItemData>().AsNoTracking()
              .Where(item => item.DataSetId == id).ToList();

            _context.Set<DataSetItemData>().RemoveRange(dataSetItemData);

            _context.Set<DataSetData>().Remove(new DataSetData { Id = id });
            _context.SaveChanges();
        }

        public void UpdateItem(DataSetItem dataSetItem)
        {
            DataSetItemData dataSetItemData = _context.Set<DataSetItemData>().AsNoTracking()
              .FirstOrDefault(item => item.Id == dataSetItem.Id);

            dataSetItemData.Row = dataSetItem.Row;

            _context.Update(dataSetItemData);

            _context.SaveChanges();
        }

        public void DeleteItem(Guid id)
        {
            var item = _context.Set<DataSetItemData>().FirstOrDefault(w => w.Id == id);
            _context.Set<DataSetItemData>().Remove(item);
            _context.SaveChanges();
        }

        public DataSet Get(Guid dataSetId)
        {
            DataSetData dataSetData = _context.Set<DataSetData>().AsNoTracking().FirstOrDefault(w => w.Id == dataSetId);

            return new DataSet(dataSetData.Id, dataSetData.Title, dataSetData.Combinations, dataSetData.Coverage, null);
        }

        public DataSet GetDataSet(Guid dataSetId)
        {
            DataSetData ds = _context.Set<DataSetData>().AsNoTracking().FirstOrDefault(w => w.Id == dataSetId);

            var dataSetItems = from w in _context.Set<DataSetItemData>().AsNoTracking()
                               where w.DataSetId == ds.Id
                               select new DataSetItem(w.Id, w.DataSetId, w.Row);

            return new DataSet(ds.Id, ds.SubProjectId, ds.CatalogId, ds.SystemId, ds.TypicalId, ds.Title, ds.Coverage, ds.Combinations, dataSetItems.ToArray());
        }

        public int GetNumberOfSameDatasets(Guid parentId)
        {
            return _context.Set<DataSetData>().AsNoTracking().Where(u => u.ParentId == parentId).Count();
        }
    }
}