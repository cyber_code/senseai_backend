﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.DataSetsModel
{
    [Table("DataSetsItems")]
    public class DataSetItemData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid DataSetId { get; set; }

        public string Row { get; set; }
    }
}