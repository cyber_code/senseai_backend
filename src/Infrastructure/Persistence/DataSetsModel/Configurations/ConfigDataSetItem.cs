﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.DataSetsModel.Configurations
{
    internal class ConfigDataSetItem : SenseAIEntityTypeConfiguration<DataSetItemData>
    {
        public override void Configure(EntityTypeBuilder<DataSetItemData> builder)
        {
            builder.ToTable(typeof(DataSetItemData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.DataSetId).IsRequired();
            builder.Property(x => x.Row);

            base.Configure(builder);
        }
    }
}