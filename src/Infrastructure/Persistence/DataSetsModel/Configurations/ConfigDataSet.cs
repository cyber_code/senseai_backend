﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.DataSetsModel.Configurations
{
    internal class ConfigDataSet : SenseAIEntityTypeConfiguration<DataSetData>
    {
        public override void Configure(EntityTypeBuilder<DataSetData> builder)
        {
            builder.ToTable(typeof(DataSetData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.CatalogId).IsRequired();
            builder.Property(x => x.TypicalId).IsRequired();
            builder.Property(x => x.TypicalId).IsRequired();
            builder.Property(x => x.Title).IsRequired();
            builder.Property(x => x.Coverage).HasColumnType("decimal(18,4)");
            builder.Property(x => x.Combinations);
            builder.Property(x => x.TypicalId);

            base.Configure(builder);
        }
    }
}