﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.DataSetsModel
{
    [Table("DataSets")]
    public class DataSetData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid SystemId { get; set; }

        public Guid TypicalId { get; set; }

        public string Title { get; set; }

        [Column(TypeName = "decimal(18,4)")] 
        public decimal Coverage { get; set; }

        public string Combinations { get; set; }

        public Guid ParentId { get; set; }
    }
}