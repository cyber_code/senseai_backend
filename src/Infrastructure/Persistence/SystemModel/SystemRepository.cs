﻿using Microsoft.EntityFrameworkCore;
using SenseAI.Domain.SystemModel;
using Persistence.Internal;
using System;
using System.Linq;
using System.Collections.Generic;
using system = SenseAI.Domain.SystemModel.System;
using Persistence.SystemCatalogModel;

namespace Persistence.SystemModel
{
    public sealed class SystemRepository : ISystemRepository
    {
        private readonly IBaseRepository<system, SystemData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public SystemRepository(IBaseRepository<system, SystemData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;

            _context = context;
        }

        public void Add(system System)
        {
            _baseRepository.Add(System);
        }

        public void Delete(Guid id)
        {
            var items = _context.Set<SystemCatalogData>().Where(i => i.SystemId == id);
            _context.Set<SystemCatalogData>().RemoveRange(items);
            _baseRepository.Delete(id);
        }

        public void Update(system System)
        {
            _baseRepository.Update(System);
        }

        public List<system> GetSystems()
        {
            return _context.Set<SystemData>().AsNoTracking().Select(ct => new system(ct.Id, ct.Title, ct.AdapterName)).ToList();
        }
    }
}