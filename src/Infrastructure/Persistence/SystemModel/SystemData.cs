﻿using Persistence.Internal;
using Persistence.SystemTagModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.SystemModel
{
    [Table("Systems")]
    public class SystemData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string AdapterName { get; set; }
    }
}