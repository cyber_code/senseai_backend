﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.SystemModel.Configurations
{
    internal class ConfigSystem : SenseAIEntityTypeConfiguration<SystemData>
    {
        public override void Configure(EntityTypeBuilder<SystemData> builder)
        {
            builder.ToTable(typeof(SystemData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.AdapterName).HasMaxLength(300).IsRequired();

            base.Configure(builder);
        }
    }
}