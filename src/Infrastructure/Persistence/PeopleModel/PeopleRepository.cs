﻿using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.PeopleModel;
using System;
using System.Linq;

namespace Persistence.PeopleModel
{
    public sealed class PeopleRepository : IPeopleRepository
    {
        private readonly IBaseRepository<People, PeopleData> _baseRepository;


        public PeopleRepository(IBaseRepository<People, PeopleData> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public void Add(People people)
        {
            _baseRepository.Add(people);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(People people)
        {
            _baseRepository.Update(people);
        }
      
    }
}
