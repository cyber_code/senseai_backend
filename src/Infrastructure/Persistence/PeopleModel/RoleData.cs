﻿using Persistence.AdminModel;
using Persistence.Internal;
using SenseAI.Domain.AdminModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.PeopleModel
{
    [Table("Roles")]
    public class RoleData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public Guid SubProjectId { get; set; }
       
        public virtual IReadOnlyCollection<PeopleData> Peoples { get; set; }
    }
}
