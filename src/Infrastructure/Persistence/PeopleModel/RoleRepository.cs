﻿using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.PeopleModel;
using System;
using System.Linq;

namespace Persistence.PeopleModel
{
    public sealed class RoleRepository : IRoleRepository
    {
        private readonly IBaseRepository<Role, RoleData> _baseRepository;


        public RoleRepository(IBaseRepository<Role, RoleData> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public void Add(Role role)
        {
            _baseRepository.Add(role);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(Role role)
        {
            _baseRepository.Update(role);
        }
      
    }
}
