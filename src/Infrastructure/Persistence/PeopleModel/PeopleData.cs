﻿using Persistence.AdminModel;
using Persistence.Internal;
using SenseAI.Domain.AdminModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.PeopleModel
{
    [Table("Peoples")]
    public class PeopleData : BaseData, IData
    {
        public Guid Id { get; set; }
        public Guid RoleId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int UserIdFromAdminPanel { get; set; }
        public Guid SubProjectId { get; set; }
        public string Color { get; set; }


        public virtual RoleData Role { get; set; }
    }
}
