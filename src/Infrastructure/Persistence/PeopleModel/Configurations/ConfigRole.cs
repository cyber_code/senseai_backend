﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.PeopleModel.Configurations
{
    internal class ConfigRole : SenseAIEntityTypeConfiguration<RoleData>
    {
        public override void Configure(EntityTypeBuilder<RoleData> builder)
        {
            builder.ToTable(typeof(RoleData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(500);
           
            base.Configure(builder);
        }
    }
}
