﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.PeopleModel.Configurations
{
    internal class ConfigPeople : SenseAIEntityTypeConfiguration<PeopleData>
    {
        public override void Configure(EntityTypeBuilder<PeopleData> builder)
        {
            builder.ToTable(typeof(PeopleData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.RoleId).IsRequired();
            builder.HasOne(spd => spd.Role)
               .WithMany(pd => pd.Peoples)
               .HasForeignKey(spd => spd.RoleId);
            base.Configure(builder);
        }
    }
}
