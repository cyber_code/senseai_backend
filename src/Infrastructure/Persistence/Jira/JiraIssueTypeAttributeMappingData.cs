﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.Jira
{
    [Table("JiraIssueTypeAttributeMapping")]
    public class JiraIssueTypeAttributeMappingData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid JiraIssueTypeMappingId { get; set; }

        public string SenseaiField { get; set; }

        public string JiraField { get; set; }

        public bool Required { get; set; }

        public int Index { get; set; }
    }
}