﻿using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.Jira
{
    public class JiraProjectMappingRepository : IJiraProjectMappingRepository
    {
        private readonly IBaseRepository<JiraProjectMapping, JiraProjectMappingData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public JiraProjectMappingRepository(IBaseRepository<JiraProjectMapping, JiraProjectMappingData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(JiraProjectMapping jiraProjectMapping)
        {
            _baseRepository.Add(jiraProjectMapping);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(JiraProjectMapping jiraProjectMapping)
        {
            _baseRepository.Update(jiraProjectMapping);
        }

        public JiraProjectMapping GetJiraProjectMappingBySubprojectId(Guid SubprojectId)
        {
            var query = (from jira in _context.Set<JiraProjectMappingData>().AsNoTracking()
                         where jira.SubprojectId == SubprojectId
                         select new JiraProjectMapping(
                             jira.Id,
                         jira.ProjectId,
                         jira.SubprojectId,
                         jira.JiraProjectKey,
                         jira.JiraProjectName,
                         jira.MapHierarchy,
                         jira.HierarchyParentId,
                         jira.HierarchyTypeId)).FirstOrDefault();
            return query;
        }

        public JiraProjectMapping GetJiraProjectMappingById(Guid Id)
        {
            var query = (from jira in _context.Set<JiraProjectMappingData>().AsNoTracking()
                         where jira.Id == Id
                         select new JiraProjectMapping(
                            jira.Id,
                            jira.ProjectId,
                            jira.SubprojectId,
                            jira.JiraProjectKey,
                            jira.JiraProjectName,
                            jira.MapHierarchy,
                            jira.HierarchyParentId,
                            jira.HierarchyTypeId
                            )
                         ).FirstOrDefault();
            return query;
        }

        public List<JiraProjectMapping> GetJiraProjectMappings()
        {
            var query = (from jira in _context.Set<JiraProjectMappingData>().AsNoTracking()
                         select new JiraProjectMapping(
                             jira.Id,
                             jira.ProjectId,
                             jira.SubprojectId,
                             jira.JiraProjectKey,
                             jira.JiraProjectName,
                             jira.MapHierarchy,
                             jira.HierarchyParentId,
                             jira.HierarchyTypeId
                        )
                        ).ToList();
            return query;
        }
    }
}