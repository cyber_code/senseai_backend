﻿using System;
using System.Collections.Generic;
using System.Text;
using Persistence.Internal;
using SenseAI.Domain.JiraModel;

namespace Persistence.Jira
{
    public class JiraIssueTypeMappingRepository : IJiraIssueTypeMappingRepository
    {
        private readonly IBaseRepository<JiraIssueTypeMapping, JiraIssueTypeMappingData> _baseRepository;

        public JiraIssueTypeMappingRepository(IBaseRepository<JiraIssueTypeMapping, JiraIssueTypeMappingData> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public void Add(JiraIssueTypeMapping jiraIssueTypeMapping)
        {
            _baseRepository.Add(jiraIssueTypeMapping);
        }

        public void Update(JiraIssueTypeMapping jiraIssueTypeMapping)
        {
            _baseRepository.Update(jiraIssueTypeMapping);
        } 

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }
    }
}
