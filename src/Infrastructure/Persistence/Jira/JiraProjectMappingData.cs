﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.Jira
{
    [Table("JiraProjectMapping")]
    public class JiraProjectMappingData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraProjectKey { get; set; }

        public string JiraProjectName { get; set; }

        public bool MapHierarchy { get; set; }

        public Guid HierarchyParentId { get; set; }

        public Guid HierarchyTypeId { get; set; }
    }
}