﻿using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.Jira
{
    public class JiraUserMappingRepository : IJiraUserMappingRepository
    {
        private readonly IBaseRepository<JiraUserMapping, JiraUserMappingData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public JiraUserMappingRepository(IBaseRepository<JiraUserMapping, JiraUserMappingData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(JiraUserMapping jiraProjectMapping)
        {
            _baseRepository.Add(jiraProjectMapping);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(JiraUserMapping jiraProjectMapping)
        {
            _baseRepository.Update(jiraProjectMapping);
        }

        public JiraUserMapping GetJiraUserMappingById(Guid SubprojectId)
        {
            var query = (from jira in _context.Set<JiraUserMappingData>().AsNoTracking()
                         where jira.SubprojectId == SubprojectId
                         select new JiraUserMapping(jira.Id, jira.SubprojectId,
                         jira.JiraUserId, jira.SenseaiUserId)).FirstOrDefault();
            return query;
        }

        //public List<JiraUserMapping> GetJiraUserMappingBySubprojectId(Guid subProjectId)
        //{
        //    var query = (from jira in _context.Set<JiraUserMappingData>().AsNoTracking()
        //                 where jira.SubprojectId == subProjectId
        //                 select new JiraUserMapping(jira.Id, jira.SubprojectId, jira.JiraUserId, jira.SenseaiUserId)).ToList();
        //    return query;
        //}
    }
}