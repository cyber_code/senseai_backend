﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.Jira
{
    [Table("JiraIssueTypeMapping")]
    public class JiraIssueTypeMappingData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid JiraProjectMappingId { get; set; }

        public string SenseaiIssueTypeField { get; set; }

        public string JiraIssueTypeFieldId { get; set; }

        public string JiraIssueTypeFieldName { get; set; }

        public bool AllowAttachments { get; set; }
    }
}