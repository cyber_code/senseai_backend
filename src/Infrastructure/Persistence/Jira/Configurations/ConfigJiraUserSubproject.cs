﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Jira.Configurations
{
    public class ConfigJiraUserSubprojectConfigs : SenseAIEntityTypeConfiguration<JiraUserSubprojectConfigsData>
    {
        public override void Configure(EntityTypeBuilder<JiraUserSubprojectConfigsData> builder)
        {
            builder.ToTable(typeof(JiraUserSubprojectConfigsData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.UserId).IsRequired();
            builder.Property(x => x.JiraToken).IsRequired();
            builder.Property(x => x.JiraUsername).IsRequired();
            builder.Property(x => x.SubprojectId).IsRequired();
            base.Configure(builder);
        }
    }
}