﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Jira.Configurations
{
    internal class ConfigJiraIssueTypeAttributeMapping : SenseAIEntityTypeConfiguration<JiraIssueTypeAttributeMappingData>
    {
        public override void Configure(EntityTypeBuilder<JiraIssueTypeAttributeMappingData> builder)
        {
            builder.ToTable(typeof(JiraIssueTypeAttributeMappingData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.JiraField).IsRequired();
            builder.Property(x => x.SenseaiField).IsRequired();
            base.Configure(builder);
        }
    }
}