﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Jira.Configurations
{
    internal class ConfigJiraIssueTypeAttributeValueMapping : SenseAIEntityTypeConfiguration<JiraIssueTypeAttributeValueMappingData>
    {
        public override void Configure(EntityTypeBuilder<JiraIssueTypeAttributeValueMappingData> builder)
        {
            builder.ToTable(typeof(JiraIssueTypeAttributeValueMappingData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.JiraValue).IsRequired();
            builder.Property(x => x.SenseaiValue).IsRequired();
            base.Configure(builder);
        }
    }
}