﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Jira.Configurations
{
    internal class ConfigJiraProjectMapping : SenseAIEntityTypeConfiguration<JiraProjectMappingData>
    {
        public override void Configure(EntityTypeBuilder<JiraProjectMappingData> builder)
        {
            builder.ToTable(typeof(JiraProjectMappingData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ProjectId).IsRequired();
            builder.Property(x => x.SubprojectId).IsRequired();
            builder.Property(x => x.JiraProjectName).IsRequired();
            base.Configure(builder);
        }
    }
}