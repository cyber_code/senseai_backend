﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Jira.Configurations
{
    internal class ConfigJiraUserMapping : SenseAIEntityTypeConfiguration<JiraUserMappingData>
    {
        public override void Configure(EntityTypeBuilder<JiraUserMappingData> builder)
        {
            builder.ToTable(typeof(JiraUserMappingData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.SubprojectId).IsRequired();
            builder.Property(x => x.JiraUserId).IsRequired();
            builder.Property(x => x.SenseaiUserId).IsRequired();
            base.Configure(builder);
        }
    }
}