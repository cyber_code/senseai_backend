﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Jira.Configurations
{
    internal class ConfigJiraIssueTypeMapping : SenseAIEntityTypeConfiguration<JiraIssueTypeMappingData>
    {
        public override void Configure(EntityTypeBuilder<JiraIssueTypeMappingData> builder)
        {
            builder.ToTable(typeof(JiraIssueTypeMappingData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.JiraIssueTypeFieldId).IsRequired();
            builder.Property(x => x.JiraIssueTypeFieldName).IsRequired();
            builder.Property(x => x.JiraProjectMappingId).IsRequired();
            builder.Property(x => x.SenseaiIssueTypeField).IsRequired();
            base.Configure(builder);
        }
    }
}