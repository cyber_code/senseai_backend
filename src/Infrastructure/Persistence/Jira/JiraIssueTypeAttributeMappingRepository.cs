﻿using Persistence.Internal;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Jira
{
    public class JiraIssueTypeAttributeMappingRepository : IJiraIssueTypeAttributeMappingRepository
    {
        private readonly IBaseRepository<JiraIssueTypeAttributeMapping, JiraIssueTypeAttributeMappingData> _baseRepository;

        public JiraIssueTypeAttributeMappingRepository(IBaseRepository<JiraIssueTypeAttributeMapping, JiraIssueTypeAttributeMappingData> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public void Add(JiraIssueTypeAttributeMapping jiraIssueTypeAttributeMapping)
        {
            _baseRepository.Add(jiraIssueTypeAttributeMapping);
        }

        public void Update(JiraIssueTypeAttributeMapping jiraIssueTypeAttributeMapping)
        {
            _baseRepository.Update(jiraIssueTypeAttributeMapping);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }
    }
}
