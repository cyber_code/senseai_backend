﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.Jira
{
    [Table("JiraUserSubprojectConfigs")]
    public class JiraUserSubprojectConfigsData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string UserId { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraUsername { get; set; }

        public string JiraToken { get; set; }
    }
}