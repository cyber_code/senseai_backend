﻿using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.JiraModel;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.Jira
{
    public class JiraUserSubprojectRepository : IJiraUserSubprojectRepository
    {
        private readonly IBaseRepository<JiraUserSubprojectConfigs, JiraUserSubprojectConfigsData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public JiraUserSubprojectRepository(IBaseRepository<JiraUserSubprojectConfigs, JiraUserSubprojectConfigsData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(JiraUserSubprojectConfigs jiraProjectMapping)
        {
            _baseRepository.Add(jiraProjectMapping);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(JiraUserSubprojectConfigs jiraProjectMapping)
        {
            _baseRepository.Update(jiraProjectMapping);
        }

        public JiraUserSubprojectConfigs GetJiraUserMappingById(Guid SubprojectId)
        {
            var query = (from jira in _context.Set<JiraUserSubprojectConfigsData>().AsNoTracking()
                         where jira.SubprojectId == SubprojectId
                         select new JiraUserSubprojectConfigs(jira.UserId, jira.SubprojectId, jira.JiraUsername, jira.JiraToken)).FirstOrDefault();
            return query;
        }

        public JiraUserSubprojectConfigs GetJiraUserSubprojectConfigs(Guid SubprojectId, string UserId)
        {
            var query = (from jira in _context.Set<JiraUserSubprojectConfigsData>().AsNoTracking()
                         where jira.SubprojectId == SubprojectId
                         && jira.UserId == UserId
                         select new JiraUserSubprojectConfigs(jira.UserId, jira.SubprojectId, jira.JiraUsername, jira.JiraToken)).FirstOrDefault();
            return query;
        }
    }
}