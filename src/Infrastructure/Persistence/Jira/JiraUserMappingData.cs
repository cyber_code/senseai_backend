﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.Jira
{
    [Table("JiraUserMapping")]
    public class JiraUserMappingData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraUserId { get; set; }

        public string SenseaiUserId { get; set; }
    }
}