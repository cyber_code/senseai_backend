﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.Jira
{
    [Table("JiraIssueTypeAttributeValueMapping")]
    public class JiraIssueTypeAttributeValueMappingData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid JiraIssueTypeAttributeMappingId { get; set; }

        public string SenseaiValue { get; set; }

        public string JiraValue { get; set; }
    }
}