﻿using Persistence.Internal;
using SenseAI.Domain.BaseModel;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Jira
{
    public sealed class JiraIssueTypeAttributeValueMappingRepository : IJiraIssueTypeAttributeValueMappingRepository
    {
        private readonly IBaseRepository<JiraIssueTypeAttributeValueMapping, JiraIssueTypeAttributeValueMappingData> _baseRepository;

        public JiraIssueTypeAttributeValueMappingRepository(IBaseRepository<JiraIssueTypeAttributeValueMapping, JiraIssueTypeAttributeValueMappingData> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public void Add(JiraIssueTypeAttributeValueMapping jiraIssueTypeAttributeValueMapping)
        {
            _baseRepository.Add(jiraIssueTypeAttributeValueMapping);
        }

        public void Update(JiraIssueTypeAttributeValueMapping jiraIssueTypeAttributeValueMapping)
        {
            _baseRepository.Update(jiraIssueTypeAttributeValueMapping);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }
    }
}
