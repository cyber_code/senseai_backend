﻿using Core;
using Core.Abstractions;
using Core.Configuration;
using Persistence.Internal;
using System;
using System.Linq;

namespace Persistence
{
    /// <summary>
    /// Represents the data settings manager
    /// </summary>
    public class DataSettingsManager
    {
        #region Fields

        public static readonly DataSettingsManager Instance;
        private bool? _databaseIsInstalled;
        private bool? _databaseUpdated;
        private SenseAIConfig _saiConfig;
        private DataSettings _currentDataSettings;
        
        private IWorkContext _workContext;

        #endregion Fields

        static DataSettingsManager()
        {
            Instance = new DataSettingsManager
            {
                _saiConfig = EngineContext.Current.Resolve<SenseAIConfig>()
            };
        }

        #region Methods

        public void SetWorkContext(IWorkContext workContext)
        {
            _workContext = workContext;
        }

        /// <summary>
        /// Load data settings
        /// </summary>
        /// <param name="filePath">File path; pass null to use the default settings file</param>
        /// <param name="reloadSettings">Whether to reload data, if they already loaded</param>
        /// <param name="fileProvider">file provider</param>
        /// <returns>Data settings</returns>
        public DataSettings DataSettings
        {
            get
            {   var connectionString = _workContext?.ConnectionString;
                var messagingLogConnectionString = _workContext?.MessagingLogConnectionString;
                if (!string.IsNullOrEmpty(connectionString))
                {
                    _currentDataSettings.DataConnectionString = connectionString;
                }
                if (!string.IsNullOrEmpty(messagingLogConnectionString))
                {
                    _currentDataSettings.MessagingLogConnectionString = messagingLogConnectionString;
                }
                return _currentDataSettings ?? InitCurrentDattaSettings();
                
                DataSettings InitCurrentDattaSettings()
                {
                
                    var tenant = _workContext?.TenantId;
                    _currentDataSettings = string.IsNullOrEmpty(tenant) && _saiConfig.DataSettings.Any() ? _saiConfig.DataSettings.First() 
                        : _saiConfig.DataSettings.FirstOrDefault(opt => opt.TenantId.Equals(tenant, StringComparison.OrdinalIgnoreCase))
                          ?? _saiConfig.DataSettings.FirstOrDefault() ?? new DataSettings();
                   
                    return _currentDataSettings;
                }
            }
        }

        /// <summary>
        /// Reset "database is installed" cached information
        /// </summary>
        public void ResetCache()
        {
            _databaseIsInstalled = null;
            _databaseUpdated = null;
        }

        #endregion Methods

        #region Properties

        /// <summary>
        /// Gets a value indicating whether database is already installed
        /// </summary>
        public bool DatabaseIsInstalled
        {
            get
            {
                if (_databaseIsInstalled.HasValue)
                {
                    return _databaseIsInstalled.Value;
                }

                _databaseIsInstalled =
                    !string.IsNullOrEmpty(DataSettings?.DataConnectionString) &&
                    !string.IsNullOrEmpty(DataSettings?.MessagingLogConnectionString);

                return _databaseIsInstalled.Value;
            }
        }

        public bool DatabaseIsUpdated
        {
            get
            {
                if (_databaseUpdated.HasValue)
                {
                    return _databaseUpdated.Value;
                }
                var dbContext = EngineContext.Current.Resolve<IDbContext>();
                _databaseUpdated = dbContext.IsDatabaseUpdated();
                return _databaseUpdated.Value;
            }
        }

        public bool InstallSampleData => _saiConfig.InstallSampleData;
        public bool IsTesting => _saiConfig.IsTesting;

        #endregion Properties
    }
}