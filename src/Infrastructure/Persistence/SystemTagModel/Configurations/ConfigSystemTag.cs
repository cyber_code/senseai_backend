﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.SystemTagModel.Configurations
{
    internal class ConfigSystemTag : SenseAIEntityTypeConfiguration<SystemTagData>
    {
        public override void Configure(EntityTypeBuilder<SystemTagData> builder)
        {
            builder.ToTable(typeof(SystemTagData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.SystemId);
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(300).IsRequired();

            builder.HasOne(x => x.System)
                   .WithMany()
                   .HasForeignKey(x => x.SystemId);

            base.Configure(builder);
        }
    }
}