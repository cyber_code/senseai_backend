﻿using System;
using Persistence.Internal;
using SenseAI.Domain.SystemTags;

namespace Persistence.SystemTagModel
{
    public sealed class SystemTagRepository : ISystemTagRepository
    {
        private readonly IBaseRepository<SystemTag, SystemTagData> _baseRepository;

        public SystemTagRepository(IBaseRepository<SystemTag, SystemTagData> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public void Add(SystemTag systemTag)
        {
            _baseRepository.Add(systemTag);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(SystemTag systemTag)
        {
            _baseRepository.Update(systemTag);
        }
    }
}