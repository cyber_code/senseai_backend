﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Persistence.Internal;
using Persistence.SystemModel;

namespace Persistence.SystemTagModel
{
    [Table("SystemTags")]
    public class SystemTagData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid SystemId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public virtual SystemData System { get; set; }
    }
}