﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Persistence.Internal;

namespace Persistence.Process.HierarchiesModel
{
    [Table("HierarchyTypes")]
    public class HierarchyTypeData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid ParentId { get; set; }

        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int Level { get; set; }
    }
}