﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain.Process.ProcessModel;

namespace Persistence.Process.HierarchiesModel
{
    public sealed class HierarchyRepository : IHierarchyRepository
    {
        private readonly IBaseRepository<Hierarchy, HierarchyData> _baseRepository;
        private readonly SenseAIObjectContext _context;
        private readonly IProcessRepository _processRepository;

        private List<HierarchyData> hierarchiesToDelete;

        public HierarchyRepository(IBaseRepository<Hierarchy, HierarchyData> baseRepository, SenseAIObjectContext senseAIObjectContext, ProcessRepository processRepository)
        {
            _baseRepository = baseRepository;
            _context = senseAIObjectContext;
            _processRepository = processRepository;
        }

        public void Add(Hierarchy hierarchy)
        {
            var hierarchyType = _context.Set<HierarchyTypeData>().AsNoTracking().FirstOrDefault(w => w.ParentId == hierarchy.HierarchyTypeId && w.SubProjectId == hierarchy.SubProjectId);
            if (hierarchyType != null)
                hierarchy.SetHierarchyTypeId(hierarchyType.Id);
            _baseRepository.Add(hierarchy);
        }

        public void AddFromImport(Hierarchy hierarchy)
        {
            _baseRepository.Add(hierarchy);
        }

        public void Update(Hierarchy hierarchy)
        {
            var updHierarchy = _context.Set<HierarchyData>().AsNoTracking().FirstOrDefault(w => w.Id == hierarchy.Id);
            if (hierarchy.ParentId == Guid.Empty)
                hierarchy.SetParentId(updHierarchy.ParentId);
            var data = _baseRepository.Map(hierarchy);

            _context.Set<HierarchyData>().Attach(data);
            var entry = _context.Entry(data);
            entry.State = EntityState.Modified;
            entry.Property(property => property.ExternalSystemId).IsModified = false;

            _context.SaveChanges();

            //var updHierarchy = _context.Set<HierarchyData>().AsNoTracking().FirstOrDefault(w => w.Id == hierarchy.Id);
            //if (hierarchy.ParentId == Guid.Empty)
            //    hierarchy.SetParentId(updHierarchy.ParentId);
            //_baseRepository.Update(hierarchy);
        }

        public bool Delete(Guid id, bool forceDelete)
        {
            var hierarchy = _context.Set<HierarchyData>().Where(f => f.Id == id).FirstOrDefault();
            if (hierarchy is null)
                throw new Exception(String.Format("Hierarchy with id: {0} dosn't exist!", id.ToString()));

            if (!HasChildElements(id))
            {
                _baseRepository.Delete(id);
                return true;
            }

            if (!forceDelete)
                return false;

            hierarchiesToDelete = new List<HierarchyData>();
            GetChildHierarchies(id);

            var parent = _context.Set<HierarchyData>().Where(h => h.Id == id).FirstOrDefault();
            hierarchiesToDelete.Add(parent);

            foreach (HierarchyData hd in hierarchiesToDelete)
            {
                var processes = _context.Set<ProcessData>().AsNoTracking()
                    .Where(p => p.HierarchyId == hd.Id).ToList();

                processes.ForEach(p => _processRepository.Delete(p.Id, true));
            }

            _context.Set<HierarchyData>().RemoveRange(hierarchiesToDelete);
            _context.SaveChanges();

            return true;
        }

        public void Move(Guid fromId, Guid toId)
        {
            var hierarchiesToupate = _context.Set<HierarchyData>().Where(u => u.ParentId == fromId).ToList();
            var processesToUpdate = _context.Set<ProcessData>().Where(u => u.HierarchyId == fromId).ToList();

            hierarchiesToupate.ForEach(h => h.ParentId = toId);
            processesToUpdate.ForEach(p => p.HierarchyId = toId);

            _context.SaveChanges();
            _baseRepository.Delete(fromId);
        }

        private bool HasChildElements(Guid hierarchyId)
        {
            bool hasChildElements = _context.Set<HierarchyData>().AsNoTracking().Where(h => h.ParentId == hierarchyId).Any();
            bool hasProcesses = _context.Set<ProcessData>().AsNoTracking().Where(h => h.HierarchyId == hierarchyId).Any();

            if (hasChildElements || hasProcesses)
                return true;

            return false;
        }

        private void GetChildHierarchies(Guid id)
        {
            var hierarchies = _context.Set<HierarchyData>().AsNoTracking()
                               .Where(h => h.ParentId == id).ToList();

            hierarchiesToDelete.AddRange(hierarchies);

            foreach (HierarchyData hd in hierarchies)
            {
                GetChildHierarchies(hd.Id);
            }
        }
    }
}