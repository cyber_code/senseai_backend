﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain.Process.ProcessModel;

namespace Persistence.Process.HierarchiesModel
{
    public sealed class HierarchyTypeRepository : IHierarchyTypeRepository
    {
        private readonly IBaseRepository<HierarchyType, HierarchyTypeData> _baseRepository;
        private readonly SenseAIObjectContext _context;
        private readonly IProcessRepository _processRepository;
        private List<HierarchyType> result;
        List<HierarchyTypeData> hierarchyTypesToDelete;

        public HierarchyTypeRepository(IBaseRepository<HierarchyType, HierarchyTypeData> baseRepository, SenseAIObjectContext context, ProcessRepository processRepository)
        {
            _baseRepository = baseRepository;
            _context = context;
            _processRepository = processRepository;
            result = new List<HierarchyType>();
        }

        public bool Add(HierarchyType ht, bool forceDelete, out string parentName)
        {
            parentName = "";
            if (ht.ParentId != Guid.Empty)
            {
                var hierarchyTypeParent = _context.Set<HierarchyTypeData>().Where(f => f.Id == ht.ParentId).FirstOrDefault();
                if (hierarchyTypeParent is null)
                    throw new Exception(String.Format("HierarchyType with id: {0} dosn't exist!", ht.ParentId.ToString()));
                parentName = hierarchyTypeParent.Title;
                var hierarchiesRelatedToType = _context.Set<HierarchyData>()
                   .Where(h => h.HierarchyTypeId == ht.ParentId);
                List<ProcessData> allProcesses = new List<ProcessData>();
                foreach (HierarchyData hd in hierarchiesRelatedToType)
                {
                    var processes = _context.Set<ProcessData>().AsNoTracking()
                        .Where(p => p.HierarchyId == hd.Id).ToList();
                    allProcesses.AddRange(processes);
                }
                if (allProcesses.Count == 0)
                {
                    AddHierarchyType(ht);
                    return true;
                }
                if (!forceDelete)
                    return false;
                allProcesses.ForEach(p => _processRepository.Delete(p.Id));
                _context.SaveChanges();
            }
            AddHierarchyType(ht);
            return true;
        }

        private void AddHierarchyType(HierarchyType ht)
        {
            int level = 1;
            var maxLevel = _context.Set<HierarchyTypeData>()
                           .Where(u => u.SubProjectId == ht.SubProjectId)
                           .Select(u => (int?)u.Level).Max();

            if (maxLevel != null)
                level = (int)maxLevel + 1;

            HierarchyType hierarchyType = new HierarchyType(ht.Id, ht.ParentId, ht.SubProjectId, ht.Title, ht.Description, level);
            _baseRepository.Add(hierarchyType);
        }

        public void Update(HierarchyType hierarchyType)
        {
            HierarchyTypeData hierarchyTypeData = _context.Set<HierarchyTypeData>()
                           .FirstOrDefault(w => w.Id == hierarchyType.Id);

            hierarchyType = new HierarchyType(hierarchyType.Id, hierarchyType.ParentId, hierarchyType.SubProjectId, hierarchyType.Title, hierarchyType.Description, hierarchyTypeData.Level);

            _baseRepository.Update(hierarchyType);
        }

        public bool Delete(Guid id, bool forceDelete)
        {
            var hierarchyType = _context.Set<HierarchyTypeData>().Where(f => f.Id == id).FirstOrDefault();
            if (hierarchyType is null)
                throw new Exception(String.Format("HierarchyType with id: {0} dosn't exist!", id.ToString()));

            bool hasChildHierarchyTypes = _context.Set<HierarchyTypeData>().AsNoTracking().Where(h => h.ParentId == id).Any();
            bool hasChildHierarchies = _context.Set<HierarchyData>().AsNoTracking().Where(h => h.HierarchyTypeId == id).Any();

            if (!(hasChildHierarchyTypes || hasChildHierarchies))
            {
                _baseRepository.Delete(id);
                return true;
            }

            if (!forceDelete)
                return false;

            hierarchyTypesToDelete = new List<HierarchyTypeData>();
            GetChildElementsForHierarchyTypes(id);

            var parent = _context.Set<HierarchyTypeData>().Where(u => u.Id == id).FirstOrDefault();
            hierarchyTypesToDelete.Add(parent);

            foreach (HierarchyTypeData hdt in hierarchyTypesToDelete)
            {
                var hierarchiesToDelete = _context.Set<HierarchyData>()
                    .Where(h => h.HierarchyTypeId == hdt.Id);

                foreach (HierarchyData hd in hierarchiesToDelete)
                {
                    var processes = _context.Set<ProcessData>().AsNoTracking()
                        .Where(p => p.HierarchyId == hd.Id).ToList();

                    processes.ForEach(p => _processRepository.Delete(p.Id));
                }
            }

            _context.Set<HierarchyTypeData>().RemoveRange(hierarchyTypesToDelete);
            _context.SaveChanges();
            return true;
        }

        private void GetChildElementsForHierarchyTypes(Guid id)
        {
            var hierarchyTypes = _context.Set<HierarchyTypeData>().AsNoTracking()
                               .Where(h => h.ParentId == id).ToList();

            hierarchyTypesToDelete.AddRange(hierarchyTypes);

            foreach (HierarchyTypeData htd in hierarchyTypes)
            {
                GetChildElementsForHierarchyTypes(htd.Id);
            }
        }

        public List<HierarchyType> GetHierarchyTypeLevels(Guid SubProjectId)
        {
            var getHierarchy = (from a in _context.Set<HierarchyTypeData>()
                                where (a.ParentId == null || a.ParentId == Guid.Empty)
                                && a.SubProjectId == SubProjectId
                                select new
                                {
                                    a.Id,
                                    ParentId = a.ParentId == null ? Guid.Empty : a.ParentId,
                                    a.SubProjectId,
                                    a.Title,
                                    a.Description
                                }
                               ).FirstOrDefault();
            if (getHierarchy != null)
            {
                //Add route item
                List<HierarchyType> listItem = new List<HierarchyType>();
                HierarchyType Item = new HierarchyType(getHierarchy.Id, getHierarchy.ParentId, getHierarchy.SubProjectId, getHierarchy.Title, getHierarchy.Description, 1);
                listItem.Add(Item);
                result.AddRange(listItem);
                //Call recursion function to get all child elements
                return AddHierarchyLevel(listItem, 2);
            }
            else
            {
                return new List<HierarchyType>();
            }
        }

        public List<HierarchyType> AddHierarchyLevel(List<HierarchyType> elements, int level)
        {
            var getHierarchyObj = new List<HierarchyType>();
            List<HierarchyType> res = new List<HierarchyType>();
            if (elements.Count() > 0)
            {
                foreach (var element in elements)
                {
                    var getHierarchy = _context.Set<HierarchyTypeData>().AsNoTracking().Where(a => a.ParentId == element.Id).ToList();
                    foreach (var item in getHierarchy)
                    {
                        getHierarchyObj.Add(new HierarchyType(item.Id, item.ParentId, item.SubProjectId, item.Title, item.Description, level));
                    }

                    foreach (var child in getHierarchyObj)
                    {
                        res.Add(new HierarchyType(child.Id, child.ParentId, child.SubProjectId, child.Title, child.Description, level));
                        result.AddRange(res);
                    }
                }
                level++;
                return AddHierarchyLevel(res, level);
            }
            else
            {
                return result;
            }
        }

        public HierarchyType GetHierarchyTypeById(Guid id)
        {
            var getHierarchy = _context.Set<HierarchyTypeData>().AsNoTracking().FirstOrDefault(h => h.Id == id);
            return new HierarchyType(id, getHierarchy.ParentId, getHierarchy.Title, getHierarchy.Description);
        }

        public void Move(Guid id)
        {
            var hierarchyTypeToDelete = _context.Set<HierarchyTypeData>().Where(u => u.Id == id).FirstOrDefault();

            var childHierarchyTypes = _context.Set<HierarchyTypeData>().Where(u => u.ParentId == id).ToList();
            childHierarchyTypes.ForEach(u => u.ParentId = hierarchyTypeToDelete.ParentId);

            var hierarchysToDelete = _context.Set<HierarchyData>().Where(h => h.HierarchyTypeId == id);

            foreach (HierarchyData hd in hierarchysToDelete)
            {
                var processes = _context.Set<ProcessData>().Where(u => u.HierarchyId == hd.Id).ToList();
                processes.ForEach(p => p.HierarchyId = hd.ParentId);

                var childHierarchies = _context.Set<HierarchyData>().Where(u => u.ParentId == hd.Id).ToList();
                childHierarchies.ForEach(h => h.ParentId = hd.ParentId);
            }

            _context.Set<HierarchyData>().RemoveRange(hierarchysToDelete);
            _context.SaveChanges();
            _baseRepository.Delete(id);
        }

        public bool CheckForHierarchyTypes(Guid SubProjectId, bool forceDelete)
        {
            if (!forceDelete)
            {
                return _context.Set<HierarchyTypeData>().Where(u => u.SubProjectId == SubProjectId).Any();
            }

            var hierarchyTypes = _context.Set<HierarchyTypeData>().Where(u => u.SubProjectId == SubProjectId && u.ParentId == Guid.Empty).ToList();
            hierarchyTypes.ForEach(h => this.Delete(h.Id, true));

            return true;
        }
    }
}