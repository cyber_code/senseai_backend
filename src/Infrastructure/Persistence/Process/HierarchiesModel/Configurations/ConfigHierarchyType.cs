﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.HierarchiesModel.Configurations
{
    internal class ConfigHierarchyType : SenseAIEntityTypeConfiguration<HierarchyTypeData>
    {
        public override void Configure(EntityTypeBuilder<HierarchyTypeData> builder)
        {
            builder.ToTable(typeof(HierarchyTypeData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.ParentId).IsRequired();
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(200).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(300);

            base.Configure(builder);    
        }
    }
}