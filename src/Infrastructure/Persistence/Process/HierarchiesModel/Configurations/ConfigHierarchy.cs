﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.HierarchiesModel.Configurations
{
    internal class ConfigHierarchy : SenseAIEntityTypeConfiguration<HierarchyData>
    {
        public override void Configure(EntityTypeBuilder<HierarchyData> builder)
        {
            builder.ToTable(typeof(HierarchyData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.ParentId).IsRequired();
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.HierarchyTypeId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(200).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(300);

            builder.HasOne(x => x.HierarchyType)
                   .WithMany()
                   .HasForeignKey(x => x.HierarchyTypeId);

            base.Configure(builder);
        }
    }
}