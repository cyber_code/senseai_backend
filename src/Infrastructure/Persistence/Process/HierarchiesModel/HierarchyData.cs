﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Persistence.Internal;

namespace Persistence.Process.HierarchiesModel
{
    [Table("Hierarchies")]
    public class HierarchyData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid ParentId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid HierarchyTypeId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public virtual HierarchyTypeData HierarchyType { get; set; }

        public string ExternalSystemId { get; set; }
    }
}