﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.ArisWorkflowModel
{
    [Table("ArisWorkflowVersions")]
    public class ArisWorkflowVersionData : BaseData
    {
        public Guid ArisWorkflowId { get; set; }

        public int VersionId { get; set; }

        public Guid ProcessId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string DesignerJson { get; set; }

        public string WorkflowImage { get; set; }

        public bool IsLast { get; set; }

        public bool IsParsed { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}