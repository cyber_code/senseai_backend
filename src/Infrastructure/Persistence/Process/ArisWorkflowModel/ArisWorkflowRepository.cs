﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.Process.ArisWorkflowModel;

namespace Persistence.Process.ArisWorkflowModel
{
    public sealed class ArisWorkflowRepository : IArisWorkflowRepository
    {
        private readonly IBaseRepository<ArisWorkflow, ArisWorkflowData> _baseRepository;

        private readonly SenseAIObjectContext _context;

        public ArisWorkflowRepository(IBaseRepository<ArisWorkflow, ArisWorkflowData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(ArisWorkflow arisWorkflow)
        {
            _baseRepository.Add(arisWorkflow);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public ArisWorkflow GetArisWorkflow(Guid id)
        {
            var arisWorkflowData = _context.Set<ArisWorkflowData>().AsNoTracking().FirstOrDefault(w => w.Id == id);
            return new ArisWorkflow(arisWorkflowData.Id, arisWorkflowData.ProcessId, arisWorkflowData.Title, arisWorkflowData.Description, arisWorkflowData.DesignerJson);
        }

        public void Save(ArisWorkflow arisWorkflow, string designerJson)
        {
            var arisWorkflowData = _context.Set<ArisWorkflowData>().AsNoTracking()
              .FirstOrDefault(w => w.Id == arisWorkflow.Id);
            arisWorkflowData.DesignerJson = designerJson;
            arisWorkflowData.IsParsed = arisWorkflow.IsParsed;
            _context.Set<ArisWorkflowData>().Update(arisWorkflowData);

            //delete arisworkflow items
            var arisWorkflowItems = _context.Set<ArisWorkflowItemData>().Where(w => w.ArisWorkflowId == arisWorkflow.Id).AsNoTracking();
            _context.Set<ArisWorkflowItemData>().RemoveRange(arisWorkflowItems);

            foreach (var item in arisWorkflow.Items)
            {
                var arisWorkflowItemData = new ArisWorkflowItemData
                {
                    Id = Guid.NewGuid(),
                    ArisWorkflowId = arisWorkflow.Id,
                    Type = (int)item.Type,
                    Title = item.Title,
                    Description = item.Description,
                    TypicalName = item.TypicalName,
                    ArisWorkflowLinkId = item.ArisWorkflowLinkId,
                };

                _context.Add(arisWorkflowItemData);
            }
            _context.SaveChanges();
        }

        public void Issue(Guid id, string arisWorkflowImage)
        {
            var arisWorkflowData = _context.Set<ArisWorkflowData>().AsNoTracking().FirstOrDefault(w => w.Id == id);
            var lastArisWorkflowVersionData = _context.Set<ArisWorkflowVersionData>().FirstOrDefault(w => w.ArisWorkflowId == id && w.IsLast);

            int versionId = 0;
            if (lastArisWorkflowVersionData != null)
            {
                lastArisWorkflowVersionData.IsLast = false;
                if (lastArisWorkflowVersionData != null)
                    versionId = _context.Set<ArisWorkflowVersionData>()
                                        .Where(t => t.ArisWorkflowId == id)
                                        .Max(t => t.VersionId) + 1;
            }
            else
            {
                lastArisWorkflowVersionData = new ArisWorkflowVersionData { VersionId = versionId };
            }

            var arisWorkflowVersionData = new ArisWorkflowVersionData
            {
                ProcessId = arisWorkflowData.ProcessId,
                ArisWorkflowId = arisWorkflowData.Id,
                VersionId = versionId,
                Title = arisWorkflowData.Title,
                Description = arisWorkflowData.Description,
                DesignerJson = arisWorkflowData.DesignerJson,
                WorkflowImage = arisWorkflowImage,
                IsLast = true,
                IsParsed = arisWorkflowData.IsParsed,
            };

            _context.Set<ArisWorkflowVersionData>().Add(arisWorkflowVersionData);
            _context.SaveChanges();

        }

        public void Update(Guid id)
        {
            var arisWorkflowData = _context.Set<ArisWorkflowData>().AsNoTracking().FirstOrDefault(w => w.Id == id);

            var arisWorkflowVersionData = _context.Set<ArisWorkflowVersionData>().FirstOrDefault(w => w.ArisWorkflowId == id && w.IsLast == true);
            arisWorkflowVersionData.DesignerJson = arisWorkflowData.DesignerJson;

            _context.Set<ArisWorkflowVersionData>().Update(arisWorkflowVersionData);
            _context.SaveChanges();
        }

        public void Rename(Guid id, string title)
        {
            var arisWorkflow = _context.Set<ArisWorkflowData>().AsNoTracking().FirstOrDefault(w => w.Id == id);
            if (arisWorkflow != null)
            {
                arisWorkflow.Title = title;
                _context.Set<ArisWorkflowData>().Update(arisWorkflow);
            }

            _context.SaveChanges();
        }

        public string RollbackWorkflow(Guid arisWorkflowId, int versionId)
        {
            var arisWorkflowVersions = _context.Set<ArisWorkflowVersionData>().AsNoTracking().Where(w => w.ArisWorkflowId == arisWorkflowId && w.VersionId != versionId);
            foreach (var item in arisWorkflowVersions)
            {
                item.IsLast = false;
                _context.Set<ArisWorkflowVersionData>().Update(item);
            }

            var arisWorkflowVersionData = _context.Set<ArisWorkflowVersionData>().AsNoTracking()
                .FirstOrDefault(w => w.ArisWorkflowId == arisWorkflowId && w.VersionId == versionId);
            if (arisWorkflowVersionData == null)
                throw new Exception("The aris workflow does not have any version.");
            arisWorkflowVersionData.IsLast = true;

            _context.Set<ArisWorkflowVersionData>().Update(arisWorkflowVersionData);

            //delete aris workflow items
            var arisWorkflowItems = _context.Set<ArisWorkflowItemData>().Where(w => w.ArisWorkflowId == arisWorkflowId);
            _context.Set<ArisWorkflowItemData>().RemoveRange(arisWorkflowItems);

            //get workflowdomain
            var arisWorkflow = GetArisWorkflowByVersion(arisWorkflowId, versionId);
                                  
            //update workflow
            var arisWorkflowData = _context.Set<ArisWorkflowData>().AsNoTracking().FirstOrDefault(w=> w.Id == arisWorkflowId);
            arisWorkflowData.DesignerJson = arisWorkflowVersionData.DesignerJson;
            var checkArisWorkflowData = _context.Set<ArisWorkflowData>().Local.FirstOrDefault(w => w.Id == arisWorkflowId);
            if (checkArisWorkflowData != null)
            {
                checkArisWorkflowData.DesignerJson = arisWorkflowVersionData.DesignerJson;
            }
            else
                _context.Set<ArisWorkflowData>().Update(arisWorkflowData);

            //add workflow items
            foreach (var item in arisWorkflow.Items)
            {
                var workflowItemData = new ArisWorkflowItemData
                {
                    ArisWorkflowId = arisWorkflow.Id,
                    Id = Guid.NewGuid(),
                    Title = item.Title,
                    Description = item.Description,
                    ArisWorkflowLinkId = item.ArisWorkflowLinkId,
                    Type = (int)item.Type,
                    TypicalName = item.TypicalName
                };
                _context.Add(workflowItemData);
            }

            _context.SaveChanges();

            return arisWorkflowVersionData.DesignerJson;
        }

        public ArisWorkflow GetArisWorkflowByVersion(Guid workflowId, int versionId)
        {
            var arisWorkflowVersionData = _context.Set<ArisWorkflowVersionData>().FirstOrDefault(w => w.ArisWorkflowId == workflowId && w.VersionId == versionId);
            ArisWorkflow arisWorkflow = new ArisWorkflow(arisWorkflowVersionData.ArisWorkflowId, arisWorkflowVersionData.ProcessId, arisWorkflowVersionData.Title,
                arisWorkflowVersionData.Description, arisWorkflowVersionData.DesignerJson);
            if (!string.IsNullOrEmpty(arisWorkflowVersionData.DesignerJson))
            {
                return ArisWorkflowFactory.Create(arisWorkflowVersionData.ArisWorkflowId, arisWorkflowVersionData.DesignerJson, arisWorkflowVersionData.ProcessId);
            }
            return arisWorkflow;
        }

        public int GetNrVersions(Guid arisWorkflowId)
        {
            return _context.Set<ArisWorkflowVersionData>().AsNoTracking().Where(w => w.ArisWorkflowId == arisWorkflowId).Count();
        }

        public string GetArisWorkflowImage(Guid arisWorkflowId, int versionId)
        {
           return _context.Set<ArisWorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.ArisWorkflowId == arisWorkflowId &&
                                                                                        w.VersionId == versionId).WorkflowImage;
        }
    }
}
