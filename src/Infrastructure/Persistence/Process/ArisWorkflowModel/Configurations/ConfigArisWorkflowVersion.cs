﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.ArisWorkflowModel.Configurations
{
    internal class ConfigArisWorkflowVersion : SenseAIEntityTypeConfiguration<ArisWorkflowVersionData>
    {
        public override void Configure(EntityTypeBuilder<ArisWorkflowVersionData> builder)
        {
            builder.ToTable(typeof(ArisWorkflowVersionData).Name);

            builder.HasKey(t => new { t.ArisWorkflowId, t.VersionId });

            builder.Property(e => e.VersionId);

            builder.Property(t => t.Title).IsRequired();

            base.Configure(builder);
        }
    }
}
