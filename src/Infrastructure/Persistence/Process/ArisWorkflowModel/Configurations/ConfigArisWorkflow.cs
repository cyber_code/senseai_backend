﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.ArisWorkflowModel.Configurations
{
    internal class ConfigArisWorkflow : SenseAIEntityTypeConfiguration<ArisWorkflowData>
    {
        public override void Configure(EntityTypeBuilder<ArisWorkflowData> builder)
        {
            builder.ToTable(typeof(ArisWorkflowData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ProcessId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(500);

            base.Configure(builder);
        }
    }
}
