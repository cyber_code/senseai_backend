﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.ArisWorkflowModel.Configurations
{
    internal class ConfigArisWorkflowItem : SenseAIEntityTypeConfiguration<ArisWorkflowItemData>
    {
        public override void Configure(EntityTypeBuilder<ArisWorkflowItemData> builder)
        {
            builder.ToTable(typeof(ArisWorkflowItemData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ArisWorkflowId);
            builder.Property(x => x.Title).HasMaxLength(1000);
            builder.Property(x => x.Description).HasMaxLength(3000);

            base.Configure(builder);
        }
    }
}
