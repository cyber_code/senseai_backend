﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.ArisWorkflowModel
{
    [Table("ArisWorkflowItems")]
    public class ArisWorkflowItemData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid ArisWorkflowId { get; set; }

        public Guid ArisWorkflowLinkId { get; set; }

        public int Type { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string TypicalName { get; set; } 
    }
}
