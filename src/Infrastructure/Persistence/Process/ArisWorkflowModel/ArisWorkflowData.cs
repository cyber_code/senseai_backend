﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.ArisWorkflowModel
{
    [Table("ArisWorkflows")]
    public class ArisWorkflowData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid ProcessId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string DesignerJson { get; set; }

        public bool IsParsed { get; set; }
    }
}