﻿using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.Process.ProcessModel
{
    public sealed class ProcessResourcesRepository : IProcessResourcesRepository
    {
        private readonly IBaseRepository<ProcessResources, ProcessResourcesData> _baseRepository;
        private readonly SenseAIObjectContext _context;
        public ProcessResourcesRepository(IBaseRepository<ProcessResources, ProcessResourcesData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }
        public void Add(ProcessResources processResources)
        {
            _baseRepository.Add(processResources);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(ProcessResources processResources)
        {
            _baseRepository.Update(processResources);
        }
        public ProcessResources GetProcessResources(Guid id)
        {
            var data = _baseRepository.GetById(id).FirstOrDefault();
            return new ProcessResources(data.Id, data.FileName, data.ProcessId, data.FilePath);
        }
        public List<ProcessResources> GetProcessesResourcesByProcessId(Guid processId)
        {
            return _context.Set<ProcessResourcesData>().AsNoTracking()
                .Where(pr => pr.ProcessId == processId)
                .Select(pr => new ProcessResources(pr.Id, pr.FileName, pr.ProcessId, pr.FilePath))
                .ToList();
        }
    }
}
