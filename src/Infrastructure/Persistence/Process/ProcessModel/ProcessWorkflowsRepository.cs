﻿using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Linq;

namespace Persistence.Process.IssuesModel
{
    public sealed class ProcessWorkflowsRepository : IProcessWorkflowRepository
    {
        private readonly IBaseRepository<ProcessWorkflow, ProcessWorkflowsData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public ProcessWorkflowsRepository(IBaseRepository<ProcessWorkflow, ProcessWorkflowsData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(ProcessWorkflow processWorkflow)
        {
            _baseRepository.Add(processWorkflow);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(ProcessWorkflow processWorkflow)
        {
            var item = _context.Set<ProcessWorkflowsData>().FirstOrDefault(i => i.Id == processWorkflow.Id);

            if (item == null)
                throw new Exception("This process workflow does not exists!");

            item.ProcessId = processWorkflow.ProcessId;
            item.WorkflowId = processWorkflow.WorkflowId;

            _context.SaveChanges();
        }
    }
}
