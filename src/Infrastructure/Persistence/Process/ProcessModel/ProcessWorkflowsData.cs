﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.ProcessModel
{
    [Table("ProcessWorkflows")]
    public class ProcessWorkflowsData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid ProcessId { get; set; }

        public Guid WorkflowId { get; set; }
    }
}
