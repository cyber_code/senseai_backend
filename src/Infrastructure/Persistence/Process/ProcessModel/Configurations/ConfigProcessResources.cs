﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Process.ProcessModel.Configurations
{
    internal class ConfigProcessResources : SenseAIEntityTypeConfiguration<ProcessResourcesData>
    {
        public override void Configure(EntityTypeBuilder<ProcessResourcesData> builder)
        {
            builder.ToTable(typeof(ProcessResourcesData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.FileName).HasMaxLength(300).IsRequired();
            builder.Property(x => x.FilePath).HasMaxLength(500);
            builder.Property(x => x.ProcessId).IsRequired();

            base.Configure(builder);
        }
    }
}
