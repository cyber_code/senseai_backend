﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.ProcessModel.Configurations
{
    internal class ConfigProcessWorkflows : SenseAIEntityTypeConfiguration<ProcessWorkflowsData>
    {
        public override void Configure(EntityTypeBuilder<ProcessWorkflowsData> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ProcessId).IsRequired();
            builder.Property(x => x.WorkflowId).IsRequired();

            base.Configure(builder);
        }
    }
}
