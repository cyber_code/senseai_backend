﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.ProcessModel.Configurations
{
    internal class ConfigProcess : SenseAIEntityTypeConfiguration<ProcessData>
    {
        public override void Configure(EntityTypeBuilder<ProcessData> builder)
        {
            builder.ToTable(typeof(ProcessData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.HierarchyId).IsRequired();
            builder.Property(x => x.CreatedId).IsRequired();
            builder.Property(x => x.OwnerId).IsRequired();
            builder.Property(x => x.ProcessType).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(200).IsRequired();
            builder.Property(x => x.Description);

            base.Configure(builder);
        }
    }
}