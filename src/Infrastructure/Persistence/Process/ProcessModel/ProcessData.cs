﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Persistence.Internal;
using SenseAI.Domain;

namespace Persistence.Process.ProcessModel
{
    [Table("Processes")]
    public class ProcessData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid HierarchyId { get; set; }

        public Guid CreatedId { get; set; }

        public Guid OwnerId { get; set; }

        public ProcessType ProcessType { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
        public Guid RequirementPriorityId { get;  set; }
        public Guid RequirementTypeId { get;  set; }
        public int ExpectedNumberOfTestCases { get; set; }
        public Guid TypicalId { get; set; }
        public string ExternalSystemId { get; set; }
    }
}