﻿using System;
using System.Linq;
using Core.Exceptions;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ArisWorkflowModel;
using Persistence.Process.GenericWorkflowModel;
using Persistence.Process.IssuesModel;
using SenseAI.Domain;
using SenseAI.Domain.Process.ProcessModel;
using process = SenseAI.Domain.Process.ProcessModel.Process;

namespace Persistence.Process.ProcessModel
{
    public sealed class ProcessRepository : IProcessRepository
    {
        private readonly IBaseRepository<process, ProcessData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public ProcessRepository(IBaseRepository<process, ProcessData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(process Process)
        {
            _baseRepository.Add(Process);
        }

        public bool Delete(Guid id, bool forceDelete)
        {
            var process = _context.Set<ProcessData>().Where(f => f.Id == id).FirstOrDefault();
            if (process is null)
                throw new Exception(String.Format("Process with id: {0} dosn't exist!", id.ToString()));

            bool hasChildElements = CheckForChildElements(id);

            if (!hasChildElements)
            {
                _baseRepository.Delete(id);
                return true;
            }
            if (!forceDelete)
                return false;

            DeleteProcessItems(id);

            _context.SaveChanges();
            _baseRepository.Delete(id);

            return true;
        }

        public bool Delete(Guid id)
        {
            var process = _context.Set<ProcessData>().Where(f => f.Id == id).FirstOrDefault();
            if (process is null)
                throw new Exception(String.Format("Process with id: {0} dosn't exist!", id.ToString()));

            bool hasChildElements = CheckForChildElements(id);

            if (!hasChildElements)
            {
                _context.Remove(process);
                return true;
            }

            DeleteProcessItems(id);
            _context.Remove(process);
            return true;
        }

        public void Update(process Process)
        {
            var data = _baseRepository.Map(Process);

            _context.Set<ProcessData>().Attach(data);
            var entry = _context.Entry(data);
            entry.State = EntityState.Modified;
            entry.Property(property => property.ExternalSystemId).IsModified = false;

            _context.SaveChanges();
        }

        public void Paste(process Process)
        {
            _baseRepository.Add(Process);
        }

        public void Paste(process Process, PasteType type)
        {
            if (type == PasteType.Copy)
            {
                _baseRepository.Add(Process);
            }
            else
            {
                _baseRepository.Update(Process);
            }
        }

        private void DeleteProcessItems(Guid id)
        {
            // delete process resources
            var processResources = _context.Set<ProcessResourcesData>().AsNoTracking()
            .Where(p => p.ProcessId == id);
            _context.Set<ProcessResourcesData>().RemoveRange(processResources);

            // get arsiWorklfows
            var arisWorkflows = _context.Set<ArisWorkflowData>().AsNoTracking()
                .Where(a => a.ProcessId == id);
            var arisWorkflowIds = arisWorkflows.Select(x => x.Id);

            // delete aris workflowitems
            var arisWorkflowItems = _context.Set<ArisWorkflowItemData>().AsNoTracking()
                .Where(u => arisWorkflowIds.Contains(u.ArisWorkflowId));
            _context.Set<ArisWorkflowItemData>().RemoveRange(arisWorkflowItems);

            // delete aris workflowVersions
            var arisWorkflowVersions = _context.Set<ArisWorkflowVersionData>().AsNoTracking()
                .Where(u => arisWorkflowIds.Contains(u.ArisWorkflowId));
            _context.Set<ArisWorkflowVersionData>().RemoveRange(arisWorkflowVersions);

            // delete aris workflows
            _context.Set<ArisWorkflowData>().RemoveRange(arisWorkflows);

            // delete process workflows
            var processWorkflows = _context.Set<ProcessWorkflowsData>().AsNoTracking()
            .Where(a => a.ProcessId == id);
            _context.Set<ProcessWorkflowsData>().RemoveRange(processWorkflows);

            // get genericWorkflows
            var genericWorkflows = _context.Set<GenericWorkflowData>().AsNoTracking()
                .Where(a => a.ProcessId == id);
            var genericWorkflowsIds = genericWorkflows.Select(i => i.Id);

            // delete generic workflowItems
            var genericWorkflowItems = _context.Set<GenericWorkflowItemData>().AsNoTracking()
                .Where(u => genericWorkflowsIds.Contains(u.GenericWorkflowId));
            _context.Set<GenericWorkflowItemData>().RemoveRange(genericWorkflowItems);

            // delete gjeneric workflow versions
            var genericWorkflowVersions = _context.Set<GenericWorkflowVersionData>().AsNoTracking()
            .Where(u => genericWorkflowsIds.Contains(u.GenericWorkflowId));
            _context.Set<GenericWorkflowVersionData>().RemoveRange(genericWorkflowVersions);

            // delete gjeneric workflows
            _context.Set<GenericWorkflowData>().RemoveRange(genericWorkflows);

            // delete issues
            var issues = _context.Set<IssueData>().AsNoTracking()
                .Where(u => u.ProcessId == id);
            _context.Set<IssueData>().RemoveRange(issues);
        }

        private bool CheckForChildElements(Guid processId)
        {
            bool result = false;

            var processResources = _context.Set<ProcessResourcesData>().AsNoTracking()
                .Where(p => p.ProcessId == processId).Any();

            var processArisWorkflows = _context.Set<ArisWorkflowData>().AsNoTracking()
                .Where(a => a.ProcessId == processId).Any();

            var processWorkflows = _context.Set<ProcessWorkflowsData>().AsNoTracking()
                .Where(a => a.ProcessId == processId).Any();

            var genericWorkflows = _context.Set<GenericWorkflowData>().AsNoTracking()
                .Where(g => g.ProcessId == processId).Any();

            var issues = _context.Set<IssueData>().AsNoTracking()
                .Where(g => g.ProcessId == processId).Any();

            if (processResources || processArisWorkflows || processWorkflows || genericWorkflows || issues)
                result = true;

            return result;
        }

        //public void Update(process Process)
        //{
        //    _baseRepository.Update(Process);
        //}
    }
}