﻿using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.Process.ProcessModel
{
    [Table("ProcessResources")]
    public class ProcessResourcesData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string FileName { get; set; }

        public Guid ProcessId { get; set; }

        public string FilePath { get; set; }

        public string ExternalSystemId { get; set; }
    }
}
