﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.RequirementTypesModel
{
    [Table("RequirementTypes")]
    public class RequirementTypeData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Code { get; set; }
    }
}