﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.RequirementTypesModel.Configurations
{
    internal class ConfigRequirementType : SenseAIEntityTypeConfiguration<RequirementTypeData>
    {
        public override void Configure(EntityTypeBuilder<RequirementTypeData> builder)
        {
            builder.ToTable(typeof(RequirementTypeData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Code).HasMaxLength(300);

            base.Configure(builder);
        }
    }
}