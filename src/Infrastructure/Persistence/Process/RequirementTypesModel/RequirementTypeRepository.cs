﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.RequirementTypesModel;

namespace Persistence.Process.RequirementTypesModel
{
    public sealed class RequirementTypeRepository : IRequirementTypeRepository
    {
        private readonly IBaseRepository<RequirementType, RequirementTypeData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public RequirementTypeRepository(IBaseRepository<RequirementType, RequirementTypeData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(RequirementType requirementType)
        {
            _baseRepository.Add(requirementType);
        }

        public void Delete(Guid id, bool forceDelete)
        {
            var requirementTypeResult = _context.Set<RequirementTypeData>().AsNoTracking().FirstOrDefault(x => x.Id == id);

            if (requirementTypeResult is null)
            {
                throw new Exception($"RequirementType with Id: {id} doesn't exist!");
            }

            var processes = (from process in _context.Set<ProcessData>().AsNoTracking()
                             where process.RequirementTypeId == id
                             select process).ToList();

            if (forceDelete)
            {
                foreach (var item in processes)
                {
                    _context.Set<ProcessData>().Remove(item);
                    _context.SaveChanges();
                }

                _baseRepository.Delete(id);
            }
            else
            {
                if (processes.Any())
                {
                    throw new Exception($"Cannot delete RequirementType in use by existing Processes!");
                }

                _baseRepository.Delete(id);
            }
        }

        public void Update(RequirementType requirementType)
        {
            var item = _context.Set<RequirementTypeData>().FirstOrDefault(i => i.Id == requirementType.Id);

            if (item is null)
            {
                throw new Exception($"RequirementType with Id: {requirementType.Id} doesn't exist!");

            }

            item.SubProjectId = requirementType.SubProjectId;
            item.Title = requirementType.Title;
            item.Code = requirementType.Code;

            _context.SaveChanges();
        }
    }
}