﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.RequirementPrioritiesModel
{
    [Table("RequirementPriorities")]
    public class RequirementPriorityData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Code { get; set; }
        public int Index { get; set; }
    }
}