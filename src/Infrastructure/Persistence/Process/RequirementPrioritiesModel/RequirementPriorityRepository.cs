﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.RequirementPrioritiesModel;

namespace Persistence.Process.RequirementPrioritiesModel
{
    public sealed class RequirementTypeRepository : IRequirementPriorityRepository
    {
        private readonly IBaseRepository<RequirementPriority, RequirementPriorityData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public RequirementTypeRepository(IBaseRepository<RequirementPriority, RequirementPriorityData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(RequirementPriority requirementPriority)
        {
            _baseRepository.Add(requirementPriority);
        }

        public void Delete(Guid id, bool forceDelete)
        {
            var requirementPriorityResult = _context.Set<RequirementPriorityData>().AsNoTracking().FirstOrDefault(x => x.Id == id);

            if (requirementPriorityResult is null)
            {
                throw new Exception($"RequirementPriority with Id: {id} doesn't exist!");
            }

            var processes = (from process in _context.Set<ProcessData>().AsNoTracking()
                             where process.RequirementPriorityId == id
                             select process).ToList();

            if (forceDelete)
            {
                foreach (var item in processes)
                {
                    _context.Set<ProcessData>().Remove(item);
                    _context.SaveChanges();
                }

                _baseRepository.Delete(id);
            }
            else
            {
                if (processes.Any())
                {
                    throw new Exception($"Cannot delete RequirementPriority in use by existing Processes!");
                }

                _baseRepository.Delete(id);
            }
        }

        public void Update(RequirementPriority requirementPriority)
        {
            var item = _context.Set<RequirementPriorityData>().FirstOrDefault(i => i.Id == requirementPriority.Id);

            if (item is null)
            {
                throw new Exception($"RequirementType with Id: {requirementPriority.Id} doesn't exist!");

            }

            item.SubProjectId = requirementPriority.SubProjectId;
            item.Title = requirementPriority.Title;
            item.Code = requirementPriority.Code;
            _context.SaveChanges();
        }
        public void UpdateIndex(RequirementPriority requirementPriority)
        {
            var item = _context.Set<RequirementPriorityData>().FirstOrDefault(i => i.Id == requirementPriority.Id);

            if (item is null)
            {
                throw new Exception($"RequirementType with Id: {requirementPriority.Id} doesn't exist!");

            }

           
            item.Index = requirementPriority.Index;
            _context.SaveChanges();
        }
    }
}