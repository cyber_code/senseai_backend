﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.RequirementPrioritiesModel.Configurations
{
    internal class ConfigRequirementPriority : SenseAIEntityTypeConfiguration<RequirementPriorityData>
    {
        public override void Configure(EntityTypeBuilder<RequirementPriorityData> builder)
        {
            builder.ToTable(typeof(RequirementPriorityData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Code).HasMaxLength(300);

            base.Configure(builder);
        }
    }
}