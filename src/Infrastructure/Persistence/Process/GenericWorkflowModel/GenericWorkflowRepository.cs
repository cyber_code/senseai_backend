﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.Process.GenericWorkflowModel;

namespace Persistence.Process.GenericWorkflowModel
{
    public sealed class GenericWorkflowRepository : IGenericWorkflowRepository
    {
        private readonly IBaseRepository<GenericWorkflow, GenericWorkflowData> _baseRepository;

        private readonly SenseAIObjectContext _context;

        public GenericWorkflowRepository(IBaseRepository<GenericWorkflow, GenericWorkflowData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(GenericWorkflow genericWorkflow)
        {
            _baseRepository.Add(genericWorkflow);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public GenericWorkflow GetGenericWorkflow(Guid id)
        {
            var genericWorkflowData = _context.Set<GenericWorkflowData>().AsNoTracking().FirstOrDefault(w => w.Id == id);
            return new GenericWorkflow(genericWorkflowData.Id, genericWorkflowData.ProcessId, genericWorkflowData.Title, genericWorkflowData.Description, genericWorkflowData.DesignerJson);
        }

        public void Save(GenericWorkflow genericWorkflow, string designerJson)
        {
            var genericWorkflowData = _context.Set<GenericWorkflowData>().AsNoTracking()
              .FirstOrDefault(w => w.Id == genericWorkflow.Id);
            genericWorkflowData.DesignerJson = designerJson;
            _context.Set<GenericWorkflowData>().Update(genericWorkflowData);

            //delete genericworkflow items
            var genericWorkflowItems = _context.Set<GenericWorkflowItemData>().Where(w => w.GenericWorkflowId == genericWorkflow.Id).AsNoTracking();
            _context.Set<GenericWorkflowItemData>().RemoveRange(genericWorkflowItems);

            foreach (var item in genericWorkflow.Items)
            {
                var genericWorkflowItemData = new GenericWorkflowItemData
                {
                    Id = Guid.NewGuid(),
                    GenericWorkflowId = genericWorkflow.Id,
                    Type = (int)item.Type,
                    Title = item.Title,
                    Description = item.Description,
                };

                _context.Add(genericWorkflowItemData);
            }
            _context.SaveChanges();
        }

        public void Issue(Guid id, string genericWorkflowImage)
        {
            var genericWorkflowData = _context.Set<GenericWorkflowData>().AsNoTracking().FirstOrDefault(w => w.Id == id);
            var lastGenericWorkflowVersionData = _context.Set<GenericWorkflowVersionData>().FirstOrDefault(w => w.GenericWorkflowId == id && w.IsLast);

            int versionId = 0;
            if (lastGenericWorkflowVersionData != null)
            {
                lastGenericWorkflowVersionData.IsLast = false;
                if (lastGenericWorkflowVersionData != null)
                    versionId = _context.Set<GenericWorkflowVersionData>()
                                        .Where(t => t.GenericWorkflowId == id)
                                        .Max(t => t.VersionId) + 1;
            }
            else
            {
                lastGenericWorkflowVersionData = new GenericWorkflowVersionData { VersionId = versionId };
            }

            var genericWorkflowVersionData = new GenericWorkflowVersionData
            {
                ProcessId = genericWorkflowData.ProcessId,
                GenericWorkflowId = genericWorkflowData.Id,
                VersionId = versionId,
                Title = genericWorkflowData.Title,
                Description = genericWorkflowData.Description,
                DesignerJson = genericWorkflowData.DesignerJson,
                WorkflowImage = genericWorkflowImage,
                IsLast = true
            };

            _context.Set<GenericWorkflowVersionData>().Add(genericWorkflowVersionData);
            _context.SaveChanges();

        }

        public void Update(Guid id)
        {
            var genericWorkflowData = _context.Set<GenericWorkflowData>().AsNoTracking().FirstOrDefault(w => w.Id == id);

            var genericWorkflowVersionData = _context.Set<GenericWorkflowVersionData>().FirstOrDefault(w => w.GenericWorkflowId == id && w.IsLast == true);
            genericWorkflowVersionData.DesignerJson = genericWorkflowData.DesignerJson;

            _context.Set<GenericWorkflowVersionData>().Update(genericWorkflowVersionData);
            _context.SaveChanges();
        }

        public void Rename(Guid id, string title)
        {
            var genericWorkflow = _context.Set<GenericWorkflowData>().AsNoTracking().FirstOrDefault(w => w.Id == id);
            if (genericWorkflow != null)
            {
                genericWorkflow.Title = title;
                _context.Set<GenericWorkflowData>().Update(genericWorkflow);
            }

            _context.SaveChanges();
        }

        public string RollbackGenericWorkflow(Guid genericWorkflowId, int versionId)
        {
            var genericWorkflowVersions = _context.Set<GenericWorkflowVersionData>().AsNoTracking().Where(w => w.GenericWorkflowId == genericWorkflowId && w.VersionId != versionId);
            foreach (var item in genericWorkflowVersions)
            {
                item.IsLast = false;
                _context.Set<GenericWorkflowVersionData>().Update(item);
            }

            var genericWorkflowVersionData = _context.Set<GenericWorkflowVersionData>().AsNoTracking()
                .FirstOrDefault(w => w.GenericWorkflowId == genericWorkflowId && w.VersionId == versionId);
            if (genericWorkflowVersionData == null)
                throw new Exception("The generic workflow does not have any version.");
            genericWorkflowVersionData.IsLast = true;

            _context.Set<GenericWorkflowVersionData>().Update(genericWorkflowVersionData);

            //delete generic workflow items
            var genericWorkflowItems = _context.Set<GenericWorkflowItemData>().Where(w => w.GenericWorkflowId == genericWorkflowId);
            _context.Set<GenericWorkflowItemData>().RemoveRange(genericWorkflowItems);

            //get workflowdomain
            var genericWorkflow = GetGenericWorkflowByVersion(genericWorkflowId, versionId);
                                  
            //update workflow
            var genericWorkflowData = _context.Set<GenericWorkflowData>().AsNoTracking().FirstOrDefault(w=> w.Id == genericWorkflowId);
            genericWorkflowData.DesignerJson = genericWorkflowVersionData.DesignerJson;
            var checkGenericWorkflowData = _context.Set<GenericWorkflowData>().Local.FirstOrDefault(w => w.Id == genericWorkflowId);
            if (checkGenericWorkflowData != null)
            {
                checkGenericWorkflowData.DesignerJson = genericWorkflowVersionData.DesignerJson;
            }
            else
                _context.Set<GenericWorkflowData>().Update(genericWorkflowData);

            //add workflow items
            foreach (var item in genericWorkflow.Items)
            {
                var workflowItemData = new GenericWorkflowItemData
                {
                    GenericWorkflowId = genericWorkflow.Id,
                    Id = Guid.NewGuid(),
                    Title = item.Title,
                    Description = item.Description,
                    Type = (int)item.Type
                };
                _context.Add(workflowItemData);
            }

            _context.SaveChanges();

            return genericWorkflowVersionData.DesignerJson;
        }

        public GenericWorkflow GetGenericWorkflowByVersion(Guid workflowId, int versionId)
        {
            var genericWorkflowVersionData = _context.Set<GenericWorkflowVersionData>().FirstOrDefault(w => w.GenericWorkflowId == workflowId && w.VersionId == versionId);
            GenericWorkflow genericWorkflow = new GenericWorkflow(genericWorkflowVersionData.GenericWorkflowId, genericWorkflowVersionData.ProcessId, genericWorkflowVersionData.Title,
                genericWorkflowVersionData.Description, genericWorkflowVersionData.DesignerJson);
            if (!string.IsNullOrEmpty(genericWorkflowVersionData.DesignerJson))
            {
                return GenericWorkflowFactory.Create(genericWorkflowVersionData.GenericWorkflowId, genericWorkflowVersionData.DesignerJson, genericWorkflowVersionData.ProcessId);
            }
            return genericWorkflow;
        }

        public int GetNrVersions(Guid genericWorkflowId)
        {
            return _context.Set<GenericWorkflowVersionData>().AsNoTracking().Where(w => w.GenericWorkflowId == genericWorkflowId).Count();
        }

        public string GetGenericWorkflowImage(Guid genericWorkflowId, int versionId)
        {
           return _context.Set<GenericWorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.GenericWorkflowId == genericWorkflowId &&
                                                                                        w.VersionId == versionId).WorkflowImage;
        }
    }
}
