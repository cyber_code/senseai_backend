﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.GenericWorkflowModel.Configurations
{
    internal class ConfigGenericWorkflow : SenseAIEntityTypeConfiguration<GenericWorkflowData>
    {
        public override void Configure(EntityTypeBuilder<GenericWorkflowData> builder)
        {
            builder.ToTable(typeof(GenericWorkflowData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ProcessId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(500);

            base.Configure(builder);
        }
    }
}
