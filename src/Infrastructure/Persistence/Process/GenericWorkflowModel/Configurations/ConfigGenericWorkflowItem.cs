﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.GenericWorkflowModel.Configurations
{
    internal class ConfigGenericWorkflowItem : SenseAIEntityTypeConfiguration<GenericWorkflowItemData>
    {
        public override void Configure(EntityTypeBuilder<GenericWorkflowItemData> builder)
        {
            builder.ToTable(typeof(GenericWorkflowItemData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.GenericWorkflowId);
            builder.Property(x => x.Title).HasMaxLength(300);
            builder.Property(x => x.Description).HasMaxLength(500);

            base.Configure(builder);
        }
    }
}
