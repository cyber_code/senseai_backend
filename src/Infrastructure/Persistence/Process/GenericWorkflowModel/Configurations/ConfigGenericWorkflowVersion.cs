﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.GenericWorkflowModel.Configurations
{
    internal class ConfigGenericWorkflowVersion : SenseAIEntityTypeConfiguration<GenericWorkflowVersionData>
    {
        public override void Configure(EntityTypeBuilder<GenericWorkflowVersionData> builder)
        {
            builder.ToTable(typeof(GenericWorkflowVersionData).Name);

            builder.HasKey(t => new { t.GenericWorkflowId, t.VersionId });

            builder.Property(e => e.VersionId);

            builder.Property(t => t.Title).IsRequired();

            base.Configure(builder);
        }
    }
}
