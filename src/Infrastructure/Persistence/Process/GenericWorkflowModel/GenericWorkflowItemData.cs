﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.GenericWorkflowModel
{
    [Table("GenericWorkflowItems")]
    public class GenericWorkflowItemData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid GenericWorkflowId { get; set; } 

        public int Type { get; set; }

        public string Title { get; set; }

        public string Description { get; set; } 
    }
}
