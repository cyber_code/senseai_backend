﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.Process.IssuesModel;

namespace Persistence.Process.IssuesModel
{
    public sealed class IssueTypeFieldsRepository : IIssueTypeFieldsRepository
    {

        private readonly IBaseRepository<IssueTypeFields, IssueTypeFieldsData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public IssueTypeFieldsRepository(IBaseRepository<IssueTypeFields, IssueTypeFieldsData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;

        }

        public void Add(IssueTypeFields issueTypeFields)
        {
            _baseRepository.Add(issueTypeFields);
        }

        public bool Delete(Guid id)
        {
            var issueTypeResult = _context.Set<IssueTypeFieldsData>().AsNoTracking().FirstOrDefault(x => x.Id == id);

            if (issueTypeResult is null)
            {
                throw new Exception($"IssueTypeFields with Id: {id} doesn't exists!");
            }


            _baseRepository.Delete(id);

            return true;

        }

        public void Update(IssueTypeFields issueTypeFields)
        {
            var item = _context.Set<IssueTypeFieldsData>().FirstOrDefault(i => i.Id == issueTypeFields.Id);

            if (item == null)
                throw new Exception("This IssueTypeFields does not exists");
            _baseRepository.Update(issueTypeFields);


        }
    }
}