﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Persistence.Internal;
using SenseAI.Domain;

namespace Persistence.Process.IssuesModel
{
    [Table("Issues")]
    public class IssueData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid ProcessId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid IssueTypeId { get; set; }

        public Guid AssignedId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public StatusType Status { get; set; }

        public int Estimate { get; set; }

        public string Reason { get; set; }

        public string Implication { get; set; }

        public string Comment { get; set; }

        public Guid TypicalId { get; set; }

        public string Attribute { get; set; }

        public bool ApprovedForInvestigation { get; set; }
        public InvestigationStatus InvestigationStatus { get; set; }

        public ChangeStatus ChangeStatus { get; set; }

        public Guid ReporterId { get; set; }

        public Guid ApprovedId { get; set; }

        public DateTime DateSubmitted { get; set; }

        public DateTime ApprovalDate { get; set; }

        public DateTime StartDate { get; set; }

        public IssuePriority Priority { get; set; }
        public IssueSeverity Severity { get; set; }
        public DateTime DueDate { get; set; }
        public TypeDefect Type { get; set; }
        public Guid ComponentId { get; set; }
        public string ExternalSystemId { get; set; }
        public string TestCaseId { get; set; }
    }
}