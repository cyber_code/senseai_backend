﻿using Persistence.Internal;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Process.IssuesModel
{
    public sealed class IssueCommentRepository : IIssueCommentRepository
    {
        private readonly IBaseRepository<IssueComment, IssueCommentData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public IssueCommentRepository(IBaseRepository<IssueComment, IssueCommentData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(IssueComment issueComment)
        {
            _baseRepository.Add(issueComment);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(IssueComment issueComment)
        {
            _baseRepository.Update(issueComment);
        }
    }
}
