﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Persistence.Internal;

namespace Persistence.Process.IssuesModel
{
    [Table("IssueResources")]
    public class IssueResourceData : BaseData, IData
    {
        public Guid Id { get; set; }
        public Guid IssueId { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }
        public string ExternalSystemId { get; set; }
    }
}