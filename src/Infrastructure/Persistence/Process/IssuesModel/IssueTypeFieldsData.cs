﻿using SenseAI.Domain;
using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.IssuesModel
{
    [Table("IssueTypeFields")]
    public class IssueTypeFieldsData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid IssueTypeId { get; set; }
        public Guid IssueTypeFieldsNameId { get; set; }

        public bool Checked { get; set; }
        public virtual IssueTypeData IssueType { get; set; }
        public virtual IssueTypeFieldsNameData IssueTypeFieldsName { get; set; }
    }
}