﻿using Persistence.Internal;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Process.IssuesModel
{
    public sealed class IssueResourceRepository : IIssueResourceRepository
    {
        private readonly IBaseRepository<IssueResource, IssueResourceData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public IssueResourceRepository(IBaseRepository<IssueResource, IssueResourceData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(IssueResource issueResource)
        {
            _baseRepository.Add(issueResource);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(IssueResource issueResource)
        {
            _baseRepository.Update(issueResource);
        }
    }
}
