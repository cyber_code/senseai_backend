﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.Process.IssuesModel;

namespace Persistence.Process.IssuesModel
{
    public sealed class IssueLinkRepository : IIssueLinkRepository
    {
        private readonly IBaseRepository<IssueLink, IssueLinkData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public IssueLinkRepository(IBaseRepository<IssueLink, IssueLinkData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(IssueLink issueLink)
        {
            var issueLinkResult = from il in _context.Set<IssueLinkData>().AsNoTracking()
                                  where il.SrcIssueId == issueLink.SrcIssueId &&
                                        il.LinkType == issueLink.LinkType &&
                                        il.DstIssueId == issueLink.DstIssueId
                                  select new { il.Id };
            if(issueLinkResult.Any())
            {
                throw new Exception($"An Issue Link of type {nameof(issueLink.LinkType)} from SrcIssueId={issueLink.SrcIssueId} to DstIssueId={issueLink.DstIssueId} already exists!");
            }
            _baseRepository.Add(issueLink);
        }

        public void Delete(Guid id)
        {
            var issueLinkResult = _context.Set<IssueLinkData>().AsNoTracking().FirstOrDefault(x => x.Id == id);

            if (issueLinkResult is null)
            {
                throw new Exception($"This IssueLink does not exist!");
            }

            _baseRepository.Delete(id);
        }

        public void Update(IssueLink issueLink)
        {
            var item = _context.Set<IssueLinkData>().FirstOrDefault(i => i.Id == issueLink.Id);

            if (item == null)
                throw new Exception("This IssueLink does not exist");

            var issueLinkResult = from il in _context.Set<IssueLinkData>().AsNoTracking()
                                  where il.SrcIssueId == issueLink.SrcIssueId &&
                                        il.LinkType == issueLink.LinkType &&
                                        il.DstIssueId == issueLink.DstIssueId
                                  select new { il.Id };

            if(issueLinkResult.Any())
            {
                throw new Exception($"An Issue Link of type {nameof(issueLink.LinkType)} from SrcIssueId={issueLink.SrcIssueId} to DstIssueId={issueLink.DstIssueId} already exists!");
            }

            _baseRepository.Update(issueLink);
        }
    }
}