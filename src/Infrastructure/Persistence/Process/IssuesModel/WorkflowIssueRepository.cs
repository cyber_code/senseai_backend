﻿using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.Process.IssuesModel
{
    public sealed class WorkflowIssueRepository : IWorkflowIssueRepository
    {
        private readonly IBaseRepository<WorkflowIssue, WorkflowIssueData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public WorkflowIssueRepository(IBaseRepository<WorkflowIssue, WorkflowIssueData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(WorkflowIssue workflowIssue)
        {
            _baseRepository.Add(workflowIssue);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(WorkflowIssue workflowIssue)
        {
            _baseRepository.Update(workflowIssue);
        }
    }
}
