﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.Process.IssuesModel;

namespace Persistence.Process.IssuesModel
{
    public sealed class IssueRepository : IIssueRepository
    {
        private readonly IBaseRepository<Issue, IssueData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public IssueRepository(IBaseRepository<Issue, IssueData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(Issue issueType)
        {
            _baseRepository.Add(issueType);
        }

        public void Delete(Guid id)
        {
            var issueResult = _context.Set<IssueData>().AsNoTracking().FirstOrDefault(x => x.Id == id);

            if (issueResult is null)
            {
                throw new Exception($"Issue with Id: {id} doesn't exists!");
            }

            var issueResources = (from issue in _context.Set<IssueData>().AsNoTracking()
                                  join issueResource in _context.Set<IssueResourceData>().AsNoTracking()
                                      on issue.Id equals issueResource.IssueId
                                  where issue.Id == id
                                  select issueResource).ToList();

            foreach (var item in issueResources)
            {
                _context.Set<IssueResourceData>().Remove(item);
                _context.SaveChanges();
            }

            var issueComments = (from issue in _context.Set<IssueData>().AsNoTracking()
                                 join issueComment in _context.Set<IssueCommentData>().AsNoTracking()
                                     on issue.Id equals issueComment.IssueId
                                 where issue.Id == id
                                 select issueComment).ToList();

            foreach (var item in issueComments)
            {
                _context.Set<IssueCommentData>().Remove(item);
                _context.SaveChanges();
            }

            _baseRepository.Delete(id);
        }

        public void Update(Issue issue)
        {
            var data = _baseRepository.Map(issue);

            _context.Set<IssueData>().Attach(data);
            var entry = _context.Entry(data);
            entry.State = EntityState.Modified;
            entry.Property(property => property.ExternalSystemId).IsModified = false;

            _context.SaveChanges();
        }
    }
}