﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Persistence.Internal;

namespace Persistence.Process.IssuesModel
{
    [Table("IssueComments")]
    public class IssueCommentData : BaseData, IData
    {
        public Guid Id { get; set; }
        public Guid IssueId { get; set; }
        public string Comment { get; set; }
        public Guid CommenterId { get; set; }
        public Guid ParentId { get; set; }

    }
}