﻿using SenseAI.Domain;
using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Persistence.Process.IssuesModel
{
    [Table("IssueTypes")]
    public class IssueTypeData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Unit { get;  set; }
        public bool Default { get;  set; }
        public virtual IReadOnlyCollection<IssueTypeFieldsData>  IssueTypeFields { get; set; }
    }
}