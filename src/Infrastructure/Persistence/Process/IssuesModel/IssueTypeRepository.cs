﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.Process.IssuesModel;

namespace Persistence.Process.IssuesModel
{
    public sealed class IssueTypeRepository : IIssueTypeRepository
    {
        private readonly IBaseRepository<IssueType, IssueTypeData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public IssueTypeRepository(IBaseRepository<IssueType, IssueTypeData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(IssueType issueType)
        {
            _baseRepository.Add(issueType);
        }

        public bool Delete(Guid id, bool forceDelete,bool removedefault)
        {
            var issueTypeResult = _context.Set<IssueTypeData>().AsNoTracking().FirstOrDefault(x => x.Id == id);

            if (issueTypeResult is null)
            {
                throw new Exception($"IssueType with Id: {id} doesn't exists!");
            }
            if(issueTypeResult.Default &&!removedefault)
            {
                throw new Exception($"Default Issue Type can not be deleted!");
            }

            var issues = (from issueType in _context.Set<IssueTypeData>().AsNoTracking()
                          join issue in _context.Set<IssueData>().AsNoTracking()
                              on issueType.Id equals issue.IssueTypeId
                          where issueType.Id == id
                          select issue).ToList();

            if (forceDelete)
            {
                foreach (var item in issues)
                {
                    _context.Set<IssueData>().Remove(item);
                    _context.SaveChanges();
                }

                _baseRepository.Delete(id);

                return true;
            }
            else
            {
                /*
                 * When ForceDelete value is false, IssueType can only be deleted removed if it
                 * doesn't contain any Issue. Otherwise, the delete operation does not occur.
                 */

                if (issues.Any())
                {
                    return false;
                }

                _baseRepository.Delete(id);

                return true;
            }
        }

        public void Update(IssueType issueType)
        {
            var item = _context.Set<IssueTypeData>().FirstOrDefault(i => i.Id == issueType.Id);

            if (item == null)
                throw new Exception("This IssueType does not exists");
            _baseRepository.Update(issueType);

        
        }
    }
}