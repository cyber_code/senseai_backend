﻿using SenseAI.Domain;
using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.IssuesModel
{
    [Table("WorkflowIssues")]
    public class WorkflowIssueData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid WorkflowId { get; set; }

        public Guid IssueId { get; set; }
    }
}