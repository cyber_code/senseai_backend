﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;
using System;

namespace Persistence.Process.IssuesModel.Configurations
{
    internal class ConfigIssueType : SenseAIEntityTypeConfiguration<IssueTypeData>
    {
        public override void Configure(EntityTypeBuilder<IssueTypeData> builder)
        {
            builder.ToTable(typeof(IssueTypeData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(500);


           // IssueTypeData[] data = { new IssueTypeData { Id = new  Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), Title = "Defect", Description = "Defect", Unit = "Story points", Default = true, SubProjectId = new Guid("3adf6db6-b640-439d-b6d3-1aaa9d9fd626") },
           //     new IssueTypeData { Id = new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), Title = "Business CR", Description = "Business Change Request", Unit = "hours", Default = true, SubProjectId = new Guid("3adf6db6-b640-439d-b6d3-1aaa9d9fd626") },
           //        new IssueTypeData { Id = new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), Title = "Technical CR", Description = "Technical Change Request", Unit = "hours", Default = true, SubProjectId = new Guid("3adf6db6-b640-439d-b6d3-1aaa9d9fd626") },
           //         new IssueTypeData { Id = new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), Title = "Task QA", Description = "Task QA", Unit = "Days", Default = true, SubProjectId = new Guid("3adf6db6-b640-439d-b6d3-1aaa9d9fd626") },

           //        new IssueTypeData { Id =new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), Title = "Defect", Description = "Defect", Unit = "Story points", Default = true, SubProjectId = new Guid("a70288d7-94d5-4aa0-8df5-df79c65bb4bc") },
           //     new IssueTypeData { Id = new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), Title = "Business CR", Description = "Business Change Request", Unit = "hours", Default = true, SubProjectId = new Guid("a70288d7-94d5-4aa0-8df5-df79c65bb4bc") },
           //        new IssueTypeData { Id = new Guid("4a3a403a-a5f3-422d-ae5d-926570f8eb78"), Title = "Technical CR", Description = "Technical Change Request", Unit = "hours", Default = true, SubProjectId = new Guid("a70288d7-94d5-4aa0-8df5-df79c65bb4bc"),},
           //          new IssueTypeData { Id = new Guid("d5f939b3-441b-4529-b123-65d361425dae"), Title = "Task QA", Description = "Task QA", Unit = "Days", Default = true, SubProjectId = new Guid("a70288d7-94d5-4aa0-8df5-df79c65bb4bc") }
           // };


           //builder.HasData(data);
            base.Configure(builder);
        }
    }
}