﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;
using System;

namespace Persistence.Process.IssuesModel.Configurations
{
    internal class ConfigIssueTypeFieldsName : SenseAIEntityTypeConfiguration<IssueTypeFieldsNameData>
    {
        public override void Configure(EntityTypeBuilder<IssueTypeFieldsNameData> builder)
        {
            builder.ToTable(typeof(IssueTypeFieldsNameData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(300).IsRequired();
            //IssueTypeFieldsNameData[] data = {
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Status" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Estimate" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Reason" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Implication" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Comment" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Application" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Attribute" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Approved For Investigation" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Investigation Status" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Change Status" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Identified By" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Approved By" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Date Submitted" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Approval Date" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Start Date" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Severity" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Priority" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Target Date" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Workflow" },
            //    new IssueTypeFieldsNameData { Id = Guid.NewGuid(), Name = "Defect Type" },

            //};
            //builder.HasData(data);

            base.Configure(builder);
        }
    }
}