﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.IssuesModel.Configurations
{
    internal class ConfigIssueResource : SenseAIEntityTypeConfiguration<IssueResourceData>
    {
        public override void Configure(EntityTypeBuilder<IssueResourceData> builder)
        {
            builder.ToTable(typeof(IssueResourceData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.IssueId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Path).HasMaxLength(300).IsRequired();

            base.Configure(builder);
        }
    }
}