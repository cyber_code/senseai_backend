﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.IssuesModel.Configurations
{
    internal class ConfigIssue : SenseAIEntityTypeConfiguration<IssueData>
    {
        public override void Configure(EntityTypeBuilder<IssueData> builder)
        {
            builder.ToTable(typeof(IssueData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.ProcessId).IsRequired();
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.IssueTypeId).IsRequired();
            builder.Property(x => x.AssignedId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(500);
            builder.Property(x => x.Status).IsRequired();

            base.Configure(builder);
        }
    }
}