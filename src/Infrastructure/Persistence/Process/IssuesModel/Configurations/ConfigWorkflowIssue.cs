﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.IssuesModel.Configurations
{
    internal class ConfigWorkflowIssue : SenseAIEntityTypeConfiguration<WorkflowIssueData>
    {
        public override void Configure(EntityTypeBuilder<WorkflowIssueData> workflowIssueConfig)
        {
            workflowIssueConfig.ToTable(typeof(WorkflowIssueData).Name);
            workflowIssueConfig.HasKey(i => i.Id);
            workflowIssueConfig.Property(i => i.WorkflowId).IsRequired();
            workflowIssueConfig.Property(i => i.IssueId).IsRequired();

            base.Configure(workflowIssueConfig);
        }
    }
}