﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;
using System;

namespace Persistence.Process.IssuesModel.Configurations
{
    internal class ConfigIssueTypeFields : SenseAIEntityTypeConfiguration<IssueTypeFieldsData>
    {
        public override void Configure(EntityTypeBuilder<IssueTypeFieldsData> builder)
        {
            builder.ToTable(typeof(IssueTypeFieldsData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.IssueTypeId).IsRequired();
            builder.Property(x => x.IssueTypeFieldsNameId).IsRequired();
            builder.HasOne(spd => spd.IssueType)
              .WithMany(pd => pd.IssueTypeFields)
              .HasForeignKey(spd => spd.IssueTypeId);
            builder.HasOne(spd => spd.IssueTypeFieldsName)
            .WithMany(pd => pd.IssueTypeFields)
            .HasForeignKey(spd => spd.IssueTypeFieldsNameId);

            #region addData
           // IssueTypeFieldsData[] data = { 
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("6397c987-631e-4aaa-8ecc-c370807106be"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("0e2ed1e7-7fd7-41b0-9505-91f7b749744a"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("68c49518-35b2-4691-9524-95be5ba93723"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("a59b3bd6-b3b7-49d4-80d5-e33f40f51599"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("ada7c874-1922-44ab-981d-adf596beece2"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("27707e4e-fdae-4372-9f9d-28b72ca768d1"), IssueTypeFieldsNameId= new Guid("401518ad-59a4-4607-b53c-61a232adca62"), Checked =true},

            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("6397c987-631e-4aaa-8ecc-c370807106be"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("0e2ed1e7-7fd7-41b0-9505-91f7b749744a"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("68c49518-35b2-4691-9524-95be5ba93723"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("a59b3bd6-b3b7-49d4-80d5-e33f40f51599"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("ada7c874-1922-44ab-981d-adf596beece2"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("1c627f25-2eb0-4f98-adb6-75a412193643"), IssueTypeFieldsNameId= new Guid("401518ad-59a4-4607-b53c-61a232adca62"), Checked =true},

            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("6397c987-631e-4aaa-8ecc-c370807106be"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("0e2ed1e7-7fd7-41b0-9505-91f7b749744a"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("68c49518-35b2-4691-9524-95be5ba93723"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("a59b3bd6-b3b7-49d4-80d5-e33f40f51599"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("ada7c874-1922-44ab-981d-adf596beece2"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d90c533d-75bd-4e69-b0e9-2e7895ddff66"), IssueTypeFieldsNameId= new Guid("401518ad-59a4-4607-b53c-61a232adca62"), Checked =false},

            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("6397c987-631e-4aaa-8ecc-c370807106be"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("0e2ed1e7-7fd7-41b0-9505-91f7b749744a"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("68c49518-35b2-4691-9524-95be5ba93723"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("a59b3bd6-b3b7-49d4-80d5-e33f40f51599"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("ada7c874-1922-44ab-981d-adf596beece2"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("41d8632c-a6c7-4df7-a01c-bed5a45d0718"), IssueTypeFieldsNameId= new Guid("401518ad-59a4-4607-b53c-61a232adca62"), Checked =false},

            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("6397c987-631e-4aaa-8ecc-c370807106be"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("0e2ed1e7-7fd7-41b0-9505-91f7b749744a"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("68c49518-35b2-4691-9524-95be5ba93723"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("a59b3bd6-b3b7-49d4-80d5-e33f40f51599"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("ada7c874-1922-44ab-981d-adf596beece2"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("401518ad-59a4-4607-b53c-61a232adca62"), Checked =false},

            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("6397c987-631e-4aaa-8ecc-c370807106be"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("0e2ed1e7-7fd7-41b0-9505-91f7b749744a"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("68c49518-35b2-4691-9524-95be5ba93723"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("a59b3bd6-b3b7-49d4-80d5-e33f40f51599"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("ada7c874-1922-44ab-981d-adf596beece2"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("dc83ecd4-ce1a-48e9-bc85-cac51406fa11"), IssueTypeFieldsNameId= new Guid("401518ad-59a4-4607-b53c-61a232adca62"), Checked =false},


            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("6397c987-631e-4aaa-8ecc-c370807106be"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("0e2ed1e7-7fd7-41b0-9505-91f7b749744a"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("68c49518-35b2-4691-9524-95be5ba93723"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("a59b3bd6-b3b7-49d4-80d5-e33f40f51599"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("ada7c874-1922-44ab-981d-adf596beece2"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d498805a-a2bc-4a51-9784-da56b0828464"), IssueTypeFieldsNameId= new Guid("401518ad-59a4-4607-b53c-61a232adca62"), Checked =false},


            //     new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("6397c987-631e-4aaa-8ecc-c370807106be"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("0e2ed1e7-7fd7-41b0-9505-91f7b749744a"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("68c49518-35b2-4691-9524-95be5ba93723"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("a59b3bd6-b3b7-49d4-80d5-e33f40f51599"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("ada7c874-1922-44ab-981d-adf596beece2"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), Checked =true},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), Checked =false},
            //    new IssueTypeFieldsData{ Id= Guid.NewGuid(), IssueTypeId=new Guid("d5f939b3-441b-4529-b123-65d361425dae"), IssueTypeFieldsNameId= new Guid("401518ad-59a4-4607-b53c-61a232adca62"), Checked =false},
            //};
            //builder.HasData(data);
            #endregion
            base.Configure(builder);
        }
    }
}