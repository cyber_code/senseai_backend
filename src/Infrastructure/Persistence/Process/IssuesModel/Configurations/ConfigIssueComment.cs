﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.IssuesModel.Configurations
{
    internal class ConfigIssueComment : SenseAIEntityTypeConfiguration<IssueCommentData>
    {
        public override void Configure(EntityTypeBuilder<IssueCommentData> builder)
        {
            builder.ToTable(typeof(IssueCommentData).Name);

            builder.HasKey(i => i.Id);
            builder.Property(i => i.IssueId).IsRequired();
            builder.Property(i => i.Comment).HasMaxLength(300).IsRequired();
            builder.Property(i => i.CommenterId).IsRequired();
            builder.Property(i => i.ParentId).IsRequired();

            base.Configure(builder);
        }
    }
}