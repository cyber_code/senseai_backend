﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.IssuesModel.Configurations
{
    internal class ConfigIssueLink : SenseAIEntityTypeConfiguration<IssueLinkData>
    {
        public override void Configure(EntityTypeBuilder<IssueLinkData> builder)
        {
            builder.ToTable(typeof(IssueLinkData).Name);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.SrcIssueId).IsRequired();
            builder.Property(x => x.LinkType).IsRequired();
            builder.Property(x => x.DstIssueId).IsRequired();

            base.Configure(builder);
        }
    }
}