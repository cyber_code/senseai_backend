﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Persistence.Internal;
using SenseAI.Domain;

namespace Persistence.Process.IssuesModel
{
    [Table("IssueLink")]
    public class IssueLinkData : BaseData, IData
    {
        public Guid Id { get; set; }
        public Guid SrcIssueId { get; set; }
        public IssueLinkType LinkType { get; set; }
        public Guid DstIssueId { get; set; }
    }
}