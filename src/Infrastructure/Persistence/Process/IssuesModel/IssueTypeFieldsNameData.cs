﻿using SenseAI.Domain;
using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Persistence.Process.IssuesModel
{
    [Table("IssueTypeFieldsNames")]
    public class IssueTypeFieldsNameData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public virtual IReadOnlyCollection<IssueTypeFieldsData> IssueTypeFields { get; set; }
    }
}