﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.Process.SprintsModel.Configurations
{
    internal class ConfigSprintIssues : SenseAIEntityTypeConfiguration<SprintIssuesData>
    {
        public override void Configure(EntityTypeBuilder<SprintIssuesData> builder)
        {
            builder.ToTable(typeof(SprintIssuesData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.SprintId).IsRequired();
            builder.Property(x => x.IssueId).IsRequired();
            builder.HasOne(spd => spd.Sprint)
               .WithMany(pd => pd.SprintIssues)
               .HasForeignKey(spd => spd.SprintId);
            base.Configure(builder);
        }
    }
}
