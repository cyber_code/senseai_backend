﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.SprintsModel.Configurations
{
    internal class ConfigSprints : SenseAIEntityTypeConfiguration<SprintsData>
    {
        public override void Configure(EntityTypeBuilder<SprintsData> builder)
        {
            builder.ToTable(typeof(SprintsData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(500);
            builder.Property(t => t.Status).HasComputedColumnSql(" case when start='0001-01-01 00:00:00.0000000' then 0 when(start < GETDATE() and( [end] >= getdate() or[end] = '0001-01-01 00:00:00.0000000'))then 1 when[end] < GETDATE() then 2  end");

            builder.HasOne(spd => spd.SubProject)
           .WithMany(pd => pd.Sprints)
           .HasForeignKey(spd => spd.SubProjectId);
            base.Configure(builder);
        }
    }
}
