﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.SprintsModel
{
    [Table("SprintIssues")]
    public class SprintIssuesData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid SprintId { get; set; }

        public Guid IssueId { get; set; }
        public int Status { get; set; }
        public virtual SprintsData Sprint { get; set; }
    }
}
