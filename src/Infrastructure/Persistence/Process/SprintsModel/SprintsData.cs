﻿using Persistence.AdminModel;
using Persistence.Internal;
using SenseAI.Domain;
using SenseAI.Domain.AdminModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Process.SprintsModel
{
    [Table("Sprints")]
    public class SprintsData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public Guid SubProjectId { get; set; }
        public virtual SubProjectData SubProject { get; set; }
        public virtual IReadOnlyCollection<SprintIssuesData> SprintIssues { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int Status
        {
            get;
            private set;
        }
        public int Duration { get; set; }
        public DurationUnit DurationUnit { get; set; }
        public int OutstandingDefectsCountOnSprintEnd { get; set; }
    }
}
