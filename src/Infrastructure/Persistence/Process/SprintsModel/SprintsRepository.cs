﻿using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.SprintsModel;
using System;
using System.Linq;

namespace Persistence.Process.SprintsModel
{
    public sealed class SprintsRepository : ISprintsRepository
    {
        private readonly IBaseRepository<Sprints, SprintsData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public SprintsRepository(IBaseRepository<Sprints, SprintsData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(Sprints Sprints)
        {
            _baseRepository.Add(Sprints);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(Sprints Sprints)
        {
            _baseRepository.Update(Sprints);
        }
        public void Start(Guid id)
        {
            var item = _context.Set<SprintsData>().FirstOrDefault(i => i.Id == id);

            if (item == null)
                throw new Exception("This sprint does not exists");
            item.Start = DateTime.Now;
            _context.SaveChanges();
        }
        public void End(Guid id)
        {
            var item = _context.Set<SprintsData>().FirstOrDefault(i => i.Id == id);

            if (item == null)
                throw new Exception("This sprint does not exists");
            item.End = DateTime.Now;
            var outstandingDefectsCount = (from issue in _context.Set<IssueData>()
                                           join issueType in _context.Set<IssueTypeData>() on issue.IssueTypeId equals issueType.Id
                                           join sprintIssues in _context.Set<SprintIssuesData>() on issue.Id equals sprintIssues.IssueId
                                           where sprintIssues.SprintId == id &&
                                                 issue.Status != SenseAI.Domain.StatusType.Done &&
                                                 issueType.Title.Equals("Defect") &&
                                                 issueType.Default == true
                                           select issue.Id).Count();
            item.OutstandingDefectsCountOnSprintEnd = outstandingDefectsCount;
            _context.SaveChanges();
        }
    }
}
