﻿using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using SenseAI.Domain.Process.SprintsModel;
using System;
using System.Linq;

namespace Persistence.Process.SprintsModel
{
    public sealed class SprintIssuesRepository : ISprintIssuesRepository
    {
        private readonly IBaseRepository<SprintIssues, SprintIssuesData> _baseRepository;

        public SprintIssuesRepository(IBaseRepository<SprintIssues, SprintIssuesData> baseRepository)
        {
            _baseRepository = baseRepository;

        }

        public void Add(SprintIssues SprintIssues)
        {
            _baseRepository.Add(SprintIssues);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(SprintIssues SprintIssues)
        {
            _baseRepository.Update(SprintIssues);
        }
       
    }
}
