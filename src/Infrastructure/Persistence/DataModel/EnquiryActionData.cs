﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.DataModel
{
    [Table("EnquiryActions")]
    public class EnquiryActionData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid CatalogId { get; set; }

        public string TypicalName  { get; set; }

        public string Actions { get; set; }
    }
}