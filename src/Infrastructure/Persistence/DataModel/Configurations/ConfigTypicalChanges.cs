﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.DataModel.Configurations
{
    internal class ConfigTypicalChanges : SenseAIEntityTypeConfiguration<TypicalChangesData>
    {
        public override void Configure(EntityTypeBuilder<TypicalChangesData> typicalConfig)
        {
            typicalConfig.ToTable(typeof(TypicalChangesData).Name);

            typicalConfig.HasKey(o => o.Id);

            typicalConfig.Property(t => t.Title)
                .IsRequired();

            typicalConfig.Property(t => t.Json)
                .IsRequired(false);
            typicalConfig.Property(t => t.Xml)
               .IsRequired(false);
            typicalConfig.Property(t => t.Type)
               .IsRequired(true);
            typicalConfig.Property(t => t.Status)
              .IsRequired(true);
            base.Configure(typicalConfig);
        }
    }
}