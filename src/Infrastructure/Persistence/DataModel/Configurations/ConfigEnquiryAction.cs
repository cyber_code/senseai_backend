﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.DataModel.Configurations
{
    internal class ConfigEnquiryAction : SenseAIEntityTypeConfiguration<EnquiryActionData>
    {
        public override void Configure(EntityTypeBuilder<EnquiryActionData> enquiryactiondataConfig)
        {
            enquiryactiondataConfig.ToTable(typeof(EnquiryActionData).Name);

            enquiryactiondataConfig.HasKey(o => o.Id);

            enquiryactiondataConfig.Property(t => t.CatalogId).IsRequired();
            enquiryactiondataConfig.Property(t => t.TypicalName).IsRequired();
            enquiryactiondataConfig.Property(t => t.Actions).IsRequired();

            base.Configure(enquiryactiondataConfig);
        }
    }
}