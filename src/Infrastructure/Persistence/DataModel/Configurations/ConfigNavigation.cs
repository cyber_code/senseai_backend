﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.DataModel.Configurations
{
    internal class ConfigNavigation : SenseAIEntityTypeConfiguration<NavigationData>
    {
        public override void Configure(EntityTypeBuilder<NavigationData> navigationConfig)
        {
            navigationConfig.ToTable(typeof(NavigationData).Name);

            navigationConfig.HasKey(o => o.Id);
            navigationConfig.Property(t => t.CatalogId).IsRequired();
            navigationConfig.Property(t => t.Name)
                .IsRequired();

            navigationConfig.Property(t => t.Url)
                .IsRequired(false);

            navigationConfig.Property(t => t.ParentId)
                .IsRequired();

            base.Configure(navigationConfig);
        }
    }
}