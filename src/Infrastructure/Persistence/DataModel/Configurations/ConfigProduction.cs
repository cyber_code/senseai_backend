﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.DataModel.Configurations
{
    internal class ConfigProduction : SenseAIEntityTypeConfiguration<ProductionDataData>
    {
        public override void Configure(EntityTypeBuilder<ProductionDataData> productiondataConfig)
        {
            productiondataConfig.ToTable(typeof(ProductionDataData).Name);

            productiondataConfig.HasKey(o => o.Id);
            productiondataConfig.Property(t => t.CatalogId).IsRequired();
            productiondataConfig.Property(t => t.ApplicationName).HasMaxLength(500).IsRequired();
            productiondataConfig.Property(t => t.Key).HasMaxLength(500).IsRequired();
            productiondataConfig.Property(x => x.Json).HasColumnType("nvarchar(max)").IsRequired();
            productiondataConfig.Property(x => x.Xml).HasColumnType("xml").IsRequired();
            productiondataConfig.Property(x => x.Date).IsRequired();

            base.Configure(productiondataConfig);
        }
    }
}