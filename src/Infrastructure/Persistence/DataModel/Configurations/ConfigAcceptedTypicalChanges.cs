﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.DataModel.Configurations
{
    internal class ConfigAcceptedTypicalChanges : SenseAIEntityTypeConfiguration<AcceptedTypicalChangesData>
    {
        public override void Configure(EntityTypeBuilder<AcceptedTypicalChangesData> typicalConfig)
        {
            typicalConfig.ToTable(typeof(TypicalChangesData).Name);

            typicalConfig.HasKey(o => o.Id);

            typicalConfig.Property(t => t.TypicalId)
                .IsRequired();

            typicalConfig.Property(t => t.NewXml)
                .IsRequired(false);
            typicalConfig.Property(t => t.OldXml)
               .IsRequired(false);
            typicalConfig.Property(t => t.OldXmlHash)
               .IsRequired(true);
            typicalConfig.Property(t => t.NewXmlHash)
              .IsRequired(true);
            base.Configure(typicalConfig);
        }
    }
}