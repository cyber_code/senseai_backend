﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.DataModel.Configurations
{
    internal class ConfigTypical : SenseAIEntityTypeConfiguration<TypicalData>
    {
        public override void Configure(EntityTypeBuilder<TypicalData> typicalConfig)
        {
            typicalConfig.ToTable(typeof(TypicalData).Name);

            typicalConfig.HasKey(o => o.Id);

            typicalConfig.Property(t => t.Title)
                .IsRequired();

            typicalConfig.Property(t => t.Json)
                .IsRequired(false);
            typicalConfig.Property(t => t.Xml)
               .IsRequired(false);
            typicalConfig.Property(t => t.Type)
               .IsRequired(true);
            base.Configure(typicalConfig);
        }
    }
}