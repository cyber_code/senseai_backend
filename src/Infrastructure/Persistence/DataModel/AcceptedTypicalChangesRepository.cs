﻿using SenseAI.Domain.DataModel;
using Persistence.Internal;
using System;
using System.Linq;

namespace Persistence.DataModel
{
    public sealed class AcceptedTypicalChangesRepository : IAcceptedTypicalChangesRepository
    {
        private readonly IBaseRepository<AcceptedTypicalChanges, AcceptedTypicalChangesData> _baseRepository;

        public AcceptedTypicalChangesRepository(IBaseRepository<AcceptedTypicalChanges, AcceptedTypicalChangesData> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public void Add(AcceptedTypicalChanges acceptedTypicalChanges)
        {
            _baseRepository.Add(acceptedTypicalChanges);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(AcceptedTypicalChanges acceptedTypicalChanges)
        {
            _baseRepository.Update(acceptedTypicalChanges);
        }

        public AcceptedTypicalChanges GetAcceptedTypicalChanges(Guid id)
        {
            var data = _baseRepository.GetById(id).FirstOrDefault();
            return new AcceptedTypicalChanges(data.Id, data.TypicalId, data.TypicalName, data.OldXml, data.NewXml, data.OldXmlHash, data.NewXmlHash,data.VersionId,data.OldJson,data.NewJson);
        }
    }
}