﻿using SenseAI.Domain.DataModel;
using Persistence.Internal;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SenseAI.Domain;
using System.Collections.Generic;
using SenseAI.Domain.TestCaseModel;

namespace Persistence.DataModel
{
    public sealed class TypicalRepository : ITypicalRepository
    {
        private readonly IBaseRepository<Typical, TypicalData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public TypicalRepository(IBaseRepository<Typical, TypicalData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(Typical typical)
        {
            _baseRepository.Add(typical);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(Typical typical)
        {
            _baseRepository.Update(typical);
        }

        public Typical GetTypical(Guid id)
        {
            var data = _baseRepository.GetById(id).FirstOrDefault();
            return new Typical(data.Id, data.CatalogId, data.Title, data.Json, data.Xml, data.XmlHash, data.Type);
        }

        public Typical GetTypical(Guid catalogId, string name, TypicalType typicalType)
        {
            var typicalData = _context.Set<TypicalData>().AsNoTracking().FirstOrDefault(w => w.CatalogId == catalogId && w.Title == name && w.Type == typicalType);
            if (typicalData == null)
                return null;
            return new Typical(typicalData.Id, typicalData.CatalogId, typicalData.Title, typicalData.Json, typicalData.Xml, typicalData.XmlHash, typicalData.Type);
        }

        public void AddEnquiryActions(Guid catalogId, string typicalName, string action)
        {
            var enqAction = _context.Set<EnquiryActionData>().FirstOrDefault(w => w.CatalogId == catalogId && w.TypicalName == typicalName);
            if (enqAction != null)
            {
                if (enqAction.TypicalName.ToLower().StartsWith("v_"))
                {
                    if (!enqAction.Actions.StartsWith("['") && !enqAction.Actions.StartsWith("']"))
                    {
                        enqAction.Actions = enqAction.Actions.Replace("[", "['").Replace("]", "']");
                    }
                    var lst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(enqAction.Actions);
                    if (lst.Any(a => a.ToLower() == action.ToLower()))
                        return;
                    enqAction.Actions = enqAction.Actions.Replace("]", "") + ",'" + action + "']";
                }
                else
                {
                    var lst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EnquiryActions>>(enqAction.Actions);
                    var lstOutsid = Newtonsoft.Json.JsonConvert.DeserializeObject<EnquiryActions>(action);

                    if (lst.Any(a => a.Action.ToLower() == lstOutsid.Action.ToLower()))
                        return;
                    lst.Add(lstOutsid);
                    enqAction.Actions = Newtonsoft.Json.JsonConvert.SerializeObject(lst);  
                }
                

                _context.Set<EnquiryActionData>().Update(enqAction);
            }
            else
            {
                EnquiryActionData enquiryActionData = new EnquiryActionData
                {
                    Id = Guid.NewGuid(),
                    CatalogId = catalogId,
                    TypicalName = typicalName,
                    Actions = "[" + action + "]"
                };
                if (enquiryActionData.TypicalName.ToLower().StartsWith("v_"))
                {
                    enquiryActionData.Actions = "['" + action + "']";
                }

                _context.Set<EnquiryActionData>().Add(enquiryActionData);

            }
            _context.SaveChanges();
        }
         
        public bool CheckIfTypicalExist(Guid catalogId, string typicalTitle)
        {
            return _context.Set<TypicalData>().AsNoTracking()
                .Where(t => t.CatalogId == catalogId && t.Title == typicalTitle).Any();
        }
    }
      
}