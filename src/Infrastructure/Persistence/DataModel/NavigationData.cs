﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.DataModel
{
    [Table("Navigations")]
    public class NavigationData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid CatalogId { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public Guid? ParentId { get; set; }
    }
}