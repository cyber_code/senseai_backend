﻿using SenseAI.Domain;
using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.DataModel
{
    [Table("AcceptedTypicalChanges")]
    public class AcceptedTypicalChangesData : BaseData, IData
    {
        public Guid Id { get; set; }

        public string OldXml { get;  set; }
        public string NewXml { get;  set; }
        public string OldJson { get;  set; }
        public string NewJson { get;  set; }
        public string OldXmlHash { get;  set; }
        public string NewXmlHash { get;  set; }

        public Guid TypicalId { get;  set; }
        public string TypicalName { get;  set; }


        public int VersionId { get;  set; }
    }
}