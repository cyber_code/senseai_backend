﻿using SenseAI.Domain;
using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.DataModel
{
    [Table("TypicalChanges")]
    public class TypicalChangesData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid CatalogId { get; set; }

        public string Title { get; set; }

        public string Json { get; set; }

        public string Xml { get; set; }

        public string XmlHash { get; set; }

        public TypicalType Type { get; set; }
        public int Status { get; set; }
    }
}