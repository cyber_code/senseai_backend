﻿using SenseAI.Domain.DataModel;
using Persistence.Internal;
using System;
using System.Linq;

namespace Persistence.DataModel
{
    public sealed class TypicalChangesRepository : ITypicalChangesRepository
    {
        private readonly IBaseRepository<TypicalChanges, TypicalChangesData> _baseRepository;

        public TypicalChangesRepository(IBaseRepository<TypicalChanges, TypicalChangesData> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public void Add(TypicalChanges typicalChanges)
        {
            _baseRepository.Add(typicalChanges);
        }

        public void Delete(Guid id)
        {
            _baseRepository.Delete(id);
        }

        public void Update(TypicalChanges typicalChanges)
        {
            _baseRepository.Update(typicalChanges);
        }

        public TypicalChanges GetTypicalChanges(Guid id)
        {
            var data = _baseRepository.GetById(id).FirstOrDefault();
            return new TypicalChanges(data.Id, data.CatalogId, data.Title, data.Json, data.Xml, data.XmlHash, data.Type,data.Status);
        }
    }
}