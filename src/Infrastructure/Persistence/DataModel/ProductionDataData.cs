﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.DataModel
{
    [Table("ProductionData")]
    public class ProductionDataData : BaseData
    {
        public int Id { get; set; }

        public Guid CatalogId { get; set; }

        public string ApplicationName { get; set; }

        public string Key { get; set; }

        public string Json { get; set; }

        public string Xml { get; set; }

        public DateTime Date { get; set; }
    }
}