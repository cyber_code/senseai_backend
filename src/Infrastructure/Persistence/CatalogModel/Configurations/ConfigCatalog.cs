﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.CatalogModel.Configurations
{
    internal class ConfigDataSets : SenseAIEntityTypeConfiguration<CatalogData>
    {
        public override void Configure(EntityTypeBuilder<CatalogData> builder)
        {
            builder.ToTable(typeof(CatalogData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ProjectId).IsRequired();
            builder.Property(x => x.SubProjectId).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(300).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(500);

            base.Configure(builder);
        }
    }
}