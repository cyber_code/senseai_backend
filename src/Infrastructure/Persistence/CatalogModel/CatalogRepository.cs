﻿using SenseAI.Domain.CatalogModel;
using Persistence.Internal;
using System;
using Persistence.SystemCatalogModel;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Persistence.CatalogModel
{
    public sealed class CatalogRepository : ICatalogRepository
    {
        private readonly IBaseRepository<Catalog, CatalogData> _baseRepository;
        private readonly SenseAIObjectContext _context;

        public CatalogRepository(IBaseRepository<Catalog, CatalogData> baseRepository, SenseAIObjectContext context)
        {
            _baseRepository = baseRepository;
            _context = context;
        }

        public void Add(Catalog catalog)
        {
            _baseRepository.Add(catalog);
        }

        public void Delete(Guid id)
        {
            var items = _context.Set<SystemCatalogData>().Where(i => i.CatalogId == id);
            _context.Set<SystemCatalogData>().RemoveRange(items);
            _baseRepository.Delete(id);
        }

        public void Update(Catalog catalog)
        {
            _baseRepository.Update(catalog);
        }

        public List<Catalog> GetCatalogs(Guid subProjectId, Guid systemId)
        {
            return  (from catalog in _context.Set<CatalogData>().AsNoTracking()
                          join
                             systemCatalog in _context.Set<SystemCatalogData>().AsNoTracking() on
                             catalog.Id equals systemCatalog.CatalogId
                          where systemCatalog.SubprojectId == subProjectId && systemCatalog.SystemId == systemId
                          select new Catalog(catalog.Id, catalog.ProjectId, catalog.SubProjectId, catalog.Title, catalog.Description, catalog.Type)).ToList();
        }
    }
}