﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.CatalogModel
{
    [Table("Catalogs")]
    public class CatalogData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int Type { get; set; }
    }
}