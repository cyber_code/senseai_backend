using Microsoft.EntityFrameworkCore; 
using Core.Abstractions;
using Core.Exceptions; 
using System; 
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Transactions;
using Persistence.SystemModel;
using Persistence.CatalogModel;
using Microsoft.VisualBasic.CompilerServices;
using Persistence.AdminModel;
using Persistence.DataSetsModel;
using Persistence.WorkflowModel;
using System.Collections.Generic;
using ProjectData = Persistence.AdminModel.ProjectData;
using Persistence.DataModel;

namespace Persistence.Internal
{
    /// <summary>
    /// Represents base object context
    /// </summary>

    public class SenseAIObjectContext : DbContext, IDbContext
    {
        public IWorkContext _workContext { get; }

        #region Ctor

        public SenseAIObjectContext(DbContextOptions<SenseAIObjectContext> options, IWorkContext workContext) : base(options)
        {
            _workContext = workContext;
        }

        #endregion Ctor

        public override int SaveChanges()
        {

            AddTimestamps();

            return base.SaveChanges();
        }

        //public override async Task<int> SaveChangesAsync()
        //{
        //    AddTimestamps();
        //    return await base.SaveChangesAsync();
        //}

        private void AddTimestamps()
        {
           
            ChangeTracker.DetectChanges();

            foreach (var entry in ChangeTracker.Entries())
            {
                if ((entry.Entity.GetType().IsAssignableFrom(typeof(BaseData))))
                {
                    continue;
                }
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DateCreated").CurrentValue = DateTime.UtcNow;
                    entry.Property("UserCreated").CurrentValue = _workContext.UserId;
                }
                else if (entry.State == EntityState.Modified)
                {
                    entry.Property("DateCreated").IsModified = false;
                    entry.Property("UserCreated").IsModified = false;
                    entry.Property("DateModified").CurrentValue = DateTime.UtcNow;
                    entry.Property("UserModified").CurrentValue = _workContext.UserId;
                }
            }

            //var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseData && (x.State == EntityState.Added || x.State == EntityState.Modified));

            //var currentUsername = !string.IsNullOrEmpty(System.Web.HttpContext.Current?.User?.Identity?.Name)
            //    ? HttpContext.Current.User.Identity.Name
            //    : "Anonymous";
            //var currentUsername = _workContext.UserId;

            //foreach (var entity in entities)
            //{
            //    if (entity.State == EntityState.Added)
            //    {
            //        ((BaseData)entity.Entity).DateCreated = DateTime.UtcNow;
            //        //((BaseData)entity.Entity).UserCreated = currentUsername;
            //    }

            //    //((BaseData)entity.Entity).DateModified = DateTime.UtcNow;
            //    //((BaseData)entity.Entity).UserModified = currentUsername;
            //}
            //DbContext.Entry(ProjectData).Property("LastUpdated").CurrentValue = DateTime.Now;
            //var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseData && (x.State == EntityState.Added || x.State == EntityState.Modified));
        }

        #region Utilities

        /// <summary>
        /// Further configuration the model
        /// </summary>
        /// <param name="modelBuilder">The builder being used to construct the model for this context</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //dynamically load all entity and query type configurations
            var typeConfigurations = Assembly.GetExecutingAssembly().GetTypes().Where(type =>
                (type.BaseType?.IsGenericType ?? false)
                    && (type.BaseType.GetGenericTypeDefinition() == typeof(SenseAIEntityTypeConfiguration<>)
                        || type.BaseType.GetGenericTypeDefinition() == typeof(SenseAIEntityTypeWithSchemaConfiguration<>)
                        || type.BaseType.GetGenericTypeDefinition() == typeof(SenseAIQueryTypeConfiguration<>)));

            foreach (var typeConfiguration in typeConfigurations)
            {
                var configuration = (IMappingConfiguration)Activator.CreateInstance(typeConfiguration);
                configuration.ApplyConfiguration(modelBuilder);
            }

            #region System & Catalog Dummy Data
            /*
            var inputSystemTupe = new SystemData { Id = Guid.NewGuid(), Title = "R17 Input System", AdapterName = "T24WebAdapter" };
            var authorizerSystemTupe = new SystemData { Id = Guid.NewGuid(), Title = "R17 Author System", AdapterName = "T24WebAdapter" };

            var systemCatalogTuple = new CatalogData { Id = Guid.Parse("764CFFEF-EC71-4D16-ACF0-EF9F80211C1C"), SubProjectId = Guid.NewGuid(), Title = "ProjectResourcing", Description = "Project Resourcing" };
            var applicationCatalogueTuple = new CatalogData { Id = Guid.Parse("764CFFEF-EC71-4D16-ACF0-EF9F80211C5C"), SubProjectId = Guid.NewGuid(), Title = "R17 Model Bank Applications", Description = "R17 Model Bank Applications" };
            var enquiryCatalogueTuple = new CatalogData { Id = Guid.Parse("6C0A8EF3-0C33-4135-B801-25A208E398BD"), SubProjectId = Guid.NewGuid(), Title = "R17 Model Bank Enquiries", Description = "R17 Model Bank Enquiries" };

            #endregion System & Catalog Dummy Data

            #region Typical Dummy Data

            var navigationTypicalJson = @"{ ""TypicalName"": ""NavigationInfo"", ""Attributes"": [] }";
            string customerJson = @"{ ""TypicalName"": ""Customer"",
	                                  ""Attributes"": [
                                                          { ""Name"": ""CUSTOMER.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""MNEMONIC"",      ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""FIRST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""LAST.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""SHORT.NAME"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """",""Length"": ""30"" },
		                                                  { ""Name"": ""NAME.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""STREET.1"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""TOWN.COUNTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""RELATION.CODE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""SECTOR"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""INDUSTRY"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""NATIONALITY"",  ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""CUSTOMER.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""RESIDENCE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""CONTACT.DATE"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""REVIEW.FREQUENCY"", ""DataType"": ""string"",  ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """",  ""MaxValue"": """", ""Length"": ""30"" },
		                                                  { ""Name"": ""RECORD.STATUS"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" }
	                                                ]
                                            }";
            var customerNauResultJson = @"{ ""TypicalName"": ""CUSTOMER.NAU.RESULT"",
	                                     ""Attributes"": [
                                                          { ""Name"": ""Customer No"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                                                          { ""Name"": ""Name"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                                                          { ""Name"": ""Relationship Officer"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                                                          { ""Name"": ""Status"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                                                          { ""Name"": ""Inputter"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" },
                                                          { ""Name"": ""ID"", ""DataType"": ""string"", ""Status"": ""True"", ""DefValue"": """", ""MinValue"": """", ""MaxValue"": """", ""Length"": ""30"" }
                                                         ]
                                                }";

            List<TypicalData> listTypicalTuple = new List<TypicalData>();

            listTypicalTuple.Add(new TypicalData { Id = Guid.NewGuid(), CatalogId = systemCatalogTuple.Id, Title = "NavigationInfo", Json = navigationTypicalJson, Type = SenseAI.Domain.TypicalType.Application });
            var scenario01CustomerTypicalInstance = new TypicalData { Id = Guid.NewGuid(), CatalogId = applicationCatalogueTuple.Id, Title = "CUSTOMER", Json = customerJson, Type = SenseAI.Domain.TypicalType.Application };
            listTypicalTuple.Add(scenario01CustomerTypicalInstance);
            listTypicalTuple.Add(new TypicalData { Id = Guid.NewGuid(), CatalogId = systemCatalogTuple.Id, Title = "CUSTOMER.NAU.RESULT", Json = customerNauResultJson, Type = SenseAI.Domain.TypicalType.Enquiry });

            listTypicalTuple.Add(new TypicalData { Id = Guid.NewGuid(), CatalogId = applicationCatalogueTuple.Id, Title = "ACCOUNT", Json = customerJson, Type = SenseAI.Domain.TypicalType.Application });
            listTypicalTuple.Add(new TypicalData { Id = Guid.NewGuid(), CatalogId = applicationCatalogueTuple.Id, Title = "SAVINGS.ACCOUNT", Json = customerJson, Type = SenseAI.Domain.TypicalType.Application });
            listTypicalTuple.Add(new TypicalData { Id = Guid.NewGuid(), CatalogId = applicationCatalogueTuple.Id, Title = "INCOME.ACCOUNT", Json = customerJson, Type = SenseAI.Domain.TypicalType.Application });
            listTypicalTuple.Add(new TypicalData { Id = Guid.NewGuid(), CatalogId = applicationCatalogueTuple.Id, Title = "PREMIUM.ACCOUNT", Json = customerJson, Type = SenseAI.Domain.TypicalType.Application });

            #endregion Typical Dummy Data

            #region Project & Subproject Dummy Data

            var projectd = new ProjectData { Id = Guid.NewGuid(),  Description = "Project 1 description", Title = "Project 1" };
            var subprojectd = new SubProjectData { Id = Guid.NewGuid(), Title = "Sub project", Description = "Sub project description", ProjectId = projectd.Id };

            var scenario01ProjectTuple = new ProjectData { Id = Guid.NewGuid(), Description = "Test Strategist", Title = "Test Strategist" };
            var scenario01SubprojectTuple = new SubProjectData { Id = Guid.NewGuid(), Title = "Main Subproject", Description = "Main Subproject", ProjectId = scenario01ProjectTuple.Id };

            #endregion Project & Subproject Dummy Data

            #region Folder Dummy Data

            var folderd = new FolderData { Id = Guid.NewGuid(), Title = "Designed Workflows", Description = "SenseAI Designed TC", SubProjectId = scenario01SubprojectTuple.Id };
            var scenario01FolderTuple = new FolderData { Id = Guid.NewGuid(), Title = "Designed Workflows", Description = "SenseAI Designed TC", SubProjectId = scenario01SubprojectTuple.Id };

            #endregion Folder Dummy Data

            #region [ DataSets ]

            var scenario01CustomerDataset = new DataSetData() { Id = Guid.Parse("A77E812D-ECD5-4C53-9904-0FD867CF7FC3"), CatalogId = applicationCatalogueTuple.Id, SubProjectId = scenario01SubprojectTuple.Id, Title = "CUSTOMER", SystemId = systemCatalogTuple.Id, TypicalId = scenario01CustomerTypicalInstance.Id, Combinations = @"[{""Attribute"":""GENDER"",""Values"":[""M"",""F""]},{""Attribute"":""SECTOR"",""Values"":[""2001"",""1001""]}]" };
            var scenario01CustomerDatasetItems = new List<DataSetItemData>
            {
                new DataSetItemData{ Id  = Guid.NewGuid(), DataSetId = scenario01CustomerDataset.Id, Row = @"[{ ""Name"": ""SHORT.NAME-1~1"", ""Value"": ""CM1"" }, { ""Name"": ""NAME.1-1~1"", ""Value"": ""CLIENT NAME"" }, { ""Name"": ""GENDER"", ""Value"": ""M"" }, { ""Name"": ""SECTOR"", ""Value"": ""1001"" }, { ""Name"": ""LANGUAGE"", ""Value"": ""1"" }, { ""Name"": ""NET.MONTHLY.IN"", ""Value"": ""2000"" }, { ""Name"": ""ID"", ""Value"": ""1938739"" }]"},
                new DataSetItemData{ Id  = Guid.NewGuid(), DataSetId = scenario01CustomerDataset.Id, Row = @"[{ ""Name"": ""SHORT.NAME-1~1"", ""Value"": ""CM1"" }, { ""Name"": ""NAME.1-1~1"", ""Value"": ""CLIENT NAME"" }, { ""Name"": ""GENDER"", ""Value"": ""F"" }, { ""Name"": ""SECTOR"", ""Value"": ""1001"" }, { ""Name"": ""LANGUAGE"", ""Value"": ""1"" }, { ""Name"": ""NET.MONTHLY.IN"", ""Value"": ""2000"" }, { ""Name"": ""ID"", ""Value"": ""1938739"" }]"},
                new DataSetItemData{ Id  = Guid.NewGuid(), DataSetId = scenario01CustomerDataset.Id, Row = @"[{ ""Name"": ""SHORT.NAME-1~1"", ""Value"": ""CM1"" }, { ""Name"": ""NAME.1-1~1"", ""Value"": ""CLIENT NAME"" }, { ""Name"": ""GENDER"", ""Value"": ""M"" }, { ""Name"": ""SECTOR"", ""Value"": ""2001"" }, { ""Name"": ""LANGUAGE"", ""Value"": ""1"" }, { ""Name"": ""NET.MONTHLY.IN"", ""Value"": ""2000"" }, { ""Name"": ""ID"", ""Value"": ""1938739"" }]"},
                new DataSetItemData{ Id  = Guid.NewGuid(), DataSetId = scenario01CustomerDataset.Id, Row = @"[{ ""Name"": ""SHORT.NAME-1~1"", ""Value"": ""CM1"" }, { ""Name"": ""NAME.1-1~1"", ""Value"": ""CLIENT NAME"" }, { ""Name"": ""GENDER"", ""Value"": ""F"" }, { ""Name"": ""SECTOR"", ""Value"": ""2001"" }, { ""Name"": ""LANGUAGE"", ""Value"": ""1"" }, { ""Name"": ""NET.MONTHLY.IN"", ""Value"": ""2000"" }, { ""Name"": ""ID"", ""Value"": ""1938739"" }]"}
            };

            var scenario01AccountDataset = new DataSetData() { Id = Guid.Parse("FC3E812D-FC35-4C53-9904-FC3867CF7FC3"), CatalogId = applicationCatalogueTuple.Id, SubProjectId = scenario01SubprojectTuple.Id, Title = "ACCOUNT", SystemId = systemCatalogTuple.Id, TypicalId = scenario01CustomerTypicalInstance.Id, Combinations = @"[{""Attribute"":""CUSTOMER"",""Values"":[""4525""]}]" };
            var scenario01AccountDatasetItems = new List<DataSetItemData>
            {
                new DataSetItemData{ Id  = Guid.NewGuid(), DataSetId = scenario01AccountDataset.Id, Row = @"[{ ""Name"": ""CUSTOMER"", ""Value"": ""4525"" }]"},
            };

            #endregion [ DataSets ]

            #region [ Workflow ]

            #region [ Workflow 1]

            var customerWorkflowTuple = new WorkflowData { Id = Guid.NewGuid(), Title = "Customer workflow", Description = "Add Customer", FolderId = folderd.Id, };
            var workflowVersiond = new WorkflowVersionData { Title = customerWorkflowTuple.Title, WorkflowId = customerWorkflowTuple.Id, VersionId = 0 };

            List<WorkflowItemData> listWorkflowItemDataTuple = new List<WorkflowItemData>
            {
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerWorkflowTuple.Id, Title = "Customer", Description = "Customer", TypicalTitle = "", CatalogTitle = "" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerWorkflowTuple.Id, Title = "See", Description = "See", TypicalTitle = "", CatalogTitle = "" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerWorkflowTuple.Id, Title = "Account", Description = "Account", TypicalTitle = "", CatalogTitle = "" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerWorkflowTuple.Id, Title = "Close account", Description = "Close account", TypicalTitle = "", CatalogTitle = "" }
            };

            Guid testcaseId = Guid.Parse("F91C4627-1B48-44CE-91FB-64413CA68395");
            Guid workflowPathId = Guid.Parse("F91C4627-1B48-44CE-91FB-64413CA68390");

            List<WorkflowTestCasesData> listpland = new List<WorkflowTestCasesData>();
            listpland.Add(new WorkflowTestCasesData { Id = Guid.NewGuid(), WorkflowId = customerWorkflowTuple.Id, WorkflowPathId = workflowPathId, TestCaseId = testcaseId, TestCaseTitle = "Test Case Title 1", TestStepId = Guid.NewGuid(), TestStepTitle = "Test Step Title 1", TypicalName = "Customer", TestStepJson = "Json" });
            listpland.Add(new WorkflowTestCasesData { Id = Guid.NewGuid(), WorkflowId = customerWorkflowTuple.Id, WorkflowPathId = workflowPathId, TestCaseId = testcaseId, TestCaseTitle = "Test Case Title 1", TestStepId = Guid.NewGuid(), TestStepTitle = "Test Step Title 2", TypicalName = "Customer", TestStepJson = "Json" });

            testcaseId = Guid.Parse("3A76727C-BB66-425C-BA1B-FD9638E15A34");
            listpland.Add(new WorkflowTestCasesData { Id = Guid.NewGuid(), WorkflowId = customerWorkflowTuple.Id, WorkflowPathId = workflowPathId, TestCaseId = testcaseId, TestCaseTitle = "Test Case Title 2", TestStepId = Guid.NewGuid(), TestStepTitle = "Test Step Title 1", TypicalName = "Customer", TestStepJson = "Json" });
            listpland.Add(new WorkflowTestCasesData { Id = Guid.NewGuid(), WorkflowId = customerWorkflowTuple.Id, WorkflowPathId = workflowPathId, TestCaseId = testcaseId, TestCaseTitle = "Test Case Title 2", TestStepId = Guid.NewGuid(), TestStepTitle = "Test Step Title 2", TypicalName = "Customer", TestStepJson = "Json" });

            #endregion [ Workflow 1]

            #region [ Real scenario: Do not delete ]

            var customerCreationScenario01WorkflowTuple = new WorkflowData { Id = Guid.Parse("677E812C-ECD8-4C53-9904-0FD867CF7BA5"), Title = "Customer Creation Scenario 01", Description = "Customer Creation Real scenario", FolderId = scenario01FolderTuple.Id, };
            var scenario01WorkflowVersiond = new WorkflowVersionData { Title = customerCreationScenario01WorkflowTuple.Title, WorkflowId = customerCreationScenario01WorkflowTuple.Id, VersionId = 0 };

            List<WorkflowItemData> scenario01ListWorkflowItemDataTuple = new List<WorkflowItemData>
            {
                //TC1
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Menu: Individual Customer", Description = "Menu: Individual Customer", TypicalTitle = "NavigationInfo", CatalogTitle = "ProjectResourcing" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Create: CUSTOMER,INPUT",    Description = "Create: CUSTOMER,INPUT", TypicalTitle = "CUSTOMER", CatalogTitle = "R17 214 Application" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Menu: Sign Off",            Description = "Menu: Sign Off", TypicalTitle = "NavigationInfo", CatalogTitle = "ProjectResourcing" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Menu: Authorise/Delete Customer", Description = "Menu: Authorise/Delete Customer", TypicalTitle = "NavigationInfo", CatalogTitle = "ProjectResourcing" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Enquiry Action: CUSTOMER.NAU.RESULT", Description = "Enquiry Action: CUSTOMER.NAU.RESULT", TypicalTitle = "CUSTOMER.NAU.RESULT", CatalogTitle = "R17 214 Enquiries" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Authorise: CUSTOMER,NAU", Description = "Authorise: CUSTOMER,NAU", TypicalTitle = "CUSTOMER", CatalogTitle = "R17 214 Application" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Menu: Sign Off", Description = "Menu: Sign Off", TypicalTitle = "NavigationInfo", CatalogTitle = "ProjectResourcing" },
                //TC2
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Menu: Open Current Account", Description = "Menu: Open Current Account", TypicalTitle = "NavigationInfo", CatalogTitle = "ProjectResourcing" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Create: ACCOUNT,CA.OPEN", Description = "Create: ACCOUNT,CA.OPEN", TypicalTitle = "ACCOUNT", CatalogTitle = "R17 214 Application" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Menu: Sign Off", Description = "Menu: Sign Off", TypicalTitle = "NavigationInfo", CatalogTitle = "ProjectResourcing" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Menu: Authorise/Delete Account", Description = "Menu: Authorise/Delete Account", TypicalTitle = "NavigationInfo", CatalogTitle = "ProjectResourcing" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Enquiry Action: ACCOUNT.NAU.RESULT", Description = "Enquiry Action: ACCOUNT.NAU.RESULT", TypicalTitle = "ACCOUNT.NAU.RESULT", CatalogTitle = "R17 214 Enquiries" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Authorise: ACCOUNT,AUTH", Description = "Authorise: ACCOUNT,AUTH", TypicalTitle = "R17 214 Application", CatalogTitle = "ACCOUNT" },
                new WorkflowItemData { Id = Guid.NewGuid(), WorkflowId = customerCreationScenario01WorkflowTuple.Id, Title = "Menu: Sign Off", Description = "Menu: Sign Off", TypicalTitle = "NavigationInfo", CatalogTitle = "ProjectResourcing" }
            };

            testcaseId = Guid.Parse("A91C4627-1B48-44CE-91FB-64413CA68395");
            workflowPathId = Guid.Parse("F91C4627-1B48-44CE-91FB-64413CA68391");

            #region [ Generated Test Cases Data ]

            List<WorkflowTestCasesData> scenario01WorkflowTestCasesDataTuple = new List<WorkflowTestCasesData>();
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 1,
                TestCaseTitle = "01. Customer Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 1,
                TestStepTitle = "Menu: Individual Customer",
                TypicalName = "NavigationInfo",
                TestStepJson = @"{ ""title"": ""Menu: Individual Customer"", ""description"": ""Menu: Individual Customer"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""ProjectResourcing"", ""typicalName"": ""NavigationInfo"", ""parameters"": "",MNU,,,,,,User Menu > Customer > Individual Customer"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 1,
                TestCaseTitle = "01. Customer Creation",
                TestStepId = Guid.Parse("c43859cc-874b-4404-8d73-c14950d1d555"),
                TSIndex = 2,
                TestStepTitle = "Create: CUSTOMER,INPUT",
                TypicalName = "CUSTOMER",
                TestStepJson = @"{ ""title"": ""Create: CUSTOMER,INPUT"", ""description"": ""Create: CUSTOMER,INPUT"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""R17 214 Application"", ""typicalName"": ""CUSTOMER"", ""parameters"": ""INPUT,I"", ""typicalInstance"":null , ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [{ ""TestStepId"": ""00000000-0000-0000-0000-000000000000"", ""AttributeName"": ""MNEMONIC"", ""PlainTextFormula"": ""JE1ORU1PTklDKEdFVF9SQU5ET01fV09SRCg5KSk="", ""UiMode"": null,""Id"": ""b0eaa6b1-46af-4903-b307-3835a036227b"" }], ""dataSetIds"": [""A77E812D-ECD5-4C53-9904-0FD867CF7FC3""] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 1,
                TestCaseTitle = "01. Customer Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 3,
                TestStepTitle = "Menu: Sign Off",
                TypicalName = "NavigationInfo",
                TestStepJson = @"{ ""title"": ""Menu: Sign Off"", ""description"": ""Menu: Sign Off"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""ProjectResourcing"", ""typicalName"": ""NavigationInfo"", ""parameters"": "",MNU,,,,,,Sign Off"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 1,
                TestCaseTitle = "01. Customer Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 4,
                TestStepTitle = "Menu: Authorise/Delete Customer",
                TypicalName = "NavigationInfo",
                TestStepJson = @"{ ""title"": ""Menu: Authorise/Delete Customer"", ""description"": ""Menu: Authorise/Delete Customer"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""ProjectResourcing"", ""typicalName"": ""NavigationInfo"", ""parameters"": "",MNU,,,,,,User Menu > Customer > Authorise/Delete Customer"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 1,
                TestCaseTitle = "01. Customer Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 5,
                TestStepTitle = "Enquiry Action: CUSTOMER.NAU.RESULT",
                TypicalName = "CUSTOMER.NAU.RESULT",
                TestStepJson = @"{ ""title"": ""Enquiry Action: CUSTOMER.NAU.RESULT"", ""description"": ""Enquiry Action: CUSTOMER.NAU.RESULT"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""R17 214 Enquiries"", ""typicalName"": ""CUSTOMER.NAU.RESULT"", ""parameters"": ""CLICK(\""Authorise\"")"", ""typicalInstance"": null, ""filters"": null, ""postFilters"":  [{ ""AttributeName"": ""Customer No"", ""OperatorName"": ""Equal"", ""AttributeValue"": null, ""DynamicData"": { ""testStepId"": ""c43859cc-874b-4404-8d73-c14950d1d555"", ""sourceAttributeName"": ""@ID"",  ""targetAttributeName"": ""Customer No"", ""plainTextFormula"": null, ""uiMode"": null } }], ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 1,
                TestCaseTitle = "01. Customer Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 6,
                TestStepTitle = "Authorise: CUSTOMER,NAU",
                TypicalName = "CUSTOMER",
                TestStepJson = @"{ ""title"": ""Authorise: CUSTOMER,NAU"", ""description"": ""Authorise: CUSTOMER,NAU"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""R17 214 Application"", ""typicalName"": ""CUSTOMER"", ""parameters"": ""NAU,A"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [{ ""testStepId"": ""c43859cc-874b-4404-8d73-c14950d1d555"", ""sourceAttributeName"": ""@ID"",  ""targetAttributeName"": ""Customer No"", ""plainTextFormula"": null, ""uiMode"": null }], ""dataSetIds"": [] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 1,
                TestCaseTitle = "01. Customer Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 7,
                TestStepTitle = "Menu: Sign Off",
                TypicalName = "NavigationInfo",
                TestStepJson = @"{ ""title"": ""Menu: Sign Off"", ""description"": ""Menu: Sign Off"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""ProjectResourcing"", ""typicalName"": ""NavigationInfo"", ""parameters"": "",MNU,,,,,,Sign Off"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });

            testcaseId = Guid.Parse("B91C4628-1C48-44CE-91FB-64413CB68396");
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 2,
                TestCaseTitle = "02. Account Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 1,
                TestStepTitle = "Menu: Open Current Account",
                TypicalName = "NavigationInfo",
                TestStepJson = @"{ ""title"": ""Menu: Open Current Account"", ""description"": ""Menu: Open Current Account"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""ProjectResourcing"", ""typicalName"": ""NavigationInfo"", ""parameters"": "",MNU,,,,,,User Menu > Account > Open Current Account"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 2,
                TestCaseTitle = "02. Account Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 2,
                TestStepTitle = "Create: ACCOUNT,CA.OPEN",
                TypicalName = "ACCOUNT",
                TestStepJson = @"{ ""title"": ""Create: ACCOUNT,CA.OPEN"", ""description"": ""Create: ACCOUNT,CA.OPEN"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""R17 214 Application"", ""typicalName"": ""ACCOUNT"", ""parameters"": ""CA.OPEN,I"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [""FC3E812D-FC35-4C53-9904-FC3867CF7FC3""] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 2,
                TestCaseTitle = "02. Account Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 3,
                TestStepTitle = "Menu: Sign Off",
                TypicalName = "NavigationInfo",
                TestStepJson = @"{ ""title"": ""Menu: Sign Off"", ""description"": ""Menu: Sign Off"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""ProjectResourcing"", ""typicalName"": ""NavigationInfo"", ""parameters"": "",MNU,,,,,,Sign Off"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 2,
                TestCaseTitle = "02. Account Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 4,
                TestStepTitle = "Menu: Authorise/Delete Account",
                TypicalName = "NavigationInfo",
                TestStepJson = @"{ ""title"": ""Menu: Authorise/Delete Account"", ""description"": ""Menu: Authorise/Delete Account"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""ProjectResourcing"", ""typicalName"": ""NavigationInfo"", ""parameters"": "",MNU,,,,,,User Menu > Account > Authorise/Delete Account"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 2,
                TestCaseTitle = "02. Account Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 5,
                TestStepTitle = "Enquiry Action: ACCOUNT.NAU.RESULT",
                TypicalName = "ACCOUNT.NAU.RESULT",
                TestStepJson = @"{ ""title"": ""Enquiry Action: ACCOUNT.NAU.RESULT"", ""description"": ""Enquiry Action: ACCOUNT.NAU.RESULT"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""R17 214 Enquiries"", ""typicalName"": ""ACCOUNT.NAU.RESULT"", ""parameters"": ""CLICK(\""Authorise\"")"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 2,
                TestCaseTitle = "02. Account Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 6,
                TestStepTitle = "Authorise: ACCOUNT,AUTH",
                TypicalName = "ACCOUNT",
                TestStepJson = @"{ ""title"": ""Authorise: ACCOUNT,AUTH"", ""description"": ""Authorise: ACCOUNT,AUTH"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""R17 214 Application"", ""typicalName"": ""ACCOUNT"", ""parameters"": ""AUTH,A"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });
            scenario01WorkflowTestCasesDataTuple.Add(new WorkflowTestCasesData
            {
                Id = Guid.NewGuid(),
                WorkflowId = customerCreationScenario01WorkflowTuple.Id,
                WorkflowPathId = workflowPathId,
                TestCaseId = testcaseId,
                TCIndex = 2,
                TestCaseTitle = "02. Account Creation",
                TestStepId = Guid.NewGuid(),
                TSIndex = 7,
                TestStepTitle = "Menu: Sign Off",
                TypicalName = "NavigationInfo",
                TestStepJson = @"{ ""title"": ""Menu: Sign Off"", ""description"": ""Menu: Sign Off"", ""adapterName"": ""T24WebAdapter"", ""catalogName"": ""ProjectResourcing"", ""typicalName"": ""NavigationInfo"", ""parameters"": "",MNU,,,,,,Sign Off"", ""typicalInstance"": null, ""filters"": null, ""postFilters"": null, ""dynamicDatas"": [], ""dataSetIds"": [] }"
            });

            #endregion [ Generated Test Cases Data ]

            #endregion [ Real scenario: Do not delete ]

            #endregion [ Workflow ]

            //Systems
            modelBuilder.Entity<SystemData>().HasData(inputSystemTupe);
            modelBuilder.Entity<SystemData>().HasData(authorizerSystemTupe);

            //Catalogues
            modelBuilder.Entity<CatalogData>().HasData(systemCatalogTuple);
            modelBuilder.Entity<CatalogData>().HasData(applicationCatalogueTuple);
            modelBuilder.Entity<CatalogData>().HasData(enquiryCatalogueTuple);

            //Typicals
            modelBuilder.Entity<TypicalData>().HasData(listTypicalTuple);
            //DataSet
            modelBuilder.Entity<DataSetData>().HasData(scenario01CustomerDataset);
            modelBuilder.Entity<DataSetItemData>().HasData(scenario01CustomerDatasetItems);

            modelBuilder.Entity<DataSetData>().HasData(scenario01AccountDataset);
            modelBuilder.Entity<DataSetItemData>().HasData(scenario01AccountDatasetItems);

            #region [ Prioject 1 ]

            modelBuilder.Entity<ProjectData>().HasData(projectd);
            modelBuilder.Entity<SubProjectData>().HasData(subprojectd);
            modelBuilder.Entity<FolderData>().HasData(folderd);
            modelBuilder.Entity<WorkflowData>().HasData(customerWorkflowTuple);
            modelBuilder.Entity<WorkflowVersionData>().HasData(workflowVersiond);
            modelBuilder.Entity<WorkflowItemData>().HasData(listWorkflowItemDataTuple);
            modelBuilder.Entity<WorkflowTestCasesData>().HasData(listpland);

            #endregion [ Prioject 1 ]

            #region [ Real scenario prioject : Do not delete ]

            modelBuilder.Entity<ProjectData>().HasData(scenario01ProjectTuple);
            modelBuilder.Entity<SubProjectData>().HasData(scenario01SubprojectTuple);
            modelBuilder.Entity<FolderData>().HasData(scenario01FolderTuple);
            modelBuilder.Entity<WorkflowData>().HasData(customerCreationScenario01WorkflowTuple);
            modelBuilder.Entity<WorkflowVersionData>().HasData(scenario01WorkflowVersiond);
            modelBuilder.Entity<WorkflowItemData>().HasData(scenario01ListWorkflowItemDataTuple);
            modelBuilder.Entity<WorkflowTestCasesData>().HasData(scenario01WorkflowTestCasesDataTuple);
            */
            #endregion [ Real scenario prioject : Do not delete ]


            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Modify the input SQL query by adding passed parameters
        /// </summary>
        /// <param name="sql">The raw SQL query</param>
        /// <param name="parameters">The values to be assigned to parameters</param>
        /// <returns>Modified raw SQL query</returns>
        protected virtual string CreateSqlWithParameters(string sql, params object[] parameters)
        {
            //add parameters to sql
            for (var i = 0; i <= (parameters?.Length ?? 0) - 1; i++)
            {
                if (!(parameters[i] is DbParameter parameter))
                {
                    continue;
                }

                sql = $"{sql}{(i > 0 ? "," : string.Empty)} @{parameter.ParameterName}";

                //whether parameter is output
                if (parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Output)
                {
                    sql = $"{sql} output";
                }
            }

            return sql;
        }

        #endregion Utilities

        #region Methods

        /// <summary>
        /// Creates a DbSet that can be used to query and save instances of entity
        /// </summary>
        /// <typeparam name="TData">data object type</typeparam>
        /// <returns>A set for the given entity type</returns>
        public new virtual DbSet<TData> Set<TData>()
            where TData : class, new()
        {
            return base.Set<TData>();
        }

        /// <summary>
        /// Generate a script to create all tables for the current model
        /// </summary>
        /// <returns>A SQL script</returns>
        public virtual string GenerateCreateScript()
        {
            return Database.GenerateCreateScript();
        }

        /// <inheritdoc />
        /// <summary>
        /// Create the database
        /// </summary>
        public bool CreateDatabase()
        {
            return Database.EnsureCreated();
        }

        public bool IsDatabaseUpdated()
        {
            return !Database.GetPendingMigrations().Any();
        }

        /// <inheritdoc />
        /// <summary>
        /// Update database based on migrations
        /// </summary>
        public void UpdateDatabase()
        {
            Database.Migrate();
        }

        /// <summary>
        /// Creates a LINQ query for the query type based on a raw SQL query
        /// </summary>
        /// <typeparam name="TQuery">Query type</typeparam>
        /// <param name="sql">The raw SQL query</param>
        /// <returns>An IQueryable representing the raw SQL query</returns>
        public virtual IQueryable<TQuery> QueryFromSql<TQuery>(string sql)
            where TQuery : class
        {
            return Query<TQuery>().FromSql(sql);
        }

        /// <summary>
        /// Creates a LINQ query for the entity based on a raw SQL query
        /// </summary>
        /// <typeparam name="TData">Data object type</typeparam>
        /// <param name="sql">The raw SQL query</param>
        /// <param name="parameters">The values to be assigned to parameters</param>
        /// <returns>An IQueryable representing the raw SQL query</returns>
        public virtual IQueryable<TData> EntityFromSql<TData>(string sql, params object[] parameters)
            where TData : class, IData, new()
        {
            return Set<TData>().FromSql(CreateSqlWithParameters(sql, parameters), parameters);
        }

        /// <summary>
        /// Executes the given SQL against the database
        /// </summary>
        /// <param name="sql">The SQL to execute</param>
        /// <param name="doNotEnsureTransaction">true - the transaction creation is not ensured; false - the transaction creation is ensured.</param>
        /// <param name="timeout">The timeout to use for command. Note that the command timeout is distinct from the connection timeout, which is commonly set on the database connection string</param>
        /// <param name="parameters">Parameters to use with the SQL</param>
        /// <returns>The number of rows affected</returns>
        public virtual int ExecuteSqlCommand(RawSqlString sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters)
        {
            //set specific command timeout
            var previousTimeout = Database.GetCommandTimeout();
            Database.SetCommandTimeout(timeout);

            var result = 0;
            if (!doNotEnsureTransaction && Transaction.Current == null)
            {
                //use with transaction
                using (var transaction = Database.BeginTransaction())
                {
                    result = Database.ExecuteSqlCommand(sql, parameters);
                    transaction.Commit();
                }
            }
            else
            {
                result = Database.ExecuteSqlCommand(sql, parameters);
            }

            //return previous timeout back
            Database.SetCommandTimeout(previousTimeout);

            return result;
        }

        /// <summary>
        /// Detach an entity from the context
        /// </summary>
        /// <typeparam name="TData">Data object type</typeparam>
        /// <param name="entity">Entity</param>
        public virtual void Detach<TData>(TData entity)
            where TData : class, IData, new()
        {
            if (entity == null)
            {
                throw new ValidationException(nameof(entity));
            }

            var entityEntry = Entry(entity);
            if (entityEntry == null)
            {
                return;
            }

            //set the entity is not being tracked by the context
            entityEntry.State = EntityState.Detached;
        }

        #endregion Methods
    }
}