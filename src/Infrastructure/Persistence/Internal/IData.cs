﻿using SenseAI.Domain.BaseModel;
using System;

namespace Persistence.Internal
{
    public interface IData : IProjectable
    {
        Guid Id { get; set; }
    }
}