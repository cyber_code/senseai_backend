﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Internal
{
    /// <summary>
    /// Represents base entity mapping configuration
    /// </summary>
    /// <typeparam name="TData">Entity type</typeparam>
    public class SenseAIEntityTypeWithSchemaConfiguration<TData> : IMappingConfiguration, IEntityTypeConfiguration<TData>
        where TData : class, new()
    {
        #region Utilities

        /// <summary>
        /// Developers can override this method in custom partial classes in order to add some custom configuration code
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        protected virtual void PostConfigure(EntityTypeBuilder<TData> builder)
        {
        }

        #endregion Utilities

        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public virtual void Configure(EntityTypeBuilder<TData> builder)
        {
            string tblName = typeof(TData).Name;
            var customTableAttribute = typeof(TData).GetCustomAttributes(false);
            if (customTableAttribute != null && customTableAttribute.Length > 0)
                tblName = ((TableAttribute)customTableAttribute[0]).Name;
            builder.ToTable(tblName, "AAProductBuilder");
            //add custom configuration
            PostConfigure(builder);
            if (typeof(BaseData).IsAssignableFrom(typeof(TData)))
            {
                builder.Property<DateTime?>("DateCreated").IsRequired(false);
                builder.Property<string>("UserCreated").IsRequired(false);
                builder.Property<DateTime?>("DateModified").IsRequired(false);
                builder.Property<string>("UserModified").IsRequired(false);
            }
        }

        /// <summary>
        /// Apply this mapping configuration
        /// </summary>
        /// <param name="modelBuilder">The builder being used to construct the model for the database context</param>
        public virtual void ApplyConfiguration(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(this);
        }

        #endregion Methods
    }
}