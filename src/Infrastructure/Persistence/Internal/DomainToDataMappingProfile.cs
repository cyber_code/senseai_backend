﻿using AutoMapper;
using Core.Abstractions;
using SenseAI.Domain.AdminModel;
using SenseAI.Domain.CatalogModel;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using system = SenseAI.Domain.SystemModel.System;
using SenseAI.Domain.SystemTags;
using process = SenseAI.Domain.Process.ProcessModel.Process;
using SenseAI.Domain.Process.IssuesModel;
using SenseAI.Domain.Process.HierarchiesModel;

using Persistence.AdminModel;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.DataSetsModel;
using Persistence.WorkflowModel;
using Persistence.WorkflowModel;

using Persistence.Process.ProcessModel;
using Persistence.Process.IssuesModel;
using Persistence.Process.HierarchiesModel;
using SenseAI.Domain.Process.ProcessModel;
using Persistence.SystemTagModel;
using SenseAI.Domain.Process.SprintsModel;
using Persistence.Process.SprintsModel;
using Persistence.SystemModel;
using SenseAI.Domain.PeopleModel;
using Persistence.PeopleModel;
using SenseAI.Domain.JiraModel;
using Persistence.Jira;
using SenseAI.Domain.TestCaseModel;
using Persistence.TestCaseModel;

namespace Persistence.Internal
{
    public class DomainToDataMappingProfile : Profile, IMapperProfile
    {
        public DomainToDataMappingProfile()
        {
            CreateMap<Workflow, WorkflowData>();
            CreateMap<WorkflowItem, WorkflowItemData>();
            CreateMap<WorkflowVersionPaths, WorkflowVersionPaths>();
            CreateMap<WorkflowVersionPathDetails, WorkflowVersionPathDetailsData>();

            CreateMap<Typical, TypicalData>();
            CreateMap<TypicalChanges, TypicalChangesData>();
            CreateMap<TypicalInstance, TypicalInstanceData>();
            CreateMap<AcceptedTypicalChanges, AcceptedTypicalChangesData>();

            CreateMap<Folder, FolderData>();
            CreateMap<Project, ProjectData>();
            CreateMap<SubProject, SubProjectData>();
            CreateMap<Settings, SettingsData>();
            CreateMap<Catalog, CatalogData>();
            CreateMap<system, SystemData>();
            CreateMap<SystemTag, SystemTagData>();
            CreateMap<DataSet, DataSetData>();
            CreateMap<DataSetItem, DataSetItemData>();
            CreateMap<Role, RoleData>();
            CreateMap<People, PeopleData>();
            CreateMap<process, ProcessData>();
            CreateMap<ProcessResources, ProcessResourcesData>();
            CreateMap<WorkflowTestCases, WorkflowTestCasesData>();
            CreateMap<ATSTestCases, ATSTestCasesData>();
            CreateMap<ATSTestSteps, ATSTestStepsData>();
            CreateMap<IssueTypeFields, IssueTypeFieldsData>();
            CreateMap<IssueTypeFieldsName, IssueTypeFieldsNameData>();
            CreateMap<IssueType, IssueTypeData>();
            CreateMap<Issue, IssueData>();
            CreateMap<IssueComment, IssueCommentData>();
            CreateMap<IssueResource, IssueResourceData>();

            CreateMap<Sprints, SprintsData>();
            CreateMap<SprintIssues, SprintIssuesData>();
            CreateMap<HierarchyType, HierarchyTypeData>();
            CreateMap<Hierarchy, HierarchyData>();
            CreateMap<JiraProjectMapping, JiraProjectMappingData>();
        }

        public int Order => 1;
    }
}