﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.TestCaseModel
{
    [Table("ATSTestCases")]
    public class ATSTestCasesData : BaseData
    {
        public ulong Noderef { get; set; }

        public string UId { get; set; }

        public Guid WorkflowId { get; set; }

        public Guid WorkflowPathId { get; set; }

        public int VersionId { get; set; }

        public Guid TestCaseId { get; set; }

        public Guid DataSetId { get; set; }

        public ulong DataSetNoderef { get; set; }

        public ulong ProjectNodref { get; set; }

        public ulong SubProjectNodref { get; set; }
    }
}
