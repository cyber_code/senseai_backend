﻿using SenseAI.Domain.WorkflowModel;
using Persistence.Internal;
using System;
using System.Linq;

namespace Persistence.WorkflowModel
{
    public sealed class TypicalInstanceRepository : ITypicalInstanceRepository
    {
        private readonly SenseAIObjectContext _context;

        public TypicalInstanceRepository(SenseAIObjectContext context)
        {
            _context = context;
        }

        public string Get(Guid typicalId)
        {
            var attributes = from tid in _context.Set<TypicalInstanceData>()
                             where tid.TypicalId == typicalId
                             select tid.Attributes;
            return attributes.FirstOrDefault();
        }
    }
}