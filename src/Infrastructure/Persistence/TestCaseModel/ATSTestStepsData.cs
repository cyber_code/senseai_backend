﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.TestCaseModel
{
    [Table("ATSTestSteps")]
    public class ATSTestStepsData : BaseData
    {
        public ulong Noderef { get; set; }

        public string UId { get; set; }

        public ulong TestCaseNoderef { get; set; }

        public Guid TestStepId { get; set; }

        public string TestStepJson { get; set; } 
    }
}
