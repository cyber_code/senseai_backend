﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("TypicalInstances")]
    public class TypicalInstanceData : BaseData, IData
    {
        public Guid Id { get; set; }

        public Guid TypicalId { get; set; }

        public string Attributes { get; set; }
    }
}