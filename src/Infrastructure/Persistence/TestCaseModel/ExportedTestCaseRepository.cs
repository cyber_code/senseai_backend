﻿using SenseAI.Domain.WorkflowModel;
using Persistence.Internal;
using System.Linq;
using System;
using Persistence.AdminModel;
using Persistence.Process.SprintsModel;

namespace Persistence.WorkflowModel
{
    public sealed class ExportedTestCaseRepository : IExportedTestCaseRepository
    {
        private readonly IBaseRepository<ExportedTestCase, ExportedTestCaseData> _baseRepository;
        private readonly IDbContext _dbContext;

        public ExportedTestCaseRepository(IBaseRepository<ExportedTestCase, ExportedTestCaseData> baseRepository, IDbContext dbContext)
        {
            _baseRepository = baseRepository;
            _dbContext = dbContext;
        }

        public void StoreExportedTestCaseReference(Guid workflowId, long testCaseId)
        {
            var exportedTestCase = new ExportedTestCase
            {
                WorkflowId = workflowId,
                TestCaseId = testCaseId,
                ExportTime = DateTime.Now,
                SprintId = getCurrentOpenSprint(workflowId),
            };
            _baseRepository.Add(exportedTestCase);
        }

        /// <summary>
        /// Returns the open (active) sprint in this workflow's subproject, or Guid.Empty if there is no active sprint.
        /// </summary>
        /// <param name="workflowId"></param>
        /// <returns></returns>
        private Guid getCurrentOpenSprint(Guid workflowId)
        {
            var subProjectId = (from workflow in _dbContext.Set<WorkflowData>()
                                join folder in _dbContext.Set<FolderData>() on workflow.FolderId equals folder.Id
                                where workflow.Id == workflowId
                                select folder.SubProjectId).FirstOrDefault();

            if (subProjectId == null || subProjectId.Equals(Guid.Empty))
                return Guid.Empty;

            var sprintId = (from sprint in _dbContext.Set<SprintsData>()
                            where sprint.SubProjectId == subProjectId
                                && sprint.Status == 1
                            select sprint.Id).FirstOrDefault();

            if (sprintId == null || sprintId.Equals(Guid.Empty))
                return Guid.Empty;
            return sprintId;
        }
    }
}