﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigTypicalInstance : SenseAIEntityTypeConfiguration<TypicalInstanceData>
    {
        public override void Configure(EntityTypeBuilder<TypicalInstanceData> builder)
        {
            builder.ToTable(typeof(TypicalInstanceData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.TypicalId).IsRequired();
            builder.Property(x => x.Attributes);

            base.Configure(builder);
        }
    }
}