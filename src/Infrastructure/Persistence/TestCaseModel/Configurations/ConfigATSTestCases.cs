﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;
using Persistence.TestCaseModel;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigATSTestCases : SenseAIEntityTypeConfiguration<ATSTestCasesData>
    {
        public override void Configure(EntityTypeBuilder<ATSTestCasesData> builder)
        {
            builder.ToTable(typeof(ATSTestCasesData).Name);
            builder.HasKey(x => x.Noderef);
            builder.Property(x => x.WorkflowId).IsRequired();
            builder.Property(x => x.WorkflowPathId).IsRequired();
            builder.Property(x => x.VersionId).IsRequired();
            builder.Property(x => x.TestCaseId).IsRequired();
            builder.Property(x => x.DataSetId).IsRequired();
            builder.Property(x => x.ProjectNodref).IsRequired();
            builder.Property(x => x.SubProjectNodref).IsRequired();

            base.Configure(builder);
        }
    }
}