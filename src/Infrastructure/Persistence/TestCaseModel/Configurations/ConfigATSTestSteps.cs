﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;
using Persistence.TestCaseModel;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigATSTestSteps : SenseAIEntityTypeConfiguration<ATSTestStepsData>
    {
        public override void Configure(EntityTypeBuilder<ATSTestStepsData> builder)
        {
            builder.ToTable(typeof(ATSTestStepsData).Name);
            builder.HasKey(x => x.Noderef);
            builder.Property(x => x.UId).IsRequired();
            builder.Property(x => x.TestCaseNoderef).IsRequired();
            builder.Property(x => x.TestStepId).IsRequired();

            base.Configure(builder);
        }
    }
}