﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigExecutionResult : SenseAIEntityTypeConfiguration<ExecutionResultData>
    {
        public override void Configure(EntityTypeBuilder<ExecutionResultData> builder)
        {
            builder.HasKey(t => new { t.WorkflowId, t.TestCaseId, t.TestCycleId, t.ExecutionTestCaseId });
            builder.Property(x => x.WorkflowId).IsRequired();
            builder.Property(x => x.TestCaseId).IsRequired();
            builder.Property(x => x.ExecutionTestCaseId).IsRequired();
            builder.Property(x => x.TestCaseTitle).IsRequired();
            builder.Property(x => x.TestCycleId).IsRequired();
            builder.Property(x => x.Status).IsRequired();

            base.Configure(builder);
        }
    }
}