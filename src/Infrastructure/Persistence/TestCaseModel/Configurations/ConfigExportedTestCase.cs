﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persistence.Internal;

namespace Persistence.WorkflowModel.Configurations
{
    internal class ConfigGeneratedTestCaseHistory : SenseAIEntityTypeConfiguration<ExportedTestCaseData>
    {
        public override void Configure(EntityTypeBuilder<ExportedTestCaseData> builder)
        {
            builder.ToTable(typeof(ExportedTestCaseData).Name);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.WorkflowId).IsRequired();
            builder.Property(x => x.TestCaseId).IsRequired();
            builder.Property(x => x.ExportTime).IsRequired();
            builder.Property(x => x.SprintId).IsRequired();

            base.Configure(builder);
        }
    }
}