﻿using SenseAI.Domain.WorkflowModel;
using Persistence.Internal;
using SenseAI.Domain.TestCaseModel;
using Core.Extensions;
using Persistence.TestCaseModel;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Persistence.WorkflowModel
{
    public sealed class ATSTestCasesRepository : IATSTestCasesRepository
    {
        private readonly IDbContext _dbContext;

        public ATSTestCasesRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(ATSTestCases atsTestCase)
        {
            var atsTestCasesData = atsTestCase.ProjectedAs<ATSTestCasesData>();
            if (_dbContext.Set<ATSTestCasesData>().AsNoTracking().FirstOrDefault(w => w.Noderef == atsTestCase.Noderef) == null)
                _dbContext.Set<ATSTestCasesData>().Add(atsTestCasesData);
            else
                _dbContext.Set<ATSTestCasesData>().Update(atsTestCasesData);


            var atsStepData = atsTestCase.Steps.ToList().Select(s => new ATSTestStepsData
            {
                Noderef = s.Noderef,
                UId = s.UId,                
                TestCaseNoderef = s.TestCaseNoderef,
                TestStepId = s.TestStepId,
                TestStepJson = Newtonsoft.Json.JsonConvert.SerializeObject(s)
            });

            var currentSteps = _dbContext.Set<ATSTestStepsData>().AsNoTracking().Where(w => w.TestCaseNoderef == atsTestCase.Noderef).Select(s => s.Noderef).ToList();
            if (currentSteps.Count() == 0)
                _dbContext.Set<ATSTestStepsData>().AddRange(atsStepData);
            else
            {
                var addSteps = atsStepData.Where(w => !currentSteps.Contains(w.Noderef));
                var updateSteps = atsStepData.Where(w => currentSteps.Contains(w.Noderef));

                _dbContext.Set<ATSTestStepsData>().AddRange(addSteps);
                _dbContext.Set<ATSTestStepsData>().UpdateRange(updateSteps);
            }

            _dbContext.SaveChanges();
        }
    }
}