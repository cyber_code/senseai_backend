﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("ExportedTestCases")]
    public class ExportedTestCaseData : BaseData, IData
    {
        public Guid Id { get; set; }
        public Guid WorkflowId { get; set; }
        public long TestCaseId { get; set; }
        public DateTime ExportTime { get; set; }
        public Guid SprintId { get; set; }
    }
}