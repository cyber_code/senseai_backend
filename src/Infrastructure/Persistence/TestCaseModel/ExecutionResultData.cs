﻿using Persistence.Internal;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.WorkflowModel
{
    [Table("ExecutionResults")]
    public class ExecutionResultData : BaseData
    {
        public Guid WorkflowId { get; set; }
        public long TestCaseId { get; set; }
        public string TestCaseTitle { get; set; }
        public ulong ExecutionTestCaseId { get; set; }
        public long TestCycleId { get; set; }
        public int Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid SprintId { get; set; }

        public int VersionId { get; set; }

        public Guid WorkflowPathId { get; set; }
    }
}
