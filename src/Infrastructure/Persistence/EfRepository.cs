﻿using Microsoft.EntityFrameworkCore;
using Core.Exceptions;
using Core.Extensions;
using SenseAI.Domain.BaseModel;
using Persistence.Internal;
using System;
using System.Linq;

namespace Persistence
{
    public class EfRepository<TEntity, TData> : IBaseRepository<TEntity, TData>
        where TEntity : BaseEntity
        where TData : class, IData, new()
    {
        private readonly IDbContext _dbContext;

        private DbSet<TData> _entities;

        public EfRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Gets a table
        /// </summary>
        public virtual IQueryable<TData> Table => Entities;

        /// <summary>
        /// Gets a table with "no tracking" enabled (EF feature) Use it only when you load record(s) only for read-only operations
        /// </summary>
        public virtual IQueryable<TData> TableNoTracking => Entities.AsNoTracking();

        /// <summary>
        /// Gets an entity set
        /// </summary>
        protected virtual DbSet<TData> Entities => _entities ?? (_entities = _dbContext.Set<TData>());

        public void Add(TEntity entity)
        {
            if (entity == null)
            {
                throw new ValidationException(nameof(entity));
            }
            try
            {
                var data = Map(entity);
                Entities.Add(data);
                _dbContext.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new SenseAIException(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }

        public void Delete(Guid id)
        {
            var data = new TData { Id = id };
            try
            {
                Entities.Attach(data);
                Entities.Remove(data);

                _dbContext.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                throw new SenseAIException(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }

        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual void Update(TEntity entity)
        {
            if (entity == null)
            {
                throw new ValidationException(nameof(entity));
            }

            try
            {
                var data = Map(entity);

                Entities.Update(data);
                _dbContext.SaveChanges();
            }
            catch (DbUpdateConcurrencyException exception)
            {
                var message = $"Entity with id {entity.Id} is already modified or deleted!";
                throw new SenseAIConflictException(message,
                    new SenseAIException(GetFullErrorTextAndRollbackEntityChanges(exception), exception));
            }
            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new SenseAIException(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }

        public virtual TData Map(TEntity entity)
        {
            return entity.ProjectedAs<TData>();
        }

        public IQueryable<TData> GetById(Guid id)
        {
            return TableNoTracking.Where(t => t.Id == id);
        }

        /// <summary>
        /// Rollback of entity changes and return full error message
        /// </summary>
        /// <param name="exception">ValidataException</param>
        /// <returns>Error message</returns>
        protected string GetFullErrorTextAndRollbackEntityChanges(DbUpdateException exception)
        {
            //rollback entity changes
            if (_dbContext is DbContext dbContext)
            {
                try
                {
                    var entries = dbContext.ChangeTracker.Entries()
                        .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified).ToList();

                    entries.ForEach(entry => entry.State = EntityState.Unchanged);
                }
                catch (Exception ex)
                {
                    exception = new DbUpdateException(exception.ToString(), ex);
                }
            }

            _dbContext.SaveChanges();
            return exception.ToString();
        }
    }
}