﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using ValidataT24Deployment;

namespace DeploymentConsole
{
    class Program
    {
        private const string ACCOUNT = "ACCOUNT";

        static void Main(string[] args)
        {
            if (args.Length==0)
            {
                var aadesigner = new AAProductDesigner();
                aadesigner.ID = "NEWACC-20190702";
                aadesigner.DESCRIPTION = "New account";
                aadesigner.PRODUCT_GROUP = "CURRENT.ACCOUNTS";
                aadesigner.CURRENCY.Add(new MVField { MVIndex = 1, SVIndex = 1, Value = "EUR" });
                aadesigner.PROPERTY_CONDITIONS.Add(new PropertyConditions(1, "ACCOUNTING", "ACCOUNTS", "TRACKING"));
                aadesigner.PROPERTY_CONDITIONS.Add(new PropertyConditions(2, "ACTIVITY.MAPPINGS", "ACCOUNTS", "TRACKING"));
                aadesigner.CALCULATION_SOURCE.Add(new CalculationsSource(1, "CRINTEREST", "CR.DAILY", "CURBALANCE"));
                aadesigner.CALCULATION_SOURCE.Add(new CalculationsSource(2, "DRINTEREST", "DR.MAXIMUM", "CURBALANCE"));
                var js = JsonConvert.SerializeObject(aadesigner);
                System.IO.File.WriteAllText(@"D:\VD-All\Developements\in.json", js);
            }
            else
            {
                var requestFilePath = args[0];

                var json = string.Empty;

                var jsonData = JsonConvert.DeserializeObject<List<InputClass>>(json);

                var AAPrdDesAccount = new AAPrdDesAccount
                {
                    BUS_DAY_CENTRES = new List<MVField>()
                };

                var account = jsonData.Where(w => w.PropertyClass == ACCOUNT);

                foreach (var item in account)
                {
                    switch (item.Field)
                    {
                        case "DESCRIPTION"://
                            AAPrdDesAccount.DESCRIPTION = item.Value;
                            break;
                        case "CATEGORY"://
                            AAPrdDesAccount.CATEGORY = item.Value;
                            break;
                        case "DATE.CONVENTION"://
                            AAPrdDesAccount.DATE_CONVENTION = item.Value;
                            break;
                        case "BUS.DAY.CENTRES"://
                            AAPrdDesAccount.BUS_DAY_CENTRES.Add(new MVField
                            {
                                MVIndex = item.MultiValueIndex,
                                SVIndex = item.SubValueIndex,
                                Value = item.Value
                            });
                            break;
                        case "PASSBOOK"://
                            AAPrdDesAccount.DESCRIPTION = item.Value;
                            break;
                        case "GENERATE.IBAN"://
                            AAPrdDesAccount.GENERATE_IBAN = item.Value;
                            break;
                        case "ACCOUNTING.COMPANY"://
                            AAPrdDesAccount.ACCOUNTING_COMPANY = item.Value;
                            break;
                        case "FULL.DESCRIPTION"://
                            AAPrdDesAccount.FULL_DESCRIPTION = item.Value;
                            break;
                        default:
                            break;
                    }                        
                }




                AAProductDesigner aadesigner = JsonConvert.DeserializeObject<AAProductDesigner>(System.IO.File.ReadAllText(requestFilePath));
                var process = new ProcessRequest();
                process.ProcessAAProductDesigner(aadesigner);
                Console.WriteLine("Successfully processed.");
            }
        }
    }
}
