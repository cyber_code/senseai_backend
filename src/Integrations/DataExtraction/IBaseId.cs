﻿using System;

namespace DataExtraction
{
    public interface IBaseId
    {
        Guid TenantId { get; set; }

        Guid ProjectId { get; set; }

        Guid SubProjectId { get; set; }

        Guid SystemId { get; }

        Guid CatalogId { get; set; }

        string HashId { get; }
    }
}