﻿using System;

namespace DataExtraction.JiraModel
{
    public class DeleteAttachmentPostModel
    {
        public DeleteAttachmentPostModel(Guid id, Guid issueId, string title, string externalSystemId)
        {
            Id = id;
            IssueId = issueId;
            Title = title;
            ExternalSystemId = externalSystemId;
        }

        public Guid Id { get; set; }

        public Guid IssueId { get; set; }

        public string Title { get; set; }

        public string ExternalSystemId { get; set; }
    }
}