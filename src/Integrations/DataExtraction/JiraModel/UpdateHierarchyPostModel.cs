﻿using System;

namespace DataExtraction.JiraModel
{
    public class UpdateHierarchyPostModel
    {
        public UpdateHierarchyPostModel(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}