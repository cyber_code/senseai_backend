﻿using System;

namespace DataExtraction.JiraModel
{
    public class UpdateRequirementStatusPostModel
    {
        public UpdateRequirementStatusPostModel(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}