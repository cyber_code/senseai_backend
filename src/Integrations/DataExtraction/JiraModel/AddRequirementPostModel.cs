﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataExtraction.JiraModel
{
    public class AddRequirementPostModel
    {
        public AddRequirementPostModel(Guid id, Guid requirementIssueTypeId, string[] peopleFields)
        {
            Id = id;
            RequirementIssueTypeId = requirementIssueTypeId;
            PeopleFields = peopleFields;
        }

        public Guid Id { get; set; }

        public Guid RequirementIssueTypeId { get; set; }

        public string[] PeopleFields { get; set; }
    }
}