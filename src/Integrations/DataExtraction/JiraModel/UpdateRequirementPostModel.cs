﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataExtraction.JiraModel
{
    public class UpdateRequirementPostModel
    {
        public UpdateRequirementPostModel(Guid id, Guid requirementIssueTypeId, string[] peopleFields)
        {
            Id = id;
            RequirementIssueTypeId = requirementIssueTypeId;
            PeopleFields = peopleFields;
        }

        public Guid Id { get; set; }

        public Guid RequirementIssueTypeId { get; set; }

        public string[] PeopleFields { get; set; }
    }
}