﻿using System;

namespace DataExtraction.JiraModel
{
    public class FirstTimeSyncModel
    {
        public FirstTimeSyncModel(Guid subprojectId, string username, string token, string url)
        {
            SubprojectId = subprojectId;
            Username = username;
            Token = token;
            Url = url;
        }

        public Guid SubprojectId { get; set; }

        public string Username { get; set; }

        public string Token { get; set; }

        public string Url { get; set; }
    }
}