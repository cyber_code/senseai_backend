﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataExtraction.JiraModel
{
    public class LinkPostModel
    {
        public Guid MainIssueId { get; set; }

        public Guid LinkedIssueId { get; set; }

        public string LinkName { get; set; }
    }
}