﻿using System;

namespace DataExtraction.JiraModel
{
    public class DeleteLinkPostModel
    {
        public DeleteLinkPostModel(Guid mainIssueId, Guid linkedIssueId, string linkName)
        {
            MainIssueId = mainIssueId;
            LinkedIssueId = linkedIssueId;
            LinkName = linkName;
        }

        public Guid MainIssueId { get; private set; }

        public Guid LinkedIssueId { get; private set; }

        public string LinkName { get; private set; }
    }
}