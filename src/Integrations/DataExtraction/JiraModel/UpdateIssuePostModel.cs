﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataExtraction.JiraModel
{
    public class UpdateIssuePostModel
    {
        public UpdateIssuePostModel(Guid id, Guid requirementIssueTypeId, string requirementLinkName, string[] peopleFields)
        {
            Id = id;
            RequirementIssueTypeId = requirementIssueTypeId;
            RequirementLinkName = requirementLinkName;
            PeopleFields = peopleFields;
        }

        public Guid Id { get; set; }

        public Guid RequirementIssueTypeId { get; set; }

        public string RequirementLinkName { get; set; }

        public string[] PeopleFields { get; set; }
    }
}