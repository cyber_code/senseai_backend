﻿using System;

namespace DataExtraction.JiraModel
{
    public class AddHierarchyPostModel
    {
        public AddHierarchyPostModel(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}