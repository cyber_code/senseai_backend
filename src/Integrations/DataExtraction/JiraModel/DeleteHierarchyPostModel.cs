﻿using System;

namespace DataExtraction.JiraModel
{
    public class DeleteHierarchyPostModel
    {
        public DeleteHierarchyPostModel(Guid id, Guid subProjectId, string externalSystemId)
        {
            Id = id;
            SubProjectId = subProjectId;
            ExternalSystemId = externalSystemId;
        }

        public Guid Id { get; }

        public Guid SubProjectId { get; }

        public string ExternalSystemId { get; }
    }
}