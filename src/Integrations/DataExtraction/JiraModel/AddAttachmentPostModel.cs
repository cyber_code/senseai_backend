﻿using Microsoft.AspNetCore.Http;
using System;

namespace DataExtraction.JiraModel
{
    public sealed class AddAttachmentPostModel
    {
        public AddAttachmentPostModel(Guid id, IFormFile fileCollection, bool isIssue)
        {
            Id = id;
            FileCollection = fileCollection;
            IsIssue = isIssue;
        }

        public Guid Id { get; set; }

        public IFormFile FileCollection { get; set; }

        public bool IsIssue { get; set; }
    }
}