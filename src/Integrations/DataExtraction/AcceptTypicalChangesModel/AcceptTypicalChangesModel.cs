﻿using Microsoft.AspNetCore.Http;
using SenseAI.Domain.DataModel;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace DataExtraction.AcceptTypicalChangesModel
{
    public class AcceptTypicalChangesModel : IBaseId
    {

        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }
        public Guid SubProjectId { get; set; }
        public Guid SystemId { get; set; }
        public Guid CatalogId { get; set; }
        public string HashId {
            get
            {
                return BitConverter.ToString(MD5.Create()
                    .ComputeHash(UTF8Encoding.UTF8.GetBytes(
                        TenantId.ToString() + "|" +
                        ProjectId.ToString() + "|" +
                        SubProjectId.ToString() + "|" +
                        SystemId.ToString() + "|" +
                        CatalogId.ToString()))).Replace("-", string.Empty);
            }
        }
       
        public Guid Id { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public bool HasAutomaticId { get; set; }
        public bool IsConfiguration { get; set; }

        public TypicalChangesAttributes[] Attributes { get; set; }
    }

   





}