﻿using System.Threading.Tasks;

namespace DataExtraction
{
    /// <summary>
    /// IRestClient
    /// </summary>
    internal interface IRestClient
    {
        /// <summary>
        /// Executes a GET-style request and callback asynchronously
        /// </summary>
        /// <typeparam name="T">Target deserialization type</typeparam>
        Task<T> GetAsync<T>(string uri);

        /// <summary>
        /// Executes a POST-style request and callback
        /// </summary>
        /// <param name="request">Request to be executed</param>
        /// <param name="uri">Api action URI</param>
        Task PostAsJsonAsync(object request, string uri);

        /// <summary>
        /// Executes a async POST-style request and callback asynchronously
        /// </summary>
        /// <param name="request">Request to be executed</param>
        /// <param name="uri">Api action URI</param>
        Task<T> PostAsJsonAsync<T>(object request, string uri);

        /// <summary>
        /// Executes a PUT-style request and callback
        /// </summary>
        /// <param name="request">Request to be executed</param>
        /// <param name="uri">Api action URI</param>
        Task<T> PutAsJsonAsync<T>(object request, string uri);

        /// <summary>
        /// Executes a PUT-style request and callback asynchronously
        /// </summary>
        /// <param name="request">Request to be executed</param>
        /// <param name="uri">Api action URI</param>
        Task PutAsJsonAsync(object request, string uri);
    }
}