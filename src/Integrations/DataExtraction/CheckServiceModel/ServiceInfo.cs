﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataExtraction.CheckServiceModel
{
    public class ServiceInfo
    {
        public string Title { get; set; }

        public bool Active { get; set; }

        public string Info { get; set; }
    }
}