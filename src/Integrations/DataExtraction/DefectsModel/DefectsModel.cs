﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace DataExtraction.Defects
{
    public class DefectsModel : IBaseId
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get { return Guid.Parse("b5bcc43f-afc8-4f67-8eb1-b633994ce363"); } }

        public Guid CatalogId { get; set; }

        public string HashId
        {
            get
            {
                return BitConverter.ToString(MD5.Create()
                    .ComputeHash(UTF8Encoding.UTF8.GetBytes(
                        TenantId.ToString() + "|" +
                        ProjectId.ToString() + "|" +
                        SubProjectId.ToString() + "|" +
                        SystemId.ToString() + "|" +
                        CatalogId.ToString()))).Replace("-", string.Empty);
            }
        }

        public IFormFile Files { get; set; }

        public List<ColumnMappingModel> Mapping { get; set; }

        public string Lang { get; set; }
    }

    public class ColumnMappingModel
    {
        public string OriginalField { get; set; }

        public string CsvField { get; set; }

        public int Index { get; set; }
    }
}