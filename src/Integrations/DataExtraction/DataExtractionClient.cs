﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json;
using Core.Abstractions.Bindings;
using Core.Configuration;
using DataExtraction.CheckServiceModel;
using DataExtraction.Defects;
using DataExtraction.JiraModel;
using Core.Abstractions;
using DataExtraction.ExecutionLogsModel;

namespace DataExtraction
{
    public class DataExtractionClient : RestClient, IDataExtractionClient
    {
        private readonly SenseAIConfig _configuration;
        private readonly IWorkContext _workContext;

        public DataExtractionClient(IDataExtractionBindingConfiguration bindingConfiguration, IWorkContext workContext, SenseAIConfig configuration)
            : base(bindingConfiguration, workContext)
        {
            _configuration = configuration;
        }

        private IDataExtractionBindingConfiguration BindingConfiguration => (IDataExtractionBindingConfiguration)_bindingConfiguration;

        public async Task<ExecutionLogsModel.ExecutionLogResponse> ExecutionLogs(ExecutionLogsModel.ExecutionLogsModel executionLogs)
        {
            var client = new HttpClient();
            var formDataContent = new MultipartFormDataContent();

            foreach (var file in executionLogs.Files)
            {
                var fileContent = new StreamContent(file.OpenReadStream())
                {
                    Headers =
                    {
                        ContentLength = file.Length,
                        ContentType = new MediaTypeHeaderValue(file.ContentType)
                    }
                };
                formDataContent.Add(fileContent, "files", file.FileName);
            }
            formDataContent.Add(new StringContent(executionLogs.TenantId.ToString()), "TenantId");
            formDataContent.Add(new StringContent(executionLogs.ProjectId.ToString()), "ProjectId");
            formDataContent.Add(new StringContent(executionLogs.SubProjectId.ToString()), "SubProjectId");
            formDataContent.Add(new StringContent(executionLogs.SystemId.ToString()), "SystemId");
            formDataContent.Add(new StringContent(executionLogs.CatalogId.ToString()), "CatalogId");
            formDataContent.Add(new StringContent(executionLogs.WorkflowId.ToString()), "WorkflowId");
            formDataContent.Add(new StringContent(executionLogs.SprintId.ToString()), "SprintId"); 
            return await PostAsync<ExecutionLogResponse>(formDataContent, $"{BindingConfiguration.Client.Endpoint}{"/api/t24/UploadExecutionLogs"}");
        }

        public List<string> GetColumnMapping()
        {
            var apiRes = GetAsync<String>($"{BindingConfiguration.Client.Endpoint}{"/api/t24/GetColumnMapping"}").Result;
            var response = JsonConvert.DeserializeObject<List<string>>(apiRes);

            return response;
        }

        public async Task<bool> SaveDefects(DefectsModel defectsModel)
        {
            //var client = new HttpClient();
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _workContext.AccessToken);
            var fileContent = new StreamContent(defectsModel.Files.OpenReadStream())
            {
                Headers =
                {
                    ContentLength = defectsModel.Files.Length,
                    ContentType = new MediaTypeHeaderValue(defectsModel.Files.ContentType)
                }
            };

            var formDataContent = new MultipartFormDataContent();
            formDataContent.Add(new StringContent(defectsModel.TenantId.ToString()), "TenantId");
            formDataContent.Add(new StringContent(defectsModel.ProjectId.ToString()), "ProjectId");
            formDataContent.Add(new StringContent(defectsModel.SubProjectId.ToString()), "SubProjectId");
            formDataContent.Add(new StringContent(defectsModel.SystemId.ToString()), "SystemId");
            formDataContent.Add(new StringContent(defectsModel.CatalogId.ToString()), "CatalogId");
            formDataContent.Add(fileContent, "files", defectsModel.Files.FileName);
            formDataContent.Add(new StringContent(JsonConvert.SerializeObject(defectsModel.Mapping)), "Mapping");
            formDataContent.Add(new StringContent(defectsModel.Lang.ToString()), "Lang");

            ////To be reviewed
            //var response = await client.PostAsync($"{BindingConfiguration.Client.Endpoint}{"/api/issues/SaveDefects"}",
            //    (MultipartFormDataContent)formDataContent);
            //return response.IsSuccessStatusCode;//
            return await PostAsync<bool>(formDataContent, $"{BindingConfiguration.Client.Endpoint}{"/api/issues/SaveDefects"}");
        }

        public List<ServiceInfo> CheckService()
        {
            var json = GetAsync<string>($"{BindingConfiguration.Client.Endpoint}{"/api/t24/Ping"}?ExtractionToolURL={BindingConfiguration.Client.Endpoint}").Result;
            var result = JsonConvert.DeserializeObject<List<ServiceInfo>>(json);
            if (result.Count > 0)
                result[0].Info = $"{BindingConfiguration.Client.Endpoint}{"/api/t24/Ping"}";

            return result;
        }

        public async Task<bool> AcceptedTypicalChanges(AcceptTypicalChangesModel.AcceptTypicalChangesModel acceptTypicalChangesModel)
        {
            var client = new HttpClient();

            var models = new[] { acceptTypicalChangesModel };

            return await PostAsJsonAsync<bool>(models, $"{BindingConfiguration.Client.Endpoint}{"/api/t24/AcceptedTypicalChanges"}");
        }
        public async Task<bool> AcceptedTypicalChanges(IEnumerable<AcceptTypicalChangesModel.AcceptTypicalChangesModel> acceptTypicalChangesModel)
        {
            var client = new HttpClient
            {
               
            };


            return await PostAsJsonAsync<bool>(acceptTypicalChangesModel, $"{BindingConfiguration.Client.Endpoint}{"/api/t24/AcceptedTypicalChanges"}");
        }
        public async Task<bool> AddIssue(AddIssuePostModel issuePostModel)
        {
            return await PostAsJsonAsync<bool>(issuePostModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/Add"}");
        }

        public async Task<bool> AddRequirement(AddRequirementPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/AddRequirement"}");
        }

        public async Task<bool> AddHierarchy(AddHierarchyPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/AddHierarchy"}");
        }

        public async Task<bool> AddAttachment(AddAttachmentPostModel postModel)
        {
            var formDataContent = new MultipartFormDataContent();

            var fileContent = new StreamContent(postModel.FileCollection.OpenReadStream())
            {
                Headers =
                {
                    ContentLength = postModel.FileCollection.Length,
                    ContentType = new MediaTypeHeaderValue(postModel.FileCollection.ContentType)
                }
            };
            formDataContent.Add(fileContent, "file", postModel.FileCollection.FileName);

            formDataContent.Add(new StringContent(postModel.Id.ToString()), "Id");
            formDataContent.Add(new StringContent(postModel.IsIssue.ToString()), "IsIssue");
            return await PostAsync<bool>(formDataContent, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/AddAttachment"}");
        }

        public async Task<bool> AddRequirementAttachment(AddAttachmentPostModel postModel)
        {
            var formDataContent = new MultipartFormDataContent();

            var fileContent = new StreamContent(postModel.FileCollection.OpenReadStream())
            {
                Headers =
                {
                    ContentLength = postModel.FileCollection.Length,
                    ContentType = new MediaTypeHeaderValue(postModel.FileCollection.ContentType)
                }
            };
            formDataContent.Add(fileContent, "file", postModel.FileCollection.FileName);

            formDataContent.Add(new StringContent(postModel.Id.ToString()), "Id");
            formDataContent.Add(new StringContent(postModel.IsIssue.ToString()), "IsIssue");
            return await PostAsync<bool>(formDataContent, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/AddRequirementAttachment"}");
        }

        public async Task<bool> UpdateIssue(UpdateIssuePostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/Update"}");
        }

        public async Task<bool> UpdateRequirement(UpdateRequirementPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/UpdateRequirement"}");
        }

        public async Task<bool> UpdateHierarchy(UpdateHierarchyPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/UpdateHierarchy"}");
        }

        public async Task<bool> UpdateRequirementStatus(UpdateRequirementStatusPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/UpdateRequirementStatus"}");
        }

        public async Task<bool> DeleteIssue(DeleteIssuePostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/Delete"}");
        }

        public async Task<bool> DeleteRequirement(DeleteRequirementPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/DeleteRequirement"}");
        }

        public async Task<bool> DeleteHierarchy(DeleteHierarchyPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/DeleteHierarchy"}");
        }

        public async Task<bool> DeleteAttachment(DeleteAttachmentPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/DeleteAttachment"}");
        }

        public async Task<bool> DeleteRequirementAttachment(DeleteAttachmentPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/DeleteRequirementAttachment"}");
        }

        public async Task<bool> LinkIssues(LinkPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/Link"}");
        }

        public async Task<bool> DeleteLink(DeleteLinkPostModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/DeleteLink"}");
        }

        public async Task<bool> FirstTimeSync(FirstTimeSyncModel postModel)
        {
            return await PostAsJsonAsync<bool>(postModel, $"{BindingConfiguration.Client.Endpoint}{"/api/jira/FirstTimeSync"}");
        }
    }
}