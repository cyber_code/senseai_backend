﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using DataExtraction.CheckServiceModel;
using DataExtraction.Defects;
using DataExtraction.JiraModel;
using DataExtraction.ExecutionLogsModel;

namespace DataExtraction
{
    public interface IDataExtractionClient
    {
        Task<ExecutionLogResponse> ExecutionLogs(ExecutionLogsModel.ExecutionLogsModel executionLogs);

        Task<bool> SaveDefects(DefectsModel defectsModel);

        List<string> GetColumnMapping();

        Task<bool> AcceptedTypicalChanges(AcceptTypicalChangesModel.AcceptTypicalChangesModel acceptTypicalChangesModel);
        Task<bool> AcceptedTypicalChanges(IEnumerable<AcceptTypicalChangesModel.AcceptTypicalChangesModel> acceptTypicalChangesModel);
        List<ServiceInfo> CheckService();

        Task<bool> AddIssue(AddIssuePostModel postModel);

        Task<bool> AddRequirement(AddRequirementPostModel postModel);

        Task<bool> AddHierarchy(AddHierarchyPostModel postModel);

        Task<bool> AddAttachment(AddAttachmentPostModel postModel);

        Task<bool> AddRequirementAttachment(AddAttachmentPostModel postModel);

        Task<bool> UpdateIssue(UpdateIssuePostModel postModel);

        Task<bool> UpdateRequirement(UpdateRequirementPostModel postModel);

        Task<bool> UpdateHierarchy(UpdateHierarchyPostModel postModel);

        Task<bool> UpdateRequirementStatus(UpdateRequirementStatusPostModel postModel);

        Task<bool> DeleteIssue(DeleteIssuePostModel postModel);

        Task<bool> DeleteRequirement(DeleteRequirementPostModel postModel);

        Task<bool> DeleteHierarchy(DeleteHierarchyPostModel postModel);

        Task<bool> DeleteAttachment(DeleteAttachmentPostModel postModel);

        Task<bool> DeleteRequirementAttachment(DeleteAttachmentPostModel postModel);

        Task<bool> LinkIssues(LinkPostModel postModel);

        Task<bool> DeleteLink(DeleteLinkPostModel postModel);

        Task<bool> FirstTimeSync(FirstTimeSyncModel postModel);
    }
}