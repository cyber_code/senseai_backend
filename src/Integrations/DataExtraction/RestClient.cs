﻿using Newtonsoft.Json;
using Core.Abstractions.Bindings;
using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Core.Abstractions;

namespace DataExtraction
{
    /// <summary>
    ///     Client to create Http requests and process response result
    /// </summary>
    ///

    public class RestClient : IRestClient
    {
        #region [ Properties ]

        protected readonly IBindingConfiguration _bindingConfiguration;
        private readonly IWorkContext _workContext;

        private HttpClient _client;

        private HttpClient Client => _client ?? (_client = new HttpClient(handler));

        #endregion [ Properties ]

        #region [ Initialization ]

        private HttpClientHandler handler = new HttpClientHandler();

        private int size = 1000;

        /// <inheritdoc />
        /// <summary>
        ///     Sets the api binging propertys for requests made by this client instance
        /// </summary>
        /// <param name="bindingConfiguration"></param>
        protected RestClient(IBindingConfiguration bindingConfiguration, IWorkContext workContext)
        {
            _bindingConfiguration = bindingConfiguration;
            _workContext = workContext;
            ConfigurateHttpClient();
        }

        private void ConfigurateHttpClient()
        {
            if (string.IsNullOrEmpty(_bindingConfiguration.Client.Endpoint))
            {
                throw new Exception("BaseAddress/Endpoint is required!");
            }

            Client.BaseAddress = new Uri(_bindingConfiguration.Client.Endpoint);
            Client.Timeout = _bindingConfiguration.Client.Timeout == 0 ? new TimeSpan(0, 0, 0, 10) : new TimeSpan(0, 0, 0, _bindingConfiguration.Client.Timeout);

            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Range = new RangeHeaderValue(0, size);

            if (_bindingConfiguration.Headers != null)
            {
                //Add here other header config's
                Client.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse(_bindingConfiguration.Headers.ContentType));
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _workContext.AccessToken?.Replace("Bearer ", string.Empty));
            }
        }

        #endregion [ Initialization ]

        #region [ IRestClient: Implementation]

        /// <inheritdoc />
        public Task<T> GetAsync<T>(string uri)
        {
            return InvokeAsync<T>(
                client => client.GetAsync(uri),
                response => response.Content.ReadAsAsync<T>());
        }

        /// <inheritdoc />
        public Task<T> PostAsJsonAsync<T>(object data, string uri)
        {
            var content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");
            return InvokeAsync<T>(
                client => client.PostAsync(uri, content),
                response => response.Content.ReadAsAsync<T>());
        }

        /// <inheritdoc />
        public Task<T> PostAsJsonAsync2<T>(object data, string uri)
        {
            return InvokeAsync<T>(
                client => client.PostAsync(uri, data, new JsonMediaTypeFormatter()),
                response => response.Content.ReadAsAsync<T>());
        }

        /// <inheritdoc />
        public Task PostAsJsonAsync(object data, string uri)
        {
            return InvokeAsync<object>(client => client.PostAsJsonAsync(uri, data));
        }

        public Task<T> PostAsync<T>(object data, string uri)
        {
            return InvokeAsync<T>(client => client.PostAsync(uri, (MultipartFormDataContent)data), 
                                  response => response.Content.ReadAsAsync<T>());
        }

        /// <inheritdoc />
        public Task PutAsJsonAsync(object data, string uri)
        {
            return InvokeAsync<object>(client => client.PutAsJsonAsync(uri, data));
        }

        /// <inheritdoc />
        public Task<T> PutAsJsonAsync<T>(object data, string uri)
        {
            return InvokeAsync<T>(
                client => client.PutAsJsonAsync(uri, data),
                response => response.Content.ReadAsAsync<T>());
        }

        #endregion [ IRestClient: Implementation]

        private async Task<T> InvokeAsync<T>(
            Func<HttpClient, Task<HttpResponseMessage>> operation,
            Func<HttpResponseMessage, Task<T>> actionOnResponse = null)
        {
            //TODO: Add ignore ssl TLS 2 certificate
            if (operation == null)
                throw new ArgumentNullException(nameof(operation));
            HttpResponseMessage response;

            try
            {
                response = await operation(Client).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                throw new Exception($"Error connectiong to resource server!{Environment.NewLine}Error message: '{e.Message}'");
            }

            if (!response.IsSuccessStatusCode)
            {
                var exception = new Exception($"Resource server returned an error. StatusCode : {response.StatusCode}");
                exception.Data.Add("StatusCode", response.StatusCode);
                throw exception;
            }

            return actionOnResponse != null ? await actionOnResponse(response).ConfigureAwait(false) : default(T);
        }
    }
}