﻿using External.QualitySuite.ApiModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading.Tasks;

namespace External.QualitySuite
{
    [DebuggerStepThrough()]
    internal partial class RepositoryServiceClient : ClientBase<IRepositoryService>, IRepositoryService
    {
        public RepositoryServiceClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        public Task<ValidataLoginInformation> GetLoginItemsUsingCredentialsAsync(Credentials credentials)
        {
            return base.Channel.GetLoginItemsUsingCredentialsAsync(credentials);
        }

        public Task<SessionKey> ValidataLoginAsync(Credentials credentials)
        {
            return base.Channel.ValidataLoginAsync(credentials);
        }

        public Task<SessionKey> ValidataInitLoginAsync(Credentials credentials)
        {
            return base.Channel.ValidataInitLoginAsync(credentials);
        }

        public Task<bool> IsSessionValidAsync(SessionKey session)
        {
            return base.Channel.IsSessionValidAsync(session);
        }

        public Task CloseSessionAsync(SessionKey session)
        {
            return base.Channel.CloseSessionAsync(session);
        }

        public Task<ValidataLoginInformation> GetValidataProjectsAsync(Credentials credentials)
        {
            return base.Channel.GetValidataProjectsAsync(credentials);
        }

        public Task<ValidataLoginInformation> GetValidataSubProjectsAsync(Credentials credentials)
        {
            return base.Channel.GetValidataSubProjectsAsync(credentials);
        }

        public Task<ValidataLoginInformation> GetValidataDisciplinesAsync(Credentials credentials)
        {
            return base.Channel.GetValidataDisciplinesAsync(credentials);
        }

        public Task<ValidataLoginInformation> GetValidataRolesAsync(Credentials credentials)
        {
            return base.Channel.GetValidataRolesAsync(credentials);
        }

        public Task<ValidataLoginInformation> GetValidataUsersAsync(Credentials credentials)
        {
            return base.Channel.GetValidataUsersAsync(credentials);
        }

        public Task<List<RepositoryServiceErrorMessages>> GetErrorAsync(SessionKey session)
        {
            return base.Channel.GetErrorAsync(session);
        }

        public Task<List<ValidataItem>> GetValidataCataloguesListAsync(SessionKey sessionKey)
        {
            return base.Channel.GetValidataCataloguesListAsync(sessionKey);
        }

        public Task<List<ValidataItem>> GetValidataTypicalsAsync(SessionKey sessionKey, string catalogName)
        {
            return base.Channel.GetValidataTypicalsAsync(sessionKey, catalogName);
        }

        public Task<List<ValidataItem>> GetAAProductsListAsync(SessionKey sessionKey, string catalogName)
        {
            return base.Channel.GetAAProductsListAsync(sessionKey, catalogName);
        }

        public Task<string> GetDefaultCatalogueApplicationAsync(SessionKey sessionKey)
        {
            return base.Channel.GetDefaultCatalogueApplicationAsync(sessionKey);
        }

        public Task<string> GetDefaultCatalogueEnquiryAsync(SessionKey sessionKey)
        {
            return base.Channel.GetDefaultCatalogueEnquiryAsync(sessionKey);
        }

        public Task<string> GetDefaultCatalogueAAAsync(SessionKey sessionKey)
        {
            return base.Channel.GetDefaultCatalogueAAAsync(sessionKey);
        }

        public Task<bool> SetDefaultCatalogueApplicationAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultCatalogueApplicationAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultCatalogueEnqueryAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultCatalogueEnqueryAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultCatalogueAAAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultCatalogueAAAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultCatalogueBrowserMultiAppAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultCatalogueBrowserMultiAppAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultEdgeCatalogueApplicationAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultEdgeCatalogueApplicationAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultEdgeCatalogueEnqueryAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultEdgeCatalogueEnqueryAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultEdgeCatalogueAAAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultEdgeCatalogueAAAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultEdgeCatalogueBrowserMultiAppAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultEdgeCatalogueBrowserMultiAppAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultCatalogueTripleAAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultCatalogueTripleAAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultEnquiryCatalogueTripleAAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultEnquiryCatalogueTripleAAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultCatalogueTCIBAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultCatalogueTCIBAsync(sessionKey, catalogueNoderef);
        }

        public Task<bool> SetDefaultCatalogueTCMBAsync(SessionKey sessionKey, string catalogueNoderef)
        {
            return base.Channel.SetDefaultCatalogueTCMBAsync(sessionKey, catalogueNoderef);
        }

        public Task<SaveTestCaseResponse> SaveTestCaseAsync(SaveTestCaseRequest request)
        {
            return base.Channel.SaveTestCaseAsync(request);
        }

        public Task<TestFolder> CreateTestFolderAsync(SessionKey sessionKey, TestFolder testFolder, string parentFolderNoderef)
        {
            return base.Channel.CreateTestFolderAsync(sessionKey, testFolder, parentFolderNoderef);
        }

        public Task<List<ValidataItemWithParent>> GetTestFolderTreeAsync(SessionKey sessionKey)
        {
            return base.Channel.GetTestFolderTreeAsync(sessionKey);
        }

        public Task<List<ValidataItemWithParent>> GetTestFoldersSingleLevelAsync(SessionKey sessionKey, ValidataItemWithParent parent)
        {
            return base.Channel.GetTestFoldersSingleLevelAsync(sessionKey, parent);
        }

        public Task<List<ValidataItemWithParent>> GetChildTestCasesAsync(SessionKey sessionKey, string parentTestFolderNoderef)
        {
            return base.Channel.GetChildTestCasesAsync(sessionKey, parentTestFolderNoderef);
        }

        public Task<List<ValidataItemWithParent>> GetChildTestStepsAsync(SessionKey sessionKey, string parentTestCaseNoderef)
        {
            return base.Channel.GetChildTestStepsAsync(sessionKey, parentTestCaseNoderef);
        }

        public Task<List<TestStep>> GetChildTestStepsCompleteAsync(SessionKey sessionKey, string parentTestCaseNoderef)
        {
            return base.Channel.GetChildTestStepsCompleteAsync(sessionKey, parentTestCaseNoderef);
        }

        public Task<List<ValidataItem>> GetValidataSystemsListAsync(SessionKey sessionKey)
        {
            return base.Channel.GetValidataSystemsListAsync(sessionKey);
        }

        public Task<List<string>> GetAttibutesNamesAsync(SessionKey sessionKey, string catalogName, string typicalName)
        {
            return base.Channel.GetAttibutesNamesAsync(sessionKey, catalogName, typicalName);
        }

        public Task<string> GetTypicalBaseClassAsync(SessionKey sessionKey, string catalogName, string typicalName)
        {
            return base.Channel.GetTypicalBaseClassAsync(sessionKey, catalogName, typicalName);
        }

        public Task<ValidataItem> CreateOrUpdateTypicalAsync(SessionKey sessionKey, string catalogName, string typicalName, string baseClass, List<string> typicalAttributeNames)
        {
            return base.Channel.CreateOrUpdateTypicalAsync(sessionKey, catalogName, typicalName, baseClass, typicalAttributeNames);
        }

        public Task<TryCompileFormulaResponse> TryCompileFormulaAsync(TryCompileFormulaRequest request)
        {
            return base.Channel.TryCompileFormulaAsync(request);
        }

        public Task<string> EvaluateAutomaticCalculationAsync(SessionKey sessionKey, AutomaticCalculation automaticCalculation)
        {
            return base.Channel.EvaluateAutomaticCalculationAsync(sessionKey, automaticCalculation);
        }

        public Task<List<ValidataItem>> GetScriptLibrarianWebApplicationsListAsync(SessionKey sessionKey)
        {
            return base.Channel.GetScriptLibrarianWebApplicationsListAsync(sessionKey);
        }

        public Task<Application> GetScriptLibrarianApplicationAsync(SessionKey sessionKey, string noderef, bool loadChildrenObjects)
        {
            return base.Channel.GetScriptLibrarianApplicationAsync(sessionKey, noderef, loadChildrenObjects);
        }

        public Task<List<Script>> SaveScriptsAsync(SessionKey sessionKey, List<Script> scripts)
        {
            return base.Channel.SaveScriptsAsync(sessionKey, scripts);
        }

        public Task<List<Application>> GetScriptLibrarianScriptsAndApplicationsAsync(SessionKey sessionKey, List<ulong> scriptNoderefs)
        {
            return base.Channel.GetScriptLibrarianScriptsAndApplicationsAsync(sessionKey, scriptNoderefs);
        }

        public Task<RM_Requirement> SaveRequirementAsync(SessionKey sessionKey, RM_Requirement requirement, string parentUseCaseNoderef)
        {
            return base.Channel.SaveRequirementAsync(sessionKey, requirement, parentUseCaseNoderef);
        }

        public Task<RM_UseCase> SaveUseCaseAsync(SessionKey sessionKey, RM_UseCase useCase, string parentFeatureNoderef)
        {
            return base.Channel.SaveUseCaseAsync(sessionKey, useCase, parentFeatureNoderef);
        }

        public Task<RM_Feature> SaveFeatureAsync(SessionKey sessionKey, RM_Feature feature, string parentSubProjectNoderef, string parentProductNoderef, string parentBANoderef)
        {
            return base.Channel.SaveFeatureAsync(sessionKey, feature, parentSubProjectNoderef, parentProductNoderef, parentBANoderef);
        }

        public Task<RM_Product> SaveProductAsync(SessionKey sessionKey, RM_Product product, string parentSubProject)
        {
            return base.Channel.SaveProductAsync(sessionKey, product, parentSubProject);
        }

        public Task<RM_BusinessArea> SaveBusinessAreaAsync(SessionKey sessionKey, RM_BusinessArea businessArea, string parentSubProject)
        {
            return base.Channel.SaveBusinessAreaAsync(sessionKey, businessArea, parentSubProject);
        }

        public Task<bool> AssociateRequirementToTestCaseAsync(SessionKey sessionKey, string requirementNoderef, string testCaseNoderef)
        {
            return base.Channel.AssociateRequirementToTestCaseAsync(sessionKey, requirementNoderef, testCaseNoderef);
        }

        public Task<bool> CheckIfTypicalNameExistsAsync(SessionKey sessionKey, string catalogName, string typicalName)
        {
            return base.Channel.CheckIfTypicalNameExistsAsync(sessionKey, catalogName, typicalName);
        }

        public Task<string> GetTypicalNameFromDescriptionAsync(SessionKey sessionKey, string catalogName, string typicalDescription)
        {
            return base.Channel.GetTypicalNameFromDescriptionAsync(sessionKey, catalogName, typicalDescription);
        }

        public Task<List<ValidataItem>> GetProductsAsync(SessionKey sessionKey, ulong subProjectId)
        {
            return base.Channel.GetProductsAsync(sessionKey, subProjectId);
        }
        public Task<List<ValidataItem>> GetAdaptersAsync(SessionKey sessionKey, ulong subProjectId)
        {
            return base.Channel.GetAdaptersAsync(sessionKey, subProjectId);
        }
        public Task<Dictionary<string, List<ValidataItem>>> GetAdaptersPerSystemsAsync(SessionKey sessionKey, ulong subProjectId)
        {
            return base.Channel.GetAdaptersPerSystemsAsync(sessionKey, subProjectId);
        }

        public Task<List<ValidataItemWithParent>> GetFoldersAndFeaturesAsync(SessionKey sessionKey, ulong productId)
        {
            return base.Channel.GetFoldersAndFeaturesAsync(sessionKey, productId);
        }

        public Task<List<ValidataItem>> GetFeaturesAsync(SessionKey sessionKey, ulong productId, ulong folderId)
        {
            return base.Channel.GetFeaturesAsync(sessionKey, productId, folderId);
        }

        public Task<List<ValidataItemWithParent>> GetFoldersAndUseCasesAsync(SessionKey sessionKey, ulong featureId)
        {
            return base.Channel.GetFoldersAndUseCasesAsync(sessionKey, featureId);
        }

        public Task<List<ValidataItem>> GetUseCasesAsync(SessionKey sessionKey, ulong featureId, ulong folderId)
        {
            return base.Channel.GetUseCasesAsync(sessionKey, featureId, folderId);
        }

        public Task<List<ValidataItem>> GetRequirementsAsync(SessionKey sessionKey, ulong useCaseId)
        {
            return base.Channel.GetRequirementsAsync(sessionKey, useCaseId);
        }

        public Task<RM_ReleaseSummary> GetReleaseSummaryAsync(SessionKey sessionKey, ulong releaseIterationId)
        {
            return base.Channel.GetReleaseSummaryAsync(sessionKey, releaseIterationId);
        }

        public virtual Task OpenAsync()
        {
            return Task.Factory.FromAsync(((ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((ICommunicationObject)(this)).EndOpen));
        }

        public virtual Task CloseAsync()
        {
            return Task.Factory.FromAsync(((ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((ICommunicationObject)(this)).EndClose));
        }
    }
}