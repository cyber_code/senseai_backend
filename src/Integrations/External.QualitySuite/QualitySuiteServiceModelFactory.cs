﻿//using External.QualitySuite.ApiModel;
//using Queries.DesignModule.GeneratedATSObjects;
//using System;
//using System.Collections.Generic;
//using System.Linq;

//namespace External.QualitySuite
//{
//    public static class QualitySuiteServiceModelFactory
//    {
//        public static TestCase TestCaseFactory(GeneratedTestCase generatedTestCase)
//        {
//            return new TestCase
//            {
//                Name = generatedTestCase.Title,
//                Description = generatedTestCase.Description,
//                Steps = generatedTestCase.TestSteps.Select(TestStepFactory).ToList<object>()
//            };
//        }

//        #region [ Test Step ]
//        public static TestStep TestStepFactory(GeneratedTestStep generatedTestStep, int stepIndex)
//        {
//            return new TestStep()
//            {
//                Noderef = 0,
//                UID = stepIndex.ToString(),
//                Name = generatedTestStep.Title,
//                Description = generatedTestStep.Description,
//                Configuration = generatedTestStep.Parameters,
//                SystemId = generatedTestStep.SystemName,
//                Adapter = generatedTestStep.AdapterName,

//                StepIndex = stepIndex,
//                ManualOrAuto = EnumsExecutionType.Automatic,
//                Priority = EnumsTestStepPriority.Medium,
//                LogicalDay = 1,
//                Filters = FiltersFactory(generatedTestStep),

//                ExpectedResult = string.Empty,
//                ExternalRef = string.Empty,
//                UserInterface = TestStepUserInterfaceType.T24TestBuilder,
//                Type = (EnumsTestStepType)Enum.Parse(typeof(EnumsTestStepType), generatedTestStep.Type),
//                AttributesWithCustomCurrencyComparison = new List<string> { },
//                AutomaticCalculations = AutomaticCalculationFactory(generatedTestStep.AutomaticCalculations),
//                EndCaseOnError = 1,
//                TestData = TestDataFactory(generatedTestStep)
//            };
//        }

//        private static ValidataRecord TestDataFactory(GeneratedTestStep generatedTestStep)
//        {
//            var testData = new ValidataRecord()
//            {
//                CatalogName = generatedTestStep.CatalogueName,
//                TypicalName = generatedTestStep.TypicaleName
//            };

//            if (generatedTestStep.TestData != null)
//            {
//                foreach (string key in generatedTestStep.TestData.Keys)
//                {
//                    testData.AddAttribute(key, generatedTestStep.TestData[key]);
//                }
//            }

//            return testData;
//        }

//        private static List<object> FiltersFactory(GeneratedTestStep generatedTestStep)
//        {
//            List<object> filters = new List<object>();

//            if (generatedTestStep.Filters != null)
//            {
//                generatedTestStep.Filters.ForEach(generatedFilter => filters.Add(new Filter()
//                {
//                    AttributeName = generatedFilter.AttributeName,
//                    Value = generatedFilter.Value,
//                    Type = FilterType.Equal,
//                    IsMask = generatedFilter.IsMaskField,
//                    IsPostFilter = generatedFilter.IsPostFilterField,
//                    AutomaticCalculations = AutomaticCalculationFactory(generatedFilter.AutomaticCalculations)
//                }));
//            }

//            return filters;
//        }

//        #region [ Automatic Calculation ]

//        private static List<object> AutomaticCalculationFactory(List<GeneratedTestStepAutomaticCalculation> generatedTestStepAutomaticCalculations)
//        {
//            var automaticCalculations = new List<object>();

//            generatedTestStepAutomaticCalculations?.ForEach(automaticCalculation =>
//            {
//                automaticCalculations.Add(new AutomaticCalculation
//                {
//                    Noderef = 0,
//                    AttributeName = automaticCalculation.AttributeName,
//                    PlainTextFormula = automaticCalculation.Formula,
//                    Items = AutomaticCalculationItemsFactory(automaticCalculation.Items)

//                });
//            });

//            return automaticCalculations;
//        }

//        private static List<object> AutomaticCalculationItemsFactory(List<GeneratedTestStepAutomaticCalculationItem> generatedTestStepAutomaticCalculationItem)
//        {
//            List<object> result = new List<object>();

//            if (generatedTestStepAutomaticCalculationItem != null)
//            {
//                foreach (var item in generatedTestStepAutomaticCalculationItem)
//                {
//                    result.Add(new AutomaticCalculationItem
//                    {
//                        CatalogName = item.CatalogName,
//                        TypicalName = item.TypicalName,
//                        AttributeName = item.AttributeName,
//                        CollectFrom = CollectionSource.Response,
//                        DataType = ExpressionType.Integer,
//                        VariableName = item.VariableName,
//                        SourceTestStepID = item.SourceTestStepID,
//                        SourceAttributeDataType = AttributeDataType.Integer
//                    });
//                }
//            }

//            return result;
//        }

//        #endregion

//        #endregion
//    }
//}