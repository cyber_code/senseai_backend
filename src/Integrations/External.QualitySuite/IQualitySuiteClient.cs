﻿using Core.Abstractions.Bindings;
using External.QualitySuite.ApiModel;
using System.Collections.Generic;

namespace External.QualitySuite
{
    public interface IQualitySuiteClient
    {
        IQualitySuiteBindingConfiguration BindingConfiguration { get; }

        IQualitySuiteClient CreateClient(Credentials credentials, ulong projectNoderef, ulong subProjectNoderef);

        IQualitySuiteClient CreateClient(Credentials credentials);

        List<ValidataItem> GetSystemsList();

        string GetTestFolderId(string testFolderName, string testFolderDescription);

        string GetTestSubFolderId(string parentNoderef, string testFolderName, string testFolderDescription);

        TestFolder SaveTestFolder(string testFolderName, string testFolderDescription);

        TestFolder SaveTestSubFolder(string testFolderName, string testFolderDescription, string parentNoderef);

        SaveTestCaseResponse SaveTestCase(string testFolderId, TestCase scriptedTestCase);

        string SaveRequirement(string name, string description);

        bool AssociateRequirementToTestCase(string requirementNoderef, string testCaseNoderef);

        List<RepositoryServiceErrorMessages> GetError();

        List<ValidataItem> GetProjectList();

        List<ValidataItem> GetSubProjectList(string projectItem);
        List<ValidataItem> GetSystems();

        List<ValidataItem> GetCatalogs();

        List<ValidataItem> GetProducts(ulong subProjectId);
        List<ValidataItem> GetAdapters(ulong subProjectId);
        Dictionary<string, List<ValidataItem>> GetAdaptersPerSystems(ulong subProjectId);
        List<ValidataItemWithParent> GetFeatures(ulong productId);

        List<ValidataItem> GetFeatures(ulong productId, ulong folderId);

        List<ValidataItemWithParent> GetUseCases(ulong featureId);

        List<ValidataItem> GetUseCases(ulong featureId, ulong folderId);

        List<ValidataItem> GetRequirements(ulong useCaseId);

        RM_ReleaseSummary GetReleaseSummary(ulong releaseSummaryId);
    }
}