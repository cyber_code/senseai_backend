﻿using System.Diagnostics;
using System.ServiceModel;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [MessageContract(WrapperName = "SaveTestCaseResponse", WrapperNamespace = "http://tempuri.org/", IsWrapped = true)]
    public partial class SaveTestCaseResponse
    {
        [MessageBodyMember(Namespace = "http://tempuri.org/", Order = 0)]
        public TestCase SaveTestCaseResult;

        [MessageBodyMember(Namespace = "http://tempuri.org/", Order = 1)]
        public int stepsWithErrorsCount;

        public SaveTestCaseResponse()
        {
        }

        public SaveTestCaseResponse(TestCase SaveTestCaseResult, int stepsWithErrorsCount)
        {
            this.SaveTestCaseResult = SaveTestCaseResult;
            this.stepsWithErrorsCount = stepsWithErrorsCount;
        }
    }
}