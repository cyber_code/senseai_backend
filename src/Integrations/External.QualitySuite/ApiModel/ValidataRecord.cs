﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "ValidataRecord", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    public partial class ValidataRecord : object
    {
        private Dictionary<string, ValidataRecordAttribute> _Attributes = new Dictionary<string, ValidataRecordAttribute>();

        private string CatalogNameField;

        private string TypicalNameField;

        [DataMember()]
        public List<ValidataRecordAttribute> AttributesList_WcfProperty_DoNotUse
        {
            get
            {
                List<ValidataRecordAttribute> list = new List<ValidataRecordAttribute>();
                foreach (ValidataRecordAttribute validataRecordAttribute in Attributes)
                {
                    list.Add(validataRecordAttribute);
                }
                return list;
            }
            set
            {
                _Attributes = new Dictionary<string, ValidataRecordAttribute>();
                foreach (ValidataRecordAttribute validataRecordAttribute in value)
                {
                    AddAttribute(validataRecordAttribute.Name, validataRecordAttribute.DisplayName, validataRecordAttribute.Value);
                }
            }
        }

        [DataMember()]
        public string CatalogName
        {
            get
            {
                return this.CatalogNameField;
            }
            set
            {
                this.CatalogNameField = value;
            }
        }

        [DataMember()]
        public string TypicalName
        {
            get
            {
                return this.TypicalNameField;
            }
            set
            {
                this.TypicalNameField = value;
            }
        }

        [DataMember()]
        public IEnumerable<ValidataRecordAttribute> Attributes
        {
            get { return _Attributes.Values; }
        }

        public void AddAttribute(string name, string value)
        {
            _Attributes[name] = new ValidataRecordAttribute { Name = name, Value = value };
        }

        public void AddAttribute(string name, string displayName, string value)
        {
            _Attributes[name] = new ValidataRecordAttribute { Name = name, Value = value, DisplayName = displayName };
        }
    }
}