﻿using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "RepositoryServiceErrorMessages", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    public partial class RepositoryServiceErrorMessages : object
    {
        private RepositoryServiceErrorMessages.MessageType CurrentMessageTypeField;

        private string ExceptionMessageField;

        private string ExceptionStackTraceField;

        private string InnerExceptionMessageField;

        private string MessageField;

        [DataMember()]
        public RepositoryServiceErrorMessages.MessageType CurrentMessageType
        {
            get
            {
                return this.CurrentMessageTypeField;
            }
            set
            {
                this.CurrentMessageTypeField = value;
            }
        }

        [DataMember()]
        public string ExceptionMessage
        {
            get
            {
                return this.ExceptionMessageField;
            }
            set
            {
                this.ExceptionMessageField = value;
            }
        }

        [DataMember()]
        public string ExceptionStackTrace
        {
            get
            {
                return this.ExceptionStackTraceField;
            }
            set
            {
                this.ExceptionStackTraceField = value;
            }
        }

        [DataMember()]
        public string InnerExceptionMessage
        {
            get
            {
                return this.InnerExceptionMessageField;
            }
            set
            {
                this.InnerExceptionMessageField = value;
            }
        }

        [DataMember()]
        public string Message
        {
            get
            {
                return this.MessageField;
            }
            set
            {
                this.MessageField = value;
            }
        }

        [DataContract(Name = "RepositoryServiceErrorMessages.MessageType", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
        public enum MessageType : int
        {
            [EnumMember()]
            Info = 0,

            [EnumMember()]
            Debug = 1,

            [EnumMember()]
            Error = 2,

            [EnumMember()]
            Exception = 3,
        }
    }
}