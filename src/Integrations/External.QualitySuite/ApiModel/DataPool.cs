﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContractAttribute(Name = "DataPool", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    [KnownType(typeof(CollectionSource))]
    [KnownType(typeof(ExpressionType))]
    [KnownType(typeof(AttributeDataType))]
    [KnownType(typeof(ApplicationType))]
    [KnownType(typeof(TestingParameterType))]
    [KnownType(typeof(EnumsExecutionType))]
    [KnownType(typeof(EnumsTestStepPriority))]
    [KnownType(typeof(EnumsTestStepType))]
    [KnownType(typeof(TestStepUserInterfaceType))]
    [KnownType(typeof(FilterType))]
    [KnownType(typeof(List<object>))]
    [KnownType(typeof(List<string>))]
    [KnownType(typeof(List<ulong>))]
    [KnownType(typeof(Application))]
    [KnownType(typeof(List<Screen>))]
    [KnownType(typeof(Screen))]
    [KnownType(typeof(List<Control>))]
    [KnownType(typeof(Control))]
    [KnownType(typeof(WebMap))]
    [KnownType(typeof(List<Script>))]
    [KnownType(typeof(Script))]
    [KnownType(typeof(List<ScriptElement>))]
    [KnownType(typeof(ScriptElement))]
    [KnownType(typeof(List<ElementParam>))]
    [KnownType(typeof(ElementParam))]
    [KnownType(typeof(List<Application>))]
    [KnownType(typeof(Credentials))]
    [KnownType(typeof(ValidataLoginInformation))]
    [KnownType(typeof(ValidataLoginInformation.InformationType))]
    [KnownType(typeof(List<ValidataItem>))]
    [KnownType(typeof(ValidataItem))]
    [KnownType(typeof(SessionKey))]
    [KnownType(typeof(List<RepositoryServiceErrorMessages>))]
    [KnownType(typeof(RepositoryServiceErrorMessages))]
    [KnownType(typeof(RepositoryServiceErrorMessages.MessageType))]
    [KnownType(typeof(TestCase))]
    [KnownType(typeof(TestStep))]
    [KnownType(typeof(Filter))]
    [KnownType(typeof(ValidataRecord))]
    [KnownType(typeof(List<ValidataRecordAttribute>))]
    [KnownType(typeof(ValidataRecordAttribute))]
    [KnownType(typeof(AutomaticCalculation))]
    [KnownType(typeof(AutomaticCalculationItem))]
    [KnownType(typeof(DataPoolItem))]
    [KnownType(typeof(TestFolder))]
    [KnownType(typeof(List<ValidataItemWithParent>))]
    [KnownType(typeof(ValidataItemWithParent))]
    [KnownType(typeof(List<TestStep>))]
    [KnownType(typeof(RM_Requirement))]
    [KnownType(typeof(RM_UseCase))]
    [KnownType(typeof(RM_Feature))]
    [KnownType(typeof(RM_Product))]
    [KnownType(typeof(RM_BusinessArea))]
    public partial class DataPool : object
    {
        private List<object> DataPoolItemsField;

        private string NameField;

        private ulong NoderefField;

        [DataMember()]
        public List<object> DataPoolItems
        {
            get
            {
                return this.DataPoolItemsField;
            }
            set
            {
                this.DataPoolItemsField = value;
            }
        }

        [DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }

        [DataMemberAttribute()]
        public ulong Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }
    }
}