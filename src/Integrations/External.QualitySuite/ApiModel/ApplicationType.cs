﻿using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContract(Name = "ApplicationType", Namespace = "http://schemas.datacontract.org/2004/07/ValidataCommon.Interfaces.ScriptLibrarian")]
    public enum ApplicationType : int
    {
        [EnumMember()]
        Desktop = 0,

        [EnumMember()]
        Web = 1,

        [EnumMember()]
        T24 = 2,
    }
}