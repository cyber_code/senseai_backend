﻿using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContract(Name = "TestStepUserInterfaceType", Namespace = "http://schemas.datacontract.org/2004/07/ValidataCommon")]
    public enum TestStepUserInterfaceType : int
    {
        [EnumMember()]
        Classic = 0,

        [EnumMember()]
        T24TestBuilder = 1,

        [EnumMember()]
        SwiftBuilder = 2,
    }
}