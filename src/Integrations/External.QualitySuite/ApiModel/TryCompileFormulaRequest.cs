﻿using System.Diagnostics;
using System.ServiceModel;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [MessageContract(WrapperName = "TryCompileFormula", WrapperNamespace = "http://tempuri.org/", IsWrapped = true)]
    public partial class TryCompileFormulaRequest
    {
        [MessageBodyMember(Namespace = "http://tempuri.org/", Order = 0)]
        public SessionKey sessionKey;

        [MessageBodyMember(Namespace = "http://tempuri.org/", Order = 1)]
        public AutomaticCalculation ac;

        public TryCompileFormulaRequest()
        {
        }

        public TryCompileFormulaRequest(SessionKey sessionKey, AutomaticCalculation ac)
        {
            this.sessionKey = sessionKey;
            this.ac = ac;
        }
    }
}