﻿using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "Credentials", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    public partial class Credentials : object
    {
        private string DisciplineNoderefField;

        private ulong LicenseCodeField;

        private string LicenseTypeField;

        private uint MachineIDField;

        private string PasswordField;

        private string ProjectNoderefField;

        private string RoleNoderefField;

        private string SubProjectNoderefField;

        private string UserField;

        [DataMember()]
        public string DisciplineNoderef
        {
            get
            {
                return this.DisciplineNoderefField;
            }
            set
            {
                this.DisciplineNoderefField = value;
            }
        }

        [DataMember()]
        public ulong LicenseCode
        {
            get
            {
                return this.LicenseCodeField;
            }
            set
            {
                this.LicenseCodeField = value;
            }
        }

        [DataMember()]
        public string LicenseType
        {
            get
            {
                return this.LicenseTypeField;
            }
            set
            {
                this.LicenseTypeField = value;
            }
        }

        [DataMember()]
        public uint MachineID
        {
            get
            {
                return this.MachineIDField;
            }
            set
            {
                this.MachineIDField = value;
            }
        }

        [DataMember()]
        public string Password
        {
            get
            {
                return this.PasswordField;
            }
            set
            {
                this.PasswordField = value;
            }
        }

        [DataMember()]
        public string ProjectNoderef
        {
            get
            {
                return this.ProjectNoderefField;
            }
            set
            {
                this.ProjectNoderefField = value;
            }
        }

        [DataMember()]
        public string RoleNoderef
        {
            get
            {
                return this.RoleNoderefField;
            }
            set
            {
                this.RoleNoderefField = value;
            }
        }

        [DataMember()]
        public string SubProjectNoderef
        {
            get
            {
                return this.SubProjectNoderefField;
            }
            set
            {
                this.SubProjectNoderefField = value;
            }
        }

        [DataMember()]
        public string User
        {
            get
            {
                return this.UserField;
            }
            set
            {
                this.UserField = value;
            }
        }
    }
}