﻿using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContract(Name = "FilterType", Namespace = "http://schemas.datacontract.org/2004/07/ValidataCommon")]
    public enum FilterType : int
    {
        [EnumMember()]
        GreaterThan = 0,

        [EnumMember()]
        GreaterOrEqual = 1,

        [EnumMember()]
        Equal = 2,

        [EnumMember()]
        LessOrEqual = 3,

        [EnumMember()]
        Less = 4,

        [EnumMember()]
        Like = 5,

        [EnumMember()]
        NotEqual = 6,

        [EnumMember()]
        NotLike = 7,

        [EnumMember()]
        TemplateName = 8,

        [EnumMember()]
        Between = 9,
    }
}