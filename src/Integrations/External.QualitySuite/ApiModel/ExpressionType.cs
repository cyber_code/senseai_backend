﻿using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContract(Name = "ExpressionType", Namespace = "http://schemas.datacontract.org/2004/07/ValidataCommon.Interfaces")]
    public enum ExpressionType : int
    {
        [EnumMember()]
        Unsupported = 0,

        [EnumMember()]
        Real = 1,

        [EnumMember()]
        Currency = 2,

        [EnumMember()]
        Integer = 3,

        [EnumMember()]
        Date = 4,

        [EnumMember()]
        String = 5,

        [EnumMember()]
        Boolean = 6,

        [EnumMember()]
        Custom = 7,
    }
}