﻿using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "AutomaticCalculationItem", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    public partial class AutomaticCalculationItem : object
    {
        private string AttributeNameField;

        private string CatalogNameField;

        private CollectionSource CollectFromField;

        private ExpressionType DataTypeField;

        private ulong NoderefField;

        private AttributeDataType SourceAttributeDataTypeField;

        private string SourceAttributeValueField;

        private string SourceTestStepIDField;

        private string TypicalNameField;

        private string VariableNameField;

        [DataMember()]
        public string AttributeName
        {
            get
            {
                return this.AttributeNameField;
            }
            set
            {
                this.AttributeNameField = value;
            }
        }

        [DataMember()]
        public string CatalogName
        {
            get
            {
                return this.CatalogNameField;
            }
            set
            {
                this.CatalogNameField = value;
            }
        }

        [DataMember()]
        public CollectionSource CollectFrom
        {
            get
            {
                return this.CollectFromField;
            }
            set
            {
                this.CollectFromField = value;
            }
        }

        [DataMember()]
        public ExpressionType DataType
        {
            get
            {
                return this.DataTypeField;
            }
            set
            {
                this.DataTypeField = value;
            }
        }

        [DataMember()]
        public ulong Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }

        [DataMember()]
        public AttributeDataType SourceAttributeDataType
        {
            get
            {
                return this.SourceAttributeDataTypeField;
            }
            set
            {
                this.SourceAttributeDataTypeField = value;
            }
        }

        [DataMember()]
        public string SourceAttributeValue
        {
            get
            {
                return this.SourceAttributeValueField;
            }
            set
            {
                this.SourceAttributeValueField = value;
            }
        }

        [DataMember()]
        public string SourceTestStepID
        {
            get
            {
                return this.SourceTestStepIDField;
            }
            set
            {
                this.SourceTestStepIDField = value;
            }
        }

        [DataMember()]
        public string TypicalName
        {
            get
            {
                return this.TypicalNameField;
            }
            set
            {
                this.TypicalNameField = value;
            }
        }

        [DataMember()]
        public string VariableName
        {
            get
            {
                return this.VariableNameField;
            }
            set
            {
                this.VariableNameField = value;
            }
        }
    }
}