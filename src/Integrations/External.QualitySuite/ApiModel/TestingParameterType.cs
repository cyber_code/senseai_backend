﻿using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContract(Name = "TestingParameterType", Namespace = "http://schemas.datacontract.org/2004/07/ValidataCommon.Interfaces.ScriptLibrarian")]
    public enum TestingParameterType : int
    {
        [EnumMember()]
        Constant = 1,

        [EnumMember()]
        InstanceID = 2,

        [EnumMember()]
        Attribute = 3,

        [EnumMember()]
        Constraint = 4,
    }
}