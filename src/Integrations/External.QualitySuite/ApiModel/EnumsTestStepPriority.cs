﻿using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContract(Name = "Enums.TestStepPriority", Namespace = "http://schemas.datacontract.org/2004/07/ValidataCommon")]
    public enum EnumsTestStepPriority : int
    {
        [EnumMember()]
        Low = 1,

        [EnumMember()]
        Medium = 2,

        [EnumMember()]
        High = 3,

        [EnumMember()]
        Critical = 4,
    }
}