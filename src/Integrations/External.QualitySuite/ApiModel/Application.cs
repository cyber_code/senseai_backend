﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "Application", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces.ScriptLibrarianObjects")]
    public partial class Application : object
    {
        private ApplicationType ApplicationTypeField;

        private string CatalogManagerField;

        private string CatalogNameField;

        private string DescriptionField;

        private string NameField;

        private string NoderefField;

        private List<Screen> ScreensListField;

        private List<Script> ScriptsListField;

        [DataMember()]
        public ApplicationType ApplicationType
        {
            get
            {
                return this.ApplicationTypeField;
            }
            set
            {
                this.ApplicationTypeField = value;
            }
        }

        [DataMember()]
        public string CatalogManager
        {
            get
            {
                return this.CatalogManagerField;
            }
            set
            {
                this.CatalogManagerField = value;
            }
        }

        [DataMember()]
        public string CatalogName
        {
            get
            {
                return this.CatalogNameField;
            }
            set
            {
                this.CatalogNameField = value;
            }
        }

        [DataMember()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }

        [DataMember()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }

        [DataMember()]
        public string Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }

        [DataMember()]
        public List<Screen> ScreensList
        {
            get
            {
                return this.ScreensListField;
            }
            set
            {
                this.ScreensListField = value;
            }
        }

        [DataMember()]
        public List<Script> ScriptsList
        {
            get
            {
                return this.ScriptsListField;
            }
            set
            {
                this.ScriptsListField = value;
            }
        }
    }
}