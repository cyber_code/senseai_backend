﻿using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContract(Name = "AttributeDataType", Namespace = "http://schemas.datacontract.org/2004/07/Validata.Common")]
    public enum AttributeDataType : uint
    {
        [EnumMember()]
        Integer = 0,

        [EnumMember()]
        Real = 1,

        [EnumMember()]
        String = 2,

        [EnumMember()]
        Boolean = 3,

        [EnumMember()]
        Currency = 4,

        [EnumMember()]
        DateTime = 5,

        [EnumMember()]
        Blob = 6,

        [EnumMember()]
        Measure = 7,

        [EnumMember()]
        Array = 8,

        [EnumMember()]
        Unknown = 9,
    }
}