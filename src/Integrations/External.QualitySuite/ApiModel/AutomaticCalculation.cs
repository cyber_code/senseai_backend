﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "AutomaticCalculation", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    [KnownType(typeof(CollectionSource))]
    [KnownType(typeof(ExpressionType))]
    [KnownType(typeof(AttributeDataType))]
    [KnownType(typeof(ApplicationType))]
    [KnownType(typeof(TestingParameterType))]
    [KnownType(typeof(EnumsExecutionType))]
    [KnownType(typeof(EnumsTestStepPriority))]
    [KnownType(typeof(EnumsTestStepType))]
    [KnownType(typeof(TestStepUserInterfaceType))]
    [KnownType(typeof(FilterType))]
    [KnownType(typeof(List<object>))]
    [KnownType(typeof(List<string>))]
    [KnownType(typeof(List<ulong>))]
    [KnownType(typeof(Application))]
    [KnownType(typeof(List<Screen>))]
    [KnownType(typeof(Screen))]
    [KnownType(typeof(List<Control>))]
    [KnownType(typeof(Control))]
    [KnownType(typeof(WebMap))]
    [KnownType(typeof(List<Script>))]
    [KnownType(typeof(Script))]
    [KnownType(typeof(List<ScriptElement>))]
    [KnownType(typeof(ScriptElement))]
    [KnownType(typeof(List<ElementParam>))]
    [KnownType(typeof(ElementParam))]
    [KnownType(typeof(List<Application>))]
    [KnownType(typeof(Credentials))]
    [KnownType(typeof(ValidataLoginInformation))]
    [KnownType(typeof(ValidataLoginInformation.InformationType))]
    [KnownType(typeof(List<ValidataItem>))]
    [KnownType(typeof(ValidataItem))]
    [KnownType(typeof(SessionKey))]
    [KnownType(typeof(List<RepositoryServiceErrorMessages>))]
    [KnownType(typeof(RepositoryServiceErrorMessages))]
    [KnownType(typeof(RepositoryServiceErrorMessages.MessageType))]
    [KnownType(typeof(TestCase))]
    [KnownType(typeof(TestStep))]
    [KnownType(typeof(Filter))]
    [KnownType(typeof(ValidataRecord))]
    [KnownType(typeof(List<ValidataRecordAttribute>))]
    [KnownType(typeof(ValidataRecordAttribute))]
    [KnownType(typeof(AutomaticCalculationItem))]
    [KnownType(typeof(DataPool))]
    [KnownType(typeof(DataPoolItem))]
    [KnownType(typeof(TestFolder))]
    [KnownType(typeof(List<ValidataItemWithParent>))]
    [KnownType(typeof(ValidataItemWithParent))]
    [KnownType(typeof(List<TestStep>))]
    [KnownType(typeof(RM_Requirement))]
    [KnownType(typeof(RM_UseCase))]
    [KnownType(typeof(RM_Feature))]
    [KnownType(typeof(RM_Product))]
    [KnownType(typeof(RM_BusinessArea))]
    public partial class AutomaticCalculation : object
    {
        private string AttributeNameField;

        private List<object> ItemsField;

        private ulong NoderefField;

        private string PlainTextFormulaField;

        private string PreserveResultFunctionNameField;

        public System.Guid DynamicDataId { get; set; }

        [DataMember()]
        public string AttributeName
        {
            get
            {
                return this.AttributeNameField;
            }
            set
            {
                this.AttributeNameField = value;
            }
        }

        [DataMember()]
        public List<object> Items
        {
            get
            {
                return this.ItemsField;
            }
            set
            {
                this.ItemsField = value;
            }
        }

        [DataMember()]
        public ulong Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }

        [DataMember()]
        public string PlainTextFormula
        {
            get
            {
                return this.PlainTextFormulaField;
            }
            set
            {
                this.PlainTextFormulaField = value;
            }
        }

        [DataMember()]
        public string PreserveResultFunctionName
        {
            get
            {
                return this.PreserveResultFunctionNameField;
            }
            set
            {
                this.PreserveResultFunctionNameField = value;
            }
        }
    }
}