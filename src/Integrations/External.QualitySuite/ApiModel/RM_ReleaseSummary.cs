﻿using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "RM_ReleaseSummary", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    public partial class RM_ReleaseSummary : object
    {
        private int TotalTestCaseExecutionsField;
        private int PassedTestCaseExecutionsField;
        private int OutstandingDefectsField;

        [DataMember]
        public int TotalTestCaseExecutions
        {
            get
            {
                return this.TotalTestCaseExecutionsField;
            }
            set
            {
                this.TotalTestCaseExecutionsField = value;
            }
        }

        [DataMember]
        public int PassedTestCaseExecutions
        {
            get
            {
                return this.PassedTestCaseExecutionsField;
            }
            set
            {
                this.PassedTestCaseExecutionsField = value;
            }
        }

        [DataMember]
        public int OutstandingDefects
        {
            get
            {
                return this.OutstandingDefectsField;
            }
            set
            {
                this.OutstandingDefectsField = value;
            }
        }
    }
}