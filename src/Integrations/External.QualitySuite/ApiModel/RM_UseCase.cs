﻿using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "RM_UseCase", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    public partial class RM_UseCase : object
    {
        private string DescriptionField;

        private string EstimatedNumberTestCasesField;

        private string NameField;

        private ulong NoderefField;

        private string PostConditionsField;

        private string PreConditionsField;

        private string RequirementPriorityField;

        private string UIDField;

        [DataMember()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }

        [DataMember()]
        public string EstimatedNumberTestCases
        {
            get
            {
                return this.EstimatedNumberTestCasesField;
            }
            set
            {
                this.EstimatedNumberTestCasesField = value;
            }
        }

        [DataMember()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }

        [DataMember()]
        public ulong Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }

        [DataMember()]
        public string PostConditions
        {
            get
            {
                return this.PostConditionsField;
            }
            set
            {
                this.PostConditionsField = value;
            }
        }

        [DataMember()]
        public string PreConditions
        {
            get
            {
                return this.PreConditionsField;
            }
            set
            {
                this.PreConditionsField = value;
            }
        }

        [DataMember()]
        public string RequirementPriority
        {
            get
            {
                return this.RequirementPriorityField;
            }
            set
            {
                this.RequirementPriorityField = value;
            }
        }

        [DataMember()]
        public string UID
        {
            get
            {
                return this.UIDField;
            }
            set
            {
                this.UIDField = value;
            }
        }
    }
}