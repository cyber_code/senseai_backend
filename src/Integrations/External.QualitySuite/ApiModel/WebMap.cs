﻿using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "WebMap", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces.ScriptLibrarianObjects")]
    public partial class WebMap : object
    {
        private string DOMPathField;

        private string HtmlIDField;

        private string HtmlNameField;

        private string HtmlTagField;

        private string ModelIDField;

        private string NoderefField;

        private string PageTitleField;

        private string PageURLField;

        [DataMember()]
        public string DOMPath
        {
            get
            {
                return this.DOMPathField;
            }
            set
            {
                this.DOMPathField = value;
            }
        }

        [DataMember()]
        public string HtmlID
        {
            get
            {
                return this.HtmlIDField;
            }
            set
            {
                this.HtmlIDField = value;
            }
        }

        [DataMember()]
        public string HtmlName
        {
            get
            {
                return this.HtmlNameField;
            }
            set
            {
                this.HtmlNameField = value;
            }
        }

        [DataMember()]
        public string HtmlTag
        {
            get
            {
                return this.HtmlTagField;
            }
            set
            {
                this.HtmlTagField = value;
            }
        }

        [DataMember()]
        public string ModelID
        {
            get
            {
                return this.ModelIDField;
            }
            set
            {
                this.ModelIDField = value;
            }
        }

        [DataMember()]
        public string Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }

        [DataMember()]
        public string PageTitle
        {
            get
            {
                return this.PageTitleField;
            }
            set
            {
                this.PageTitleField = value;
            }
        }

        [DataMember()]
        public string PageURL
        {
            get
            {
                return this.PageURLField;
            }
            set
            {
                this.PageURLField = value;
            }
        }
    }
}