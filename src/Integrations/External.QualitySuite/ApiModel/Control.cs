﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "Control", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces.ScriptLibrarianObjects")]
    [KnownType(typeof(CollectionSource))]
    [KnownType(typeof(ExpressionType))]
    [KnownType(typeof(AttributeDataType))]
    [KnownType(typeof(ApplicationType))]
    [KnownType(typeof(TestingParameterType))]
    [KnownType(typeof(EnumsExecutionType))]
    [KnownType(typeof(EnumsTestStepPriority))]
    [KnownType(typeof(EnumsTestStepType))]
    [KnownType(typeof(TestStepUserInterfaceType))]
    [KnownType(typeof(FilterType))]
    [KnownType(typeof(List<object>))]
    [KnownType(typeof(List<string>))]
    [KnownType(typeof(List<ulong>))]
    [KnownType(typeof(Application))]
    [KnownType(typeof(List<Screen>))]
    [KnownType(typeof(Screen))]
    [KnownType(typeof(List<Control>))]
    [KnownType(typeof(WebMap))]
    [KnownType(typeof(List<Script>))]
    [KnownType(typeof(Script))]
    [KnownType(typeof(List<ScriptElement>))]
    [KnownType(typeof(ScriptElement))]
    [KnownType(typeof(List<ElementParam>))]
    [KnownType(typeof(ElementParam))]
    [KnownType(typeof(List<Application>))]
    [KnownType(typeof(Credentials))]
    [KnownType(typeof(ValidataLoginInformation))]
    [KnownType(typeof(ValidataLoginInformation.InformationType))]
    [KnownType(typeof(List<ValidataItem>))]
    [KnownType(typeof(ValidataItem))]
    [KnownType(typeof(SessionKey))]
    [KnownType(typeof(List<RepositoryServiceErrorMessages>))]
    [KnownType(typeof(RepositoryServiceErrorMessages))]
    [KnownType(typeof(RepositoryServiceErrorMessages.MessageType))]
    [KnownType(typeof(TestCase))]
    [KnownType(typeof(TestStep))]
    [KnownType(typeof(Filter))]
    [KnownType(typeof(ValidataRecord))]
    [KnownType(typeof(List<ValidataRecordAttribute>))]
    [KnownType(typeof(ValidataRecordAttribute))]
    [KnownType(typeof(AutomaticCalculation))]
    [KnownType(typeof(AutomaticCalculationItem))]
    [KnownType(typeof(DataPool))]
    [KnownType(typeof(DataPoolItem))]
    [KnownType(typeof(TestFolder))]
    [KnownType(typeof(List<ValidataItemWithParent>))]
    [KnownType(typeof(ValidataItemWithParent))]
    [KnownType(typeof(List<TestStep>))]
    [KnownType(typeof(RM_Requirement))]
    [KnownType(typeof(RM_UseCase))]
    [KnownType(typeof(RM_Feature))]
    [KnownType(typeof(RM_Product))]
    [KnownType(typeof(RM_BusinessArea))]
    public partial class Control : object
    {
        private string DescriptionField;

        private string ElementTypeField;

        private string IDField;

        private string NameField;

        private string NoderefField;

        private string ParentScreenNameField;

        private object WebMapField;

        [DataMember()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }

        [DataMember()]
        public string ElementType
        {
            get
            {
                return this.ElementTypeField;
            }
            set
            {
                this.ElementTypeField = value;
            }
        }

        [DataMember()]
        public string ID
        {
            get
            {
                return this.IDField;
            }
            set
            {
                this.IDField = value;
            }
        }

        [DataMember()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }

        [DataMember()]
        public string Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }

        [DataMember()]
        public string ParentScreenName
        {
            get
            {
                return this.ParentScreenNameField;
            }
            set
            {
                this.ParentScreenNameField = value;
            }
        }

        [DataMember()]
        public object WebMap
        {
            get
            {
                return this.WebMapField;
            }
            set
            {
                this.WebMapField = value;
            }
        }
    }
}