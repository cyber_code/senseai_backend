﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "Script", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces.ScriptLibrarianObjects")]
    public partial class Script : object
    {
        private string DescriptionField;

        private string IDField;

        private string NameField;

        private string NoderefField;

        private string ParentApplicationNoderefField;

        private List<ScriptElement> ScriptElementsListField;

        [DataMember()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }

        [DataMember()]
        public string ID
        {
            get
            {
                return this.IDField;
            }
            set
            {
                this.IDField = value;
            }
        }

        [DataMember()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }

        [DataMember()]
        public string Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }

        [DataMember()]
        public string ParentApplicationNoderef
        {
            get
            {
                return this.ParentApplicationNoderefField;
            }
            set
            {
                this.ParentApplicationNoderefField = value;
            }
        }

        [DataMember()]
        public List<ScriptElement> ScriptElementsList
        {
            get
            {
                return this.ScriptElementsListField;
            }
            set
            {
                this.ScriptElementsListField = value;
            }
        }
    }
}