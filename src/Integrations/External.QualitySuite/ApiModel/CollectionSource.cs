﻿using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContract(Name = "CollectionSource", Namespace = "http://schemas.datacontract.org/2004/07/ValidataCommon.Interfaces")]
    public enum CollectionSource : int
    {
        [EnumMember()]
        TestData = 0,

        [EnumMember()]
        ActualResult = 1,

        [EnumMember()]
        Response = 2,

        [EnumMember()]
        ExpectedResult = 3,

        [EnumMember()]
        VirtualData = 4,
    }
}