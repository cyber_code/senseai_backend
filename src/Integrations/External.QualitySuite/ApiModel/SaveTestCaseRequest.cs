﻿using System.Diagnostics;
using System.ServiceModel;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [MessageContract(WrapperName = "SaveTestCase", WrapperNamespace = "http://tempuri.org/", IsWrapped = true)]
    public partial class SaveTestCaseRequest
    {
        [MessageBodyMember(Namespace = "http://tempuri.org/", Order = 0)]
        public SessionKey sessionKey;

        [MessageBodyMember(Namespace = "http://tempuri.org/", Order = 1)]
        public TestCase testCase;

        [MessageBodyMember(Namespace = "http://tempuri.org/", Order = 2)]
        public string parentFolderNoderef;

        public SaveTestCaseRequest()
        {
        }

        public SaveTestCaseRequest(SessionKey sessionKey, TestCase testCase, string parentFolderNoderef)
        {
            this.sessionKey = sessionKey;
            this.testCase = testCase;
            this.parentFolderNoderef = parentFolderNoderef;
        }
    }
}