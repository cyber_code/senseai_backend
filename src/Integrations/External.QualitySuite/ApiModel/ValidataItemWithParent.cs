﻿using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "ValidataItemWithParent", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    public partial class ValidataItemWithParent : object
    {
        private string NameField;

        private string NoderefField;

        private string ParentNoderefField;

        private string TypeField;

        [DataMember()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }

        [DataMember()]
        public string Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }

        [DataMember()]
        public string ParentNoderef
        {
            get
            {
                return this.ParentNoderefField;
            }
            set
            {
                this.ParentNoderefField = value;
            }
        }

        [DataMember()]
        public string Type
        {
            get
            {
                return this.TypeField;
            }
            set
            {
                this.TypeField = value;
            }
        }
    }
}