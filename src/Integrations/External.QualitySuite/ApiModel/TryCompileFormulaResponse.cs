﻿using System.Diagnostics;
using System.ServiceModel;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [MessageContract(WrapperName = "TryCompileFormulaResponse", WrapperNamespace = "http://tempuri.org/", IsWrapped = true)]
    public partial class TryCompileFormulaResponse
    {
        [MessageBodyMember(Namespace = "http://tempuri.org/", Order = 0)]
        public bool TryCompileFormulaResult;

        [MessageBodyMember(Namespace = "http://tempuri.org/", Order = 1)]
        public string errorMessage;

        public TryCompileFormulaResponse()
        {
        }

        public TryCompileFormulaResponse(bool TryCompileFormulaResult, string errorMessage)
        {
            this.TryCompileFormulaResult = TryCompileFormulaResult;
            this.errorMessage = errorMessage;
        }
    }
}