﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace External.QualitySuite.ApiModel
{
    [ServiceContract(Name = "RepositoryServiceInterfaces.IRepositoryService", ConfigurationName = "IRepositoryService")]
    public interface IRepositoryService
    {
        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/GetLoginItemsUsingCredentials", ReplyAction = "http://tempuri.org/IAuthenticate/GetLoginItemsUsingCredentialsResponse")]
        Task<ValidataLoginInformation> GetLoginItemsUsingCredentialsAsync(Credentials credentials);

        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/ValidataLogin", ReplyAction = "http://tempuri.org/IAuthenticate/ValidataLoginResponse")]
        Task<SessionKey> ValidataLoginAsync(Credentials credentials);


        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/ValidataInitLogin", ReplyAction = "http://tempuri.org/IAuthenticate/ValidataInitLoginResponse")]
        Task<SessionKey> ValidataInitLoginAsync(Credentials credentials);

        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/IsSessionValid", ReplyAction = "http://tempuri.org/IAuthenticate/IsSessionValidResponse")]
        Task<bool> IsSessionValidAsync(SessionKey session);

        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/CloseSession", ReplyAction = "http://tempuri.org/IAuthenticate/CloseSessionResponse")]
        Task CloseSessionAsync(SessionKey session);

        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/GetValidataProjects", ReplyAction = "http://tempuri.org/IAuthenticate/GetValidataProjectsResponse")]
        Task<ValidataLoginInformation> GetValidataProjectsAsync(Credentials credentials);

        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/GetValidataSubProjects", ReplyAction = "http://tempuri.org/IAuthenticate/GetValidataSubProjectsResponse")]
        Task<ValidataLoginInformation> GetValidataSubProjectsAsync(Credentials credentials);

        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/GetValidataDisciplines", ReplyAction = "http://tempuri.org/IAuthenticate/GetValidataDisciplinesResponse")]
        Task<ValidataLoginInformation> GetValidataDisciplinesAsync(Credentials credentials);

        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/GetValidataRoles", ReplyAction = "http://tempuri.org/IAuthenticate/GetValidataRolesResponse")]
        Task<ValidataLoginInformation> GetValidataRolesAsync(Credentials credentials);

        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/GetValidataUsers", ReplyAction = "http://tempuri.org/IAuthenticate/GetValidataUsersResponse")]
        Task<ValidataLoginInformation> GetValidataUsersAsync(Credentials credentials);

        [OperationContractAttribute(Action = "http://tempuri.org/IAuthenticate/GetError", ReplyAction = "http://tempuri.org/IAuthenticate/GetErrorResponse")]
        Task<List<RepositoryServiceErrorMessages>> GetErrorAsync(SessionKey session);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetValidataCataloguesList", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetValidataCataloguesListResponse")]
        Task<List<ValidataItem>> GetValidataCataloguesListAsync(SessionKey sessionKey);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetValidataTypicals", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetValidataTypicalsResponse")]
        Task<List<ValidataItem>> GetValidataTypicalsAsync(SessionKey sessionKey, string catalogName);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetAAProductsList", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetAAProductsListResponse")]
        Task<List<ValidataItem>> GetAAProductsListAsync(SessionKey sessionKey, string catalogName);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetDefaultCatalogueApplication", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetDefaultCatalogueApplicationResponse")]
        Task<string> GetDefaultCatalogueApplicationAsync(SessionKey sessionKey);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetDefaultCatalogueEnquiry", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetDefaultCatalogueEnquiryResponse")]
        Task<string> GetDefaultCatalogueEnquiryAsync(SessionKey sessionKey);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetDefaultCatalogueAA", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetDefaultCatalogueAAResponse")]
        Task<string> GetDefaultCatalogueAAAsync(SessionKey sessionKey);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueApplication", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueApplicationResponse")]
        Task<bool> SetDefaultCatalogueApplicationAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueEnquery", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueEnqueryResponse")]
        Task<bool> SetDefaultCatalogueEnqueryAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueAA", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueAAResponse")]
        Task<bool> SetDefaultCatalogueAAAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueBrowserMultiApp", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueBrowserMultiAppResponse")]
        Task<bool> SetDefaultCatalogueBrowserMultiAppAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultEdgeCatalogueApplication", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultEdgeCatalogueApplicationResponse")]
        Task<bool> SetDefaultEdgeCatalogueApplicationAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultEdgeCatalogueEnquery", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultEdgeCatalogueEnqueryResponse")]
        Task<bool> SetDefaultEdgeCatalogueEnqueryAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultEdgeCatalogueAA", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultEdgeCatalogueAAResponse")]
        Task<bool> SetDefaultEdgeCatalogueAAAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultEdgeCatalogueBrowserMultiApp", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultEdgeCatalogueBrowserMultiAppResponse")]
        Task<bool> SetDefaultEdgeCatalogueBrowserMultiAppAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueTripleA", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueTripleAResponse")]
        Task<bool> SetDefaultCatalogueTripleAAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultEnquiryCatalogueTripleA", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultEnquiryCatalogueTripleAResponse")]
        Task<bool> SetDefaultEnquiryCatalogueTripleAAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueTCIB", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueTCIBResponse")]
        Task<bool> SetDefaultCatalogueTCIBAsync(SessionKey sessionKey, string catalogueNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueTCMB", ReplyAction = "http://tempuri.org/ITestTreeAccess/SetDefaultCatalogueTCMBResponse")]
        Task<bool> SetDefaultCatalogueTCMBAsync(SessionKey sessionKey, string catalogueNoderef);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/SaveTestCase", ReplyAction = "http://tempuri.org/ITestTreeAccess/SaveTestCaseResponse")]
        Task<SaveTestCaseResponse> SaveTestCaseAsync(SaveTestCaseRequest request);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/CreateTestFolder", ReplyAction = "http://tempuri.org/ITestTreeAccess/CreateTestFolderResponse")]
        Task<TestFolder> CreateTestFolderAsync(SessionKey sessionKey, TestFolder testFolder, string parentFolderNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetTestFolderTree", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetTestFolderTreeResponse")]
        Task<List<ValidataItemWithParent>> GetTestFolderTreeAsync(SessionKey sessionKey);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetTestFoldersSingleLevel", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetTestFoldersSingleLevelResponse")]
        Task<List<ValidataItemWithParent>> GetTestFoldersSingleLevelAsync(SessionKey sessionKey, ValidataItemWithParent parent);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetChildTestCases", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetChildTestCasesResponse")]
        Task<List<ValidataItemWithParent>> GetChildTestCasesAsync(SessionKey sessionKey, string parentTestFolderNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetChildTestSteps", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetChildTestStepsResponse")]
        Task<List<ValidataItemWithParent>> GetChildTestStepsAsync(SessionKey sessionKey, string parentTestCaseNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetChildTestStepsComplete", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetChildTestStepsCompleteResponse")]
        Task<List<TestStep>> GetChildTestStepsCompleteAsync(SessionKey sessionKey, string parentTestCaseNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetValidataSystemsList", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetValidataSystemsListResponse")]
        Task<List<ValidataItem>> GetValidataSystemsListAsync(SessionKey sessionKey);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetAttibutesNames", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetAttibutesNamesResponse")]
        Task<List<string>> GetAttibutesNamesAsync(SessionKey sessionKey, string catalogName, string typicalName);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/GetTypicalBaseClass", ReplyAction = "http://tempuri.org/ITestTreeAccess/GetTypicalBaseClassResponse")]
        Task<string> GetTypicalBaseClassAsync(SessionKey sessionKey, string catalogName, string typicalName);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/CreateOrUpdateTypical", ReplyAction = "http://tempuri.org/ITestTreeAccess/CreateOrUpdateTypicalResponse")]
        Task<ValidataItem> CreateOrUpdateTypicalAsync(SessionKey sessionKey, string catalogName, string typicalName, string baseClass, List<string> typicalAttributeNames);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/TryCompileFormula", ReplyAction = "http://tempuri.org/ITestTreeAccess/TryCompileFormulaResponse")]
        Task<TryCompileFormulaResponse> TryCompileFormulaAsync(TryCompileFormulaRequest request);

        [OperationContractAttribute(Action = "http://tempuri.org/ITestTreeAccess/EvaluateAutomaticCalculation", ReplyAction = "http://tempuri.org/ITestTreeAccess/EvaluateAutomaticCalculationResponse")]
        Task<string> EvaluateAutomaticCalculationAsync(SessionKey sessionKey, AutomaticCalculation automaticCalculation);

        [OperationContractAttribute(Action = "http://tempuri.org/IScriptLibrarianAccess/GetScriptLibrarianWebApplicationsList", ReplyAction = "http://tempuri.org/IScriptLibrarianAccess/GetScriptLibrarianWebApplicationsListResponse")]
        Task<List<ValidataItem>> GetScriptLibrarianWebApplicationsListAsync(SessionKey sessionKey);

        [OperationContractAttribute(Action = "http://tempuri.org/IScriptLibrarianAccess/GetScriptLibrarianApplication", ReplyAction = "http://tempuri.org/IScriptLibrarianAccess/GetScriptLibrarianApplicationResponse")]
        Task<Application> GetScriptLibrarianApplicationAsync(SessionKey sessionKey, string noderef, bool loadChildrenObjects);

        [OperationContractAttribute(Action = "http://tempuri.org/IScriptLibrarianAccess/SaveScripts", ReplyAction = "http://tempuri.org/IScriptLibrarianAccess/SaveScriptsResponse")]
        Task<List<Script>> SaveScriptsAsync(SessionKey sessionKey, List<Script> scripts);

        [OperationContractAttribute(Action = "http://tempuri.org/IScriptLibrarianAccess/GetScriptLibrarianScriptsAndApplications", ReplyAction = "http://tempuri.org/IScriptLibrarianAccess/GetScriptLibrarianScriptsAndApplicationsResponse")]
        Task<List<Application>> GetScriptLibrarianScriptsAndApplicationsAsync(SessionKey sessionKey, List<ulong> scriptNoderefs);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/SaveRequirement", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/SaveRequirementResponse")]
        Task<RM_Requirement> SaveRequirementAsync(SessionKey sessionKey, RM_Requirement requirement, string parentUseCaseNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/SaveUseCase", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/SaveUseCaseResponse")]
        Task<RM_UseCase> SaveUseCaseAsync(SessionKey sessionKey, RM_UseCase useCase, string parentFeatureNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/SaveFeature", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/SaveFeatureResponse")]
        Task<RM_Feature> SaveFeatureAsync(SessionKey sessionKey, RM_Feature feature, string parentSubProjectNoderef, string parentProductNoderef, string parentBANoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/SaveProduct", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/SaveProductResponse")]
        Task<RM_Product> SaveProductAsync(SessionKey sessionKey, RM_Product product, string parentSubProject);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/SaveBusinessArea", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/SaveBusinessAreaResponse")]
        Task<RM_BusinessArea> SaveBusinessAreaAsync(SessionKey sessionKey, RM_BusinessArea businessArea, string parentSubProject);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/AssociateRequirementToTestCase", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/AssociateRequirementToTestCaseResponse")]
        Task<bool> AssociateRequirementToTestCaseAsync(SessionKey sessionKey, string requirementNoderef, string testCaseNoderef);

        [OperationContractAttribute(Action = "http://tempuri.org/RepositoryServiceInterfaces.IRepositoryService/CheckIfTypicalNameExists", ReplyAction = "http://tempuri.org/RepositoryServiceInterfaces.IRepositoryService/CheckIfTypicalNameExistsResponse")]
        Task<bool> CheckIfTypicalNameExistsAsync(SessionKey sessionKey, string catalogName, string typicalName);

        [OperationContractAttribute(Action = "http://tempuri.org/RepositoryServiceInterfaces.IRepositoryService/GetTypicalNameFromDescription", ReplyAction = "http://tempuri.org/RepositoryServiceInterfaces.IRepositoryService/GetTypicalNameFromDescriptionResponse")]
        Task<string> GetTypicalNameFromDescriptionAsync(SessionKey sessionKey, string catalogName, string typicalDescription);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/GetProducts", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/GetProductsResponse")]
        Task<List<ValidataItem>> GetProductsAsync(SessionKey sessionKey, ulong subProjectId);
        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/GetAdapters", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/GetAdaptersResponse")]
        Task<List<ValidataItem>> GetAdaptersAsync(SessionKey sessionKey, ulong subProjectId);
        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/GetAdaptersPerSystems", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/GetAdaptersPerSystemsResponse")]
        Task<Dictionary<string, List<ValidataItem>>> GetAdaptersPerSystemsAsync(SessionKey sessionKey, ulong subProjectId);
        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/GetFoldersAndFeatures", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/GetFoldersAndFeaturesResponse")]
        Task<List<ValidataItemWithParent>> GetFoldersAndFeaturesAsync(SessionKey sessionKey, ulong productId);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/GetFeatures", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/GetFeaturesResponse")]
        Task<List<ValidataItem>> GetFeaturesAsync(SessionKey sessionKey, ulong productId, ulong folderId);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/GetFoldersAndUseCases", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/GetFoldersAndUseCasesResponse")]
        Task<List<ValidataItemWithParent>> GetFoldersAndUseCasesAsync(SessionKey sessionKey, ulong featureId);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/GetUseCases", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/GetUseCasesResponse")]
        Task<List<ValidataItem>> GetUseCasesAsync(SessionKey sessionKey, ulong featureId, ulong folderId);

        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/GetRequirements", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/GetRequirementsResponse")]
        Task<List<ValidataItem>> GetRequirementsAsync(SessionKey sessionKey, ulong useCaseId);
        [OperationContractAttribute(Action = "http://tempuri.org/IRequrementsManagerAccess/GetReleaseSummary", ReplyAction = "http://tempuri.org/IRequrementsManagerAccess/GetReleaseSummaryResponse")]
        Task<RM_ReleaseSummary> GetReleaseSummaryAsync(SessionKey sessionKey, ulong releaseIterationId);
    }
}