﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "TestStep", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    [KnownType(typeof(CollectionSource))]
    [KnownType(typeof(ExpressionType))]
    [KnownType(typeof(AttributeDataType))]
    [KnownType(typeof(ApplicationType))]
    [KnownType(typeof(TestingParameterType))]
    [KnownType(typeof(EnumsExecutionType))]
    [KnownType(typeof(EnumsTestStepPriority))]
    [KnownType(typeof(EnumsTestStepType))]
    [KnownType(typeof(TestStepUserInterfaceType))]
    [KnownType(typeof(FilterType))]
    [KnownType(typeof(List<object>))]
    [KnownType(typeof(List<string>))]
    [KnownType(typeof(List<ulong>))]
    [KnownType(typeof(Application))]
    [KnownType(typeof(List<Screen>))]
    [KnownType(typeof(Screen))]
    [KnownType(typeof(List<Control>))]
    [KnownType(typeof(Control))]
    [KnownType(typeof(WebMap))]
    [KnownType(typeof(List<Script>))]
    [KnownType(typeof(Script))]
    [KnownType(typeof(List<ScriptElement>))]
    [KnownType(typeof(ScriptElement))]
    [KnownType(typeof(List<ElementParam>))]
    [KnownType(typeof(ElementParam))]
    [KnownType(typeof(List<Application>))]
    [KnownType(typeof(Credentials))]
    [KnownType(typeof(ValidataLoginInformation))]
    [KnownType(typeof(ValidataLoginInformation.InformationType))]
    [KnownType(typeof(List<ValidataItem>))]
    [KnownType(typeof(ValidataItem))]
    [KnownType(typeof(SessionKey))]
    [KnownType(typeof(List<RepositoryServiceErrorMessages>))]
    [KnownType(typeof(RepositoryServiceErrorMessages))]
    [KnownType(typeof(RepositoryServiceErrorMessages.MessageType))]
    [KnownType(typeof(TestCase))]
    [KnownType(typeof(Filter))]
    [KnownType(typeof(ValidataRecord))]
    [KnownType(typeof(List<ValidataRecordAttribute>))]
    [KnownType(typeof(ValidataRecordAttribute))]
    [KnownType(typeof(AutomaticCalculation))]
    [KnownType(typeof(AutomaticCalculationItem))]
    [KnownType(typeof(DataPool))]
    [KnownType(typeof(DataPoolItem))]
    [KnownType(typeof(TestFolder))]
    [KnownType(typeof(List<ValidataItemWithParent>))]
    [KnownType(typeof(ValidataItemWithParent))]
    [KnownType(typeof(List<TestStep>))]
    [KnownType(typeof(RM_Requirement))]
    [KnownType(typeof(RM_UseCase))]
    [KnownType(typeof(RM_Feature))]
    [KnownType(typeof(RM_Product))]
    [KnownType(typeof(RM_BusinessArea))]
    public partial class TestStep : object
    {
        private int ActualResultField;

        private string AdapterField;

        private List<string> AttributesWithCustomCurrencyComparisonField;

        private List<object> AutomaticCalculationsField;

        private string BaseClassField;

        private string ConfigurationField;

        private string DescriptionField;

        private bool DoNotCompareEmptyValueInExpectedResultsField;

        private int EndCaseOnErrorField;

        private bool ExpectedFailField;

        private string ExpectedResultField;

        private object ExpectedResultRecordField;

        private string ExternalRefField;

        private List<object> FiltersField;

        private int LogicalDayField;

        private EnumsExecutionType ManualOrAutoField;

        private string NameField;

        private string LabelField;

        private ulong NoderefField;

        private int OnErrorField;

        private EnumsTestStepPriority PriorityField;

        private string ResultTypeField;

        private ulong ScriptNoderefField;

        private int StepIndexField;

        private string SystemIdField;

        private object TestDataField;

        private bool TransactionExistsField;

        private EnumsTestStepType TypeField;

        private string UIDField;

        private TestStepUserInterfaceType UserInterfaceField;

        private string VersionDefinitionField;

        [DataMember()]
        public int ActualResult
        {
            get
            {
                return this.ActualResultField;
            }
            set
            {
                this.ActualResultField = value;
            }
        }

        [DataMember()]
        public string Adapter
        {
            get
            {
                return this.AdapterField;
            }
            set
            {
                this.AdapterField = value;
            }
        }

        [DataMember()]
        public List<string> AttributesWithCustomCurrencyComparison
        {
            get
            {
                return this.AttributesWithCustomCurrencyComparisonField;
            }
            set
            {
                this.AttributesWithCustomCurrencyComparisonField = value;
            }
        }

        [DataMember()]
        public List<object> AutomaticCalculations
        {
            get
            {
                return this.AutomaticCalculationsField;
            }
            set
            {
                this.AutomaticCalculationsField = value;
            }
        }

        [DataMember()]
        public string BaseClass
        {
            get
            {
                return this.BaseClassField;
            }
            set
            {
                this.BaseClassField = value;
            }
        }

        [DataMember()]
        public string Configuration
        {
            get
            {
                return this.ConfigurationField;
            }
            set
            {
                this.ConfigurationField = value;
            }
        }

        [DataMember()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }

        [DataMember()]
        public bool DoNotCompareEmptyValueInExpectedResults
        {
            get
            {
                return this.DoNotCompareEmptyValueInExpectedResultsField;
            }
            set
            {
                this.DoNotCompareEmptyValueInExpectedResultsField = value;
            }
        }

        [DataMember()]
        public int EndCaseOnError
        {
            get
            {
                return this.EndCaseOnErrorField;
            }
            set
            {
                this.EndCaseOnErrorField = value;
            }
        }

        [DataMember()]
        public bool ExpectedFail
        {
            get
            {
                return this.ExpectedFailField;
            }
            set
            {
                this.ExpectedFailField = value;
            }
        }

        [DataMember()]
        public string ExpectedResult
        {
            get
            {
                return this.ExpectedResultField;
            }
            set
            {
                this.ExpectedResultField = value;
            }
        }

        [DataMember()]
        public object ExpectedResultRecord
        {
            get
            {
                return this.ExpectedResultRecordField;
            }
            set
            {
                this.ExpectedResultRecordField = value;
            }
        }

        [DataMember()]
        public string ExternalRef
        {
            get
            {
                return this.ExternalRefField;
            }
            set
            {
                this.ExternalRefField = value;
            }
        }

        [DataMember()]
        public List<object> Filters
        {
            get
            {
                return this.FiltersField;
            }
            set
            {
                this.FiltersField = value;
            }
        }

        [DataMember()]
        public int LogicalDay
        {
            get
            {
                return this.LogicalDayField;
            }
            set
            {
                this.LogicalDayField = value;
            }
        }

        [DataMember()]
        public EnumsExecutionType ManualOrAuto
        {
            get
            {
                return this.ManualOrAutoField;
            }
            set
            {
                this.ManualOrAutoField = value;
            }
        }

        [DataMember()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }

        [DataMember()]
        public string Label
        {
            get
            {
                return this.LabelField;
            }
            set
            {
                this.LabelField = value;
            }
        }

        [DataMember()]
        public ulong Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }

        [DataMember()]
        public int OnError
        {
            get
            {
                return this.OnErrorField;
            }
            set
            {
                this.OnErrorField = value;
            }
        }

        [DataMember()]
        public EnumsTestStepPriority Priority
        {
            get
            {
                return this.PriorityField;
            }
            set
            {
                this.PriorityField = value;
            }
        }

        [DataMember()]
        public string ResultType
        {
            get
            {
                return this.ResultTypeField;
            }
            set
            {
                this.ResultTypeField = value;
            }
        }

        [DataMember()]
        public ulong ScriptNoderef
        {
            get
            {
                return this.ScriptNoderefField;
            }
            set
            {
                this.ScriptNoderefField = value;
            }
        }

        [DataMember()]
        public int StepIndex
        {
            get
            {
                return this.StepIndexField;
            }
            set
            {
                this.StepIndexField = value;
            }
        }

        [DataMember()]
        public string SystemId
        {
            get
            {
                return this.SystemIdField;
            }
            set
            {
                this.SystemIdField = value;
            }
        }

        [DataMember()]
        public object TestData
        {
            get
            {
                return this.TestDataField;
            }
            set
            {
                this.TestDataField = value;
            }
        }

        [DataMember()]
        public bool TransactionExists
        {
            get
            {
                return this.TransactionExistsField;
            }
            set
            {
                this.TransactionExistsField = value;
            }
        }

        [DataMember()]
        public EnumsTestStepType Type
        {
            get
            {
                return this.TypeField;
            }
            set
            {
                this.TypeField = value;
            }
        }

        [DataMember()]
        public string UID
        {
            get
            {
                return this.UIDField;
            }
            set
            {
                this.UIDField = value;
            }
        }

        [DataMember()]
        public TestStepUserInterfaceType UserInterface
        {
            get
            {
                return this.UserInterfaceField;
            }
            set
            {
                this.UserInterfaceField = value;
            }
        }

        [DataMember()]
        public string VersionDefinition
        {
            get
            {
                return this.VersionDefinitionField;
            }
            set
            {
                this.VersionDefinitionField = value;
            }
        } 
    }
}