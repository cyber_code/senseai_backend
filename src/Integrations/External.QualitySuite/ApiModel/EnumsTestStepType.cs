﻿using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContract(Name = "Enums.TestStepType", Namespace = "http://schemas.datacontract.org/2004/07/ValidataCommon")]
    public enum EnumsTestStepType : int
    {
        [EnumMember()]
        Action = 1,

        [EnumMember()]
        Check = 2,

        [EnumMember()]
        Login = 3,
    }
}