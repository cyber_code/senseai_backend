﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "ValidataLoginInformation", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
    public partial class ValidataLoginInformation : object
    {
        private Credentials CredentialsField;
        private ValidataLoginInformation.InformationType IncludedТypeField;
        private List<ValidataItem> ValidataLoginItemListField;

        [DataMember()]
        public Credentials Credentials
        {
            get
            {
                return this.CredentialsField;
            }
            set
            {
                this.CredentialsField = value;
            }
        }

        [DataMember()]
        public ValidataLoginInformation.InformationType IncludedТype
        {
            get
            {
                return this.IncludedТypeField;
            }
            set
            {
                this.IncludedТypeField = value;
            }
        }

        [DataMember()]
        public List<ValidataItem> ValidataLoginItemList
        {
            get
            {
                return this.ValidataLoginItemListField;
            }
            set
            {
                this.ValidataLoginItemListField = value;
            }
        }

        [DataContract(Name = "ValidataLoginInformation.InformationType", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces")]
        public enum InformationType : int
        {
            [EnumMember()]
            Project = 0,

            [EnumMember()]
            SubProject = 1,

            [EnumMember()]
            Discipline = 2,

            [EnumMember()]
            Role = 3,

            [EnumMember()]
            User = 4,
        }
    }
}