﻿using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DataContract(Name = "Enums.ExecutionType", Namespace = "http://schemas.datacontract.org/2004/07/ValidataCommon")]
    public enum EnumsExecutionType : int
    {
        [EnumMember()]
        Manual = 1,

        [EnumMember()]
        Automatic = 2,
    }
}