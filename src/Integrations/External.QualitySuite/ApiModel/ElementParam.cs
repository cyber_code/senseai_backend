﻿using System.Diagnostics;
using System.Runtime.Serialization;

namespace External.QualitySuite.ApiModel
{
    [DebuggerStepThrough()]
    [DataContract(Name = "ElementParam", Namespace = "http://schemas.datacontract.org/2004/07/RepositoryServiceInterfaces.ScriptLibrarianObjects")]
    public partial class ElementParam : object
    {
        private int IndexField;

        private string NameField;

        private string NoderefField;

        private TestingParameterType TypeField;

        private string ValueField;

        [DataMember()]
        public int Index
        {
            get
            {
                return this.IndexField;
            }
            set
            {
                this.IndexField = value;
            }
        }

        [DataMember()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }

        [DataMember()]
        public string Noderef
        {
            get
            {
                return this.NoderefField;
            }
            set
            {
                this.NoderefField = value;
            }
        }

        [DataMember()]
        public TestingParameterType Type
        {
            get
            {
                return this.TypeField;
            }
            set
            {
                this.TypeField = value;
            }
        }

        [DataMember()]
        public string Value
        {
            get
            {
                return this.ValueField;
            }
            set
            {
                this.ValueField = value;
            }
        }
    }
}