﻿using Core.Abstractions.Bindings;
using External.QualitySuite.ApiModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace External.QualitySuite
{
    public class QualitySuiteClient : IQualitySuiteClient
    {
        #region [ Private Memebers ]

        private readonly RepositoryServiceClient _client;

        private readonly IQualitySuiteBindingConfiguration _bindingConfiguration;

        private Credentials _credentials;

        private SessionKey _session = null;

        #endregion [ Private Memebers ]

        #region [ Properties ]

        public IQualitySuiteBindingConfiguration BindingConfiguration
        {
            get { return _bindingConfiguration; }
        }

        #endregion [ Properties ]

        #region [ Ctor ]

        public QualitySuiteClient(IQualitySuiteBindingConfiguration bindingConfiguration)
        {
            _bindingConfiguration = bindingConfiguration;
        }

        private QualitySuiteClient(RepositoryServiceClient client)
        {
            _client = client;
        }

        #endregion [ Ctor ]

        #region [ IQualitySuiteClient: Implementation ]


        /// <summary>
        /// Read project and subproject from configuration json file
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        public IQualitySuiteClient CreateClient(Credentials credentials)
        {
            var requestTimeout = new TimeSpan(0, 0, _bindingConfiguration.Client.Timeout, 0);

            var customTransportSecurityBinding = new CustomBinding(new BasicHttpBinding
            {
                Security = { Mode = BasicHttpSecurityMode.None },
                ReceiveTimeout = requestTimeout,
                SendTimeout = requestTimeout,
                CloseTimeout = requestTimeout,
                MaxReceivedMessageSize = int.MaxValue
            });

            customTransportSecurityBinding.Elements[0] = new TextMessageEncodingBindingElement
            {
                MessageVersion = MessageVersion.CreateVersion(EnvelopeVersion.Soap12, AddressingVersion.WSAddressing10)
            };

            EndpointAddress endpoint = new EndpointAddress(_bindingConfiguration.Client.Endpoint);
            var client = new RepositoryServiceClient(customTransportSecurityBinding, endpoint);

            var qualitySuiteClient = new QualitySuiteClient(client);

            qualitySuiteClient.InitLogin(credentials);

            return qualitySuiteClient;
        }

        public IQualitySuiteClient CreateClient(Credentials credentials, ulong projectNoderef, ulong subProjectNoderef)
        {
            var requestTimeout = new TimeSpan(0, 0, _bindingConfiguration.Client.Timeout, 0);

            var customTransportSecurityBinding = new CustomBinding(new BasicHttpBinding
            {
                Security = { Mode = BasicHttpSecurityMode.None },
                ReceiveTimeout = requestTimeout,
                SendTimeout = requestTimeout,
                CloseTimeout = requestTimeout,
                MaxReceivedMessageSize = int.MaxValue
            });

            customTransportSecurityBinding.Elements[0] = new TextMessageEncodingBindingElement
            {
                MessageVersion = MessageVersion.CreateVersion(EnvelopeVersion.Soap12, AddressingVersion.WSAddressing10)
            };

            EndpointAddress endpoint = new EndpointAddress(_bindingConfiguration.Client.Endpoint);
            var client = new RepositoryServiceClient(customTransportSecurityBinding, endpoint);

            var qualitySuiteClient = new QualitySuiteClient(client);

            credentials.ProjectNoderef = projectNoderef.ToString();
            credentials.SubProjectNoderef = subProjectNoderef.ToString();

            qualitySuiteClient.Login(credentials);

            return qualitySuiteClient;
        }

        public List<ValidataItem> GetSystemsList()
        {
            //return await MeasureExecutionTime("GetSystemsList", async () =>
            //{
            return _client.GetValidataSystemsListAsync(_session).Result;
            //});
        }

        public TestFolder SaveTestFolder(string testFolderName, string testFolderDescription)
        {
            //return await MeasureExecutionTime("SaveTestFolder", async () =>
            //{
            var testFolder = new TestFolder() { Name = testFolderName, Description = testFolderDescription ?? testFolderName };
            return _client.CreateTestFolderAsync(_session, testFolder, _credentials.SubProjectNoderef).Result;
            //});
        }

        public TestFolder SaveTestSubFolder(string testFolderName, string testFolderDescription, string parentNoderef)
        {
            //return await MeasureExecutionTime("SaveTestFolder", async () =>
            //{
            var testFolder = new TestFolder() { Name = testFolderName, Description = testFolderDescription ?? testFolderName };
            return _client.CreateTestFolderAsync(_session, testFolder, parentNoderef).Result;
            //});
        }

        public string GetTestFolderId(string testFolderName, string testFolderDescription)
        {
            var testFolderTree = _client.GetTestFolderTreeAsync(_session).Result;

            var folder = testFolderTree.Find(ct => ct.Name == testFolderName && ct.ParentNoderef == "0");

            if (folder != null)
                return folder.Noderef;

            var tfData = this.SaveTestFolder(testFolderName, testFolderDescription);
            return tfData.Noderef;
        }

        public string GetTestSubFolderId(string parentNoderef, string testFolderName, string testFolderDescription)
        {
            var testFolderTree = _client.GetTestFoldersSingleLevelAsync(_session, new ValidataItemWithParent
            {
                Noderef = parentNoderef,
                ParentNoderef = parentNoderef
            }).Result;

            var folder = testFolderTree.Find(ct => ct.Name == testFolderName);

            if (folder != null)
                return folder.Noderef;

            var tfData = this.SaveTestSubFolder(testFolderName, testFolderDescription, parentNoderef);
            return tfData.Noderef;
        }

        public SaveTestCaseResponse SaveTestCase(string testFolderId, TestCase scriptedTestCase)
        {
            // _client.GetValidataProjectsAsync()
            //return await MeasureExecutionTime("SaveTestCase", async () =>
            //{
            var saveTestCaseTask = _client.SaveTestCaseAsync(new SaveTestCaseRequest()
            {
                sessionKey = _session,
                parentFolderNoderef = testFolderId,
                testCase = scriptedTestCase
            });

            saveTestCaseTask.Wait();
            return saveTestCaseTask.Result;
            //});
        }

        public List<RepositoryServiceErrorMessages> GetError()
        {
            return _client.GetErrorAsync(_session).Result;
        }

        public string SaveRequirement(string name, string description)
        {
            var savedResuirement = _client.SaveRequirementAsync(_session, new RM_Requirement()
            {
                Name = name,
                Description = description
            }, "0").Result;


            return savedResuirement?.Noderef.ToString();
        }

        public bool AssociateRequirementToTestCase(string requirementNoderef, string testCaseNoderef)
        {
            return _client.AssociateRequirementToTestCaseAsync(_session, requirementNoderef, testCaseNoderef).Result;
        }

        #endregion [ IQualitySuiteClient: Implementation ]

        public List<ValidataItem> GetProjectList()
        {
            var projects = _client.GetValidataProjectsAsync(_credentials).Result;
            var project = projects.ValidataLoginItemList;
            if (project != null)
                return project;
            throw new ArgumentException("The given project name not found in QualitySuite.");
        }

        public List<ValidataItem> GetSubProjectList(string projectItem)
        {
            var credencial = _credentials;
            credencial.ProjectNoderef = projectItem;
            var subProjects = _client.GetValidataSubProjectsAsync(credencial).Result;
            var subProject = subProjects.ValidataLoginItemList;//.FindAll(ct => ct.Noderef == projectItem);
            if (subProject != null)
                return subProject;
            throw new ArgumentException("The given subproject name not found in QualitySuite.");
        }

        public List<ValidataItem> GetSystems()
        {
            var credencial = _credentials;
            var systems = _client.GetValidataSystemsListAsync(_session).Result;
            if (systems != null)
                return systems;
            throw new ArgumentException("No systems found in ATS.");
        }

        public List<ValidataItem> GetCatalogs()
        {
            var credencial = _credentials;
            var systems = _client.GetValidataCataloguesListAsync(_session).Result;
            if (systems != null)
                return systems;
            throw new ArgumentException("No catalogs found in ATS.");
        }

        public List<ValidataItem> GetProducts(ulong subProjectId)
        {
            var products = _client.GetProductsAsync(_session, subProjectId).Result;
            if (products != null)
                return products;
            throw new ArgumentException("The given subproject does not have any Product.");
        }
        public List<ValidataItem> GetAdapters(ulong subProjectId)
        {
            var adapters = _client.GetAdaptersAsync(_session, subProjectId).Result;
            if (adapters != null)
                return adapters;
            throw new ArgumentException("The given subproject does not have any Adapters.");
        }
        public Dictionary<string, List<ValidataItem>> GetAdaptersPerSystems(ulong subProjectId)
        {
            var adapters = _client.GetAdaptersPerSystemsAsync(_session, subProjectId).Result;
            if (adapters != null)
                return adapters;
            throw new ArgumentException("The given subproject does not have any Adapters.");
        }
        public List<ValidataItemWithParent> GetFeatures(ulong productId)
        {
            var features = _client.GetFoldersAndFeaturesAsync(_session, productId).Result;
            if (features != null)
                return features;
            throw new ArgumentException("The given product does not have any Features.");
        }

        public List<ValidataItem> GetFeatures(ulong productId, ulong folderId)
        {
            var features = _client.GetFeaturesAsync(_session, productId, folderId).Result;
            if (features != null)
                return features;
            throw new ArgumentException("The given product does not have any Features.");
        }

        public List<ValidataItemWithParent> GetUseCases(ulong featureId)
        {
            var usecases = _client.GetFoldersAndUseCasesAsync(_session, featureId).Result;
            if (usecases != null)
                return usecases;
            throw new ArgumentException("The given feature does not have any use case.");
        }

        public List<ValidataItem> GetUseCases(ulong featureId, ulong folderId)
        {
            var usecases = _client.GetUseCasesAsync(_session, featureId, folderId).Result;
            if (usecases != null)
                return usecases;
            throw new ArgumentException("The given feature does not have any use case.");
        }

        public List<ValidataItem> GetRequirements(ulong useCaseId)
        {
            var requirements = _client.GetRequirementsAsync(_session, useCaseId).Result;
            if (requirements != null)
                return requirements;
            throw new ArgumentException("The given usecase does not have any requirements.");
        }

        public RM_ReleaseSummary GetReleaseSummary(ulong releaseIterationId)
        {
            var releaseSummary = _client.GetReleaseSummaryAsync(_session, releaseIterationId).Result;
            if (releaseSummary != null)
                return releaseSummary;
            throw new ArgumentException("Could not calculate release summary for the given release iteration");
        }




        private void Login(Credentials credentials)
        {
            this._credentials = credentials;
            //return await MeasureExecutionTime("Login", async () =>
            //{
            if (_session == null)
            {
                _session = _client.ValidataLoginAsync(_credentials).Result ??
                           throw new Exception("Cannot authorise user");
            }

            //});
        }

        private void InitLogin(Credentials credentials)
        {
            this._credentials = credentials;
            //return await MeasureExecutionTime("Login", async () =>
            //{
            if (_session == null)
            {
                _session = _client.ValidataInitLoginAsync(_credentials).Result ??
                           throw new Exception("Cannot authorise user");
            }

            //});
        }

        #region [ Helper ]

        //private async Task<T> MeasureExecutionTime<T>(string meaustingMessage, Func<Task<T>> a)
        //{
        //    Stopwatch watch = Stopwatch.StartNew();
        //    watch.Start();

        //    var result = await a();
        //    watch.Stop();
        //    Console.WriteLine($"{meaustingMessage} exe time: {watch.ElapsedMilliseconds}");

        //    return result;
        //}

        #endregion [ Helper ]
    }
}