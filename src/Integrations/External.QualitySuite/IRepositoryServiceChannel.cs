﻿using External.QualitySuite.ApiModel;
using System.ServiceModel;

namespace External.QualitySuite
{
    internal interface IRepositoryServiceChannel : IRepositoryService, IClientChannel
    {
    }
}