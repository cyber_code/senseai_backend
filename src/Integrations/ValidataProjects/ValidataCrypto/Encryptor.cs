﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ValidataCrypto
{
    public class Encryptor
    {
        public static Encryptor Default = new Encryptor("6E8cZ6F3P1NR5CcrDpudYhhaM+FhRMblbN0Ke0LP+zM=", "RW2WBvLyRc5WXdY8oAEEkQ==");

        private readonly byte[] _BlockKey;
        private readonly byte[] _BlockIV;

        public Encryptor(string blockKey, string blockIv)
        {
            _BlockKey = Convert.FromBase64String(blockKey);
            _BlockIV = Convert.FromBase64String(blockIv);
        }

        public string EncryptText(string text)
        {
            byte[] toEncrypt = Encoding.ASCII.GetBytes(text);
            byte[] encrypted = EncryptBytes(toEncrypt);
            string result = Convert.ToBase64String(encrypted, 0, encrypted.Length);

            return result;
        }

        public byte[] EncryptBytes(byte[] toEncrypt)
        {
            RijndaelManaged myRijndael = new RijndaelManaged();
            ICryptoTransform encryptor = myRijndael.CreateEncryptor(_BlockKey, _BlockIV);

            MemoryStream msEncrypt = new MemoryStream();
            CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
            csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
            csEncrypt.FlushFinalBlock();

            return msEncrypt.ToArray();
        }

        public string DecryptText(string encryptedText)
        {
            try
            {
                byte[] encryptedBytes = Convert.FromBase64String(encryptedText);
                byte[] fromEncrypt = DecryptBytes(encryptedBytes);
                string result = Encoding.ASCII.GetString(fromEncrypt).TrimEnd('\0');
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to decrypt text '{0}'", encryptedText), ex);
            }
        }

        public byte[] DecryptBytes(byte[] encryptedBytes)
        {
            RijndaelManaged myRijndael = new RijndaelManaged();

            ICryptoTransform decryptor = myRijndael.CreateDecryptor(_BlockKey, _BlockIV);

            MemoryStream msDecrypt = new MemoryStream(encryptedBytes);
            CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);

            byte[] fromEncrypt = new byte[encryptedBytes.Length];

            csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
            return fromEncrypt;
        }
    }
}