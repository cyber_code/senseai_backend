﻿using NUnit.Framework;

namespace ValidataCrypto.UnitTests
{
    [TestFixture]
    public class TestEncryptor
    {
        [Test]
        public void TestEncryption()
        {
            AssertEncryptionReversible("");
            AssertEncryptionReversible("aa");
            AssertEncryptionReversible("z");
            AssertEncryptionReversible("All kinds of characters: 1234567890-=qwertyuiop[]asdfghjkl;'\\zxcvbnm,./~!@#$%^&*()_+");
        }

        private static void AssertEncryptionReversible(string text)
        {
            string encryptedText = Encryptor.Default.EncryptText(text);
            Assert.AreNotEqual(text, encryptedText);
            Assert.AreEqual(text, Encryptor.Default.DecryptText(encryptedText));
        }
    }
}