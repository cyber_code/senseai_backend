﻿using System.Diagnostics;
using NUnit.Framework;

namespace ValidataCrypto.UnitTests
{
    [TestFixture]
    public class TestHasher
    {
        private const string password = "myP@5sw0rd"; // original password
        private const string wrongPassword = "password"; // wrong password

        [Test]
        public void TestDefaultHashing()
        {
            string hash = Hasher.ComputeHash(password);
            Assert.IsTrue(Hasher.VerifyHash(password, hash));
        }

        [Test]
        public void TestHashing()
        {
            Trace.WriteLine("COMPUTING HASH VALUES\r\n");

            foreach (string method in Hasher.SupportedHashingMethods)
            {
                string hash = Hasher.ComputeHash(password, method, null);
                Assert.IsTrue(Hasher.VerifyHash(password, method, hash));
                Assert.IsFalse(Hasher.VerifyHash(wrongPassword, method, hash));

                Trace.WriteLine(string.Format("{0} : {1}", method, hash));
            }
        }
    }
}