﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Net;
using System.Security.Principal;

namespace ValidataSharedDirs
{
    
    public class SharedWrapper
    {
        public List<string> SingleOperationLog = new List<string>();
        public const char WIN_PATH_SEP = '\\';
        public const char UNIX_PATH_SEP = '/';
        private const string SHARED_DIR_PREFIX = @"\\";

        public const byte LF_BYTE = 10;
        public const byte CR_BYTE = 13;

        public string TargetEnv;
        //Win = CR LF
        //Unix = LF

        private string _Source;
        private string _Destination;

        private byte[] _SourceBytes;
        private byte[] _DestinationBytes;

        private string _User;
        private string _Pass;
        private string _ErrorText;
        internal char _AssumedPathSeparator; 
        private char[] _BothPathSeparators;
        private bool _CopyInProgress;
        private int _CopyTimeOut = 12000;

        public enum CRTreatment { RemoveCR, AddCR, None };
        CRTreatment _CRMode = CRTreatment.None;
        bool _IsSourceFile = false;
        public IEnumerable<string> ListResultFiles; 
        public IEnumerable<string> ListResultFolders;

        public SharedWrapper(string credentialsUser, string credentialsPass, bool isWin, int copyTimeout)
        {
            _User = credentialsUser;
            _Pass = credentialsPass;

            _AssumedPathSeparator = isWin ? WIN_PATH_SEP : UNIX_PATH_SEP ;
            _ErrorText = "";
            _CopyInProgress = false;
            _BothPathSeparators = new char[]{WIN_PATH_SEP,UNIX_PATH_SEP};

        }

        public bool CopyFileFrom(string remoteIP, string remoteFilePath, string localFile, out string errMsg)
        {
            SingleOperationLog.Clear();
            remoteFilePath = remoteFilePath.Trim(_BothPathSeparators);
            remoteFilePath = remoteFilePath.Replace(WIN_PATH_SEP, _AssumedPathSeparator).Replace(UNIX_PATH_SEP, _AssumedPathSeparator);

            string fullNetFilePath = SHARED_DIR_PREFIX + remoteIP + _AssumedPathSeparator + remoteFilePath;
            string remoteFolder = Path.GetDirectoryName(remoteFilePath);
            remoteFolder = SHARED_DIR_PREFIX + remoteIP + _AssumedPathSeparator + remoteFolder;

            string fullDestination = localFile;

            _Source = fullNetFilePath;
            _Destination = fullDestination;

            var netConn = sendCredentialsMaybe(remoteFolder);
            if (netConn != null)
                using (netConn)
                    startCopyAndWait(false);
            else
                startCopyAndWait(false);

            errMsg = _ErrorText;
            return _ErrorText == "";
        }

        public bool CopyFrom(string remoteIP, string remoteFilePath, string localFolder, out string errMsg)
        {
            SingleOperationLog.Clear();
            remoteFilePath = remoteFilePath.Trim(_BothPathSeparators);
            remoteFilePath = remoteFilePath.Replace(WIN_PATH_SEP, _AssumedPathSeparator).Replace(UNIX_PATH_SEP, _AssumedPathSeparator);

            string fullNetFilePath = SHARED_DIR_PREFIX + remoteIP + _AssumedPathSeparator + remoteFilePath;
            string justFileName = Path.GetFileName(remoteFilePath);
            string remoteFolder = Path.GetDirectoryName(remoteFilePath);
            remoteFolder = SHARED_DIR_PREFIX + remoteIP + _AssumedPathSeparator + remoteFolder;

            string fullDestination = localFolder + WIN_PATH_SEP + justFileName;
            
            _Source = fullNetFilePath;
            _Destination = fullDestination;

            var netConn = sendCredentialsMaybe(remoteFolder);
            if (netConn != null)
                using (netConn)
                    startCopyAndWait(false);
            else
                startCopyAndWait(false);

            errMsg = _ErrorText;
            return _ErrorText == "";
        }

        public bool CopyTo(string remoteIP, string remoteFolder, string localFilePath, out string errMsg)
        {
            SingleOperationLog.Clear();
            _Source = localFilePath;

            remoteFolder = remoteFolder.Trim(_BothPathSeparators);
            remoteFolder = remoteFolder.Replace(WIN_PATH_SEP, _AssumedPathSeparator).Replace(UNIX_PATH_SEP, _AssumedPathSeparator);

            string justFileName = Path.GetFileName(localFilePath);
            string fullNetFilePath = SHARED_DIR_PREFIX + remoteIP + _AssumedPathSeparator + remoteFolder + _AssumedPathSeparator + justFileName;
            
            _Destination = fullNetFilePath;

            remoteFolder = SHARED_DIR_PREFIX + remoteIP + _AssumedPathSeparator + remoteFolder;
            var netConn = sendCredentialsMaybe(remoteFolder);

            if (netConn != null)
                using (netConn)
                    startCopyAndWait(false);
            else
                startCopyAndWait(false);

            errMsg = _ErrorText;
            return _ErrorText == "";
        }

        internal void CopyBytesTo(string remoteIP, string targetFilePath, byte[] inputFileContents, CRTreatment CRMode, bool isSrc, out string errMsg)
        {
            SingleOperationLog.Clear();
            _CRMode = CRMode;
            _IsSourceFile = isSrc;
            _SourceBytes = inputFileContents;

            targetFilePath = targetFilePath.Trim(_BothPathSeparators);
            targetFilePath = targetFilePath.Replace(WIN_PATH_SEP, _AssumedPathSeparator).Replace(UNIX_PATH_SEP, _AssumedPathSeparator);

            string fullNetFilePath = SHARED_DIR_PREFIX + remoteIP + _AssumedPathSeparator + targetFilePath;

            _Destination = fullNetFilePath;

            string remoteFolder = System.IO.Path.GetDirectoryName(targetFilePath);
            remoteFolder = SHARED_DIR_PREFIX + remoteIP + _AssumedPathSeparator + remoteFolder;
            var netConn = sendCredentialsMaybe(remoteFolder);

            if (netConn != null)
                using (netConn)
                    startCopyAndWait(true);
            else
                startCopyAndWait(true);
            errMsg = _ErrorText;
        }

        internal byte[] CopyBytesFrom(string remoteIP, string remoteFilePath, CRTreatment CRMode, out string errMsg)
        {
            SingleOperationLog.Clear();
            _CRMode = CRMode;

            remoteFilePath = remoteFilePath.Trim(_BothPathSeparators);
            remoteFilePath = remoteFilePath.Replace(WIN_PATH_SEP, _AssumedPathSeparator).Replace(UNIX_PATH_SEP, _AssumedPathSeparator);

            string fullNetFilePath = SHARED_DIR_PREFIX + remoteIP + _AssumedPathSeparator + remoteFilePath;
            
            _Destination = null;
            _Source = fullNetFilePath;


            string remoteFolder = System.IO.Path.GetDirectoryName(remoteFilePath);
            remoteFolder = SHARED_DIR_PREFIX + remoteIP + _AssumedPathSeparator + remoteFolder;
            var netConn = sendCredentialsMaybe(remoteFolder);

            if (netConn != null)
                using (netConn)
                    startCopyAndWait(true);
            else
                startCopyAndWait(true);

            errMsg = _ErrorText;
            return _DestinationBytes;
        }
      
        internal void DeleteFile(string machineNameOrIP, string remoteFilePath, out string errMsg)
        {
            SingleOperationLog.Clear();
            remoteFilePath = remoteFilePath.Trim(_BothPathSeparators);
            remoteFilePath = remoteFilePath.Replace(WIN_PATH_SEP, _AssumedPathSeparator).Replace(UNIX_PATH_SEP, _AssumedPathSeparator);
            string fullNetFilePath = SHARED_DIR_PREFIX + machineNameOrIP + _AssumedPathSeparator + remoteFilePath;
           
            string remoteFolder = Path.GetDirectoryName(remoteFilePath);
            remoteFolder = SHARED_DIR_PREFIX + machineNameOrIP + _AssumedPathSeparator + remoteFolder;

            var netConn = sendCredentialsMaybe(remoteFolder);
            try
            {
                SingleOperationLog.Add(string.Format("Deleting file {0}", fullNetFilePath));

                if (netConn != null)
                    using (netConn)
                        File.Delete(fullNetFilePath);
                else
                    File.Delete(fullNetFilePath);
            }
            catch (Exception ex)
            {
                _ErrorText = ex.GetType() + " while deleting path: " + remoteFilePath + "\r\n" + ex.Message;
            }
            errMsg = _ErrorText;
        }

        private void copyWrapper()
        {
            try
            {
                SingleOperationLog.Add(string.Format("Copying file {0} to {1}",_Source, _Destination));

                File.Copy(_Source, _Destination, true);
                _CopyInProgress = false;
            }
            catch (Exception ex)
            {
                _ErrorText = ex.GetType() + " while copying to path: " + _Destination + "\r\n" + ex.Message;
                _CopyInProgress = false;
            }
        }

        private void copyBytesWrapper()
        {
            try
            {
                SingleOperationLog.Add(string.Format("Opening a \"FileMode.Create\" stream to {0}", _Destination));
                using (Stream sw = new FileStream(_Destination, FileMode.Create))
                {
                    List<byte> bytelist = new List<byte>();

                    for (int bcc = 0; bcc < _SourceBytes.Count<byte>(); bcc++)
                    {
                        byte bt = _SourceBytes[bcc];

                        //TODO: check how this works
                        switch (_CRMode)
                        {
                            case CRTreatment.None:
                                bytelist.Add(bt);
                                break;

                            case CRTreatment.AddCR:
                                if (bt == LF_BYTE)
                                    bytelist.Add(CR_BYTE);
                                bytelist.Add(bt);
                                break;

                            case CRTreatment.RemoveCR:
                                if (bt != CR_BYTE)
                                    bytelist.Add(bt);
                                break;

                        }

                    }

                    if (bytelist.Count > 0)
                    {
                        // The bellow enusres a newline at the end - as in the FTP connector
                        if (bytelist.Last<byte>() != LF_BYTE)
                        {
                            if (_IsSourceFile)
                            {
                                if (_CRMode == CRTreatment.AddCR)
                                    bytelist.Add(CR_BYTE);
                                bytelist.Add(LF_BYTE);
                            }
                        }
                        byte[] buffer = bytelist.ToArray();

                        SingleOperationLog.Add(string.Format("Writing bytes buffer."));

                        sw.Write(buffer, 0, bytelist.Count);
                    }

                    sw.Close();

                    _CopyInProgress = false;
                }
            }
            catch (Exception ex)
            {
                _ErrorText = ex.GetType() + " while writing bytes to path: " + _Destination + "\r\n" + ex.Message;
                _CopyInProgress = false;
            }
        }
    
        private NetworkConnection sendCredentialsMaybe(string remoteDir)
        {
            if (!string.IsNullOrEmpty(_User))
            {
                try
                {
                    try
                    {
                        WindowsIdentity currUsr = WindowsIdentity.GetCurrent();
                        SingleOperationLog.Add(string.Format("Running as {0} ({1})", System.Windows.Forms.SystemInformation.UserName, currUsr.Name));
                        WindowsPrincipal principal = new WindowsPrincipal(currUsr);
                        if (principal != null)
                            SingleOperationLog.Add(string.Format("{0}unning as Administrator", principal.IsInRole(WindowsBuiltInRole.Administrator) ? "R" : "Not r"));
                    }
                    catch
                    {
                        SingleOperationLog.Add(string.Format("Error getting current user"));
                    }
                    SingleOperationLog.Add(string.Format("Sending Network Credentials {0}/******* over to {1}", _User, remoteDir));
                    return new NetworkConnection(remoteDir, new NetworkCredential(_User, _Pass));
                }
                catch (Exception exc)
                {
                    SingleOperationLog.Add(string.Format("Sending Network Credentials failed! {0}: ", exc.GetType()));
                    SingleOperationLog.Add(string.Format(exc.Message));
                    SingleOperationLog.Add(string.Format("Will proceed with operation without credentials."));
                    
                    // Mostly due to the "..multiple connections ... bla bla issue"
                    // Then it would work without credentials too.

                }
            }
            return null;

        }

        private void startCopyAndWait( bool useBytes)
        {
            _CopyInProgress = true;
            _ErrorText = "";
            Thread copyThr;

            if (useBytes)
            {

                copyThr = new Thread(new ThreadStart(copyBytesWrapper));                
            }
            else
            {
                copyThr = new Thread(new ThreadStart(copyWrapper));
            }

            copyThr.Start();

            int tcc = 0;
            while (_CopyInProgress)
            {
                Thread.Sleep(10);
                tcc += 10;

                if (tcc > _CopyTimeOut)
                {
                    _ErrorText = "Defined timeout for copying exceeded.";
                    copyThr.Abort();
                    break;
                }
            }
        }

        internal void CreateDirectory(string machineNameOrIP, string remFolder, string dirForPermissions)
        {

            remFolder = remFolder.Trim(_BothPathSeparators);
            remFolder = remFolder.Replace(WIN_PATH_SEP, _AssumedPathSeparator).Replace(UNIX_PATH_SEP, _AssumedPathSeparator);
            string fullNetFilePath = SHARED_DIR_PREFIX + machineNameOrIP + _AssumedPathSeparator + remFolder;

            dirForPermissions = dirForPermissions.Trim(_BothPathSeparators);
            dirForPermissions = dirForPermissions.Replace(WIN_PATH_SEP, _AssumedPathSeparator).Replace(UNIX_PATH_SEP, _AssumedPathSeparator);
            string fullPermDir = SHARED_DIR_PREFIX + machineNameOrIP + _AssumedPathSeparator + dirForPermissions;

            var netConn = sendCredentialsMaybe(fullPermDir);
            if (netConn != null)
                using (netConn)
                    createDirWrapper(fullNetFilePath, fullPermDir);
            else
                createDirWrapper(fullNetFilePath, fullPermDir);



        }

        private void createDirWrapper(string fullNetFilePath, string fullPermDir)
        {
            DirectoryInfo infoDir = null;
            try
            {

                if (Directory.Exists(fullNetFilePath))
                {
                    SingleOperationLog.Add(string.Format("Directory {0} already exists, will cancel creation.", fullNetFilePath));

                    return;
                }

                SingleOperationLog.Add(string.Format("Creating directory {0} with \"System.Security.AccessControl.AccessControlSections.Access\"", fullNetFilePath));
                System.Security.AccessControl.DirectorySecurity dirSec = new System.Security.AccessControl.DirectorySecurity(fullPermDir, System.Security.AccessControl.AccessControlSections.Access);
                infoDir = Directory.CreateDirectory(fullNetFilePath, dirSec);

               
            }
            catch (Exception e)
            {
                SingleOperationLog.Add(string.Format("CreateDirectory with access control failed. {0}: ",e.GetType()));
                SingleOperationLog.Add(e.Message);

                try
                {
                    SingleOperationLog.Add(string.Format("Creating directory {0} without Access control", fullNetFilePath));

                    infoDir = Directory.CreateDirectory(fullNetFilePath);
                }
                catch (Exception ex)
                {
                    _ErrorText = ex.GetType() + " while creating path: " + fullNetFilePath + "\r\n" + ex.Message;
                }
            }
            if (infoDir != null)
            {
                var dirname = infoDir.FullName;
                SingleOperationLog.Add(string.Format("Result is dir with name: {0}", !string.IsNullOrEmpty(dirname) ? dirname : "NULL"));

            }
            else { SingleOperationLog.Add(string.Format("CreateDirectory has probably failed")); }
        }

        internal void ListDir(string machineNameOrIP, string dir, out string errMsg)
        {
            errMsg = "";
            ListResultFiles = null;
            ListResultFolders = null;

            dir = SHARED_DIR_PREFIX + machineNameOrIP + _AssumedPathSeparator + dir;
            try
            {
                var netConn = sendCredentialsMaybe(dir);
                if (netConn != null)
                    using (netConn)
                    {
                        ListResultFiles = listCleanEntriesWrapper(dir, true);
                        ListResultFolders = listCleanEntriesWrapper(dir, false);
                    }
                else
                {
                    ListResultFiles = listCleanEntriesWrapper(dir, true);
                    ListResultFolders = listCleanEntriesWrapper(dir, false);
                }
            }
            catch(Exception exc)
            {
                errMsg = exc.GetType() + " while listing path: " + dir + "\r\n" + exc.Message;
            }
        }
        private List<string> listCleanEntriesWrapper(string dir, bool files)
        {
            SingleOperationLog.Add(string.Format("Listing {0} of {1}", files ? "files" : "subdirectories", dir));
            List<string> lrf = new List<string>();
            var entries = files ? Directory.EnumerateFiles(dir) : Directory.EnumerateDirectories(dir);
            foreach (var en in entries)
            {
                lrf.Add(en.Replace(dir + _AssumedPathSeparator, ""));
            }
            return lrf;
        }
    }
}
