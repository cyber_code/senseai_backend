﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidataSharedDirs
{
    [TestClass]
    public class TestSharedDirs
    {
        private string erMsg;

        private string _FTPUser ;
        private string _FTPPass ;
        private string _remoteIP ;

        private string _RemoteDir ;
        private string _LocalfilePath ;
        private string _RemoteFile;

        SharedConnector _ShConn;
        private string _RemoteFilePath { get { return string.Format("{0}{1}{2}", _RemoteDir, _ShConn.DirectorySeparator, _RemoteFile); } }

        #region init
        private void InitSharedConnector(string telnetSetName)
        {
            OFSCommonMethods.IO.Settings telnetSet = new OFSCommonMethods.IO.Settings() { PhantomSet = telnetSetName};
            OFSCommonMethods.IO.Telnet.ExecutionStepContainer.LoadConnectionSettings(telnetSet);

            _ShConn = new SharedConnector(telnetSet);
        }

        private void SetCredentials(string FTPUser, string FTPPass, string remoteIP)
        {
            _FTPUser = FTPUser;
            _FTPPass = FTPPass;
            _remoteIP = remoteIP;
        }
        private void SetPaths(string remoteDir, string localfilePath, string remoteFile)
        {
            _RemoteFile = remoteFile;
            _RemoteDir = remoteDir;
            _LocalfilePath = localfilePath;
        }

        #endregion

        [TestMethod]
        public void TestCopyUp()
        {
            //       remoteDir,          localfilePath,              remoteFile
            SetPaths("txt.files", @"..\..\UnitTests\TextFile1.txt", "TextFile1.txt");
            InitSharedConnector("67-abc-shared");

            //SetCredentials("bongo", "wongo", "GORDANWIN10");
            //SharedWrapper sw = new SharedWrapper(_FTPUser, _FTPPass, true, 5000);
            //sw.CopyTo(_remoteIP, _remoteDir, _localfilePath, out erMsg);

            _ShConn.UploadFile(_RemoteFilePath, System.IO.File.ReadAllBytes(_LocalfilePath), true);

            Assert.IsFalse(_ShConn.CurrentOperationLogLines.Length == 0);
            Assert.IsTrue(string.IsNullOrEmpty(_ShConn.ErrorMessage));
        }

        [TestMethod]
        public void TestCopyDown()
        {
            //       remoteDir,          localfilePath,              remoteFile
            SetPaths("txt.files", @"..\..\UnitTests\TextFile1.txt", "TextFile1.txt");
            InitSharedConnector("67-abc-shared");

            //SetCredentials("bongo", "wongo", "GORDANWIN10");
            //SharedWrapper sw = new SharedWrapper(_FTPUser, _FTPPass, true, 5000);
            //sw.CopyTo(_remoteIP, _remoteDir, _localfilePath, out erMsg);

            var bytes = _ShConn.DownloadFile( _RemoteFilePath);

            Assert.IsFalse(bytes.Length == 0);
            Assert.IsFalse(_ShConn.CurrentOperationLogLines.Length == 0);
            Assert.IsTrue(string.IsNullOrEmpty(_ShConn.ErrorMessage));
        }

        [TestMethod]
        public void TestDelete()
        {
            //       remoteDir,          localfilePath,              remoteFile
            SetPaths("txt.files", @"..\..\UnitTests\TextFile1.txt", "TextFile1.txt");
            InitSharedConnector("67-abc-shared");

            //SetCredentials("bongo", "wongo", "GORDANWIN10");
            //SharedWrapper sw = new SharedWrapper(_FTPUser, _FTPPass, true, 5000);
            //sw.CopyTo(_remoteIP, _remoteDir, _localfilePath, out erMsg);

            _ShConn.DeleteFile(_RemoteFilePath, out erMsg);

            Assert.IsFalse(_ShConn.CurrentOperationLogLines.Length == 0);
            Assert.IsTrue(string.IsNullOrEmpty(erMsg));

        }

        [TestMethod]
        public void TestList()
        {
            //       remoteDir,          localfilePath,              remoteFile
            SetPaths("txt.files", @"..\..\UnitTests\TextFile1.txt", "TextFile1.txt");
            InitSharedConnector("67-abc-shared");

            //SetCredentials("bongo", "wongo", "GORDANWIN10");
            //SharedWrapper sw = new SharedWrapper(_FTPUser, _FTPPass, true, 5000);
            //sw.CopyTo(_remoteIP, _remoteDir, _localfilePath, out erMsg);

            try
            {
                var listed = _ShConn.ListDirectoryItems(_RemoteDir);
            }
            catch (Exception ee)
            {
                erMsg = ee.Message;
            }
            Assert.IsFalse(_ShConn.CurrentOperationLogLines.Length == 0);
            Assert.IsTrue(string.IsNullOrEmpty(erMsg));
            Assert.IsTrue(string.IsNullOrEmpty(_ShConn.ErrorMessage));

        }

        [TestMethod]
        public void TestMkDir()
        {
            //       remoteDir,          localfilePath,              remoteFile
            SetPaths("txt.files", @"..\..\UnitTests\TextFile1.txt", "TextFile1.txt");
            InitSharedConnector("67-abc-shared");

            //SetCredentials("bongo", "wongo", "GORDANWIN10");
            //SharedWrapper sw = new SharedWrapper(_FTPUser, _FTPPass, true, 5000);
            //sw.CopyTo(_remoteIP, _remoteDir, _localfilePath, out erMsg);

            var newDir = string.Format("{0}{1}{2}", _RemoteDir, _ShConn.DirectorySeparator, "bongogs");
            _ShConn.ParentPermissionsDir = _RemoteDir;
            _ShConn.CreateDirectoryRecursively(newDir);

            Assert.IsFalse(_ShConn.CurrentOperationLogLines.Length == 0);
            Assert.IsTrue(string.IsNullOrEmpty(_ShConn.ErrorMessage));

        }



    }
}
