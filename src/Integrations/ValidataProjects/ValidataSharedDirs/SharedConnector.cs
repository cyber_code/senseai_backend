﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OFSCommonMethods.IO;
using System.Windows.Forms;

namespace ValidataSharedDirs
{

    public class SharedConnector : ValidataCommon.IABCFileConnector, ValidataCommon.IFileTransfer , IDisposable
    {
        SharedWrapper _SharedWrapper;
        public string CurrentOperationLog
        {
            get
            {
                string res = "";
                foreach (string ln in _SharedWrapper.SingleOperationLog)
                {
                    res += ln;
                    res += "\r\n";
                }
                return res;
            }
        }

        public string[] CurrentOperationLogLines
        {
            get
            {
                if (_SharedWrapper.SingleOperationLog.Count > 0) return _SharedWrapper.SingleOperationLog.ToArray();
                return new string[0];
            }
        }


        Settings _TelnetSet;
        string _TempFolder;
        string _ErrorMessage;
        string _DirForPermissions;
        bool _AssumedWinOS;
        public bool UseBinary { get; set; }
        public string Hostname { get;set; }
        public string HostnameWithoutPrefix { get { return Hostname.Replace("ftp://", ""); } }
        public bool IsWinOS { get { return _AssumedWinOS; } set {_AssumedWinOS = value;} }
        public char DirectorySeparator { get {return _SharedWrapper._AssumedPathSeparator;} }
        public string User {get; set;}
        public string ParentPermissionsDir { set { _DirForPermissions = value; } }

        #region Constructors
        
        public SharedConnector(Settings telnetSet, string tempPath)
        {
            _TelnetSet = telnetSet;
            _TempFolder = tempPath;
            _AssumedWinOS = ValidataFtp.FtpPath.IsFullyQualifiedWindowsPath(_TelnetSet.FtpHomeFullPath);
            _SharedWrapper = new SharedWrapper(_TelnetSet.FtpUsername, _TelnetSet.FtpPassword, _AssumedWinOS, 12000);
            _ErrorMessage = "";
        }
        public SharedConnector(Settings telnetSet, bool isEnvWin, string logsSubfolder)
        {
            Hostname = telnetSet.GlobusServer;
            _TelnetSet = telnetSet;
            _AssumedWinOS = isEnvWin;
            _SharedWrapper = new SharedWrapper(_TelnetSet.FtpUsername, _TelnetSet.FtpPassword, _AssumedWinOS, 12000);
            _ErrorMessage = "";
            ValidataFtp.FtpSimpleLog.LogsSubfolder = logsSubfolder;
        }
        public SharedConnector(Settings telnetSet)
        {
            Hostname = telnetSet.GlobusServer;
            _TelnetSet = telnetSet;
            _AssumedWinOS = ValidataFtp.FtpPath.IsFullyQualifiedWindowsPath(_TelnetSet.FtpHomeFullPath);
            _SharedWrapper = new SharedWrapper(_TelnetSet.FtpUsername, _TelnetSet.FtpPassword, _AssumedWinOS, 12000);
            _ErrorMessage = "";
        }
        
        #endregion

        #region Downloads

        public void DownloadFileToTemp(string remoteFilePath)
        {
            if (_AssumedWinOS)
                remoteFilePath.Replace(SharedWrapper.UNIX_PATH_SEP, SharedWrapper.WIN_PATH_SEP);
            ///else
            //  remoteFilePath.Replace(SharedWrapper.WIN_PATH_SEP, SharedWrapper.UNIX_PATH_SEP);

            string machineNameOrIP = _TelnetSet.GlobusServer.Split(new char[] { ':' })[0];
            _SharedWrapper.CopyFrom(machineNameOrIP, remoteFilePath, _TempFolder, out _ErrorMessage);

        }

        public bool Download(string localFilePath, string remoteFilePath, out string errorMessage)
        {
            string machineNameOrIP = _TelnetSet.GlobusServer.Split(new char[] { ':' })[0];

            _SharedWrapper.CopyFileFrom(machineNameOrIP, remoteFilePath, localFilePath, out _ErrorMessage);
            errorMessage = _ErrorMessage;
            return HasError;
        }

        public byte[] DownloadFile(string remoteFilePath)
        {
            string tempFile = System.IO.Path.GetTempFileName();           
            string machineNameOrIP = _TelnetSet.GlobusServer.Split(new char[] { ':' })[0];
            
            while(true)
            {
                ValidataFtp.FtpSimpleLog.LogLine(string.Format("DOWNLOADING: {0}", remoteFilePath));

                Download(tempFile, remoteFilePath, out _ErrorMessage);
                ValidataFtp.FtpSimpleLog.LogLines(CurrentOperationLogLines);

                if (HasError)
                {
                    ValidataFtp.FtpSimpleLog.LogLine(string.Format("Error downloading: " + remoteFilePath));
                    ValidataFtp.FtpSimpleLog.LogLine(string.Format("Exception: " + _ErrorMessage));

                    reportError();
                }
                else break;
            }
            return System.IO.File.ReadAllBytes(tempFile);
        }

        #endregion

        #region Uploads

        public bool Upload(string inputFilePath, string targetFilePath, out string errorMessage)
        {
            var remFolder = System.IO.Path.GetDirectoryName(targetFilePath);
            
            string machineNameOrIP = _TelnetSet.GlobusServer.Split(new char[] { ':' })[0];
            ValidataFtp.FtpSimpleLog.LogLine(string.Format("UPLOADING: {0}", targetFilePath));
            ValidataFtp.FtpSimpleLog.LogLines(CurrentOperationLogLines);

            _SharedWrapper.CopyTo(machineNameOrIP, remFolder, inputFilePath, out _ErrorMessage);
            
            errorMessage = _ErrorMessage;
            return HasError;
        }

        public void UploadFile(string targetFilePath, byte[] inputFileContents, bool isSourceFile)
        {
            
            string machineNameOrIP = _TelnetSet.GlobusServer.Split(new char[] { ':' })[0];
            SharedWrapper.CRTreatment crt = SharedWrapper.CRTreatment.None;
            if (isSourceFile)
            {
                if (!inputFileContents.Contains<byte>(SharedWrapper.CR_BYTE) && _AssumedWinOS)
                    crt = SharedWrapper.CRTreatment.AddCR;
                if (inputFileContents.Contains<byte>(SharedWrapper.CR_BYTE) && !_AssumedWinOS)
                    crt = SharedWrapper.CRTreatment.RemoveCR;

            }
            
            while (true)
            {
                ValidataFtp.FtpSimpleLog.LogLine(string.Format("UPLOADING: {0}", targetFilePath));
                _SharedWrapper.CopyBytesTo(machineNameOrIP, targetFilePath, inputFileContents, crt, isSourceFile, out _ErrorMessage);
                ValidataFtp.FtpSimpleLog.LogLines(CurrentOperationLogLines);

                if (HasError)
                {
                    ValidataFtp.FtpSimpleLog.LogLine(string.Format("Error uploading: " + targetFilePath));
                    ValidataFtp.FtpSimpleLog.LogLine(string.Format("Exception: " + _ErrorMessage));
                    reportError();
                }
                else break;
            }
        }

        public void UploadFile(string remoteFilePath) //not used, not implemented
        {
            if (_AssumedWinOS)
                remoteFilePath.Replace(SharedWrapper.UNIX_PATH_SEP, SharedWrapper.WIN_PATH_SEP);
            ///else
            //  remoteFilePath.Replace(SharedWrapper.WIN_PATH_SEP, SharedWrapper.UNIX_PATH_SEP);

            string machineNameOrIP = _TelnetSet.GlobusServer.Split(new char[] { ':' })[0];
            _SharedWrapper.CopyTo(machineNameOrIP, "", "", out _ErrorMessage);

        }

        #endregion

        #region Deletions

        public void DeleteFile(string path)
        {
            string machineNameOrIP = _TelnetSet.GlobusServer.Split(new char[] { ':' })[0];
            while(true)
            {
                ValidataFtp.FtpSimpleLog.LogLine(string.Format("DELETING: {0}", path));
                _SharedWrapper.DeleteFile(machineNameOrIP, path, out _ErrorMessage);
                ValidataFtp.FtpSimpleLog.LogLines(CurrentOperationLogLines);

                if (HasError)
                {
                    ValidataFtp.FtpSimpleLog.LogLine(string.Format("Error deleting: " + path));
                    ValidataFtp.FtpSimpleLog.LogLine(string.Format("Exception: " + _ErrorMessage));
                    reportError();
                }
                else break;
            }
        }
        
        public bool DeleteFile(string path, out string errorMessage)
        {
            DeleteFile(path);

            errorMessage = _ErrorMessage;
            return HasError;

        }

        #endregion

        public void Dispose()
        { 
        }
        public void CreateDirectoryRecursively(string remFolder)
        {
            string machineNameOrIP = _TelnetSet.GlobusServer.Split(new char[] { ':' })[0];
            var dfp = _DirForPermissions != null ? _DirForPermissions : _TelnetSet.FtpHome;
            

            while (true)
            {
                ValidataFtp.FtpSimpleLog.LogLine(string.Format("CREATING DIRECTORY: {0}", remFolder));
                _SharedWrapper.CreateDirectory(machineNameOrIP, remFolder, dfp);
                ValidataFtp.FtpSimpleLog.LogLines(CurrentOperationLogLines);

                if (HasError)
                {
                    ValidataFtp.FtpSimpleLog.LogLine(string.Format("Error creating directory: " + remFolder));
                    ValidataFtp.FtpSimpleLog.LogLine(string.Format("Exception: " + _ErrorMessage));
                    reportError();
                }
                else break;
            }
        }

        public List<ValidataCommon.FileNameListItem> ListDirectoryItems(string dir)
        {
            var res = new List<ValidataCommon.FileNameListItem>();
            string machineNameOrIP = _TelnetSet.GlobusServer.Split(new char[] { ':' })[0];
            while (true)
            {
                _SharedWrapper.ListDir(machineNameOrIP, dir, out _ErrorMessage);
                ValidataFtp.FtpSimpleLog.LogLines(CurrentOperationLogLines);

                if (HasError)
                {
                    ValidataFtp.FtpSimpleLog.LogLine(string.Format("Error listing contents of directory: " + dir));
                    ValidataFtp.FtpSimpleLog.LogLine(string.Format("Exception: " + _ErrorMessage));

                    reportError();
                }
                else break;
            }

            foreach (string listingLine in _SharedWrapper.ListResultFiles)
            {
                var it = new ValidataFtp.FtpListItem();
                it.Name = listingLine;
                it.IsDirectory = false;
                it.FullPath = dir + ((dir.Length <= 1) ? "" : DirectorySeparator.ToString()) + it.Name;
                res.Add(it);
            }
            foreach (string listingLine in _SharedWrapper.ListResultFolders)
            {
                var it = new ValidataFtp.FtpListItem();
                it.Name = listingLine;
                it.IsDirectory = true;
                it.FullPath = dir + ((dir.Length <= 1) ? "" : DirectorySeparator.ToString()) + it.Name;
                res.Add(it);
            }
            return res;
        }

        private void reportError()
        {
             
            // if there is a problem say so, and ask the user if he wants to retry
            DialogResult result = MessageBox.Show(
                    _ErrorMessage,
                    HostnameWithoutPrefix + " Shared dirs operation failed",
                    MessageBoxButtons.RetryCancel,
                    MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1
                );

            if (result == DialogResult.Cancel)
            {
                throw new Exception(_ErrorMessage);
            }
        }

        public bool HasError
        {
            get { return !String.IsNullOrEmpty(_ErrorMessage); }
        }

        public string ErrorMessage { get { return _ErrorMessage; } }
    }
}
