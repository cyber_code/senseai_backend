﻿namespace SCApiInterface
{
    public interface IConfigurationUnit : IHierUnit
    {
        UnitAttribute Status { get; set; }

        string Description { get; set; }
    }
}