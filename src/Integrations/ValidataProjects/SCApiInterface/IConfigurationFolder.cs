﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCApiInterface
{
    public interface IConfigurationFolder : IConfigurationUnit
    {
        ConfigurationUnitStatus WorkPackStatus
        {
            get;
            set;
        }

        ConfigurationUnitStatus ChangeRequestStatus
        {
            get;
            set;
        }

        ConfigurationUnitStatus TaskStatus
        {
            get;
            set;
        }

        IUnit Inherits
        {
            get;
            set;
        }

        bool IsAuto
        {
            get;
        }

        IEnumerable<ITask> Tasks
        {
            get;
        }
    }
}
