﻿namespace SCApiInterface
{
    public interface IRecord : IDataSourceUnit
    {
        string Name2 { get; set; }
        
        string CatalogName { get; set; }

        string TypicalName { get; set; }

        string VersionName { get; set; }
    }
}