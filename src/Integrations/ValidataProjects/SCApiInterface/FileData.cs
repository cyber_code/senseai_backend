﻿using System;
using System.IO;
using System.Text;

namespace SCApiInterface
{
    [Serializable]
    public class FileData
    {
        #region Private & Protected Members

        protected virtual byte[] BinArray { get; set; }

        #endregion

        #region Class Lifecycle

        // Default constructor is needed for serialization
        public FileData()
        {
        }

        public FileData(byte[] binArray)
        {
            BinArray = binArray;
        }

        public FileData(string str)
        {
            BinArray = StringToByteArr(str);
        }

        public FileData(Stream stream)
        {
            BinArray = new byte[stream.Length];
            stream.Read(BinArray, 0, (int) stream.Length);
        }

        #endregion

        #region Public Methods

        public byte[] ToBinaryArray()
        {
            return BinArray;
        }

        public string ToXml()
        {
            return ByteArrToString(BinArray);
        }

        public static string ByteArrToString(byte[] arr)
        {
            return Encoding.UTF8.GetString(arr);
        }

        public static byte[] StringToByteArr(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }

        public bool IsEqual(FileData fileData)
        {
            byte[] a = BinArray;
            if (a != null && a.Length == 0)
            {
                a = null;
            }
            byte[] b = fileData.BinArray;
            if (b != null && b.Length == 0)
            {
                b = null;
            }

            if (a == null && b == null)
            {
                return true;
            }

            if (a == null || b == null)
            {
                return false;
            }

            if (a.Length != b.Length)
            {
                return false;
            }

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i])
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        public virtual bool IsEmpty()
        {
            return BinArray == null || BinArray.Length == 0;
        }

        public string DataAsString()
        {
            var res = new StringBuilder();
            foreach(int i in BinArray)
            {
                res.Append((char)i);
            }
            return res.ToString();
        }
    }
}