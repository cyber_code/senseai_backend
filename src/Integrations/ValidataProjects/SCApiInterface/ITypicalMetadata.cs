﻿namespace SCApiInterface
{
    public interface ITypicalMetadata : IUnit
    {
        FileData Data { get; }

        string[] TagStructureAttributes { get; }

        string TagStructureDisplayDelimiter { get; }
    }
}