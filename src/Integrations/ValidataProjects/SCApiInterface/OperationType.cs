﻿namespace SCApiInterface
{
    public enum OperationType
    {
        Unknown,
        Get,
        GetUnit,
        GetUnits,
        GetParent,
        GetChildren,
        GetTasks,
        Checkout,
        UndoCheckout,
        Checkin,
        Delete,
        Link,
        Create,
        Search,
        Configure,
        Update,
        LoadFolder,
        LoadFile,
        LoadRecord,
        GetConfiguredTask,
        GetProjectConfigurationRules,
        Move,
        GetHistory,
        GetHistoryVersion,
        GetLatest,
        GetForeignKeys,
        GetMetadata,
        EventReceived,
        GetReleases,
        GetLatestReleases,
        GetDiagnosticsSnapshot,
        GetUseCases,
        GetRequirements,
        GetDeploymentsByOwnerId,
        SetProjectConfigurationRules,
        InitializePerformanceTestingEnvironment,
        ExecuteDiagnostic,
        GetTasksOfproject,
        Merge,
        GetDefaultUnit,
        GetUnitAttributeDefinitions,
        GetNotifications,
        GetBusinessModel,
        GetCatalogDefinition,
        GetT24Environments,
        GetDeployments,
        IsTaskInProjectConfiguration,
        GetAccessRoles,
        UpdateChangeRequestAttachments,

        Other
    }
}
