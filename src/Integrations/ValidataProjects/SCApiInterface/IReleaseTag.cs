﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCApiInterface
{
    public interface IReleaseTag : IUnit
    {
        IReleaseTag Previous
        {
            get;
        }

        IReleaseTag Next
        {
            get;
        }
    }
}
