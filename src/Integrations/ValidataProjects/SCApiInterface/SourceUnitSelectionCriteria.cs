﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCApiInterface
{
    [Serializable]
    public class SourceUnitSelectionCriteria
    {
        #region Private Enums

        private enum SelectionType
        {
            FullMatch,

            InterfaceMatch
        }

        #endregion

        #region Private Members

        private SelectionType _SelectionType = SelectionType.FullMatch;

        private Type UnitInterfaceType;

        private string Name;

        private string RelationalPath;

        private string CatalogName;

        private string TypicalName;

        #endregion

        #region Class Lifecycle

        private SourceUnitSelectionCriteria()
        {
        }

        private SourceUnitSelectionCriteria(SelectionType selectionType)
        {
            _SelectionType = selectionType;
        }

        #endregion

        public override string ToString()
        {
            StringBuilder sbResult = new StringBuilder();

            var lst = UnitInterfaceType.ToString().Split(new char[] { '.' });
            sbResult.AppendFormat("{0}: ", lst[lst.Length - 1]);

            if (UnitInterfaceType == typeof(IFile) || UnitInterfaceType == typeof(IFolder))
            {
                sbResult.AppendFormat("'{0}\\{1}'", RelationalPath, Name);
            }
            else if (UnitInterfaceType == typeof(IDeployPackage))
            {
                sbResult.AppendFormat("'{0}'", Name);
            }
            else if (UnitInterfaceType == typeof(IRecord))
            {
                sbResult.AppendFormat("Tag='{0}' ([{1}]{2})", Name, CatalogName, TypicalName);
            }
            else
            {
                System.Diagnostics.Debug.Fail("Invalid unit type");
                sbResult.Append("Invalid unit type");
            }

            return sbResult.ToString();
        }

        public static SourceUnitSelectionCriteria Create(ISourceUnit sourceUnit)
        {
            if (sourceUnit == null)
            {
                return null;
            }

            SourceUnitSelectionCriteria result = new SourceUnitSelectionCriteria(SelectionType.FullMatch);
            result.Name = sourceUnit.Name;

            if (sourceUnit is IFile)
            {
                result.UnitInterfaceType = typeof(IFile);
                result.RelationalPath = sourceUnit.RelationalPath;
            }
            else if (sourceUnit is IFolder)
            {
                result.UnitInterfaceType = typeof(IFolder);
                result.RelationalPath = sourceUnit.RelationalPath;
            }
            else if (sourceUnit is IRecord)
            {
                result.UnitInterfaceType = typeof(IRecord);
                result.CatalogName = (sourceUnit as IRecord).CatalogName;
                result.TypicalName = (sourceUnit as IRecord).TypicalName;
            }
            else if (sourceUnit is IDeployPackage)
            {
                result.UnitInterfaceType = typeof(IDeployPackage);
            }
            else
            {
                System.Diagnostics.Debug.Fail("Unit type not supported!");
                return null;
            }

            return result;
        }

        public static SourceUnitSelectionCriteria Create(string interfaceType)
        {
            SourceUnitSelectionCriteria result = new SourceUnitSelectionCriteria(SelectionType.InterfaceMatch);
            result.UnitInterfaceType = GetTypeFromInterfaceString(interfaceType);

            return result;
        }

        private static Type GetTypeFromInterfaceString(string interfaceType)
        {
            switch (interfaceType)
            {
                case "IFile":
                    return typeof(IFile);
                case "IFolder":
                    return typeof(IFolder);
                case "IRecord":
                    return typeof(IRecord);
                case "IDeployPackage":
                    return typeof(IDeployPackage);
                default:
                    throw new Exception("Unsupported unit interface type");
            }
        }

        public bool Match(ISourceUnit sourceUnit)
        {
            switch (_SelectionType)
            {
                case SelectionType.FullMatch:
                    return FullMatch(sourceUnit);
                case SelectionType.InterfaceMatch:
                    return InterfaceMatch(sourceUnit);
                default:
                    throw new NotImplementedException("Unknown selection type!");
            }
        }

        private bool FullMatch(ISourceUnit sourceUnit)
        {
            if (UnitInterfaceType == typeof(IFile))
            {
                return (sourceUnit is IFile) &&
                        (sourceUnit.Name == this.Name) &&
                        (sourceUnit.RelationalPath == this.RelationalPath);
            }
            else if (UnitInterfaceType == typeof(IFolder))
           {
                if (!(sourceUnit is IFile) && !(sourceUnit is IFolder))
                {
                    return false;
                }

                if (sourceUnit is IFolder)
                if (sourceUnit.Name == this.Name)
                if (sourceUnit.RelationalPath == this.RelationalPath)
                {
                    // root folder
                    return true;
                }

                string relPath = this.RelationalPath + "\\" + this.Name;
                relPath = relPath.TrimStart(new char[] { '\\' });
                string suRelPath = sourceUnit.RelationalPath.TrimStart(new char[] { '\\' });

                return (relPath == suRelPath) ||
                         suRelPath.StartsWith(relPath + "\\");
            }
            else if (UnitInterfaceType == typeof(IRecord))
            {
                if(!(sourceUnit is IRecord) ||
                    ((sourceUnit as IRecord).CatalogName != this.CatalogName) ||
                    ((sourceUnit as IRecord).TypicalName != this.TypicalName)
                    )
                {
                    return false;
                }

                return (this.Name==null/*every record of typical*/) ||
                       (sourceUnit.Name == this.Name);
            }
            else if (UnitInterfaceType == typeof(IDeployPackage))
            {
                return (sourceUnit is IDeployPackage) &&
                        (sourceUnit.Name == this.Name);
            }
            else
            {
                System.Diagnostics.Debug.Fail("Unsupprted unit type!");
                return false;
            }
        }

        private bool InterfaceMatch(ISourceUnit sourceUnit)
        {
            if(UnitInterfaceType == typeof(IFile))
            {
                return sourceUnit is IFile;
            }

            if(UnitInterfaceType == typeof(IFolder))
            {
                return sourceUnit is IFolder;
            }

            if(UnitInterfaceType == typeof(IRecord))
            {
                return sourceUnit is IRecord;
            }

            if(UnitInterfaceType == typeof(IDeployPackage))
            {
                return sourceUnit is IDeployPackage;
            }

            System.Diagnostics.Debug.Fail("Unsupprted unit type!");
            return false;
        }
    }
}
