﻿namespace SCApiInterface
{
    public enum BaselineType
    {
        Standard,
        Integration
    }

    public enum BaselineStatus
    {
        Created,
        Completed
    }

    public enum BaselineConflictType
    {
        ProjectConfigurationConflict,
        BaselineConflict
    }
}