﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IProject : IBranchableUnit
    {
        UnitAttribute Type { get; set; }

        IEnumerable<IConfigurationRule> ConfigurationRules { get; }

        IConfigurationRule CreateConfigurationRule(IUser user, string name);

        IBaseline ParentBaseline { get; }

        IEnumerable<IProject> ParentBaselineProjects { get; }
    }
}