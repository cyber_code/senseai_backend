﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCApiInterface
{
    public interface ISummaryTask : ISummaryHierUnit, IHierUnit
    {
        ulong[] TasksNoderefs
        {
            get; set;
        }
    }
}
