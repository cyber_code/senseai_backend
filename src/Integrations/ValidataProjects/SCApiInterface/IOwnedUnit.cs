﻿using System;

namespace SCApiInterface
{
    public interface IOwnedUnit : IUnit
    {
        IUser Owner { get; }

        IRelease Release { get; set; }

        DateTime CreateDate { get; }

        DateTime UpdateDate { get; }
    }
}