﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace SCApiInterface
{
    [Serializable]
    public class UnitAttribute
    {
        #region Enums

        public enum DataType
        {
            String = 0,

            Integer,

            DateTime,

            Default = String
        }

        #endregion

        #region Private Members

        private string _Value;

        private DataType _DataType = DataType.Default;

        #endregion

        #region Public Properties

        public DataType ValueType
        {
            get { return _DataType; }
            set { _DataType = value; }
        }

        public string Value
        {
            get { return _Value; }

            set
            {
                if (_Value == value)
                {
                    return;
                }

                if (IsReadOnly)
                {
                    // todo: use: ExceptionManager.InternalSCException()
                    throw new Exception("SECURITY VIOLATION: Unable to se up attribute value!");
                }

                if (!IsPossibleTransition(value))
                {
                    // todo: use: ExceptionManager.InternalSCException()
                    throw new Exception(string.Format("SECURITY VIOLATION: Unable to set up attribute value! ({0})", value));
                }

                _Value = value;
            }
        }

        [XmlIgnore]
        public int IntValue
        {
            get
            {
                ValidateDataType(DataType.Integer, "GET");

                try { return int.Parse(this.Value); }
                catch { return default(int); }
            }
            set
            {
                ValidateDataType(DataType.Integer, "SET");

                this.Value = value.ToString();
            }
        }

        [XmlIgnore]
        public DateTime DateTimeValue
        {
            get
            {
                ValidateDataType(DataType.DateTime, "GET");

                try { return new DateTime(long.Parse(Value)); }
                catch { return DateTime.MinValue; }
            }
            set
            {
                ValidateDataType(DataType.DateTime, "SET");

                DateTime val = value;
                if (Quality != null)
                {
                    if (val <= Quality.DateTimeMinValue || val >= Quality.DateTimeMaxValue)
                    {
                        val = DateTime.MinValue;
                    }
                }
                else
                {
                    System.Diagnostics.Debug.Fail("Please supply quality for each DateTime UnitAttributes");
                }

                this.Value = val.Ticks.ToString();
            }
        }

        public virtual string[] NextTransitions { get; set; }

        public bool IsReadOnly
        {
            get
            {
                return (NextTransitions != null) &&
                       (NextTransitions.Length == 0);
            }
        }

        public UnitAttributeQuality Quality
        {
            get;
            set;
        }

        public bool IsDefined
        {
            get {
                switch (_DataType)
                {
                    case DataType.DateTime:
                        return (Quality == null) || (DateTimeValue > Quality.DateTimeMinValue);
                    case DataType.String:
                    case DataType.Integer:
                    default:
                        return true;
                }
            }
        }

        #endregion

        #region Class Lifecycle

        public UnitAttribute()
        {
            // empty constructor needed for XML serializaion  - don't remove
        }

        public UnitAttribute(string value, string[] nextTransitions)
        {
            _Value = value;
            NextTransitions = nextTransitions;
        }

        public UnitAttribute(string value)
            : this(value, null)
        {
        }

        public UnitAttribute(int value) 
            : this(value.ToString())
        {
            _DataType = DataType.Integer;
        }

        public UnitAttribute(DateTime value)
            : this(value.Ticks.ToString())
        {
            _DataType = DataType.DateTime;
        }

        #endregion

        #region Public Methods

        public bool IsPossibleTransition(string value)
        {
            if (NextTransitions == null)
            {
                return true;
            }

            return IsStringInArray(value, NextTransitions);
        }

        private static bool IsStringInArray(string val, string[] sArray)
        {
            foreach (string s in sArray)
            {
                if (s == val)
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region Private Methods

        private void ValidateDataType(DataType dataType, string method)
        {
            if (_DataType != dataType)
            {
                throw new Exception(
                    string.Format(
                            "Internal Error: Unable to {0} {1} value of {2} attribute",
                            method,
                            dataType,
                            _DataType
                        )
                    );
            }
        }

        #endregion

        #region Object Overrides

        public override string ToString()
        {
            switch (_DataType)
            {
                case DataType.DateTime:
                    return DateTimeValue.ToString();
                case DataType.Integer:
                    return IntValue.ToString();
                case DataType.String:
                default:
                    return _Value;
            }
        }

        #endregion

        #region Operators

        public static bool operator ==(UnitAttribute a1, UnitAttribute a2)
        {
            return (string.Compare(a1, a2) == 0);
        }

        public static bool operator !=(UnitAttribute a1, UnitAttribute a2)
        {
            return (string.Compare(a1, a2) != 0);
        }

        public static bool operator >(UnitAttribute a1, UnitAttribute a2)
        {
            if (a1.ValueType != a2.ValueType)
            {
                throw new Exception("Unable to compare (>) unit attributes with different value types");
            }

            switch (a1.ValueType)
            {
                case DataType.Integer:
                    return a1.IntValue > a2.IntValue;
                case DataType.DateTime:
                    return a1.DateTimeValue > a2.DateTimeValue;
                default:
                    throw new Exception("Unable to compare (>) string unit attributes");
            }
        }

        public static bool operator >=(UnitAttribute a1, UnitAttribute a2)
        {
            return a1 == a2 || a1 > a2;
        }

        public static bool operator <(UnitAttribute a1, UnitAttribute a2)
        {
            if (a1.ValueType != a2.ValueType)
            {
                throw new Exception("Unable to compare (<) unit attributes with different value types");
            }

            switch (a1.ValueType)
            {
                case DataType.Integer:
                    return a1.IntValue < a2.IntValue;
                case DataType.DateTime:
                    return a1.DateTimeValue < a2.DateTimeValue;
                default:
                    throw new Exception("Unable to compare (<) string unit attributes");
            }
        }

        public static bool operator <=(UnitAttribute a1, UnitAttribute a2)
        {
            return a1 == a2 || a1 < a2;
        }

        public static implicit operator string(UnitAttribute attr)
        {
            if ((attr as object) == null)
            {
                return null;
            }

            return attr.Value;
        }

        public static implicit operator UnitAttribute(string str)
        {
            return new UnitAttribute(str);
        }

        public static implicit operator int(UnitAttribute attr)
        {
            if ((attr as object) == null)
            {
                return default(int);
            }

            return attr.IntValue;
        }

        public static implicit operator UnitAttribute(int value)
        {
            return new UnitAttribute(value);
        }

        public static implicit operator DateTime(UnitAttribute attr)
        {
            if ((attr as object) == null)
            {
                return DateTime.MinValue;
            }

            return attr.DateTimeValue;
        }

        public static implicit operator UnitAttribute(DateTime value)
        {
            return new UnitAttribute(value);
        }

        #endregion
    }
}