﻿namespace SCApiInterface
{
    public interface IFile : IDataSourceUnit
    {
        bool IsBinary { get; }
        bool IsDLPackage { get; }
    }
}