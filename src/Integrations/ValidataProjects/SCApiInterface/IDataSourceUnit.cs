﻿namespace SCApiInterface
{
    public interface IDataSourceUnit : ISourceUnit, IData
    {
        ContentType ContentType { get; set; }

        int OrderPlace { get; set; }
    }
}