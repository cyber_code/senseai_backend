﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IBaselineEdit
    {
        UnitAttribute Name { get; set; }

        BaselineStatus Status { get; set; }

        BaselineType Type { get; set; }

        IRelease Release { get; set; }

        ICollection<IProject> Projects { get; }

        ICollection<IRequirement> Requirements { get; }

        IProject RootProject { get; set; }

        ICollection<ISourceUnit> SourceUnits { get; }

        ICollection<ITask> ExcludedTasks { get; }

        ICollection<IConflict> Conflicts { get; }
    }
}