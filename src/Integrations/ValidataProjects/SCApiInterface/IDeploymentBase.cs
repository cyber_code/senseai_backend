﻿using System;

namespace SCApiInterface
{
    public interface IDeploymentBase : IHierUnit, IData
    {
        IT24Environment Environment { get; }
    }
}
