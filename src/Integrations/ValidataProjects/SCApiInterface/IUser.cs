﻿using System;

namespace SCApiInterface
{
    public interface IUser : IUnit
    {
        Int64 ClientId { get; set; }
        string LoginName { get; }
        bool IsDeletedFromDB { get; }

        DateTime LastActive { get; set; }
    }
}