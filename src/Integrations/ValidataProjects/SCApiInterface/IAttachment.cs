﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCApiInterface
{
    [System.ServiceModel.ServiceKnownType(typeof(IAttachment))]
    public interface IAttachment : IHierUnit, IData
    {
        // Seems to be useless
        // string ArchiveName { get; set; }

        [System.Runtime.Serialization.DataMember]
        string FileName { get; set; }

        [System.Runtime.Serialization.DataMember]
        string DocumentType { get; set; }
    }
}
