﻿using System;

namespace SCApiInterface
{
    [Serializable]
    public class UserCredentials
    {
        public string Discipline
        {
            get; private set;
        }

        public string Role
        {
            get;
            private set;
        }

        public string UserName
        {
            get;
            private set;
        }

        public string Password
        {
            get;
            private set;
        }

        public UserCredentials(string discipline, string role, string userName, string password)
        {
            Discipline = discipline;
            Role = role;
            UserName = userName;
            Password = password;
        }
    }
}
