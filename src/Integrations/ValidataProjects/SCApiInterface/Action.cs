﻿using System;

namespace SCApiInterface
{
    [Serializable]
    public enum UnitAction
    {
        Undefined,

        Create,

        Branch,

        Merge,

        Modify,

        Delete
    }
}