﻿namespace SCApiInterface
{
    public enum UnitType
    {
        File,
        Record,
        DeploymentPackage
    }

    public enum StorableUnitType
    {
        File,
        Folder,
        Record,
        DeploymentPackage,
        Project
    }
}