﻿namespace SCApiInterface
{
    public enum ClientFeedbackType
    {
        Cancel,
        Feedback,
        Continue
    }
}