﻿namespace SCApiInterface
{
    public interface IFeature : IExtendableConfigurationUnit
    {
        string Purpose { get; set; }

        string ProductName { get; set; }

        string ProductNoderef { get; set; }

        string StreamNoderef { get; set; }
        IReleaseStream ReleaseStream { get; }

        IUseCase CreateUseCase(IUser user, string name);
    }
}