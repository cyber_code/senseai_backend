﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IHierUnit : IOwnedUnit
    {
        IUnit Parent { get; }

        ulong ParentNoderef { get; }

        IEnumerable<IUnit> Children { get; }
    }
}