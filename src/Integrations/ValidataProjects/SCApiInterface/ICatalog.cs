﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCApiInterface
{
    [Serializable]
    public class TypicalSimpleDefinition
    {
        public string Name;

        public string BaseClassName;

        public ulong BaseClassNoderef;
    }

    public interface ICatalog : IUnit
    {
        List<TypicalSimpleDefinition> Typicals
        {
            get;
        }
    }
}
