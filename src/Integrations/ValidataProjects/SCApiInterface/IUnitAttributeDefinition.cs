﻿using System;

namespace SCApiInterface
{
    public interface IUnitAttributeDefinition : IUnit
    {
        Type UnitType { get; }

        string[] LookUpValues { get; set; }

        bool IsLookUp { get; }
    }
}