﻿using System;
using System.Collections.Generic;

namespace SCApiInterface
{
    public class PackageProperty : IHierUnit
    {
        public IUnit InfantParent
        {
            get { return Parent; }
        }

        public bool EquivalentWithDiffs(IUnit item)
        {
            return Value != ((PackageProperty) item).Value;
        }

        public string Value { get; set; }
        public int CompareTo(IUnit other)
        {
            return Name.Value.CompareTo(((PackageProperty) other).Name.Value);
        }

        public ulong Noderef
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public UnitAttribute Name { get; set; }

        public IUnit Clone(IUser user)
        {
            throw new NotImplementedException();
        }

        public IUser Owner
        {
            get { throw new NotImplementedException(); }
        }

        public IRelease Release
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public DateTime CreateDate
        {
            get { throw new NotImplementedException(); }
        }

        public DateTime UpdateDate
        {
            get { throw new NotImplementedException(); }
        }

        public IUnit Parent { get; set; }

        public ulong ParentNoderef
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerable<IUnit> Children
        {
            get { throw new NotImplementedException(); }
        }
        public override string ToString()
        {
            return Name.Value;
        }
    }
}