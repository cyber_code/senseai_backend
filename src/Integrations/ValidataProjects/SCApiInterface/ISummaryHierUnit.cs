﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCApiInterface
{
    public interface ISummaryHierUnit
    {
        ISummaryTask SummaryTaskParent
        {
            get;
        }

        ulong SummaryTaskParentNoderef
        {
            get;
        }
    }
}
