﻿namespace SCApiInterface
{
    public interface IInfant <T>
    {
        T InfantParent { get; }

        bool EquivalentWithDiffs(T item);
    }
}