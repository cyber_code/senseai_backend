﻿namespace SCApiInterface
{
    public interface IUseCase : IExtendableConfigurationUnit
    {
        string PreConditions { get; set; }

        string PostConditions { get; set; }

        IReleaseStream ReleaseStream { get; }

        IRequirement CreateRequirement(IUser user, string name);
    }
}