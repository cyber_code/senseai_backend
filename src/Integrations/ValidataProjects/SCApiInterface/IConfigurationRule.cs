﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IConfigurationRule : IHierUnit
    {
        SelectionSettings WorkPackSelectionSettings { get; set; }

        SelectionSettings ChangeRequestSelectionSettings { get; set; }

        SelectionSettings TaskSelectionSettings { get; set; }

        IUnit Inherits { get; set; }

        bool IsAuto { get; }

        bool IsAllowed(IWorkPackage workPackage);

        bool IsAllowed(IChangeRequest changeRequest);

        bool IsAllowed(ITask task);

        bool IsEqual(IConfigurationRule rule);
    }
}
