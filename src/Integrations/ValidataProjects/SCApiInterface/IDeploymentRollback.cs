﻿using System;
using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IDeploymentRollback : IDeploymentBase
    {
        IList<IUnit> Deployments { get; }

        ulong[] DeploymentsNoderefs { get; }
    }
}
