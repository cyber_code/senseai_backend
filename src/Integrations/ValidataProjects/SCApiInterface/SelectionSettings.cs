﻿using System;
using System.Collections.Generic;

namespace SCApiInterface
{
    [Serializable]
    public class SelectionSettings
    {
        #region Public Properties

        /// <summary>
        /// Settings for Status Attribute
        /// </summary>
        public UnitAttribute Status
        {
            get;
            set;
        }

        /// <summary>
        /// Key is attribute name, Value is value + restrictions
        /// </summary>
        public Dictionary<string, UnitAttribute> Advanced
        {
            get;
            set;
        }

        /// <summary>
        /// Gets: Are advanced settings defined
        /// </summary>
        public bool IsAdvancedSettingsDefined
        {
            get {
                if (Advanced == null)
                {
                    return false;
                }

                foreach(string key in Advanced.Keys)
                {
                    if(!string.IsNullOrEmpty(Advanced[key]))
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        #endregion

        #region Class Lifecycle

        public SelectionSettings(string statusValue)
        {
            Status = new UnitAttribute(statusValue);
            Advanced = new Dictionary<string, UnitAttribute>();
        }

        public SelectionSettings(SelectionSettings src)
        {
            this.Status = new UnitAttribute(src.Status.Value, src.Status.NextTransitions);

            this.Advanced = new Dictionary<string, UnitAttribute>();
            foreach (string key in src.Advanced.Keys)
            {
                this.Advanced.Add(
                        key,
                        new UnitAttribute(src.Advanced[key].Value, src.Advanced[key].NextTransitions)
                    );
            }
        }

        #endregion

        #region Public Methods

        public bool IsEqual(SelectionSettings other)
        {
            if(this.Status.ToString() != other.Status.ToString())
            {
                return false;
            }

            if (this.Advanced == null && other.Advanced == null)
            {
                return true;
            }

            if (this.Advanced == null || other.Advanced == null)
            {
                return false;
            }

            if (this.Advanced.Count != other.Advanced.Count)
            {
                return false;
            }

            foreach (string key in this.Advanced.Keys)
            {
                if (!other.Advanced.ContainsKey(key))
                {
                    return false;
                }
            }

            foreach (string key in this.Advanced.Keys)
            {
                UnitAttribute uaThisValue = this.Advanced[key];
                UnitAttribute uaOtherValue = other.Advanced[key];
                if (uaThisValue.ToString() != uaOtherValue.ToString())
                {
                    return false;
                }
            }

            return true;
        }

        #endregion
    }
}
