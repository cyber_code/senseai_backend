﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IT24Environment : IHierUnit, IData
    {
        string TelnetConfiguration { get; set; }

        string SystemType { get; set; }

        string InputVersion { get; set; }

        string ServerIncludePath { get; set; }

        string ServerName { get; set; }

        string ServerUser { get; set; }

        string ServerPassword { get; set; }

        string FtpUrl { get; set; }

        string FtpUser { get; set; }

        string FtpPassword { get; set; }

        bool FtpEnableSsl { get; set; }

        bool FtpUsePassive { get; set; }

        string Protocol { get; set; }

        string IdentityFilePath { get; set; }

        bool IsDefaultEnv { get; set; }

        string Passphrase { get; set; }

        IUser LockedBy { get; set; }

        IEnumerable<IUnit> Deployments { get; }
    }
}
