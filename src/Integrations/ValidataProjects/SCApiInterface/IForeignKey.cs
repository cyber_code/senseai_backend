﻿namespace SCApiInterface
{
    public interface IForeignKey : IUnit
    {
        string Catalog { get; }

        string Typical { get; }

        string Attribute { get; }

        string ReferencingCatalog { get; }

        string ReferencingTypical { get; }

        string ReferencingAttribute { get; }
    }
}