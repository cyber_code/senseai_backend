﻿using System;

namespace SCApiInterface
{
    [Serializable]
    public class LogInResultArgs
    {
        public enum OperationResult
        {
            Success,
            InvalidUserNameOrPassword,
            LicensesExceeded,
            NoLicenseForSAS,
            InvalidLicense,
            NoPermissions,
            NoConnection,
            DifferentVersion,
            AlreadyLoggedIn,
            UnknownError
        }

        public OperationResult Result
        {
            get; set;
        }

        public SASClientLogInMode Mode
        {
            get; set;
        }

        public bool IsAdmin
        { 
            get; set;
        }

        public string ErrorMessage
        {
            get; set;
        }

        public LogInResultArgs()
        {
            Result = OperationResult.Success;
        }

        public LogInResultArgs(OperationResult result, SASClientLogInMode mode, bool isAdmin, string errorMessage)
        {
            Result = result;
            Mode = mode;
            IsAdmin = isAdmin;
            ErrorMessage = errorMessage;
        }
    }
}
