﻿using System;

namespace SCApiInterface
{
    [Serializable]
    public class UnitAttributeQuality
    {
        public int IntMinValue
        {
            get;
            set;
        }

        public int IntMaxValue
        {
            get;
            set;
        }

        public DateTime DateTimeMinValue
        {
            get;
            set;
        }

        public DateTime DateTimeMaxValue
        {
            get;
            set;
        }

        public UnitAttributeQuality()
        {
        }

        public UnitAttributeQuality(int min, int max)
        {
            IntMinValue = min;
            IntMaxValue = max;
        }

        public UnitAttributeQuality(DateTime min, DateTime max)
        {
            DateTimeMinValue = min;
            DateTimeMaxValue = max;
        }
    }
}
