﻿using System;
using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IDeployment : IDeploymentBase
    {
        IBaseline Baseline { get; }

        ulong BaselineNoderef { get; }

        DeploymentType DeploymentType { get; set; }

        IList<IUnit> DeploymentPackages { get; }

        IDeploymentRollback DeploymentRollback { get; }

        ulong DeploymentRollbackNoderef { get; }

        string ExecutionStatus { get; set; }

        DateTime DeploymentCompletionTime { get; set; }

        DateTime DeploymentStartTime { get; set; }

        string ScheduledJobDetails { get; set; }

        string MachineID { get; set; }
    }
}
