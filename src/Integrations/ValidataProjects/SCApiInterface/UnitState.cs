﻿using System;

namespace SCApiInterface
{
    [Serializable, Flags]
    public enum UnitState
    {
        CheckedIn = 1,

        CheckedOut = 2,

        Create = 4,

        Deleted = 8
    }
}