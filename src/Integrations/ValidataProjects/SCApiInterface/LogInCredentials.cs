﻿using System;

namespace SCApiInterface
{
    [Serializable]
    public class LogInCredentials
    {
        public string MachineID
        {
            get; private set;
        }

        public string MacAddress
        {
            get;
            private set;
        }

        public string User
        {
            get;
            private set;
        }

        public SASClientLogInMode LogInMode
        {
            get;
            private set;
        }

        public LogInCredentials(string machineID, string macAddress, string user, SASClientLogInMode mode)
        {
            MachineID = machineID;
            MacAddress = macAddress;
            User = user;
            LogInMode = mode;
        }
    }
}
