﻿namespace SCApiInterface
{
    public enum Severity
    {
        None = 0,

        Low,

        Medium,

        High,

        Critical
    }
}