﻿namespace SCApiInterface
{
    public interface IClientFeedback
    {
        OperationType OperationType { get; set; }

        object Data { get; set; }

        ClientFeedbackType ClientFeedbackType { get; set; }
    }
}