﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCApiInterface
{
    public interface IWorkPack : IConfigurationUnit
    {
        IEnumerable<IChangeRequest> ChangeRequests
        {
            get;
        }

        IChangeRequest CreateChangeRequest(IUser user, string name);
    }
}
