﻿namespace SCApiInterface
{
    public interface ICheckInConflict : IConflict
    {
        ulong SourceForMergeNoderef { get; }

        ulong TargetForMergeNoderef { get; }

        ulong CommonPredecessorNoderef { get; }

        ISourceUnit Resolution { get; set; }
    }
}