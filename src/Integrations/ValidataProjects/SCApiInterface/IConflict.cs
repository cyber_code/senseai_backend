﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IConflict : IUnit
    {
        string Message { get; }

        Severity Severity { get; }

        IEnumerable<IUnit> ConflictedUnits { get; }

        ulong[] ConflictedUnitsNoderefs { get; }

        string[] ReleaseObjectsToMergeBy { get; }

        MergeToken MergeProposition { get; }
    }
}