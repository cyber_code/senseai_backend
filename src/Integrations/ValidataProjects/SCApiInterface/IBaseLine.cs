﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IBaseline : IHierUnit
    {
        BaselineStatus Status { get; }

        BaselineType Type { get; set; }

        IEnumerable<IProject> Projects { get; }

        IEnumerable<IRequirement> Requirements { get; }

        IProject RootProject { get; }

        IEnumerable<ISourceUnit> SourceUnits { get; }

        IEnumerable<ITask> ExcludedTasks { get; }

        IEnumerable<IConflict> Conflicts { get; }

        IEnumerable<IDeployment> Deployments { get; }
    }
}