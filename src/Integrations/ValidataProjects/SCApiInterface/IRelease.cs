﻿using System;
using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IRelease : IHierUnit
    {
        ulong StreamNoderef { get; }

        UnitAttribute Status { get; set; }

        IReleaseStream Stream { get; }

        IRelease Previous { get; }

        IRelease Next { get; }

        ulong PreviousNoderef { get; }

        ulong NextNoderef { get; }

        bool IsGreatherThan(IRelease release);

        UnitAttribute StartDate { get; set; }

        UnitAttribute EndDate { get; set; }

        IList<string> ClosedStatuses { get; set; }

        IEnumerable<IUnit> GetAssociatedChildren(Type type);
    }
}