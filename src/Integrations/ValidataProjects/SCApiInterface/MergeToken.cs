﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCApiInterface
{
    //[System.ServiceModel.ServiceKnownType(typeof(SCApiInterface.MergeBatch))]
    //[System.Runtime.Serialization.KnownType(typeof(SCApiInterface.MergeBatch))]
    //[System.Runtime.Serialization.DataContract()]

   [Serializable]
   [System.Runtime.Serialization.DataContract]
    public class MergeBatch
    {
        [System.Runtime.Serialization.DataMember]
        public List<ulong> NoderefsToMerge = new List<ulong>();

        [System.Runtime.Serialization.DataMember]
        public string OverlappedPath;

        [System.Runtime.Serialization.DataMember]
        public string LatestCRName;

        public bool HasEqualNoderefs(MergeBatch mb)
        {
            if (mb.NoderefsToMerge.Count != this.NoderefsToMerge.Count) return false;

            for (int mcc = 0; mcc < NoderefsToMerge.Count; mcc++ )
            {
                if (NoderefsToMerge[mcc] != mb.NoderefsToMerge[mcc])
                    return false;
            }
            return true;
        }
    }

    [Serializable]
    [System.Runtime.Serialization.CollectionDataContract]
    public class MergeBatchCollection : List<MergeBatch>
    { 
    
    }
    
    [Serializable]
    public class MergeToken
    {
        
        public bool ConflictsMatch = false;

        public ITask DefaultTask = null;

        [System.Runtime.Serialization.DataMember]
        private MergeBatchCollection _MergeBatches = new MergeBatchCollection();

        [System.Runtime.Serialization.DataMember]
        public MergeBatchCollection MergeBatches { get { return _MergeBatches; } }

        public void AddBatch(List<ulong> noderefsToMerge, string path, string crName)
        {
            _MergeBatches.Add(new MergeBatch() { 
            
                NoderefsToMerge = noderefsToMerge,
                OverlappedPath = path,
                LatestCRName = crName

            });        
        }

        public bool IsEquivalent(MergeToken mt)
        {
            if (_MergeBatches.Count != mt.MergeBatches.Count)
                return false;

            foreach (var batch in _MergeBatches)
            {
                if (!mt._MergeBatches.Any<MergeBatch>(mb => mb.HasEqualNoderefs(batch)))
                    return false;
            }

            return true;
        }


    }
}
