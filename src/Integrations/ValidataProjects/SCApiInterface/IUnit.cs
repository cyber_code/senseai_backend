﻿using System;

namespace SCApiInterface
{
    public interface IUnit : IInfant<IUnit>, IComparable<IUnit>
    {
        ulong Noderef { get; }

        bool IsReadOnly { get; }

        UnitAttribute Name { get; set; }

        IUnit Clone(IUser user);
    }
}