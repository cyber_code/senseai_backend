﻿using System.Collections.Generic;

namespace SCApiInterface
{
  
    public interface IDeployPackage : IDataSourceUnit
    {
        PackageTypes PackageType { get; set; }
        string TargetPath { get; set; }
        string BConId { get; set; }
        Dictionary<string, string> UserFields { get; set; }
    }

    public enum PackageTypes
    {
        Normal,
        CoreUpdate,
        TamUpdate
    }
}