﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IWorkPackage : IConfigurationUnit
    {
        IEnumerable<IChangeRequest> ChangeRequests { get; }

        IChangeRequest CreateChangeRequest(IUser user, string name, string status);

        bool IsDefault { get; set; }
    }
}