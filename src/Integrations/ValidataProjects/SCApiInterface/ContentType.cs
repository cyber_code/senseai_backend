﻿using System;

namespace SCApiInterface
{
    [Serializable, Flags]
    public enum ContentType
    {
        Unknown = 0x00,

        Binary = 0x01,

        Text = 0x02,

        XML = 0x04,

        DLPackage = 0x08,

        ValueMask = 0x0F,

        Chunk = 0x80,

        ModifiersMask = 0xF0,

        NonBinaryValueMask = 0x0E

    }
}