﻿using System;

namespace SCApiInterface
{
    public interface INotification : IUnit
    {
        string Priority { get; }

        DateTime ReviewDate { get; }

        DateTime EventDate { get; }

        UnitAttribute NotificationStatus { get; set; }

        string EventID { get; }

        string Message { get; }
    }
}
