﻿namespace SCApiInterface
{
    public interface IFolder : ISourceUnit
    {
        IFile CreateFile(IUser user, string name);

        IFolder CreateFolder(IUser user, string name);

        void AssignSourceUnit(ISourceUnit su);
    }
}