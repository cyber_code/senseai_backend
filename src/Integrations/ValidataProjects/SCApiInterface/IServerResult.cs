﻿using System;
using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IServerResult
    {
        Int64 ClientId { get; set; }

        Object Data { get; }

        OperationType OperationType { get; }

        IEnumerable<IUnit> Units { get; }

        IUnit Parent { get; }

        ServerResultType ServerResultType { get; }

        string Message { get; }

        Guid Transaction { get; }
    }
}