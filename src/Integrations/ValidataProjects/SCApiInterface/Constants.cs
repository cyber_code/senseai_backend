﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public class Constants
    {
        public const string DETAILS_MARKER = "[DETAILS]";

        public const int GetLatestChunkSize = 50;

        public const int CheckInMaxSize = 20;

        public static List<string> StandardAttributes = new List<string>() { "Status", "Version", "Name", "Action", "InstanceID", "ID", "Thread", "TargetPath", "PackageType", "FileData", "OrderPlace", "Comments" };
    }

    public enum ChangeManagementUnits
    {
        WorkPackage,
        ChangeRequest,
        Task
    }
}