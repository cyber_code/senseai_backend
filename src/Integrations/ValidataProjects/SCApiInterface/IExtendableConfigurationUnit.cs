﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IExtendableConfigurationUnit : IConfigurationUnit
    {
        IUnit Overrides { get; }

        ulong OverridesNoderef { get; }

        IUnit Extends { get; }

        ulong ExtendsNoderef { get; }

        IEnumerable<IUnit> OverridedBy { get; }

        ulong[] OverridedByNoderefs { get; }

        IEnumerable<IUnit> ExtendedBy { get; }

        ulong[] ExtendedByNoderefs { get; }

        UnitAttribute Priority { get; set; }
    }
}