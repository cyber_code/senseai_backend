﻿namespace SCApiInterface
{
    public interface ISourceUnit : IBranchableUnit
    {
        string RelationalPath { get; set; }

        string RelationalNameFull { get; }


        bool IsReleaseValid { get; }
    }
}