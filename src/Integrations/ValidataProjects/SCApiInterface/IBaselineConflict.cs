﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IBaselineConflict : IUnit
    {
        IEnumerable<ISourceUnit> ConflictingUnits { get; }

        BaselineConflictType ConflictType { get; }
    }
}