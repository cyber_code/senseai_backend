﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IReleaseStream : IHierUnit
    {
        string Description { get; set; }

        IRelease FirstRelease { get; }

        IRelease LastRelease { get; }

        IEnumerable<IRelease> Releases { get; }

        UnitAttribute StartDate { get; }

        UnitAttribute EndDate { get; }

        bool IsDefault { get; set; }
    }
}