﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IChangeRequest : IConfigurationUnit
    {
        IEnumerable<ITask> Tasks { get; }

        ITask CreateTask(IUser user, string name, string status);

        bool UseClientsUniqueID { get; set; }

        UnitAttribute UniqueID { get; set; }

        UnitAttribute Comments { get; set; }

        bool DeployedOnDEV { get; set; }

        bool DeployedOnSIT { get; set; }

        bool DeployedOnUAT { get; set; }

        bool DeployedOnProdCopy { get; set; }

        bool DeployedOnProduction { get; set; }

        bool TestedOnSIT { get; set; }

        bool TestedOnUAT { get; set; }

        bool TestedOnProdCopy { get; set; }

        bool TestedOnProduction { get; set; }

        //bool ReleaseStream { get; set; }

        //bool ReleaseIteration { get; set; }

        //bool WorkPackage { get; set; }

        //bool CQRelease { get; set; }

        //bool ABCCRCQdefectID { get; set; }

        //bool ABCNAME { get; set; }

        string CQHeadline { get; set; }

        string CQSystem { get; set; }

        string CQModule { get; set; }

        string CQImpact { get; set; }

        string CQSubmitter { get; set; }

        string CQOwner { get; set; }

        string ABCSCROwner { get; set; }

        string CQStatus { get; set; }

        string ABCStatus { get; set; }

        string CQDevDueDate { get; set; }
        //UnitAttribute CQDevDueDate { get; set; }

        string CQResolution { get; set; }

        bool CQRequiresDeployment { get; set; }

        //bool OnDev { get; set; }

        //bool OnSIT { get; set; }

        //bool TstOnSit { get; set; }

        //bool OnUAT { get; set; }

        //bool TstOnUAT { get; set; }

        //bool Approved { get; set; }

        //bool OnProdCpy { get; set; }

        //bool TstOnProdCpy { get; set; }

        //bool OnProd { get; set; }

        //bool TstOnProd { get; set; }

        string BaselineName { get; set; }

        string ProjectName { get; set; }

        string ProjectType { get; set; }

        bool Approved { get; set; }

        string[] ObservationNames { get; }

        ulong[] AttachmentsNoderefs { get; }

        bool IsDefault { get; set; }
    }
}