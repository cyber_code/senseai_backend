﻿using System.Collections.Generic;

namespace SCApiInterface
{
    public interface IBranchableUnit : IHierUnit
    {
        string Version { get; set; }

        UnitState State { get; set; }

        UnitAction Action { get; set; }

        string Comments { get; set; }

        IBranchableUnit Predecessor { get; }

        IBranchableUnit SecondPredecessor { get; set; }

        IEnumerable<IBranchableUnit> Successors { get; }

        bool HistoryLoaded { get; }

        ulong PredecessorNoderef { get; }

        ulong SecondPredecessorNoderef { get; }

        ulong[] SuccessorsNoderefs { get; }

        ulong Thread { get; }
    }
}