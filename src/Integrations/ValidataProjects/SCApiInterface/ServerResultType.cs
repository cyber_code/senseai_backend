﻿namespace SCApiInterface
{
    public enum ServerResultType
    {
        Error,
        Feedback,
        AccessDenied,
        Success,
        InvalidClientUnitState,
        CommunicationError,
    }
}