﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCApiInterface
{
    public interface IBusinessModel : IUnit
    {
        List<string> CatalogNames
        {
            get;
        }
    }
}
