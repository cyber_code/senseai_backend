﻿using System.Collections.Generic;

namespace VSSH
{
    public abstract class VSshFtpBase
    {
        #region Enums

        public enum Protocol
        {
            SFTP,
            SCP,
        }

        #endregion

        #region Constants

        protected const int DefaultPort = 22;

        #endregion

        #region Private Members

        protected string _Host;

        protected int _Port;

        protected string _UserName;

        protected string _Password;

        protected string _IdentityFile;

        protected string _Passphrase;

        protected readonly Protocol _Protocol = Protocol.SFTP;


        #endregion

        #region Class Lifecycle

        public VSshFtpBase(string host, string userName, string password, Protocol protocol)
            : this(host, DefaultPort, userName, password, protocol) { }

        public VSshFtpBase(string host, int port, string userName, string password, Protocol protocol)
        {
            _Host = host;
            _Port = port;
            _UserName = userName;
            _Password = password;
            _Protocol = protocol;
        }

        #endregion

        #region Public Methods

        public void AddIdentityFile(string fileName)
        {
            AddIdentityFile(fileName, null);
        }

        public void AddIdentityFile(string fileName, string passphrase)
        {
            _IdentityFile = fileName;
            _Passphrase = passphrase;
        }

        public abstract bool Connect(out string errMsg);

        public abstract void Disconnect();

        public abstract bool Upload(string localFilePath, string serverFilePath, out string errMsg);

        public abstract bool Download(string serverFilePath, string localFilePath, out string errMsg);

        public abstract bool CreateDirectory(string directory, out string errMsg);

        public abstract bool Delete(string filePath, out string errMsg);

        public abstract bool ChangeAccessRights(string destPath, string rights, out string errMsg);

        public abstract Dictionary<string, string> List(string serverPath, out string errMsg);

        #endregion

    }
}