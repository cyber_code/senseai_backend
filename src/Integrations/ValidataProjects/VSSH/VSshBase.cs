﻿using System;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace VSSH
{
    public abstract class VSshBase
    {
        protected string _Host;
        protected string _User;
        protected string _Password;

        /// <summary>
        /// Default TCP port of SSH protocol
        /// </summary>
        private const int SSH_TCP_PORT = 22;

        /// <summary>
        /// Constructs a new SSH instance
        /// </summary>
        /// <param name="sftpHost">The remote SSH host</param>
        /// <param name="user">The login username</param>
        /// <param name="password">The login password</param>
        public VSshBase(string sftpHost, string user, string password)
        {
            _Host = sftpHost;
            _User = user;
            _Password = password;
        }

        /// <summary>
        /// Constructs a new SSH instance
        /// </summary>
        /// <param name="sftpHost">The remote SSH host</param>
        /// <param name="user">The login username</param>
        protected VSshBase(string sftpHost, string user)
            : this(sftpHost, user, null) {}

        public abstract bool ShellOpened { get; }


        public abstract void Write(byte[] buffer, bool isLogoutStep);
        public abstract byte[] Receive();

        /// <summary>
        /// Adds identity file for publickey user authentication
        /// </summary>
        /// <param name="privateKeyFile">The path to the private key file</param>
        public abstract void AddIdentityFile(string privateKeyFile);
       
        /// <summary>
        /// Adds identity file for publickey user authentication
        /// </summary>
        /// <param name="privateKeyFile">The path to the private key file</param>
        /// <param name="passphrase">A passphrase for decrypting the private key file</param>
        public abstract void AddIdentityFile(string privateKeyFile, string passphrase);

        protected abstract string ChannelType { get; }
        public abstract void Connect(int tcpPort, int timeout);

        protected virtual void OnConnected() {}

        protected virtual void OnChannelReceived() {}

        /// <summary>
        /// Closes the SSH subsystem
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Return true if the SSH subsystem is connected
        /// </summary>
        public abstract bool Connected { get; }

        /// <summary>
        /// Gets the Cipher algorithm name used in this SSH connection.
        /// </summary>
        public abstract string Cipher { get; }

        /// <summary>
        /// Gets the MAC algorithm name used in this SSH connection.
        /// </summary>
        public abstract string Mac { get; }

        /// <summary>
        /// Gets the server SSH version string.
        /// </summary>

        public abstract string ServerVersion { get; }

        /// <summary>
        /// Gets the client SSH version string.
        /// </summary>
        public abstract string ClientVersion { get; }

        public abstract string Host { get; }

        public abstract int Port { get; }
       
        /// <summary>
        /// The password string of the SSH subsystem
        /// </summary>
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        public string Username
        {
            get { return _User; }
        }

        public static Version Version
        {
            get
            {
                Assembly asm = Assembly.GetAssembly(typeof (VSshBase));
                return asm.GetName().Version;
            }
        }

        protected void CheckConnected()
        {
            if (!Connected)
            {
                throw new Exception("SSH session is not connected.");
            }
        }

        #region Escape sequences

        private const string EscapeCharsPattern = "\\[[0-9;?]*[^0-9;]";
        // The below 4 are required for the extra processing of terminal input

        private bool _SkippedLast = false;
        private bool _ShouldSplit = false;
        private bool _ShouldJoin = false;
        private int _LastLineNum = 0;
        // This one might be suitable to make conifgurable in telnetcommdef.
        private int _TerminalWidth = 80;
        private string _PrevTxmRemainder = "";



        /// <summary>
        /// Shift  In character
        /// </summary>
        public const char SI = (char)15;

        /// <summary>
        /// Escape character
        /// </summary>
        public const char ESC = (char)27;

        public static readonly string JShellSequence = ESC + "[K\b" + ESC + "[C";

        public static readonly string ShellSequence = ESC + "[K" + ESC + "[J";


        /// <summary>
        /// Removes escape sequence characters from the input string.
        /// </summary>
        public string RemoveSSHEscSequences(string str, bool joinTerminalLines)
        {
            #region for debug
            // System.Diagnostics.Debug.WriteLine("before processing:  |" + str + "|");

            // Experiment with libvt100
            // str = SSHScreenDecoder.SymbolsToStrings(str, System.Text.Encoding.UTF8);
            //System.Diagnostics.Debug.WriteLine("------------------------------");
            //System.Diagnostics.Debug.WriteLine("start section:");
            //System.Diagnostics.Debug.WriteLine(str);
            //System.Diagnostics.Debug.WriteLine("------------------------------");
            #endregion

            if (str.Length == 0)
            {
                // just to avoid the costly regex operations
                return str;
            }

            str = str.Replace("(B)0", "");
            str = str.Replace(JShellSequence, "");
            str = str.Replace(ShellSequence, ""); // might be handled by EscapeCharsPattern

            #region Uncomment for Unit Testing
            //string method = System.Configuration.ConfigurationManager.AppSettings["SSHmethod"];
            //if (method == "false")
            //{
            //    joinTerminalLines = false;
            //}
            //if (method == "orig")
            //    str = origprocessEscSequences(str);
            //if (method == "new" || string.IsNullOrEmpty(method))
            //{
            //    // nothign str will be processed depending on joinTerminalLines
            //}
            #endregion

            if (joinTerminalLines)
                str = processEscSequences(str);

            Regex.Replace(str, EscapeCharsPattern, "");

            // System.Diagnostics.Debug.WriteLine("after processing:  |" + str + "|");
            //System.Diagnostics.Debug.WriteLine("------------------------------");

            str = str.Replace(SI.ToString(), ""); // 15 = SI (shift in)
            str = Regex.Replace(str, ESC + "=*", ""); // 27 = ESC (escape)
            //str = Regex.Replace(str, "\\s*\r\n", "\r\n");
            // TODO: MY
            str = str.Replace("]0;~", "");


            return str;
        }
		
        protected string processEscSequences(string str)
        {
            str = _PrevTxmRemainder + str;
            _PrevTxmRemainder = "";
			
            var resultStr = new StringBuilder("");
            int mcc = 0;

            int lineStartPos = 0;
            int lineEndPos = 0;
            int nextStartPos = 0;

            var escSeqInStr = Regex.Matches(str, EscapeCharsPattern);
            if (escSeqInStr != null && escSeqInStr.Count > 0)
            {
                bool endedWithNl = false;

                while (lineEndPos < str.Length || nextStartPos != lineEndPos)
                {
                    int? lineNum = null;
                    int? colNum;

                    lineStartPos = nextStartPos;

                    // The esc section
                    if (mcc < escSeqInStr.Count)
                    {
                        var match = escSeqInStr[mcc++];

                        lineEndPos = match.Index;
                        nextStartPos = parseNextEscSequence(match, out lineNum, out colNum);
                    }
                    else
                    {
                        nextStartPos = lineEndPos = str.Length;
                    }

                    // The string before the esc section
                    string lastLine = str.Substring(lineStartPos, lineEndPos - lineStartPos);

                    int lineLen = lastLine.Length;

                    // normalize
                    lastLine = lastLine.Replace("\r\n", "\n");

                    joinSplitLogic(lineNum); // this sets _ShouldJoin and _ShouldSplit

                    if (!string.IsNullOrWhiteSpace(lastLine) || lineLen != _TerminalWidth)
                    {

                        if (!attemptTrim(lastLine))
                        {
                            if (endedWithNl && _ShouldJoin)
                                resultStr.Length -= 1;
                            //resultStr.Remove(resultStr.Length - 1, 1);

                            // attemptTrim(resultStr);

                            if (_ShouldSplit)
                            {
                                lastLine += "\n";
                                // System.Diagnostics.Debug.WriteLine("** Added /n at end");
                            }
                            endedWithNl = false;
                        }
                        else
                            endedWithNl = true;


                        resultStr.Append(lastLine);

                        _SkippedLast = false;

                        // TODO: ..or outside the skip-if ?
                        _LastLineNum = lineNum ?? _LastLineNum;

                    }
                    else
                    {
                        _SkippedLast = true;
                        // System.Diagnostics.Debug.WriteLine("Skipped: |" + lastLine + "|");
                    }
                }

                // normalize back
                resultStr = resultStr.Replace("\n", "\r\n");

                str = resultStr.ToString();
            }

            // handling if the transmisson has been cut of mid - escape sequence.,
            // e.g. 

            return handleCutEscapeSequence(str);
        }


        [Obsolete] // left for unit testing comparison
        protected string origprocessEscSequences(string str)
        {
            str = _PrevTxmRemainder + str;
            _PrevTxmRemainder = "";

            string resultStr = "";
            int mcc = 0;

            int lineStartPos = 0;
            int lineEndPos = 0;
            int nextStartPos = 0;

            var escSeqInStr = Regex.Matches(str, EscapeCharsPattern);
            if (escSeqInStr != null && escSeqInStr.Count > 0)
            {
                while (lineEndPos < str.Length || nextStartPos != lineEndPos)
                {
                    int? lineNum = null;
                    int? colNum;

                    lineStartPos = nextStartPos;

                    // The esc section
                    if (mcc < escSeqInStr.Count)
                    {
                        var match = escSeqInStr[mcc++];

                        lineEndPos = match.Index;
                        nextStartPos = parseNextEscSequence(match, out lineNum, out colNum);
                    }
                    else
                    {
                        nextStartPos = lineEndPos = str.Length;
                    }

                    // The string before the esc section
                    string lastLine = str.Substring(lineStartPos, lineEndPos - lineStartPos);

                    int lineLen = lastLine.Length;

                    // normalize
                    lastLine = lastLine.Replace("\r\n", "\n");

                    joinSplitLogic(lineNum);

                    if (!string.IsNullOrWhiteSpace(lastLine) || lineLen != _TerminalWidth)
                    {
                        if (!attemptTrim(lastLine))
                        {
                            attemptTrim(resultStr);

                            if (_ShouldSplit)
                            {
                                lastLine += "\n";
                                // System.Diagnostics.Debug.WriteLine("** Added /n at end");
                            }
                        }
                        resultStr += lastLine;
                        _SkippedLast = false;

                        // TODO: ..or outside the skip-if ?
                        _LastLineNum = lineNum ?? _LastLineNum;

                    }
                    else
                    {
                        _SkippedLast = true;
                        // System.Diagnostics.Debug.WriteLine("Skipped: |" + lastLine + "|");
                    }
                }

                // normalize back
                resultStr = resultStr.Replace("\n", "\r\n");

                str = resultStr;
            }

            // handling if the transmisson has been cut of mid - escape sequence.,
            // e.g. 

            return handleCutEscapeSequence(str);
        }

        protected string handleCutEscapeSequence(string str)
        {
            // Example 1:
            // <MSG>BATCH,/S/PROCESS,AUTHOR/******,SG1^EU.BASE.VERIF[24;1HY</MSG>'

            // Example 2:
            // ... 13DM/T24MIG.BP TO T24MIG.BP $DM.MNEMONIC.APPLNS OVERWRITING,PROCESS.INFO:3:384= [24;'
            // ... 1H
            //1 $DM.MNEMONIC.APPLNS,PROCESS.INFO:3:385 = 1 records copied, PROCESS.INFO: 3:386

            // Example 3:
            // ...DATA/RELEASE/TMNS000-R13DM/T24MIG.BP TO T24MIG.BP I_F.DM.SERVICE.SCHEDULER OVERW
            //[24;'
            // ... 2017-02-23 13:15:29,822 [Worker R14-COB1 - 3] DEBUG - 1550 characters read: '1H                                                                                RITING,PROCESS.INFO:3:582=1 records copied,PROCESS.INFO:3:583=156 PROGRAMS (Incl

            //uding INSERTs), marked for Release, PROCESS.INFO:3:584 = JBCDEV_LIB currently set t

            var escIndex = str.IndexOf(ESC);
            if (escIndex < 1) return str;

            _PrevTxmRemainder = str.Substring(escIndex);

            return str.Substring(0, escIndex);
        }

        protected bool attemptTrim(string lastLine)
        {
            if (lastLine.EndsWith("\n"))
            {
                if (_ShouldJoin)
                {
                    lastLine = lastLine.Remove(lastLine.Length - 1, 1);
                    // System.Diagnostics.Debug.WriteLine("** Removed /n at end");
                }
                return true;
            }
            return false;
        }

        protected int parseNextEscSequence(Match match, out int? lineNum, out int? colNum)
        {
            lineNum = null;
            colNum = null;

            //System.Diagnostics.Debug.Assert(
            //    match.Value.StartsWith(ESC.ToString()) && match.Value.EndsWith("H"),
            //    string.Format("Escape sequence {0} incorrectly recognized by regex", match.Value));


            try
            {
                string nextEscSection = match.Value;
                var escSplits = nextEscSection.Split(new char[] { '[', ';', 'H' });

                lineNum = int.Parse(escSplits[1]);
                colNum = int.Parse(escSplits[2]);

                // System.Diagnostics.Debug.WriteLine("Match = " + m.Value + ", Line =  " + lineNum + ", Col = " + colNum + ". LastLine = " + _LastLineNum);
            }
            catch (Exception eee)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("Exception parsing escape section \"{0}\" : {1}", match.Value, eee.Message));
            }

            // whatever happened the start of the next line will be returned
            return match.Index + match.Length;
        }

        protected void joinSplitLogic(int? thisLineNum)
        {
            //System.Diagnostics.Debug.WriteLine("\nlastline = " + lastLine);
            // System.Diagnostics.Debug.WriteLine("Should split=" + _ShouldSplit.ToString() + ", join = " + _ShouldJoin.ToString());
            // System.Diagnostics.Debug.WriteLine("Last/this num " + _LastLineNum.ToString() + "/" + thisLineNum.ToString());

            if (thisLineNum != null)
            {
                if (!_SkippedLast)
                {
                    _ShouldJoin = false;
                    _ShouldSplit = false;

                    // System.Diagnostics.Debug.WriteLine("Reseting false/false due to non-skipped last");
                }

                if (_LastLineNum == thisLineNum)
                {

                }
                else if (_LastLineNum > thisLineNum)
                {
                    _ShouldJoin = true;
                }
                else if (_LastLineNum < thisLineNum)
                {
                    _ShouldSplit = true;
                }
            }
            else
            {
                _ShouldJoin = false;
                _ShouldSplit = false;
                // System.Diagnostics.Debug.WriteLine("Reset due to null linenum");
            }

            // System.Diagnostics.Debug.WriteLine("Now  split=" + _ShouldSplit.ToString() + ", join = " + _ShouldJoin.ToString());
        }
        #endregion

    }
}