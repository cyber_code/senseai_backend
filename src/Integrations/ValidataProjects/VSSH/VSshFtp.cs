﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Tamir.SharpSsh;
using Tamir.SharpSsh.jsch;

namespace VSSH
{
    public class VSshFtp : VSshFtpBase
    {
     
        private SshTransferProtocolBase _Sftp;
        

        #region Class Lifecycle

        public VSshFtp(string host, string userName, string password, Protocol protocol)
            : this(host, DefaultPort, userName, password, protocol) { }

        public VSshFtp(string host, int port, string userName, string password, Protocol protocol)
                : base(host, port, userName, password, protocol) { }

        #endregion

        #region Public Methods
        
        public override bool Connect(out string errMsg)
        {
            errMsg = null;

            try
            {
                switch (_Protocol)
                {
                    case Protocol.SCP:
                        _Sftp = new Scp(_Host, _UserName);
                        break;

                    case Protocol.SFTP:
                        _Sftp = new Sftp(_Host, _UserName);
                        break;

                    default:
                        throw new Exception("Unknown protocol type!");
                }

                if (!string.IsNullOrEmpty(_Password))
                {
                    _Sftp.Password = _Password;
                }

                if (!string.IsNullOrEmpty(_IdentityFile))
                {
                    if (!string.IsNullOrEmpty(_Passphrase))
                    {
                        _Sftp.AddIdentityFile(_IdentityFile, _Passphrase);
                    }
                    else
                    {
                        _Sftp.AddIdentityFile(_IdentityFile);
                    }
                }

                _Sftp.OnTransferStart += OnTransferStart;
                _Sftp.OnTransferProgress += OnTransferProgress;
                _Sftp.OnTransferEnd += OnTransferEnd;

                _Sftp.Connect();

                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        public override void Disconnect()
        {
            try
            {
                if (_Sftp == null)
                {
                    Debug.Fail("Not connected!");
                    return;
                }

                _Sftp.Close();
                _Sftp.OnTransferStart -= OnTransferStart;
                _Sftp.OnTransferProgress -= OnTransferProgress;
                _Sftp.OnTransferEnd -= OnTransferEnd;
            }
            catch {}
            finally
            {
                _Sftp = null;
            }
        }

        public override bool Upload(string localFilePath, string serverFilePath, out string errMsg)
        {
            errMsg = null;

            try
            {
                _Sftp.Put(localFilePath, serverFilePath);
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.ToString();
                return false;
            }
        }

        public override bool Download(string serverFilePath, string localFilePath, out string errMsg)
        {
            errMsg = null;

            try
            {
                _Sftp.Get(serverFilePath, localFilePath);
                return true;
            }
            catch(SftpException ex)
            {
                errMsg = ex.toString();
                return false;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        public override bool CreateDirectory(string directory, out string errMsg)
        {
            errMsg = null;

            try
            {
                _Sftp.Mkdir(directory);
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        public override bool Delete(string filePath, out string errMsg)
        {
            errMsg = null;

            try
            {
                _Sftp.Delete(filePath);
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        public override bool ChangeAccessRights(string destPath, string rights, out string errMsg)
        {
            errMsg = null;

            try
            {
                _Sftp.ChangeAccessRights(destPath, rights);
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        public override Dictionary<string, string> List(string serverPath, out string errMsg)
        {
            errMsg = null;

            try
            {
                Dictionary<string, string> result = new Dictionary<string, string>();

                if (_Sftp is Sftp)
                {
                    Hashtable ht = (_Sftp as Sftp).ListDirectory(serverPath);
                    foreach (string fileName in ht.Keys)
                    {
                        result.Add(fileName, (string) ht[fileName]);
                    }
                }
                else
                {
                    throw new NotImplementedException();
                }

                return result;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return null;
            }
        }

        #endregion

        #region Private Methods

        private void OnTransferStart(string src, string dst, int transferredBytes, int totalBytes, string message)
        {
            // TODO - Progress
        }

        private void OnTransferProgress(string src, string dst, int transferredBytes, int totalBytes, string message)
        {
            // TODO - Progress
        }

        private void OnTransferEnd(string src, string dst, int transferredBytes, int totalBytes, string message)
        {
            // TODO - Progress
        }

        #endregion
    }
}