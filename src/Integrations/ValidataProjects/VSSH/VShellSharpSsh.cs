﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Tamir.SharpSsh.jsch;
using Tamir.Streams;

namespace VSSH
{
    public class VShellSharpSsh : VSshBase
    {
        private Stream _SshIO;

        protected JSch _JSch;
        protected Session _Session;
        protected Channel _Channel;


        public VShellSharpSsh(string host, string user, string password)
            : base(host, user, password) {

            _JSch = new JSch();
        }

        public VShellSharpSsh(string host, string user)
            : base(host, user) {

            _JSch = new JSch();
        }

        protected override void OnChannelReceived()
        {
            base.OnChannelReceived();
            _SshIO = GetStream();
        }

        protected override string ChannelType
        {
            get { return "shell"; }
        }

        public Stream IO
        {
            get { return _SshIO; }
        }

        /// <summary>
        /// Writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
        /// </summary>
        /// <param name="buffer">An array of bytes. This method copies count bytes from buffer to the current stream. </param>
        public override void Write(byte[] buffer, bool isLogoutStep)
        {
            Write(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// Writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
        /// </summary>
        /// <param name="buffer">An array of bytes. This method copies count bytes from buffer to the current stream. </param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin copying bytes to the current stream.</param>
        /// <param name="count">The number of bytes to be written to the current stream. </param>
        private void Write(byte[] buffer, int offset, int count)
        {
            IO.Write(buffer, offset, count);
            IO.Flush();
        }   

        /// <summary>
        /// Creates a new I/O stream of communication with this SSH shell connection
        /// </summary>
        public Stream GetStream()
        {
            return new CombinedStream(_Channel.getInputStream(), _Channel.getOutputStream());
        }

        public override bool ShellOpened
        {
            get
            {
                if (_Channel != null)
                {
                    return !_Channel.isClosed();
                }
                return false;
            }
        }

        public virtual bool ShellConnected
        {
            get
            {
                if (_Channel != null)
                {
                    return _Channel.isConnected();
                }
                return false;
            }
        }

        public override byte[] Receive()
        {
            const int tempBuffSize = 1024;

            List<byte> result = new List<byte>();

            while (((CombinedStream) IO).HasBytes)
            {
                byte[] tempBuff = new byte[tempBuffSize];
                int readCount = IO.Read(tempBuff, 0, tempBuff.Length);
                if (readCount == -1)
                {
                    break;
                }

                // TODO - Use other logic!!!
                for (int i = 0; i < readCount; i++)
                {
                    result.Add(tempBuff[i]);
                }
            }

            return result.ToArray();
        }

        #region from base class


        /// <summary>
        /// Connect to remote SSH server
        /// </summary>
        /// <param name="tcpPort">The destination TCP port for this connection</param>
        /// <param name="timeout">The timeout</param>
        public override void Connect(int tcpPort, int timeout)
        {
            ConnectSession(tcpPort, timeout);
            ConnectChannel();
        }

        protected virtual void ConnectSession(int tcpPort, int timeout)
        {
            _Session = _JSch.getSession(_User, _Host, tcpPort);
            if (Password != null)
            {
                _Session.setUserInfo(new KeyboardInteractiveUserInfo(Password));
            }
            Hashtable config = new Hashtable();
            config.Add("StrictHostKeyChecking", "no");
            _Session.setConfig(config);
            _Session.connect(timeout);
        }

        public override bool Connected
        {
            get
            {
                if (_Session != null)
                {
                    return _Session.isConnected();
                }
                return false;
            }
        }
        protected virtual void ConnectChannel()
        {
            _Channel = _Session.openChannel(ChannelType);
            OnChannelReceived();
            _Channel.connect();
            OnConnected();
        }
        public override void Close()
        {
            if (_Channel != null)
            {
                _Channel.disconnect();
                _Channel = null;
            }

            if (_Session != null)
            {
                _Session.disconnect();
                _Session = null;
            }
        }

        public override void AddIdentityFile(string privateKeyFile)
        {
            _JSch.addIdentity(privateKeyFile);
        }

        public override void AddIdentityFile(string privateKeyFile, string passphrase)
        {
            _JSch.addIdentity(privateKeyFile, passphrase);
        }


        public override string Host
        {
            get
            {
                CheckConnected();
                return _Session.getHost();
            }
        }

        public override string ClientVersion
        {
            get
            {
                CheckConnected();
                return _Session.getClientVersion();
            }
        }
        public override string Mac
        {
            get
            {
                CheckConnected();
                return _Session.getMac();
            }
        }
        public override string Cipher
        {
            get
            {
                CheckConnected();
                return _Session.getCipher();
            }
        }

        public HostKey HostKey
        {
            get
            {
                CheckConnected();
                return _Session.getHostKey();
            }
        }

        public override int Port
        {
            get
            {
                CheckConnected();
                return _Session.getPort();
            }
        }
        public override string ServerVersion
        {
            get
            {
                CheckConnected();
                return _Session.getServerVersion();
            }
        }


        #endregion

    }

    /// <summary>
    /// For password and KI auth modes
    /// </summary>
    class KeyboardInteractiveUserInfo : UserInfo, UIKeyboardInteractive
    {
        private readonly string _Password;

        public KeyboardInteractiveUserInfo(string password)
        {
            _Password = password;
        }

        #region UIKeyboardInteractive Members

        public string[] promptKeyboardInteractive(string destination, string name, string instruction, string[] prompt, bool[] echo)
        {
            return new string[] { _Password };
        }

        #endregion

        #region UserInfo Members

        public bool promptYesNo(string message)
        {
            return true;
        }

        public bool promptPassword(string message)
        {
            return true;
        }

        public string getPassword()
        {
            return _Password;
        }

        public bool promptPassphrase(string message)
        {
            return true;
        }

        public string getPassphrase()
        {
            return null;
        }

        public void showMessage(string message) { }

        #endregion
    }
}