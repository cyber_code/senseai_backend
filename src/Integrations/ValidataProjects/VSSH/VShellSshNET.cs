﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Renci.SshNet;
using System.Threading;
using Renci.SshNet.Common;

namespace VSSH
{
    /// <summary>
    /// This class is a main class for new SSH development using Renci.SshNet open source library
    /// </summary>
    public class VShellSshNET : VSshBase
    {
        public string TerminalName
        {
            get
            {
                return _TerminalName;
            }
        }

        private readonly string _TerminalName = DEFAULT_TERMINAL_NAME;

        /// <summary>
        /// Default TCP port of SSH protocol
        /// </summary>
        protected const int SSH_TCP_PORT = 22;
        private const string DEFAULT_TERMINAL_NAME = "vt100"; // this apparently is SUPER important to be dumb for Bitwise

        private Encoding ShellEncoding = Encoding.UTF8;

        protected Renci.SshNet.ConnectionInfo _connectionInfo;
        protected Renci.SshNet.SshClient RenciSSHClient; // unlike when using Timbla ssh in Renci.Ssh session is not public object so we will use SshClient object for our use which contain it's own session inside.


        internal ShellStream ShellStream;

        /// <summary>
        /// Constructs a new SSH instance
        /// </summary>
        /// <param name="host">The remote SSH host</param>
        /// <param name="user">The login username</param>
        /// <param name="password">The login password</param>

        public VShellSshNET(string host, string user, string password)
            : base(host,user,password)
        {
            _connectionInfo = new Renci.SshNet.ConnectionInfo(_Host, _User, new Renci.SshNet.PasswordAuthenticationMethod(_User, _Password));            
        }

        public VShellSshNET(string host, int port, string user, string password)
           : base(host, user, password)
        {
            _connectionInfo = new Renci.SshNet.ConnectionInfo(_Host, port, _User, new Renci.SshNet.PasswordAuthenticationMethod(_User, _Password));
        }

        public VShellSshNET(string host, int port, string terminal, string user, string password)
           : this(host,port, user, password)
        {
            _TerminalName = terminal;
        }

        /// <summary>
        /// Connect to remote SSH server
        /// </summary>
        /// <param name="tcpPort">The destination TCP port for this connection</param>
        /// <param name="timeout">The timeout</param>
        public  void Connect( int timeout)
        {

            RenciSSHClient = new Renci.SshNet.SshClient(_connectionInfo);

            // timeout is in miliseconds in case if it is out of range it will be set to default value of 2 minutes
            try
            {
                RenciSSHClient.KeepAliveInterval = new TimeSpan(0, 0, timeout / 1000);
            }
            catch (Exception ex)
            {
                RenciSSHClient.KeepAliveInterval = new TimeSpan(0, 0, 120);
            }

            var terminalModes = new Dictionary<TerminalModes, uint>();
            terminalModes.Add(TerminalModes.ECHO, 1);
            terminalModes.Add(TerminalModes.VSWTCH, 1);

            RenciSSHClient.Connect();
            ShellStream = RenciSSHClient.CreateShellStream(TerminalName, 80, 24, 800, 600, 1024,terminalModes);
        }
        public override void Connect(int tcpPort, int timeout)
        {
            throw new NotImplementedException();
        }

        public void Write(string cmd, bool isLogoutStep = false)
        {
            ShellStream.WriteLine(cmd);

			 if (isLogoutStep) return;
			 
            while (ShellStream.Length == 0)
                Thread.Sleep(500);

            return;

        }

        /// <summary>
        /// Writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
        /// </summary>
        /// <param name="buffer">An array of bytes. This method copies count bytes from buffer to the current stream. </param>
        public override void Write(byte[] buffer, bool isLogoutStep)
        {
            string str = Encoding.Default.GetString(buffer);
            Write(str,isLogoutStep);
            return;
        }

        /// <summary>
        /// Creates a new I/O stream of communication with this SSH shell connection
        /// </summary>
        public Stream GetStream()
        {
            return ShellStream;
        }

        public override bool ShellOpened
        {
            get
            {
                return Connected;
            }
        }

        public override byte[] Receive()
        {
            StringBuilder sbs = new StringBuilder();
            string currentline = string.Empty;
            while (ShellStream.DataAvailable)
            {
                currentline = ShellStream.Read();
                if (!string.IsNullOrWhiteSpace(currentline))
                    sbs.AppendLine(currentline);
            }
            return System.Text.Encoding.Default.GetBytes(sbs.ToString());

        }


        #region from base class

        protected override string ChannelType
        {
            get
            {
                throw new NotImplementedException();
            }

            //on tamir it is:
            //get { return "shell"; }

            //here would be something like
            // ShellRequestInfo.Name = "shell" that is added in
            //_connectionInfo.ChannelRequests
        }

        /// <summary>
        /// Closes the SSH subsystem
        /// </summary>
        public override void Close()
        {
            // in Renci Ssh channel maintence is on lower lever so we don't need to manage it here

            if (RenciSSHClient != null)
            {
                RenciSSHClient.Disconnect();
                RenciSSHClient = null;
            }
        }

        /// <summary>
        /// Return true if the SSH subsystem is connected
        /// </summary>
        public override bool Connected
        {
            get
            {
                if (RenciSSHClient != null)
                {
                    //return _Session.isConnected();
                    return RenciSSHClient.IsConnected;
                }
                return false;
            }
        }

        /// <summary>
        /// Adds identity file for publickey user authentication
        /// </summary>
        /// <param name="privateKeyFile">The path to the private key file</param>
        public override void AddIdentityFile(string privateKeyFile)
        {
            //_JSch.addIdentity(privateKeyFile);

            _connectionInfo.AuthenticationMethods.Add(new Renci.SshNet.PrivateKeyAuthenticationMethod(_User, new Renci.SshNet.PrivateKeyFile(privateKeyFile)));
        }

        /// <summary>
        /// Adds identity file for publickey user authentication
        /// </summary>
        /// <param name="privateKeyFile">The path to the private key file</param>
        /// <param name="passphrase">A passphrase for decrypting the private key file</param>
        public override void AddIdentityFile(string privateKeyFile, string passphrase)
        {
            //_JSch.addIdentity(privateKeyFile, passphrase);

            _connectionInfo.AuthenticationMethods.Add(new Renci.SshNet.PrivateKeyAuthenticationMethod(_User, new Renci.SshNet.PrivateKeyFile(privateKeyFile, passphrase)));
        }

        protected void OnChannelReceived()
        {
            // _SshIO = ShellStream.
        }

        #region Not used apparently

        public override string ServerVersion
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string Host
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string ClientVersion
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public override string Mac
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public override string Cipher
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        

        public override int Port
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion


        #endregion

    }
}