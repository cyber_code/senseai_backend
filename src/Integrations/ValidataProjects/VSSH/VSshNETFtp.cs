﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.IO;

namespace VSSH
{
    public class VSshNETFtp : VSshFtpBase
    {
        public string WorkingDir
        {
            get { return _Sftp != null ? _Sftp.WorkingDirectory : ""; }
        }
        private SftpClient _Sftp;

        public bool Connected { get { return _Sftp != null && _Sftp.IsConnected; } }

        #region Class Lifecycle

        public VSshNETFtp(string host, string userName, string password, Protocol protocol)
            : this(host, DefaultPort, userName, password, protocol) { }

        public VSshNETFtp(string host, int port, string userName, string password, Protocol protocol)
            : base(host, port, userName, password, protocol) { }

        #endregion

        #region Public Methods

        public override bool Connect(out string errMsg)
        {
            errMsg = null;

            try
            {
                switch (_Protocol)
                {
                    case Protocol.SCP:
                        //_Sftp = new Scp(_Host, _UserName);
                        throw new Exception("SCP Protocol not implemented for Renci SSH.Net library");
                        break;

                    case Protocol.SFTP:
                        _Sftp = new SftpClient(_Host, _Port, _UserName, _Password);
                        break;

                    default:
                        throw new Exception("Unknown protocol type!");
                }

                //if (!string.IsNullOrEmpty(_Password))
                //{
                //    _Sftp.Password = _Password;
                //}

                if (!string.IsNullOrEmpty(_IdentityFile))
                {
                    if (!string.IsNullOrEmpty(_Passphrase))
                    {
                        _Sftp.ConnectionInfo.AuthenticationMethods.Add(new PrivateKeyAuthenticationMethod(_UserName, new PrivateKeyFile(_IdentityFile, _Passphrase)));
                    }
                    else
                    {
                        _Sftp.ConnectionInfo.AuthenticationMethods.Add(new PrivateKeyAuthenticationMethod(_UserName, new PrivateKeyFile(_IdentityFile)));
                    }
                }
                // those events don't exist in REnci SSH.Net library but there is _Sftp.ErrorOccurred maybe we should use it...
                //_Sftp.OnTransferStart += OnTransferStart;
                //_Sftp.OnTransferProgress += OnTransferProgress;
                //_Sftp.OnTransferEnd += OnTransferEnd;

                _Sftp.Connect();

                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        public override void Disconnect()
        {
            try
            {
                if (_Sftp == null)
                {
                    Debug.Fail("Not connected!");
                    return;
                }

                _Sftp.Disconnect();

                //_Sftp.OnTransferStart -= OnTransferStart;
                //_Sftp.OnTransferProgress -= OnTransferProgress;
                //_Sftp.OnTransferEnd -= OnTransferEnd;
            }
            catch { }
            finally
            {
                if (_Sftp != null)
                {
                    _Sftp.Dispose();
                    _Sftp = null;
                }
            }
        }

        public override bool Upload(string localFilePath, string serverFilePath, out string errMsg)
        {
            errMsg = null;

            try
            {
                Stream file1 = File.OpenRead(localFilePath);
                _Sftp.UploadFile(file1, serverFilePath, true);
                file1.Close();
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.ToString();
                return false;
            }
        }

        public override bool Download(string serverFilePath, string localFilePath, out string errMsg)
        {
            errMsg = null;

            try
            {
                Stream file1 = File.OpenWrite(localFilePath);
                _Sftp.DownloadFile(serverFilePath, file1);
                file1.Close();
                return true;
            }
            //catch (SftpException ex)
            //{
            //    errMsg = ex.toString();
            //    return false;
            //}
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        public override bool CreateDirectory(string directory, out string errMsg)
        {
            errMsg = null;

            try
            {
                _Sftp.CreateDirectory(directory);
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        public override bool Delete(string filePath, out string errMsg)
        {
            errMsg = null;

            try
            {
                _Sftp.Delete(filePath);
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        public override bool ChangeAccessRights(string destPath, string rights, out string errMsg)
        {
            errMsg = null;

            try
            {
                _Sftp.ChangePermissions(destPath, short.Parse(rights));
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        public override Dictionary<string, string> List(string serverPath, out string errMsg)
        {
            errMsg = null;

            try
            {
                Dictionary<string, string> result = new Dictionary<string, string>();

                if (_Sftp is SftpClient)
                {

                    IEnumerable<SftpFile> fileList = _Sftp.ListDirectory(serverPath);
                    foreach (SftpFile file in fileList)
                    {
                        // check how this was functional with Tamir !!!
                        if (file.Name == "." || file.Name == "..") continue;
                        result.Add(file.Name, file.ToAttributesString());
                    }
                }
                else
                {
                    throw new NotImplementedException();
                }

                return result;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return null;
            }
        }

        #endregion
        
    }
}