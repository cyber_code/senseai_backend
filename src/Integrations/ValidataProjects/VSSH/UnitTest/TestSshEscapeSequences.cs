﻿using System;
using NUnit.Framework;

namespace VSSH.UnitTest
{
    [TestFixture]
    public class TestSshEscapeSequences
    {
        [Test]
        public void TestJShellSequence()
        {
            VShellSharpSsh vsh = new VShellSharpSsh("","");
            string command = "js" + VShellSharpSsh.JShellSequence + "h";
            Assert.AreEqual("jsh", vsh.RemoveSSHEscSequences(command, false));
        }

        [Test]
        public void TestShellSequence()
        {
            VShellSharpSsh vsh = new VShellSharpSsh("", "");
            string command = "s" + VShellSharpSsh.ShellSequence + "h";
            Assert.AreEqual("sh", vsh.RemoveSSHEscSequences(command, false));
        }

        [Test]
        [Explicit]
        public void TestIdentityFile()
        {

            try
            {
                VShellSharpSsh vssh = new VShellSharpSsh("", "");
                vssh.AddIdentityFile(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"open-ssh");

                VShellSharpSsh vssh1 = new VShellSharpSsh("", "");
                vssh.AddIdentityFile(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"open-ssh1");

                VShellSharpSsh vssh2 = new VShellSharpSsh("", "");
                vssh.AddIdentityFile(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"open-ssh2");

                VShellSharpSsh vssh3 = new VShellSharpSsh("", "");
                vssh.AddIdentityFile(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"open-ssh3");

                VShellSharpSsh vssh4 = new VShellSharpSsh("", "");
                vssh.AddIdentityFile(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"open-ssh34");
            }
            catch
            {
            }

            try
            {
                VShellSharpSsh vssh = new VShellSharpSsh("", "");
                vssh.AddIdentityFile(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"niki1.ppk");
            }
            catch
            {
            }

            try
            {
                VShellSharpSsh vssh = new VShellSharpSsh("", "");
                vssh.AddIdentityFile(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"kleppers.ppk");
            }
            catch
            {
            }

            try
            {
                VShellSharpSsh vssh = new VShellSharpSsh("", "");
                vssh.AddIdentityFile(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"niki");
            }
            catch
            {
            }
        }
    }
}