﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace VSSH.UnitTest
{
    [TestClass]
    public class TestRenciConnection
    {

        VShellSshNET _RenciShell;
        string _TerminalName = "vt100";

        [TestMethod]
        public void TestR14BitwiseAdministrator()
        {
            string host = "192.168.1.67";
            string userName = "Administrator";
            string password = "Manager123";

            establishConnection(host, userName, password);

            if (!_RenciShell.Connected)
                Assert.Fail("not connected");



            _RenciShell.Close();

        }
        [TestMethod]
        public void TestUbuntuEcho()
        {
            string host = "192.168.1.148";
            string userName = "ubsitest";
            string password = "manager";

            establishConnection(host, userName, password, 147);

            if (!_RenciShell.Connected)
                Assert.Fail("not connected");


            byte[] responseBytes;
            string nextString;

            List<string> allResp = new List<string>();


            // send commands add responses:

            _RenciShell.Write("pwd");

            responseBytes = _RenciShell.Receive();
            nextString = System.Text.Encoding.Default.GetString(responseBytes);
            allResp.Add(nextString);// VShellSsh2.RemoveSSHEscSequences(nextString, false));

            _RenciShell.Write("echo 123");
            responseBytes = _RenciShell.Receive();
            nextString = System.Text.Encoding.Default.GetString(responseBytes);

            allResp.Add(nextString);// VShellSsh2.RemoveSSHEscSequences(nextString, false));

            _RenciShell.Write("df");
            responseBytes = _RenciShell.Receive();
            nextString = System.Text.Encoding.Default.GetString(responseBytes);

            allResp.Add(nextString);// VShellSsh2.RemoveSSHEscSequences(nextString, false));


            _RenciShell.Close();

        }


        [TestMethod]
        public void TestUnixEcho()
        {
            string host = "sftpsrv.validataad.local";
            string userName = "sshtestuser1";
            string password = "Manager-123";

            establishConnection(host, userName, password, 22);

            if (!_RenciShell.Connected)
                Assert.Fail("not connected");


            byte[] responseBytes;
            string nextString;

            List<string> allResp = new List<string>();


            // send commands add responses:

            _RenciShell.Write("pwd");

            responseBytes = _RenciShell.Receive();
            nextString = System.Text.Encoding.Default.GetString(responseBytes);
            allResp.Add(nextString);// VShellSsh2.RemoveSSHEscSequences(nextString, false));

            _RenciShell.Write("echo 123");
            responseBytes = _RenciShell.Receive();
            nextString = System.Text.Encoding.Default.GetString(responseBytes);

            allResp.Add(nextString);// VShellSsh2.RemoveSSHEscSequences(nextString, false));

            _RenciShell.Write("df");
            responseBytes = _RenciShell.Receive();
            nextString = System.Text.Encoding.Default.GetString(responseBytes);

            allResp.Add(nextString);// VShellSsh2.RemoveSSHEscSequences(nextString, false));


            _RenciShell.Close();

        }


        [TestMethod]
        public void TestR14BitwiseR14COB()
        {
            string host = "192.168.1.67";
            string userName = "R14COB";
            string password = "R14COB";

            establishConnection(host, userName, password);

            if (!_RenciShell.Connected)
                Assert.Fail("not connected");

            _RenciShell.Close();

        }

        [TestMethod]
        public void TestRR14COBRemoteCmd()
        {
            string host = "192.168.1.67";
            string userName = "R14COB";
            string password = "R14COB";

            establishConnection(host, userName, password);

            if (!_RenciShell.Connected)
                Assert.Fail("not connected");

            List<string> allResp = new List<string>();


            // send commands add responses:

            allResp.Add(transceive("", false, ">"));
            allResp.Add(transceive("cd .."));
            allResp.Add(transceive("pwd"));
            allResp.Add(transceive("cd bnk.run"));
            allResp.Add(transceive("sleep 7", false, ">"));
            allResp.Add(transceive("remote.cmd"));
            allResp.Add(transceive("N"));
            allResp.Add(transceive("tSS TELNET.OFS"));
            allResp.Add(transceive("bad OFS"));
            allResp.Add(transceive("exit", true));
            allResp.Add(transceive("N"));
            allResp.Add(transceive("cmd.exe"));
            allResp.Add(transceive("jdiag"));

            _RenciShell.Close();

        }

        #region Actual work

        private void establishConnection(string host, string userName, string password, int port = 22)
        {
            _RenciShell = new VShellSshNET(host, port, _TerminalName, userName, password);
            _RenciShell.Connect( 15000);
        }

        internal string transceive(string commandString, bool isExit = false, string expectText = "")
        {
            if (!string.IsNullOrEmpty(commandString))
                _RenciShell.Write(commandString, isExit);

            if (!string.IsNullOrEmpty(expectText))
                return _RenciShell.RemoveSSHEscSequences(_RenciShell.ShellStream.Expect(new Regex(string.Format(@"([$#{0}])", expectText))), true);

            var responseBytes = _RenciShell.Receive();

            string nextString = System.Text.Encoding.Default.GetString(responseBytes);
            return _RenciShell.RemoveSSHEscSequences(nextString, true);
        }

        #endregion
    }
}
