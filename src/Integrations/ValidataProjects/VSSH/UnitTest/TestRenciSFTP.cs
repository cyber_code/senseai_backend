﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace VSSH.UnitTest
{
    [TestClass]
    public class TestRenciSFTP
    {

        VSshNETFtp _RenciSFTP;

        [TestMethod]
        public void TestR14BitwiseUpload()
        {
            string host = "192.168.1.67";
            string userName = "bnkuser";
            string password = "bnkuser";

            string localPath = "..\\..\\UnitTest\\TESTFILE.txt";
            string destinationPath = "TESTFILE.txt";

            establishConnection(host, userName, password);
            uploadTestFile(localPath, destinationPath);

            _RenciSFTP.Disconnect();
        }

        [TestMethod]
        public void TestR14BitwiseList()
        {
            string host = "192.168.1.67";
            string userName = "R14COB";
            string password = "R14COB";

            string destinationPath = ".";

            establishConnection(host, userName, password);

            var res = listDirectory(destinationPath);
            if (res.Count != 0)
            {


            }

            _RenciSFTP.Disconnect();
        }

        [TestMethod]
        public void TestUnixUpload()
        {
            string host = "sftpsrv.validataad.local";
            string userName = "sshtestuser1";
            string password = "Manager-123";

            string localPath = "..\\..\\UnitTest\\TESTFILE.txt";
            string destinationPath = "TESTFILE.txt";

            establishConnection(host, userName, password);
            uploadTestFile(localPath, destinationPath);

            _RenciSFTP.Disconnect();
        }

        [TestMethod]
        public void TestUnixList()
        {
            string host = "sftpsrv.validataad.local";
            string userName = "sshtestuser1";
            string password = "Manager-123";

            string destinationPath = ".";

            establishConnection(host, userName, password);

            var res = listDirectory(destinationPath);
            if (res.Count != 0)
            {


            }

            _RenciSFTP.Disconnect();
        }


        #region Actual work

        private Dictionary<string, string> listDirectory(string destinationPath)
        {
            string errMsg;
            var results = _RenciSFTP.List(destinationPath, out errMsg);

            if (results == null || !string.IsNullOrEmpty(errMsg))
                Assert.Fail("list failed; " + errMsg);

            return results;
        }

        private bool uploadTestFile(string localPath, string destinationPath)
        {
            string errMsg;
            var succ = _RenciSFTP.Upload(localPath, destinationPath, out errMsg);

            if (!succ || !string.IsNullOrEmpty(errMsg))
                Assert.Fail("not uploaded; " + errMsg);

            return true;
        }
        private bool establishConnection(string host, string userName, string password, int port = 22)
        {
            string errMsg;
            _RenciSFTP = new VSshNETFtp(host, port, userName, password, VSshNETFtp.Protocol.SFTP);
            _RenciSFTP.Connect(out errMsg);

            if (!_RenciSFTP.Connected || !string.IsNullOrEmpty(errMsg))
                Assert.Fail("not connected; " + errMsg);

            return true;
        }

        #endregion
    }
}
