﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace ValidataCommon
{
    [Serializable]
    public class PropertyFieldAttributeDefinition : AttributeDefinition
    {
        public bool IsMultipleProperty { get; set; }

        public int MultiplePropertyCount { get; set; }

        #region Constructors

        public PropertyFieldAttributeDefinition()
            : base()
        {
        }

        public PropertyFieldAttributeDefinition(AttributeDefinition attribute)
            : base(attribute)
        {
            if (attribute is PropertyFieldAttributeDefinition)
            {
                var src = attribute as PropertyFieldAttributeDefinition;
                IsMultipleProperty = src.IsMultipleProperty;
                MultiplePropertyCount = src.MultiplePropertyCount;
            }
        }

        public PropertyFieldAttributeDefinition(DataColumn column)
            : base(column)
        {
            if (column.ExtendedProperties != null)
            {
                if (column.ExtendedProperties.ContainsKey("IsMultipleProperty"))
                    IsMultipleProperty = column.ExtendedProperties["IsMultipleProperty"].ToString().ToUpper() == "TRUE";
                if (IsMultipleProperty && column.ExtendedProperties.ContainsKey("MultiplePropertyCount"))
                {
                    int count;
                    if (int.TryParse(column.ExtendedProperties["MultiplePropertyCount"].ToString(), out count))
                        MultiplePropertyCount = count;
                }
            }
        }

        #endregion

        public override List<AttributeDefinition> GetExpandedMultipleValue()
        {
            if (!IsMultipleProperty)
            {
                return base.GetExpandedMultipleValue();
            }

            List<AttributeDefinition> expandedDefinitions = new List<AttributeDefinition>();

            string propertyName, propertyFieldName;
            AAAttributeName.SplitAttributeName(Name, out propertyName, out propertyFieldName);

            if (string.IsNullOrEmpty(propertyName))
            {
                Debug.Fail("Should be impossible!");
                return base.GetExpandedMultipleValue();
            }

            for (int i = 0; i < MultiplePropertyCount; i++)
            {
                string attributeName = AAAttributeName.GetAttributeName(propertyName, i + 1, 1, propertyFieldName);

                AttributeDefinition ad = new PropertyFieldAttributeDefinition()
                                             {
                                                 Name = attributeName,
                                                 IsMultiValue = IsMultiValue,
                                                 MultipleValuesCount = MultipleValuesCount,
                                                 SuperMultipleValuesCount = SuperMultipleValuesCount
                                             };

                expandedDefinitions.AddRange(ad.GetExpandedMultipleValue());

                if (propertyFieldName == "@ID")
                {
                    string attributeNameEffectiveDate = AAAttributeName.GetAttributeName(propertyName, i + 1, 1, "EFFECTIVE");
                    expandedDefinitions.Add(new PropertyFieldAttributeDefinition()
                                                {
                                                    Name = attributeNameEffectiveDate,
                                                    IsMultiValue = false,
                                                    MultipleValuesCount = 0,
                                                    SuperMultipleValuesCount = 0
                                                }
                        );
                }
            }

            return expandedDefinitions;
        }

        public override DataColumn ToDataColumn()
        {
            DataColumn column = base.ToDataColumn();
            if (IsMultipleProperty)
            {
                column.ExtendedProperties.Add("IsMultipleProperty", IsMultipleProperty);
                column.ExtendedProperties.Add("MultiplePropertyCount", MultiplePropertyCount);
            }
            return column;
        }
    }
}