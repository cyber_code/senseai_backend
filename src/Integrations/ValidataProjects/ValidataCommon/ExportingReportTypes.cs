﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Handlers;

namespace ValidataCommon
{
    [Flags]
    public enum ExportingReportTypes
    {
        None = 0,
        All = 1,
        Passed = 2,
        FailedConsolidated = 4,
        FailedDetailed = 8
    };

    public static class FlagsHelper
    {

        public static ExportingReportTypes SetFlag(this ExportingReportTypes flags, ExportingReportTypes flag, bool value)
        {
            if (value)
                flags |= flag;
            else
                flags &= ~flag;

            return flags;
        }

        public static String GetReports(this ExportingReportTypes flags)
        {
            StringBuilder result = new StringBuilder();

            if (flags == ExportingReportTypes.None) result.Append("All");
            if (flags.HasFlag(ExportingReportTypes.All)) result.Append("S;");
            if (flags.HasFlag(ExportingReportTypes.Passed)) result.Append("P;");
            if (flags.HasFlag(ExportingReportTypes.FailedConsolidated)) result.Append("FC + ES;");
            if (flags.HasFlag(ExportingReportTypes.FailedDetailed)) result.Append("FD;");

            return result.ToString();
        }

        public static String GetFlagName(ExportingReportTypes currentFlag)
        {
            switch (currentFlag)
            {
                case ExportingReportTypes.None:
                    return "";
                case ExportingReportTypes.All:
                    return "Summary";
                case ExportingReportTypes.Passed:
                    return "Passed";
                case ExportingReportTypes.FailedConsolidated:
                    return "Failed Consolidated + Error Summary";
                case ExportingReportTypes.FailedDetailed:
                    return "Failed Detailed";
                default:
                    throw new Exception("Undefined Alternative style report");
            }
        }
    }


}
