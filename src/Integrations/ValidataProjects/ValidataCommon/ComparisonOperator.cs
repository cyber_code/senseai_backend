﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ValidataCommon
{
    /// <summary>
    /// Describes Comparison Operators. Have to be same as "enum Operators" in ValidationServices
    /// </summary>
    public enum ComparisonOperator : uint
    {
        GreaterThan = 0,
        GreaterThanOrEqual = 1,
        Equal = 2,
        NotEqual = 3,
        LowerThan = 4,
        LowerThanOrEqual = 5,
        Like = 6,
        StartsWith = 7,
        EndsWith = 8
    }

    public static class ComparisonOperators
    {
        public static IEnumerable<ComparisonOperator> Values { get { return Enum.GetValues(typeof(ComparisonOperator)).Cast<ComparisonOperator>(); } }

        public static IEnumerable<string> DisplayNames { get { return Values.Select(n => n.GetDisplayText()); } }

        public static ComparisonOperator FromDisplayName(string operatorName)
        {
            foreach (ComparisonOperator co in Values)
            {
                if (co.GetDisplayText() == operatorName || co.GetShortDisplayText() == operatorName)
                    return co;
            }

            return ComparisonOperator.Equal;    // default one
        }
    }

    public static class ComparisonOperatorExtensions
    {

        public static string GetDisplayText(this ComparisonOperator comparisonOperator)
        {
            switch (comparisonOperator)
            {
                case ComparisonOperator.GreaterThan:
                    return "greater than";
                case ComparisonOperator.GreaterThanOrEqual:
                    return "greater than or equal to";
                case ComparisonOperator.Equal:
                    return "equals";
                case ComparisonOperator.NotEqual:
                    return "not equals";
                case ComparisonOperator.LowerThan:
                    return "less than";
                case ComparisonOperator.LowerThanOrEqual:
                    return "less than or equal to";
                case ComparisonOperator.Like:
                    return "like";
                case ComparisonOperator.StartsWith:
                    return "starts with";
                case ComparisonOperator.EndsWith:
                    return "ends with";
            }

            System.Diagnostics.Debug.Fail("Unknown operator type");
            return "equals";
        }

        public static string GetShortDisplayText(this ComparisonOperator comparisonOperator)
        {
            switch (comparisonOperator)
            {
                case ComparisonOperator.GreaterThan:
                    return ">";
                case ComparisonOperator.GreaterThanOrEqual:
                    return ">=";
                case ComparisonOperator.Equal:
                    return "=";
                case ComparisonOperator.NotEqual:
                    return "!=";
                case ComparisonOperator.LowerThan:
                    return "<";
                case ComparisonOperator.LowerThanOrEqual:
                    return "<=";
                case ComparisonOperator.Like:
                    return "contains";
                case ComparisonOperator.StartsWith:
                    return "starts with";
                case ComparisonOperator.EndsWith:
                    return "ends with";
            }

            System.Diagnostics.Debug.Fail("Unknown operator type");
            return "=";
        }
    }
}
