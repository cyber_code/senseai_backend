﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class DifferenceErrorRule
    {
        public String Name { get; set; }
        public bool Enabled { get; set; }
    }
}
