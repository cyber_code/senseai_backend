﻿namespace ValidataCommon
{
    public enum ComparisonOutcome
    {
        Success,
        Failed,
        Skipped
    }

    public class ShortComparisonResult
    {
        public ComparisonOutcome Outcome;
        public string Details;

        #region Class Lifecycle

        /// <summary>
        /// Initializes a new instance of the <see cref="ShortComparisonResult"/> class.
        /// </summary>
        public ShortComparisonResult() {}

        /// <summary>
        /// Initializes a new instance of the <see cref="ShortComparisonResult"/> class.
        /// </summary>
        /// <param name="outcome">The outcome.</param>
        /// <param name="details">The details.</param>
        public ShortComparisonResult(ComparisonOutcome outcome, string details)
        {
            Outcome = outcome;
            Details = details;
        }

        #endregion

        /// <summary>
        /// Gets the failed.
        /// </summary>
        /// <param name="reason">The reason.</param>
        /// <returns></returns>
        public static ShortComparisonResult GetFailed(string reason)
        {
            return new ShortComparisonResult(ComparisonOutcome.Failed, reason);
        }

        /// <summary>
        /// Gets the skipped.
        /// </summary>
        /// <param name="reason">The reason.</param>
        /// <returns></returns>
        public static ShortComparisonResult GetSkipped(string reason)
        {
            return new ShortComparisonResult(ComparisonOutcome.Skipped, reason);
        }

        /// <summary>
        /// Sets the fail.
        /// </summary>
        public void SetFail()
        {
            Outcome = ComparisonOutcome.Failed;
        }

        /// <summary>
        /// Sets the success.
        /// </summary>
        public void SetSuccess()
        {
            Outcome = ComparisonOutcome.Success;
        }

        /// <summary>
        /// Sets the skipped.
        /// </summary>
        public void SetSkipped()
        {
            Outcome = ComparisonOutcome.Skipped;
        }

        /// <summary>
        /// Sets the success or fail.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        public void SetSuccessOrFail(bool success)
        {
            if (success)
            {
                SetSuccess();
            }
            else
            {
                SetFail();
            }
        }
    }
}