﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace ValidataCommon.UsersPool
{
    /// <summary>
    /// Loading and saving of users pools
    /// </summary>
    [Serializable]
    public class UsersPools
    {
        public static readonly  string UserPoolsDefaultPath = Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\Assemblies\UsersPools.xml";

        /// <summary>
        /// Directory for user pools per suite
        /// </summary>
        public static readonly  string PathToUsersPooolsPerCycle = Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\UsersPoolsPerCycle\";

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersPools"/> class.
        /// </summary>
        public UsersPools()
        {
            UsersPoolList = new List<UsersPool>();
        }

        /// <summary>
        /// Gets or sets the users pool list.
        /// </summary>
        /// <value>The users pool list.</value>
        [XmlElement("UsersPool")]
        public List<UsersPool> UsersPoolList { get; set; }

        /// <summary>
        /// Loads the user pools.
        /// </summary>
        /// <returns></returns>
        public static UsersPools LoadDefaultUserPools()
        {
            return LoadUserPoolsFromFile(UserPoolsDefaultPath);
        }

        /// <summary>
        /// Loads the suite user pools.
        /// </summary>
        /// <param name="testSuiteID">The suite ID.</param>
        /// <returns></returns>
        public static UsersPools LoadCycleUserPools(string testSuiteID)
        {
            string cycleSpecificPath = Path.Combine(PathToUsersPooolsPerCycle, testSuiteID + ".xml");
            if (File.Exists(cycleSpecificPath))
                return LoadUserPoolsFromFile(cycleSpecificPath);

            return LoadDefaultUserPools();
        }

        /// <summary>
        /// Loads the user pools.
        /// </summary>
        /// <returns></returns>
        public static UsersPools LoadUserPoolsFromFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new IOException(string.Format("File '{0}' does not exist.", filePath));
            }

            try
            {
                XmlDocument envDoc = new XmlDocument();
                envDoc.Load(filePath);

                UsersPools userpool = (UsersPools) XmlSerializationHelper.Deserialize(typeof (UsersPools), envDoc.DocumentElement.OuterXml);

                return userpool;
            }
            catch (Exception ex)
            {
                // The serialization error messages are cryptic at best.Give a hint at what happened
                throw new InvalidOperationException(string.Format("Failed to initialize user pools. Check users pool definitions in '{0}'", filePath), ex);
            }
        }

        /// <summary>
        /// Gets the user pool.
        /// </summary>
        /// <param name="poolName">Name of the pool.</param>
        /// <returns></returns>
        public UsersPool GetUserPool(string poolName)
        {
            if (UsersPoolList == null)
            {
                return null;
            }

            return UsersPoolList.FirstOrDefault(pool => pool.Name == poolName);
        }

        /// <summary>
        /// Saves the user pools.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        public void Save(string fileName)
        {
            XmlSerializationHelper.SerializeToXmlFile(this, fileName);
        }
    }
}