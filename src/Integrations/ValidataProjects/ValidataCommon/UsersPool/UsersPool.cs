﻿using System;
using System.Xml.Serialization;

namespace ValidataCommon.UsersPool
{
    /// <summary>
    /// Definition of user pool (currently only for T24 execution)
    /// </summary>
    [Serializable]
    public class UsersPool
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the prefix.
        /// </summary>
        /// <value>The prefix.</value>
        [XmlAttribute]
        public string Prefix { get; set; }

        /// <summary>
        /// Gets or sets the min.
        /// </summary>
        /// <value>The min.</value>
        [XmlAttribute]
        public int Min { get; set; }

        /// <summary>
        /// Gets or sets the max, but without additional/spare users.
        /// </summary>
        /// <value>The min.</value>
        [XmlAttribute]
        public int MaxWithoutAdditional { get; set; }

        /// <summary>
        /// Gets or sets the max.
        /// </summary>
        /// <value>The max.</value>
        [XmlAttribute]
        public int Max { get; set; }

        /// <summary>
        /// Gets or sets the total length to enable padding with zeroes
        /// </summary>
        /// <value>The total length.</value>
        [XmlAttribute]
        public int UserNameLength { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [XmlAttribute]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the pin.
        /// </summary>
        /// <value>The PIN.</value>
        [XmlAttribute]
        public string Pin { get; set; }

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>The count.</value>
        [XmlIgnore]
        public int Count
        {
            get { return Max - Min + 1; }
        }

        /// <summary>
        /// Minimum spare virtual user
        /// </summary>
        [XmlIgnore]
        public int MinSpareVirtUser
        {
            get { return MaxWithoutAdditional - Min + 1; }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public string GetUserName(int index)
        {
            if (index < 0)
            {
                throw new ArgumentException(string.Format("The index cannot be a negative number. Index = {0}", index));
            }

            int number = Min + index;
            if (number > Max)
            {
                throw new ArgumentException(string.Format("Not enough users in the pool. Requested = {0} Max = {1}", number, Max));
            }

            int numberOfPaddingZeroes = 0;
            if (UserNameLength != 0)
            {
                numberOfPaddingZeroes = Math.Max(0, UserNameLength - number.ToString().Length - Prefix.Length);
            }

            string userName = Prefix + new string('0', numberOfPaddingZeroes) + number;

            return userName;
        }

        /// <summary>
        /// Get all spare user names
        /// </summary>
        /// <returns></returns>
        public System.Collections.Generic.IEnumerable<string> GetSpareUserNames()
        {
            for (int i = MinSpareVirtUser; i < Count; i++)
            {
                yield return GetUserName(i);
            }
        }

        ///<summary>
        /// Name of default UsersPool
        ///</summary>
        public const string DefaultName = "Default";

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Pin))
            {
                return string.Format("{0}: {1} [{2}-{3}] {4} (pin: {5})", Name, Prefix, Min, Max, Password, Pin);
            }
            else
            {
                return string.Format("{0}: {1} [{2}-{3}] {4}", Name, Prefix, Min, Max, Password);
            }
        }
    }
}
