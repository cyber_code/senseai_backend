﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace ValidataCommon
{
    /// <summary>
    /// Enum containing T24 field behaviors
    /// </summary>
    public enum FieldBehavior
    {
        /// <summary>
        /// Normal input field
        /// </summary>
        NONE,

        /// <summary>
        /// Automatically updated field
        /// </summary>
        EXTERN,

        /// <summary>
        /// field content can only be inputted or very first record is not authorised
        /// </summary>
        NOCHANGE,

        /// <summary>
        /// indirectly updated by input of another feld
        /// </summary>
        NOINPUT,

        /// <summary>
        /// 'NV.EXTERN' is same like 'EXTERN' but Not Visible
        /// </summary>
        NV_EXTERN,


        /// <summary>
        /// Default field behaviour (e.g. NONE==INPUT)
        /// </summary>
        Default = NONE
    }

    /// <summary>
    /// Representation of SS SYS.VAL.PROG / USR.VAL.PROG field value
    /// </summary>
    public class ValProg
    {
        #region Enums

        /// <summary>
        /// Field indexes
        /// </summary>
        private enum FieldIndex : int
        {
            ValidationRoutine = 0,

            LookupValues,

            Behavior,

            Miltiline = 6,

            LookupTexts = 10,
        }

        #endregion

        #region Private Members

        /// <summary>
        /// List of values
        /// </summary>
        private readonly List<string> _Values = new List<string>();

        #endregion

        #region Public Properties

        /// <summary>
        /// Get validation routine complete text
        ///     Sample: AC2XXXX
        /// </summary>
        public string ValidationRoutine
        {
            get;
            private set;
        }

        /// <summary>
        /// Get whether XXX.VAL.PROG defines lookup values
        /// </summary>
        public bool HasLookupValues
        {
            get;
            private set;
        }

        /// <summary>
        /// Get whether XXX.VAL.PROG defines lookup texts
        /// </summary>
        public bool HasLookupTexts
        {
            get;
            private set;
        }

        /// <summary>
        /// Get lookup values
        /// </summary>
        public IEnumerable<string> LookupValues
        {
            get;
            private set;
        }

        /// <summary>
        /// Get lookup values
        /// </summary>
        public IEnumerable<string> LookupTexts
        {
            get;
            private set;
        }

        /// <summary>
        /// Field behavior (INPUT/NOINPUT...)
        /// </summary>
        public FieldBehavior Behavior
        {
            get;
            private set;
        }

        /// <summary>
        /// TextBox is multiline
        /// </summary>
        public bool IsMultiline
        {
            get;
            private set;
        }
        
        #endregion

        #region Constructor

        /// <summary>
        /// Constructs
        /// </summary>
        /// <param name="value">XXX.VAL.PROG value</param>
        public ValProg(string value)
        {
            try { Parse(value); }
            catch { }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Join("&", _Values);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Parses XXX.VAL.PROG value
        /// </summary>
        /// <param name="valProg">XXX.VAL.PROG value</param>
        private void Parse(string valProg)
        {
            string[] parts = NormalizeValProg(valProg).Split(new char[] { '&' });
            for (int i = 0; i < parts.Length; i++)
            {
                _Values.Add(parts[i]);
            }

            // Populate properties
            string fieldValue0 = GetFieldValue(FieldIndex.ValidationRoutine);
            ValidationRoutine = fieldValue0;

            {
                string fieldValue1 = GetFieldValue(FieldIndex.LookupValues);
                HasLookupValues = !string.IsNullOrEmpty(fieldValue1);
                LookupValues = HasLookupValues ? fieldValue1.Split(new char[] { '_' }) : null;
            }

            {
                string fieldValue10 = GetFieldValue(FieldIndex.LookupTexts);
                HasLookupTexts = !string.IsNullOrEmpty(fieldValue10);
                LookupTexts = HasLookupTexts ? fieldValue10.Split(new char[] { '_' }) : new string[0];
            }

            {
                string fieldValue2 = GetFieldValue(FieldIndex.Behavior);
                this.Behavior = GetFieldBehavior(fieldValue2);
            }

            {
                string fieldValue6 = GetFieldValue(FieldIndex.Miltiline);
                this.IsMultiline = fieldValue6 == "TEXT" || fieldValue0 == "IN2TEXT";
            }
        }

        /// <summary>
        /// Normalizes the val prog.
        /// </summary>
        /// <param name="valProg">The val prog.</param>
        /// <returns></returns>
        private string NormalizeValProg(string valProg)
        {
            //IN2&MS_MISS_MR_DR_REV_MRS&NOCOPY&&&&&&&&MS]Mme_MISS]Mlle_MR]Mr_DR]Dr_REV]Père_MRS]Mme
            string[] array = valProg.Split('&');
            if (array.Length == 11)
            {
                //MS]Mme_MISS]Mlle_MR]Mr_DR]Dr_REV]Père_MRS]Mme
                string[] subArray = array[10].Split('_');
                subArray = subArray.Select(item => item.Split(']').First()).ToArray();
                array[10] = string.Join("_", subArray);

                valProg = string.Join("&", array);
            }

            return valProg;
        }

        /// <summary>
        /// Get field behavior from string
        /// </summary>
        /// <param name="fieldValue2"></param>
        /// <returns></returns>
        private static FieldBehavior GetFieldBehavior(string fieldValue2)
        {
            switch (fieldValue2)
            {
                case "":
                    return FieldBehavior.NONE;
                case "EXTERN":
                    return FieldBehavior.EXTERN;
                case "NOCHANGE":
                    return FieldBehavior.NOCHANGE;
                case "NOINPUT":
                    return FieldBehavior.NOINPUT;
                case "NV.EXTERN":
                    return FieldBehavior.NV_EXTERN;
                default:
                    return FieldBehavior.Default;
            }
        }

        /// <summary>
        /// Get value by index
        /// </summary>
        /// <param name="fieldIndex">Field Index</param>
        /// <returns>Field value</returns>
        private string GetFieldValue(FieldIndex fieldIndex)
        {
            return GetFieldValue((int)fieldIndex);
        }

        /// <summary>
        /// Get value by index
        /// </summary>
        /// <param name="fieldIndex">Field Index</param>
        /// <returns>Field value</returns>
        public string GetFieldValue(int fieldIndex)
        {
            return fieldIndex >= _Values.Count
                ? string.Empty 
                : _Values[fieldIndex];
        }

        #endregion
    }
}
