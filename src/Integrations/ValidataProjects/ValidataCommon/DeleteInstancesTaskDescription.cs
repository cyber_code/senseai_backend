﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ValidataCommon
{
    [Serializable]
    public class DeleteInstancesTaskDescription
    {
        
        public DeleteInstancesTaskDescription()
        {

        }

        #region Public Properties

        public String CatalogName { get; set; }
        public String TypicalName { get; set; }
        public String GroupName { get; set; }

        public ulong TypicalNoderef { get; set; }
        public long GroupID { get; set; }

        public String TaskFolder { get; set; }


        #endregion

        #region Serialization

        public static DeleteInstancesTaskDescription DeserializeFromString(string serialized)
        {
            if (!string.IsNullOrEmpty(serialized))
            {
                XmlSerializer deSerializator = new XmlSerializer(typeof(DeleteInstancesTaskDescription));
                // Reading the XML report
                DeleteInstancesTaskDescription deleteInstancesTaskDescription;
                try
                {
                    deleteInstancesTaskDescription = deSerializator.Deserialize(
                        new StringReader(serialized)) as DeleteInstancesTaskDescription;
                }
                catch(InvalidOperationException)
                {
                    deleteInstancesTaskDescription = null;
                }

                if (deleteInstancesTaskDescription == null)
                {
                    ApplicationException exp = new ApplicationException(
                        "The delete instances task description cannot been successfully read.");
                    exp.Data.Add("serialized", serialized);
                    throw exp;
                }

                return deleteInstancesTaskDescription;
            }
            else
            {
                return new DeleteInstancesTaskDescription();
            }
        }

        public string SerializeToString()
        {
            StringWriter sw = new StringWriter();
            XmlSerializer serializator = new XmlSerializer(GetType());
            serializator.Serialize(sw, this);
            return sw.ToString();
        }

        #endregion
    }
}
