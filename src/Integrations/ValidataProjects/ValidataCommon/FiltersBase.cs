﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Validata.Common;

namespace ValidataCommon
{
    public class FiltersBase
    {

        /// <summary>
        /// Logical Operation Attribute Name
        /// </summary>
        public const string FILTER_LOGICAL_OPERATION_ATTRIBUTE = "logicalOperation";

        #region Private And Protected Members

        /// <summary>
        /// Filters collection
        /// </summary>
        private List<FilterElement> _Filters = new List<FilterElement>();

        public FiltersBase()
        {
            
        }

        public FiltersBase(List<FilterElement> filters)
        {
            AddRange(filters);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Filters collection
        /// </summary>
        public List<FilterElement> FiltersCollection
        {
            get { return _Filters; }
            set { _Filters = value; }
        }

        /// <summary>
        /// Current count of filters
        /// </summary>
        public int Count
        {
            get { return _Filters.Count; }
        }

        /// <summary>
        /// Indexer implementation.
        /// </summary>
        public FilterElement this[int index]
        {
            get { return _Filters[index]; }
            set { _Filters[index] = value; }
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Adds filter to filters collection
        /// </summary>
        /// <param name="attrName">Filter attribute name</param>
        /// <param name="attrFilter">Filter attribute value</param>
        /// <param name="attrDataType">Filter attribute data type</param>
        /// <param name="ft">Filter type</param>
        /// <returns>If succeeded returns true, otherwise - false</returns>
        public bool AddFilter(string attrName, string attrFilter, AttributeDataType attrDataType, FilterType ft)
        {
            if (attrName != null && attrFilter != null)
            {
                FilterElement f = new FilterElement(attrName, attrFilter, attrDataType, ft);
                _Filters.Add(f);

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Adds filter to filters collection
        /// </summary>
        /// <param name="filter">Adapter Filter</param>
        /// <returns>If succeeded returns true, otherwise - false</returns>
        public bool AddFilter(FilterElement filter)
        {
            if (filter != null)
            {
                _Filters.Add(filter);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Serializes the filters section to the XmlWriter
        /// </summary>
        /// <param name="xmlWriter">XmlWriter where to serialize the Filters section</param>
        public void ToXml(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(FilterElement.FILTERS_TAG);

            foreach (FilterElement ft in _Filters)
            {
                ft.ToXml(xmlWriter);
            }

            xmlWriter.WriteEndElement();

            xmlWriter.Flush();
        }

        /// <summary>
        /// Reads the Filter section from XML
        /// </summary>
        /// <param name="filterReader">The XML string defining the Filters section.</param>
        public void FromXml(XmlReader filterReader)
        {
            // Init reader
            bool nodeIsRead = false;
            // Read input data and locate root typical
            while (nodeIsRead || filterReader.Read())
            {
                //ReadOuterXML operations cause the next node to be loaded so we need to skip reading 
                if (nodeIsRead)
                {
                    nodeIsRead = false;
                }
                if (filterReader.NodeType == XmlNodeType.Element)
                {
                    switch (filterReader.Name)
                    {
                        case FilterElement.FILTER_TAG:
                            string logOp = filterReader.GetAttribute(FILTER_LOGICAL_OPERATION_ATTRIBUTE);
                            using (XmlReader reader = filterReader.ReadSubtree())
                            {
                                FilterElement filter = FilterElement.FromXML(reader, logOp);
                                AddFilter(filter);
                            }
                            nodeIsRead = true;
                            break;
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach(FilterElement filter in _Filters)
            {
                sb.AppendLine(filter.ToString());
            }
            return sb.ToString();
        }

        public void AddRange(List<FilterElement> list)
        {
            if (list == null)
                return;
            foreach (var filterElement in list)
            {
                AddFilter(filterElement);
            }
        }
    }
}
