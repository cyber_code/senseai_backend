﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Delimited File Reader
    /// </summary>
    public class DelimitedFileReader : IDisposable
    {
        #region Private variables

        private Stream _Stream;
        private StreamReader _Reader;
        private char _Separator;

        #endregion

        /// <summary>
        /// Creates a new reader for the given stream, encoding and separator character.
        /// </summary>
        /// <param name="s">The stream to read the data from.</param>
        /// <param name="enc">The encoding used.</param>
        /// <param name="separator">The separator character between the fields</param>
        public DelimitedFileReader(Stream s, Encoding enc, char separator)
        {
            _Separator = separator;
            _Stream = s;
            if (!s.CanRead)
            {
                throw new InvalidOperationException("Could not read the given data stream!");
            }
            _Reader = (enc != null) ? new StreamReader(s, enc) : new StreamReader(s);
        }

        /// <summary>
        /// Creates a new reader for the given text file path, encoding and field separator.
        /// </summary>
        /// <param name="filename">The name of the file to be read.</param>
        /// <param name="enc">The encoding used.</param>
        /// <param name="separator">The field separator character.</param>
        public DelimitedFileReader(string filename, Encoding enc, char separator)
            : this(new FileStream(filename, FileMode.Open, FileAccess.Read), enc, separator)
        {
        }

        /// <summary>
        /// Returns the fields for the next row of data (or null if at eof)
        /// </summary>
        /// <returns>A string array of fields or null if at the end of file.</returns>
        public string[] ReadDelimitedLine()
        {
            string data = _Reader.ReadLine();

            if (data == null)
            {
                return null;
            }

            if (data.Length == 0)
            {
                return new string[0];
            }

            return data.Split(new char[] {_Separator});
        }

        /// <summary>
        /// Gets all CSV lines.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string[]> EnumerateAllLines()
        {
            while (true)
            {
                string[] row = ReadDelimitedLine();
                if (row == null)
                {
                    break;
                }
                else
                {
                    yield return row;
                }
            }
        }

        /// <summary>
        /// Disposes the reader. The underlying stream is closed.
        /// </summary>
        public void Dispose()
        {
            // Closing the reader closes the underlying stream, too
            if (_Reader != null)
            {
                _Reader.Close();
            }
            else
            {
                if (_Stream != null)
                {
                    _Stream.Close(); // In case we failed before the reader was constructed
                }
            }

            GC.SuppressFinalize(this);
        }
    }
}