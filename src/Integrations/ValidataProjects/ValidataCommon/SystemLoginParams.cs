﻿using System;
using System.Diagnostics;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Parameters for login in the system
    /// </summary>
    public class SystemLoginParams
    {
        private const char Delimiter = '~';

        #region Members

        private ulong _Project;
        private ulong _Dicipline;
        private ulong _Role;
        private ulong _User;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the project.
        /// </summary>
        /// <value>The project.</value>
        public ulong Project
        {
            [DebuggerStepThrough]
            get { return _Project; }
            [DebuggerStepThrough]
            set { _Project = value; }
        }

        /// <summary>
        /// Gets or sets the dicipline.
        /// </summary>
        /// <value>The dicipline.</value>
        public ulong Dicipline
        {
            [DebuggerStepThrough]
            get { return _Dicipline; }
            [DebuggerStepThrough]
            set { _Dicipline = value; }
        }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        /// <value>The role.</value>
        public ulong Role
        {
            [DebuggerStepThrough]
            get { return _Role; }
            [DebuggerStepThrough]
            set { _Role = value; }
        }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>The user.</value>
        public ulong User
        {
            [DebuggerStepThrough]
            get { return _User; }
            [DebuggerStepThrough]
            set { _User = value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemLoginParams"/> class.
        /// </summary>
        public SystemLoginParams() {}

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemLoginParams"/> class.
        /// </summary>
        /// <param name="project">The _ project.</param>
        /// <param name="dicipline">The _ dicipline.</param>
        /// <param name="role">The _ role.</param>
        /// <param name="user">The _ user.</param>
        public SystemLoginParams(ulong project, ulong dicipline, ulong role, ulong user)
        {
            _Project = project;
            _Dicipline = dicipline;
            _Role = role;
            _User = user;
        }

        #endregion

        /// <summary>
        /// Decodes authentication information supplied as parameter - user, password etc.
        /// </summary>
        /// <param name="authParam">Authentication Parameters</param>
        /// <returns>Decoded parameters string</returns>
        private static string DecodeFromBase64(string authParam)
        {
            byte[] decoded = Convert.FromBase64String(authParam);

            StringBuilder ap = new StringBuilder();

            for (int i = 0; i < decoded.Length; i++)
            {
                ap.Append((char) (decoded[i]));
            }

            return ap.ToString();
        }

        /// <summary>
        /// Gets as text.
        /// </summary>
        /// <value>As text.</value>
        public string AsText
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(_Project);
                sb.Append(Delimiter);
                sb.Append(_Dicipline);
                sb.Append(Delimiter);
                sb.Append(_Role);
                sb.Append(Delimiter);
                sb.Append(_User);

                return sb.ToString();
            }
        }

        /// <summary>
        /// Initialze from base64 text.
        /// </summary>
        /// <param name="encodedParams">The encoded params.</param>
        public void FromBase64Text(string encodedParams)
        {
            string[] authParams = DecodeFromBase64(encodedParams).Split(Delimiter);
            _Project = ulong.Parse(authParams[0]);
            _Dicipline = ulong.Parse(authParams[1]);
            _Role = ulong.Parse(authParams[2]);
            _User = ulong.Parse(authParams[3]);
        }

        /// <summary>
        /// Parses the base64 text.
        /// </summary>
        /// <param name="encodedParams">The encoded params.</param>
        /// <returns></returns>
        public static SystemLoginParams ParseBase64Text(string encodedParams)
        {
            SystemLoginParams p = new SystemLoginParams();
            p.FromBase64Text(encodedParams);
            return p;
        }

        /// <summary>
        /// Returns the base64 text.
        /// </summary>
        /// <returns></returns>
        public string AsBase64EncodedText
        {
            get
            {
                string text = AsText;

                byte[] bytes = new byte[text.Length];
                char[] chars = text.ToCharArray();

                for (int i = 0; i < chars.Length; i++)
                {
                    bytes[i] = (byte) chars[i];
                }

                return Convert.ToBase64String(bytes);
            }
        }
    }
}