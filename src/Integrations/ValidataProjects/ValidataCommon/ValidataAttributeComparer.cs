using System;
using System.Collections.Generic;
using Validata.Common;

namespace ValidataCommon
{
    /// <summary>
    /// Compares attribute values according to their type
    /// </summary>
    public class ValidataAttributeComparer : IComparer<string>
    {
        private AttributeDataType _AttributeType;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidataAttributeComparer"/> class.
        /// </summary>
        /// <param name="_AttributeType">Type of the _ attribute.</param>
        public ValidataAttributeComparer(AttributeDataType _AttributeType)
        {
            this._AttributeType = _AttributeType;
        }

        /// <summary>
        /// Compares the attribute values.
        /// </summary>
        /// <param name="valueOne">The value one.</param>
        /// <param name="valueTwo">The value two.</param>
        /// <returns></returns>
        public int Compare(string valueOne, string valueTwo)
        {
            int res;
            switch (_AttributeType)
            {
                case AttributeDataType.Integer:
                    res = CompareIntegerAttributeValues(valueOne, valueTwo);
                    break;

                case AttributeDataType.Measure:
                case AttributeDataType.Real:
                    res = CompareDoubleAttributeValues(valueOne, valueTwo);
                    break;

                case AttributeDataType.Currency:
                    res = CompareDecimalAttributeValues(valueOne, valueTwo);
                    break;

                case AttributeDataType.DateTime:
                    res = CompareDateTimeAttributeValues(valueOne, valueTwo);
                    break;

                case AttributeDataType.String:
                default:
                    res = CompareStringAttributeValues(valueOne, valueTwo);
                    break;
            }
            return res;
        }

        /// <summary>
        /// Compares the string attribute values.
        /// </summary>
        /// <param name="valueOne">The value one.</param>
        /// <param name="valueTwo">The value two.</param>
        /// <returns></returns>
        public static int CompareStringAttributeValues(string valueOne, string valueTwo)
        {
            return string.Compare(valueOne, valueTwo, true); //case insensitive
        }

        /// <summary>
        /// Compares the date time attribute values.
        /// </summary>
        /// <param name="valueOne">The value one.</param>
        /// <param name="valueTwo">The value two.</param>
        /// <returns></returns>
        public static int CompareDateTimeAttributeValues(string valueOne, string valueTwo)
        {
            DateTime dateOne, dateTwo;

            ValidataConverter.TryGetDateTimeFromTicks(valueOne, out dateOne);
            ValidataConverter.TryGetDateTimeFromTicks(valueTwo, out dateTwo);

            return DateTime.Compare(dateOne, dateTwo);
        }

        /// <summary>
        /// Compares the decimal attribute values.
        /// </summary>
        /// <param name="valueOne">The value one.</param>
        /// <param name="valueTwo">The value two.</param>
        /// <returns></returns>
        public static int CompareDecimalAttributeValues(string valueOne, string valueTwo)
        {
            int res;
            decimal decOne = ValidataConverter.GetDecimal(valueOne);
            decimal decTwo = ValidataConverter.GetDecimal(valueTwo);
            if (decOne > decTwo)
            {
                res = 1;
            }
            else if (decOne == decTwo)
            {
                res = 0;
            }
            else
            {
                res = -1;
            }
            return res;
        }

        /// <summary>
        /// Compares the double attribute values.
        /// </summary>
        /// <param name="valueOne">The value one.</param>
        /// <param name="valueTwo">The value two.</param>
        /// <returns></returns>
        public static int CompareDoubleAttributeValues(string valueOne, string valueTwo)
        {
            int res;
            double dblOne = ValidataConverter.GetDouble(valueOne);
            double dblTwo = ValidataConverter.GetDouble(valueTwo);
            if (dblOne > dblTwo)
            {
                res = 1;
            }
            else if (dblOne == dblTwo)
            {
                res = 0;
            }
            else
            {
                res = -1;
            }
            return res;
        }

        /// <summary>
        /// Compares the integer attribute values.
        /// </summary>
        /// <param name="valueOne">The value one.</param>
        /// <param name="valueTwo">The value two.</param>
        /// <returns></returns>
        public static int CompareIntegerAttributeValues(string valueOne, string valueTwo)
        {
            int res;
            int intOne = ValidataConverter.GetInt(valueOne);
            int intTwo = ValidataConverter.GetInt(valueTwo);
            if (intOne > intTwo)
            {
                res = 1;
            }
            else if (intOne == intTwo)
            {
                res = 0;
            }
            else
            {
                res = -1;
            }
            return res;
        }
    }
}