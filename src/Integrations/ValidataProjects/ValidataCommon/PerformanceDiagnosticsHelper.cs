﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    public static class PerformanceDiagnosticsHelper
    {
        private class Stats
        {
            internal string StackTrace;

            internal int Calls;

            internal readonly List<TimeSpan> Durations = new List<TimeSpan>();

            internal TimeSpan Total
            {
                get
                {
                    TimeSpan res = TimeSpan.Zero;
                    Durations.ForEach(n => res += n);
                    return res;
                }
            }
        }

        private readonly static Dictionary<string, Stats> _Stats = new Dictionary<string, Stats>();

        private static DateTime _CurrOperationStartedAt = DateTime.MinValue;

        private static string _CurrOperationStackTrance;

        private static string _CurrOperationCallArgs;

        public static void Start(string args)
        {
            // TODO: _CurrOperationCallArgs = args;
            _CurrOperationStartedAt = DateTime.Now;
            _CurrOperationStackTrance = Environment.StackTrace;
        }

        public static void End()
        {
            if (!_Stats.ContainsKey(_CurrOperationStackTrance))
                _Stats[_CurrOperationStackTrance] = new Stats() { StackTrace = _CurrOperationStackTrance };

            _Stats[_CurrOperationStackTrance].Calls++;
            _Stats[_CurrOperationStackTrance].Durations.Add(DateTime.Now - _CurrOperationStartedAt);
        }

        public static void Clear()
        {
            _Stats.Clear();
        }

        public static string GetDump()
        {
            var orderedStats = _Stats.Values.OrderBy(n => n.Total).ToList();

            StringBuilder sbResult = new StringBuilder();
            for (int i = orderedStats.Count - 1; i >= 0; i--)
            {

                sbResult.AppendFormat(
                    "------------------------------------------------------------\r\n" +
                    " CALLS: " + orderedStats[i].Calls + "\r\n" +
                    " TOTAL: " + orderedStats[i].Total.TotalSeconds + " sec\r\n" +
                    "------------------------------------------------------------\r\n" +
                    // TODO: " CALL PARAMS: " + orderedStats[i].pa
                    "------------------------------------------------------------\r\n" +
                    orderedStats[i].StackTrace + "\r\n" +
                    "============================================================\r\n"
                    );
            }

            return sbResult.ToString();
        }
    }
}
