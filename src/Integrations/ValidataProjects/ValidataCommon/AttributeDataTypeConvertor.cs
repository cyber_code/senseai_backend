using System;
using System.Collections.Generic;

namespace Validata.Common
{
    /// <summary>
    /// Converts AttributeDataType to and from string/System.Type
    /// </summary>
    public static class AttributeDataTypeConvertor
    {
        #region AttributeDataType<->String conversion

        /// <summary>
        /// Converts AttributeDataType to string
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <returns></returns>
        public static string ToString(AttributeDataType dataType)
        {
            switch (dataType)
            {
                case AttributeDataType.String:
                    return "string";
                case AttributeDataType.Integer:
                    return "integer";
                case AttributeDataType.Real:
                    return "real";
                case AttributeDataType.Boolean:
                    return "boolean";
                case AttributeDataType.DateTime:
                    return "date";
                case AttributeDataType.Blob:
                    return "blob";
                case AttributeDataType.Currency:
                    return "currency";
                case AttributeDataType.Measure:
                    return "measure";
                case AttributeDataType.Array:
                    return "array";
                default:
                    return "unkown";
            }
        }

        /// <summary>
        /// Converts the string to enum
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <returns></returns>
        public static AttributeDataType ToEnum(string dataType)
        {
            dataType = dataType.ToLower();

            switch (dataType)
            {
                case "string":
                    return AttributeDataType.String;
                case "integer":
                    return AttributeDataType.Integer;
                case "real":
                    return AttributeDataType.Real;
                case "boolean":
                    return AttributeDataType.Boolean;
                case "date":
                    return AttributeDataType.DateTime;
                case "blob":
                    return AttributeDataType.Blob;
                case "currency":
                    return AttributeDataType.Currency;
                case "measure":
                    return AttributeDataType.Measure;
                case "array":
                    return AttributeDataType.Array;
                default:
                    return AttributeDataType.Unknown;
            }
        }

        #endregion

        #region AttributeDataType <-> System Type conversion

        /// <summary>
        /// Converts the AttributeDataType to a primitive type.
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <returns></returns>
        public static Type ToSystemType(AttributeDataType dataType)
        {
            switch (dataType)
            {
                case AttributeDataType.String:
                    return typeof (string);
                case AttributeDataType.Integer:
                    return typeof (long);
                case AttributeDataType.Real:
                    return typeof (double);
                case AttributeDataType.Boolean:
                    return typeof (bool);
                case AttributeDataType.DateTime:
                    return typeof (DateTime);
                case AttributeDataType.Blob:
                    return typeof (byte[]);
                case AttributeDataType.Currency:
                    return typeof (decimal);
                case AttributeDataType.Measure:
                    return null;
                case AttributeDataType.Array:
                    return typeof(List<string>);
                default:
                    return null;
            }
        }

        /// <summary>
        /// Converts the primitive type to a AttributeDataType.
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <returns></returns>
        public static AttributeDataType FromSystemType(Type dataType)
        {
            switch (dataType.Name)
            {
                case "Int32":
                case "Int64":
                case "UInt32":
                case "UInt64":
                    return AttributeDataType.Integer;
                case "Real":
                case "Double":
                    return AttributeDataType.Real;
                case "DateTime":
                    return AttributeDataType.DateTime;
                case "Decimal":
                    return AttributeDataType.Currency;
                case "Boolean":
                    return AttributeDataType.Boolean;
                case "Byte[]":
                    return AttributeDataType.Blob;
            }

            if (dataType == typeof(List<string>))
                return AttributeDataType.Array;

            // default
            return AttributeDataType.String;
        }

        #endregion

        /// <summary>
        /// Getsall the valid data types.
        /// </summary>
        /// <returns></returns>
        public static AttributeDataType[] GetValidDataTypes()
        {
            List<AttributeDataType> validItems = new List<AttributeDataType>();
            foreach (AttributeDataType dt in Enum.GetValues(typeof(AttributeDataType)))
            {
                if (dt != AttributeDataType.Unknown)
                    validItems.Add(dt);
            }

            return validItems.ToArray();
        }
    }
}