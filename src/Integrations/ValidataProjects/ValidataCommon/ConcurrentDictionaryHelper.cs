﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidataCommon
{
    /// <summary>
    /// ConcurrentDictionary Helper class.
    /// </summary>
    public static class ConcurrentDictionaryHelper
    {
        /// <summary>
        /// Use this method for direct updating some specific item in the concurrent dictionary.
        /// </summary>
        /// <typeparam name="key"></typeparam>
        /// <typeparam name="value"></typeparam>
        /// <param name="concurrentDic"></param>
        /// <param name="m_Key"></param>
        /// <param name="m_Value"></param>
        public static void UpdateItem<key, value>(this ConcurrentDictionary<key, value> concurrentDic, key m_Key, value m_Value)
        {
            value file;
            concurrentDic.TryRemove(m_Key, out file);
            concurrentDic.TryAdd(m_Key, m_Value);
        }
    }
}
