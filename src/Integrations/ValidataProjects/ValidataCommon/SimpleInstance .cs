﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ValidataCommon;

namespace ValidataCommon
{
    public class SimpleAttribute : IAttribute
    {
        public SimpleAttribute(string name, string value)
        {
            Name = name;
            Value = value;
        }

        #region IAttribute Members

        public string Name { get; set; }
        public string Value { get; set; }

        #endregion
    }

    public class SimpleInstance : IValidataRecord
    {
        #region IValidataRecord Members

        public string CatalogName { get; set; }

        public string TypicalName { get; set; }

        private readonly List<IAttribute> _Attributes = new List<IAttribute>();
        private readonly Dictionary<string, IAttribute> _AttributesByName = new Dictionary<string, IAttribute>();

        public IEnumerable<IAttribute> Attributes
        {
            get { return _Attributes; }
            set
            {
                _Attributes.Clear();
                _AttributesByName.Clear();

                if (value != null)
                {
                    foreach (IAttribute attribute in value)
                    {
                        AddAttribute(attribute.Name, attribute.Value);
                    }
                }
            }
        }

        public IAttribute FindAttribute(string name)
        {
            return _AttributesByName.ContainsKey(name) ? _AttributesByName[name] : null;
        }

        #endregion

        public SimpleInstance()
        {
        }

        public SimpleInstance(IValidataRecord src)
        {
            this.CatalogName = src.CatalogName;
            this.TypicalName = src.TypicalName;

            if (src.Attributes != null)
            {
                foreach (IAttribute n in src.Attributes)
                {
                    AddAttribute(n.Name, n.Value);
                }
            }
        }

        public void AddAttribute(string name, string value)
        {
            var attr = new SimpleAttribute(name, value);
            _Attributes.Add(attr);
            _AttributesByName[attr.Name] = attr;
        }

        public void RemoveAttribute(string name)
        {
            _Attributes.RemoveAll(n => n.Name == name);
            _AttributesByName.Remove(name);
        }
    }
}
