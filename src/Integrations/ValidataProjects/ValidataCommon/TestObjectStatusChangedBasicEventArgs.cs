﻿using System;

namespace ValidataCommon
{
    [Serializable]
    public class TestObjectStatusChangedBasicEventArgs
    {
        /// <summary>
        /// TestCycle; TestCase or TestStep
        /// </summary>
        public string ObjectType { get; set; }

        /// <summary>
        /// The status string of the object:
        ///                 TestCycle & TestCase: ValidataCommon.Enums.ExecutionStatus
        ///                 TestStep: ValidataCommon.Enums.Enums.TestStepStatus
        /// </summary>
        public string Status { get; set; }
        public Enums.ExecutionAdapterMode ExecutionAdapterMode { get; set; }
        public string ID { get; set; }

        public ulong Noderef { get; set; }

        public string ParentCycleID { get; set; }

        public TestObjectStatusChangedBasicEventArgs()
        {
        }

        public TestObjectStatusChangedBasicEventArgs(string objectType, string status, Enums.ExecutionAdapterMode executionAdapterMode, string id, ulong noderef, string parentCycleID)
        {
            this.ObjectType = objectType;
            this.Status = status;
            this.ID = id;
            this.Noderef = noderef;
            this.ParentCycleID = parentCycleID;
            this.ExecutionAdapterMode = executionAdapterMode;
        }
    }
}
