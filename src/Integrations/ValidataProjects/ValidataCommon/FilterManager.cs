using System.Collections.Generic;
using System.Linq;
using ValidataCommon;
using ValidataCommon.Interfaces;

namespace ValidataCommon
{
    /// <summary>
    /// 
    /// </summary>
    public class FilterManager
    {
        private Dictionary<string, FilterElementList> _Filters;

        /// <summary>
        /// Constructs
        /// </summary>
        /// <param name="filters"></param>
        public FilterManager(IEnumerable<IFilteringConstraint> filters = null)
        {
            if (filters != null)
                this._Filters = GetFiltersGroupedByAttribute(filters);
        }

        /// <summary>
        /// Tests the row with filters.
        /// </summary>
        /// <param name="attributeNames">The attributes.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public bool DoesRowSatisfyFilters(string[] attributeNames, string[] values)
        {
            if (_Filters == null)
                return true;

            int index = 0;
            foreach (string attributeName in attributeNames)
            {
                var filtersForAttribute = GetFiltersForAttribute(attributeName);
                if (filtersForAttribute != null && !DoesValueSatisfyFilters(values[index], filtersForAttribute))
                    return false;

                index++;
            }

            return true;
        }

        ///// <summary>
        ///// Test the intance satisfy filters.
        ///// </summary>
        ///// <param name="instance">The instance.</param>
        ///// <returns></returns>
        //public bool DoesInstanceSatisfyFilters(IValidataRecord instance)
        //{
        //    if (_Filters == null)
        //        return true;

        //    IEnumerable<IAttribute> multivaluedAttributes = instance.Attributes.Where(t => t.Name.Contains("~"));
        //    IEnumerable<IAttribute> singleAttributes = instance.Attributes.Where(t => !t.Name.Contains("~"));

        //    //FilterElementList filtersForAttribute = new FilterElementList();
        //    List<bool> checkIfNeedToReturn = new List<bool>();

        //    List<IAttribute> tempMultivaluedAttributes = new List<IAttribute>();
        //    var fieldNameTemp = "";

        //    //check in multivalued attributes
        //    foreach (IAttribute attribute in multivaluedAttributes)
        //    {
        //        var normalizedName = attribute.Name.Split('-').FirstOrDefault();

        //        if (fieldNameTemp == normalizedName || fieldNameTemp == "")
        //        {
        //            tempMultivaluedAttributes.Add(attribute);
        //        }
        //        else
        //        {
        //            foreach (IAttribute attr in tempMultivaluedAttributes)
        //            {


        //                FilterElementList filtersForAttribute = GetFiltersForMultivalueAttribute(attr.Name);
        //                if (filtersForAttribute != null && DoesValueSatisfyFilters(attr.Value, filtersForAttribute))
        //                {
        //                    checkIfNeedToReturn.Add(true);
        //                    break;
        //                }
        //                else
        //                {
        //                    checkIfNeedToReturn.Add(false);
        //                }
        //            }

        //            tempMultivaluedAttributes.Clear();
        //            tempMultivaluedAttributes.Add(attribute);
        //        }
        //        fieldNameTemp = normalizedName;

        //    }
        //    var checkORCondition = _Filters.Any(t => t.Value.Any(a => a.Condition == FilterCondition.OR));
        //    if (checkORCondition)
        //    {
        //        foreach (IAttribute attribute in singleAttributes)
        //        {
        //            if (_Filters.ContainsKey(attribute.Name))
        //            {
        //                FilterElementList filtersForAttribute = GetFiltersForAttribute(attribute.Name);
        //                if (filtersForAttribute != null && DoesValueSatisfyFilters(attribute.Value, filtersForAttribute))
        //                {
        //                    checkIfNeedToReturn.Add(true);
        //                    break;
        //                }
        //                else
        //                {
        //                    checkIfNeedToReturn.Add(false);
        //                }
        //            }
        //        }
        //        if (!checkIfNeedToReturn.Contains(true) && checkIfNeedToReturn.Count > 0) return false;
        //    }
        //    else
        //    {
        //        if (_Filters.Keys.Count(t => !t.Contains("~")) == 0 && _Filters.Keys.Any(t => t.Contains("~")))
        //        {
        //            if (!checkIfNeedToReturn.Contains(true) && checkIfNeedToReturn.Count > 0) return false;
        //        }
        //        //check if filter contains multivalue and there is not any true 
        //        //because we are in AND condition both multivalue and single value must be tru in order to continue
        //        else if (_Filters.Keys.Any(t => t.Contains("~")) && !checkIfNeedToReturn.Contains(true))
        //        {
        //            return false;
        //        }

        //        //if a field is used in filter but do not exist in instance return false because it is NULL in db
        //        foreach (var filter in _Filters)
        //        {
        //            if (!instance.Attributes.Any(t=>t.Name == filter.Key))
        //            {
        //                return false;
        //            }
        //        }
        //        foreach (IAttribute attribute in instance.Attributes)
        //        {
        //            FilterElementList filtersForAttribute = GetFiltersForAttribute(attribute.Name);
        //            if (filtersForAttribute != null && !DoesValueSatisfyFilters(attribute.Value, filtersForAttribute))
        //                return false;

        //        }
        //    }


        //    List<bool> checkFilterAttributes = new List<bool>();
        //    foreach (var filter in _Filters)
        //    {
        //        var attribute = instance.FindAttribute(filter.Key);
        //        var isFilterMultivaluedField = filter.Key.Contains("~");
        //        if (isFilterMultivaluedField && attribute == null)
        //        {
        //            var normalizedFilterName = filter.Key.Split('-').FirstOrDefault();
        //            attribute = instance.Attributes.FirstOrDefault(t => t.Name.StartsWith(normalizedFilterName));
        //        }

        //        if (attribute == null)
        //        {
        //            if (filter.Value.Any(filterForAttribute => filterForAttribute.Type != FilterType.NotEqual))
        //            {
        //                checkFilterAttributes.Add(false);
        //            }
        //            else
        //            {
        //                checkFilterAttributes.Add(true);
        //            }
        //        }
        //        else checkFilterAttributes.Add(true);

        //    }

        //    return checkFilterAttributes.Any(t => true);
        //}

        /// <summary>
        /// Test the intance satisfy filters using limit property.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public FilterManagerSatisfyResult DoesInstanceSatisfyFilters(IValidataRecord instance)
        {
            if (_Filters == null)
                return new FilterManagerSatisfyResult(true, null);

            IEnumerable<IAttribute> multivaluedAttributes = instance.Attributes.Where(t => t.Name.Contains("~"));
            IEnumerable<IAttribute> singleAttributes = instance.Attributes.Where(t => !t.Name.Contains("~"));

            //FilterElementList filtersForAttribute = new FilterElementList();
            List<bool> checkIfNeedToReturn = new List<bool>();

            List<IAttribute> tempMultivaluedAttributes = new List<IAttribute>();
            var fieldNameTemp = "";

            //check in multivalued attributes
            foreach (IAttribute attribute in multivaluedAttributes)
            {
                var normalizedName = attribute.Name.Split('-').FirstOrDefault();

                if (fieldNameTemp == normalizedName || fieldNameTemp == "")
                {
                    tempMultivaluedAttributes.Add(attribute);
                }
                else
                {
                    foreach (IAttribute attr in tempMultivaluedAttributes)
                    {
                        FilterElementList filtersForAttribute = GetFiltersForMultivalueAttribute(attr.Name);

                        if (filtersForAttribute != null && DoesValueSatisfyFilters(attr.Value, filtersForAttribute))
                        {
                            checkIfNeedToReturn.Add(true);
                            break;
                        } else {
                            checkIfNeedToReturn.Add(false);
                        }
                    }

                    tempMultivaluedAttributes.Clear();
                    tempMultivaluedAttributes.Add(attribute);
                }

                fieldNameTemp = normalizedName;
            }

            var checkORCondition = _Filters.Any(t => t.Value.Any(a => a.Condition == FilterCondition.OR));

            if (checkORCondition)
            {
                foreach (IAttribute attribute in singleAttributes)
                {
                    if (_Filters.ContainsKey(attribute.Name))
                    {
                        FilterElementList filtersForAttribute = GetFiltersForAttribute(attribute.Name);
                        if (filtersForAttribute != null && DoesValueSatisfyFilters(attribute.Value, filtersForAttribute))
                        {
                            checkIfNeedToReturn.Add(true);
                            break;
                        }
                        else
                        {
                            checkIfNeedToReturn.Add(false);
                        }
                    }
                }
                if (!checkIfNeedToReturn.Contains(true) && checkIfNeedToReturn.Count > 0)
                {
                    return new FilterManagerSatisfyResult(false, null);
                }
            }
            else
            {
                if (_Filters.Keys.Count(t => !t.Contains("~")) == 0 && _Filters.Keys.Any(t => t.Contains("~")))
                {
                    if (!checkIfNeedToReturn.Contains(true) && checkIfNeedToReturn.Count > 0)
                    {
                        return new FilterManagerSatisfyResult(false, null);
                    }
                }
                //check if filter contains multivalue and there is not any true 
                //because we are in AND condition both multivalue and single value must be tru in order to continue
                else if (_Filters.Keys.Any(t => t.Contains("~")) && !checkIfNeedToReturn.Contains(true))
                {
                    return new FilterManagerSatisfyResult(false, null);
                }

                //if a field is used in filter but do not exist in instance return false because it is NULL in db
                foreach (var filter in _Filters)
                {
                    if (!instance.Attributes.Any(t => t.Name == filter.Key))
                    {
                        return new FilterManagerSatisfyResult(false, null);
                    }
                }
                foreach (IAttribute attribute in instance.Attributes)
                {

                    FilterElementList filtersForAttribute = GetFiltersForAttribute(attribute.Name);
                    if (filtersForAttribute != null && !DoesValueSatisfyFilters(attribute.Value, filtersForAttribute))
                    {
                        return new FilterManagerSatisfyResult(false, null);
                    }

                }
            }
            var checkFilterAttributes = new List<FilterManagerSatisfyResult>();

            foreach (var filter in _Filters)
            {
                var attribute = instance.FindAttribute(filter.Key);
                var isFilterMultivaluedField = filter.Key.Contains("~");

                if (isFilterMultivaluedField && attribute == null)
                {
                    var normalizedFilterName = filter.Key.Split('-').FirstOrDefault();
                    attribute = instance.Attributes.FirstOrDefault(t => t.Name.StartsWith(normalizedFilterName));
                }

                if (attribute == null)
                {
                    if (filter.Value.Any(filterForAttribute => filterForAttribute.Type != FilterType.NotEqual))
                    {
                        checkFilterAttributes.Add(new FilterManagerSatisfyResult(false, null));
                    }
                    else
                    {
                        checkFilterAttributes.Add(new FilterManagerSatisfyResult(true, filter.Value.First(filterForAttribute => filterForAttribute.Type == FilterType.NotEqual)));
                    }
                }
                else
                {
                    foreach (var item in filter.Value)
                    {
                        if (item.Type == FilterType.Equal)
                        {
                            if (item.AttributeName == attribute.Name && item.AttributeFilter == attribute.Value)
                            {
                                return new FilterManagerSatisfyResult(true, item);
                            }
                        }
                        else if (item.Type == FilterType.Like)
                        {
                            if (item.AttributeName == attribute.Name && item.AttributeFilter.Contains(attribute.Value))
                            {
                                return new FilterManagerSatisfyResult(true, item);
                            }
                        }
                        else if (item.Type == FilterType.NotLike)
                        {
                            if (item.AttributeName == attribute.Name && !item.AttributeFilter.Contains(attribute.Value))
                            {
                                return new FilterManagerSatisfyResult(true, item);
                            }
                        }
                        else if (item.Type == FilterType.NotEqual)
                        {
                            if (item.AttributeName == attribute.Name && item.AttributeFilter != attribute.Value)
                            {
                                return new FilterManagerSatisfyResult(true, item);
                            }
                        }
                        else
                        {
                            if (item.AttributeName == attribute.Name && item.AttributeFilter == attribute.Value)
                            {
                                return new FilterManagerSatisfyResult(true, item);
                            }
                        }

                    }

                    //var matchingFitler = filter.Value
                    //    .Select(filterItem => new FilterElementList { filterItem })
                    //    .First(filterElementList => DoesValueSatisfyFilters(attribute.Value, filterElementList));

                    //checkFilterAttributes.Add(new FilterManagerSatisfyResult(true, matchingFitler.First()));
                }

            }

            if (checkFilterAttributes.Any(t => true))
            {
                return new FilterManagerSatisfyResult(true, checkFilterAttributes.First().Filter);
            }

            return new FilterManagerSatisfyResult(false, null);
        }

        /// <summary>
        /// Checks if the value satisfy filters.
        /// </summary>
        /// <param name="attrValue">The attr value.</param>
        /// <param name="filters">The filters.</param>
        /// <returns></returns>
        private static bool DoesValueSatisfyFilters(string attrValue, FilterElementList filters)
        {
            List<bool> checkIfNeedToReturn = new List<bool>();
            /////sort filters//////
            var sortedFilters = new FilterElementList();
           
            foreach (var item in filters)
            {
                if (!item.TestValue.StartsWith("AdvanceFilter"))
                {
                    sortedFilters.Add(item);
                }
            }
            ////////
            if (sortedFilters.Select(t => t.Condition).Contains(FilterCondition.OR))
            {
                foreach (FilterElement ft in sortedFilters)
                {
                        if (!ft.Test(attrValue))
                        {
                            checkIfNeedToReturn.Add(false);
                        }
                        else
                        {
                            checkIfNeedToReturn.Add(true);
                        }
                }

                if (!checkIfNeedToReturn.Contains(true) && checkIfNeedToReturn.Count > 0) return false;
            }
            else
            {
                foreach (FilterElement ft in sortedFilters)
                {
                    if (!ft.Test(attrValue))
                        return false;
                }
            }

            return true;
        }

        private FilterElementList GetFiltersForAttribute(string attributeName)
        {
           return _Filters.ContainsKey(attributeName) && _Filters[attributeName] != null
              ? _Filters[attributeName]
              : null;
        }

        private FilterElementList GetFiltersForMultivalueAttribute(string attributeName)
        {
            FilterElementList selectedFilter = new FilterElementList();
            var normalizedName = attributeName.Split('-').FirstOrDefault();
            foreach (var item in _Filters)
            {
                var normalizedFilterName = item.Key.Split('-').FirstOrDefault();
                if (normalizedName == normalizedFilterName)
                {
                    selectedFilter = item.Value;
                }  
            }
            return selectedFilter.Count > 0 ? selectedFilter : null;
         //   var filters = _Filters.Where(t => t.Key.Contains("~"));
         //   var list = filters.SelectMany(s => s.Key.Split('-')).Where(t => !t.Contains("~")).ToArray();
         //   return list.Contains(normalizedName) ? selectedFilter : null;
        }


        private FilterElementList GetFiltersForMultiValueAttribute(string attributeName)
        {
            return _Filters.ContainsKey(attributeName) && _Filters[attributeName] != null
                 ? _Filters[attributeName]
                 : null;
        }

        /// <summary>
        /// Gets the filters, grouped by attribute (relies on the asssumptions that there is only one typical)
        /// </summary>
        /// <param name="filters">Filtering constraints</param>
        /// <returns></returns>
        private static Dictionary<string, FilterElementList> GetFiltersGroupedByAttribute(IEnumerable<IFilteringConstraint> filters)
        {
            var result = new Dictionary<string, FilterElementList>();

            foreach (IFilteringConstraint filter in filters)
            {
                //It is not real filter
                if (filter.Operator == FilterType.TemplateName || filter.TestValue.StartsWith("AdvanceFilter"))
                    continue;

                if (!result.ContainsKey(filter.AttributeName))
                    result[filter.AttributeName] = new FilterElementList();

                result[filter.AttributeName].AddFilter(filter);
            }
            
            return result;
        }
    }

    public sealed class FilterManagerSatisfyResult
    {
        public FilterManagerSatisfyResult(bool satisfies, FilterElement filter)
        {
            Satisfies = satisfies;
            Filter = filter;
        }

        public bool Satisfies { get; private set; }

        public FilterElement Filter { get; private set; }
    }
}