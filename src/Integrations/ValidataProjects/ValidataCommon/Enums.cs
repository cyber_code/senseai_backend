using System;
namespace ValidataCommon
{
    /// <summary>
    /// Validata Common Enumerations
    /// </summary>
    public class Enums
    {
        /// <summary>
        /// Execution status enumeration
        /// </summary>
        public enum ExecutionStatus
        {
            /// <summary>
            /// 'Not Issued' Test Suite Status
            /// </summary>
            NotIssued = 0,

            /// <summary>
            /// 'Not Run' Test Suite Status
            /// </summary>
            NotRun = 1,

            /// <summary>
            /// 'Passed' Test Suite Status
            /// </summary>
            Passed = 2,

            /// <summary>
            /// 'Failed' Test Suite Status
            /// </summary>
            Failed = 3,

            /// <summary>
            /// 'Unable to Run' Test Suite Status
            /// </summary>
            UnableToRun = 4,

            /// <summary>
            /// 'Deferred' Test Suite Status
            /// </summary>
            Deferred = 5,

            /// <summary>
            /// 'In progress' Test Suite Status
            /// </summary>
            InProgress = 6,

            /// <summary>
            /// 'Scheduled' Test Suite Status 
            /// </summary>
            Scheduled = 7,

            /// <summary>
            /// 'Waiting' Test Suite Status 
            /// </summary>
            Waiting = 8,

            /// <summary>
            /// 'FailedWithRandomIssue' Test Case Status 
            /// </summary>
            FailedWithRandomIssue = 9
        }

        /// <summary>
        /// Test step status enumeration
        /// </summary>
        public enum TestStepStatus
        {
            /// <summary>
            /// 'Not selected' Test Step Status
            /// </summary>
            NotSelected = 0,

            /// <summary>
            /// 'Not Run' Test Step Status
            /// </summary>
            NotRun = 1,

            /// <summary>
            /// 'Not Possible'  Test Step Status
            /// </summary>
            NotPossible = 2,

            /// <summary>
            /// 'Passed' Test Step Status
            /// </summary>
            Passed = 3,

            /// <summary>
            /// 'Failed' Test Step Status
            /// </summary>
            Failed = 4,

            /// <summary>
            /// 'Deferred' Test Step Status
            /// </summary>
            Deferred = 5,

            /// <summary>
            /// Waiting for execution Test Step status
            /// </summary>
            Waiting = 6,

            /// <summary>
            /// Automated Execution Is in Progress Test Step status
            /// </summary>
            InProgress = 7,
        }

        /// <summary>
        /// Test step priority enumeration
        /// </summary>
        public enum TestStepPriority
        {
            /// <summary>
            /// 'Low' Test Step Priority
            /// </summary>
            Low = 1,

            /// <summary>
            /// 'Medium' Test Step Priority
            /// </summary>
            Medium = 2,

            /// <summary>
            /// 'High' Test Step Priority
            /// </summary>
            High = 3,

            /// <summary>
            /// 'Critical' Test Step Priority
            /// </summary>
            Critical = 4
        }

        /// <summary>
        /// Test Step "Type" attribute
        /// </summary>
        public enum TestStepType
        {
            /// <summary>
            /// "Action" step
            /// </summary>
            Action = 1,

            /// <summary>
            /// "Check" step
            /// </summary>
            Check = 2,

            /// <summary>
            /// "Login" step
            /// </summary>
            Login = 3
        }

        /// <summary>
        /// Error codes
        /// </summary>
        public enum ErrorCodes
        {
            /// <summary>
            /// Successful execution
            /// </summary>
            Success = 0,

            /// <summary>
            /// Unknown error occured
            /// </summary>
            Unknown,

            /// <summary>
            /// Invalid Command Line
            /// </summary>
            InvalidCommandLine,

            /// <summary>
            /// Invalid Command Line
            /// </summary>
            InvalidVirtualData,

            /// <summary>
            /// Missing Configuration File
            /// </summary>
            MissingConfigurationFile,

            /// <summary>
            /// Incomplete/incompatible Configuration File
            /// </summary>
            UncompatibleConfigurationFile,

            /// <summary>
            /// File Access Error
            /// </summary>
            FileAccessError,

            /// <summary>
            /// Data Retrieval Error
            /// </summary>
            DataRetrievalError,

            /// <summary>
            /// Execution Entry Point Not Found
            /// </summary>
            ExecutionEntryPointNotFound,

            /// <summary>
            /// DataSet Flush Error
            /// </summary>
            DataSetFlushError,

            /// <summary>
            /// Mapping Engine Error
            /// </summary>
            MappingEngineError,

            /// <summary>
            /// Mapping Schema Not Found
            /// </summary>
            MappingSchemaNotFound,

            /// <summary>
            /// Script Parser Error
            /// </summary>
            ScriptParserError,

            /// <summary>
            /// Functional Adapter Error
            /// </summary>
            FunctionalAdapterError,

            /// <summary>
            /// Adapter Returned Errors
            /// </summary>
            AdapterReturnedErrors,

            /// <summary>
            /// Wrong Adapter Gateway Path
            /// </summary>
            AdapterGatewayPathError,

            /// <summary>
            /// Automatic Calculation Error
            /// </summary>
            AutomaticCalculationError,

            /// <summary>
            /// Unsupported adapter type
            /// </summary>
            UnsupportedAdapter,

            /// <summary>
            /// Validation or transformation error
            /// </summary>
            MigrationOperationError,

            /// <summary>
            /// Check all login details provided - project, subproject, discipline, role, user and pass
            /// </summary>
            InvalidLoginDetails,

            /// <summary>
            /// Timeout when executing external application call (used for VConnReportViewer)
            /// </summary>
            ExternalProcessTimeoutError,

            /// <summary>
            /// The latest operation returned fatal error that requires aborting the whole process
            /// </summary>
            AbortOperation,

            /// <summary>
            /// Adapter call failed because of unexpected(random or infrastructure related) reason.
            /// </summary>
            FailedWithRandomIssue
        }

        /// <summary>
        /// Execution mode for test suites and steps
        /// </summary>
        public enum ExecutionMode
        {
            /// <summary>
            /// Functional testing
            /// </summary>
            Functional = 0,

            /// <summary>
            /// Performance testing
            /// </summary>
            Performance = 1,
        }
        public enum ExecutionAdapterMode
        {
            /// <summary>
            /// Execute with real adapter
            /// </summary>
            Real = 0,
            /// <summary>
            /// Execute with virtual adapter
            /// </summary>
            Virtual = 1,
            /// <summary>
            /// Manual execute
            /// </summary>
            Manual = 2
        }
        /// <summary>
        /// Defines whether to add any new attribute located, or only extra mv/sv to already existing attributes
        /// </summary>
        public enum AutoUpdateTypicalsMode { MvSvOnly, Any, None }

        /// <summary>
        /// Possible values for logging actions
        /// </summary>
        public enum LogAction
        {
            /// <summary>
            /// No action
            /// </summary>
            NotSet = 0,

            /// <summary>
            /// Log PK violation
            /// </summary>
            PKViolation = 1,

            /// <summary>
            /// Log FK violation
            /// </summary>
            FKViolation = 2,

            /// <summary>
            /// Log data loss 
            /// </summary>
            DataLoss = 3,

            /// <summary>
            /// Some value in PK is null
            /// </summary>
            PKNullValue = 4,

            /// <summary>
            /// PodNum value is problematic
            /// </summary>
            PodNumValue = 6,

            /// <summary>
            /// There is no match for the transaction credit-debit pair
            /// </summary>
            TransactionMissingContraTransaction = 7,

            /// <summary>
            /// The reciprocal transaction has no data for country
            /// </summary>
            TransactionMissingContraCountry = 8,

            /// <summary>
            /// The transaction has a internal account (no real customer) or is paired with such an internal transaction
            /// </summary>
            TransactionPartOfInternalOperation = 9,

            /// <summary>
            /// The instance was ignored for export (not referenced by transaction)
            /// </summary>
            InstanceIgnored = 11,

            /// <summary>
            /// The transaction is reversed (annulated) or its reciprocal is reversed
            /// </summary>
            TransactionPartOfReversedCouple = 12,
        }

        /// <summary>
        /// Wizard Mode
        /// </summary>
        public enum WizardMode
        {
            /// <summary>
            /// Standard Mode
            /// </summary>
            Normal = 0,

            /// <summary>
            /// Scheduled Task Definition Mode
            /// </summary>
            ScheduledTaskDefinition = 1
        }

        /// <summary>
        /// Test step execution type
        /// </summary>
        public enum ExecutionType
        {
            /// <summary>
            /// Manual executable test step
            /// </summary>
            Manual = 1,

            /// <summary>
            /// Automatic executable test step
            /// </summary>
            Automatic = 2
        }

        /// <summary>
        /// Cycle/Job execution type
        /// </summary>
        public enum ExecuteAs
        {
            /// <summary>
            /// Functional Type
            /// </summary>
            Functional,

            /// <summary>
            /// Performance Type
            /// </summary>
            Performance
        }
    }

    /// <summary>
    /// Defines the deployment conflict resolution behavior for fields which are empty (missing) in SAS, 
    /// but have values in the target environment
    /// </summary>
    public enum EmptyValueDeploymentHandling
    {
        /// <summary>
        /// The default option will be used (defined on the record level)
        /// </summary>
        Default,

        /// <summary>
        /// The value will be deleted in the environment using - or NULL
        /// </summary>
        DeleteIfEmpty,

        /// <summary>
        /// The value will not be deployed
        /// </summary>
        SkipIfEmpty
    }

    /// <summary>
    /// Defines the deployment behavior for fields that have identical values in SAS Client and the target environment
    /// </summary>
    public enum UnchangedValueDeploymentHandling
    {
        /// <summary>
        /// The default option will be used
        /// </summary>
        Default,

        /// <summary>
        ///The field will NOT be included in the deployment
        /// </summary>
        SkipIfNotChanged,

        /// <summary>
        /// The field will be included in the deployment
        /// </summary>
        InputIfNotChanged
    }

    [System.Serializable]
    public enum ScreenshotLevel
    {
        None,
        Error,
        Full
    }

    [System.Serializable]
    public enum ScreenshotWindowLevel
    {
        All,
        AllExceptFirst,
        First,
        Last        
    }

    public enum TestStepUserInterfaceType
    {
        Classic,
        T24TestBuilder,
        SwiftBuilder
    }

    public enum ACTargetDataType : int
    {
        TestData = 0,
        VirtualData = 1,
        ExpectedResult = 2
    }

    public enum MethodType
    {
        Get,
        Post,
        Put,
        Patch,
        Delete

    }

    public static class TestStepUserInterfaceTypeExtensions
    {
        public static string GetAttributeValue(this TestStepUserInterfaceType t)
        {
            switch (t)
            {
                case TestStepUserInterfaceType.Classic:
                    return "TestTreeManager";
                case TestStepUserInterfaceType.T24TestBuilder:
                    return "T24TestBuilder";
                case TestStepUserInterfaceType.SwiftBuilder:
                    return "SwiftBuilder";
                default:
                    throw new System.NotImplementedException("Not implemented for user interface type: " + t.ToString());
            }
        }
    }

    [Flags]
    public enum CycleExecutionAbortTrigger
    {
        /// <summary>
        /// Execute all test steps of the test suite, regardless whether a step has failed
        /// </summary>
        None = 0,

        /// <summary>
        /// When a test step fails with an unexepected error (i.e. could not complete the specified action or unexpected exception occurred)
        /// </summary>
        UnexpectedFailure = 1 << 0,

        /// <summary>
        /// When a check step fails due not matching rules, stop the test suite execution
        /// </summary>
        FailedComparisonInCheckStep = 1 << 1, // 2
    }

    public enum ExecutionCondition
    {
        NoRegression = 0,
        SingleRequirementReadyForRegression,
        AllRequirementsReadyForRegression
    }
}