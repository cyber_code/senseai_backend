﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ValidataCommon
{
    ///<summary> Helper for Binary Serialization
    ///</summary>
    public static class BinarySerializationHelper
    {
        ///<summary>Converts an object to byte array
        ///</summary>
        ///<param name="obj"></param>
        ///<returns></returns>
        public static byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
            {
                return null;
            }

            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        ///<summary>Convert a byte array to an Object
        ///</summary>
        ///<param name="arrBytes"></param>
        ///<returns></returns>
        public static object ByteArrayToObject(byte[] arrBytes)
        {
            if (arrBytes == null)
            {
                return null;
            }

            BinaryFormatter binForm = new BinaryFormatter();
            using (MemoryStream memStream = new MemoryStream())
            {
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                return binForm.Deserialize(memStream);
            }
        }

        /// <summary>
        /// Deserializes an object from binary file.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="filename">The filename.</param>
        /// <returns></returns>
        public static object DeserializeFromBinaryFile(Type type, string filename)
        {
            if (!File.Exists(filename))
            {
                throw new IOException(string.Format("File '{0}' does not exist", filename));
            }

            BinaryFormatter binForm = new BinaryFormatter();

            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                try
                {
                    return binForm.Deserialize(stream);
                }
                catch (Exception ex)
                {
                    // The serialization error messages are cryptic at best.Give a hint at what happened
                    throw new InvalidOperationException(string.Format("Failed to create object from '{0}'", filename), ex);
                }
            }
        }

        /// <summary>
        /// Serializes to Binary file.
        /// </summary>
        /// <param name="objectToSerialize">object to serialize</param>
        /// <param name="filename">The filename.</param>
        public static void SerializeToBinaryFile(object objectToSerialize, string filename)
        {
            BinaryFormatter bf = new BinaryFormatter();

            #region Check Directory
            {
                string dir = Path.GetDirectoryName(filename);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            }
            #endregion

            using (FileStream stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                bf.Serialize(stream, objectToSerialize);
                stream.Flush();
                stream.Close();
            }
        }   
    }
}