﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    public partial class StringMethods
    {
        public class Path
        {
            /// <summary>
            /// Compares string 'str' against list of masks 'mask'
            /// </summary>
            /// <param name="str">String to match</param>
            /// <param name="mask">List of masks separated by ';'</param>
            /// <param name="ignoreCase">Ignore Case</param>
            /// <returns>'true' is match, 'false' otherwise</returns>
            public static bool CompareWildcards(string str, string mask, bool ignoreCase)
            {
                if (string.IsNullOrEmpty(mask))
                {
                    return false;
                }

                if (mask == "*")
                {
                    return true;
                }

                string[] arrMasks = mask.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                return CompareWildcards(str, arrMasks, ignoreCase);
            }

            /// <summary>
            /// Compares string 'str' against list of masks
            /// </summary>
            /// <param name="str">String to compare</param>
            /// <param name="masks">Array of masks</param>
            /// <param name="ignoreCase">Ignore Case</param>
            /// <returns>'true' is match, 'false' otherwise</returns>
            public static bool CompareWildcards(string str, string[] masks, bool ignoreCase)
            {
                if (masks == null || masks.Length == 0)
                {
                    return false;
                }

                foreach (string m in masks)
                {
                    if (CompareWildcard(str, m, ignoreCase))
                    {
                        return true;
                    }
                }

                return false;
            }

            /// <summary>
            /// Compares string 'str' against single wild card mask
            /// </summary>
            /// <param name="str">String to compare</param>
            /// <param name="mask">Masks</param>
            /// <param name="ignoreCase">Ignore Case</param>
            /// <returns>'true' is match, 'false' otherwise</returns>
            public static bool CompareWildcard(string str, string mask, bool ignoreCase)
            {
                if (str == null)
                {
                    return false;
                }

                if (str == string.Empty && mask == string.Empty)
                {
                    return true;
                }

                if (string.IsNullOrEmpty(mask))
                {
                    return false;
                }

                mask = mask.Trim();

                int i = 0;
                int k = 0;

                while (k < str.Length && i < mask.Length)
                {
                    switch (mask[i])
                    {
                        case '*':
                            while (k < str.Length)
                            {
                                if (CompareWildcard(str.Substring(k + 1), mask.Substring(i + 1), ignoreCase))
                                {
                                    return true;
                                }

                                k++;
                            }

                            return false;

                        case '?':
                            break;

                        default:
                            if (!ignoreCase && str[k] != mask[i])
                            {
                                return false;
                            }
                            else if (ignoreCase && Char.ToLower(str[k]) != Char.ToLower(mask[i]))
                            {
                                return false;
                            }

                            break;
                    }

                    k++;
                    i++;
                }

                if (k == str.Length)
                if (i == mask.Length || mask[i] == ';' || mask[i] == '*')
                {
                    return true;
                }

                return false;
            }
        }
    }
}
