﻿namespace ValidataCommon
{
    /// <summary>
    /// Analyzes T24 file names
    /// </summary>
    public static class T24FilesAnalyzer
    {
        /// <summary>
        /// Determines whether [is dollar file] [the specified file name].
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        /// 	<c>true</c> if [is dollar file] [the specified file name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsDollarFile(string fileName)
        {
            return fileName.StartsWith("$");
        }

        /// <summary>
        /// Gets the routine name from dollar file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public static string GetRoutineNameFromDollarFile(string fileName)
        {
            if (! IsDollarFile(fileName))
            {
                return null;
            }

            return fileName.Substring(1);
        }
    }
}