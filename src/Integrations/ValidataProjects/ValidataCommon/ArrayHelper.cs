﻿using System.Collections.Generic;
namespace ValidataCommon
{
    /// <summary>
    /// Helper function for arrays
    /// </summary>
    public static class ArrayHelper
    {
        /// <summary>
        /// Checks if the arrays the same.
        /// </summary>
        /// <param name="array1">The array1.</param>
        /// <param name="array2">The array2.</param>
        /// <returns></returns>
        public static bool AreArraysTheSame(string[] array1, string[] array2)
        {
            if (array1 == array2)
            {
                return true;
            }

            if (array1 == null || array2 == null)
            {
                return false;
            }

            if (array1.Length != array2.Length)
            {
                return false;
            }

            for (int i = 0; i < array1.Length; i++)
            {
                if (array1[i] != array2[i])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the generic arrays are same.
        /// </summary>
        /// <param name="array1">The array1.</param>
        /// <param name="array2">The array2.</param>
        /// <returns></returns>
        public static bool AreArraysTheSame<T>(T[] array1, T[] array2)
        {
            if (ReferenceEquals(array1, array2))
            {
                return true;
            }

            if (array1 == null || array2 == null)
            {
                return false;
            }

            if (array1.Length != array2.Length)
            {
                return false;
            }

            EqualityComparer<T> comparer = EqualityComparer<T>.Default;
            for (int i = 0; i < array1.Length; i++)
            {
                if (!comparer.Equals(array1[i], array2[i]))
                {
                    return false;
                }
            }

            return true;
        }
    }
}