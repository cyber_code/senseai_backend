﻿namespace ValidataCommon
{
    public interface IFileTransfer
    {
        bool Upload(string inputFilePath, string targetFilePath, out string errorMessage);
        bool Download(string localFilePath, string targetFilePath, out string errorMessage);
    }
}
