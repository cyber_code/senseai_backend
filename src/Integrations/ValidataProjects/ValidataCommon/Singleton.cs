﻿namespace ValidataCommon
{
    /// <summary>
    /// Generic singleton pattern
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton <T> where T : class, new()
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Singleton&lt;T&gt;"/> class.
        /// </summary>
        public Singleton() {}

        private class SingletonCreator
        {
            /// <summary>
            /// Initializes the <see cref="Singleton&lt;T&gt;.SingletonCreator"/> class.
            /// </summary>
            static SingletonCreator() {}
            // Private object instantiated with private constructor
            internal static readonly T instance = new T();
        }

        /// <summary>
        /// Gets the unique instance.
        /// </summary>
        /// <value>The unique instance.</value>
        public static T UniqueInstance
        {
            get { return SingletonCreator.instance; }
        }
    }
}