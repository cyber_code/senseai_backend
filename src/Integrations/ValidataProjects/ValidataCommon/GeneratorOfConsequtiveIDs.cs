using System;

namespace ValidataCommon
{
    /// <summary>
    /// Generator of consequitive IDs
    /// </summary>
    public class GeneratorOfConsequtiveIDs
    {
        /// <summary>
        /// Gets the next ID.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public static string GetNextID(string id)
        {
            int nonDigitPosition = FindFirstNonDigitReverse(id);
            if (nonDigitPosition == id.Length - 1)
            {
                // the ID does nto end with a numbers
                return id + "1";
            }
            else if (nonDigitPosition < 0)
            {
                // the ID is numeric
                return (Convert.ToUInt64(id) + 1).ToString();
            }
            else
            {
                // there is a nonNumeric and numeric part
                string prefix = id.Substring(0, nonDigitPosition + 1);
                string numericSuffix = id.Substring(nonDigitPosition + 1, id.Length - nonDigitPosition - 1);
                string newSuffix = (Convert.ToUInt64(numericSuffix) + 1).ToString();
                return prefix + newSuffix;
            }
        }

        /// <summary>
        /// Finds the first non digit starting from the end.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        private static int FindFirstNonDigitReverse(string id)
        {
            for (int i = id.Length - 1; i >= 0; i--)
            {
                char c = id[i];
                if (!Char.IsDigit(c))
                {
                    return i;
                }
            }

            return -1;
        }
    }
}