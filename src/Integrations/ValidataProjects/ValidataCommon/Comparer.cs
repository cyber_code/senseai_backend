﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Validata.Common;
using System.Globalization;

namespace ValidataCommon
{
    /// <summary>
    /// Validata attribute value comparer
    /// </summary>
    public static class Comparer
    {
        /// <summary>
        /// Default date time format
        /// </summary>
        public const string DefaultDateTimeFormat = "dd/MM/yyyy HH:mm:ss";

        /// <summary>
        /// Default date format
        /// </summary>
        public const string DefaultDateFormat = "dd/MM/yyyy";

        /// <summary>
        /// Default time format
        /// </summary>
        public const string DefaultTimeFormat = "HH:mm:ss";

        private static readonly IFormatProvider _CultureInfo = CultureInfo.InvariantCulture;

        /// <summary>
        /// Compare date time attribute values
        /// </summary>
        /// <param name="expValue">Expected Value</param>
        /// <param name="actValue">Actual Value</param>
        /// <param name="comparisonOperator">Comparison Operator</param>
        /// <param name="compareDateComponent">Whether to compare Date part of the values</param>
        /// <param name="compareTimeComponent">Whether to compare Time part of the values</param>
        /// <returns>Comparison result</returns>
        public static ShortComparisonResult CompareDateTimeValues(string expValue, string actValue, ComparisonOperator comparisonOperator
            , bool compareDateComponent = true, bool compareTimeComponent = true)
        {
            if (!compareDateComponent && !compareTimeComponent)
                return ShortComparisonResult.GetFailed("Invalid date time comparison - neither A nor B have been specified for comparison");

            DateTime expDateTimeValue, actDateTimeValue;
            if (!TryParseDateTime(expValue, out expDateTimeValue) || !TryParseDateTime(actValue, out actDateTimeValue))
                return ShortComparisonResult.GetFailed("Either the expected or the actual value are not valid date time objects");

            if (!compareDateComponent)
            {
                expDateTimeValue = DateTime.MinValue.Add(expDateTimeValue.TimeOfDay);
                actDateTimeValue = DateTime.MinValue.Add(actDateTimeValue.TimeOfDay);
                
            }
            else if (!compareTimeComponent)
            {
                expDateTimeValue = expDateTimeValue.Date;
                actDateTimeValue = actDateTimeValue.Date;
            }

            return CompareDateTimeValues(comparisonOperator, expDateTimeValue, actDateTimeValue);
        }

        /// <summary>
        /// Compare attribute values
        /// </summary>
        /// <param name="dataType">Comparison Data Type</param>
        /// <param name="expValue">Expected Value</param>
        /// <param name="actValue">Actual Value</param>
        /// <param name="comparisonOperator">Comparison Operator</param>
        /// <param name="tolerance">Tolerance</param>
        /// <returns>Comparison result</returns>
        public static ShortComparisonResult CompareValues(AttributeDataType dataType, string expValue, string actValue, ComparisonOperator comparisonOperator, double tolerance = 0)
        {
            if (comparisonOperator == ComparisonOperator.Equal
                && expValue == actValue
                && actValue == "No Value")
            {
                return new ShortComparisonResult(ComparisonOutcome.Success, string.Empty);
            }

            switch (dataType)
            {
                case AttributeDataType.String:
                    return CompareStringValues(comparisonOperator, expValue, actValue);

                case AttributeDataType.Integer:
                    int expIntValue;
                    int actIntValue;

                    if (int.TryParse(expValue, NumberStyles.Any, _CultureInfo, out expIntValue)
                        && int.TryParse(actValue, NumberStyles.Any, _CultureInfo, out actIntValue))
                    {
                        return CompareIntValues(comparisonOperator, expIntValue, actIntValue, tolerance);
                    }
                    else
                    {
                        return ShortComparisonResult.GetFailed("Either the expected or the actual value are not valid integer numbers");
                    }

                case AttributeDataType.Real:
                case AttributeDataType.Measure:
                    double expRealValue;
                    double actRealValue;

                    expValue = NormalizeDecimalValue(expValue);
                    actValue = NormalizeDecimalValue(actValue);

                    if (double.TryParse(expValue, NumberStyles.Any, _CultureInfo, out expRealValue)
                        && double.TryParse(actValue, NumberStyles.Any, _CultureInfo, out actRealValue))
                    {
                        return CompareDoubleValues(comparisonOperator, expRealValue, actRealValue, tolerance);
                    }
                    else
                    {
                        return ShortComparisonResult.GetFailed("Either the expected or the actual value are not valid real numbers");
                    }

                case AttributeDataType.Currency:
                    decimal expCurrencyValue;
                    decimal actCurrencyValue;

                    expValue = NormalizeDecimalValue(expValue);
                    actValue = NormalizeDecimalValue(actValue);

                    if (decimal.TryParse(expValue, NumberStyles.Any, _CultureInfo, out expCurrencyValue)
                        && decimal.TryParse(actValue, NumberStyles.Any, _CultureInfo, out actCurrencyValue))
                    {
                        return CompareDecimalValues(comparisonOperator, expCurrencyValue, actCurrencyValue, tolerance);
                    }
                    else
                    {
                        return ShortComparisonResult.GetFailed("Either the expected or the actual value are not valid decimal numbers");
                    }

                case AttributeDataType.DateTime:
                    return CompareDateTimeValues(expValue, actValue, comparisonOperator, true, true);

                case AttributeDataType.Boolean:
                    bool expBoolValue;
                    bool actBoolValue;

                    if (bool.TryParse(expValue, out expBoolValue)
                        && bool.TryParse(actValue, out actBoolValue))
                    {
                        return CompareBoolValues(comparisonOperator, expBoolValue, actBoolValue);
                    }
                    else
                    {
                        return ShortComparisonResult.GetFailed("Either the expected or the actual values are not valid booleans");
                    }

                case AttributeDataType.Blob:
                    return ShortComparisonResult.GetSkipped("BLOB values cannot be compared");

                default:
                    throw new ApplicationException("Unexpected data type for attribute");
            }
        }

        /// <summary>
        /// 	Replace comma with dot in case of comma is used for decimal point separator
        /// </summary>
        /// <param name="value"> </param>
        /// <returns> </returns>
        private static string NormalizeDecimalValue(string value)
        {
            value = value.Replace("T", "000").Replace("M", "000000").Replace("B", "000000000");
            if (value.Split(',').Length == 2 && value.LastIndexOf('.') == -1)
                return value.Replace(",", ".");

            return value;
        }

        /// <summary>
        /// 	Compares string values.
        /// </summary>
        /// <param name="comparisonOperator"> The comparison rule operator. </param>
        /// <param name="expValue"> The expected value. </param>
        /// <param name="actValue"> The actual value. </param>
        /// <returns> Comparison Result object </returns>
        private static ShortComparisonResult CompareStringValues(ComparisonOperator comparisonOperator, string expValue, string actValue)
        {
            ShortComparisonResult res = new ShortComparisonResult();

            switch (comparisonOperator)
            {
                case ComparisonOperator.GreaterThan:
                    res.SetSuccessOrFail(string.Compare(actValue, expValue) > 0);
                    break;

                case ComparisonOperator.GreaterThanOrEqual:
                    res.SetSuccessOrFail(string.Compare(actValue, expValue) >= 0);
                    break;

                case ComparisonOperator.Equal:
                    res.SetSuccessOrFail(actValue == expValue);
                    break;

                case ComparisonOperator.NotEqual:
                    res.SetSuccessOrFail(actValue != expValue);
                    break;

                case ComparisonOperator.LowerThan:
                    res.SetSuccessOrFail(string.Compare(actValue, expValue) < 0);
                    break;

                case ComparisonOperator.LowerThanOrEqual:
                    res.SetSuccessOrFail(string.Compare(actValue, expValue) <= 0);
                    break;

                case ComparisonOperator.Like:
                    res.SetSuccessOrFail(actValue.Contains(expValue));
                    break;

                case ComparisonOperator.StartsWith:
                    res.SetSuccessOrFail(actValue.StartsWith(expValue));
                    break;

                case ComparisonOperator.EndsWith:
                    res.SetSuccessOrFail(actValue.EndsWith(expValue));
                    break;
            }

            return res;
        }

        /// <summary>
        /// 	Compares integer values.
        /// </summary>
        /// <param name="comparisonOperator"> The comparison operator. </param>
        /// <param name="expValue"> The expected value. </param>
        /// <param name="actValue"> The actual value. </param>
        /// <param name="tolerance"> The tolerance used in comparison. </param>
        /// <returns> Comparison Result object </returns>
        private static ShortComparisonResult CompareIntValues(ComparisonOperator comparisonOperator, int expValue, int actValue, double tolerance)
        {
            ShortComparisonResult res = new ShortComparisonResult();

            switch (comparisonOperator)
            {
                case ComparisonOperator.GreaterThan:
                    res.SetSuccessOrFail(actValue > expValue);
                    break;

                case ComparisonOperator.GreaterThanOrEqual:
                    res.SetSuccessOrFail(actValue >= expValue);
                    break;

                case ComparisonOperator.Equal:
                    res.SetSuccessOrFail(AreIntegersTheSame(expValue, actValue, tolerance));
                    SaveToleranceDetails(res, tolerance);
                    break;

                case ComparisonOperator.NotEqual:
                    res.SetSuccessOrFail(!AreIntegersTheSame(expValue, actValue, tolerance));
                    SaveToleranceDetails(res, tolerance);
                    break;

                case ComparisonOperator.LowerThan:
                    res.SetSuccessOrFail(actValue < expValue);
                    break;

                case ComparisonOperator.LowerThanOrEqual:
                    res.SetSuccessOrFail(actValue <= expValue);
                    break;

                case ComparisonOperator.Like:
                    res.SetSuccessOrFail(actValue.ToString().Contains(expValue.ToString()));
                    break;

                case ComparisonOperator.StartsWith:
                    res.SetSuccessOrFail(actValue.ToString().StartsWith(expValue.ToString()));
                    break;

                case ComparisonOperator.EndsWith:
                    res.SetSuccessOrFail(actValue.ToString().EndsWith(expValue.ToString()));
                    break;
            }

            return res;
        }

        private static void SaveToleranceDetails(ShortComparisonResult res, double tolerance)
        {
            if (tolerance != 0)
            {
                res.Details = string.Format("Used tolerance of {0} when comparing values", tolerance);
            }
        }

        /// <summary>
        /// 	Compares double values.
        /// </summary>
        /// <param name="comparisonOperator"> The comparison operator. </param>
        /// <param name="expValue"> The expected value. </param>
        /// <param name="actValue"> The actual value. </param>
        /// <param name="tolerance"> The tolerance used in comparison. </param>
        /// <returns> Comparison Result object </returns>
        private static ShortComparisonResult CompareDoubleValues(ComparisonOperator comparisonOperator, double expValue, double actValue, double tolerance)
        {
            ShortComparisonResult res = new ShortComparisonResult();

            switch (comparisonOperator)
            {
                case ComparisonOperator.GreaterThan:
                    res.SetSuccessOrFail(actValue > expValue);
                    break;

                case ComparisonOperator.GreaterThanOrEqual:
                    res.SetSuccessOrFail(actValue >= expValue);
                    break;

                case ComparisonOperator.Equal:
                    res.SetSuccessOrFail(AreDoublesTheSame(expValue, actValue, tolerance));
                    SaveToleranceDetails(res, tolerance);
                    break;

                case ComparisonOperator.NotEqual:
                    res.SetSuccessOrFail(!AreDoublesTheSame(expValue, actValue, tolerance));
                    SaveToleranceDetails(res, tolerance);
                    break;

                case ComparisonOperator.LowerThan:
                    res.SetSuccessOrFail(actValue < expValue);
                    break;

                case ComparisonOperator.LowerThanOrEqual:
                    res.SetSuccessOrFail(actValue <= expValue);
                    break;

                case ComparisonOperator.Like:
                    res.SetSuccessOrFail(actValue.ToString().Contains(expValue.ToString()));
                    break;

                case ComparisonOperator.StartsWith:
                    res.SetSuccessOrFail(actValue.ToString().StartsWith(expValue.ToString()));
                    break;

                case ComparisonOperator.EndsWith:
                    res.SetSuccessOrFail(actValue.ToString().EndsWith(expValue.ToString()));
                    break;
            }

            return res;
        }

        /// <summary>
        /// 	Compares decimal values.
        /// </summary>
        /// <param name="comparisonOperator"> The comparison operator. </param>
        /// <param name="expValue"> The expected value. </param>
        /// <param name="actValue"> The actual value. </param>
        /// <param name="tolerance"> The tolerance. </param>
        /// <returns> Comparison Result object </returns>
        private static ShortComparisonResult CompareDecimalValues(ComparisonOperator comparisonOperator, decimal expValue, decimal actValue, double tolerance)
        {
            ShortComparisonResult res = new ShortComparisonResult();

            switch (comparisonOperator)
            {
                case ComparisonOperator.GreaterThan:
                    res.SetSuccessOrFail(actValue > expValue);
                    break;

                case ComparisonOperator.GreaterThanOrEqual:
                    res.SetSuccessOrFail(actValue >= expValue);
                    break;

                case ComparisonOperator.Equal:
                    res.SetSuccessOrFail(AreDecimalsTheSame(expValue, actValue, (decimal)tolerance));
                    SaveToleranceDetails(res, tolerance);
                    break;

                case ComparisonOperator.NotEqual:
                    res.SetSuccessOrFail(!AreDecimalsTheSame(expValue, actValue, (decimal)tolerance));
                    SaveToleranceDetails(res, tolerance);
                    break;

                case ComparisonOperator.LowerThan:
                    res.SetSuccessOrFail(actValue < expValue);
                    break;

                case ComparisonOperator.LowerThanOrEqual:
                    res.SetSuccessOrFail(actValue <= expValue);
                    break;

                case ComparisonOperator.Like:
                    res.SetSuccessOrFail(actValue.ToString().Contains(expValue.ToString()));
                    break;

                case ComparisonOperator.StartsWith:
                    res.SetSuccessOrFail(actValue.ToString().StartsWith(expValue.ToString()));
                    break;

                case ComparisonOperator.EndsWith:
                    res.SetSuccessOrFail(actValue.ToString().EndsWith(expValue.ToString()));
                    break;
            }

            return res;
        }

        /// <summary>
        /// 	Compares date time values.
        /// </summary>
        /// <param name="comparisonOperator"> The comparison operator. </param>
        /// <param name="expValue"> The expected value. </param>
        /// <param name="actValue"> The actual value. </param>
        /// <returns> Comparison Result object </returns>
        private static ShortComparisonResult CompareDateTimeValues(ComparisonOperator comparisonOperator, DateTime expValue, DateTime actValue)
        {
            ShortComparisonResult res = new ShortComparisonResult();

            switch (comparisonOperator)
            {
                case ComparisonOperator.GreaterThan:
                    res.SetSuccessOrFail(actValue > expValue);
                    break;

                case ComparisonOperator.GreaterThanOrEqual:
                    res.SetSuccessOrFail(actValue >= expValue);
                    break;

                case ComparisonOperator.Equal:
                    res.SetSuccessOrFail(actValue == expValue);
                    break;

                case ComparisonOperator.NotEqual:
                    res.SetSuccessOrFail(actValue != expValue);
                    break;

                case ComparisonOperator.LowerThan:
                    res.SetSuccessOrFail(actValue < expValue);
                    break;

                case ComparisonOperator.LowerThanOrEqual:
                    res.SetSuccessOrFail(actValue <= expValue);
                    break;

                case ComparisonOperator.Like:
                    res.SetSkipped();
                    res.Details = "'Like' operator not applicable for Date Time values, comparison skipped";
                    break;

                case ComparisonOperator.StartsWith:
                    res.SetSkipped();
                    res.Details = "'Starts With' operator not applicable for Date Time values, comparison skipped";
                    break;

                case ComparisonOperator.EndsWith:
                    res.SetSkipped();
                    res.Details = "'Ends With' operator not applicable for Date Time values, comparison skipped";
                    break;
            }

            return res;
        }

        /// <summary>
        /// 	Compares boolean values.
        /// </summary>
        /// <param name="comparisonOperator"> The comparison operator. </param>
        /// <param name="expValue"> The expected value. </param>
        /// <param name="actValue"> The actual value. </param>
        /// <returns> Comparison Result object </returns>
        private static ShortComparisonResult CompareBoolValues(ComparisonOperator comparisonOperator, bool expValue, bool actValue)
        {
            ShortComparisonResult res = new ShortComparisonResult();

            switch (comparisonOperator)
            {
                case ComparisonOperator.GreaterThan:
                    res.SetSkipped();
                    res.Details = "'Greater Than' operator not applicable for Boolean values, comparison skipped";
                    break;

                case ComparisonOperator.GreaterThanOrEqual:
                    res.SetSkipped();
                    res.Details = "'Greater Than Or Equal' operator not applicable for Boolean values, comparison skipped";
                    break;

                case ComparisonOperator.Equal:
                    res.SetSuccessOrFail(actValue == expValue);
                    break;

                case ComparisonOperator.NotEqual:
                    res.SetSuccessOrFail(actValue != expValue);
                    break;

                case ComparisonOperator.LowerThan:
                    res.SetSkipped();
                    res.Details = "'Lower Than' operator not applicable for Boolean values, comparison skipped";
                    break;

                case ComparisonOperator.LowerThanOrEqual:
                    res.SetSkipped();
                    res.Details = "'Lower Than Or Equal' operator not applicable for Boolean values, comparison skipped";
                    break;

                case ComparisonOperator.Like:
                    res.SetSkipped();
                    res.Details = "'Like' operator not applicable for Boolean values, comparison skipped";
                    break;

                case ComparisonOperator.StartsWith:
                    res.SetSkipped();
                    res.Details = "'Starts With' operator not applicable for Boolean values, comparison skipped";
                    break;

                case ComparisonOperator.EndsWith:
                    res.SetSkipped();
                    res.Details = "'Ends With' operator not applicable for Boolean values, comparison skipped";
                    break;
            }

            return res;
        }

        private static bool AreDoublesTheSame(double expected, double actual, double tolerance)
        {
            return Math.Abs(expected - actual) <= (expected * tolerance / 100);
        }

        private static bool AreDecimalsTheSame(decimal expected, decimal actual, decimal tolerance)
        {
            return Math.Abs(expected - actual) <= (expected * (tolerance / 100));
        }

        private static bool AreIntegersTheSame(int expected, int actual, double tolerance)
        {
            return Math.Abs(expected - actual) <= (expected * tolerance / 100);
        }

        private static bool TryParseDateTime(string value, out DateTime result)
        {
            // First try using ticks because date times are stored as ticks in validata DB
            long ticks;
            if (long.TryParse(value, NumberStyles.Any, _CultureInfo, out ticks))
            {
                result = new DateTime(ticks);
                return true;
            }

            // Try using default time format
            if (DateTime.TryParseExact(value, DefaultDateTimeFormat, _CultureInfo, DateTimeStyles.AllowWhiteSpaces, out result))
                return true;

            if (DateTime.TryParseExact(value, DefaultDateFormat, _CultureInfo, DateTimeStyles.AllowWhiteSpaces, out result))
                return true;

            if (DateTime.TryParseExact(value, DefaultTimeFormat, _CultureInfo, DateTimeStyles.AllowWhiteSpaces, out result))
                return true;

            // Not success
            result = DateTime.MinValue;
            return false;
        }
    }
}
