﻿using System;
using System.Diagnostics;

namespace ValidataCommon
{
    /// <summary>
    /// AA attribute name
    /// </summary>
    public class AAAttributeName
    {
        /// <summary>
        /// Prefix for the property
        /// </summary>
        public const string PropertyPrefix = "{";

        /// <summary>
        /// Gets the name of the attribute.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="propertyFieldName">Name of the property field.</param>
        /// <returns></returns>
        public static string GetAttributeName(string propertyName, string propertyFieldName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                return propertyFieldName;
            }

            return string.Format("{{{0}}}.{1}", propertyName, propertyFieldName);
        }

        /// <summary>
        /// Gets the name of the attribute.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="multiValueIndex">Index of the multi value.</param>
        /// <param name="subValueIndex">Index of the sub value.</param>
        /// <param name="propertyFieldName">Name of the property field.</param>
        /// <returns></returns>
        public static string GetAttributeName(string propertyName, int multiValueIndex, int subValueIndex, string propertyFieldName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                return propertyFieldName;
            }

            return string.Format("{{{0}-{1}~{2}}}.{3}", propertyName, multiValueIndex, subValueIndex, propertyFieldName);
        }

        /// <summary>
        /// Splits the name of the attribute.
        /// </summary>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="propertyFieldName">Name of the property field.</param>
        public static void SplitAttributeName(string attributeName, out string propertyName, out string propertyFieldName)
        {
            if (string.IsNullOrEmpty(attributeName))
            {
                throw new ArgumentException("Attribute name should not be null or empty");
            }

            if (!attributeName.StartsWith("{"))
            {
                propertyName = null;
                propertyFieldName = attributeName;
                return;
            }

            int pos = attributeName.IndexOf("}.");
            if (pos < 0)
            {
                Debug.Fail("What a name?");
                propertyName = null;
                propertyFieldName = attributeName;
                return;
            }

            propertyName = attributeName.Substring(1, pos - 1);
            propertyFieldName = attributeName.Substring(pos + "}.".Length);
        }

        public static bool IsMasterField(string attributeName)
        {
            string propertyName, propertyFieldName;
            SplitAttributeName(attributeName, out  propertyName, out  propertyFieldName);
            return string.IsNullOrEmpty(propertyName);
        }

        /// <summary>
        /// 	Parses the name of the complex field.
        /// </summary>
        /// <param name="complexFieldName"> Name of the complex field. </param>
        /// <returns> </returns>
        public static AttributeNameParsingResult ParseComplexFieldName(string complexFieldName)
        {
            string shortNameProperty, shortNameField;
            string propertyName, fieldName;
            AAAttributeName.SplitAttributeName(complexFieldName, out propertyName, out fieldName);
            int multiValueIndexField, subValueIndexField;
            int multiValueIndexProperty, subValueIndexProperty;
            bool isPropertyMultivalue = AttributeNameParser.ParseComplexFieldName(propertyName, out shortNameProperty,
                                                                                  out multiValueIndexProperty,
                                                                                  out subValueIndexProperty);
            bool isComplex = AttributeNameParser.ParseComplexFieldName(fieldName, out shortNameField,
                                                                       out multiValueIndexField, out subValueIndexField);


            return new AttributeNameParsingResult
                       {
                           FullFieldName = complexFieldName,
                           IsComplex = isComplex,
                           ShortFieldName = shortNameField,
                           MultiValueIndex = multiValueIndexField,
                           SubValueIndex = subValueIndexField,
                           IsAAAttribute = true,
                           IsPropertyComplex = isPropertyMultivalue,
                           MultiValueIndexProperty = multiValueIndexProperty,
                           SubValueIndexProperty = subValueIndexProperty,
                           PropertyShortName = shortNameProperty
                       };
        }
    }
}