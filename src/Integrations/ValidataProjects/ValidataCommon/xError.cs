using System;

namespace ValidataCommon
{
    /// <summary>
    /// Type of error/warning
    /// </summary>
    public enum ErrorTypes
    {
        /// <summary>
        /// Warning
        /// </summary>
        Warning = 0,

        /// <summary>
        /// Error
        /// </summary>
        Error,
    }

    /// <summary>
    /// Error/Warning details
    /// </summary>
    public class xError
    {
        #region Private And Protected Members

        /// <summary>
        /// Short error description
        /// </summary>
        private string _ErrorMessage;

        /// <summary>
        /// Detailed error description
        /// </summary>
        private string _ErrorDetails;

        /// <summary>
        /// Determine error type
        /// </summary>
        private ErrorTypes _ErrorType = ErrorTypes.Error;

        #endregion

        #region Public Properties

        /// <summary>
        /// Short error description
        /// </summary>
        public string ErrorShort
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }

        /// <summary>
        /// Detailed error description
        /// </summary>
        public string ErrorLong
        {
            get { return _ErrorDetails; }
            set { _ErrorDetails = value; }
        }

        /// <summary>
        /// Determine error type
        /// </summary>
        public ErrorTypes ErrorType
        {
            get { return _ErrorType; }
            set { _ErrorType = value; }
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Initializes a new instance of the <see cref="xError"/> class.
        /// </summary>
        /// <param name="errMessageShort">The err message short.</param>
        public xError(string errMessageShort)
        {
            _ErrorMessage = errMessageShort;
            _ErrorDetails = "No Details";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="xError"/> class.
        /// </summary>
        /// <param name="errMessageShort">The err message short.</param>
        /// <param name="errorMessageDetailed">The error message detailed.</param>
        public xError(string errMessageShort, string errorMessageDetailed)
        {
            _ErrorMessage = errMessageShort;
            _ErrorDetails = errorMessageDetailed;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="xError"/> class.
        /// </summary>
        /// <param name="errMessageShort"></param>
        /// <param name="errorMessageDetailed"></param>
        /// <param name="errType"></param>
        public xError(string errMessageShort, string errorMessageDetailed, ErrorTypes errType)
        {
            _ErrorMessage = errMessageShort;
            _ErrorDetails = errorMessageDetailed;
            _ErrorType = errType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="xError"/> class.
        /// </summary>
        /// <param name="exception">The exception.</param>
        public xError(Exception exception)
        {
            _ErrorMessage = exception.Message;
            _ErrorDetails = GetFullExceptionTreeInfo(exception);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="xError"/> class.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <param name="exception">The exception.</param>
        public xError(string operation, Exception exception)
        {
            _ErrorMessage = operation;
            if (exception != null && exception.Message != _ErrorMessage)
            {
                if (_ErrorMessage != null && _ErrorMessage.EndsWith("\r\n"))
                {
                    _ErrorMessage += "Details: " + exception.Message;
                }
                else
                {
                    _ErrorMessage += ". Details: " + exception.Message;
                }
            }
            
            _ErrorDetails = GetFullExceptionTreeInfo(exception);
        }

        #endregion

        /// <summary>
        /// Gets the full exception tree info.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        public static string GetFullExceptionTreeInfo(Exception ex)
        {
            if (ex == null)
            {
                return "";
            }
            
            return ex.ToString() + (ex.InnerException != null
                                        ? Environment.NewLine + GetFullExceptionTreeInfo(ex.InnerException)
                                        : ""
                                   );
        }
    }
}