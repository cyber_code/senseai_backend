﻿
namespace ValidataCommon
{
    /// <summary>
    /// Static class containing T24 Application (table) names
    /// </summary>
    public static class T24ApplicationName
    {
        /// <summary>
        /// HELPTEXT.MAINMENU Table
        /// </summary>
        public static string HelpText_MainMenu = "HELPTEXT.MAINMENU";

        /// <summary>
        /// HELPTEXT.MENU Table
        /// </summary>
        public static string HelpText_Menu = "HELPTEXT.MENU";

        /// <summary>
        /// EB.TABBED.SCREEN table
        /// </summary>
        public static string EbTabbedScreen = "EB.TABBED.SCREEN";

        /// <summary>
        /// USER Table
        /// </summary>
        public static string User = "USER";

        /// <summary>
        /// LANGUAGE Table
        /// </summary>
        public static string Language = "LANGUAGE";

        /// <summary>
        /// CURRENCY Table
        /// </summary>
        public static string Currency = "CURRENCY";

        /// <summary>
        /// ASCII.VAL.TABLE Table
        /// </summary>
        public static string AsciiValTable = "ASCII.VAL.TABLE";

        /// <summary>
        /// ASCII.VALUES Table
        /// </summary>
        public static string AsciiValues = "ASCII.VALUES";

        /// <summary>
        /// PGM.FILE Table
        /// </summary>
        public static string PgmFile = "PGM.FILE";

        /// <summary>
        /// ABBREVIATION Table
        /// </summary>
        public static string Abbreviation = "ABBREVIATION";

        /// <summary>
        /// BROWSER.PREFERENCES Table
        /// </summary>
        public static string BrowserPreferences = "BROWSER.PREFERENCES";

        /// <summary>
        /// EB.COMPOSITE.SCREEN Table
        /// </summary>
        public static string EbCompositeScreen = "EB.COMPOSITE.SCREEN";

        /// <summary>
        /// ENQUIRY Table
        /// </summary>
        public static string Enquiry = "ENQUIRY";
        
        /// <summary>
        /// EB.LOOKUP Table
        /// </summary>
        public static string EBLookup = "EB.LOOKUP";
    }
}
