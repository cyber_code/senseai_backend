﻿using System;

namespace ValidataCommon
{
    /// <summary>
    /// Helper for domain usernames
    /// </summary>
    public static class DomainUsernamesHelper
    {
        private class UserNameParts
        {
            public readonly string DomainName;
            public readonly string UserName;

            public UserNameParts(string domainName, string userName)
            {
                DomainName = domainName;
                UserName = userName;
            }

            public override string ToString()
            {
                return DomainName + PrefixedDomainDelimiter + UserName;
            }

            public override bool Equals(object obj)
            {
                UserNameParts other = (UserNameParts) obj;
                return string.Compare(DomainName, other.DomainName, true) == 0 && string.Compare(UserName, other.UserName, true) == 0;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        private const string PrefixedDomainDelimiter = "\\";
        private const string PostfixedDomainDelimiter = "@";

        private static UserNameParts ParseName(string name)
        {
            return ParseNameStartingWithDomain(name) ?? ParseNameEndingWithDomain(name);
        }

        private static UserNameParts ParseNameStartingWithDomain(string name)
        {
            string[] parts = name.Split(new string[] {PrefixedDomainDelimiter}, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
            {
                return null;
            }

            return new UserNameParts(parts[0], parts[1]);
        }

        private static UserNameParts ParseNameEndingWithDomain(string name)
        {
            string[] parts = name.Split(new string[] {PostfixedDomainDelimiter}, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
            {
                return null;
            }

            return new UserNameParts(parts[1], parts[0]);
        }

        /// <summary>
        /// Checks if a name looks like a valid domain name
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static bool LooksLikeValidName(string name)
        {
            return ParseName(name) != null;
        }

        /// <summary>
        /// Ares the names equivalent.
        /// </summary>
        /// <param name="name1">The name1.</param>
        /// <param name="name2">The name2.</param>
        /// <returns></returns>
        public static bool AreNamesEquivalent(string name1, string name2)
        {
            UserNameParts parsedName1 = ParseName(name1);
            UserNameParts parsedName2 = ParseName(name2);
            return parsedName1 != null && parsedName2 != null && parsedName1.Equals(parsedName2);
        }
    }
}