﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace ValidataCommon
{
    [Serializable]
    public class ReportTaskDescription
    {
        private String _password;

        public ReportTaskDescription()
        {
            ReportsToBeGenerated = new List<StageReports>();
            Tasks = new List<TaskData>();
            AFTaskMapping = new List<AFTaskMap>();
        }

        public void InitReportFolder(ulong jobNoderef, ulong taskNoderef)
        {
            string logDirectory = string.Format(@"{0}AdapterFiles\Temp\MJOB_{1:d8}_TASK_{2:d8}_Reporting", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") , jobNoderef,
                                                taskNoderef);
            if (!Directory.Exists(logDirectory))
                Directory.CreateDirectory(logDirectory);

            ReportTaskFolder = logDirectory;
        }

        #region Public Properties

        public String Name { get; set; }

        public ulong ProjectID { get; set; }

        public ulong SubProjectID { get; set; }

        public ulong DisciplineID { get; set; }

        public ulong RoleID { get; set; }

        public ulong UserID { get; set; }

        [XmlIgnore]
        public String Password
        {
            get { return _password; }
            set { _password = value; }
        }

    
               
        public String OutputFolder { get; set; }

        public String ReportTaskFolder { get; set; }

        public String ReportDefinitonName { get; set; }

        public int StageID { get; set; }

        public bool AutomaticStart { get; set; }
        public bool AutomaticContinue { get; set; }

        public List<TaskData> Tasks { get; set; }
        public List<StageReports> ReportsToBeGenerated { get; set; }
        public List<AFTaskMap> AFTaskMapping { get; set; }

        public bool UseReportDefinitionTasks { get; set; }

        /// <summary>
        /// Flag to exclude report task from Migration Traceability Reports
        /// </summary>
        public bool ExcludeFromMTReports { get; set; }

        #endregion

        #region Serialization

        /// <summary>
        /// Execute the process of serialization of the data
        /// </summary>
        /// <returns>false- cancel the save</returns>
        public bool PerformSerialization()
        {
            return SerializeToXml(this);
        }

        public bool SerializeToXml()
        {
            return SerializeToXml(this);
        }

        /// <summary>
        /// Actual serialization of the data
        /// </summary>
        /// <param name="reportDescription">the report to serialize</param>
        /// <returns>false- cancel the save</returns>
        public static bool SerializeToXml(ReportTaskDescription reportTaskDescription)
        {
            string filePath = GenerateReportTaskFullPath(reportTaskDescription.Name);
            XmlSerializer serializator = new XmlSerializer(reportTaskDescription.GetType());
            try
            {
                using(StreamWriter fileStream = File.CreateText(filePath))
                {
                    serializator.Serialize(fileStream, reportTaskDescription);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return true;
        }

        /// <summary>
        /// Create backup of the report, shows message to warn if old file is to be rewriten
        /// </summary>
        /// <param name="reportDescription">the description of the report</param>
        /// <returns>false - to cancel the save</returns>
        internal static bool CreateReportBackup(ReportTaskDescription reportTaskDescription)
        {
            string filePath = GenerateReportTaskFullPath(reportTaskDescription.Name);
            String fileBackup = GenerateBackupName(filePath);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Copy(filePath, fileBackup, true); // make backup
            }
            return true;
        }


        private static string GenerateBackupName(string filePath)
        {
            return filePath + ".bak";
        }

        public static ReportTaskDescription DeserializeFromXml(string fullPath)
        {
            XmlSerializer deSerializator = new XmlSerializer(typeof(ReportTaskDescription));
            // Reading the XML report
            if (System.IO.File.Exists(fullPath))
            {
                Stream readerReport = new FileStream(fullPath, FileMode.Open);
                ReportTaskDescription reportTaskDescription =
                    deSerializator.Deserialize(readerReport) as ReportTaskDescription;
                System.Diagnostics.Debug.Assert(reportTaskDescription != null,
                                                "Report task could not be loaded! \r\n" + fullPath);

                return reportTaskDescription;
            }
            return null;
        }

        private static string GenerateReportTaskFullPath(string reportName)
        {
            String reportPath = GetReportPath();
            if (!Directory.Exists(reportPath))
                Directory.CreateDirectory(reportPath);
            //String dateInfo = DateTime.Now.ToString("yyyyMMdd-HHmmss-");
            String dateInfo = ""; // no change of the name - u.e. overwrite it
            String filePath = String.Concat(reportPath, dateInfo, reportName, ".xml");
            return filePath;
        }

        public static string GetReportPath()
        {
            String fullPath = String.Concat(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "\\ReportTasks\\");
            return fullPath;
        }

        public static ReportTaskDescription DeserializeFromString(string serialized)
        {
            if (!string.IsNullOrEmpty(serialized))
            {
                XmlSerializer deSerializator = new XmlSerializer(typeof (ReportTaskDescription));
                // Reading the XML report
                ReportTaskDescription reportTaskDescription;
                try
                {
                    reportTaskDescription = deSerializator.Deserialize(new StringReader(serialized)) as ReportTaskDescription;
                }
                catch(InvalidOperationException)
                {
                    reportTaskDescription = null;
                }

                if(reportTaskDescription == null)
                {
                    ApplicationException exp = new ApplicationException("The report description cannot been successfully read.");
                    exp.Data.Add("serialized", serialized);
                    throw exp;
                }

                return reportTaskDescription;
            }
            else
            {
                return new ReportTaskDescription();
            }
        }

        public string SerializeToString()
        {
            StringWriter sw = new StringWriter();
            XmlSerializer serializator = new XmlSerializer(GetType());
            serializator.Serialize(sw, this);
            return sw.ToString();
        }

        public void SerializeToXml(string reportFile)
        {
            //File.WriteAllText(reportFile, SerializeToString());

            XmlSerializer serializator = new XmlSerializer(GetType());
            using (StreamWriter fileStream = File.CreateText(reportFile))
            {
                serializator.Serialize(fileStream, this);
            }
        }

        #endregion
    }

    [Serializable]
    public class TaskShortDescription
    {
        public ulong TaskNoderef { get; set; }
        public ulong JobNoderef { get; set; }
        public string TaskName { get; set; }

        public TaskShortDescription()
        {
        }

        public TaskShortDescription(TaskShortDescription src)
        {
            TaskNoderef = src.TaskNoderef;
            JobNoderef = src.JobNoderef;
            TaskName = src.TaskName;
        }
    }

    [Serializable]
    public class TaskShortDescriptionList : List<TaskShortDescription>
    {
    }

    [Serializable]
    public class TaskData
    {
        public ulong TaskNoderef { get; set; }
        public int TaskIndex { get; set; }
        public String TaskGUID { get; set; }
        public ulong JobNoderef { get; set; }
        public String JobGUID { get; set; }
        public string TaskName { get; set; }
        public string SourceTypical { get; set; }
        public string TargetTypical { get; set; }
        public bool IsUpdated { get; set; }

        public TaskShortDescriptionList IncludeHistoryTasks
        {
            get;
            set;
        }

        public string IncludeHistoryTasksCountStr
        {
            get { return IncludeHistoryTasks.Count == 0 ? "No" : IncludeHistoryTasks.Count.ToString(); }
        }


        public TaskData()
        {
            IncludeHistoryTasks = new TaskShortDescriptionList();
            IsUpdated = false;
        }

        public override String ToString()
        {
            return String.Format("Task:{0} / Job:{1}-{2}", TaskNoderef, JobNoderef, TaskName);
        }
    }

    [Serializable]
    public class StageInfo
    {
        public string Name { get; set; }
        public string CatalogName { get; set; }
        public string TypicalName { get; set; }
        public string StageType { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    [Serializable]
    public class AFTaskMap
    {
        public TaskData SourceTask { get; set; }
        public TaskData TargetTask { get; set; }
    }

    [Serializable]
    public class StageReports
    {
        public bool AlternativeLayout { get; set; }

        public ExportingReportTypes AlternativeLayoutReportsList { get; set; }

        public Scope ExportScope
        {
            get { return Scope.Complete; }
            set { return; } //  We always generate complete reports in this mode
        }

        public ReportLayout ExportReportLayout
        {
            get;
            set;
        }

        public StatisticsLayouts StatisticsLayout
        {
            get;
            set;
        }

        public bool ExportInMultipleFiles
        {
            get { return CountOfRecordsPerFile > 0; }
            set { return; } //  Value is based on CountOfRecordsPerFile
        }

        public int CountOfRecordsPerFile
        {
            get;
            set;
        }

        public bool ExportAttributesWithErrorsOnly { get; set; }

        public bool ProduceErrorSummary { get; set; }

        public String ReportName { get; set; }

        public String InstancePostFilter { get; set; }
        public bool UseErrorMessageFilter { get; set; }
        public bool EnableFieldRename { get; set; }
        public bool DeDuplicationLayout { get; set; }
        public String ExportFormat { get; set; }
        public List<string> FieldsToExport { get; set; }

        public string FieldsToExportCounter
        {
            get
            {
                return FieldsToExport == null ? "All" : FieldsToExport.Count == 0 ? "All" : "Selected: " + FieldsToExport.Count;
            }
        }

        public string AlternativeStyleReportsText
        {
            get
            {
                return AlternativeLayoutReportsList.GetReports();
            }
        }


        public void CopyFrom(StageReports sourceObject)
        {
            AlternativeLayout = sourceObject.AlternativeLayout;
            AlternativeLayoutReportsList = sourceObject.AlternativeLayoutReportsList;
            CountOfRecordsPerFile = sourceObject.CountOfRecordsPerFile;
            ExportAttributesWithErrorsOnly = sourceObject.ExportAttributesWithErrorsOnly;
            ExportReportLayout = sourceObject.ExportReportLayout;
            ProduceErrorSummary = sourceObject.ProduceErrorSummary;
            ReportName = sourceObject.ReportName;
            InstancePostFilter = sourceObject.InstancePostFilter;
            UseErrorMessageFilter = sourceObject.UseErrorMessageFilter;
            EnableFieldRename = sourceObject.EnableFieldRename;
            ExportFormat = sourceObject.ExportFormat;
            FieldsToExport = sourceObject.FieldsToExport;
            DeDuplicationLayout = sourceObject.DeDuplicationLayout;
            StatisticsLayout = sourceObject.StatisticsLayout;
        }
    }

    public enum Scope
    {
        CurrentPage,

        Complete
    }

    public enum ReportLayout
    {
        AttributesInRows,
        AttributeNamesInHeaderValueInColumns
    }

    public class ReportLayoutNameEngine
    {
        private static readonly Dictionary<ReportLayout, string> TypeNameMap;

        static ReportLayoutNameEngine()
        {
            TypeNameMap = new Dictionary<ReportLayout, string>
                                 {
                                     {ReportLayout.AttributesInRows, "Vertical"},
                                     {ReportLayout.AttributeNamesInHeaderValueInColumns, "Horizontal"}
                                 };
        }

        ///<summary>
        ///</summary>
        public static Dictionary<ReportLayout, string> LayoutNames
        {
            get { return new ReportLayoutNameMap(TypeNameMap); }
        }

        /// <summary>
        /// Returns the correct name for the specified report layout to be displayed in UI.
        /// </summary>
        /// <param name="reportLayout">The report layout which name is needed.</param>
        /// <returns>Plain language name of the status.</returns>
        public static string GetReportLayoutName(ReportLayout reportLayout)
        {
            return TypeNameMap[reportLayout];
        }
    }

    public class ReportLayoutNameMap : Dictionary<ReportLayout, string>
    {
        public ReportLayoutNameMap(IDictionary<ReportLayout, string> source)
            : base(source)
        {

        }
    }




    /// <summary>
    /// 
    /// </summary>
    public enum StatisticsLayouts
    {
        /// <summary>
        /// 
        /// </summary>
        Reconciliation,
        /// <summary>
        /// 
        /// </summary>
        StatisticsExport,
        /// <summary>
        /// 
        /// </summary>
        StatisticsExportList,
        /// <summary>
        /// Report with T24 Validations errors executed over transformation 
        /// </summary>
        TransformationValidation
    }

    /// <summary>
    /// 
    /// </summary>
    public class StatisticsReportLayoutEngine
    {
        private static readonly Dictionary<StatisticsLayouts, string> TypeNameMap;

        static StatisticsReportLayoutEngine()
        {
            TypeNameMap = new Dictionary<StatisticsLayouts, string>
                                 {
                                     {StatisticsLayouts.Reconciliation, "Reconciliation report"},
                                     {StatisticsLayouts.StatisticsExport, "Statistics Export"},
                                     {StatisticsLayouts.StatisticsExportList, "Statistics Export List"},
                                     {StatisticsLayouts.TransformationValidation, "Transformation Validation report"}
                                 };
        }

        ///<summary>
        ///</summary>
        public static Dictionary<StatisticsLayouts, string> LayoutNames
        {
            get { return new StatisticsReportLayoutMap(TypeNameMap); }
        }

        /// <summary>
        /// Returns the correct name for the specified report layout to be displayed in UI.
        /// </summary>
        /// <param name="statisticsReportLayout">The report layout which name is needed.</param>
        /// <returns>Plain language name of the status.</returns>
        public static string GetReportLayoutName(StatisticsLayouts statisticsReportLayout)
        {
            return TypeNameMap[statisticsReportLayout];
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class StatisticsReportLayoutMap : Dictionary<StatisticsLayouts, string>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        public StatisticsReportLayoutMap(IDictionary<StatisticsLayouts, string> source)
            : base(source)
        {

        }
    }
}
