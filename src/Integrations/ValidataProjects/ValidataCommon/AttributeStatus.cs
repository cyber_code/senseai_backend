namespace Validata.Common
{
    /// <summary>
    /// Attribute Status
    /// </summary>
    public enum AttributeStatus : int
    {
        /// <summary>
        /// Optional Attribute
        /// </summary>
        Optional = 0,

        /// <summary>
        /// Read Only Attribute
        /// </summary>
        ReadOnly = 1,

        /// <summary>
        /// Mandatory Attribute
        /// </summary>
        Mandatory = 2
    }
}