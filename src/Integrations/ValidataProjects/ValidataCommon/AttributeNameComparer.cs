﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using NUnit.Framework.Constraints;

namespace ValidataCommon
{
    public class AttributeNameComparer : Comparer<String>
    {
        public override int Compare(string x, string y)
        {
            if (String.IsNullOrEmpty(x) && String.IsNullOrEmpty(y))
                return 0;

            if (String.IsNullOrEmpty(x)) 
                return -1;

            if (String.IsNullOrEmpty(y))
                return 1;

            if (String.Compare(x, y, StringComparison.Ordinal) == 0)
                return 0;

            var xx = AttributeNameParser.ParseComplexFieldName(x);
            var yy = AttributeNameParser.ParseComplexFieldName(y);
            
            if (!xx.IsComplex && yy.IsComplex)
            {
                return -1;
            }
            else if (xx.IsComplex && !yy.IsComplex)
            {
                return 1;
            }
            else if (!xx.IsComplex && !yy.IsComplex)
            {
                return String.Compare(x, y, StringComparison.Ordinal);
            }
            else
            {
                // Check whether the same short name
                var result = String.Compare(xx.ShortFieldName, yy.ShortFieldName, StringComparison.Ordinal);
                if (result != 0)
                    return result;

                // compare MV's
                result = xx.MultiValueIndex.CompareTo(yy.MultiValueIndex);
                if (result != 0)
                    return result;

                // compare SV's
                result = xx.SubValueIndex.CompareTo(yy.SubValueIndex);

                return result;
            }

            //If we are here something is wrong with the attribute names
            throw new Exception(String.Format("Cannot compare attribute names '{0}' and '{1}' ", x, y));
        }
    }
}
