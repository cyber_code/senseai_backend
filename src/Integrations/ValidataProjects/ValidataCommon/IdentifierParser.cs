using System;

namespace ValidataCommon
{
    internal class IdentifierParser
    {
        public string Prefix = string.Empty;
        public ulong NumericPart;
        public string Postfix = string.Empty;

        public IdentifierParser(string id)
        {
            Prefix = ExtractNotNumericPortion(ref id);
            if(id.Length == 0)
            {
                return; // the ID had no digits
            }

            NumericPart = ulong.Parse(ExtractNumericPortion(ref id));
            Postfix = id;
        }

        private static string ExtractNumericPortion(ref string id)
        {
            return ExtractPortion(ref id, false);
        }

        private static string ExtractNotNumericPortion(ref string id)
        {
            return ExtractPortion(ref id, true);
        }

        private static string ExtractPortion(ref string id, bool readDigits)
        {
            int index = FindIndexOfDigitOrNonDigit(id, readDigits);
            string result;
            if (index == 0)
            {
                result = string.Empty;
            }
            else if (index == id.Length)
            {
                result = id;
                id = string.Empty;
            }
            else
            {
                result = id.Substring(0, index);
                id = id.Substring(index, id.Length - index);
            }

            return result;
        }

        private static int FindIndexOfDigitOrNonDigit(string id, bool isDigit)
        {
            for (int i = 0; i < id.Length; i++)
            {
                if (isDigit == Char.IsDigit(id[i]))
                {
                    return i;
                }
            }

            return id.Length;
        }
    }
}