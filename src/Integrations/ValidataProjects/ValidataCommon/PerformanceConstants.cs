using System.Collections.Generic;
using System.IO;
using System.Xml;
using System;
namespace ValidataCommon
{
    /// <summary>
    /// Constants related to performance testing
    /// </summary>
    public static class PerformanceConstants
    {
        private static readonly  string PerformanceIndicatorsPath = Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\Assemblies\PerformanceIndicators.xml";

        private static ServerPerformanceIndicator[] _ServerIndicators;

        /// <summary>
        /// Server performance indicators
        /// </summary>
        public static ServerPerformanceIndicator[] ServerIndicators 
        {
            get
            {
                if (_ServerIndicators == null)
                    return new ServerPerformanceIndicator[0];

                return _ServerIndicators;
            }
        }

        /// <summary>
        /// Server performance indicators names
        /// </summary>
        public static string[] ServerIndicatorsNames
        {
            get
            {
                List<string> indicatorsNames = new List<string>();
                
                foreach (ServerPerformanceIndicator perfIndicator in ServerIndicators)
                {
                    indicatorsNames.Add(perfIndicator.Name);
                }

                return indicatorsNames.ToArray();
            }
        }

        static PerformanceConstants()
        {
            LoadPerformanceIndicators();
        }

        /// <summary>
        /// Gets the indicator names.
        /// </summary>
        /// <returns></returns>
        public static string[] GetIndicatorsNames()
        {
            string[] localIndicators =    new string[]
                    {
                        "Transaction Per Second",
                        "Transaction Response Time",
                        HttpAdapterConstants.PerformanceIndicators.DownloadTime,
                        HttpAdapterConstants.PerformanceIndicators.NumberOfRequests,
                        HttpAdapterConstants.PerformanceIndicators.WebServerProcessingTime,
                        HttpAdapterConstants.PerformanceIndicators.NumberOfDownloadedBytes,
                    };

            List<string> indicators = new List<string>();

            foreach (ServerPerformanceIndicator perfIndicator in ServerIndicators)
            {
                indicators.Add(perfIndicator.Name);
            }

            indicators.AddRange(localIndicators);

            return indicators.ToArray();

        }

        /// <summary>
        /// Gets the units of measurement.
        /// </summary>
        /// <param name="name">The name of the indicator.</param>
        /// <returns></returns>
        public static string GetUnitsOfMeasurement(string name)
        {
            switch (name)
            {
                case "Transaction Per Second":
                    return "";
                case "Transaction Response Time":
                    return "ms";
                case HttpAdapterConstants.PerformanceIndicators.DownloadTime:
                    return "ms";
                case HttpAdapterConstants.PerformanceIndicators.WebServerProcessingTime:
                    return "ms";
                default:
                    return GetIndicatorMeasurement(name);
            }
        }

        /// <summary>
        /// Load Performance Indicators
        /// </summary>
        private static void LoadPerformanceIndicators()
        {
            if (!File.Exists(PerformanceIndicatorsPath))
                throw new IOException("Performance indicators file does not exists: " + PerformanceIndicatorsPath);

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(PerformanceIndicatorsPath);

                XmlNodeList indicatorsList = doc.SelectNodes("//PerformanceIndicator");
                _ServerIndicators = new ServerPerformanceIndicator[indicatorsList.Count];

                for (int i = 0; i < indicatorsList.Count; i++)
                {
                    string name = indicatorsList[i].Attributes["Name"].Value;
                    string measurement = indicatorsList[i].Attributes["Measurement"].Value;

                    _ServerIndicators[i] = new ServerPerformanceIndicator(name, measurement);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Unable to load performance indicators.", ex);
            }
        }

        /// <summary>
        /// Get performance indicator measurement
        /// </summary>
        /// <param name="indicatorName"></param>
        /// <returns></returns>
        private static string GetIndicatorMeasurement(string indicatorName)
        {
            if (String.IsNullOrEmpty(indicatorName))
                return String.Empty;

            foreach (ServerPerformanceIndicator perfIndicator in ServerIndicators)
            {
                if (perfIndicator.Name == indicatorName)
                    return perfIndicator.Mesurement;
            }

            return String.Empty;
        }
    }
}