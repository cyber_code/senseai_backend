﻿using System.IO;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Delimited File Writer
    /// </summary>
    public class DelimitedFileWriter : StreamWriter
    {
        private char _Separator;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelimitedFileWriter"/> class.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="enc">The enc.</param>
        /// <param name="separator">The separator.</param>
        /// <param name="append">if set to <c>true</c> [append].</param>
        public DelimitedFileWriter(string filename, Encoding enc, char separator, bool append)
            : base(filename, append, enc)
        {
            _Separator = separator;
        }

        /// <summary>
        /// Writes the fields.
        /// </summary>
        /// <param name="content">The content.</param>
        public void WriteFields(string[] content)
        {
            string line = string.Join(_Separator.ToString(), content);
            WriteLine(line);
        }
    }
}