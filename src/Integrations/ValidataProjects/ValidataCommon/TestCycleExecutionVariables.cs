﻿using ValidataCommon.Interfaces;

namespace ValidataCommon
{
    /// <summary>
    /// Execution context for adapter calls (for test suites, but not only)
    /// </summary>
    public class TestCycleExecutionVariables : IAdapterExecutionContext
    {
        /// <summary>
        /// Gets the test suite ID (or the execution ID if not in the scope of the test suite)
        /// </summary>
        public string TestCycleID { get; set; }

        /// <summary>
        /// Gets the test case ID (valid only in the context of test suite execution)
        /// </summary>
        public ulong TestCaseID { get; set; }

        /// <summary>
        /// Gets the test step ID (valid only in the context of test suite execution)
        /// </summary>
        public ulong TestStepID { get; set; }

        /// <summary>
        /// Gets the name of the environment.
        /// </summary>
        /// <value>
        /// The name of the environment.
        /// </value>
        public string EnvironmentName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DequeueT24UserDelegate DequeueT24User { get; set; }

        public DequeueT24UserDelegateNew DequeueT24UserNew { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ReleaseT24UsersDelegate ReleaseT24Users { get; set; }

        /// <summary>
        /// Gets or sets the index of the instance.
        /// </summary>
        /// <value>
        /// The index of the instance.
        /// </value>
        public int InstanceIndex { get; set; }

        /// <summary>
        /// Gets the temp directory.
        /// </summary>
        public string TempDirectory { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestCycleExecutionVariables"/> class.
        /// </summary>
        public TestCycleExecutionVariables()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestCycleExecutionVariables"/> class.
        /// </summary>
        /// <param name="src">The SRC.</param>
        public TestCycleExecutionVariables(IAdapterExecutionContext src)
        {
            TestCycleID = src.TestCycleID;
            TestCaseID = src.TestCaseID;
            TestStepID = src.TestStepID;
            EnvironmentName = src.EnvironmentName;
            TempDirectory = src.TempDirectory;
        }
    }
}