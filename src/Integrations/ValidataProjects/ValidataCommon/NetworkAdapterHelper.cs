﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;

namespace ValidataCommon
{
    public class NetworkAdapterHelper
    {
        public static string GetAdapterMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in nics)
            {
                if (adapter.OperationalStatus == OperationalStatus.Up)
                {
                    PhysicalAddress address = adapter.GetPhysicalAddress();
                    string macAddress = address.ToString();
                    if (!String.IsNullOrEmpty(macAddress))
                    {
                        return macAddress;
                    }
                }
            }

            return String.Empty;
        }
    }
}
