﻿using System;
using System.Linq;

namespace ValidataCommon
{
    public class AAHtmlFormName
    {
        public const string AA_PROPERTY_FORM_PREFIX = "AA.ARR.";
        public const string AA_SIM_PROPERTY_FORM_PREFIX = "AA.SIM.";

        protected readonly string FormPrefix;

        public readonly string Name;

        public string AAPropertyName
        {
            get;
            private set;
        }

        public string Effective
        {
            get;
            private set;
        }

        public string MvIndex
        {
            get;
            private set;
        }

        public string AppVersionName
        {
            get;
            private set;
        }

        public AAHtmlFormName(string formPrefix, string name, bool isBrowserMultiApp = false, string transactionId = null)
        {
            this.FormPrefix = formPrefix;
            this.Name = name;

            if (!string.IsNullOrEmpty(formPrefix))
            {
                if (isBrowserMultiApp)
                    SplitBrowserMultiAppFormName(transactionId);
                else
                    SplitFormName();
            }
        }

        private void SplitBrowserMultiAppFormName(string transactionId)
        {
            // split this
            //MXMB.CUSTOMER.ADDRESS,BMAP.IN100610

            AppVersionName = "";
            if (!String.IsNullOrEmpty(transactionId))
            {
                AAPropertyName = Name.Replace(transactionId, "");
            }
            else
            {
                int countDigits = 0;
                foreach (char c in Name.ToCharArray().Reverse())
                {
                    if (char.IsDigit(c))
                    {
                        countDigits++;
                    }
                    else
                        break;
                }

                AAPropertyName = Name.Substring(0, Name.Length - countDigits);
            }


        }

        private void SplitFormName()
        {
            // split this
            //AA.ARR.INTEREST,AA.PERIODIC.PAAA10006WP8ZV-DEPOSITINT-20100111.1:companyId

            if (!Name.StartsWith(FormPrefix))
            {
                throw new ApplicationException(string.Format("Invalid AA form name '{0}': name must start with: '{1}'", Name, FormPrefix));
            }

            int pos = Name.IndexOf("-");
            if (pos < 0)
            {
                throw new ApplicationException(string.Format("Invalid AA form name '{0}': cannot find AA property name", Name));
            }

            AppVersionName = Name.Substring(0, pos);

            string temp = Name.Substring(pos + 1);

            pos = temp.IndexOf("-");
            if (pos < 0)
            {
                throw new ApplicationException(string.Format("Invalid AA form name '{0}': cannot find AA property efective", Name));
            }

            AAPropertyName = temp.Substring(0, pos);

            temp = temp.Substring(pos + 1);

            var parts = temp.Split(new char[] { '.' });
            System.Diagnostics.Debug.Assert(parts.Length == 2);

            if (parts.Length > 0)
            {
                this.Effective = parts[0];
            }

            if (parts.Length > 1)
            {
                this.MvIndex = parts[1];
            }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
