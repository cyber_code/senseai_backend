﻿using System;

namespace ValidataCommon
{
    public class MultiValueSplitElement : RuleElement
    {
        private String _multiValueSeparator = ":";
        private String _subValueSeparator = "!";

        public String MultiValueSeparator
        {
            get { return _multiValueSeparator; }
            set { _multiValueSeparator = String.IsNullOrEmpty(value) ? ":" : value; }
        }

        public String SubValueSeparator
        {
            get { return _subValueSeparator; }
            set { _subValueSeparator = String.IsNullOrEmpty(value) ? "!" : value; }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MultiValueSplitElement()
        {

        }

        /// <summary>
        /// Data constructor - initialize fields
        /// </summary>
        /// <param name="catalogName">Catalog name</param>
        /// <param name="typicalName">Typical name</param>
        /// <param name="fieldName">Field Name</param>
        /// <param name="multiValueSeparator">Separator for Multi-Values</param>
        /// <param name="subValueSeperator">Separator for Sub-Values</param>
        public MultiValueSplitElement(string catalogName, string typicalName, string fieldName, string multiValueSeparator, string subValueSeperator)
            : base(catalogName, typicalName, fieldName)
        {
            MultiValueSeparator = multiValueSeparator;
            SubValueSeparator = subValueSeperator; 
        }


        public bool GetSeparatorElement(RuleElement element, ref String multiValueSeparator, ref String subValueSeparator)
        {
            try
            {
                bool bBaseEqual = CheckRuleElement(element);
                if (bBaseEqual)
                {
                    multiValueSeparator = this.MultiValueSeparator;
                    subValueSeparator = this.SubValueSeparator;
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
