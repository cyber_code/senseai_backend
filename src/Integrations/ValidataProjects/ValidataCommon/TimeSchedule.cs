﻿using System;
using System.Xml.Serialization;

namespace ValidataCommon
{
    /// <summary>
    /// Time schedule
    /// </summary>
    [Serializable]
    public class TimeSchedule
    {
        /// <summary>
        /// Gets or sets the start offset.
        /// </summary>
        /// <value>The start offset.</value>
        [XmlIgnore]
        public TimeSpan StartOffset { get; set; }

        /// <summary>
        /// Gets or sets the XML start offset.
        /// </summary>
        /// <value>The XML start offset.</value>
        [XmlElement("StartOffset")]
        public string XmlStartOffset
        {
            get { return StartOffset.Ticks.ToString(); }
            set { StartOffset = new TimeSpan(long.Parse(value)); }
        }

        /// <summary>
        /// Gets or sets the initial users count.
        /// </summary>
        /// <value>The initial users count.</value>
        public int InitialUsersCount { get; set; }

        /// <summary>
        /// Gets or sets the ramp up period.
        /// </summary>
        /// <value>The ramp up period.</value>
        [XmlIgnore]
        public TimeSpan RampUpPeriod { get; set; }

        /// <summary>
        /// Gets or sets the XML ramp up period.
        /// </summary>
        /// <value>The XML ramp up period.</value>
        [XmlElement("RampUpPeriod")]
        public string XmlRampUpPeriod
        {
            get { return RampUpPeriod.Ticks.ToString(); }
            set { RampUpPeriod = new TimeSpan(long.Parse(value)); }
        }

        /// <summary>
        /// Gets or sets the ramp up count.
        /// </summary>
        /// <value>The ramp up count.</value>
        public int RampUpCount { get; set; }

        /// <summary>
        /// Gets or sets the duration of the execution.
        /// </summary>
        /// <value>The duration of the execution.</value>
        [XmlIgnore]
        public TimeSpan ExecutionDuration { get; set; }

        /// <summary>
        /// Gets or sets the duration of the XML execution.
        /// </summary>
        /// <value>The duration of the XML execution.</value>
        [XmlElement("ExecutionDuration")]
        public string XmlExecutionDuration
        {
            get { return ExecutionDuration.Ticks.ToString(); }
            set { ExecutionDuration = new TimeSpan(long.Parse(value)); }
        }

        /// <summary>
        /// Gets or sets the ramp down period.
        /// </summary>
        /// <value>The ramp down period.</value>
        [XmlIgnore]
        public TimeSpan RampDownPeriod { get; set; }

        /// <summary>
        /// Gets or sets the XML ramp down period.
        /// </summary>
        /// <value>The XML ramp down period.</value>
        [XmlElement("RampDownPeriod")]
        public string XmlRampDownPeriod
        {
            get { return RampDownPeriod.Ticks.ToString(); }
            set { RampDownPeriod = new TimeSpan(long.Parse(value)); }
        }

        /// <summary>
        /// Gets or sets the ramp down count.
        /// </summary>
        /// <value>The ramp down count.</value>
        public int RampDownCount { get; set; }

        /// <summary>
        /// Gets or sets the final users count.
        /// </summary>
        /// <value>The final users count.</value>
        public int FinalUsersCount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [force close].
        /// </summary>
        /// <value><c>true</c> if [force close]; otherwise, <c>false</c>.</value>
        public bool ForceClose { get; set; }

        /// <summary>
        /// Calculates the duration of the ramp ups.
        /// </summary>
        /// <param name="maxRequestedVUsers">The max requested V users.</param>
        /// <returns></returns>
        public TimeSpan CalculateRampUpsDuration(int maxRequestedVUsers)
        {
            if (RampUpCount == 0)
            {
                return TimeSpan.Zero;
            }

            int numberOfRampUps = (maxRequestedVUsers - InitialUsersCount)/RampUpCount;
            if (((maxRequestedVUsers - InitialUsersCount)%RampUpCount) != 0)
            {
                numberOfRampUps++;
            }

            if (InitialUsersCount == 0)
            {
                numberOfRampUps--;
            }

            return new TimeSpan(RampUpPeriod.Ticks*numberOfRampUps);
        }

        /// <summary>
        /// Calculates the ramp downs count.
        /// </summary>
        /// <param name="maxRequestedVUsers">The max requested V users.</param>
        /// <returns></returns>
        public int CalculateRampDownsCount(int maxRequestedVUsers)
        {
            if (RampDownCount == 0)
            {
                return 0;
            }

            int numberOfRampDowns = (maxRequestedVUsers - FinalUsersCount)/RampDownCount;
            if ((maxRequestedVUsers - FinalUsersCount)%RampDownCount != 0)
            {
                numberOfRampDowns++;
            }

            if (FinalUsersCount == 0)
            {
                numberOfRampDowns--;
            }

            return numberOfRampDowns;
        }

        /// <summary>
        /// Calculates the duration of the ramp downs.
        /// </summary>
        /// <param name="maxRequestedVUsers">The max requested V users.</param>
        /// <returns></returns>
        public TimeSpan CalculateRampDownsDuration(int maxRequestedVUsers)
        {
            return new TimeSpan(RampDownPeriod.Ticks*CalculateRampDownsCount(maxRequestedVUsers));
        }

        /// <summary>
        /// Calculates the total duration of the execution.
        /// </summary>
        /// <param name="maxRequestedVUsers">The max requested V users.</param>
        /// <returns></returns>
        public TimeSpan CalculateTotalExecutionDuration(int maxRequestedVUsers)
        {
            return
                CalculateRampUpsDuration(maxRequestedVUsers) +
                ExecutionDuration +
                CalculateRampDownsDuration(maxRequestedVUsers);
        }
    }
}