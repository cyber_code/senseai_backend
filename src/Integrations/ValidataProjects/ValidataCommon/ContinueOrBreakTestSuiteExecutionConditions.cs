﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ValidataCommon
{
    /// <summary>
    /// Flag and status, used for synchronizing test execution
    /// </summary>
    public class FlagAndStatus
    {
        /// <summary>
        /// Name of flag
        /// </summary>
        public string FlagName { get; set; }

        /// <summary>
        /// Status of the flag
        /// </summary>
        public int FlagStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}={1}", FlagName, FlagStatus == 0 ? "False" : "True");
        }

        /// <summary>
        /// Parses a string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        internal static FlagAndStatus FromString(string str)
        {
            var parts = (str ?? "").Trim().Split(new char[] {'='}, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
                return null;

            return new FlagAndStatus
            {
                FlagName = parts[0].Trim(),
                FlagStatus = (parts[1].Trim() == "0" || string.Compare(parts[1].Trim(), "false", true) == 0) ? 0 : 1
            };
        }

        /// <summary>
        /// Gets a value indicating whether [is valid].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is valid]; otherwise, <c>false</c>.
        /// </value>
        public bool IsValid
        {
            get { return string.IsNullOrEmpty(ValidationError); }
        }

        /// <summary>
        /// Gets the validation error.
        /// </summary>
        /// <value>
        /// The validation error.
        /// </value>
        public string ValidationError
        {
            get
            {
                if (FlagName.IsEmptyOrNoValue())
                    return "Flag name is not specified";
                if (!FlagName.IsStringContaingOnlyLettersDigitsOrUnderscores())
                    return "Flag name contains invalid characters (must contain only letters, digits or underscores)";
                if (FlagStatus != 0 && FlagStatus != 1)
                    return "Invalid flag status (value must be '0' or '1')";

                return null;
            }
        }
    }

    /// <summary>
    /// Conditions for continuing or breaking execution
    /// </summary>
    public abstract class ContinueOrBreakTestSuiteExecutionConditions
    {
        /// <summary>
        /// The wait for
        /// </summary>
        public readonly List<FlagAndStatus> WaitFor = new List<FlagAndStatus>();

        /// <summary>
        /// Gets the logical operator.
        /// </summary>
        /// <value>
        /// The logical operator.
        /// </value>
        protected abstract string LogicalOperator { get; }


        /// <summary>
        /// Initializes from string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        protected bool InitFromString(string str)
        {
            string errorMessage;
            bool res = InitFromString(str, out errorMessage);
            ValidationError = errorMessage;
            return res;
        }

        /// <summary>
        /// Initializes from string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns></returns>
        private bool InitFromString(string str, out string errorMessage)
        {
            List<string> items = str.Replace("=", " = ").Split(new char[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries).Select(n => n.Trim()).ToList();

            while (items.Count >= 3)
            {
                string leftOperand = items[0];
                string oper = items[1];
                string rightOperand = items[2];

                if (oper != "=")
                {
                    errorMessage = string.Format("Unexpected operand '{0}' (possible value: '=')", oper);
                    return false;
                }

                if (rightOperand.ToLower() != "true" && rightOperand.ToLower() != "false")
                {
                    errorMessage = string.Format("Unexpected right operand '{0}' (possible values: True/False)", rightOperand);
                    return false;
                }

                string flagAndStatusString = string.Format("{0}{1}{2}", leftOperand, oper, rightOperand);
                FlagAndStatus flagAndStatus = FlagAndStatus.FromString(flagAndStatusString);
                if (!string.IsNullOrEmpty(flagAndStatus.ValidationError))
                {
                    errorMessage = string.Format("{0}: ({1})", flagAndStatus.ValidationError, flagAndStatusString);
                    return false;
                }

                WaitFor.Add(flagAndStatus);

                items.RemoveRange(0, 3);

                if (items.Count >= 4)
                {
                    // first should be Logical Operator
                    if (string.Compare(items.First(), LogicalOperator, true) != 0)
                    {
                        errorMessage = string.Format("Invalid logical operator: '{0}' (expected: '{1}')", items.First(), LogicalOperator);
                        return false;
                    }

                    items.RemoveAt(0);
                }
            }

            if (items.Count != 0)
            {
                errorMessage = string.Format("Unexpected expression: '{0}'", string.Join(" ", items));
                return false;
            }

            errorMessage = null;
            return true;
        }

        /// <summary>
        /// Gets the validation error.
        /// </summary>
        /// <value>
        /// The validation error.
        /// </value>
        public string ValidationError { get; private set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (WaitFor.Count == 0)
                return "";

            return string.Join(" " + LogicalOperator + " ", WaitFor.Select(n => n.ToString()));
        }

        /// <summary>
        /// Gets a value indicating whether [is valid].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is valid]; otherwise, <c>false</c>.
        /// </value>
        public bool IsValid
        {
            get { return WaitFor.Count > 0 && WaitFor.Any(n => n.IsValid); }
        }
    }

    /// <summary>
    /// AND conditions fro continuing execution
    /// </summary>
    public class ContinueTestSuiteExecutionConditions : ContinueOrBreakTestSuiteExecutionConditions
    {
        /// <summary>
        /// Gets the logical operator.
        /// </summary>
        /// <value>
        /// The logical operator.
        /// </value>
        protected override string LogicalOperator
        {
            get { return "AND"; }
        }

        /// <summary>
        /// Creates from a string
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static ContinueTestSuiteExecutionConditions FromString(string str)
        {
            var result = new ContinueTestSuiteExecutionConditions();
            result.InitFromString(str.IsEmptyOrNoValue() ? "" : str);
            return result;
        }
    }

    /// <summary>
    /// OR conditions fro stopping execution
    /// </summary>
    public class BreakTestSuiteExecutionConditions : ContinueOrBreakTestSuiteExecutionConditions
    {
        /// <summary>
        /// Gets the logical operator.
        /// </summary>
        /// <value>
        /// The logical operator.
        /// </value>
        protected override string LogicalOperator
        {
            get { return "OR"; }
        }

        /// <summary>
        /// Creates from a string
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static BreakTestSuiteExecutionConditions FromString(string str)
        {
            var result = new BreakTestSuiteExecutionConditions();
            result.InitFromString(str.IsEmptyOrNoValue() ? "" : str);
            return result;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RaiseFlagEvent
    {
        /// <summary>
        /// Gets or sets the name of the flag.
        /// </summary>
        /// <value>
        /// The name of the flag.
        /// </value>
        public string FlagName { get; set; }

        /// <summary>
        /// Creates from a string
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static RaiseFlagEvent FromString(string str)
        {
            return new RaiseFlagEvent {FlagName = str.IsEmptyOrNoValue() ? "" : str};
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return FlagName ?? "";
        }

        /// <summary>
        /// Gets a value indicating whether [is valid].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is valid]; otherwise, <c>false</c>.
        /// </value>
        public bool IsValid
        {
            get { return FlagName.IsStringContaingOnlyLettersDigitsOrUnderscores(); }
        }

        /// <summary>
        /// Gets the validation error.
        /// </summary>
        /// <value>
        /// The validation error.
        /// </value>
        public string ValidationError
        {
            get
            {
                return FlagName.IsStringContaingOnlyLettersDigitsOrUnderscores()
                    ? null
                    : "Flag name contains invalid characters (must contain only letters, digits or underscores)";
            }
        }
    }
}