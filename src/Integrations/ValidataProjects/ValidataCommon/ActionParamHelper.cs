﻿using System;

namespace ValidataCommon
{
    /// <summary>
    /// Helper for action params, the catalog and typical are now stored in a custom format: [catalog]:typical
    /// </summary>
    public class ActionParamHelper
    {
        private const string DefaultCatalog = "FinancialObjects";

        /// <summary>
        /// Joins the catalog and typical. If the catalog is the default (i.e. FinancialObjects), skip it
        /// </summary>
        /// <param name="catalogName">Name of the catalog.</param>
        /// <param name="entityName">Name of the typical/mapping schema.</param>
        /// <returns></returns>
        public static string CombineCatalogAndEntityNames(string catalogName, string entityName)
        {
            if (catalogName == DefaultCatalog)
            {
                // we need to do this to preserve the working of old ActionParams, which where contained only the entity name 
                // (before introducing of support for multiple catalogs)
                return entityName;
            }
            else
            {
                return "[" + catalogName + "]:" + entityName;
            }
        }

        /// <summary>
        /// Gets the full catalog and entity 
        /// </summary>
        /// <param name="actionParamFO">The action param FO.</param>
        /// <returns></returns>
        public static string GetFullCatalogAndEntityDescription(string actionParamFO)
        {
            if(actionParamFO == "" || actionParamFO == "No Value" )
            {
                return "";
            }

            string catalogName, entityName;
            if (!SplitCatalogAndEntitylNames(actionParamFO, out catalogName, out entityName))
            {
                return "";
            }
            else
            {
               return  "[" + catalogName + "]:" + entityName;
            }
        }

        /// <summary>
        /// Splits the name of the catalog and typical. The catalog is optionally stored
        /// </summary>
        /// <param name="actionParamFO">The fo value.</param>
        /// <param name="catalogName">Name of the catalog.</param>
        /// <param name="enityName">Name of the typical/mapping schema.</param>
        /// <returns></returns>
        public static bool SplitCatalogAndEntitylNames(string actionParamFO, out string catalogName, out string enityName)
        {
            if (string.IsNullOrEmpty(actionParamFO) || actionParamFO == "No Value")
            {
                catalogName = "";
                enityName = "";
                return false;
            }

            string[] parts = actionParamFO.Split(new string[] {"]:"}, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length == 0)
            {
                catalogName = "";
                enityName = "";
                return false;
            }

            if (parts.Length == 1)
            {
                parts = actionParamFO.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
            }

            if (parts.Length == 1)
            {
                // older items don't have catalog, so use the default one
                catalogName = DefaultCatalog;
                enityName = actionParamFO;
                return true;
            }

            if (parts.Length == 2)
            {
                enityName = parts[1];

                string catalogPart = parts[0];
                if (catalogPart.StartsWith("["))
                {
                    catalogName = catalogPart.Substring(1, catalogPart.Length - 1); // strip off the leading [
                }
                else
                {
                    catalogName = catalogPart;
                }

                return true;
            }

            throw new InvalidOperationException("The catalog and typical name cannot be processed");
        }
    }
}