﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Generic parser for adapter parameters
    /// </summary>
    public static class AdapterConfiguration
    {
        public static string AdapterFilesRootPath = Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles";
        /// <summary>
        /// 	Configuration fileds delimiter
        /// </summary>
        private const string ConfigurationFieldDelimiter = ";";

        /// <summary>
        /// 	Field Name / Value delimiter
        /// </summary>
        private const string FieldNameValueDelimiter = "=";

        /// <summary>
        /// 	Splits configuration string to field name/value pairs
        /// </summary>
        /// <param name="configuration"> </param>
        /// <param name="defaultParameterName">Default patameter - if value is not null the name would be used for first parameter w/o specified name</param>
        /// <returns> </returns>
        public static Dictionary<string, string> Parse(string configuration, string defaultParameterName = null)
        {
            const char doubleQuotesReplacement = (char)0x02;

            Dictionary<string, string> result = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(configuration) || configuration.Trim() == string.Empty || configuration.Trim() == "No Value")
            {
                return result;
            }

            // first replace double quotes with special symbol
            configuration = configuration.Replace("\"\"", "" + doubleQuotesReplacement).Trim();

            var cfgFields = SplitToConfigFields(configuration);
            for (int i = 0; i < cfgFields.Count; i++)
            {
                string field = cfgFields[i];
                int idxOfFieldNameValueDelimiter = field.IndexOf(FieldNameValueDelimiter);

                if (idxOfFieldNameValueDelimiter <= 0 && !string.IsNullOrEmpty(defaultParameterName))
                {
                    field = string.Format("{0}{1}{2}", defaultParameterName, idxOfFieldNameValueDelimiter < 0 ? FieldNameValueDelimiter : "", field);
                    idxOfFieldNameValueDelimiter = field.IndexOf(FieldNameValueDelimiter);
                }

                if (idxOfFieldNameValueDelimiter <= 0)
                    throw new ApplicationException(string.Format("Invalid adapter configuration string. Unable to read field value: '{0}'", field));

                string fieldName = field.Substring(0, idxOfFieldNameValueDelimiter).Trim();
                string fieldValue = field.Substring(idxOfFieldNameValueDelimiter + 1).Trim();

                if (fieldValue.StartsWith("\"") && fieldValue.EndsWith("\""))
                {
                    fieldValue = fieldValue.Substring(1, fieldValue.Length - 2);
                }

                result[fieldName.Trim()] = fieldValue.Trim().Replace("" + doubleQuotesReplacement, "\"");
            }

            return result;
        }

        /// <summary>
        ///     Builds adapter configuration string out of parameters
        /// </summary>
        /// <returns></returns>
        public static string Build(params KeyValuePair<string, string>[] fieldValues)
        {
            const string formatFieldNameValue = "{0}" + FieldNameValueDelimiter + "{1}" + ConfigurationFieldDelimiter;

            StringBuilder sbResult = new StringBuilder();

            foreach (var param in fieldValues)
            {
                if (!string.IsNullOrEmpty(param.Value))
                    sbResult.AppendFormat(formatFieldNameValue, param.Key, EscapeFieldValue(param.Value));
            }

            return sbResult.ToString();
        }

        /// <summary>
        /// 	Split configuration to fields
        /// </summary>
        /// <returns> </returns>
        private static List<string> SplitToConfigFields(string configuration)
        {
            List<string> result = new List<string>();

            if (string.IsNullOrEmpty(configuration) || configuration.Trim() == string.Empty)
            {
                return result;
            }

            string cfg = configuration.Trim();

            if (!cfg.EndsWith(ConfigurationFieldDelimiter))
            {
                cfg += ConfigurationFieldDelimiter;
            }

            bool hasOpenQuotation = false;
            string currentItem = string.Empty;
            foreach (char c in cfg)
            {
                if (c == '\"')
                {
                    hasOpenQuotation = !hasOpenQuotation;
                }
                else if (c == ConfigurationFieldDelimiter[0])
                {
                    if (!hasOpenQuotation)
                    {
                        if (currentItem.Trim() != string.Empty)
                        {
                            currentItem = currentItem.Trim();
                            result.Add(currentItem.Trim());
                        }

                        currentItem = string.Empty;
                        continue;
                    }
                }

                currentItem += c;
            }

            if (currentItem.Trim() != string.Empty)
            {
                throw new ApplicationException(string.Format("Invalid adapter configuration string. Unable to read field value: '{0}'", currentItem.Trim()));
            }

            return result;
        }

        /// <summary>
        /// 	Escapes with quotes the string in case it contains 'ConfigurationFieldDelimiter' or 'FieldNameValueDelimiter' delimiters
        /// </summary>
        /// <returns> </returns>
        private static string EscapeFieldValue(string value)
        {
            if (value.IndexOf(ConfigurationFieldDelimiter) >= 0
                || value.IndexOf(FieldNameValueDelimiter) >= 0)
            {
                // We have to quote the string
                return string.Format("\"{0}\"", value.Replace("\"", "\"\""));
            }

            return value;
        }
    }
}
