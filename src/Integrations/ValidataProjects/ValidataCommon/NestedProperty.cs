﻿using System.Collections.Generic;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// A key-value property that allows nested key-value properties
    /// </summary>
    public class NestedProperty
    {
        /// <summary>
        /// The key of the property 
        /// </summary>
        public string Key;

        /// <summary>
        /// The value of the property 
        /// </summary>
        public string Value;

        /// <summary>
        /// Whether to visualize or not the value
        /// </summary>
        public bool HideValue;

        /// <summary>
        /// Child properties
        /// </summary>
        public List<NestedProperty> Children = new List<NestedProperty>();

        /// <summary>
        /// Initializes a new instance of the <see cref="NestedProperty"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        public NestedProperty(string key)
        {
            Key = key;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NestedProperty"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public NestedProperty(string key, string value)
        {
            Key = key;
            Value = value;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is empty.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is empty; otherwise, <c>false</c>.
        /// </value>
        public bool IsEmpty
        {
            get { return string.IsNullOrWhiteSpace(Value) && Children.Count == 0 && !HideValue; }
        }

        /// <summary>
        /// Gets the full description.
        /// </summary>
        public string FullDescription
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                //sb.AppendFormat("<b>{0}</b>", Key);
                sb.Append(Key);

                if (!HideValue)
                {
                    if (string.IsNullOrWhiteSpace(Value))
                        sb.Append(":");
                    else
                        sb.Append(" = ").Append(Value);
                }

                foreach (var child in Children)
                {
                    if (child.IsEmpty)
                        continue;
                    sb.AppendLine();
                    sb.Append("    ");
                    sb.Append(child.FullDescription);
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return FullDescription;
        }

        /// <summary>
        /// Add new child to children of the property
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public NestedProperty AddChild(string key, string value = null)
        {
            var np = new NestedProperty(key, value);
            this.Children.Add(np);
            return np;
        }

        /// <summary>
        /// Add new child to children of the property
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The boolean value.</param>
        /// <returns></returns>
        public NestedProperty AddChild(string key, bool value)
        {
            return AddChild(key, value ? "Yes" : "No");
        }

        /// <summary>
        /// Add new child to children of the property
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">Multiple values.</param>
        /// <returns></returns>
        public NestedProperty AddChild(string key, IEnumerable<string> value)
        {
            return AddChild(key, string.Join("; ", (value ?? new List<string>())));
        }
    }
}