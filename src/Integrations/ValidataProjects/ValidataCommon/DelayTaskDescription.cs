﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ValidataCommon
{
    [Serializable]
    public class DelayTaskDescription
    {
        public DelayTaskDescription()
        {
        }

        public int Delay { get; set; }

        public static DelayTaskDescription DeserializeFromString(string serialized)
        {
            if (!string.IsNullOrEmpty(serialized))
            {
                try
                {
                    return new XmlSerializer(typeof(DelayTaskDescription)).Deserialize(new StringReader(serialized)) as DelayTaskDescription;
                }
                catch(InvalidOperationException)
                {
                    ApplicationException exp = new ApplicationException("The delay task description cannot been successfully read.");
                    exp.Data.Add("serialized", serialized);
                    throw exp;
                }
            }

            return new DelayTaskDescription();
        }

        public string SerializeToString()
        {
            StringWriter sw = new StringWriter();
             new XmlSerializer(GetType()).Serialize(sw, this);
            return sw.ToString();
        }
    }
}
