﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Parse CSV data
    /// </summary>
    public class CsvParser
    {
        /// <summary>
        /// Parses the fields and pushes the fields into the result arraylist
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="separator">The separator</param>
        /// <returns></returns>
        public static List<string> ParseCsvFields(string data, char separator)
        {
            List<string> result = new List<string>();
            int pos = -1;
            while (pos < data.Length)
            {
                result.Add(ParseCsvField(data, separator, ref pos));
            }
            return result;
        }

        /// <summary>
        /// Parses the field at the given position of the data, modified pos to match
        /// the first unparsed position and returns the parsed field
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="separator">The separator</param>
        /// <param name="startSeparatorPosition">The start separator position.</param>
        /// <returns></returns>
        private static string ParseCsvField(string data, char separator, ref int startSeparatorPosition)
        {
            if (startSeparatorPosition == data.Length - 1)
            {
                startSeparatorPosition++;
                return ""; // The last field is empty
            }

            int fromPos = startSeparatorPosition + 1;

            // Determine if this is a quoted field
            if (data[fromPos] == '"')
            {
                // If we're at the end of the string, let's consider this a field that only contains the quote
                if (fromPos == data.Length - 1)
                {
                    return "\"";
                }

                // Otherwise, return a string of appropriate length with double quotes collapsed
                // Note that FSQ returns data.Length if no single quote was found
                int nextSingleQuote = FindSingleQuote(data, fromPos + 1);
                startSeparatorPosition = nextSingleQuote + 1;
                return data.Substring(fromPos + 1, nextSingleQuote - fromPos - 1).Replace("\"\"", "\"");
            }

            // The field ends in the next separator or EOL
            int nextSeparator = data.IndexOf(separator, fromPos);
            if (nextSeparator == -1)
            {
                startSeparatorPosition = data.Length;
                return data.Substring(fromPos);
            }
            else
            {
                startSeparatorPosition = nextSeparator;
                return data.Substring(fromPos, nextSeparator - fromPos);
            }
        }

        /// <summary>
        /// Returns the index of the next single quote mark in the string (starting from startFrom).
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="startFrom">The start from.</param>
        /// <returns></returns>
        private static int FindSingleQuote(string data, int startFrom)
        {
            int i = startFrom - 1;
            while (++i < data.Length)
            {
                if (data[i] == '"')
                {
                    // If this is a double quote, bypass the chars
                    if (i < data.Length - 1 && data[i + 1] == '"')
                    {
                        i++;
                        continue;
                    }
                    else
                    {
                        return i;
                    }
                }
            }

            // If no quote found, return the end value of i (data.Length)
            return i;
        }
    }
}
