﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ValidataCommon.Interfaces;

namespace ValidataCommon
{
    /// <summary>
    /// Helper class for read/write screenshots
    /// </summary>
    public class ScreenshotHelper
    {
        /// <summary>
        /// Screenshots root folder
        /// </summary>
        public static readonly  string SCREENSHOTS_PATH = Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\Logs\Screenshots";

        public const string DateFormat = "HH-mm-ss.fff";

        /// <summary>
        /// Load screenshots
        /// </summary>
        /// <param name="cycleId">test cycle id</param>
        /// <param name="testStepId">test step id</param>
        /// <returns></returns>
        public static List<ScreenshotItem> LoadScreenshots(string cycleId, string testStepId)
        {
            string path = GetScreenshotPath(cycleId, testStepId);

            if (!Directory.Exists(path))
                return new List<ScreenshotItem>();

            List<ScreenshotItem> screenshots = new List<ScreenshotItem>();
            List<string> extensionsToShow = GetExtensionsToShow();

            foreach (string file in Directory.GetFiles(path, "*.*"))
            {
                string extension = Path.GetExtension(file);
                if (string.IsNullOrEmpty(extension))
                    continue;
                if (!extensionsToShow.Contains(extension.ToLower()))
                    continue;
                screenshots.Add(new ScreenshotItem() { CycleID = cycleId, TestStepID = testStepId, Name = Path.GetFileName(file) });
            }

            return screenshots;
        }

        /// <summary>
        /// TODO make it read extension form testengine.exe.config if required
        /// </summary>
        /// <returns></returns>
        private static List<string> GetExtensionsToShow()
        {
            string[] extensions = { ".jpg", ".bmp", ".png", ".gif", ".tif" };
            return extensions.ToList<string>();
        }

        /// <summary>
        /// Returns screenshots path for specific cycle and step
        /// </summary>
        /// <param name="cycleId">test cycle id</param>
        /// <param name="testStepId">test step id</param>
        /// <returns></returns>
        public static string GetScreenshotPath(string cycleId, string testStepId)
        {
            return Path.Combine(SCREENSHOTS_PATH, String.Format("{0}\\{1}", cycleId, testStepId));
        }
        /// <summary>
        /// CopyScreenshots From one testcase To another
        /// </summary>
        /// <param name="fromCycleId"></param>
        /// <param name="fromTestStepId"></param>
        /// <param name="toCycleId"></param>
        /// <param name="toTestStepId"></param>
        public static void CopyScreenshotsFromTo(string fromCycleId, string fromTestStepId, string toCycleId, string toTestStepId)
        {
            var pathFrom = new DirectoryInfo(GetScreenshotPath(fromCycleId, fromTestStepId));
            if (!Directory.Exists(pathFrom.FullName))
                return; //there isn't any screenshots
            var pathTo = GetScreenshotPath(toCycleId, toTestStepId);
            
            if (Directory.Exists(pathTo))
                Directory.Delete(pathTo, true);
            Directory.CreateDirectory(pathTo);

            foreach (var fileInfo in pathFrom.GetFiles())
                fileInfo.CopyTo(Path.Combine(pathTo, fileInfo.Name));
        }
        /// <summary>
        /// Get Screenshot Level
        /// </summary>
        /// <param name="scrValue">None, Error, Full</param>
        /// <returns></returns>
        public static ScreenshotLevel GetScreenshotLevel(string scrValue)
        {
            if (String.IsNullOrEmpty(scrValue))
                return ScreenshotLevel.None;

            string screenshotValue = scrValue.Trim();
            switch (screenshotValue.ToLower())
            {
                case "error":
                    return ScreenshotLevel.Error;

                case "full":
                    return ScreenshotLevel.Full;

                case "none":
                default:
                    return ScreenshotLevel.None;
            }
        }

        /// <summary>
        /// Saves screenshot for specific suite and step.
        /// </summary>
        /// <param name="testSuiteId">The screenshot cycle.</param>
        /// <param name="testStepId">The screenshot step.</param>
        /// <param name="screenshot">The screenshot to save.</param>
        public static void Save(IAdapterExecutionContext executionContext, ScreenshotInfo screenshot)
        {
            if (String.IsNullOrEmpty(executionContext.TestCycleID) ||
                screenshot == null || screenshot.Image == null || screenshot.ImageFormat == null)
            {
                return;
            }

            string directory = GetScreenshotPath(executionContext.TestCycleID, executionContext.TestStepID.ToString());

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            string date = screenshot.DateTime.ToString(DateFormat);
            string format = screenshot.ImageFormat.ToString().ToLower();

            string fileName = String.Format("{0}\\{1}.{2}", directory, date, format);
            screenshot.Image.Save(fileName);
        }
    }
}
