﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ValidataCommon
{
    public class MD5HashManager
    {
        /// <summary>
        /// Carriage Return (ASCII 13)
        /// </summary>
        private const char CR = (char) 13;

        /// <summary>
        /// Line Feed (ASCII 10)
        /// </summary>
        private const char LF = (char) 10;

        /// <summary>
        /// Removes Carriage Returns and compute MD5 hash code
        /// </summary>
        /// <param name="filePath">File path to compute hash</param>
        /// <returns>MD5 hash code</returns>
        public string RemoveCRComputeFileHash(string filePath)
        {
            StringBuilder content = new StringBuilder();
            //TODO: Do somethig else if the file is binary
            using (StreamReader reader = new StreamReader(filePath, Encoding.Default))
            {
                while (reader.Peek() >= 0)
                {
                    char ch = (char) reader.Read();
                    if (ch == CR)
                    {
                        continue;
                    }

                    content.Append(ch);
                }
            }

            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5.ComputeHash(Encoding.Default.GetBytes(content.ToString())))
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }

        /// <summary>
        /// Removes Carriage Returns and compute MD5 hash code
        /// </summary>
        /// <param name="reader">File stream reader to compute hash</param>
        /// <returns>MD5 hash code</returns>
        public string RemoveCRComputeFileHash(StreamReader reader)
        {
            StringBuilder content = new StringBuilder();
            //TODO: Do somethig else if the file is binary
            while (reader.Peek() >= 0)
            {
                char ch = (char) reader.Read();
                if (ch == CR)
                {
                    continue;
                }

                content.Append(ch);
            }

            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5.ComputeHash(Encoding.Default.GetBytes(content.ToString())))
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }

        /// <summary>
        /// Removes Carriage Returns and compute MD5 hash code
        /// </summary>
        /// <param name="binaryData">Binary Data</param>
        /// <returns>MD5 hash code</returns>
        public string RemoveCRComputeFileHash(byte[] binaryData)
        {
            string str = Encoding.Default.GetString(binaryData);

            StringBuilder content = new StringBuilder();
            //TODO: Do somethig else if the file is binary
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == CR)
                {
                    continue;
                }

                content.Append(str[i]);
            }

            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5.ComputeHash(Encoding.Default.GetBytes(content.ToString())))
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }

        /// <summary>
        /// Compute MD5 hash for the specified file
        /// </summary>
        /// <param name="filePath">File path to compute hash</param>
        /// <returns>MD5 hash code</returns>
        public string ComputeFileHash(string filePath)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                foreach (byte b in md5.ComputeHash(fs))
                {
                    sb.Append(b.ToString("x2").ToLower());
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Compute MD5 hash for the specified file stream
        /// </summary>
        /// <param name="fs">File stream to compute hash</param>
        /// <returns>MD5 hash code</returns>
        public string ComputeFileHash(FileStream fs)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5.ComputeHash(fs))
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }

        /// <summary>
        /// Compute MD5 hash for the binary array
        /// </summary>
        /// <param name="binaryData">Binary Data</param>
        /// <returns>MD5 hash code</returns>
        public string ComputeFileHash(byte[] binaryData)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5.ComputeHash(binaryData))
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }

        /// <summary>
        /// Compute MD5 hash for the specified string
        /// </summary>
        /// <param name="content">String content to compute hash</param>
        /// <returns>MD5 hash code</returns>
        public string ComputeStringHash(string content)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5.ComputeHash(Encoding.Default.GetBytes(content)))
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }


        /// <summary>
        /// Adds Carriage Returns and Line Feeds and computes MD5 hash code
        /// </summary>
        /// <param name="filePath">File path to compute hash</param>
        /// <param name="skipCR">Skip CR</param>
        /// <returns>MD5 hash code</returns>
        public string AddCRLFComputeFileHash(string filePath, bool skipCR)
        {
            StringBuilder content = new StringBuilder();
            //TODO: Do somethig else if the file is binary
            using (StreamReader reader = new StreamReader(filePath, Encoding.Default))
            {
                while (reader.Peek() >= 0)
                {
                    char ch = (char) reader.Read();
                    if (skipCR && ch == CR)
                    {
                        continue;
                    }

                    if (ch == CR || ch == LF)
                    {
                        content.Append(CR);
                        content.Append(LF);
                        continue;
                    }

                    content.Append(ch);
                }
            }

            if (!skipCR)
            {
                content.Append(CR);
            }
            content.Append(LF);

            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5.ComputeHash(Encoding.Default.GetBytes(content.ToString())))
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }

        /// <summary>
        /// Adds Carriage Returns and Line Feeds and computes MD5 hash code
        /// </summary>
        /// <param name="reader">File stream reader to compute hash</param>
        /// <param name="skipCR">Skip CR</param>
        /// <returns>MD5 hash code</returns>
        public string AddCRLFComputeFileHash(StreamReader reader, bool skipCR)
        {
            StringBuilder content = new StringBuilder();
            //TODO: Do somethig else if the file is binary
            while (reader.Peek() >= 0)
            {
                char ch = (char) reader.Read();
                if (skipCR && ch == CR)
                {
                    continue;
                }

                if (ch == CR || ch == LF)
                {
                    content.Append(CR);
                    content.Append(LF);
                    continue;
                }

                content.Append(ch);
            }

            if (!skipCR)
            {
                content.Append(CR);
            }
            content.Append(LF);

            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5.ComputeHash(Encoding.Default.GetBytes(content.ToString())))
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }

        /// <summary>
        /// Adds Carriage Returns and Line Feeds and computes MD5 hash code
        /// </summary>
        /// <param name="binaryData">Binary Data</param>
        /// <param name="skipCR">skip CR</param>
        /// <returns>MD5 hash code</returns>
        public string AddCRLFComputeFileHash(byte[] binaryData, bool skipCR)
        {
            string str = Encoding.Default.GetString(binaryData);

            StringBuilder content = new StringBuilder();
            //TODO: Do somethig else if the file is binary
            for (int i = 0; i < str.Length; i++)
            {
                if (skipCR && str[i] == CR)
                {
                    continue;
                }
                if (str[i] == CR || str[i] == LF)
                {
                    content.Append(CR);
                    content.Append(LF);
                    continue;
                }
                content.Append(str[i]);
            }

            if (!skipCR)
            {
                content.Append(CR);
            }
            content.Append(LF);

            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5.ComputeHash(Encoding.Default.GetBytes(content.ToString())))
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }
    }
}