﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace ValidataCommon
{
    public class KeyValueItem
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    [XmlRoot(ElementName="NameValueList")]
    public class NameValueXMLWrapper : List<KeyValueItem>
    {
        public static NameValueXMLWrapper CreateFromString(string sourceXML)
        {
            NameValueXMLWrapper nameValueStorage;

            if (!string.IsNullOrEmpty(sourceXML) && File.Exists(sourceXML))
            {
                try
                {
                    // Create an instance of the XmlSerializer class;
                    // specify the type of object to be deserialized.
                    XmlSerializer serializer = new XmlSerializer(typeof(NameValueXMLWrapper));
                    using (StreamReader sr = new StreamReader(sourceXML))
                    {
                        XmlReader xrRoot = XmlReader.Create(sr);
                        object deserialized = serializer.Deserialize(xrRoot);
                        nameValueStorage = deserialized as NameValueXMLWrapper;
                    }
                }
                catch (Exception)
                {
                    nameValueStorage = new NameValueXMLWrapper();
                }
            }
            else
            {
                nameValueStorage = new NameValueXMLWrapper();
            }

            return nameValueStorage;
        }

        /// <summary>
        /// Creates an XML string with the name/value data.
        /// </summary>
        /// <returns></returns>
        public string WriteXML()
        {
            if (Count > 0)
            {
                StringBuilder sbHelper = new StringBuilder();
                //  Serialize data
                using (XmlWriter writer = XmlWriter.Create(sbHelper))
                {
                    XmlSerializer xrModule = new XmlSerializer(this.GetType());
                    xrModule.Serialize(writer, this);
                    writer.Close();
                }
                return sbHelper.ToString();
            }
            else
                return string.Empty;
        }

        /// <summary>
        /// Add new key-value pair
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(string key, string value)
        {
            this.Add(new KeyValueItem { Key = key, Value = value });
        }
    }
}
