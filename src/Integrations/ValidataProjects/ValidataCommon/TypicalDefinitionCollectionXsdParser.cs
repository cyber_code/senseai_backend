﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using Validata.Common;

namespace ValidataCommon
{
    /// <summary>
    /// Parses XSD files to typical definitions
    /// </summary>
    public static class TypicalDefinitionCollectionXsdParser
    {
        private const int THRESHOLD_BYTES_TO_USE_CUSTOM_XSD_PARSING = 500000;

        /// <summary>
        /// Gets a list of typicals from the XSD schema.
        /// </summary>
        /// <param name="xsdStream">The XSD stream.</param>
        /// <returns></returns>
        public static TypicalDefinitionCollection ReadSchema(Stream xsdStream)
        {
            // this is done to try to fix errors in manually created XSD files (but might come with a small performance cost).
            MemoryStream encodedNamesStream = GetStreamWithInvalidNamesProperlyEncoded(xsdStream);

            if (encodedNamesStream.Length > THRESHOLD_BYTES_TO_USE_CUSTOM_XSD_PARSING)
            {
                // The default Microsoft implementation with Datasets has issues with huge XSD files (eats a lot of memory, very slow & could lead to a crash). So use a custom reading instead.
                // We currently keep the old Microsoft parsing logic for smaller XSDs to minimize the risk with possible issues in the new implementation
                return ReadSchemaCustom(encodedNamesStream);
            }

            return ReadSchemaWithDataSet(encodedNamesStream);
        }

        private static TypicalDefinitionCollection ReadSchemaWithDataSet(Stream xsdStream)
        {
            DataSet ds = new DataSet();
            ds.Locale = CultureInfo.InvariantCulture;
            ds.ReadXmlSchema(xsdStream);
            return FromDataSet(ds);
        }

        /// <summary>
        /// Encode special characters in the attribute names
        /// </summary>
        /// <param name="xsdStream"></param>
        /// <returns></returns>
        private static MemoryStream GetStreamWithInvalidNamesProperlyEncoded(Stream xsdStream)
        {
            var doc = new XmlDocument();
            doc.Load(xsdStream);

            XmlNodeList nodeList1 = doc.GetElementsByTagName("xs:element");

            foreach (XmlNode element in nodeList1)
            {
                if (element.Attributes != null)
                {
                    XmlNode attr = element.Attributes.GetNamedItem("name");
                    if (attr != null)
                    {
                        try
                        {
                            XmlConvert.VerifyName(attr.Value);
                        }
                        catch (XmlException)
                        {
                            // only change the value if has been invalid (most likely manually created by a user)
                            attr.Value = XmlConvert.EncodeName(attr.Value);
                        }
                    }
                }
            }

            var ms = new MemoryStream();
            doc.Save(ms);
            ms.Position = 0;

            return ms;
        }

        private static TypicalDefinitionCollection FromDataSet(DataSet ds)
        {
            var result = new TypicalDefinitionCollection();
            foreach (DataTable table in ds.Tables)
            {
                if (table.TableName == "dataroot")
                {
                    continue;
                }

                result.Add(new TypicalDefinition(table));
            }

            return result;
        }

        /// <summary>
        /// Reads the schema.
        /// </summary>
        /// <param name="xsdString">The XSD string.</param>
        /// <returns></returns>
        public static TypicalDefinitionCollection ReadSchema(string xsdString)
        {
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(xsdString)))
            {
                return ReadSchema(ms);
            }
        }

        /// <summary>
        /// Reads the schema (manual implementation, not using Microsoft implementation).
        /// </summary>
        /// <param name="xsdStream">The XSD stream.</param>
        /// <returns></returns>
        public static TypicalDefinitionCollection ReadSchemaCustom(Stream xsdStream)
        {
            TypicalDefinitionCollection typCol = new TypicalDefinitionCollection();
            typCol.Clear();

            var doc = new XmlDocument
            {
                PreserveWhitespace = true
            };
            doc.Load(xsdStream);

            XmlNamespaceManager xmlnsManager = new XmlNamespaceManager(doc.NameTable);

            // Add the namespaces used in books.xml to the XmlNamespaceManager.
            xmlnsManager.AddNamespace("xs", "http://www.w3.org/2001/XMLSchema");
            xmlnsManager.AddNamespace("msdata", "urn:schemas-microsoft-com:xml-msdata");
            xmlnsManager.AddNamespace("msprop", "urn:schemas-microsoft-com:xml-msprop");

            XmlNodeList typicalNodes = doc.SelectNodes("xs:schema/xs:element/xs:complexType/xs:choice/xs:element", xmlnsManager);
            if (typicalNodes == null)
            {
                return typCol;
            }

            foreach (XmlNode typicalNode in typicalNodes)
            {
                
                // parse attributes name and msprop:Catalog
                XmlAttribute nameAttrib = typicalNode.Attributes["name"];
                XmlAttribute catalogAttrib = typicalNode.Attributes["msprop:Catalog"];
                XmlAttribute baseclassAttrib = typicalNode.Attributes["msprop:BaseClass"];

                TypicalDefinition typDef = new TypicalDefinition
                {
                    Xml = typicalNode.OuterXml,
                    Name = GetXmlValue(nameAttrib),
                    CatalogName = GetXmlValue(catalogAttrib),
                    BaseClass = baseclassAttrib != null ? GetXmlValue(baseclassAttrib) : null
                };

                typCol.Add(typDef);

                // get inner xs:element
                XmlNodeList attrNodes = typicalNode.SelectNodes("xs:complexType/xs:sequence/xs:element", xmlnsManager);
                if (attrNodes == null)
                {
                    continue;
                }

                foreach (XmlNode attrNode in attrNodes)
                {
                    //  example of attributes element:
                    /* <xs:element 
                                 name="Test_x0020_2" 
                                 msprop:DataLength="20" 
                                 msprop:MaxValue="100"
                                 msprop:Status="Mandatory" 
                                 msprop:Prefix="Prefix" 
                                 msprop:LookupValues="Ala,Bala" 
                                 msprop:MinValue="5"
                                 msprop:Ignore="True" 
                                 msprop:IsMultiValue="True" 
                                 msprop:PaddingChar="P" 
                                 msprop:SuperMultipleValuesCount="2" 
                                 msprop:Remarks="Remarks" 
                                 msprop:Description="Description" 
                                 msprop:MultipleValuesCount="5" 
                                 msprop:Tolerance="4" 
                                 msprop:DefaultValue="88" 
                                 type="xs:string" 
                                 minOccurs="0" /> */

                    // var minOccursAttrib = innerNode.Attributes["minOccurs"]; // xsd specific only, not used for now71
                    AttributeDefinition attrDef;
                    if (typDef.BaseClass == "T24_AAProduct")
                    {
                        attrDef = new PropertyFieldAttributeDefinition
                        {
                            IsMultipleProperty = GetBoolValue(attrNode.Attributes["msprop:IsMultipleProperty"]),
                            MultiplePropertyCount = GetUshortValue(attrNode.Attributes["msprop:MultiplePropertyCount"])
                        };
                    }
                    else
                    {
                        attrDef = new AttributeDefinition();
                    }

                    attrDef.Name = GetXmlValue(attrNode.Attributes["name"]);

                    //TODO are the rest this really XML encoded?
                    attrDef.DataType = GetAttributeDataType(GetXmlValue(attrNode.Attributes["type"]));
                    attrDef.Status = GetAttributeStatus(GetXmlValue(attrNode.Attributes["msprop:Status"]));
                    attrDef.DefaultValue = GetXmlValue(attrNode.Attributes["msprop:DefaultValue"]); 
                    attrDef.MinValue = GetXmlValue(attrNode.Attributes["msprop:MinValue"]);
                    attrDef.MaxValue = GetXmlValue(attrNode.Attributes["msprop:MaxValue"]);
                    attrDef.Remarks = GetXmlValue(attrNode.Attributes["msprop:Remarks"]);
                    attrDef.Description = GetXmlValue(attrNode.Attributes["msprop:Description"]);
                    attrDef.Prefix = GetXmlValue(attrNode.Attributes["msprop:Prefix"]);
                    attrDef.PaddingChar = GetXmlValue(attrNode.Attributes["msprop:PaddingChar"]);
                    attrDef.GroupId = GetUlongValue(attrNode.Attributes["msprop:GroupId"]);
                    attrDef.Tolerance = GetUlongValue(attrNode.Attributes["msprop:Tolerance"]);
                    attrDef.DataLength = GetUlongValue(attrNode.Attributes["msprop:DataLength"]);

                    // some XSDs generated by the TypicalUpdater can have those values too
                    attrDef.IsMultiValue = GetBoolValue(attrNode.Attributes["msprop:IsMultiValue"]);
                    attrDef.MultipleValuesCount = GetUshortValue(attrNode.Attributes["msprop:MultipleValuesCount"]);
                    ushort superMultipleValuesCount = GetUshortValue(attrNode.Attributes["msprop:SuperMultipleValuesCount"]);
                    attrDef.SuperMultipleValuesCount = (superMultipleValuesCount == 0) ? (ushort) 1 : superMultipleValuesCount;

                    string lookupValuesStr = GetXmlValue(attrNode.Attributes["msprop:LookupValues"]);
                    if (!string.IsNullOrEmpty(lookupValuesStr))
                    {
                        string[] lookupValues = lookupValuesStr.Split(',');
                        attrDef.LookupValues = lookupValues;
                    }

                    XmlAttribute ignoreAttrib = attrNode.Attributes["msprop:Ignore"];
                    if (ignoreAttrib != null)
                    {
                        string ignore = GetXmlValue(ignoreAttrib);
                        attrDef.Ignore = bool.Parse(ignore);
                    }

                    typDef.Attributes.Add(attrDef);
                }
            }

            return typCol;
        }

        private static bool GetBoolValue(XmlAttribute attrib)
        {
            string data = GetXmlValue(attrib);
            if (!string.IsNullOrEmpty(data))
            {
                bool value;
                if (bool.TryParse(data, out value))
                {
                    return value;
                }
            }
            return false;
        }

        private static ushort GetUshortValue(XmlAttribute attrib)
        {
            string data = GetXmlValue(attrib);
            if (!string.IsNullOrEmpty(data))
            {
                ushort value;
                if (ushort.TryParse(data, out value))
                {
                    return value;
                }
            }
            return 0;
        }

        private static ulong GetUlongValue(XmlAttribute attrib)
        {
            string data = GetXmlValue(attrib);
            if (!string.IsNullOrEmpty(data))
            {
                ulong value;
                if (ulong.TryParse(data, out value))
                {
                    return value;
                }
            }
            return 0;
        }

        private static AttributeDataType GetAttributeDataType(string dataType)
        {
            if (string.IsNullOrEmpty(dataType))
            {
                return AttributeDataType.String; // Default type - String
            }

            // TODO what about "measure"

            /*
                type="xs:string" - string
                type="xs:long" - integer
                type="xs:double" - real
                type="xs:boolean" - boolean
                type="xs:decimal" - currency
                type="xs:dateTime" - datetime
                type="xs:base64Binary" - blob
             */

            switch (dataType)
            {
                case "xs:string":
                    return AttributeDataType.String;
                case "xs:long":
                    return AttributeDataType.Integer;
                case "xs:double":
                    return AttributeDataType.Real;
                case "xs:boolean":
                    return AttributeDataType.Boolean;
                case "xs:decimal":
                    return AttributeDataType.Currency;
                case "xs:dateTime":
                    return AttributeDataType.DateTime;
                case "xs:base64Binary":
                    return AttributeDataType.Blob;
                default:
                    return AttributeDataType.Unknown;
            }
        }

        private static AttributeStatus GetAttributeStatus(string status)
        {
            if (string.IsNullOrEmpty(status))
            {
                return AttributeStatus.Optional;
            }
            return (AttributeStatus) Enum.Parse(typeof (AttributeStatus), status);
        }

        private static string GetXmlValue(XmlAttribute attribute)
        {
            if (attribute == null)
            {
                return string.Empty;
            }

            return XmlConvert.DecodeName(attribute.Value);
        }
    }
}