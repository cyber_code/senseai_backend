﻿using System;
using System.Globalization;

namespace ValidataCommon
{
    /// <summary>
    /// T24 Date parser
    /// </summary>
    public class T24DateParser
    {
        /// <summary>
        /// Default T24 Format
        /// </summary>
        public const string T24_DATE_FORMAT = "yyyyMMdd";

        /// <summary>
        /// Alternative format
        /// </summary>
        public const string T24_DATE_FORMAT_ALT = "dd MMM yyyy";

        /// <summary>
        /// Parses the date time.
        /// </summary>
        /// <param name="inputDate">The input date.</param>
        /// <param name="resultDate">The result date.</param>
        /// <returns></returns>
        public static bool TryParse(string inputDate, out DateTime resultDate)
        {
            if (!DateTime.TryParseExact(inputDate, T24_DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out resultDate))
            {
                if (!DateTime.TryParseExact(inputDate, T24_DATE_FORMAT_ALT, CultureInfo.InvariantCulture, DateTimeStyles.None, out resultDate))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Parses the specified input date.
        /// </summary>
        /// <param name="inputDate">The input date.</param>
        /// <returns></returns>
        public static DateTime Parse(string inputDate)
        {
            DateTime dt;
            if (! TryParse(inputDate, out dt))
            {
                throw new ApplicationException("The date " + inputDate + " is not a valid T24 date");
            }
            return dt;
        }
    }
}