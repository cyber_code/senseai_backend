﻿namespace ValidataCommon
{
    /// <summary>
    /// 	DB related constants
    /// </summary>
    public static class DbConstants
    {
        /// <summary>
        /// The maximum length of string attribute values (determined by Oracle / MSSQL Server limitations)
        /// </summary>
        public const int MaxLengthOfStringAttribute = 4000;
    }
}