﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Validata.Common;

namespace ValidataCommon
{
    /// <summary>
    /// An attribute definition is a representation of a Validata attribute
    /// </summary>
    [Serializable]
    public class AttributeDefinition : ICloneable, IMetadataAttribute
    {
        /// <summary>
        /// Default multi values count
        /// </summary>
        public const ushort DefaultMultiValuesCount = 4;

        /// <summary>
        /// Separator for lookup values
        /// </summary>
        public const char LookupValuesSeparator = ',';

        #region Public properties

        /// <summary>
        /// Name
        /// </summary>
        public string Name
        {
            get { return GetNSV(_Name); }
            set { _Name = value; }
        }

        /// <summary>
        /// DisplayName
        /// </summary>
        public string DisplayName
        {
            get;
            set;
        }

        /// <summary>
        /// Minimum Value
        /// </summary>
        public string MinValue
        {
            get { return GetNSV(_MinValue); }
            set { _MinValue = value; }
        }

        /// <summary>
        /// Maximum Value
        /// </summary>
        public string MaxValue
        {
            get { return GetNSV(_MaxValue); }
            set { _MaxValue = value; }
        }

        /// <summary>
        /// Default Value
        /// </summary>
        public string DefaultValue
        {
            get { return GetNSV(_DefaultValue); }
            set { _DefaultValue = value; }
        }

        /// <summary>
        /// Data Length
        /// </summary>
        public ulong DataLength
        {
            get { return _DataLength; }
            set { _DataLength = value; }
        }

        /// <summary>
        /// Padding Character
        /// </summary>
        public string PaddingChar
        {
            get { return GetNSV(_PaddingChar); }
            set { _PaddingChar = value; }
        }

        /// <summary>
        /// Data Type
        /// </summary>
        public AttributeDataType DataType
        {
            get { return _DataType; }
            set { _DataType = value; }
        }

        /// <summary>
        /// Status
        /// </summary>
        public AttributeStatus Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        /// <summary>
        /// Tolerance
        /// </summary>
        public double Tolerance
        {
            get { return _Tolerance; }
            set { _Tolerance = value; }
        }

        /// <summary>
        /// Attribute Prefix 
        /// </summary>
        public string Prefix
        {
            get { return GetNSV(_Prefix); }
            set { _Prefix = value; }
        }

        /// <summary>
        /// Description
        /// </summary>
        public string Description
        {
            get { return GetNSV(_Description); }
            set { _Description = value; }
        }

        /// <summary>
        /// Comments
        /// </summary>
        public string Remarks
        {
            get { return GetNSV(_Remarks); }
            set { _Remarks = value; }
        }

        /// <summary>
        /// Flag if the field supports multiple values (to be expanded in MultipleValuesCount fields)
        /// </summary>
        public bool IsMultiValue
        {
            get { return _IsMultiValue; }
            set { _IsMultiValue = value; }
        }

        /// <summary>
        /// Count of multiple values fields
        /// </summary>
        public ushort MultipleValuesCount
        {
            get { return _MultipleValuesCount; }
            set { _MultipleValuesCount = value; }
        }

        /// <summary>
        /// Count of multi-multi values fields
        /// </summary>
        public ushort SuperMultipleValuesCount
        {
            get { return _SuperMultipleValuesCount; }
            set { _SuperMultipleValuesCount = value; }
        }

        /// <summary>
        /// Ignore the attribute when updating
        /// </summary>
        public bool Ignore
        {
            get { return _Ignore; }
            set { _Ignore = value; }
        }

        /// <summary>
        /// Group Id
        /// </summary>
        /// <value></value>
        public ulong GroupId
        {
            get { return _GroupId; }
            set { _GroupId = value; }
        }

        /// <summary>
        /// Look up values list
        /// </summary>
        /// <value></value>
        public string[] LookupValues
        {
            get { return _LookupValues; }
            set { _LookupValues = value; }
        }

        public ulong DisciplineNoderef
        {
            get { return _DisciplineNoderef; }
            set { _DisciplineNoderef = value; }
        }

        public ulong UserNoderef
        {
            get { return _UserNoderef; }
            set { _UserNoderef = value; }
        }

        #endregion

        #region Private members

        /// <summary>
        /// Name
        /// </summary>
        private string _Name;

        /// <summary>
        /// Minimum Value
        /// </summary>
        private string _MinValue;

        /// <summary>
        /// Maximum Value
        /// </summary>
        private string _MaxValue;

        /// <summary>
        /// Default Value
        /// </summary>
        private string _DefaultValue;

        /// <summary>
        ///  Data Length
        /// </summary>
        private ulong _DataLength;

        /// <summary>
        /// Padding Charachter
        /// </summary>
        private string _PaddingChar;

        /// <summary>
        /// DataType
        /// </summary>
        private AttributeDataType _DataType = AttributeDataType.String;

        /// <summary>
        ///  Status
        /// </summary>
        private AttributeStatus _Status = AttributeStatus.Optional;

        /// <summary>
        /// Tolerance
        /// </summary>
        private double _Tolerance;

        /// <summary>
        /// Attribute Prefix 
        /// </summary>
        private string _Prefix;

        /// <summary>
        /// Description
        /// </summary>
        private string _Description;

        /// <summary>
        /// Comments
        /// </summary>
        private string _Remarks;

        /// <summary>
        /// Flag if the field supports multiple values (to be expanded in MultipleValuesCount fields)
        /// </summary>
        private bool _IsMultiValue;

        /// <summary>
        /// Count of multiple values fields
        /// </summary>
        private ushort _MultipleValuesCount;

        /// <summary>
        /// Count of super multiple values fields
        /// </summary>
        private ushort _SuperMultipleValuesCount = 1;

        /// <summary>
        /// Ignore the attribute when updating
        /// </summary>
        private bool _Ignore;

        /// <summary>
        /// The ID of the group
        /// </summary>
        private ulong _GroupId;

        /// <summary>
        /// The lookup values 
        /// </summary>
        public string[] _LookupValues;

        private ulong _DisciplineNoderef;
        private ulong _UserNoderef;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeDefinition"/> class.
        /// </summary>
        public AttributeDefinition()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeDefinition"/> class.
        /// </summary>
        /// <param name="attribute">The attribute.</param>
        public AttributeDefinition(AttributeDefinition src)
        {
            _Name = src._Name;
            _MinValue = src._MinValue;
            _MaxValue = src._MaxValue;
            _DefaultValue = src._DefaultValue;
            _DataLength = src._DataLength;
            _PaddingChar = src._PaddingChar;
            _DataType = src._DataType;
            _Status = src._Status;
            _Tolerance = src._Tolerance;
            _Prefix = src._Prefix;
            _Description = src._Description;
            _Remarks = src._Remarks;
            _IsMultiValue = src._IsMultiValue;
            _MultipleValuesCount = src._MultipleValuesCount;
            _SuperMultipleValuesCount = src._SuperMultipleValuesCount;
            _Ignore = src._Ignore;
            _GroupId = src._GroupId;
            _LookupValues = src._LookupValues;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeDefinition"/> class.
        /// </summary>
        /// <param name="column">The column.</param>
        public AttributeDefinition(DataColumn column)
        {
            Name = column.ColumnName;
            DataType = AttributeDataTypeConvertor.FromSystemType(column.DataType);
            SetAdditionalProperties(column.ExtendedProperties);
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            return Equals(obj as AttributeDefinition, true);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <param name="fullComparison">Define whether to compare all attribute properties or essential</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public bool Equals(AttributeDefinition other, bool fullComparison)
        {
            if (other == null)
                return false;
            if (other.Name != Name)
                return false;
            if (other.MinValue != MinValue)
                return false;
            if (other.MaxValue != MaxValue)
                return false;
            if (other.DefaultValue != DefaultValue)
                return false;
            if (other.DataLength != DataLength)
                return false;
            if (other._DataType != _DataType)
                return false;
            if (other.IsMultiValue != IsMultiValue)
                return false;
            if (other.MultipleValuesCount != MultipleValuesCount)
                return false;
            if (other.SuperMultipleValuesCount != SuperMultipleValuesCount)
                return false;

            if (fullComparison)
            {
                if (other.PaddingChar != PaddingChar)
                    return false;
                if (other.Status != Status)
                    return false;
                if (other.Tolerance != Tolerance)
                    return false;
                if (other.Prefix != Prefix)
                    return false;
                if (other.Description != Description)
                    return false;
                if (other.Remarks != Remarks)
                    return false;
                if (other.Ignore != Ignore)
                    return false;
                if (other.GroupId != GroupId)
                    return false;
            }

            if (!ArrayHelper.AreArraysTheSame(other.LookupValues, LookupValues))
                return false;

            return true;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Name + " (" + _DataType + ")";
        }

        #endregion

        private void SetAdditionalProperties(PropertyCollection additionalProperties)
        {
            if (additionalProperties != null)
            {
                if (additionalProperties.ContainsKey("PaddingChar"))
                {
                    PaddingChar = additionalProperties["PaddingChar"].ToString();
                }

                if (additionalProperties.ContainsKey("Status"))
                {
                    Status = (AttributeStatus)Enum.Parse(typeof(AttributeStatus), additionalProperties["Status"].ToString(), true);
                }

                if (additionalProperties.ContainsKey("DataLength"))
                {
                    DataLength = ulong.Parse(additionalProperties["DataLength"].ToString());
                }

                if (additionalProperties.ContainsKey("Description"))
                {
                    Description = additionalProperties["Description"].ToString();
                }

                if (additionalProperties.ContainsKey("Remarks"))
                {
                    Remarks = additionalProperties["Remarks"].ToString();
                }

                if (additionalProperties.ContainsKey("MinValue"))
                {
                    MinValue = additionalProperties["MinValue"].ToString();
                }

                if (additionalProperties.ContainsKey("MaxValue"))
                {
                    MaxValue = additionalProperties["MaxValue"].ToString();
                }

                if (additionalProperties.ContainsKey("DefaultValue"))
                {
                    DefaultValue = additionalProperties["DefaultValue"].ToString();
                }

                if (additionalProperties.ContainsKey("Tolerance"))
                {
                    Tolerance = double.Parse(additionalProperties["Tolerance"].ToString(), CultureInfo.InvariantCulture);
                }

                if (additionalProperties.ContainsKey("IsMultiValue"))
                {
                    IsMultiValue = bool.Parse(additionalProperties["IsMultiValue"].ToString());
                }

                if (additionalProperties.ContainsKey("MultipleValuesCount"))
                {
                    MultipleValuesCount = ushort.Parse(additionalProperties["MultipleValuesCount"].ToString());
                }

                if (additionalProperties.ContainsKey("SuperMultipleValuesCount"))
                {
                    SuperMultipleValuesCount = ushort.Parse(additionalProperties["SuperMultipleValuesCount"].ToString());
                }

                if (additionalProperties.ContainsKey("Prefix"))
                {
                    Prefix = additionalProperties["Prefix"].ToString();
                }

                if (additionalProperties.ContainsKey("Ignore"))
                {
                    Ignore = bool.Parse(additionalProperties["Ignore"].ToString());
                }

                if (additionalProperties.ContainsKey("GroupId"))
                {
                    GroupId = ulong.Parse(additionalProperties["GroupId"].ToString());
                }

                if (additionalProperties.ContainsKey("LookupValues"))
                {
                    string lookupVal = additionalProperties["LookupValues"].ToString();
                    if (!String.IsNullOrEmpty(lookupVal))
                        LookupValues = lookupVal.Split(new char[] { LookupValuesSeparator }, StringSplitOptions.RemoveEmptyEntries);
                }
            }
        }

        /// <summary>
        /// Gets a data column.
        /// </summary>
        /// <returns></returns>
        public virtual DataColumn ToDataColumn()
        {
            DataColumn column = new DataColumn(Name);
            column.DataType = AttributeDataTypeConvertor.ToSystemType(DataType);
            FillAdditionalProperties(column.ExtendedProperties);
            return column;
        }

        /// <summary>
        /// Fills the additional properties.
        /// </summary>
        /// <param name="additionalProperties">The additional properties.</param>
        public void FillAdditionalProperties(PropertyCollection additionalProperties)
        {
            if (! string.IsNullOrWhiteSpace(PaddingChar) && PaddingChar != "!")
            {
                // In many attributes this "!" exist, most likely due some weirdness in older versions of Typical Updater and some other standalone tools GenerateOptionsFile and ValidataTool
                additionalProperties["PaddingChar"] = PaddingChar;
            }

            if (Status != AttributeStatus.Optional)
            {
                additionalProperties["Status"] = Status.ToString();
            }

            if (DataLength != 0)
            {
                additionalProperties["DataLength"] = DataLength.ToString();
            }

            if (Description != null)
            {
                additionalProperties["Description"] = Description;
            }

            if (Remarks != null)
            {
                additionalProperties["Remarks"] = Remarks;
            }

            if (!string.IsNullOrWhiteSpace(MinValue))
            {
                additionalProperties["MinValue"] = MinValue;
            }

            if (!string.IsNullOrWhiteSpace(MaxValue))
            {
                additionalProperties["MaxValue"] = MaxValue;
            }

            if (!string.IsNullOrWhiteSpace(DefaultValue))
            {
                additionalProperties["DefaultValue"] = DefaultValue;
            }

            if (Tolerance != 0)
            {
                additionalProperties["Tolerance"] = Tolerance.ToString(CultureInfo.InvariantCulture);
            }

            if (IsMultiValue)
            {
                additionalProperties["IsMultiValue"] = IsMultiValue.ToString();
            }

            if (MultipleValuesCount != 0)
            {
                additionalProperties["MultipleValuesCount"] = MultipleValuesCount.ToString();
            }

            if (SuperMultipleValuesCount != 1)
            {
                additionalProperties["SuperMultipleValuesCount"] = SuperMultipleValuesCount.ToString();
            }

            if (! string.IsNullOrWhiteSpace(Prefix))
            {
                // often the prefix is " ", which better be ignored
                additionalProperties["Prefix"] = Prefix;
            }

            if (Ignore)
            {
                additionalProperties["Ignore"] = Ignore.ToString();
            }

            if (GroupId != 0)
            {
                additionalProperties["GroupId"] = GroupId.ToString();
            }

            if (LookupValues != null)
            {
                additionalProperties["LookupValues"] = string.Join(LookupValuesSeparator.ToString(), LookupValues);
            }
        }

        private static string GetNSV(string value)
        {
            if (string.IsNullOrEmpty(value) || value == "No Value")
            {
                return null;
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// Gets the expanded multiple value.
        /// </summary>
        /// <returns></returns>
        public virtual List<AttributeDefinition> GetExpandedMultipleValue()
        {
            if (!IsMultiValue)
            {
                return new List<AttributeDefinition>() { this };
            }

            List<AttributeDefinition> expandedDefinitions = new List<AttributeDefinition>();

            for (int s = 0; s < SuperMultipleValuesCount; s++)
            {
                for (int m = 0; m < MultipleValuesCount; m++)
                {
                    AttributeDefinition newAttribute = (AttributeDefinition)Clone();
                    newAttribute.Name = string.Format("{0}-{1}~{2}", newAttribute.Name, (m + 1), (s + 1));
                    newAttribute.IsMultiValue = false;
                    newAttribute.MultipleValuesCount = 0;
                    newAttribute.SuperMultipleValuesCount = 1; // does not matter much

                    expandedDefinitions.Add(newAttribute);
                }
            }

            return expandedDefinitions;
        }
    }
}