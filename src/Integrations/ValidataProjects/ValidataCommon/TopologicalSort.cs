﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidataCommon
{
    public sealed class TopologicalSort<T>
    {
        private readonly Dictionary<T, Node<T>> _Dict = new Dictionary<T, Node<T>>();

        public TopologicalSort() { }

        public TopologicalSort(IEnumerable<T> nodes)
        {
            foreach (var node in nodes)
            {
                _Dict.Add(node, new Node<T>(node));
            }
        }

        public TopologicalSort(List<T> nodes)
        {
            foreach (var node in nodes)
            {
                _Dict.Add(node, new Node<T>(node));
            }
        }

        /// <summary>
        /// Add relation between graph nodes
        /// </summary>
        /// <param name="successor">derived</param>
        /// <param name="predecessor">base</param>
        /// <returns></returns>
        public bool Edge(T successor, T predecessor)
        {
            if (!_Dict.ContainsKey(successor))
                _Dict.Add(successor, new Node<T>(successor));

            if (!_Dict.ContainsKey(predecessor))
                _Dict.Add(predecessor, new Node<T>(predecessor));

            if (!_Dict[predecessor].Dependencies.Contains(_Dict[successor]))
                _Dict[predecessor].Dependencies.Add(_Dict[successor]);

            return true;
        }

        /// <summary>
        /// Topological sort
        /// </summary>
        /// <returns></returns>
        public List<T> Sort()
        {
            var nodes = new List<Node<T>>(_Dict.Values);

            var list = new List<Node<T>>();
            nodes.ForEach(n => n.Visited = false);
            nodes.ForEach(n => Visit(n, list, n));  // added n as the root

            list.Reverse();
            return list.ConvertAll(n => n.ID);
        }

        private static bool Visit<T>(Node<T> n, ICollection<Node<T>> list, Node<T> root)
        {
            if (n.Visited)
            {
                return n.Root != root;
            }
            n.Visited = true;
            n.Root = root;
            foreach (Node<T> dependency in n.Dependencies)
            {
                if (!Visit(dependency, list, root) && n != root)
                {
                    throw new Exception(String.Format("Dependencies test suite detected from [{0}] to [{1}].", n.ID, dependency.ID));
                }
            }
            list.Add(n);
            return true;
        }

        /// <summary>
        /// Custom sort, has not so optimal performance as topological sort but preserve the asc order of the dependent objects 
        /// </summary>
        /// <returns></returns>
        public List<T> Sort2()
        {
            try
            {
                Sort();
            }
            catch (Exception)
            {
                //If test suite exists throw exception
                throw;
            }

            List<Node<T>> items = _Dict.Values.ToList();

            var rootNodes = new List<Node<T>>(items).Where(n => !n.Dependencies.Any());

            var roots = rootNodes.Select(node => new Composit<T>(node, items)).ToList();

            var res = new List<T>();

            foreach (var root in roots)
            {
                Dictionary<int, List<T>> nodesByLavel = GetCompositNodesByLevel(root);

                List<int> keys = nodesByLavel.Keys.ToList();
                foreach (var key in keys)
                {
                    nodesByLavel[key] = nodesByLavel[key].OrderBy(n => n.GetHashCode()).ToList();
                }

                int maxLevel = nodesByLavel.Keys.Max();

                for (int i = maxLevel; i >= 0; i--)
                {
                    var nodesForLevel = nodesByLavel[i];
                    foreach (var node in nodesForLevel)
                        if (!res.Contains(node))
                            res.Add(node);
                }
            }

            return res;
        }

        private Dictionary<int, List<T>> GetCompositNodesByLevel(Composit<T> tree)
        {
            var res = new Dictionary<int, List<T>>();

            tree.ExtractNodesByLevel(res);

            return res;
        }
    }

    internal class Node<T>
    {
        public T ID { get; private set; }
        public bool Visited { get; set; }
        public List<Node<T>> Dependencies { get; private set; }
        public Node<T> Root { get; set; }  // keep track of the root for each node
        public Node(T id)
        {
            ID = id;
            Dependencies = new List<Node<T>>();
        }
    }

    internal class Composit<T>
    {
        public Composit<T> Parent { get; set; }
        public int Level { get; private set; }
        public T Node { get; private set; }
        public List<Composit<T>> Childrens { get; private set; }

        public Composit(Node<T> node, List<Node<T>> allNodes, Composit<T> parent = null)
        {
            Node = node.ID;
            Parent = parent;
            Childrens = new List<Composit<T>>();

            if (Parent != null)
                Level = Parent.Level + 1;
            else
            {
                Level = 0;
            }

            foreach (var childNode in allNodes.Where(n => n.Dependencies.Any(k => Equals(k.ID, node.ID))))
                Childrens.Add(new Composit<T>(childNode, allNodes, this));
        }

        public void ExtractNodesByLevel(Dictionary<int, List<T>> dict)
        {
            if (dict == null)
                dict = new Dictionary<int, List<T>>();

            List<T> nodes;
            if (dict.TryGetValue(Level, out nodes))
            {
                nodes.Add(Node);
            }
            else
            {
                nodes = new List<T> { Node };
                dict[Level] = nodes;
            }

            foreach (var node in Childrens)
                node.ExtractNodesByLevel(dict);
        }
    }
}
