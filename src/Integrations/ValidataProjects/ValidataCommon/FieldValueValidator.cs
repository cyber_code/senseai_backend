﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ValidataCommon.Interfaces;

namespace ValidataCommon
{
    /// <summary>
    /// 
    /// </summary>
    public class FieldValueValidator
    {
        private readonly IIN2RoutineExecutor _IN2RoutineExecutor;
        private readonly FieldInfo _FieldInfo;
        private readonly FieldInfo _SSFieldInfo;
        private readonly string _AttributeNameFull;
        private readonly string _ShortFieldName;
        private readonly int _MVIndex;
        private readonly int _SVIndex;

        private readonly bool _IsMandatory;
      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <param name="ssField"></param>
        /// <param name="attributeName"></param>
        /// <param name="shortFieldName"></param>
        /// <param name="mvIndex"></param>
        /// <param name="svIndex"></param>
        /// <param name="iN2RoutineExecutor"></param>
        public FieldValueValidator(FieldInfo field, FieldInfo ssField, string attributeName, string shortFieldName, int mvIndex, int svIndex, IIN2RoutineExecutor iN2RoutineExecutor)
        {
            this._FieldInfo = field;
            this._SSFieldInfo = ssField;
            this._AttributeNameFull = attributeName;
            this._ShortFieldName = shortFieldName;
            this._MVIndex = mvIndex;
            this._SVIndex = svIndex;
            _IN2RoutineExecutor = iN2RoutineExecutor;
            _IsMandatory = CalculateIsMandatory(_FieldInfo, _SSFieldInfo, _MVIndex, _SVIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string Validate(string value)
        {
            if (this._IsMandatory && string.IsNullOrEmpty(value))
                return "Mandatory field value is empty";

            if (_FieldInfo == null)
                return null;

            // TODO - MAYBE WE HAVE TO INVOLVE VERSION DEFINITION ATTRIBUTE GROUPS
            if ((_MVIndex > 1 || _SVIndex > 1) && string.IsNullOrEmpty(value))
                return null;    // no need to check other than the first MV/SV if their value is empty

            value = value ?? "";

            if (_FieldInfo.FieldLengthMax > 0 && value.Length > _FieldInfo.FieldLengthMax)
                return string.Format("The value length ({0}) exceeds the specified maximum length: {1}", value.Length, _FieldInfo.FieldLengthMax);

            if (_FieldInfo.FieldLengthMin > 0 && value.Length < _FieldInfo.FieldLengthMin)
                return string.Format("The value length ({0}) is less than the specified minimun length: {1}", value.Length, _FieldInfo.FieldLengthMin);

            if (!string.IsNullOrEmpty(_FieldInfo.ValidationRoutine)
                && _IN2RoutineExecutor.ContainsRoutine(_FieldInfo.ValidationRoutine)
                && (this._IsMandatory || !string.IsNullOrEmpty(value)))
            {
                string errMsg;
                if (!_IN2RoutineExecutor.Validate(_FieldInfo, _AttributeNameFull, value, out errMsg))
                    return string.Format("{0} FAILED: {1}", _FieldInfo.ValProg, errMsg);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ToValidationRulesString()
        {
            var result = new List<string>();

            if (_FieldInfo == null && _SSFieldInfo != null && _SSFieldInfo.FieldName != "@ID")
                return "* WARNING: Field not in version";

            if (_SSFieldInfo != null && this._IsMandatory)
                result.Add("Mandatory");

            if (_FieldInfo != null)
            {
                if (!string.IsNullOrEmpty(_FieldInfo.ValidationRoutine) && _IN2RoutineExecutor != null)
                {
                    result.Add(_FieldInfo.ValProg);
                    if (_IN2RoutineExecutor.IsNonExecutable(_FieldInfo.ValidationRoutine))
                        result.Insert(0, "*WARNING: CANNOT BE EXECUTED");
                    else if (!_IN2RoutineExecutor.ContainsRoutine(_FieldInfo.ValidationRoutine))
                        result.Insert(0, "*WARNING: NOT EXISTING ROUTINE");
                }

                if (_FieldInfo.FieldLengthMin > 0)
                    result.Add(string.Format("Min Length: {0}", _FieldInfo.FieldLengthMin));
                if (_FieldInfo.FieldLengthMax > 0)
                    result.Add(string.Format("Max Length: {0}", _FieldInfo.FieldLengthMax));
                //if (_FieldInfo.IsFixedLookUpList)
                //    result.Add("Lookup values: " + string.Join(", ", _FieldInfo.FixedLookUpListValues));
            }
            else
            {
                // Not in version
            }

            return string.Join("; ", result);
        }

        public override string ToString()
        {
            StringBuilder sbRes = new StringBuilder();
            if (_FieldInfo != null)
                sbRes.Append(_FieldInfo.FieldName);
            else if (_SSFieldInfo != null)
                sbRes.Append(_SSFieldInfo.FieldName);
            else
                sbRes.Append("[UNKNOWN]");
            sbRes.Append(": ");
            sbRes.Append(ToValidationRulesString());
            return sbRes.ToString();
        }

        private static bool CalculateIsMandatory(FieldInfo fi, FieldInfo ssFi, int mvIndex, int svIndex)
        {
            if (ssFi != null)
            {
                return (ssFi.IsMandatory || ssFi.FieldLengthMin > 0) && ssFi.Behavior != FieldBehavior.NOCHANGE
                    && mvIndex <=1 && svIndex<=1;
            }

            // we don't check the definition in the actual version only in SS
            return false;
        }
    }
}
