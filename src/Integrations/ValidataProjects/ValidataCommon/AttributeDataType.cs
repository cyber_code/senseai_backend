namespace Validata.Common
{
    /// <summary>
    /// Attribute Data Type
    /// </summary>
    public enum AttributeDataType : uint
    {
        /// <summary>
        /// Signed integer value
        /// </summary>
        Integer = 0,

        /// <summary>
        /// A floating-point (decimal) value
        /// </summary>
        Real,

        /// <summary>
        /// String value (maximum length 4000 characters)
        /// </summary>
        String,

        /// <summary>
        /// Boolean value (true or false)
        /// </summary>
        Boolean,

        /// <summary>
        /// Currency value (decimal value with fixed precision)
        /// </summary>
        Currency,

        /// <summary>
        /// Date and time value. Precision is up to seconds
        /// </summary>
        DateTime,

        /// <summary>
        /// Binary data stored as a series of bytes
        /// </summary>
        Blob,

        /// <summary>
        /// Measure (can be calculated or aggregated measure) 
        /// See <see href="CalculatedandAggregatedMeasures.htm">Calculated and Aggregated Measures.</see>
        /// </summary>
        Measure,

        /// <summary>
        /// Array (equivalent to List of strings)
        /// </summary>
        Array,
        
        /// <summary>
        /// Unknown value
        /// </summary>
        Unknown,
    }
}