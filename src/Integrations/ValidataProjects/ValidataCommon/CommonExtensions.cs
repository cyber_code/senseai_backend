﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ValidataCommon
{
    public static class CommonExtensions
    {
        public static bool IsWhiteSpaceOrNoValue(this string str)
        {
            return string.IsNullOrEmpty(str) || str.Trim() == "No Value";
        }

        public static bool IsEmptyOrNoValue(this string str)
        {
            return string.IsNullOrEmpty(str) || str == "No Value";
        }

        public static bool IsNumeric(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;

            return !str.ToCharArray().Any(n => !char.IsDigit(n));
        }

        public static bool IsStringContaingOnlyLettersDigitsOrUnderscores(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;

            return str.ToCharArray().All(n => char.IsDigit(n) || char.IsLetter(n) || n == '_');
        }

        public static bool IsUpperAlpha(this string str)
        {
            foreach (char ch in str)
                if (!ch.IsUpperAlpha())
                    return false;

            return true;
        }

        public static bool IsUpperAlpha(this char c)
        {
            return c >= 'A' && c <= 'Z';
        }

        public static bool IsEqualTo(this List<string> lsA, List<string> lsB)
        {
            if (lsA == null || lsB == null)
                return lsA == null && lsB == null;

            if (lsA.Count != lsB.Count)
                return false;

            for (int i = 0; i < lsA.Count; i++)
            {
                if (lsA[i] != lsB[i])
                    return false;
            }

            return true;
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this Hashtable hashtable)
        {
            var result = new Dictionary<TKey, TValue>();

            if (hashtable == null)
                return result;

            foreach (object key in hashtable.Keys)
            {
                result[(TKey)key] = (TValue)hashtable[key];
            }

            return result;
        }

        public static MethodInfo[] GetMethods(this Type type, string methodName)
        {
            return type.GetMethods().Where(n => n.Name == methodName).ToArray();
        }

        public static MethodInfo FindMethod(this Type type, string methodName, params Type[] argumentTypes)
        {
            MethodInfo[] methods = type.GetMethods(methodName);

            foreach (MethodInfo mi in methods.Where(n => n.GetParameters().Length == argumentTypes.Length))
            {
                ParameterInfo[] prms = mi.GetParameters();
                bool found = true;
                for (int i = 0; i < prms.Length; i++)
                {
                    if (prms[i].ParameterType != argumentTypes[i]
                        && !argumentTypes[i].GetInterfaces().Where(n => n.Namespace != "System").Contains(prms[i].ParameterType))
                    {
                        found = false;
                        break;
                    }
                }

                if (found)
                    return mi;
            }

            return null;
        }
    }
}
