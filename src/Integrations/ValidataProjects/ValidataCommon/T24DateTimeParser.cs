﻿using System;
using System.Globalization;

namespace ValidataCommon
{
    /// <summary>
    /// Parser of dates in T24 format
    /// </summary>
    public static class T24DateTimeParser
    {
        /// <summary>
        /// Default T24 Format
        /// </summary>
        public const string T24_DATE_TIME_FORMAT = "yyMMddHHmm";

        /// <summary>
        /// Parses the date time.
        /// </summary>
        /// <param name="inputDateTime">The input date.</param>
        /// <param name="resultDateTime">The result date.</param>
        /// <returns></returns>
        public static bool TryParse(string inputDateTime, out DateTime resultDateTime)
        {
            if (!DateTime.TryParseExact(inputDateTime, T24_DATE_TIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out resultDateTime))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Parses the specified input date.
        /// </summary>
        /// <param name="inputDateTime">The input date.</param>
        /// <returns></returns>
        public static DateTime Parse(string inputDateTime)
        {
            DateTime dt;
            if (!TryParse(inputDateTime, out dt))
            {
                throw new ApplicationException("The date/time " + inputDateTime + " is not a valid T24 date/time");
            }
            return dt;
        }
    }
}