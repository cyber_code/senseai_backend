﻿using System;
using System.Collections.Generic;

namespace ValidataCommon
{
    /// <summary>
    /// Container of FieldInfo items
    /// </summary>
    [Serializable]
    public class FieldInfoList : List<FieldInfo>
    {
        /// <summary>
        /// Contains Field info with passed field name
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public bool Contains(string fieldName)
        {
            return FindByName(fieldName) != null;
        }

        /// <summary>
        /// Finds the name of the by.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public FieldInfo FindByName(string fieldName)
        {
            foreach (FieldInfo fi in this)
            {
                if (fi.FieldName == fieldName)
                {
                    return fi;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds all fields by name.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public IEnumerable<FieldInfo> FindAllByName(string fieldName)
        {
            foreach (FieldInfo fi in this)
            {
                if (fi.FieldName == fieldName)
                {
                    yield return fi;
                }
            }
        }

        /// <summary>
        /// Finds the name of the by.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="mv"></param>
        /// <param name="sv"></param>
        /// <returns></returns>
        internal FieldInfo Find(string fieldName, int mv, int sv, string assocVersion)
        {
            foreach (FieldInfo fi in this)
            {
                if (fi.FieldName == fieldName &&
                    fi.MVIndex == mv &&
                    fi.SVIndex == sv &&
                    fi.AssociatedVersion == assocVersion)
                {
                    return fi;
                }
            }

            return null;
        }

        internal bool AreEqualTo(FieldInfoList other)
        {
            if (other == null || other.Count != this.Count)
                return false;

            for (int i = 0; i < this.Count; i++)
            {
                if (!this[i].IsEqualTo(other[i]))
                    return false;
            }

            return true;
        }
    }
}