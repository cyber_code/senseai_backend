﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ValidataCommon
{
    /// <summary>
    /// Helper for storing user-selected execution pools for test suites and batch jobs
    /// </summary>
    public static class ExecutionPoolsInfoStorageHelper
    {
        /// <summary>
        /// Gets the agents allowed for execution.
        /// </summary>
        /// <param name="batchID">The batch identifier.</param>
        /// <param name="suiteID">The suite identifier.</param>
        /// <returns></returns>
        public static List<string> GetAgentsAllowedForExecution(string batchID, string suiteID)
        {
            string type;
            string id;
            if (!string.IsNullOrEmpty(batchID) && batchID != "0")
            {
                id = batchID;
                type = "batch";
            }
            else
            {
                id = suiteID;
                type = "suite";
            }
            string path = string.Format(@"{0}AdapterFiles\ExecutionPools\{1}-{2}.txt", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") , type, id);
            if (!File.Exists(path))
            {
                return null;
            }

            return new List<string>(File.ReadAllLines(path));
        }

        /// <summary>
        /// Saves the execution pools.
        /// </summary>
        /// <param name="executionAgents">The execution agents.</param>
        /// <param name="isBatchJob">if set to <c>true</c> [is batch job].</param>
        /// <param name="id">The identifier.</param>
        public static void SaveExecutionPools(List<string> executionAgents, bool isBatchJob, ulong id)
        {
            if (executionAgents.Count == 0)
                return;

            string text = string.Join(Environment.NewLine, executionAgents);
            string type = isBatchJob ? "batch" : "suite";
            string path = string.Format(@"{0}AdapterFiles\ExecutionPools\{1}-{2}.txt", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") ,type, id);
            Utils.AssureDirectoryExists(path);
            File.WriteAllText(path, text);
        }

        /// <summary>
        /// Copies the execution agent preferences if any.
        /// </summary>
        /// <param name="isBatchJob">if set to <c>true</c> [is batch job].</param>
        /// <param name="oldID">The old identifier.</param>
        /// <param name="newID">The new identifier.</param>
        public static void CopyExecutionAgentPreferencesIfAny(bool isBatchJob, ulong oldID, ulong newID)
        {
            string type = isBatchJob ? "batch" : "suite";
            string path = string.Format(@"{0}AdapterFiles\ExecutionPools\{1}-{2}.txt", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") ,type, oldID);
            string destPath = string.Format(@"{0}AdapterFiles\ExecutionPools\{1}-{2}.txt", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%")  ,type, newID);
            if (File.Exists(path))
            {
                File.Copy(path, destPath);
            }
        }
    }
}