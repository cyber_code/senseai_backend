﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ValidataCommon
{
    public class DiskStorageVariablesCache
    {
        private static DiskStorageVariablesCache _Instance = null;

        private class Variable
        {
            internal string Name;

            private readonly SortedDictionary<int, string> _Values = new SortedDictionary<int, string>();

            internal IEnumerable<int> Indices
            {
                get { return _Values.Keys; }
            }

            internal string this[int index]
            {
                set { _Values[index] = value; }
                get { return _Values.ContainsKey(index) ? _Values[index] : null; }
            }
        }

        private class ExecutionVariables : Dictionary<string, Variable>
        {
            internal Variable GetVariable(string slotName)
            {
                if (!this.ContainsKey(slotName))
                {
                    this[slotName] = new Variable();
                }

                return this[slotName];
            }

            private const string ValuesDelimiter = "\t";

            private const string LineDelimiter = "\r\n";

            public override string ToString()
            {
                StringBuilder sbResult = new StringBuilder();
                foreach (string variableName in this.Keys)
                {
                    Variable variable = this[variableName];
                    foreach (int index in variable.Indices)
                    {
                        sbResult.Append(variableName);
                        sbResult.Append(ValuesDelimiter);
                        sbResult.Append(index);
                        sbResult.Append(ValuesDelimiter);
                        sbResult.Append(variable[index]);
                        sbResult.Append(LineDelimiter);
                    }
                }

                return sbResult.ToString();
            }

            internal static ExecutionVariables FromString(string value)
            {
                ExecutionVariables result = new ExecutionVariables();
                string[] lines = value.Split(new string[] { LineDelimiter }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string line in lines)
                {
                    int posNameIdxDelimiter = line.IndexOf(ValuesDelimiter);
                    int posIdxValueDelimiter = (posNameIdxDelimiter >= 0) ?
                        line.IndexOf(ValuesDelimiter, posNameIdxDelimiter + 1) :
                        -1;
                    if (posNameIdxDelimiter < 0 || posNameIdxDelimiter < 0)
                    {
                        System.Diagnostics.Debug.WriteLine("Invalid line");
                        continue;
                    }

                    string resName = line.Substring(0, posNameIdxDelimiter);
                    string strResIndex = line.Substring(posNameIdxDelimiter + 1, posIdxValueDelimiter - posNameIdxDelimiter - 1);
                    int resIndex;
                    if (!int.TryParse(strResIndex, out resIndex))
                    {
                        System.Diagnostics.Debug.WriteLine("Invalid line");
                        continue;
                    }
                    string resValue = line.Substring(posIdxValueDelimiter + 1);

                    var variable = result.GetVariable(resName);
                    variable[resIndex] = resValue;
                }

                return result;
            }
        }

        private readonly Dictionary<string, ExecutionVariables> _VariablesPerExecution =
            new Dictionary<string, ExecutionVariables>();

        public static void SetValue(string tempDirectory, string executionId, string slotName, int arrayIndex, string value)
        {
            try
            {
                InitInstance();

                var executionVariables = _Instance.GetExecutionVariables(tempDirectory, executionId);

                var variable = executionVariables.GetVariable(slotName);

                variable[arrayIndex] = value;

                _Instance.SaveExecutionVariablesToFile(tempDirectory, executionId, executionVariables);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    string.Format("Unable to cache value '{0}' for: Exec ID: {0} Name: {1} Index: {2}: {3}", value, executionId, slotName, arrayIndex, ex.Message),
                    ex);
            }
        }

        public static string GetValue(string tempDirectory, string executionId, string slotName, int arrayIndex)
        {
            try
            {
                InitInstance();

                var executionVariables = _Instance.GetExecutionVariables(tempDirectory, executionId);

                var variable = executionVariables.GetVariable(slotName);

                return variable[arrayIndex];
            }
            catch (Exception ex)
            {
                throw new Exception(
                    string.Format("Unable to retrieve cached value for: Exec ID: {0} Name: {1} Index: {2}: {3}", executionId, slotName, arrayIndex, ex.Message),
                    ex);
            }
        }

        private ExecutionVariables GetExecutionVariables(string tempDirectory, string executionId)
        {
            if (!_VariablesPerExecution.ContainsKey(executionId))
            {
                _VariablesPerExecution[executionId] = LoadExecutionVariablesFromFile(tempDirectory, executionId);
            }

            return _VariablesPerExecution[executionId];
        }

        private static void InitInstance()
        {
            if (_Instance == null)
            {
                _Instance = new DiskStorageVariablesCache();
            }
        }

        private void SaveExecutionVariablesToFile(string tempDirectory, string executionId, ExecutionVariables executionVariables)
        {
            string filePath = GetCacheFilePath(tempDirectory, executionId);
            File.WriteAllText(filePath, executionVariables.ToString());
        }

        private ExecutionVariables LoadExecutionVariablesFromFile(string tempDirectory, string executionId)
        {
            string filePath = GetCacheFilePath(tempDirectory, executionId);
            if (!File.Exists(filePath))
            {
                return new ExecutionVariables();
            }

            return ExecutionVariables.FromString(File.ReadAllText(filePath));
        }

        private string GetCacheFilePath(string tempDirectory, string executionId)
        {
            if (!Directory.Exists(tempDirectory))
            {
                Directory.CreateDirectory(tempDirectory);
            }

            string filePath = EscapeFileName(string.Format("{0}.dat", executionId));
            return Path.Combine(tempDirectory, filePath);
        }

        private static string EscapeFileName(string fileName)
        {
            const string pattern = "_x00{0:X2}_";

            foreach (char c in Path.GetInvalidFileNameChars())
            {
                fileName = fileName.Replace(c.ToString(), string.Format(pattern, (byte)c));
            }

            return fileName;
        }

        internal static void ClearCache()
        {
            _Instance._VariablesPerExecution.Clear();
        }
    }
}
