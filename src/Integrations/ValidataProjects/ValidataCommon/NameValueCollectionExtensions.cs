﻿using System.Collections.Specialized;
using System.Linq;

namespace ValidataCommon
{
    public static class NameValueCollectionExtensions
    {
        //public static bool AreEqual(this NameValueCollection thisCollection, NameValueCollection otherCollection, string[] ignoreAttributes)
        //{
        //    return NameValueCollectionExtensions.AreEqual(thisCollection, otherCollection, true, ignoreAttributes);
        //}

        public static bool AreEqual(this NameValueCollection thisCollection, NameValueCollection otherCollection, bool verifyOrder, string[] ignoreAttributes)
        {
            if (thisCollection == null && otherCollection == null)
            {
                return true;
            }

            if (thisCollection == null || otherCollection == null)
            {
                return false;
            }

            if (ignoreAttributes != null && ignoreAttributes.Length > 0)
            {
                thisCollection = new NameValueCollection(thisCollection);
                ignoreAttributes.ToList().ForEach(n => thisCollection.RemoveNoCase(n));

                otherCollection = new NameValueCollection(otherCollection);
                ignoreAttributes.ToList().ForEach(n => otherCollection.RemoveNoCase(n));
            }

            if (thisCollection.Count != otherCollection.Count)
            {
                return false;
            }

            if (verifyOrder)
            {
                for (int i = 0; i < thisCollection.Count; i++)
                {
                    if (thisCollection.GetKey(i) != otherCollection.GetKey(i)
                        || thisCollection.Get(i) != otherCollection.Get(i))
                    {
                        return false;
                    }
                }
            }
            else
            {
                foreach (var key in thisCollection.AllKeys)
                {
                    if (otherCollection.Get(key) != thisCollection.Get(key))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public static void RemoveNoCase(this NameValueCollection col, string name)
        {
            var toRemove = col.AllKeys.FirstOrDefault(n => string.Compare(n, name, true) == 0);
            if(toRemove != null)
            {
                col.Remove(toRemove);
            }
        }
    }
}
