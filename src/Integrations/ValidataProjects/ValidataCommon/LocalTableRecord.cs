﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ValidataCommon
{
    [Serializable]
    public class T24RecordField
    {
        [XmlAttribute("name")]
        public string Name;

        [XmlAttribute("value")]
        public string Value;

        public bool IsEqualTo(T24RecordField other)
        {
            return Name == other.Name && Value == other.Value;
        }
    }

    [Serializable]
    public class T24RecordFieldList : List<T24RecordField>
    {
        public string GetValue(string fieldName, string defaultValue)
        {
            foreach (var fld in this)
            {
                if (fld.Name == fieldName)
                {
                    return fld.Value;
                }
            }

            return string.Empty;
        }

        internal bool IsEqualTo(T24RecordFieldList other)
        {
            if (other == null || other.Count != this.Count)
                return false;

            for (int i = 0; i < Count; i++)
            {
                if (!this[i].IsEqualTo(other[i]))
                    return false;
            }

            return true;
        }
    }

    [Serializable]
    public class DecisionGroup
    {
        /// <summary>
        /// DECIS.FIELD
        /// </summary>
        public string DecisionField;

        /// <summary>
        /// REPLACE.FILE
        /// </summary>
        public string ReplaceFile;

        /// <summary>
        /// REPLACE.FLD
        /// </summary>
        public string ReplaceField;

        /// <summary>
        /// DECISION
        /// </summary>
        public string Decision;

        /// <summary>
        /// DECISION.FR
        /// </summary>
        public string DecisionFrom;

        /// <summary>
        /// DECISION.TO
        /// </summary>
        public string DecisionTo;

        public bool IsEqualTo(DecisionGroup other)
        {
            return DecisionField == other.DecisionField
                && ReplaceFile == other.ReplaceFile
                && ReplaceField == other.ReplaceField
                && Decision == other.Decision
                && DecisionFrom == other.DecisionFrom
                && DecisionTo == other.DecisionTo;
        }
    }

    [Serializable]
    public class VettingTable
    {
        /// <summary>
        /// VETTING.TABLE
        /// </summary>
        public string VettingTableField;

        /// <summary>
        /// REMARK
        /// </summary>
        public string Remark;

        /// <summary>
        /// APPLICATION
        /// </summary>
        public string Application;

        /// <summary>
        /// DECISION GROUP []
        /// </summary>
        public List<DecisionGroup> DecisionGroups = new List<DecisionGroup>();

        internal DecisionGroup GetOrCreateDecisionGroupAtIndex(int index)
        {
            while (DecisionGroups.Count <= index)
            {
                DecisionGroups.Add(new DecisionGroup());
            }

            return DecisionGroups[index];
        }

        public bool IsEqaulTo(VettingTable other)
        {
            bool decisionGroupsAreTheSame = DecisionGroups.Count == other.DecisionGroups.Count;
            if(decisionGroupsAreTheSame)
            {
                for(int i = 0; i< DecisionGroups.Count; i++)
                {
                    if (!DecisionGroups[i].IsEqualTo(other.DecisionGroups[i]))
                    {
                        decisionGroupsAreTheSame = false;
                        break;
                    }
                }
            }
            return VettingTableField == other.VettingTableField
                && Remark == other.Remark
                && Application == other.Application
                && decisionGroupsAreTheSame;
        }
    }

    [Serializable]
    public class VettingTableList : List<VettingTable>
    {
        public bool IsEqaulTo(VettingTableList other)
        {
            if (other == null || other.Count != this.Count)
                return false;

            for (int i = 0; i < Count; i++)
            {
                if (!this[i].IsEqaulTo(other[i]))
                    return false;
            }

            return true;
        }
    }

    [Serializable]
    public class LocalTableRecord
    {
        [XmlIgnore]
        public string T24_ID
        {
            get { return Attributes.GetValue("@ID", ""); }
        }

        [XmlIgnore]
        public string ShortName
        {
            get { return Attributes.GetValue("SHORT.NAME", ""); }
        }

        [XmlElement("field")]
        public T24RecordFieldList Attributes = new T24RecordFieldList();

        [XmlElement("vt")]
        public VettingTableList VettingTables = new VettingTableList();

        public string VirtualTable
        {
            get { return this.Attributes.GetValue("VIRTUAL.TABLE", ""); }
        }

        public LocalTableRecord()
        {
        }

        public LocalTableRecord(IValidataRecord record)
        {
            if (record != null)
            {
                InitializeMembers(record);
            }
        }

        private void InitializeMembers(IValidataRecord record)
        {
            foreach (var attribute in record.Attributes)
            {
                string shortFieldName;
                int mvIndex, svIndex;
                AttributeNameParser.ParseComplexFieldName(attribute.Name, out shortFieldName, out mvIndex, out svIndex);

                if (mvIndex < 1)
                    mvIndex = 1;

                if (svIndex < 1)
                    svIndex = 1;

                PushFieldValue(shortFieldName, mvIndex, svIndex, attribute.Value);
            }
        }

        private void PushFieldValue(string fieldName, int mvIndex, int svIndex, string fieldValue)
        {
            System.Diagnostics.Debug.Assert(mvIndex>=1);
            mvIndex-=1;

            System.Diagnostics.Debug.Assert(svIndex>=1);
            svIndex-=1;

            switch (fieldName)
            {
                case "VETTING.TABLE":
                    GetOrCreateVettingTablesAtIndex(mvIndex).VettingTableField = fieldValue;
                    break;

                case "REMARK":
                    GetOrCreateVettingTablesAtIndex(mvIndex).Remark = fieldValue;
                    break;
                case "APPLICATION":
                    GetOrCreateVettingTablesAtIndex(mvIndex).Application = fieldValue;
                    break;
                case "DECIS.FIELD":
                    GetOrCreateVettingTablesAtIndex(mvIndex).GetOrCreateDecisionGroupAtIndex(svIndex).DecisionField = fieldValue;
                    break;
                case "REPLACE.FILE":
                    GetOrCreateVettingTablesAtIndex(mvIndex).GetOrCreateDecisionGroupAtIndex(svIndex).ReplaceFile = fieldValue;
                    break;
                case "REPLACE.FLD":
                    GetOrCreateVettingTablesAtIndex(mvIndex).GetOrCreateDecisionGroupAtIndex(svIndex).ReplaceField = fieldValue;
                    break;
                case "DECISION":
                    GetOrCreateVettingTablesAtIndex(mvIndex).GetOrCreateDecisionGroupAtIndex(svIndex).Decision = fieldValue;
                    break;
                case "DECISION.FR":
                    GetOrCreateVettingTablesAtIndex(mvIndex).GetOrCreateDecisionGroupAtIndex(svIndex).DecisionFrom = fieldValue;
                    break;
                case "DECISION.TO":
                    GetOrCreateVettingTablesAtIndex(mvIndex).GetOrCreateDecisionGroupAtIndex(svIndex).DecisionTo = fieldValue;
                    break;
                default:
                    Attributes.Add(new T24RecordField { Name = fieldName, Value = fieldValue });
                    break;
            }
        }

        private VettingTable GetOrCreateVettingTablesAtIndex(int index)
        {
            while (VettingTables.Count <= index)
            {
                VettingTables.Add(new VettingTable());
            }

            return VettingTables[index];
        }

        public bool IsEqualTo(LocalTableRecord other)
        {
            return Attributes.IsEqualTo(other.Attributes)
                && VettingTables.IsEqaulTo(other.VettingTables);
        }
    }
}
