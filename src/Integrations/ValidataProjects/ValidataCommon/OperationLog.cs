﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Log for a list of operations steps - names, durations etc.
    /// </summary>
    public class OperationLog
    {
        private List<OperationStep> _Steps = new List<OperationStep>();

        private class OperationStep
        {
            public string Name;
            public DateTime StartTime;
            public DateTime EndTime = DateTime.MinValue;

            public OperationStep(string name, DateTime startTime)
            {
                Name = name;
                StartTime = startTime;
            }

            public bool IsCompleted
            {
                get { return EndTime != DateTime.MinValue; }
            }

            public void Complete()
            {
                if (! IsCompleted)
                {
                    EndTime = DateTime.Now;
                }
            }

            public string GetDescription()
            {
                return OperationDurationToText(Name, StartTime, EndTime);
            }

            public override string ToString()
            {
                return GetDescription();
            }

            public static string OperationDurationToText(string name, DateTime startTime, DateTime endTime)
            {
                return string.Format("{0}: {1}",
                                     name,
                                     Utils.FormatTimeSpan(endTime - startTime));
            }
        }

        private OperationStep GetFirstStep()
        {
            return GetStep(0);
        }

        private OperationStep GetLastStep()
        {
            return GetStep(_Steps.Count - 1);
        }


        private OperationStep GetStep(int index)
        {
            if (index < 0 || index >= _Steps.Count)
            {
                return null;
            }
            else
            {
                return _Steps[index];
            }
        }

        /// <summary>
        /// Gets the start time.
        /// </summary>
        /// <value>The start time.</value>
        public DateTime StartTime
        {
            get
            {
                OperationStep step = GetFirstStep();
                return (step != null ? step.StartTime : DateTime.MinValue);
            }
        }

        /// <summary>
        /// Gets the end time of the last completed step.
        /// </summary>
        /// <value>The end time.</value>
        public DateTime EndTime
        {
            get
            {
                OperationStep step = GetLastStep();
                if (step == null)
                {
                    return DateTime.MinValue;
                }
                else if (step.EndTime == DateTime.MinValue)
                {
                    return step.StartTime; // well, this is the best candidate
                }
                else
                {
                    return step.EndTime;
                }
            }
        }

        /// <summary>
        /// Gets the name of the current operation.
        /// </summary>
        /// <value>The name of the current operation.</value>
        public string CurrentOperationName
        {
            get
            {
                OperationStep step = GetLastStep();
                return (step != null ? step.Name : "");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is current operation completed.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is current operation completed; otherwise, <c>false</c>.
        /// </value>
        public bool IsCurrentOperationCompleted
        {
            get
            {
                OperationStep step = GetLastStep();
                return (step != null ? step.IsCompleted : true);
            }
        }

        /// <summary>
        /// Adds the new step.
        /// </summary>
        /// <param name="stepName">Name of the step.</param>
        public void AddNewStep(string stepName)
        {
            AddNewStep(stepName, DateTime.Now);
        }

        private void AddNewStep(string stepName, DateTime startDate)
        {
            CompleteCurrentStep();
            _Steps.Add(new OperationStep(stepName, startDate));
        }

        /// <summary>
        /// Completes the current step.
        /// </summary>
        public void CompleteCurrentStep()
        {
            OperationStep lastStep = GetLastStep();
            if (lastStep != null)
            {
                lastStep.Complete();
            }
        }

        private string GetTotalDescription()
        {
            if (_Steps.Count == 0)
            {
                return "";
            }
            else
            {
                return OperationStep.OperationDurationToText("Total", StartTime, EndTime);
            }
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <returns></returns>
        public string GetDescription()
        {
            if (_Steps.Count == 0)
            {
                return "";
            }

            StringBuilder sb = new StringBuilder();

            foreach (OperationStep step in _Steps)
            {
                sb.AppendLine(step.GetDescription());
            }
            sb.AppendLine(GetTotalDescription());

            return sb.ToString();
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return GetDescription();
        }
    }
}