﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    public class InstanceComparerInfo
    {
        public string CatalogName;
        public string TypicalName;

        public bool HasCommpresedAttributes;
        public Dictionary<string, string> MultiValueSeparators;
        public Dictionary<string, string> SubValueSeparators;
        public Dictionary<string, string> AttributeNamesToShortNames;
        public List<string> AttributeNamesList;

        public void Init(string catalogName, string typicalName, List<string> attributeNames)
        {
            CatalogName = catalogName;
            TypicalName = typicalName;
            MultiValueSeparators = new Dictionary<string, string>();
            SubValueSeparators = new Dictionary<string, string>();
            AttributeNamesToShortNames = new Dictionary<string, string>();
            AttributeNamesList = new List<string>();

            string subSeparator = "";
            string multiSeparator = "";
            foreach (string iter in attributeNames)
            {
                string attributeName = iter;
                InstanceAttributesComparer.Instance.GetDelimiters(catalogName, typicalName, ref attributeName, ref subSeparator, ref multiSeparator);
                AttributeNamesToShortNames.Add(iter, attributeName);
                if (AttributeNamesList.Contains(attributeName))
                    continue;
                AttributeNamesList.Add(attributeName);
                if (string.IsNullOrEmpty(subSeparator) && string.IsNullOrEmpty(multiSeparator))
                    continue;
                MultiValueSeparators.Add(attributeName, multiSeparator);
                SubValueSeparators.Add(attributeName, subSeparator?? "");
                HasCommpresedAttributes = true;
            }
        }

       
    }

    public class InstanceComparerHelper
    {
        public static Dictionary<string, InstanceComparerInfo> Comparers = new Dictionary<string, InstanceComparerInfo>();
        public static bool HasCommpresedAttributes;

        public static string GetFullName(string catalogName, string typicalName)
        {
            return string.Concat("[", catalogName, "]", typicalName);
        }

        public static void InitComparer(string catalogName, string typicalName, List<string> attributeNames)
        {
            HasCommpresedAttributes = true;
            string fullName = GetFullName(catalogName, typicalName);
            
            InstanceComparerInfo info = new InstanceComparerInfo();
            info.Init(catalogName, typicalName, attributeNames);

            Comparers[fullName] = info;
            if (info.HasCommpresedAttributes)
                HasCommpresedAttributes = true;
        }




        public static InstanceComparerInfo GetInfo(string catalogName, string typicalName)
        {
            string fullName = GetFullName(catalogName, typicalName);
            if (Comparers.ContainsKey(fullName))
            {
                InstanceComparerInfo info = Comparers[fullName];
                return info;
            }
            return null;
        }
    }
}
