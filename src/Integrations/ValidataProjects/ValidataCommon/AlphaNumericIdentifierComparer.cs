using System.Collections.Generic;

namespace ValidataCommon
{
    /// <summary>
    /// Alpha-numeric comparisong of identifiers
    /// </summary>
    public class AlphaNumericIdentifierComparer : Comparer<string>
    {
        /// <summary>
        /// When overridden in a derived class, performs a comparison of two objects of the same type and returns a value indicating whether one object is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>
        /// Value Condition Less than zero x is less than y.Zero x equals y.Greater than zero x is greater than y.
        /// </returns>
        /// <exception cref="T:System.ArgumentException">Type T does not implement either the <see cref="T:System.IComparable`1"></see> generic interface or the <see cref="T:System.IComparable"></see> interface.</exception>
        public override int Compare(string x, string y)
        {
            IdentifierParser xID = new IdentifierParser(x);
            IdentifierParser yID = new IdentifierParser(y);

            int res = string.Compare(xID.Prefix, yID.Prefix);
            if (res == 0)
            {
                if (xID.NumericPart > yID.NumericPart)
                {
                    res = 1;
                }
                else if (xID.NumericPart < yID.NumericPart)
                {
                    res = -1;
                }
                else
                {
                    res = string.Compare(xID.Postfix, yID.Postfix);
                }
            }

            return res;
        }
    }
}