using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using log4net;

namespace ValidataCommon
{
    /// <summary>
    /// Manager of version definitions
    /// </summary>
    public class VersionDefinitionsManager
    {
        public static readonly string DefaultVersionDefinitionsPath = String.IsNullOrEmpty(ConfigurationManager.AppSettings["AdapterFilesRootPath"]) ?
            Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\Assemblies\VersionDefinitions" :
            Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["AdapterFilesRootPath"]) + @"\Assemblies\VersionDefinitions";

        private string _VersionDefinitionsPath = DefaultVersionDefinitionsPath;

        private const string SystemLog = "Validata.SAS.System";

        private static readonly ILog _Logger = LogManager.GetLogger(SystemLog);

        private readonly Dictionary<string, Dictionary<string, VersionDefinition>> _VersionDefinitions =
            new Dictionary<string, Dictionary<string, VersionDefinition>>(); // The first key is typical name (might be combined with catalog)

        private readonly bool _IgnoreDefaultVersions = true;

        private readonly Object _Lock = new Object();

        private readonly Dictionary<string, string> _Errors = new Dictionary<string, string>();

        private Action<string> _SetStatusMethod = null;

        /// <summary>
        /// Gets the accumulated errors while reading version definitions
        /// </summary>
        /// <value>The errors.</value>
        public Dictionary<string, string> Errors
        {
            get { return _Errors; }
        }

        /// <summary>
        /// Defines if on every request to check if there is a new/updated definition of the version.
        ///  The alternative is to load them all in memory in the beginning and to ignore changes
        /// </summary>
        public bool AutomaticallyCheckForChanges { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="VersionDefinitionsManager"/> class.
        /// </summary>
        public VersionDefinitionsManager()
        {
            AutomaticallyCheckForChanges = GetFromAppConfigIfToAutomaticallyCheckForChanges();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VersionDefinitionsManager"/> class.
        /// </summary>
        /// <param name="ignoreDefaultVersions">if set to <c>true</c> [ignore default versions].</param>
        public VersionDefinitionsManager(bool ignoreDefaultVersions)
            : this()
        {
            _IgnoreDefaultVersions = ignoreDefaultVersions;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VersionDefinitionsManager"/> class.
        /// </summary>
        /// <param name="versionDefinitionsPath">Path to directory containing version definitions</param>
        public VersionDefinitionsManager(string versionDefinitionsPath)
            : this()
        {
            _VersionDefinitionsPath = versionDefinitionsPath;
        }

        /// <summary>
        /// Gets the auto load version setting from app config.
        /// </summary>
        /// <returns></returns>
        private static bool GetFromAppConfigIfToAutomaticallyCheckForChanges()
        {
            string temp = ConfigurationManager.AppSettings["LoadVersionDefinitionsAutomatically"] ?? "true";
            return temp.ToLower() == "true";
        }

        /// <summary>
        /// Gets the total versions count.
        /// </summary>
        /// <value> The total versions count. </value>
        public int TotalVersionsCount
        {
            get { return _VersionDefinitions.Values.Sum(verDef => verDef.Count); }
        }

        /// <summary>
        /// Gets all definitions.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VersionDefinition> GetAllDefinitions()
        {
            return from appVersions in _VersionDefinitions from version in appVersions.Value select version.Value;
        }

        /// <summary>
        /// Reads all of the version definitions in the specified path
        /// </summary>
        /// <param name="folderPath">Directory containing version definition files</param>
        public void ReadDefinitionFromConfigFiles(string folderPath)
        {
            _Errors.Clear();
            LoadVersionDefinitionsByFilePattern(folderPath, "*.xml");
        }

        public void ReadSingleDefinitionFromConfigFiles(string filePath)
        {
            // _Errors.Clear();
            LoadSingleVersionDefinitionsByFilePattern(filePath);
        }

        /// <summary>
        /// Reads specified version definitions in directory
        /// </summary>
        /// <param name="folderPath">Directory containing version definition files</param>
        /// <param name="fileNames">File names of versions to be loaded</param>
        public void ReadDefinitionFromConfigFiles(string folderPath, List<string> fileNames)
        {
            _Errors.Clear();

            foreach (var fileName in fileNames)
            {
                var filePath = Path.Combine(folderPath, fileName);
                var def = GetVersionDefinitionFromFile(filePath);
                if (def != null)
                    AddVersionDefinition(def);
            }
        }

        /// <summary>
        /// Reads the definition from files, matching a certain naming pattern
        /// </summary>
        /// <param name="folderPath">The folder path.</param>
        /// <param name="pattern">Search pattern</param>
        private void LoadVersionDefinitionsByFilePattern(string folderPath, string pattern)
        {
            if (!Directory.Exists(folderPath))
                return;

            foreach (string filePath in Directory.GetFiles(folderPath, pattern))
            {
                var def = GetVersionDefinitionFromFile(filePath);
                if (def != null)
                    AddVersionDefinition(def);
            }
        }

        private void LoadSingleVersionDefinitionsByFilePattern(string filePath)
        {
            if (!File.Exists(filePath))
                return;

            var def = GetVersionDefinitionFromFile(filePath);
            if (def != null)
                AddVersionDefinition(def);
        }

        /// <summary>
        /// Reads the definition from config files.
        /// </summary>
        /// <param name="applicationKey">The key contains the catalog and the typical</param>
        /// <param name="versionName"></param>
        /// <returns></returns>
        private VersionDefinition ReadDefinitionFromConfigFile(string applicationKey, string versionName)
        {
            string versionFileName = String.IsNullOrEmpty(versionName)
                ? string.Format("{0}.xml", applicationKey)
                : string.Format("{0},{1}.xml", applicationKey, versionName);

            string filePath = Path.Combine(_VersionDefinitionsPath, versionFileName);

            return GetVersionDefinitionFromFile(filePath);
        }

        /// <summary>
        /// Reads version definition from specified file
        /// </summary>
        /// <param name="filePath">File path</param>
        /// <returns></returns>
        private VersionDefinition GetVersionDefinitionFromFile(string filePath)
        {
            try
            {
                if (!File.Exists(filePath))
                    return null;

                VersionDefinition def = (VersionDefinition)XmlSerializationHelper.DeserializeFromXmlFile(typeof(VersionDefinition), filePath);
                def.FileModifiedDate = File.GetLastWriteTime(filePath);
                def.CatalogName = GetCatalogNameFromFileName(filePath);

                //if (_VersionDefinitionsPath != DefaultVersionDefinitionsPath)
                def.SourceFilePath = filePath;

                return def;
            }
            catch (Exception ex)
            {
                _Logger.Warn(string.Format("File '{0}' does not seem to be a valid version definition.", filePath), ex);
                _Errors[filePath] = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            return null;
        }

        private string GetCatalogNameFromFileName(string filePath)
        {
            var fileName = System.IO.Path.GetFileNameWithoutExtension(filePath);

            string catalog, typical, version;
            SplitVersionDefinitionFileName(fileName, out catalog, out typical, out version);

            return catalog ?? string.Empty;
        }

        private void AddVersionDefinition(VersionDefinition definition)
        {
            lock (_Lock)
            {
                if (definition.IsCommaVersion && _IgnoreDefaultVersions)
                {
                    // ignore version like CUSTOMER, 
                    return;
                }

                Dictionary<string, VersionDefinition> applicationVersions;

                string key = GetKeyFromCatalogAndTypical(definition.CatalogName ?? "", definition.ApplicationName);

                if (_VersionDefinitions.ContainsKey(key))
                {
                    applicationVersions = _VersionDefinitions[key];
                }
                else
                {
                    applicationVersions = new Dictionary<string, VersionDefinition>();
                    _VersionDefinitions[key] = applicationVersions;
                }

                AddVersionDefinitionToApplication(applicationVersions, definition);
            }
        }

        private void AddVersionDefinitionToApplication(Dictionary<string, VersionDefinition> definitions, VersionDefinition definition)
        {
            string versionName = definition.VersionName;
            if (definitions.ContainsKey(versionName))
            {
                if (_SetStatusMethod != null)
                    _SetStatusMethod(string.Format("There is already a version '{0}' for '{1}'. The old one will be replaced", versionName, definition.ApplicationName));
            }

            definitions[versionName] = definition;
        }

        /// <summary>
        /// Gets the version by application key
        /// </summary>
        /// <param name="applicationKey">Name of the application (or a combined key with the catalog).</param>
        /// <param name="versionName">Name of the version.</param>
        public VersionDefinition GetVersion(string applicationKey, string versionName)
        {
            if (AutomaticallyCheckForChanges)
                return GetVersionAndLoadIfNeeded(applicationKey, versionName);

            if (!_VersionDefinitions.ContainsKey(applicationKey))
            {
                return null;
            }

            Dictionary<string, VersionDefinition> applicationVersions = _VersionDefinitions[applicationKey];

            if (String.IsNullOrEmpty(versionName))
            {
                if (applicationVersions.ContainsKey(String.Empty))
                {
                    return applicationVersions[String.Empty];
                }
                return null;
            }

            if (!applicationVersions.ContainsKey(versionName))
            {
                return null;
            }

            return applicationVersions[versionName];
        }

        /// <summary>
        /// Gets the version. First search for exact catalog match, then in the global versoin definitions.
        /// </summary>
        /// <param name="catalogName">Name of the catalog.</param>
        /// <param name="typicalName">Name of the typical.</param>
        /// <param name="versionName">Name of the version.</param>
        /// <returns></returns>
        public VersionDefinition GetVersion(string catalogName, string typicalName, string versionName, Action<string> setstatusdel = null)
        {
            _SetStatusMethod = setstatusdel;
            if (AutomaticallyCheckForChanges)
                return GetVersionAndLoadIfNeeded(catalogName, typicalName, versionName, true);

            return GetVersion(catalogName, typicalName, versionName, true);
        }

        /// <summary>
        /// Gets the version. First search for exact catalog match, then in the global versoin definitions.
        /// </summary>
        /// <param name="catalogName">Name of the catalog.</param>
        /// <param name="typicalName">Name of the typical.</param>
        /// <param name="versionName">Name of the version.</param>
        /// <param name="searchInGlobalVersions">if set to <c>true</c> [search in global versions].</param>
        /// <returns></returns>
        public VersionDefinition GetVersion(string catalogName, string typicalName, string versionName, bool searchInGlobalVersions)
        {
            if (AutomaticallyCheckForChanges)
                return GetVersionAndLoadIfNeeded(catalogName, typicalName, versionName, searchInGlobalVersions);

            if (string.IsNullOrEmpty(catalogName))
            {
                // Debug.Fail("You should not be using this overload. Try without 'catalogName'");
                return GetVersion(typicalName, versionName);
            }

            // try to find version in the specified catalog 
            string key = GetKeyFromCatalogAndTypical(catalogName, typicalName);
            VersionDefinition def = GetVersion(key, versionName);
            if (def != null)
            {
                return def;
            }

            // try to find a global version
            if (searchInGlobalVersions)
            {
                return GetVersion(typicalName, versionName);
            }

            return null;
        }

        /// <summary>
        /// Gets all version names (only finds those which don't have specified catalog)
        /// </summary>
        /// <param name="applicationKey">Name of the application that could also contain a catalog prefix.</param>
        /// <returns></returns>
        public string[] GetAllVersionNames(string applicationKey)
        {
            if (AutomaticallyCheckForChanges)
                return GetAllVersionNamesAndLoadIfNeeded(applicationKey);

            if (!_VersionDefinitions.ContainsKey(applicationKey))
                return new string[0];

            Dictionary<string, VersionDefinition> applicationVersions = _VersionDefinitions[applicationKey];

            List<string> versionNames = applicationVersions.Values.Select(def => def.VersionName).ToList();

            versionNames.Sort();

            return versionNames.ToArray();
        }

        /// <summary>
        /// Gets all version names.
        /// </summary>
        /// <param name="catalogName">Name of the catalog.</param>
        /// <param name="applicationName">Name of the application.</param>
        /// <returns></returns>
        public string[] GetAllVersionNames(string catalogName, string applicationName)
        {
            if (AutomaticallyCheckForChanges)
                return GetAllVersionNamesAndLoadIfNeeded(catalogName, applicationName, true);

            return GetAllVersionNames(catalogName, applicationName, true);
        }

        /// <summary>
        /// Gets all version names.
        /// </summary>
        /// <param name="catalogName">Name of the application.</param>
        /// <param name="applicationName">Name of the application.</param>
        /// <param name="includeVersionsForAllCatalogs">Include versions that are applicabel for all catalogs.</param>
        /// <returns></returns>
        public string[] GetAllVersionNames(string catalogName, string applicationName, bool includeVersionsForAllCatalogs)
        {
            if (AutomaticallyCheckForChanges)
                return GetAllVersionNamesAndLoadIfNeeded(catalogName, applicationName, includeVersionsForAllCatalogs);

            if (string.IsNullOrEmpty(catalogName))
                return GetAllVersionNames(applicationName);

            string key = GetKeyFromCatalogAndTypical(catalogName, applicationName);

            List<string> versionNames = new List<string>();
            if (_VersionDefinitions.ContainsKey(key))
            {
                Dictionary<string, VersionDefinition> applicationVersions = _VersionDefinitions[key];
                foreach (VersionDefinition def in applicationVersions.Values)
                {
                    versionNames.Add(def.VersionName);
                }
            }

            if (includeVersionsForAllCatalogs)
            {
                string[] globalVersions = GetAllVersionNames(applicationName);
                foreach (string versionName in globalVersions)
                {
                    if (!versionNames.Contains(versionName))
                    {
                        versionNames.Add(versionName);
                    }
                }
            }

            versionNames.Sort();

            return versionNames.ToArray();
        }

        /// <summary>
        /// Gets all versins of AA typicals in the specified catalog
        /// </summary>
        /// <param name="catalogName">Name of the catalog.</param>
        /// <returns></returns>
        public static string[] GetAllAAVersionsInCatalog(string catalogName)
        {
            string folderPath = DefaultVersionDefinitionsPath;

            if (!Directory.Exists(folderPath))
                return new string[0];

            var result = new List<string>();

            var mgr = new VersionDefinitionsManager();
            var pattern = NormalizeSearchPattern(String.Format("[{0}]", catalogName)) + "*.xml";
            foreach (string filePath in Directory.GetFiles(folderPath, pattern))
            {
                var def = mgr.GetVersionDefinitionFromFile(filePath);
                if (def != null && def.IsAA)
                    result.Add(def.FullName);
            }

            return result.ToArray();
        }

        /// <summary>
        /// Gets the version by application key
        /// </summary>
        /// <param name="applicationKey">Name of the application or a combined key with the catalog.</param>
        /// <param name="versionName">Name of the version.</param>
        private VersionDefinition GetVersionAndLoadIfNeeded(string applicationKey, string versionName)
        {
            VersionDefinition vd;

            lock (_Lock)
            {
                if (applicationKey == null)
                    throw new NullReferenceException("Internal Error: T24 Application must be defined!");

                if (versionName == null)
                    versionName = string.Empty;

                if (!_VersionDefinitions.ContainsKey(applicationKey))
                {
                    // Read and add version definition
                    vd = ReadDefinitionFromConfigFile(applicationKey, versionName);
                    if (vd != null)
                        AddVersionDefinition(vd);

                    return vd;
                }

                Dictionary<string, VersionDefinition> applicationVersions = _VersionDefinitions[applicationKey];

                if (!applicationVersions.ContainsKey(versionName))
                {
                    // Read and add version definition to application
                    vd = ReadDefinitionFromConfigFile(applicationKey, versionName);
                    if (vd != null)
                        AddVersionDefinitionToApplication(applicationVersions, vd);

                    return vd;
                }

                vd = applicationVersions[versionName];

                // Check if version definition is up to date
                if (!File.Exists(vd.SourceFilePath))
                {
                    // Version definition source file does not exist anymore, so remove it
                    applicationVersions.Remove(versionName);
                    if (applicationVersions.Count == 0)
                        _VersionDefinitions.Remove(applicationKey);

                    return null;
                }

                if (vd.IsSourceFileModified)
                {
                    // Version definition source file has been modified since version definition was initially read and need to be reloaded
                    vd = ReadDefinitionFromConfigFile(applicationKey, versionName);
                    if (vd != null)
                        AddVersionDefinitionToApplication(applicationVersions, vd);
                }
            }

            return vd;
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <param name="catalogName">Name of the catalog.</param>
        /// <param name="typicalName">Name of the typical.</param>
        /// <param name="versionName">Name of the version.</param>
        /// <param name="searchInGlobalVersions">if set to <c>true</c> [search in global versions].</param>
        /// <returns></returns>
        private VersionDefinition GetVersionAndLoadIfNeeded(string catalogName, string typicalName, string versionName, bool searchInGlobalVersions)
        {
            if (string.IsNullOrEmpty(catalogName))
            {
                // Debug.Fail("You should not be using this overload. Try without 'catalogName'");
                return GetVersionAndLoadIfNeeded(typicalName, versionName);
            }

            // try to find a global version
            string key = GetKeyFromCatalogAndTypical(catalogName, typicalName);
            VersionDefinition def = GetVersionAndLoadIfNeeded(key, versionName);
            if (def != null)
            {
                lock (_Lock)
                {
                    #region Check if version definition is up to date

                    Dictionary<string, VersionDefinition> applicationVersions = _VersionDefinitions[key];

                    if (!File.Exists(def.SourceFilePath))
                    {
                        //Version definition source file does not exist
                        applicationVersions.Remove(versionName);
                        if (applicationVersions.Count == 0)
                        {
                            _VersionDefinitions.Remove(key);
                        }

                        return null;
                    }

                    if (def.IsSourceFileModified)
                    {
                        //Version definition source file has been modified since version definition was initially read and need to be reloaded
                        def = ReadDefinitionFromConfigFile(key, versionName);
                        if (def != null)
                            AddVersionDefinitionToApplication(applicationVersions, def);
                    }

                    #endregion
                }

                return def;
            }

            if (searchInGlobalVersions)
            {
                // try to find a global version
                var glVer = GetVersionAndLoadIfNeeded(typicalName, versionName);
                return glVer;
            }

            lock (_Lock)
            {
                // Read and add version definition
                VersionDefinition vd = ReadDefinitionFromConfigFile(key, versionName);
                if (vd != null)
                    AddVersionDefinition(vd);

                return vd;
            }
        }


        /// <summary>
        /// Gets all version names (only finds those which don't have specified catalog)
        /// </summary>
        /// <param name="applicationKey">Name of the application (can contains catalog prefix).</param>
        /// <returns></returns>
        private string[] GetAllVersionNamesAndLoadIfNeeded(string applicationKey)
        {
            lock (_Lock)
            {
                if (!_VersionDefinitions.ContainsKey(applicationKey))
                {
                    LoadVersionDefinitionsByFilePattern(_VersionDefinitionsPath, String.Format("{0}*.xml", NormalizeSearchPattern(applicationKey)));
                }

                if (!_VersionDefinitions.ContainsKey(applicationKey))
                    return new string[0];

                List<string> versionNames = new List<string>();
                List<VersionDefinition> vdToAdd = new List<VersionDefinition>();
                Dictionary<string, VersionDefinition> appVersions = _VersionDefinitions[applicationKey];

                foreach (VersionDefinition def in appVersions.Values)
                {
                    if (!File.Exists(def.SourceFilePath))
                        continue;

                    if (def.IsSourceFileModified)
                    {
                        // Version definition source file has been modified since version definition was initially read and need to be reloaded
                        VersionDefinition vd = ReadDefinitionFromConfigFile(applicationKey, def.VersionName);
                        if (vd != null)
                            vdToAdd.Add(vd);
                    }
                    else
                    {
                        versionNames.Add(def.VersionName);
                    }
                }

                foreach (VersionDefinition vd in vdToAdd)
                {
                    AddVersionDefinitionToApplication(appVersions, vd);
                    versionNames.Add(vd.VersionName);
                }

                versionNames.Sort();

                return versionNames.ToArray();
            }
        }

        /// <summary>
        /// Gets all version names.
        /// </summary>
        /// <param name="catalogName">Name of the application.</param>
        /// <param name="applicationName">Name of the application.</param>
        /// <param name="includeVersionsForAllCatalogs">Include versions that are applicabel for all catalogs.</param>
        /// <returns></returns>
        private string[] GetAllVersionNamesAndLoadIfNeeded(string catalogName, string applicationName, bool includeVersionsForAllCatalogs)
        {
            if (string.IsNullOrEmpty(catalogName))
            {
                return GetAllVersionNamesAndLoadIfNeeded(applicationName);
            }

            string key = GetKeyFromCatalogAndTypical(catalogName, applicationName);
            List<string> versionNames = new List<string>();

            lock (_Lock)
            {
                if (!_VersionDefinitions.ContainsKey(key))
                {
                    LoadVersionDefinitionsByFilePattern(_VersionDefinitionsPath, NormalizeSearchPattern(String.Format("[{0}]{1}", catalogName, applicationName)) + "*.xml");
                }

                if (_VersionDefinitions.ContainsKey(key))
                {
                    Dictionary<string, VersionDefinition> applicationVersions = _VersionDefinitions[key];
                    List<VersionDefinition> vdToAdd = new List<VersionDefinition>();
                    foreach (VersionDefinition def in applicationVersions.Values)
                    {
                        if (!File.Exists(def.SourceFilePath))
                            continue;

                        if (def.IsSourceFileModified)
                        {
                            // Version definition source file has been modified since version definition was initially read and need to be reloaded
                            VersionDefinition vd = ReadDefinitionFromConfigFile(key, def.VersionName);
                            if (vd != null)
                            {
                                vdToAdd.Add(vd);
                            }
                        }
                        else
                        {
                            versionNames.Add(def.VersionName);
                        }
                    }

                    foreach (VersionDefinition vd in vdToAdd)
                    {
                        AddVersionDefinitionToApplication(applicationVersions, vd);
                        versionNames.Add(vd.VersionName);
                    }
                }
            }

            if (includeVersionsForAllCatalogs)
            {
                string[] globalVersions = GetAllVersionNamesAndLoadIfNeeded(applicationName);
                foreach (string versionName in globalVersions)
                {
                    if (!versionNames.Contains(versionName))
                    {
                        versionNames.Add(versionName);
                    }
                }
            }

            versionNames.Sort();

            return versionNames.ToArray();
        }

        private static string GetKeyFromCatalogAndTypical(string catalogName, string typicalName)
        {
            if (string.IsNullOrEmpty(catalogName))
                return typicalName;

            return "[" + catalogName + "]" + typicalName;
        }

        internal static void SplitVersionDefinitionFileName(string fileNameWithoutExtension, out string catalog, out string typical, out string version)
        {
            catalog = string.Empty;

            if (fileNameWithoutExtension.StartsWith("["))
            {
                int idx = fileNameWithoutExtension.IndexOf(']');
                if (idx > 0)
                {
                    catalog = fileNameWithoutExtension.Substring(1, idx - 1).Trim();
                    fileNameWithoutExtension = fileNameWithoutExtension.Substring(idx + 1).Trim();
                }
            }

            version = string.Empty;
            var commaPos = fileNameWithoutExtension.IndexOf(",");
            if (commaPos > 0)
            {
                version = fileNameWithoutExtension.Substring(commaPos + 1).Trim();
                fileNameWithoutExtension = fileNameWithoutExtension.Substring(0, commaPos).Trim();
            }

            typical = fileNameWithoutExtension;
        }

        private static string NormalizeSearchPattern(string pattern)
        {
            string tmp = pattern;
            foreach (char c in Path.GetInvalidFileNameChars())
            {
                tmp = tmp.Replace(c.ToString(), "?");
            }

            return tmp;
        }

        public void SaveVersions(string catalogName, params VersionDefinition[] versions)
        {
            foreach (var versioDef in versions)
            {
                versioDef.CatalogName = catalogName;

                string outputFileName = Path.Combine(_VersionDefinitionsPath, versioDef.SuggestedFileName);
                XmlSerializationHelper.SerializeToXmlFile(versioDef, outputFileName);
            }
        }

        /// <summary>
        /// Load single version definition from the default path
        ///     Note: Throws 'FileNotFoundException' exception in case the version definition file is not existing
        /// </summary>
        /// <param name="catalogName"></param>
        /// <param name="typicalName"></param>
        /// <param name="versionName"></param>
        /// <returns></returns>
        internal static VersionDefinition LoadVersionFromFile(string catalogName, string typicalName, string versionName)
        {

            var fileName = new System.Text.StringBuilder(GetKeyFromCatalogAndTypical(catalogName, typicalName));
            if (!string.IsNullOrEmpty(versionName))
            {
                if (!versionName.StartsWith(","))
                    fileName.Append(",");
                fileName.Append(versionName);
            }

            fileName.Append(".xml");

            string filePath = Path.Combine(DefaultVersionDefinitionsPath, fileName.ToString());

            var result = new VersionDefinitionsManager().GetVersionDefinitionFromFile(filePath);

            if (result == null)
                throw new FileNotFoundException(string.Format("Version definition file '{0}' not found!", filePath));

            return result;
        }
    }
}