﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace ValidataCommon
{
    /// <summary>
    /// Logs exceptions into current projects log using log4net
    /// </summary>
    public class ExceptionPublisher
    {

        #region Logging

        //<file value="C:\\AdapterFiles\\Logs\\VConnReportViewer\\VConnReportViewer.txt" />
        //<file type="log4net.Util.PatternString" value="%property{LogName}" />
        private ILog _logger;

        #endregion


        #region static usage
        private static ExceptionPublisher _exceptionPublisher;


        public static ILog Logger
        {
            get
            {
                ValidateLogger();
                return _exceptionPublisher._logger;
            }

            set
            {
                ValidateLogger();
                _exceptionPublisher.InitLogger(value);
            }
        }

        private static void ValidateLogger()
        {
            if (_exceptionPublisher == null)
                InitExceptionPublisher();
        }

        public static void InitExceptionPublisher()
        {
           _exceptionPublisher = new ExceptionPublisher();
        }

        public static bool IsInit { get { return _exceptionPublisher == null; } }

        public static void Init(string loggerName)
        {
             ValidateLogger();
            _exceptionPublisher._logger = LogManager.GetLogger(loggerName);
        }

        public static void Publish(string message, Exception ex)
        {
            _exceptionPublisher._logger.Error(message, ex);
        }

        public static void Publish(Exception ex)
        {
            _exceptionPublisher._logger.Error("Error! Exception during execution. Details:", ex);
        }
        #endregion


        public void InitLogger(ILog logger)
        {
            _logger = logger;
        }
    }
}
