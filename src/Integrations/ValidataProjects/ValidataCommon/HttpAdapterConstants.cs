namespace ValidataCommon
{
    /// <summary>
    /// Constants related to the HTTP Adapter
    /// </summary>
    public static class HttpAdapterConstants
    {
        /// <summary>
        /// Performance indicators for the HTTP Adapter
        /// </summary>
        public static class PerformanceIndicators
        {
            /// <summary>
            /// Name of HTTP Adapter performance inidicator for NumberOfDownloadedBytes
            /// </summary>
            public const string NumberOfDownloadedBytes = "Downloaded Bytes";

            /// <summary>
            /// Name of HTTP Adapter performance inidicator for NumberOfRequests
            /// </summary>
            public const string NumberOfRequests = "HTTP Requests";

            /// <summary>
            /// Name of HTTP Adapter performance inidicator for WebServerProcessingTime
            /// </summary>
            public const string WebServerProcessingTime = "Web Server Processing Time";

            /// <summary>
            /// Name of HTTP Adapter performance inidicator for DownloadTime
            /// </summary>
            public const string DownloadTime = "Download Time";

            /// <summary>
            /// Name of HTTP Adapter performance inidicator for ValidataProcessingTimes
            /// </summary>
            public const string ValidataProcessingTimes = "Validata Processing Time";

            /// <summary>
            /// Name of HTTP Adapter performance inidicator for TotalTime
            /// </summary>
            public const string TotalTime = "Total Processing Time";
        }

        /// <summary>
        /// The base class used for showing that the typical is used for storing HTTP requests
        /// </summary>
        public const string BaseClass = "HttpRequest";

        /// <summary>
        /// The comment in the catalog to determine, if it is used for HTTP performance testing
        /// </summary>
        public const string CatalogComment = "HTTP Performance Adapter Catalog";

        /// <summary>
        /// The name of the typical which stores the results of the adapters execution
        /// </summary>
        public const string ResultsTypicalName = "HTTP Adapter Result";
    }
}