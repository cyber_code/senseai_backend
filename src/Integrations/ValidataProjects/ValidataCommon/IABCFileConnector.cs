﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    public interface FileNameListItem
    {
        string Name { get; set; }
        bool IsDirectory { get; set; }
        string FullPath { get; set; }
    }
    public interface IABCFileConnector
    {
        bool UseBinary { get; set; }
        void UploadFile(string destFilePath, byte[] fileContents, bool isSourceFile);
        void CreateDirectoryRecursively(string destDirectoryPath);
        byte[] DownloadFile(string fileName);
        void DeleteFile(string destFilePath);
        string Hostname { get; }
        string User { get; }

        string HostnameWithoutPrefix { get; }

        bool IsWinOS { get; set; }

        List<FileNameListItem> ListDirectoryItems(string p);

        char DirectorySeparator { get; }
    }
}
