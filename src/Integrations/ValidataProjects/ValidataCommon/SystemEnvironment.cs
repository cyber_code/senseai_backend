﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidataCommon
{
    public class SystemEnvironment
    {
        public ulong SystemID;

        public string SystemName;

        public ulong EnvironmentID;

        public readonly Dictionary<string, List<ulong>> StepsByAdapter = new Dictionary<string, List<ulong>>();
    }
}
