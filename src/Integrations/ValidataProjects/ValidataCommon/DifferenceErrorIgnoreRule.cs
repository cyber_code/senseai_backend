﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    public class DifferenceErrorIgnoreRule : DifferenceErrorRule
    {
        public IgnoreRuleElement Source { get; set; }
        public IgnoreRuleElement Target { get; set; }
    }
}
