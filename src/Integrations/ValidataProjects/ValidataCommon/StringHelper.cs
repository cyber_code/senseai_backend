using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Web;

namespace ValidataCommon
{
    /// <summary>
    /// Helper methods for strings
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// Gets the string from memory stream.
        /// </summary>
        /// <param name="m">The m.</param>
        /// <returns></returns>
        public static string GetStringFromMemoryStream(MemoryStream m)
        {
            if (m == null || m.Length == 0)
            {
                return null;
            }

            m.Flush();
            m.Position = 0;
            StreamReader sr = new StreamReader(m);
            string s = sr.ReadToEnd();

            return s;
        }

        /// <summary>
        /// Gets the memory stream from string.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        public static MemoryStream GetMemoryStreamFromString(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return null;
            }

            MemoryStream m = new MemoryStream();
            StreamWriter sw = new StreamWriter(m);
            sw.Write(s);
            sw.Flush();

            return m;
        }

        /// <summary>
        /// Determines whether [has HTML escapable characters] [the specified name].
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>
        /// 	<c>true</c> if [has HTML escapable characters] [the specified name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasHtmlEscapableCharacters(string name)
        {
            return (Utils.GetHtmlEncodedValue(name) != name);
        }

        /// <summary>
        /// Html characters that require encoding (for web version)
        /// </summary>
        public const string HtmlCharactersEscaped = "&gt; or &lt; or &amp; or &quot; or &apos;";

        /// <summary>
        /// Html characters that require encoding
        /// </summary>
        public const string HtmlCharacters = "\" or ' or & or < or > ";

        /// <summary>
        /// Determines whether [is string in array] [the specified s array].
        /// </summary>
        /// <param name="sArray">The s array.</param>
        /// <param name="val">The val.</param>
        /// <returns>
        /// 	<c>true</c> if [is string in array] [the specified s array]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsStringInArray(string val, string[] sArray)
        {
            foreach (string s in sArray)
            {
                if (s == val)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Searches the text for occurrences of a specific string.  The search is case-sensitive.
        /// </summary>
        /// <param name="text">The text to search.</param>
        /// <param name="stringToFind">The string to look for.</param>
        public static int GetNumberOfOccurrences(string text, string stringToFind)
        {
            if (text == null || stringToFind == null)
                return 0;

            int count = 0;
            if ((text == string.Empty) || (stringToFind == string.Empty))
                return 0;

            while (true)
            {
                int pos = text.IndexOf(stringToFind);
                if (pos < 0)
                    break;

                count++;
                text = text.Substring(pos + stringToFind.Length);
            }

            return count;
        }

        /// <summary>
        /// Compresses the string wtih GZip
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string CompressString(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }

            memoryStream.Position = 0;

            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);

            var gZipBuffer = new byte[compressedData.Length + 4];
            Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
            return Convert.ToBase64String(gZipBuffer);
        }

        /// <summary>
        /// Decompresses the string (compresseed wtih GZip)
        /// </summary>
        /// <param name="compressedText">The compressed text.</param>
        /// <returns></returns>
        public static string DecompressString(string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }
    }
}