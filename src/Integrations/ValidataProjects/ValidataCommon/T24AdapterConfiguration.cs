using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// T24 Adapter Action
    /// </summary>
    public enum T24AdapterAction
    {
        /// <summary>
        /// Enquiry
        /// </summary>
        Enquiry = 0,

        /// <summary>
        /// Insert
        /// </summary>
        I,

        /// <summary>
        /// Authorize
        /// </summary>
        A,

        /// <summary>
        /// See
        /// </summary>
        S,

        /// <summary>
        /// Input and Authorize
        /// </summary>
        IA,

        /// <summary>
        /// Validate Input
        /// </summary>
        VI,

        /// <summary>
        /// Delete
        /// </summary>
        D,

        /// <summary>
        /// On Hold
        /// </summary>
        HLD,

        /// <summary>
        /// Verify
        /// </summary>
        V,

        /// <summary>
        /// Reverse
        /// </summary>
        R,

        /// <summary>
        /// Unknown action
        /// </summary>
        Unknown,

        /// <summary>
        /// Navigate
        /// </summary>
        N,

        /// <summary>
        /// Confirm
        /// </summary>
        C,

        /// <summary>
        /// Table Row Action
        /// </summary>
        TRA,

        /// <summary>
        /// Generic Click action
        /// </summary>
        GenericClick,

        /// <summary>
        /// Sign Off command
        /// </summary>
        SignOff,

        /// <summary>
        /// Menu navigation
        /// </summary>
        MNU,

        /// <summary>
        /// Tab navigation
        /// </summary>
        TAB,

        /// <summary>
        /// Close page
        /// </summary>
        ClosePage,

        /// <summary>
        /// See Check Steps for T24 Input Versions
        /// </summary>
        IS,

        /// <summary>
        /// Sign On command
        /// </summary>
        SignOn,

        /// <summary>
        /// Perform Action
        /// </summary>
        Perform,
        /// <summary>
        /// Ssimulation enquiry action
        /// </summary>
        SIM,
    }

    /// <summary>
    /// T24 dapter configuration
    /// </summary>
    public class T24AdapterConfiguration
    {
        private const string MenuItemDelimiter = ">";
        /// <summary>
        /// Virtual enquiry parameter
        /// </summary>
        public const string VirtualEnquiryParameter = "E.V";
        /// <summary>
        /// Configuration / Command / Action
        /// </summary>
        public string Configuration { get; set; }

        /// <summary>
        /// Execution Action (Configuration / Command / Action)
        /// </summary>
        public T24AdapterAction Action
        {
            get { return ParseAction(Configuration); }
        }

        /// <summary>
        /// T24 Application Version
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Input user
        /// </summary>
        public string InputUser { get; set; }

        /// <summary>
        /// Input password
        /// </summary>
        public string InputPassword { get; set; }

        /// <summary>
        /// Authorization user
        /// </summary>
        public string AuthorizationUser { get; set; }

        /// <summary>
        /// Authorization password
        /// </summary>
        public string AuthorizationPassword { get; set; }

        /// <summary>
        /// Company
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// T24 Menu expression in format: MenuItem1 > MenuItem2 > MenuItem3 > ...
        /// </summary>
        public string MenuExpression { get; set; }

        /// <summary>
        /// List of T24 Menu items
        /// Gets list of the separate items contained in MenuExpression property
        /// Sets MenuExpression property out of the given ordered list of menu items
        /// </summary>
        public string[] MenuItems
        {
            get
            {
                if (string.IsNullOrWhiteSpace(MenuExpression))
                {
                    return new string[0];
                }

                var parts = MenuExpression.Split(new string[] { MenuItemDelimiter }, StringSplitOptions.None);

                List<string> result = new List<string>();
                parts.ToList().ForEach(n => result.Add(n.Trim()));
                return result.ToArray();
            }

            set
            {
                StringBuilder sbMenuExpr = new StringBuilder();

                if (value != null)
                {
                    string delimiter = string.Format(" {0} ", MenuItemDelimiter);
                    value.ToList().ForEach(n => sbMenuExpr.AppendFormat("{0}{1}", sbMenuExpr.Length != 0 ? delimiter : "", n));
                }

                MenuExpression = sbMenuExpr.ToString();
            }
        }

        #region Class Lifecycle

        /// <summary>
        /// Default constructor
        /// </summary>
        private T24AdapterConfiguration()
        {
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Possible combinations for the configuration field:
        /// Command - 1 
        /// Command,User,Password - 3
        /// Command,User,Password,AuthUser,AuthPassword - 5
        /// Version,Command - 2
        /// Version,Command,User,Password - 4
        /// Version,Command,User,Password,AuthUser,AuthPassword - 6
        /// Version,Command,User,Password,AuthUser,AuthPassword,Company - 7
        /// Version,Command,User,Password,AuthUser,AuthPassword,Company,MenuExpression - 8
        ///					  
        /// Note: If the Command is 'A', both parameters AuthUser and AuthPassword might follow the command
        /// </summary>
        /// <returns>Built adapter configuration string</returns>
        public override string ToString()
        {
            if (MenuExpression != null)
            {
                // Version,Command,User,Password,AuthUser,AuthPassword,Company,MenuExpression - 8
                return BuildAdapterConfiguration(Version, Configuration, InputUser, InputPassword, AuthorizationUser, AuthorizationPassword, Company,
                                                 MenuExpression.Contains(',') ? String.Format("\"{0}\"", MenuExpression) : MenuExpression);
            }
            else if (Company != null)
            {
                // Version,Command,User,Password,AuthUser,AuthPassword,Company - 7
                return BuildAdapterConfiguration(Version, Configuration, InputUser, InputPassword, AuthorizationUser, AuthorizationPassword, Company);
            }
            else if (Version != null)
            {
                if (!string.IsNullOrEmpty(AuthorizationUser) || !string.IsNullOrEmpty(AuthorizationPassword))
                {
                    // Version,Command,User,Password,AuthUser,AuthPassword - 6
                    return BuildAdapterConfiguration(Version, Configuration, InputUser, InputPassword, AuthorizationUser, AuthorizationPassword);
                }
                else if (!string.IsNullOrEmpty(InputPassword) || !string.IsNullOrEmpty(InputPassword))
                {
                    // Version,Command,User,Password - 4
                    return BuildAdapterConfiguration(Version, Configuration, InputUser, InputPassword);
                }
                else
                {
                    // Version,Command - 2
                    return BuildAdapterConfiguration(Version, Configuration);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(AuthorizationUser) || !string.IsNullOrEmpty(AuthorizationPassword))
                {
                    // Command,User,Password,AuthUser,AuthPassword - 5
                    return BuildAdapterConfiguration(Configuration, InputUser, InputPassword, AuthorizationUser, AuthorizationPassword);
                }
                else if (!string.IsNullOrEmpty(InputPassword) || !string.IsNullOrEmpty(InputPassword))
                {
                    // Command,User,Password - 3
                    return BuildAdapterConfiguration(Configuration, InputUser, InputPassword);
                }
                else
                {
                    // Command - 1 
                    return BuildAdapterConfiguration(Configuration);
                }
            }
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Possible combinations for the configuration field:
        /// Command - 1 
        /// Command,User,Password - 3
        /// Command,User,Password,AuthUser,AuthPassword - 5
        /// Version,Command - 2
        /// Version,Command,User,Password - 4
        /// Version,Command,User,Password,AuthUser,AuthPassword - 6
        /// Version,Command,User,Password,AuthUser,AuthPassword,Company - 7
        /// Version,Command,User,Password,AuthUser,AuthPassword,Company,MenuExpression - 8
        ///
        /// Note: If the Command is 'A', both parameters AuthUser and AuthPassword might follow the command
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns>Initialized Configuration Object</returns>
        public static T24AdapterConfiguration Parse(string configuration)
        {
            T24AdapterConfiguration result = new T24AdapterConfiguration();

            if (configuration.IsEmptyOrNoValue())
            {
                result.Configuration = string.Empty;
                return result;
            }

            string[] param = CsvParser.ParseCsvFields(configuration, ',').ToArray();

            if (param.Length <= 1 || configuration.TrimStart().StartsWith("GenericClick", StringComparison.OrdinalIgnoreCase))
            {
                result.Configuration = configuration.Trim();
                return result;
            }

            if (param.Length == 2 || param.Length == 4 || param.Length == 6 || param.Length == 7 || param.Length == 8)
            {
                // Version, Command,....
                result.Version = param[0].Trim();
                configuration = param[1].Trim();

                if (param.Length == 4 || param.Length == 6 || param.Length == 7 || param.Length == 8)
                {
                    // Version, Command, User, Password
                    result.InputUser = param[2].Trim();
                    result.InputPassword = param[3].Trim();

                    // Version, Command, User, Password, AuthUser, AuthPassword
                    if (param.Length == 6 || param.Length == 7 || param.Length == 8)
                    {
                        result.AuthorizationUser = param[4].Trim();
                        result.AuthorizationPassword = param[5].Trim();

                        if (param.Length == 7 || param.Length == 8)
                        {
                            result.Company = param[6].Trim();
                            // todo - probably this method should be executed for all other types
                            //  'null' value should mean that the default adapter configuration value should not be overwritten
                            result.NullPropertiesIfEmpty();

                            if (param.Length == 8)
                            {
                                result.MenuExpression = param[7].Trim();
                                result.NullPropertiesIfEmpty();
                            }
                        }
                    }
                }
            }
            else if (param.Length == 3 || param.Length == 5)
            {
                // Command,...
                configuration = param[0].Trim();

                //  Command, User, Password
                result.InputUser = param[1].Trim();
                result.InputPassword = param[2].Trim();

                if (configuration.ToUpper() == "A")
                {
                    result.AuthorizationUser = result.InputUser;
                    result.AuthorizationPassword = result.InputPassword;
                }

                // Command, User, Password, AuthUser, AuthPassword
                if (param.Length == 5)
                {
                    result.AuthorizationUser = param[3].Trim();
                    result.AuthorizationPassword = param[4].Trim();
                }
            }

            result.Configuration = configuration;

            return result;
        }

        /// <summary>
        /// Set null value to each property in case its current value is "" (string.empty)
        /// Note: only 'Configuration' (command) one is not touched
        /// </summary>
        private void NullPropertiesIfEmpty()
        {
            if (Version == string.Empty)
            {
                Version = null;
            }

            if (InputUser == string.Empty)
            {
                InputUser = null;
            }

            if (InputPassword == string.Empty)
            {
                InputPassword = null;
            }

            if (AuthorizationUser == string.Empty)
            {
                AuthorizationUser = null;
            }

            if (AuthorizationPassword == string.Empty)
            {
                AuthorizationPassword = null;
            }

            if (Company == string.Empty)
            {
                Company = null;
            }

            if (MenuExpression == string.Empty)
            {
                MenuExpression = null;
            }
        }

        #endregion

        #region Private Static Methods

        /// <summary>
        /// Parse configuration to T24AdapterAction
        /// </summary>
        /// <param name="configuration">Configuration string</param>
        /// <returns>T24AdapterAction object</returns>
        private static T24AdapterAction ParseAction(string configuration)
        {
            configuration = configuration.Replace("SIM.", "");
            if (configuration == VirtualEnquiryParameter)
            {
                configuration = "E";
            }
            if (configuration == null)
            {
                Debug.Fail("Don't allow it!");
                return T24AdapterAction.Unknown;
            }

            if (configuration == "No Value")
            {
                configuration = "";
            }

            if (configuration.Equals("SIM"))
            {
                return T24AdapterAction.TRA;
            }

            if (configuration.ToUpper().StartsWith("CLICK"))
            {
                return T24AdapterAction.TRA;
            }

            if (configuration.TrimStart().StartsWith("GenericClick", StringComparison.OrdinalIgnoreCase))
            {
                return T24AdapterAction.GenericClick;
            }

            switch (configuration.ToUpper())
            {
                case "":
                case "E":
                case "EH":
                    return T24AdapterAction.Enquiry;

                case "I":
                    return T24AdapterAction.I;

                case "A":
                    return T24AdapterAction.A;

                case "S":
                    return T24AdapterAction.S;

                case "IA":
                    return T24AdapterAction.IA;

                case "VI":
                    return T24AdapterAction.VI;

                case "IS":
                    return T24AdapterAction.IS;

                case "D":
                    return T24AdapterAction.D;

                case "HLD":
                    return T24AdapterAction.HLD;

                case "V":
                case "VP":
                    return T24AdapterAction.V;

                case "R":
                    return T24AdapterAction.R;

                case "N":
                    return T24AdapterAction.N;

                case "C":
                    return T24AdapterAction.C;

                case "P":
                    return T24AdapterAction.Perform;

                case "MNU":
                    return T24AdapterAction.MNU;

                case "TAB":
                    return T24AdapterAction.TAB;

                case "SIGNOFF":
                    return T24AdapterAction.SignOff;

                case "CLOSE":
                    return T24AdapterAction.ClosePage;

                case "SIGNON":
                    return T24AdapterAction.SignOn;
                case "SIM":
                    return T24AdapterAction.SIM;
                default:
                    // System.Diagnostics.Debug.Fail("TODO - Enrich with D, VA, HA, LIVE, HIS...");
                    return T24AdapterAction.Unknown;
            }
        }

        private static string BuildAdapterConfiguration(params string[] args)
        {
            StringBuilder sbResult = new StringBuilder();
            for (int i = 0; i < args.Length; i++)
            {
                if (!string.IsNullOrEmpty(args[i]))
                {
                    sbResult.Append(args[i]);
                }

                if (i != (args.Length - 1))
                {
                    sbResult.Append(',');
                }
            }

            return sbResult.ToString();
        }

        #endregion
    }
}