﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace ValidataCommon
{
    ///<summary>
    /// Validate an XML document with XSD schema. 
    /// The validation is performed by checking whether the XML document is a well-formed
    ///</summary>
    public class XmlValidator
    {
        enum XsdSourceType
        {
            Stream,
            File
        }

        private readonly XsdSourceType _SourceType;

        private readonly XmlReader _SchemaReader;

        private string _XsdFilePath;

        ///<summary>
        /// Validation Error Count
        ///</summary>
        public int ErrorsCount;

        ///<summary>
        /// Validation Error Message
        ///</summary>
        public string ErrorMessage = "";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stream">Is should be embeded resource.</param>
        public XmlValidator(Stream stream)
        {
            try
            {
                _SourceType = XsdSourceType.Stream;

                if (stream != null)
                {
                    var resourceStreamReader = new StreamReader(stream);
                    _SchemaReader = XmlReader.Create(resourceStreamReader);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ErrorMessage + ex.Message + "\r\n";
                ErrorsCount++;
            }      
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="xsdFilePath">Path to the xsd file</param>
        public XmlValidator(string xsdFilePath)
        {
            _SourceType = XsdSourceType.File;
            _XsdFilePath = xsdFilePath;
        }

        ///<summary>
        /// Process validation
        ///</summary>
        ///<param name="strXMLDoc">Xml to be validated</param>
        public void Validate(string strXMLDoc)
        {
            try
            {
                var settings = new XmlReaderSettings { NameTable = new NameTable() };
                var mgr = new XmlNamespaceManager(settings.NameTable);
                mgr.AddNamespace("", "");
                var ctxt = new XmlParserContext(settings.NameTable, mgr, "", XmlSpace.Default);
                settings.ValidationType = ValidationType.Schema;

                if(_SourceType == XsdSourceType.Stream)
                {
                    settings.Schemas.Add(null, _SchemaReader);
                }
                else if(_SourceType == XsdSourceType.File)
                {
                    settings.Schemas.Add(null, _XsdFilePath);
                }

                settings.ValidationFlags = settings.ValidationFlags & XmlSchemaValidationFlags.ReportValidationWarnings;
                settings.ValidationEventHandler += ValidationHandler;

                using (XmlReader reader = XmlReader.Create(new StringReader(strXMLDoc), settings, ctxt))
                {
                    while (reader.Read()) { }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ErrorMessage + ex.Message + "\r\n";
                ErrorsCount++;
            }
        }

        private void ValidationHandler(object sender, ValidationEventArgs args)
        {
            ErrorMessage = ErrorMessage + args.Message + "\r\n";
            ErrorsCount++;
        }
    }
}
