﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Provides fast algorithm for sort objects based on their dependencies. 
    /// </summary>
    /// <remarks>
    ///  http://en.wikipedia.org/wiki/Topological_sorting
    /// </remarks>
    public class DependencyGraph<T>
    {
        private readonly List<T> _itemsInOriginalOrder = new List<T>();
        private Dictionary<T, Dictionary<T, object>> _matrix = new Dictionary<T, Dictionary<T, object>>();

        #region Public methods

        /// <summary>
        /// Adds a list of objects that will be sorted.
        /// </summary>
        public void SetObjects(IEnumerable<T> objects)
        {
            if (objects == null || objects.Count() == 0)
                throw new ArgumentOutOfRangeException("No objects for sorting are provided!");

            _itemsInOriginalOrder.Clear();
            _itemsInOriginalOrder.AddRange(objects);

            _matrix.Clear();
            objects.ToList().ForEach(n => _matrix[n] = new Dictionary<T, object>());
        }

        /// <summary>
        /// Sets dependencies of given object.
        /// </summary>
        public void SetObjectDependencies(T obj, IEnumerable<T> dependsOnObjects)
        {
            if (dependsOnObjects == null)
                throw new ArgumentOutOfRangeException("Depends on objects must be defined!");

            if (!_matrix.ContainsKey(obj))
                throw new ArgumentOutOfRangeException("The passed 'obj' is not registered for sorting. Please add it first using 'AddObjects()' method!");

            // set dependencies
            _matrix[obj].Clear();
            dependsOnObjects.ToList().ForEach(n => _matrix[obj][n] = null);
        }

        /// <summary>
        /// Get the sorted list of objects
        /// </summary>
        public List<T> GetSortedList()
        {
            var result = new List<T>(_matrix.Count);

            // backup the original matrix because the logic modifys it
            var originalMatrix = Clone(_matrix);

            try
            {
                while (_matrix.Count > 0)
                {
                    T independentObject;
                    if (!GetFirstIndependentObject(out independentObject))
                        throw new CircularReferenceException(string.Format("Circular reference found at item: '{0}'", _matrix.Keys.First().ToString()));

                    result.Add(independentObject);
                    this.DeleteObject(independentObject);
                }
            }
            finally
            {
                // restore the matrix
                _matrix = originalMatrix;
            }

            return result;
        }

        /// <summary>
        /// Get the sorted list of objects
        /// </summary>
        public List<T> GetSortedListSafe(out List<T> circularReferenceItems)
        {
            var result = new List<T>(_matrix.Count);
            circularReferenceItems = new List<T>();

            // backup the original matrix because the logic modifys it
            var originalMatrix = Clone(_matrix);

            try
            {
                while (_matrix.Count > 0)
                {
                    T independentObject;
                    if (!GetFirstIndependentObject(out independentObject))
                    {
                        var item = _matrix.First().Key;
                        circularReferenceItems.Add(item);
                        this.DeleteObject(item);
                        continue;
                    }

                    result.Add(independentObject);
                    this.DeleteObject(independentObject);
                }
            }
            finally
            {
                // restore the matrix
                _matrix = originalMatrix;
            }

            return result;
        }

        /// <summary>
        /// Check whether the current list is sorted
        /// </summary>
        /// <returns>True if the list is sorded, otherwise False</returns>
        public bool IsOrdered()
        {
            T dummy;
            return IsOrdered(out dummy);
        }

        /// <summary>
        /// Check whether the current list is sorted
        /// </summary>
        /// <param name="firstNotWellPositionedItem">First not well positioned item</param>
        /// <returns>True if the list is sorded, otherwise False</returns>
        public bool IsOrdered(out T firstNotWellPositionedItem)
        {
            firstNotWellPositionedItem = default(T);
            // check the original order
            var previousItems = new List<T>();
            foreach (var item in _itemsInOriginalOrder)
            {
                // check whether all of depend on items are presented in the list of previous items
                var dependsOn = _matrix[item].Keys.ToList();
                if (dependsOn.Any(n => !previousItems.Contains(n)))
                {
                    firstNotWellPositionedItem = item;
                    return false;
                }

                previousItems.Add(item);
            }

            return true;
        }

        /// <summary>
        /// Get all objects in the original order without applied sorting
        /// </summary>
        /// <returns>All objects in the original order</returns>
        public List<T> GetObjects()
        {
            return _itemsInOriginalOrder;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Gets the first independent object
        /// </summary>
        /// <param name="result">Outputs first independent object, if there is no result is 'null'</param>
        /// <returns>True if independent object found</returns>
        private bool GetFirstIndependentObject(out T result)
        {
            foreach (KeyValuePair<T, Dictionary<T, object>> pair in _matrix)
            {
                if (pair.Value.Count > 0)
                    continue; // has dependency, skip it

                result = pair.Key;
                return true;
            }

            result = default(T);
            return false;
        }

        /// <summary>
        /// Deletes given object from the matrix.
        /// </summary>
        private void DeleteObject(T obj)
        {
            _matrix.Remove(obj);

            // Remove the dependency reference from all objects
            foreach (KeyValuePair<T, Dictionary<T, object>> pair in _matrix)
            {
                pair.Value.Remove(obj);
            }
        }

        /// <summary>
        /// Clones a matrix
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        private static Dictionary<T, Dictionary<T, object>> Clone(Dictionary<T, Dictionary<T, object>> matrix)
        {
            var result = new Dictionary<T, Dictionary<T, object>>();

            foreach (var item in matrix)
            {
                var clonedValue = new Dictionary<T, object>();
                foreach(var subItem in item.Value)
                {
                    clonedValue[subItem.Key] = subItem.Value;
                }

                result[item.Key] = clonedValue;
            }

            return result;
        }

        #endregion

        
    }

    /// <summary>
    /// Represents a circular reference exception when sorting dependency objects.
    /// </summary>
    [Serializable]
    public class CircularReferenceException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CircularReferenceException"/> class.
        /// </summary>
        public CircularReferenceException()
            : base("Circular reference found.")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CircularReferenceException"/> class.
        /// </summary>
        /// <param name="message">Exception message</param>
        public CircularReferenceException(string message)
            : base(message)
        {
        }
    }
}
