﻿namespace ValidataCommon
{
    /// <summary>
    /// 	Parser of Validata attributes names (may be simple or containing multivalue/subvalues, like Name-1~1).
    /// </summary>
    public class AttributeNameParser
    {
        /// <summary>
        /// 	Determine if the attribute is multivalue
        /// </summary>
        /// <param name="attrName"> Name of the attribute. </param>
        /// <returns> </returns>
        public static bool IsMultiValue(string attrName)
        {
            string shortName;
            int multiValueIndex, subValueIndex;
            return ParseComplexFieldName(attrName, out shortName, out multiValueIndex, out subValueIndex);
        }

        /// <summary>
        /// 	Strip "-X~Y"
        /// </summary>
        /// <param name="attrName"> attribute name </param>
        /// <returns> Stripped name, without multivalue </returns>
        public static string GetShortFieldName(string attrName)
        {
            string shortName;
            int multiValueIndex, subValueIndex;
            ParseComplexFieldName(attrName, out shortName, out multiValueIndex, out subValueIndex);
            return shortName;
        }

        /// <summary>
        /// 	Gets the index of multi value.
        /// </summary>
        /// <param name="attrName"> Name of the attribute. </param>
        /// <returns> </returns>
        public static int GetMultiValueIndex(string attrName)
        {
            string shortName;
            int multiValueIndex, subValueIndex;
            ParseComplexFieldName(attrName, out shortName, out multiValueIndex, out subValueIndex);
            return multiValueIndex;
        }

        /// <summary>
        /// 	Gets the sub index of multi value.
        /// </summary>
        /// <param name="attrName"> Name of the attribute. </param>
        /// <returns> </returns>
        public static int GetSubValueIndex(string attrName)
        {
            string shortName;
            int multiValueIndex, subValueIndex;
            ParseComplexFieldName(attrName, out shortName, out multiValueIndex, out subValueIndex);
            return subValueIndex;
        }

        /// <summary>
        /// 	Parses the name of the complex field.
        /// </summary>
        /// <param name="complexFieldName"> Name of the complex field. </param>
        /// <returns> </returns>
        public static AttributeNameParsingResult ParseComplexFieldName(string complexFieldName)
        {
            string shortName;
            int multiValueIndex, subValueIndex;
            bool isComplex = ParseComplexFieldName(complexFieldName, out shortName, out multiValueIndex, out subValueIndex);

            return new AttributeNameParsingResult
                       {
                           FullFieldName = complexFieldName,
                           IsComplex = isComplex,
                           ShortFieldName = shortName,
                           MultiValueIndex = multiValueIndex,
                           SubValueIndex = subValueIndex
                       };
        }

        /// <summary>
        /// 	Parses the name of the complex field (multivalue and subvalue)
        /// </summary>
        /// <param name="complexFieldName"> Name of the complex field. </param>
        /// <param name="actualFieldName"> Actual name of the field. </param>
        /// <param name="multiValueIndex"> The multi value index. </param>
        /// <param name="subValueIndex"> The sub value index. </param>
        /// <returns> True if it is really complex, otherwise false </returns>
        public static bool ParseComplexFieldName(string complexFieldName, out string actualFieldName, out int multiValueIndex, out int subValueIndex)
        {
            actualFieldName = complexFieldName ?? "";
            multiValueIndex = 0;
            subValueIndex = 0;

            if (string.IsNullOrEmpty(complexFieldName))
                return false;

            int indexesPartStartPoss = complexFieldName.LastIndexOf("-");
            if (indexesPartStartPoss == -1)
                return false;

            string indexesPart = complexFieldName.Substring(indexesPartStartPoss + 1);

            string[] indexes = indexesPart.Split(new[] { '~' });

            if (indexes.Length != 2)
                return false;

            int mv, sv;
            if (int.TryParse(indexes[0], out mv) 
                && mv >= 0
                && int.TryParse(indexes[1], out sv) 
                && sv >= 0
                && indexesPartStartPoss > 0)
            {
                actualFieldName = complexFieldName.Substring(0, indexesPartStartPoss);
                multiValueIndex = mv;
                subValueIndex = sv;
                return true;
            }

            return false;
        }

        /// <summary>
        /// 	Determines whether [is multi or sub value of] [the specified complex field name].
        /// </summary>
        /// <param name="complexFieldName"> Name of the complex field. </param>
        /// <param name="shortAttributeName"> Short name of the attribute. </param>
        /// <returns> <c>true</c> if [is multi or sub value of] [the specified complex field name]; otherwise, <c>false</c> . </returns>
        public static bool IsMultiOrSubValueOf(string complexFieldName, string shortAttributeName)
        {
            if (!complexFieldName.StartsWith(shortAttributeName))
                return false;

            string normalizedAttributeName = GetShortFieldName(complexFieldName);
            return shortAttributeName == normalizedAttributeName;
        }
    }

    /// <summary>
    /// 	The result of attribute name parsing
    /// </summary>
    public class AttributeNameParsingResult
    {
        /// <summary>
        /// 	The orginial full field name
        /// </summary>
        public string FullFieldName;

        /// <summary>
        /// 	The parsed short field name, without multivalue/subvalue indices
        /// </summary>
        public string ShortFieldName;

        /// <summary>
        /// 	The multivalue index
        /// </summary>
        public int MultiValueIndex;

        /// <summary>
        /// 	The subvalue index
        /// </summary>
        public int SubValueIndex;

        /// <summary>
        /// 	Is the field complex (i.e. with multivalues/subvalue)
        /// </summary>
        public bool IsComplex;


        /// <summary>
        /// 
        /// </summary>
        public bool IsAAAttribute;

        /// <summary>
        /// 
        /// </summary>
        public bool IsPropertyComplex;

        /// <summary>
        /// 
        /// </summary>
        public string PropertyShortName;


        /// <summary>
        /// 
        /// </summary>
        public int MultiValueIndexProperty;

        /// <summary>
        /// 
        /// </summary>
        public int SubValueIndexProperty;
    }
}
