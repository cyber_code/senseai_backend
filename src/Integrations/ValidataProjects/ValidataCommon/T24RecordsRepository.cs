﻿using System.Collections.Generic;
using System.IO;
using System;
using System.Configuration;

namespace ValidataCommon
{
    /// <summary>
    /// Records Repository
    /// </summary>
    public class T24RecordsRepository
    {
        /// <summary>
        /// Base directory for xml files containing application records
        /// </summary>
        public static readonly string DefaultRecordsRepositoryDirectory = String.IsNullOrEmpty(ConfigurationManager.AppSettings["AdapterFilesRootPath"])
            ? Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\Assemblies\RecordsRepository\" :
            Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["AdapterFilesRootPath"]) + @"\Assemblies\RecordsRepository\";

        public static string RecordsRepositoryDirectory = DefaultRecordsRepositoryDirectory;

        #region Singleton

        /// <summary>
        /// Class instance
        /// </summary>
        private static T24RecordsRepository _Instance;

        /// <summary>
        /// Singleton class instance
        /// </summary>
        public static T24RecordsRepository Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new T24RecordsRepository();
                }

                return _Instance;
            }
        }

        /// <summary>
        /// Dafault constructor
        /// </summary>
        private T24RecordsRepository() { }

        #endregion

        /// <summary>
        /// Applications records cache
        /// </summary>
        private class RecordsCollectionInfo
        {
            internal readonly string FileName;
            internal readonly T24RecordsCollection RecordsCollection;
            internal DateTime LastWriteTime;
            internal bool IsObsolete
            {
                get { return System.IO.File.GetLastWriteTime(FileName) > LastWriteTime; }
            }

            internal RecordsCollectionInfo(string fileName, T24RecordsCollection recordsCollection)
            {
                FileName = fileName;
                RecordsCollection = recordsCollection;
                LastWriteTime = string.IsNullOrEmpty(fileName)
                    ? DateTime.MinValue
                    : System.IO.File.GetLastWriteTime(fileName);
            }
        }

        private readonly Dictionary<string, RecordsCollectionInfo> _Cache = new Dictionary<string, RecordsCollectionInfo>();

        /// <summary>
        /// Syncronization object
        /// </summary>
        private readonly object _Sync = new object();

        /// <summary>
        /// Set (save to disk) records for T24 application
        /// </summary>
        /// <param name="catalogName">Validata catalog name (if empty it is assumed as common/default)</param>
        /// <param name="applicatioName">T24 application name</param>
        /// <param name="recordsCollection">Records collection</param>
        /// <param name="errorMessasge">Output error message if function failed (e.g. return 'False')</param>
        /// <returns>Is success</returns>
        public bool SetRecords(string catalogName, string applicatioName, T24RecordsCollection recordsCollection, out string errorMessasge)
        {
            string appFilePath = GetFilePathForApplication(catalogName, applicatioName);

            lock (_Sync)
            {
                _Cache[appFilePath] = new RecordsCollectionInfo(appFilePath, recordsCollection);

                // verify directory exists
                if (!Directory.Exists(RecordsRepositoryDirectory))
                    Directory.CreateDirectory(RecordsRepositoryDirectory);

                return T24RecordsCollection.SaveToXmlFile(recordsCollection, appFilePath, out errorMessasge);
            }
        }

        /// <summary>
        /// Get all records for T24 application
        /// </summary>
        /// <param name="catalogName">Validata catalog name</param>
        /// <param name="applicationName">T24 application name</param>
        /// <param name="errorMessasge">Error messasge in case of error</param>
        /// <returns>Records collection or null if error occurs</returns>
        public T24RecordsCollection GetRecords(string catalogName, string applicationName, out string errorMessasge)
        {
            return GetRecords(catalogName, applicationName, false, true, out errorMessasge);
        }

        /// <summary>
        /// Get all records for T24 application
        /// </summary>
        /// <param name="catalogName">Validata catalog name</param>
        /// <param name="applicationName">T24 application name</param>
        /// <param name="refreshCache">Reload for file storage</param>
        /// <param name="getDefaultIfVersionPerCatalogNotFound">Return default version (not catalog bound) if version per given catalog not found</param>
        /// <param name="errorMessasge">Error messasge in case of error</param>
        /// <returns>Records collection or null if error occurs</returns>
        public T24RecordsCollection GetRecords(string catalogName, string appName, bool refreshCache, bool getDefaultIfVersionPerCatalogNotFound, out string errorMessasge)
        {
            errorMessasge = null;

            string appFilePath = GetFilePathForApplication(catalogName, appName);

            lock (_Sync)
            {
                if (_Cache.ContainsKey(appFilePath)
                    && (refreshCache || _Cache[appFilePath].IsObsolete))
                {
                    _Cache.Remove(appFilePath);
                }

                if (_Cache.ContainsKey(appFilePath))
                {
                    if (_Cache[appFilePath].RecordsCollection == null)
                    {
                        errorMessasge = string.Format("'{0}' file missing or contains invalid data", appFilePath);
                    }

                    return _Cache[appFilePath].RecordsCollection;
                }

                var recordsCollection = T24RecordsCollection.LoadFromXmlFile(appFilePath, out errorMessasge);

                if (recordsCollection == null &&
                    getDefaultIfVersionPerCatalogNotFound &&
                    !string.IsNullOrEmpty(catalogName))
                {
                    string dummyErrMesssage;
                    recordsCollection = GetRecords(string.Empty, appName, false, false, out dummyErrMesssage);
                }

                if (recordsCollection != null)
                {
                    recordsCollection.CatalogName = catalogName;
                    recordsCollection.ApplicationName = appName;

                    _Cache[appFilePath] = new RecordsCollectionInfo(appFilePath, recordsCollection);
                }

                return recordsCollection;
            }
        }

        /// <summary>
        /// Get xml file name for T24 application
        /// </summary>
        /// <param name="catalogName">Validata catalog name</param>
        /// <param name="applicationName">T24 application name</param>
        /// <returns></returns>
        public static string GetFilePathForApplication(string catalogName, string applicationName)
        {
            string fileName = string.Format("{0}.xml", applicationName);

            if (!string.IsNullOrEmpty(catalogName))
            {
                fileName = string.Format("[{0}]{1}", catalogName, fileName);
            }

            return Path.Combine(RecordsRepositoryDirectory, EscapeFileName(fileName));
        }

        /// <summary>
        /// Encode special characters
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static string EscapeFileName(string fileName)
        {
            const string pattern = "_x00{0:X2}_";

            foreach (char c in Path.GetInvalidFileNameChars())
            {
                fileName = fileName.Replace(c.ToString(), string.Format(pattern, (byte)c));
            }

            return fileName;
        }

        /// <summary>
        /// Find records extracted for given environment 
        /// </summary>
        /// <param name="applicationName">Application name</param>
        /// <param name="communicationMethod">Communication Method</param>
        /// <param name="configurationSet">Configuration set</param>
        /// <param name="errorMessage">Output error message</param>
        /// <returns>Application records (null if not found)</returns>
        public static T24RecordsCollection FindRecordsPerEnvironment(string applicationName, string communicationMethod, string configurationSet, out string errorMessage)
        {
            errorMessage = null;

            try
            {
                if (!Directory.Exists(RecordsRepositoryDirectory))
                    return null;

                string searchPattern = string.Format("*{0}.xml", EscapeFileName(applicationName));

                foreach (var filePath in Directory.GetFiles(RecordsRepositoryDirectory, searchPattern))
                {
                    var recordsCollection = T24RecordsCollection.LoadFromXmlFile(filePath, out errorMessage);

                    if (recordsCollection.CommunicationMethod == communicationMethod &&
                        recordsCollection.ConfigurationSet == configurationSet)
                    {
                        return recordsCollection;
                    }
                }
            }
            catch
            {
            }

            return null;
        }
    }
}