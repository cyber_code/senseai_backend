﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace ValidataCommon
{
    public class Localizer
    {
        private readonly Dictionary<string, string> _Resources = new Dictionary<string, string>();
        private readonly string _FileName = String.IsNullOrEmpty(ConfigurationManager.AppSettings["AdapterFilesRootPath"])
            ? Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\localization.csv" :
            Environment.ExpandEnvironmentVariables(@ConfigurationManager.AppSettings["AdapterFilesRootPath"]) + @"\localization.csv";

        private bool _Loaded;

        private void Load()
        {
            if (! File.Exists(_FileName))
            {
                return;
            }

            foreach (string line in File.ReadAllLines(_FileName))
            {
                string[] items = line.Split('|');
                if (items.Length != 2)
                {
                    continue;
                }

                if (!_Resources.ContainsKey(items[0]))
                {
                    _Resources.Add(items[0], items[1].Replace(@"\r\n", Environment.NewLine));
                }
            }
        }

        /// <summary>
        /// Gets the localized value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value (in English).</param>
        /// <returns></returns>
        public string GetValue(string key, string defaultValue)
        {
            if (!_Loaded)
            {
                Load();
                _Loaded = true;
            }

            string localizedValue;
            if (_Resources.TryGetValue(key, out localizedValue))
            {
                return localizedValue;
            }

            return defaultValue;
        }
    }
}