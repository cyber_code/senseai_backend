﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.Win32;
using System.Linq;

namespace ValidataCommon
{
    /// <summary>
    /// Helper for reading/writing from Windows Registy
    /// </summary>
    public static class RegistryManager
    {
        /// <summary>
        /// Retrieves the specified String value. Returns a System.String object
        /// </summary>
        public static string GetStringValue(RegistryKey hiveKey, string strSubKey, string strValue)
        {
            object objData = null;
            RegistryKey subKey = null;

            try
            {
                subKey = hiveKey.OpenSubKey(strSubKey);
                if (subKey == null)
                {
                    //strRegError = "Cannot open the specified sub-key";
                    return null;
                }
                objData = subKey.GetValue(strValue);
                if (objData == null)
                {
                    //strRegError = "Cannot open the specified value";
                    return null;
                }
                subKey.Close();
                hiveKey.Close();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return null;
            }

            //strRegError = null;
            return objData.ToString();
        }

        /// <summary>
        /// Retrieves the specified DWORD value. Returns a System.Int32 object
        /// </summary>
        public static uint GetDWORDValue(RegistryKey hiveKey, string strSubKey, string dwValue)
        {
            object objData = null;
            RegistryKey subKey = null;

            try
            {
                subKey = hiveKey.OpenSubKey(strSubKey);
                if (subKey == null)
                {
                    //strRegError = "Cannot open the specified sub-key";
                    return 0;
                }
                objData = subKey.GetValue(dwValue);
                if (objData == null)
                {
                    //strRegError = "Cannot open the specified value";
                    return 0;
                }
                subKey.Close();
                hiveKey.Close();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return 0;
            }

            //strRegError = null;
            return UInt32.Parse(objData.ToString());
        }

        /// <summary>
        /// Retrieves the specified Binary value. Returns a System.Byte[] object
        /// </summary>
        public static byte[] GetBinaryValue(RegistryKey hiveKey, string strSubKey, string binValue)
        {
            object objData = null;
            RegistryKey subKey = null;

            try
            {
                subKey = hiveKey.OpenSubKey(strSubKey);
                if (subKey == null)
                {
                    //strRegError = "Cannot open the specified sub-key";
                    return null;
                }
                objData = subKey.GetValue(binValue);
                if (objData == null)
                {
                    //strRegError = "Cannot open the specified value";
                    return null;
                }
                subKey.Close();
                hiveKey.Close();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return null;
            }

            //strRegError = null;
            return (byte[])objData;
        }



        /// <summary>
        /// Sets/creates the specified String value
        /// </summary>
        public static void SetStringValue(RegistryKey hiveKey, string strSubKey, string strValue, string strData)
        {
            RegistryKey subKey = null;

            try
            {
                subKey = hiveKey.CreateSubKey(strSubKey);
                if (subKey == null)
                {
                    //strRegError = "Cannot create/open the specified sub-key";
                    return;
                }
                subKey.SetValue(strValue, strData);
                subKey.Close();
                hiveKey.Close();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return;
            }

            //strRegError = null;
            return;
        }

        /// <summary>
        /// Sets/creates the specified DWORD value
        /// </summary>
        public static void SetDWORDValue(RegistryKey hiveKey, string strSubKey, string strValue, uint dwData)
        {
            RegistryKey subKey = null;

            try
            {
                subKey = hiveKey.CreateSubKey(strSubKey);
                if (subKey == null)
                {
                    //strRegError = "Cannot create/open the specified sub-key";
                    return;
                }
                subKey.SetValue(strValue, dwData);
                subKey.Close();
                hiveKey.Close();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return;
            }

            //strRegError = null;
            return;
        }

        /// <summary>
        /// Sets/creates the specified Binary value
        /// </summary>
        public static void SetBinaryValue(RegistryKey hiveKey, string strSubKey, string strValue, byte[] nnData)
        {
            RegistryKey subKey = null;

            try
            {
                subKey = hiveKey.CreateSubKey(strSubKey);
                if (subKey == null)
                {
                    //strRegError = "Cannot create/open the specified sub-key";
                    return;
                }
                subKey.SetValue(strValue, nnData);
                subKey.Close();
                hiveKey.Close();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return;
            }

            //strRegError = null;
            return;
        }



        /// <summary>
        /// Creates a new subkey or opens an existing subkey
        /// </summary>
        public static void CreateSubKey(RegistryKey hiveKey, string strSubKey)
        {
            RegistryKey subKey = null;

            try
            {
                subKey = hiveKey.CreateSubKey(strSubKey);
                if (subKey == null)
                {
                    //strRegError = "Cannot create the specified sub-key";
                    return;
                }
                subKey.Close();
                hiveKey.Close();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return;
            }

            //strRegError = null;
            return;
        }

        /// <summary>
        /// Deletes a subkey and any child subkeys recursively
        /// </summary>
        public static void DeleteSubKeyTree(RegistryKey hiveKey, string strSubKey)
        {
            try
            {
                hiveKey.DeleteSubKeyTree(strSubKey);
                hiveKey.Close();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return;
            }

            //strRegError = null;
            return;
        }

        /// <summary>
        /// Deletes the specified value from this (current) key
        /// </summary>
        public static void DeleteValue(RegistryKey hiveKey, string strSubKey, string strValue)
        {
            RegistryKey subKey = null;
            try
            {
                subKey = hiveKey.OpenSubKey(strSubKey, true);
                if (subKey == null)
                {
                    //strRegError = "Cannot open the specified sub-key";
                    return;
                }
                subKey.DeleteValue(strValue);
                subKey.Close();
                hiveKey.Close();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return;
            }

            //strRegError = null;
            return;
        }

        /// <summary>
        /// Retrieves the type of the specified Registry value
        /// </summary>
        public static Type GetValueType(RegistryKey hiveKey, string strSubKey, string strValue)
        {
            RegistryKey subKey = null;
            object objData = null;

            try
            {
                subKey = hiveKey.OpenSubKey(strSubKey);
                if (subKey == null)
                {
                    //strRegError = "Cannot open the specified sub-key";
                    return null;
                }
                objData = subKey.GetValue(strValue);
                if (objData == null)
                {
                    //strRegError = "Cannot retrieve the type of the specified value";
                    return null;
                }
                subKey.Close();
                hiveKey.Close();

            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return null;
            }

            //strRegError = null;
            return objData.GetType();
        }

        /// <summary>
        /// Load values in a heap
        /// </summary>
        /// <param name="hiveKey"></param>
        /// <param name="strSubKey"></param>
        /// <returns></returns>
        public static KeyValuePair<string, object>[] GetValuesList(RegistryKey hiveKey, string strSubKey)
        {
            RegistryKey subKey = null;

            try
            {
                subKey = hiveKey.OpenSubKey(strSubKey);
                if (subKey == null)
                {
                    //strRegError = "Cannot open the specified sub-key";
                    return null;
                }

                return subKey.GetValueNames().Select(n=> new KeyValuePair<string, object>(n, subKey.GetValue(n))).ToArray();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return null;
            }
            finally
            {
                if (subKey != null) 
                    subKey.Close();

                hiveKey.Close();
            }
        }

        /// <summary>
        /// Set values in a heap
        /// </summary>
        /// <param name="hiveKey"></param>
        /// <param name="strSubKey"></param>
        /// <param name="newValuesList"></param>
        /// <param name="valueKind"> </param>
        public static void SetValuesList(RegistryKey hiveKey, string strSubKey, Dictionary<string, object> newValuesList, RegistryValueKind valueKind)
        {
            try
            {
                RegistryKey subKey = hiveKey.OpenSubKey(strSubKey, true);
                
                if(subKey == null)
                    subKey = hiveKey.CreateSubKey(strSubKey);

                if (subKey == null)
                    return;

                string[] subValuesNames = subKey.GetValueNames();

                foreach (string subValueName in subValuesNames)
                {
                    subKey.DeleteValue(subValueName);
                }

                foreach (KeyValuePair<string, object> pair in newValuesList)
                {
                    subKey.SetValue(pair.Key, pair.Value, valueKind);
                }

                subKey.Close();
                hiveKey.Close();
            }
            catch (Exception)
            {
                //strRegError = exc.Message;
                return;
            }
        }
    }
}
