﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace ValidataCommon
{
    /// <summary>
    /// Mapper of Windows users to Validata users
    /// </summary>
    public class WindowsToValidataUsersRouter
    {
        /// <summary>
        /// The default mapping file path
        /// </summary>
        public static readonly string DefaultUserMappingFilePath = String.IsNullOrEmpty(ConfigurationManager.AppSettings["AdapterFilesRootPath"])
            ? Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%")+ @"AdapterFiles\WindowsUsers.txt" :
            Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["AdapterFilesRootPath"]) + @"\WindowsUsers.txt";

        private const string DomainDelimiter = "\\";

        internal const string DummyUserNameForDefaultDomain = "__defaultDomain";

        private Dictionary<string, string> _UsersMapping;

        private readonly string _UserMappingFilePath;

        /// <summary>
        /// The default windows domain name
        /// </summary>
        private string _DefaultWindowsDomainName;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsToValidataUsersRouter"/> class.
        /// </summary>
        public WindowsToValidataUsersRouter() : this(DefaultUserMappingFilePath) {}

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsToValidataUsersRouter"/> class.
        /// </summary>
        /// <param name="userMappingFilePath">The user mapping file path.</param>
        public WindowsToValidataUsersRouter(string userMappingFilePath)
        {
            _UserMappingFilePath = userMappingFilePath;
            ReloadUserMappings();
        }

        /// <summary>
        /// Gets the name of the corresponding validata user.
        /// </summary>
        /// <param name="windowsUserName">Name of the windows user.</param>
        /// <returns></returns>
        public string GetCorrespondingValidataUserName(string windowsUserName)
        {
            string fullyQualifiedName = GetFullyQualifiedWindowsUserName(windowsUserName).ToLowerInvariant();
            return _UsersMapping.ContainsKey(fullyQualifiedName)
                       ? _UsersMapping[fullyQualifiedName]
                       : StripOutDomainName(windowsUserName);
        }

        private string GetFullyQualifiedWindowsUserName(string userName)
        {
            return userName.Contains(DomainDelimiter)
                       ? userName
                       : string.Concat(_DefaultWindowsDomainName, DomainDelimiter, userName);
        }

        private static string StripOutDomainName(string fullyQualifiedName)
        {
            string[] parts = fullyQualifiedName.Split(new char[] {DomainDelimiter[0]}, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 2)
            {
                throw new Exception(string.Format("Invalid user name, containing more than one {0}: {1}", DomainDelimiter, fullyQualifiedName));
            }
            else if (parts.Length == 2)
            {
                return parts[1];
            }
            else if (parts.Length == 1)
            {
                return parts[0];
            }
            throw new Exception(string.Format("Invalid user name: " + fullyQualifiedName));
        }

        /// <summary>
        /// Gets the normalized user mappings file.
        /// </summary>
        /// <returns></returns>
        public void ReloadUserMappings()
        {
            Dictionary<string, string> res = new Dictionary<string, string>();

            Dictionary<string, string> map = ReadUsersMappingFile(_UserMappingFilePath, out _DefaultWindowsDomainName);

            if (string.IsNullOrEmpty(_DefaultWindowsDomainName))
            {
                throw new Exception(string.Format("The default domain name is not specified in file '{0}'. Expected a line, starting with {1}",
                                                  _UserMappingFilePath,
                                                  DummyUserNameForDefaultDomain));
            }

            foreach (KeyValuePair<string, string> pair in map)
            {
                string normalizedKey = GetFullyQualifiedWindowsUserName(pair.Key).ToLowerInvariant();
                res[normalizedKey] = pair.Value;
            }

            _UsersMapping = res;
        }

        /// <summary>
        /// Reads the users mapping file.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="defaultDomain">The default domain.</param>
        /// <returns></returns>
        public static Dictionary<string, string> ReadUsersMappingFile(string path, out string defaultDomain)
        {
            defaultDomain = null;
            Dictionary<string, string> res = new Dictionary<string, string>();

            if (!File.Exists(path))
            {
                return res;
            }

            const char delimiter = ':';
            foreach (string line in File.ReadAllLines(path))
            {
                string[] parts = line.Split(new char[] {delimiter}, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length != 2)
                {
                    throw new Exception(string.Format("Unexpected line in users mapping file '{0}'. Line: {1}", path, line));
                }

                string windowsUserName = parts[0].Trim();
                string validataUserName = parts[1].Trim();

                if (string.Compare(windowsUserName, DummyUserNameForDefaultDomain, true) == 0)
                {
                    // a bit of a hack (using XML file instead of this custom format, would make this obsolete)
                    defaultDomain = validataUserName;
                }
                else
                {
                    res[windowsUserName] = validataUserName;
                }
            }

            return res;
        }
    }
}