﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidataCommon
{
    /// <summary>
    /// AdapterGatewayInfo
    /// </summary>
    public static class AdapterGatewayInfo
    {
        /// <summary>
        /// Adapter gateway executable name
        /// </summary>
        public const string AdapterGatewayExecutableName = "AdapterGateway.exe";

        private const string AdapterGatewayDirectory = "AdapterGateway";

        private static string _Directory;
        
        /// <summary>
        /// Get full directory path to AdapterGateway
        /// </summary>
        /// <param name="validataRootDirectory"></param>
        /// <returns></returns>
        public static string GetDirectory(string validataRootDirectory)
        {
                if (_Directory == null)
                {
                    //var validataRootDirectory = Global.GetCurrentRequestSafe().PhysicalApplicationPath;

                    if (string.IsNullOrEmpty(Path.GetFileName(validataRootDirectory)))
                        validataRootDirectory = Path.GetDirectoryName(validataRootDirectory);
                    validataRootDirectory = Path.GetDirectoryName(validataRootDirectory);
                    _Directory = Path.Combine(validataRootDirectory, AdapterGatewayDirectory);

#if DEBUG
                    if (!System.IO.Directory.Exists(_Directory))
                    {
                        var sourceRoot = Path.GetDirectoryName(_Directory);
                        sourceRoot = Path.GetDirectoryName(sourceRoot);
                        sourceRoot = Path.GetDirectoryName(sourceRoot);
                        sourceRoot = Path.GetDirectoryName(sourceRoot);

                        var dir1 = Path.Combine(sourceRoot, @"Validata\AdapterGateway\bin\Debug");
                        var exe1 = Path.Combine(dir1, AdapterGatewayExecutableName);

                        var dir2 = Path.Combine(sourceRoot, @"Validata\AdapterGateway\bin\x86\Debug");
                        var exe2 = Path.Combine(dir2, AdapterGatewayExecutableName);


                        if (File.Exists(exe1) && File.Exists(exe2))
                        {
                            bool isFirstNewer = File.GetLastWriteTime(exe1) > File.GetLastWriteTime(exe2);
                            _Directory = isFirstNewer ? dir1 : dir2;
                        }
                        else if (File.Exists(exe1))
                        {
                            _Directory = dir1;
                        }
                        else if (File.Exists(exe2))
                        {
                            _Directory = dir2;
                        }
                        else
                        {
                            throw new System.ApplicationException("Inspecified or uncompiled 'AdapterGateway' executable");
                        }
                    }
#endif
                }

                return _Directory;
        }

        private static string _FullPath;
        public static string FullPath(string validataRootDirectory)
        {
                if (_FullPath == null)
                    _FullPath = Path.Combine(GetDirectory(validataRootDirectory), AdapterGatewayExecutableName);

            return _FullPath;

        }
    }
}
