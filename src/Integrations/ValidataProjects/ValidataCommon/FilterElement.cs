﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Web;
using System.Xml;
using Validata.Common;

namespace ValidataCommon
{
    #region Public Enumerations

    /// <summary>
    /// 	Filter type
    /// </summary>
    public enum FilterType
    {
        /// <summary>
        /// 	Greater Than
        /// </summary>
        GreaterThan = 0,

        /// <summary>
        /// 	Greater Or Equal
        /// </summary>
        GreaterOrEqual = 1,

        /// <summary>
        /// 	Equal
        /// </summary>
        Equal = 2,

        /// <summary>
        /// 	Less Or Equal
        /// </summary>
        LessOrEqual = 3,

        /// <summary>
        /// 	Less
        /// </summary>
        Less = 4,

        /// <summary>
        /// 	Like
        /// </summary>
        Like = 5,

        /// <summary>
        /// 	Not Equal
        /// </summary>
        NotEqual = 6,

        /// <summary>
        /// 	Not Like
        /// </summary>
        NotLike = 7,

        /// <summary>
        /// 	Enquiry Result Template Name
        /// </summary>
        TemplateName = 8,
        /// <summary>
        /// Between
        /// </summary>
        Between = 9


    }

    public enum FilterCondition
    {

        AND = 0,
        OR = 1,
        Unknown
    }

    #endregion

    /// <summary>
    /// </summary>
    [Serializable]
    public class FilterElement : ValidataCommon.Interfaces.IFilteringConstraint
    {
        #region Private and Protected Members

        protected string _AttributeName;
        protected string _AttributeLimit;
        protected string _AttributeFilter;
        protected AttributeDataType _AttributeDataType;
        protected FilterType _FilterType;
        protected FilterCondition _FilterCondition;

        protected decimal _DecTestVal;
        protected double _DblTestVal;
        protected string _StrTestVal;
        protected long _IntTestVal;
        protected DateTime _DateTestVal;
        protected bool _IsInitialized;

        #endregion

        #region Filters

        /// <summary>
        /// 	Filters tag
        /// </summary>
        public const string FILTERS_TAG = "filters";

        /// <summary>
        /// 	Enquiry Mask
        /// </summary>
        public const string ENQUIRY_MASK_TAG = "enquiryMask";

        /// <summary>
        /// 	Filter Tag
        /// </summary>
        public const string FILTER_TAG = "filter";

        /// <summary>
        /// 	Filter Attribute Name Tag
        /// </summary>
        public const string FILTER_ATTRIBUTE_NAME_TAG = "AttributeName";

        /// <summary>
        /// 	Filter Attribute Filter Tag
        /// </summary>
        public const string FILTER_ATTRIBUTE_FILTER_TAG = "AttributeFilter";

        /// <summary>
        /// 	Filter Attribute Data Type Tag
        /// </summary>
        public const string FILTER_ATTRIBUTE_DATA_TYPE_TAG = "AttributeDataType";

        /// <summary>
        /// 	Filter CONDITION Tag
        /// </summary>
        public const string FILTER_CONITION_TAG = "FilterCondition";

        /// <summary>
        /// 	Filter Logical Operation Tag
        /// </summary>
        public const string FILTER_LOGICAL_OPERATION_TAG = "logicalOperation";

        #endregion

        #region Public Enumerations

        /// <summary>
        /// 	Names of filters to display in the GUI
        /// </summary>
        public static string[] FilterDisplayNames = { ">", ">=", "=", "<=", "<", "like", "<>", "not like" };

        #endregion

        #region Public Static Methods

        /// <summary>
        /// 	Gets the T24 filter criteria.
        /// </summary>
        /// <param name="fltType"> Type of the filter. </param>
        /// <returns> </returns>
        public static string GetT24FilterCriteria(FilterType fltType)
        {
            switch (fltType)
            {
                case FilterType.Equal:
                    return "EQ";
                case FilterType.NotEqual:
                    return "NE";
                case FilterType.Like:
                    return "LK";
                case FilterType.NotLike:
                    return "UL";
                case FilterType.GreaterThan:
                    return "GT";
                case FilterType.Less:
                    return "LT";
                case FilterType.GreaterOrEqual:
                    return "GE";
                case FilterType.LessOrEqual:
                    return "LE";
                case FilterType.Between:
                    return "RG";
                default:
                    throw new NotSupportedException("The filter " + fltType + " is not supported.");
            }
        }

        /// <summary>
        /// 	Gets the display name of the filter type from.
        /// </summary>
        /// <param name="filterSymbol"> The filter symbol. </param>
        /// <returns> </returns>
        public static FilterType GetFilterTypeFromDisplayName(string filterSymbol)
        {
            int index = FindDisplayNameIndex(filterSymbol);
            if (index < 0)
            {
                throw new InvalidOperationException(filterSymbol + " is not expected");
            }
            else
            {
                return (FilterType)index;
            }
        }

        /// <summary>
        /// 	Gets the display name of the filter type string from.
        /// </summary>
        /// <param name="filterSymbol"> The filter symbol. </param>
        /// <returns> </returns>
        public static string GetFilterTypeStringFromDisplayName(string filterSymbol)
        {
            return GetFilterTypeFromDisplayName(filterSymbol).ToString();
        }

        /// <summary>
        /// 	Converts Logical operation type to Filter Type
        /// </summary>
        /// <param name="logicalOperation"> Logical operation type </param>
        /// <returns> Returns converted Logical operation type </returns>
        public static FilterType LogicalOperationToFilterType(string logicalOperation)
        {
            return (FilterType)Enum.Parse(typeof(FilterType), logicalOperation, true);
        }

        /// <summary>
        /// 	Constructs a Filter object from XML.
        /// </summary>
        /// <param name="filterReader"> The XML defining the filter. </param>
        /// <param name="logicalOperation"> The logical operation attribute. </param>
        /// <returns> The constructed filter instance. </returns>
        public static FilterElement FromXML(XmlReader filterReader, string logicalOperation)
        {
            string attrName = "";
            string attrFilter = "";
            string attrLimit = "0";
            AttributeDataType attrDataType = AttributeDataType.Unknown;
            FilterCondition condition = FilterCondition.Unknown;
            while (filterReader.Read())
            {
                if (filterReader.NodeType == XmlNodeType.Element)
                {
                    switch (filterReader.Name)
                    {
                        case FILTER_TAG:
                            {
                                //do nothing
                                break;
                            }

                        case FILTER_ATTRIBUTE_NAME_TAG:
                            {
                                attrName = filterReader.ReadString();
                                break;
                            }
                        case FILTER_ATTRIBUTE_FILTER_TAG:
                            {
                                attrFilter = filterReader.ReadString();
                                break;
                            }
                        case FILTER_ATTRIBUTE_DATA_TYPE_TAG:
                            {
                                attrDataType = AttributeDataTypeConvertor.ToEnum(filterReader.ReadString());
                                break;
                            }
                        case FILTER_CONITION_TAG:
                            {
                                condition = filterReader.ReadString() == "OR" ? FilterCondition.OR : filterReader.ReadString() == "AND" ? FilterCondition.AND : FilterCondition.Unknown;
                                break;
                            }
                    }
                }
            }

            FilterType fType = LogicalOperationToFilterType(logicalOperation);
            FilterElement ft = new FilterElement(attrName, attrFilter, attrDataType, condition, fType, attrLimit);
            return ft;
        }

        #endregion

        #region Private Static methods

        private static int FindDisplayNameIndex(string filterDisplayName)
        {
            for (int i = 0; i < FilterDisplayNames.Length; i++)
            {
                if (FilterDisplayNames[i] == filterDisplayName)
                {
                    return i;
                }
            }

            return -1;
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// 	Default constructor
        /// </summary>
        public FilterElement()
        {
            _FilterType = FilterType.Equal;
        }

        /// <summary>
        /// 	Construtctor
        /// </summary>
        /// <param name="attrName"> Attribute name </param>
        /// <param name="attrFilter"> Attribute filter </param>
        /// <param name="attrDataType"> Attribute data type </param>
        /// <param name="ft"> Filter type </param>
        public FilterElement(string attrName, string attrFilter, AttributeDataType attrDataType, FilterType ft)
        {
            _AttributeName = attrName;
            AttributeFilter = attrFilter;
            _AttributeDataType = attrDataType;
            _FilterType = ft;

            InitCulturesAndTestValues();
            _IsInitialized = true;
        }

        public FilterElement(string attrName, string attrFilter, AttributeDataType attrDataType, FilterCondition fc, FilterType ft, string Limit)
        {
            _AttributeName = attrName;
            AttributeFilter = attrFilter;
            _AttributeDataType = attrDataType;
            _FilterCondition = fc;
            _FilterType = ft;

            InitCulturesAndTestValues();
            _IsInitialized = true;
            _AttributeLimit = Limit;
        }

        /// <summary>
        /// 	Construtctor
        /// </summary>
        /// <param name="src">Source</param>
        public FilterElement(ValidataCommon.Interfaces.IFilteringConstraint src)
        {
            _AttributeName = src.AttributeName;
            AttributeFilter = src.TestValue;
            _AttributeDataType = src.DataType;
            _FilterCondition = src.Condition;
            _FilterType = src.Operator;

            InitCulturesAndTestValues();
            _IsInitialized = true;
        }

        #endregion

        /// <summary>
        /// 	Attribute name
        /// </summary>
        public string AttributeName
        {
            get { return _AttributeName; }
            set { _AttributeName = value; }
        }

        /// <summary>
        /// 	Attribute limit
        /// </summary>
        public string AttributeLimit
        {
            get { return _AttributeLimit; }
            set { _AttributeLimit = value; }
        }

        /// <summary>
        /// 	Attribute filter
        /// </summary>
        public string AttributeFilter
        {
            get { return _AttributeFilter; }
            set
            {
                _AttributeFilter = value;
                if (_AttributeFilter.CompareTo("No Value") == 0)
                {
                    _AttributeFilter = string.Empty;
                }
            }
        }

        public FilterCondition Condition
        {
            get { return _FilterCondition; }
            set { _FilterCondition = value; }
        }

        /// <summary>
        /// 	Attribute data type
        /// </summary>
        public AttributeDataType AttributeDataType
        {
            get { return _AttributeDataType; }
            set { _AttributeDataType = value; }
        }

        /// <summary>
        /// 	Filter type
        /// </summary>
        public FilterType Type
        {
            get { return _FilterType; }
            set { _FilterType = value; }
        }

        /// <summary>
        /// Get whether the filter is post or pre
        /// </summary>
        public bool IsPostFilter { get; set; }

        /// <summary>
        /// Get whether the filter is mask or not
        /// </summary>
        public bool IsMask { get; set; }

        /// <summary>
        /// 	Gets the filter sql operator.
        /// </summary>
        /// <value> The filter sql operator. </value>
        public string FilterSqlOperator
        {
            get
            {
                switch (_FilterType)
                {
                    case FilterType.Equal:
                        return "=";
                    case FilterType.GreaterOrEqual:
                        return ">=";
                    case FilterType.GreaterThan:
                        return ">";
                    case FilterType.Less:
                        return "<";
                    case FilterType.LessOrEqual:
                        return "<=";
                    case FilterType.Like:
                        return " LIKE ";
                    case FilterType.NotEqual:
                        return "<>";
                    case FilterType.NotLike:
                        return " NOT LIKE ";
                }
                return "=";
            }
        }

        /// <summary>
        /// 	Serializes the filters section to the XmlWriter
        /// </summary>
        /// <param name="xmlWriter"> XmlWriter where to serialize the Filters section </param>
        public void ToXml(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(FILTER_TAG);

            xmlWriter.WriteAttributeString(FILTER_LOGICAL_OPERATION_TAG, Type.ToString().ToUpper());

            xmlWriter.WriteElementString(FILTER_ATTRIBUTE_NAME_TAG, AttributeName);

            xmlWriter.WriteElementString(FILTER_ATTRIBUTE_FILTER_TAG, AttributeFilter);

            xmlWriter.WriteElementString(FILTER_CONITION_TAG, Condition.ToString().ToUpper());

            xmlWriter.WriteElementString(FILTER_ATTRIBUTE_DATA_TYPE_TAG, Utils.GetHtmlEncodedValue(AttributeDataTypeConvertor.ToString(AttributeDataType).ToUpper()));

            xmlWriter.WriteEndElement();
        }

        #region Overrides

        /// <summary>
        /// 	Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see> .
        /// </summary>
        /// <returns> A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see> . </returns>
        public override string ToString()
        {
            return string.Format("[{0}] {1} {2}", AttributeName, Type, AttributeFilter);
        }

        public override bool Equals(object obj)
        {
            FilterElement o = obj as FilterElement;

            return o != null
                   && AttributeName == o.AttributeName
                   && AttributeFilter == o.AttributeFilter
                   && AttributeDataType == o.AttributeDataType
                   && Type == o.Type
                   && IsPostFilter == o.IsPostFilter;
        }

        #endregion

        /// <summary>
        /// 	This method checks if the passed value satisfies the rule defined in this filter
        /// </summary>
        /// <param name="attrValue"> The value to be tested. </param>
        /// <returns> True if the value satisfies the filter or false otherwise </returns>
        public bool Test(string attrValue)
        {
            if (!_IsInitialized)
            {
                InitCulturesAndTestValues();
                _IsInitialized = true;
            }

            // According to the type of the attribute check the value
            switch (AttributeDataType)
            {
                case AttributeDataType.String:
                    return CheckString(attrValue);
                case AttributeDataType.Integer:
                    return CheckInt(attrValue);
                case AttributeDataType.Real:
                    return CheckDouble(attrValue);
                case AttributeDataType.Currency:
                    return CheckDecimal(attrValue);
                case AttributeDataType.DateTime:
                    return CheckDateTime(attrValue);
            }

            //The value was not one of the possible types for this filter, so we return false
            return false;
        }

        #region Protected and Private methods

        #region Private methods

        /// <summary>
        /// 	Initialize the culture variables and
        /// </summary>
        protected void InitCulturesAndTestValues()
        {
            try
            {
                switch (AttributeDataType)
                {
                    case AttributeDataType.String:
                        _StrTestVal = AttributeFilter;
                        break;
                    case AttributeDataType.Integer:
                        _IntTestVal = Convert.ToInt64(AttributeFilter);
                        break;
                    case AttributeDataType.Real:
                        _DblTestVal = Convert.ToDouble(AttributeFilter);
                        break;
                    case AttributeDataType.Currency:
                        _DecTestVal = Convert.ToDecimal(AttributeFilter);
                        break;
                    case AttributeDataType.DateTime:
                        if (!ConvertToDate(AttributeFilter, out _DateTestVal))
                            throw new ApplicationException("Bad test value for Date Time filter!");
                        break;
                }
            }
            catch (FormatException fex)
            {
                throw new ApplicationException(string.Format("Bad test value for filter! Field Type: '{0}'  Test Value: '{1}'", AttributeDataType, AttributeFilter), fex);
            }
        }

        private bool CheckDouble(string attrValue)
        {
            // make sure, we have the proper decimal separator
            attrValue = attrValue.Replace(",", ".");

            double dblVal;
            if (!double.TryParse(attrValue, NumberStyles.Any, CultureInfo.InvariantCulture, out dblVal))
            {
                return false;
            }

            switch (_FilterType)
            {
                case FilterType.GreaterThan:
                    return (dblVal > _DblTestVal);
                case FilterType.GreaterOrEqual:
                    return (dblVal >= _DblTestVal);
                case FilterType.Equal:
                    return (dblVal == _DblTestVal);
                case FilterType.LessOrEqual:
                    return (dblVal <= _DblTestVal);
                case FilterType.Less:
                    return (dblVal < _DblTestVal);
                case FilterType.NotEqual:
                    return (dblVal != _DblTestVal);
                default:
                    return false;
            }
        }

        private bool CheckDecimal(string attrValue)
        {
            // make sure we have the proper decimal separator
            attrValue = attrValue.Replace(",", ".");

            decimal decVal;
            if (!decimal.TryParse(attrValue, NumberStyles.Currency, CultureInfo.InvariantCulture, out decVal))
            {
                return false;
            }

            switch (_FilterType)
            {
                case FilterType.GreaterThan:
                    return (decVal > _DecTestVal);
                case FilterType.GreaterOrEqual:
                    return (decVal >= _DecTestVal);
                case FilterType.Equal:
                    return (decVal == _DecTestVal);
                case FilterType.LessOrEqual:
                    return (decVal <= _DecTestVal);
                case FilterType.Less:
                    return (decVal < _DecTestVal);
                case FilterType.NotEqual:
                    return (decVal != _DecTestVal);
                default:
                    return false;
            }
        }

        private bool CheckInt(string attrValue)
        {
            long intVal;
            if (!long.TryParse(attrValue, out intVal))
            {
                return false;
            }

            switch (_FilterType)
            {
                case FilterType.GreaterThan:
                    return (intVal > _IntTestVal);
                case FilterType.GreaterOrEqual:
                    return (intVal >= _IntTestVal);
                case FilterType.Equal:
                    return (intVal == _IntTestVal);
                case FilterType.LessOrEqual:
                    return (intVal <= _IntTestVal);
                case FilterType.Less:
                    return (intVal < _IntTestVal);
                case FilterType.NotEqual:
                    return (intVal != _IntTestVal);
                default:
                    return false;
            }
        }

        private bool CheckString(string attrValue)
        {
            if (attrValue == null || attrValue.CompareTo("No Value") == 0)
            {
                attrValue = string.Empty;
            }

            switch (_FilterType)
            {
                case FilterType.GreaterThan:
                    return attrValue.CompareTo(_StrTestVal) > 0;
                case FilterType.GreaterOrEqual:
                    return attrValue.CompareTo(_StrTestVal) >= 0;
                case FilterType.Equal:
                    return attrValue.CompareTo(_StrTestVal) == 0;
                case FilterType.LessOrEqual:
                    return attrValue.CompareTo(_StrTestVal) <= 0;
                case FilterType.Less:
                    return attrValue.CompareTo(_StrTestVal) < 0;
                case FilterType.Like:
                    // return attrValue.IndexOf(_StrTestVal) >= 0;
                    return CompareStrWildcard(_StrTestVal, attrValue);
                case FilterType.NotEqual:
                    return attrValue.CompareTo(_StrTestVal) != 0;
                case FilterType.NotLike:
                    return !CompareStrWildcard(_StrTestVal, attrValue);
                default:
                    Debug.Fail("Unexpected filter type");
                    return false;
            }
        }

        /*
        private void BuildRegEx()
        {
            System.Diagnostics.Debug.Assert(_RegExTextValue == null);
            string strR = "^" + System.Text.RegularExpressions.Regex.Escape(_StrTestVal)
                .Replace("\\*", ".*")
                .Replace("\\?", ".") + "$";
            _RegExTextValue = new System.Text.RegularExpressions.Regex(strR);
        }*/

        private static bool CompareStrWildcard(string chkVal, string val)
        {
            // TODO: Uncomment all RegEx items to do real wildcard search.
            //       The problem is current difference in syntax of Export/Import
            //       Currently the export routine uses Oracle % (authomaticaly added) for wildcard, 
            //       user could be confused!
            /*
            System.Diagnostics.Debug.Assert(_RegExTextValue != null);
            System.Text.RegularExpressions.Match m = _RegExTextValue.Match(val);
            return (m.Length != 0);*/

            return (val.IndexOf(chkVal) >= 0);
        }

        private bool CheckDateTime(string attrValue)
        {
            DateTime dateVal;

            if (ConvertToDate(attrValue, out dateVal))
            {
                switch (_FilterType)
                {
                    case FilterType.GreaterThan:
                        return (dateVal > _DateTestVal);
                    case FilterType.GreaterOrEqual:
                        return (dateVal >= _DateTestVal);
                    case FilterType.Equal:
                        return (dateVal == _DateTestVal);
                    case FilterType.LessOrEqual:
                        return (dateVal <= _DateTestVal);
                    case FilterType.Less:
                        return (dateVal < _DateTestVal);
                    case FilterType.NotEqual:
                        return (dateVal != _DateTestVal);
                }
            }
            return false;
        }

        private static bool CompareWithTaskItems(string chkVal, string val)
        {

            return val.CompareTo(chkVal) == 0;
        }

        #endregion

        /// <summary>
        /// 	Convert a text representation of a date to a DateTime object representation.
        /// </summary>
        /// <param name="filterVal"> The test value </param>
        /// <param name="dateVal"> Stores the result if conversion is successfull </param>
        /// <returns> True if conversion is successfull, false otherwise. </returns>
        protected static bool ConvertToDate(string filterVal, out DateTime dateVal)
        {
            dateVal = ValidataConverter.ParseDateTime(filterVal, DateParsingOptions.All);
            return dateVal != DateTime.MinValue;
        }

        #endregion

        #region Implementation ValidataCommon.Interfaces.IFilteringConstraint

        public string TestValue { get { return this.AttributeFilter; } }

        public AttributeDataType DataType { get { return this.AttributeDataType; } }

        public FilterType Operator { get { return this.Type; } }

        #endregion
    }

    [Serializable]
    public class FilterElementList : System.Collections.Generic.List<FilterElement>
    {
        public void AddFilter(Interfaces.IFilteringConstraint filter)
        {
            this.Add(new FilterElement(filter.AttributeName, filter.TestValue, filter.DataType, filter.Condition, filter.Operator, filter.AttributeLimit));
        }
    }
}