using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Generic Log
    /// </summary>
    public class Logger : IDisposable
    {
        #region Private and Protected members

        private readonly string _LogFileName;

        private FileStream _FileStream;
        private StreamWriter _StreamWriter;
        private bool _Disposed;

        private bool _OverwriteLog;

        #endregion

        #region Public properties

        /// <summary>
        /// Adds a date time prefix to the message
        /// </summary>
        public bool AddDateTimePrefix;

        /// <summary>
        /// Output the message to the console
        /// </summary>
        public bool OutputToConsole;

        /// <summary>
        /// Returns the name of the log file where messages are written
        /// </summary>
        public string LogFileName
        {
            get { return _LogFileName; }
        } 

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Initializes a new instance of the GenericLog.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="addDateTimePrefix">if set to <c>true</c> [add date time prefix].</param>
        /// <param name="outputToConsole">if set to <c>true</c> [output to console].</param>
        public Logger(string fileName, bool addDateTimePrefix, bool outputToConsole)
        {
            _LogFileName = fileName;
            AddDateTimePrefix = addDateTimePrefix;
            OutputToConsole = outputToConsole;
            _OverwriteLog = false;
        }


        /// <summary>
        /// Initializes a new instance of the GenericLog.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="addDateTimePrefix">if set to <c>true</c> [add date time prefix].</param>
        /// <param name="outputToConsole">if set to <c>true</c> [output to console].</param>
        /// <param name="overwriteLog">if set to <c>true</c> and the file exists it is overwritten.</param>
        public Logger(string fileName, bool addDateTimePrefix, bool outputToConsole, Boolean overwriteLog)
        {
            _LogFileName = fileName;
            AddDateTimePrefix = addDateTimePrefix;
            OutputToConsole = outputToConsole;
            _OverwriteLog = overwriteLog;
        }

        #endregion

        #region File stream management

        private void OpenStreamsIfClosed()
        {
            if (_FileStream == null)
            {
                try
                {
                    AssureLogDirectoryExists();

                    _FileStream = _OverwriteLog
                        ? File.Open(_LogFileName, FileMode.Create, FileAccess.Write, FileShare.Read)
                        : File.Open(_LogFileName, FileMode.Append, FileAccess.Write, FileShare.Read);
                }
                catch (IOException)
                {
                    // try once again with different file name
                    _FileStream = _OverwriteLog
                        ? File.Open(_LogFileName + ".1", FileMode.Create, FileAccess.Write, FileShare.Read)
                        : File.Open(_LogFileName + ".1", FileMode.Append, FileAccess.Write, FileShare.Read);
                }

                _OverwriteLog = false; // once the log is overwritten, we would like to only append
                _StreamWriter = new StreamWriter(_FileStream, Encoding.Default);
            }
        }

        private void AssureLogDirectoryExists()
        {
            var directory = Path.GetDirectoryName(_LogFileName);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }

        private void CloseStreamsIfOpen()
        {
            if (_FileStream != null)
            {
                _StreamWriter.Close();
                _StreamWriter = null;

                _FileStream.Close();
                _FileStream = null;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Writes a formated message line to the log
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void WriteMessageFormat(string format, params object[] args)
        {
            WriteMessage(string.Format(format, args), true);
        }

        /// <summary>
        /// Writes a message line to the log
        /// </summary>
        /// <param name="message">Message</param>
        public void WriteMessage(string message)
        {
            WriteMessage(message, true);
        }

        /// <summary>
        /// Writes a message to the log
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="writeNewLine">Write new line</param>
        public void WriteMessage(string message, bool writeNewLine)
        {
            try
            {
                if (String.IsNullOrEmpty(message))
                {
                    return;
                }

                if (AddDateTimePrefix)
                {
                    message = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.ffff ") + message;
                }

                if (OutputToConsole)
                {
                    if (writeNewLine)
                    {
                        Console.WriteLine(message);
                    }
                    else
                    {
                        Console.Write(message);
                    }
                }

                OpenStreamsIfClosed();

                if (writeNewLine)
                {
                    _StreamWriter.WriteLine(message);
                }
                else
                {
                    _StreamWriter.Write(message);
                }

                _StreamWriter.Flush();
            }
            catch (Exception ex)
            {
               // Debug.Fail("Failed to write a log entry", ex.ToString());
                Trace.Write(ex);
            }
        }

        #endregion

        #region IDisposable Members

        ///<summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        ///</summary>
        ///<filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Displose
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_Disposed)
            {
                if (disposing)
                {
                    CloseStreamsIfOpen();
                }

                _Disposed = true;
            }
        }

        #endregion
    }
}