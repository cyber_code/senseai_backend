﻿using System;
using System.Collections.Generic;

namespace ValidataCommon.Interfaces
{
    /// <summary>
    /// interface for accessing data from extraction streaming adapters
    /// </summary>
    public interface IAdapterResult: IDisposable
    {
        IEnumerable<IValidataRecord> GetEnumerable();
    }
}
