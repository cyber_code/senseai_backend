﻿namespace ValidataCommon
{
    /// <summary>
    /// Interface describing a typical including additional info
    /// </summary>
    public interface IMetadataEx : IMetadata
    {
        /// <summary>
        /// Base class of the metadata/typical
        /// </summary>
        string BaseClass { get; }
    }
}
