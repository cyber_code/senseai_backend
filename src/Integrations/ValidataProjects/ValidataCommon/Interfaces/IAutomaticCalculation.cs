﻿using System;
using System.Linq;
using System.Collections.Generic;
using Validata.Common;

namespace ValidataCommon.Interfaces
{
    public interface IAutomaticCalculation
    {
        ulong Noderef { get; }
        string AttributeName { get; }
        string PlainTextFormula { get; }
        string PreserveResultFunctionName { get; }
        List<IAutomaticCalculationItem> Items { get; }
    }

    public enum CollectionSource : int
    {
        TestData = 0,
        ActualResult = 1,
        Response = 2,
        ExpectedResult = 3,
        VirtualData = 4
    }

    public enum ExpressionType
    {
        Unsupported,
        Real,
        Currency,
        Integer,
        Date,
        String,
        Boolean,

        Custom
    }

    public interface IAutomaticCalculationItem
    {
        ulong Noderef { get; }
        string VariableName { get; }
        CollectionSource CollectFrom { get; }
        string AttributeName { get; }
        string CatalogName { get; }
        string TypicalName { get; }
        ExpressionType DataType { get; }
        string SourceTestStepID { get; }

        string SourceAttributeValue { get; }
        AttributeDataType SourceAttributeDataType { get; }
    }

    public struct AutomaticCalculationSourceType
    {
        public CollectionSource CollectFrom;
        public string CatalogName;
        public string TypicalName;

        public AutomaticCalculationSourceType(CollectionSource collectFrom, string catalogName, string typicalName)
        {
            CollectFrom = collectFrom;
            CatalogName = catalogName;
            TypicalName = typicalName;
        }

        public override string ToString()
        {
            return string.Format("{0} of type [{1}]:{2}", CollectFrom, CatalogName, TypicalName);
        }
    }

    public static class IAutomaticCalculationExtensions
    {
        public static ExpressionType ToExpressionType(this AttributeDataType dataType)
        {
            switch (dataType)
            {
                case AttributeDataType.Integer:
                    return ExpressionType.Integer;
                case AttributeDataType.Real:
                    return ExpressionType.Real;
                case AttributeDataType.String:
                    return ExpressionType.String;
                case AttributeDataType.Boolean:
                    return ExpressionType.Boolean;
                case AttributeDataType.Currency:
                    return ExpressionType.Currency;
                case AttributeDataType.DateTime:
                    return ExpressionType.Date;
            }

            return ExpressionType.Unsupported;
        }

        public static AttributeDataType ToDataType(this ExpressionType value)
        {
            switch (value)
            {
                case ExpressionType.Integer:
                    return AttributeDataType.Integer;
                case ExpressionType.Real:
                    return AttributeDataType.Real;
                case ExpressionType.String:
                    return AttributeDataType.String;
                case ExpressionType.Boolean:
                    return AttributeDataType.Boolean;
                case ExpressionType.Currency:
                    return AttributeDataType.Currency;
                case ExpressionType.Date:
                    return AttributeDataType.DateTime;
            }

            return AttributeDataType.Unknown;
        }

        public static ExpressionType ToExpressionType(this Type type)
        {
            if (type == typeof(decimal))
                return ExpressionType.Currency;

            if (type == typeof(DateTime))
                return ExpressionType.Date;

            if (type == typeof(Int16) ||
                type == typeof(Int32) ||
                type == typeof(Int64) ||
                type == typeof(uint) ||
                type == typeof(UInt16) ||
                type == typeof(UInt32) ||
                type == typeof(UInt64))
            {
                return ExpressionType.Integer;
            }

            if (type == typeof(float) ||
                type == typeof(double))
                return ExpressionType.Real;

            if (type == typeof(bool))
                return ExpressionType.Boolean;

            if (type == typeof(string))
                return ExpressionType.String;

            if (type == typeof(List<String>))
                return ExpressionType.Custom;

            return ExpressionType.Real;
        }

        public static Type ToSystemType(this ExpressionType expressionType, object value)
        {
            switch (expressionType)
            {
                case ExpressionType.Currency:
                    return typeof(decimal);
                case ExpressionType.Date:
                    return typeof(DateTime);
                case ExpressionType.Integer:
                    return typeof(Int64);
                case ExpressionType.Real:
                    return typeof(double);
                case ExpressionType.Boolean:
                    return typeof(bool);
                case ExpressionType.String:
                    return typeof(string);
                case ExpressionType.Custom:
                    return (value == null) ? typeof(object) : value.GetType();
                default:
                    return typeof(double);
            }
        }

        public static bool IsEqualTo(this IAutomaticCalculation ac, IAutomaticCalculation other, bool testNoderefs = false)
        {
            return (!testNoderefs || (ac.Noderef == other.Noderef))
                && ac.PlainTextFormula.IsSimilarTo(other.PlainTextFormula)
                && ac.PreserveResultFunctionName.IsSimilarTo(other.PreserveResultFunctionName)
                && ac.AttributeName == other.AttributeName
                && ac.Items.AreEqualTo(other.Items, testNoderefs);
        }

        private static bool AreEqualTo(this List<IAutomaticCalculationItem> items, List<IAutomaticCalculationItem> others, bool testNoderefs)
        {
            if ((items == null || items.Count == 0) && (others == null || others.Count == 0))
                return true;

            if ((items == null || items.Count == 0) || (others == null || others.Count == 0))
                return false;

            if (items.Count != others.Count)
                return false;

            if (testNoderefs)
            {
                foreach (var item in items)
                {
                    var other = others.FirstOrDefault(n => n.Noderef == item.Noderef);
                    if (!item.IsEqualTo(other, testNoderefs))
                        return false;
                }
            }
            else
            {
                foreach (var item in items)
                {
                    var other = others.FirstOrDefault(n => n.VariableName == item.VariableName);
                    if (!item.IsEqualTo(other, testNoderefs))
                        return false;
                }
            }

            return true;
        }

        public static bool IsEqualTo(this IAutomaticCalculationItem item, IAutomaticCalculationItem other, bool testNoderef = false)
        {
            if (item == null && other == null)
                return true;
            if (item == null || other == null)
                return false;

            return (!testNoderef || (item.Noderef == other.Noderef))
                && item.VariableName.IsSimilarTo(other.VariableName)
                && item.CollectFrom == other.CollectFrom
                && item.AttributeName.IsSimilarTo(other.AttributeName)
                && item.CatalogName.IsSimilarTo(other.CatalogName)
                && item.TypicalName.IsSimilarTo(other.TypicalName)
                && item.DataType == other.DataType
                && item.SourceTestStepID.IsSimilarTo(other.SourceTestStepID);
        }

        private static bool IsSimilarTo(this string a, string b)
        {
            if (a.IsEmptyOrNoValue() && b.IsEmptyOrNoValue())
                return true;

            if (a.IsEmptyOrNoValue() || b.IsEmptyOrNoValue())
                return false;

            return a.Trim() == b.Trim();
        }
    }
}
