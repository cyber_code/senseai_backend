﻿namespace ValidataCommon.Interfaces
{
    public interface ITagUniqueCalculator
    {
        string EvalFunction(string functionName, object argument);
    }
}
