﻿using System;

namespace ValidataCommon.Interfaces
{
    /// <summary>
    /// Delegate for subscription for available T24 user from the users pool
    /// </summary>
    /// <param name="envName">TelnetSet(T24 Adapter)/Configuration(T24WebAdapter, GlobusDesktopAdapter) Name</param>
    /// <param name="userName">Search for available, equivalent to this user in the users pool</param>
    /// <param name="adapter">Adapter Type</param>
    public delegate Tuple<string, string> DequeueT24UserDelegate(string envName, string userName, AdapterType adapter, DequeueT24UserLogMessageHandler onLogMessage);

    /// <summary>
    /// Delegate for subscription for available user from the users pool
    /// </summary>
    /// <param name="envName">TelnetSet(Adapter)/Configuration(T24WebAdapter, GlobusDesktopAdapter) Name</param>
    /// <param name="userName">Search for available, equivalent to this user in the users pool</param>
    /// <param name="adapterName">Adapter Type Name</param>
    public delegate Tuple<string, string> DequeueT24UserDelegateNew(string envName, string userName, string adapterName, DequeueT24UserLogMessageHandler onLogMessage);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    public delegate void DequeueT24UserLogMessageHandler(string message);

    /// <summary>
    /// Delegate for release of all T24 users used in the scope of the cycle for the current AdapterExecutionContext
    /// </summary>
    public delegate void ReleaseT24UsersDelegate();

    /// <summary>
    /// Execution Context for Adapters
    /// </summary>
    public interface IAdapterExecutionContext
    {
        /// <summary>
        /// Gets the test suite ID (or the execution ID if not in the scope of the test sute)
        /// </summary>
        string TestCycleID { get; }

        /// <summary>
        /// Gets the test case ID (valid only in the context of test sute execution)
        /// </summary>
        ulong TestCaseID { get; }
        
        /// <summary>
        /// Gets the test step ID (valid only in the context of test suite execution)
        /// </summary>
        ulong TestStepID { get; }

        /// <summary>
        /// Gets the temp directory.
        /// </summary>
        string TempDirectory { get; }

        /// <summary>
        /// Gets the name of the environment.
        /// </summary>
        /// <value>
        /// The name of the environment.
        /// </value>
        string EnvironmentName { get; }

        /// <summary>
        /// Delegate for subscription for available T24 user from the users pool
        /// </summary>
        DequeueT24UserDelegate DequeueT24User { get; set; }

        /// <summary>
        /// Delegate for subscription for available user from the users pool
        /// </summary>
        DequeueT24UserDelegateNew DequeueT24UserNew { get; set; }

        /// <summary>
        /// Delegate for release of T24 users
        /// </summary>
        ReleaseT24UsersDelegate ReleaseT24Users { get; set; }
    }
}
