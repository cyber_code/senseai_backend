﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    public static class IMetadataExtensions
    {
        public static Dictionary<string, List<IMetadataAttribute>> GroupAttributesByShortName(this IMetadata metadata)
        {
            if (metadata == null)
                throw new ArgumentNullException("Metadata must be defined!");

            var result = new Dictionary<string, List<IMetadataAttribute>>();
            if (metadata.AttributesList == null)
                return result;

            foreach (var attr in metadata.AttributesList)
            {
                string shortName;
                int multiValueIndex, subValueIndex;
                AttributeNameParser.ParseComplexFieldName(attr.Name, out shortName, out multiValueIndex, out subValueIndex);
                if (!result.ContainsKey(shortName))
                    result[shortName] = new List<IMetadataAttribute>();
                result[shortName].Add(attr);
            }

            return result;
        }
    }
}
