﻿namespace ValidataCommon
{
    public interface IValidataUIDRecord : IValidataRecord
    {
        ulong Noderef
        {
            get;
        }
    }
}
