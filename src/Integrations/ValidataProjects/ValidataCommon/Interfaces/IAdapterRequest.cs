﻿using System.Collections.Generic;

namespace ValidataCommon.Interfaces
{
    public interface IAdapterRequest
    {
        IAdapterExecutionContext ExecutionContext { get; }

        IAdapterParameters Parameters { get; }

        IMetadataEx RequestMetadata { get; }

        IEnumerable<IValidataRecord> RequestData { get; }

        IMetadataEx ResultMetadata { get; }

        void AddLogMessage(string message);
    }

    public interface IWriteAdapterRequest : IAdapterRequest
    {
    }

    public interface IReadAdapterRequest : IAdapterRequest
    {
        IEnumerable<IFilteringConstraint> Filters { get; }
        IEnumerable<IFilteringConstraint> PostFilters { get; }
    }
}
