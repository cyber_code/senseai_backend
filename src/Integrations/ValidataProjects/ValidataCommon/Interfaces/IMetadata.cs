using System.Collections.Generic;

namespace ValidataCommon
{
    /// <summary>
    /// Interface describing a typical
    /// </summary>
    public interface IMetadata
    {
        /// <summary>
        /// Gets the name of the catalog.
        /// </summary>
        /// <value>The name of the catalog.</value>
        string CatalogName { get; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        string Name { get; }

        /// <summary>
        /// Base class of the metadata/typical
        /// </summary>
        string BaseClass { get; }

        /// <summary>
        /// Gets the attributes list.
        /// </summary>
        /// <value>The attributes list.</value>
        List<IMetadataAttribute> AttributesList { get; }
    }
}
