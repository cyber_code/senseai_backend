﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon.Interfaces
{
    public interface IPerformanceTestingResultsAnalysisData
    {
        string TransactionName { get; }
        IErrorInfo ErrorOccuranceInfo { get; }
        bool FirstFound { get; }
        int FirstIndex { get; }
        int NUsersOfFirstDelay { get; }

        int HighErrorPerc { get; }
        int HighRespIncr { get; }
    }


    public interface IErrorInfo
    {
        bool ErrorFound { get; set; }
        DateTime MomentOfFirstError { get; set; }
        string TypeOfError { get; set; }
        int ActualLoadOfError { get; set; }

        int HttpErrorCnt { get; set; }
        int AppErrorCnt { get; set; }
        int WebErrorCnt { get; set; }
        int LoadOfError { get; set; }

        int TotalNumberOfExecutions { get; set; }
        int TotErrorCnt { get;  }
        float ErrorsPerc { get;  }
        
    }
}
