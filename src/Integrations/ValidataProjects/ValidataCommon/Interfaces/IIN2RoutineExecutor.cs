﻿namespace ValidataCommon.Interfaces
{
    /// <summary>
    /// T24 IN2 Routine Executor Interface
    /// </summary>
    public interface IIN2RoutineExecutor
    {
        /// <summary>
        /// Contains routine
        /// </summary>
        /// <param name="routine"></param>
        /// <returns></returns>
        bool ContainsRoutine(string routine);

        /// <summary>
        /// Validates value
        /// </summary>
        /// <param name="fieldInfo"></param>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        bool Validate(FieldInfo fieldInfo, string fieldName, string value, out string errorMessage);

        /// <summary>
        /// Is non executable routine
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        bool IsNonExecutable(string routine);
    }
}
