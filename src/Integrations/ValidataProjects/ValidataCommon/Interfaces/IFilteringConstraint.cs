﻿using Validata.Common;

namespace ValidataCommon.Interfaces
{
    public interface IFilteringConstraint
    {
        string AttributeName { get; }
        string TestValue { get; }           // TODO: Matches to AttributeFilter of the AXMLEngine.Filter
        AttributeDataType DataType { get; } // TODO: Matches to AttributeDataType of the AXMLEngine.Filter
        FilterType Operator { get; }        // TODO: Matches to Type of the AXMLEngine.Filter

        FilterCondition Condition { get; }
        string AttributeLimit { get; }


    }
}
