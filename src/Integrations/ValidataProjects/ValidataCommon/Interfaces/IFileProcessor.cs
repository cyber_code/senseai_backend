﻿
using System;

namespace ValidataCommon.Interfaces
{
    /// <summary>
    /// Entry point for file processor of the Generic File Interface adapter
    /// </summary>
    public interface IFileProcessor : IDisposable
    {
        /// <summary>
        /// Used in Postpone mode. Point to the file location with the preserved data.
        /// </summary>
        string KeyDataStoragePlace { get; set; }

        /// <summary>
        /// Generate file contents for the request
        /// </summary>
        /// <param name="request">Adapter Gateway request data</param>
        /// <param name="contents">Output file contents</param>
        /// <returns>Adapter result</returns>
        IAdapterResult Generate(IWriteAdapterRequest request, out IFileContents contents);

        /// <summary>
        /// Parses file contents
        /// </summary>
        /// <param name="request">Adapter Gateway request data</param>
        /// <param name="contents">File contents</param>
        /// <returns>Adapter result</returns>
        IAdapterResult Parse(IReadAdapterRequest request, IFileContents contents);

        /// <summary>
        /// Parses file contents
        /// </summary>
        /// <param name="request">Adapter Gateway request data</param>
        /// <param name="contents">File contents</param>
        /// <param name="errors">File with errors</param>
        /// <returns>Adapter result</returns>
        IAdapterResult Parse(IReadAdapterRequest request, IFileContents contents, IFileContents errors);
    }
}
