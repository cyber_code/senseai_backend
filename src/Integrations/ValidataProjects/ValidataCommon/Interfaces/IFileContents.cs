﻿using System.IO;

namespace ValidataCommon.Interfaces
{
    public interface IFileContents
    {
        /// <summary>
        /// Data size
        /// </summary>
        long Size { get; }

        /// <summary>
        /// Is datareader is presented (not null) only it would be used for reading of the generated contents
        /// </summary>
        StreamReader GetDataReader();

        bool IsBinary { get; }
        string ToString();
        byte[] ToByteArray();
    }
}
