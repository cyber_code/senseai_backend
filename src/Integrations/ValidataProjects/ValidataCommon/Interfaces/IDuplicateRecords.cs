﻿namespace ValidataCommon.Interfaces
{
    public interface IDuplicateRecords
    {
        string UniqueID { get; }
        int CountOfDuplicates { get; }
    }
}
