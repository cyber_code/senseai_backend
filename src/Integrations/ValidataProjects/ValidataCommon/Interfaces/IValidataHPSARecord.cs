﻿using System.Collections.Generic;

namespace ValidataCommon
{
    public interface IValidataHPSARecord : IValidataUIDRecord
    {
        string TagUnique { get; }

        IEnumerable<ILogRecord> LogRecords { get; }
    }
}
