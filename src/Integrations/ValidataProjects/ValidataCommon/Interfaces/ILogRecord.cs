﻿using System;

namespace ValidataCommon
{
    [Flags]
    public enum LogRecordType
    {
        LevelMask = 0x000F,
        AttributeLevel = 0x0001,
        InstanceLevel = 0x0002,

        TypeMask = 0x00F0,
        Error = 0x0010,
        Warning = 0x0020,
        Skipped = 0x0040,


        ErrorMask = 0xFF00, 
        Validation = 0x0100,
        Transformation = 0x0200,
        Rejection = 0x0400,
        OnlyInT24 = 0x0800,
        Difference_Att_value = 0x1000,
        Difference_Att_T24_Empty = 0x2000,
        Difference_Att_Source_Empty = 0x4000,
        OnlyInValidata = 0x8000,
    }

    public interface ILogRecord
    {
        /// <summary>
        /// Type of log message
        /// </summary>
        LogRecordType Type
        {
            get;
        }

        /// <summary>
        /// Log message
        /// </summary>
        string Message
        {
            get;
        }

        /// <summary>
        /// Referenced attribute name
        /// </summary>
        string RefAttributeName
        {
            get;
        }
    }
}
