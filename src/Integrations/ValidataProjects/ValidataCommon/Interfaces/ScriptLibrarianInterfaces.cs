﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;


namespace ValidataCommon.Interfaces.ScriptLibrarian
{
    public enum ApplicationType
    {
        /// <summary>
        /// 	Windows Desktop Application
        /// </summary>
        Desktop,

        /// <summary>
        /// 	Web application
        /// </summary>
        Web,

        /// <summary>
        /// 	T24 application, not used ?!?
        /// </summary>
        T24
    }

    public interface IApplication
    {
        /// <summary>
        /// 	Gets or sets the noderef.
        /// </summary>
        /// <value> The noderef. </value>
        string Noderef { get; set; }

        /// <summary>
        /// 	Gets or sets the name.
        /// </summary>
        /// <value> The name. </value>
        string Name { get; set; }

        /// <summary>
        /// 	Gets or sets the description.
        /// </summary>
        /// <value> The description. </value>
        string Description { get; set; }

        /// <summary>
        /// 	Gets or sets the type of the application.
        /// </summary>
        /// <value> The type of the application. </value>
        ApplicationType ApplicationType { get; set; }

        /// <summary>
        /// 	Gets or sets the attribute 'Catalogue Manager'
        /// </summary>
        /// <value> The catalog manager. </value>
        string CatalogManager { get; set; }

        /// <summary>
        /// 	Gets or sets the attribute 'Catalogue Name'
        /// </summary>
        /// <value> The name of the catalog. </value>
        string CatalogName { get; set; }

        /// <summary>
        /// 	Gets or sets the screens.
        /// </summary>
        /// <value> The screens. </value>
        IEnumerable<IScreen> Screens { get; set; }

        /// <summary>
        /// 	Gets or sets the scripts.
        /// </summary>
        /// <value> The scripts. </value>
        IEnumerable<IScript> Scripts { get; set; }
    }

    public interface IScreen
    {
        /// <summary>
        /// 	Gets or sets the noderef.
        /// </summary>
        /// <value> The noderef. </value>
        string Noderef { get; set; }

        /// <summary>
        /// 	Gets or sets the Unique ID
        /// </summary>
        /// <value> The noderef. </value>
        string ID { get; set; }

        /// <summary>
        /// 	Gets or sets the name.
        /// </summary>
        /// <value> The name. </value>
        string Name { get; set; }

        /// <summary>
        /// 	Gets or sets the description.
        /// </summary>
        /// <value> The description. </value>
        string Description { get; set; }

        /// <summary>
        /// 	Gets or sets the controls.
        /// </summary>
        /// <value> The controls. </value>
        IEnumerable<IControl> Controls { get; set; }
    }

    public interface IControl
    {
        /// <summary>
        /// 	Gets or sets the noderef.
        /// </summary>
        /// <value> The noderef. </value>
        string Noderef { get; set; }

        /// <summary>
        /// 	Gets or sets the DB ID.
        /// </summary>
        /// <value> The ID. </value>
        string ID { get; set; }

        /// <summary>
        /// 	Gets or sets the name.
        /// </summary>
        /// <value> The name. </value>
        string Name { get; set; }

        /// <summary>
        /// 	Gets or sets the description.
        /// </summary>
        /// <value> The description. </value>
        string Description { get; set; }

        /// <summary>
        /// 	Gets or sets the type of the element (e.g. Text Box, Radio Box, etc.)
        /// </summary>
        /// <value> The type of the element. </value>
        string ElementType { get; set; }

        /// <summary>
        /// 	Gets or sets the name of the parent screen.
        /// </summary>
        /// <value> The name of the parent screen. </value>
        string ParentScreenName { get; set; }

        /// <summary>
        /// 	Gets or sets the web map.
        /// </summary>
        /// <value> The web map. </value>
        IWebMap WebMap { get; set; }
    }

    public interface IScript
    {
        /// <summary>
        /// 	Gets or sets the noderef.
        /// </summary>
        /// <value> The noderef. </value>
        string Noderef { get; set; }

        /// <summary>
        /// 	Gets or sets the DB ID.
        /// </summary>
        /// <value> The ID. </value>
        string ID { get; set; }


        /// <summary>
        /// 	Gets or sets the name.
        /// </summary>
        /// <value> The name. </value>
        string Name { get; set; }

        /// <summary>
        /// 	Gets or sets the description.
        /// </summary>
        /// <value> The description. </value>
        string Description { get; set; }

        /// <summary>
        /// 	Gets or sets the parent application noderef.
        /// </summary>
        /// <value> The parent application noderef. </value>
        string ParentApplicationNoderef { get; set; }

        /// <summary>
        /// 	Gets or sets the script elements.
        /// </summary>
        /// <value> The script elements. </value>
        IEnumerable<IScriptElement> ScriptElements { get; set; }
    }

    public interface IScriptElement
    {
        /// <summary>
        /// 	Gets or sets the UID.
        /// </summary>
        /// <value> The noderef. </value>
        string ID { get; set; }

        /// <summary>
        /// 	Gets or sets the noderef.
        /// </summary>
        /// <value> The noderef. </value>
        string Noderef { get; set; }

        /// <summary>
        /// 	Gets or sets the name.
        /// </summary>
        /// <value> The name. </value>
        string Name { get; set; }

        /// <summary>
        /// 	Gets or sets the index.
        /// </summary>
        /// <value> The index. </value>
        int Index { get; set; }

        /// <summary>
        /// 	Gets or sets the keyword (CLICK, GET, SET, etc.)
        /// </summary>
        /// <value> The keyword. </value>
        string Keyword { get; set; }

        /// <summary>
        /// 	Gets or sets the control param.
        /// </summary>
        /// <value> The control param. </value>
        IControl ControlParam { get; set; }

        /// <summary>
        /// 	Gets or sets the additional params.
        /// </summary>
        /// <value> The additional params. </value>
        IEnumerable<IElementParam> Params { get; set; }
    }

    // todo - this is duplicated with one in Validata.TestEngine.Extensibility
    /// <summary>
    /// 	Application Testing parameter types
    /// </summary>
    public enum TestingParameterType
    {
        /// <summary>
        /// 	Value is a constant
        /// </summary>
        Constant = 1,

        /// <summary>
        /// 	Value is an Instance ID
        /// </summary>
        InstanceID = 2,

        /// <summary>
        /// 	Value is a Typical.Attribute string
        /// </summary>
        Attribute = 3,

        /// <summary>
        /// 	Value is a IF constraint
        /// </summary>
        Constraint = 4
    }

    public interface IElementParam
    {
        /// <summary>
        /// 	Gets or sets the noderef.
        /// </summary>
        /// <value> The noderef. </value>
        string Noderef { get; set; }

        /// <summary>
        /// 	Gets or sets the name.
        /// </summary>
        /// <value> The name. </value>
        string Name { get; set; }

        /// <summary>
        /// 	Gets or sets the index.
        /// </summary>
        /// <value> The index. </value>
        int Index { get; set; }

        /// <summary>
        /// 	Gets or sets the value.
        /// </summary>
        /// <value> The value. </value>
        string Value { get; set; }

        /// <summary>
        /// 	Gets or sets the type.
        /// </summary>
        /// <value> The type. </value>
        TestingParameterType Type { get; set; }
    }

    public interface IWebMap
    {
        /// <summary>
        /// 	Gets or sets the noderef.
        /// </summary>
        /// <value> The noderef. </value>
        string Noderef { get; set; }

        /// <summary>
        /// 	Gets or sets the DOM path.
        /// </summary>
        /// <value> The DOM path. </value>
        string DOMPath { get; set; }

        /// <summary>
        /// 	Gets or sets the HTML ID.
        /// </summary>
        /// <value> The HTML ID. </value>
        string HtmlID { get; set; }

        /// <summary>
        /// 	Gets or sets the name of the HTML.
        /// </summary>
        /// <value> The name of the HTML. </value>
        string HtmlName { get; set; }

        /// <summary>
        /// 	Gets or sets the HTML tag.
        /// </summary>
        /// <value> The HTML tag. </value>
        string HtmlTag { get; set; }

        /// <summary>
        /// 	Gets or sets the model ID.
        /// </summary>
        /// <value> The model ID. </value>
        string ModelID { get; set; }

        /// <summary>
        /// 	Gets or sets the page title.
        /// </summary>
        /// <value> The page title. </value>
        string PageTitle { get; set; }

        /// <summary>
        /// 	Gets or sets the page URL.
        /// </summary>
        /// <value> The page URL. </value>
        string PageURL { get; set; }
    }

    public static class IWebMapExtensions
    {
        public static bool IsIdenticalTo(this IControl control, IControl other)
        {
            return control.Name == other.Name
                   && control.ElementType == other.ElementType
                   && control.ParentScreenName == other.ParentScreenName
                   && control.WebMap != null
                   && other.WebMap != null
                   && control.WebMap.IsIdenticalTo(other.WebMap);
        }

        public static bool IsIdenticalTo(this IWebMap webMap, IWebMap other)
        {
            return webMap.DOMPath == other.DOMPath
                   && webMap.HtmlID == other.HtmlID
                   && webMap.HtmlName == other.HtmlName
                   && webMap.HtmlTag == other.HtmlTag
                   && webMap.PageTitle == other.PageTitle
                   && webMap.PageURL == other.PageURL;
        }
    }

    /// <summary>
    /// Extends IElementParam
    /// </summary>
    public static class ElementParamExtensions
    {
        public static IElementParam FindAtIndex(this List<IElementParam> ls, int paramIndex)
        {
            return ls.FirstOrDefault(n => n.Index == paramIndex);
        }

        public static string StringValueAtIndex(this List<IElementParam> ls, int paramIndex)
        {
            var prm = ls.FindAtIndex(paramIndex);
            return (prm != null ? prm.Value : "");
        }
    }
}