﻿using System;
using System.Collections.Generic;

namespace ValidataCommon.Interfaces
{
    public interface IConsolidationReportReader : IDisposable
    {
        /// <summary>
        /// Get next report item (null if end reached)
        /// </summary>
        /// <returns></returns>
        IConsolidationReportItem Next();
    }

    #region Enums

    /// <summary>
    /// Type of action applied to item during consolidation
    /// </summary>
    public enum ConsolidationReportItemActionType
    {
        /// <summary>
        /// Equal item (the imported item is same as one in DB)
        /// </summary>
        Equal,

        /// <summary>
        /// Item is different (imported item is different than one in the DB)
        /// </summary>
        Different,

        /// <summary>
        /// New item (the item exists only in the imported data and not in DB)
        /// </summary>
        New,

        /// <summary>
        /// DB only item (item exists only in database and don't exists in the imported data)
        /// </summary>
        DBOnly,
    }

    #endregion

    public interface IConsolidationReportItem
    {
        /// <summary>
        /// Item action type
        /// </summary>
        ConsolidationReportItemActionType ActionType { get; }

        /// <summary>
        /// Unique identifier of the compared record
        /// </summary>
        string TagUnique { get; }

        /// <summary>
        /// Retrieve differences (old/new) of all attributes which belong to the record
        /// </summary>
        IEnumerable<IAttributeDifference> AttributesTransitions { get; }
    }

    public interface IAttributeDifference
    {
        /// <summary>
        /// Name of the attribute
        /// </summary>
        string AttributeName { get; }

        /// <summary>
        /// Old value of the attribute
        /// </summary>
        string OldValue { get; }

        /// <summary>
        /// New value of the attribute
        /// </summary>
        string NewValue { get; }
    }
}
