﻿using System.Collections.Generic;

namespace ValidataCommon
{
    public interface IValidataRecord
    {
        /// <summary>
        /// Get the name of the catalog
        /// </summary>
        string CatalogName { get; set; }

        /// <summary>
        /// Get the name of the typical
        /// </summary>
        string TypicalName { get; set; }

        /// <summary>
        /// Gets list of attributes
        /// </summary>
        IEnumerable<IAttribute> Attributes { get; }

        /// <summary>
        /// Find attribute by name
        /// </summary>
        /// <param name="name">Attribute name</param>
        /// <returns>Attribute of null if not found</returns>
        IAttribute FindAttribute(string name);
    }
}
