using Validata.Common;

namespace ValidataCommon
{
    /// <summary>
    /// Interface describing a metadata attribute
    /// </summary>
    public interface IMetadataAttribute
    {
        /// <summary>
        /// Name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Minimum Value
        /// </summary>
        string MinValue { get; set; }

        /// <summary>
        /// Maximum Value
        /// </summary>
        string MaxValue { get; set; }

        /// <summary>
        /// Default Value
        /// </summary>
        string DefaultValue { get; set; }

        /// <summary>
        /// Data Length
        /// </summary>
        ulong DataLength { get; set; }

        /// <summary>
        /// Padding Character
        /// </summary>
        string PaddingChar { get; set; }

        /// <summary>
        /// Data Type
        /// </summary>
        AttributeDataType DataType { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        AttributeStatus Status { get; set; }

        /// <summary>
        /// Tolerance
        /// </summary>
        double Tolerance { get; set; }

        /// <summary>
        /// Attribute Prefix 
        /// </summary>
        string Prefix { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Comments
        /// </summary>
        string Remarks { get; set; }

        /// <summary>
        /// Flag if the field supports multiple values (to be expanded in MultipleValuesCount fields)
        /// </summary>
        bool IsMultiValue { get; set; }

        /// <summary>
        /// Count of multiple values fields
        /// </summary>
        ushort MultipleValuesCount { get; set; }

        /// <summary>
        /// Count of multi-multi values fields
        /// </summary>
        ushort SuperMultipleValuesCount { get; set; }

        /// <summary>
        /// Ignore the attribute when updating
        /// </summary>
        bool Ignore { get; set; }

        /// <summary>
        /// Group Id
        /// </summary>
        ulong GroupId { get; set; }

        /// <summary>
        /// Look up values list
        /// </summary>
        string[] LookupValues { get; set; }

        ulong DisciplineNoderef { get; set; }
        ulong UserNoderef { get; set; }
    }
}