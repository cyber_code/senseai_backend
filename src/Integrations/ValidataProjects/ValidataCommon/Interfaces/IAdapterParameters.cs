﻿using System.Collections.Generic;

namespace ValidataCommon.Interfaces
{
    public interface IAdapterParameters
    {
        Dictionary<string, string> Parameters { get; }
    }

    public static class IAdapterParametersExtensions
    {
        /// <summary>
        /// Get adapter parameter value
        /// </summary>
        /// <param name="adapterParameters"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetValue(this IAdapterParameters adapterParameters, string parameterName, string defaultValue = null)
        {
            return adapterParameters != null && adapterParameters.Parameters != null && adapterParameters.Parameters.ContainsKey(parameterName)
                ? adapterParameters.Parameters[parameterName]
                : defaultValue;
        }

        /// <summary>
        /// Get adapter parameter boolean value
        /// </summary>
        /// <param name="adapterParameters"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool GetBoolValue(this IAdapterParameters adapterParameters, string parameterName, bool defaultValue = false)
        {
            var str = adapterParameters.GetValue(parameterName, defaultValue.ToString());
            bool res;
            return bool.TryParse(str, out res) ? res : defaultValue;
        }
    }
}
