﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    public class DifferenceErrorConversionRule : DifferenceErrorRule
    {
        public String ConversionType { get; set; }
        public RuleElement Source { get; set; }
        public RuleElement Target { get; set; }
    }
}
