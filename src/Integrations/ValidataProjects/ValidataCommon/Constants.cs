using System;
using System.Collections.Generic;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Constants
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Association Type Parent Child
        /// </summary>
        public const string AT_PARENT_CHILD = "Parent / Child";
        
        /// <summary>
        /// The global instance group that 
        /// </summary>
        public const string OBJECTS_COPIED_TO_CYCLE = "CopiedToCycle";

        /// <summary>
        /// T24 main menu constant
        /// </summary>
        public const string T24_MAIN_MENU = "T24.MAIN.MENU";

        /// <summary>
        /// Project Resourcing Catalogue Name
        /// </summary>
        public const string prjResourceCatalogName = "ProjectResourcing";

        /// <summary>
        /// Default Association Name
        /// </summary>
        public const string defaultAssociationTypeName = "Default Association";

        /// <summary>
        /// Product Typical Name
        /// </summary>
        public const string typicalProduct = "Product";

        /// <summary>
        /// Subproject to product association type
        /// </summary>
        public const string assocSubprojectToProduct = "PROJECT_PRODUCT_COLLECTION";
    }
}
