﻿using System.Collections.Concurrent;
using System.Threading;

namespace ValidataCommon
{

    /// <summary>
    /// Class deling with named events
    /// </summary>
    public static class NamedEvent
    {
        public static ConcurrentBag<EventWaitHandle> WaitHandlers = new ConcurrentBag<EventWaitHandle>();

        /// <summary>
        /// raises event
        /// </summary>
        /// <param name="name">Event name</param>
        public static void RaiseEvent(string name)
        {
            EventWaitHandle waitHandle = GetGlobalEventHandle(name, false, EventResetMode.ManualReset);

            //To prevent object release before check for raised event from the other process
            WaitHandlers.Add(waitHandle);

            waitHandle.Set();
        }

        /// <summary>
        /// Resets event
        /// </summary>
        /// <param name="name">Event name</param>
        public static void ResetEvent(string name)
        {
            EventWaitHandle waitHandle = GetGlobalEventHandle(name, false, EventResetMode.ManualReset);
            waitHandle.Reset();
        }

        /// <summary>
        /// Close event handler
        /// </summary>
        /// <param name="name"></param>
        public static void CloseEventHandle(string name)
        {
            EventWaitHandle waitHandle = GetGlobalEventHandle(name, false, EventResetMode.ManualReset);
            waitHandle.Close();
        }

        /// <summary>
        /// Checks whether event is raised
        /// </summary>
        /// <param name="name">Event name</param>
        /// <returns>Rasied or not</returns>
        public static bool IsEventRaised(string name)
        {
            var waitHandler = OpenExistingGlobalEventWaitHandle(name);
            if (waitHandler != null)
                return waitHandler.WaitOne(0);

            return false;

            //EventWaitHandle waitHandle = GetGlobalEventHandle(name, false, EventResetMode.ManualReset);
            //return waitHandle.WaitOne(0);
        }

        /// <summary>
        /// Wait for event occurence
        /// </summary>
        /// <param name="eventName">Event Name</param>
        /// <param name="millisecondsTimeout">Timeout in milliseconds</param>
        /// <returns></returns>
        public static bool WaitOne(string eventName, int millisecondsTimeout)
        {
            EventWaitHandle waitHandle = GetGlobalEventHandle(eventName, false, EventResetMode.ManualReset);
            return waitHandle.WaitOne(millisecondsTimeout);
        }

        /// <summary>
        /// Open or create wait event
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="initialState">Initial State</param>
        /// <param name="mode">Reset Mode</param>
        /// <returns>Wait event handle</returns>
        private static EventWaitHandle GetGlobalEventHandle(string name, bool initialState, EventResetMode mode)
        {
            name = "Global\\" + name;

            return new EventWaitHandle(initialState, mode, name);
        }

        /// <summary>
        /// Build uniqe event name based on typical name and specific ID
        /// </summary>
        /// <param name="typicalName">Typical Name</param>
        /// <param name="specificId">Specific ID</param>
        /// <returns>Event name string</returns>
        public static string GetValidataSpecificEventName(string typicalName, string specificId)
        {
            return string.Format("ValEvent_{0}_{1}", typicalName, specificId);
        }

        /// <summary>
        /// Build uniqe event name
        /// </summary>
        /// <param name="specificId">Specific ID</param>
        /// <returns>Event name string</returns>
        public static string GetAdapterGatewayCycleLoadingCompleteEventName(string specificId)
        {
            return string.Format("ValEvent_AdapterGateway_CycleLoaded_{0}", specificId);
        }

        /// <summary>
        /// Build uniqe event name for T24 users signed on
        /// </summary>
        /// <param name="specificId">Specific ID</param>
        /// <returns>Event name string</returns>
        public static string GetAdapterGatewayUserSignOnCompleteEventName(string specificId)
        {
            return string.Format("ValEvent_AdapterGateway_UserSignedOn_{0}", specificId);
        }

        /// <summary>
        /// Build uniqe event name based
        /// </summary>
        /// <param name="batchJobId">Batch Job ID</param>
        /// /// <param name="cycleId">Cycle ID</param>
        /// <returns>Event name string</returns>
        public static string GetSynchronizedStartExecutionEventName(string batchJobId, string cycleId)
        {
            return string.Format("ValEvent_SyncStart_{0}_{1}", batchJobId, cycleId);
        }

        public static EventWaitHandle OpenExistingGlobalEventWaitHandle(string name)
        {
            name = "Global\\" + name;

            EventWaitHandle ewh;
            try
            {
                EventWaitHandle.TryOpenExisting(name, out ewh);
            }
            catch (WaitHandleCannotBeOpenedException)
            {
                return null;
            }

            return ewh;
        }

        public static bool CloseExistingGlobalEventWaitHandle(string name)
        {
            name = "Global\\" + name;

            try
            {
                var ewh = EventWaitHandle.OpenExisting(name);
                ewh.Close();
            }
            catch (WaitHandleCannotBeOpenedException)
            {
                return false;
            }

            return true;
        }
    }
}