﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ValidataCommon
{
    /// <span class="code-SummaryComment"><summary/></span>
    /// Provides a method for performing a deep copy of an object.
    /// Xml Serialization is used to perform the copy.
    /// <span class="code-SummaryComment"></span>
    public static class ObjectCopier
    {
        /// <span class="code-SummaryComment"><summary/></span>
        /// Perform a deep Copy of the object.
        /// <span class="code-SummaryComment"></span>
        /// <span class="code-SummaryComment"><typeparam name="T">The type of object being copied.</typeparam></span>
        /// <span class="code-SummaryComment"><param name="source">The object instance to copy.</param></span>
        /// <span class="code-SummaryComment"><returns>The copied object.</returns></span>
        public static T Clone<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            return DeserializedObject<T>(SerializeObject(source));
        }

        private static string SerializeObject<T>(T obj)
        {
            var serializer = new XmlSerializer(typeof(T));

            var sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, obj);
            }

            return sb.ToString();
        }

        private static T DeserializedObject<T>(string xml)
        {
            var serializer = new XmlSerializer(typeof(T));

            using (TextReader strReader = new StringReader(xml))
            {
                using (XmlReader reader = XmlReader.Create(strReader))
                {
                    return (T)serializer.Deserialize(reader);
                }
            }
        }
    }
}
