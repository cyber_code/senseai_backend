﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    public partial class StringMethods
    {
        public static string[] GetStatusesFromString(string statuses)
        {
            return GetStatusesListFromString(statuses).ToArray();
        }

        public static T? ReadEnumFromString<T>(string value) where T : struct
        {
            return ReadEnumFromString<T>(value, false);
        }

        public static string ResponseToStr(Queue<string> response, bool skipFirstLine, bool skipLastLine)
        {
            var responser = new StringBuilder();
            var responseIterator = response.GetEnumerator();

            responseIterator.MoveNext();
            for(int index = 0; index < response.Count; index++)
            {
                if ((index == 0) && skipFirstLine)
                {
                    responseIterator.MoveNext();
                    continue;
                }
                if ((index == response.Count - 1) && skipLastLine)
                    break;

                string line = responseIterator.Current;
                if ((index == response.Count - 1) || ((index == response.Count - 2) && skipLastLine))
                    responser.Append(line);
                else
                    responser.AppendLine(line);

                responseIterator.MoveNext();
            }

            return responser.ToString();
        }

        public static T? ReadEnumFromString<T>(string value, bool caseSensitive) where T : struct
        {
            try
            {
                var values = Enum.GetValues(typeof (T));
                if (!caseSensitive)
                    if (value != null)
                    {
                        value = value.ToUpper();
                    }

                foreach (T enumVal in values)
                {
                    string enumValue = caseSensitive ? enumVal.ToString() : enumVal.ToString().ToUpper();
                    if (enumValue == value)
                    {
                        return enumVal;
                    }
                }
            }
            catch(Exception ex)
            {
                // Log
            }

            return null;
        }

        public static List<string> GetStatusesListFromString(string statuses)
        {
            if (statuses == null)
            {
                statuses = string.Empty;
            }

            var result = new List<string>();

            foreach (string r in statuses.Split(new char[] { ';' }))
            {
                string rr = r.Trim();
                if (rr == string.Empty)
                {
                    continue;
                }

                if (result.Contains(rr))
                {
                    continue;
                }

                result.Add(rr);
            }

            return result;
        }

        public static string GetStringFromStatuses(string[] statuses)
        {
            string result = string.Empty;
            foreach (string s in statuses)
            {
                result += s;
                result += "; ";
            }

            if (result != string.Empty)
            {
                result = result.Remove(result.Length - 2);
            }

            return result;
        }

        public static int[] SplitNumberSequence(string automatedLoadIncrease, char[] delimiters)
        {
            string[] aliValues = new string[0];
            if (!string.IsNullOrEmpty(automatedLoadIncrease) && automatedLoadIncrease != "No Value")
            {
                aliValues =
                    automatedLoadIncrease.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }

            List<int> lsValues = new List<int>();
            foreach (string s in aliValues)
            {
                string tempVal = s.Trim();
                if (tempVal == string.Empty)
                {
                    continue;
                }
                int val;
                if (int.TryParse(tempVal, out val) && val > 0)
                {
                    lsValues.Add(val);
                }
                else
                {
                    return null;
                }
            }

            return lsValues.ToArray();
        }

        /// <summary>
        /// Escapes all possible conflicts in the pattern and then calls the Regex.Split
        /// </summary>
        /// <param name="input">the original input string to split</param>
        /// <param name="pattern">the original pattern in which all regex special symbols would be escaped</param>
        /// <returns>the array string split</returns>
        public static string[] RegexSplit(string input, string pattern)
        {
            string tmp = System.Text.RegularExpressions.Regex.Escape(pattern);
            return System.Text.RegularExpressions.Regex.Split(input, tmp);
        }

        /// <summary>
        /// Split using a multi char string as a separator
        /// </summary>
        /// <param name="input">the original input string to split</param>
        /// <param name="pattern">the separator string</param>
        /// <returns>the array string split</returns>
        public static string[] StringSplit(string input, string pattern)
        {
            return input.Split(new[] {pattern},StringSplitOptions.None);
        }
    }
}
