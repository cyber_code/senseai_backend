﻿using System;
using System.Drawing.Imaging;
using System.Reflection;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Configuration;

namespace ValidataCommon
{
    public class ScreenshotContainer
    {
        public ScreenshotContainer()
        {
            if (!int.TryParse(ConfigurationManager.AppSettings["MaxScreenshotsToKeep"], out MaxScreenshotsToKeep))
            {
                MaxScreenshotsToKeep = 40;
            }

            if (!int.TryParse(ConfigurationManager.AppSettings["ScreenshotsBeforeErrorToKeep"], out ScreenshotsBeforeErrorToKeep))
            {
                ScreenshotsBeforeErrorToKeep = 3;
            }
        }

        #region Fields

        public ScreenshotLevel ScreenshotLevel = ScreenshotLevel.Error;

        public ImageFormat ImageFormat = ImageFormat.Png;

        private readonly ScreenshotInfoList _ScreenshotInfoList = new ScreenshotInfoList();

        /// <summary>
        /// Defines the maximum number of Screenshots to keep in the Container.
        /// </summary>
        public int MaxScreenshotsToKeep = 50;

        /// <summary>
        /// Defines the number of Screenshots to keep before the one showing the occured error.
        /// </summary>
        public int ScreenshotsBeforeErrorToKeep = 3;

        private List<ScreenshotInfo> _ScreenshotsBeforeError = new List<ScreenshotInfo>();

        private object _Sync = new object();

        private string _LastActionName = String.Empty;

        #endregion

        #region Methods

        /// <summary>
        /// Keeps a Screenshot with regards to the defined ScreenshotLevel.
        /// </summary>
        /// <param name="actionName">Message containing description of the circumstances.</param>
        /// <param name="windowHandle">Handler of the window to capture.</param>
        /// <param name="errorMessage">Error message.</param>
        public void Capture(string actionName, int windowHandle, string errorMessage)
        {
            Capture(actionName, windowHandle, 0, 0, errorMessage, false);
        }

        /// <summary>
        /// Keeps a Screenshot with regards to the defined ScreenshotLevel.
        /// </summary>
        /// <param name="actionName">Message containing description of the circumstances.</param>
        /// <param name="windowHandle">Handle of the main window to capture.</param>
        /// <param name="childWindowHandle">Handle of the child window to capture.</param>
        /// <param name="alertWindowHandle">Handle of an alert window - alert/prompt/comfirm.</param>
        /// <param name="errorMessage">Error message.</param>
        /// <param name="isWebExecution">Defines whether the execution is web or desktop </param>
        public void Capture(string actionName, int windowHandle, int childWindowHandle, int alertWindowHandle, string errorMessage, bool isWebExecution = true)
        {
            Capture(actionName, windowHandle, childWindowHandle, alertWindowHandle, errorMessage, null, isWebExecution);
        }

        /// <summary>
        /// Keeps a Screenshot with regards to the defined ScreenshotLevel.
        /// </summary>
        /// <param name="actionName">Message containing description of the circumstances.</param>
        /// <param name="windowHandle">Handle of the main window to capture.</param>
        /// <param name="childWindowHandle">Handle of the child window to capture.</param>
        /// <param name="alertWindowHandle">Handle of an alert window - alert/prompt/comfirm.</param>
        /// <param name="errorMessage">Error message.</param>
        /// <param name="seleniumScreenshot">Screenshot from selenium driver</param>
        /// <param name="isWebExecution">Defines whether the execution is web or desktop </param>
        public void Capture(string actionName, int windowHandle, int childWindowHandle, int alertWindowHandle, string errorMessage, Image seleniumScreenshot, bool isWebExecution = true)
        {
            System.Diagnostics.Debug.Assert(windowHandle != 0 || seleniumScreenshot != null);

            lock (_Sync)
            {
                ScreenshotInfo screenShot;

                if (ScreenshotLevel == ScreenshotLevel.None)
                {
                    // don't create/add captures
                    return;
                }

                if (MaxScreenshotsToKeep > 0)
                {
                    while (_ScreenshotInfoList.Count >= MaxScreenshotsToKeep)
                    {
                        _ScreenshotInfoList.RemoveAt(0);
                    }
                }

                if ((actionName == "SetValue" /*|| actionName == "GetValue"*/) && String.IsNullOrEmpty(errorMessage) && _LastActionName == actionName)
                {
                    if (ScreenshotLevel == ScreenshotLevel.Error)
                    {
                        if (_ScreenshotsBeforeError.Count > 0)
                        {
                            screenShot = CreateScreenshotInfo(actionName, windowHandle, childWindowHandle, alertWindowHandle, errorMessage, seleniumScreenshot);
                            _ScreenshotsBeforeError[_ScreenshotsBeforeError.Count - 1] = screenShot;
                            return;
                        }
                    }
                    else
                    {
                        if (_ScreenshotInfoList.Count > 0)
                        {
                            screenShot = CreateScreenshotInfo(actionName, windowHandle, childWindowHandle, alertWindowHandle, errorMessage, seleniumScreenshot);
                            _ScreenshotInfoList[_ScreenshotInfoList.Count - 1] = screenShot;
                            return;
                        }
                    }
                }

                _LastActionName = actionName;

                screenShot = CreateScreenshotInfo(actionName, windowHandle, childWindowHandle, alertWindowHandle, errorMessage, seleniumScreenshot);

                if (ScreenshotLevel == ScreenshotLevel.Error)
                {
                    if (String.IsNullOrEmpty(errorMessage))
                    {
                        while (_ScreenshotsBeforeError.Count >= ScreenshotsBeforeErrorToKeep)
                        {
                            _ScreenshotsBeforeError.RemoveAt(0);
                        }

                        if (!isWebExecution)
                        {
                            DeleteLastByHandle(_ScreenshotsBeforeError, windowHandle);
                        }
                        _ScreenshotsBeforeError.Add(screenShot);
                    }
                    else
                    {
                        foreach (var screenshotBeforeError in _ScreenshotsBeforeError)
                        {
                            _ScreenshotInfoList.Add(screenshotBeforeError);
                        }

                        if (!isWebExecution)
                        {
                            DeleteLastByHandle(_ScreenshotInfoList, windowHandle);
                        }
                        _ScreenshotInfoList.Add(screenShot);
                    }
                }
                else
                {
                    if (!isWebExecution)
                    {
                        DeleteLastByHandle(_ScreenshotInfoList, windowHandle);
                    }
                    _ScreenshotInfoList.Add(screenShot);
                }
            }
        }

        private static bool DeleteLastByHandle(IList<ScreenshotInfo> screens, int windowHandle)
        {
            if (screens.Count != 0)
            {
                var lastInserted = screens[screens.Count - 1];
                if (lastInserted.WindowHandle == windowHandle)
                {
                    screens.Remove(lastInserted);
                    return true;
                }
            }
            return false;
        }

        public ScreenshotInfoList ToList(string imageFormat)
        {
            lock (_Sync)
            {
                var result = new ScreenshotInfoList();

                _ScreenshotInfoList.ForEach(n => result.Add(n.Clone(ScreenshotInfo.ImageFormatFromString(imageFormat))));

                return result;
            }
        }

        public void Clear()
        {
            lock (_Sync)
            {
                _ScreenshotInfoList.Clear();
            }
        }

        protected virtual ScreenshotInfo CreateScreenshotInfo(string actionName, int windowHandle, int childWindowHandle, int alertWindowHandle, string errorMessage, Image seleniumScreenshot)
        {
            var image = (seleniumScreenshot == null) ? WindowCapturer.WindowSnap(new IntPtr(windowHandle), new IntPtr(childWindowHandle), new IntPtr(alertWindowHandle)) : seleniumScreenshot;

            return new ScreenshotInfo
                {
                    WindowHandle = windowHandle,
                    DateTime = DateTime.Now,
                    ActionName = actionName,
                    ErrorMessage = errorMessage,
                    ImageFormat = ImageFormat,
                    Image = image
                };
        }

        #endregion
    }
}
