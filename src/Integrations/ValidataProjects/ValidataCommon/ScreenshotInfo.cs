﻿using System;
using System.Drawing.Imaging;
using System.Linq;
using System.Xml.Serialization;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Reflection;

namespace ValidataCommon
{
    /// <summary>
    /// Class containing screenshot image and related info
    /// </summary>
    [Serializable]
    public class ScreenshotInfo
    {
        ///<summary>
        /// Handle to the window
        ///</summary>
        public int WindowHandle { get; set; }

        /// <summary>
        /// Get/Set time taken
        /// </summary>
        public DateTime DateTime
        {
            get;
            set;
        }

        /// <summary>
        /// Get/Set image format string
        /// </summary>
        internal string ImageFormatString;

        internal ImageFormat ImageFormat
        {
            get { return ImageFormatFromString(ImageFormatString); }
            set { ImageFormatString = ImageFormatToString(value); }
        }

        /// <summary>
        /// Get/Set The image
        /// </summary>
        public Image Image
        {
            get { return ImageBytes == null ? null : ImageBytes.ToImage(); }
            set { ImageBytes = (value == null) ? null : value.ToByteArray(ImageFormat); }
        }

        /// <summary>
        /// Get/Set Image bytes
        /// </summary>
        public byte[] ImageBytes
        {
            get;
            set;
        }

        /// <summary>
        /// Get/Set Message containing description of the circumstances
        /// </summary>
        public string ActionName
        {
            get;
            set;
        }

        /// <summary>
        /// Get/Set Error message
        /// </summary>
        public string ErrorMessage
        {
            get;
            set;
        }

        /// <summary>
        /// Clone 
        /// </summary>
        /// <param name="imageFormat"></param>
        /// <returns></returns>
        internal ScreenshotInfo Clone(ImageFormat imageFormat)
        {
            return new ScreenshotInfo
            {
                DateTime = this.DateTime,
                ImageFormat = imageFormat,
                Image = this.Image,
                ActionName = this.ActionName,
                ErrorMessage = this.ErrorMessage
            };
        }

        internal static ImageFormat ImageFormatFromString(string imageFormatStr)
        {
            var property = typeof(ImageFormat).GetProperty(imageFormatStr, BindingFlags.Static | BindingFlags.Public);
            return (property == null)
                ? ImageFormat.Bmp // default
                : property.GetValue(null, null) as ImageFormat;
        }

        private static readonly Dictionary<Guid, string> _knownImageFormats =
            (from p in typeof(ImageFormat).GetProperties(BindingFlags.Static | BindingFlags.Public)
             where p.PropertyType == typeof(ImageFormat)
             let value = (ImageFormat)p.GetValue(null, null)
             select new {value.Guid, Name = value.ToString() })
            .ToDictionary(p => p.Guid, p => p.Name);

        internal static string ImageFormatToString(ImageFormat imageFormat)
        {
            string name;
            if (_knownImageFormats.TryGetValue(imageFormat.Guid, out name))
                return name;
            return null;
        }
    }

    internal static class ImageExtensions
    {
        internal static byte[] ToByteArray(this Image image, ImageFormat imageFormat)
        {
            MemoryStream memStream = new MemoryStream();
            image.Save(memStream, imageFormat);
            return memStream.ToArray();
        }

        internal static Image ToImage(this byte[] byteArray)
        {
            MemoryStream memStream = new MemoryStream(byteArray);
            return Image.FromStream(memStream);
        }
    }

    [Serializable]
    public class ScreenshotInfoList : List<ScreenshotInfo>
    {
    }
}
