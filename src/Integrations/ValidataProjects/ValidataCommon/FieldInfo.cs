using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace ValidataCommon
{
    /// <summary>
    /// Value of T24 Version LANGUAGE.FIELD
    /// </summary>
    public enum LanguageFieldValue
    {
        /// <summary>
        /// No association with language code
        /// </summary>
        N,

        /// <summary>
        /// Associated with a language code
        /// </summary>
        Y,

        /// <summary>
        /// Sub-valued language field
        /// </summary>
        S,

        /// <summary>
        /// Default value
        /// </summary>
        Default = N
    }

    /// <summary>
    /// Class providing access to field parameters for different languages
    /// </summary>
    [Serializable]
    public class FieldLanguagePack
    {
        /// <summary>
        /// Language code
        /// </summary>
        public string LanguageCode;

        /// <summary>
        /// Display text
        /// </summary>
        public string DisplayText;

        /// <summary>
        /// Constructs
        /// </summary>
        public FieldLanguagePack()
        {
        }

        /// <summary>
        /// Constructs
        /// </summary>
        /// <param name="languageCode">Language Code</param>
        /// <param name="displayText">Display Text</param>
        public FieldLanguagePack(string languageCode, string displayText)
        {
            LanguageCode = languageCode;
            DisplayText = displayText;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool IsEqualTo(FieldLanguagePack other)
        {
            return LanguageCode == other.LanguageCode && DisplayText == other.DisplayText;
        }
    }

    /// <summary>
    /// The metadata information for T24 field
    /// </summary>
    [Serializable]
    public class FieldInfo
    {
        #region Public Members

        /// <summary>
        /// Attribute Display Type
        /// "NODISPLAY", "COMBOBOX", "TOGGLE" and "VERTICAL.OPTIONS"
        /// </summary>
        public string DisplayType;

        /// <summary>
        /// The name of the field
        /// </summary>
        public string FieldName;

        /// <summary>
        /// T24 Version FIELD.NO field
        /// </summary>
        public string FieldNo;

        /// <summary>
        /// Determines whether in deployment this field should be used (or ignored)
        /// NOTE: It is 'NO.INPUT.FIELD' from T24 Version
        /// </summary>
        public bool IsSynchronizable;

        /// <summary>
        /// Defines the deployment conflict resolution behavior for fields which  are empty (missing) in SAS, 
        /// but have values in the target environment
        /// </summary>
        public EmptyValueDeploymentHandling EmptyValueHandling = EmptyValueDeploymentHandling.Default;

        /// <summary>
        /// Defines the deployment behavior for fields that have identical values in SAS Client and the target environment
        /// </summary>
        public UnchangedValueDeploymentHandling UnchangedValueHandling = UnchangedValueDeploymentHandling.Default;

        /// <summary>
        /// Determines whether this field must participate when creating a record
        /// </summary>
        public bool IsMandatory;

        /// <summary>
        /// Field defautl value (corresponds to 'AUT.NEW.CONTENT' mv for the given field)
        /// </summary>
        public string DefaultValue;

        /// <summary>
        /// Field old default value (corresponds to 'AUT.OLD.CONTENT' mv for the given field)
        /// NOTE: Currently it is not know whether this attribute/field is usable!!!
        /// </summary>
        public string DefaultValueOldContent;

        /// <summary>
        /// Is it multivalue or not
        /// </summary>
        public bool HasMultiValues;

        /// <summary>
        /// Name of the associated version from which the field is inherited (empty if non-inherited)
        ///  </summary>
        public string AssociatedVersion;

        /// <summary>
        /// Display Name of the field
        /// </summary>
        public string DisplayName;

        /// <summary>
        /// Indicates in which column the field defined by the User is to start.
        /// </summary>
        public string Column;

        /// <summary>
        /// Indicates the length for the Text (such as the field number, the field name etc...) 
        /// </summary>
        public string TextCharMax;

        /// <summary>
        /// Indicates to the User the length and start-position of the field being defined.
        /// Example: 1 63, where the position is 1 and length is 63.
        /// </summary>
        public string TableColumn;

        /// <summary>
        /// Indicates to the User in which line of the data section of the screen or report the field that he just defined will appear.
        /// </summary>
        public int TableLine;

        /// <summary>
        /// Expansion
        /// </summary>
        public string Expansion;

        /// <summary>
        /// Multi value index
        /// </summary>
        public int MVIndex = -1;

        /// <summary>
        /// Sub value index
        /// </summary>
        public int SVIndex = -1;

        /// <summary>
        /// SS ValProg field of an attribute
        /// </summary>
        public string ValProg;

        /// <summary>
        /// SS DisplayFmt field of an attribute
        /// </summary>
        public string DisplayFmt;

        /// <summary>
        /// SS SingleMult field of an attribute
        /// </summary>
        public string SingleMult;

        /// <summary>
        /// SS LangField field of an attribute
        /// </summary>
        public string LangField;

        /// <summary>
        /// SS RelFile field of an attribute
        /// </summary>
        public string RelFile;

        /// <summary>
        /// VERSION DROP.DOWN field of the attribute
        /// </summary>
        public string DropDown;

        /// <summary>
        /// Group ID
        /// </summary>
        public int GroupID = -1;

        /// <summary>
        /// Sub group ID
        /// </summary>
        public int SubGroupID = -1;

        /// <summary>
        /// OBJECT.MAP.ENQ enquiry N.ARRAY field (With removed mandatory mark if it is from Associated version)
        /// </summary>
        public string NArray;

        /// <summary>
        /// OBJECT.MAP.ENQ enquiry N.ARRAY field (Original)
        /// </summary>
        public string NArrayOriginal;

        /// <summary>
        /// Field info for other languages
        /// </summary>
        public List<FieldLanguagePack> OtherLanguages = new List<FieldLanguagePack>();

        /// <summary>
        /// Corresponding local table record
        /// </summary>
        public LocalTableRecord LocalTableInfo = new LocalTableRecord();

        /// <summary>
        /// Is expansion NO
        /// </summary>
        public bool IsExpansionNo
        {
            get { return Expansion != null && string.Compare(Expansion, "NO", true) == 0; }
        }

        #endregion

        #region Private Members

        /// <summary>
        /// ValProg Wrapper
        /// </summary>
        [NonSerialized]
        private ValProg _ValProgWrapper = null;

        /// <summary>
        /// NArray Wrapper
        /// </summary>
        [NonSerialized]
        private NArrayWrapper _NArrayWrapper = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Get whether the attribute has fixed look up list
        /// Obtained from XXX.VAL.PROG field
        /// </summary>
        public bool IsFixedLookUpList
        {
            get
            {
                InitializeValProgWrapper();

                return _ValProgWrapper.HasLookupValues;
            }
        }

        /// <summary>
        /// Get lookup values of fixed lookup list
        /// </summary>
        public IEnumerable<string> FixedLookUpListValues
        {
            get
            {
                InitializeValProgWrapper();

                System.Diagnostics.Debug.Assert(IsFixedLookUpList);
                return _ValProgWrapper.LookupValues;
            }
        }

        /// <summary>
        /// Get lookup values of fixed lookup list
        /// </summary>
        public IEnumerable<string> FixedLookUpListTexts
        {
            get
            {
                InitializeValProgWrapper();

                //System.Diagnostics.Debug.Assert(IsFixedLookUpList);
                return _ValProgWrapper.LookupTexts;
            }
        }

        /// <summary>
        /// Get whether the attribute has dynamic look up list
        /// Obtained from (SS)XXX.REL.FILE field or (VERSION)DROP.DOWN
        /// NOTE: The look up values are presented by all records in table with name equal to 'RelFile'|'DropDown' property
        /// </summary>
        public bool IsDynamicLookUpList
        {
            get { return !string.IsNullOrEmpty(this.RelFile) || !string.IsNullOrEmpty(this.DropDown); }
        }

        /*
        /// <summary>
        /// Get T24 table name for dynamic lookup fields
        /// </summary>
        public string DynamicLookupTableName
        {
            get { return this.RelFile; }
        }*/

        /// <summary>
        /// Field behavious (Input/No Input/No Change...)
        /// </summary>
        public FieldBehavior Behavior
        {
            get
            {
                InitializeValProgWrapper();

                if (!this.IsSynchronizable)
                {
                    // if field is not syncronizable, thats means that
                    //  it is no input field defined in the version
                    return FieldBehavior.NOINPUT;
                }

                // take behavior from SS
                return _ValProgWrapper.Behavior;
            }
        }

        /// <summary>
        /// Language field value
        /// </summary>
        public LanguageFieldValue LanguageFieldValue
        {
            get
            {
                switch (this.LangField)
                {
                    case "Y":
                        return LanguageFieldValue.Y;
                    case "S":
                        return LanguageFieldValue.S;
                    case "N":
                        return LanguageFieldValue.N;
                    default:
                        return LanguageFieldValue.Default;
                }
            }
        }

        /// <summary>
        /// Validation routine name
        /// </summary>
        public string ValidationRoutine
        {
            get
            {
                InitializeValProgWrapper();
                return _ValProgWrapper.ValidationRoutine;
            }
        }

        /// <summary>
        /// Min length of the field ('-1' - default e.g. not defined)
        /// </summary>
        public int FieldLengthMin
        {
            get
            {
                InitializeNArrayWrapper();

                return _NArrayWrapper.Min;
            }
        }

        /// <summary>
        /// Max length of the field ('-1' - default e.g. not defined)
        /// </summary>
        public int FieldLengthMax
        {
            get
            {
                InitializeNArrayWrapper();

                return _NArrayWrapper.Max;
            }
        }

        /// <summary>
        /// Determine if TextBox is multiline
        /// </summary>
        public bool IsMultiline
        {
            get
            {
                InitializeValProgWrapper();

                return _ValProgWrapper.IsMultiline;
            }
        }

        /// <summary>
        /// Defines if the field require input (it is mandatory field)
        /// </summary>
        public bool RequiresInput
        {
            get
            {
                return IsMandatory || FieldLengthMin > 0;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Deep clone
        /// </summary>
        /// <returns></returns>
        public FieldInfo Clone()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, this);
                stream.Seek(0, SeekOrigin.Begin);
                return (FieldInfo)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return FieldName +
                   (IsSynchronizable ? " [S]" : "") +
                   (HasMultiValues ? " [M]" : "") +
                   (IsMandatory ? " [R]" : "");
        }

        /// <summary>
        /// Gets the clean display name (e.g. instead of 'Customer : ', return 'Customer')
        /// </summary>
        /// <value>The clean display name of the field.</value>
        public string CleanDisplayName
        {
            get
            {
                string displayName = (DisplayName ?? "").Trim();
                if (displayName.EndsWith(":"))
                {
                    displayName = displayName.Substring(0, displayName.Length - 1);
                    displayName = displayName.Trim();
                }

                return displayName;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has meaningful display name.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has meaningful display name; otherwise, <c>false</c>.
        /// </value>
        public bool HasMeaningfulDisplayName
        {
            get { return CleanDisplayName.Length > 0; }
        }

        /// <summary>
        /// Get version field display text for language code
        /// </summary>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        public string GetDisplayTextForLanguageCode(string languageCode)
        {
            foreach (var langPack in this.OtherLanguages)
            {
                if (langPack.LanguageCode == languageCode)
                {
                    return langPack.DisplayText;
                }
            }

            // If not found ==> Return default
            return this.DisplayName;
        }

        /// <summary>
        /// Check for equality of this and other field infos
        /// </summary>
        /// <param name="other">Other field info</param>
        /// <returns>Returns true if this and other field info are equal, otherwise result is false</returns>
        public bool IsEqualTo(FieldInfo other)
        {
            bool otherLanguagesAreEqual = OtherLanguages.Count == other.OtherLanguages.Count;
            if (otherLanguagesAreEqual)
            {
                for (int i = 0; i < OtherLanguages.Count; i++)
                {
                    if (!OtherLanguages[i].IsEqualTo(other.OtherLanguages[i]))
                    {
                        otherLanguagesAreEqual = false;
                        break;
                    }
                }
            }

            return DisplayType == other.DisplayType
                && FieldName == other.FieldName
                && FieldNo == other.FieldNo
                && IsSynchronizable == other.IsSynchronizable
                && IsMandatory == other.IsMandatory
                && DefaultValue == other.DefaultValue
                && DefaultValueOldContent == other.DefaultValueOldContent
                && HasMultiValues == other.HasMultiValues
                && AssociatedVersion == other.AssociatedVersion
                && DisplayName == other.DisplayName
                && Column == other.Column
                && TextCharMax == other.TextCharMax
                && TableColumn == other.TableColumn
                && TableLine == other.TableLine
                && Expansion == other.Expansion
                && MVIndex == other.MVIndex
                && SVIndex == other.SVIndex
                && ValProg == other.ValProg
                && DisplayFmt == other.DisplayFmt
                && SingleMult == other.SingleMult
                && LangField == other.LangField
                && RelFile == other.RelFile
                && GroupID == other.GroupID
                && SubGroupID == other.SubGroupID
                && NArray == other.NArray
                && NArrayOriginal == other.NArrayOriginal
                && otherLanguagesAreEqual
                && LocalTableInfo.IsEqualTo(other.LocalTableInfo);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Parses once 'ValProg' property in order to construct '_ValProgWrapper' member
        /// </summary>
        private void InitializeValProgWrapper()
        {
            if (_ValProgWrapper != null)
            {
                // already parsed
                return;
            }

            _ValProgWrapper = new ValProg(this.ValProg);
        }

        /// <summary>
        /// Parses once 'NArray' property in order to construct '_NArrayWrapper' member
        /// </summary>
        private void InitializeNArrayWrapper()
        {
            if (_NArrayWrapper != null)
            {
                // already parsed
                return;
            }

            _NArrayWrapper = new NArrayWrapper(this.NArray);
        }

        #endregion
    }
}