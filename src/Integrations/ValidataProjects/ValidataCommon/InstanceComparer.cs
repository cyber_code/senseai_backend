﻿//#define IGNORE_FIRST
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ValidataCommon
{
    public delegate InstanceAttributesComparisonResult CompareExecutor(
        IEnumerable<IAttribute> first, IEnumerable<IAttribute> second, RuleElement sourceElement,
        RuleElement targetElement);


    public class InstanceAttributesComparerException : Exception
    {
        public InstanceAttributesComparerException () : base()
        {
        }

        public InstanceAttributesComparerException(String error)
            :base(error)
        {
            
        }

        public InstanceAttributesComparerException(String error, Exception internalException)
            : base(error, internalException)
        {

        }

    }

    /// <summary>
    /// Compares attributes of two instances
    /// </summary>
    public class InstanceAttributesComparer
    {
        public static InstanceAttributesComparer Instance
        {
            get
            {
                if (_instanceAttributesComparer == null)
                    _instanceAttributesComparer = new InstanceAttributesComparer();
                return _instanceAttributesComparer;
            }
        }

        private static InstanceAttributesComparer _instanceAttributesComparer;
        public static CompareExecutor CompareFunction;
        public static InstanceAttributesComparisonResult Compare(IValidataRecord first, IValidataRecord second)
        {
            if (CompareFunction == null)
            {
                try
                {
                    CompareFunction = Instance.Compare;
                }
                catch (InstanceAttributesComparerException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new InstanceAttributesComparerException("Configuration error detected, check DifferenceError.Config", ex);
                }

            }
            RuleElement source = new RuleElement(first.CatalogName, first.TypicalName, null);
            RuleElement target = new RuleElement(second.CatalogName, second.TypicalName, null);
            return CompareFunction(first.Attributes, second.Attributes, source, target);
        }

        private DifferenceErrorConfiguration _differenceErrorConfiguration;

        public InstanceAttributesComparer()
        {
            try
            {
                _differenceErrorConfiguration = DifferenceErrorConfiguration.DeserializeFromXml();
            }
            catch (Exception ex)
            {
                
                throw new InstanceAttributesComparerException("Required configuration file \"DifferenceError.Config\" is missing.", ex);
            }
            
        }


        public InstanceAttributesComparer(string filePath)
        {
            try
            {
                _differenceErrorConfiguration = DifferenceErrorConfiguration.DeserializeFromXml(filePath);
                CompareFunction = this.Compare;
            }
            catch (Exception ex)
            {

                throw new InstanceAttributesComparerException("Required configuration file \"DifferenceError.Config\" is missing.", ex);
            }

        }

        public void GetSeparators(String catalogName, String typicalName, String attributeName, out String multiValueSeparator, out String subValueSeparator)
        {
            RuleElement source = new RuleElement(catalogName, typicalName, attributeName);
            multiValueSeparator = String.Empty;
            subValueSeparator = String.Empty;

            foreach (MultiValueSplitElement element in _differenceErrorConfiguration.MultiValueRuleList)
            {
                if (element.GetSeparatorElement(source, ref multiValueSeparator, ref subValueSeparator))
                {
                    return;
                }

            }
        }

        public string GetMultiValueSeparators(String catalogName, String typicalName, String attributeName)
        {
            RuleElement source = new RuleElement(catalogName, typicalName, attributeName);

            foreach (MultiValueSplitElement element in _differenceErrorConfiguration.MultiValueRuleList)
            {
                String multiValueSeparator = String.Empty;
                String subValueSeparator = String.Empty;
                if (element.GetSeparatorElement(source, ref multiValueSeparator, ref subValueSeparator))
                {
                    return multiValueSeparator;
                }

            }
            return String.Empty;
        }

        public string GetSubValueSeparators(String catalogName, String typicalName, String attributeName)
        {
            RuleElement source = new RuleElement(catalogName, typicalName, attributeName);

            foreach (MultiValueSplitElement element in _differenceErrorConfiguration.MultiValueRuleList)
            {
                String multiValueSeparator = String.Empty;
                String subValueSeparator = String.Empty;
                if (element.GetSeparatorElement(source, ref multiValueSeparator, ref subValueSeparator))
                {
                    return subValueSeparator;
                }

            }
            return String.Empty;
        }

        private static InstanceComparerInfo _info;

        public static int CompareAttributeName(IAttribute first, IAttribute second)
        {
            return string.Compare(first.Name, second.Name);
        }

        public InstanceAttributesComparisonResult Compare(
            IEnumerable<IAttribute> first, IEnumerable<IAttribute> second, RuleElement sourceElement, RuleElement targetElement)
        {
            InstanceAttributesComparisonResult result = new InstanceAttributesComparisonResult();

            _info = InstanceComparerHelper.GetInfo(sourceElement.CatalogName, sourceElement.TypicalName);

            List<IAttribute> firstSorted = new List<IAttribute>(first);
            firstSorted.Sort(CompareAttributeName);
            List<IAttribute> secondSorted = new List<IAttribute>(second);
            secondSorted.Sort(CompareAttributeName);

            int posInFirst = 0;
            int posInSecond = 0;

            while (posInFirst < firstSorted.Count && posInSecond < secondSorted.Count)
            {
                IAttribute firstAttribute = firstSorted[posInFirst];
                IAttribute secondAttribute = secondSorted[posInSecond];

                switch (string.Compare(firstAttribute.Name, secondAttribute.Name))
                {
                    case 0:
                        String errorMessage = String.Empty;
                        if (CompareValues(firstAttribute, secondAttribute, sourceElement, targetElement,
                                          out errorMessage))
                        {
                            result.AddIdentical(firstAttribute.Name, firstAttribute.Value);
                        }
                        else
                        {
                            result.AddDifferent(firstAttribute.Name, firstAttribute.Value, secondAttribute.Value,
                                                errorMessage);
                        }
                        posInFirst++;
                        posInSecond++;
                        break;
                    case -1:
                        // Check if there is a multivalue with separator
                        if (_info != null)
                        {
                            if (_info.AttributeNamesToShortNames.ContainsKey(firstAttribute.Name))
                            {
                                String attributeName = _info.AttributeNamesToShortNames[firstAttribute.Name];
                                string separatorMv;
                                _info.MultiValueSeparators.TryGetValue(attributeName, out separatorMv);
                                if (!string.IsNullOrEmpty(separatorMv))
                                {
                                    string separatorSv;
                                    _info.SubValueSeparators.TryGetValue(attributeName, out separatorSv);
                                    string sourceValue = RemoveUnnecessarySeparators(firstAttribute.Value, separatorMv,
                                                                                     separatorSv);
                                    if (string.IsNullOrEmpty(sourceValue))
                                    {
                                        result.AddIdentical(firstAttribute.Name, firstAttribute.Value);
                                        posInFirst++;
                                        break;
                                    }

                                }
                            }
                        }
                        result.AddOnlyInFirst(firstAttribute.Name, firstAttribute.Value);
                        posInFirst++;
                        break;
                    case 1:
                        result.AddOnlyInSecond(secondAttribute.Name, secondAttribute.Value);
                        posInSecond++;
                        break;
                }
            }

            // AddRemainingUnmatchedAttributes for first instance
            for (int i = posInFirst; i < firstSorted.Count; i++)
            {
                result.AddOnlyInFirst(firstSorted[i].Name, firstSorted[i].Value);
            }

            // AddRemainingUnmatchedAttributes for second instance
            for (int i = posInSecond; i < secondSorted.Count; i++)
            {
                result.AddOnlyInSecond(secondSorted[i].Name, secondSorted[i].Value);
            }

            return result;
        }


        public static bool CompareCompressedMultiValue(String fieldName, String sourceValue, String targetValue, String delimiterMultiValue, String delimiterSubValue, out String errorMessage)
        {
            errorMessage = String.Empty;
            
            if (String.Compare(sourceValue, targetValue, StringComparison.OrdinalIgnoreCase) == 0) return true;
            sourceValue = RemoveUnnecessarySeparators(sourceValue, delimiterMultiValue, delimiterSubValue);
            targetValue = RemoveUnnecessarySeparators(targetValue, delimiterMultiValue, delimiterSubValue);
            if (String.Compare(sourceValue, targetValue, StringComparison.OrdinalIgnoreCase) == 0) return true;

            StringBuilder error = new StringBuilder();

            //string[] mValues = valuesToAlign[i].OutputValue.Split(new string[] { mutliValueSeparator }, StringSplitOptions.None);
            String[] sourceAttributes = ValidataCommon.StringMethods.StringSplit(sourceValue, delimiterMultiValue);
            String[] targetAttributes = ValidataCommon.StringMethods.StringSplit(targetValue, delimiterMultiValue);

            for (int indexM = 0; indexM < sourceAttributes.Length; indexM++)
            {
                if (string.IsNullOrEmpty(sourceAttributes[indexM]) || !sourceAttributes[indexM].Contains(delimiterSubValue))
                {
                    if (targetAttributes.Length <= indexM)
                    {
                        error.AppendLine(String.Format("{0} is missing in the target system.", GetCompleteFieldName(fieldName, indexM, 1)));
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(sourceAttributes[indexM]) && String.Compare(sourceAttributes[indexM], targetAttributes[indexM]) != 0)
                        {
                            error.AppendLine(String.Format("Attribute {0} with value '{1}' is different from '{2}'.", GetCompleteFieldName(fieldName, indexM, 0), sourceAttributes[indexM], targetAttributes[indexM]));
                        }
                    }
                }
                else
                {
                    if (targetAttributes.Length <= indexM)
                    {
                        error.AppendLine(String.Format("{0} is missing in the target system.", GetCompleteFieldName(fieldName, indexM, 1)));
                    }
                    else if (string.IsNullOrEmpty(targetAttributes[indexM]))
                    {
                        error.AppendLine(String.Format("Attribute {0} with value '{1}' is different from '{2}'.", GetCompleteFieldName(fieldName, indexM, 0), sourceAttributes[indexM], targetAttributes[indexM]));
                    }
                    else
                    {

                        String[] sourceSv = ValidataCommon.StringMethods.StringSplit(sourceAttributes[indexM], delimiterSubValue);
                        String[] targetSv;
                        if (targetAttributes[indexM].Contains(delimiterSubValue))
                        {
                            targetSv = ValidataCommon.StringMethods.StringSplit(targetAttributes[indexM], delimiterSubValue);
                        }
                        else
                        {
                            targetSv = new string[] { targetAttributes[indexM] };
                        }

                        for (int indexS = 0; indexS < sourceSv.Length; indexS++)
                        {
                            if (targetSv.Length <= indexS)
                            {
                                error.AppendLine(String.Format("{0} is missing in the target system.",
                                                               GetCompleteFieldName(fieldName, indexM, indexS)));
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(sourceSv[indexS]) &&
                                    String.Compare(sourceSv[indexS], targetSv[indexS]) != 0)
                                {
                                    error.AppendLine(
                                        String.Format("Attribute {0} with value '{1}' is different from '{2}'.",
                                                      GetCompleteFieldName(fieldName, indexM, indexS), sourceSv[indexS],
                                                      targetSv[indexS]));
                                }
                            }
                        }
                    }
                }
            }

            errorMessage = error.ToString();
            return error.Length == 0;
        }

        private static String GetCompleteFieldName(String fieldName, int multiValueIndex, int subValueIndex)
        {
            if(String.IsNullOrEmpty(fieldName)) return String.Empty;

            return String.Format("{0}-{1}~{2}", fieldName, multiValueIndex + 1, subValueIndex + 1);
        }

        private bool CompareValues(IAttribute firstAttribute, IAttribute secondAttribute, RuleElement sourceElement, RuleElement targetElement,out String errorMessage)
        {
            bool bEqual = false;
            bool bIgnore = false;
            errorMessage = String.Empty;

            sourceElement.FieldName = firstAttribute.Name;
            targetElement.FieldName = secondAttribute.Name;


#if IGNORE_FIRST
            foreach (var differenceErrorIgnoreRule in _differenceErrorConfiguration.DifferenceErrorIgnoreRuleList)
            {
                if (differenceErrorIgnoreRule.Source.CheckValueElement(sourceElement, firstAttribute.Value) && 
                    differenceErrorIgnoreRule.Target.CheckValueElement(targetElement, secondAttribute.Value))
                {
                    // Means the attributes match ignore rule, so we do not check, just assume equality
                    bIgnore = true;
                    bEqual = true;
                    break;
                }
            }
            if (bIgnore == false)
            {
                foreach (
                    var differenceErrorConversionRule in _differenceErrorConfiguration.DifferenceErrorConversionRuleList
                    )
                {
                    if (differenceErrorConversionRule.Source.CheckRuleElement(sourceElement) &&
                        differenceErrorConversionRule.Target.CheckRuleElement(targetElement))
                    {
                        // means attribute match the Conversion Rule, so execute function to compare decimals
                        bEqual = CompareDecimals(firstAttribute.Value, secondAttribute.Value);
                        if (bEqual)
                        {
                            bIgnore = true;
                            break;
                        }

                    }

                }
                if (bIgnore == false)
                {
                    // Means no ignore rule was applied, so need to compare values
                    if (firstAttribute.Value.Equals(secondAttribute.Value))
                        bEqual = true;
                }
            }

#else // COMPARE FIRST
            if (firstAttribute.Value.Equals(secondAttribute.Value))
            {
                bIgnore = true;
                bEqual = true;
            }
            else 
            {
                String separatorMV = String.Empty;
                String separatorSV = String.Empty;

                if (_info != null)
                {
                    String attributeName = _info.AttributeNamesToShortNames[firstAttribute.Name];

                    _info.SubValueSeparators.TryGetValue(attributeName, out separatorSV);
                    _info.MultiValueSeparators.TryGetValue(attributeName, out separatorMV);

                    //  GetDelimiters(sourceElement.CatalogName,sourceElement.TypicalName, ref attributeName, ref separatorMV,ref separatorSV);

                    if (!string.IsNullOrEmpty(separatorMV) || !string.IsNullOrEmpty(separatorSV))
                    {
                        bEqual = CompareCompressedMultiValue(attributeName, firstAttribute.Value, secondAttribute.Value,
                                                             separatorMV,
                                                             separatorSV, out errorMessage);
                        bIgnore = bEqual;
                    }
                }
            }

            if (bIgnore == false)
            {

                foreach (var differenceErrorIgnoreRule in _differenceErrorConfiguration.DifferenceErrorIgnoreRuleList)
                {
                    if (differenceErrorIgnoreRule.Source.CheckValueElement(sourceElement, firstAttribute.Value) &&
                        differenceErrorIgnoreRule.Target.CheckValueElement(targetElement, secondAttribute.Value))
                    {
                        // Means the attributes match ignore rule, so we do not check, just assume equality
                        bIgnore = true;
                        bEqual = true;
                        break;
                    }
                }

                if (bIgnore == false)
                {
                    foreach (
                        var differenceErrorConversionRule in
                            _differenceErrorConfiguration.DifferenceErrorConversionRuleList
                        )
                    {
                        if (differenceErrorConversionRule.Source.CheckRuleElement(sourceElement) &&
                            differenceErrorConversionRule.Target.CheckRuleElement(targetElement))
                        {
                            // means attribute match the Conversion Rule, so execute function to compare decimals
                            bEqual = CompareDecimals(firstAttribute.Value, secondAttribute.Value);
                            if (bEqual)
                            {
                                bIgnore = true;
                                break;
                            }

                        }

                    }
                }
            }
#endif

            return bEqual;
        }

        private bool CompareDecimals(string p, string p2)
        {
            double d1, d2;
            if (!double.TryParse(p, out d1))
                return false;
            if (!double.TryParse(p2, out d2))
                return false;
            return (d1 == d2);
        }

        public void GetDelimiters(string catalogName, string typicalName, ref string attributeName, ref string subSeparator, ref string multiSeparator)
        {
            Instance.GetSeparators(catalogName, typicalName, attributeName, out multiSeparator, out subSeparator);
            if (AttributeNameParser.IsMultiValue(attributeName))
            {
                attributeName = AttributeNameParser.GetShortFieldName(attributeName);
                if (string.IsNullOrEmpty(multiSeparator) && string.IsNullOrEmpty(subSeparator))
                {
                    Instance.GetSeparators(catalogName, typicalName, attributeName, out multiSeparator, out subSeparator);
                }
            }
        }

        #region RemoveUnnecessarySeparators
        private static string RemoveUnnecessarySeparators(string value, string multiValueSeparator, string subValueSeparator)
        {
            string[] multiValues = value.Split(new string[] { multiValueSeparator }, StringSplitOptions.None);
            for (int i = 0; i < multiValues.Length; i++)
            {
                multiValues[i] = RemoveUnnecessarySubvalueSeparators(multiValues[i], subValueSeparator);
            }

            int lastMeaningfulIndex = GetIndexOfLastNonEmptyValue(multiValues);
            if (lastMeaningfulIndex < 0)
            {
                return "";
            }
            bool hasValue = false;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= lastMeaningfulIndex; i++)
            {
                if (i > 0)
                {
                    sb.Append(multiValueSeparator);
                }
                sb.Append(multiValues[i]);
                if (!multiValues[i].IsEmptyOrNoValue())
                    hasValue = true;
            }
            return !hasValue ? "" : sb.ToString();
        }

        private static string RemoveUnnecessarySubvalueSeparators(string value, string separator)
        {
            int numberOfEmptyCharsAtEnd = FindNumberOfStringsAtEnd(value, separator);
            if (numberOfEmptyCharsAtEnd == 0)
            {
                return value;
            }
            else if (numberOfEmptyCharsAtEnd != value.Length)
            {
                return value.Substring(0, value.Length - numberOfEmptyCharsAtEnd);
            }
            else
            {
                return "";
            }
        }

        private static int FindNumberOfStringsAtEnd(string text, string pattern)
        {
            if (String.IsNullOrEmpty(text))
                return 0;

            int count = 0;
            int i;
            while ((i = text.LastIndexOf(pattern, System.StringComparison.Ordinal)) == text.Length - pattern.Length && i > -1)
            {
                count++;
                text = text.Substring(0, text.Length - pattern.Length);
            }

            return count * pattern.Length;
        }

        private static int GetIndexOfLastNonEmptyValue(string[] subValues)
        {
            for (int i = subValues.Length - 1; i >= 0; i--)
            {
                if (subValues[i].Length > 0)
                {
                    return i;
                }
            }

            return -1;
        }
        #endregion
    }

    /// <summary>
    /// Results of comparison of attributes of two instances
    /// </summary>
    public class InstanceAttributesComparisonResult
    {
        #region Nested classes

        public struct DistinctValues
        {
            public string FirstValue;
            public string SecondValue;
            public String ErrorMessage;

            public DistinctValues(string firstValue, string secondValue, string errorMessage = "")
            {
                FirstValue = firstValue;
                SecondValue = secondValue;
                ErrorMessage = errorMessage;
            }


        }

        #endregion

        #region Members

        public Dictionary<string, string> Identical = new Dictionary<string, string>();
        public Dictionary<string, DistinctValues> DifferentValues = new Dictionary<string, DistinctValues>();
        public Dictionary<string, string> OnlyInFirst = new Dictionary<string, string>();
        public Dictionary<string, string> OnlyInSecond = new Dictionary<string, string>();

        #endregion

        #region Adding items

        public void AddDifferent(string attributeName, string firstValue, string secondValue, String errorMessage = "")
        {
            DifferentValues.Add(attributeName, new DistinctValues(firstValue, secondValue, errorMessage));
        }

        public void AddIdentical(string attributeName, string attributeValue)
        {
            Identical.Add(attributeName, attributeValue);
        }

        public void AddOnlyInFirst(string attributeName, string attributeValue)
        {
            if (attributeValue != "")
            {
                OnlyInFirst.Add(attributeName, attributeValue);
            }
        }

        public void AddOnlyInSecond(string attributeName, string attributeValue)
        {
            if (attributeValue != "")
            {
                OnlyInSecond.Add(attributeName, attributeValue);
            }
        }

        #endregion

        #region Getting result as detailed description

        public string GetDifferencesText()
        {
            if (DifferentValues.Count == 0)
            {
                return "";
            }

            StringBuilder builder = new StringBuilder();

            builder.AppendFormat("{0} different attribute values:", DifferentValues.Count);
            builder.AppendLine();

            foreach (KeyValuePair<string, DistinctValues> diff in DifferentValues)
            {
                builder.AppendFormat("[{0}] : [First Instance] {1} [Second Instance] {2}",
                                     diff.Key, diff.Value.FirstValue, diff.Value.SecondValue);
                builder.AppendLine();
            }

            return builder.ToString();
        }

        public string GetAllDissimilaritiesText()
        {
            if (IsCompletetelyIdentical)
            {
                return "";
            }
            else
            {
                return GetDifferencesText() + GetUniqueForBothText();
            }
        }

        public string GetUniqueForBothText()
        {
            return GetOnlyInFirstText() + GetOnlyInSecondText();
        }

        public string GetOnlyInFirstText()
        {
            return GetOnlyInOneOfTheTwoText(OnlyInFirst, true);
        }

        public string GetOnlyInSecondText()
        {
            return GetOnlyInOneOfTheTwoText(OnlyInSecond, false);
        }

        private static string GetOnlyInOneOfTheTwoText(Dictionary<string, string> items, bool isFirst)
        {
            if (items.Count == 0)
            {
                return "";
            }

            StringBuilder builder = new StringBuilder();

            builder.AppendFormat("Attributes present only in {0} instance:", isFirst ? "first" : "second");
            builder.AppendLine();

            foreach (KeyValuePair<string, string> item in items)
            {
                builder.AppendFormat("[{0}] : {1}", item.Key, item.Value);
                builder.AppendLine();
            }

            return builder.ToString();
        }

        #endregion

        public bool IsCompletetelyIdentical
        {
            get { return DifferentValues.Count == 0 && OnlyInFirst.Count == 0 && OnlyInSecond.Count == 0; }
        }
    }
}