﻿namespace ValidataCommon
{
    /// <summary>
    /// 	Verifier of DB values
    /// </summary>
    public static class DbValuesVerifier
    {
        /// <summary>
        /// 	Trims the length of the string attribute value to fit max.
        /// </summary>
        /// <returns></returns>
        public static string TrimStringAttributeValueToFitInMaxAllowedLength(string val)
        {
            if (val == null)
                return null;

            if (val.Length <= DbConstants.MaxLengthOfStringAttribute)
            {
                return val;
            }

            return val.Substring(0, DbConstants.MaxLengthOfStringAttribute);
        }
    }
}