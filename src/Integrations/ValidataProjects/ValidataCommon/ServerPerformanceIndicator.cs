﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Performance indicator measured on the server
    /// </summary>
    public class ServerPerformanceIndicator
    {
        /// <summary>
        /// ServerPerformanceIndicator
        /// </summary>
        /// <param name="name"></param>
        /// <param name="measurement"></param>
        public ServerPerformanceIndicator(string name, string measurement)
        {
            Name = name;
            Mesurement = measurement;
        }

        /// <summary>
        /// Name of the performance indicator
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// UOM of the performance indicator
        /// </summary>
        public string Mesurement { get; set; }
    }
}
