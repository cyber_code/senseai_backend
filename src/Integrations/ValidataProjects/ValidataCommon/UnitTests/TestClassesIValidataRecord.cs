﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon.UnitTests
{
    class _TestAttribute : IAttribute
    {
        public _TestAttribute(string name, string value)
        {
            Name = name;
            Value = value;
        }

        #region IAttribute Members

        public string Name { get; set; }
        public string Value { get; set; }

        #endregion
    }

    class _TestValidataRecord : IValidataRecord
    {
        public _TestValidataRecord(string cat, string typ)
        {
            CatalogName = cat;
            TypicalName = typ;
        }

        internal void AddAttribute(string name, string value)
        {
            AttributeList.Add(new _TestAttribute(name, value));
        }

        #region IValidataRecord Members

        public string CatalogName { get; set; }

        public string TypicalName { get; set; }

        public List<IAttribute> AttributeList;
        public IEnumerable<IAttribute> Attributes { get { return AttributeList; } }

        public IAttribute FindAttribute(string name)
        {
            return AttributeList.FirstOrDefault(n => n.Name == name);
        }

        #endregion
    }
}
