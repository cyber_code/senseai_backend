﻿using System.IO;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Testing T24 Records Repository
    /// </summary>
    [TestFixture]
    public class T24RecordsRepositoryTests
    {
        /// <summary>
        /// Tests the serialization common.
        /// </summary>
        [Test]
        public void TestSerializationCommon()
        {
            const string appName = "TEST.APP.COMMON";

            DeleteRecordsReporitoryFile(null, appName);

            T24RecordsCollection records = new T24RecordsCollection();
            records.Add(GetRecord(appName, 1));

            var dtStart = records.DateExtracted;
            var configSet = records.ConfigurationSet = "My Config Set";
            var commMethod = records.CommunicationMethod = "My Communication Method";

            string errorMessage;
            bool result = T24RecordsRepository.Instance.SetRecords(string.Empty, appName, records, out errorMessage);
            Assert.IsTrue(result);

            var resultRecords = T24RecordsRepository.Instance.GetRecords(null, appName, true, true, out errorMessage);
            Assert.IsNotNull(resultRecords);

            Assert.AreEqual(resultRecords.DateExtracted, dtStart);
            Assert.AreEqual(resultRecords.ConfigurationSet, configSet);
            Assert.AreEqual(resultRecords.CommunicationMethod, commMethod);
        }

        /// <summary>
        /// Tests the serialization catalog unbound.
        /// </summary>
        [Test]
        public void TestSerializationCatalogUnbound()
        {
            const string appName = "TEST.APP";

            DeleteRecordsReporitoryFile(null, appName);

            T24RecordsCollection records = new T24RecordsCollection();
            records.Add(GetRecord(appName, 1));
            records.Add(GetRecord(appName, 2));
            records.Add(GetRecord(appName, 3));

            string errorMessage;
            bool result = T24RecordsRepository.Instance.SetRecords(string.Empty, appName, records, out errorMessage);
            Assert.IsTrue(result);
            
            // get from cache
            var resultRecords = T24RecordsRepository.Instance.GetRecords(string.Empty, appName, out errorMessage);
            Assert.IsNotNull(resultRecords);
            CompareRecordCollections(records, resultRecords);

            // get from disk
            resultRecords = T24RecordsRepository.Instance.GetRecords(null, appName, true, true, out errorMessage);
            Assert.IsNotNull(resultRecords);
            CompareRecordCollections(records, resultRecords);
        }

        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void TestSerializationCatalogBound()
        {
            const string catalogName = "MyCatalog";
            const string appName = "TEST.APP";
            const string appName1 = "TEST.APP.1";

            DeleteRecordsReporitoryFile(null, appName);
            DeleteRecordsReporitoryFile(catalogName, appName);
            DeleteRecordsReporitoryFile(null, appName1);
            DeleteRecordsReporitoryFile(catalogName, appName1);

            // exact match
            {
                T24RecordsCollection records = new T24RecordsCollection();
                records.Add(GetRecord(appName, 1));
                records.Add(GetRecord(appName, 2));
                records.Add(GetRecord(appName, 3));

                string errorMessage;
                bool result = T24RecordsRepository.Instance.SetRecords(catalogName, appName, records, out errorMessage);
                Assert.IsTrue(result);

                // get from cache
                var resultRecords = T24RecordsRepository.Instance.GetRecords(catalogName, appName, out errorMessage);
                Assert.IsNotNull(resultRecords);
                CompareRecordCollections(records, resultRecords);

                // get from disk
                resultRecords = T24RecordsRepository.Instance.GetRecords(catalogName, appName, true, true, out errorMessage);
                Assert.IsNotNull(resultRecords);
                CompareRecordCollections(records, resultRecords);
            }

            // default
            {
                T24RecordsCollection records = new T24RecordsCollection();
                records.Add(GetRecord(appName1, 1));
                records.Add(GetRecord(appName1, 2));
                records.Add(GetRecord(appName1, 3));
                records.Add(GetRecord(appName1, 4));

                string errorMessage;
                bool result = T24RecordsRepository.Instance.SetRecords("", appName1, records, out errorMessage);
                Assert.IsTrue(result);

                var resultRecords = T24RecordsRepository.Instance.GetRecords("MyCatalog", appName1, out errorMessage);
                Assert.IsNotNull(resultRecords);
                CompareRecordCollections(records, resultRecords);
            }
        }

        private void DeleteRecordsReporitoryFile(string catalogName, string appName)
        {
            string fullPath = T24RecordsRepository.GetFilePathForApplication(catalogName, appName);
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
        }

        private static T24Record GetRecord(string appName, int idx)
        {
            T24Record result = new T24Record(appName);
            result.Fields.Add("IDX", idx.ToString());

            for (int i = 0; i < idx*2; i++)
            {
                string fieldName = "FIELD-" + i;
                string fieldValue = "VALUE-" + i;
                result.Fields.Add(fieldName, fieldValue);
            }

            return result;
        }

        private void CompareRecordCollections(T24RecordsCollection records, T24RecordsCollection resultRecords)
        {
            Assert.AreEqual(records.Count, resultRecords.Count);

            foreach (var record in records.Records)
            {
                // find result record
                T24Record resultRecord = null;
                foreach (var r in resultRecords.Records)
                {
                    if (r.Fields.GetValue("IDX") == record.Fields.GetValue("IDX"))
                    {
                        resultRecord = r;
                        break;
                    }
                }

                Assert.IsNotNull(resultRecord);

                // compare attributes
                foreach (var field in record.Fields)
                {
                    Assert.AreEqual(field.Value, record.Fields.GetValue(field.Name));
                }
            }
        }
    }
}