using System;
using System.Globalization;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Tests the validata converter
    /// </summary>
    [TestFixture]
    public class TestValidataConvertor
    {
        /// <summary>
        /// Tests the get int.
        /// </summary>
        [Test]
        public void TestGetInt()
        {
            Assert.AreEqual(1, ValidataConverter.GetInt("1"));
            Assert.AreEqual(-1, ValidataConverter.GetInt("-1"));
            Assert.AreEqual(1, ValidataConverter.GetInt(" 1 "));

            Assert.AreEqual(ValidataConverter.DEFAULT_INT, ValidataConverter.GetInt(null));
            Assert.AreEqual(ValidataConverter.DEFAULT_INT, ValidataConverter.GetInt(ValidataConverter.NO_VALUE));
            Assert.AreEqual(ValidataConverter.DEFAULT_INT, ValidataConverter.GetInt(" "));
            Assert.AreEqual(ValidataConverter.DEFAULT_INT, ValidataConverter.GetInt("alabala"));
            Assert.AreEqual(ValidataConverter.DEFAULT_INT, ValidataConverter.GetInt(long.MaxValue.ToString()));
        }

        /// <summary>
        /// Tests the get ulong.
        /// </summary>
        [Test]
        public void TestGetULong()
        {
            Assert.AreEqual(1, ValidataConverter.GetULong("1"));
            Assert.AreEqual(ulong.MaxValue, ValidataConverter.GetULong(ulong.MaxValue.ToString()));
            Assert.AreEqual(ulong.MinValue, ValidataConverter.GetULong(ulong.MinValue.ToString()));
            Assert.AreEqual(1, ValidataConverter.GetULong(" 1 "));

            Assert.AreEqual(ValidataConverter.DEFAULT_ULONG, ValidataConverter.GetULong("-1"));
            Assert.AreEqual(ValidataConverter.DEFAULT_ULONG, ValidataConverter.GetULong(null));
            Assert.AreEqual(ValidataConverter.DEFAULT_ULONG, ValidataConverter.GetULong(ValidataConverter.NO_VALUE));
            Assert.AreEqual(ValidataConverter.DEFAULT_ULONG, ValidataConverter.GetULong(" "));
            Assert.AreEqual(ValidataConverter.DEFAULT_ULONG, ValidataConverter.GetULong("alabala"));
        }

        /// <summary>
        /// Tests the get real.
        /// </summary>
        [Test]
        public void TestGetReal()
        {
            Assert.AreEqual(1.1, ValidataConverter.GetDouble("1.1"));
            Assert.AreEqual(1.1, ValidataConverter.GetDouble(" 1.1 "));

            Assert.AreEqual(1.1, ValidataConverter.GetDouble(" 1,1 "));
            Assert.AreEqual(1.1, ValidataConverter.GetDouble(" 1,1 "));

            Assert.AreEqual(123.45, ValidataConverter.GetDouble("123,45"));

            Assert.AreEqual(11111.1, ValidataConverter.GetDouble("111,11.10"));
            Assert.AreEqual(11111.1, ValidataConverter.GetDouble("111,11.1"));
            Assert.AreEqual(11111.1, ValidataConverter.GetDouble("111,11.100"));

            // special case for single comma -> to be treated as thousands separator, if there are three items 
            Assert.AreEqual(1111, ValidataConverter.GetDouble("1,111"));
            Assert.AreEqual(11111, ValidataConverter.GetDouble("11,111"));
            Assert.AreEqual(111111, ValidataConverter.GetDouble("111,111"));
            Assert.AreEqual(1111111, ValidataConverter.GetDouble("1,111,111"));

            Assert.AreEqual(ValidataConverter.DEFAULT_REAL, ValidataConverter.GetDouble(null));
            Assert.AreEqual(ValidataConverter.DEFAULT_REAL, ValidataConverter.GetDouble(ValidataConverter.NO_VALUE));
            Assert.AreEqual(ValidataConverter.DEFAULT_REAL, ValidataConverter.GetDouble(" "));
            Assert.AreEqual(ValidataConverter.DEFAULT_REAL, ValidataConverter.GetDouble("alabala"));

            // NOTE: This is a limitation of the convertor, when dealing with very big numbers
            Assert.AreNotEqual(double.MaxValue, ValidataConverter.GetDouble(double.MaxValue.ToString(CultureInfo.InvariantCulture)));
            Assert.AreNotEqual(double.MinValue, ValidataConverter.GetDouble(double.MinValue.ToString(CultureInfo.InvariantCulture)));
        }

        /// <summary>
        /// Test normzalize decimal to poin in real
        /// </summary>
        [Test]
        public void TestNormalizeDecimalPointInReal()
        {
            Assert.AreEqual("1.1", ValidataConverter.NormalizeDecimalPointInReal("1.1"));
            Assert.AreEqual("1.1", ValidataConverter.NormalizeDecimalPointInReal("1,1"));
            Assert.AreEqual("111,12.80", ValidataConverter.NormalizeDecimalPointInReal("111,12.80"));
            Assert.AreEqual("111.1280", ValidataConverter.NormalizeDecimalPointInReal("111,1280"));
            
            Assert.AreEqual("111.12", ValidataConverter.NormalizeDecimalPointInReal("111,12"));
            Assert.AreEqual("221,221", ValidataConverter.NormalizeDecimalPointInReal("221,221"));
            Assert.AreEqual("1,221,221", ValidataConverter.NormalizeDecimalPointInReal("1,221,221"));

            double dbl;
            double.TryParse(ValidataConverter.NormalizeDecimalPointInReal(ValidataConverter.NormalizeDecimalPointInReal("111,12.80")), NumberStyles.Any, CultureInfo.InvariantCulture, out dbl);
            Assert.AreEqual(11112.80, dbl);
            double.TryParse(ValidataConverter.NormalizeDecimalPointInReal(ValidataConverter.NormalizeDecimalPointInReal("111,1280")), NumberStyles.Any, CultureInfo.InvariantCulture, out dbl);
            Assert.AreEqual(111.1280, dbl);
            double.TryParse(ValidataConverter.NormalizeDecimalPointInReal(ValidataConverter.NormalizeDecimalPointInReal("1,221,221")), NumberStyles.Any, CultureInfo.InvariantCulture, out dbl);
            Assert.AreEqual(1221221, dbl);
        }

        /// <summary>
        /// Tests the get decimal.
        /// </summary>
        [Test]
        public void TestGetDecimal()
        {
            Assert.AreEqual(1.1, ValidataConverter.GetDecimal("1.1"));
            Assert.AreEqual(decimal.MaxValue, ValidataConverter.GetDecimal(decimal.MaxValue.ToString(CultureInfo.InvariantCulture)));
            Assert.AreEqual(decimal.MinValue, ValidataConverter.GetDecimal(decimal.MinValue.ToString(CultureInfo.InvariantCulture)));
            Assert.AreEqual(1.1, ValidataConverter.GetDecimal(" 1.1 "));
            Assert.AreEqual(1.1, ValidataConverter.GetDecimal(" 1,1 "));

            Assert.AreEqual(ValidataConverter.DEFAULT_DECIMAL, ValidataConverter.GetDecimal(null));
            Assert.AreEqual(ValidataConverter.DEFAULT_DECIMAL, ValidataConverter.GetDecimal(ValidataConverter.NO_VALUE));
            Assert.AreEqual(ValidataConverter.DEFAULT_DECIMAL, ValidataConverter.GetDecimal(" "));
            Assert.AreEqual(ValidataConverter.DEFAULT_DECIMAL, ValidataConverter.GetDecimal("alabala"));
        }

        /// <summary>
        /// Tests the get boolean.
        /// </summary>
        [Test]
        public void TestGetBoolean()
        {
            Assert.IsFalse(ValidataConverter.GetBoolean("0"));
            Assert.IsFalse(ValidataConverter.GetBoolean(" 0"));
            Assert.IsFalse(ValidataConverter.GetBoolean("false"));
            Assert.IsFalse(ValidataConverter.GetBoolean("FALSE"));
            Assert.IsFalse(ValidataConverter.GetBoolean("f"));
            
            Assert.IsTrue(ValidataConverter.GetBoolean("TRUE"));
            Assert.IsTrue(ValidataConverter.GetBoolean("t"));
            Assert.IsTrue(ValidataConverter.GetBoolean("true"));
            Assert.IsTrue(ValidataConverter.GetBoolean("1"));

            Assert.AreEqual(ValidataConverter.DEFAULT_BOOLEAN, ValidataConverter.GetBoolean(null));
            Assert.AreEqual(ValidataConverter.DEFAULT_BOOLEAN, ValidataConverter.GetBoolean(ValidataConverter.NO_VALUE));
            Assert.AreEqual(ValidataConverter.DEFAULT_BOOLEAN, ValidataConverter.GetBoolean(" "));
            Assert.AreEqual(ValidataConverter.DEFAULT_BOOLEAN,ValidataConverter.GetBoolean("alabala"));
            Assert.AreEqual(ValidataConverter.DEFAULT_BOOLEAN, ValidataConverter.GetBoolean("2"));
        }

        /// <summary>
        /// Tests the get date.
        /// </summary>
        [Test]
        public void TestGetDate()
        {
            DateTime dateTime = new DateTime(2007, 3, 4, 1, 1, 1);

            Assert.AreEqual(dateTime.Date, ValidataConverter.GetDateTime(dateTime.ToString("yyyyMMdd")));
            Assert.AreEqual(dateTime, ValidataConverter.GetDateTime(dateTime.Ticks.ToString()));
            Assert.AreEqual(dateTime, ValidataConverter.GetDateTime(dateTime.ToString(CultureInfo.InvariantCulture)));
            Assert.AreEqual(dateTime, ValidataConverter.GetDateTime(dateTime.ToString(new CultureInfo("en-US", true))));

            Assert.AreEqual(DateTime.MinValue, ValidataConverter.GetDateTime(null));
            Assert.AreEqual(DateTime.MinValue, ValidataConverter.GetDateTime(ValidataConverter.NO_VALUE));
            Assert.AreEqual(DateTime.MinValue, ValidataConverter.GetDateTime(" "));
            Assert.AreEqual(DateTime.MinValue, ValidataConverter.GetDateTime("alabala"));
        }
    }
}