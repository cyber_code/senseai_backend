﻿using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    [TestFixture]
    public class NArrayWrapperTest
    {
        [Test]
        public void TestNArrayParsing()
        {
            Test("35.3.C", 35, 3);
            Test("35", 35, -1);
            Test("10.3", 10, 3);
            Test("4..C", 4, -1);
            Test(".8.C", -1, 8);
            Test("..X", -1, -1);
            Test(".", -1, -1);
            Test("..", -1, -1);
            Test("", -1, -1);
            Test(null, -1, -1);
        }

        private void Test(string nArrayValue, int expectedMaxValue, int expectedMinValue)
        {
            var r = new NArrayWrapper(nArrayValue);
            Assert.AreEqual(r.Max, expectedMaxValue);
            Assert.AreEqual(r.Min, expectedMinValue);
        }
    }
}
