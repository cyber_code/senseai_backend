﻿using System;
using System.IO;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Tests http adapter time schedule calculations for execution
    /// </summary>
    [TestFixture]
    public class TimeScheduleCalculationsTest
    {
        /// <summary>
        /// Testing calculation of total ramp up duration
        /// </summary>
        [Test]
        public void Test_CalculateRampUpsDuration()
        {
            var rampUpInterval = new TimeSpan(0, 0, 20);

            // Test with InitialUsersCount == 0 and max users aliquot to RampUpCount
            {
                TimeSchedule ts = new TimeSchedule()
                {
                    InitialUsersCount = 0,
                    RampUpCount = 5,
                    RampUpPeriod = rampUpInterval
                };

                var result = ts.CalculateRampUpsDuration(15);
                // we do 3 ramp ups, but because InitialUserCount == 0 it starts imediately,
                //  thats why we have to reach the peak at 2 intervals
                var expected = new TimeSpan(rampUpInterval.Ticks * 2);
                Assert.AreEqual(result, expected);
            }

            // Test with InitialUsersCount == 0 and max users NOT aliquot to RampUpCount
            {
                TimeSchedule ts = new TimeSchedule()
                {
                    InitialUsersCount = 0,
                    RampUpCount = 5,
                    RampUpPeriod = rampUpInterval
                };

                var result = ts.CalculateRampUpsDuration(16);
                // we do 4 ramp ups, but because InitialUserCount == 0 it starts imediately,
                //  thats why we have to reach the peak at 3 intervals
                var expected = new TimeSpan(rampUpInterval.Ticks * 3);
                Assert.AreEqual(result, expected);
            }

            // Test with InitialUsersCount != 0 and max users aliquot to RampUpCount
            {
                TimeSchedule ts = new TimeSchedule()
                {
                    InitialUsersCount = 5,
                    RampUpCount = 5,
                    RampUpPeriod = rampUpInterval
                };

                var result = ts.CalculateRampUpsDuration(15);
                // we do 2 ramp ups: ((15-5)/5)
                var expected = new TimeSpan(rampUpInterval.Ticks * 2);
                Assert.AreEqual(result, expected);
            }

            // Test with InitialUsersCount != 0 and max users NOT aliquot to RampUpCount
            {
                TimeSchedule ts = new TimeSchedule()
                {
                    InitialUsersCount = 5,
                    RampUpCount = 5,
                    RampUpPeriod = rampUpInterval
                };

                var result = ts.CalculateRampUpsDuration(16);
                // we do 3 ramp ups
                var expected = new TimeSpan(rampUpInterval.Ticks * 3);
                Assert.AreEqual(result, expected);
            }

        }

        /// <summary>
        /// Testing calculation of total ramp down duration
        /// </summary>
        [Test]
        public void Test_CalculateRampDownsDuration()
        {
            var rampDownInterval = new TimeSpan(0, 0, 20);

            // Test with FinalUsersCount == 0 and max users aliquot to RampDownCount
            {
                TimeSchedule ts = new TimeSchedule()
                {
                    FinalUsersCount = 0,
                    RampDownCount = 5,
                    RampDownPeriod = rampDownInterval
                };

                var result = ts.CalculateRampDownsDuration(15);
                var expected = new TimeSpan(rampDownInterval.Ticks * 2);
                Assert.AreEqual(result, expected);
            }

            // Test with FinalUsersCount == 0 and max users NOT aliquot to RampDownCount
            {
                TimeSchedule ts = new TimeSchedule()
                {
                    FinalUsersCount = 0,
                    RampDownCount = 5,
                    RampDownPeriod = rampDownInterval
                };

                var result = ts.CalculateRampDownsDuration(16);
                var expected = new TimeSpan(rampDownInterval.Ticks * 3);
                Assert.AreEqual(result, expected);
            }

            // Test with FinalUsersCount != 0 and max users aliquot to RampDownCount
            {
                TimeSchedule ts = new TimeSchedule()
                {
                    FinalUsersCount = 5,
                    RampDownCount = 5,
                    RampDownPeriod = rampDownInterval
                };

                var result = ts.CalculateRampDownsDuration(15);
                // we do 2 ramp downs: ((15-5)/5)
                var expected = new TimeSpan(rampDownInterval.Ticks * 2);
                Assert.AreEqual(result, expected);
            }

            // Test with FinalUsersCount != 0 and max users NOT aliquot to RampDownCount
            {
                TimeSchedule ts = new TimeSchedule()
                {
                    FinalUsersCount = 5,
                    RampDownCount = 5,
                    RampDownPeriod = rampDownInterval
                };

                var result = ts.CalculateRampDownsDuration(16);
                // we do 3 ramp downs
                var expected = new TimeSpan(rampDownInterval.Ticks * 3);
                Assert.AreEqual(result, expected);
            }
        }
    }
}
