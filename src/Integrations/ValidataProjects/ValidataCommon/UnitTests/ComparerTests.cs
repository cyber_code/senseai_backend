﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Validata.Common;

namespace ValidataCommon.UnitTests
{
    [TestFixture]
    public class ComparerTests
    {
        /// <summary>
        /// Tests Decimal values comparison
        /// </summary>		
        [Test]
        public void CompareDecimalValues()
        {
            // Initialize without rules
            ShortComparisonResult res = Comparer.CompareValues(AttributeDataType.Currency, "-30120.50", "-30,120.50", ComparisonOperator.Equal);
            Assert.IsFalse(res.Outcome == ComparisonOutcome.Failed, "Decimal values -30120.50 and -30,120.50 are not correctly compared !");

            res = Comparer.CompareValues(AttributeDataType.Currency, "-3000120.50", "-3,000,120.50", ComparisonOperator.Equal);
            Assert.IsFalse(res.Outcome == ComparisonOutcome.Failed, "Decimal values -3000120.50 and -3,000,120.50 are not correctly compared !");

            res = Comparer.CompareValues(AttributeDataType.Currency, "-3000120.50", "-3000120,50", ComparisonOperator.Equal);
            Assert.IsFalse(res.Outcome == ComparisonOutcome.Failed, "Decimal values -3000120.50 and -3000120,50 are not correctly compared !");
        }

        [Test]
        public void CompareAttributeNames()
        {
            var comp = new AttributeNameComparer();
            Assert.AreEqual(0, comp.Compare("CUSTOMER", "CUSTOMER"));

            Assert.Greater(comp.Compare("CUSTOMER", "ADDRESS"), 0);

            Assert.Less(comp.Compare("ADDRESS", "CUSTOMER"),0);

            Assert.AreEqual(0,comp.Compare("CUSTOMER-1~1", "CUSTOMER-1~1"));

            Assert.Less(comp.Compare("CUSTOMER-1~1", "CUSTOMER-2~1"),0);

            Assert.Greater(comp.Compare("CUSTOMER-2~1", "CUSTOMER-1~1"),0);

            Assert.Less(comp.Compare("CUSTOMER-5~1", "CUSTOMER-25~1"),0);

            Assert.Greater(comp.Compare("CUSTOMER-25~1", "CUSTOMER-5~1"),0);

            Assert.AreEqual(0,comp.Compare("{PROPERTY}.CUSTOMER", "{PROPERTY}.CUSTOMER"));

            Assert.Greater(comp.Compare("{PROPERTY}.CUSTOMER", "{PROPERTY}.ADDRESS"), 0);

            Assert.Less(comp.Compare("{PROPERTY}.ADDRESS", "{PROPERTY}.CUSTOMER"), 0);

            Assert.AreEqual(0, comp.Compare("{PROPERTY}.CUSTOMER-1~1", "{PROPERTY}.CUSTOMER-1~1"));

            Assert.Less(comp.Compare("{PROPERTY}.CUSTOMER-1~1", "{PROPERTY}.CUSTOMER-2~1"), 0);

            Assert.Greater(comp.Compare("{PROPERTY}.CUSTOMER-2~1", "{PROPERTY}.CUSTOMER-1~1"), 0);

            Assert.Less(comp.Compare("{PROPERTY}.CUSTOMER-5~1", "{PROPERTY}.CUSTOMER-25~1"), 0);

            Assert.Greater(comp.Compare("{PROPERTY}.CUSTOMER-25~1", "{PROPERTY}.CUSTOMER-5~1"), 0);


        }
    }
}
