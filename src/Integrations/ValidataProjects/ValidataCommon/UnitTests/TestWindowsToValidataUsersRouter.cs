﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Test WindowsToValidataUsersRouter
    /// </summary>
    [TestFixture]
    public class TestWindowsToValidataUsersRouter
    {
        /// <summary>
        /// Creates the read and use.
        /// </summary>
        [Test]
        public void CreateReadAndUse()
        {
            StringBuilder sampleFileText = new StringBuilder();
            sampleFileText.AppendLine(@"__defaultDomain:TestDomain");
            sampleFileText.AppendLine(@"user1:vuser1");
            sampleFileText.AppendLine(@"TestDomain\user2:vuser2");
            sampleFileText.AppendLine(@"AnotherDomain\user2:vuser3");
                
            string tempFile = Path.GetTempFileName();
            File.WriteAllText(tempFile, sampleFileText.ToString());

            WindowsToValidataUsersRouter router = new WindowsToValidataUsersRouter(tempFile);
            Assert.AreEqual("vuser1", router.GetCorrespondingValidataUserName(@"TestDomain\user1"));
            Assert.AreEqual("vuser2", router.GetCorrespondingValidataUserName(@"TestDomain\user2"));
            Assert.AreEqual("vuser3", router.GetCorrespondingValidataUserName(@"AnotherDomain\user2"));
            Assert.AreEqual("someOtherUser", router.GetCorrespondingValidataUserName(@"AnotherDomain\someOtherUser"));

            File.Delete(tempFile);
        }
    }
}
