using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Test the generation of concecutive IDs
    /// </summary>
    [TestFixture]
    public class GeneratorOfConsequtiveIDsTest
    {
        /// <summary>
        /// Tests the simple numeric.
        /// </summary>
        [Test]
        public void TestSimpleNumeric()
        {
            Assert.AreEqual("2", GeneratorOfConsequtiveIDs.GetNextID("1"));
        }

        /// <summary>
        /// Tests the numeric ending with nine.
        /// </summary>
        [Test]
        public void TestNumericEndingWithNine()
        {
            Assert.AreEqual("10", GeneratorOfConsequtiveIDs.GetNextID("9"));
        }

        /// <summary>
        /// Tests the long numeric.
        /// </summary>
        [Test]
        public void TestLongNumeric()
        {
            Assert.AreEqual("11111111111111111112", GeneratorOfConsequtiveIDs.GetNextID("11111111111111111111"));
        }

        /// <summary>
        /// Tests the not numeric.
        /// </summary>
        [Test]
        public void TestNotNumeric()
        {
            Assert.AreEqual("1A1", GeneratorOfConsequtiveIDs.GetNextID("1A"));
            Assert.AreEqual("AAAAA1", GeneratorOfConsequtiveIDs.GetNextID("AAAAA"));
        }

        /// <summary>
        /// Tests the mixed ending with number.
        /// </summary>
        [Test]
        public void TestMixedEndingWithNumber()
        {
            Assert.AreEqual("A124", GeneratorOfConsequtiveIDs.GetNextID("A123"));
            Assert.AreEqual("A1000", GeneratorOfConsequtiveIDs.GetNextID("A999"));
            Assert.AreEqual("333AAA124", GeneratorOfConsequtiveIDs.GetNextID("333AAA123"));
        }

        /// <summary>
        /// Tests the mixed ending with letter.
        /// </summary>
        [Test]
        public void TestMixedEndingWithLetter()
        {
            Assert.AreEqual("A123A1", GeneratorOfConsequtiveIDs.GetNextID("A123A"));
        }
    }
}