using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Test the AlphaNumericIdentifierComparer
    /// </summary>
    [TestFixture]
    public class AlphaNumericIdentifierComparerTest
    {
        /// <summary>
        /// Tests the ordering.
        /// </summary>
        [Test]
        public void TestOrdering()
        {
            string[] notOrdered = new string[]
                {
                    "PRE1",
                    "PRE2",
                    "PRE11",
                    "2",
                    "11",
                    "1",
                    "",
                    "PRE1c",
                    "PRE1a",
                    "PRE1b",
                };

            List<string> list = new List<string>(notOrdered);
            list.Sort(new AlphaNumericIdentifierComparer());
            string[] ordered = list.ToArray();

            foreach (string s in ordered)
            {
                Trace.WriteLine(s);
            }

            Assert.AreEqual("", ordered[0]);
            Assert.AreEqual("1", ordered[1]);
            Assert.AreEqual("2", ordered[2]);
            Assert.AreEqual("11", ordered[3]);
            Assert.AreEqual("PRE1", ordered[4]);
            Assert.AreEqual("PRE1a", ordered[5]);
            Assert.AreEqual("PRE1b", ordered[6]);
            Assert.AreEqual("PRE1c", ordered[7]);
            Assert.AreEqual("PRE2", ordered[8]);
            Assert.AreEqual("PRE11", ordered[9]);
        }
    }
}