﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{

 
    [TestFixture]
    class TestDifferenceErrorConfigurationSerialization
    {

        private DifferenceErrorConfiguration _Save;
        private DifferenceErrorConfiguration _Load;

        [Test]
        public void TestSaveLoadCompare()
        {
            TestSave();
            TestLoad();

            Assert.AreEqual(_Save.Version, _Load.Version);
            Assert.AreEqual(_Save.DifferenceErrorIgnoreRuleList.Count, _Load.DifferenceErrorIgnoreRuleList.Count);
            Assert.AreEqual(_Save.DifferenceErrorConversionRuleList.Count, _Load.DifferenceErrorConversionRuleList.Count);
            for (int i = 0; i < _Save.DifferenceErrorIgnoreRuleList.Count; i++)
            {
                Assert.AreEqual(_Save.DifferenceErrorIgnoreRuleList[i], _Save.DifferenceErrorIgnoreRuleList[i]);
            }
            for (int i = 0; i < _Save.DifferenceErrorConversionRuleList.Count; i++)
            {
                Assert.AreEqual(_Save.DifferenceErrorConversionRuleList[i], _Save.DifferenceErrorConversionRuleList[i]);
            }
        }

        [TestFixtureSetUp]
        public void TestSave()
        {
            // configuration
            _Save = new DifferenceErrorConfiguration();
            _Save.Version = 1;
            _Save.DifferenceErrorIgnoreRuleList = new List<DifferenceErrorIgnoreRule>();
            _Save.DifferenceErrorConversionRuleList = new List<DifferenceErrorConversionRule>();

            // Difference Ignore
            DifferenceErrorIgnoreRule ignoreRule = null;

            // 1st rule
            ignoreRule = new DifferenceErrorIgnoreRule();
            ignoreRule.Name = "Ignore NEW in ARRANGEMENT";
            ignoreRule.Enabled = true;
            ignoreRule.Source = new IgnoreRuleElement("*", "*", "ARRANGEMENT", "NEW");
            ignoreRule.Target = new IgnoreRuleElement("*", "*", "ARRANGEMENT", "*");
            _Save.DifferenceErrorIgnoreRuleList.Add(ignoreRule);

            // 2nd rule
            ignoreRule = new DifferenceErrorIgnoreRule();
            ignoreRule.Name = "Ignore NULL in AA.*";
            ignoreRule.Enabled = true;
            ignoreRule.Source = new IgnoreRuleElement("*", "AA.*", "*", "NULL");
            ignoreRule.Target = new IgnoreRuleElement("*", "AA.*", "*", "*");
            _Save.DifferenceErrorIgnoreRuleList.Add(ignoreRule);

            // Difference Conversion
            DifferenceErrorConversionRule conversionRule = new DifferenceErrorConversionRule();

            // Decimal
            conversionRule.Name = "Convert Decimals";
            conversionRule.Enabled = true;
            conversionRule.Source = new RuleElement("*", "*", "*");
            conversionRule.Target = new RuleElement("*", "*", "*");
            conversionRule.ConversionType = "DECIMAL";
            _Save.DifferenceErrorConversionRuleList.Add(conversionRule);

            // store
            DifferenceErrorConfiguration.SerializeToXml(_Save, Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");
        }

        [Test]
        private void TestLoad()
        {
            _Load = DifferenceErrorConfiguration.DeserializeFromXml(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");
        }




        [Test]
        private void TestCompareFunction()
        {
            TestDecimal();
            TestARRANGEMENT();
            TestAA();
            TestOtherAA();

            //if (result.DifferentValues.Where(ProcessDifferences).Count() > 0 ||
            //    result.OnlyInFirst.Where(ProcessNullDifferencies).Count() > 0)
            //{
            //    AddDifferenceError(instanceTag, result);
            //}
            //else
            //{
            //    //  Remove any error messages for this data instance:
            //    //  Lack of error response at this stage will be treated as if the data instance is valid
            //    Errors.Remove(instanceTag);
            //}
        }

        [Test]
        public void TestAA()
        {
            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");

            _TestValidataRecord testDec1S = new _TestValidataRecord("Cat1", "AA.test");
            testDec1S.AttributeList = new List<IAttribute>();
            testDec1S.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "NULL"));

            _TestValidataRecord testDec1T = new _TestValidataRecord("Cat1", "AA.test");
            testDec1T.AttributeList = new List<IAttribute>();
            testDec1T.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "2.0"));


            InstanceAttributesComparisonResult result =
                InstanceAttributesComparer.Compare(testDec1S, testDec1T);

            Assert.AreEqual(result.Identical.Count, 1);


            _TestValidataRecord testDec2S = new _TestValidataRecord("Cat2", "AA.test");
            testDec2S.AttributeList = new List<IAttribute>();
            testDec2S.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "2"));

            _TestValidataRecord testDec2T = new _TestValidataRecord("Cat2", "AA.test");
            testDec2T.AttributeList = new List<IAttribute>();
            testDec2T.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "2.1"));


            result = InstanceAttributesComparer.Compare(testDec2S, testDec2T);

            Assert.AreNotEqual(result.Identical.Count, 1);
            InstanceAttributesComparer.CompareFunction = null;
        }

        [Test]
        public void TestOtherAA()
        {
            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");

            _TestValidataRecord testDec1S = new _TestValidataRecord("Cat1", "AA");
            testDec1S.AttributeList = new List<IAttribute>();
            testDec1S.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "NULL"));

            _TestValidataRecord testDec1T = new _TestValidataRecord("Cat1", "AA");
            testDec1T.AttributeList = new List<IAttribute>();
            testDec1T.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "2.0"));


            InstanceAttributesComparisonResult result =
                InstanceAttributesComparer.Compare(testDec1S, testDec1T);

            Assert.AreNotEqual(result.Identical.Count, 1);


            _TestValidataRecord testDec2S = new _TestValidataRecord("Cat2", "AA");
            testDec2S.AttributeList = new List<IAttribute>();
            testDec2S.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "2"));

            _TestValidataRecord testDec2T = new _TestValidataRecord("Cat2", "AA");
            testDec2T.AttributeList = new List<IAttribute>();
            testDec2T.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "2.1"));


            result = InstanceAttributesComparer.Compare(testDec2S, testDec2T);

            Assert.AreNotEqual(result.Identical.Count, 1);
            InstanceAttributesComparer.CompareFunction = null;
        }

        [Test]
        public void TestARRANGEMENT()
        {
            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");

            _TestValidataRecord testDec1S = new _TestValidataRecord("Cat1", "Type1");
            testDec1S.AttributeList = new List<IAttribute>();
            testDec1S.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "NEW"));

            _TestValidataRecord testDec1T = new _TestValidataRecord("Cat1", "Type1");
            testDec1T.AttributeList = new List<IAttribute>();
            testDec1T.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "2.0"));


            InstanceAttributesComparisonResult result =
                InstanceAttributesComparer.Compare(testDec1S, testDec1T);

            Assert.AreEqual(result.Identical.Count, 1);


            _TestValidataRecord testDec2S = new _TestValidataRecord("Cat2", "Type2");
            testDec2S.AttributeList = new List<IAttribute>();
            testDec2S.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "2"));

            _TestValidataRecord testDec2T = new _TestValidataRecord("Cat2", "Type2");
            testDec2T.AttributeList = new List<IAttribute>();
            testDec2T.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "2.1"));


            result = InstanceAttributesComparer.Compare(testDec2S, testDec2T);

            Assert.AreNotEqual(result.Identical.Count, 1);
            InstanceAttributesComparer.CompareFunction = null;
        }

        [Test]
        public void TestDecimal()
        {
            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");

            _TestValidataRecord testDec1S = new _TestValidataRecord("Cat1", "Type1");
            testDec1S.AttributeList = new List<IAttribute>();
            testDec1S.AttributeList.Add(new _TestAttribute("DEC", "2"));

            _TestValidataRecord testDec1T = new _TestValidataRecord("Cat1", "Type1");
            testDec1T.AttributeList = new List<IAttribute>();
            testDec1T.AttributeList.Add(new _TestAttribute("DEC", "2.0"));


            InstanceAttributesComparisonResult result =
                InstanceAttributesComparer.Compare(testDec1S, testDec1T);

            Assert.AreEqual(result.Identical.Count,1);


            _TestValidataRecord testDec2S = new _TestValidataRecord("Cat2", "Type2");
            testDec2S.AttributeList = new List<IAttribute>();
            testDec2S.AttributeList.Add(new _TestAttribute("DEC", "2"));

            _TestValidataRecord testDec2T = new _TestValidataRecord("Cat2", "Type2");
            testDec2T.AttributeList = new List<IAttribute>();
            testDec2T.AttributeList.Add(new _TestAttribute("DEC", "2.1"));


            result = InstanceAttributesComparer.Compare(testDec2S, testDec2T);

            Assert.AreNotEqual(result.Identical.Count, 1);
            InstanceAttributesComparer.CompareFunction = null;
        }


        [Test]
        public void Test10K()
        {
            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");

            int good = 5;
            DateTime start = DateTime.Now;
            _TestValidataRecord testDec10KS = new _TestValidataRecord("Cat1", "AA.test");
            testDec10KS.AttributeList = new List<IAttribute>();
            testDec10KS.AttributeList.Add(new _TestAttribute("AA", "NULL"));
            testDec10KS.AttributeList.Add(new _TestAttribute("DEC1", "2"));
            testDec10KS.AttributeList.Add(new _TestAttribute("DEC2", "2"));
            testDec10KS.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "NEW"));
            for (int s = testDec10KS.AttributeList.Count; s < good + 1; s++)
            {
                testDec10KS.AttributeList.Add(new _TestAttribute("A" + s, "NEW" + s));
            }
            for (int s = testDec10KS.AttributeList.Count; s < 50; s++)
            {
                testDec10KS.AttributeList.Add(new _TestAttribute("W" + s, "S" + s));
            }

            _TestValidataRecord testDec10KT = new _TestValidataRecord("Cat1", "AA.test");
            testDec10KT.AttributeList = new List<IAttribute>();
            testDec10KT.AttributeList.Add(new _TestAttribute("AA", "2.0"));
            testDec10KT.AttributeList.Add(new _TestAttribute("DEC1", "2.0"));
            testDec10KT.AttributeList.Add(new _TestAttribute("DEC2", "2.1"));
            testDec10KT.AttributeList.Add(new _TestAttribute("ARRANGEMENT", "2.0"));
            for (int s = testDec10KT.AttributeList.Count; s < good + 1; s++)
            {
                testDec10KT.AttributeList.Add(new _TestAttribute("A" + s, "NEW" + s));
            }
            for (int s = testDec10KT.AttributeList.Count; s < 50; s++)
            {
                testDec10KT.AttributeList.Add(new _TestAttribute("W" + s, "T" + s));
            }

            for (int test = 0; test < 100000; test++)
            {
                InstanceAttributesComparisonResult result =
                    InstanceAttributesComparer.Compare(testDec10KS, testDec10KT);
                Assert.AreEqual(good, result.Identical.Count);
            }

            DateTime end = DateTime.Now;

            TimeSpan diff = end - start;
            System.Console.WriteLine("Start: {0} End: {1} Diff: {2}", start.ToShortTimeString(), end.ToShortTimeString(), diff.ToString());
            InstanceAttributesComparer.CompareFunction = null;
        }

        [Test][Ignore]
        public void TestMultiValue()
        {
            InstanceAttributesComparer a = new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT1.Config");

            String result =  a.GetMultiValueSeparators("T24R10", "CUSTOMER", "NAME-1~1");

            Assert.AreEqual(":", result);

            result = a.GetMultiValueSeparators("T24R10", "CUSTOMER1", "NAME-1~1");

            Assert.AreEqual(":1", result);

            result = a.GetSubValueSeparators("T24R10", "CUSTOMER1", "NAME-1~1");

            Assert.AreEqual("!1", result);
        }

        [Test][Ignore]
        public void TestCompareMultiValues()
        {
            String fieldName = "NAME-1~1";
            String delimiterMV = "::";
            String delimiterSV = "!!";
            String sourceValue = "111::222::333::444::555::666";
            String targetValue = "111::222::333::444::555::666";
            String errorMessage;

            Assert.IsTrue(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsEmpty(errorMessage);

            sourceValue = "111::222::332::444::555::666";
            targetValue = "111::222::333::444::555::666";

            Assert.IsFalse(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsTrue(errorMessage.Length > 0);
            Console.WriteLine(errorMessage);

            sourceValue = "111::222::333::444";
            targetValue = "111::222::333::444::555::666";

            Assert.IsTrue(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsEmpty(errorMessage);


            sourceValue = "111::222::::444::555::666";
            targetValue = "111::222::333::444::555::666";

            Assert.IsTrue(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsEmpty(errorMessage);

            sourceValue = "111::222::332::444::555::666";
            targetValue = "111::222::333::444";

            Assert.IsFalse(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsTrue(errorMessage.Length > 0);
            Console.WriteLine(errorMessage);
            
            sourceValue = "111::222::333::444::555::666";
            targetValue = "111::222::333::445::555::666";

            Assert.IsFalse(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsTrue(errorMessage.Length > 0);
            Console.WriteLine(errorMessage);

            sourceValue = "111::222::332::444::555::666";
            targetValue = "";

            Assert.IsFalse(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsTrue(errorMessage.Length > 0);
            Console.WriteLine(errorMessage);

            sourceValue = "";
            targetValue = "111::222::333::444";

            Assert.IsTrue(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsEmpty(errorMessage);

            sourceValue = "111::222::331!!332!!333::444::555::666";
            targetValue = "111::222::331!!332!!333::444::555::666";

            Assert.IsTrue(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsEmpty(errorMessage);

            sourceValue = "111::222::331!!332!!333::444::555::666";
            targetValue = "111::222::331!!333!!333::445::555::666";

            Assert.IsFalse(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsTrue(errorMessage.Length > 0);
            Console.WriteLine(errorMessage);


            sourceValue = "111::222::331!!!!333::444::555::666";
            targetValue = "111::222::331!!333!!333::444::555::666";

            Assert.IsTrue(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsEmpty(errorMessage);

            
        }

        [Test]
        public void TestCompareCompressedMultiValueWithEmptyValues()
        {
            String fieldName = "NAME-1~1";
            String delimiterMV = "::";
            String delimiterSV = "!!";
            String sourceValue;
            String targetValue;
            String errorMessage;

            sourceValue = "111::222";
            targetValue = "111::222";

            Assert.IsTrue(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsTrue(errorMessage.Length == 0);
            Console.WriteLine(errorMessage);

            sourceValue = "111!!222";
            targetValue = "111!!222";

            Assert.IsTrue(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsTrue(errorMessage.Length == 0);
            Console.WriteLine(errorMessage);

            sourceValue = "111::";
            targetValue = "111";

            Assert.IsTrue(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsTrue(errorMessage.Length == 0);
            Console.WriteLine(errorMessage);

            sourceValue = "::";
            targetValue = "";

            Assert.IsTrue(InstanceAttributesComparer.CompareCompressedMultiValue(fieldName, sourceValue, targetValue, delimiterMV, delimiterSV, out errorMessage));
            Assert.IsTrue(errorMessage.Length == 0);
            Console.WriteLine(errorMessage);
        }
    }
}
