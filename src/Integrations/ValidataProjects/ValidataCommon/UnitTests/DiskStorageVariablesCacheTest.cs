﻿using NUnit.Framework;
using System;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Test for domain names functions
    /// </summary>
    [TestFixture]
    public class DiskStorageVariablesCacheTest
    {
        [Test]
        public void Test()
        {
            string cacheDirectory = Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\Temp\ACResultStorage_TEMP\";

            const string executionId1 = "asdfasd::\\>???";
            const string executionId2 = "11---::asdfasd::\\>???";

            DiskStorageVariablesCache.SetValue(cacheDirectory, executionId1, "DDD", 0, "value");
            DiskStorageVariablesCache.SetValue(cacheDirectory, executionId1, "DDD1", 1, "value-1");
            DiskStorageVariablesCache.SetValue(cacheDirectory, executionId1, "DDD1", 3, "value-3");
            DiskStorageVariablesCache.SetValue(cacheDirectory, executionId1, "DDD55", 55, "value-55");

            DiskStorageVariablesCache.SetValue(cacheDirectory, executionId2, "DDD", 0, "value-DDD");

            DiskStorageVariablesCache.ClearCache();

            string result;

            result = DiskStorageVariablesCache.GetValue(cacheDirectory, executionId1, "DDD", 0);
            Assert.AreEqual(result, "value");

            result = DiskStorageVariablesCache.GetValue(cacheDirectory, executionId1, "DDD1", 1);
            Assert.AreEqual(result, "value-1");

            result = DiskStorageVariablesCache.GetValue(cacheDirectory, executionId1, "DDD1", 3);
            Assert.AreEqual(result, "value-3");

            result = DiskStorageVariablesCache.GetValue(cacheDirectory, executionId1, "DDD55", 55);
            Assert.AreEqual(result, "value-55");

            result = DiskStorageVariablesCache.GetValue(cacheDirectory, executionId2, "DDD", 0);
            Assert.AreEqual(result, "value-DDD");
        }
    }
}
