﻿using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    [TestFixture]
    public class T24AdapterConfigurationParserTest
    {
        [Test]
        public void TestParse1Field()
        {
            // Command
            TestParseAndConstruct("S", null, "S", null, null, null, null, null);
            TestParseAndConstruct("", null, "", null, null, null, null, null);
            TestParseAndConstruct("No Value", null, "", null, null, null, null, null);
        }

        [Test]
        public void TestParse2Fields()
        {
            // Version,Command
            TestParseAndConstruct("CORP,I", "CORP", "I", null, null, null, null, null);
            TestParseAndConstruct(",I", null, "I", null, null, null, null, null);
        }

        [Test]
        public void TestParse3Fields()
        {
            // Command,User,Password
            TestParseAndConstruct("I,USR,PASS", null, "I", "USR", "PASS", null, null, null);
            TestParseAndConstruct("I,,PASS", null, "I", null, "PASS", null, null, null);
        }

        [Test]
        public void TestParse4Fields()
        {
            // Version,Command,User,Password
            TestParseAndConstruct("CORP,I,USR,PASS", "CORP", "I", "USR", "PASS", null, null, null);
            TestParseAndConstruct(",I,USR,PASS", null, "I", "USR", "PASS", null, null, null);
        }

        [Test]
        public void TestParse5Fields()
        {
            // Command,User,Password,AuthUser,AuthPassword
            TestParseAndConstruct("I,USR,PASS,AUSR,APASS", null, "I", "USR", "PASS", "AUSR", "APASS", null);
            TestParseAndConstruct("I,,,AUSR,APASS", null, "I", null, null, "AUSR", "APASS", null);
        }

        [Test]
        public void TestParse6Fields()
        {
            // Version,Command,User,Password,AuthUser,AuthPassword
            TestParseAndConstruct("CORP,I,USR,PASS,AUSR,APASS", "CORP", "I", "USR", "PASS", "AUSR", "APASS", null);
            TestParseAndConstruct("CORP,I,,,AUSR,APASS", "CORP", "I", null, null, "AUSR", "APASS", null);
        }

        [Test]
        public void TestParse7Fields()
        {
            // Version,Command,User,Password,AuthUser,AuthPassword,Company
            TestParseAndConstruct("CORP,I,USR,PASS,AUSR,APASS,MYCOMP", "CORP", "I", "USR", "PASS", "AUSR", "APASS", "MYCOMP");
            TestParseAndConstruct("CORP,I,,,,,MYCOMP", "CORP", "I", null, null, null, null, "MYCOMP");
            TestParseAndConstruct(",I,,,,,MYCOMP", null, "I", null, null, null, null, "MYCOMP");
        }

        private void TestParseAndConstruct(string configuration, string expectedVersion, string expectedCommand, string expectedUser, string expectedPassword, string expectedAuthUser, string expectedAuthPassword, string expectedCompany)
        {
            var result = T24AdapterConfiguration.Parse(configuration);

            AreEqual(result.Version, expectedVersion);
            AreEqual(result.Configuration, expectedCommand);
            AreEqual(result.InputUser, expectedUser);
            AreEqual(result.InputPassword, expectedPassword);
            AreEqual(result.AuthorizationUser, expectedAuthUser);
            AreEqual(result.AuthorizationPassword, expectedAuthPassword);
            AreEqual(result.Company, expectedCompany);

            // Test construct
            AreEqual(result.ToString(), configuration, true);
        }

        private void AreEqual(string actualValue, string expectedValue)
        {
            AreEqual(actualValue, expectedValue, false);
        }

        private void AreEqual(string actualValue, string expectedValue, bool assumeNoValueAsEmpty)
        {
            if (assumeNoValueAsEmpty)
            {
                if ((string.IsNullOrEmpty(expectedValue) || expectedValue == "No Value")
                    && (string.IsNullOrEmpty(actualValue) || actualValue == "No Value"))
                {
                    return; // OK
                }
            }
            else
            {
                if (string.IsNullOrEmpty(expectedValue) && string.IsNullOrEmpty(actualValue))
                {
                    return; // OK
                }
            }

            Assert.AreEqual(expectedValue, actualValue);
        }
    }
}
