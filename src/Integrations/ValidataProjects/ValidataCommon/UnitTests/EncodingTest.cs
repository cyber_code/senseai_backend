using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Testing XML encoding
    /// </summary>
    [TestFixture]
    public class EncodingTest
    {
        ///<summary>
        /// Test special characters
        ///</summary>
        [Test]
        public void TestSpecialCharacter()
        {

            byte[] buf = new byte[16];
            for (int i = 0; i < 16; i ++)
            {
                buf[i] = (byte) i;
            }

            StreamReader utf8reader = new StreamReader(new MemoryStream(buf), Encoding.UTF8);
            string utf8string = utf8reader.ReadToEnd();
            Trace.WriteLine("String: " + utf8string);
            Trace.WriteLine(CheckUnicodeString(utf8string));

            string xmlEncodedTextUtf8 = XmlConvert.EncodeName(utf8string);
            Trace.WriteLine("String after XmlConvert.EncodeName: " + xmlEncodedTextUtf8);

            string htmlEncodedTextUtf8 = Utils.GetHtmlEncodedValue(utf8string);
            Trace.WriteLine("String after HttpUtility.HtmlEncode: " + htmlEncodedTextUtf8);

            string xmlStringTextUtf8 = GetTextOutputOfXmlWriter(htmlEncodedTextUtf8, false);
            Trace.WriteLine("String after XmlTextWriter.WriteString: " + CheckUnicodeString(xmlStringTextUtf8));

            string xmlSafeStringTextUtf8 = GetTextOutputOfXmlWriter(htmlEncodedTextUtf8, true);
            Trace.WriteLine("String after SafeXmlTextWriter.WriteString: " + CheckUnicodeString(xmlSafeStringTextUtf8));
        }

        private static string CheckUnicodeString(String value)
        {
            for (int i = 0; i < value.Length; ++i)
            {
                if (value[i] > 0xFFFD)
                {
                    return "Invalid Unicode";
                }
                else if (value[i] < 0x20 && value[i] != '\t' & value[i] != '\n' & value[i] != '\r')
                {
                    return "Invalid Xml Characters";
                }
            }

            return "OK";
        }

        private static string GetTextOutputOfXmlWriter(string text, bool safeMode)
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter stringWriter = new StringWriter(sb))
            {
                using (XmlTextWriter xmlTextWriter = safeMode ? new SafeXmlTextWriter(stringWriter) : new XmlTextWriter(stringWriter))
                {
                    xmlTextWriter.WriteString(text);
                }
            }

            return sb.ToString();
        }
    }
}