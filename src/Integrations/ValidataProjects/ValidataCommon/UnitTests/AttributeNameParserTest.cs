using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Test the identifier parsing
    /// </summary>
    [TestFixture]
    public class AttributeNameParserTest
    {
        #region Test Invalid

        /// <summary>
        /// Tests the empty.
        /// </summary>
        [Test]
        public void TestEmpty()
        {
            VerifyNonMultivalue("");
        }

        /// <summary>
        /// Tests simplest form of non-multivalue attribute
        /// </summary>
        [Test]
        public void TestSimpleNonMultivalue()
        {
            VerifyNonMultivalue("A");
        }

        /// <summary>
        /// Tests non-multivalue attribute with dash
        /// </summary>
        [Test]
        public void TestNonMultivalueWithOneTypeOfSpecialSymbol()
        {
            VerifyNonMultivalue("-");
            VerifyNonMultivalue("A-B");
            VerifyNonMultivalue("A-");
            VerifyNonMultivalue("A-");
            VerifyNonMultivalue("-A");
            VerifyNonMultivalue("-AA-");

            VerifyNonMultivalue("~");
            VerifyNonMultivalue("A~B");
            VerifyNonMultivalue("A~");
            VerifyNonMultivalue("A~");
            VerifyNonMultivalue("~A");
            VerifyNonMultivalue("~AA~");
        }

        /// <summary>
        /// Tests the non multivalue with both type of special symbol.
        /// </summary>
        [Test]
        public void TestNonMultivalueWithEmptyFieldPrefix()
        {
            VerifyNonMultivalue("-0~0");
        }

        /// <summary>
        /// Tests the non multivalue when switched separators.
        /// </summary>
        [Test]
        public void TestNonMultivalueWithSwitchedDelimiters()
        {
            VerifyNonMultivalue("A~0-0");
        }

        /// <summary>
        /// Tests the non numeric indices.
        /// </summary>
        [Test]
        public void TestNonMultivalueWithNonNumericIndices()
        {
            VerifyNonMultivalue("-~");
            VerifyNonMultivalue("A-~");
            VerifyNonMultivalue("A-B~C");
            VerifyNonMultivalue("A-B~C");
            VerifyNonMultivalue("A-~0");
            VerifyNonMultivalue("-0~");
            VerifyNonMultivalue("A-0~");
        }

        private static void VerifyNonMultivalue(string fieldName)
        {
            Assert.IsFalse(AttributeNameParser.IsMultiValue(fieldName));
            Assert.AreEqual(fieldName, AttributeNameParser.GetShortFieldName(fieldName));
            Assert.AreEqual(0, AttributeNameParser.GetMultiValueIndex(fieldName));
            Assert.AreEqual(0, AttributeNameParser.GetSubValueIndex(fieldName));
        }

        #endregion

        #region Test Valid

        /// <summary>
        /// Tests the valid multivalue.
        /// </summary>
        [Test]
        public void TestValidMultivalue()
        {
            VerifyValidMultivalue("A-0~0", "A", 0, 0);
            VerifyValidMultivalue("A-0~1", "A", 0, 1);
            VerifyValidMultivalue("A-1~0", "A", 1, 0);
            VerifyValidMultivalue("A-1~1", "A", 1, 1);
            VerifyValidMultivalue("CUSTOMER-1~2", "CUSTOMER", 1, 2);
        }

        /// <summary>
        /// Tests the double occurance of pattern.
        /// </summary>
        [Test]
        public void TestValidMultivalueWithDoubleOccuranceOfPattern()
        {
            VerifyValidMultivalue("A-0~0-1~1", "A-0~0", 1, 1);
        }

        [Test]
        public void TestAA()
        {
            VerifyNonMultivalue("{ABC-2~3}.ATTR");
            VerifyValidMultivalue("{ABC-1~1}.ATTR-2~1", "{ABC-1~1}.ATTR", 2, 1);
        }

        [Test]
        public void TestUncommonNames()
        {
            VerifyNonMultivalue("ABC-DE");
            VerifyNonMultivalue("ABC-2DE");
            VerifyNonMultivalue("ABC-2~DE");
        }

        private static void VerifyValidMultivalue(string fieldName, string shortName, int multiValueIndex, int subValueIndex)
        {
            Assert.IsTrue(AttributeNameParser.IsMultiValue(fieldName));
            Assert.AreEqual(shortName, AttributeNameParser.GetShortFieldName(fieldName));
            Assert.AreEqual(multiValueIndex, AttributeNameParser.GetMultiValueIndex(fieldName));
            Assert.AreEqual(subValueIndex, AttributeNameParser.GetSubValueIndex(fieldName));
        }

        #endregion



        [Test]
        public void Test_AtributeNameParser_AAmultProperty()
        {
            string atributeName = "{BALANCE-1~1}.Test-1~1";

            string actualFieldName;
            int mv;
            int sv;
            ValidataCommon.AttributeNameParser.ParseComplexFieldName(atributeName, out actualFieldName, out mv, out sv);
        }
    }
}