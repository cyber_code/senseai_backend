using System;
using System.IO;
using System.Xml.Serialization;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Tests the validata converter
    /// </summary>
    [TestFixture]
    public class TestVersionDefinitionManager
    {
        private static readonly  string TempDir = Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"temp\SampleVersionDefinitions";

        /// <summary>
        /// Tests the creation and reading of versions
        /// </summary>
        [Test]
        public void CreateAndRead()
        {
            VersionDefinitionsManager manager = CreateVersionsAndInitializeManager();

            string[] globalCustomerVersions = manager.GetAllVersionNames("CUSTOMER");
            Assert.AreEqual(2, globalCustomerVersions.Length);
            Assert.AreEqual("CUST2", globalCustomerVersions[0]);
            Assert.AreEqual("CUST3", globalCustomerVersions[1]);

            string[] catalog1OnlyCustomerVersions = manager.GetAllVersionNames("Catalog1", "CUSTOMER", false);
            Assert.AreEqual(2, catalog1OnlyCustomerVersions.Length);
            Assert.AreEqual("CUST1", catalog1OnlyCustomerVersions[0]);
            Assert.AreEqual("CUST2", catalog1OnlyCustomerVersions[1]);

            string[] catalog2AndGlobalCustomerVersions = manager.GetAllVersionNames("Catalog2", "CUSTOMER", true);
            Assert.AreEqual(3, catalog2AndGlobalCustomerVersions.Length);
            Assert.AreEqual("CUST1", catalog2AndGlobalCustomerVersions[0]);
            Assert.AreEqual("CUST2", catalog2AndGlobalCustomerVersions[1]);
            Assert.AreEqual("CUST3", catalog2AndGlobalCustomerVersions[2]);

            Assert.AreEqual(0, manager.GetAllVersionNames("ACCOUNT").Length);
            Assert.AreEqual(1, manager.GetAllVersionNames("Catalog1", "ACCOUNT").Length);

            VersionDefinition catalog1cust2 = manager.GetVersion("Catalog1", "CUSTOMER", "CUST2");
            Assert.AreEqual(4, catalog1cust2.Fields.Count);

            VersionDefinition globalCust2 = manager.GetVersion("CUSTOMER", "CUST2");
            Assert.AreEqual(3, globalCust2.Fields.Count);
        }

        [Test]
        [Explicit]
        public void TestDeserializationLoading()
        {
            for (int i = 0; i < 30000; i ++)
            {
                new XmlSerializer(typeof (VersionDefinition));
            }
        }

        /// <summary>
        /// Tests the creation and reading of versions
        /// </summary>
        [Test]
        [Explicit]
        public void TestLoadingAllFromDefaultLocation()
        {
            VersionDefinitionsManager manager = new VersionDefinitionsManager();
            manager.ReadDefinitionFromConfigFiles(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\Assemblies\VersionDefinitions");
            Console.Write("Total version definitions = " + manager.TotalVersionsCount);
        }

        /// <summary>
        /// Tests the loading some versions from the default location
        /// </summary>
        [Test]
        [Explicit]
        public void TestVersionsForAA_ACTIVITY()
        {
            VersionDefinitionsManager manager = new VersionDefinitionsManager();
            string[] items = manager.GetAllVersionNames("R11 Model Bank", "AA.ACTIVITY");
            Console.Write("Versions count = " + items.Length);
        }

        private static VersionDefinitionsManager CreateVersionsAndInitializeManager()
        {
            if (Directory.Exists(TempDir))
            {
                Directory.Delete(TempDir, true);
            }

            Directory.CreateDirectory(TempDir);

            CreateFewSampleVersions();
            VersionDefinitionsManager manager = new VersionDefinitionsManager {AutomaticallyCheckForChanges = false};
            manager.ReadDefinitionFromConfigFiles(TempDir);
            return manager;
        }

        private static void CreateFewSampleVersions()
        {
            CreateAndSaveSampleVersion("Catalog2", "CUSTOMER", "CUST1", 2);
            CreateAndSaveSampleVersion("Catalog1", "CUSTOMER", "CUST1", 2);
            CreateAndSaveSampleVersion("Catalog1", "CUSTOMER", "CUST2", 4);
            CreateAndSaveSampleVersion("", "CUSTOMER", "CUST2", 3);
            CreateAndSaveSampleVersion("", "CUSTOMER", "CUST3", 1);

            CreateAndSaveSampleVersion("Catalog1", "ACCOUNT", "ACCT1", 2);
        }

        private static void CreateAndSaveSampleVersion(string catalog, string application, string version, int fieldsCount)
        {
            VersionDefinition def = CreateSampleVersion(catalog, application, version, fieldsCount);

            string fileName = Path.Combine(TempDir, def.SuggestedFileName);
            XmlSerializationHelper.SerializeToXmlFile(def, fileName);
        }

        private static VersionDefinition CreateSampleVersion(string catalog, string application, string version, int fieldsCount)
        {
            VersionDefinition def = new VersionDefinition
                                        {
                                            CatalogName = catalog,
                                            ApplicationName = application,
                                            VersionName = version,
                                        };

            for (int i = 0; i < fieldsCount; i++)
            {
                // TODO add some variety
                def.Fields.Add(new FieldInfo
                                   {
                                       FieldName = "Attr" + i,
                                       IsSynchronizable = true,
                                       DisplayName = "Displ" + i,
                                   });
            }
            return def;
        }

        [Test]
        public void Test_SplitVersionDefinitionFileName()
        {
            Test_SplitVersionDefinitionFileName("[R8AIX-Applications]FUNDS.TRANSFER,OTBK", "R8AIX-Applications", "FUNDS.TRANSFER", "OTBK");
            Test_SplitVersionDefinitionFileName("[ R8AIX-Applications   ]   FUNDS.TRANSFER  ,OTBK", "R8AIX-Applications", "FUNDS.TRANSFER", "OTBK");
            Test_SplitVersionDefinitionFileName("[R8AIX-Applications]FUNDS.TRANSFER", "R8AIX-Applications", "FUNDS.TRANSFER", "");
            Test_SplitVersionDefinitionFileName("[R8AIX-Applications  ]FUNDS.TRANSFER,", "R8AIX-Applications", "FUNDS.TRANSFER", "");
            Test_SplitVersionDefinitionFileName("DE.INTERFACE", "", "DE.INTERFACE", "");
            Test_SplitVersionDefinitionFileName("DE.FORMAT.XML,AUDIT", "", "DE.FORMAT.XML", "AUDIT");
            Test_SplitVersionDefinitionFileName("DE.FORMAT.XML  ,   AUDIT", "", "DE.FORMAT.XML", "AUDIT");
        }

        private void Test_SplitVersionDefinitionFileName(string fileNameWithoutExtension, string expectedCatalog, string expectedTypical, string expectedVersion)
        {
            string catalog, typical, version;
            VersionDefinitionsManager.SplitVersionDefinitionFileName(fileNameWithoutExtension, out catalog, out typical, out version);
            Assert.AreEqual(expectedCatalog, catalog);
            Assert.AreEqual(expectedTypical, typical);
            Assert.AreEqual(expectedVersion, version);
        }
    }
}