﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    [TestFixture]
    public class LocalTableRecord_Test
    {
        const string _testOFS = "DESCRIPTION:1:1=Some description,SHORT.NAME:1:1=MNEMONIC,MAXIMUM.CHAR:1:1=43,MINIMUM.CHAR:1:1=7,CHAR.TYPE:1:1=A,VETTING.TABLE:1:1=VETAFFFF,VETTING.TABLE:2:1=VETAFFXX,REMARK:1:1=Some remarks,REMARK:2:1=Some remar 2,APPLICATION:1:1=CUSTOMER,APPLICATION:2:1=ACCOUNT,DECIS.FIELD:1:1=@ID,DECIS.FIELD:1:2=@ID,DECIS.FIELD:2:1=@ID,DECISION:1:1=EQ,DECISION:1:2=EQ,DECISION:2:1=LE,DECISION.FR:1:1=1234,DECISION.FR:1:2=43321,DECISION.FR:2:1=123123,DECISION.TO:1:1=2222,CURR.NO:1:1=1,INPUTTER:1:1=235_AUTHORISER__OFS_BROWSERTC,DATE.TIME:1:1=1201110939,AUTHORISER:1:1=9574_INPUTTER_OFS_TELNET.OFS,CO.CODE:1:1=GB0010001,DEPT.CODE:1:1=1";

        private static _TestValidataRecord GetTestInstance()
        {
            var result = new _TestValidataRecord("R10", "LOCAL.TABLE");
            result.AttributeList = new List<IAttribute>();
            string[] fieldNameValuePairs = _testOFS.Split(new char[]{','});
            foreach(string fieldNameValuePair in fieldNameValuePairs)
            {
                var parts = fieldNameValuePair.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                string name = parts[0];
                string value = parts.Length>1 ? parts[1]:string.Empty;

                int posFirst = name.IndexOf(':');
                int posSecond = name.IndexOf(':', posFirst + 1);
                name = name.Substring(0, posFirst) + "-" + name.Substring(posFirst + 1, posSecond - posFirst - 1) + "~" + name.Substring(posSecond + 1);

                result.AddAttribute(name, value);
            }

            return result;
        }

        [Test]
        public void Test_Initialize()
        {
            var testInstance = GetTestInstance();

            LocalTableRecord localTableRecord = new LocalTableRecord(testInstance);

            // todo write assert tests for all of the fields
        }

        [Test]
        public void Test_Serialization()
        {
            var testInstance = GetTestInstance();
            LocalTableRecord localTableRecord = new LocalTableRecord(testInstance);

            string strXml = XmlSerializationHelper.SerializeToXml(localTableRecord);

            LocalTableRecord localTableRecord1 = (LocalTableRecord)XmlSerializationHelper.Deserialize(typeof(LocalTableRecord), strXml);

            // todo write assert tests for comparison of all fields
        }
    }
}
