﻿using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Test for domain names functions
    /// </summary>
    [TestFixture]
    public class DomainUsernamesHelperTest
    {
        /// <summary>
        /// Tests the valid names.
        /// </summary>
        [Test]
        public void TestValidNames()
        {
            Assert.IsTrue(DomainUsernamesHelper.LooksLikeValidName("domain\\user"));
            Assert.IsTrue(DomainUsernamesHelper.LooksLikeValidName("user@domain"));
        }

        /// <summary>
        /// Tests the invalid names.
        /// </summary>
        [Test]
        public void TestInvalidNames()
        {
            Assert.IsFalse(DomainUsernamesHelper.LooksLikeValidName(""));
            Assert.IsFalse(DomainUsernamesHelper.LooksLikeValidName("user"));

            Assert.IsFalse(DomainUsernamesHelper.LooksLikeValidName("domain\\"));
            Assert.IsFalse(DomainUsernamesHelper.LooksLikeValidName("\\domain"));
            Assert.IsFalse(DomainUsernamesHelper.LooksLikeValidName("domain\\user\\sth"));

            Assert.IsFalse(DomainUsernamesHelper.LooksLikeValidName("user@domain@gsg"));
            Assert.IsFalse(DomainUsernamesHelper.LooksLikeValidName("@domain"));
            Assert.IsFalse(DomainUsernamesHelper.LooksLikeValidName("@domain"));
        }

        /// <summary>
        /// Tests the comparison.
        /// </summary>
        [Test]
        public void TestComparison()
        {
            Assert.IsTrue(DomainUsernamesHelper.AreNamesEquivalent("domain\\user", "Domain\\USER"));
            Assert.IsTrue(DomainUsernamesHelper.AreNamesEquivalent("domain\\user", "Domain\\USER"));

            Assert.IsFalse(DomainUsernamesHelper.AreNamesEquivalent("domain\\user", "domain\\user2"));
            Assert.IsFalse(DomainUsernamesHelper.AreNamesEquivalent("domain\\user", "user"));
        }
    }
}