using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Test the identifier parsing
    /// </summary>
    [TestFixture]
    public class IdentifierParserTest
    {
        /// <summary>
        /// Tests the empty.
        /// </summary>
        [Test]
        public void TestEmpty()
        {
            IdentifierParser parser = new IdentifierParser("");
            Assert.AreEqual("", parser.Prefix);
            Assert.AreEqual(0, parser.NumericPart);
            Assert.AreEqual("", parser.Postfix);
        }

        /// <summary>
        /// Tests the numeric.
        /// </summary>
        [Test]
        public void TestNumeric()
        {
            IdentifierParser parser = new IdentifierParser("22");
            Assert.AreEqual("", parser.Prefix);
            Assert.AreEqual(22, parser.NumericPart);
            Assert.AreEqual("", parser.Postfix);
        }

        /// <summary>
        /// Tests the not numeric.
        /// </summary>
        [Test]
        public void TestNotNumeric()
        {
            IdentifierParser parser = new IdentifierParser("ala");
            Assert.AreEqual("ala", parser.Prefix);
            Assert.AreEqual(0, parser.NumericPart);
            Assert.AreEqual("", parser.Postfix);
        }

        /// <summary>
        /// Tests the numeric with postfix.
        /// </summary>
        [Test]
        public void TestNumericWithPostfix()
        {
            IdentifierParser parser = new IdentifierParser("22ala");
            Assert.AreEqual("", parser.Prefix);
            Assert.AreEqual(22, parser.NumericPart);
            Assert.AreEqual("ala", parser.Postfix);
        }

        /// <summary>
        /// Tests the numeric with prefix.
        /// </summary>
        [Test]
        public void TestNumericWithPrefix()
        {
            IdentifierParser parser = new IdentifierParser("ala22");
            Assert.AreEqual("ala", parser.Prefix);
            Assert.AreEqual(22, parser.NumericPart);
            Assert.AreEqual("", parser.Postfix);
        }

        /// <summary>
        /// Tests the numeric with prefix and postfix.
        /// </summary>
        [Test]
        public void TestNumericWithPrefixAndPostfix()
        {
            IdentifierParser parser = new IdentifierParser("ala22bala");
            Assert.AreEqual("ala", parser.Prefix);
            Assert.AreEqual(22, parser.NumericPart);
            Assert.AreEqual("bala", parser.Postfix);
        }
    }
}