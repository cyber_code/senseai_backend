﻿using System;
using System.Linq;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Testing of users pools
    /// </summary>
    [TestFixture]
    public class UserPoolTest
    {
        /// <summary>
        /// Tests the correct values.
        /// </summary>
        [Test]
        public void TestValidNameGeneration()
        {
            UsersPool.UsersPool pool = new UsersPool.UsersPool();
            pool.Prefix = "U";
            pool.Min = 2;
            pool.Max = 3;

            Assert.AreEqual("U2", pool.GetUserName(0));
            Assert.AreEqual("U3", pool.GetUserName(1));

            pool.UserNameLength = 4;
            Assert.AreEqual("U002", pool.GetUserName(0));
            Assert.AreEqual("U003", pool.GetUserName(1));
        }

        /// <summary>
        /// Tests the pool overflow.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void TestPoolOverflow()
        {
            UsersPool.UsersPool pool = new UsersPool.UsersPool();
            pool.Prefix = "U";
            pool.Min = 2;
            pool.Max = 3;

            pool.GetUserName(2);
        }

        /// <summary>
        /// Tests MaxWithoutAdditional
        /// </summary>
        [Test]
        public void TestPoolMaxWithoutAdditional()
        {
            var pool = new UsersPool.UsersPool
            {
                Prefix = "U",
                Min = 0,
                Max = 1,
                MaxWithoutAdditional = 1
            };

            Assert.AreEqual(0, pool.GetSpareUserNames().Count());

            pool.MaxWithoutAdditional = 0;
            Assert.AreEqual(1, pool.GetSpareUserNames().Count());
            Assert.AreEqual("U1", pool.GetSpareUserNames().First());

            pool.Min = 6;
            pool.Max = 16;
            pool.MaxWithoutAdditional = 16;
            Assert.AreEqual(0, pool.GetSpareUserNames().Count());

            pool.MaxWithoutAdditional = 14;
            Assert.AreEqual(2, pool.GetSpareUserNames().Count());
            Assert.AreEqual("U15", pool.GetSpareUserNames().First());
            Assert.AreEqual("U16", pool.GetSpareUserNames().Last());
        }
    }
}