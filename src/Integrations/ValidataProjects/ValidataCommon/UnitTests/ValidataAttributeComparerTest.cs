using System;
using NUnit.Framework;

namespace ValidataCommon.UnitTests
{
    /// <summary>
    /// Test for comparing attribute values
    /// </summary>
    [TestFixture]
    public class ValidataAttributeComparerTest
    {
        /// <summary>
        /// Tests the string comparison.
        /// </summary>
        [Test]
        public void TestStringComparison()
        {
            Assert.AreEqual(0, ValidataAttributeComparer.CompareStringAttributeValues("2", "2"));
            Assert.AreEqual(-1, ValidataAttributeComparer.CompareStringAttributeValues("11", "2"));
            Assert.AreEqual(1, ValidataAttributeComparer.CompareStringAttributeValues("2", "11"));

            Assert.AreEqual(0, ValidataAttributeComparer.CompareStringAttributeValues("a", "A"));
        }

        /// <summary>
        /// Tests the integer comparison.
        /// </summary>
        [Test]
        public void TestIntegerComparison()
        {
            Assert.AreEqual(0, ValidataAttributeComparer.CompareIntegerAttributeValues("2", "2"));
            Assert.AreEqual(1, ValidataAttributeComparer.CompareIntegerAttributeValues("11", "2"));
            Assert.AreEqual(-1, ValidataAttributeComparer.CompareIntegerAttributeValues("2", "11"));

            Assert.AreEqual(0, ValidataAttributeComparer.CompareIntegerAttributeValues(
                                   "not a valid integer", ValidataConverter.DEFAULT_INT.ToString()));
        }

        /// <summary>
        /// Tests the double comparison.
        /// </summary>
        [Test]
        public void TestDoubleComparison()
        {
            Assert.AreEqual(0, ValidataAttributeComparer.CompareDoubleAttributeValues("2.2", "2.200"));
            Assert.AreEqual(1, ValidataAttributeComparer.CompareDoubleAttributeValues("11.11", "2.2"));
            Assert.AreEqual(-1, ValidataAttributeComparer.CompareDoubleAttributeValues("2.2", "11"));

            Assert.AreEqual(0, ValidataAttributeComparer.CompareDoubleAttributeValues(
                                   "not a valid double", ValidataConverter.DEFAULT_REAL.ToString()));
        }

        /// <summary>
        /// Tests the decimal comparison.
        /// </summary>
        [Test]
        public void TestDecimalComparison()
        {
            Assert.AreEqual(0, ValidataAttributeComparer.CompareDecimalAttributeValues("2.2", "2.200"));
            Assert.AreEqual(1, ValidataAttributeComparer.CompareDecimalAttributeValues("11.11", "2.2"));
            Assert.AreEqual(-1, ValidataAttributeComparer.CompareDecimalAttributeValues("2.2", "11"));

            Assert.AreEqual(0, ValidataAttributeComparer.CompareDecimalAttributeValues(
                                   "not a valid decimal", ValidataConverter.DEFAULT_DECIMAL.ToString()));
        }

        /// <summary>
        /// Tests the string comparison.
        /// </summary>
        [Test]
        public void TestDatesComparison()
        {
            DateTime past = new DateTime(2007, 4, 20);
            DateTime future = past.AddMinutes(1);

            string pastTicks = past.Ticks.ToString();
            string futureTicks = future.Ticks.ToString();

            Assert.AreEqual(0, ValidataAttributeComparer.CompareDateTimeAttributeValues(pastTicks, pastTicks));
            Assert.AreEqual(1, ValidataAttributeComparer.CompareDecimalAttributeValues(futureTicks, pastTicks));
            Assert.AreEqual(-1, ValidataAttributeComparer.CompareDecimalAttributeValues(pastTicks, futureTicks));

            Assert.AreEqual(0, ValidataAttributeComparer.CompareDecimalAttributeValues(
                                   "not a valid date", DateTime.MinValue.Ticks.ToString()));
        }
    }
}