﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ValidataCommon
{
    /// <summary>
    /// T24 Field class
    /// </summary>
    [Serializable]
    public class T24Field
    {
        /// <summary>
        /// Get/Set Field Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Get/Set Field value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// To String
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} = {1}", Name, Value);
        }
    }

    /// <summary>
    /// Collection of T24 record fields
    /// </summary>
    [Serializable]
    public class T24RecordFieldCollection : List<T24Field>
    {
        /// <summary>
        /// Append new field to the collection
        /// </summary>
        /// <param name="name">Field name</param>
        /// <param name="value">Field value</param>
        public void Add(string name, string value)
        {
            Add(new T24Field {Name = name, Value = value});
        }

        internal string GetValue(string fieldName)
        {
            // TODO - may be it is good to implement dictionary here!!!
            //      currently this method is used only for Unit Test so...
            foreach (var field in this)
            {
                if (field.Name == fieldName)
                {
                    return field.Value;
                }
            }

            return null;
        }
    }

    /// <summary>
    /// Representation of T24 record
    /// </summary>
    [Serializable]
    public class T24Record
    {
        /// <summary>
        /// Application name
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Attribute collection
        /// </summary>
        [XmlElement(ElementName = "Field")]
        public T24RecordFieldCollection Fields { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public T24Record()
        {
            Fields = new T24RecordFieldCollection();
        }

        /// <summary>
        /// Constructs instance
        /// </summary>
        /// <param name="applicationName">T24 Application Name</param>
        public T24Record(string applicationName)
            : this()
        {
            ApplicationName = applicationName;
        }
    }

    /// <summary>
    /// List of T24Records
    /// </summary>
    [Serializable]
    [XmlRoot("ArrayOfT24Record")]
    public class T24RecordsCollection
    {
        /// <summary>
        /// Used communication method for extraction
        /// </summary>
        [XmlAttribute("communicationMethod")]
        public string CommunicationMethod = string.Empty;

        /// <summary>
        /// Used configuration set for extraction
        /// </summary>
        [XmlAttribute("configurationSet")]
        public string ConfigurationSet = string.Empty;

        /// <summary>
        /// Date time when records are extracted
        /// </summary>
        [XmlAttribute("dateExtracted")]
        public DateTime DateExtracted = DateTime.Now;

        /// <summary>
        /// Records
        /// </summary>
        [XmlElement("T24Record")]
        public List<T24Record> Records = new List<T24Record>();

        /// <summary>
        /// Count of recrod in the collection
        /// </summary>
        [XmlIgnore]
        public int Count
        {
            get { return Records.Count; }
        }

        /// <summary>
        /// Catalog name for T24 records
        /// </summary>
        [XmlIgnore]
        public string CatalogName
        {
            get;
            set;
        }

        /// <summary>
        /// Application name for T24 records
        /// </summary>
        [XmlIgnore]
        public string ApplicationName
        {
            get;
            set;
        }

        /// <summary>
        /// Index by @ID
        /// </summary>
        private Dictionary<string, T24Record> _IndexByT24ID;

        /// <summary>
        /// Add new record to the collection
        /// </summary>
        /// <param name="record">T24 record</param>
        public void Add(T24Record record)
        {
            this.Records.Add(record);
        }

        /// <summary>
        /// Find record by @ID
        /// </summary>
        /// <param name="t24Id"></param>
        /// <returns></returns>
        public T24Record FindRecordByT24ID(string t24Id)
        {
            if (_IndexByT24ID == null)
            {
                throw new ApplicationException("Index by @ID is not initialized!");
            }

            return _IndexByT24ID.ContainsKey(t24Id) ? _IndexByT24ID[t24Id] : null;
        }

        /// <summary>
        /// Deserializes T24RecordsCollection from xml file
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="errorMessage">Error message if error occurs</param>
        /// <returns>Initialized T24RecordsCollection or null if error occurs</returns>
        public static T24RecordsCollection LoadFromXmlFile(string fileName, out string errorMessage)
        {
            errorMessage = null;
            try
            {
                var result =  (T24RecordsCollection) XmlSerializationHelper.DeserializeFromXmlFile(typeof (T24RecordsCollection), fileName);
                result.BuildRecordsIndexByT24ID();
                return result;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return null;
            }
        }

        /// <summary>
        /// Serializes collection to xml file 
        /// </summary>
        /// <param name="collection">T24RecordsCollection</param>
        /// <param name="fileName">File Name</param>
        /// <param name="errorMessage">Error message if error occurs</param>
        /// <returns>Success/Fail</returns>
        public static bool SaveToXmlFile(T24RecordsCollection collection, string fileName, out string errorMessage)
        {
            errorMessage = null;
            try
            {
                XmlSerializationHelper.SerializeToXmlFile(collection, fileName);
                return true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return false;
            }
        }

        #region Private Methods

        /// <summary>
        /// Create index (Dictionary) by @ID
        /// </summary>
        private void BuildRecordsIndexByT24ID()
        {
            _IndexByT24ID = new Dictionary<string, T24Record>();
            foreach (var record in Records)
            {
                var t24IdValue = record.Fields.GetValue("@ID");
                if (string.IsNullOrEmpty(t24IdValue) || _IndexByT24ID.ContainsKey(t24IdValue))
                {
                    System.Diagnostics.Debug.Fail("How it is possible to have null or duplicate @ID?");
                    continue;
                }

                _IndexByT24ID[t24IdValue] = record;
            }
        }

        #endregion
    }
}
