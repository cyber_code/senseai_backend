using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace ValidataCommon
{
    /// <summary>
    /// Definition of T24 Version
    /// </summary>
    [Serializable]
    public class VersionDefinition
    {
        /// <summary>
        /// The name of the catalog. If it is not specified, the version applies to all catalogs
        /// </summary>
        public string CatalogName = "";

        /// <summary>
        /// The name of the application
        /// </summary>
        public string ApplicationName = "";

        /// <summary>
        /// The name of the version (without the comma)
        /// </summary>
        public string VersionName = "";

        /// <summary>
        /// Description of the version
        /// </summary>
        public string Description = "";

        /// <summary>
        /// Main language code of the version (LANGUAGE.CODE-1~1)
        /// </summary>
        public string LanguageCode = "";

        /// <summary>
        /// Other languages (LANGUAGE.CODE-X~1)
        /// </summary>
        public List<string> OtherLanguages = new List<string>();

        /// <summary>
        /// Is Core Application (e.g. created from ss only)
        /// </summary>
        public bool IsCoreApp { get; set; }

        /// <summary>
        /// Is AA typical version
        /// </summary>
        public bool IsAA { get; set; }

        /// <summary>
        /// Gets the full name (application + version)
        /// </summary>
        /// <value>The full name.</value>
        public string FullName
        {
            get
            {
                return IsCoreApp
                           ? (ApplicationName ?? "")
                           : string.Format("{0},{1}", ApplicationName ?? "", VersionName ?? "");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is catalog bound.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is catalog bound; otherwise, <c>false</c>.
        /// </value>
        public bool IsCatalogBound
        {
            get { return !string.IsNullOrEmpty(CatalogName); }
        }

        /// <summary>
        /// Gets the suggested name of the file where the version should be saved
        /// </summary>
        /// <value>The default name of the file.</value>
        public string SuggestedFileName
        {
            get
            {
                var suggestName = IsCatalogBound
                           ? string.Format("[{0}]{1}.xml", CatalogName ?? "", FullName)
                           : string.Format("{0}.xml", FullName);

                // it is possible that the FullName ends with ','
                // in that case the filepath is not as suggeted above

                return suggestName.Replace(",.xml", ".xml");

            }
        }

        /// <summary>
        /// Version definition source file modified date
        /// </summary>
        [XmlIgnore]
        public DateTime FileModifiedDate { get; set; }

        [NonSerialized]
        private string _SourceFilePath;
        /// <summary>
        /// Version definition source file path
        /// </summary>
        [XmlIgnore]
        public string SourceFilePath
        {
            get
            {
                if (_SourceFilePath == null)
                    _SourceFilePath = Path.Combine(VersionDefinitionsManager.DefaultVersionDefinitionsPath, SuggestedFileName);

                return _SourceFilePath;
            }
            set { _SourceFilePath = value; }
        }

        /// <summary>
        /// Determine if version definition source file has been modified
        /// </summary>
        [XmlIgnore]
        public bool IsSourceFileModified
        {
            get { return FileModifiedDate != File.GetLastWriteTime(SourceFilePath); }
        }

        /// <summary>
        /// The fields participating in the version
        /// </summary>
        public FieldInfoList Fields = new FieldInfoList();

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string name = "";
            if (! string.IsNullOrEmpty(CatalogName))
            {
                name += "[" + CatalogName + "]:";
            }

            name += FullName + " " + Fields.Count;

            return name;
        }

        /// <summary>
        /// Parses and assigns Application and Version names from given full version name
        /// </summary>
        /// <param name="fullName">Full version name</param>
        public void InitializeFromFullVersionName(string fullName)
        {
            int posOfComma = fullName.IndexOf(',');
            if (posOfComma < 0)
            {
                ApplicationName = fullName;
                IsCoreApp = true;
                return;
            }

            ApplicationName = fullName.Substring(0, posOfComma);
            VersionName = fullName.Substring(posOfComma + 1);
        }

        /// <summary>
        /// Gets a value indicating whether this instance is default version.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is default version; otherwise, <c>false</c>.
        /// </value>
        public bool IsCommaVersion
        {
            get { return !IsCoreApp && string.IsNullOrEmpty(VersionName); }
        }

        /// <summary>
        /// Add fields
        /// </summary>
        /// <param name="fieldInfos"></param>
        public void AddFieldsRange(IEnumerable<FieldInfo> fieldInfos)
        {
            foreach (FieldInfo fi in fieldInfos)
            {
                AddField(fi);
            }
        }

        /// <summary>
        /// Add field
        /// </summary>
        /// <param name="field"></param>
        public void AddField(FieldInfo field)
        {
            Debug.Assert(field.FieldName != "*");

            FieldInfo existing = Fields.Find(field.FieldName, field.MVIndex, field.SVIndex, field.AssociatedVersion);
            if (existing == null)
            {
                Fields.Add(field);
            }
        }

        public bool IsEqualTo(VersionDefinition other)
        {
            return CatalogName == other.CatalogName
                && ApplicationName == other.ApplicationName
                && VersionName ==other.VersionName
                && Description ==other.Description
                && LanguageCode == other.LanguageCode
                && OtherLanguages.IsEqualTo(other.OtherLanguages)
                && Fields.AreEqualTo(other.Fields);
        }

        public VersionDefinition Clone()
        {
            string xml = XmlSerializationHelper.SerializeToXml(this);
            return XmlSerializationHelper.FromXML<VersionDefinition>(xml);
        }
    }
}