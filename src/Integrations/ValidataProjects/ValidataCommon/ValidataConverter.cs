using System;
using System.Globalization;
using System.Text;
using System.Xml;
using Validata.Common;

namespace ValidataCommon
{
    /// <summary>
    /// Options for parsing dates
    /// </summary>
    [Flags]
    public enum DateParsingOptions
    {
        /// <summary>
        /// T24 Date Format
        /// </summary>
        yyyyMMdd = 1,

        /// <summary>
        /// From DateTime Ticks
        /// </summary>
        Ticks = 2,

        /// <summary>
        /// InvariantCulture - DD/MM/YYYY HH:mm:ss 
        /// </summary>
        Invariant = 4,

        /// <summary>
        /// CurrentCulture
        /// </summary>
        CurrentCulture = 8,

        /// <summary>
        /// en-US Culture Info - MM/DD/YYYY HH:mm:ss 
        /// </summary>
        enUs = 16,

        /// <summary>
        /// All possible formats
        /// </summary>
        All = yyyyMMdd | Ticks | Invariant | CurrentCulture | enUs
    }

    /// <summary>
    /// ValidataConverter
    /// </summary>
    public static class ValidataConverter
    {
        #region Public Constants

        /// <summary>
        /// No Value constant
        /// </summary>
        public const string NO_VALUE = "No Value";

        /// <summary>
        /// Default Integer Value
        /// </summary>
        public const int DEFAULT_INT = 0;

        /// <summary>
        /// Default ULong Value
        /// </summary>
        public const ulong DEFAULT_ULONG = 0;

        /// <summary>
        /// Default Decimal Value
        /// </summary>
        public const decimal DEFAULT_DECIMAL = 0;

        /// <summary>
        /// Default Real Value
        /// </summary>
        public const double DEFAULT_REAL = 0;

        /// <summary>
        /// Default Boolean Value
        /// </summary>
        public const bool DEFAULT_BOOLEAN = false;

        /// <summary>
        /// Default DateTime Value
        /// </summary>
        public static readonly DateTime DEFAULT_DATE = DateTime.MinValue;

        /// <summary>
        /// Gets the invariant numeric format 
        /// </summary>
        /// <returns></returns>
        public static IFormatProvider NumericFormatInfo
        {
            get { return CultureInfo.InvariantCulture.NumberFormat; }
        }

        #endregion 

        #region Public Methods

        /// <summary>
        /// Retrieves Integer value from string
        /// </summary>
        /// <param name="val">Value</param>
        /// <returns>Integer value</returns>
        public static int GetInt(string val)
        {
            if (HasAnyData(val))
            {
                int i;
                if (int.TryParse(val, out i))
                {
                    return i;
                }
                else
                {
                    return DEFAULT_INT;
                }
            }
            else
            {
                return DEFAULT_INT;
            }
        }

        /// <summary>
        /// Retrieves ULong value from string
        /// </summary>
        /// <param name="val">Value</param>
        /// <returns>ULong value</returns>
        public static ulong GetULong(string val)
        {
            if (HasAnyData(val))
            {
                ulong u;
                if (ulong.TryParse(val, out u))
                {
                    return u;
                }
                else
                {
                    return DEFAULT_ULONG;
                }
            }
            else
            {
                return DEFAULT_ULONG;
            }
        }

        /// <summary>
        /// Retrieves Decimal value from string
        /// </summary>
        /// <param name="val">Value</param>
        /// <returns>ULong value</returns>
        public static decimal GetDecimal(string val)
        {
            if (HasAnyData(val))
            {
                decimal dec;
                if (
                    decimal.TryParse(val.Replace(",", "."), NumberStyles.Currency, CultureInfo.InvariantCulture, out dec))
                {
                    return dec;
                }

                if (decimal.TryParse(val, out dec))
                {
                    return dec;
                }
            }

            return DEFAULT_DECIMAL;
        }

        /// <summary>
        /// Retrieves a double value from a string
        /// </summary>
        /// <param name="val">Value</param>
        /// <returns>ULong value</returns>
        public static double GetDouble(string val)
        {
            if (HasAnyData(val))
            {
                double dbl;
                if (TryGetDouble(val, out dbl))
                    return dbl;
            }

            return DEFAULT_REAL;
        }


        /// <summary>
        /// Tries the get decimal value.
        /// </summary>
        /// <param name="val">The value as string.</param>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        public static bool TryGetDecimal(string val, out decimal result)
        {
            if (val == null)
            {
                result = decimal.Zero;
                return false;
            }

            string normlizedValue = NormalizeDecimalPointInReal(val);

            // expect the number to be parsable in invariant culture 
            if (decimal.TryParse(normlizedValue, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                return true;
            }

            // but as a last resort, try to use the current culture
            if (decimal.TryParse(val, out result))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Tries the get double value.
        /// </summary>
        /// <param name="val">The value as string.</param>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        public static bool TryGetDouble(string val, out double result)
        {
            string normlizedValue = NormalizeDecimalPointInReal(val);

            // expect the number to be parsable in invariant culture 
            if (double.TryParse(normlizedValue, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                return true;
            }

            // but as a last resort, try to use the current culture
            if (double.TryParse(val, out result))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Normalize Decimal Point In Real Number
        /// </summary>
        /// <param name="strReal">Value</param>
        /// <returns>String value</returns>
        public static string NormalizeDecimalPointInReal(string strReal)
        {
            string[] pointSep = strReal.Split('.');

            if (pointSep.Length == 2)
                return strReal; // there is a point -> it should be the decimal point

            if (pointSep.Length > 2)
                return ""; // shouldn't have more than one point -> invalid

            string[] commaSep = strReal.Split(',');
            if (commaSep.Length == 1)
                return strReal; // no commas found -> no need to convert

            if (commaSep.Length == 2 && commaSep[1].Length != 3)
            {
                // the comma is obviously not a thousands separator, but a decimal one -> we can replace it with a point
                return strReal.Replace(",", ".");
            }

            return strReal; // shouldn't really reach here
        }

        /// <summary>
        /// Retrieves Boolean value from string
        /// </summary>
        /// <param name="val">Value</param>
        /// <returns>Boolean value</returns>
        public static bool GetBoolean(string val)
        {
            if (HasAnyData(val))
            {
                if (val == "1")
                {
                    return true;
                }
                else if (val.ToLower() == "t")
                {
                    return true;
                }
                else if (val.ToLower() == "true")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return DEFAULT_BOOLEAN;
            }
        }

        /// <summary>
        /// Determines whether the specified string value has content.
        /// </summary>
        /// <param name="val">The val.</param>
        /// <returns>
        /// 	<c>true</c> if the specified val has content; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasAnyData(string val)
        {
            return (val != null && val != NO_VALUE && val.Trim().Length != 0);
        }

        #region Date parsing

        /// <summary>
        /// Retrieves DateTime object from string, trying all avaiable strategies
        /// </summary>
        /// <param name="val">Value</param>
        /// <returns>DateTime object on sucessful conversion, otherwise returns DateTime.MinDate</returns>
        public static DateTime GetDateTime(string val)
        {
            return ParseDateTime(val, DateParsingOptions.All);
        }

        /// <summary>
        /// Tries the get date time from ticks.
        /// </summary>
        /// <param name="dtValue">The dt value.</param>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public static bool TryGetDateTimeFromTicks(string dtValue, out DateTime dateTime)
        {
            long ticks;
            if (long.TryParse(dtValue, out ticks))
            {
                dateTime = new DateTime(ticks);
                return true;
            }
            else
            {
                dateTime = DEFAULT_DATE;
                return false;
            }
        }

        /// <summary>
        /// Retrieves DateTime object from string
        /// </summary>
        /// <param name="val">The string representation of date</param>
        /// <param name="options">Options how to try to convert the value to a date time</param>
        /// <returns>DateTime object on sucessful conversion, otherwise returns DateTime.MinDate</returns>
        public static DateTime ParseDateTime(string val, DateParsingOptions options)
        {
            DateTime dt;
            TryParseDateTime(val, options, out dt);
            return dt;
        }

        /// <summary>
        /// Tries to parse the date time, using various strategies
        /// </summary>
        /// <param name="val">The string representation of date.</param>
        /// <param name="parsingOptions">The parsing options.</param>
        /// <param name="dt">The date time.</param>
        /// <returns></returns>
        public static bool TryParseDateTime(string val, DateParsingOptions parsingOptions, out DateTime dt)
        {
            dt = DEFAULT_DATE;

            if (val == null || val.Trim().Length == 0 || val == NO_VALUE)
            {
                return false;
            }

            if ((parsingOptions & DateParsingOptions.yyyyMMdd) > 0)
            {
                if (DateTime.TryParseExact(val, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dt))
                {
                    return true;
                }
            }

            if ((parsingOptions & DateParsingOptions.Ticks) > 0)
            {
                if (TryGetDateTimeFromTicks(val, out dt))
                {
                    return true;
                }
            }

            if ((parsingOptions & DateParsingOptions.Invariant) > 0)
            {
                if (DateTime.TryParse(val, CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dt))
                {
                    return true;
                }
            }

            if ((parsingOptions & DateParsingOptions.CurrentCulture) > 0)
            {
                if (DateTime.TryParse(val, out dt))
                {
                    return true;
                }
            }

            if ((parsingOptions & DateParsingOptions.enUs) > 0)
            {
                if (DateTime.TryParse(val, new CultureInfo("en-US", true), DateTimeStyles.NoCurrentDateDefault, out dt))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        /// <summary>
        /// Transforms the string value to validata format.
        /// </summary>
        /// <param name="attributeValue">The attribute value.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static object TransformStringValueToValidataFormat(string attributeValue, AttributeDataType type)
        {
            switch (type)
            {
                case AttributeDataType.Measure:
                    return attributeValue;
                case AttributeDataType.String:
                    return attributeValue;
                case AttributeDataType.Integer:
                    return GetInt(attributeValue);
                case AttributeDataType.Real:
                    return GetDouble(attributeValue);
                case AttributeDataType.Boolean:
                    return GetBoolean(attributeValue);
                case AttributeDataType.DateTime:
                    return GetDateTime(attributeValue).Ticks.ToString(NumericFormatInfo);
                        // TODO - that'a a bit weird, but that's how we store it in DB
                case AttributeDataType.Currency:
                    return GetDecimal(attributeValue);
                case AttributeDataType.Blob:
                    return DecodeBlobValueFromString(attributeValue);
                default:
                    return attributeValue;
            }
        }

        /// <summary>
        /// Gets the BLOB.
        /// </summary>
        /// <param name="encodedBlobValue">The encoded BLOB value.</param>
        /// <returns></returns>
        public static byte[] DecodeBlobValueFromString(string encodedBlobValue)
        {
            return Encoding.UTF8.GetBytes(XmlConvert.DecodeName(encodedBlobValue));
        }

        /// <summary>
        /// Transforms the string value in a format suitable for storage in Validata DB.
        /// </summary>
        /// <param name="attributeValue">The attribute value.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static string GetStringValueForDB(string attributeValue, AttributeDataType type)
        {
            switch (type)
            {
                case AttributeDataType.Blob:
                    return string.Empty;
                case AttributeDataType.Measure:
                    return attributeValue;
                case AttributeDataType.String:
                    return attributeValue != "" ? attributeValue : NO_VALUE;
                case AttributeDataType.Integer:
                    return GetInt(attributeValue).ToString(NumericFormatInfo);
                case AttributeDataType.Real:
                    return GetDouble(attributeValue).ToString(NumericFormatInfo);
                case AttributeDataType.Boolean:
                    return GetBoolean(attributeValue).ToString(NumericFormatInfo);//TODO Isn't it better to use 1/0
                case AttributeDataType.DateTime:
                    return GetDateTime(attributeValue).Ticks.ToString(NumericFormatInfo);// TODO - that'a a bit weird, but that's how we store it in DB
                case AttributeDataType.Currency:
                    return GetDecimal(attributeValue).ToString(NumericFormatInfo);
                default:
                    return attributeValue;
            }
        }

        /// <summary>
        /// Gets the string value for display.
        /// </summary>
        /// <param name="attributeValue">The attribute value.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
         public static string GetStringValueForDisplay(string attributeValue, AttributeDataType type)
         {
             switch (type)
             {
                 case AttributeDataType.Blob:
                     return "[Binary Large Object]";
                 case AttributeDataType.Measure:
                     return attributeValue;
                 case AttributeDataType.String:
                     return attributeValue;
                 case AttributeDataType.Integer:
                     return GetInt(attributeValue).ToString(NumericFormatInfo);
                 case AttributeDataType.Real:
                     return GetDouble(attributeValue).ToString(NumericFormatInfo);
                 case AttributeDataType.Boolean:
                     return GetBoolean(attributeValue).ToString(NumericFormatInfo);
                 case AttributeDataType.DateTime:
                     DateTime dt = GetDateTime(attributeValue);
                     return dt == DEFAULT_DATE ? "" : dt.ToString("dd/MM/yyyy HH:mm:ss");
                 case AttributeDataType.Currency:
                     return GetDecimal(attributeValue).ToString(NumericFormatInfo);
                 default:
                     return attributeValue;
             }
         }

        /// <summary>
        /// Gets the default value for a given type as string
        /// </summary>
        /// <param name="dataType"> Data type</param>
        /// <returns> Default value for passed data type </returns>
        public static string GetDefaultValueAsString(AttributeDataType dataType)
        {
            switch (dataType)
            {
                case AttributeDataType.String:
                    return "No Value";
                case AttributeDataType.Integer:
                    return "0";
                case AttributeDataType.Real:
                case AttributeDataType.Currency:
                    return "0.0";
                case AttributeDataType.Boolean:
                    return "False";
                case AttributeDataType.DateTime:
                    return "0";
                case AttributeDataType.Blob:
                    return string.Empty;
                case AttributeDataType.Measure:
                    return "0";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Ares the values equivalent.
        /// </summary>
        /// <param name="val1">The val1.</param>
        /// <param name="val2">The val2.</param>
        /// <param name="dataType">Type of the data.</param>
        /// <returns></returns>
        public static bool AreValuesEquivalent(string val1, string val2, AttributeDataType dataType)
        {
            return GetStringValueForDB(val1, dataType) == GetStringValueForDB(val2, dataType);
        }

        #endregion 
    }
}