﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ValidataCommon.Interfaces;

namespace ValidataCommon
{
    public class VersionFieldValidator
    {
        public readonly string Catalog;
        public readonly string Typical;
        public readonly string Version;
        public readonly VersionDefinition VersionDefinition;
        public readonly VersionDefinition StandardSelection;
        private readonly Dictionary<string, string> _ValidationRulesStrings = new Dictionary<string, string>();
        private readonly Dictionary<string, FieldValueValidator> _FieldValidators = new Dictionary<string, FieldValueValidator>();
        private readonly IIN2RoutineExecutor _IN2RoutineExecutor;

        private bool IsAA
        {
            get
            {
                if (VersionDefinition != null)
                    return VersionDefinition.IsAA;
                if (StandardSelection != null)
                    return StandardSelection.IsAA;
                return false;
            }
        }

        public VersionFieldValidator(string versionFileName, IIN2RoutineExecutor iN2RoutineExecutor)
        {
            if (versionFileName.EndsWith(".xml", StringComparison.OrdinalIgnoreCase))
                versionFileName = versionFileName.Substring(0, versionFileName.Length - ".xml".Length);
            VersionDefinitionsManager.SplitVersionDefinitionFileName(versionFileName, out Catalog, out Typical, out Version);

            VersionDefinition = VersionDefinitionsManager.LoadVersionFromFile(Catalog, Typical, Version);
            StandardSelection = string.IsNullOrEmpty(Version)
                ? VersionDefinition
                : VersionDefinitionsManager.LoadVersionFromFile(Catalog, Typical, "");
            _IN2RoutineExecutor = iN2RoutineExecutor;
        }

        public string GetValidationRulesString(string fieldName)
        {
            if (!_ValidationRulesStrings.ContainsKey(fieldName))
                _ValidationRulesStrings[fieldName] = GetFieldValueValidator(fieldName).ToValidationRulesString();

            return _ValidationRulesStrings[fieldName];
        }

        public string Validate(string targetAttribute, string value)
        {
            return GetFieldValueValidator(targetAttribute).Validate(value);
        }

        private FieldValueValidator GetFieldValueValidator(string attributeName)
        {
            if (!_FieldValidators.ContainsKey(attributeName))
            {
                string shortFieldName;
                int mvIndex, svIndex;

                if (IsAA)
                {
                    AttributeNameParsingResult fieldNameParseRes = AAAttributeName.ParseComplexFieldName(attributeName);
                    shortFieldName = string.IsNullOrEmpty(fieldNameParseRes.PropertyShortName)
                        ? fieldNameParseRes.ShortFieldName
                        : AAAttributeName.GetAttributeName(fieldNameParseRes.PropertyShortName, fieldNameParseRes.ShortFieldName);
                    mvIndex = fieldNameParseRes.MultiValueIndex;
                    svIndex = fieldNameParseRes.SubValueIndex;
                }
                else
                {
                    AttributeNameParsingResult fieldNameParseRes = AttributeNameParser.ParseComplexFieldName(attributeName);
                    shortFieldName = fieldNameParseRes.ShortFieldName;
                    mvIndex = fieldNameParseRes.MultiValueIndex;
                    svIndex = fieldNameParseRes.SubValueIndex;
                }

                var field = VersionDefinition.Fields.FindByName(shortFieldName);
                var ssField = StandardSelection.Fields.FindByName(shortFieldName);

                _FieldValidators[attributeName] = new FieldValueValidator(field, ssField, attributeName, shortFieldName, mvIndex, svIndex, _IN2RoutineExecutor);
            }

            return _FieldValidators[attributeName];
        }
    }
}
