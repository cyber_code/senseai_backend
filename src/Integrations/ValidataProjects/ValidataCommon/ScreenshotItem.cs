﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.IO;

namespace ValidataCommon
{
    /// <summary>
    /// Screenshot Item
    /// </summary>
    [Serializable]
    public class ScreenshotItem
    {
        /// <summary>
        /// Screenshot name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Created date
        /// </summary>
        public DateTime Created
        {
            get
            {
                if (String.IsNullOrEmpty(Name))
                    return DateTime.Now;

                DateTime time;
                if (DateTime.TryParseExact(Path.GetFileNameWithoutExtension(Name), ScreenshotHelper.DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out time))
                {
                    return time;
                }

                return DateTime.Now;
            }

        }

        /// <summary>
        /// Test cycle id
        /// </summary>
        public string CycleID { get; set; }

        /// <summary>
        /// Test step id
        /// </summary>
        public string TestStepID { get; set; }
    }
}
