﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// 
    /// </summary>
    public class RuleElement
    {
        private String _CatalogName = "*";
        private String _TypicalName = "*";
        private String _FieldName = "*";

        /// <summary>
        /// Ctor for serialization
        /// </summary>
        public RuleElement()
        {
            
        }

        /// <summary>
        /// Data constructor - initialize fields
        /// </summary>
        /// <param name="catalogName"></param>
        /// <param name="typicalName"></param>
        /// <param name="fieldName"></param>
        public RuleElement(string catalogName, string typicalName, string fieldName)
        {
            CatalogName = catalogName;
            TypicalName = typicalName;
            FieldName = fieldName;
        }

        public String CatalogName
        {
            get { return _CatalogName; }
            set { _CatalogName = String.IsNullOrEmpty(value) ? "*" : value; }
        }

        public string TypicalName
        {
            get { return _TypicalName; }
            set { _TypicalName = String.IsNullOrEmpty(value) ? "*" : value; }
        }

        public string FieldName
        {
            get { return _FieldName; }
            set { _FieldName = String.IsNullOrEmpty(value) ? "*" : value; }
        }

        public string FieldShortName
        {
            get { return GetShortName(FieldName); }
        }

        private static string GetShortName(string name)
        {
            string shortname;
            int mv;
            int sv;
            AttributeNameParser.ParseComplexFieldName(name, out shortname, out mv, out sv);
            return shortname;
        }

        public bool CheckRuleElement(RuleElement element)
        {
            if (!CheckCatalog(element.CatalogName))
                return false;
            if (!CheckTypical(element.TypicalName))
                return false;
            if (!CheckField(element.FieldName))
                return false;
            return true;
        }

        private bool CheckField(string fieldname)
        {
            if (FieldName.Equals("*"))
                return true;
            if (FieldName.Contains("*}."))
            {
                string[] parts = FieldName.Split('*');
                return (fieldname.StartsWith(parts[0]) && fieldname.Contains(parts[1]));
            }
            if (FieldName.EndsWith("*"))
                return fieldname.StartsWith(FieldName.Remove(FieldName.Length - 1));
            if (FieldName.StartsWith("*"))
                return fieldname.EndsWith(FieldName.Remove(0));
            if (FieldName.Equals(fieldname))
                return true;
            return FieldShortName == GetShortName(fieldname);
        }

        private bool CheckTypical(string typicalname)
        {
            if (TypicalName.Equals("*"))
                return true;
            if (TypicalName.EndsWith("*"))
                return typicalname.StartsWith(TypicalName.Remove(TypicalName.Length - 1));
            if (TypicalName.StartsWith("*"))
                return typicalname.EndsWith(TypicalName.Remove(0));
            return TypicalName.Equals(typicalname);
        }

        private bool CheckCatalog(string catalog)
        {
            if (CatalogName.Equals("*"))
                return true;
            if (CatalogName.EndsWith("*"))
                return catalog.StartsWith(CatalogName.Remove(CatalogName.Length - 1));
            if (CatalogName.StartsWith("*"))
                return catalog.EndsWith(CatalogName.Remove(0));
            return CatalogName.Equals(catalog);
        }
    }
}
