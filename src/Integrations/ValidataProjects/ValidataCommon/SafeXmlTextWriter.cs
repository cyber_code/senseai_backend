﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

namespace ValidataCommon
{
    /// <summary>
    /// XML writer that is extra sensitive to invalid XML chars (it should produce valid XML)
    /// see http://msdn2.microsoft.com/en-us/library/k1y7hyy9(VS.71).aspx?topic=306132
    /// </summary>
    public class SafeXmlTextWriter : XmlTextWriter
    {
        /// <summary>
        /// The default character to substitute invalid XML characters with
        /// </summary>
        public const char DefaulInvalidXmlCharSubstite = '?';

        /// <summary>
        /// Character to substitue invalid chars
        /// </summary>
        public char InvalidXmlCharSubstitute = DefaulInvalidXmlCharSubstite;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SafeXmlTextWriter"/> class.
        /// </summary>
        /// <param name="w">The TextWriter to write to. It is assumed that the TextWriter is already set to the correct encoding.</param>
        public SafeXmlTextWriter(TextWriter w) : base(w)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SafeXmlTextWriter"/> class.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="encoding">The encoding.</param>
        public SafeXmlTextWriter(string filename, Encoding encoding) : base(filename, encoding)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SafeXmlTextWriter"/> class.
        /// </summary>
        /// <param name="w">The w.</param>
        /// <param name="encoding">The encoding.</param>
        public SafeXmlTextWriter(Stream w, Encoding encoding) : base(w, encoding)
        {
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Writes the string.
        /// </summary>
        /// <param name="text">The text.</param>
        public override void WriteString(String text)
        {
            string safeText  = GetValidXmlString(text, InvalidXmlCharSubstitute);
            base.WriteString(safeText);
        }

        #endregion

        #region Static methods

        /// <summary>
        /// Gets the valid XML string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string GetValidXmlString(string value)
        {
            return GetValidXmlString(value, DefaulInvalidXmlCharSubstite);
        }

        /// <summary>
        /// Checks the unicode string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="substituteChar">Char to substitute the invalid characters</param>
        /// <returns></returns>
        public static string GetValidXmlString(string value, char substituteChar)
        {
            if (value == null)
            {
                return value;
            }

            IEnumerable<char> invalidChars = FindInvalidXmlCharacters(value);

            foreach (char c in invalidChars)
            {
                Debug.WriteLine(string.Format("Subsituting '{0}' with '{1}' in '{2}'", c, substituteChar, value));
                value = value.Replace(c, substituteChar);
            }

            return value;
        }

        /// <summary>
        /// Finds the invalid XML characters.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private static IEnumerable<char> FindInvalidXmlCharacters(string value)
        {
            Dictionary<char, object> invalidChars = new Dictionary<char, object>();

            for (int i = 0; i < value.Length; ++i)
            {
                char c = value[i];
                if (IsInvalidChar(c))
                {
                    invalidChars[c] = null;
                }
            }

            return invalidChars.Keys;
        }

        /// <summary>
        /// Determines whether [is invalid char] [the specified c].
        /// </summary>
        /// <param name="c">The c.</param>
        /// <returns>
        /// 	<c>true</c> if [is invalid char] [the specified c]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInvalidChar(char c)
        {
            return IsInvalidXmlChar(c) || IsInvalidUnicodeChar(c);
        }

        /// <summary>
        /// Determines whether [is invalid XML char] [the specified c].
        /// </summary>
        /// <param name="c">The c.</param>
        /// <returns>
        /// 	<c>true</c> if [is invalid XML char] [the specified c]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInvalidXmlChar(char c)
        {
            return (c < 0x20 && c != '\t' & c != '\n' & c != '\r');
        }

        /// <summary>
        /// Determines whether [is invalid unicode char] [the specified c].
        /// </summary>
        /// <param name="c">The c.</param>
        /// <returns>
        /// 	<c>true</c> if [is invalid unicode char] [the specified c]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInvalidUnicodeChar(char c)
        {
            return c > 0xFFFD;
        }

        #endregion
    }
}