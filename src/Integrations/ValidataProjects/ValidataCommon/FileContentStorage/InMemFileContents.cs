using System.IO;
using System.Linq;
using System.Text;
using ValidataCommon.Interfaces;

namespace ValidataCommon.FileContentStorage
{
    /// <summary>
    /// 
    /// </summary>
    public class InMemFileContents : FileContents
    {
        private readonly object _Contents;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="src"></param>
        public InMemFileContents(IFileContents src = null)
        {
            if (src != null)
            {
                IsBinary = src.IsBinary;
                _Contents = src.IsBinary ? (object)src.ToByteArray() : (object)src.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contents"></param>
        public InMemFileContents(string contents)
        {
            _Contents = contents;
            IsBinary = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        public InMemFileContents(byte[] bytes)
        {
            _Contents = bytes;
            IsBinary = true;
        }

        public override byte[] ToByteArray()
        {
            if (_Contents == null)
                return null;

            if (!IsBinary)
                return Encoding.UTF8.GetBytes(_Contents.ToString());

            return _Contents as byte[];
        }

        public override string ToString()
        {
            if (_Contents == null)
                return null;

            if (IsBinary)    // todo - deal with the encoding
                Encoding.UTF8.GetString(_Contents as byte[]);

            return _Contents.ToString();
        }

        public override long Size
        {
            get
            {
                if (IsBinary && ToByteArray() != null)
                    return ToByteArray().Length;
                if (!IsBinary && ToString() != null)
                    return ToString().Length;
                return 0;
            }
        }

        public override StreamReader GetDataReader()
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public override IFileContents Append(IFileContents b)
        {
            if (this.IsBinary)
            {
                var aa = this.ToByteArray();
                if (aa == null) aa = new byte[0];
                var bb = b.ToByteArray();
                if (bb == null) bb = new byte[0];

                return new InMemFileContents(aa.Concat(bb).ToArray());
            }
            else
            {
                return new InMemFileContents(string.Format("{0}{1}", this, b.ToString()));
            }
        }
    }
}