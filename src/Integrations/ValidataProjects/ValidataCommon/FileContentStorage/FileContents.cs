using System.IO;
using ValidataCommon.Interfaces;

namespace ValidataCommon.FileContentStorage
{
    public abstract class FileContents : IFileContents
    {
        public bool IsBinary { get; protected set; }

        public abstract IFileContents Append(IFileContents b);

        public abstract long Size { get; }

        public abstract StreamReader GetDataReader();

        public abstract byte[] ToByteArray();
    }
}