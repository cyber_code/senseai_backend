using System.IO;
using ValidataCommon.Interfaces;

namespace ValidataCommon.FileContentStorage
{
    /// <summary>
    /// 
    /// </summary>
    public class DiskFileContents : FileContents
    {
        private readonly string _FileName;

        public string FileName { get { return _FileName; } }

        private StreamWriter _Writer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="isBinnary"></param>
        public DiskFileContents(string fileName, bool isBinnary)
        {
            _FileName = fileName;
            IsBinary = isBinnary;
        }

        public override StreamReader GetDataReader()
        {
            if (File.Exists(_FileName))
            {
                return new StreamReader(_FileName);
            }
            else
            {
                return null;
            }
        }

        public override long Size
        {
            get
            {
                try { return new FileInfo(_FileName).Length; }
                catch { return 0; }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override byte[] ToByteArray()
        {
            if (string.IsNullOrEmpty(_FileName) || !File.Exists(_FileName))
                return null;

            return File.ReadAllBytes(_FileName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            if (string.IsNullOrEmpty(_FileName) || !File.Exists(_FileName))
                return null;

            return File.ReadAllText(_FileName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public override IFileContents Append(IFileContents b)
        {
            if (_Writer == null)
                _Writer = new StreamWriter(_FileName, true);

            var dataReader = b.GetDataReader();
            if (dataReader == null)
            {
                _Writer.Write(b.ToString());
            }
            else
            {
                using (StreamReader reader = dataReader)
                {
                    _Writer.Write(reader.ReadToEnd());// todo - read chunks!!! otherwise possible memory problem
                }
            }

            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Flush()
        {
            if (_Writer != null)
            {
                _Writer.Flush();
                _Writer.Close();
                _Writer = null;
            }
        }
    }
}