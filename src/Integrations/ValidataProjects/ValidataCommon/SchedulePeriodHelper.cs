﻿using System;
using System.Linq;

namespace ValidataCommon
{
    /// <summary>
    /// Helper for scheduling periods 
    /// </summary>
    public class SchedulePeriodHelper
    {
        private const char DAY = 'd';
        private const char HOUR = 'h';
        private const char MINUTE = 'm';
        private const char SECOND = 's';

        private static readonly char[] SPECIFIERS = new[] {DAY, HOUR, MINUTE, SECOND};

        /// <summary>
        /// Decodes the time schedule.
        /// </summary>
        /// <param name="schedule">The schedule.</param>
        /// <returns></returns>
        public static TimeSchedule DecodeTimeSchedule(string schedule)
        {
            string[] split = schedule.Split(',');
            if (split.Length != 9)
            {
                throw new ApplicationException("Invalid Time Schedule string, must have 9 comma-separated values");
            }

            var result = new TimeSchedule
                             {
                                 InitialUsersCount = int.Parse(split[0]),
                                 StartOffset = DecodePeriod(split[1]),
                                 RampUpPeriod = DecodePeriod(split[2]),
                                 RampUpCount = int.Parse(split[3]),
                                 ExecutionDuration = DecodePeriod(split[4]),
                                 RampDownPeriod = DecodePeriod(split[5]),
                                 RampDownCount = int.Parse(split[6]),
                                 FinalUsersCount = int.Parse(split[7]),
                                 ForceClose = bool.Parse(split[8])
                             };

            return result;
        }

        /// <summary>
        /// Encodes the time schedule.
        /// </summary>
        /// <param name="schedule">The schedule.</param>
        /// <returns></returns>
        public static string EncodeTimeSchedule(TimeSchedule schedule)
        {
            string result = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                                          schedule.InitialUsersCount,
                                          EncodePeriod(schedule.StartOffset),
                                          EncodePeriod(schedule.RampUpPeriod),
                                          schedule.RampUpCount,
                                          EncodePeriod(schedule.ExecutionDuration),
                                          EncodePeriod(schedule.RampDownPeriod),
                                          schedule.RampDownCount,
                                          schedule.FinalUsersCount,
                                          schedule.ForceClose);

            return result;
        }

        /// <summary>
        /// Decodes the period.
        /// </summary>
        /// <param name="period">The period.</param>
        /// <returns></returns>
        public static TimeSpan DecodePeriod(string period)
        {
            int days = 0;
            int hours = 0;
            int minutes = 0;
            int seconds = 0;

            string currentNumber = string.Empty;
            for (int i = 0; i < period.Length; i++)
            {
                char curChar = period[i];
                if (Char.IsNumber(curChar))
                {
                    currentNumber += curChar;
                }
                else
                {
                    if (SPECIFIERS.Contains(curChar))
                    {
                        int number = int.Parse(currentNumber);
                        switch (curChar)
                        {
                            case DAY:
                                days = number;
                                break;
                            case HOUR:
                                hours = number;
                                break;
                            case MINUTE:
                                minutes = number;
                                break;
                            case SECOND:
                                seconds = number;
                                break;
                        }
                        currentNumber = string.Empty;
                    }
                }
            }

            var result = new TimeSpan(days, hours, minutes, seconds);
            return result;
        }

        /// <summary>
        /// Encodes the period.
        /// </summary>
        /// <param name="period">The period.</param>
        /// <returns></returns>
        public static string EncodePeriod(TimeSpan period)
        {
            string result = string.Empty;

            if (period.Days > 0)
            {
                result += period.Days + DAY.ToString() + " ";
            }

            if (period.Hours > 0)
            {
                result += period.Hours + HOUR.ToString() + " ";
            }

            if (period.Minutes > 0)
            {
                result += period.Minutes + MINUTE.ToString() + " ";
            }

            if (period.Seconds > 0)
            {
                result += period.Seconds + SECOND.ToString() + " ";
            }

            if (result.EndsWith(" "))
            {
                result = result.Substring(0, result.Length - 1);
            }

            if (result == string.Empty)
            {
                result = "0s";
            }

            return result;
        }
    }
}