﻿using System;
using System.Linq;
using System.Threading;

namespace ValidataCommon
{
    ///<summary>
    ///</summary>
    public class Safe : IDisposable
    {
        private readonly object[] _padlocks;
        private readonly bool[] _securedFlags;

        private Safe(object padlock, int milliSecondTimeout)
        {
            _padlocks = new[] {padlock};
            _securedFlags = new[] {Monitor.TryEnter(padlock, milliSecondTimeout)};
        }
        private Safe(object[] padlocks, int milliSecondTimeout)
        {
            _padlocks = padlocks;
            _securedFlags = new bool[_padlocks.Length];
            for (int i = 0; i < _padlocks.Length; i++)
                _securedFlags[i] = Monitor.TryEnter(padlocks[i], milliSecondTimeout);
        }
 
        ///<summary>
        ///</summary>
        public bool Secured
        {
            get { return _securedFlags.All(s => s); }
        }
 
        ///<summary>
        ///</summary>
        ///<param name="padlocks"></param>
        ///<param name="millisecondTimeout"></param>
        ///<param name="codeToRun"></param>
        ///<exception cref="TimeoutException"></exception>
        public static void Lock(object[] padlocks, int millisecondTimeout, Action codeToRun)
        {
            using (var bolt = new Safe(padlocks, millisecondTimeout))
                if (bolt.Secured)
                    codeToRun();
                else
                    throw new TimeoutException(string.Format("Safe.Lock wasn't able to acquire a lock in {0}ms",
                                                             millisecondTimeout));
        }
 
        ///<summary>
        ///</summary>
        ///<param name="padlock"></param>
        ///<param name="millisecondTimeout"></param>
        ///<param name="codeToRun"></param>
        ///<exception cref="TimeoutException"></exception>
        public static void Lock(object padlock, int millisecondTimeout, Action codeToRun)
        {
            using (var bolt = new Safe(padlock, millisecondTimeout))
                if (bolt.Secured)
                    codeToRun();
                else
                    throw new TimeoutException(string.Format("Safe.Lock wasn't able to acquire a lock in {0}ms",
                                                             millisecondTimeout));
        }
 
        #region Implementation of IDisposable
 
        public void Dispose()
        {
            for (int i = 0; i < _securedFlags.Length; i++)
                if (_securedFlags[i])
                {
                    Monitor.Exit(_padlocks[i]);
                    _securedFlags[i] = false;
                }
        }
        #endregion
    }
}
