﻿namespace ValidataCommon
{
    public interface IAttribute
    {
        string Name
        {
            get;
            set;
        }

        string Value
        {
            get;
            set;
        }
    }
}
