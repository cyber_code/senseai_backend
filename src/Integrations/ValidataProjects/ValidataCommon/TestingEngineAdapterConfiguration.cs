﻿
namespace ValidataCommon
{
    /// <summary>
    /// 
    /// </summary>
    public class TestingEngineAdapterConfiguration
    {
        /// <summary>
        /// 
        /// </summary>
        public bool WorkOnlyWithTopWindow { get; set; }

        private bool _AutomaticallyMatchControlsToAttributeNames = true;
        public bool AutomaticallyMatchControlsToAttributeNames { get { return _AutomaticallyMatchControlsToAttributeNames; } set { _AutomaticallyMatchControlsToAttributeNames = value; } }

        /// <summary>
        /// 
        /// </summary>
        public TestingEngineAdapterConfiguration()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        public TestingEngineAdapterConfiguration(string str)
        {
            try
            {
                var parts = str.Split(new char[] { ';' }, System.StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length > 0)
                {
                    int pos = parts[0].LastIndexOf("=");
                    if (pos > 0 )
                    {
                        string key = parts[0].Substring(0, pos).Trim();
                        string val = parts[0].Substring(pos + 1).Trim();
                        if (string.Compare(key, "WorkOnlyWithTopWindow", true) == 0)
                        {
                            bool bVal;
                            WorkOnlyWithTopWindow = bool.TryParse(val, out bVal) ? bVal : false;
                        }
                        else if (string.Compare(key, "AutoCtrlMatch", true) == 0)
                        {
                            if (!bool.TryParse(val, out _AutomaticallyMatchControlsToAttributeNames))
                                _AutomaticallyMatchControlsToAttributeNames = true;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var res = new System.Text.StringBuilder();

            if(WorkOnlyWithTopWindow)
                res.Append("WorkOnlyWithTopWindow=True");

            if (!_AutomaticallyMatchControlsToAttributeNames)
            {
                if (res.Length > 0)
                    res.Append("; ");

                res.Append("AutoCtrlMatch=False");
            }

            return res.ToString();
        }
    }
}
