﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Runtime.Serialization;

namespace ValidataCommon
{
    /// <summary>
    /// T24 Attribute
    /// </summary>
    [Serializable]
    public class T24Attribute
    {
        #region Classes

        /// <summary>
        /// Field info extracted from the version definition
        /// </summary>
        [Serializable]
        public class FieldVersionInfo
        {
            /// <summary>
            /// Field No
            /// </summary>
            public string FieldNo;

            /// <summary>
            /// Display Name
            /// </summary>
            public string DisplayName;

            /// <summary>
            /// Expansion
            /// </summary>
            public string Expansion;

            /// <summary>
            /// Is Expansion No
            /// </summary>
            public bool IsExpansionNo
            {
                get { return Expansion != null && string.Compare(Expansion, "NO", true) == 0; }
            }

            /// <summary>
            /// Default constructor
            /// </summary>
            public FieldVersionInfo()
            {
            }

            /// <summary>
            /// Copy constructor
            /// </summary>
            public FieldVersionInfo(FieldVersionInfo src)
            {
                FieldNo = src.FieldNo;
                DisplayName = src.DisplayName;
                Expansion = src.Expansion;
            }

            /// <summary>
            /// To String
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return string.Format("{0}; FieldNo: {1}; Expansion: {2}", DisplayName, FieldNo, Expansion);
            }
        }

        #endregion

        #region Private Constants
        /// <summary>
        /// Validata "No Value" string
        /// </summary>
        private const string NO_VALUE = "No Value"; 
        #endregion

        #region Properties

        private int _MultiValueIndex = 0;
        /// <summary>
        /// Multi Value Index
        /// </summary>
        public int MultiValueIndex
        {
            get { return _MultiValueIndex; }
            set { _MultiValueIndex = value; }
        }

        private int _SubValueIndex = 0;
        /// <summary>
        /// Sub Value Index
        /// </summary>
        public int SubValueIndex
        {
            get { return _SubValueIndex; }
            set { _SubValueIndex = value; }
        }

        private string _Name = String.Empty;
        /// <summary>
        /// Short attribute name
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _ValidataName = String.Empty;
        /// <summary>
        /// Full validata attribute name
        /// </summary>
        public string ValidataName
        {
            get { return _ValidataName; }
            set { _ValidataName = value; }
        }

        private bool _IsMultiValue = false;
        /// <summary>
        /// Determine if the attribute is multi value
        /// </summary>
        public bool IsMultiValue
        {
            get { return _IsMultiValue; }
            set { _IsMultiValue = value; }
        }

        private bool _IsMultiValueOnly = false;
        /// <summary>
        /// Determine if the attribute is multi value and does not have sub values
        /// </summary>
        public bool IsMultiValueOnly
        {
            get { return _IsMultiValueOnly; }
            set { _IsMultiValueOnly = value; }
        }

        private bool _IsSubValueOnly = false;
        /// <summary>
        /// Determine if the attribute has only sub values
        /// </summary>
        public bool IsSubValueOnly
        {
            get { return _IsSubValueOnly; }
            set { _IsSubValueOnly = value; }
        } 

        private bool _HasDeletedMultiAndSubValues = false;
        /// <summary>
        /// Determine whether there are deleted multi or sub values
        /// </summary>
        public bool HasDeletedMultiAndSubValues
        {
            get { return _HasDeletedMultiAndSubValues; }
            set { _HasDeletedMultiAndSubValues = value; }
        } 

        /// <summary>
        /// Full T24 web attribute name
        /// </summary>
        public string FullName
        {
            get 
            {
                if (_IsMultiValue)
                {
                    if (_IsMultiValueOnly)
                        return String.Format("{0}:{1}", _Name, _MultiValueIndex);
                    else if (_IsSubValueOnly)
                        return String.Format("{0}:{1}", _Name, _SubValueIndex);
                    else
                        return String.Format("{0}:{1}:{2}", _Name, _MultiValueIndex, _SubValueIndex);
                }
                else
                {
                    return _Name;
                }
            }
        }

        private string _Value = String.Empty;
        /// <summary>
        /// Attribute value
        /// </summary>
        public string Value
        {
            get 
            {
                if (String.IsNullOrEmpty(_Value) || _Value == NO_VALUE)
                    return String.Empty;

                return _Value; 
            }
            set { _Value = value; }
        }

        private List<T24Attribute> _Items = new List<T24Attribute>();
        /// <summary>
        /// Contains multi value attributes of the attribute
        /// </summary>
        public List<T24Attribute> Items
        {
            get { return _Items; }
            set { _Items = value; }
        }

        private List<T24Attribute> _SubItems = new List<T24Attribute>();
        /// <summary>
        /// Contains sub value attributes of the attribute
        /// </summary>
        public List<T24Attribute> SubItems
        {
            get { return _SubItems; }
            set { _SubItems = value; }
        }

        /// <summary>
        /// Attribute value is empty
        /// </summary>
        public bool IsAttribEmpty
        {
            get
            {
                if (String.IsNullOrEmpty(_Value) || _Value == NO_VALUE)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Attribute is empty
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                if (_IsMultiValue)
                {
                    if (_IsMultiValueOnly || _IsSubValueOnly)
                    {
                        if (_Items.Count > 0)
                            return !_Items.Exists(a => !a.IsEmpty);
                        else
                            return IsAttribEmpty;
                    }
                    else
                    {
                        if (_Items.Count > 0 || _SubItems.Count > 0)
                            return !(_Items.Exists(a => !a.IsEmpty) || _SubItems.Exists(a => !a.IsEmpty));
                        else
                            return IsAttribEmpty;
                    }
                }
                else
                {
                    return IsAttribEmpty;
                }
            }
        }

        /// <summary>
        /// AskT24ForMultiValues
        /// </summary>
        public bool AskT24ForMultiValues = true;

        public bool IsMultiline { get; set; }

        public bool IsBlob { get; set; }

        /// <summary>
        /// Field info extracted from the version definition
        /// </summary>
        public FieldVersionInfo VersionInfo { get; set; }

        #endregion

        #region Lifecycle

        /// <summary>
        /// T24Attribute
        /// </summary>
        /// <param name="src"></param>
        public T24Attribute(T24Attribute src)
        {
            this._MultiValueIndex = src._MultiValueIndex;
            this._SubValueIndex = src._SubValueIndex;
            this._Name = src._Name;
            this._ValidataName = src._ValidataName;
            this._IsMultiValue = src._IsMultiValue;
            this._IsMultiValueOnly = src._IsMultiValueOnly;
            this._IsSubValueOnly = src._IsSubValueOnly;
            this._HasDeletedMultiAndSubValues = src._HasDeletedMultiAndSubValues;
            this._Value = src._Value;
            this.AskT24ForMultiValues = src.AskT24ForMultiValues;
            this.IsMultiline = src.IsMultiline;
            this.IsBlob = src.IsBlob;

            foreach (T24Attribute ga in src._Items)
            {
                this._Items.Add(new T24Attribute(ga));
            }

            foreach (T24Attribute ga in src._SubItems)
            {
                this._SubItems.Add(new T24Attribute(ga));
            }

            if (src.VersionInfo != null)
            {
                VersionInfo = new FieldVersionInfo(src.VersionInfo);
            }
        }

        /// <summary>
        /// T24Attribute
        /// </summary>
        /// <param name="name"></param>
        /// <param name="multiValueIndex"></param>
        /// <param name="subValueIndex"></param>
        /// <param name="value"></param>
        public T24Attribute(string name, int multiValueIndex, int subValueIndex, string value)
        {
            _Name = name;
            _MultiValueIndex = multiValueIndex;
            _SubValueIndex = subValueIndex;
            _Value = value;
        }

        /// <summary>
        /// T24Attribute
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public T24Attribute(string name, string value)
        {
            _Name = name;
            _Value = value;
        }

        /// <summary>
        /// T24Attribute
        /// </summary>
        /// <param name="name"></param>
        public T24Attribute(string name)
        {
            _Name = name;
        } 
        #endregion

        #region Overrides

        public override string ToString()
        {
            return string.Format("{0}={1}", string.IsNullOrEmpty(ValidataName)  ? FullName : ValidataName, Value);
        }

        #endregion
    }
}
