﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ValidataCommon
{
    [Serializable]
    public class ManualTaskDescription
    {
        public string Comments { get; set; }

        /// <summary>
        /// Indicates that this job will block the exectuion until import of results is finished
        /// </summary>
        public bool BlockExecutionOfDependantJobs;

        /// <summary>
        /// Indicator for postponed import of results instances
        /// </summary>
        public bool PostponedImport;

        [XmlIgnore]
        public TimeSpan ActualExecutionDuration { get; set; }

        public int ActualExecutionDurationInSeconds
        {
            get { return (int)ActualExecutionDuration.TotalSeconds; }
            set { ActualExecutionDuration = new TimeSpan(0, 0, value); }
        }

        public ManualTaskDescription()
        {
            Comments = String.Empty;
        }

        public static ManualTaskDescription DeserializeFromString(string serialized)
        {
            if (!string.IsNullOrEmpty(serialized))
            {
                try
                {
                    return new XmlSerializer(typeof(ManualTaskDescription)).Deserialize(new StringReader(serialized)) as ManualTaskDescription;
                }
                catch(InvalidOperationException)
                {
                    ApplicationException exp = new ApplicationException("The manual task description cannot been successfully read.");
                    exp.Data.Add("serialized", serialized);
                    throw exp;
                }
            }

            return new ManualTaskDescription();
        }

        public string SerializeToString()
        {
            StringWriter sw = new StringWriter();
            new XmlSerializer(GetType()).Serialize(sw, this);
            return sw.ToString();
        }
    }
}

