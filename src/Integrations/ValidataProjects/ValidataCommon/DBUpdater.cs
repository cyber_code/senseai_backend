﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ValidataCommon
{
    [Serializable]
    public class DBXSDsUpdater
    {
        [XmlElement]
        public ulong ProjectNoderef { get; set; }

        [XmlElement]
        public ulong SubProjectNoderef { get; set; }

        [XmlElement]
        public ulong DisciplineNoderef { get; set; }

        [XmlElement]
        public ulong RoleNoderef { get; set; }

        [XmlElement]
        public ulong UserNoderef { get; set; }

        public List<XSD> XSDs { get; set; }      
    }

    [Serializable]
    public class XSD
    {
        [XmlAttribute]
        public string path { get; set; }
    }

    [Serializable]
    public class DBAssociationUpdater
    {
        [XmlElement]
        public string Name { get; set; }

        [XmlElement]
        public string ABAlias { get; set; }

        [XmlElement]
        public string BAAlias { get; set; }

        [XmlElement]
        public string Icon { get; set; }

        [XmlElement]
        public ulong SourceBaseClassNoderef { get; set; }

        [XmlElement]
        public ulong TargetBaseClassNoderef { get; set; }
    }
}
