﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// List of typical definitions
    /// </summary>
    public class TypicalDefinitionCollection : List<TypicalDefinition>
    {
        #region Public Static Methods

        /// <summary>
        /// Initalizes the list of typical definitions from the XSD file.
        /// </summary>
        /// <param name="xsdFileName">Name of the XSD file.</param>
        /// <returns></returns>
        public static TypicalDefinitionCollection FromXsdFile(string xsdFileName)
        {
            Stream stream = new FileStream(xsdFileName, FileMode.Open);
            try
            {
                return TypicalDefinitionCollectionXsdParser.ReadSchema(stream);
            }
            finally
            {
                stream.Close();
            }
        }

        /// <summary>
        /// Initalizes the list of typical definitions from the XSD files.
        /// </summary>
        /// <param name="xsdFileNames"></param>
        /// <returns></returns>
        public static TypicalDefinitionCollection FromXsdFiles(string[] xsdFileNames)
        {
            var result = new TypicalDefinitionCollection();

            foreach (var xsdFileName in xsdFileNames)
            {
                result.AddRange(TypicalDefinitionCollection.FromXsdFile(xsdFileName));
            }

            return result;
        }

        /// <summary>
        ///Initalizes the list of typical definitions from a string XSD.
        /// </summary>
        /// <param name="xsdString">The XSD string.</param>
        /// <returns></returns>
        public static TypicalDefinitionCollection FromXsd(string xsdString)
        {
            return TypicalDefinitionCollectionXsdParser.ReadSchema(xsdString);
        }

        /// <summary>
        /// Writes the typicals definition to the XSD file.
        /// </summary>
        /// <param name="typicalDefinition">The typical definition.</param>
        /// <param name="xsdFileName">Name of the XSD file.</param>
        public static void ToXsdFile(TypicalDefinition typicalDefinition, string xsdFileName)
        {
            DataSet ds = GetDataSet(new TypicalDefinitionCollection {typicalDefinition});
            ds.WriteXmlSchema(xsdFileName);
        }

        /// <summary>
        /// Gets an XSD string from the typical definitions
        /// </summary>
        /// <param name="typicalDefinition">The typical definition.</param>
        /// <returns></returns>
        public static string ToXsd(TypicalDefinition typicalDefinition)
        {
            MemoryStream ms = new MemoryStream();
            using (StreamWriter sw = new StreamWriter(ms))
            {
                DataSet ds = GetDataSet(new TypicalDefinitionCollection {typicalDefinition});
                ds.WriteXmlSchema(sw);
            }

            return Encoding.UTF8.GetString(ms.ToArray());
            //was: return Encoding.UTF8.GetString(ms.GetBuffer()); - This doesn't allocate new memory, but fills the returning string with 0 bytes at the end by
        }

        /// <summary>
        /// Writes the typical definitions to the XSD file.
        /// </summary>
        /// <param name="typicalDefinitionCollection">The typical definition collection.</param>
        /// <param name="xsdFileName">Name of the XSD file.</param>
        public static void ToXsdFile(TypicalDefinitionCollection typicalDefinitionCollection, string xsdFileName)
        {
            DataSet ds = GetDataSet(typicalDefinitionCollection);
            ds.WriteXmlSchema(xsdFileName);
        }

        /// <summary>
        /// Gets an XSD string from the typical definitions
        /// </summary>
        /// <param name="typicalDefinitionCollection">The typical definition collection.</param>
        /// <returns></returns>
        public static string ToXsd(TypicalDefinitionCollection typicalDefinitionCollection)
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                DataSet ds = GetDataSet(typicalDefinitionCollection);
                ds.WriteXmlSchema(sw);
            }

            return sb.ToString();
        }


        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default constructor
        /// </summary>
        public TypicalDefinitionCollection()
            : base()
        {
        }

        /// <summary>
        /// Constructs collection from source
        /// </summary>
        /// <param name="src"></param>
        public TypicalDefinitionCollection(IEnumerable<TypicalDefinition> src)
            : base(src)
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Determines whether [contains] [the specified catalog name].
        /// </summary>
        /// <param name="catalogName">Name of the catalog.</param>
        /// <param name="typicalName">Name of the typical.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified catalog name]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(string catalogName, string typicalName)
        {
            bool res = Find(catalogName, typicalName) != null;
            return res;
        }

        /// <summary>
        /// Finds the specified catalog name.
        /// </summary>
        /// <param name="catalogName">Name of the catalog.</param>
        /// <param name="typicalName">Name of the typical.</param>
        /// <returns></returns>
        public TypicalDefinition Find(string catalogName, string typicalName)
        {
            return this.FirstOrDefault(td => td.CatalogName == catalogName && td.Name == typicalName);
        }

        /// <summary>
        /// Finds all typicals by base class
        /// </summary>
        /// <param name="baseClass">Typical base class</param>
        /// <returns></returns>
        public TypicalDefinitionCollection FindAllByBaseClass(string baseClass)
        {
            return new TypicalDefinitionCollection(this.FindAll(n => n.BaseClass == baseClass));
        }

        /// <summary>
        /// Finds all typicals by name
        /// </summary>
        /// <param name="typicalName">Typical name</param>
        /// <returns></returns>
        public TypicalDefinitionCollection FindAllByName(string typicalName)
        {
            return new TypicalDefinitionCollection(this.FindAll(n => n.Name == typicalName));
        }

        /// <summary>
        /// Find first typical with given name
        /// </summary>
        /// <param name="typicalName"></param>
        /// <returns></returns>
        public TypicalDefinition FindByName(string typicalName)
        {
            return FindAllByName(typicalName).FirstOrDefault();
        }

        /// <summary>
        /// Creates a dataset containing tables for all typicals in the collection
        /// </summary>
        /// <returns></returns>
        public DataSet ToDataSet()
        {
            return GetDataSet(this);
        }

        #endregion

        #region Private Static Methods

        private static DataSet GetDataSet(IEnumerable<TypicalDefinition> typicalDefinitionCollection)
        {
            DataSet ds = new DataSet("TypicalDefinitions");

            foreach (TypicalDefinition td in typicalDefinitionCollection)
            {
                ds.Tables.Add(td.ToDataTable());
            }

            return ds;
        }

        #endregion
    }
}