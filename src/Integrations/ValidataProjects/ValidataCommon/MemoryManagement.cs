/* This class is not used -> so it is commented out

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace ValidataCommon
{    
    /// <summary>
    /// Memory management and diagnosing
    /// </summary>
    /// 
    
    public class MemoryManagement
    {
        [DllImport("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize", ExactSpelling = true,
            CharSet = CharSet.Ansi, SetLastError = true)]
        private static extern int SetProcessWorkingSetSize(IntPtr process,
                                                           int minimumWorkingSetSize,
                                                           int maximumWorkingSetSize);

        /// <summary>
        /// Flushes the memory, by forcing garbage collection
        /// </summary>
        public static void FlushMemory()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                SetProcessWorkingSetSize(Process.GetCurrentProcess().Handle, -1, -1);
            }
        }

        /// <summary>
        /// Gets the current memory usage statistic (working set and virtual memory).
        /// </summary>
        /// <param name="context">The context.</param>
        public static string GetCurrentMemoryUsageStats(string context)
        {
            return string.Format("[{0}] WorkingSet: {1} KB; TotalMemory: {2} KB",
                                 context,
                                 Environment.WorkingSet/1024,
                                 GC.GetTotalMemory(false)/1024);
        }
    }
}

*/