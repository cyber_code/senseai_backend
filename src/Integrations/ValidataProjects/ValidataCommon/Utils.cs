
using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ValidataCommon
{
    /// <summary>
    /// Utility functions.
    /// </summary>
    public class Utils
    {
        #region ToolTips methods

        /// <summary>
        /// Activate tooltip message for ListControl items
        /// </summary>
        /// <param name="lc"></param>
        public static void BindTooltip(ListControl lc)
        {
            for (int i = 0; i < lc.Items.Count; i++)
            {
                lc.Items[i].Attributes.Add("title", lc.Items[i].Text);
            }
        }

        /// <summary>
        /// Activate tooltip message for ListControls items belonging to the some Web Control
        /// </summary>
        /// <param name="control"></param>
        public  static void SetTooltipsRecursively(Control control)
        {
            if (control != null)
            {
                if (control.Controls != null)
                {
                    foreach (Control ctrl in control.Controls)
                    {
                        var lstControl = ctrl as ListControl;
                        if (lstControl != null)
                        {
                            BindTooltip(lstControl);
                        }
                        else
                        {
                            SetTooltipsRecursively(ctrl);
                        }
                    }
                }
            }
        }

        #endregion

        #region HTML methods

        /// <summary>
        /// Get HTML Encoded Value
        /// </summary>
        /// <param name="val">Value</param>
        /// <returns>HTML Encoded Value</returns>
        public static string GetHtmlEncodedValue(string val)
        {
            if (val == null)
            {
                return null;
            }
            // First perform HtmlDecode in order to clean any previous encodings - either by the external systems or adapters
            //fixed framework 4.0 single quote encoding.
            return HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(val)).Replace("&#39;", "'");
        }

        /// <summary>
        /// Get HTML Decoded Value
        /// </summary>
        /// <param name="val">Value</param>
        /// <returns>HTML Decoded Value</returns>
        public static string GetHtmlDecodedValue(string val)
        {
            return HttpUtility.HtmlDecode(val);
        }

        /// <summary>
        /// Gets the string with non breaking spaces.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns>The value with replaced white spaces with non breaking spaces</returns>
        public static string GetWithNonBreakingSpaces(string val)
        {
            return val.Replace(" ", "&nbsp;");
        }

        #endregion

        #region File methods

        /// <summary>
        /// Reads the contents of a text file in a way not requiring a file lock
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public static string ReadFileContents(string fileName)
        {
            return ReadFileContents(fileName, Encoding.Default);
        }

        /// <summary>
        /// Reads the contents of a text file in a way not requiring a file lock
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns></returns>
        public static string ReadFileContents(string fileName, Encoding encoding)
        {
            // NOTE: Don't use the built-in File.ReadAllText() as it doesn't work with files that have been locked by another process
            string result;
            using (FileStream fs = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (StreamReader reader = new StreamReader(fs, encoding))
                {
                    result = reader.ReadToEnd();
                }
            }

            return result;
        } 

        /// <summary>
        /// Read all lines of a file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string[] ReadAllLines(string fileName)
        {
            return ReadFileContents(fileName).Split(new string[] { "\r", "\n", "\r\n" }, StringSplitOptions.None);
        }

        /// <summary>
        /// Deletes the file or fails silently
        /// </summary>
        /// <param name="filePath">Name of the file.</param>
        public static bool DeleteFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
                return false;

            try
            {
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                    return true;
                }
            }
            catch
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// Checks if directory exists for the passed file path. If not, create it
        /// </summary>
        /// <param name="filePath"></param>
        public static void AssureDirectoryExists(string filePath)
        {
            string dir = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        /// <summary>
        /// Constructs a valid path by replacing the invalid characters in the path
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="validReplacementChar">The replacement char.</param>
        /// <returns></returns>
        public static string GetValidPath(string path, char validReplacementChar)
        {
            return ReplaceInvalidChars(path, Path.GetInvalidPathChars(), validReplacementChar);
        }

        /// <summary>
        /// Constructs a valid path by replacing the invalid characters in the path
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="validReplacementChar">The valid replacement char.</param>
        /// <returns></returns>
        public static string GetValidFileName(string path, char validReplacementChar)
        {
            return ReplaceInvalidChars(path, Path.GetInvalidFileNameChars(), validReplacementChar);
        }

        private static string ReplaceInvalidChars(string str, char[] invalidChars, char replacemenChar)
        {
            foreach (char c in invalidChars)
            {
                str = str.Replace(c, replacemenChar);
            }

            return str;
        }

      
        #endregion

        #region DateTime methods

        /// <summary>
        /// For a given string containing a value for a date attribute, get the tick count.
        /// </summary>
        /// <param name="dateValue"></param>
        /// <returns></returns>
        public static string GetTickValueForDate(string dateValue)
        {
            return ValidataConverter.GetDateTime(dateValue).Ticks.ToString();
        }

        /// <summary>
        /// Formats the time span.
        /// </summary>
        /// <param name="elapsed">The elapsed.</param>
        /// <returns></returns>
        public static string FormatTimeSpan(TimeSpan elapsed)
        {
            StringBuilder sb = new StringBuilder();
            bool found = false;
            if (elapsed.Days > 0)
            {
                sb.Append(elapsed.Days).Append(" days, ");
                found = true;
            }

            if (found || elapsed.Hours > 0)
            {
                sb.Append(elapsed.Hours).Append(" hours, ");
                found = true;
            }

            if (found || elapsed.Minutes > 0)
            {
                sb.Append(elapsed.Minutes).Append(" minutes, ");
                found = true;
            }

            if (found || elapsed.Seconds > 0)
            {
                sb.Append(elapsed.Seconds).Append(" seconds");
                found = true;
            }

            if (!found)
            {
                sb.Append(elapsed.Milliseconds).Append(" miliseconds");
            }

            return sb.ToString();
        }

        #endregion

        #region String methods

        /// <summary>
        /// Check 2 strings are equal. NOTE: null and empty are assumed as equal
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool AreStringsEquivalent(string a, string b)
        {
            if (string.IsNullOrEmpty(a) && string.IsNullOrEmpty(b))
            {
                return true;
            }

            return (a == b);
        }

        #endregion
    }
}