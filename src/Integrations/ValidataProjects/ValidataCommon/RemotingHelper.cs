﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Remoting;

namespace ValidataCommon
{
    /// <summary>
    /// Helper for .NET Remoting with interfaces 
    /// see http://www.thinktecture.com/resourcearchive/net-remoting-faq/useinterfaceswithconfigfiles
    /// </summary>
    public class RemotingHelper
    {
        private static bool _isInit;
        private static IDictionary<Type, WellKnownClientTypeEntry> _WellKnownTypes;

        /// <summary>
        /// Gets the well-known remoting object.
        /// </summary>
        /// <returns></returns>
        public static T GetObject <T>()
        {
            WellKnownClientTypeEntry entr = GetWellknownType(typeof (T));
            return (T) Activator.GetObject(entr.ObjectType, entr.ObjectUrl);
        }

        /// <summary>
        /// Gets the well-known remoting object.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <returns></returns>
        public static T GetObject <T>(object state)
        {
            WellKnownClientTypeEntry entr = GetWellknownType(typeof (T));
            return (T) Activator.GetObject(entr.ObjectType, entr.ObjectUrl, state);
        }

        /// <summary>
        /// Gets the type of the wellknown.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static WellKnownClientTypeEntry GetWellknownType(Type type)
        {
            if (!_isInit)
            {
                InitTypeCache();
            }

            WellKnownClientTypeEntry entr;
            _WellKnownTypes.TryGetValue(type, out entr);
            if (entr == null)
            {
                throw new RemotingException("Type " + type.FullName + " not found!");
            }

            return entr;
        }

        /// <summary>
        /// Initializes the type cache.
        /// </summary>
        private static void InitTypeCache()
        {
            _isInit = true;
            _WellKnownTypes = new Dictionary<Type, WellKnownClientTypeEntry>();
            foreach (WellKnownClientTypeEntry entr in RemotingConfiguration.GetRegisteredWellKnownClientTypes())
            {
                if (entr.ObjectType != null)
                {
                    _WellKnownTypes.Add(entr.ObjectType, entr);
                }
                else
                {
                    Debug.Fail("Type " + entr.TypeName + " cannot be loaded");
                }
            }
        }
    }
}