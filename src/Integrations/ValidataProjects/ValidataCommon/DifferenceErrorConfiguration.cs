﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ValidataCommon
{
    [Serializable]
    public class DifferenceErrorConfiguration
    {
        #region Members
        public int Version { get; set; }
        public List<DifferenceErrorIgnoreRule> DifferenceErrorIgnoreRuleList { get; set; }
        public List<DifferenceErrorConversionRule> DifferenceErrorConversionRuleList { get; set; }
        public List<MultiValueSplitElement> MultiValueRuleList { get; set; }
        #endregion

        #region Serialization

        /// <summary>
        /// Execute the process of serialization of the data
        /// </summary>
        public void PerformSerialization()
        {
            SerializeToXml(this);
        }

        public void SerializeToXml()
        {
            SerializeToXml(this);
        }

        /// <summary>
        /// Generate path and execute serialization of the data
        /// </summary>
        /// <param name="configuratinon">the configuratinon to serialize</param>
        public static void SerializeToXml(DifferenceErrorConfiguration configuratinon)
        {
            string filePath = GenerateFullPath();
            SerializeToXml(configuratinon, filePath);
        }

        /// <summary>
        /// Actual serialization of the data
        /// </summary>
        /// <param name="configuratinon">the configuratinon to serialize</param>
        /// <param name="filePath"></param>
        /// <returns>false- cancel the save</returns>
        public static void SerializeToXml(DifferenceErrorConfiguration configuratinon, string filePath)
        {
            ValidataCommon.XmlSerializationHelper.SerializeToXmlFile(configuratinon, filePath);
        }

        public static DifferenceErrorConfiguration DeserializeFromXml()
        {
            return DeserializeFromXml(GenerateFullPath());
        }

        public static DifferenceErrorConfiguration DeserializeFromXml(string fullPath)
        {
            DifferenceErrorConfiguration configuration = ValidataCommon.XmlSerializationHelper.DeserializeFromXmlFile(typeof(DifferenceErrorConfiguration), fullPath) as DifferenceErrorConfiguration;

            return configuration;
        }

        public static string GenerateFullPath()
        {
            //String configPath = @"C:\AdapterFiles\Assemblies";
            //if (!Directory.Exists(configPath))
            //    Directory.CreateDirectory(configPath);
            //String filePath = String.Concat(configPath, @"\DifferenceError.Config");
            return Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\Assemblies\DifferenceError.Config";
        }

        #endregion
    }
}
