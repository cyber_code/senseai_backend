﻿namespace ValidataCommon
{
    /// <summary>
    /// Progress update for long running operations 
    /// </summary>
    public class LongRunningOperationProgress
    {
        private string _OperationID = "";
        private string _LastMesage = "";
        private int _ProgressStepsTotalCount;
        private int _ProgressStepsCompletedCount;
        private bool _IsInProgress = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="LongRunningOperationProgress"/> class.
        /// </summary>
        /// <param name="operationID">The operation identifier.</param>
        public LongRunningOperationProgress(string operationID)
        {
            _OperationID = operationID;
        }

        /// <summary>
        /// Gets the operation identifier.
        /// </summary>
        /// <value>
        /// The operation identifier.
        /// </value>
        public string OperationID
        {
            get { return _OperationID; }
        }

        /// <summary>
        /// Gets the last message.
        /// </summary>
        /// <value>
        /// The last message.
        /// </value>
        public string LastMessage
        {
            get { return _LastMesage; }
        }

        /// <summary>
        /// Gets a value indicating whether [should display progress].
        /// </summary>
        /// <value>
        /// <c>true</c> if [should display progress]; otherwise, <c>false</c>.
        /// </value>
        public bool ShouldDisplayProgress
        {
            get { return _ProgressStepsTotalCount > 0; }
        }

        /// <summary>
        /// Sets the status message.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public void SetStatusMessage(string status)
        {
            _LastMesage = status;
        }

        /// <summary>
        /// Sets the progress steps count.
        /// </summary>
        /// <param name="count">The count.</param>
        public void SetProgressStepsCount(int count)
        {
            _ProgressStepsTotalCount = count;
        }

        /// <summary>
        /// Marks the step as completed.
        /// </summary>
        public void MarkStepAsCompleted()
        {
            _ProgressStepsCompletedCount ++;
        }

        /// <summary>
        /// Gets the percent completed.
        /// </summary>
        /// <value>
        /// The percent completed.
        /// </value>
        public int PercentCompleted
        {
            get
            {
                if (_ProgressStepsTotalCount == 0)
                    return 0;

                return _ProgressStepsCompletedCount*100/_ProgressStepsTotalCount;
            }
        }

        /// <summary>
        /// Marks the completed.
        /// </summary>
        public void MarkCompleted()
        {
            _IsInProgress = false;
        }

        /// <summary>
        /// Gets a value indicating whether [is in progress].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is in progress]; otherwise, <c>false</c>.
        /// </value>
        public bool IsInProgress
        {
            get { return _IsInProgress; }
        }
    }
}