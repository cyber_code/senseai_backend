﻿

using System;
namespace ValidataCommon
{
    /// <summary>
    /// NArray wrapper
    /// </summary>
    internal class NArrayWrapper
    {
        /// <summary>
        /// Min value (-1 not defined)
        /// </summary>
        internal readonly int Min = -1;

        /// <summary>
        /// Max value (-1 not defined)
        /// </summary>
        internal readonly int Max = -1;

        private const char Delimiter = '.';

        #region Constructor

        /// <summary>
        /// Constructs
        /// </summary>
        /// <param name="value">NArray value</param>
        internal NArrayWrapper(string value)
        {
            try { Parse(value, out Min, out Max); }
            catch { }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Parses NArray value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        private void Parse(string value, out int min, out int max)
        {
            min = -1;
            max = -1;

            if (String.IsNullOrEmpty(value))
                return;

            string[] parts = value.Split(new char[] { Delimiter });

            if (parts.Length >= 1)
            {
                if(!int.TryParse(parts[0], out max))
                {
                    max = -1;
                }
            }

            if (parts.Length >= 2)
            {
                if (!int.TryParse(parts[1], out min))
                {
                    min = -1;
                }
            }
        }

        #endregion
    }
}
