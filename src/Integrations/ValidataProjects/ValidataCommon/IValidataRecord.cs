﻿using System.Collections.Generic;

namespace ValidataCommon
{
    public interface IValidataRecord
    {
        string CatalogName
        {
            get;
        }

        string TypicalName
        {
            get;
        }

        IEnumerable<IAttribute> Attributes
        {
            get;
        }
    }
}
