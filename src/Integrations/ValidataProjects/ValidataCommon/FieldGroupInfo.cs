﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    public class FieldGroupInfo
    {
        private readonly List<FieldInfo> _FieldInfos;

        public readonly List<FieldGroupInfo> SubGroups;

        public bool IsSingleField { get { return GetCountOfFields() == 1; } }

        public FieldInfo SingleFieldInfo
        {
            get
            {
                if (_FieldInfos != null)
                    return _FieldInfos.First();
                return
                    SubGroups.First().SingleFieldInfo;
            }
        }

        public int FieldNo
        {
            get
            {
                if (_FieldInfos != null)
                    return _FieldInfos.Min(n => GetFieldIndex(n));
                return
                    SubGroups.Min(n => n.FieldNo);
            }
        }

        public int GroupID
        {
            get
            {
                if (_FieldInfos != null)
                    return _FieldInfos.First().GroupID;
                else
                    return SubGroups.First().GroupID;
            }
        }

        public int SubGroupID
        {
            get
            {
                if (_FieldInfos != null)
                    return _FieldInfos.First().SubGroupID;
                else
                    return SubGroups.First().SubGroupID;
            }
        }

        public FieldInfoList Fields
        {
            get
            {
                var result = new FieldInfoList();
                if (_FieldInfos != null)
                    result.AddRange(_FieldInfos);
                else
                    SubGroups.ForEach(n => result.AddRange(n.Fields));
                return result;
            }
        }

        public FieldGroupInfo(List<FieldInfo> fieldInfos, bool expandSubgroups)
        {
            if (fieldInfos == null || fieldInfos.Count == 0)
                throw new ArgumentNullException("Filed infos must be defined!");

            if (!expandSubgroups)
            {
                _FieldInfos = fieldInfos.OrderBy(n => n.FieldNo).ToList();
            }
            else
            {
                SubGroups = new List<FieldGroupInfo>();
                foreach (var subGroup in fieldInfos.GroupBy(n => n.SubGroupID).Select(n => n.ToList()))
                {
                    SubGroups.Add(new FieldGroupInfo(subGroup, false));
                }
            }
        }

        private static int GetFieldIndex(FieldInfo n)
        {
            int nn;
            return int.TryParse(n.FieldNo, out nn) ? nn : -1;
        }

        private int GetCountOfFields()
        {
            if (_FieldInfos != null)
                return _FieldInfos.Count;
            else
                return SubGroups.Sum(n => n.GetCountOfFields());
        }

        public override string ToString()
        {
            bool isSub = _FieldInfos != null;
            var children = isSub
                ? string.Join("; ", _FieldInfos.Select(n => n.FieldName))
                : string.Join("; ", SubGroups.Select(n => n.ToString()));
            return string.Format("{0}Group: {1} ({3})", isSub ? "Sub" : "", isSub ? SubGroupID : GroupID, children);
        }
    }

    public class AttributePair
    {
        public string Name;
        public string LeftValue;
        public string RightValue;

        public string ShortName { get { return AttributeNameParser.GetShortFieldName(Name); } }

        public int MV
        {
            get
            {
                string shortName;
                int mv, sv;
                return AttributeNameParser.ParseComplexFieldName(Name, out shortName, out mv, out sv) ? mv : -1;
            }
        }

        public int SV
        {
            get
            {
                string shortName;
                int mv, sv;
                return AttributeNameParser.ParseComplexFieldName(Name, out shortName, out mv, out sv) ? sv : -1;
            }
        }

        public bool AreValuesDifferent { get { return LeftValue != RightValue; } }

        public string SvAsString()
        {
            return SV < 0 ? null : SV.ToString();
        }

        public string MvAsString()
        {
            return MV < 0 ? null : MV.ToString();
        }

        public override string ToString()
        {
            return string.Format("{0}: {1} | {2}", Name, LeftValue ?? "[null]", RightValue ?? "[null]");
        }
    }

    public class AttributePairList : List<AttributePair>
    {
        public bool HasDifferences { get { return this.Any(n => n.AreValuesDifferent); } }

        public AttributePairList()
        {
        }

        public AttributePairList(IEnumerable<AttributePair> src)
            : base(src)
        {
        }
    }
}
