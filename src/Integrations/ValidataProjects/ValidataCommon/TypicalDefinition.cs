﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Definition of a typical (i.e. its attributes and other stuff)
    /// </summary>
    [Serializable]
    public class TypicalDefinition : IMetadata, ICloneable
    {
        private string _Comments;

        private string _xml;
        /// <summary>
        /// Gets or sets the name of the catalog.
        /// </summary>
        /// <value>The name of the catalog.</value>
        public string CatalogName { get; set; }

        /// <summary>
        /// Gets or sets the name of the typical
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the base class of the typical
        /// </summary>
        /// <value>The name of the base class</value>
        public string BaseClass { get; set; }

        /// <summary>
        /// Gets or sets the description of the typical
        /// </summary>
        /// <value>The description of the typical</value>
        public string Description { get; set; }

        /// <summary>
        /// The attributes of the typical
        /// </summary>
        public List<AttributeDefinition> Attributes = new List<AttributeDefinition>();

        /// <summary>
        /// Gets the attributes list.
        /// </summary>
        /// <value>The attributes list.</value>
        public List<IMetadataAttribute> AttributesList
        {
            get { return Attributes.Cast<IMetadataAttribute>().ToList(); }
        }

        /// <summary>
        /// Determine whether the multiple value fields are expanded
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public bool IsMultipleValueFieldsExpand { get; private set; }

        #region Constructors


        /// <summary>
        /// Creates on new instance of the typical definition
        /// </summary>
        public TypicalDefinition() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="TypicalDefinition"/> class.
        /// </summary>
        /// <param name="table">The table.</param>
        public TypicalDefinition(DataTable table)
        {
            Name = table.TableName;

            if (table.ExtendedProperties.Contains("Catalog"))
            {
                CatalogName = (string)table.ExtendedProperties["Catalog"];
            }

            if (table.ExtendedProperties.Contains("BaseClass"))
            {
                BaseClass = (string)table.ExtendedProperties["BaseClass"];
            }

            if (table.ExtendedProperties.Contains("Description"))
            {
                Description = (string)table.ExtendedProperties["Description"];
            }

            foreach (DataColumn dataColumn in table.Columns)
            {
                if (dataColumn.ColumnName == "dataroot_Id")
                {
                    continue;
                }

                if (BaseClass == "T24_AAProduct")
                {
                    Attributes.Add(new PropertyFieldAttributeDefinition(dataColumn));
                }
                else
                {
                    Attributes.Add(new AttributeDefinition(dataColumn));
                }
            }
        }

        #endregion

        /// <summary>
        /// Converts it to data table.
        /// </summary>
        /// <returns></returns>
        public DataTable ToDataTable()
        {
            DataTable table = new DataTable(Name);
            table.ExtendedProperties["Catalog"] = CatalogName;

            if (!String.IsNullOrEmpty(BaseClass))
                table.ExtendedProperties["BaseClass"] = BaseClass;

            if (!String.IsNullOrEmpty(Description))
                table.ExtendedProperties["Description"] = Description;

            foreach (AttributeDefinition attribute in Attributes)
            {
                table.Columns.Add(attribute.ToDataColumn());
            }
            return table;
        }

        /// <summary>
        /// Expands the multiple value fields (as Validata typicals represent typical attrbute in F-X~Y
        /// </summary>
        public void ExpandMultipleValueFields()
        {
            List<AttributeDefinition> expandedAttributes = new List<AttributeDefinition>();

            foreach (AttributeDefinition oldAttribute in Attributes)
            {
                expandedAttributes.AddRange(oldAttribute.GetExpandedMultipleValue());
            }

            Attributes = expandedAttributes;
            IsMultipleValueFieldsExpand = true;
        }

        /// <summary>
        /// Get normalized typical definition
        /// </summary>
        /// <returns></returns>
        public TypicalDefinition ToNormalized()
        {
            return new TypicalDefinition { CatalogName = this.CatalogName, Name = this.Name, BaseClass = this.BaseClass, Attributes = GetNormalizedFields() };
        }

        /// <summary>
        /// Get typical definition with expanded attributes (MV's and SV's)
        /// </summary>
        /// <returns></returns>
        public TypicalDefinition ToExpanded()
        {
            var result = new TypicalDefinition { CatalogName = this.CatalogName, Name = this.Name, BaseClass = this.BaseClass, Attributes = new List<AttributeDefinition>(this.Attributes) };
            result.ExpandMultipleValueFields();
            return result;
        }

        /// <summary>
        /// Normalize fields by converting several multi value fields to a single field
        /// </summary>
        /// <returns></returns>
        public List<AttributeDefinition> GetNormalizedFields()
        {
            List<string> orderedAttributeNames = new List<string>(
                Attributes.Select(ad => AttributeNameParser.GetShortFieldName(ad.Name)).Distinct());

            Dictionary<string, AttributeDefinition> normalizedAttributes = new Dictionary<string, AttributeDefinition>();

            // Add simple (not-multi value) attributes
            foreach (AttributeDefinition ad in Attributes.Where(ad => !AttributeNameParser.IsMultiValue(ad.Name)))
            {
                normalizedAttributes[ad.Name] = ad;
            }

            //Group multi value attributes by attribute short name
            var groupByName = Attributes.Where(ad => AttributeNameParser.IsMultiValue(ad.Name))
                .GroupBy(ad => AttributeNameParser.GetShortFieldName(ad.Name));

            foreach (IGrouping<string, AttributeDefinition> groupName in groupByName)
            {
                string attrName = groupName.Key;

                var groupByMV = groupName.GroupBy(ad => AttributeNameParser.GetMultiValueIndex(ad.Name).ToString());
                ushort mvCount = (ushort)groupByMV.Count();
                ushort svCountTotal = (ushort)groupByMV.Sum(a => a.Count());
                ushort svCount = (ushort)(svCountTotal / mvCount);

                AttributeDefinition newAttribute = (AttributeDefinition)groupName.FirstOrDefault().Clone();
                newAttribute.Name = attrName;
                newAttribute.IsMultiValue = true;
                newAttribute.MultipleValuesCount = mvCount;
                newAttribute.SuperMultipleValuesCount = svCount;

                normalizedAttributes[attrName] = newAttribute;
            }

            List<AttributeDefinition> result = new List<AttributeDefinition>(normalizedAttributes.Count);

            List<string> problematicAttributes = new List<string>();
            foreach (string attrName in orderedAttributeNames)
            {
                AttributeDefinition attr;
                if (!normalizedAttributes.TryGetValue(attrName, out attr))
                {
                    problematicAttributes.Add(attrName);
                }
                else
                {
                    result.Add(attr);
                }
            }

            if (problematicAttributes.Count > 0)
            {
                throw new ApplicationException("Failed ot construct a compact representation of attributes, due to the following attribute(s): " + string.Join(", ", problematicAttributes));
            }

            if (problematicAttributes.Count > 0)
            {
                throw new ApplicationException("Failed ot construct a compact representation of attributes, due to the following attribute(s): " + string.Join(", ", problematicAttributes));
            }

            return result;
        }

        /// <summary>
        /// Removes the ignored attributes.
        /// </summary>
        public void RemoveIgnoredAttributes()
        {
            List<AttributeDefinition> attributesToBeUpdated = new List<AttributeDefinition>();

            foreach (AttributeDefinition attribute in Attributes)
            {
                if (!attribute.Ignore)
                {
                    attributesToBeUpdated.Add(attribute);
                }
            }

            Attributes = attributesToBeUpdated;
        }

        /// <summary>
        /// Removes the trailing spaces from attributes' names.
        /// </summary>
        public void RemoveAttributesNamesTrailingSpaces()
        {
            List<AttributeDefinition> attributesToBeUpdated = new List<AttributeDefinition>();

            foreach (AttributeDefinition attribute in Attributes)
            {
                if (attribute.Name != null)
                {
                    attribute.Name = attribute.Name.Trim();
                }
                attributesToBeUpdated.Add(attribute);
            }

            Attributes = attributesToBeUpdated;
        }

        /// <summary>
        /// Gets the name of the attributes by normalized.
        /// </summary>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <returns></returns>
        public List<AttributeDefinition> GetAttributesByNormalizedName(string attributeName)
        {
            List<AttributeDefinition> matches = new List<AttributeDefinition>();
            foreach (AttributeDefinition attr in Attributes)
            {
                if (attr.Name == attributeName)
                {
                    matches.Add(attr);
                }
                else if (IsMultiOrSubValueOf(attr.Name, attributeName))
                {
                    matches.Add(attr);
                }
            }

            return matches;
        }

        private static bool IsMultiOrSubValueOf(string multiIrSubValueName, string attributeName)
        {
            string normalizedAttributeName = AttributeNameParser.GetShortFieldName(multiIrSubValueName);
            return attributeName == normalizedAttributeName;
        }

        /// <summary>
        /// Toes the XSD.
        /// </summary>
        /// <returns></returns>
        public string ToXsd()
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                DataSet ds = new DataSet();
                ds.Tables.Add(ToDataTable());
                ds.WriteXmlSchema(sw);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Gets the single fields count.
        /// </summary>
        public int SingleFieldsCount
        {
            get { return Attributes.Count(a => !a.IsMultiValue); }
        }

        /// <summary>
        /// Gets the count of multivalue fields.
        /// </summary>
        public int MultiValueFieldsCount
        {
            get { return Attributes.Count(a => a.IsMultiValue); }
        }

        /// <summary>
        /// Gets the single fields count.
        /// </summary>
        public int TotalExpandedFieldsCount
        {
            get
            {
                int count = 0;
                foreach (AttributeDefinition attr in Attributes)
                {
                    if (attr.IsMultiValue)
                        count += attr.MultipleValuesCount * attr.SuperMultipleValuesCount;
                    else
                        count += 1;
                }

                return count;
            }
        }
        /// <summary>
        /// Xml representation of this typical definition
        /// </summary>
        public string Xml
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_xml))
                    _xml = ToXsd();
                return _xml;
            }
            internal set { _xml = value; }
        }

        #region Overrides

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0} ({1} attributes)", Name, Attributes.Count);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            return Equals(obj as TypicalDefinition, true);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <param name="fullAttributesComparison">Define whether to compare all properties of attributes or essential</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public bool Equals(TypicalDefinition other, bool fullAttributesComparison)
        {
            if (other == null)
                return false;

            if (other.Name != Name)
                return false;

            if (other.CatalogName != CatalogName)
                return false;

            if (Attributes.Count != other.Attributes.Count)
                return false;

            for (int i = 0; i < Attributes.Count; i++)
            {
                if (!Attributes[i].Equals(other.Attributes[i], fullAttributesComparison))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        /// <summary>
        /// compute hash of this typical 
        /// </summary>
        /// <returns></returns>
        public string ComputeHash()
        {
            return new MD5HashManager().ComputeStringHash(Xml);
        }
        #endregion

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        public string GetComments()
        {
            return _Comments;
        }
        public void SetComments(string comments)
        {
            _Comments = comments ;
        }
    }
}