﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// 
    /// </summary>
    public class IgnoreRuleElement : RuleElement
    {
        private String _Value = "*";


        public String Value
        {
            get { return _Value; }
            set { _Value = String.IsNullOrEmpty(value) ? "*" : value; }
        }

        /// <summary>
        /// For serialization
        /// </summary>
        public IgnoreRuleElement()
            :base()
        {
            
        }

        /// <summary>
        /// Data constructor - initialize fields
        /// </summary>
        /// <param name="catalogName"></param>
        /// <param name="typicalName"></param>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        public IgnoreRuleElement (string catalogName, string typicalName, string fieldName, string value)
            : base(catalogName, typicalName, fieldName)
        {
            Value = value; 
        }

        public bool CheckValueElement(RuleElement element, string value)
        {
            try
            {
                bool bBaseEqual = CheckRuleElement(element);
                if (!bBaseEqual)
                    return false;
                if (Value.Equals("*"))
                    return bBaseEqual;
                if (Value.EndsWith("*"))
                    return value.StartsWith(Value.Remove(Value.Length - 1));
                if (Value.StartsWith("*"))
                    return value.EndsWith(Value.Remove(0));
                return (Value.Equals(value));
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
