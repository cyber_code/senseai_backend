﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ValidataCommon
{
    /// <summary>
    /// Class to help reading/writing BLOB values to text files (like CSV)
    /// </summary>
    public static class BinHexHelper
    {
        #region Conversion to BinHex respresentation

        /// <summary>
        /// Writes the bin hex.
        /// </summary>
        /// <param name="valueToEncode">The value to be encoded as bin hex.</param>
        public static string WriteBinHexString(string valueToEncode)
        {
            StringBuilder sb = new StringBuilder();
            byte[] buff = Encoding.UTF8.GetBytes(valueToEncode);
            for (int i = 0; i < buff.Length; i++)
            {
                sb.Append(EncodedByteAsBinHex(buff[i]));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Writes the bin hex.
        /// </summary>
        /// <param name="sw">The sw.</param>
        /// <param name="buffer">The buffer.</param>
        public static void WriteBinHex(StreamWriter sw, byte[] buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                sw.Write(EncodedByteAsBinHex(buffer[i]));
            }
        }

        /// <summary>
        /// Gets the bin hex.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <returns></returns>
        public static string GetBinHex(byte[] buffer)
        {
            if (buffer == null)
            {
                return "";
            }

            StringBuilder sb = new StringBuilder(buffer.Length*2);
            foreach (byte b in buffer)
            {
                sb.Append(EncodedByteAsBinHex(b));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Encodeds the byte as bin hex.
        /// </summary>
        /// <param name="b">The b.</param>
        /// <returns></returns>
        public static string EncodedByteAsBinHex(byte b)
        {
            return b.ToString("X2", CultureInfo.InvariantCulture);
        }

        #endregion

        #region Conversion from BinHex respresentation

        /// <summary>
        /// Gets the string value from bin hex.
        /// </summary>
        /// <param name="binHexValue">The bin hex value.</param>
        /// <returns></returns>
        public static string GetStringValueFromBinHex(string binHexValue)
        {
            byte[] blobValue = GetBinHexValue(binHexValue);
            return Encoding.UTF8.GetString(blobValue);
        }

        /// <summary>
        /// Gets the bin hex value (to save in the database as bytes)
        /// </summary>
        /// <param name="val">The val.</param>
        /// <returns></returns>
        public static byte[] GetBinHexValue(string val)
        {
            val = val.Trim(); //TODO why do we trim it? isn't it dangerous to trim a valid char?

            if ((val.Length%2) != 0)
            {
                throw new InvalidOperationException(
                    string.Format("The string '{0}' is not properly hex encoded. It has and odd number of characters", val));
            }

            int len = val.Length/2;

            byte[] result = new byte[len];
            for (int i = 0; i < len; i++)
            {
                string hexVal = val.Substring(i*2, 2);
                try
                {
                    result[i] = byte.Parse(hexVal, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                }
                catch (FormatException ex)
                {
                    throw new InvalidOperationException(string.Format("The string '{0}' does note appear to be a valid hex number", hexVal), ex);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the MD5 value for a string
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string GetMD5Value(string value)
        {
            byte[] bs = Encoding.UTF8.GetBytes(value);
            return ComputeMD5Hash(bs);
        }

        /// <summary>
        /// Computes the MD5 hash.
        /// </summary>
        /// <returns></returns>
        public static string ComputeMD5Hash(byte[] bytes)
        {
            if (bytes == null)
            {
                return string.Empty;
            }

            byte[] hash = new MD5CryptoServiceProvider().ComputeHash(bytes);
            StringBuilder s = new StringBuilder();
            foreach (byte b in hash)
            {
                s.Append(b.ToString("x2"));
            }

            return s.ToString();
        }

        #endregion

        /// <summary>
        /// Are the bytes the same.
        /// </summary>
        /// <param name="array1">The array1.</param>
        /// <param name="array2">The array2.</param>
        /// <returns></returns>
        public static bool AreBytesTheSame(byte[] array1, byte[] array2)
        {
            int len1 = GetArrayLengthSafe(array1);
            int len2 = GetArrayLengthSafe(array2);

            if (len1 != len2)
            {
                return false;
            }

            for (int i = 0; i < len1; i++)
            {
                if (array1[i] != array2[i])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the array length safe.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <returns></returns>
        private static int GetArrayLengthSafe(byte[] array)
        {
            if (array == null)
            {
                return 0;
            }
            else
            {
                return array.Length;
            }
        }
    }
}