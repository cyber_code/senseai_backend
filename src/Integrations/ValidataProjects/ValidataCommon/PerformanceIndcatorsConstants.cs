namespace ValidataCommon
{
    /// <summary>
    /// Constants related to performance indicators
    /// </summary>
    public static class PerformanceIndcatorsConstants
    {
        /// <summary>
        /// Aggregate functions for peformance indicators
        /// </summary>
        public static class AggregateFunctions
        {
            /// <summary>
            /// Name of the aggregate function for MAX values
            /// </summary>
            public const string Max = "MAX";

            /// <summary>
            /// Name of the aggregate function for MIN values
            /// </summary>
            public const string Min = "MIN";

            /// <summary>
            /// Name of the aggregate function for AVG values
            /// </summary>
            public const string Average = "AVG";
        }
    }
}