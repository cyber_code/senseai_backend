﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ValidataCommon
{
    public class CompareFileByDate : IComparer
    {
        int IComparer.Compare(Object a, Object b)
        {
            FileInfo fia = new FileInfo((string)a);
            FileInfo fib = new FileInfo((string)b);

            DateTime cta = fia.LastWriteTime;
            DateTime ctb = fib.LastWriteTime;

            return DateTime.Compare(cta, ctb);
        }
    }
}
