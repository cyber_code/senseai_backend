﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class TagStructure
    {
        #region Constants

        public const string DefaultTagStructureDelimiter = "; ";

        public const char ASCII_StartOfText = (char) 0x02;

        public const char ASCII_EndOfText = (char) 0x03;

        public const string CompanyAttributeName = "CO.CODE";

        #endregion

        #region Properties

        public ulong Noderef { get; private set; }

        public string DisplayDelimiter { get; private set; }

        public string[] Atttributes { get; private set; }

        #endregion

        #region Class Lifecycle

        public TagStructure(ulong noderef, string displayDelimiter, string[] attributes)
        {
            Noderef = noderef;
            DisplayDelimiter = displayDelimiter;
            if (string.IsNullOrEmpty(DisplayDelimiter))
            {
                DisplayDelimiter = DefaultTagStructureDelimiter;
            }

            if (attributes == null || attributes.Length == 0)
            {
                throw new Exception("Unable to create tag structure object without attributes!");
            }

            Atttributes = attributes;
        }

        #endregion

        #region Convert

        public override string ToString()
        {
            string result = DisplayDelimiter;
            result += ASCII_StartOfText;
            foreach (string s in Atttributes)
            {
                result += s;
                result += ASCII_EndOfText;
            }

            return result;
        }

        public static TagStructure FromString(string txt)
        {
            try
            {
                string[] strs = txt.Split(new char[] {ASCII_StartOfText}, StringSplitOptions.RemoveEmptyEntries);
                string delimiter = null;
                string values;
                switch (strs.Length)
                {
                    case 0:
                        return null;
                    case 1:
                        values = strs[0];
                        break;
                    case 2:
                    default:
                        delimiter = strs[0];
                        values = strs[1];
                        break;
                }

                string[] strsValues = values.Split(new char[] {ASCII_EndOfText}, StringSplitOptions.RemoveEmptyEntries);
                if (strsValues.Length == 0)
                {
                    return null;
                }

                return new TagStructure(0, delimiter, strsValues);
            }
            catch
            {
                return null;
            }
        }

        public static string CalculateRecordName(Dictionary<string, string> attributes, TagStructure tagStructure)
        {
            StringBuilder res = new StringBuilder();
            for (int i = 0; i < tagStructure.Atttributes.Length; i++)
            {
                res.Append(
                    attributes.ContainsKey(tagStructure.Atttributes[i])
                        ? attributes[tagStructure.Atttributes[i]]
                        : "No Value"
                    );
                if (i < (tagStructure.Atttributes.Length - 1))
                {
                    res.Append(tagStructure.DisplayDelimiter);
                }
            }

            if (res.ToString() == string.Empty)
            {
                res.Append("No Tag Structure");
            }

            return res.ToString();
        }

        public static string CalculateRecordName(Dictionary<string, string> attributes, string[] tagStructureAttributes, string displayDelimiter)
        {
            return CalculateRecordName(
                attributes,
                new TagStructure(0, displayDelimiter, tagStructureAttributes)
                );
        }

        public static string GetCompany(Dictionary<string, string> attributes)
        {
            if (!attributes.ContainsKey(CompanyAttributeName))
            {
                return null;
            }

            return attributes[CompanyAttributeName];
        }

        #endregion
    }
}