﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    //public enum LengthCheckObject
    //{
    //    BCON,
    //    RI,
    //    WP,
    //    CR,
    //    Task,
    //    Packet
    //}

    public static class Helper
    {
        public enum LengthCheckObject
        {
            BCON,
            RI,
            WP,
            CR,
            Task,
            Packet
        }


        public const string PACK_PLACEHOLDER = "Packet ";
        public const string LENGTHS_PACK_DELIMITER = "|";

        #region sequencing

        public static string getNextSequenceForPrefix(string taskPrefix, IEnumerable<string> peers, int minLen)
        {
            int biggestNum = 0;
            int nameLen = minLen;

            if (peers != null)
                foreach (var unitName in peers)
                {
                    if (unitName.ToLower().StartsWith(taskPrefix.ToLower()) && unitName.Length > taskPrefix.Length)
                    {
                        int lastSeq;
                        var unitSeq = unitName.Substring(taskPrefix.Length);

                        if (int.TryParse(unitSeq, out lastSeq) && lastSeq > biggestNum)
                        {
                            biggestNum = lastSeq;
                            if (unitSeq.Length > nameLen)
                                nameLen = unitSeq.Length;
                        }
                    }
                }

            return (biggestNum + 1).ToString().PadLeft(nameLen, '0');
        }

        #endregion

        public static string getPacketName(string taskFullName, string nextSeq)
        {
            // TODO: impolement with autocalc..

            // rs char ri name . wp name . cr name . task name nextseq
            return string.Format("{0}{1}", taskFullName, nextSeq);
        }

        public static string getTaskNameByConvention(string rsName, string riName, string wpName, string crName, string taskName)
        {
            // TODO: implement with autocalc..

            // rs char ri name . wp name . cr name . task name nextseq
            return string.Format("{0}{1}.{2}.{3}.{4}", rsName[0], riName, wpName, crName, taskName);
        }

        public static string generateCRName(string prefix, string nextSeq, string originalName)
        {
            // TODO: implement with autocalc..

            return string.Format("{0}{1}", prefix, nextSeq);
        }
        public static string generateTaskName(string prefix, string nextSeq, string originalName)
        {
            // TODO: implement with autocalc..

            return string.Format("{0}{1}", prefix, nextSeq);
        }

        #region not used:
        
        //public static List<string> getCandidatePacketsForTask(List<string> peerPackets, string defaultTaskName)
        //{
        //    List<string> taskPackets = new List<string>();

        //    foreach (var peerPack in peerPackets)
        //    {
        //        if (peerPack == null) continue;

        //        string[] packNameParts = peerPack.Split(new char[] { '.' }, StringSplitOptions.None);

        //        // CHAR  RI DIGITS . WP DIGITS . CR DIGITS . T DIGITS PACK DIGITS
        //        if (packNameParts.Length == 4)
        //        {
        //            string taskPlusPack = packNameParts[3];
        //            if (taskPlusPack.StartsWith(defaultTaskName) && taskPlusPack.Length > defaultTaskName.Length)
        //            {
        //                string pckSeq = taskPlusPack.Substring(defaultTaskName.Length);
        //                int pckNum;
        //                if (int.TryParse(pckSeq, out pckNum))
        //                {
        //                    taskPackets.Add(pckSeq);
        //                }
        //            }
        //        }
        //    }

        //    return taskPackets;

        //}

        //public static string getNextSequence( IEnumerable<string> peers, int minLen)
        //{
            
        //    int biggestNum = 0;
        //    int nameLen = minLen;

        //    if (peers != null)
        //        foreach (var unitName in peers)
        //        {
        //            int lastSeq;
        //            var name = unitName;

        //            if (int.TryParse(name, out lastSeq) && lastSeq > biggestNum)
        //            {
        //                biggestNum = lastSeq;
        //                if (name.Length > nameLen)
        //                    nameLen = name.Length;
        //            }
        //        }

        //    return (biggestNum + 1).ToString().PadLeft(nameLen, '0');
        //}

        #endregion



        public static string generateProjectName(string type, string taskName)
        {
            // TODO: implement with autocalc..

            char projectSuffix = 'P';
            if (type.Length > 0)
                projectSuffix = type[0];

            return string.Format("{0}{1}", taskName, projectSuffix);
        }

        public static string getTaskHeirarchyName(string wpName, string crName, string taskName)
        {
            return string.Format("{0}.{1}.{2}", wpName, crName, taskName);
        }

        public static string getOutPackedLengths(string packedDescription, out int minCRlen, out int minTaskLen)
        {
            minCRlen = 0;
            minTaskLen = 0;
            string newdesc = packedDescription;

            var parts = packedDescription.Split(new string[] { LENGTHS_PACK_DELIMITER }, StringSplitOptions.None);
            if (parts.Length == 3)
            {
                newdesc = parts[0];
                int.TryParse(parts[1], out minCRlen);
                int.TryParse(parts[2], out minTaskLen);
            }
            return newdesc;
        }

        public static string getInPackedLengths(string originalDescription, int minCRlen, int minTaskLen)
        {
            return string.Format("{0}{1}{2}{3}{4}", originalDescription, LENGTHS_PACK_DELIMITER, minCRlen.ToString(), LENGTHS_PACK_DELIMITER, minTaskLen.ToString() );
        }
    }
}

