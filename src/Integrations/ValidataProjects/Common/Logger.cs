﻿using System;
using System.Collections.Generic;
using System.IO;
using log4net;
using log4net.Config;
using log4net.Core;

namespace Common
{
    public class Logger : ILog
    {
        public enum LogEventType
        {
            Debug = 0,
            Assert,
            Error,
            Fatal,
            Warn,
            Info
        }

        public delegate void LogEventHandle(LogEventType logEventType, string message);

        public event LogEventHandle OnLogEvent;

        public static string ConfigFile { get; set; }

        private readonly ILog _log; // = LogManager.GetLogger(typeof (Logger));

        public bool IsDebugEnabled
        {
            get { return _log.IsDebugEnabled; }
        }

        public bool IsErrorEnabled
        {
            get { return _log.IsErrorEnabled; }
        }

        public bool IsFatalEnabled
        {
            get { return _log.IsFatalEnabled; }
        }

        public bool IsInfoEnabled
        {
            get { return _log.IsInfoEnabled; }
        }

        public bool IsWarnEnabled
        {
            get { return _log.IsWarnEnabled; }
        }

        private Logger()
        {
            string configFile = "Log4Net.config";
            if (!string.IsNullOrEmpty(ConfigFile))
                configFile = ConfigFile;

            XmlConfigurator.Configure(new FileInfo(configFile));
            _log = LogManager.GetLogger("Validata.SAS.System");
        }

        private Logger(string loggerName)
        {
            _log = LogManager.GetLogger(loggerName);
        }

        private class LoggerCreator
        {
            static LoggerCreator()
            {
            }

            internal static readonly Logger uniqueInstance = new Logger();
        }

        public static Logger UniqueInstance
        {
            get { return LoggerCreator.uniqueInstance; }
        }

        private static readonly Dictionary<string, Logger> _loggers = new Dictionary<string, Logger>();

        public Logger this[string logger]
        {
            get
            {
                if (_loggers.ContainsKey(logger))
                    return _loggers[logger];

                var loggerObject = new Logger(logger);
                _loggers.Add(logger, loggerObject);
                return loggerObject;
            }
        }

        #region ILog Members

        #region Debug

        public void Debug(object message, Exception exception)
        {
            FireLogEvent(LogEventType.Debug, message, exception);
            _log.Debug(message, exception);
        }

        public void Debug(object message)
        {
            FireLogEvent(LogEventType.Debug, message);
            _log.Debug(message);
        }

        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            FireLogEvent(LogEventType.Debug, provider, format, args);
            _log.DebugFormat(provider, format, args);
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            FireLogEvent(LogEventType.Debug, format, arg0, arg1, arg2);
            _log.DebugFormat(format, arg0, arg1, arg2);
        }

        public void DebugFormat(string format, object arg0, object arg1)
        {
            FireLogEvent(LogEventType.Debug, format, arg0, arg1);
            _log.DebugFormat(format, arg0, arg1);
        }

        public void DebugFormat(string format, object arg0)
        {
            FireLogEvent(LogEventType.Debug, format, arg0);
            _log.DebugFormat(format, arg0);
        }

        public void DebugFormat(string format, params object[] args)
        {
            FireLogEvent(LogEventType.Debug, format, args);
            _log.DebugFormat(format, args);
        }

        #endregion

        #region Errors

        public void Error(object message, Exception exception)
        {
            FireLogEvent(LogEventType.Error, message, exception);
            _log.Error(message, exception);
        }

        public void Error(object message)
        {
            FireLogEvent(LogEventType.Error, message);
            _log.Error(message);
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            FireLogEvent(LogEventType.Error, provider, format, args);
            _log.ErrorFormat(provider, format, args);
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            FireLogEvent(LogEventType.Error, format, arg0, arg1, arg2);
            _log.ErrorFormat(format, arg0, arg1, arg2);
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
            FireLogEvent(LogEventType.Error, format, arg0, arg1);
            _log.ErrorFormat(format, arg0, arg1);
        }

        public void ErrorFormat(string format, object arg0)
        {
            FireLogEvent(LogEventType.Error, format, arg0);
            _log.ErrorFormat(format, arg0);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            FireLogEvent(LogEventType.Error, format, args);
            _log.ErrorFormat(format, args);
        }

        #endregion

        #region Fatal

        public void Fatal(object message, Exception exception)
        {
            FireLogEvent(LogEventType.Fatal, message, exception);
            _log.Fatal(message, exception);
        }

        public void Fatal(object message)
        {
            FireLogEvent(LogEventType.Fatal, message);
            _log.Fatal(message);
        }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            FireLogEvent(LogEventType.Fatal, provider, format, args);
            _log.FatalFormat(provider, format, args);
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            FireLogEvent(LogEventType.Fatal, format, arg0, arg1, arg2);
            _log.FatalFormat(format, arg0, arg1, arg2);
        }

        public void FatalFormat(string format, object arg0, object arg1)
        {
            FireLogEvent(LogEventType.Fatal, format, arg0, arg1);
            _log.FatalFormat(format, arg0, arg1);
        }

        public void FatalFormat(string format, object arg0)
        {
            FireLogEvent(LogEventType.Fatal, format, arg0);
            _log.FatalFormat(format, arg0);
        }

        public void FatalFormat(string format, params object[] args)
        {
            FireLogEvent(LogEventType.Fatal, format, args);
            _log.FatalFormat(format, args);
        }

        #endregion

        #region Info

        public void Info(object message, Exception exception)
        {
            FireLogEvent(LogEventType.Info, message, exception);
            _log.Info(message, exception);
        }

        public void Info(object message)
        {
            FireLogEvent(LogEventType.Info, message);
            _log.Info(message);
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            FireLogEvent(LogEventType.Info, provider, format, args);
            _log.InfoFormat(provider, format, args);
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            FireLogEvent(LogEventType.Info, format, arg0, arg1, arg2);
            _log.InfoFormat(format, arg0, arg1, arg2);
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
            FireLogEvent(LogEventType.Info, format, arg0, arg1);
            _log.InfoFormat(format, arg0, arg1);
        }

        public void InfoFormat(string format, object arg0)
        {
            FireLogEvent(LogEventType.Info, format, arg0);
            _log.InfoFormat(format, arg0);
        }

        public void InfoFormat(string format, params object[] args)
        {
            FireLogEvent(LogEventType.Info, format, args);
            _log.InfoFormat(format, args);
        }

        #endregion

        #region Warn

        public void Warn(object message, Exception exception)
        {
            FireLogEvent(LogEventType.Warn, message, exception);
            _log.Warn(message, exception);
        }

        public void Warn(object message)
        {
            FireLogEvent(LogEventType.Warn, message);
            _log.Warn(message);
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            FireLogEvent(LogEventType.Warn, provider, format, args);
            _log.WarnFormat(provider, format, args);
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            FireLogEvent(LogEventType.Warn, format, arg0, arg1, arg2);
            _log.WarnFormat(format, arg0, arg1, arg2);
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
            FireLogEvent(LogEventType.Warn, format, arg0, arg1);
            _log.WarnFormat(format, arg0, arg1);
        }

        public void WarnFormat(string format, object arg0)
        {
            FireLogEvent(LogEventType.Warn, format, arg0);
            _log.WarnFormat(format, arg0);
        }

        public void WarnFormat(string format, params object[] args)
        {
            FireLogEvent(LogEventType.Warn, format, args);
            _log.WarnFormat(format, args);
        }

        #endregion

        #endregion

        #region ILoggerWrapper Members

        ILogger ILoggerWrapper.Logger
        {
            get { return _log.Logger; }
        }

        #endregion

        #region Asserts

        public void Assert(bool test, object errorMessage)
        {
            string assertMessage = string.Empty;
            if (errorMessage is string)
                assertMessage = (string) errorMessage;

            Assert(test, errorMessage, assertMessage);
        }

        public void Assert(bool test, object errorMessage, string assertMessage)
        {
            if (!test)
                Error("Assert> " + errorMessage);

            Assert(test, assertMessage);
        }

        public void Assert(bool test, object errorMessage, Exception exception)
        {
            string assertMessage = string.Empty;
            if (errorMessage is string)
                assertMessage = (string) errorMessage;

            Assert(test, errorMessage, assertMessage, exception);
        }

        public void Assert(bool test, object errorMessage, string assertMessage, Exception exception)
        {
            if (!test)
                Error("Assert> " + errorMessage, exception);

            Assert(test, assertMessage);
        }

        public void Assert(bool test, string message)
        {
            if (!test)
            {
                FireLogEvent(LogEventType.Assert, message);
            }
            else
            {
                System.Diagnostics.Debug.Assert(test, message);
            }
        }

        #endregion

        #region Private Fire Event

        private void FireLogEvent(LogEventType logEventType, IFormatProvider provider, string format, object[] args)
        {
            FireLogEvent(logEventType, string.Format(provider, format, args));
        }

        private void FireLogEvent(LogEventType logEventType, string format, object arg0, object arg1, object arg2)
        {
            FireLogEvent(logEventType, string.Format(format, arg0, arg1, arg2));
        }

        private void FireLogEvent(LogEventType logEventType, string format, object arg0, object arg1)
        {
            FireLogEvent(logEventType, string.Format(format, arg0, arg1));
        }

        private void FireLogEvent(LogEventType logEventType, string format, object arg0)
        {
            FireLogEvent(logEventType, string.Format(format, arg0));
        }

        private void FireLogEvent(LogEventType logEventType, string format, object[] args)
        {
            FireLogEvent(logEventType, string.Format(format, args));
        }

        private void FireLogEvent(LogEventType logEventType, object message, Exception exception)
        {
            FireLogEvent(logEventType, string.Format("{0}\r\n{1}", message, exception));
        }

        private void FireLogEvent(LogEventType logEventType, object message)
        {
            if (OnLogEvent != null)
            {
                OnLogEvent(logEventType, message != null ? message.ToString() : "");
            }
        }

        #endregion
    }
}