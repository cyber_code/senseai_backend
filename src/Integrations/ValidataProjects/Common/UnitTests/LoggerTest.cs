﻿using System;
using System.IO;
using NUnit.Framework;

namespace Common.UnitTests
{
    [TestFixture]
    [Explicit]
    public class LoggerTest
    {
        private static Logger LoggerInstance
        {
            get { return Logger.UniqueInstance; }
        }

        [Test]
        public void WriteDebugToLog()
        {
            LoggerInstance.Debug("Test debug text");

            File.Copy(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%")+"example.log", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%")+"example.tmp");
            string[] lines = File.ReadAllLines(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%")+"example.tmp");
            File.Copy(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.log", "C:\\example.tmp");
          

            Assert.AreEqual("DEBUG - Test debug text", lines[lines.Length - 1]);
            File.Delete(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%")+"example.tmp");
            
        }

        [Test]
        public void WriteInfoToLog()
        {
            LoggerInstance.Info("Test info text");

            File.Copy(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.log", "C:\\example.tmp");
            string[] lines = File.ReadAllLines(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");

            Assert.AreEqual("INFO - Test info text", lines[lines.Length - 1]);
            File.Delete(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");
        }

        [Test]
        public void WriteWarnToLog()
        {
            LoggerInstance.Warn("Test warning text");

            File.Copy(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.log", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");
            string[] lines = File.ReadAllLines(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");

            Assert.AreEqual("WARN - Test warning text", lines[lines.Length - 1]);
            File.Delete(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");
        }

        [Test]
        public void WriteErrorToLog()
        {
            LoggerInstance.Error("Test error text");

            File.Copy(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.log", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");
            string[] lines = File.ReadAllLines(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");

            Assert.AreEqual("ERROR - Test error text", lines[lines.Length - 1]);
            File.Delete(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");
        }

        [Test]
        public void WriteFatalToLog()
        {
            LoggerInstance.Fatal("Test fatal text");

            File.Copy(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.log", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");
            string[] lines = File.ReadAllLines(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");

            Assert.AreEqual("FATAL - Test fatal text", lines[lines.Length - 1]);
            File.Delete(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "example.tmp");
        }
    }
}