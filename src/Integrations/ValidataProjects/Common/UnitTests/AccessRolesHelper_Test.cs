﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using System.IO;
using log4net.Core;

namespace Common.UnitTests
{
    [TestFixture]
    public class AccessRolesHelper_Test
    {
        [Test]
        public void Test_AccessRolesHelper()
        {
            AccessRolesHelper helper = new AccessRolesHelper();
            helper.AddDiscipline("d-1",1);
            helper.AddRole("d-1",1, "r-1-1", 2);
            helper.AddRole("d-1", 1, "r-1-2", 3);
            helper.AddUser("d-1", 1, "r-1-2", 3, "u-1-2-1", 4);
            helper.AddUser("d-1", 1, "r-1-2", 3, "u-1-2-2", 5);
            helper.AddUser("d-1", 1, "r-1-2", 3, "u-1-2-3", 6);

            helper.AddDiscipline("d-2", 10);
            helper.AddRole("d-2", 10, "r-2-1", 11);
            helper.AddUser("d-2", 10, "r-2-1", 11, "u-2-1-1", 12);

            Test_AccessRolesHelper_Validate(helper);

            string s = AccessRolesHelper.ToString(helper);
            AccessRolesHelper helper1 = AccessRolesHelper.FromString(s);

            Test_AccessRolesHelper_Validate(helper1);
        }

        private void Test_AccessRolesHelper_Validate(AccessRolesHelper hlp)
        {
            Assert.IsTrue(hlp.GetDisciplineNames().Count<string>() == 2);
            Assert.IsTrue(hlp.GetDisciplineNames().Contains<string>("d-1"));
            Assert.IsTrue(hlp.GetRoleNames("d-1").Count<string>() == 2);
            Assert.IsTrue(hlp.GetRoleNames("d-1").Contains<string>("r-1-1"));
            Assert.IsTrue(hlp.GetRoleNames("d-1").Contains<string>("r-1-2"));
            Assert.IsTrue(hlp.GetUserNames("d-1", "r-1-2").Count<string>() == 3);
            Assert.IsTrue(hlp.GetUserNames("d-1", "r-1-2").Contains<string>("u-1-2-1"));
            Assert.IsTrue(hlp.GetUserNames("d-1", "r-1-2").Contains<string>("u-1-2-2"));
            Assert.IsTrue(hlp.GetUserNames("d-1", "r-1-2").Contains<string>("u-1-2-3"));

            Assert.IsTrue(hlp.GetDisciplineNames().Contains<string>("d-2"));
            Assert.IsTrue(hlp.GetRoleNames("d-2").Count<string>() == 1);
            Assert.IsTrue(hlp.GetRoleNames("d-2").Contains<string>("r-2-1"));
            Assert.IsTrue(hlp.GetUserNames("d-2", "r-2-1").Count<string>() == 1);
            Assert.IsTrue(hlp.GetUserNames("d-2", "r-2-1").Contains<string>("u-2-1-1"));
        }
    }
}

