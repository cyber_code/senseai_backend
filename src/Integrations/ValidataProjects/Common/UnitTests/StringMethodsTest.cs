﻿using System;
using NUnit.Framework;
using System.IO;
using ValidataCommon;


namespace Common.UnitTests
{
    [TestFixture]
    public class StringMethodsTest
    {
        [Test]
        public void CompareWildcards()
        {
            bool res = false;

            res = StringMethods.Path.CompareWildcards(null, "*", true);
            Assert.IsTrue(res);

            res = StringMethods.Path.CompareWildcards("niki", (string)null, true);
            Assert.IsTrue(!res);

            res = StringMethods.Path.CompareWildcards(null, (string[])null, true);
            Assert.IsTrue(!res);

            res = StringMethods.Path.CompareWildcards("niki", new string[] { }, true);
            Assert.IsTrue(!res);

            res = StringMethods.Path.CompareWildcards("niki", new string[] { "aaa", null }, true);
            Assert.IsTrue(!res);

            res = StringMethods.Path.CompareWildcards(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "lib\\bib", "*ib\\bI?", true);
            Assert.IsTrue(res);

            res = StringMethods.Path.CompareWildcards(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "lib\\bib", "*ib\\bI?", false);
            Assert.IsTrue(!res);

            res = StringMethods.Path.CompareWildcards(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "lib\\bib\\niki.oooo", "*.oooo", true);
            Assert.IsTrue(res);

            res = StringMethods.Path.CompareWildcards(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "lib\\bib\\niki.vv", "*.niki; *.vv", true);
            Assert.IsTrue(res);

            res = StringMethods.Path.CompareWildcards(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "lib\\bib\\niki.vv", "*.niki;", true);
            Assert.IsTrue(!res);

            res = StringMethods.Path.CompareWildcard("/homenew/MIGVB03/bnk.run/lib/lib50.so.136", "*.so", true);
            Assert.IsTrue(!res);
        }

        [Test]
        public void CompareWildcardsWithRule()
        {
            bool res;
            res = StringMethods.Path.CompareWildcards("CUSTOMER I", "CUSTO?ER I", true);
            Assert.IsTrue(res);

        }
    }
}
