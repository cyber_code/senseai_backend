﻿using System;
using NUnit.Framework;
using System.IO;


namespace Common.UnitTests
{
    [TestFixture]
    public class FolderMapTest
    {
        [Test]
        public void SerializeDeserialize()
        {
            FolderMapList mpList = new FolderMapList();
            mpList.Add(new FolderMap("F/*", "../0/Target", FolderMap.Actions.Catalog));
            mpList.Add(new FolderMap("D/*", "../1/Target", FolderMap.Actions.Catalog));
            mpList.Add(new FolderMap("E/*", "../2/Target", FolderMap.Actions.Catalog));
            mpList.Add(new FolderMap("C/*", "../3/Target", FolderMap.Actions.Catalog));
            mpList.Add(new FolderMap("B/*", "../4/Target", FolderMap.Actions.Catalog));

            string toXML = mpList.ToXML();
            FolderMapList fmpList = FolderMapList.FromXML(toXML);            
        }
    }
}
