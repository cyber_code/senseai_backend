﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace Common
{
    public class OperationLog
    {
        public OperationLog(string name, bool delayedStart, bool complexOperation)
        {
            Name = name;
            if (!delayedStart)
            {
                Start();
            }

            if (complexOperation)
            {
                _SubOperations = new List<OperationLog>();
            }
        }

        public string Name
        {
            get;
            set;
        }

        public TimeSpan Length { get; set; }

        private DateTime _Start = DateTime.MinValue;

        public void Start()
        {
            _Start = DateTime.UtcNow;
        }

        public static readonly ILog Log = LogManager.GetLogger("Validata.SAS.System");


        public TimeSpan End()
        {
            if (_Start == DateTime.MinValue)
                throw new ApplicationException("OperationLog.End called without previous call of Start");

            Length = DateTime.UtcNow - _Start;
            Log.InfoFormat("Operation {0} took {1} ms", Name, Length.TotalMilliseconds);
            return Length;
        }

        private List<OperationLog> _SubOperations;

        public List<OperationLog> SubOperations
        {
            get
            {
                if (_SubOperations == null)
                    throw new ApplicationException("OperationLog.SubOperations called for a single operation");
                return _SubOperations;
            }
        }

        public TimeSpan SumOfSubOperations()
        {
            TimeSpan total = new TimeSpan(0);
            foreach (OperationLog operationLog in SubOperations)
            {
                total += operationLog.Length;
            }
            return total;
        }
    }
}

