﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Common
{
    [Serializable]
    public class AccessRolesHelper
    {
        #region Helper Classes

        private static class ASCII
        {
            internal const char SOH = (char) 0x01;

            internal const char STX = (char)0x02;

            internal const char ETX = (char)0x03;

            internal const char EOT = (char)0x04;

            internal const char NL = (char)0x0A;
        }

        public struct AccessItem
        {
            public string Name;

            public ulong Noderef;

            public override string ToString()
            {
                return string.Format("{0}:{1}", Noderef, Name);
            }

            public static AccessItem FromString(string str)
            {
                int pos = str.IndexOf(':');
                return new AccessItem()
                           {
                               Noderef = ulong.Parse(str.Substring(0, pos)),
                               Name = str.Substring(pos + 1),
                           };
            }
        }

        private class ListUsers : List<AccessItem> {}

        private class ListRoles : Dictionary<AccessItem, ListUsers>
        {
            internal ListUsers FindByName(string name)
            {
                foreach (AccessItem ui in Keys)
                {
                    if (ui.Name == name)
                    {
                        return this[ui];
                    }
                }

                return null;
            }
        }

        private class ListDisciplines : Dictionary<AccessItem, ListRoles>
        {
            internal ListRoles FindByName(string name)
            {
                foreach (AccessItem ui in Keys)
                {
                    if (ui.Name == name)
                    {
                        return this[ui];
                    }
                }

                return null;
            }
        }

        #endregion

        #region Private Members

        private readonly ListDisciplines _ListDisciplines = new ListDisciplines();

        #endregion

        #region Class Lifecycle

        public AccessRolesHelper() {}

        #endregion

        #region Public Methods

        public AccessItem AddDiscipline(string disciplineName, ulong noderef)
        {
            AccessItem key = new AccessItem() {Name = disciplineName, Noderef = noderef};
            return AddDiscipline(key);
        }

        internal AccessItem AddDiscipline(AccessItem key)
        {
            if (!_ListDisciplines.ContainsKey(key))
            {
                _ListDisciplines.Add(key, new ListRoles());
            }

            return key;
        }

        public AccessItem AddRole(string disciplineName, ulong disciplineNoderef, string roleName, ulong roleNoderef)
        {
            AccessItem disciplineKey = new AccessItem() {Name = disciplineName, Noderef = disciplineNoderef};
            AccessItem key = new AccessItem() {Name = roleName, Noderef = roleNoderef};
            return AddRole(disciplineKey, key);
        }

        internal AccessItem AddRole(AccessItem disciplineKey, AccessItem key)
        {
            AddDiscipline(disciplineKey);

            if (!_ListDisciplines[disciplineKey].ContainsKey(key))
            {
                _ListDisciplines[disciplineKey].Add(key, new ListUsers());
            }

            return key;
        }

        public AccessItem AddUser(string disciplineName,
                                  ulong disciplineNoderef,
                                  string roleName,
                                  ulong roleNoderef,
                                  string userName,
                                  ulong userNoderef)
        {
            AccessItem disciplineKey = new AccessItem() {Name = disciplineName, Noderef = disciplineNoderef};
            AccessItem roleKey = new AccessItem() {Name = roleName, Noderef = roleNoderef};
            AccessItem key = new AccessItem() {Name = userName, Noderef = userNoderef};
            return AddUser(disciplineKey, roleKey, key);
        }

        internal AccessItem AddUser(AccessItem disciplineKey, AccessItem roleKey, AccessItem key)
        {
            AddRole(disciplineKey, roleKey);

            if (!_ListDisciplines[disciplineKey][roleKey].Contains(key))
            {
                _ListDisciplines[disciplineKey][roleKey].Add(key);
            }

            return key;
        }

        public ulong FindRoleNoderef(string disciplineName, string roleName)
        {
            ListRoles roles = _ListDisciplines.FindByName(disciplineName);
            if (roles == null)
            {
                return 0;
            }

            foreach (AccessItem ai in roles.Keys)
            {
                if (ai.Name == roleName)
                {
                    return ai.Noderef;
                }
            }

            return 0;
        }

        public IEnumerable<AccessItem> GetDisciplineItems()
        {
            return _ListDisciplines.Keys;
        }

        public IEnumerable<string> GetDisciplineNames()
        {
            return GetNames(GetDisciplineItems());
        }

        public IEnumerable<AccessItem> GetRoleItems(AccessItem acDiscipline)
        {
            if (!_ListDisciplines.ContainsKey(acDiscipline))
            {
                Debug.Fail("???");
                return new List<AccessItem>();
            }

            return _ListDisciplines[acDiscipline].Keys;
        }

        public IEnumerable<string> GetRoleNames(string disciplineName)
        {
            ListRoles lsRoles = _ListDisciplines.FindByName(disciplineName);

            if (lsRoles == null)
            {
                return new List<string>();
            }

            return GetNames(lsRoles.Keys);
        }

        public IEnumerable<AccessItem> GetUserItems(AccessItem acDiscipline, AccessItem acRole)
        {
            if (!_ListDisciplines.ContainsKey(acDiscipline))
            {
                Debug.Fail("???");
                return new List<AccessItem>();
            }

            if (!_ListDisciplines[acDiscipline].ContainsKey(acRole))
            {
                Debug.Fail("???");
                return new List<AccessItem>();
            }

            return _ListDisciplines[acDiscipline][acRole];
        }

        public IEnumerable<string> GetUserNames(string disciplineName, string roleName)
        {
            ListRoles lsRoles = _ListDisciplines.FindByName(disciplineName);
            if (lsRoles == null)
            {
                return new List<string>();
            }

            ListUsers lsUsers = lsRoles.FindByName(roleName);
            if (lsUsers == null)
            {
                return new List<string>();
            }

            return GetNames(lsUsers);
        }

        #endregion

        #region Public Static Methods

        public static string ToString(AccessRolesHelper arh)
        {
            string result = string.Empty;

            foreach (AccessItem discipline in arh.GetDisciplineItems())
            {
                result += discipline.ToString();
                result += ASCII.STX;

                foreach (AccessItem role in arh.GetRoleItems(discipline))
                {
                    result += role.ToString();
                    result += ASCII.SOH;

                    foreach (AccessItem user in arh.GetUserItems(discipline, role))
                    {
                        result += user.ToString();
                        result += ASCII.NL;
                    }

                    result += ASCII.ETX;
                }

                result += ASCII.EOT;
            }

            return result;
        }

        public static AccessRolesHelper FromString(string str)
        {
            AccessRolesHelper result = new AccessRolesHelper();

            string[] disciplineDefs = str.Split(new char[] {ASCII.EOT}, StringSplitOptions.RemoveEmptyEntries);
            foreach (string disciplineDef in disciplineDefs)
            {
                string[] disciplineParts = disciplineDef.Split(new char[] {ASCII.STX}, StringSplitOptions.RemoveEmptyEntries);
                AccessItem aiDiscipline = result.AddDiscipline(AccessItem.FromString(disciplineParts[0]));
                if (disciplineParts.Length == 1)
                {
                    continue;
                }

                string[] roleDefs = disciplineParts[1].Split(new char[] {ASCII.ETX}, StringSplitOptions.RemoveEmptyEntries);
                foreach (string roleDef in roleDefs)
                {
                    string[] roleParts = roleDef.Split(new char[] {ASCII.SOH}, StringSplitOptions.RemoveEmptyEntries);
                    AccessItem aiRole = result.AddRole(aiDiscipline, AccessItem.FromString(roleParts[0]));
                    if (roleParts.Length == 1)
                    {
                        continue;
                    }

                    string[] users = roleParts[1].Split(new char[] {ASCII.NL}, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string user in users)
                    {
                        result.AddUser(aiDiscipline, aiRole, AccessItem.FromString(user));
                    }
                }
            }

            return result;
        }

        #endregion

        #region Private Static Methods

        private static IEnumerable<string> GetNames(IEnumerable<AccessItem> lsItems)
        {
            return lsItems.Select(ui => ui.Name).ToList();
        }

        #endregion
    }
}