﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;

namespace Common.Riff
{
    public enum ChangeType
    {
        Remove = 0,
        Insert = 1,
        Change = 2
    }

    public class Change
    {
        public Change()
        {
            Changes = new string[0];
        }


        public ChangeType ChgType
        {
            get;
            set;
        }

        public int Start
        {
            get; 
            set;
        }

        public int Size
        {
            get; 
            set;
        }

        public string[] Changes
        {
            get;
            set;
        }

        internal void ApplyOn(IList<string> target, ref int offset)
        {
            switch(ChgType)
            {
                case ChangeType.Change:
                    Remove(target, offset);
                    offset = Insert(target, offset) - Size; // -Size because of the Remove size
                    break;
                case ChangeType.Insert:
                    offset = Insert(target, offset);
                    break;
                case ChangeType.Remove:
                    offset = Remove(target, offset);
                    break;
            }
        }

        private int Remove(IList<string> target, int offset)
        {
            for (int i = 0; i < Size; i++)
            {
                if (target.Count > 0)
                    target.RemoveAt((Math.Min(target.Count - 1, Start + offset)));
            }
            offset -= Size;
            return offset;
        }

        private int Insert(IList<string> target, int offset)
        {
            for (int ind = 0; ind < Changes.Length; ind++ )
            {
                string str = Changes[ind];
                target.Insert(Math.Min(Start + offset + ind, target.Count), str);
            }
            offset += Changes.Length;
            return offset;
        }

        private const int _CodePackLength = sizeof (int);
        private const int _WholeCodePackLength = 1 + _CodePackLength * 3;

        public void SaveToStream(Stream stream)
        {
            byte[] code = new byte[_WholeCodePackLength];

            // generate riff header for the change
            code[0] = (byte) ChgType;

            byte[] start = BitConverter.GetBytes(Start);
            Write4Bytes(code, start, 1);

            byte[] size = BitConverter.GetBytes(Size);
            Write4Bytes(code, size, 1 + _CodePackLength);

            byte[] length = BitConverter.GetBytes(GetChangeLength());
            Write4Bytes(code, length, 1 + _CodePackLength * 2);

            // write the code to the stream
            stream.Write(code, 0, code.Length);

            byte[] eolBytes = _encoding.GetBytes(Environment.NewLine);

            // prepare and write the changes strings as bytes with EOL between each
            int changeIndex = 0;
            foreach(string str in Changes)
            {
                byte[] strAsBytes = _encoding.GetBytes(str);
                stream.Write(strAsBytes, 0, strAsBytes.Length);
                if (++changeIndex < Changes.Length || str == "")
                    stream.Write(eolBytes, 0, eolBytes.Length);
            }
        }

        private static void Write4Bytes(byte[] code, byte[] start, int offset)
        {
            for (var i = 0; i < 4; i++)
            {
                code[offset + i] = start[3 - i];
            }
        }

        private static void Read4Bytes(byte[] bytes, byte[] code, int offset)
        {
            for (var i = 0; i < 4; i++)
            {
                bytes[3 - i] = code[offset + i];
            }
        }

        // Change if we need Unicode or other endcoding
        private readonly Encoding _encoding = new UnicodeEncoding(); //new ASCIIEncoding();

        private int GetChangeLength()
        {
            if (Changes.Count() == 0) return 0;

            int size = 0;
            int eolByteCount = _encoding.GetByteCount(Environment.NewLine);
           
            foreach(string str in Changes)
            {
                size += _encoding.GetByteCount(str) + eolByteCount;
            }

            return  
                Changes.Last() == "" 
                ? size                 // do not decrease size, it is neccessary to store the last EOL change
                : size - eolByteCount; // correction for the last EOL added in the loop
        }

        public void LoadFromStream(Stream stream)
        {
            // declare code byte array
            byte[] code = new byte[_WholeCodePackLength];

            // read code from stream
            stream.Read(code, 0, code.Length);

            // assign ChangeType
            byte[] bytes = new byte[_CodePackLength];
            ChgType = (ChangeType)code[0];

            // assign Start
            Read4Bytes(bytes, code, 1);
            Start = BitConverter.ToInt32(bytes, 0);

            // assign Size
            Read4Bytes(bytes, code, 1 + _CodePackLength);
            Size = BitConverter.ToInt32(bytes, 0);

            // assign string length
            Read4Bytes(bytes, code, 1 + _CodePackLength * 2);
            int length = BitConverter.ToInt32(bytes, 0);

            // read Changes string (not splitted)
            byte[] bytesChanges = new byte[length];
            stream.Read(bytesChanges, 0, length);

            // split and assign Changes string array
            if (0 < length && length <= MAX_LINE_LEN)
            {
                var chList = _encoding.GetString(bytesChanges).Split(new[] {Environment.NewLine}, StringSplitOptions.None).ToList();

                if (lastTwoEmpy(chList))
                     chList.RemoveAt(chList.Count - 1);

                // otherwise (because of string.Split) you get one extra ""

                Changes = chList.ToArray();
                
            }
        }

        public static bool lastTwoEmpy(IEnumerable<string> text)
        {
            int lastChIdx = text.Count() - 1;

            return 
                lastChIdx > 0
                && text.ElementAt(lastChIdx - 1) == string.Empty
                && text.ElementAt(lastChIdx) == string.Empty;

        }

        public static bool isOnlyEmpy(IEnumerable<string> text)
        {
            foreach (string ln in text)
                if (ln != string.Empty)
                    return false;
            return true;
        }

        private const int MAX_LINE_LEN = 4000000; // 4 Millions characters
    }
}
