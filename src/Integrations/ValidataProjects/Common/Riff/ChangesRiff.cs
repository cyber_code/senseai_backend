﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Common.Riff
{
    public class ChangesRiff
    {
        private readonly List<Change> _changes = new List<Change>();
        public IList<Change> Changes
        { 
            get
            {
                return _changes;
            }
        }

        public Change AddChange(ChangeType type, int start, int size, string[] changes)
        {
            var ch = new Change
                         {
                             ChgType = type,
                             Start = start,
                             Size = size,
                             Changes = changes
                         };
            Changes.Add(ch);
            return ch;
        }

        public static byte[] CalculateFullData(byte[] chunkBytes, byte[] predBytes)
        {
            byte[] fullData;
            var changes = new ChangesRiff();
            changes.LoadFromByteArray(chunkBytes);
            var ori = new MemoryStream(predBytes);
            var newMemory = new MemoryStream();
            changes.Apply(ori, newMemory);
            fullData = newMemory.ToArray();
            return fullData;
        }
        public void SaveToStream(Stream stream)
        {
            foreach(Change change in Changes)
            {
                change.SaveToStream(stream);
            }
            stream.Flush();
        }

        public void LoadFromStream(Stream stream)
        {
            Changes.Clear();
            // loop while stream is depleted
            while (stream.Position < stream.Length)
            {
                var change = new Change();
                change.LoadFromStream(stream);
                Changes.Add(change);
            }
        }

        protected void ApplyChanges(IList<string> target)
        {
            int offset = 0;
            foreach(var change in Changes)
            {
                change.ApplyOn(target, ref offset);
            }
        }

        public void Apply(Stream source, Stream target)
        {
            var reader = new StreamReader(source);

            string srcString = reader.ReadToEnd();
            var nlString = srcString.Contains("\r\n") ? "\r\n" : "\n"; // fantastic
            var text = srcString.Split(new string[] { nlString }, StringSplitOptions.None).ToList<string>();

            reader.Close();

            if (Change.isOnlyEmpy(text))
            {
                text.RemoveAt(0);
            }

            ApplyChanges(text);

            var writer = new StreamWriter(target); // , currentEncoding);
            writer.NewLine = nlString; // tremendous

            if (text.Count > 0)
            {
                if (Change.isOnlyEmpy(text))
                {
                    text.Add(string.Empty);
                }

                for (int i = 0; i < text.Count - 1; i++)
                {
                    writer.WriteLine(text[i]);
                }
                writer.Write(text[text.Count - 1]);
            }
            writer.Flush();
        }

        
        public void LoadFromByteArray(byte[] fileData)
        {
            try
            {
                if (fileData == null)
                    return;
                if (fileData.Length == 0)
                    return;
                MemoryStream mstr = new MemoryStream(fileData);
                LoadFromStream(mstr);
                mstr.Close();

            }
            catch (Exception)
            {
            }
        }

        public byte[] ToByteArray()
        {
            MemoryStream mstr = new MemoryStream();
            try
            {
                SaveToStream(mstr);
                return mstr.ToArray();
            }
            finally
            {
                mstr.Close();
            }
        }
    }
}
