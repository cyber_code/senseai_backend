﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using ValidataCommon;

namespace Common
{
    public class FileAnalyzer
    {
        public static FileAnalyzer Instance
        {
            get { return Singleton<FileAnalyzer>.UniqueInstance; }
        }

        private const byte ASCII_START = 32;
        private const byte ASCII_END = 128;

        private static readonly byte[] OTHER_ASCII = new byte[] {9, 10, 12, 13, 26};

        private const byte ASCII_PERCENT_TRESSHOLD = 94;

        public long BytesToAnalyze
        {
            get { return _BytesToAnalyze; }
            set { _BytesToAnalyze = value; }
        }

        private long _BytesToAnalyze = 1024*50;

        public bool IsBinary(string file)
        {
            FileStream fileStream = null;

            try
            {
                File.SetAttributes(file, FileAttributes.Normal);
                fileStream = new FileStream(file, FileMode.Open);
                return IsBinary(fileStream);
            }
            finally
            {
                if (fileStream != null)
                {
                    fileStream.Close();
                }
            }
        }

        public bool IsBinaryContent(string fileContent)
        {
            if (fileContent.Length > _BytesToAnalyze)
            {
                fileContent.Remove((int) _BytesToAnalyze);
            }

            var chars = new List<byte>();
            foreach (char ch in fileContent)
            {
                chars.Add((byte) ch);
            }

            int asciiPercent = GetAscIIPercent(chars);
            return asciiPercent < ASCII_PERCENT_TRESSHOLD;
        }

        public bool IsBinary(Stream stream)
        {
            long readCount = BytesToAnalyze;
            if (stream.Length < readCount)
            {
                readCount = stream.Length;
            }
            if (stream.Length == 0)
            {
                return false;
            }

            byte[] content = new byte[readCount];
            stream.Read(content, 0, (int) readCount);

            int asciiPercent = GetAscIIPercent(content);

            bool isEncoding = CheckEncoding(content);
            if (isEncoding)
            {
                return true;
            }

            return asciiPercent < ASCII_PERCENT_TRESSHOLD;
        }

/*
If the first two bytes are hex FE FF, the file is tentatively UTF-16 BE.
If the first two bytes are hex FF FE, and the following two bytes are not hex 00 00 , the file is tentatively UTF-16 LE.
If the first four bytes are hex 00 00 FE FF, the file is tentatively UTF-32 BE.
If the first four bytes are hex FF FE 00 00, the file is tentatively UTF-32 LE.

If, through the above checks, you have determined a tentative encoding, then check only for the corresponding encoding below, to ensure that the file is not a binary file which happens to match a byte-order mark.

If you have not determined a tentative encoding, the file might still be a text file in one of these encodings, since the byte-order mark is not mandatory, so check for all encodings in the following list:
If the file contains only big-endian two-byte words with the decimal values 9–13, 32–126, and 128 or above, the file is probably UTF-16 BE.
If the file contains only little-endian two-byte words with the decimal values 9–13, 32–126, and 128 or above, the file is probably UTF-16 LE.
If the file contains only big-endian four-byte words with the decimal values 9–13, 32–126, and 128 or above, the file is probably UTF-32 BE.
If the file contains only little-endian four-byte words with the decimal values 9–13, 32–126, and 128 or above, the file is probably UTF-32 LE.
*/

        private static KeyValuePair<byte, byte>[] TWO_BYTES_ENC = new KeyValuePair<byte, byte>[]
                                                               {
                                                                   new KeyValuePair<byte, byte>(0xFE, 0xFF),
                                                                   new KeyValuePair<byte, byte>(0xFF, 0xFE)
                                                               };

        private static bool CheckEncoding(byte[] content)
        {
            if (content.Length < 2)
            {
                return false;
            }

            foreach (KeyValuePair<byte, byte> pair in TWO_BYTES_ENC)
            {
                if ((content[0] == pair.Key) && (content[1] == pair.Value))
                {
                    return true;
                }

                if (content.Length > 3)
                {
                    if ((content[0] == 0) && (content[1] == 0) &&
                        (content[0] == pair.Key) && (content[1] == pair.Value))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static int GetAscIIPercent(ICollection<byte> content)
        {
            int size = content.Count;
            int ascIIs = 0;

            int zeroes = 0;

            foreach (byte b in content)
            {
                if (b == 0)
                {
                    zeroes++;
                }

                if (IsAscII(b))
                {
                    ascIIs++;

                    if ((zeroes == 1) || (zeroes == 3))
                    {
                        ascIIs += zeroes;
                        zeroes = 0;
                    }
                }
            }

            return (int) (((double) ascIIs/size)*100);
        }

        private static bool IsAscII(byte b)
        {
            return ((b >= ASCII_START) && (b <= ASCII_END)) || OTHER_ASCII.Contains(b);
        }
    }
}