﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ValidataCommon;

namespace Common
{
    [Serializable]
    public class FolderMapList : List<FolderMap>
    {
        #region Constructor

        /// <summary>
        /// base constructor
        /// </summary>
        public FolderMapList() {}

        #endregion

        #region Public Methods

        /// <summary>
        /// Convert FolderMapList to XML string
        /// </summary>
        /// <returns>string</returns>
        public string ToXML()
        {
            return XmlSerializationHelper.SerializeToXml(this);
        }

        /// <summary>
        /// Convert XML string to FolderMapList object
        /// </summary>
        /// <param name="toXML">xml string</param>
        /// <returns>FolderMapList object</returns>
        public static FolderMapList FromXML(string toXML)
        {
            if (!String.IsNullOrEmpty(toXML))
            {
                return (FolderMapList) XmlSerializationHelper.Deserialize(typeof (FolderMapList), toXML);
            }

            return new FolderMapList();
        }

        public bool ContainsEquivalentRule(FolderMap inFm)
        {
            return this.Any(fm => fm.Equals(inFm));
        }

        #endregion

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();

            foreach (FolderMap fm in this)
            {
                result.AppendFormat("{0} -> {1} ({2})\r\n", fm.Source, fm.Target, fm.Action);
            }

            return result.ToString();
        }
    }
}