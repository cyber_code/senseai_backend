﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ValidataCommon;

namespace Common
{
    [Serializable]
    public class EnvironmentRestrictions
    {
        #region Properties

        public bool IsDirectDeploymentRestricted;
        public List<ulong> RolesWithPermissionsForDirectDeployment = new List<ulong>();

        public bool IsBaselineDeploymentRestricted;
        public List<ulong> RolesWithPermissionsForBaselineDeployment = new List<ulong>();

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public EnvironmentRestrictions() {}

        #endregion

        #region Public Methods

        /// <summary>
        /// Convert EnvironmentRestrictions to XML string
        /// </summary>
        /// <returns>string</returns>
        public string ToXML()
        {
            return XmlSerializationHelper.SerializeToXml(this);
        }

        /// <summary>
        /// Convert XML string to EnvironmentRestrictions object
        /// </summary>
        /// <param name="toXML">xml string</param>
        /// <returns>EnvironmentRestrictions object</returns>
        public static EnvironmentRestrictions FromXML(string toXML)
        {
            if (!String.IsNullOrEmpty(toXML))
            {
                return (EnvironmentRestrictions) XmlSerializationHelper.Deserialize(typeof (EnvironmentRestrictions), toXML);
            }

            return new EnvironmentRestrictions();
        }

        public bool HasRightsForDirectDeployment(ulong roleNoderef)
        {
            if (!IsDirectDeploymentRestricted)
            {
                return true;
            }

            Debug.Assert(roleNoderef != 0);

            return RolesWithPermissionsForDirectDeployment.Contains(roleNoderef);
        }

        public bool HasRightsForBaselineDeployment(ulong roleNoderef)
        {
            if (!IsBaselineDeploymentRestricted)
            {
                return true;
            }

            Debug.Assert(roleNoderef != 0);

            return RolesWithPermissionsForBaselineDeployment.Contains(roleNoderef);
        }

        #endregion

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();

            {
                result.Append(IsDirectDeploymentRestricted ? "DD Restricted, following roles are permitted: " : "DD Not Restricted");
                if (IsDirectDeploymentRestricted)
                {
                    foreach (ulong roleNoderef in RolesWithPermissionsForDirectDeployment)
                    {
                        result.AppendFormat("{0}; ", roleNoderef);
                    }
                }
            }

            result.AppendLine();

            {
                result.Append(IsBaselineDeploymentRestricted ? "BL Restricted, following roles are permitted: " : "BL Not Restricted");
                if (IsBaselineDeploymentRestricted)
                {
                    foreach (ulong roleNoderef in RolesWithPermissionsForBaselineDeployment)
                    {
                        result.AppendFormat("{0}; ", roleNoderef);
                    }
                }
            }

            return result.ToString();
        }
    }
}