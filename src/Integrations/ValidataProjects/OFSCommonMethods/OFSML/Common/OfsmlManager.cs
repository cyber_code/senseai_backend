﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using AXMLEngine;
using OFSCommonMethods.OFSEnquiry;
using OFSCommonMethods.OFSML.OFSML130;
using OFSStringConvertor;
using Validata.Common;
using ValidataCommon;

namespace OFSCommonMethods.OFSML.Common
{
    public class OfsmlManager
    {
        #region Private Constants

        private const string ID_ATTRIBUTE_NAME = "ID";

        #endregion

        #region Private Members

        private readonly IOfsmlCommands _commands;
        private string _reqTypicalName;

        #endregion

        #region Class Lifecycle

        public OfsmlManager(IOfsmlCommands commands, string reqTypicalName)
        {
            _commands = commands;
            _reqTypicalName = reqTypicalName;
        }

        #endregion

        #region Public Methods

        public string GenerateOfsmlRequest(string user, string password, 
                                           string authorizeUser, string authorizePassword,
                                           string version, string company,
                                           List<Filter> filters,
                                           GlobusRequest.RequestType requestType,
                                           OperationCode operationCode, DataProcessType type,
                                           Instance instance
                                           )
        {
            string messageText = "";

            switch (requestType)
            {
                case GlobusRequest.RequestType.Enquiry:
                case GlobusRequest.RequestType.History:
                    messageText = GenerateOfsmlEnquiryRequest(user, password, filters, requestType);
                    break;

                case GlobusRequest.RequestType.Application:
                case GlobusRequest.RequestType.DataProcess:
                    messageText = GenerateOfsmlTransactionRequest(
                        user, password, 
                        authorizeUser, authorizePassword, 
                        version, company,
                        operationCode, type,
                        instance);

                    break;
            }

            return messageText;
        }

        public string GenerateOfsmlEnquiryRequest(string user, string password,
                                          List<Filter> filters,
                                          GlobusRequest.RequestType requestType)
        {
            NormalizeFilterItems(filters);

            if (requestType == GlobusRequest.RequestType.History && !_reqTypicalName.StartsWith("%"))
                _reqTypicalName = "%" + _reqTypicalName;

            string messageText = _commands.OfsmlStandardEnquiryRequest(user, password, _reqTypicalName, filters);
            return messageText;
        }

        public string GenerateOfsmlTransactionRequest(string user, string password,
                                                      string authorizeUser, string authorizePassword,
                                                      string version, string company,
                                                      OperationCode operationCode, DataProcessType type,
                                                      Instance instance)
            
        {
            if (instance == null)
            {
                throw new ApplicationException("Instance not set for a data process command!");
            }

            string transactionid = null;
            List<Ofsml130MVField> fields = null;

            var functionCode = GetFunctionCode(type);

            if (functionCode != FunctionCode.INPUT)
            {
                transactionid = instance.GetAttributeValue("@ID").Trim();
            }
            else
            {
                fields = new List<Ofsml130MVField>();

                for (int i = 0; i < instance.Attributes.Count; i++)
                {
                    InstanceAttribute attr = instance.Attributes[i];
                    if (attr.Name == "ID" || attr.Name == "@ID" || (IsNoValue(attr.Value)))
                        continue;

                    string fieldName;
                    int multiValueIndex, subValueIndex;
                    AttributeNameParser.ParseComplexFieldName(attr.Name, out fieldName, out multiValueIndex, out subValueIndex);

                    var field = new Ofsml130MVField(
                        fieldName,
                        multiValueIndex == 0 ? "" : multiValueIndex.ToString(),
                        subValueIndex == 0 ? "" : subValueIndex.ToString(),
                        attr.Value);

                    fields.Add(field);
                }
            }

            string ofsmlCommand;

            if (functionCode == FunctionCode.AUTHORISE)
                ofsmlCommand = _commands.OfsmlTransactionRequest(authorizeUser, authorizePassword, _reqTypicalName, version, company,
                                                   transactionid, functionCode, operationCode, fields);
            else
            {
                ofsmlCommand = _commands.OfsmlTransactionRequest(user, password, _reqTypicalName, version, company,
                                                   transactionid, functionCode, operationCode, fields);
            }


            return ofsmlCommand;
        }

        public string ReadOfsmlTransactionResponse(string response, 
                                                   MetadataTypical responseTypical,
                                                   ref bool error,
                                                   ref string transactionID
                                                   )
        {
            var ofsml130Commands = new Ofsml130Commands();
            var ofsm130TransactionResponse = ofsml130Commands.GetOfsm130TransactionResponse(response);

            if (ofsm130TransactionResponse.Type == ResponseType.TransactionFailed)
            {
                error = true;
                string errorMessage = ofsm130TransactionResponse.Ofsml130OfsTransactionFailed.ToString();
                return Utils.GetHtmlEncodedValue(errorMessage);
            }

            var transactionProcessedResponse = ofsm130TransactionResponse.Ofsml130TransactionProcessedResponse;

            if (transactionProcessedResponse.TransactionIds.Count == 0)
                throw new MissingFieldException("Transaction Id is missing.");

            transactionID = transactionProcessedResponse.TransactionIds[0];

            var fieldsList = new List<string>();
            var fields = new StringBuilder();

            string creationDate = "";
            string versionNumber = "";

            int overrideIndex = 1;
            foreach (var overrideField in transactionProcessedResponse.OverrideFields)
            {
                const string fieldName = "OVERRIDE";
                string fieldValue = "{" + overrideField.Code + "}" + overrideField.Value;
                transactionProcessedResponse.MVFields.Add(new Ofsml130MVField(fieldName, overrideIndex.ToString(), "1", fieldValue));
                overrideIndex++;
            }

            foreach (var field in transactionProcessedResponse.MVFields)
            {
                if (field.Name == "DATE.TIME")
                    creationDate = field.Value;

                if (field.Name == "CURR.NO")
                {
                    versionNumber = field.Value;
                }

                WriteFieldValuesAsXML(
                    responseTypical, 
                    field.Name, field.Value, int.Parse(field.Mv), int.Parse(field.Sv),
                    fieldsList, fields);
            }

            DateTime dt = GetDateTime(creationDate);
            creationDate = dt.ToString("dd/MM/yyyy HH:mm");

            string attributes = string.Format(" Catalog=\"{0}\" CreateDate=\"{1}\" VersionNo=\"{2}\"",
                    responseTypical.CatalogName,
                    creationDate,
                    versionNumber
                );

            fields.Append(InTag(Utils.GetHtmlEncodedValue(transactionID), "@ID", ""));
            string result = InTag(fields.ToString(), responseTypical.Name, attributes);

            return result;

        }

        public List<string> ReadOfsmlHistoryResponse(string response)
        {
            var ofsml130Commands = new Ofsml130Commands();
            var ofsm130StandardEnquiryResponse = ofsml130Commands.GetOfsm130StandardEnquiryResponse(response);

            var ids = new List<string>();
            foreach (var record in ofsm130StandardEnquiryResponse.EnquiryRecords)
            {
                if (record.Columns.Count > 0)
                {
                    ids.Add(record.Columns[0].Value);
                }
            }

            return ids;
        }

        public string ReadOfsmlEnquiryResponse( string phantomSet,
                                                string enquiryName,
                                                MetadataTypical responseTypical,
                                                string response,
                                                EnquiryResultParsingRotine enquiryResultParsingRoutine
                                               )
        {
            System.Diagnostics.Debug.Assert(enquiryResultParsingRoutine != null);

            var ofsml130Commands = new Ofsml130Commands();
            var ofsm130StandardEnquiryResponse = ofsml130Commands.GetOfsm130StandardEnquiryResponse(response);
            EnquiryResponse rows = EnquiryResponseResultParser.GetResult(ofsm130StandardEnquiryResponse);

            return ReadEnquiryResponse(
                    phantomSet, 
                    response,
                    responseTypical,
                    enquiryName,
                    enquiryResultParsingRoutine,
                    rows
                );
        }

 

        /// <summary>
        /// Closes a value in a tag with a specified name.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <param name="tag">The tag name.</param>
        /// <param name="attributes"></param>
        /// <returns>The resulting tag.</returns>
        public String InTag(String val, String tag, string attributes)
        {
            return
                "<" + XmlConvert.EncodeName(tag) + attributes +
                ((val.Length > 0) ? ">" + val + "</" + XmlConvert.EncodeName(tag) : "/") + ">" + "\r\n";
        }

        #endregion

        #region Private Methods

        private static FunctionCode GetFunctionCode(DataProcessType type)
        {
            switch (type)
            {
                case DataProcessType.CreateAndAuthorize:
                    return FunctionCode.INPUT;

                case DataProcessType.Authorize:
                    return FunctionCode.AUTHORISE;

                case DataProcessType.Reverse:
                    return FunctionCode.REVERSE;

                case DataProcessType.Delete:
                    return FunctionCode.DELETE;

                case DataProcessType.Select:
                    return FunctionCode.SEE;

                case DataProcessType.Create:
                    return FunctionCode.INPUT;
                default:
                    throw new ApplicationException(string.Format("Wrong function code '{0}' ! Accepted ones: INPUT, SEE, AUTHORISE, REVERSE and DELETE. ", type));
            }
        }

        private string ReadEnquiryResponse(string phantomSet, string actualResponse, MetadataTypical responseTypical, string enquiryName, EnquiryResultParsingRotine enquiryParsingRoutine, EnquiryResponse rows)
        {
            System.Diagnostics.Debug.Assert(enquiryParsingRoutine != null);

            var headers = new ArrayList();
            for (int i = 0; i < responseTypical.Attributes.Count; i++)
            {
                TypicalAttribute attr = responseTypical.Attributes[i];
                if (attr.Name != ID_ATTRIBUTE_NAME)
                    headers.Add(XmlConvert.DecodeName(attr.Name));
            }

            var levelStructure = enquiryParsingRoutine.LevelStructure;

            if (levelStructure == null)
            {
                // not defined by the execution call parameters -> try to get it from global configuration xml
                bool hasCustomTemplate = !String.IsNullOrEmpty(enquiryParsingRoutine.TemplateName);
                levelStructure = EnquiryResponseRule.GetRuleFromXML(
                        phantomSet,
                        hasCustomTemplate ? enquiryParsingRoutine.TemplateName : enquiryName,
                        "T24OFSAdapter",
                        hasCustomTemplate
                    );
                
            }

            if (levelStructure != null)
            {
                rows = GetEnquiryRequestResult(levelStructure, rows);
                if (rows.Rows == null || rows.Rows.Count == 0)
                {
                    if (actualResponse.Length > 255)
                        actualResponse = actualResponse.Substring(0, 254) + "...";

                    string errorMsg =
                        string.Format(
                            "Configuration rule for '{0}' enquiry do not match with any record from enquiry result: {1}",
                            enquiryName, actualResponse);
                    throw new EnquiryConfigurationException(errorMsg, EnquiryExceptionType.NoRuleMatch);
                }
            }

            var instanceXML = new StringBuilder();
            var responseXML = new StringBuilder();

            if (rows != null)
            {
                if (rows.Rows.Count == 0)
                {
                    throw new EnquiryConfigurationException(OFSResponseReaderBase.NO_RECORDS_STRING, EnquiryExceptionType.Undefined);
                }

                List<TypicalAttribute> attributesWithoutID =
                    responseTypical.Attributes.FindAll(attr => attr.Name != ID_ATTRIBUTE_NAME);

                foreach (EnquiryResponseRow row in rows.Rows)
                {
                    for (int fieldPos = 0; fieldPos < row.Cells.Count; fieldPos++)
                    {
                        if (attributesWithoutID.Count <= fieldPos) continue;

                        string fieldValue = Utils.GetHtmlEncodedValue(row.Cells[fieldPos].Value);
                        AttributeDataType dataType = attributesWithoutID[fieldPos].Type;
                        fieldValue = ValidataConverter.GetStringValueForDB(fieldValue, dataType);
                        fieldValue = SafeXmlTextWriter.GetValidXmlString(fieldValue);

                        instanceXML.Append(InTag(fieldValue, (string)headers[fieldPos], ""));
                    }

                    string attributes = string.Format(" Catalog=\"{0}\"", responseTypical.CatalogName);

                    responseXML.Append(InTag(instanceXML.ToString(), responseTypical.Name, attributes));
                    instanceXML = new StringBuilder();
                }
            }

            return responseXML.ToString();
        }

        private static EnquiryResponse GetEnquiryRequestResult(LevelStructureHierarchy rule, EnquiryResponse rows)
        {
            var singleBrancheLevels = EnquiryResponseRule.GetSingleBranchLevels(rule);

            EnquiryResponse resultRequest = null;
            foreach (LevelStructureHierarchy level in singleBrancheLevels)
            {
                resultRequest = EnquiryResponseRuleResult.GetResult(rows, level);
                if (resultRequest.Rows.Count > 0) break;
            }
            return resultRequest;
        }

        /// <summary>
        /// Checks the field for multiple values
        /// </summary>
        /// <param name="responseTypical"></param>
        /// <param name="globusFieldName"></param>
        /// <param name="globusFieldValue"></param>
        /// <param name="fieldNum">Field number</param>
        /// <param name="subfieldNum"></param>
        /// <param name="fieldsList"></param>
        /// <param name="fields"></param>
        private void WriteFieldValuesAsXML(MetadataTypical responseTypical, 
                                           string globusFieldName, string globusFieldValue, 
                                           int fieldNum, int subfieldNum,
                                           ICollection<string> fieldsList, StringBuilder fields
                                           )
        {
            // The check for multiple values is done in following order:
            // First is searched for Name-x~y
            // Second is searched for Name-x - ??? maybe this is not needed. Such attributes should not be in the database!!!
            // Third is searched for Name

            TypicalAttribute typAttr = FindAttributeInTypical(responseTypical, globusFieldName, fieldNum, subfieldNum);

            if (typAttr == null)
            { //we could not find the attribute
                return;
            }

            //we first process the read value 
            string fieldValue = Utils.GetHtmlEncodedValue(globusFieldValue);
            fieldValue = ValidataConverter.GetStringValueForDB(fieldValue, typAttr.Type);

            if (!fieldsList.Contains(typAttr.Name))
            {
                fieldsList.Add(typAttr.Name);

                fields.Append(InTag(fieldValue, typAttr.Name, ""));
            }
        }

        /// <summary>
        /// Finds the right attribute in Validata typical for this Globus attribute
        /// </summary>
        /// <param name="typ">The Validata typical</param>
        /// <param name="fieldName">The name of the field</param>
        /// <param name="fieldNum">The multiple value index</param>
        /// <param name="subFieldNum">The subvalue index</param>
        /// <returns>The correct Validata typical attribute or null.</returns>
        private static TypicalAttribute FindAttributeInTypical(MetadataTypical typ, string fieldName, int fieldNum, int subFieldNum)
        {
            // The check for multiple values is done in following order:
            // First is searched for Name-x~y
            // Second is searched for Name-x - ??? maybe this is not needed. Such attributes should not be in the database!!!
            // Third is searched for Name

            string fullForm = fieldName + "-" + fieldNum + "~" + subFieldNum;

            if (typ.AttributesByName.ContainsKey(fullForm))
            {
                return typ.AttributesByName[fullForm];
            }

            string partialForm = fieldName + "-" + fieldNum;

            if (typ.AttributesByName.ContainsKey(partialForm))
            {
                return typ.AttributesByName[partialForm];
            }

            if (typ.AttributesByName.ContainsKey(fieldName))
            {
                return typ.AttributesByName[fieldName];
            }
            return null;
        }

        private static DateTime GetDateTime(string creationDate)
        {
            DateTime dt = DateTime.Now;
            try
            {
                int centuryYear = int.Parse(creationDate.Substring(0, 2));
                int year = centuryYear > 50 ? 1900 + centuryYear : 2000 + centuryYear;
                int month = int.Parse(creationDate.Substring(2, 2));
                int day = int.Parse(creationDate.Substring(4, 2));
                int hour = int.Parse(creationDate.Substring(6, 2));
                int minute = int.Parse(creationDate.Substring(8, 2));
                dt = new DateTime(year, month, day, hour, minute, 0, 0);
            }
            catch{}

            return dt;
        }

        /// <summary>
        /// Determines if a string value is equivalent to the special Validata value 'No Value'
        /// </summary>
        /// <param name="fieldValue">The string value.</param>
        /// <returns>True if the string is equivalent to 'No Value'</returns>
        private static bool IsNoValue(string fieldValue)
        {
            return string.Compare(fieldValue.Trim(), "No Value", true) == 0; 
        }

        private static void NormalizeFilterItems(IEnumerable<Filter> filters)
        {
            foreach (var filter in filters)
            {
                filter.AttributeName = StripMultiValueNames(filter.AttributeName);
                filter.AttributeFilter = EvaluateDateTimeVariables(filter.AttributeFilter);
            }
        }

        /// <summary>
        /// Strip "-X~Y"
        /// </summary>
        /// <param name="attrName">attribute name</param>
        /// <returns>Clean attribute name</returns>
        private static string StripMultiValueNames(string attrName)
        {
            if (string.IsNullOrEmpty(attrName))
            {
                return string.Empty;
            }

            return AttributeNameParser.GetShortFieldName(attrName);
        }

        /// <summary>
        /// Evaluates the globus date time variables used for filters like {GLOBUS_TODAY} etc.
        /// </summary>
        /// <param name="originalValue">The original value</param>
        /// <returns>The value with the evaluated variables</returns>
        private static string EvaluateDateTimeVariables(string originalValue)
        {
            if (originalValue.Contains("{GLOBUS_TODAY}")
                || originalValue.Contains("{GLOBUS_THIS_MONTH}")
                || originalValue.Contains("{GLOBUS_THIS_YEAR}"))
            {
                DateTime today = DateTime.Today;

                string globusToday = today.Year.ToString().Substring(today.Year.ToString().Length - 2) +
                                     String.Format("{0:D2}", today.Month) +
                                     String.Format("{0:D2}", today.Day) +
                                     "...";

                string globusThisMonth = today.Year.ToString().Substring(today.Year.ToString().Length - 2) +
                                         String.Format("{0:D2}", today.Month) +
                                         "...";

                string globusThisYear = today.Year.ToString().Substring(today.Year.ToString().Length - 2) + "...";

                originalValue = originalValue.Replace("{GLOBUS_TODAY}", globusToday);
                originalValue = originalValue.Replace("{GLOBUS_THIS_MONTH}", globusThisMonth);
                originalValue = originalValue.Replace("{GLOBUS_THIS_YEAR}", globusThisYear);

                return originalValue;
            }

            return originalValue;
        }

        #endregion
    }
}
