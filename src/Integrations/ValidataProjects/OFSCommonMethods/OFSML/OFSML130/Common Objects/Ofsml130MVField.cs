﻿using System;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130MVField : OfsmlResponseNode
    {
        public string Name
        { 
            get;
            private set;
        }

        public string Mv
        {
            get;
            private set;
        }

        public string Sv
        {
            get;
            private set;
        }

        public string Value
        {
            get;
            private set;
        }

        public Ofsml130MVField(XmlNode node) : base(node)
        {
            if (node.Name != "field" && node.Name != "auxField" && node.Name != "error")
                throw new ApplicationException("Wrong response format !");

            foreach (XmlAttribute attribute in node.Attributes)
            {
                switch (attribute.Name)
                {
                    case "name":
                        Name = attribute.Value;
                        break;
                    case "mv":
                        Mv = attribute.Value;
                        break;
                    case "sv":
                        Sv = attribute.Value;
                        break;                  
                    default:
                        break;

                }
            }

            Value = node.InnerText;
        }

        public Ofsml130MVField(string name, string mv, string sv, string value): base(null)
        {
            Name = name;
            Mv = mv;
            Sv = sv;
            Value = value;

            var xmlDocument = new XmlDocument();

            XmlElement rootElement = xmlDocument.CreateElement("field");
            rootElement.InnerText = value;

            XmlAttribute nameAttr = xmlDocument.CreateAttribute("name");
            nameAttr.Value = name;
            rootElement.Attributes.Append(nameAttr);

            if(!string.IsNullOrEmpty(mv))
            {
                XmlAttribute mvAttr = xmlDocument.CreateAttribute("mv");
                mvAttr.Value = mv;
                rootElement.Attributes.Append(mvAttr);
            }

            if (!string.IsNullOrEmpty(sv))
            {
                XmlAttribute svAttr = xmlDocument.CreateAttribute("sv");
                svAttr.Value = sv;
                rootElement.Attributes.Append(svAttr);
            }

            _node = rootElement;
        }
    }
}
