﻿using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130ErrorField : Ofsml130MVField
    {
        public string Code
        {
            get; private set;
        }

        public Ofsml130ErrorField(XmlNode node) : base(node)
        {
            foreach (XmlAttribute attribute in node.Attributes)
            {
                switch (attribute.Name)
                {
                    case "code":
                        Code = attribute.Value;
                        break;
                    default:
                        break;

                }
            }
        }

        public Ofsml130ErrorField(string name, string code, string mv, string sv, string value) : base(name, mv, sv, value)
        {
            Code = code;
        }
    }
}
