using System;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public abstract class Ofsml130TransactionRequest : IXMLNode
    {
        private readonly XmlNode _xmlNode;

        protected Ofsml130TransactionRequest(string application, string version, string transactionId, OperationCode operation, FunctionCode function)
        {
            var xmlDocument = new XmlDocument();

            XmlElement rootElement = xmlDocument.CreateElement(GetElmentName(function));

            XmlAttribute applicationAttr = xmlDocument.CreateAttribute("application");
            applicationAttr.Value = application;
            rootElement.Attributes.Append(applicationAttr);

            if(!string.IsNullOrEmpty(version))
            {
                XmlAttribute versionAttr = xmlDocument.CreateAttribute("version");
                versionAttr.Value = version;
                rootElement.Attributes.Append(versionAttr);
            }

            XmlAttribute operationAttr = xmlDocument.CreateAttribute("operation");
            operationAttr.Value = operation.ToString();
            rootElement.Attributes.Append(operationAttr);

            if (!string.IsNullOrEmpty(transactionId))
            {
                XmlElement transactionIdNode = xmlDocument.CreateElement("transactionId");
                transactionIdNode.InnerText = transactionId;
                rootElement.AppendChild(transactionIdNode);

            }

            _xmlNode = rootElement;
        }

        private static string GetElmentName(FunctionCode function)
        {
            switch(function)
            {
                case FunctionCode.INPUT:
                    return "ofsTransactionInput";
                case FunctionCode.SEE:
                    return "ofsTransactionSee";
                case FunctionCode.AUTHORISE:
                    return "ofsTransactionAuthorise";
                case FunctionCode.DELETE:
                    return "ofsTransactionDelete";
                case FunctionCode.REVERSE:
                    return "ofsTransactionReverse";
                default:
                    throw new ApplicationException("Unexpected FunctionCode !");
            }
        }

        public virtual XmlNode XmlNode()
        {
            return _xmlNode;
        }
    }
}
