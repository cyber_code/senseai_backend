using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public abstract class Ofsml130RootElement : IXMLNode
    {
        #region Private Members

        private const string Encoding = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
        private readonly XmlNode _xmlNode;

        #endregion

        #region Protected Members
        
        protected XmlDocument _xmlDocument = new XmlDocument();

        protected Ofsml130RootElement()
        {
            XmlElement rootElement = _xmlDocument.CreateElement("T24");

            XmlAttribute attr1 = _xmlDocument.CreateAttribute("xmlns:xsi");
            attr1.Value = "http://www.w3.org/2001/XMLSchema-instance";

            XmlAttribute attr2 = _xmlDocument.CreateAttribute("xmlns");
            attr2.Value = "http://www.temenos.com/T24/OFSML/130";

            rootElement.Attributes.Append(attr1);
            rootElement.Attributes.Append(attr2);

            _xmlDocument.AppendChild(rootElement);
            _xmlNode = rootElement;
        }

        #endregion

        #region Public Members

        public virtual XmlNode XmlNode()
        {
            return _xmlNode;
        }

        public override string ToString()
        {
            return Encoding + _xmlDocument.OuterXml;
        }

        #endregion
    }
}
