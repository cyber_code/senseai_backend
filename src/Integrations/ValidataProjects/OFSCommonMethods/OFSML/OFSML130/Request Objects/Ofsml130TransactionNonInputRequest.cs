﻿namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130TransactionNonInputRequest : Ofsml130TransactionRequest
    {
        public Ofsml130TransactionNonInputRequest(string application, string version, string transactionId, OperationCode operation, FunctionCode function) 
            : base(application, version, transactionId, operation, function)
        {
        }
    }
}
