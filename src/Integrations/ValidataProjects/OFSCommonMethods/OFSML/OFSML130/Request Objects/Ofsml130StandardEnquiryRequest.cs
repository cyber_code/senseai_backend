﻿using System.Collections.Generic;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130StandardEnquiryRequest : IXMLNode
    {
        #region Private Members

        private readonly List<Ofsml130SelectionCriteria> _selectionCriterias = new List<Ofsml130SelectionCriteria>();

        #endregion

        #region Public Properties

        public string Name
        {
            get; private set;
        }

        #endregion

        #region Class Lifecycle

        public Ofsml130StandardEnquiryRequest(string name)
        {
            Name = name;
        }

        #endregion

        #region Public Methods

        public void AddSelectionCriteria(Ofsml130SelectionCriteria ofsml130SelectionCriteria)
        {
            _selectionCriterias.Add(ofsml130SelectionCriteria);
        }

        public IEnumerable<Ofsml130SelectionCriteria> GetSelectionCriterias()
        {
            return _selectionCriterias;
        }

        #endregion

        #region IXMLNode Members

        public XmlNode XmlNode()
        {
            var xmlDocument = new XmlDocument();

            XmlElement rootElement = xmlDocument.CreateElement("ofsStandardEnquiry");

            XmlAttribute nameAttr = xmlDocument.CreateAttribute("name");
            nameAttr.Value = Name;

            rootElement.Attributes.Append(nameAttr);

            foreach (var selectionCriteria in _selectionCriterias)
            {
                XmlNode criteria = rootElement.OwnerDocument.ImportNode(selectionCriteria.XmlNode(), true);
                rootElement.AppendChild(criteria);
            }

            return rootElement; 
        }

        #endregion
  
    }
}
