﻿using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public abstract class Ofsml130ServiceRequestGroup : Ofsml130RootElement
    {
        private readonly XmlNode _xmlNode;

        protected Ofsml130ServiceRequestGroup(Ofsml130SecurityContext ofsml130SecurityContext)
        {
            var t24Node = base.XmlNode();

            XmlElement rootElement = _xmlDocument.CreateElement("serviceRequest");
            XmlNode importedNode = _xmlDocument.ImportNode(ofsml130SecurityContext.XmlNode(), true);
            rootElement.AppendChild(importedNode);

            t24Node.AppendChild(rootElement);

            _xmlNode = rootElement;
        }

        public override XmlNode XmlNode()
        {
            return _xmlNode;
        }
    }
}