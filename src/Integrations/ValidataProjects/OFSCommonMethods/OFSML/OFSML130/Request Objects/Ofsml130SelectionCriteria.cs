using System;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130SelectionCriteria : IXMLNode
    {
        #region Private Members

        private readonly XmlNode _xmlNode;

        #endregion

        #region Public Properties

        public SelectionOperand Operand
        {
            get; private set;
        }

        public string FieldName
        {
            get; private set;
        }

        public string Value
        { 
            get; private set;
        }

        #endregion

        #region Class Lifecycle

        public Ofsml130SelectionCriteria(string operand, string fieldName, string value)
            : this((SelectionOperand)Enum.Parse(typeof(SelectionOperand), operand, false),fieldName, value)
        {
        }

        public Ofsml130SelectionCriteria(SelectionOperand operand, string fieldName, string value)
        {
            Operand = operand;
            FieldName = fieldName;
            Value = value;

            _xmlNode = CreateNode();
        }

        #endregion

        #region IXMLNode Members

        public XmlNode XmlNode()
        {
            return _xmlNode;
        }

        #endregion

        #region Private Methods

        private XmlNode CreateNode()
        {
            var xmlDocument = new XmlDocument();

            XmlElement rootElement = xmlDocument.CreateElement("selectionCriteria");

            XmlAttribute operandAttr = xmlDocument.CreateAttribute("operand");
            operandAttr.Value = Operand.ToString();

            XmlElement fieldName = xmlDocument.CreateElement("fieldName");
            fieldName.InnerText = FieldName;
            XmlElement value = xmlDocument.CreateElement("value");
            value.InnerText = Value;

            rootElement.Attributes.Append(operandAttr);
            rootElement.AppendChild(fieldName);
            rootElement.AppendChild(value);

            return rootElement;
        }

        #endregion
    }
}
