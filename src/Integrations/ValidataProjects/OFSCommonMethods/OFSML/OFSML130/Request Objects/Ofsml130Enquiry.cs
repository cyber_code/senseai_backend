﻿using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130Enquiry : Ofsml130ServiceRequestGroup
    {
        public Ofsml130Enquiry(Ofsml130SecurityContext ofsml130SecurityContext,
                               Ofsml130StandardEnquiryRequest ofsml130StandardEnquiryRequest) : 
            base(ofsml130SecurityContext)
        {
            XmlNode node = base.XmlNode();

            XmlNode importedNode = _xmlDocument.ImportNode(ofsml130StandardEnquiryRequest.XmlNode(), true);
            node.AppendChild(importedNode);
        }
    }
}
