using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130SecurityContext : IXMLNode
    {
        #region Private Members

        private readonly XmlNode _xmlNode;

        #endregion

        #region Public Properties

        public string UserName
        {
            get; private set;
        }

        public string Password
        {
            get; private set;
        }

        public string Company
        {
            get;
            private set;
        }

        #endregion

        #region Class Lifecycle

        public Ofsml130SecurityContext(string userName, string password, string company)
        {
            UserName = userName;
            Password = password;
            Company = company;

            _xmlNode = CreateNode();
        }

        #endregion

        #region IXMLNode Members

        public XmlNode XmlNode()
        {
            return _xmlNode;
        }

        #endregion

        #region Private Methods

        private XmlNode CreateNode()
        {
            var xmlDocument = new XmlDocument();

            XmlElement rootElement = xmlDocument.CreateElement("securityContext");

            XmlElement userName = xmlDocument.CreateElement("userName");
            userName.InnerText = UserName;
            rootElement.AppendChild(userName);

            XmlElement password = xmlDocument.CreateElement("password");
            password.InnerText = Password;
            rootElement.AppendChild(password);

            if(!string.IsNullOrEmpty(Company))
            {
                XmlElement company = xmlDocument.CreateElement("company");
                company.InnerText = Company;
                rootElement.AppendChild(company);
            }

            return rootElement;
        }

        #endregion
    }
}
