﻿using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130Transaction : Ofsml130ServiceRequestGroup
    {
        public Ofsml130Transaction(Ofsml130SecurityContext ofsml130SecurityContext,
                                   Ofsml130TransactionRequest ofsml130TransactionRequest) 
            : base(ofsml130SecurityContext)
        {
            XmlNode node = base.XmlNode();

            XmlNode importedNode = _xmlDocument.ImportNode(ofsml130TransactionRequest.XmlNode(), true);
            node.AppendChild(importedNode);
        }
    }
}
