namespace OFSCommonMethods.OFSML.OFSML130
{
    public enum SelectionOperand
    {
        EQ, 
        NE, 
        GE, 
        GT, 
        LE, 
        LT, 
        UL, 
        LK, 
        NR, 
        RG
    }
}