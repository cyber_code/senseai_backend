﻿using System.Collections.Generic;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    class Ofsml130TransactionInputRequest : Ofsml130TransactionRequest
    {
        public Ofsml130TransactionInputRequest(string application, string version, string transactionId, OperationCode operation, IEnumerable<Ofsml130MVField> fields) 
            : base(application, version, transactionId, operation, FunctionCode.INPUT)
        {
            XmlNode parentNode = base.XmlNode();

            foreach (var field in fields)
            {
                XmlNode fieldNode = parentNode.OwnerDocument.ImportNode(field.XmlNode(), true);
                parentNode.AppendChild(fieldNode);
            }
        }
    }
}
