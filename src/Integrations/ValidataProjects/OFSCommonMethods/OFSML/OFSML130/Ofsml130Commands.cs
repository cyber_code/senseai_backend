using System;
using System.Collections.Generic;
using System.Xml;
using AXMLEngine;
using ValidataCommon;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130Commands : IOfsmlCommands
    {
        public Ofsml130StandardEnquiryResponse GetOfsm130StandardEnquiryResponse(string response)
        {
            var document = new XmlDocument();
            document.LoadXml(response);

            XmlNode node = document.LastChild.FirstChild;

            var serviceResponse = new Ofsml130ServiceResponse(node);
            return serviceResponse.Ofsml130StandardEnquiryResponse;
        }

        public Ofsml130ServiceResponse GetOfsm130TransactionResponse(string response)
        {
            var document = new XmlDocument();
            document.LoadXml(response);

            XmlNode node = document.LastChild.FirstChild;

            var serviceResponse = new Ofsml130ServiceResponse(node);
            return serviceResponse;
        }

        public string OfsmlStandardEnquiryRequest(string userName, string password, string enqName, List<Filter> filters)
        {
            var security = new Ofsml130SecurityContext(userName, password, null);
            var enquiry = new Ofsml130StandardEnquiryRequest(enqName);

            foreach (var filter in filters)
            {
                SelectionOperand t24Constraint = GetGlobusConstraint(filter.Type);
                enquiry.AddSelectionCriteria(new Ofsml130SelectionCriteria(t24Constraint, filter.AttributeName, filter.AttributeFilter));
            }

            var enq = new Ofsml130Enquiry(security, enquiry);

            return enq.ToString();
        }

        public string OfsmlTransactionRequest(string userName, string password,
                                       string applicaiton, string version, 
                                       string company,
                                       string transactionId, 
                                       FunctionCode function,
                                       OperationCode operation,
                                       List<Ofsml130MVField> fields)
        {
            var security = new Ofsml130SecurityContext(userName, password, company);
            Ofsml130Transaction ofsml130Transaction;

            if(function == FunctionCode.INPUT)
            {
                var inputTransaction = new Ofsml130TransactionInputRequest(applicaiton, version, transactionId, operation, fields);
                ofsml130Transaction = new Ofsml130Transaction(security, inputTransaction);
            }
            else
            {
                var nonInputTransaction = new Ofsml130TransactionNonInputRequest(applicaiton, version, transactionId, operation, function);
                ofsml130Transaction = new Ofsml130Transaction(security, nonInputTransaction);
            }

            return ofsml130Transaction.ToString();
        }

        private static SelectionOperand GetGlobusConstraint(FilterType filterType)
        {
            switch (filterType)
            {
                case FilterType.GreaterThan:
                    return SelectionOperand.GT;
                case FilterType.GreaterOrEqual:
                    return SelectionOperand.GE;
                case FilterType.Equal:
                    return SelectionOperand.EQ;
                case FilterType.LessOrEqual:
                    return SelectionOperand.LE;
                case FilterType.Less:
                    return SelectionOperand.LT;
                case FilterType.Like:
                    return SelectionOperand.LK;
                case FilterType.NotEqual:
                    return SelectionOperand.NE;
                case FilterType.NotLike:
                    return SelectionOperand.UL;
                default:
                    throw new NotSupportedException("The filter " + filterType + " is not supported by T24");
            }
        }
    }
}
