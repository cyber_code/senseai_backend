﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using NUnit.Framework;

namespace OFSCommonMethods.OFSML.OFSML130.Unit_Tests
{
    [TestFixture]
    public class OfsmlTests
    {
        [Test]
        [Explicit]
        public void EnquiryTestOfsm130()
        {
            var security = new Ofsml130SecurityContext("VALINPUT", "123456", null);
            var enquiry = new Ofsml130StandardEnquiryRequest("%CUSTOMER");
            enquiry.AddSelectionCriteria(new Ofsml130SelectionCriteria(SelectionOperand.EQ, "@ID", "100724"));

            var enq = new Ofsml130Enquiry(security, enquiry);

            string xml = enq.ToString();
            Console.WriteLine(xml);
            // TODO verify result
        }

        [Test]
        [Explicit]
        public void SeeTransactionTestOfsm130()
        {
            var security = new Ofsml130SecurityContext("VALINPUT", "123456", null);
            var see = new Ofsml130TransactionNonInputRequest(
                "CUSTOMER", null, "100724", OperationCode.PROCESS, FunctionCode.SEE);

            var seeTransaction = new Ofsml130Transaction(security, see);

            string xml = seeTransaction.ToString();
            Console.WriteLine(xml);
            // TODO verify result
        }

        [Test]
        [Explicit]
        public void InputTransactionTestOfsm130()
        {
            var security = new Ofsml130SecurityContext("VALINPUT", "123456", null);

            var fieldsNodes = new List<Ofsml130MVField>
                                  {
                                      new Ofsml130MVField("DEBIT.ACCT.NO", null, null, "15137"),
                                      new Ofsml130MVField("CREDIT.AMOUNT", null, null, "300"),
                                      new Ofsml130MVField("CREDIT.ACCT.NO", null, null, "15172")
                                  };

            var inputTransaction = new Ofsml130TransactionInputRequest(
                "FUNDS.TRANSFER", "MB.AI.AC", null, OperationCode.VALIDATE, fieldsNodes);

            var transaction = new Ofsml130Transaction(security, inputTransaction);

            string xml = transaction.ToString();
            Console.WriteLine(xml);
            // TODO verify result
        }

        [Test]
        [Explicit]
        public void InputTransactionTestOfsm130_2()
        {
            var security = new Ofsml130SecurityContext("VALINPUT", "123456", null);

            var fieldsNodes = new List<Ofsml130MVField>
                                  {
                                      new Ofsml130MVField("TRANSACTION.TYPE", null, null, "OT"),
                                      new Ofsml130MVField("DEBIT.ACCT.NO", null, null, "15137"),
                                      new Ofsml130MVField("DEBIT.CURRENCY", null, null, "USD"),
                                      new Ofsml130MVField("DEBIT.AMOUNT", null, null, "1000"),
                                      new Ofsml130MVField("DEBIT.VALUE.DATE", null, null, "20080109"),
                                      new Ofsml130MVField("CREDIT.ACCT.NO", null, null, "15415"),
                                      new Ofsml130MVField("CREDIT.CURRENCY", null, null, "EUR"),
                                      new Ofsml130MVField("CREDIT.VALUE.DATE", null, null, "20080109"),
                                      new Ofsml130MVField("BEN.CUSTOMER", "1", "1", "Ursula Larken"),
                                      new Ofsml130MVField("BEN.OUR.CHARGES", null, null, "SHA"),
                                      new Ofsml130MVField("COMMISSION.CODE", null, null, "D"),
                                      new Ofsml130MVField("COMMISSION.TYPE", "1", "1", "BLBILL"),
                                      new Ofsml130MVField("COMMISSION.AMT", "1", "1", "USD10"),
                                      new Ofsml130MVField("COMMISSION.FOR", "1", "1", "SENDER"),
                                      new Ofsml130MVField("CHARGE.CODE", null, null, "D")
                                  };

            var inputTransaction = new Ofsml130TransactionInputRequest("FUNDS.TRANSFER", null, null, OperationCode.PROCESS, fieldsNodes);

            var transaction = new Ofsml130Transaction(security, inputTransaction);

            string xml = transaction.ToString();
            Console.WriteLine(xml);
            // TODO verify result
        }

        [Test]
        [Explicit]
        public void ReadOfsm130StandardEnquiryResponse()
        {
            // TODO add the file in the project
            string response = File.ReadAllText(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"Ofsm130StandardEnquiryResponse.txt");

            var document = new XmlDocument();
            document.LoadXml(response);

            XmlNode node = document.LastChild.FirstChild;

            var res = new Ofsml130ServiceResponse(node);
            // TODO verify result
        }

        [Test]
        [Explicit]
        public void ReadOfsml130TransactionProcessedResponse()
        {
            // TODO add the file in the project
            string response = File.ReadAllText(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"Ofsml130TransactionProcessedResponse.txt");

            var document = new XmlDocument();
            document.LoadXml(response);

            XmlNode node = document.LastChild.FirstChild;

            var res = new Ofsml130ServiceResponse(node);
            // TODO verify result
        }
    }
}