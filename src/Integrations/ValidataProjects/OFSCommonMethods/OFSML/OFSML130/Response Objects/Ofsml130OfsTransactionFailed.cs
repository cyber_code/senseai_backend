﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130OfsTransactionFailed : OfsmlResponseNode
    {
        private readonly List<string> _transactionIds = new List<string>();
        private readonly Ofsml130ErrorField _errorField; 

        public string Application
        {
            get;
            private set;
        }

        public string Function
        {
            get;
            private set;
        }

        public string Held
        {
            get;
            private set;
        }

        public string Operation
        {
            get;
            private set;
        }

        public List<string> TransactionIds
        {
            get { return _transactionIds; }
        }

        public Ofsml130ErrorField ErrorField
        {
            get { return _errorField; }
        }

        public Ofsml130OfsTransactionFailed(XmlNode node) : base(node)
        {
            if (node.Name != "ofsTransactionFailed")
                throw new ApplicationException("Wrong response format !");

            foreach (XmlAttribute attribute in node.Attributes)
            {
                switch (attribute.Name)
                {
                    case "application":
                        Application = attribute.Value;
                        break;
                    case "function":
                        Function = attribute.Value;
                        break;
                    case "held":
                        Held = attribute.Value;
                        break;
                    case "operation":
                        Operation = attribute.Value;
                        break;
                    default:
                        break;

                }
            }

            foreach (XmlNode subNode in node.ChildNodes)
            {
                switch (subNode.Name)
                {
                    case "transactionId":
                        _transactionIds.Add(subNode.InnerText);
                        break;

                    case "error":
                        _errorField = new Ofsml130ErrorField(subNode);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
