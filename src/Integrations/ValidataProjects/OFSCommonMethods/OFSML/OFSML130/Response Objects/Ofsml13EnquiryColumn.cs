﻿using System;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml13EnquiryColumn : OfsmlResponseNode
    {
        public string Id
        {
            get; private set;
        }

        public string Label
        {
            get; private set;
        }

        public string Type
        {
            get; private set;
        }

        public Ofsml13EnquiryColumn(XmlNode node) : base(node)
        {
            if (node.Name != "enquiryColumn" && node.Name != "auxOverride")
                throw new ApplicationException("Wrong response format !");

            foreach (XmlAttribute attribute in node.Attributes)
            {
                switch (attribute.Name)
                {
                    case "id":
                        Id = attribute.Value;
                        break;
                    case "label":
                        Label = attribute.Value;
                        break;
                    case "type":
                        Type = attribute.Value;
                        break;
                    default:
                        break;

                }
            }
        }
    }
}
