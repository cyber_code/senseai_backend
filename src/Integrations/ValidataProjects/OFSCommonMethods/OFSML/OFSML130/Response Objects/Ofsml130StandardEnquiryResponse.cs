﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130StandardEnquiryResponse : OfsmlResponseNode
    {
        #region Private Members

        private readonly List<string> _enquiryHeaders = new List<string>();
        private readonly List<Ofsml13EnquiryColumn> _enquiryColumns = new List<Ofsml13EnquiryColumn>();
        private readonly List<Ofsml13StandardEnquiryRecord> _enquiryRecords = new List<Ofsml13StandardEnquiryRecord>();

        #endregion

        #region Public Members

        public string Name
        {
            get; private set;
        }

        public string Status
        {
            get; private set;
        }

        public List<string> EnquiryHeaders
        {
            get { return _enquiryHeaders; }
        }

        public List<Ofsml13EnquiryColumn> EnquiryColumns
        {
            get { return _enquiryColumns; }
        }

        public List<Ofsml13StandardEnquiryRecord> EnquiryRecords
        {
            get {return _enquiryRecords;}
        }

        #endregion

        #region Class Lifecycle

        public Ofsml130StandardEnquiryResponse(XmlNode node) : base(node)
        {
            if (node.Name != "ofsStandardEnquiry")
                throw new ApplicationException("Wrong response format !");

            foreach (XmlAttribute attribute in node.Attributes)
            {
                switch (attribute.Name)
                {
                    case "name":
                        Name = attribute.Value;
                        break;
                    case "status":
                        Status = attribute.Value;
                        break;
                    default:
                        break;

                }
            }

            foreach (XmlNode subNode in node.ChildNodes)
            {
                switch (subNode.Name)
                {
                    case "enquiryHeader":
                        _enquiryHeaders.Add(subNode.InnerText);
                        break;
                    case "enquiryColumn":
                        _enquiryColumns.Add(new Ofsml13EnquiryColumn(subNode));
                        break;
                    case "enquiryRecord":
                        _enquiryRecords.Add(new Ofsml13StandardEnquiryRecord(subNode));
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion
    }
}
