﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130TransactionProcessedResponse : OfsmlResponseNode
    {

        #region Private Members

        private readonly List<Ofsml130MVField> _mvFields = new List<Ofsml130MVField>();
        private readonly List<Ofsml130OverrideField> _overrideFields = new List<Ofsml130OverrideField>();
        private readonly List<Ofsml130MVField> _auxFields = new List<Ofsml130MVField>();
        private readonly List<Ofsml130OverrideField> _auxOverrideFields = new List<Ofsml130OverrideField>();

        private readonly List<string> _transactionIds = new List<string>();

        #endregion

        #region Public Members

        public string Application
        {
            get; private set;
        }

        public string Version
        {
            get; private set;
        }

        public string Function
        {
            get; private set;
        }

        public string Operation
        {
            get; private set;
        }

        public string ProcessingStatus
        {
            get; private set;
        }

        public string AuxProcessingStatus
        {
            get; private set;
        }

        public List<Ofsml130MVField> MVFields
        {
            get { return _mvFields; }
        }

        public List<Ofsml130OverrideField> OverrideFields
        {
            get { return _overrideFields; }
        }

        public List<Ofsml130MVField> AuxFields
        {
            get { return _auxFields; }
        }

        public List<Ofsml130OverrideField> AuxOverrideFields
        {
            get { return _auxOverrideFields; }
        }

        public List<string> TransactionIds
        {
            get { return _transactionIds; }
        }

        #endregion

        #region Class Lifecycle

        public Ofsml130TransactionProcessedResponse(XmlNode node) : base(node)
        {
            if (node.Name != "ofsTransactionProcessed")
                throw new ApplicationException("Wrong response format !");

            foreach(XmlAttribute attribute in node.Attributes)
            {
                switch(attribute.Name)
                {
                    case "application":
                        Application = attribute.Value;
                        break;
                    case "version":
                        Version = attribute.Value;
                        break;
                    case "function":
                        Function = attribute.Value;
                        break;
                    case "operation":
                        Operation = attribute.Value;
                        break;
                    case "processingStatus":
                        ProcessingStatus = attribute.Value;
                        break;
                    case "auxProcessingStatus":
                        AuxProcessingStatus = attribute.Value;
                        break;
                    default:
                        break;

                }
            }

            foreach(XmlNode subNode in node.ChildNodes)
            {
                switch(subNode.Name)
                {
                    case "transactionId":
                        _transactionIds.Add(subNode.InnerText);
                        break;
                    case "field":
                        _mvFields.Add(new Ofsml130MVField(subNode));
                        break;
                    case "override":
                        _overrideFields.Add(new Ofsml130OverrideField(subNode));
                        break;
                    case "auxField":
                        _auxFields.Add(new Ofsml130MVField(subNode));
                        break;
                    case "auxOverride":
                        _auxOverrideFields.Add(new Ofsml130OverrideField(subNode));

                        break;
                    default:
                        break;
                }
            }
        }

        #endregion
    }
}
