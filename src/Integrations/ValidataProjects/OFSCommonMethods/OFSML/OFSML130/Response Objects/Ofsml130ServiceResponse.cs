﻿using System;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130ServiceResponse : OfsmlResponseNode
    {
        #region Private Members

        private Ofsml130TransactionProcessedResponse _ofsml130TransactionProcessedResponse;
        private Ofsml130StandardEnquiryResponse _ofsml130StandardEnquiryResponse;
        private Ofsml130OfsTransactionFailed _ofsml130OfsTransactionFailed;

        #endregion

        #region Public members

        public ResponseType Type
        {
            get; 
            private set;
        }

        public Ofsml130TransactionProcessedResponse Ofsml130TransactionProcessedResponse
        {
            get { return _ofsml130TransactionProcessedResponse; }
        }

        public Ofsml130StandardEnquiryResponse Ofsml130StandardEnquiryResponse
        {
            get { return _ofsml130StandardEnquiryResponse; }
        }

        public Ofsml130OfsTransactionFailed Ofsml130OfsTransactionFailed
        {
            get { return _ofsml130OfsTransactionFailed; }
        }

        #endregion

        #region Class Lifecycle

        public Ofsml130ServiceResponse(XmlDocument xmlDocument) : base(null)
        {
            try
            {
                XmlNode node = xmlDocument.LastChild.FirstChild;
                if(node == null)
                    throw new Exception();

                _node = node;
                LoadXml(node);
            }
            catch (Exception)
            {
                throw new ApplicationException("Wrong response format !");
            }
            
        }

        public Ofsml130ServiceResponse(XmlNode node) : base(node)
        {
            LoadXml(node);
        }

        #endregion

        #region Private Methods

        private void LoadXml(XmlNode node)
        {
            if (node.Name != "serviceResponse")
                throw new ApplicationException("Wrong response format !");

            if (node.ChildNodes.Count == 0)
                throw new ApplicationException("Wrong response format !");

            XmlNode childNode = node.ChildNodes[0];

            switch (childNode.Name)
            {
                case "ofsTransactionProcessed":
                    {
                        _ofsml130TransactionProcessedResponse = new Ofsml130TransactionProcessedResponse(childNode);
                        Type = ResponseType.TransactionProcessedResponse;
                        break;
                    }
                case "ofsStandardEnquiry":
                    {
                        _ofsml130StandardEnquiryResponse = new Ofsml130StandardEnquiryResponse(childNode);
                        Type = ResponseType.StandardEnquiryResponse;
                        break;
                    }
                case "ofsTransactionFailed":
                    {
                        _ofsml130OfsTransactionFailed = new Ofsml130OfsTransactionFailed(childNode);
                        Type = ResponseType.TransactionFailed;
                        break;
                    }

                default:
                    throw new ApplicationException("Wrong response format !");
            }
        }

        #endregion
    }

    public enum ResponseType
    {
        TransactionProcessedResponse,
        StandardEnquiryResponse,
        TransactionFailed
    }
}
