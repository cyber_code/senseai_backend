﻿using System;
using System.Xml;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml130OverrideField : OfsmlResponseNode
    {
        public string Code
        {
            get; private set;
        }

        public string Value
        {
            get; private set;
        }

        public Ofsml130OverrideField(XmlNode node) : base(node)
        {
            if (node.Name != "override" && node.Name != "auxOverride")
                throw new ApplicationException("Wrong response format !");

            foreach (XmlAttribute attribute in node.Attributes)
            {
                switch (attribute.Name)
                {
                    case "code":
                        Code = attribute.Value;
                        break;
                    default:
                        break;

                }
            }

            Value = node.InnerText;
        }
    }
}
