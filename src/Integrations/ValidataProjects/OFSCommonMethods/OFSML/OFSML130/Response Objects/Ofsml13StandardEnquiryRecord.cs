﻿using System;
using System.Collections.Generic;
using System.Xml;
using OFSCommonMethods.OFSEnquiry;

namespace OFSCommonMethods.OFSML.OFSML130
{
    public class Ofsml13StandardEnquiryRecord : OfsmlResponseNode
    {
        private readonly List<EnquiryResponseCell> _columns = new List<EnquiryResponseCell>();

        public List<EnquiryResponseCell> Columns
        {
            get { return _columns; }
        }

        public Ofsml13StandardEnquiryRecord(XmlNode node) : base(node)
        {
            if (node.Name != "enquiryRecord")
                throw new ApplicationException("Wrong response format !");

            foreach (XmlNode subNode in node.ChildNodes)
            {
                switch (subNode.Name)
                {
                    case "column":
                        _columns.Add(new EnquiryResponseCell(subNode.InnerText));
                        break;
                   
                    default:
                        break;
                }
            }
        }
    }
}
