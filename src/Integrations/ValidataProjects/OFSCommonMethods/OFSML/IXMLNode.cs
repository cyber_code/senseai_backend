﻿using System.Xml;

namespace OFSCommonMethods.OFSML
{
    public interface IXMLNode
    {
        XmlNode XmlNode();
    }
}
