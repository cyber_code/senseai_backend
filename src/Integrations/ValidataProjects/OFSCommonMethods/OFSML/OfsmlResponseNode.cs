﻿using System.Xml;

namespace OFSCommonMethods.OFSML
{
    public abstract class OfsmlResponseNode : IXMLNode
    {
        protected XmlNode _node;
        protected OfsmlResponseNode(XmlNode node)
        {
            _node = node;
        }

        public override string ToString()
        {
            return _node.OuterXml;
        }

        public XmlNode XmlNode()
        {
            return _node;
        }
    }
}
