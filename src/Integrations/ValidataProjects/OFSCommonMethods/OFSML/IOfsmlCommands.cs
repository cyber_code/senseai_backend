﻿using System.Collections.Generic;
using AXMLEngine;
using OFSCommonMethods.OFSML.OFSML130;

namespace OFSCommonMethods.OFSML
{
    public interface IOfsmlCommands
    {
        string OfsmlStandardEnquiryRequest(
            string userName, 
            string password, 
            string enqName,
            List<Filter> filters);

        string OfsmlTransactionRequest(
            string userName, 
            string password,
            string applicaiton, 
            string version,
            string company,
            string transactionId,
            FunctionCode function,
            OperationCode operation,
            List<Ofsml130MVField> fields);
    }
}
