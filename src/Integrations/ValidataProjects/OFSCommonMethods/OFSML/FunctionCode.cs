﻿namespace OFSCommonMethods.OFSML
{
    public enum FunctionCode
    {
        INPUT,
        SEE,
        AUTHORISE,
        REVERSE,
        DELETE
    }
}
