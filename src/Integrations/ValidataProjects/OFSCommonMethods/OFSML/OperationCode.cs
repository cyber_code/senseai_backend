﻿namespace OFSCommonMethods.OFSML
{
    public enum OperationCode
    {
        PROCESS,
        VALIDATE,
        BUILD
    }
}
