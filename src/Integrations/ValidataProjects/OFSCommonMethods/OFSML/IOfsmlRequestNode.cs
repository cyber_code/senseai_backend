﻿using System.Xml;

namespace OFSCommonMethods.OFSML
{
    public interface IOfsmlRequestNode
    {
        XmlNode XmlNode();
    }
}
