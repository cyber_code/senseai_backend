﻿using System.IO;
using System.Xml;
using AXMLEngine;
using System.Data;
using System.Collections.Generic;

namespace OFSCommonMethods
{
    public class GlobusMetadataReader
    {
        public const string DummyMetadataCatalog = "DummyMetadataCatalog";
        public const string XmlTag_Validata = "Validata";

        public static Instance ReadInstanceFromVxml(string ofsResponseAsVxml)
        {
            Instance result;
            using (TextReader textReader = new StringReader(ofsResponseAsVxml))
            {
                using (XmlReader xmlReader = XmlReader.Create(textReader))
                {
                    while (xmlReader.NodeType != XmlNodeType.Element)
                    {
                        xmlReader.Read();
                    }

                    if (xmlReader.Name == XmlTag_Validata)
                    {
                        xmlReader.Read();
                    }

                    result = Instance.FromXML(xmlReader);
                }
            }

            return result;
        }

        public static bool IsExpectedTypical(MetadataTypical typical)
        {
            return typical.CatalogName == DummyMetadataCatalog && typical.BaseClass == GlobusRequestBase.T24_DataProcess;
        }

        public static MetadataTypical GetMetadataTypical(string typicalName)
        {
            MetadataTypical metadataTypical = new MetadataTypical();
            metadataTypical.Name = typicalName;
            metadataTypical.BaseClass = GlobusRequestBase.T24_DataProcess;
            metadataTypical.ID = "1"; // has to exist
            metadataTypical.CatalogName = DummyMetadataCatalog;
            return metadataTypical;
        }

        public static MetadataTypical GetMetadataTypicalForStandardSelection()
        {
            return GetMetadataTypical("STANDARD.SELECTION");
        }

        public static MetadataTypical GetMetadataTypicalForDMMappingDefinition()
        {
            return GetMetadataTypical("DM.MAPPING.DEFINITION");
        }

        public static MetadataTypical GetMetadataTypicalForVersion()
        {
            return GetMetadataTypical("VERSION");
        }

        public static MetadataTypical GetMetadataTypicalFromXSDFile(string xsdFileName)
        {
            List<MetadataTypical> lsAllTypicals = GetMetadataTypicalsFromXSDFile(xsdFileName);
            System.Diagnostics.Debug.Assert(lsAllTypicals.Count == 1);
            return lsAllTypicals[0];
        }

        internal static List<MetadataTypical> GetMetadataTypicalsFromXSDFile(string xsdFileName)
        {
            DataSet ds = new DataSet();
            ds.ReadXmlSchema(xsdFileName);

            List<MetadataTypical> result = new List<MetadataTypical>();

            foreach (DataTable table in ds.Tables)
            {
                if (table.TableName == "dataroot")
                {
                    continue;
                }

                result.Add(GetMetadataTypicalFromDataTable(table));
            }

            return result;
        }

        private static MetadataTypical GetMetadataTypicalFromDataTable(DataTable table)
        {
            MetadataTypical result = new MetadataTypical();
            result.Name = table.TableName;

            if (table.ExtendedProperties.Contains("Catalog"))
            {
                result.CatalogName = (string)table.ExtendedProperties["Catalog"];
            }

            foreach (DataColumn dataColumn in table.Columns)
            {
                if (dataColumn.ColumnName == "dataroot_Id")
                {
                    continue;
                }

                result.AddAttribute(GetTypicalAttributeFromDataColumn(dataColumn));
            }

            return result;
        }

        private static TypicalAttribute GetTypicalAttributeFromDataColumn(DataColumn column)
        {
            TypicalAttribute result = new TypicalAttribute();
            result.Name = column.ColumnName;
            result.Type = Validata.Common.AttributeDataTypeConvertor.FromSystemType(column.DataType);

            if (column.ExtendedProperties != null)
            {
                if (column.ExtendedProperties.ContainsKey("DataLength"))
                {
                    result.DataLength = ulong.Parse(column.ExtendedProperties["DataLength"].ToString());
                }

                if (column.ExtendedProperties.ContainsKey("Remarks"))
                {
                    result.Remarks = column.ExtendedProperties["Remarks"].ToString();
                }
            }

            return result;
        }
    }
}