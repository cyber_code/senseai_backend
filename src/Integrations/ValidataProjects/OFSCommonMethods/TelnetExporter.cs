using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Security.Principal;
using System.Text;
using System.Linq;
using System.Threading;
using System.Xml;
using AXMLEngine;
using OFSCommonMethods.IO;
using OFSCommonMethods.IO.Telnet;
using ValidataCommon;

namespace OFSCommonMethods
{
    /// <summary>
    /// Manager of multiple Telnet requests/responses
    /// </summary>
    public class TelnetExporter
    {
        protected class GeneratorResultDetails
        {
            public int InstanceIndex;
            public int Thread;
            public string Instances;
            public string Errors;
            public bool WriteInstances;
            public string FirstMessage;
            public string Result;
            public DateTime StartTime;
            public DateTime EndTime;
        }

        #region Private Members

        private List<GeneratorResultDetails> _CurrentDetailsChunk = new List<GeneratorResultDetails>();

        protected TelnetMessageGenerator[] _Generators;

        /// <summary>
        /// Hashtable of catalogs that contain arrays of typicals
        /// </summary>
        private readonly Hashtable _Catalogs = new Hashtable();

        /// <summary>
        /// The message queue manager
        /// </summary>
        private readonly MessageQueueManager _MessageQueue;

        /// <summary>
        /// The parameters that have to be passed to the DoOFS or DoHistory routines
        /// </summary>
        private OFSParameters _OFSParameters;

        /// <summary>
        /// The GUID of the current thread
        /// </summary>
        public string _GUID;

        /// <summary>
        /// Processed requests
        /// </summary>
        private List<GlobusRequestShort> _ProcessedInstances;

        /// <summary>
        /// Full Output VXML XmlTextWriter
        /// </summary>
        private readonly XmlWriter _OutputVxmlWriter;

        /// <summary>
        /// Full Error XmlTextWriter
        /// </summary>
        private readonly XmlWriter _ErrorsXmlWriter;

        /// <summary>
        /// XML writer for executon times
        /// </summary>
        private readonly XmlWriter _ExecutionTimesXmlWriter;

        /// <summary>
        /// Full OFS Globus Messages XmlTextWriter
        /// </summary>
        private readonly XmlWriter _OFSGlobusMessageWriter;

        /// <summary>
        /// Full OFS Validata Messages XmlTextWriter
        /// </summary>
        private readonly XmlWriter _OFSValidataMessageWriter;

        private bool _AlwaysUseAuthentication;

        private bool _SkipRestOfInstances;

        private int _StopOnNotResponding = -1;

        private uint _MaxNumberOfInstancesInChunk = uint.MinValue;

        private int _SleepInMillisecondsBeforeRetry = int.MinValue;

        private int _CountOfInstances;

        private readonly object _SyncObject = new object();

        /// <summary>
        /// True if the OFS should be in VALIDATE mode
        /// </summary>
        private bool _IsValidateMode;

        /// <summary>
        /// True if the OFS should be in VALIDATE mode
        /// </summary>
        private bool _IsIAMode = true;

        /// <summary>
        /// Sends I messages with no attributes
        /// </summary>
        private bool _IsHAMode;

        /// <summary>
        /// True if the OFS should be in VALIDATE mode
        /// </summary>
        public bool IsValidateMode
        {
            get { return _IsValidateMode; }
            set { _IsValidateMode = value; }
        }

        /// <summary>
        /// True if the export to Globus should be in separate insert and autorize steps
        /// </summary>
        public bool IsIAMode
        {
            get { return _IsIAMode; }
            set { _IsIAMode = value; }
        }

        public bool IsHAMode
        {
            get { return _IsHAMode; }
            set { _IsHAMode = value; }
        }

        private bool StopOnNotResponding
        {
            get
            {
                if (_StopOnNotResponding < 0)
                {
                    try
                    {
                        string val = "True";
                        _StopOnNotResponding = ((string.Compare(val, "true", true) == 0) ? 1 : 0);
                    }
                    catch
                    {
                        _StopOnNotResponding = 0;
                    }
                }

                return (_StopOnNotResponding == 1);
            }
        }

        private GlobusImportType _ImportType;

        private List<string> _EnquiryResults;

        public uint MaxNumberOfInstancesInChunk
        {
            get
            {
                if (_MaxNumberOfInstancesInChunk == uint.MinValue)
                {
                    uint confValue;
                    confValue = 400;

                    _MaxNumberOfInstancesInChunk = confValue;
                }

                return _MaxNumberOfInstancesInChunk;
            }
        }

        public int SleepInMillisecondsBeforeRetry
        {
            get
            {
                if (_SleepInMillisecondsBeforeRetry == int.MinValue)
                {
                    int configValue;
                    configValue = 500; //TODO this might be too big number

                    _SleepInMillisecondsBeforeRetry = configValue;
                }

                return _SleepInMillisecondsBeforeRetry;
            }
        }

        public GlobusImportType ImportType
        {
            get { return _ImportType; }
            set { _ImportType = value; }
        }

        protected Thread[] _Threads;

        protected virtual int TimeoutStep1
        {
            get { return 300000; }
        }

        private readonly AXMLEngine.AXMLEngine _Engine;

        private readonly Settings _Settings;

        private List<SettingsSet> _SettingsSets;

        protected string TelnetConnectionsGroupID = "t";

        protected List<TSSTelnetComm> _TelnetConnectors;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Initializes a new instance of the <see cref="TelnetExporter"/> class.
        /// </summary>
        /// <param name="messageQueue">The message queue.</param>
        /// <param name="engine">The engine.</param>
        /// <param name="settings">The settings.</param>
        /// <param name="ofsParam">The ofs param.</param>
        /// <param name="vxmlWriter">The XML writer.</param>
        /// <param name="errorWriter">The error writer.</param>
        /// <param name="executionTimesWriter">The execution times writer.</param>
        /// <param name="ofsGlobusMessageWriter">The ofs globus message writer.</param>
        /// <param name="ofsValidataMessageWriter">The ofs validata message writer.</param>
        /// <param name="adapterIdentity">The adapter identity.</param>
        public TelnetExporter(MessageQueueManager messageQueue,
                              AXMLEngine.AXMLEngine engine,
                              Settings settings,
                              OFSParameters ofsParam,
                              XmlWriter vxmlWriter,
                              XmlWriter errorWriter,
                              XmlWriter executionTimesWriter,
                              XmlWriter ofsGlobusMessageWriter,
                              XmlWriter ofsValidataMessageWriter,
                              WindowsIdentity adapterIdentity)
        {
            _MessageQueue = messageQueue;
            _OFSParameters = ofsParam;
            _Settings = settings;
            _Engine = engine;
            _OFSParameters.AdapterIdentity = adapterIdentity;

            // organize typicals in catalogs
            BuildTypicalCatalogs(engine.DefaultRequest.RequestMetadata.Typicals);

            _OutputVxmlWriter = vxmlWriter;
            _ErrorsXmlWriter = errorWriter;
            _ExecutionTimesXmlWriter = executionTimesWriter;
            _OFSValidataMessageWriter = ofsValidataMessageWriter;
            _OFSGlobusMessageWriter = ofsGlobusMessageWriter;

            _EnquiryResults = new List<string>();

            if(_ExecutionTimesXmlWriter != null)
                ExecutionTimingsList.OpenRootNode(_ExecutionTimesXmlWriter);
        }

        #endregion

        #region Methods

        public void SetRecordsToProcess(IEnumerable<IValidataRecord> records)
        {
            _EnquiryResults = records.Select(n => n.FindAttribute("@ID")).Where(n => n != null).Select(n => n.Value).Distinct().ToList();
        }

        public void DoWriteInstances(XmlWriter xmlWriter)
        {
            try
            {
                if (_EnquiryResults != null && _EnquiryResults.Count > 0)
                {
                    ProcessEnquiryResults(xmlWriter);
                }
            }
            finally
            {
                FinalizeAllThreads(xmlWriter);
            }
        }

        public void DoWriteEnquiries(XmlWriter xmlWriter)
        {
            try
            {
                if (_Engine.DefaultRequest.Filters.FilterList.Count > 0)
                {
                    foreach (Filter currFilter in _Engine.DefaultRequest.Filters.FilterList)
                    {
                        // find free generator
                        TelnetMessageGenerator freeGenerator = WaitAndGetFirstFreeENQGenerator();

                        // set the filters to the current one
                        freeGenerator.Filters = new List<Filter> {currFilter};
                        freeGenerator.IsBusy = true;
                        freeGenerator.VxmlWriter = xmlWriter;
                        freeGenerator.EnquiryResults = new List<string>();

                        freeGenerator.GoEvent.Set();
                    }
                }
                else
                {
                    // find free generator
                    TelnetMessageGenerator freeGenerator = WaitAndGetFirstFreeENQGenerator();
                    freeGenerator.IsBusy = true;
                    freeGenerator.VxmlWriter = xmlWriter;
                    freeGenerator.EnquiryResults = new List<string>();

                    freeGenerator.GoEvent.Set();
                }

                // Wait for all to finish
                WaitAllENQGeneratorsToFinish();
            }
            finally
            {
                FinalizeAllThreads();
            }
        }

        public void DoEnquiryAndWriteInstances(XmlWriter xmlWriter)
        {
            try
            {
                _Generators[0].IsBusy = true;
                _Generators[0].VxmlWriter = xmlWriter;
                _Generators[0].EnquiryResults = new List<string>();

                _Generators[0].GoEvent.Set();
                WaitHandle.WaitAny(new WaitHandle[] { _Generators[0].EndEvent });

                if (_Generators[0].EnquiryResults != null && _Generators[0].EnquiryResults.Count > 0)
                {
                    _EnquiryResults = _Generators[0].EnquiryResults;
                    // Reuse first generator and thread
                    TelnetMessageGenerator firstGenerator = _Generators[0];

                    firstGenerator.EndEvent = new ManualResetEvent(false);
                    firstGenerator.EnquiryResults = new List<string>();

                    firstGenerator.ClearData();
                    _Threads[0] = new Thread(firstGenerator.DoProduction);
                    _Threads[0].Name = string.Format("'{0}' Telnet gen: {1}; comm: {2}; time: {3}", "DoProduction", 0, 0, DateTime.Now.ToLongTimeString());
                    _Threads[0].Start();

                    ProcessEnquiryResults(xmlWriter);
                }
            }
            finally
            {
                FinalizeAllThreads(xmlWriter);
            }
        }

        private void ProcessEnquiryResults(XmlWriter vxmlWriter)
        {
            for (int i = 0; i < _EnquiryResults.Count; i++)
            {
                Instance inst = OFSMessageGeneratorBase.CreateFakeInstance(_EnquiryResults[i]);
                ExecuteCommandInAnyAvailableGenerator(inst, vxmlWriter);
            }
        }

        public void FinalizeAllThreads()
        {
            FinalizeAllThreads(null);
        }

        protected virtual void FinalizeAllThreads(XmlWriter vxmlWriter)
        {
            try
            {
                CancelExecutionOfGenerators();

                WaitForGeneratorsToCompleteExecution();

                ProcessCompletedGenerators();

                FlushCurrentResultsChunk(vxmlWriter);

                FinalizeOutput();
            }
            finally
            {
                _OFSGlobusMessageWriter.Flush();
                _OFSValidataMessageWriter.Flush();
                _OutputVxmlWriter.Flush();
                _ErrorsXmlWriter.Flush();
                _ExecutionTimesXmlWriter.Flush();

                _OFSGlobusMessageWriter.Close();
                _OFSValidataMessageWriter.Close();
                _OutputVxmlWriter.Close();
                _ErrorsXmlWriter.Close();
            }
        }

        protected void WaitForGeneratorsToCompleteExecution()
        {
            foreach (TelnetMessageGenerator generator in _Generators)
            {
                WaitHandle.WaitAny(new WaitHandle[] { generator.EndEvent });
            }
        }

        private void FinalizeOutput()
        {
            ExecutionTimingsList.CloseRootNode(_ExecutionTimesXmlWriter);
        }

        private void WriteExecutionTimes(IEnumerable<ExecutionTiming> timings)
        {
            ExecutionTimingsList executionTimingsList = new ExecutionTimingsList();
            executionTimingsList.AddRange(timings);
            executionTimingsList.ToXML(_ExecutionTimesXmlWriter);
        }

        public void InitializeThreadsAndConnect()
        {
            _CountOfInstances = 0;
            bool isExport = (_Engine.DefaultRequest.Type == RequestType.Export);

            _SettingsSets = new List<SettingsSet>();
            _SettingsSets.Add(new SettingsSet(_Settings));

            if (!String.IsNullOrEmpty(_Settings.AdditionalPhantomSet))
            {
                string[] telnetSets = _Settings.AdditionalPhantomSet.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string telnetSet in telnetSets)
                {
                    Settings settings = new Settings();
                    settings.PhantomSet = telnetSet.Trim();
                    ExecutionStepContainer.LoadConnectionSettings(settings);

                    _SettingsSets.Add(new SettingsSet(settings));
                }
            }

            int minNumberOfThreads = 0;
            int threadsInitializationTimeout = 0;
            foreach (SettingsSet set in _SettingsSets)
            {
                int numberOfThreads = isExport
                                          ? set.Settings.TelnetExportToT24ThreadsCount
                                          : set.Settings.TelnetImportFromT24ThreadsCount;

                int currentMinNumberOfThreads = isExport
                                          ? set.Settings.TelnetExportToT24MinThreadsCount
                                          : set.Settings.TelnetImportFromT24MinThreadsCount;

                int currentThreadsResponseTimeout = isExport && set.Settings.TelnetThreadsConnectResponseTimeout > 0 
                          ? set.Settings.TelnetThreadsConnectResponseTimeout
                          : String.IsNullOrEmpty(set.Settings.TelnetResponseTimeOut) ? 5000 : int.Parse(set.Settings.TelnetResponseTimeOut);


                int currentThreadsInitializationTimeout = isExport
                                          ? set.Settings.TelnetExportToT24ThreadsInitializationTimeout
                                          : set.Settings.TelnetImportFromT24ThreadsInitializationTimeout;

                if (currentMinNumberOfThreads < 0 || currentMinNumberOfThreads > numberOfThreads)
                {
                    currentMinNumberOfThreads = numberOfThreads;
                }

                set.NumberOfThreads = numberOfThreads;
                set.ThreadsInitializationTimeout = currentThreadsInitializationTimeout;
                set.ThreadsResponseTimeout = currentThreadsResponseTimeout;

                minNumberOfThreads += currentMinNumberOfThreads;
                threadsInitializationTimeout += currentThreadsInitializationTimeout;
            }

            List<int> initializedConnectorIndices = TelnetCommPool.InitializeTelnetConnectionsPool(_SettingsSets, TelnetConnectionsGroupID, out _TelnetConnectors);

            System.Diagnostics.Debug.Assert(initializedConnectorIndices.Count == TelnetCommPool.CommunicatorsCount);
            System.Diagnostics.Debug.WriteLine("Number of active threads " + TelnetCommPool.CommunicatorsCount);

            if (initializedConnectorIndices.Count < minNumberOfThreads)
            {
                throw new Exception(
                            string.Format(
                                    "The minimum needed number of threads {0} could not be reached in defined timeout: {1} milliseconds. Only {2} where created. " +
                                    "(See attribute: 'initializationTimeout' of 'TelnetExportToT24ThreadsCount'/'TelnetMultythreadedImportFromT24' in telnet set to increase the initialization timeout)",
                                    minNumberOfThreads,
                                    threadsInitializationTimeout,
                                     TelnetCommPool.CommunicatorsCount
                                )
                        );
            }

            _Threads = new Thread[initializedConnectorIndices.Count];
            _Generators = new TelnetMessageGenerator[initializedConnectorIndices.Count];

            int genIdx = 0;
            foreach (int i in initializedConnectorIndices)
            {
                InitializeGeneratorAndStartThread(i, genIdx++);
            }
        }

        private void InitializeGeneratorAndStartThread(int communicatorIdx, int generatorIdx)
        {
            if (generatorIdx >= _Generators.Length)
            {
                // throw a meaningful error
                throw new ApplicationException(string.Format( "Trying to access generator with index {0} when there are only {1} T24 message generators", generatorIdx, _Generators.Length));
            }

            Settings settings = null;
            if (_SettingsSets != null)
            {
                var settingSet = _SettingsSets.FirstOrDefault(n=>n.InitializedConnectorIndices != null && n.InitializedConnectorIndices.Contains(communicatorIdx));
                if(settingSet != null)
                    settings = settingSet.Settings;
            }

            if (settings == null)
                settings = _Settings;

            TSSTelnetComm communicator = null;
            try
            {
                if (!_SkipRestOfInstances)
                    communicator = TelnetCommPool.GetOrCreateCommunicatorForPerformance(communicatorIdx, settings, TelnetConnectionsGroupID);
            }
            catch (Exception)
            {
                _SkipRestOfInstances = true;
            }

            TelnetMessageGenerator generator = new TelnetMessageGenerator(
                _MessageQueue,
                _Engine,
                settings,
                _OFSParameters,
                communicator,
                _AlwaysUseAuthentication,
                _IsValidateMode);

            generator.ImportType = _ImportType;
            //generator.IsBusy = true;
           // generator.VxmlWriter = _w;
            generator.MultithreadedCommunicatorIdx = communicatorIdx;
            generator.TelnetConnectionsGroupID = TelnetConnectionsGroupID;
            generator.GoEvent = new AutoResetEvent(false);
            generator.EndEvent = new ManualResetEvent(false);
            generator.CustomEnquiries = _Settings.CustomEnquiries;

            _Generators[generatorIdx] = generator;

            string genName = GetThreadStartName(generator);
            _Threads[generatorIdx] = new Thread(GetThreadStart(generator));
            _Threads[generatorIdx].Name = string.Format("'{0}' Telnet gen: {1}; comm: {2}; time: {3}", genName, generatorIdx, communicatorIdx, DateTime.Now.ToLongTimeString());
            _Threads[generatorIdx].Start();
           // generator.GoEvent.Set();
        }

        private ThreadStart GetThreadStart(TelnetMessageGenerator generator)
        {
            if (_Engine.DefaultRequest.Type == RequestType.Export)
                return generator.DoOFS;
            if(_ImportType == GlobusImportType.TESTDATA
                || _ImportType == GlobusImportType.MULTI_ENQ)
                return generator.DoEnquiry;

            return generator.DoProduction;
        }

        private string GetThreadStartName(TelnetMessageGenerator generator)
        {
            if (_Engine.DefaultRequest.Type == RequestType.Export)
                return "DoOFS";
            if (_ImportType == GlobusImportType.TESTDATA
                || _ImportType == GlobusImportType.MULTI_ENQ)
                return "DoEnquiry";

            return "DoProduction";
        }

        private void WriteInstances(IEnumerable<string> instances, XmlWriter xmlWriter)
        {
            foreach (string instance in instances)
            {
                if (xmlWriter != null)
                {
                    xmlWriter.WriteRaw(instance);
                }
                else
                {
                    _OutputVxmlWriter.WriteRaw(instance);
                    _OutputVxmlWriter.WriteRaw("\n\t");
                }
            }

            if (xmlWriter != null)
            {
                xmlWriter.Flush();
            }
            else
            {
                _OutputVxmlWriter.Flush();
            }
        }

        private void WriteErrors(IEnumerable<string> errors)
        {
            foreach (string error in errors)
            {
                OFSMessageGenerator.GenerateErrorFile(null, null, null, null, error, _ErrorsXmlWriter);
            }

            _ErrorsXmlWriter.Flush();
        }

        private void WriteFirstMessages(IEnumerable<string> firstMessages)
        {
            foreach (string messages in firstMessages)
            {
                _OFSValidataMessageWriter.WriteRaw(messages);
                _OFSValidataMessageWriter.WriteRaw("\n\t");
            }

            _OFSValidataMessageWriter.Flush();
        }

        private void WriteResults(IEnumerable<string> results)
        {
            foreach (string result in results)
            {
                _OFSGlobusMessageWriter.WriteRaw(result);
                _OFSGlobusMessageWriter.WriteRaw("\n\t");
            }

            _OFSGlobusMessageWriter.Flush();
        }

        protected void ProcessCompletedGenerators()
        {
            for (int i = 0; i < _Generators.Length; i++)
            {
                if (!_Generators[i].IsBusy)
                {
                    // TODO save only the ones that have not been processed
                    SaveGeneratorDetails(i, null);
                }
            }
        }

        private void FlushCurrentResultsChunk(XmlWriter vxmlWriter)
        {
            WriteInstances(GetInstancesInChunk(), vxmlWriter);
            WriteErrors(GetErrorsInChunk());
            WriteFirstMessages(GetFirstMessagesInChunk());
            WriteResults(GetResultsInChunk());
            WriteExecutionTimes(GetExecutionTimesInChunk());

            _CurrentDetailsChunk = new List<GeneratorResultDetails>();
        }

        private IEnumerable<string> GetInstancesInChunk()
        {
            foreach (GeneratorResultDetails details in _CurrentDetailsChunk)
            {
                if (!String.IsNullOrEmpty(details.Instances) && details.WriteInstances)
                {
                    yield return details.Instances;
                }
            }
        }

        private IEnumerable<string> GetErrorsInChunk()
        {
            foreach (GeneratorResultDetails details in _CurrentDetailsChunk)
            {
                if (!String.IsNullOrEmpty(details.Errors))
                {
                    yield return details.Errors;
                }
            }
        }

        private IEnumerable<string> GetFirstMessagesInChunk()
        {
            foreach (GeneratorResultDetails details in _CurrentDetailsChunk)
            {
                if (!String.IsNullOrEmpty(details.FirstMessage))
                {
                    string msg = GetXmlFragmentString(
                        "message",
                        new Dictionary<string, object>
                            {
                                {"InstanceID", details.InstanceIndex},
                                {"Thread", details.Thread},
                            },
                        details.FirstMessage);

                    yield return msg;
                }
            }
        }

        private static string GetXmlFragmentString(string elementName, Dictionary<string, object> attributes, string elementText)
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (XmlWriter writer = new SafeXmlTextWriter(sw))
                {
                    writer.WriteStartElement(elementName);
                    foreach (KeyValuePair<string, object> attr in attributes)
                    {
                        writer.WriteAttributeString(attr.Key, attr.Value.ToString());
                    }

                    writer.WriteString(elementText);
                }
            }

            return sb.ToString();
        }

        private IEnumerable<string> GetResultsInChunk()
        {
            foreach (GeneratorResultDetails details in _CurrentDetailsChunk)
            {
                if (!String.IsNullOrEmpty(details.Result))
                {
                    string result = GetXmlFragmentString(
                        "result",
                        new Dictionary<string, object>
                            {
                                {"InstanceID", details.InstanceIndex},
                                {"Thread", details.Thread},
                            },
                        details.Result);

                    yield return result;
                }
            }
        }

        private IEnumerable<ExecutionTiming> GetExecutionTimesInChunk()
        {
            foreach (GeneratorResultDetails details in _CurrentDetailsChunk)
            {
                yield return new ExecutionTiming
                                 {
                                     InstanceID = (ulong)details.InstanceIndex,
                                     Start = details.StartTime,
                                     End = details.EndTime,
                                 };
            }
        }

        private void SaveGeneratorDetails(int generatorIndex, XmlWriter vxmlWriter)
        {
            TelnetMessageGenerator generator = _Generators[generatorIndex];
            if (!generator.IsInitialized)
            {
                return;
            }

            GeneratorResultDetails details = new GeneratorResultDetails();
            details.Thread = generatorIndex;
            details.InstanceIndex = generator.InstanceIndex;
            details.StartTime = generator.OperationStart;
            details.EndTime = generator.OperationEnd;
            generator.GetResult(out details.Instances, out details.Errors, out details.WriteInstances, out details.FirstMessage, out details.Result);

            System.Diagnostics.Debug.Assert(!string.IsNullOrEmpty(details.Instances) || !string.IsNullOrEmpty(details.Errors)
                , "At this stage either instances should be presented either error. Otherwise something is wrong in the preceeding logic!");

            ProcessNewGeneratorDetails(vxmlWriter, details);
        }

        protected void CancelExecutionOfGenerators()
        {
            Double timeOut;
            Double.TryParse(_Settings.TelnetResponseTimeOut, out timeOut);
            for (int i = 0; i < _Generators.Length; i++)
            {
                DateTime dtStart = DateTime.Now;

                while (_Generators[i].IsBusy && ((DateTime.Now - dtStart).TotalMilliseconds < timeOut))
                {
                    Thread.Sleep(50);
                }

                _Generators[i].CancelExecution = true;
                _Generators[i].GoEvent.Set();
            }
        }

        private static bool IsInState(Thread thread, ThreadState state)
        {
            // the ThreadState is FlagAttribute enum, so we should use bitwise logic
            return (thread.ThreadState & state) == state;
        }

        private int GetIndexOfFirstWaitingThread()
        {
            for (int i = 0; i < _Generators.Length; i++)
            {
                if (IsInState(_Threads[i], ThreadState.WaitSleepJoin) && !_Generators[i].IsBusy)
                {
                    // unused thread that is blocked since it has no work to do
                    return i;
                }
            }

            return -1;
        }

        private TelnetMessageGenerator WaitAndGetFirstFreeENQGenerator()
        {
            DateTime timeoutExpirationTime = DateTime.Now.AddMilliseconds(TimeoutStep1);

            do
            {
                for (int i = 0; i < _Generators.Length; i++)
                {
                    if (!_Generators[i].IsBusy 
                        && (!_Threads[i].IsAlive || IsInState(_Threads[i], ThreadState.WaitSleepJoin)))
                    {
                        // Create thread and start it
                        _Threads[i] = new Thread(_Generators[i].DoEnquiry);
                        _Threads[i].Name = string.Format("'{0}' Telnet gen: {1}; comm: {2}; time: {3}", "DoEnquiry", i, i, DateTime.Now.ToLongTimeString());
                        _Threads[i].Start();

                        // Reset generator
                        var freeGenerator = _Generators[i];
                        freeGenerator = _Generators[i];
                        freeGenerator.GoEvent.Reset();
                        freeGenerator.EndEvent.Reset();
                        freeGenerator.EnquiryResults = new List<string>();
                        freeGenerator.ClearData();

                        return freeGenerator;
                    }
                }

                System.Threading.Thread.Sleep(50);
            } while (timeoutExpirationTime > DateTime.Now);

            throw new ApplicationException("Timeout for waiting for a free ENQ processing thread expired!");
        }

        private void WaitAllENQGeneratorsToFinish()
        {
            DateTime timeoutExpirationTime = DateTime.Now.AddMilliseconds(TimeoutStep1);

            var freeGenerators = new bool [_Generators.Length];

            do
            {
                for (int i = 0; i < _Generators.Length; i++)
                {
                    if (!_Generators[i].IsBusy
                        && (!_Threads[i].IsAlive || IsInState(_Threads[i], ThreadState.WaitSleepJoin)))
                    {
                        freeGenerators[i] = true;
                    }
                }

                if (freeGenerators.All(n => n))
                    return;

                System.Threading.Thread.Sleep(100);
            } while (timeoutExpirationTime > DateTime.Now);

            throw new ApplicationException("Timeout for waiting for a free ENQ processing thread expired!");
        }

        private int GetIndexOfFirstCompletedThread()
        {
            for (int i = 0; i < _Generators.Length; i++)
            {
                if (IsThreadCompleted(_Threads[i]))
                {
                    return i;
                }
            }
            return -1;
        }

        private static bool IsThreadCompleted(Thread thread)
        {
            return IsInState(thread, ThreadState.Stopped)
                   || IsInState(thread, ThreadState.StopRequested)
                   || IsInState(thread, ThreadState.Aborted)
                   || IsInState(thread, ThreadState.AbortRequested);
        }

        private void ExecuteCommandInAnyAvailableGenerator(Instance instance, XmlWriter vxmlWriter)
        {
            // NOTE: actually locking is not necessary, since this function currently is not called concurrently, but just in case :)
            lock (_SyncObject)
            {
                _CountOfInstances++;

                long ticksOfTimeoutTime = DateTime.Now.AddMilliseconds(TimeoutStep1).Ticks;
                while (ticksOfTimeoutTime > DateTime.Now.Ticks)
                {
                    // if there is a thread that waits use it
                    int indexOfWaiting = GetIndexOfFirstWaitingThread();
                    if (indexOfWaiting >= 0)
                    {
                        PrepareGeneratorAndLetItRun(indexOfWaiting, instance);
                        return;
                    }

                    // else if there is a thread that has completed, initialize it and use it
                    int indexOfCompleted = GetIndexOfFirstCompletedThread();
                    if (indexOfCompleted >= 0)
                    {
                        int communicatorIndex = _Generators[indexOfCompleted].MultithreadedCommunicatorIdx;
                        System.Diagnostics.Debug.Assert(communicatorIndex >= 0);
                        SaveGeneratorDetails(indexOfCompleted, vxmlWriter);
                        InitializeGeneratorAndStartThread(communicatorIndex, indexOfCompleted);
                        PrepareGeneratorAndLetItRun(indexOfCompleted, instance);
                        return;
                    }

                    // else if no available generator is found, wait a while before trying to find one again
                    Thread.Sleep(SleepInMillisecondsBeforeRetry);
                }

                // No avaiable generator was found during the allowed time - we can only log an error
                LogRequestThatTimedOut(vxmlWriter);
            }
        }

        private void PrepareGeneratorAndLetItRun(int i, Instance inst)
        {
            TelnetMessageGenerator generator = _Generators[i];
            generator.IsBusy = true;
            generator.InstanceIndex = _CountOfInstances - 1;
            generator.Instance = inst;
            generator.SkipInstance = _SkipRestOfInstances;


            // let the processing start
            generator.GoEvent.Set();
        }

        private void LogRequestThatTimedOut(XmlWriter vxmlWriter)
        {
            GeneratorResultDetails skippedItemDetails = new GeneratorResultDetails();
            skippedItemDetails.Thread = -1;
            skippedItemDetails.InstanceIndex = _CountOfInstances - 1;
            skippedItemDetails.StartTime = DateTime.Now;
            skippedItemDetails.EndTime = skippedItemDetails.StartTime;
            skippedItemDetails.Errors = "Timeout while waiting to process an instance at index = " + skippedItemDetails.InstanceIndex;

            ProcessNewGeneratorDetails(vxmlWriter, skippedItemDetails);
        }

        protected virtual void ProcessNewGeneratorDetails(XmlWriter vxmlWriter, GeneratorResultDetails details)
        {
            _CurrentDetailsChunk.Add(details);

            if (_CurrentDetailsChunk.Count >= MaxNumberOfInstancesInChunk)
            {
                FlushCurrentResultsChunk(vxmlWriter);
            }
        }

        /// <summary>
        /// Organizes the typicals in this AXML request by their catalog name. Later, this collection
        /// will be used to find the typical by catalog name and typical name
        /// </summary>
        /// <param name="typicals">The collection of all typicals in the request</param>
        private void BuildTypicalCatalogs(TypicalCollection typicals)
        {
            for (int i = 0; i < typicals.Count; i++)
            {
                MetadataTypical tmpTypical = typicals[i];

                Hashtable typicalHash;
                if (_Catalogs.ContainsKey(tmpTypical.CatalogName))
                {
                    typicalHash = (Hashtable)_Catalogs[tmpTypical.CatalogName];
                    typicalHash[tmpTypical.Name] = tmpTypical;
                }
                else
                {
                    typicalHash = new Hashtable();
                    _Catalogs[tmpTypical.CatalogName] = typicalHash;
                    typicalHash[tmpTypical.Name] = tmpTypical;
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the parameters of the Globus task
        /// </summary>
        public OFSParameters Parameters
        {
            get { return _OFSParameters; }
            set { _OFSParameters = value; }
        }

        public List<GlobusRequestShort> ProcessedInstances
        {
            get { return _ProcessedInstances; }
            set { _ProcessedInstances = value; }
        }

        public bool AlwaysUseAuthentication
        {
            get { return _AlwaysUseAuthentication; }
            set { _AlwaysUseAuthentication = value; }
        }

        #endregion
    }

    public class SettingsSet
    {
        #region Properties

        public Settings Settings { get; set; }
        public int NumberOfThreads { get; set; }
        public int ThreadsInitializationTimeout { get; set; }
        public int ThreadsResponseTimeout { get; set; }

        public List<int> InitializedConnectorIndices { get; set; }

        #endregion

        #region Initialization

        public SettingsSet()
        {
            this.InitializedConnectorIndices = new List<int>();
        }

        public SettingsSet(Settings settings)
            : this()
        {
            this.Settings = settings;
        }

        public SettingsSet(Settings settings, int numberOfThreads, int threadsInitializationTimeout)
            : this(settings)
        {
            this.NumberOfThreads = numberOfThreads;
            this.ThreadsInitializationTimeout = threadsInitializationTimeout;
        }

        #endregion
    }
}