﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Configuration;
using System.Web;
using System.Xml;
using System.Linq;
using AXMLEngine;
using OFSCommonMethods.OFSEnquiry;
using OFSStringConvertor;
using ValidataCommon;

namespace OFSCommonMethods
{
    public delegate void CustomAttributeAppenderHandler(NameValueCollection currentFieldValues, string attributeName, string attributeValue);

    public abstract class OFSResponseReaderBase
    {
        #region Public Constants

        /// <summary>
        /// A string that contains a result that no records were found for an enquiry
        /// </summary>
        public const string NO_RECORDS_STRING = "No records were found that matched the selection criteria";

        /// <summary>
        /// Destination of EnquiryConfigurationFile
        /// </summary>
        public static readonly string EnquiryConfigurationFileName = String.IsNullOrEmpty(ValidataCommon.AdapterConfiguration.AdapterFilesRootPath)
            ? Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\Assemblies\EnquiryConfiguration.xml" :
            Environment.ExpandEnvironmentVariables(ValidataCommon.AdapterConfiguration.AdapterFilesRootPath) + @"\Assemblies\EnquiryConfiguration.xml";

        #endregion

        #region Private Helper Classes

        private class OfsReponse
        {
            internal string TransactionID;

            internal string MessageID;

            internal string StatusIndicator;

            internal string FullText;

            internal string FieldsText;

            internal bool IsSuccess
            {
                get
                {
                    return StatusIndicator == "1"
                        && !FullText.Contains("DUPLICATE.TRAP:1:1=TRUE");
                }
            }

            internal string ErrorMessage
            {
                get
                {
                    if (IsSuccess)
                    {
                        return null;
                    }

                    if (!String.IsNullOrEmpty(FieldsText))
                    {
                        // The error that is 
                        return FieldsText;
                    }

                    return FullText;
                }
            }

            public override string ToString()
            {
                return IsSuccess
                    ? MessageID + " -  success"
                    : (MessageID ?? "") + " -  failed: " + ErrorMessage;
            }
        }

        #endregion

        #region Private and Protected Constants

        protected const string ID_ATTRIBUTE_NAME = "ID";

        #endregion

        #region Private and Protected Members

        /// <summary>
        /// The transaction ID assigned by Globus
        /// </summary>
        protected string _TransactionID;

        /// <summary>
        /// The OFS unique ID assigned to the user transaction
        /// </summary>
        protected string _OfsID;

        /// <summary>
        /// CURR.NO of the currently processed record
        /// </summary>
        protected string _CURRNO;

        /// <summary>
        /// Indicates if an error has occured
        /// </summary>
        protected bool _HasError;

        /// <summary>
        /// The typical describing the expected response.
        /// </summary>
        protected MetadataTypical _ResponseTypical = null;

        /// <summary>
        /// PhantomSet
        /// </summary>
        protected readonly string _PhantomSet = string.Empty;

        /// <summary>
        /// Contains list of typicals attribute names that starts with digit
        /// </summary>
        protected Dictionary<string, List<string>> _SpecialTypicalAttributes = null;

        /// <summary>
        /// Check for live record not changed
        /// </summary>
        protected readonly bool CheckForLiveRecordNotChanged;

        /// <summary>
        /// Determine if the FOREX is flat - with one LEG ot it is not flat - with two LEGs
        /// </summary>
        protected readonly bool _FxIsFlat = true;

        /// <summary>
        /// Determines whether to authorize first or second leg (for IA OFS commands)
        /// </summary>
        protected readonly bool _FxAuthorizeFirstLeg = true;

        /// <summary>
        /// Creation Date
        /// </summary>
        protected string _CreationDate = "";

        /// <summary>
        /// Version Number
        /// </summary>
        protected string _VersionNumber = "";

        /// <summary>
        /// Contains all added attribute names and values
        /// </summary>
        protected NameValueCollection _FieldsAndValues;

        /// <summary>
        /// The ValidataID
        /// </summary>
        protected string _ValidataID;

        /// <summary>
        /// The ID of the second leg of an FX Swap
        /// </summary>
        private string _FxSecondLegID;

        #endregion

        #region Public properties

        /// <summary>
        /// The transaction ID assigned by Globus to th
        /// </summary>
        public string TransactionID
        {
            get { return _TransactionID; }
        }

        /// <summary>
        /// The OFS unique ID assigned to the user transaction
        /// </summary>
        public string OFSID
        {
            get { return _OfsID; }
        }

        /// <summary>
        /// has error in transaction
        /// </summary>
        public bool HasError
        {
            get { return _HasError; }
        }

        /// <summary>
        /// Returns CUUR.NO of the currently processed record
        /// </summary>
        public string CURRNO
        {
            get { return _CURRNO; }
        }

        /// <summary>
        /// Allows attributes, not existing in the typical (by default they are ignored)
        /// </summary>
        public bool AllowAttributesNotInTypical;

        /// <summary>
        /// The @ID of the second leg of an FX Swap
        /// </summary>
        public string FxSecondLegID
        {
            get { return _FxSecondLegID; }
        }

        /// <summary>
        /// Override default logic for appending the parsed field values from the OFS message
        /// </summary>
        public CustomAttributeAppenderHandler CustomAttributeAppender;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Constructs an OFSResponseReaderBase instance
        /// </summary>.
        public OFSResponseReaderBase()
        {
            // TODO: make configurable
            AllowAttributesNotInTypical = true;

            _FxIsFlat = true;
            _FxAuthorizeFirstLeg = true;
            CheckForLiveRecordNotChanged = false;
        }

        public OFSResponseReaderBase(Dictionary<string, List<string>> specialTypicalAttributes)
            : this()
        {
            _SpecialTypicalAttributes = specialTypicalAttributes;
        }

        public OFSResponseReaderBase(Dictionary<string, List<string>> specialTypicalAttributes, string validataID)
            : this(specialTypicalAttributes)
        {
            _ValidataID = validataID;
        }

        public OFSResponseReaderBase(string phantomSet, Dictionary<string, List<string>> specialTypicalAttributes)
            : this(specialTypicalAttributes)
        {
            _PhantomSet = phantomSet;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Parses an OFS response message.
        /// </summary>
        /// <param name="ofsMessage">The text of the message as received from OFS.</param>
        /// <param name="responseTypical">The tag under which the resulting XML is packed.</param>
        /// <param name="isAuthorize">True if reading response to an authorize message</param>
        /// <param name="recordId">Record ID</param>
        /// <returns>The XML that is the result of the parsing and the conversion.</returns>
        public string ReadOFS(GlobusMessage.OFSMessage ofsMessage, MetadataTypical responseTypical, bool isAuthorize, string recordId, bool allowDifferentResponseID = false)
        {
            _ResponseTypical = responseTypical;
            _HasError = false;

            bool result;
            string errorMessage;

            if (responseTypical.BaseClass == GlobusRequest.T24_AAProduct)
            {
                result = ReadOFS_AA(ofsMessage, out errorMessage);
            }
            else if (ofsMessage.SubItems.Count > 0)
            {
                // validate the rest of the response
                try
                {
                    result = ValidateMultipartOFSMessage(ofsMessage, out errorMessage);
                }
                catch
                {
                    // TODO - May be log error

                    // in case of crash in function 'ReadOFS_AA' it means that the response we are trying to parse
                    //  is not well AA formated, thus continue execution as nothing happened
                    errorMessage = null;
                    result = true;
                }

                if (result)
                {
                    result = ReadOFS_Common(ofsMessage.Text, isAuthorize, recordId, out errorMessage, allowDifferentResponseID);
                }
            }
            else
            {
                result = ReadOFS_Common(ofsMessage.Text, isAuthorize, recordId, out errorMessage, allowDifferentResponseID);
            }

            if (!result)
            {
                _HasError = true;
                return errorMessage;
            }

            return GenerateFieldsAndValuesXml();
        }

        public static bool IsOfsResponseSuccessful(string ofsResponse, string id, bool allowDifferentResponseID)
        {
            return ParseOfsResponse(ofsResponse, id, allowDifferentResponseID).IsSuccess;
        }

        #endregion

        #region Protected Static Methods

        protected static EnquiryResponse GetEnquiryRequestResult(LevelStructureHierarchy rule, EnquiryResponse rows)
        {
            List<LevelStructureHierarchy> singleBranchLevels = EnquiryResponseRule.GetSingleBranchLevels(rule);

            EnquiryResponse resultRequest = null;
            foreach (LevelStructureHierarchy level in singleBranchLevels)
            {
                resultRequest = EnquiryResponseRuleResult.GetResult(rows, level);
                if (resultRequest.Rows.Count > 0)
                {
                    break;
                }
            }
            return resultRequest;
        }

        /// <summary>
        /// Counts the columns in history response that has no headers.
        /// </summary>
        /// <param name="msgChars">An array of characters containing the history response.</param>
        /// <param name="readPos">The current reading position.</param>
        /// <returns>An array of strings that contain default names for the columns.</returns>
        protected static List<string> CountColumns(char[] msgChars, int readPos)
        {
            List<string> results = new List<string>();
            bool flagQuotes = false;
            int c = 0;
            for (int i = readPos; i < msgChars.Length; i++)
            {
                if (msgChars[i] == ',' && flagQuotes == false)
                {
                    break;
                }
                if (msgChars[i] == '"')
                {
                    flagQuotes = !flagQuotes;
                    if (!flagQuotes)
                    {
                        results.Add("Field" + c);
                        c++;
                    }
                }
            }
            return results;
        }

        /// <summary>
        /// Gets list of header names, included in the Response message
        /// </summary>
        /// <param name="msgChars"> Response message</param>
        /// <param name="startIndex"> Index in the message to start reading </param>
        /// <returns> List of header names </returns>
        protected static List<string> GetHeaderNames(char[] msgChars, ref int startIndex)
        {
            List<string> headers = new List<string>();
            bool start = true;

            while (startIndex < msgChars.Length)
            {
                char c = msgChars[startIndex];
                if (Char.IsWhiteSpace(c))
                {
                    startIndex++;
                }
                else if (c == ',')
                {
                    break;
                }
                else if (c == '/')
                {
                    startIndex++;
                    string headerName = ReadHeaderName(msgChars, ref startIndex);
                    headers.Add(headerName);
                }
                else if (start && (Char.IsLetter(c) || c == '@'))
                {
                    string headerName = ReadHeaderName(msgChars, ref startIndex);
                    headers.Add(headerName);
                    start = false;
                }
                else if (c == '"')
                {
                    break;
                }
            }
            return headers;
        }

        /// <summary>
        /// Read one header name
        /// </summary>
        /// <param name="msgChars"> Response message</param>
        /// <param name="startIndex"> Index in the message to start reading </param>
        /// <returns>Header name</returns>
        private static string ReadHeaderName(char[] msgChars, ref int startIndex)
        {
            bool colonsFound = false;
            StringBuilder headerName = new StringBuilder();
            bool start = true;

            while (startIndex < msgChars.Length)
            {
                char c = msgChars[startIndex];
                if (c == '/')
                {
                    if (start)
                    {
                        startIndex++;
                        start = false;
                        continue;
                    }
                    else
                    {
                        //we have found the beginning of the next header - stop reading
                        break;
                    }
                }
                if ((c == ',') && !start)
                {
                    //we have found the beginning of the data values - stop reading
                    break;
                }

                start = false;

                if (!colonsFound)
                {
                    if (c != ':')
                    {
                        startIndex++;
                    }
                    else
                    {
                        startIndex += 2;
                        colonsFound = true;
                    }
                }
                else
                {
                    headerName.Append(c);
                    startIndex++;
                }
            }
            return headerName.ToString();
        }

        /// <summary>
        /// Check whether character is valid OFS symbol
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        protected static bool IsValidOFSAttributeNameCharacter(char c)
        {
            return (
                    Char.IsLetterOrDigit(c) ||
                    c == '.' ||
                    c == '-'
                );
        }

        /// <summary>
        /// Gets the next value from the message
        /// </summary>
        /// <param name="msgChars"> Enquery response message </param>
        /// <param name="startIndex"> Index in the message to start reading</param>
        /// <returns> Field value </returns>
        protected static string GetFieldValue(char[] msgChars, ref int startIndex)
        {
            StringBuilder sb = new StringBuilder();

            bool start = true;

            while (startIndex < msgChars.Length)
            {
                //skip comma
                if (msgChars[startIndex] == ',')
                {
                    if (start)
                    {
                        //get past the comma
                        startIndex++;
                    }
                    else
                    {
                        //the comma is part of the value
                        sb.Append(msgChars[startIndex]);
                        startIndex++;
                    }
                }
                else if (msgChars[startIndex] != '"')
                {
                    sb.Append(msgChars[startIndex]);
                    startIndex++;
                }
                else
                {
                    //get past the end quotation or past the first one
                    if (start)
                    {
                        startIndex++;
                        start = false;
                    }
                    else
                    {
                        startIndex++;
                        break;
                    }
                }
            }
            return OFSMessageConverter.ExportMsgSubstChars(sb.ToString());
        }

        /// <summary>
        /// Skips the empty characters from the response message
        /// </summary>
        /// <param name="msgChars"> Response message </param>
        /// <param name="startIndex"> Index in the message to start processing </param>
        protected static void SkipEmpty(char[] msgChars, ref int startIndex)
        {
            while (startIndex < msgChars.Length)
            {
                if (Char.IsWhiteSpace(msgChars[startIndex]))
                {
                    startIndex++;
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Parses the creation date.
        /// </summary>
        /// <param name="creationDate">The creation date.</param>
        /// <returns></returns>
        public static DateTime ParseCreationDate(string creationDate)
        {
            try
            {
                int centuryYear;
                int month;
                int day;
                int hour;
                int minute;

                if (!string.IsNullOrEmpty(creationDate) && creationDate.Length >= 10 &&
                     int.TryParse(creationDate.Substring(0, 2), out centuryYear) &&
                     int.TryParse(creationDate.Substring(2, 2), out month) &&
                     int.TryParse(creationDate.Substring(4, 2), out day) &&
                     int.TryParse(creationDate.Substring(6, 2), out hour) &&
                     int.TryParse(creationDate.Substring(8, 2), out minute))
                 {
                     int year = centuryYear > 50 ? 1900 + centuryYear : 2000 + centuryYear;

                     return new DateTime(year, month, day, hour, minute, 0, 0);
                 }
                 else
                 {
                     return DateTime.MinValue;
                 }
            }
            catch
            {
                return DateTime.MinValue;
                //TODO better error handling
            }
        }

        /// <summary>
        /// Build T24 creation date as string
        /// </summary>
        /// <param name="date">The creation date.</param>
        /// <returns></returns>
        public static string GetT24CreationDateString(DateTime date)
        {
            if (date == DateTime.MinValue || date == DateTime.MaxValue)
                return String.Empty;

            if (date.Year < 1900)
                return String.Empty;

            int centuryYear = date.Year >= 2000 ? date.Year - 2000 : date.Year - 1900;

            return String.Format("{0:00}{1:00}{2:00}{3:00}{4:00}", centuryYear, date.Month, date.Day, date.Hour, date.Minute);
        }

        /// <summary>
        /// Closes a value in a tag with a specified name.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <param name="tag">The tag name.</param>
        /// <param name="attributes"></param>
        /// <returns>The resulting tag.</returns>
        protected static string EncloseInXmlTag(string val, string tag, string attributes)
        {
            string closingTagWithValue = (val.Length > 0) ?
                                        string.Format(">{0}</{1}", val, XmlConvert.EncodeName(tag)) :
                                        "/";


            return string.Format("<{0}{1}{2}>\r\n",
                                 XmlConvert.EncodeName(tag),
                                 attributes,
                                 closingTagWithValue
                                 );
        }

        /// <summary>
        /// Extracts actual OFS response message from full one
        /// Note that in some rare cases the ofs result contains more than one line
        /// In normal case the value of the result is same as 'messageText' argument value
        /// </summary>
        /// <param name="messageText"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private static string ExtractActualResponseMessage(string messageText, out string errorMessage)
        {
            errorMessage = null;

            if (!messageText.Contains("/"))
            {
                //if there is no slash in the response,
                //  we received an error message like "APPLICATION MISSING"
                if (messageText != null)
                {
                    string tempStr = string.Empty;
                    tempStr += (char)0x02;
                    messageText = messageText.Replace(tempStr, "\r\n");
                }

                errorMessage = Utils.GetHtmlEncodedValue(messageText);
                return null;
            }

            if (messageText.IndexOf((char)0x02) > 0)
            {
                string[] arrMsg = messageText.Split(new char[] { (char)0x02 });
                string bestPart = null;
                foreach (string ts in arrMsg)
                {
                    if (!ts.Contains("/"))
                    {
                        continue;
                    }

                    if (bestPart == null)
                    {
                        bestPart = ts;
                        continue;
                    }

                    if (ts.Contains("//"))
                    {
                        bestPart = ts;
                        break;
                    }
                }

                System.Diagnostics.Debug.Assert(bestPart != null);
                if (bestPart != null)
                {
                    return bestPart;
                }

                return messageText;
            }

            return messageText;
        }

        public static string DeduceTransactionID(string ofsResponse)
        {
            int idxOfResultCode = ofsResponse.IndexOf("/1,");
            if (idxOfResultCode < 0)
            {
                idxOfResultCode = ofsResponse.IndexOf("/-1,");
            }

            if(idxOfResultCode > 0)
            {
                // find previous slash which should be the end delimiter of the @ID
                string temp = ofsResponse.Substring(0, idxOfResultCode);
                int idxOfAtIDEndDelimiter = temp.LastIndexOf('/');
                if (idxOfAtIDEndDelimiter > 0)
                {
                    return ofsResponse.Substring(0, idxOfAtIDEndDelimiter);
                }
            }

            // old logic which doesn't handle / in @ID
            string[] parts = ofsResponse.Split('/');
            return parts.Length >= 2 ? parts[0] : null;
        }

        private static OfsReponse ParseOfsResponse(string ofsResponseText, string id, bool allowDifferentResponseID)
        {
            OfsReponse response = new OfsReponse { FullText = ofsResponseText };

            if (allowDifferentResponseID || string.IsNullOrEmpty(id))
            {
                id = DeduceTransactionID(ofsResponseText);
                if (id == null)
                {
                    return response;
                }
            }

            response.TransactionID = id;

            if (!allowDifferentResponseID && !ofsResponseText.StartsWith(id + "/"))
            {
                return response;
            }

            // leave only the {MessageID}/1,{Text}
            string ofsResponseWithoutTransactionID = ofsResponseText.Substring(id.Length + 1);

            // make sure there are commas (they come after the success indicator)
            if (ofsResponseWithoutTransactionID.Split(',').Length <= 1)
            {
                return response;
            }

            int slashIndex = ofsResponseWithoutTransactionID.IndexOf('/');
            if (slashIndex < 0)
            {
                return response;
            }

            int commaIndex = ofsResponseWithoutTransactionID.IndexOf(',', slashIndex);
            if (commaIndex < 0)
            {
                return response;
            }

            response.MessageID = ofsResponseWithoutTransactionID.Substring(0, slashIndex);
            response.StatusIndicator = ofsResponseWithoutTransactionID.Substring(slashIndex + 1, commaIndex - slashIndex - 1);
            response.FieldsText = ofsResponseWithoutTransactionID.Substring(commaIndex + 1);

            return response;
        }

        #endregion

        #region Protected Methods

        protected bool CheckWhetherSpecialTypicalAttributeFollows(int startIndex, string msg)
        {
            if (_SpecialTypicalAttributes != null && _ResponseTypical != null)
            {
                if (_SpecialTypicalAttributes.ContainsKey(_ResponseTypical.Name))
                {
                    List<string> attribNameList = _SpecialTypicalAttributes[_ResponseTypical.Name];

                    foreach (string attribName in attribNameList)
                    {
                        if (msg.Substring(startIndex).StartsWith(attribName))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks whether passed string starts with a field name
        /// <field-name>:<number>:<number>=
        /// </summary>
        /// <param name="msg"> String to be checked</param>
        /// After that character we will expect to see a field name</param>
        /// <returns> true - if start with field name, false - otherwise </returns>
        protected bool CheckWhetherFieldNameFollows(string msg)
        {
            int charEnumerator = 0;

            // Deletes white spaces
            while (msg.Length > charEnumerator && Char.IsWhiteSpace(msg[charEnumerator]))
            {
                charEnumerator++;
            }

            // Checks whether starts with a letter
            if (msg.Length > charEnumerator && !Char.IsLetter(msg[charEnumerator])
                && !CheckWhetherSpecialTypicalAttributeFollows(charEnumerator, msg))
            {
                return false;
            }
            else
            {
                charEnumerator++;
            }

            // Reads the name - letters or digits or '.'
            while (msg.Length > charEnumerator && IsValidOFSAttributeNameCharacter(msg[charEnumerator]))
            {
                charEnumerator++;
            }

            // Checks first colon
            if (msg.Length > charEnumerator && msg[charEnumerator] == ':')
            {
                charEnumerator++;
            }
            else
            {
                return false;
            }

            // Reads first number6
            if (msg.Length > charEnumerator && Char.IsDigit(msg[charEnumerator]))
            {
                while (msg.Length > charEnumerator && Char.IsDigit(msg[charEnumerator]))
                {
                    charEnumerator++;
                }
            }
            else
            {
                return false;
            }

            // Checks second colon
            if (msg.Length > charEnumerator && msg[charEnumerator] == ':')
            {
                charEnumerator++;
            }
            else
            {
                return false;
            }

            // Reads second number
            if (msg.Length > charEnumerator && Char.IsDigit(msg[charEnumerator]))
            {
                while (Char.IsDigit(msg[charEnumerator]))
                {
                    charEnumerator++;
                }
            }
            else
            {
                return false;
            }

            // If the sign 'equal' follows - this is a field name
            if (msg.Length > charEnumerator && msg[charEnumerator] == '=')
            {
                return true;
            }

            // otherwise - not a field name            
            return false;
        }

        /// <summary>
        /// Reads next pair 'field-name, value' from the input message
        /// </summary>
        /// <param name="msg"></param>
        /// <returns> The new position to start reading </returns>
        protected int ReadNextLine(string msg)
        {
            int i = 0;

            if (msg.Length == 0)
                return -1; // The message is over

            while ((i = msg.IndexOf(",", i + 1)) >= 0)
            {
                if (CheckWhetherFieldNameFollows(msg.Substring(i + 1)))
                {
                    return i;
                }
            }
            // We are on the last item
            return msg.Length;
        }

        /// <summary>
        /// Generate result VXML string
        /// </summary>
        /// <returns></returns>
        protected virtual string GenerateFieldsAndValuesXml()
        {
            StringBuilder fieldsAndValuesXml = new StringBuilder();

            foreach (string attributeName in _FieldsAndValues.AllKeys)
            {
                string attributeValue = _FieldsAndValues[attributeName];
                fieldsAndValuesXml.Append(EncloseInXmlTag(attributeValue, attributeName, ""));
            }

            fieldsAndValuesXml.Append(EncloseInXmlTag(Utils.GetHtmlEncodedValue(_TransactionID), "@ID", ""));

            if (!string.IsNullOrEmpty(_ValidataID))
            {
                fieldsAndValuesXml.Insert(0, EncloseInXmlTag(Utils.GetHtmlEncodedValue(_ValidataID), "ID", ""));
            }

            string attributes = string.Format(" Catalog=\"{0}\" CreateDate=\"{1}\" VersionNo=\"{2}\"",
                                              _ResponseTypical.CatalogName,
                                              _CreationDate,
                                              _VersionNumber);

            return EncloseInXmlTag(fieldsAndValuesXml.ToString(), _ResponseTypical.Name, attributes);
        }

        /// <summary>
        /// Finds the right attribute in Validata typical for this Globus attribute
        /// </summary>
        /// <param name="typical">The Validata typical</param>
        /// <param name="shortFieldName">The name of the field</param>
        /// <param name="mvIndex">The multiple value index</param>
        /// <param name="svIndex">The subvalue index</param>
        /// <returns>The correct Validata typical attribute or null.</returns>
        private TypicalAttribute FindAttributeInTypical(MetadataTypical typical, string shortFieldName, int mvIndex, int svIndex)
        {
            // The check for multiple values is done in following order:
            // First is searched for Name-x~y
            // Second is searched for Name-x - ??? maybe this is not needed. Such attributes should not be in the database!!!
            // Third is searched for Name

            string fullForm = string.Format("{0}-{1}~{2}", shortFieldName, mvIndex, svIndex);
            if (typical.AttributesByName.ContainsKey(fullForm))
            {
                return typical.AttributesByName[fullForm];
            }

            string partialForm = string.Format("{0}-{1}", shortFieldName, mvIndex);
            if (typical.AttributesByName.ContainsKey(partialForm))
            {
                return typical.AttributesByName[partialForm];
            }

            if (typical.AttributesByName.ContainsKey(shortFieldName))
            {
                return typical.AttributesByName[shortFieldName];
            }

            return null;
        }

        /// <summary>
        /// Adds the current attribute value.
        /// </summary>
        /// <param name="fieldNamePrefix">The field name prefix.</param>
        /// <param name="fieldShortName">Short name of the field.</param>
        /// <param name="mvIndex">Index of the mv.</param>
        /// <param name="svIndex">Index of the sv.</param>
        /// <param name="fieldValue">The field value.</param>
        private void AddCurrentAttributeValue(string fieldNamePrefix, string fieldShortName, int mvIndex, int svIndex, string fieldValue)
        {
            // The check for multiple values is done in following order:
            // First is searched for Name-x~y
            // Second is searched for Name-x - ??? maybe this is not needed. Such attributes should not be in the database!!!
            // Third is searched for Name
            string fieldShortNameWithPrefix = string.IsNullOrEmpty(fieldNamePrefix) ?
                                            fieldShortName :
                                            (fieldNamePrefix + fieldShortName);

            TypicalAttribute typicalAttribute = FindAttributeInTypical(_ResponseTypical, fieldShortNameWithPrefix, mvIndex, svIndex);

            // HACK - this catalog name makes logic to behave in same way as  flag AllowAttributesNotInTypical is up
            //if (_ResponseTypical.CatalogName == GlobusMetadataReader.DummyMetadataCatalog)
            //{
            //    AllowAttributesNotInTypical = true;
            //}

            if (typicalAttribute == null && !AllowAttributesNotInTypical)
            {
                //we could not find the attribute
                return;
            }

            string typicalAttributeName = (typicalAttribute != null) ?
                typicalAttribute.Name :
                string.Format("{0}-{1}~{2}", fieldShortName, mvIndex, svIndex);

            //for fx swaps only
            if (_ResponseTypical.Name == GlobusRequest.FX_SWAP)
            if (fieldShortName == GlobusRequest.FX_SECOND_LEG_ID && mvIndex == 1 && svIndex == 1)
            if (_FxAuthorizeFirstLeg)
            {
                _FxSecondLegID = OFSMessageConverter.ExportMsgSubstChars(fieldValue);
            }

            AddAttributeWithValue(typicalAttributeName, fieldValue);
        }

        private void AddAttributeWithValue(string attributeName, string attributeValue)
        {
            //we first process the read value 
            attributeValue = Utils.GetHtmlEncodedValue(OFSMessageConverter.ExportMsgSubstChars(attributeValue));

            TypicalAttribute typicalAttribute;
            if (_ResponseTypical.AttributesByName.TryGetValue(attributeName, out typicalAttribute))
            {
                attributeValue = ValidataConverter.GetStringValueForDB(attributeValue, typicalAttribute.Type);
            }
            else
            {
                if (!AllowAttributesNotInTypical)
                {
                    System.Diagnostics.Debug.Fail(string.Format("Can't find attribute '{0}' in '{1}'", attributeName, _ResponseTypical.Name));
                }
            }

            if (CustomAttributeAppender != null)
            {
                CustomAttributeAppender(_FieldsAndValues, attributeName, attributeValue);
            }
            else
            {
                // the default logic
                if (_FieldsAndValues[attributeName] == null)
                {
                    _FieldsAndValues.Add(attributeName, attributeValue);
                }
            }
        }

        /// <summary>
        /// Parses attributes from ofs message part containing fields and values
        /// </summary>
        /// <param name="msgAttributes">OFS message with fields and values</param>
        /// <param name="prefix">Prefix for the generated validata attribute</param>
        /// <param name="createDate">(ref) Creation date field value ("DATE.TIME")</param>
        /// <param name="versionNumber">(ref) Version number field value ("CURR.NO")</param>
        protected void ReadAttributes(string msgAttributes, string prefix, ref string createDate, ref string versionNumber)
        {
            if (string.IsNullOrEmpty(msgAttributes))
            {
                return;
            }

            int i;
            string text = "";

            while ((i = ReadNextLine(msgAttributes)) >= 0)
            {
                text = msgAttributes.Substring(0, i);

                int j = text.IndexOf(":");
                string currentFieldName = text.Substring(0, j);
                text = text.Substring(j + 1, text.Length - j - 1);

                j = text.IndexOf("=", StringComparison.Ordinal);
                string multi = text.Substring(0, j);

                // Extract the ordinal number for that field
                int fieldNum = 1;
                int subfieldNum = 1;
                try
                {
                    string[] fieldComponents = multi.Split(':');

                    if (fieldComponents.Length >= 0)
                    {
                        if (!int.TryParse(fieldComponents[0], out fieldNum))
                        {
                            fieldNum = 1;
                        }

                        if (fieldComponents.Length >= 1)
                        {
                            if (!int.TryParse(fieldComponents[1], out subfieldNum))
                            {
                                subfieldNum = 1;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // TODO better handling
                    System.Diagnostics.Debug.WriteLine(ex);
                }

                text = text.Substring(j + 1, text.Length - j - 1);

                if (currentFieldName == "DATE.TIME")
                {
                    createDate = text;
                }
                else if (currentFieldName == "CURR.NO")
                {
                    this._CURRNO = text;
                    versionNumber = text;
                }

                // check for multiple values for that field
                AddCurrentAttributeValue(prefix, currentFieldName, fieldNum, subfieldNum, text);

                msgAttributes = msgAttributes.Length >= i + 1 ?
                    msgAttributes.Substring(i + 1, msgAttributes.Length - i - 1) :
                    String.Empty;
            }
        }

        protected EnquiryResponse PostFilterResponseRows(EnquiryResponse response, MetadataTypical responseTypical, List<Filter> postFilters)
        {
            if (postFilters == null || postFilters.Count == 0)
                return response;

            bool matchFound;
            EnquiryResponse result = new EnquiryResponse();
            for (int indexRow = 0; indexRow < response.Rows.Count; indexRow++)
            {
                matchFound = true;

                for (int indexFilter = 0; indexFilter < postFilters.Count; indexFilter++)
                {
                    if (postFilters[indexFilter].TestValue != null)
                    {
                        int responseCellsCount = response.Rows[indexRow].Cells.Count;
                       
                        //Determine whether any cell value is mapped to an enquiry result attribute
                        bool isAnyValueMapped = response.Rows[indexRow].IsAnyValueMapped;
                        int cellIndex = -1;
                        if (isAnyValueMapped)
                        {
                            //Find the post filter attribute name in the list of mapped values
                            cellIndex = response.Rows[indexRow].Cells.FindIndex(c => c.Name == postFilters[indexFilter].AttributeName);
                        }
                        else
                        {

                            cellIndex = PostFilterAttributePosition(responseTypical, postFilters[indexFilter].AttributeName);
                        }
                        

                        if (cellIndex < 0 || cellIndex >= responseCellsCount)
                        {
                            matchFound = false;
                            break;
                        }

                        //WriteMessageToLog("FilterValue = '" + filters[indexFilter].FilterValue + "' CellValue = '" + response.Rows[indexRow].Cells[cellIndex] + "'");

                        if (!postFilters[indexFilter].TestValue.Equals(response.Rows[indexRow].Cells[cellIndex].Value))
                        {
                            matchFound = false;
                            break;
                        }
                    }
                }

                if (matchFound)
                {
                    result.Rows.Add(response.Rows[indexRow]);
                }
            }

            return result;
        }

        public int PostFilterAttributePosition(MetadataTypical responseTypical, string attributeName)
        {
            int position = 0;

            foreach (TypicalAttribute attribute in responseTypical.Attributes)
            {
                if (attribute.Name == ID_ATTRIBUTE_NAME)
                    continue;

                if (attribute.Name == attributeName)
                {
                    return position;
                }

                position++;
            }

            return -1;
        }

        #endregion

        #region Protected and Private Methods - AA reading

        protected abstract OFSResponseReaderBase CreateClonedOFSResponseReader();

        private bool ReadOFS_Common(string messageText, bool isAuthorize, string recordID, out string errorMessage, bool allowDifferentResponseID)
        {
            if (CheckForLiveRecordNotChanged)
            {
                if (messageText.TrimEnd().EndsWith("/-1/NO,LIVE RECORD NOT CHANGED"))
                {
                    // ??? TODO should this be considered as an ERROR ???
                    errorMessage = GlobusRequestBase.LIVE_RECORD_NOT_CHANGED;
                    return false;
                }

                if (messageText.TrimEnd().EndsWith("/-1/NO,NOT AUTH. RECORD NOT CHANGED"))
                {
                    // ??? TODO should this be considered as an ERROR ???
                    errorMessage = GlobusRequestBase.LIVE_RECORD_NOT_CHANGED;
                    return false;
                }

                //"AD.US.PARTICIPATION--19500101//-1/NO,NOT AUTH. RECORD NOT CHANGED"
            }

            string errorStartMark = "//-1/NO,";
            string errorEndMark = "</request>";
            string errorRequestStartMark = "<request>";

            // <request>AA1730677LVV-OFFICERS-20171102.1//-1/NO,PRIMARY.OFFICER:1:1=  INPUT MISSING</request>
            // so from above request generate below error message
            // Property {OFFICERS},  PRIMARY.OFFICER:1:1=  INPUT MISSING; Property {ACCOUNTS}, AMOUNT = INPUT MISSING; ..
            if (messageText.Contains(errorStartMark) && messageText.Contains(errorEndMark) && messageText.Contains(errorRequestStartMark))
            {
                int currentErrorStartPos = messageText.IndexOf(errorStartMark);
                errorMessage = string.Empty;
                do
                {
                    int errorStartPos = currentErrorStartPos + errorStartMark.Length;
                    int errorEndPos = messageText.IndexOf(errorEndMark, errorStartPos);
                    int errorMsgLength = errorEndPos - errorStartPos;
                    string messageBody = messageText.Substring(errorStartPos, errorMsgLength);

                    int requestBeginPos = 0;
                    while(requestBeginPos < currentErrorStartPos)
                    {
                        int tmp = messageText.IndexOf(errorRequestStartMark, requestBeginPos + errorRequestStartMark.Length);
                        if (tmp < currentErrorStartPos) requestBeginPos = tmp;
                        else break;
                    }
                    int startHyphen = messageText.IndexOf('-', requestBeginPos);
                    int endHyphen = messageText.IndexOf('-', startHyphen + 1);
                    if(startHyphen > errorStartPos || endHyphen > errorStartPos)
                    {
                        // that is some error... not sure what to do in that case
                        // but most probably this case will never happen...
                    }
                    string property = messageText.Substring(startHyphen + 1, endHyphen - startHyphen - 1);
                    messageBody = string.Format("Property {{{0}}}, {1}", property, messageBody);

                    if (errorMessage.IsEmptyOrNoValue()) errorMessage = messageBody;
                    else errorMessage += ";" + messageBody;
                    currentErrorStartPos = messageText.IndexOf(errorStartMark, errorEndPos);
                }
                while (currentErrorStartPos != -1);
                return false;
            }

            messageText = ExtractActualResponseMessage(messageText, out errorMessage);
            if (messageText == null)
            {
                System.Diagnostics.Debug.Assert(errorMessage != null);
                _HasError = true;
                return false;
            }

            OfsReponse response = ParseOfsResponse(messageText, recordID, allowDifferentResponseID);
            _TransactionID = response.TransactionID;
            _OfsID = response.MessageID;

            if (!response.IsSuccess)
            {
                errorMessage = Utils.GetHtmlEncodedValue(response.ErrorMessage);
                return false;
            }

            //now we are getting the fields
            _FieldsAndValues = new NameValueCollection();

            _CreationDate = "";

            _VersionNumber = "";

            bool isLdLoan = _ResponseTypical.Name == GlobusRequest.LD_LOAN;

            bool isForex = _ResponseTypical.Name == GlobusRequest.FX_SWAP;

            string msgAttributes = response.FieldsText;

            if (isLdLoan)
            {  
                //read ld loan that can contain a schedule endtry
                ReadLDLoan(msgAttributes, ref _CreationDate, ref _VersionNumber);
            }
            else if (isForex && !_FxIsFlat)
            {   
                //read an fx swap
                ReadFxSwap(msgAttributes, ref _CreationDate, ref _VersionNumber, isAuthorize);
            }
            else
            {
                ReadAttributes(msgAttributes, string.Empty, ref _CreationDate, ref _VersionNumber);
            }

            DateTime dt = ParseCreationDate(_CreationDate);
            _CreationDate = dt.ToString("dd/MM/yyyy HH:mm");

            return true;
        }

        private bool ValidateMultipartOFSMessage(GlobusMessage.OFSMessage ofsMessage, out string errorMessage)
        {
            return GetReadersFromMultipartOFSMessage(ofsMessage, out errorMessage) != null;
        }

        private bool ReadOFS_AA(GlobusMessage.OFSMessage ofsMessage, out string errorMessage)
        {
            System.Diagnostics.Debug.Assert(_ResponseTypical.BaseClass == GlobusRequest.T24_AAProduct);

            var lsReaders = GetReadersFromMultipartOFSMessage(ofsMessage, out errorMessage);
            if (lsReaders == null)
            {
                return false;
            }

            return CombineMultipleOFSResponses(lsReaders, out errorMessage);
        }

        private void ExtractOFSErrorMessages(GlobusMessage.OFSMessage ofsMessage, out string errorMessage)
        {
            errorMessage = null;
            string ofsMessageTextErrorStartMark = "//-1,";
            string ofsMessageSubItemErrorStartMark = "//-1/NO,";

            if (ofsMessage.Text.Contains(ofsMessageTextErrorStartMark) && !ofsMessage.Text.Contains(ofsMessageSubItemErrorStartMark) && ofsMessage.SubItems != null)
            {
                // try to extract error messages from SubItems
                foreach(string subItem in ofsMessage.SubItems)
                {
                    if (!subItem.Contains(ofsMessageSubItemErrorStartMark)) continue;

                    int currentErrorStartPos = subItem.IndexOf(ofsMessageSubItemErrorStartMark);
                    int errorStartPos = currentErrorStartPos + ofsMessageSubItemErrorStartMark.Length;
                    string messageBody = subItem.Substring(errorStartPos);
                    string propertyBody = subItem.Substring(0, currentErrorStartPos);

                    int startHyphen = propertyBody.IndexOf('-');
                    int endHyphen = propertyBody.IndexOf('-', startHyphen + 1);
                    string property;
                    if (startHyphen < 0 || endHyphen < 0)
                    {
                        // this is the case when left part of message don't contain property ie it is like AAACT17310FFF5LB2W
                        property = propertyBody;
                    }
                    else
                    {
                        // this is the case when left part of message contain property ie it is like AA17310PBSP1-CLOSURE-20171106
                        property = propertyBody.Substring(startHyphen + 1, endHyphen - startHyphen - 1);
                    }
                    messageBody = string.Format("Property {{{0}}}, {1}", property, messageBody);

                    if (errorMessage.IsEmptyOrNoValue()) errorMessage = messageBody;
                    else errorMessage += ";" + messageBody;
                }
            }
            else if (ofsMessage.Text.Contains(ofsMessageSubItemErrorStartMark))
            {
                errorMessage = ofsMessage.Text.Substring(ofsMessage.Text.IndexOf(ofsMessageSubItemErrorStartMark) + ofsMessageSubItemErrorStartMark.Length);
            }
        }

        private List<OFSResponseReaderBase> GetReadersFromMultipartOFSMessage(GlobusMessage.OFSMessage ofsMessage, out string errorMessage)
        {
            errorMessage = null;
            ExtractOFSErrorMessages(ofsMessage, out errorMessage);
            if (!string.IsNullOrWhiteSpace(errorMessage)) return null;

            List<string> lsOFSMessages = new List<string>();
            lsOFSMessages.Add(ofsMessage.Text);
            lsOFSMessages.AddRange(ofsMessage.SubItems);

            List<OFSResponseReaderBase> lsReaders = new List<OFSResponseReaderBase>();

            #region DEBUG
#if DEBUG
            SortedDictionary<string, List<string>> tempGrouped =
                new SortedDictionary<string, List<string>>();
#endif
            #endregion

            bool ignoreXmlResponseErrorsIfAllResponsesAreOK;
            ignoreXmlResponseErrorsIfAllResponsesAreOK = false;
            string xmlErrorMessage = null;

            foreach (string msg in lsOFSMessages)
            {
                if (string.IsNullOrEmpty(msg)) continue;

                if (msg.StartsWith("<") && msg.EndsWith(">"))
                {
                    string errMessage = CheckOFSMessage(msg);
                    if (!String.IsNullOrEmpty(errMessage))
                    {
                        errorMessage = errMessage;
                        if (!ignoreXmlResponseErrorsIfAllResponsesAreOK)
                        {
                            return null;
                        }

                        xmlErrorMessage = errorMessage;
                        continue;
                    }
                }

                OFSResponseReaderBase reader = CreateClonedOFSResponseReader();
                // reset base classs of the cloned reader in order to use the parsing logic
                //  for simple records (not AA).
                // Cloning of typical is done because the _ResponseTypical object is shared.
                reader._ResponseTypical = reader._ResponseTypical.Clone(false);
                reader._ResponseTypical.BaseClass = "";


                bool result = reader.ReadOFS_Common(msg, false, null, out errorMessage, false);
                if (!result)
                {
                    int pos = msg.IndexOf('/');
                    if (pos > 0)
                    {
                        errorMessage += string.Format(" (Property: {0})", msg.Substring(0, pos));

                        if (!string.IsNullOrEmpty(xmlErrorMessage))
                        {
                            errorMessage += "; Other Error: " + xmlErrorMessage;
                        }
                    }

                    return null;
                }

                #region DEBUG
#if DEBUG

                if (!tempGrouped.ContainsKey(reader.TransactionID))
                {
                    tempGrouped.Add(reader.TransactionID, new List<string>());
                }
                tempGrouped[reader.TransactionID].Add(msg);
#endif
                #endregion

                lsReaders.Add(reader);
            }

            #region DEBUG
#if DEBUG
            StringBuilder sbTemp = new StringBuilder();
            foreach (string key in tempGrouped.Keys)
            {
                sbTemp.AppendLine("----------------------------------------------------------------------");
                sbTemp.AppendLine("---- " + key);
                sbTemp.AppendLine("----------------------------------------------------------------------");

                int i = 1;
                foreach (string s in tempGrouped[key])
                {
                    sbTemp.AppendLine(i.ToString() + ")");
                    sbTemp.AppendLine(s);
                    sbTemp.AppendLine();
                    i++;
                }


                sbTemp.AppendLine();
            }

#endif
            #endregion

            return lsReaders;
        }

        /// <summary>
        /// Checks OFS message for errors. If OFS response message is in XML format, return msg tag content
        /// </summary>
        /// <param name="ofsMessage"></param>
        /// <returns></returns>
        private string CheckOFSMessage(string ofsMessage)
        {
            try
            {
                int startMsgPos = ofsMessage.IndexOf("<msg>");
                if (startMsgPos > -1)
                {
                    int endMsgPos = ofsMessage.IndexOf("</msg>");
                    if (endMsgPos > -1)
                    {
                        return ofsMessage.Substring(startMsgPos + 5, endMsgPos - startMsgPos - 5);
                    }
                }
            }
            catch{                
            }

            return String.Empty;
        }

        private bool CombineMultipleOFSResponses(List<OFSResponseReaderBase> lsReaders, out string errorMessage)
        {
            errorMessage = null;

            this._FieldsAndValues = new NameValueCollection();

            List<string> multipleProperties = GetMultipleAAProperties();

            Dictionary<string, List<string>> multiplePropertyCurrentMVIndex =
                new Dictionary<string, List<string>>();

            for (int i = 0; i < lsReaders.Count; i++)
            {
                OFSResponseReaderBase reader = lsReaders[i];

                if (string.IsNullOrEmpty(reader.TransactionID))
                {
                    System.Diagnostics.Debug.Fail("WHY ???");
                    continue;
                }

                if (i == 0)
                {
                    this._TransactionID = reader._TransactionID;
                    this._CreationDate = reader._CreationDate;
                    this._VersionNumber = reader._VersionNumber;
                    this._CURRNO = reader._CURRNO;
                }

                string[] idItems = reader.TransactionID.Split(new char[] { '-' });

                if (idItems.Length < 2 && i != 0)
                {
                    continue;
                }

                string propertyName = (i == 0) ? null : idItems[1].Trim();

                int multiplePropertyMVIndex = -1;
                if (propertyName != null && multipleProperties.Contains(propertyName))
                {
                    if (!multiplePropertyCurrentMVIndex.ContainsKey(propertyName))
                    {
                        multiplePropertyCurrentMVIndex.Add(propertyName, new List<string>());
                    }

                    if (!multiplePropertyCurrentMVIndex[propertyName].Contains(reader.TransactionID))
                    {
                        multiplePropertyCurrentMVIndex[propertyName].Add(reader.TransactionID);
                    }

                    multiplePropertyMVIndex = multiplePropertyCurrentMVIndex[propertyName].IndexOf(reader.TransactionID) + 1;
                }

                // Add property.@ID
                if (!string.IsNullOrEmpty(propertyName) && !string.IsNullOrEmpty(reader.TransactionID))
                {
                    string propertyFieldName = GetPropertyFieldName(propertyName, "@ID", multiplePropertyMVIndex);
                    this._FieldsAndValues[propertyFieldName] = reader.TransactionID;
                }

                // Add Rest of the property.fields
                foreach (string fieldName in reader._FieldsAndValues.AllKeys)
                {
                    string fieldValue = reader._FieldsAndValues[fieldName];
                    string propertyFieldName = GetPropertyFieldName(propertyName, fieldName, multiplePropertyMVIndex);

                    this._FieldsAndValues[propertyFieldName] = fieldValue;
                }
            }

            return true;
        }

        private string GetPropertyFieldName(string propertyName, string fieldName, int multiplePropertyMVIndex)
        {
            string result;
            if (propertyName != null && multiplePropertyMVIndex > 0)
            {
                result = AAAttributeName.GetAttributeName(propertyName, multiplePropertyMVIndex, 1, fieldName);
            }
            else
            {
                result = AAAttributeName.GetAttributeName(propertyName, fieldName);
            }

            // Fix MV/SV attributes in order to match typical's attributes 
            if (_ResponseTypical.AttributesByName.ContainsKey(result))
            {
                return result;
            }

            string shortName;
            int mvIndex, svIndex;
            bool isMV = AttributeNameParser.ParseComplexFieldName(result, out shortName, out mvIndex, out svIndex);
            if (isMV)
            {
                if (_ResponseTypical.AttributesByName.ContainsKey(shortName))
                    if (mvIndex == 1 && svIndex == 1)
                    {
                        return shortName;
                    }
            }
            else // SV
            {
                if (_ResponseTypical.AttributesByName.ContainsKey(result + "-1~1"))
                {
                    result += "-1~1";
                }
            }

            //System.Diagnostics.Debug.Fail("Attribute is not presented in the typical");
            return result;
        }

        private List<string> GetMultipleAAProperties()
        {
            if (_ResponseTypical == null)
            {
                System.Diagnostics.Debug.Fail("Where it is?");
                return new List<string>();
            }

            List<string> result = new List<string>();

            foreach (TypicalAttribute ta in _ResponseTypical.Attributes)
            {
                string propertyName;
                string propertyFieldName;
                AAAttributeName.SplitAttributeName(ta.Name, out propertyName, out propertyFieldName);
                if (string.IsNullOrEmpty(propertyName))
                {
                    continue;
                }

                string shortName;
                int mvIndex;
                int svIndex;
                if (AttributeNameParser.ParseComplexFieldName(propertyName, out shortName, out mvIndex, out svIndex))
                if (!result.Contains(shortName))
                {
                    result.Add(shortName);
                }
            }

            return result;
        }

        #region Private methods

        private void ReadLDLoan(string attribList, ref string createDate, ref string versionNumber)
        {
            int delimPos = attribList.IndexOf(GlobusRequest.LD_DELIM);

            if (delimPos != -1)
            {
                //there is a schedule for this loan
                string normalAttributes = attribList.Substring(0, delimPos);
                string scheduleAttributes = attribList.Substring(delimPos + GlobusRequest.LD_DELIM.Length);

                ReadAttributes(normalAttributes, string.Empty, ref createDate, ref versionNumber);
                ReadAttributes(scheduleAttributes, GlobusRequest.LD_SCHEDULE_PREFIX, ref createDate, ref versionNumber);
            }
            else
            {
                ReadAttributes(attribList, string.Empty, ref createDate, ref versionNumber);
            }
        }

        private void ReadFxSwap(string attribList, ref string createDate, ref string versionNumber, bool isAuthorize)
        {
            if (isAuthorize)
            {
                //read leg 2 attributes
                ReadAttributes(attribList, GlobusRequest.FX_SECOND_LEG, ref createDate, ref versionNumber);
            }
            else
            {
                //read leg 1 attributes
                ReadAttributes(attribList, GlobusRequest.FX_FIRST_LEG, ref createDate, ref versionNumber);
            }
        }

        #endregion  

        #endregion
    }
}
