﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods
{
    public class T24MVAttributeStructure
    {
        public string Name;
        public SortedList<int, MultiValue> MultiValuesList = new SortedList<int, MultiValue>();

        private readonly string _MultiValueSeparator;
        private readonly string _SubValueSeparator;

        public static bool ProcessErrors { get; set; }

        public T24MVAttributeStructure(string multiValueSeparator, string subValueSeparator)
        {
            _MultiValueSeparator = multiValueSeparator;
            _SubValueSeparator = subValueSeparator;
        }
        

        public void AddValue(string mvIndexStr, string svIndexStr, string value)
        {
            int mvIndex = int.Parse(mvIndexStr);

            MultiValue mv;
            if(!MultiValuesList.TryGetValue(mvIndex, out mv))
            {
                mv = new MultiValue
                         {
                             Index = mvIndex
                         };
                MultiValuesList.Add(mvIndex, mv);
            }

            if(string.IsNullOrEmpty(svIndexStr))
            {
                mv.Value = value;
            }
            else
            {
                int svIndex = int.Parse(svIndexStr);

                try
                {
                    mv.AddSubValue(svIndex, value);
                }
                catch (ArgumentException ex)
                {
                    //throw new Exception(string.Format("A subValue with index :{0} already exists for the attribute {1}.", svIndex, Name), ex);
                }
            }
        }

        public override string ToString()
        {
            var res = new StringBuilder();
            res.Append(Name);
            res.Append(":1:1=");

            res.Append(GetValue());

            return res.ToString();
        }

        public string GetValue()
        {
            StringBuilder res = new StringBuilder();
            int multiValueIndexCounter = 1;
            foreach(var mvPair in MultiValuesList)
            {
                int multiValueIndex = mvPair.Key;
                MultiValue mv = mvPair.Value; 

                if(multiValueIndex != multiValueIndexCounter)
                {
                    for (int j = multiValueIndexCounter; j < multiValueIndex; j++ )
                    {
                        res.Append(_MultiValueSeparator);
                    }
                }

                multiValueIndexCounter = multiValueIndex;

                if (mv.SubValues.Count == 0)
                {
                    res.Append(mv.Value);
                }
                else
                {
                    int subValueIndexCounter = 1;
                    foreach(var svPair in mv.SubValues)
                    {
                        int subValueIndex = svPair.Key;
                        SubValue sv = svPair.Value;

                        if (subValueIndex != subValueIndexCounter)
                        {
                            for (int j = subValueIndexCounter; j < subValueIndex; j++)
                            {
                                res.Append(_SubValueSeparator);
                            }
                        }

                        res.Append(sv.Value);

                        subValueIndexCounter = subValueIndex;
                    }
                }
            }
            return res.ToString();
        }
    }

    public class MultiValue : AttributeValue 
    {
        public SortedList<int, SubValue> SubValues = new SortedList<int, SubValue>();

        public void AddSubValue(int svIndex, string value)
        {
            if (T24MVAttributeStructure.ProcessErrors)
            {
                if (SubValues.ContainsKey(svIndex))
                    SubValues[svIndex].Value += ", " + value;
                else
                    SubValues.Add(svIndex, new SubValue
                                               {
                                                   Index = svIndex,
                                                   Value = value
                                               });

            }
            else
            {
                SubValues.Add(svIndex, new SubValue
                                           {
                                               Index = svIndex,
                                               Value = value
                                           });

            }
        }
    }

    public class SubValue: AttributeValue
    {
    }

    public class AttributeValue
    {
        public int Index;
        public string Value;
    }
}
