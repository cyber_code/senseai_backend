using System.Collections.Generic;

namespace OFSCommonMethods
{
    public class ValidataOFSMessageList : List<ValidataOFSMessage>
    {
        public ValidataOFSMessageList(IEnumerable<ValidataOFSMessage> collection)
            : base(collection)
        {
        }

        public ValidataOFSMessageList(int capacity) : base(capacity)
        {
        }

        public ValidataOFSMessageList()
        {
        }
    }
}