﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AXMLEngine;
using ValidataCommon;

namespace OFSCommonMethods
{
    public class MultiValueShrinkStretch
    {
        #region Private Members

        private readonly DifferenceErrorConfiguration _DifferenceErrorConfiguration = DifferenceErrorConfiguration.DeserializeFromXml();

        private Dictionary<string, MultiValueSplitElement> _MutltiValuesProcessRules;
        private Dictionary<string, bool> _MultiValueNameWithIndexes;

//        private bool _ProcessAA;

        #endregion

        public MultiValueShrinkStretch(MetadataTypical requestTypical)
        {
            RequesTypical = requestTypical;
//            _ProcessAA = requestTypical.BaseClass == OFSCommonMethods.GlobusRequestBase.T24_AAProduct;
            Get(requestTypical);
        }

        public bool HasMVToParse
        {
            get { return _MutltiValuesProcessRules.Count > 0; }
        }

        public MetadataTypical RequesTypical { get; set; }

        public void Get(MetadataTypical requestTypical)
        {
            _MutltiValuesProcessRules = new Dictionary<string, MultiValueSplitElement>();
            _MultiValueNameWithIndexes = new Dictionary<string, bool>();

            //Get information for the fields that required custom MultiValue action
            foreach (var attr in requestTypical.Attributes)
            {
                var rule = new RuleElement(requestTypical.CatalogName, requestTypical.Name, attr.Name);

                foreach (MultiValueSplitElement ruleElement in _DifferenceErrorConfiguration.MultiValueRuleList)
                {
                    if (ruleElement.CheckRuleElement(rule))
                    {
                        string name;
                        //if (_ProcessAA)
                        //{
                        //    AttributeNameParsingResult res = AAAttributeName.ParseComplexFieldName(attr.Name);
                        //    name = res.ShortFieldName;
                        //   _MultiValueNameWithIndexes[name] = res.IsComplex;
                        //}
                        //else
                        //{
                            string shortName;
                            int mv, sv;
                            if (AttributeNameParser.ParseComplexFieldName(attr.Name, out shortName, out mv, out sv))
                            {
                                name = shortName;
                                _MultiValueNameWithIndexes[name] = true;
                            }
                            else
                            {
                                name = attr.Name;
                                _MultiValueNameWithIndexes[name] = false;
                            }
                        //}
                        _MutltiValuesProcessRules[name] = ruleElement;
                        break;
                    }
                }
            }
        }


        public string GetOfsMessageWithShrinkedMultivalues(String ofsMessage)
        {
            Instance instance = Instance.FromXmlString(ofsMessage);
            instance = MergeMultiValues(instance);
            return instance.ToXmlString(true);
        }

        private static void AddAttributeToInstance(MetadataTypical requesTypical, string attributeName, Instance instance, StringBuilder value)
        {
            if (!requesTypical.AttributesByName.ContainsKey(attributeName))
            {
                attributeName = String.Format("{0}-1~1", attributeName);
                if (!requesTypical.AttributesByName.ContainsKey(attributeName))
                {
                    return;
                }
            }
            instance.AddAttribute(attributeName, value.ToString());
        }

        internal Instance MergeMultiValues(Instance instance)
        {
            var concatenatedFields = new SortedDictionary<string, string>(new AttributeNameComparer());

            foreach (var attribute in instance.Attributes)
            {
                if (attribute.Name.StartsWith("PROPERTY") || attribute.Name.StartsWith("FIELD.NAME") || attribute.Name.StartsWith("FIELD.VALUE")) continue;

                MultiValueSplitElement rule;

                if (_MutltiValuesProcessRules.TryGetValue(GetShortName(attribute.Name), out rule))
                {
                    if(!concatenatedFields.ContainsKey(attribute.Name))
                        concatenatedFields.Add(attribute.Name, attribute.Value);
                }
            }

            if (concatenatedFields.Count > 0)
            {
                AttributeNameParsingResult prevParsedField = null;
                var result = new StringBuilder();

                int index = 0;
                foreach (var field in concatenatedFields)
                {
                    var parsedField = AttributeNameParser.ParseComplexFieldName(field.Key);
                    if (prevParsedField == null) prevParsedField = parsedField;

                    if (parsedField.ShortFieldName == prevParsedField.ShortFieldName)
                    {
                        if (index > 0)
                        {
                            MultiValueSplitElement rule;
                            if (_MutltiValuesProcessRules.TryGetValue(parsedField.ShortFieldName, out rule)) ;
                            {
                                result.Append(parsedField.MultiValueIndex != prevParsedField.MultiValueIndex
                                    ? rule.MultiValueSeparator
                                    : rule.SubValueSeparator);
                            }
                        }
                        index++;
                    }
                    else
                    {
                        AddAttributeToInstance(RequesTypical, prevParsedField.ShortFieldName, instance, result);

                        //Initialize the new field
                        result = new StringBuilder();
                        index = 1;
                    }

                    result.Append(field.Value);
                    instance.RemoveAttribute(field.Key);

                    prevParsedField = parsedField;
                }

                if (prevParsedField != null && result.Length > 0)
                {
                    AddAttributeToInstance(RequesTypical, prevParsedField.ShortFieldName, instance, result);
                }
            }
            return instance;
        }

        public Instance SplitMultiValues(Instance inst)
        {
            MultiValueSplitElement rule;
            Instance instance = new Instance() { CatalogName = inst.CatalogName, TypicalName = inst.TypicalName };
            foreach (var attribute in inst.Attributes)
            {
                if (attribute.Name == "@ID")
                {
                    instance.AddAttribute(attribute.Name, attribute.Value);
                    continue;
                }

                string shortName = GetShortName(attribute.Name);

                if (!_MutltiValuesProcessRules.TryGetValue(shortName, out rule) || attribute.Value == null)
                {
                    instance.AddAttribute(attribute.Name, attribute.Value);
                }
                else
                {
                    int mvI = 0;
                    int svI = 0;
                    AttributeNameParser.ParseComplexFieldName(attribute.Name, out shortName, out mvI, out svI);
                    if (mvI == 0) mvI = 1;
                    if (svI == 0) svI = 1;
                    string[] mv = ValidataCommon.StringMethods.StringSplit(attribute.Value,
                                                                                 rule.MultiValueSeparator);
                    foreach (string mvPart in mv)
                    {
                        string[] sv = ValidataCommon.StringMethods.RegexSplit(mvPart, rule.SubValueSeparator);
                        foreach (string svPart in sv)
                        {
                            instance.AddAttribute(string.Format("{0}-{1}~{2}", shortName, mvI, svI), svPart);
                            svI++;
                        }
                        mvI++;
                        svI = 1;
                    }
                }
            }
            return instance;
        }

        private string GetShortName(string name)
        {
            //if (name.StartsWith("{"))
            //{
            //    AttributeNameParsingResult res = AAAttributeName.ParseComplexFieldName(name);
            //    return res.ShortFieldName;
            //}

            int mvI = 0;
            int svI = 0;
            string shortName = string.Empty;
            AttributeNameParser.ParseComplexFieldName(name, out shortName, out mvI, out svI);
            return shortName;
        }

        #region Shrink
        /// <summary>
        /// This function process the OFS response from T24 and joins multivalues in single value
        /// </summary>
        /// <param name="ofsMessage"></param>
        /// <returns></returns>
        //public string GetOfsMessageWithShrinkedMultivalues(string ofsMessage)
        //{
        //    var reader = new OFSResponseReader();
        //    string resultOfs = reader.ReadOFS(ofsMessage, typical, false, null);

            //if (_ProcessAa)
            //{
            //    if (ofsMessage.Contains("<request>"))
            //    {
            //        // process multipart messages
            //        System.Text.StringBuilder ofsFull = new StringBuilder();
            //        string[] multiparts = ofsMessage.Split(new[] { "<request>" }, StringSplitOptions.None);
            //        int part = 0;
            //        foreach (string multipart in multiparts)
            //        {
            //            part++;
            //            ofsFull.Append(GetOfsMessageWithShrinkedMultivalues_AA(multipart));
            //            if (part <  multiparts.Length)
            //                ofsFull.Append("<request>");
            //        }
            //        return ofsFull.ToString();
            //    }
            //    else
            //        return GetOfsMessageWithShrinkedMultivalues_AA(ofsMessage);
            //}
            //else
            //    return GetOfsMessageWithShrinkedMultivalues_NormalApplication(ofsMessage);
        //}




        //private bool SimpleCheckValidField(string value)
        //{
        //    if (value.Contains("="))
        //    {
        //        int index = value.IndexOf(":");
        //        if (index != -1)
        //        {
        //            if (Char.IsDigit(value[index + 1]))
        //            {
        //                index = value.IndexOf(":", index+1);
        //                if (index != -1)
        //                {
        //                    if (Char.IsDigit(value[index + 1]))
        //                    {
        //                        return true;
        //                    }
        //                }    
        //            }
        //        }
                
        //    }
        //    return false;
        //}


        //private IEnumerable<string> GetOFSPart(string ofsMessage)
        //{
        //    int curPos = 0;
        //    int newPos = 1;
        //    if (ofsMessage.Length == 0)
        //        yield return null; // The OFS is over

        //    while ((newPos = ofsMessage.IndexOf(",", newPos + 1)) >= 0)
        //    {
        //        int nextFieldPos = ofsMessage.IndexOf(",", newPos + 1);
        //        if (nextFieldPos != -1)
        //        {
        //            string value = ofsMessage.Substring(newPos+1, nextFieldPos - newPos + 1);
        //            if (SimpleCheckValidField(value))
        //            {
        //                value = ofsMessage.Substring(curPos, newPos - curPos);
        //                curPos = newPos + 1;
        //                yield return value;
        //            }
        //            else
        //                newPos = nextFieldPos; 

        //        }
        //        else
        //        {
        //            string value = ofsMessage.Substring(curPos, newPos - curPos);
        //            curPos = newPos + 1;
        //            yield return value;
        //        }
        //    }
        //    // We are on the last item
        //    yield return ofsMessage.Substring(curPos);
        //}

        /// <summary>
        /// This function process the OFS response from T24 and joins multivalues in single value, Normal Application
        /// </summary>
        /// <param name="ofsMessage"></param>
        /// <returns></returns>
        //private string GetOfsMessageWithShrinkedMultivalues_NormalApplication(string ofsMessage)
        //{
        //    bool initialPart = true;
        //    //string[] parts = ofsMessage.Split(new[] { ',' }, StringSplitOptions.None);
        //    var finalList = new List<string>();
        //    var dict = new Dictionary<string, T24MVAttributeStructure>();
        //    MultiValueSplitElement rule = null;

        //    foreach (var part in GetOFSPart(ofsMessage))
        //    {
        //        if (initialPart)
        //        {
        //            initialPart = false;
        //            T24MVAttributeStructure.ProcessErrors = part.EndsWith("-1");
        //        }
        //        string[] splitByFieldAndValue = part.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries);

        //        //App name, user, pass, etc
        //        if (splitByFieldAndValue.Length < 2)
        //        {
        //            finalList.Add(part);
        //            continue;
        //        }

        //        string[] pieces = splitByFieldAndValue[0].Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

        //        //Single value
        //        if (pieces.Length == 1)
        //        {
        //            finalList.Add(part);
        //            continue;
        //        }

        //        //only multi value
        //        if (pieces.Length == 2)
        //        {
        //            string attrName = pieces[0];

        //            if (!_MutltiValuesProcessRules.TryGetValue(attrName, out rule))
        //            {
        //                finalList.Add(part);
        //                continue;
        //            }

        //            string mvIndex = pieces[1];
        //            string attrValue = splitByFieldAndValue[1];

        //            T24MVAttributeStructure t24AttributeStructure;
        //            if (!dict.TryGetValue(attrName, out t24AttributeStructure))
        //            {
        //                t24AttributeStructure = new T24MVAttributeStructure(rule.MultiValueSeparator, rule.SubValueSeparator) { Name = attrName };
        //                dict[attrName] = t24AttributeStructure;
        //            }

        //            t24AttributeStructure.AddValue(mvIndex, null, attrValue);
        //        }
        //            //multi value and sub value
        //        else if (pieces.Length == 3)
        //        {
        //            string attrName = pieces[0];

        //            if (!_MutltiValuesProcessRules.TryGetValue(attrName, out rule))
        //            {
        //                finalList.Add(part);
        //                continue;
        //            }

        //            string mvIndex = pieces[1];
        //            string svIndex = pieces[2];
        //            string attrValue = splitByFieldAndValue[1];
        //            AddMultiValue(attrName, attrValue, dict, rule, mvIndex, svIndex);
        //        }
        //        else
        //        {
        //            Debug.Assert(false, "Unexpected OFS Attribute structure.");
        //        }
        //    }

        //    finalList.AddRange(dict.Values.Select(attr => attr.ToString()));

        //    return string.Join(",", finalList);
        //}

        //private void AddMultiValue(string attrName, string attrValue, Dictionary<string, T24MVAttributeStructure> dict, MultiValueSplitElement rule, string mvIndex, string svIndex)
        //{
            

        //    T24MVAttributeStructure t24AttributeStructure;
        //    if (!dict.TryGetValue(attrName, out t24AttributeStructure))
        //    {
        //        t24AttributeStructure = new T24MVAttributeStructure(rule.MultiValueSeparator,
        //                                                            rule.SubValueSeparator)
        //                                    {Name = attrName};
        //        dict[attrName] = t24AttributeStructure;
        //    }

        //    t24AttributeStructure.AddValue(mvIndex, svIndex, attrValue);
        //}


        //private string GetOfsMessageWithShrinkedMultivalues_AA(string ofsMessage)
        //{
        //    string fieldName = string.Empty;
        //    string tempName = string.Empty;
        //    string fieldValue = string.Empty;
        //    string propIndex = "0";
        //    int multiIndex = 0;
        //    string[] pieces = ofsMessage.Split(new[] { ',' }, StringSplitOptions.None);
        //    var finalList = new List<string>();
        //    var dict = new Dictionary<string, T24MVAttributeStructure>();
        //    MultiValueSplitElement rule = null;
        //    string mvIndex = "1";
        //    string svIndex = "1";

        //    foreach (var piece in pieces)
        //    {
        //        if (piece.IndexOf('=') == -1)
        //        {
        //            // leading part, no equation
        //            finalList.Add(piece);
        //            continue;
        //        }
        //        if (piece.IndexOf("::=") != -1)
        //        {
        //            // base attribute single value
        //            CheckAndAddMultiField(finalList, multiIndex, propIndex, ref fieldName, ref tempName, ref dict);
        //            finalList.Add(piece);
        //            continue;
        //        }
        //        // multi value base attribute or property
        //        string[] splitByFieldAndValue = piece.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
        //        string[] attPieces = splitByFieldAndValue[0].Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries);
        //        string attrName = attPieces[0];

        //        switch (attrName)
        //        {
        //            case "PROPERTY":
        //                {
        //                    CheckAndAddMultiField(finalList, multiIndex, propIndex, ref fieldName, ref tempName, ref dict);
        //                    propIndex = attPieces[1];
        //                    finalList.Add(piece);
        //                    multiIndex = 0;
        //                    break;
        //                }
        //            case "FIELD.NAME":
        //                {
        //                    string [] nameParts = splitByFieldAndValue[1].Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
        //                    tempName = nameParts[0];
        //                    rule = null;
        //                    if (!_MutltiValuesProcessRules.TryGetValue(tempName, out rule))
        //                    {
        //                        CheckAndAddMultiField(finalList, multiIndex, propIndex, ref fieldName, ref tempName, ref dict);
        //                        multiIndex++;
        //                        finalList.Add(string.Concat(attrName, ":", propIndex, ":", multiIndex.ToString(), "=",
        //                                                    splitByFieldAndValue[1]));
        //                        continue;
        //                    }
        //                    if (string.IsNullOrEmpty(fieldName))
        //                    {
        //                        mvIndex = nameParts[1];
        //                        svIndex = nameParts[2];
        //                        multiIndex++;
        //                        fieldName = tempName;
        //                    }
        //                    else if (tempName == fieldName)
        //                    {
        //                        mvIndex = nameParts[1];
        //                        svIndex = nameParts[2];
        //                    }
        //                    else
        //                    {
        //                        CheckAndAddMultiField(finalList, multiIndex, propIndex, ref fieldName, ref tempName, ref dict);
        //                        multiIndex++;
        //                        fieldName = tempName;
        //                    }
                            

        //                    break;
        //                }
        //            case "FIELD.VALUE":
        //                {
        //                    if (string.IsNullOrEmpty(tempName) || rule == null)
        //                    {
        //                        finalList.Add(string.Concat(attrName, ":", propIndex, ":", multiIndex.ToString(), "=",
        //                                                    splitByFieldAndValue[1]));
        //                        continue;
        //                    }
        //                    AddMultiValue(fieldName, splitByFieldAndValue[1], dict, rule, mvIndex, svIndex);
        //                    break;
        //                }

        //            default:
        //                {
        //                    attrName = attPieces[0];
        //                    tempName = attrName;

        //                    if (string.IsNullOrEmpty(fieldName))
        //                    {
        //                        fieldName = tempName;
        //                    }
        //                    else if (fieldName != tempName)
        //                    {
        //                        CheckAndAddMultiField(finalList, multiIndex, propIndex, ref fieldName, ref tempName, ref dict);
        //                        // base multivalue attribute
        //                        fieldName = attrName;
        //                    }
        //                    else
        //                        fieldName = attrName;

        //                    if (!_MutltiValuesProcessRules.TryGetValue(attrName, out rule))
        //                    {
        //                        finalList.Add(piece);
        //                        fieldName = string.Empty;
        //                        continue;
        //                    }
        //                    multiIndex = 1;
        //                    propIndex = string.Empty;
        //                    mvIndex = attPieces[1];
        //                    svIndex = attPieces[2];
        //                    string attrValue = splitByFieldAndValue[1];
        //                    AddMultiValue(attrName, attrValue, dict, rule, mvIndex, svIndex);
                           

                          
        //                }
        //                break;
        //        }
            
        //    }

        //    return string.Join(",", finalList);
        //}

        //private void CheckAndAddMultiField(List<string> finalList, int multiIndex, string propIndex, ref string fieldName, ref string tempName, ref Dictionary<string, T24MVAttributeStructure> values)
        //{
        //    if (!string.IsNullOrEmpty(fieldName))
        //    {
        //        bool multiFlag;
        //        _MultiValueNameWithIndexes.TryGetValue(fieldName, out multiFlag);
        //        if (!string.IsNullOrEmpty(propIndex))
        //        {
        //            finalList.Add(string.Concat("FIELD.NAME", ":", propIndex, ":", multiIndex.ToString(), "=", fieldName,
        //                                        multiFlag ? ":1:1" : string.Empty));
        //            finalList.Add(string.Concat("FIELD.VALUE", ":", propIndex, ":", multiIndex.ToString(), "=",
        //                                        values[fieldName].GetValue()));
        //        }
        //        else
        //        {
        //            finalList.Add(string.Concat(fieldName, ":1:1=", values[fieldName].GetValue()));
        //        }
        //    }
        //    fieldName = string.Empty;
        //    tempName = string.Empty;
        //    values = new Dictionary<string, T24MVAttributeStructure>();
        //}

        #endregion

        #region Stretch

        /// <summary>
        /// This function is used to process OFS request, to expand all multivalues
        /// that have delimiters in the rules in difference error config
        /// </summary>
        /// <param name="ofsMessage"></param>
        /// <returns></returns>
        //public string GetOfsMessageWithStretchedMultivalues(string ofsMessage)
        //{
        //    if (_ProcessAa)
        //        return GetOfsMessageWithStretchedMultivalues_AA(ofsMessage);
        //    else
        //        return GetOfsMessageWithStretchedMultivalues_NormalApplication(ofsMessage);
        //}

        /// <summary>
        /// This function should apply on the request OFS already created, to expand all multivalues
        /// that have delimiters in the rules in difference error config, Normal T24 Application 
        /// </summary>
        /// <param name="ofsMessage"></param>
        /// <returns></returns>
        //private string GetOfsMessageWithStretchedMultivalues_NormalApplication(string ofsMessage)
        //{
        //    string[] pieces = ofsMessage.Split(new[] { ',' }, StringSplitOptions.None);

        //    var finalList = new List<string>();

        //    foreach (var piece in pieces)
        //    {
        //        int indexEq = piece.IndexOf("=");
        //        if (indexEq == -1)
        //        {
        //            finalList.Add(piece);
        //            continue;
        //        }
        //        if (piece.IndexOf("::=") != -1)
        //        {

        //            string[] fieldValuePair = piece.Split(new[] {"::="}, StringSplitOptions.None);
        //            string piecetmp = piece.Replace("::=", ":1:1=");


        //            if (fieldValuePair.Length < 2)
        //            {
        //                finalList.Add(piecetmp);
        //            }
        //            else if (fieldValuePair.Length == 2)
        //            {
        //                ProcessNormalFieldOrBaseAttribute(piecetmp, fieldValuePair, finalList);
        //            }
        //            continue;
        //        }
        //        int indexSep = piece.IndexOf(":");
        //        if (indexSep != -1)
        //        {
        //            ProcessNormalFieldOrBaseAttribute(piece, new string[] {piece.Substring(0,indexSep), piece.Substring(indexEq+1)}, finalList);
        //        }
        //        else
        //        {
        //            finalList.Add(piece);
        //        }

        //    }

        //    return String.Join(",", finalList);
        //}

        /// <summary>
        /// Process normaln application attributes, or AA application Base Attributes
        /// </summary>
        /// <param name="piece"></param>
        /// <param name="fieldValuePair"></param>
        /// <param name="finalList"></param>
        //private void ProcessNormalFieldOrBaseAttribute(string piece, string[] fieldValuePair, List<string> finalList)
        //{
        //    string fieldName = fieldValuePair[0];

        //    MultiValueSplitElement mvRule;
        //    if (!_MutltiValuesProcessRules.TryGetValue(fieldName, out mvRule))
        //    {
        //        finalList.Add(piece);
        //        return;
        //    }

        //    string fieldValue = fieldValuePair[1];

        //    string[] multiValues = fieldValue.Split(new[] { mvRule.MultiValueSeparator }, StringSplitOptions.None);

        //    if (multiValues.Length == 1)
        //    {
        //        finalList.Add(piece);
        //        return;
        //    }

        //    for (int i = 1; i <= multiValues.Length; i++)
        //    {
        //        if (multiValues[i - 1] == String.Empty)
        //            continue;

        //        string[] subValues = multiValues[i - 1].Split(new[] { mvRule.SubValueSeparator }, StringSplitOptions.None);

        //        if (subValues.Length == 1)
        //        {
        //            finalList.Add(string.Format("{0}:{1}:1={2}", fieldName, i, multiValues[i - 1]));
        //        }
        //        else
        //            for (int j = 1; j <= subValues.Length; j++)
        //            {
        //                finalList.Add(string.Format("{0}:{1}:{2}={3}", fieldName, i, j, subValues[j - 1]));
        //            }
        //    }
        //}

        /// <summary>
        /// This function process AA request OFS, to expand all multivalues
        /// that have delimiters in the rules in difference error config, AA T24 Application 
        /// </summary>
        /// <param name="ofsMessage"></param>
        /// <returns></returns>
        //private string GetOfsMessageWithStretchedMultivalues_AA(string ofsMessage)
        //{
        //    MultiValueSplitElement rule = null;
        //    string tempName = string.Empty;
        //    string tempValue = string.Empty;
        //    string propIndex = "0";
        //    int multiIndex = 0;
        //    string[] pieces = ofsMessage.Split(new[] { ',' }, StringSplitOptions.None);

        //    var finalList = new List<string>();

        //    foreach (var piece in pieces)
        //    {
        //        if (piece.IndexOf('=') == -1)
        //        {
        //            // leading part, no equation
        //            finalList.Add(piece);
        //            continue;
        //        }
        //        if (piece.IndexOf("::=") != -1)
        //        {
        //            // base attributes of properties
        //            string[] fieldValuePair = piece.Split(new[] {"::="}, StringSplitOptions.None);
        //            string piecetmp = piece.Replace("::=", ":1:1=");
        //            ProcessNormalFieldOrBaseAttribute(piecetmp, fieldValuePair, finalList);
        //            continue;
        //        }
        //        string[] splitByFieldAndValue = piece.Split(new[] {'='}, StringSplitOptions.RemoveEmptyEntries);
        //        string[] attPieces = splitByFieldAndValue[0].Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries);
        //        string attrName = attPieces[0];

        //        switch (attrName)
        //        {
        //            case "PROPERTY":
        //                {
        //                    propIndex = attPieces[1];
        //                    multiIndex = 0;
        //                    finalList.Add(piece);
        //                    break;
        //                }
        //            case "FIELD.NAME":
        //                {
        //                    multiIndex++;
        //                    tempName = splitByFieldAndValue[1];
        //                    int index = tempName.IndexOf(':');
        //                    if (index > 0)
        //                        tempName = tempName.Substring(0, index);
        //                    rule = null;
        //                    if (!_MutltiValuesProcessRules.TryGetValue(tempName, out rule))
        //                    {
        //                        tempName = string.Empty;
        //                        finalList.Add(string.Concat(attrName, ":", propIndex, ":", multiIndex.ToString(), "=",
        //                                                    splitByFieldAndValue[1]));
        //                    }
        //                    break;
        //                }
        //            case "FIELD.VALUE":
        //                {
        //                    if (string.IsNullOrEmpty(tempName) || rule == null)
        //                    {
        //                        finalList.Add(string.Concat(attrName, ":", propIndex, ":", multiIndex.ToString(), "=",
        //                                                    splitByFieldAndValue[1]));
        //                        break;
        //                    }
        //                    multiIndex--;
        //                    string[] mv = ValidataCommon.StringMethods.RegexSplit(splitByFieldAndValue[1],
        //                                                                          rule.MultiValueSeparator);
        //                    int mvI = 0;
        //                    int svI = 0;
        //                    foreach (string mvPart in mv)
        //                    {
        //                        string[] sv = ValidataCommon.StringMethods.RegexSplit(mvPart, rule.SubValueSeparator);
        //                        mvI++;
        //                        svI = 0;
        //                        foreach (string svPart in sv)
        //                        {
        //                            svI++;
        //                            multiIndex++;
        //                            finalList.Add(string.Concat("FIELD.NAME", ":", propIndex, ":", multiIndex.ToString(),
        //                                                        "=", tempName, ":", mvI, ":", svI));
        //                            finalList.Add(string.Concat(attrName, ":", propIndex, ":", multiIndex.ToString(),
        //                                                        "=",
        //                                                        svPart));
        //                        }
        //                    }
        //                    break;
        //                }
        //            default:
        //                {
        //                    rule = null;
        //                    if (!_MutltiValuesProcessRules.TryGetValue(attrName, out rule))
        //                    {
        //                        tempName = string.Empty;
        //                        finalList.Add(piece);
        //                    }
        //                    int mvI = 0;
        //                    int svI = 0;
        //                    int.TryParse(attPieces[1], out mvI);
        //                    int.TryParse(attPieces[2], out svI);
        //                    mvI--;
        //                    svI--;
        //                    string[] mv = ValidataCommon.StringMethods.RegexSplit(splitByFieldAndValue[1],
        //                                                                          rule.MultiValueSeparator);
        //                    foreach (string mvPart in mv)
        //                    {
        //                        string[] sv = ValidataCommon.StringMethods.RegexSplit(mvPart, rule.SubValueSeparator);
        //                        mvI++;
        //                        foreach (string svPart in sv)
        //                        {
        //                            svI++;
        //                            finalList.Add(string.Concat(attrName, ":", mvI.ToString(), ":", svI.ToString(),
        //                                                        "=",
        //                                                        svPart));
        //                        }
        //                        svI = 0;
        //                    }
        //                }
        //                break;

        //        }
        //    }
        //    return String.Join(",", finalList);
        //}
    
        #endregion
    }

    public static class MultiValueShrinkStretchCache
    {
        private static string _ProcessName;
        private static string ProcessName
        {
            get
            {
                if(_ProcessName == null)
                    _ProcessName = Process.GetCurrentProcess().ProcessName;

                return _ProcessName;
            }
        }

        private static readonly Dictionary<string, MultiValueShrinkStretch> _Cache = new Dictionary<string, MultiValueShrinkStretch>();

        private static object _Sync = new object();

        public static MultiValueShrinkStretch GetByTypical(MetadataTypical typical)
        {
            if (ProcessName.StartsWith("AdapterGateway") || ProcessName.StartsWith("DataMigrationStudio"))
            {
                string typicalId = typical.ToString();

                lock (_Sync)
                {
                    if (!_Cache.ContainsKey(typicalId))
                        _Cache[typicalId] = new MultiValueShrinkStretch(typical);
                }

                return _Cache[typicalId];
            }
            else
            { 
                // this should be request manager - we don't need this here
                return null;
            }
        }
    }
}
