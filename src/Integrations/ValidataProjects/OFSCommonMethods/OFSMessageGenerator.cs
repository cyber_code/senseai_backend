using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;
using System.Xml;
using AXMLEngine;
using ValidataCommon;
using OFSCommonMethods.IO;

namespace OFSCommonMethods
{
    /// <summary>
    /// Generator of OFS Messages
    /// </summary>
    public class OFSMessageGenerator : OFSMessageGeneratorBase
    {
        #region Private Members

        ///// <summary>
        ///// Collection of GlobusReques objects processed by this message generator
        ///// </summary>
        //private GlobusRequestCollection _GlobusRequests = new GlobusRequestCollection();

        /// <summary>
        /// Processed requests
        /// </summary>
        private List<GlobusRequestShort> _ProcessedInstances = new List<GlobusRequestShort>();

        /// <summary>
        /// Current execution time out
        /// </summary>
        private long _CurrentStep1TimeOut;

        /// <summary>
        /// Full Output VXML XmlTextWriter
        /// </summary>
        private XmlWriter _OutputVXMLTextWriter;

        ///// <summary>
        ///// Full Output VXML XmlTextWriter
        ///// </summary>
        //private XmlWriter _OutputAXMLTextWriter;

        /// <summary>
        /// Full Error XmlTextWriter
        /// </summary>
        private XmlWriter _ErrorXMLTextWriter;

        /// <summary>
        /// Full OFS Globus Messages XmlTextWriter
        /// </summary>
        private XmlWriter _OFSGlobusMessageWriter;

        /// <summary>
        /// Full OFS Validata Messages XmlTextWriter
        /// </summary>
        private XmlWriter _OFSValidataMessageWriter;

        private uint _ErrorID = 0;

        private readonly int _GeneratorIndex = 0;

        private bool _SkipRestOfInstances = false;

        private int _StopOnNotResponding = -1;

        private uint _CurrentNumberOfInstacesInChunk = uint.MinValue;

        private uint _CurrentChunkStartIndex = uint.MinValue;

        private uint _MaxNumberOfInstancesInChunk = uint.MinValue;

        private int _SleepInMillisecondsBeforeRetry = int.MinValue;

        private string _ErrorTempFilePath;

        private string _ValidataMessageFilePath;

        private string _GlobusMessageFilePath;

        /// <summary>
        /// True if the OFS should be in VALIDATE mode
        /// </summary>
        private bool _IsValidateMode = false;

        /// <summary>
        /// True if the OFS should be in VALIDATE mode
        /// </summary>
        private bool _IsIAMode = true;

        /// <summary>
        /// Sends I messages with no attributes
        /// </summary>
        private bool _IsHAMode = false;

        /// <summary>
        /// Determine wheather to import authorised records only
        /// </summary>
        private bool _ImportAuthorisedRecordsOnly = false;

        private GlobusImportType _ImportType;
        private Settings _TelnetSet;

        #endregion

        #region Public Properties

        /// <summary>
        /// True if the OFS should be in VALIDATE mode
        /// </summary>
        public bool IsValidateMode
        {
            get { return _IsValidateMode; }
            set { _IsValidateMode = value; }
        }

        /// <summary>
        /// True if the export to Globus should be in separate insert and autorize steps
        /// </summary>
        public bool IsIAMode
        {
            get { return _IsIAMode; }
            set { _IsIAMode = value; }
        }

        public bool IsHAMode
        {
            get { return _IsHAMode; }
            set { _IsHAMode = value; }
        }

        private bool StopOnNotResponding = true;

        public uint MaxNumberOfInstancesInChunk
        {
            get
            {
                if (_MaxNumberOfInstancesInChunk == uint.MinValue)
                {
                    _MaxNumberOfInstancesInChunk = 400;
                }

                return _MaxNumberOfInstancesInChunk;
            }
        }

        public int SleepInMillisecondsBeforeRetry
        {
            get
            {
                if (_SleepInMillisecondsBeforeRetry == int.MinValue)
                {
                    int confValue = 500;

                    _SleepInMillisecondsBeforeRetry = confValue;
                }

                return _SleepInMillisecondsBeforeRetry;
            }
        }

        //public XmlWriter OutputAXMLTextWriter
        //{
        //    get { return _OutputAXMLTextWriter; }
        //    set { _OutputAXMLTextWriter = value; }
        //}

        public GlobusImportType ImportType
        {
            get { return _ImportType; }
            set { _ImportType = value; }
        }

        public string ErrorTempFilePath
        {
            get { return _ErrorTempFilePath; }
            set { _ErrorTempFilePath = value; }
        }

        public string ValidataMessageFilePath
        {
            get { return _ValidataMessageFilePath; }
            set { _ValidataMessageFilePath = value; }
        }

        public string GlobusMessageFilePath
        {
            get { return _GlobusMessageFilePath; }
            set { _GlobusMessageFilePath = value; }
        }

        #endregion

       
        #region Public methods 

        /// <summary>
        /// Create Globus Requests
        /// </summary>
        /// <param name="instances"> XML string containing instances </param>
        /// <param name="requestTypical"> Request typical </param>
        /// <param name="responseTypical"> Response typical </param>
        /// <param name="configuration"> Configuration parameter </param>
        /// <param name="instances"></param>
        /// <param name="version"></param>
        /// <param name="company"></param>
        public void CreateGlobusRequests(InstanceCollection instances, MetadataTypical requestTypical,
                                         MetadataTypical responseTypical, string configuration, string version,
                                         string company)
        {
            int numProcessed = 0;
            // we will set up DataProcessType according to configuration parameter
            DataProcessType dpType = GlobusRequest.GetDataProcessType(configuration);

            bool flagNeedsAuthorization = (configuration.IndexOf('A') >= 0);

            foreach (Instance inst in instances)
            {
                MetadataTypical typ;
                GlobusRequest.RequestType reqType;

                GetRequestType(inst, out typ, out reqType);

                // this is necessary, because for Enquiry types, we don't need instances tag, 
                // but it is prepared during forward transformation and we have to skip it
                if (reqType == GlobusRequest.RequestType.Enquiry)
                {
                    break;
                }

                GlobusRequest request = new GlobusRequest(typ, inst, reqType, dpType, version, company);

                // Set up Authorization - if needed
                request.NeedsAuthorize = flagNeedsAuthorization;

                //_GlobusRequests.AddGlobusRequest(request);

                numProcessed++;
            }

            if (numProcessed == 0)
            {
                GlobusRequest request;
                if (configuration == "HIS" || configuration == "LIVE")
                {
                    request = new GlobusRequest(
                        requestTypical, null, GlobusRequest.RequestType.History, dpType, version, company);
                }
                else
                {
                    GlobusRequest.RequestType reqType = GlobusRequest.GetMessageType(requestTypical);
                    request = new GlobusRequest(requestTypical, null, reqType, dpType, version, company);
                }
                request.ResponseTypical = responseTypical;
            }
        }

        private void GetRequestType(Instance inst, out MetadataTypical typ, out GlobusRequest.RequestType reqType)
        {
            typ = FindTypicalByNameAndCatalog(inst.TypicalName, inst.Catalog);

            if (typ == null)
                throw new ApplicationException(String.Format("The adapter request does not contain metadata for '{0}:{1}'.", inst.Catalog, inst.TypicalName));

            reqType = GlobusRequest.GetMessageType(typ);
        }

        private GlobusMessage CreateFileWithoutRegisteringRequest(GlobusRequest request, bool waitingForAuthorization,
            string requestUID)
        {
            string firstCommand = request.GenerateMessageText(
                        _CommandUser, _CommandPassword,
                        _AuthorizeUser, _AuthorizePassword,
                        _Filters, waitingForAuthorization, 
                        _IsValidateMode, false, _AlwaysUseAuthentication,
                        _OfsType);

            GlobusMessage globusMessage = _MessageQueue.ProcessMessage_Bulk(waitingForAuthorization, firstCommand, _GUID);

            request.MessageFileID = _GUID;

            if (waitingForAuthorization)
            {
                request.WaitingForAuthorization = true;
            }
            else
            {
                request.StartWaitingForProcessing();
            }

            return globusMessage;
        }

        private GlobusMessage CreateFileWithoutRegisteringRequest(GlobusRequest request, bool waitingForAuthorization,
                                                                  GlobusRequestShort shortRequest)
        {
            _GUID = shortRequest.GUID;
            return CreateFileWithoutRegisteringRequest(request, waitingForAuthorization, String.Empty);
        }

        private string GetOFSRequestID(GlobusRequestShort shortRequest)
        {   
            if (shortRequest.ValidataID == null 
                || shortRequest.ValidataID == String.Empty)
            {
                shortRequest.ValidataID = _ProcessedInstances.Count.ToString();
            }

            string requestUID = shortRequest.ValidataID;
            return requestUID;
        }

        /// <summary>
        /// Logs the validata OFS command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="requestUID">The request UID.</param>
        /// <param name="requestShort">The request short.</param>
        /// <param name="execState">State of the exec (TODO not used).</param>
        private void LogValidataOFSCommand(string command, string requestUID, GlobusRequestShort requestShort,
                                           GlobusRequest.ExecutionState execState)
        {
            _OFSValidataMessageWriter.WriteStartElement(AXMLConstants.OFS_VALIDATA_TAG);
            _OFSValidataMessageWriter.WriteAttributeString(AXMLConstants.GENERATOR_ID_TAG, _GeneratorIndex.ToString());
            _OFSValidataMessageWriter.WriteAttributeString(AXMLConstants.REQUEST_ATTRIBUTE_ID, requestUID);
            _OFSValidataMessageWriter.WriteAttributeString(AXMLConstants.REQUEST_EXECUTION_STATE,
                                                           requestShort.ExecutionState.ToString());
            _OFSValidataMessageWriter.WriteAttributeString(AXMLConstants.OFS_GLOBUS_ID, requestShort.GlobusID);
            _OFSValidataMessageWriter.WriteString(command);
            _OFSValidataMessageWriter.WriteEndElement();
            _OFSValidataMessageWriter.WriteRaw("\n\t");
        }

        private void LogGlobusOFSMessage(GlobusMessage message, GlobusRequestShort requestShort,
                                         GlobusRequest.ExecutionState execState)
        {
            // in case of AA display the whole OFS, for normal app - the same Text
            LogGlobusOFSCommand(message.Message.ToLongString(), requestShort, execState);
        }

        private void LogGlobusOFSMessage(string message, GlobusRequestShort requestShort,
                                        GlobusRequest.ExecutionState execState)
        {
            LogGlobusOFSCommand(message, requestShort, execState);
        }

        private void LogGlobusOFSCommand(string globusMessageText, GlobusRequestShort requestShort,
                                         GlobusRequest.ExecutionState execState)
        {
            string requestUID = GetOFSRequestID(requestShort);

            _OFSGlobusMessageWriter.WriteStartElement(AXMLConstants.OFS_VALIDATA_TAG);
            _OFSGlobusMessageWriter.WriteAttributeString(AXMLConstants.GENERATOR_ID_TAG, _GeneratorIndex.ToString());
            _OFSGlobusMessageWriter.WriteAttributeString(AXMLConstants.REQUEST_ATTRIBUTE_ID, requestUID);
            _OFSGlobusMessageWriter.WriteAttributeString(AXMLConstants.REQUEST_EXECUTION_STATE, execState.ToString());
            _OFSGlobusMessageWriter.WriteAttributeString(AXMLConstants.OFS_GLOBUS_ID, requestShort.GlobusID);
            _OFSGlobusMessageWriter.WriteString(globusMessageText);
            _OFSGlobusMessageWriter.WriteEndElement();
            _OFSGlobusMessageWriter.WriteRaw("\n\t");
        }

        public void GetInstances(object dataset, WriteInstancesEventArgs args)
        {
            if (dataset.GetType() == typeof (AXMLDataset))
            {
                AXMLDataset dataSet = (AXMLDataset) dataset;

                bool reslt = DoEnquiry(args);

                _OFSGlobusMessageWriter.Flush();
                _OFSValidataMessageWriter.Flush();
                _OutputVXMLTextWriter.Flush();
                _ErrorXMLTextWriter.Flush();

                _OFSGlobusMessageWriter.Close();
                _OFSValidataMessageWriter.Close();
                _OutputVXMLTextWriter.Close();
                _ErrorXMLTextWriter.Close();

                GenerateErrorFile(dataSet.ErrorsFilePath, _ErrorTempFilePath, _ValidataMessageFilePath, _GlobusMessageFilePath, String.Empty);
            }
        }

        private bool DoEnquiry(WriteInstancesEventArgs args)
        {
            EventLogger.StartLogging(true);

            EventLogger.LogEvents("One thread", GlobusEventType.ExecPointReached, "Worker Thread Started");

            bool typicalMetadataQueryMode = GlobusMetadataReader.IsExpectedTypical(_OFSParameters.RequestTypical);
            
            Instance instance = null;
            DataProcessType dataProcessType = DataProcessType.None;
            GlobusRequest.RequestType reqType = GlobusRequest.RequestType.Enquiry;
            if (_ImportType != GlobusImportType.TESTDATA)
            {
                reqType = GlobusRequest.RequestType.History;
            }
            else if (typicalMetadataQueryMode) //HACK this is not really an enquiry
            {
                reqType = GlobusRequest.RequestType.DataProcess;
                dataProcessType = GlobusRequest.GetDataProcessType(_OFSParameters.Configuration); 
                if (_OFSParameters.Instances != null && _OFSParameters.Instances.Count > 0)
                {
                    instance = _OFSParameters.Instances[0];
                }
            }

            GlobusRequest request = new GlobusRequest(_OFSParameters.RequestTypical, instance,
                reqType, dataProcessType, _OFSParameters.Version, _OFSParameters.Company, _TelnetSet.PhantomSet);
            request.CustomEnquiries = _TelnetSet.CustomEnquiries;

            EventLogger.LogEvents("One thread", GlobusEventType.ExecPointReached, "T24 Requests Created");

            bool isImpTestData = (_ImportType == GlobusImportType.TESTDATA);
            string firstCommand = request.GenerateMessageText(
                    _CommandUser, _CommandPassword,
                    _AuthorizeUser, _AuthorizePassword,
                    _Filters, false, _IsValidateMode,
                    isImpTestData, _AlwaysUseAuthentication,
                    _OfsType);

            GlobusMessage message = _MessageQueue.ProcessMessage_Bulk(false, firstCommand, _GUID);

            request.MessageFileID = message.ID;
            request.State = GlobusRequest.ExecutionState.WaitingForProcessing;
            request.ResponseTypical = _OFSParameters.ResponseTypical;

            GlobusRequestShort reqShort = new GlobusRequestShort(1);

            reqShort.GUID = _GUID;

            reqShort.SetShortResponseProperties(request);

            LogValidataOFSCommand(request.FirstMessage, _GUID, reqShort,
                                  GlobusRequest.ExecutionState.WaitingForProcessing);

            EventLogger.LogEvents("One thread", GlobusEventType.ExecPointReached, "Requests Written to T24");

            bool reslt = ReadEnquiryResponse(reqShort, request, args.xmlWriter);

            if (typicalMetadataQueryMode) // HACK part of the hack
            {
                args.xmlWriter.WriteRaw(request.Result);
            }

            return reslt;
        }

        public bool ReadEnquiryResponse(GlobusRequestShort requestShort, GlobusRequest request, XmlWriter axmlWriter)
        {
            bool continueProcessing = true;

            _CurrentStep1TimeOut = DateTime.Now.AddMilliseconds(TimeoutStep1).Ticks;
            while (continueProcessing && _CurrentStep1TimeOut > DateTime.Now.Ticks)
            {
                continueProcessing = false;
                string requestGUID = requestShort.GUID;

                GlobusMessage globusMessage = _MessageQueue.DataMessageRead(requestShort, requestGUID);

                if (globusMessage != null)
                {
                    LogGlobusOFSMessage(globusMessage, requestShort, GlobusRequest.ExecutionState.Processed);

                    _CurrentStep1TimeOut = DateTime.Now.AddMilliseconds(TimeoutStep1).Ticks;

                    if (requestShort.ExecutionState == GlobusRequest.ExecutionState.WaitingForProcessing)
                    {
                        List<string> enquiryResults = request.SetResponse(
                            globusMessage.Message, 
                            null, 
                            axmlWriter, 
                            null, 
                            _TreatOFSResponseSingleValueAsError, 
                            EnquiryResultParsingRotine.None,
                            _OfsType);

                        requestShort.ExecutionState = GlobusRequest.ExecutionState.Processed;
                        if (!request.Failed)
                        {
                            if (requestShort.RequestType == GlobusRequest.RequestType.History)
                            {
                                CreateProductionRequests(enquiryResults, axmlWriter);
                                enquiryResults.Clear();
                                enquiryResults = null;
                            }
                        }
                        else
                        {
                            AddInvalidRowXML(requestShort, globusMessage,
                                             GlobusRequest.ExecutionState.WaitingForProcessing,
                                             GlobusRequest.ExecutionState.WaitingForAuthorization);
                        }
                    }
                }
                else
                {
                    Thread.Sleep(SleepInMillisecondsBeforeRetry);
                    continueProcessing = true;
                }

                globusMessage = null;
                requestGUID = null;
            }

            bool hasUnprocessed = WriteUnprocessedRequest(GlobusRequest.ExecutionState.WaitingForProcessing, requestShort);
            return !hasUnprocessed;
        }

        public void CreateProductionRequests(List<string> globusIDs, XmlWriter axmlWriter)
        {
            _ProcessedInstances.Clear();
            _Filters.Clear();

            for (int i = 0; i < globusIDs.Count; i++)
            {
                string newGUID = Guid.NewGuid().ToString();
                //at this stage we do care about companies, so the last argument is needed
                GlobusRequest tempRequest = new GlobusRequest(
                    _OFSParameters.ResponseTypical, CreateFakeInstance(globusIDs[i]),
                    GlobusRequest.RequestType.DataProcess, DataProcessType.Select,
                    _OFSParameters.Version, _OFSParameters.Company);

                string firstCommand = tempRequest.GenerateMessageText(
                        _CommandUser, _CommandPassword,
                        _AuthorizeUser, _AuthorizePassword,
                        _Filters, false, _IsValidateMode, false, _AlwaysUseAuthentication,
                        _OfsType);

                GlobusMessage message = _MessageQueue.ProcessMessage_Bulk(false, firstCommand, newGUID);

                tempRequest.MessageFileID = message.ID;
                tempRequest.State = GlobusRequest.ExecutionState.WaitingForProcessing;
                tempRequest.ResponseTypical = _OFSParameters.ResponseTypical;

                GlobusRequestShort reqShort = new GlobusRequestShort(_ProcessedInstances.Count);

                reqShort.GUID = newGUID;

                reqShort.SetShortResponseProperties(tempRequest);

                if (!_ProcessedInstances.Contains(reqShort))
                {
                    _ProcessedInstances.Add(reqShort);
                    _CurrentNumberOfInstacesInChunk++;
                    if (_CurrentNumberOfInstacesInChunk == MaxNumberOfInstancesInChunk)
                    {
                        GetCompleteInstances(axmlWriter, true);
                    }
                }
            }

            GetCompleteInstances(axmlWriter, true);

            EventLogger.LogEvents(_GUID, GlobusEventType.ExecPointReached, "History Step 2 Written T24 Files");
        }

        public bool GetCompleteInstances(XmlWriter axmlWriter, bool getHistoryRecords)
        {
            bool continueProcessing = true;

            _CurrentStep1TimeOut = DateTime.Now.AddMilliseconds(TimeoutStep1).Ticks;

            while (continueProcessing && _CurrentStep1TimeOut > DateTime.Now.Ticks)
            {
                continueProcessing = false;

                bool noRecordsProcessed = true;
                int currentChunkStartIndex = Convert.ToInt32(_CurrentChunkStartIndex);
                for (int i = currentChunkStartIndex; i < _ProcessedInstances.Count; i++)
                {
                    GetCompleteInstancesRecursive(axmlWriter, _ProcessedInstances[i],
                        ref continueProcessing, ref noRecordsProcessed, getHistoryRecords, i);
                }

                if (noRecordsProcessed)
                {
                    break;
                }
                else
                {
                    Thread.Sleep(SleepInMillisecondsBeforeRetry);
                }
            }
            _CurrentChunkStartIndex += _CurrentNumberOfInstacesInChunk;
            _CurrentNumberOfInstacesInChunk = uint.MinValue;
            bool hasUnprocessed = WriteUnprocessedInstances(GlobusRequest.ExecutionState.WaitingForProcessing);
            
            if (_ImportType == GlobusImportType.HIS
                && getHistoryRecords)
            {
                AddHistoryShortRequests(axmlWriter);
            }

            return !hasUnprocessed;
        }

        private void GetCompleteInstancesRecursive(XmlWriter axmlWriter, GlobusRequestShort requestShort,
            ref bool continueProcessing, ref bool noRecordsProcessed, bool getHistoryRecords, int index)
        {
            if (!requestShort.Failed
                && requestShort.ExecutionState == GlobusRequest.ExecutionState.WaitingForProcessing)
            {
                noRecordsProcessed = false;

                string requestGUID = requestShort.GUID;

                //check the state after processing
                GlobusMessage globusMessage = _MessageQueue.DataMessageRead(requestShort, requestGUID);

                if (globusMessage != null)
                {
                    LogGlobusOFSMessage(globusMessage, requestShort,
                                        GlobusRequest.ExecutionState.WaitingForProcessing);

                    _CurrentStep1TimeOut = DateTime.Now.AddMilliseconds(TimeoutStep1).Ticks;

                    GlobusRequest globusRequest = new GlobusRequest(
                        requestShort, _OFSParameters.ResponseTypical, 
                        _OFSParameters.Version, _OFSParameters.Company);

                    requestShort.SetResponse(
                        globusRequest, 
                        globusMessage, 
                        _TreatOFSResponseSingleValueAsError,
                        _OfsType);

                    if (!requestShort.Failed)
                    {
                        if (_ImportType != GlobusImportType.HIS && _ImportAuthorisedRecordsOnly)
                        {
                            Instance firstInst = Instance.FromXmlString(globusRequest.Result);
                            InstanceAttribute recordStatus = firstInst.GetAttributeByShortName("RECORD.STATUS");

                            if (recordStatus != null)
                            {
                                #region Execute request reqursive with CURR.NO - 1
                                int currNo;
                                if (int.TryParse(requestShort.CURRNO, out currNo))
                                {
                                    if (currNo > 1)
                                    {
                                        string ID = string.Format("{0};{1}", requestShort.GlobusID, currNo - 1);
                                        requestShort = CreateShortRequest(ID);

                                        GetCompleteInstancesRecursive(axmlWriter, requestShort,
                                            ref continueProcessing, ref noRecordsProcessed, getHistoryRecords, index);
                                    }
                                    else
                                    {
                                        _ProcessedInstances.RemoveAt(index);
                                    }

                                    return;
                                }
                                #endregion
                            }
                            else
                            {
                                axmlWriter.WriteRaw(globusRequest.Result);
                            }
                        }
                        else
                        {
                            axmlWriter.WriteRaw(globusRequest.Result);
                        }
                    }
                    else
                    {
                        if (_ImportType != GlobusImportType.HIS && _ImportAuthorisedRecordsOnly)
                        {
                            if (!String.IsNullOrEmpty(globusMessage.Message.Text) && 
                                globusMessage.Message.Text.Contains("HISTORY RECORD MISSING"))
                            {
                                #region Execute request reqursive with CURR.NO - 1
                                int currNo;
                                if (int.TryParse(requestShort.CURRNO, out currNo))
                                {
                                    if (currNo > 1)
                                    {
                                        string ID = string.Format("{0};{1}", requestShort.GlobusID, currNo - 1);
                                        requestShort = CreateShortRequest(ID);

                                        GetCompleteInstancesRecursive(axmlWriter, requestShort,
                                            ref continueProcessing, ref noRecordsProcessed, getHistoryRecords, index);
                                    }
                                    else
                                    {
                                        _ProcessedInstances.RemoveAt(index);
                                    }

                                    return;
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            if (!globusMessage.Message.Text.Contains("HISTORY RECORD MISSING"))
                            {
                                AddInvalidRowXML(requestShort, globusMessage,
                                                 GlobusRequest.ExecutionState.WaitingForProcessing,
                                                 GlobusRequest.ExecutionState.WaitingForProcessing);
                            }
                        }
                    }


                    if (_ImportType == GlobusImportType.HIS && !getHistoryRecords)
                    {
                        requestShort.ExecutionState = GlobusRequest.ExecutionState.Finished;
                    }
                    else
                    {
                        requestShort.ExecutionState = GlobusRequest.ExecutionState.Processed;
                    }

                    _ProcessedInstances[index] = requestShort;

                    globusRequest = null;
                }
                else
                {
                    continueProcessing = true;
                }

                globusMessage = null;
                requestGUID = null;
            }

            requestShort = null;
        }

        private GlobusRequestShort CreateShortRequest(string ID)
        {
            string newGUID = Guid.NewGuid().ToString();

            GlobusRequest tempRequest = new GlobusRequest(
                _OFSParameters.ResponseTypical, CreateFakeInstance(ID),
                                   GlobusRequest.RequestType.DataProcess,
                                   DataProcessType.Select, _OFSParameters.Version,
                                   _OFSParameters.Company);

            tempRequest.NeedsAuthorize = false;
            tempRequest.State = GlobusRequest.ExecutionState.WaitingForProcessing;

            string firstCommand = tempRequest.GenerateMessageText(
                    _CommandUser, _CommandPassword,
                    _AuthorizeUser, _AuthorizePassword,
                    _Filters, false,
                    _IsValidateMode, false, _AlwaysUseAuthentication,
                    _OfsType
                );

            GlobusMessage message = _MessageQueue.ProcessMessage_Bulk(false, firstCommand, newGUID);

            tempRequest.MessageFileID = message.ID;
            tempRequest.State = GlobusRequest.ExecutionState.WaitingForProcessing;
            tempRequest.ResponseTypical = _OFSParameters.ResponseTypical;

            GlobusRequestShort reqShort = new GlobusRequestShort(tempRequest);

            reqShort.GUID = newGUID;

            reqShort.SetShortResponseProperties(tempRequest);

            return reqShort;
        }

        private void AddHistoryShortRequests(XmlWriter axmlWriter)
        {
            int numberOfOriginals = _ProcessedInstances.Count;
            for (int i = 0; i < numberOfOriginals; i++)
            {
                GlobusRequestShort requestShort = _ProcessedInstances[i];

                if (!requestShort.Failed
                    && requestShort.ExecutionState == GlobusRequest.ExecutionState.Processed)
                {
                    int numberOfVersions;
                    if (int.TryParse(requestShort.CURRNO, out numberOfVersions))
                    {
                        requestShort.ExecutionState = GlobusRequest.ExecutionState.Finished;

                        for (int j = 1; j < numberOfVersions; j++)
                        {
                            string newGUID = Guid.NewGuid().ToString();

                            string ID = string.Format("{0};{1}", requestShort.GlobusID, j);

                            GlobusRequest tempRequest =
                                new GlobusRequest(_OFSParameters.ResponseTypical, CreateFakeInstance(ID),
                                                  GlobusRequest.RequestType.DataProcess,
                                                  DataProcessType.Select, _OFSParameters.Version,
                                                  _OFSParameters.Company);

                            tempRequest.NeedsAuthorize = false;
                            tempRequest.State = GlobusRequest.ExecutionState.WaitingForProcessing;

                            string firstCommand = tempRequest.GenerateMessageText(
                                    _CommandUser, _CommandPassword,
                                    _AuthorizeUser, _AuthorizePassword,
                                    _Filters, false,
                                    _IsValidateMode, false, _AlwaysUseAuthentication,
                                    _OfsType);

                            GlobusMessage message = _MessageQueue.ProcessMessage_Bulk(false, firstCommand, newGUID);

                            tempRequest.MessageFileID = message.ID;
                            tempRequest.State = GlobusRequest.ExecutionState.WaitingForProcessing;
                            tempRequest.ResponseTypical = _OFSParameters.ResponseTypical;

                            GlobusRequestShort reqShort = new GlobusRequestShort(_ProcessedInstances.Count);

                            reqShort.GUID = newGUID;

                            reqShort.SetShortResponseProperties(tempRequest);
                            if (!_ProcessedInstances.Contains(reqShort))
                            {
                                _ProcessedInstances.Add(reqShort);

                                _CurrentNumberOfInstacesInChunk++;

                                if (_CurrentNumberOfInstacesInChunk == MaxNumberOfInstancesInChunk)
                                {
                                    GetCompleteInstances(axmlWriter, false);
                                }
                            }
                        }
                    }
                }
            }

            GetCompleteInstances(axmlWriter, false);
        }

        public void ProcessInstance(object dataset, ReadInstancesEventArgs args)
        {
            if (_SkipRestOfInstances)
            {
                return;
            }

            _CurrentNumberOfInstacesInChunk++;
            DoOFSInstance(dataset, args);

            if (_CurrentNumberOfInstacesInChunk == MaxNumberOfInstancesInChunk)
            {
                bool res = FinalizeChunkProcessing();
                if (!res)
                    _SkipRestOfInstances = true;
            }
        }

        public bool FinalizeChunkProcessing()
        {
            if (_CurrentNumberOfInstacesInChunk > uint.MinValue)
            {
                bool res = DoOFSInstanceSecond();
                if (!res && StopOnNotResponding)
                    return false;

                if (_IsIAMode)
                {
                    res = DoOFSInstanceThird();
                    if (!res && StopOnNotResponding)
                        return false;
                }

                _CurrentNumberOfInstacesInChunk = uint.MinValue;
            }

            return true;
        }

        public void DoOFSInstance(object dataset, ReadInstancesEventArgs args)
        {
            //EventLogger.LogEvents(_GUID, GlobusEventType.ExecPointReached, "Worker Thread Started");
            _GUID = IO.FileBasedIOManager.CreateGUID();
            _MessageIndex++;

            bool flagNeedsAuthorization = (_OFSParameters.Configuration.IndexOf('A') >= 0 ? true : false);

            Instance inst;
            if (!IsHAMode)
            {
                inst = args.instance;
            }
            else
            {
                inst = new Instance(args.instance.TypicalName, args.instance.Catalog);
                inst.AddAttribute("ID", args.instance.GetAttributeValue("ID"));
                inst.AddAttribute("@ID", args.instance.GetAttributeValue("@ID"));
            }

            DataProcessType dpType = GlobusRequest.GetDataProcessType(_OFSParameters.Configuration);

            MetadataTypical typ;
            GlobusRequest.RequestType reqType;

            GetRequestType(inst, out typ, out reqType);

            GlobusRequest request = null;
            // this is necessary, because for Enquiry types, we don't need instances tag, 
            // but it is prepared during forward transformation and we have to skip it
            if (reqType != GlobusRequest.RequestType.Enquiry)
            {
                request = new GlobusRequest(typ, inst, reqType, dpType, _OFSParameters.Version, _OFSParameters.Company);

                // Set up Authorization - if needed
                request.NeedsAuthorize = flagNeedsAuthorization;
            }

            if (request == null)
            {
                request = new GlobusRequest(typ, null, reqType, dpType, _OFSParameters.Version, _OFSParameters.Company);

                request.ResponseTypical = typ;
            }

            GlobusRequestShort reqShort;
            if (request.Instance != null)
            {
                reqShort = new GlobusRequestShort(request.Instance);
            }
            else
            {
                reqShort = new GlobusRequestShort(_ProcessedInstances.Count);
            }

            reqShort.GUID = Guid.NewGuid().ToString();

            try
            {
                if (_OFSParameters.Configuration == "A") // only Authorization is needed
                {
                    //In this case only authorization must be carried out

                    //TODO:CHECK ... if there are no XML File
                    CreateFileWithoutRegisteringRequest(request, true, reqShort);

                    reqShort.SetShortResponseProperties(request);
                    LogValidataOFSCommand(request.FirstMessage, GetOFSRequestID(reqShort), reqShort,
                                      GlobusRequest.ExecutionState.WaitingForAuthorization);

                    //AuthorizeOnly();
                }
                else
                {
                    CreateFileWithoutRegisteringRequest(request, false, reqShort);

                    reqShort.SetShortResponseProperties(request);

                    request.NeedsAuthorize = flagNeedsAuthorization;
                    reqShort.ExecutionState = GlobusRequest.ExecutionState.WaitingForProcessing;
                    LogValidataOFSCommand(request.FirstMessage, GetOFSRequestID(reqShort), reqShort,
                                      GlobusRequest.ExecutionState.WaitingForProcessing);
                }

                _ProcessedInstances.Add(reqShort);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvents(_GUID, GlobusEventType.Crash, ex.Message);

                string errorMessage = GetErrorMessageFull(ex);
                AddInvalidRowXML(reqShort, errorMessage
                                 , GlobusRequest.ExecutionState.Initial
                                 , GlobusRequest.ExecutionState.WaitingForProcessing
                                 , GlobusRequest.ErrorState.Undefined);
            }
        }

        public static string GetErrorMessageFull(Exception ex)
        {
            return string.Format("Message=[{0}] StackTrace=[{1}]", ex.Message, ex.StackTrace);
        }

        private void WritingVXMLInstance(GlobusRequestShort shortRequest, string instanceXML)
        {
            if (IsForVXMLWriting(shortRequest))
            {
                AddInstanceVXML(instanceXML);
            }
        }

        /// <summary>
        /// Add Error XML
        /// </summary>
        private void AddInstanceVXML(string instanceXML)
        {
            _OutputVXMLTextWriter.WriteRaw(instanceXML);
            _OutputVXMLTextWriter.WriteRaw("\n\t");
        }

        /// <summary>
        /// Add Error XML
        /// </summary>
        private void AddInvalidRowXML(GlobusRequestShort shortRequest, string errorMessage,
            GlobusRequest.ExecutionState firstState,
            GlobusRequest.ExecutionState secondState,
            GlobusRequest.ErrorState errorState)
        {
            AddInvalidRowXML(shortRequest, errorMessage, firstState, secondState, errorState, Error.ErrorTypes.Normal);
        }

        /// <summary>
        /// Add Error XML
        /// </summary>
        private void AddInvalidRowXML(GlobusRequestShort shortRequest, string errorMessage,
            GlobusRequest.ExecutionState firstState,
            GlobusRequest.ExecutionState secondState,
            GlobusRequest.ErrorState errorState,
            AXMLEngine.Error.ErrorTypes errType)
        {
            string requestUID = GetOFSRequestID(shortRequest);

            _ErrorID++;
            _ErrorXMLTextWriter.WriteRaw("\n\t");
            _ErrorXMLTextWriter.WriteStartElement(AXMLConstants.ERROR_TAG);
            _ErrorXMLTextWriter.WriteAttributeString(AXMLConstants.ERRORSET_ATTRIBUTE_ID, _ErrorID.ToString());
            _ErrorXMLTextWriter.WriteAttributeString(AXMLConstants.ERRORSET_ATTRIBUTE_TYPE, errType.ToString());

            _ErrorXMLTextWriter.WriteAttributeString(AXMLConstants.GENERATOR_ID_TAG, _GeneratorIndex.ToString());


            _ErrorXMLTextWriter.WriteAttributeString(AXMLConstants.REQUEST_ATTRIBUTE_ID, requestUID);

            _ErrorXMLTextWriter.WriteAttributeString(AXMLConstants.REQUEST_EXECUTION_STATE, firstState.ToString());

            if (errorState != GlobusRequest.ErrorState.Undefined)
            {
                _ErrorXMLTextWriter.WriteAttributeString(AXMLConstants.REQUEST_AFTER_EXECUTION_STATE,
                                                         secondState.ToString());
            }
            else
            {
                _ErrorXMLTextWriter.WriteAttributeString(AXMLConstants.REQUEST_AFTER_EXECUTION_STATE, String.Empty);
            }

            _ErrorXMLTextWriter.WriteAttributeString(AXMLConstants.REQUEST_ERROR_STATE, errorState.ToString());

            _ErrorXMLTextWriter.WriteAttributeString(AXMLConstants.OFS_GLOBUS_ID, shortRequest.GlobusID);

            _ErrorXMLTextWriter.WriteString(errorMessage);

            _ErrorXMLTextWriter.WriteEndElement();
        }

        private void AddInvalidRowXML(GlobusRequestShort shortRequest, GlobusMessage globusMessage,
                                      GlobusRequest.ExecutionState firstState, GlobusRequest.ExecutionState secondState)
        {
            string errorMessage = globusMessage.Message.Text;
            AddInvalidRowXML(shortRequest, errorMessage, firstState, secondState, GlobusRequest.ErrorState.Globus);
        }

        /// <summary>
        /// Check the state of process(Finished). Yes - instance is for writing  
        /// </summary>
        /// <param name="shortRequest"></param>
        /// <returns></returns>
        public static bool IsForVXMLWriting(GlobusRequestShort shortRequest)
        {
            GlobusRequest.ExecutionState state = shortRequest.ExecutionState; 
            GlobusRequest.RequestType type = shortRequest.RequestType;



            return IsForVXMLWriting(state, type);
        }

        private static bool IsForVXMLWriting(GlobusRequest.ExecutionState state, GlobusRequest.RequestType type)
        {
            switch (type)
            {
                case GlobusRequest.RequestType.Enquiry:
                case GlobusRequest.RequestType.History:
                    //TODO: ...
                    return false;
                case GlobusRequest.RequestType.Application:
                    //TODO: ...
                    return false;
                case GlobusRequest.RequestType.AAProduct:
                case GlobusRequest.RequestType.DataProcess:
                    return (state == GlobusRequest.ExecutionState.Finished);
            }

            System.Diagnostics.Debug.Fail("Are you missing somethig?");
            return false;
        }

        public static bool IsForVXMLWriting(GlobusRequest request)
        {
            GlobusRequest.ExecutionState state = request.State; 
            GlobusRequest.RequestType type = request.TypeOfRequest;
            return IsForVXMLWriting(state, type);
        }

        /// <summary>
        /// Execution - Reading Globus messages from DataOUT and precessed them
        /// </summary>
        public bool DoOFSInstanceSecond()
        {
            bool continueProcessing = true;

            _CurrentStep1TimeOut = DateTime.Now.AddMilliseconds(TimeoutStep1).Ticks;

            while (continueProcessing && _CurrentStep1TimeOut > DateTime.Now.Ticks)
            {
                continueProcessing = false;

                bool noRecordsProcessed = true;
                for (int i = 0; i < _ProcessedInstances.Count; i++)
                {
                    GlobusRequestShort requestShort = _ProcessedInstances[i];

                    //requestShort.DataProcessType == DataProcessType.Authorize

                    if ((!requestShort.Failed)
                        && (requestShort.ExecutionState != GlobusRequest.ExecutionState.Finished)
                        && 
                        ((requestShort.DataProcessType == DataProcessType.Authorize 
                                && requestShort.ExecutionState == GlobusRequest.ExecutionState.WaitingForAuthorization)
                              ||
                             (requestShort.DataProcessType != DataProcessType.Authorize 
                                && requestShort.ExecutionState != GlobusRequest.ExecutionState.WaitingForAuthorization))
                        )
                    {
                        noRecordsProcessed = false;

                        string requestGUID = requestShort.GUID;
                        GlobusMessage globusMessage;
                        if ((requestShort.DataProcessType != DataProcessType.Authorize) &&
                             (requestShort.ExecutionState != GlobusRequest.ExecutionState.WaitingForAuthorization) )
                        {   
                            globusMessage = _MessageQueue.DataMessageRead(requestShort, requestGUID);

                            if (globusMessage != null)
                            {
                                ProcessInsertedInstance(i, requestShort, globusMessage);
                            }
                            else
                            {
                                continueProcessing = true;
                            }
                        }
                        else if ((requestShort.DataProcessType == DataProcessType.Authorize) &&
                                 (requestShort.ExecutionState == GlobusRequest.ExecutionState.WaitingForAuthorization))
                        {
                            globusMessage = _MessageQueue.AuthorizeMessageRead(requestShort, requestGUID);

                            if (globusMessage != null)
                            {
                                ProcessOnlyAutorizedInstance(i, requestShort, globusMessage);
                            }
                            else
                            {
                                continueProcessing = true;
                            }
                        }

                        globusMessage = null;
                        requestGUID = null;
                    }

                    requestShort = null;
                }

                if (noRecordsProcessed)
                {
                    break;
                }
                else
                {
                    Thread.Sleep(SleepInMillisecondsBeforeRetry);
                }
            }

            bool hasUnprocessed = WriteUnprocessedInstances(GlobusRequest.ExecutionState.WaitingForProcessing);
            return !hasUnprocessed;
        }

        private void ProcessOnlyAutorizedInstance(int i, GlobusRequestShort requestShort, GlobusMessage globusMessage)
        {
            string logGlobusMsg = string.Empty;
           

            _CurrentStep1TimeOut = DateTime.Now.AddMilliseconds(TimeoutStep1).Ticks;

            GlobusRequest globusRequest = new GlobusRequest(
                    requestShort, _OFSParameters.ResponseTypical, _OFSParameters.Version, _OFSParameters.Company);

            var resultTxt = requestShort.SetResponse(
                globusRequest, 
                globusMessage, 
                _TreatOFSResponseSingleValueAsError,
                _OfsType);

            if (!requestShort.Failed)
            {
                RecreateGlobusRequestInstance(globusRequest);
                WritingVXMLInstance(requestShort, globusRequest.Result);
            }
            else
            {
               AddInvalidRowXML(requestShort, resultTxt,
                                 GlobusRequest.ExecutionState.WaitingForProcessing,
                                 GlobusRequest.ExecutionState.WaitingForAuthorization, GlobusRequest.ErrorState.Globus);

                // Log the error instead of the message text
                logGlobusMsg = resultTxt;
            }


            if (!string.IsNullOrEmpty(logGlobusMsg))
                LogGlobusOFSMessage(logGlobusMsg, requestShort, GlobusRequest.ExecutionState.Finished);
            else
                LogGlobusOFSMessage(globusMessage, requestShort, GlobusRequest.ExecutionState.Finished);


            _ProcessedInstances[i] = requestShort;

            globusRequest = null;
        }


        private void ProcessInsertedInstance(int i, GlobusRequestShort requestShort, GlobusMessage globusMessage)
        {
            string logGlobusMsg = string.Empty;

            _CurrentStep1TimeOut = DateTime.Now.AddMilliseconds(TimeoutStep1).Ticks;

            if (requestShort.ExecutionState == GlobusRequest.ExecutionState.WaitingForProcessing)
            {
                GlobusRequest globusRequest = new GlobusRequest(
                    requestShort, _OFSParameters.ResponseTypical, _OFSParameters.Version, _OFSParameters.Company);

                var resultTxt = requestShort.SetResponse(
                    globusRequest, 
                    globusMessage, 
                    _TreatOFSResponseSingleValueAsError,
                    _OfsType);

                if (!requestShort.Failed)
                {
                    RecreateGlobusRequestInstance(globusRequest);
                    try
                    {
                        if (_IsIAMode)
                        {
                            CreateFileWithoutRegisteringRequest(globusRequest, true, requestShort);

                            requestShort.SetShortResponseProperties(globusRequest);

                            string requestUID = GetOFSRequestID(requestShort);

                            LogValidataOFSCommand(globusRequest.FirstMessage, requestUID, requestShort,
                                                  GlobusRequest.ExecutionState.WaitingForAuthorization);
                        }
                        else
                        {
                            requestShort.ExecutionState = GlobusRequest.ExecutionState.Finished;
                        }

                        WritingVXMLInstance(requestShort, globusRequest.Result);
                    }
                    catch (Exception ex)
                    {
                        AddInvalidRowXML(requestShort, ex.Message,
                                         GlobusRequest.ExecutionState.WaitingForProcessing,
                                         GlobusRequest.ExecutionState.WaitingForAuthorization,
                                         GlobusRequest.ErrorState.Undefined);

                        // Log the error instead of the message text
                        logGlobusMsg = ex.Message;

                    }
                }
                else
                {
                    // in case of Failed resultTxt contains the error message from Globus                    

                    AddInvalidRowXML(requestShort, resultTxt,
                                     GlobusRequest.ExecutionState.WaitingForProcessing,
                                     GlobusRequest.ExecutionState.WaitingForAuthorization, GlobusRequest.ErrorState.Globus);

                    logGlobusMsg = resultTxt;
                }

                requestShort.ExecutionState = GlobusRequest.ExecutionState.WaitingForAuthorization;

                _ProcessedInstances[i] = requestShort;

                globusRequest = null;

            }

            if (!string.IsNullOrEmpty (logGlobusMsg))
                LogGlobusOFSMessage(logGlobusMsg, requestShort, GlobusRequest.ExecutionState.WaitingForAuthorization);
            else
                LogGlobusOFSMessage(globusMessage, requestShort, GlobusRequest.ExecutionState.WaitingForAuthorization);
        }

        public bool DoOFSInstanceThird()
        {
            bool continueProcessing = true;

            _CurrentStep1TimeOut = DateTime.Now.AddMilliseconds(TimeoutStep1).Ticks;

            while (continueProcessing && _CurrentStep1TimeOut > DateTime.Now.Ticks)
            {
                continueProcessing = false;
                bool noRecordsProcessed = true;
                for (int i = 0; i < _ProcessedInstances.Count; i++)
                {
                    GlobusRequestShort requestShort = _ProcessedInstances[i];

                    if ((!requestShort.Failed)
                        && (requestShort.ExecutionState != GlobusRequest.ExecutionState.Finished))
                    {
                        noRecordsProcessed = false;

                        string requestGUID = requestShort.GUID;

                        GlobusMessage globusMessage = _MessageQueue.AuthorizeMessageRead(requestShort, requestGUID);

                        if (globusMessage != null)
                        {
                            string logGlobusMsg = string.Empty;

                            _CurrentStep1TimeOut = DateTime.Now.AddMilliseconds(TimeoutStep1).Ticks;

                            if (requestShort.ExecutionState == GlobusRequest.ExecutionState.WaitingForAuthorization)
                            {
                                GlobusRequest globusRequest = new GlobusRequest(
                                        requestShort, _OFSParameters.ResponseTypical,
                                        _OFSParameters.Version, _OFSParameters.Company
                                    );

                               var resultText = requestShort.SetResponse(
                                    globusRequest, 
                                    globusMessage, 
                                    _TreatOFSResponseSingleValueAsError,
                                    _OfsType);

                                if (!requestShort.Failed)
                                {
                                    RecreateGlobusRequestInstance(globusRequest);

                                    requestShort.SetShortResponseProperties(globusRequest);

                                    //ShortRequestChangeExecutionState(requestShort);

                                    WritingVXMLInstance(requestShort, globusRequest.Result);
                                }
                                else
                                {
                                    AddInvalidRowXML(requestShort, resultText,
                                                     GlobusRequest.ExecutionState.WaitingForAuthorization,
                                                     GlobusRequest.ExecutionState.Finished, GlobusRequest.ErrorState.Globus);

                                    logGlobusMsg = resultText;
                                }

                                requestShort.ExecutionState = GlobusRequest.ExecutionState.Finished;

                                _ProcessedInstances[i] = requestShort;
                                globusRequest = null;
                            }

                            if (!string.IsNullOrEmpty(logGlobusMsg))
                                LogGlobusOFSMessage(logGlobusMsg, requestShort, GlobusRequest.ExecutionState.Finished);
                            else
                                LogGlobusOFSMessage(globusMessage, requestShort, GlobusRequest.ExecutionState.Finished);

                        }
                        else
                        {
                            continueProcessing = true;
                        }

                        globusMessage = null;
                        requestGUID = null;
                    }

                    requestShort = null;
                }

                if (noRecordsProcessed)
                {
                    break;
                }
                else
                {
                    Thread.Sleep(SleepInMillisecondsBeforeRetry);
                }
            }

            bool hasUnprocessed = WriteUnprocessedInstances(GlobusRequest.ExecutionState.WaitingForAuthorization);
            return !hasUnprocessed;
        }

        /// <summary>
        /// Process and write to file unprocessed(timeout) instances
        /// </summary>
        /// <param name="executionState"></param>
        /// <returns>True - if has unprocessed requests</returns>
        public bool WriteUnprocessedInstances(GlobusRequest.ExecutionState executionState)
        {
            bool hasUnprocessed = false;

            for (int i = 0; i < _ProcessedInstances.Count; i++)
            {
                GlobusRequestShort shortRequest = _ProcessedInstances[i];

                if (WriteUnprocessedRequest(executionState, shortRequest))
                {
                    shortRequest.Failed = true;
                    _ProcessedInstances[i] = shortRequest;

                    hasUnprocessed = true;
                }
            }

            return hasUnprocessed;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="executionState"></param>
        /// <param name="shortRequest"></param>
        /// <returns>True - if has unprocessed requests</returns>
        public bool WriteUnprocessedRequest(GlobusRequest.ExecutionState executionState, GlobusRequestShort shortRequest)
        {
            if (!(shortRequest.Failed) && shortRequest.ExecutionState == executionState)
            {
                string requestID = GetOFSRequestID(shortRequest);

                string errorForNotProcessedMessages = "Time-out waiting for T24 response!";

                AddInvalidRowXML(shortRequest, errorForNotProcessedMessages, executionState, executionState,
                                 GlobusRequest.ErrorState.TimeOut, Error.ErrorTypes.Critical);

                shortRequest.Failed = true;

                return true;
            }

            return false;
        }

        public void RecreateGlobusRequestInstance(GlobusRequest globusRequest)
        {
            globusRequest.Instance = Instance.FromXmlString(globusRequest.Result);
        }

        /// <summary>
        /// Generate error file
        /// </summary>
        /// <param name="errorFilePath"></param>
        /// <param name="errorTempFilePath"></param>
        /// <param name="ofsValidataMessageFilePath"></param>
        /// <param name="ofsGlobusMessageFilePath"></param>
        /// <param name="exceptionMessage"></param>
        public static void GenerateErrorFile(string errorFilePath,
            string errorTempFilePath, string ofsValidataMessageFilePath,
            string ofsGlobusMessageFilePath, string exceptionMessage)
        {
            using (XmlWriter xmlWriter = new SafeXmlTextWriter(errorFilePath, Encoding.UTF8))
            {
                GenerateErrorFile(errorTempFilePath, ofsValidataMessageFilePath, ofsGlobusMessageFilePath, exceptionMessage, null, xmlWriter);
            }
        }

        public static void GenerateErrorFile(string errorTempFilePath, string ofsValidataMessageFilePath, string ofsGlobusMessageFilePath
            , string exceptionMessage, string errorMessage, XmlWriter xmlWriter)
        {
            if ((!String.IsNullOrEmpty(errorTempFilePath) && File.Exists(errorTempFilePath))
                && (!String.IsNullOrEmpty(ofsValidataMessageFilePath) && File.Exists(ofsValidataMessageFilePath))
                && (!String.IsNullOrEmpty(ofsGlobusMessageFilePath) && File.Exists(ofsGlobusMessageFilePath)))
            {
                GenerateAndWriteError(errorTempFilePath, ofsValidataMessageFilePath, ofsGlobusMessageFilePath,
                                      xmlWriter);
            }
            else if (exceptionMessage != null && exceptionMessage != String.Empty)
            {
                StringBuilder sb = new StringBuilder();

                CreateErrorXML(sb, String.Empty, exceptionMessage, String.Empty);
                WriteErrorRow(xmlWriter, sb.ToString());
            }
            else if (errorMessage != null && errorMessage != String.Empty)
            {

                WriteErrorRow(xmlWriter, errorMessage);
            }

        }

        /// <summary>
        /// Generates the critical error file.
        /// </summary>
        /// <param name="errorFilePath">The error file path.</param>
        /// <param name="message">The message.</param>
        public static void GenerateCriticalErrorFile(string errorFilePath, string message)
        {
            using (XmlWriter xmlWriter = new SafeXmlTextWriter(errorFilePath, Encoding.UTF8))
            {
                xmlWriter.WriteRaw("\n\t");
                xmlWriter.WriteStartElement(AXMLConstants.ERROR_TAG);
                xmlWriter.WriteAttributeString(AXMLConstants.ERRORSET_ATTRIBUTE_ID, 0.ToString());
                xmlWriter.WriteAttributeString(AXMLConstants.ERRORSET_ATTRIBUTE_TYPE, Error.ErrorTypes.Critical.ToString());
                xmlWriter.WriteString(message);

                xmlWriter.WriteEndElement();
            }
        }


        /// <summary>
        /// Creates the error XML.
        /// </summary>
        /// <param name="sbError">The sb error.</param>
        /// <param name="globusId">The globus id.</param>
        /// <param name="ofsError">The ofs error.</param>
        /// <param name="ofsSource">The ofs source.</param>
        public static void CreateErrorXML(StringBuilder sbError, string globusId, string ofsError, string ofsSource)
        {
            sbError.Append("<error>");

            // @ID if present
            sbError.Append("@ID=[");
            sbError.Append(Utils.GetHtmlEncodedValue(globusId));
            sbError.Append("] ");

            // Error message (it is already HTML encoded)
            sbError.Append("Error=[");
            sbError.Append(Utils.GetHtmlEncodedValue(ofsError));
            sbError.Append("] ");

            // OFS Message
            sbError.Append("OFS=[");
            sbError.Append(Utils.GetHtmlEncodedValue(ofsSource));
            sbError.Append("]");

            sbError.Append("</error>");
        }

        /// <summary>
        /// Generates and write complete error.
        /// </summary>
        /// <param name="errorTempFilePath">The error temp file path.</param>
        /// <param name="ofsValidataMessageFilePath">The OFS validata message file path.</param>
        /// <param name="ofsGlobusMessageFilePath">The OFS globus message file path.</param>
        /// <param name="xmlErrosWriter">The XML erros writer.</param>
        public static void GenerateAndWriteError(string errorTempFilePath, string ofsValidataMessageFilePath,
                                                 string ofsGlobusMessageFilePath, XmlWriter xmlErrosWriter)
        {
            XmlReaderSettings settings = XmlFragmentLogHelper.GetSettings();
            XmlReader xmlReaderValidataOFS = XmlReader.Create(ofsValidataMessageFilePath, settings);

            foreach (OFSDetailedError error in OFSDetailedErrorReader.EnumerateAllItems(errorTempFilePath))
            {
                string errorXML;

                if (xmlReaderValidataOFS.Read())
                {
                    errorXML = ConstructErrorXmlFragment(error, xmlReaderValidataOFS);
                }
                else
                {
                    // try one time...
                    xmlReaderValidataOFS.ReadOuterXml();
                    if (xmlReaderValidataOFS.EOF)
                    {
                        xmlReaderValidataOFS = RecreateReader(xmlReaderValidataOFS);
                    }

                    if (xmlReaderValidataOFS.Read())
                    {
                        errorXML = ConstructErrorXmlFragment(error, xmlReaderValidataOFS);
                    }
                    else
                    {
                        errorXML = ConstructErrorXmlFragment(error, xmlReaderValidataOFS);
                    }
                }

                WriteErrorRow(xmlErrosWriter, errorXML);
            }

            xmlReaderValidataOFS.Close();
        }

        /// <summary>
        /// Write row error data
        /// </summary>
        /// <param name="xmlWriter"></param>
        /// <param name="errorXML"></param>
        /// <returns></returns>
        public static string WriteErrorRow(XmlWriter xmlWriter, string errorXML)
        {
            errorXML = errorXML.Replace("\n", String.Empty);
            xmlWriter.WriteRaw(errorXML);
            xmlWriter.WriteRaw("\n\t");
            return errorXML;
        }

        /// <summary>
        /// Gets the error attributes.
        /// </summary>
        /// <param name="errorID">The error ID.</param>
        /// <param name="generatorID">The generator ID.</param>
        /// <param name="requestID">The request ID.</param>
        /// <param name="requestExeState">State of the request exe.</param>
        /// <param name="requestAfterExeState">State of the request after exe.</param>
        /// <param name="requestErrorState">State of the request error.</param>
        /// <param name="globusID">The globus ID.</param>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
        public static string GetErrorAttributes(
            out string errorID, out string generatorID, out string requestID, 
            out string requestExeState, out string requestAfterExeState, 
            out string requestErrorState, out string globusID, XmlReader reader)
        {
            errorID = reader.GetAttribute(AXMLConstants.ERRORSET_ATTRIBUTE_ID);
            generatorID = reader.GetAttribute(AXMLConstants.GENERATOR_ID_TAG);
            requestID = reader.GetAttribute(AXMLConstants.REQUEST_ATTRIBUTE_ID);
            requestExeState = reader.GetAttribute(AXMLConstants.REQUEST_EXECUTION_STATE);
            requestAfterExeState = reader.GetAttribute(AXMLConstants.REQUEST_AFTER_EXECUTION_STATE);
            requestErrorState = reader.GetAttribute(AXMLConstants.REQUEST_ERROR_STATE);
            globusID = reader.GetAttribute(AXMLConstants.OFS_GLOBUS_ID);
            return reader.ReadInnerXml();
        }

        #endregion

        #region Private methods


        /// <summary>
        /// Constructs the error XML.
        /// </summary>
        /// <returns></returns>
        private static string ConstructErrorXmlFragment(OFSDetailedError error, XmlReader readerValidataOFS)
        {
            StringBuilder sbError = new StringBuilder();

            string globusId = error.GlobusID;

            if (readerValidataOFS != null
                && error.RequestErrorState != String.Empty
                && error.RequestErrorState != GlobusRequest.ErrorState.Undefined.ToString())
            {
                string valGeneratorID;
                string valRequestID;
                string valRequestExeState;

                string ofsSource = GetOFSValidata(error.GeneratorID, error.RequestID, error.RequestExeState, readerValidataOFS,
                                                  out valGeneratorID, out valRequestID, out valRequestExeState,
                                                  ref globusId, true);

                ofsSource = ofsSource.Replace("\n", String.Empty);

                CreateErrorXML(sbError, globusId, error.ErrorMessage, ofsSource);
            }
            else
            {
                CreateErrorXML(sbError, globusId, error.ErrorMessage, String.Empty);
            }

            return sbError.ToString();
        }

        /// <summary>
        /// Gets the OFS validata.
        /// </summary>
        /// <param name="errorGeneratorID">The error generator ID.</param>
        /// <param name="errorRequestID">The error request ID.</param>
        /// <param name="errorRequestExeState">State of the error request exe.</param>
        /// <param name="reader">The reader.</param>
        /// <param name="generatorID">The generator ID.</param>
        /// <param name="requestID">The request ID.</param>
        /// <param name="requestExeState">State of the request exe.</param>
        /// <param name="globusID">The globus ID.</param>
        /// <param name="readAgainXML">if set to <c>true</c> [read again XML].</param>
        /// <returns></returns>
        public static string GetOFSValidata(string errorGeneratorID, string errorRequestID, string errorRequestExeState,
                                            XmlReader reader, out string generatorID, out string requestID,
                                            out string requestExeState, ref string globusID, bool readAgainXML)
        {
            string resultMessage = String.Empty;

            bool readNext = true;

            generatorID = String.Empty;
            requestID = String.Empty;
            requestExeState = String.Empty;

            while (readNext)
            {
                reader.MoveToContent();
                generatorID = reader.GetAttribute(AXMLConstants.GENERATOR_ID_TAG);
                requestID = reader.GetAttribute(AXMLConstants.REQUEST_ATTRIBUTE_ID);
                requestExeState = reader.GetAttribute(AXMLConstants.REQUEST_EXECUTION_STATE);

                if (globusID == String.Empty || globusID == "No Value")
                {
                    globusID = reader.GetAttribute(AXMLConstants.OFS_GLOBUS_ID);
                }

                if (errorGeneratorID == generatorID
                    && errorRequestID == requestID
                    && errorRequestExeState == requestExeState
                    && reader.Read())
                {
                    readNext = false;
                    //resultMessage = reader.ReadInnerXml();

                    resultMessage = reader.ReadContentAsString();
                }
                else
                {
                    reader.ReadOuterXml();
                    if (reader.EOF)
                    {
                        readNext = false;
                        reader = RecreateReader(reader);

                        if (readAgainXML)
                        {
                            readAgainXML = false;

                            resultMessage = GetOFSValidata(errorGeneratorID, errorRequestID, errorRequestExeState,
                                                           reader, out generatorID, out requestID, out requestExeState,
                                                           ref globusID, readAgainXML);
                        }
                    }
                }
            }

            return resultMessage;
        }

        private static XmlReader RecreateReader(XmlReader reader)
        {
            string pathToErrorFile = reader.BaseURI;
            return XmlReader.Create(pathToErrorFile, reader.Settings);
        }

        #endregion
    }
}