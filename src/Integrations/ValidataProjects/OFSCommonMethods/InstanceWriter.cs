using System;
using System.IO;
using System.Xml;
using AXMLEngine;

namespace OFSCommonMethods
{
    public class InstanceWriter
    {
        private string _InstanceVXMLFilePath = String.Empty;

        public string InstanceVXMLFilePath
        {
            get { return _InstanceVXMLFilePath; }
            set { _InstanceVXMLFilePath = value; }
        }

        public void AddXMLInstances(object dataset, WriteInstancesEventArgs args)
        {
            if (dataset.GetType() == typeof (AXMLDataset))
            {
                if (_InstanceVXMLFilePath != String.Empty)
                {
                    if (File.Exists(_InstanceVXMLFilePath))
                    {
                        XmlReaderSettings settings = new XmlReaderSettings();
                        settings.ConformanceLevel = ConformanceLevel.Fragment;
                        settings.IgnoreWhitespace = true;
                        settings.IgnoreComments = true;

                        using (XmlReader reader = XmlReader.Create(_InstanceVXMLFilePath, settings))
                        {
                            if (reader.Read())
                            {
                                while (true)
                                {
                                    reader.MoveToContent();

                                    args.xmlWriter.WriteRaw("\n\t");
                                    //args.xmlWriter.WriteRaw(reader.ReadOuterXml());
                                    args.xmlWriter.WriteRaw(reader.ReadInnerXml());
                                    if (reader.EOF)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}