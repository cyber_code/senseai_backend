using System;
using System.Collections;

namespace OFSCommonMethods
{
    /// <summary>
    /// This class keeps and organizes the access to a collection of Globus requests.
    /// </summary>
    public class GlobusRequestCollection : IList
    {
        #region Private members

        /// <summary>
        /// An array of all requests
        /// </summary>
        private ArrayList _Requests = new ArrayList();

        /// <summary>
        /// A hashtable allowing to search for a request with the ide of the message that
        /// this request created.
        /// </summary>
        private Hashtable _ByMessageFileID = new Hashtable();

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Creates an instance of the GlobusRequestCollection class.
        /// </summary>
        public GlobusRequestCollection()
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Adds a GlobusRequest to this collection
        /// </summary>
        /// <param name="req">The GlobusRequest object.</param>
        /// <returns>The index of the GlobusRequest in the request array.</returns>
        public int AddGlobusRequest(GlobusRequestBase req)
        {
            int idx = _Requests.Add(req);
            string fileID = req.MessageFileID;

            if (fileID != "")
            {
                _ByMessageFileID.Add(fileID, req);
            }

            return idx;
        }

        /// <summary>
        /// Gets the count of requests in this collection that still have to be processed.
        /// </summary>
        /// <returns>The number of pending requests.</returns>
        public int GetBeingProcessedCount()
        {
            int count = 0;
            for (int i = 0; i < _Requests.Count; i++)
            {
                GlobusRequestBase request = (GlobusRequestBase)_Requests[i];
                if (request.WaitingForProcessing || request.WaitingForAuthorization)
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// Attaches a time-out error result to all pending requests.
        /// </summary>
        public void SetResultsForNotPassedMessages()
        {
            for (int i = 0; i < _Requests.Count; i++)
            {
                GlobusRequestBase request = (GlobusRequestBase)_Requests[i];
                if (request.WaitingForProcessing || request.WaitingForAuthorization)
                {
                    request.Result = "Time-out waiting for T24 response! ";
                    request.SetFinished();
                    request.Failed = true;
                }
            }
        }

        public void SetResultsForFailed(string errorMsg)
        {
            for (int i = 0; i < _Requests.Count; i++)
            {
                GlobusRequestBase request = (GlobusRequestBase)_Requests[i];
                request.Result = errorMsg;
                request.SetFinished();
                request.Failed = true;
            }
        }


        /// <summary>
        /// Finds the request in this collection for which this message belongs using the message ID.
        /// </summary>
        /// <param name="fileID">The ID of the message.</param>
        /// <returns>The Globus request corresponding to this message or null.</returns>
        public GlobusRequestBase ExtractByMessageFileID(string fileID)
        {
            lock (_ByMessageFileID.SyncRoot)
            {
                if (_ByMessageFileID.ContainsKey(fileID))
                {
                    GlobusRequestBase req = (GlobusRequestBase)_ByMessageFileID[fileID];

                    //remove it from the register
                    _ByMessageFileID.Remove(fileID);

                    return req;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Registers a Globus request as being processed.
        /// </summary>
        /// <param name="req">The GlobusRequestBase object.</param>
        public void RegisterAsBeingProcessed(GlobusRequestBase req)
        {
            _ByMessageFileID[req.MessageFileID] = req;
        }

        #endregion

        #region IList Members

        /// <summary>
        /// Gets a value indicating whether the Requests List is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get { return true; }
        }

        /// <summary>
        /// An indexer returning the GlobusRequestBase with the given index.
        /// </summary>
        public object this[int index]
        {
            get { return _Requests[index]; }
            set { }
        }

        /// <summary>
        /// Removes the Globus Request with the given index
        /// </summary>
        /// <param name="index">The index of the request</param>
        public void RemoveAt(int index)
        {
            // TODO:  Add GlobusRequestCollection.RemoveAt implementation
        }

        /// <summary>
        /// Inserts the request at the given index.
        /// </summary>
        /// <param name="index">The index where to insert the request</param>
        /// <param name="value">The GlobusRequestBase instance.</param>
        public void Insert(int index, object value)
        {
            // TODO:  Add GlobusRequestCollection.Insert implementation
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the Requests List
        /// </summary>
        /// <param name="value">The object to remove from the Requests List. The value can be a null reference (Nothing in Visual Basic)</param>
        public void Remove(object value)
        {
            // TODO:  Add GlobusRequestCollection.Remove implementation
        }

        /// <summary>
        /// Determines whether an element is in the Requests List
        /// </summary>
        /// <param name="value">The object to locate in the Requests List. The value can be a null reference (Nothing in Visual Basic)</param>
        /// <returns>true if item is found in the Requests List; otherwise, false</returns>
        public bool Contains(object value)
        {
            return false;
        }

        /// <summary>
        /// Removes all elements from the Requests List
        /// </summary>
        public void Clear()
        {
            _Requests.Clear();
            _ByMessageFileID.Clear();
        }

        /// <summary>
        /// Returns the zero-based index of the first occurrence of a value in 
        /// the Requests List 
        /// </summary>
        /// <param name="value">The Object to locate in the Requests List. The value can be a null reference (Nothing in Visual Basic)</param>
        /// <returns>The zero-based index of the first occurrence of value within the entire Requests List, if found; otherwise, -1</returns>
        public int IndexOf(object value)
        {
            return _Requests.IndexOf(value);
        }

        /// <summary>
        /// Adds an object to the end of the Requests List
        /// </summary>
        /// <param name="value">The Object to be added to the end of the Requests List. The value can be a null reference (Nothing in Visual Basic)</param>
        /// <returns>The Requests List index at which the value has been added.</returns>
        public int Add(object value)
        {
            if (value is GlobusRequestBase)
            {
                return AddGlobusRequest((GlobusRequestBase)value);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Requests List has a fixed size
        /// </summary>
        public bool IsFixedSize
        {
            get { return true; }
        }

        #endregion

        #region ICollection Members

        /// <summary>
        /// Gets a value indicating whether access to the Requests List is synchronized (thread-safe)
        /// </summary>
        public bool IsSynchronized
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the number of elements actually contained in the Requests List
        /// </summary>
        public int Count
        {
            get { return _Requests.Count; }
        }

        /// <summary>
        /// Copies the Requests List or a portion of it to a one-dimensional array
        /// </summary>
        /// <param name="array">The one-dimensional Array that is the destination of the elements copied from ArrayList. The Array must have zero-based indexing</param>
        /// <param name="index">The zero-based index in array at which copying begins</param>
        public void CopyTo(Array array, int index)
        {
            if (ReferenceEquals(array, null))
            {
                throw new ArgumentNullException("array", "Null array reference");
            }

            if (index < 0)
            {
                throw new ArgumentOutOfRangeException("index", "Index is out of range");
            }

            if (array.Rank > 1)
            {
                throw new ArgumentException("array", "Array is multi-dimensional");
            }

            foreach (object o in _Requests)
            {
                array.SetValue(o, index);
                index++;
            }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the Requests List
        /// </summary>
        public object SyncRoot
        {
            get { return null; }
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Gets an enumerator for the Globus requests.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            // TODO:  Add GlobusRequestCollection.GetEnumerator implementation
            return _Requests.GetEnumerator();
        }

        #endregion
    }
}