﻿using System;
using System.Collections.Generic;
using System.Linq;
using AXMLEngine;

namespace OFSCommonMethods
{
    public class GlobusDMD
    {
        public string Name;
        public string ApplicationName;
        public int IDPosition;
        public SortedList<int, GlobusDMDAttributeMetadata> Attributes = new SortedList<int, GlobusDMDAttributeMetadata>();

        public static GlobusDMD FromInstance(Instance instance)
        {
            var result = new GlobusDMD
                             {
                                 Name = instance.GetAttributeValue("@ID"),
                                 ApplicationName = instance.GetAttributeValue("APPLICATION.NAME-1~1")
                               
                             };

            if (!int.TryParse(instance.GetAttributeValue("ID.POSITION-1~1"), out result.IDPosition))
            {
                //TODO check if ID.TYPE=’AUTO’ 
                result.IDPosition = 2;
            }

            var fields = new SortedList<int, GlobusDMDAttributeMetadata>();

            foreach (var attributeName in instance.GetAttributeNames())
            {
                if (attributeName.Contains("FIELD.POSITION"))
                {
                    var fieldIndex = int.Parse(attributeName.Substring(15, attributeName.Length - (15 + attributeName.Length - attributeName.IndexOf("~", System.StringComparison.Ordinal))));

                    string fieldNameWithIndex = instance.GetAttributeValue("APPL.FIELD.NAME-" + fieldIndex + "~1") == string.Empty 
                        ? instance.GetAttributeValue("APPL-" + fieldIndex + "~1") 
                        : instance.GetAttributeValue("APPL.FIELD.NAME-" + fieldIndex + "~1");

                    if (string.IsNullOrEmpty(fieldNameWithIndex))
                        continue;

                    int possition;

                    if (!int.TryParse(instance.GetAttributeValue(attributeName), out possition))
                    {
                        throw new Exception(string.Format("Attribute [{0}] has not numeric value: [{1}]! ", attributeName, instance.GetAttributeValue(attributeName)));
                    }

                    if (!fields.ContainsKey(possition))
                        fields.Add(possition, new GlobusDMDAttributeMetadata
                                       {
                                           FieldName = fieldNameWithIndex,
                                           FieldPosition = possition
                                       });
                    }
            }

            if (!fields.ContainsKey(result.IDPosition))
            {
                fields.Add(result.IDPosition, new GlobusDMDAttributeMetadata { FieldPosition = result.IDPosition, FieldName = "T24_ID" });
            }

            List<int> gaps = Enumerable.Range(fields.First().Key, fields.Last().Key - fields.First().Key + 1).Except(fields.Keys).ToList();

            foreach(var gap in gaps)
            {
                fields.Add(gap, new GlobusDMDAttributeMetadata
                {
                    FieldName = string.Format("DUMMY_{0}", gap.ToString("D3")),
                    FieldPosition = gap
                });
            }

            foreach(var item in fields)
                result.Attributes.Add(item.Key, item.Value);

            var occurences = new Dictionary<string, int>();
            foreach (var item in result.Attributes)
            {
                var field = item.Value;

                int num;
                if(!occurences.TryGetValue(field.FieldName, out num))
                {
                    occurences[field.FieldName] = 1;
                }
                else
                {
                    occurences[field.FieldName] = num + 1;
                    field.FieldName = string.Format("{0}_{1}", field.FieldName, num);
                }
            }

            return result;
        }
    }
}