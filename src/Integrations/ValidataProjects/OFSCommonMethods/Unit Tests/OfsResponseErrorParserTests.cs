﻿using NUnit.Framework;

namespace OFSCommonMethods.Unit_Tests
{
    [TestFixture]
    internal class OfsResponseErrorParserTests
    {
        [Test]
        public void TestRulesLoading()
        {
            Assert.IsTrue(OfsResponseErrorParser.ParseRules.Count > 0);
        }
    }
}