﻿using System;
using NUnit.Framework;
using OFSCommonMethods.IO;
using OFSCommonMethods.IO.Telnet;

namespace OFSCommonMethods.Unit_Tests
{
    [TestFixture]
    public class TestTelnetCommunication
    {
        [Test]
        [Explicit]
        public void Test_()
        {
            Settings settings = new Settings();
            const string url = "10.10.10.100";
            const int port = 23;

            settings.GlobusUserName = "migvb01";
            settings.GlobusPassword = "123456";

            string[] commands = new string[]
                                    {
                                        "ENQUIRY.SELECT,,DIMITAR/123456,CURRENCY-LIST",
                                        "ENQUIRY.SELECT,,DIMITAR/123456,%CUSTOMER",
                                        "CUSTOMER,/S/PROCESS,DIMITAR/123456,@ID=100724",
                                        "CUSTOMER,/S/PROCESS,DIMITAR/123456,@ID=1"
                                    };
            try
            {
                Console.WriteLine("Connecting to T24 Telnet Service...");
                Console.WriteLine("\tIP: " + url + port.ToString());
                Console.WriteLine("\tACCOUNT" + settings.GlobusUserName);
                Console.WriteLine("\tPASSWORD" + settings.GlobusPassword);

                Console.WriteLine("DONE!");
                Console.WriteLine("");

                foreach (string command in commands)
                {
                    string response;
                    Console.WriteLine("Sending Command: " + command);
                    DateTime dtStart = DateTime.Now;

                    TSSTelnetComm comm = TelnetCommPool.GetCommunicator(url, port, settings, "UnitTest");
                    comm.SendCommand(command, out response);

                    Console.WriteLine("Response Received:");

                    if (response != null)
                    {
                        response = response.Trim();
                    }

                    Console.WriteLine(response);
                    Console.WriteLine("EXECUTION TIME: " + (DateTime.Now - dtStart).TotalMilliseconds.ToString());
                    Console.WriteLine("");

                    Assert.IsNotNull(response);
                    Assert.IsNotEmpty(response);
                }

                TelnetCommPool.Release(url, port, settings, "UnitTest"); 
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERRROR: " + ex.Message);
                Assert.Fail(ex.Message);
            }
        }

        [Test]
        public void Test_ParserServer()
        {
            Test_ParseServer("localhost:22", Settings.IOType.Telnet, "localhost", 22);
            Test_ParseServer("127.0.0.1:23", Settings.IOType.Telnet, "127.0.0.1", 23);
            Test_ParseServer("niki", Settings.IOType.Telnet, "niki", 23);
            Test_ParseServer("localhost", Settings.IOType.SharpSsH, "localhost", 22);
            Test_ParseServer("localhost", Settings.IOType.SSH, "localhost", 22);
            Test_ParseServer("10.10.10.12", Settings.IOType.Telnet, "10.10.10.12", 23);
            Test_ParseServer("10.10.10.12", Settings.IOType.SharpSsH, "10.10.10.12", 22);
            Test_ParseServer("10.10.10.12", Settings.IOType.SSH, "10.10.10.12", 22);
            Test_ParseServer("10.10.10.12:55", Settings.IOType.SharpSsH, "10.10.10.12", 55);
            Test_ParseServer("10.10.10.12:55", Settings.IOType.SSH, "10.10.10.12", 55);
        }

        private void Test_ParseServer(string ip, Settings.IOType ioType, string expectedUrl, int expectedPort)
        {
            string url;
            int port;
            string result = SocketBasedIOManager.ParseServer(ip, ioType, out url, out port);
            Assert.IsNull(result);
            Assert.IsTrue(url == expectedUrl);
            Assert.IsTrue(port == expectedPort);
        }

        [Test]
        public void TrySaveTelnetCommErrors()
        {
            TelnetCommErrors comms = new TelnetCommErrors();
            comms.Add(new HandledCommError());
            comms[0].ErrorMessage = "test";
            comms[0].RetryCount = 5;
            comms[0].RetryTimeout = 300000;
            comms.ToXmlFile();
        }
    }
}