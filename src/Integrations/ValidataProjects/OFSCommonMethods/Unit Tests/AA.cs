﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OFSCommonMethods.IO;
using System.IO;

namespace OFSCommonMethods.Unit_Tests
{
    [TestFixture]
    public class TestAA
    {
        private string SamplesResponsesDir;

        [TestFixtureSetUp]
        public void Setup()
        {
            var projectDirectory = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent;
            SamplesResponsesDir = projectDirectory.FullName + @"\Unit Tests\Sample Responses\";
        }

        [Test]
        public void Test_SplitProcessDetails()
        {
            Test_SplitProcessDetails(
                    "AA.ARR.CUSTOMER,AA.SIMPLE I AA10005Y9747-CUSTOMER-20100105.1 UPDATE",
                    "AA.ARR.CUSTOMER,AA.SIMPLE",
                    "I",
                    "AA10005Y9747-CUSTOMER-20100105.1",
                    "UPDATE"
                );

            Test_SplitProcessDetails(
                    "AA.ARR.ACCOUNT,AA.SIMPLE I AA10005Y9747-ACCOUNT-20100105.1 MAINTAIN",
                    "AA.ARR.ACCOUNT,AA.SIMPLE",
                    "I",
                    "AA10005Y9747-ACCOUNT-20100105.1",
                    "MAINTAIN"
                );

            Test_SplitProcessDetails(
                    "AA.ARR.PAYMENT.SCHEDULE,AA.SIMPLE M AA10005Y9747-DEPOSITS.SCHEDULE-20100105 DEPOSIT",
                    "AA.ARR.PAYMENT.SCHEDULE,AA.SIMPLE",
                    "M",
                    "AA10005Y9747-DEPOSITS.SCHEDULE-20100105",
                    "DEPOSIT"
                );

            Test_SplitProcessDetails(
                    "AA.ARR.ACTIVITY.MESSAGING M AA10005Y9747-MESSAGING-20100105 SEND.MESSAGE",
                    "AA.ARR.ACTIVITY.MESSAGING",
                    "M",
                    "AA10005Y9747-MESSAGING-20100105",
                    "SEND.MESSAGE"
                );

            Test_SplitProcessDetails(
                    "AA.ARR.PAYMENT.SCHEDULE,AA.SIMPLE I AA100066X9BZ-DEPOSITS.SCHEDULE-20100105.1 UPDATE AA.ARR.PAYMENT.SCHEDULE,AA.SIMPLE I AA100066X9BZ-DEPOSITS.SCHEDULE-20100106 UPDATE",
                    0,
                    "AA.ARR.PAYMENT.SCHEDULE,AA.SIMPLE",
                    "I",
                    "AA100066X9BZ-DEPOSITS.SCHEDULE-20100105.1",
                    "UPDATE"
                );

            Test_SplitProcessDetails(
                    "AA.ARR.PAYMENT.SCHEDULE,AA.SIMPLE I AA100066X9BZ-DEPOSITS.SCHEDULE-20100105.1 UPDATE AA.ARR.PAYMENT.SCHEDULE,AA.SIMPLE I AA100066X9BZ-DEPOSITS.SCHEDULE-20100106 UPDATE",
                    1,
                    "AA.ARR.PAYMENT.SCHEDULE,AA.SIMPLE",
                    "I",
                    "AA100066X9BZ-DEPOSITS.SCHEDULE-20100106",
                    "UPDATE"
                );
        }

        private void Test_SplitProcessDetails(string processDetails, string expectedVersion, string expectedAction, string expectedRecordId, string expectedUnknown)
        {
            Test_SplitProcessDetails(processDetails, 0, expectedVersion, expectedAction, expectedRecordId, expectedUnknown);
        }

        private void Test_SplitProcessDetails(string processDetails, int recordAtPosition, string expectedVersion, string expectedAction, string expectedRecordId, string expectedUnknown)
        {
            List<AAProcessDetails> result = AAProcessDetails.SplitProcessesDetails(processDetails);

            Assert.AreEqual(result[recordAtPosition].VersionFull, expectedVersion);
            Assert.AreEqual(result[recordAtPosition].Action, expectedAction);
            Assert.AreEqual(result[recordAtPosition].ID, expectedRecordId);
            Assert.AreEqual(result[recordAtPosition].Unknown, expectedUnknown);
        }

        [Test]
        public void Test_ReadFTResponseWithFailureInLinkedAA()
        {
            string typicalXsd = SamplesResponsesDir + "FUNDS.TRANSFER.xsd";

            var ofsResponseMessage = TestTelnetIOManager.GetMultipartResponseOFSMessage("FUNDS.TRANSFER.txt");

            var typical = GlobusMetadataReader.GetMetadataTypicalFromXSDFile(typicalXsd);
            typical.BaseClass = GlobusRequest.T24_DataProcess;

            OFSResponseReader reader = new OFSResponseReader();
            GlobusMessage.OFSMessage ofsMessage = TelnetIOManager.GetOFSMessageWithoutTags(TestTelnetIOManager.GetDefaultSettings(), ofsResponseMessage);
            string result = reader.ReadOFS(ofsMessage, typical, false, null);
            Assert.IsTrue(reader.HasError);
            Assert.AreEqual("ACTIVITY:1:1=No early repayments are allowed. (Property: AA100067LHDD-AR.PRINCIPAL.DECREASE-20100106)", result);
        }

        [Test]
        public void Test_Read_AA_SDA_BASIC_CURRENCY()
        {
            string typicalXsd = SamplesResponsesDir + "SDA.BASIC.xsd";

            var ofsResponseMessage = TestTelnetIOManager.GetMultipartResponseOFSMessage("SDA.BASIC,CURRENCY.txt");

            var typical = GlobusMetadataReader.GetMetadataTypicalFromXSDFile(typicalXsd);
            typical.BaseClass = GlobusRequest.T24_AAProduct;

            OFSResponseReader reader = new OFSResponseReader();
            GlobusMessage.OFSMessage ofsMessage = TelnetIOManager.GetOFSMessageWithoutTags(TestTelnetIOManager.GetDefaultSettings(), ofsResponseMessage);
            string result = reader.ReadOFS(ofsMessage, typical, false, null);
            Assert.IsFalse(reader.HasError);

            // TODO - TEST ACCOUNT.REFERENCE (is missing because it appears as {BALANCE} in OFS while it is {ACCOUNT} in typical)
        }
    }
}
