﻿using NUnit.Framework;
using OFSCommonMethods.IO;
using OFSCommonMethods.IO.Telnet;

namespace OFSCommonMethods.Unit_Tests
{
    [TestFixture]
    public class TestFtpConfigurationAttributesLoad
    {
        private const string PhantomSet = "AIX-migvb02";
        private const string HostNameAndPort = "ftp://10.10.10.100:21";
        private const string Username = "migvb02";
        private const string Password = "123456";
        private const bool IsSFtp = false;

        private Settings _Settings;
        
        [TestFixtureSetUp]
        public void SetUp()
        {
            _Settings = new Settings {PhantomSet = PhantomSet};
        }

        [Test]
        public void TestValidAttributesLoaded()
        {
            ExecutionStepContainer.LoadConnectionSettings(_Settings);
            Assert.AreEqual(HostNameAndPort, _Settings.FtpHostNameAndPort);
            Assert.AreEqual(Username, _Settings.FtpUsername);
            Assert.AreEqual(Password, _Settings.FtpPassword);
            Assert.AreEqual(IsSFtp, _Settings.IsSFtp);
            Assert.AreEqual(true, _Settings.FtpKeepAliveDefault);
            Assert.AreEqual(true, _Settings.FtpKeepAliveFirst);
            Assert.AreEqual(true, _Settings.FtpUsePassive);
        }



    }
}
