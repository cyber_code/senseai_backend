﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using AXMLEngine;
using NUnit.Framework;
using OFSCommonMethods.IO;
using OFSCommonMethods.IO.Telnet;
using OFSStringConvertor;
using Validata.Common;

namespace OFSCommonMethods.Unit_Tests
{
    [TestFixture]
    public class TestOFSResponseReader
    {
        private string SamplesResponsesDir;

        [TestFixtureSetUp]
        public void Setup()
        {
            var projectDirectory = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent;
            SamplesResponsesDir = projectDirectory.FullName + @"\Unit Tests\Sample Responses\";
        }

        [Test]
        public void TestOfsResponseReading()
        {
            Assert.IsTrue(IsOfsResponseSuccessful(@"A/B/C//1,RECORDS.PER.PAGE:1:1=1", "A/B/C"));
            Assert.IsFalse(IsOfsResponseSuccessful(@"BNK/B.TRACER.EXPORT/1,DUPLICATE.TRAP:1:1=TRUE", "BNK/B.TRACER.EXPORT"));
            Assert.IsTrue(IsOfsResponseSuccessful(@"BNK/B.TRACER.OUT.COLLECTION//1,DESCRIPTION:1:1=BNK/B.TRACER.OUT.COLLECTION", "BNK/B.TRACER.OUT.COLLECTION"));
            Assert.IsFalse(IsOfsResponseSuccessful(@"SECURITY.VIOLATION", "whatever"));
            Assert.IsTrue(IsOfsResponseSuccessful(@"100112//1,MNEMONIC:1:1=ALBR02TR,SHORT.NAME:1:1=THEO ALBRECHT,NAME.1:1:1=MR THEO ALBRECHT", "100112"));
            Assert.IsFalse(IsOfsResponseSuccessful(@"V.AUTH.RELEASE.ACC.MARGIN//-1/NO,SOURCE.TYPE:1:1=LIVE RECORD NOT CHANGED", "V.AUTH.RELEASE.ACC.MARGIN"));
            Assert.IsTrue(IsOfsResponseSuccessful(@"ABV/MSGID/1,RECORDS.PER.PAGE:1:1=1", "ABV"));
            Assert.IsFalse(IsOfsResponseSuccessful(@"12/456/1,DUPLICATE.TRAP:1:1=TRUE", "12")); // special case
        }

        [Test]
        [Explicit]
        public void TestPostOfsResponseReading()
        {
            var typical = new MetadataTypical();
            typical.Name = "ACCOUNT";

            typical.AddAttribute(new TypicalAttribute("@ID", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER-1~1", AttributeDataType.String, "", 1000));

            var reader = new OFSResponseReader();
            //GlobusMessage.OFSMessage message = new GlobusMessage.OFSMessage("VA2oDmXz_180|120529//-1,CUSTOMER:1:1=MISSING CUSTOMER - RECORD,CUSTOMER:1:1=INPUT MISSING");
            GlobusMessage.OFSMessage message = new GlobusMessage.OFSMessage("");
            String result = reader.ReadOFS(message, typical, false, "VA2oDmXz_180");
        }

        private static bool IsOfsResponseSuccessful(string ofsResponse, string id)
        {
            return OFSResponseReader.IsOfsResponseSuccessful(ofsResponse, id, false);
        }

        /// <summary>
        /// Tests the get customer metadata response.  STANDARD.SELECTION,/S/PROCESS,/,CUSTOMER
        /// </summary>
        [Test]
        public void TestGetCustomerMetadataResponse()
        {
            string responseFileName = SamplesResponsesDir + "CustomerMetadata.txt";

            MetadataTypical metadataTypical = GlobusMetadataReader.GetMetadataTypicalForStandardSelection();

            string vxml = ReadOFSFromFile(responseFileName, metadataTypical);

            Instance instance = GlobusMetadataReader.ReadInstanceFromVxml(vxml);
            GlobusTypeMetadata metadata = GlobusTypeMetadata.FromInstance(instance);

            Assert.AreEqual("CUSTOMER", metadata.Name);
            Assert.AreEqual(68, metadata.Attributes.Count);

            GlobusAttributeMetadata firstElement = metadata.Attributes[0];
            Assert.AreEqual("@ID", firstElement.FieldName);
            Assert.IsFalse(firstElement.IsMultiValue);
            Assert.IsFalse(firstElement.IsSubValue);
            Assert.IsFalse(firstElement.IsUserDefined);

            GlobusAttributeMetadata lastElement = metadata.Attributes[metadata.Attributes.Count - 1];
            Assert.AreEqual("SEC.TYPE.SAFEKP", lastElement.FieldName);
            Assert.IsFalse(lastElement.IsMultiValue);
            Assert.IsFalse(firstElement.IsSubValue);
            Assert.IsTrue(lastElement.IsUserDefined);
        }

        /// <summary>
        /// Tests the get customer metadata response.  STANDARD.SELECTION,/S/PROCESS,/,CUSTOMER
        /// </summary>
        [Test]
        public void TestGet_FT_COMMISSION_TYPE_MetadataResponse()
        {
            string responseFileName = SamplesResponsesDir + "FT.COMMISSION.TYPE.txt";

            MetadataTypical metadataTypical = GlobusMetadataReader.GetMetadataTypicalForStandardSelection();

            string vxml = ReadOFSFromFile(responseFileName, metadataTypical);

            Instance instance = GlobusMetadataReader.ReadInstanceFromVxml(vxml);
            GlobusTypeMetadata metadata = GlobusTypeMetadata.FromInstance(instance);

            Assert.AreEqual("FT.COMMISSION.TYPE", metadata.Name);
            Assert.AreEqual(54, metadata.Attributes.Count);

            GlobusAttributeMetadata firstElement = metadata.Attributes[0];
            Assert.AreEqual("@ID", firstElement.FieldName);
            Assert.IsFalse(firstElement.IsMultiValue);
            Assert.IsFalse(firstElement.IsUserDefined);

            GlobusAttributeMetadata lastElement = metadata.Attributes[metadata.Attributes.Count - 1];
            Assert.AreEqual("FT.COMMISSION.TYPE", lastElement.FieldName);
            Assert.IsFalse(lastElement.IsMultiValue);
            Assert.IsFalse(lastElement.IsSubValue);
            Assert.IsTrue(lastElement.IsUserDefined);
        }

        [Test]
        public void TestGet_MG_MORTGAGEMetadataResponse()
        {
            string responseFileName = SamplesResponsesDir + "MG.MORTGAGE.txt";
            string typicalXsd = SamplesResponsesDir + "R9_MG_MORTGAGE.xsd";

            MetadataTypical metadataTypical = GlobusMetadataReader.GetMetadataTypicalFromXSDFile(typicalXsd);

            string vxml = ReadOFSFromFile(responseFileName, metadataTypical);

            Instance instance = GlobusMetadataReader.ReadInstanceFromVxml(vxml);

            Assert.AreEqual(instance.AttributeByName("DD.EXCG.RATE").Value, "33.50000000");
        }

        [Test]
        [Explicit]
        public void TestEnquiryResponseResultParser_MB_DE_MSG_SUM()
        {
            var typical = new MetadataTypical();
            typical.Name = "MB.DE.MSG.SUM";

            typical.AddAttribute(new TypicalAttribute("@ID", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("BANK.DATE", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("MESSAGE", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("MSG.TYPE", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("DISP.TEXT", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("HEADER.CARRIER", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("TO.ADDRESS", AttributeDataType.String, "", 1000));

            var reader = new OFSResponseReader();

            string response = File.ReadAllText(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"Response.txt");

            using (var writer = new XmlTextWriter(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"ParsedInstances.txt", null))
            {
                reader.ReadEnquiryResponse("MB.DE.MSG.SUM", response, typical, writer, null, false, EnquiryResultParsingRotine.None);
                writer.Flush();
            }
        }

        [Test]
        [Explicit]
        public void TestEnquiryResponseResultParser_IDIE_CUSTOMER_SRCH_R2()
        {
            var typical = new MetadataTypical();
            typical.Name = "IDIE.CUSTOMER.SRCH.R2";

            typical.AddAttribute(new TypicalAttribute("Cust", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Name", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("ID.No", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("B.Date", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Othr", AttributeDataType.String, "", 1000));

            var reader = new OFSResponseReader();

            string response = File.ReadAllText(@"D:\response.txt");

            using (var writer = new XmlTextWriter(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"ParsedInstances.txt", null))
            {
                LevelStructureHierarchy lsh = LevelStructureHierarchy.FromXmlString(
                        File.ReadAllText(@"D:\enq_cfg.xml")
                    );
                EnquiryResultParsingRotine erpr = new EnquiryResultParsingRotine(lsh);

                reader.ReadEnquiryResponse("IDIE.CUSTOMER.SRCH.R2",
                    response, typical, writer, null, false, erpr
                    );
                writer.Flush();
            }
        }

        [Test]
        [Explicit]
        public void TestEnquiryResponseResultParser_SS()
        {
            var ssTypical = GlobusMetadataReader.GetMetadataTypicalForStandardSelection();

            var reader = new OFSResponseReader();

            string response = File.ReadAllText(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"response1.txt");

            using (var writer = new XmlTextWriter(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"ParsedInstances1.txt", null))
            {
                reader.ReadEnquiryResponse(ssTypical.Name, response, ssTypical, writer, null, false, EnquiryResultParsingRotine.None);
                writer.Flush();
            }
        }

        [Test]
        public void TestResponseParsing_FailedIDWithBackslash()
        {
            var typical = new MetadataTypical();
            typical.Name = "TEST";
            typical.AddAttribute(new TypicalAttribute("@ID", AttributeDataType.String, "", 1000));

            var reader = new OFSResponseReader();
            string vxml = reader.ReadOFS(
                        new GlobusMessage.OFSMessage(@"BNK/B.TRACER.OUT.COLLECTION//-1/NO,@ID:1:1=RECORD MISSING"),
                        typical,
                        false,
                        "BNK/B.TRACER.OUT.COLLECTION"
                    );
            Assert.IsTrue(reader.HasError);
            Assert.AreEqual("@ID:1:1=RECORD MISSING", vxml);
        }

        [Test]
        public void TestResponseParsing_SucccessfulIDWithoutBackslash()
        {
            var typical = new MetadataTypical();
            typical.Name = "TEST";
            typical.AddAttribute(new TypicalAttribute("@ID", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("AAA", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("TEXT", AttributeDataType.String, "", 1000));

            var reader = new OFSResponseReader();
            string vxml = reader.ReadOFS(
                        new GlobusMessage.OFSMessage(@"AA/OPA/1,AAA:1:1=WOW,TEXT:1:1=O?A"),
                        typical,
                        false,
                        null
                    );
            Assert.IsFalse(reader.HasError);
            Assert.IsTrue(vxml.Contains("WOW"));
            Assert.IsTrue(vxml.Contains("O,A"));
        }


        [Test]
        public void TestEnquiryResponseResultParser()
        {
            /*
            var typical = new MetadataTypical();
            
            typical.AddAttribute(new TypicalAttribute("Customer", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Account", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Currency", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Name_x0020_of_x0020_the_x0020_Account", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Locked_x0020_Amt", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Avail_x0020_Bal_x0020_Not_x0020_Gra", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Avail_x0020_Bla_x0020_Gra", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Working_x0020_Bal_x0020_Gra", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Working_x0020_Bal", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Online_x0020_Bal", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Lim_x0020_Ref", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("Cover_x0020_Chk", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("IBAN", AttributeDataType.String, "", 1000));

            var reader = new OFSResponseReader("AIX-G13", null);

            string response;
            using( var myFile = new StreamReader("c:\\Response1.txt"))
            {
                response = myFile.ReadToEnd();
                var settings = new Settings {PhantomSet = "AIX-migvb01"};

                ExecutionStepContainer.LoadConnectionSettings(settings);
                response = TelnetIOManager.PostProcessResult(settings, response);
                myFile.Close();
            }

            using( var writer = new XmlTextWriter("c:\\Writer.txt", null))
            {
                reader.ReadEnquiryResponse("NB.SADB.PNB.ENQ", response, typical, writer, null,
                                       false, "");
              
                writer.Flush();
            }
            */
        }

        private static string ReadOFSFromFile(string fileName, MetadataTypical metadataTypical)
        {
            var reader = new OFSResponseReader();
            string message = reader.ReadOFS(
                    new GlobusMessage.OFSMessage(File.ReadAllText(fileName)),
                    metadataTypical,
                    false,
                    null
                );
            return message;
        }

        [Test]
        public void TestMultipartResponseReader()
        {
            string multipartOfsMessage = TestTelnetIOManager.GetDefaultMultipartResponseOFSMessage();
            GlobusMessage.OFSMessage ofsMessage =
                TelnetIOManager.GetOFSMessageWithoutTags(TestTelnetIOManager.GetDefaultSettings(), multipartOfsMessage);

            OFSResponseReader reader = new OFSResponseReader();
            string result = reader.ReadOFS(ofsMessage, GetAA_FIXED_FLOATING_DEP_Typical(), false, null);
        }

        [Test]
        public void TestOFSResponseReader_EB_GC_CONSTRAINTS()
        {
            const string ofsResponseMessage = "EC.OPEN.ORDER/90000671//1,TEST.FIELD:1:1=SECURITY.NO,OPERAND:1:1=EQ,EVAL.VALUE:1:1=000555-340";

            var typical = new MetadataTypical();
            typical.Name = "EB.GC.CONSTRAINTS";

            typical.AddAttribute(new TypicalAttribute("@ID", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("TEST.FIELD-1~1", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("OPERAND-1~1", AttributeDataType.String, "", 1000));
            typical.AddAttribute(new TypicalAttribute("EVAL.VALUE", AttributeDataType.String, "", 1000));

            OFSResponseReader reader = new OFSResponseReader();
            string result = reader.ReadOFS(new GlobusMessage.OFSMessage(ofsResponseMessage), typical, false, null);
            Assert.IsFalse(reader.HasError);
            Assert.AreEqual(reader.TransactionID, "EC.OPEN.ORDER/90000671");
        }


        [Test]
        [Explicit]
        public void TestMultiValueShrinkStretch()
        {
            string msg =
                "VALXk0Cv_0|MD1327403841//-1,CATEGORY:1:1=MISSING CATEGORY - RECORD,CHARGE.CODE:1:2=INVALID CHARGE CODE,CATEGORY:1:1=INPUT MISSING";

            var typical = new MetadataTypical
            {
                CatalogName = "Any",
                Name = "Any"
            };
            typical.AddAttribute(new TypicalAttribute("CATEGORY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CHARGE.CODE", AttributeDataType.String, "", 0));

            var mvProcessor = new MultiValueShrinkStretch(typical);

            Settings t24OFSTelnetSettings = new Settings() { PhantomSet = "R11-169" };
            ExecutionStepContainer.LoadConnectionSettings(t24OFSTelnetSettings);
            GlobusMessage.OFSMessage ofsMessage = TelnetIOManager.GetOFSMessageWithoutTags(t24OFSTelnetSettings, msg);
            var reader = new OFSResponseReader();
            string resultOfs = reader.ReadOFS(ofsMessage, typical, false, null);

            if (!reader.HasError)
            {
                var res = mvProcessor.GetOfsMessageWithShrinkedMultivalues(resultOfs);
            }
            
        }

        [Test]
        [Explicit]
        public void TestMultiValueShrinkStretch_CU()
        {
            // REQUEST
            string msg =
                "VTACtjdp_0|<MSG>CUSTOMER,MIG//PROCESS,VCONN1/123456/,1032337,ACCOUNT.OFFICER::=1,ADDRESS:1:2=اندèار انرابظ,CO.CODE::=JO0010001,CONTACT.DATE::=20070906,COUNTRY:1:1=Jordan,COUNTRY:2:1=Jordan,CUS.RES.TYPE::=RESIDENT,CUSTOMER.SINCE::=20070906,CUSTOMER.STATUS::=1,CUSTOMER.TYPE::=ACTIVE,EXTERN.CUS.ID:1:1=503164,EXTERN.SYS.ID:1:1=LEGACY,FAX.1:1:1=4623591,GB.POSTAL.CODE::=000102,GENDER::=MALE,INDUSTRY::=1408,LANGUAGE::=2,MARITAL.STATUS::=SINGLE,MNEMONIC::=M503164,NAME.1:1:1=AUTOMIGRATION,NAME.1:2:1=صودèâ ادخار هèطلé انهجنس انè×وé,NAME.2:2:1=نشؤèو اناسرç,NATIONALITY::=JO,NO.OF.DEPENDENTS::=0,PHONE.1:1:1=4623490,POST.CODE.AREA::=000102,POST.CODE:1:1=11183,POST.CODE:2:1=11183,PREF.CHANNEL:1:1=EMAIL,RESIDENCE::=JO,RESIDENCE.STATUS:1:1=OTHER,SECTOR::=1000,SHORT.NAME:1:1=MIGRATION,SHORT.NAME:2:1=هجنس صودèâ,SMS.1:1:1=4623590,STREET:1:1=MIGRATION,STREET:2:1=êحلط ندé لرظ انبومêç انخاصç,SUB.SEGMENT::=0220,TARGET::=50,TOWN.COUNTRY:1:1=DEER GHBAR PHELEDLFIA,TOWN.COUNTRY:2:1=دêر عبار لêنادنلêا</MSG>";

            var typical = new MetadataTypical
                              {
                                  CatalogName = "Any",
                                  Name = "Any"
                              };
            typical.AddAttribute(new TypicalAttribute("ACCOUNT.OFFICER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ADDRESS-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CONTACT.DATE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("COUNTRY-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUS.RES.TYPE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER.SINCE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER.STATUS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER.TYPE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("DELIVERY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("EXTERN.CUS.ID-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("EXTERN.SYS.ID-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("FAMILY.NAME", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("GENDER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("INDUSTRY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("LANGUAGE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("MARITAL.STATUS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("MNEMONIC", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NAME.1-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NAME.2-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NATIONALITY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NO.OF.CHILDREN", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NO.OF.DEPENDENTS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NO.OF.WIVES", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("PHONE.1-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("PREF.CHANNEL-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("RESIDENCE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("RESIDENCE.STATUS-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("SALARY.TO.JAB", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("SECTOR", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("SHORT.NAME-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("STREET-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TARGET", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TITLE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TRANS.TO.JAB", AttributeDataType.String, "", 0));


            var mvProcessor = new MultiValueShrinkStretch(typical);
        //    var res = mvProcessor.GetOfsMessageWithStretchedMultivalues(msg);

        }




        [Test]
        [Explicit]
        public void TestMultiValueShrinkStretch_CU1()
        {
            var typical = new MetadataTypical
            {
                CatalogName = "Any",
                Name = "Any"
            };
            typical.AddAttribute(new TypicalAttribute("ACCOUNT.OFFICER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ADDRESS-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CONTACT.DATE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("COUNTRY-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUS.RES.TYPE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER.SINCE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER.STATUS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER.TYPE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("DELIVERY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("EXTERN.CUS.ID-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("EXTERN.SYS.ID-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("FAMILY.NAME", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("GENDER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("INDUSTRY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("LANGUAGE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("MARITAL.STATUS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("MNEMONIC", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NAME.1-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NAME.2-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NATIONALITY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NO.OF.CHILDREN", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NO.OF.DEPENDENTS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NO.OF.WIVES", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("PHONE.1-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("PREF.CHANNEL-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("RESIDENCE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("RESIDENCE.STATUS-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("SALARY.TO.JAB", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("SECTOR", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("SHORT.NAME-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("STREET-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TARGET", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TITLE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TRANS.TO.JAB", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TOWN.COUNTRY-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TOWN.COUNTRY-2~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("POST.CODE-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("POST.CODE-2~1", AttributeDataType.String, "", 0));


            var mvProcessor = new MultiValueShrinkStretch(typical);

            //RESPONSE
            var msg =
                "VTAPKvFL_26910|1062115//1,MNEMONIC:1:1=M551511,SHORT.NAME:1:1=MIGRATION,SHORT.NAME:2:1=ظهارê ظهار è ظèêس طنان,NAME.1:1:1=AUTOMIGRATION,NAME.1:2:1=ظهار çزاظ هوèر ظهارê è طنان ظâاب,NAME.2:1:1=AUTOMIGRATION,NAME.2:2:1=هوèر ظèêس,STREET:1:1=MIGRATION,STREET:2:1=انهدêوة انرêاضêة / شارظ شهس اندêو,ADDRESS:1:2=انهدêوة انرêاضêة / شارظ شهس اندêو ا,ADDRESS:2:2=ظهارة رâه 16 / âرب هدارس اناتحاد,TOWN.COUNTRY:1:1=INSIDE AMMAN,TOWN.COUNTRY:2:1=هوا×â اخرé/داخن ظهاو,POST.CODE:1:1=11196,POST.CODE:2:1=11196,COUNTRY:1:1=Jordan,COUNTRY:2:1=Jordan,SECTOR:1:1=1002,ACCOUNT.OFFICER:1:1=1,INDUSTRY:1:1=1459,TARGET:1:1=4,NATIONALITY:1:1=JO,CUSTOMER.STATUS:1:1=1,RESIDENCE:1:1=JO,CONTACT.DATE:1:1=20110705,LANGUAGE:1:1=2,COMPANY.BOOK:1:1=JO0010001,CLS.CPARTY:1:1=NO,TITLE:1:1=MES,FAMILY.NAME:1:1=MIGRATION,GENDER:1:1=MALE,MARITAL.STATUS:1:1=SINGLE,PHONE.1:1:1=5159382,SMS.1:1:1=0799806090,RESIDENCE.STATUS:1:1=OTHER,PREF.CHANNEL:1:1=EMAIL,FAX.1:1:1=0799806090,CUSTOMER.SINCE:1:1=20110705,CUSTOMER.TYPE:1:1=ACTIVE,AML.CHECK:1:1=NULL,AML.RESULT:1:1=NULL,INTERNET.BANKING.SERVICE:1:1=NULL,MOBILE.BANKING.SERVICE:1:1=NULL,EXTERN.SYS.ID:1:1=LEGACY,EXTERN.CUS.ID:1:1=551511,CUS.RES.TYPE:1:1=RESIDENT,TRANS.TO.JAB:1:1=YES,DELIVERY:1:1=BY MAIL,POST.CODE.AREA:1:1=000108,GB.POSTAL.CODE:1:1=000108,SALARY.TO.JAB:1:1=Yes: Approved form,OVERRIDE:1:1=EB.CUS.ADDR.RES.CHANGED.AGREE}Field ADDRESS/RESDIDENCE changed. Still agree?,CURR.NO:1:1=2,INPUTTER:1:1=89_INPUTTER__OFS_GENERIC.OFS.PROCESS,DATE.TIME:1:1=1404212016,AUTHORISER:1:1=89_INPUTTER_OFS_GENERIC.OFS.PROCESS,CO.CODE:1:1=JO0010001,DEPT.CODE:1:1=1";
            
            Settings t24OFSTelnetSettings = new Settings() {PhantomSet = "R11-169"};
            ExecutionStepContainer.LoadConnectionSettings(t24OFSTelnetSettings);
            GlobusMessage.OFSMessage ofsMessage = TelnetIOManager.GetOFSMessageWithoutTags(t24OFSTelnetSettings, msg);
            var reader = new OFSResponseReader();
            string resultOfs = reader.ReadOFS(ofsMessage, typical, false, null);

            resultOfs = mvProcessor.GetOfsMessageWithShrinkedMultivalues(resultOfs);


        }

        [Test]
        [Explicit]
        public void TestMultiValueShrinkStretch_CU_File_AA()
        {
            var typical = new MetadataTypical
            {
                CatalogName = "Any",
                Name = "Any",
                BaseClass = OFSCommonMethods.GlobusRequestBase.T24_AAProduct
                              };
            typical.AddAttribute(new TypicalAttribute("ARR.SEQUENCE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ARRANGEMENT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ACTIVITY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ACTION", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("OWNER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("PRIMARY.OWNER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CHANGED.FIELDS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NEGOTIATED.FLDS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ID.COMP.1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ID.COMP.2", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ID.COMP.3", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ACTION", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ACTION", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("EFFECTIVE.DATE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("PRODUCT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("INITIATION.TYPE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ORIG.CONTRACT.DATE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("MASTER.AAA", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ACTIVITY.CLASS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.ACCOUNT.TITLE.2", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.ALT.ID", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.ACCOUNT.REFERENCE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.APP.METHOD", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.PAYIN.SETTLEMENT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.CURRENCY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{ACTIVITY.CHARGES}.APP.METHOD", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{SETTLEMENT}.PAYIN.SETTLEMENT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{SETTLEMENT}.PAYOUT.SETTLEMENT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{RENEWAL}.PRODUCT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{ODCOMMISSION}.CURRENCY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CURR.NO", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("INPUTTER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("DATE.TIME", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("AUTHORISER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CO.CODE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("DEPT.CODE", AttributeDataType.String, "", 0));

            typical.AddAttribute(new TypicalAttribute("{CUSTOMER}.OWNER", AttributeDataType.String, "", 0));

            var mvProcessor = new MultiValueShrinkStretch(typical);

            //RESPONSE

            Settings t24OFSTelnetSettings = new Settings() { PhantomSet = "R11-169" };
            ExecutionStepContainer.LoadConnectionSettings(t24OFSTelnetSettings);
            string fileName = @"..\..\Unit Tests\Sample Responses\JAB_CUSTOMER_POSTOFS_AA.txt";

            var lines = File.ReadAllLines(fileName);
            foreach (var line in lines)
            {
                GlobusMessage.OFSMessage ofsMessage = TelnetIOManager.GetOFSMessageWithoutTags(t24OFSTelnetSettings, line);
                var reader = new OFSResponseReader() ;
                string resultOfs = reader.ReadOFS(ofsMessage, typical, false, null);
                if (!reader.HasError)
                {
                    resultOfs = mvProcessor.GetOfsMessageWithShrinkedMultivalues(resultOfs);
                }
            }


        }

        [Test]
        [Explicit]
        public void TestMultiValueShrinkStretch_CU_File()
        {
            var typical = new MetadataTypical
            {
                CatalogName = "Any",
                Name = "Any"
            };
            typical.AddAttribute(new TypicalAttribute("ACCOUNT.OFFICER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ADDRESS-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CONTACT.DATE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("COUNTRY-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUS.RES.TYPE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER.SINCE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER.STATUS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER.TYPE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("DELIVERY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("EXTERN.CUS.ID-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("EXTERN.SYS.ID-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("FAMILY.NAME", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("GENDER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("INDUSTRY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("LANGUAGE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("MARITAL.STATUS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("MNEMONIC", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NAME.1-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NAME.2-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NATIONALITY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NO.OF.CHILDREN", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NO.OF.DEPENDENTS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NO.OF.WIVES", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("PHONE.1-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("PREF.CHANNEL-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("RESIDENCE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("RESIDENCE.STATUS-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("SALARY.TO.JAB", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("SECTOR", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("SHORT.NAME-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("STREET-1~1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TARGET", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TITLE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("TRANS.TO.JAB", AttributeDataType.String, "", 0));
            var mvProcessor = new MultiValueShrinkStretch(typical);

            //RESPONSE
            
            Settings t24OFSTelnetSettings = new Settings() { PhantomSet = "R11-169" };
            ExecutionStepContainer.LoadConnectionSettings(t24OFSTelnetSettings);
            string fileName = @"..\..\Unit Tests\Sample Responses\JAB_CUSTOMER_POSTOFS.txt";

            var lines = File.ReadAllLines(fileName);
            foreach (var line in lines)
            {
                GlobusMessage.OFSMessage ofsMessage = TelnetIOManager.GetOFSMessageWithoutTags(t24OFSTelnetSettings, line);
                var reader = new OFSResponseReader();
                string resultOfs = reader.ReadOFS(ofsMessage, typical, false, null);

                if (!reader.HasError)
                {
                    resultOfs = mvProcessor.GetOfsMessageWithShrinkedMultivalues(resultOfs);
                }
            }


        }

















        [Test]
        [Ignore]
        [Explicit]
        public void TestMultiValueSplit_AA()
        {
            var typical = new MetadataTypical
            {
                CatalogName = "AnyCat",
                Name = "AATestproduct",
                BaseClass = OFSCommonMethods.GlobusRequestBase.T24_AAProduct
            };
            typical.AddAttribute(new TypicalAttribute("ARR.SEQUENCE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ARRANGEMENT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ACTIVITY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ACTION", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("OWNER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("PRIMARY.OWNER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CHANGED.FIELDS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("NEGOTIATED.FLDS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ID.COMP.1", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ID.COMP.2", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ID.COMP.3", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ACTION", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ACTION", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("EFFECTIVE.DATE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CUSTOMER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("PRODUCT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("INITIATION.TYPE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ORIG.CONTRACT.DATE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("MASTER.AAA", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("ACTIVITY.CLASS", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.ACCOUNT.TITLE.2", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.ALT.ID", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.ACCOUNT.REFERENCE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.APP.METHOD", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.PAYIN.SETTLEMENT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{BALANCE}.CURRENCY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{ACTIVITY.CHARGES}.APP.METHOD", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{SETTLEMENT}.PAYIN.SETTLEMENT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{SETTLEMENT}.PAYOUT.SETTLEMENT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{RENEWAL}.PRODUCT", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("{ODCOMMISSION}.CURRENCY", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CURR.NO", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("INPUTTER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("DATE.TIME", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("AUTHORISER", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("CO.CODE", AttributeDataType.String, "", 0));
            typical.AddAttribute(new TypicalAttribute("DEPT.CODE", AttributeDataType.String, "", 0));

            typical.AddAttribute(new TypicalAttribute("{CUSTOMER}.OWNER", AttributeDataType.String, "", 0));

            var mvProcessor = new MultiValueShrinkStretch(typical);

            Instance inst = new Instance("AATestproduct", "AnyCat");

            inst.AddAttribute("ARR.SEQUENCE", "1");
            inst.AddAttribute("ARRANGEMENT", "1");
            inst.AddAttribute("ACTIVITY", "NEW");
            inst.AddAttribute("ACTION", "ALL~BALL");
            inst.AddAttribute("OWNER", "");
            inst.AddAttribute("PRIMARY.OWNER", "1");
            inst.AddAttribute("CHANGED.FIELDS", "");
            inst.AddAttribute("NEGOTIATED.FLDS", "");
            inst.AddAttribute("ID.COMP.1", "");
            inst.AddAttribute("ID.COMP.2", "");
            inst.AddAttribute("ID.COMP.3", "");
            inst.AddAttribute("ACTION", "");
            inst.AddAttribute("EFFECTIVE.DATE", "");
            inst.AddAttribute("CUSTOMER", "");
            inst.AddAttribute("PRODUCT", "");
            inst.AddAttribute("INITIATION.TYPE", "");
            inst.AddAttribute("ORIG.CONTRACT.DATE", "");
            inst.AddAttribute("MASTER.AAA", "");
            inst.AddAttribute("ACTIVITY.CLASS", "");
            inst.AddAttribute("{BALANCE}.ACCOUNT.TITLE.2", "");
            inst.AddAttribute("{BALANCE}.ALT.ID", "");
            inst.AddAttribute("{BALANCE}.ACCOUNT.REFERENCE", "1~1");
            inst.AddAttribute("{BALANCE}.APP.METHOD", "");
            inst.AddAttribute("{BALANCE}.PAYIN.SETTLEMENT", "");
            inst.AddAttribute("{BALANCE}.CURRENCY", "");
            inst.AddAttribute("{ACTIVITY.CHARGES}.APP.METHOD", "");
            inst.AddAttribute("{SETTLEMENT}.PAYIN.SETTLEMENT-2~1", "1~1}");
            inst.AddAttribute("{SETTLEMENT}.PAYOUT.SETTLEMENT", "1~1}2");
            inst.AddAttribute("{RENEWAL}.PRODUCT", "");
            inst.AddAttribute("{ODCOMMISSION}.CURRENCY", "");
            inst.AddAttribute("CURR.NO", "");
            inst.AddAttribute("INPUTTER", "");
            inst.AddAttribute("DATE.TIME", "");
            inst.AddAttribute("AUTHORISER", "");
            inst.AddAttribute("CO.CODE", "");
            inst.AddAttribute("DEPT.CODE", "");
            inst.AddAttribute("{CUSTOMER}.OWNER", "");

            Instance newInst = mvProcessor.SplitMultiValues(inst);
        }



























        //[Test]
        //[Ignore]
        //[Explicit]
        //public void TestMultiValueShrinkStretch_AA()
        //{
        //    string msg;
        //    //  "VALjbtZF_0|<MSG>AA.ARRANGEMENT.ACTIVITY,MIG/I/PROCESS,VCONN1/123456/JO0010039,,ARRANGEMENT::=NEW,ACTIVITY::=LENDING-TAKEOVER-ARRANGEMENT,EFFECTIVE.DATE::=20131001,CUSTOMER::=1058315,PRODUCT::=HL.READY.BUILT.HOUSE,PROPERTY:1:1=CUSTOMER,FIELD.NAME:1:1=OWNER:1:1,FIELD.VALUE:1:1=1058315~-~1046902,FIELD.NAME:1:2=TEST,FIELD.VALUE:1:2=1</MSG>";
        //    var typical = new MetadataTypical
        //                      {
        //                          CatalogName = "Any",
        //                          Name = "Any",
        //                          BaseClass = OFSCommonMethods.GlobusRequestBase.T24_AAProduct
        //                      };
        //    typical.AddAttribute(new TypicalAttribute("ARR.SEQUENCE", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("ARRANGEMENT", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("ACTIVITY", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("EFFECTIVE.DATE", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("CUSTOMER", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("PRODUCT", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("{CUSTOMER}.OWNER", AttributeDataType.String, "", 0));

        //    var mvProcessor = new MultiValueShrinkStretch(typical);

        //    // AA Account with multi in OWNER - real OFS
        //    msg =
        //        "VALjbtZF_0|<MSG>AA.ARRANGEMENT.ACTIVITY,MIG/I/PROCESS,VCONN1/123456/JO0010039,,ARRANGEMENT::=NEW,ACTIVITY::=LENDING-TAKEOVER-ARRANGEMENT,EFFECTIVE.DATE::=20131001,CUSTOMER::=1058315,PRODUCT::=HL.READY.BUILT.HOUSE,CURRENCY::=JOD,ORIG.CONTRACT.DATE::=20090707,PROPERTY:1:1=CUSTOMER,FIELD.NAME:1:1=OWNER,FIELD.VALUE:1:1=1058315~-~1046902,PROPERTY:2:1=ACCOUNT,FIELD.NAME:2:1=ALT.ID:1:1,FIELD.VALUE:2:1=390107212404074603,FIELD.NAME:2:2=ACCOUNT.REFERENCE,FIELD.VALUE:2:2=1175491,PROPERTY:3:1=PRINCIPALINT,FIELD.NAME:3:1=FLOATING.INDEX:1:1,FIELD.VALUE:3:1=2,FIELD.NAME:3:2=MARGIN.TYPE:1:1,FIELD.VALUE:3:2=SINGLE,FIELD.NAME:3:3=MARGIN.RATE:1:1,FIELD.VALUE:3:3=6.0,FIELD.NAME:3:4=MARGIN.OPER:1:1,FIELD.VALUE:3:4=ADD,PROPERTY:4:1=SETTLEMENT,FIELD.NAME:4:1=PAYIN.SETTLEMENT:1:1,FIELD.VALUE:4:1=YES,FIELD.NAME:4:2=PAYIN.SETTLEMENT:2:1,FIELD.VALUE:4:2=NO,FIELD.NAME:4:3=PAYIN.SETTLEMENT:3:1,FIELD.VALUE:4:3=NO,FIELD.NAME:4:4=PAYIN.SETTLEMENT:4:1,FIELD.VALUE:4:4=NO,FIELD.NAME:4:5=PAYIN.SETTLEMENT:5:1,FIELD.VALUE:4:5=NO,FIELD.NAME:4:6=PAYIN.ACCOUNT:1:1,FIELD.VALUE:4:6=390130101404074601,FIELD.NAME:4:7=PAYMENT.TYPE:1:1,FIELD.VALUE:4:7=ACTUAL,PROPERTY:5:1=COMMITMENT,FIELD.NAME:5:1=AMOUNT,FIELD.VALUE:5:1=49813.983,FIELD.NAME:5:2=MATURITY.DATE,FIELD.VALUE:5:2=20230131,PROPERTY:6:1=LIMIT,FIELD.NAME:6:1=LIMIT.REFERENCE,FIELD.VALUE:6:1=6605,FIELD.NAME:6:2=LIMIT.SERIAL,FIELD.VALUE:6:2=01</MSG>";
        //    //var res = mvProcessor.GetOfsMessageWithStretchedMultivalues(msg);
        //    //var res2 = mvProcessor.GetOfsMessageWithShrinkedMultivalues(res, typical);
        //    //Assert.AreEqual(res2, msg);

        //    // AA Account with multi in CUSTOMER,  and OWNER - not real OFS - customer is not MV - just for test
        //    msg =
        //        "VALjbtZF_0|<MSG>AA.ARRANGEMENT.ACTIVITY,MIG/I/PROCESS,VCONN1/123456/JO0010039,,ARRANGEMENT::=NEW,ACTIVITY::=LENDING-TAKEOVER-ARRANGEMENT,EFFECTIVE.DATE::=20131001,CUSTOMER::=1058315~1058314,PRODUCT::=HL.READY.BUILT.HOUSE,CURRENCY::=JOD,ORIG.CONTRACT.DATE::=20090707,PROPERTY:1:1=CUSTOMER,FIELD.NAME:1:1=OWNER,FIELD.VALUE:1:1=1058315~-~1046902,PROPERTY:2:1=ACCOUNT,FIELD.NAME:2:1=ALT.ID:1:1,FIELD.VALUE:2:1=390107212404074603,FIELD.NAME:2:2=ACCOUNT.REFERENCE,FIELD.VALUE:2:2=1175491,PROPERTY:3:1=PRINCIPALINT,FIELD.NAME:3:1=FLOATING.INDEX:1:1,FIELD.VALUE:3:1=2,FIELD.NAME:3:2=MARGIN.TYPE:1:1,FIELD.VALUE:3:2=SINGLE,FIELD.NAME:3:3=MARGIN.RATE:1:1,FIELD.VALUE:3:3=6.0,FIELD.NAME:3:4=MARGIN.OPER:1:1,FIELD.VALUE:3:4=ADD,PROPERTY:4:1=SETTLEMENT,FIELD.NAME:4:1=PAYIN.SETTLEMENT:1:1,FIELD.VALUE:4:1=YES,FIELD.NAME:4:2=PAYIN.SETTLEMENT:2:1,FIELD.VALUE:4:2=NO,FIELD.NAME:4:3=PAYIN.SETTLEMENT:3:1,FIELD.VALUE:4:3=NO,FIELD.NAME:4:4=PAYIN.SETTLEMENT:4:1,FIELD.VALUE:4:4=NO,FIELD.NAME:4:5=PAYIN.SETTLEMENT:5:1,FIELD.VALUE:4:5=NO,FIELD.NAME:4:6=PAYIN.ACCOUNT:1:1,FIELD.VALUE:4:6=390130101404074601,FIELD.NAME:4:7=PAYMENT.TYPE:1:1,FIELD.VALUE:4:7=ACTUAL,PROPERTY:5:1=COMMITMENT,FIELD.NAME:5:1=AMOUNT,FIELD.VALUE:5:1=49813.983,FIELD.NAME:5:2=MATURITY.DATE,FIELD.VALUE:5:2=20230131,PROPERTY:6:1=LIMIT,FIELD.NAME:6:1=LIMIT.REFERENCE,FIELD.VALUE:6:1=6605,FIELD.NAME:6:2=LIMIT.SERIAL,FIELD.VALUE:6:2=01</MSG>";
        //    //res = mvProcessor.GetOfsMessageWithStretchedMultivalues(msg);
        //    //res2 = mvProcessor.GetOfsMessageWithShrinkedMultivalues(res, typical);
        //    //Assert.AreEqual(res2, msg);

        //    // AA Account with multi in EFFECTIVE.DATE, CUSTOMER, and OWNER - not real OFS - just for test two sequntial multi values
        //    msg =
        //        "VALjbtZF_0|<MSG>AA.ARRANGEMENT.ACTIVITY,MIG/I/PROCESS,VCONN1/123456/JO0010039,,ARRANGEMENT::=NEW,ACTIVITY::=LENDING-TAKEOVER-ARRANGEMENT,EFFECTIVE.DATE::=20131001~20131002,CUSTOMER::=1058315~1058314,PRODUCT::=HL.READY.BUILT.HOUSE,CURRENCY::=JOD,ORIG.CONTRACT.DATE::=20090707,PROPERTY:1:1=CUSTOMER,FIELD.NAME:1:1=OWNER,FIELD.VALUE:1:1=1058315~-~1046902,PROPERTY:2:1=ACCOUNT,FIELD.NAME:2:1=ALT.ID:1:1,FIELD.VALUE:2:1=390107212404074603,FIELD.NAME:2:2=ACCOUNT.REFERENCE,FIELD.VALUE:2:2=1175491,PROPERTY:3:1=PRINCIPALINT,FIELD.NAME:3:1=FLOATING.INDEX:1:1,FIELD.VALUE:3:1=2,FIELD.NAME:3:2=MARGIN.TYPE:1:1,FIELD.VALUE:3:2=SINGLE,FIELD.NAME:3:3=MARGIN.RATE:1:1,FIELD.VALUE:3:3=6.0,FIELD.NAME:3:4=MARGIN.OPER:1:1,FIELD.VALUE:3:4=ADD,PROPERTY:4:1=SETTLEMENT,FIELD.NAME:4:1=PAYIN.SETTLEMENT:1:1,FIELD.VALUE:4:1=YES,FIELD.NAME:4:2=PAYIN.SETTLEMENT:2:1,FIELD.VALUE:4:2=NO,FIELD.NAME:4:3=PAYIN.SETTLEMENT:3:1,FIELD.VALUE:4:3=NO,FIELD.NAME:4:4=PAYIN.SETTLEMENT:4:1,FIELD.VALUE:4:4=NO,FIELD.NAME:4:5=PAYIN.SETTLEMENT:5:1,FIELD.VALUE:4:5=NO,FIELD.NAME:4:6=PAYIN.ACCOUNT:1:1,FIELD.VALUE:4:6=390130101404074601,FIELD.NAME:4:7=PAYMENT.TYPE:1:1,FIELD.VALUE:4:7=ACTUAL,PROPERTY:5:1=COMMITMENT,FIELD.NAME:5:1=AMOUNT,FIELD.VALUE:5:1=49813.983,FIELD.NAME:5:2=MATURITY.DATE,FIELD.VALUE:5:2=20230131,PROPERTY:6:1=LIMIT,FIELD.NAME:6:1=LIMIT.REFERENCE,FIELD.VALUE:6:1=6605,FIELD.NAME:6:2=LIMIT.SERIAL,FIELD.VALUE:6:2=01</MSG>";
        //    //res = mvProcessor.GetOfsMessageWithStretchedMultivalues(msg);
        //    //res2 = mvProcessor.GetOfsMessageWithShrinkedMultivalues(res, typical);
        //    //Assert.AreEqual(res2, msg);


        //    // AA Account with multi in EFFECTIVE.DATE, CUSTOMER, and OWNER - not real OFS - just for test two sequntial multi values, customer is multivalue
        //    typical = new MetadataTypical
        //    {
        //        CatalogName = "Any",
        //        Name = "Any",
        //        BaseClass = OFSCommonMethods.GlobusRequestBase.T24_AAProduct
        //    };
        //    typical.AddAttribute(new TypicalAttribute("ARR.SEQUENCE", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("ARRANGEMENT", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("ACTIVITY", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("EFFECTIVE.DATE", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("CUSTOMER-1~1", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("PRODUCT", AttributeDataType.String, "", 0));
        //    typical.AddAttribute(new TypicalAttribute("{CUSTOMER}.OWNER", AttributeDataType.String, "", 0));

        //    mvProcessor = new MultiValueShrinkStretch(typical);
        //    //msg =
        //    //    "VALjbtZF_0|<MSG>AA.ARRANGEMENT.ACTIVITY,MIG/I/PROCESS,VCONN1/123456/JO0010039,,ARRANGEMENT::=NEW,ACTIVITY::=LENDING-TAKEOVER-ARRANGEMENT,EFFECTIVE.DATE::=20131001~20131002,CUSTOMER:1:1=1058315~1058314,PRODUCT::=HL.READY.BUILT.HOUSE,CURRENCY::=JOD,ORIG.CONTRACT.DATE::=20090707,PROPERTY:1:1=CUSTOMER,FIELD.NAME:1:1=OWNER,FIELD.VALUE:1:1=1058315~-~1046902,PROPERTY:2:1=ACCOUNT,FIELD.NAME:2:1=ALT.ID:1:1,FIELD.VALUE:2:1=390107212404074603,FIELD.NAME:2:2=ACCOUNT.REFERENCE,FIELD.VALUE:2:2=1175491,PROPERTY:3:1=PRINCIPALINT,FIELD.NAME:3:1=FLOATING.INDEX:1:1,FIELD.VALUE:3:1=2,FIELD.NAME:3:2=MARGIN.TYPE:1:1,FIELD.VALUE:3:2=SINGLE,FIELD.NAME:3:3=MARGIN.RATE:1:1,FIELD.VALUE:3:3=6.0,FIELD.NAME:3:4=MARGIN.OPER:1:1,FIELD.VALUE:3:4=ADD,PROPERTY:4:1=SETTLEMENT,FIELD.NAME:4:1=PAYIN.SETTLEMENT:1:1,FIELD.VALUE:4:1=YES,FIELD.NAME:4:2=PAYIN.SETTLEMENT:2:1,FIELD.VALUE:4:2=NO,FIELD.NAME:4:3=PAYIN.SETTLEMENT:3:1,FIELD.VALUE:4:3=NO,FIELD.NAME:4:4=PAYIN.SETTLEMENT:4:1,FIELD.VALUE:4:4=NO,FIELD.NAME:4:5=PAYIN.SETTLEMENT:5:1,FIELD.VALUE:4:5=NO,FIELD.NAME:4:6=PAYIN.ACCOUNT:1:1,FIELD.VALUE:4:6=390130101404074601,FIELD.NAME:4:7=PAYMENT.TYPE:1:1,FIELD.VALUE:4:7=ACTUAL,PROPERTY:5:1=COMMITMENT,FIELD.NAME:5:1=AMOUNT,FIELD.VALUE:5:1=49813.983,FIELD.NAME:5:2=MATURITY.DATE,FIELD.VALUE:5:2=20230131,PROPERTY:6:1=LIMIT,FIELD.NAME:6:1=LIMIT.REFERENCE,FIELD.VALUE:6:1=6605,FIELD.NAME:6:2=LIMIT.SERIAL,FIELD.VALUE:6:2=01</MSG>";
        //    //res = mvProcessor.GetOfsMessageWithStretchedMultivalues(msg);
        //    //res2 = mvProcessor.GetOfsMessageWithShrinkedMultivalues(res, typical);
        //    //Assert.AreEqual(res2, msg);
        //}

        //[Test]
        //[Ignore]
        //[Explicit]
        //public void TestMultiValueShrinkStretch_Forex()
        //{

        //    //string msg;
        //    ////  "VALjbtZF_0|<MSG>AA.ARRANGEMENT.ACTIVITY,MIG/I/PROCESS,VCONN1/123456/JO0010039,,ARRANGEMENT::=NEW,ACTIVITY::=LENDING-TAKEOVER-ARRANGEMENT,EFFECTIVE.DATE::=20131001,CUSTOMER::=1058315,PRODUCT::=HL.READY.BUILT.HOUSE,PROPERTY:1:1=CUSTOMER,FIELD.NAME:1:1=OWNER:1:1,FIELD.VALUE:1:1=1058315~-~1046902,FIELD.NAME:1:2=TEST,FIELD.VALUE:1:2=1</MSG>";
        //    //var typical = new MetadataTypical
        //    //{
        //    //    CatalogName = "Any",
        //    //    Name = "Any",
        //    //};
        //    //typical.AddAttribute(new TypicalAttribute("ARR.SEQUENCE", AttributeDataType.String, "", 0));
        //    //typical.AddAttribute(new TypicalAttribute("ARRANGEMENT", AttributeDataType.String, "", 0));
        //    //typical.AddAttribute(new TypicalAttribute("ACTIVITY", AttributeDataType.String, "", 0));
        //    //typical.AddAttribute(new TypicalAttribute("EFFECTIVE.DATE", AttributeDataType.String, "", 0));
        //    //typical.AddAttribute(new TypicalAttribute("CUSTOMER", AttributeDataType.String, "", 0));
        //    //typical.AddAttribute(new TypicalAttribute("PRODUCT", AttributeDataType.String, "", 0));
        //    //typical.AddAttribute(new TypicalAttribute("{CUSTOMER}.OWNER", AttributeDataType.String, "", 0));

        //    //var mvProcessor = new MultiValueShrinkStretch(typical);

        //    // FOREX postOFS FORWARD no LEGS
        //    //msg = "VA26hmpb_0|<MSG>FOREX,MIG//PROCESS,VCONN1/123456/JO0010001,,AMOUNT.BOUGHT::=80000,AMOUNT.SOLD::=14585.23,COUNTERPARTY::=150405,CPY.CORR.ADD:1:1=Bank Of America N.A,CURRENCY.BOUGHT::=DKK,CURRENCY.SOLD::=USD,DEAL.DATE::=20131001,DEAL.TYPE::=FW,DEALER.DESK::=03,DEALER.NOTES:1:1=11472522,OUR.ACCOUNT.PAY:1:1=121371,OUR.ACCOUNT.REC:1:1=121398,VALUE.DATE.BUY::=20131129,VALUE.DATE.SELL::=20131129</MSG>";
        //    //var res = mvProcessor.GetOfsMessageWithStretchedMultivalues(msg);
        //    //var res2 = mvProcessor.GetOfsMessageWithShrinkedMultivalues(res, typical);
        //    //Assert.AreEqual(res2, msg);

        //    // FOREX postOFS SWAP with 2 LEGS
        //    //msg = "VA2jIhLb_1|<MSG>FOREX,MIG//PROCESS,VCONN1/123456,,AMOUNT.BOUGHT::=3376500,CO.CODE::=JO0010001,COUNTERPARTY::=150105,CPY.CORR.ADD:1:1=HSBC-London_HSBC Bank.New York,CURRENCY.BOUGHT::=USD,DEAL.TYPE::=SW,DEALER.DESK::=03,DEALER.NOTES:1:1=11415370_11415371,OUR.ACCOUNT.PAY:1:1=121061_121282,OUR.ACCOUNT.REC:1:1=121282_121061,SPOT.RATE::=1.350600000,VALUE.DATE.BUY::=20131015_20131021,FORWARD.RATE::=_1.350630000</MSG>";
        //    //res = mvProcessor.GetOfsMessageWithStretchedMultivalues(msg);
        //    //res2 = mvProcessor.GetOfsMessageWithShrinkedMultivalues(res, typical);
        //    //Assert.AreEqual(res2, msg);



        //}

        private MetadataTypical GetAA_FIXED_FLOATING_DEP_Typical()
        {
            string typicalXsd = SamplesResponsesDir + "FinancialObjects_FIXED.FLOATING.DEP.xsd";
            MetadataTypical result = GlobusMetadataReader.GetMetadataTypicalFromXSDFile(typicalXsd);
            result.BaseClass = GlobusRequest.T24_AAProduct;
            return result;
        }
    }
}