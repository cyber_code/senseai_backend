﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using AXMLEngine;
using Validata.Common;
using ValidataCommon;

namespace OFSCommonMethods.Unit_Tests
{
    [TestFixture]
    public class TestOFSRequestGeneration
    {
        [Test]
        public void TestEnquiryConstraintsGeneration()
        {
            TestEnquiryConstraintsGeneration(",TEST:EQ=VaLuE"
                , new Filter("TEST", "VaLuE", AttributeDataType.String, FilterType.Equal));

            TestEnquiryConstraintsGeneration(",TESTA:NE=\"v 1\",TESTB:LK=v2"
                , new Filter("TESTA", "v 1", AttributeDataType.String, FilterType.NotEqual)
                , new Filter("TESTB", "v2", AttributeDataType.String, FilterType.Like));

            // Range operators (obsolete)
            TestEnquiryConstraintsGeneration(",TEST:RG=1 5"
                , new Filter("TEST", "[1 5]", AttributeDataType.String, FilterType.Like));
            TestEnquiryConstraintsGeneration(",TEST:NR=12 5555"
                , new Filter("TEST", "[12 5555]", AttributeDataType.String, FilterType.NotLike));

            // NEW METHOD TO USE EXTENDED (FROM R12) OPERATORS
            // Range operators (New)
            TestEnquiryConstraintsGeneration(",TEST:RG=1 5"
                , new Filter("TEST", "#RG 1 5", AttributeDataType.String, FilterType.Like));
            TestEnquiryConstraintsGeneration(",TEST:NR=12 5555"
                , new Filter("TEST", "#NR 12 5555", AttributeDataType.String, FilterType.Like));

            // Escape prefix
            TestEnquiryConstraintsGeneration(",TEST:LK=#NIKI"
                , new Filter("TEST", "##NIKI", AttributeDataType.String, FilterType.Like));

            // ContainsC:\Validata-Repository\src\Validata\OFSCommonMethods\Unit Tests\TestOFSRequestGeneration.cs
            TestEnquiryConstraintsGeneration(",TEST:CT=IDDQDASAS"
                , new Filter("TEST", "#CT IDDQDASAS", AttributeDataType.String, FilterType.Like));
        }

        private void TestEnquiryConstraintsGeneration(string expected, params Filter[] filters)
        {
            StringBuilder sbActual = new StringBuilder();
            GlobusRequestBase.GenerateSearchConstraint(sbActual, filters);

            Assert.AreEqual(expected, sbActual.ToString());
        }
    }
}
