using System;
using System.Collections.Generic;
using System.Xml;
using System.Configuration;

namespace OFSCommonMethods
{
    public class PhantomSetInfo
    {
        #region Private Members

        /// <summary>
        /// Phantom Set Name
        /// </summary>
        private string _Name;

        /// <summary>
        /// Data In Path
        /// </summary>
        private string _DataIn;

        /// <summary>
        /// Data Out Path
        /// </summary>
        private string _DataOut;

        /// <summary>
        /// Auth In Path
        /// </summary>
        private string _AuthIn;

        /// <summary>
        /// Auth Out Path
        /// </summary>
        private string _AuthOut;

        #endregion

        #region Public Properties

        /// <summary>
        /// Phantom Set Name
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// Data In Path
        /// </summary>
        public string DataIn
        {
            get { return _DataIn; }
            set { _DataIn = value; }
        }

        /// <summary>
        /// Data Out Path
        /// </summary>
        public string DataOut
        {
            get { return _DataOut; }
            set { _DataOut = value; }
        }

        /// <summary>
        /// Auth In Path
        /// </summary>
        public string AuthIn
        {
            get { return _AuthIn; }
            set { _AuthIn = value; }
        }

        /// <summary>
        /// Auth Out Path
        /// </summary>
        public string AuthOut
        {
            get { return _AuthOut; }
            set { _AuthOut = value; }
        }

        /// <summary>
        /// Phantom Sets File Name
        /// </summary>
        private static readonly string PHANTOM_SET_FILE_NAME = Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\Assemblies\phantom_sets.xml";
        

        /// <summary>
        /// Retrieves information about a phantom set
        /// </summary>
        /// <param name="phantomSetName">Phantom Set Name</param>
        /// <returns>Phantom Set Info object if the set is found, otherwise null</returns>
        public static PhantomSetInfo GetPhantomSetInfo(string phantomSetName)
        {
            try
            {
                List<PhantomSetInfo> phantoms = LoadPhantomSets();

                foreach (PhantomSetInfo phi in phantoms)
                {
                    if (phi.Name == phantomSetName)
                    {
                        return phi;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        /// <summary>
        /// Loads all phantom sets from the configuration file
        /// </summary>
        /// <returns></returns>
        public static List<PhantomSetInfo> LoadPhantomSets()
        {
            List<PhantomSetInfo> phiList = new List<PhantomSetInfo>();

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(PHANTOM_SET_FILE_NAME);

                XmlNodeList nodes = doc.SelectNodes("/phantomSets/phantomSet");

                foreach (XmlNode node in nodes)
                {
                    PhantomSetInfo phi = new PhantomSetInfo();

                    phi.Name = node.Attributes["name"].Value;
                    phi.DataIn = node.Attributes["root"].Value + node.Attributes["dataIn"].Value;
                    phi.DataOut = node.Attributes["root"].Value + node.Attributes["dataOut"].Value;
                    phi.AuthIn = node.Attributes["root"].Value + node.Attributes["authIn"].Value;
                    phi.AuthOut = node.Attributes["root"].Value + node.Attributes["authOut"].Value;

                    phiList.Add(phi);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error loading phantom sets", ex);
            }

            return phiList;
        }

        #endregion
    }
}