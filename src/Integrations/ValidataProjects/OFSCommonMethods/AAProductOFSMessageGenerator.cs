﻿using System;
using System.Collections.Generic;
using AXMLEngine;
using ValidataCommon;

namespace OFSCommonMethods
{
    /*
               See mozhesh da pravish
               Dokolkoto znam Authorize se pravi prosto kato izvikash AA.ARRANGEMENT A i podadesh Arrangement IDto
               Mislja che trjabva da se izpolzva ne @ID to na AA.ARRANGEMENT.ACTIVITY koeto si izvikal za da go suzdadesh a drugoto ID koeto se vrushta kato chast ot response-a
               Naprimer
               <requests><request>AAACT100060XJXS0S1//1,ARRANGEMENT:1:1=AA10006NCFBZ,
               Purvoto @ID e idto na operatsijat kojato e suzdala arrangement-a
               AA10006NCFBZ
                   */

    //  AA.ARRANGEMENT S ArrangementID
    //  Ne sum chuval da operatsija V(erify) ili VALIDATE
    //  Za Validate mislja che se polzva simulatora

    public static class AAProductOFSMessageGenerator
    {
        #region Private Helper Classes

        private class AAProperty
        {
            internal readonly string Name;

            internal readonly int Index;

            internal readonly InstanceAttributesCollection Fields = new InstanceAttributesCollection();

            internal bool IsNoProperty
            {
                get { return string.IsNullOrEmpty(Name); }
            }

            internal AAProperty(string name, int index)
            {
                Name = name;
                Index = index;
            }

            internal void AddField(string name, string value)
            {
                Fields.Add(new InstanceAttribute(name, value));
            }
        }

        private class AAPropertyCollection
        {
            private List<AAProperty> _Properties = new List<AAProperty>();

            private Dictionary<string, AAProperty> _PropertyMap = new Dictionary<string, AAProperty>();

            internal IEnumerable<AAProperty> List
            {
                get { return _Properties; }
            }

            internal AAProperty Find(string propertyName)
            {
                return _PropertyMap.ContainsKey(propertyName) ?
                    _PropertyMap[propertyName] :
                    null;
            }

            internal AAProperty Add(string propertyName)
            {
                AAProperty result = new AAProperty(propertyName, _Properties.Count);
                _PropertyMap.Add(propertyName, result);
                _Properties.Add(result);

                return result;
            }
        }

        #endregion

        #region Public Methods

        public static string GenerateRequest(DataProcessType dataProcessType, string commandUser, string commandPassword, string company, string version, bool isValidate, Instance instance, string msgId = "")
        {
            System.Diagnostics.Debug.Assert(dataProcessType != DataProcessType.None, "DataProcessType should be defined");

            System.Diagnostics.Debug.Assert(dataProcessType != DataProcessType.CreateAndAuthorize
                , "Unable to use mixed IA at this level, please split it in the caller method");

            if (instance == null)
                throw new ApplicationException("Instance not set for a data process command!");

            OFSStringBuilder ofsCommand = new OFSStringBuilder();
            ofsCommand.Append("AA.ARRANGEMENT.ACTIVITY");
            ofsCommand.Append(',');
            if (IsValidAttributeValue(version))
            {
                ofsCommand.Append(version);
            }

            ofsCommand.Append('/');
            ofsCommand.Append(GetOFSCommandFromDataProcessType(dataProcessType));
            ofsCommand.Append('/');
            ofsCommand.Append(isValidate ? "VALIDATE" : "PROCESS");
            ofsCommand.Append(',');

            ofsCommand.Append(UsefieldHelper.GetSignonUser(commandUser, instance));
            ofsCommand.Append('/');
            ofsCommand.Append(UsefieldHelper.GetSignonPassword(commandPassword, instance));

            if (!string.IsNullOrEmpty(company))
            {
                ofsCommand.Append('/');
                ofsCommand.Append(UsefieldHelper.GetCompany(company, instance));
            }

            ofsCommand.Append(',');

            string t24ID = instance.GetAttributeValue("@ID");
            if (IsValidAttributeValue(t24ID))
            {
                ofsCommand.AppendID(t24ID);
            }

            if (!string.IsNullOrEmpty(msgId))
            {
                ofsCommand.Append("/" + msgId);
            }

            if (dataProcessType != DataProcessType.Create)
            {
                // For those process types we don't need to send the rest of the attributes
                // Only create/update requires full list of attributes
                return ofsCommand.ToString();
            }

            AAPropertyCollection properties = GroupAttributesByProperty(instance.Attributes);

            foreach (AAProperty property in properties.List)
            {
                if (property.IsNoProperty)
                {
                    if (string.IsNullOrEmpty(instance.TypicalName))
                        throw new Exception("Internal Exception: please populate typical name of the passed instance");

                    AppendNoPropertyFields(ofsCommand, property.Fields, instance.TypicalName);
                }
                else
                {
                    AppendPropertyFields(ofsCommand, property);
                }
            }

            return ofsCommand.ToString();
        }

        #endregion

        #region Private Methods

        private static void AppendNoPropertyFields(OFSStringBuilder ofsCommand, InstanceAttributesCollection instanceAttributesCollection, string typicalName)
        {
            /*
                "ARRANGEMENT"
                "ACTIVITY"
                "EFFECTIVE.DATE"
                "CUSTOMER"
                "PRODUCT"
                "CURRENCY"
            */
            var mandatoryFieldDefaults = new Dictionary<string, string>
                    {
                        {"ARRANGEMENT", "NEW"},
                        {"PRODUCT", typicalName},
                    };

            foreach (string fieldName in mandatoryFieldDefaults.Keys)
            {
                InstanceAttribute valueOverride = instanceAttributesCollection.Find(fieldName);
                if(valueOverride == null || !IsValidAttributeValue(valueOverride.Value))
                {
                    ofsCommand.Append(',');
                    FormatFieldNameAndValue(ofsCommand, fieldName, mandatoryFieldDefaults[fieldName]);
                }
            }

            foreach (InstanceAttribute ia in instanceAttributesCollection)
            {
                if (ia.Name == "ID" || ia.Name == "@ID")
                {
                    continue;
                }

                if (mandatoryFieldDefaults.ContainsKey(ia.Name) && !IsValidAttributeValue(ia.Value))
                {
                    // allready added from mandatories
                    continue;
                }

                ofsCommand.Append(',');
                FormatFieldNameAndValue(ofsCommand, ia.Name, ia.Value);
            }
        }

        private static void AppendPropertyFields(OFSStringBuilder ofsCommand, AAProperty aaProperty)
        {
            if(aaProperty.Fields.Count == 0)
            {
                System.Diagnostics.Debug.Fail(
                        "Property contains no fields - is it notmall or not?\r\n"+
                        "The property would not be appended to the final OFS message!"
                    );
            }

            ofsCommand.Append(',');

            bool isMultivalue = AttributeNameParser.IsMultiValue(aaProperty.Name);
            if (!isMultivalue)
            {
                FormatFieldNameAndValue(ofsCommand, "PROPERTY", aaProperty.Name, true, aaProperty.Index, 1);
            }
            else
            {
                string propertyName = AttributeNameParser.GetShortFieldName(aaProperty.Name);
                FormatFieldNameAndValue(ofsCommand, "PROPERTY", propertyName, true, aaProperty.Index, 1);
            }

            InstanceAttribute effective = isMultivalue ? aaProperty.Fields.Find("EFFECTIVE") : null;

            if (effective != null)
            {
                if (IsValidAttributeValue(effective.Value))
                {
                    ofsCommand.Append(',');
                    FormatFieldNameAndValue(ofsCommand, "EFFECTIVE", effective.Value, true, aaProperty.Index, 1);
                }
            }

            int fieldIdx = 1;
            foreach (InstanceAttribute ia in aaProperty.Fields)
            {
                if (isMultivalue && ia.Name == "EFFECTIVE")
                {
                    // skip - it is added above
                    continue;
                }

                if (ia.Name == "@ID")
                {
                    continue;
                }

                if (!IsValidAttributeValue(ia.Value))
                {
                    continue;
                }

                ofsCommand.Append(',');
                FormatFieldNameAndValue(ofsCommand, "FIELD.NAME", GetFormatedFieldName(ia.Name), true, aaProperty.Index, fieldIdx);

                ofsCommand.Append(',');
                FormatFieldNameAndValue(ofsCommand, "FIELD.VALUE", ia.Value, true, aaProperty.Index, fieldIdx);

                fieldIdx++;
            }
        }

        private static AAPropertyCollection GroupAttributesByProperty(InstanceAttributesCollection instanceAttributesCollection)
        {
            // Normally they should be grouped, but do it to be sure for further logic

            AAPropertyCollection result = new AAPropertyCollection();
            result.Add(""); // add master since it must be presented for auto - gen values

            foreach (InstanceAttribute ia in instanceAttributesCollection)
            {
                if (ia.Name == "ID" || ia.Name == "@ID" || !IsValidAttributeValue(ia.Value))
                    continue;

                string propertyName;
                string fieldName;
                AAAttributeName.SplitAttributeName(ia.Name, out propertyName, out fieldName);

                if (propertyName == null)
                    propertyName = string.Empty;

                AAProperty aaProperty = result.Find(propertyName);
                if(aaProperty == null)
                    aaProperty = result.Add(propertyName);

                aaProperty.AddField(fieldName, ia.Value);
            }

            return result;
        }

        private static string GetFormatedFieldName(string validataAttributeName)
        {
            string fieldName;
            int multiValueIndex, subValueIndex;
            bool isMultivalue = AttributeNameParser.ParseComplexFieldName(validataAttributeName, out fieldName, out multiValueIndex, out subValueIndex);

            OFSStringBuilder sbResult = new OFSStringBuilder();

            sbResult.Append(fieldName);
            if (isMultivalue)
            {
                sbResult.Append(':');
                sbResult.Append(multiValueIndex);
            }
            
            if (isMultivalue)
            {
                sbResult.Append(':');
                sbResult.Append(subValueIndex);
            }

            return sbResult.ToString();
        }

        private static void FormatFieldNameAndValue(OFSStringBuilder ofsCommand, string validataAttributeName, string fieldValue)
        {
            string fieldName;
            int multiValueIndex, subValueIndex;
            bool isMultivalue = AttributeNameParser.ParseComplexFieldName(validataAttributeName, out fieldName, out multiValueIndex, out subValueIndex);

            FormatFieldNameAndValue(ofsCommand, fieldName, fieldValue, isMultivalue, multiValueIndex, subValueIndex);
        }

        private static void FormatFieldNameAndValue(OFSStringBuilder ofsCommand, string fieldName, string fieldValue, bool isMultivalue, int multiValueIndex, int subValueIndex)
        {
            ofsCommand.Append(fieldName);
            ofsCommand.Append(':');
            if (isMultivalue)
            {
                ofsCommand.Append(multiValueIndex);
            }
            ofsCommand.Append(':');
            if (isMultivalue)
            {
                ofsCommand.Append(subValueIndex);
            }

            ofsCommand.Append('=');

            ofsCommand.Append(fieldValue);
        }

        private static string GetOFSCommandFromDataProcessType(DataProcessType dataProcessType)
        {
            switch (dataProcessType)
            {
                case DataProcessType.Create:
                    return "I";
                case DataProcessType.Select:
                    return "S";
                case DataProcessType.Delete:
                    return "D";
                case DataProcessType.Reverse:
                    return "R";
                case DataProcessType.Authorize:
                    return "A";
                case DataProcessType.Verify:
                    return "V";
                default:
                    System.Diagnostics.Debug.Fail("Invalid data proces type for input OFS message");
                    return "";
            }
        }

        private static bool IsValidAttributeValue(string value)
        {
            return 
                value != null && 
                value.Trim() != string.Empty && 
                value.Trim().ToUpper() != "NO VALUE";
        }

        #endregion
    }
}
