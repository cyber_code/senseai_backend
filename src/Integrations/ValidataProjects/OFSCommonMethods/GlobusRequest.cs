using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using AXMLEngine;
using OFSCommonMethods.IO;
using OFSCommonMethods.IO.Telnet;
using OFSCommonMethods.OFSEnquiry;

namespace OFSCommonMethods
{
    /// <summary>
    /// This class encloses functionality and state information for one Globus request
    /// </summary>
    public class GlobusRequest : GlobusRequestBase
    {
        #region Enumerations

        /// <summary>
        /// Enumerates the error states for GlobusRequest objects
        /// </summary>
        public enum ErrorState
        {
            Globus,
            TimeOut,
            Undefined
        }

        #endregion

        #region Private Members

        /// <summary>
        /// True if the request has warning
        /// </summary>
        private bool _IsWarning;

        #endregion

        #region Public Properties

        /// <summary>
        /// True if the request has warning.
        /// </summary>
        public bool IsWarning
        {
            get { return _IsWarning; }
            set { _IsWarning = value; }
        }

        public string TransactionID
        {
            get { return _TransactionID; }
            set { _TransactionID = value; }
        }

        public bool IsAAProcessDetails { get; set; }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Create a GlobusRequest object instance.
        /// </summary>
        /// <param name="typical">The typical for this request. The name of the typical is also the 
        /// name of the Globus enquiry, data table or application</param>
        /// <param name="instance">The AXML instance that this request was created for.</param>
        /// <param name="reqType">The type of the request.</param>
        /// <param name="dpType">The data processing type for this request.</param>
        /// <param name="version">The version to be used.</param>
        /// <param name="company">Company</param>
        public GlobusRequest(MetadataTypical typical, Instance instance, RequestType reqType, DataProcessType dpType, string version, string company)
            : base(typical, instance, reqType, dpType, version, company, null)
        {
        }

        /// <summary>
        /// Create a GlobusRequest object instance.
        /// </summary>
        /// <param name="typical">The typical for this request. The name of the typical is also the name of the Globus enquiry, data table or application</param>
        /// <param name="instance">The AXML instance that this request was created for.</param>
        /// <param name="reqType">The type of the request.</param>
        /// <param name="dpType">The data processing type for this request.</param>
        /// <param name="version">The version to be used.</param>
        /// <param name="company">Company</param>
        /// <param name="phantomSet">Name of phantom set/telnet set</param>
        public GlobusRequest(MetadataTypical typical, Instance instance, RequestType reqType, DataProcessType dpType, string version, string company, string phantomSet)
            : this(typical, instance, reqType, dpType, version, company)
        {

            this._PhantomSet = phantomSet;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobusRequest"/> class.
        /// </summary>
        /// <param name="shortRequest">The short request.</param>
        /// <param name="typical">The typical for this request. The name of the typical is also the name of the Globus enquiry, data table or application.</param>
        /// <param name="version">The version.</param>
        /// <param name="company">The company.</param>
        public GlobusRequest(GlobusRequestShort shortRequest, MetadataTypical typical, string version, string company)
            : base(typical, null, shortRequest.RequestType, shortRequest.DataProcessType, version, company, null)
        {
            _State = shortRequest.ExecutionState;

            if (!string.IsNullOrEmpty(shortRequest.ValidataID))
            {
                _ValidataID = shortRequest.ValidataID;
            }
        }
        public static Instance MultivaluePreProcessInstance(MetadataTypical requestTypical, Instance instance)
        {
            if (instance == null)
                return null;

            var multiValueShrinkStretch = MultiValueShrinkStretchCache.GetByTypical(requestTypical);
            return multiValueShrinkStretch != null && multiValueShrinkStretch.HasMVToParse
                ? multiValueShrinkStretch.SplitMultiValues(instance)
                : instance;
        }

        protected override Instance PreProcessInstance(MetadataTypical requestTypical, Instance instance)
        {
            return MultivaluePreProcessInstance(requestTypical, instance);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Attaches to the GlobusRequest the OFS response to the OFS message produced by this
        /// instance.
        /// </summary>
        /// <param name="responseOfsMessage">The text of the OFS response.</param>
        /// <param name="responseTypical">The collection of typicals used.</param>
        /// <param name="axmlWriter">AXML writer for enquiry response</param>
        /// <param name="logger">Pointer to the logging function</param>
        /// <param name="treatOFSResponseSingleValueAsError">Define to treat OFS Response single value as error or not</param>
        /// <param name="enquiryResultParsingRoutine">The enquire parsing routine</param>
        /// <param name="ofsType"></param>
        /// <param name="useOldHistoryReadResponse"></param>
        /// <returns>The resulting ArrayList is null if DataProcessType.None.</returns>
        public List<string> SetResponse(GlobusMessage.OFSMessage responseOfsMessage,
                                        MetadataTypical responseTypical,
                                        XmlWriter axmlWriter,
                                        TSSTelnetComm.LogMessageHandle logger,
                                        bool treatOFSResponseSingleValueAsError,
                                        EnquiryResultParsingRotine enquiryResultParsingRoutine,
                                        Settings.OFSType ofsType,
                                        bool useOldHistoryReadResponse = false,
                                        List<Filter> filters = null)
        {
            System.Diagnostics.Debug.Assert(enquiryResultParsingRoutine != null);

            if (ofsType == Settings.OFSType.OFSML)
            {
                if (TypeOfRequest == RequestType.Enquiry ||
                    TypeOfRequest == RequestType.History)
                {
                    return SetOfsmlEnquiryResponse(responseOfsMessage.Text, enquiryResultParsingRoutine);
                }

                SetOfsmlTransactionResponse(responseOfsMessage.Text);
                return new List<string>();
            }

            List<string> results = null;
            try
            {
                if (_State == ExecutionState.WaitingForProcessing)
                {
                    if (_DataProcessType != DataProcessType.None)
                    {
                        OFSResponseReader reader = new OFSResponseReader(_SpecialTypicalAttributes, _ValidataID) { IsAAProcessDetails = this.IsAAProcessDetails };

                        _State = SetOfsDataProcessResponse(reader, responseOfsMessage, responseTypical ?? _ResponseTypical);
                    }
                    else // DataProcessType.None - for import History and Enquiries
                    {
                        OFSResponseReader reader = new OFSResponseReader(_PhantomSet, _SpecialTypicalAttributes) { IsAAProcessDetails = this.IsAAProcessDetails };

                        if (_RequestType == RequestType.History)
                        {
                            results = useOldHistoryReadResponse
                                ? reader.ReadHistoryResponseOld(responseOfsMessage.Text)
                                : reader.ReadHistoryResponse(responseOfsMessage.Text);
                        }
                        else
                        {
                            if (responseTypical == null)
                                responseTypical = _ResponseTypical;

                            System.Diagnostics.Debug.Assert(axmlWriter != null);
                            _Result = reader.ReadEnquiryResponse(this.RequestTypicalName,
                                                                 responseOfsMessage.Text,
                                                                 responseTypical,
                                                                 axmlWriter,
                                                                 logger,
                                                                 treatOFSResponseSingleValueAsError,
                                                                 enquiryResultParsingRoutine,
                                                                 filters);

                            // form the list of ids?
                            // return 

                            if (String.IsNullOrEmpty(_Result)
                                && !String.IsNullOrEmpty(responseOfsMessage.Text))
                            {
                                _Result = responseOfsMessage.Text;
                            }
                        }

                        _Failed = reader.HasError;
                        _State = ExecutionState.Finished;
                    }
                }
                else if (_State == ExecutionState.WaitingForAuthorization)
                {
                    OFSResponseReader reader = new OFSResponseReader(_SpecialTypicalAttributes, _ValidataID) { IsAAProcessDetails = this.IsAAProcessDetails };

                    SetOfsAuthorizationResponse(reader, responseOfsMessage);

                    _State = ExecutionState.Finished;
                }
            }
            catch (EnquiryConfigurationException)
            {
                throw;
            }
            catch (Exception e)
            {
                LogParsingExceptionInfo(responseOfsMessage.ToLongString(), e);
            }

            return results;
        }

        /// <summary>
        /// Set response as error initialized from exception
        /// </summary>
        /// <param name="ex">Exception</param>
        public void SetErrorResponse(Exception ex)
        {
            LogExceptionInfo("", ex.Message, ex);
        }

        /// <summary>
        /// Generates the text of an OFS message based on the type and settings of this GlobusRequest instance.
        /// </summary>
        /// <param name="user">The name of the Globus user to be used in the OFS message.</param>
        /// <param name="password">The password for the user.</param>
        /// <param name="authorizeUser">User name for authorization</param>
        /// <param name="authorizePassword">User password for authorization</param>
        /// <param name="filters">The colection of filtering rules that must be added to an enquiry request.</param>
        /// <param name="waitingForAuthorization">Is waiting for authorization</param>
        /// <param name="isValidate">Is in Validation mode</param>
        /// <param name="isImpTestData">Is in Import Test Data mode</param>
        /// <param name="authenticateAlways">Authenticate always</param>
        /// <param name="ofsType"></param>
        /// <returns>The text of the OFS message.</returns>
        public string GenerateMessageText(string user,
                                          string password,
                                          string authorizeUser,
                                          string authorizePassword,
                                          List<Filter> filters,
                                          bool waitingForAuthorization,
                                          bool isValidate,
                                          bool isImpTestData,
                                          bool authenticateAlways,
                                          Settings.OFSType ofsType)
        {
            if (ofsType == Settings.OFSType.OFSML)
            {
                return GenerateOfsmlRequest(user, password, authorizeUser, authorizePassword, filters, isValidate);
            }
            else
            {
                return GenerateOfsRequest(user, password, authorizeUser, authorizePassword, filters, authenticateAlways, !isImpTestData, isValidate, waitingForAuthorization);
            }
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Create Globus Requests
        /// </summary>
        /// <param name="instances"> XML string containing instances </param>
        /// <param name="requestTypical"> Request typical </param>
        /// <param name="responseTypical"> Response typical </param>
        /// <param name="configuration"> Configuration parameter </param>
        /// <param name="version"></param>
        /// <param name="company"></param>
        /// <param name="catalogs"></param>
        /// <param name="flagNeedsAuthorization"></param>
        public static GlobusRequestCollection CreateGlobusRequests(InstanceCollection instances,
                                                                   MetadataTypical requestTypical,
                                                                   MetadataTypical responseTypical,
                                                                   string configuration,
                                                                   string version,
                                                                   string company,
                                                                   Hashtable catalogs,
                                                                   out bool flagNeedsAuthorization)
        {
            GlobusRequestCollection globusRequests = new GlobusRequestCollection();
            int numProcessed = 0;
            // we will set up DataProcessType according to configuration parameter
            DataProcessType dpType = GetDataProcessType(configuration);

            flagNeedsAuthorization = (configuration.IndexOf('A') >= 0);

            foreach (Instance inst in instances)
            {
                MetadataTypical typ;
                RequestType reqType;
                GetRequestType(catalogs, inst, out typ, out reqType);

                // this is necessary, because for Enquiry types, we don't need instances tag, but it is prepared during forward transformation and we have to skip it
                if (reqType == RequestType.Enquiry)
                {
                    break;
                }

                GlobusRequest request = new GlobusRequest(typ, inst, reqType, dpType, version, company);

                // Set up Authorization - if needed
                request.NeedsAuthorize = flagNeedsAuthorization;

                globusRequests.AddGlobusRequest(request);

                numProcessed++;
            }

            if (numProcessed == 0)
            {
                GlobusRequest request;
                if (configuration == "HIS" || configuration == "LIVE")
                {
                    request = new GlobusRequest(requestTypical, null, RequestType.History, dpType, version, company);
                }
                else
                {
                    RequestType reqType = GetMessageType(requestTypical);
                    request = new GlobusRequest(requestTypical, null, reqType, dpType, version, company);
                }
                request.ResponseTypical = responseTypical;

                globusRequests.AddGlobusRequest(request);
            }

            return globusRequests;
        }

        public static RequestType GetRequestType(Hashtable catalogs, Instance inst, out MetadataTypical typ, out RequestType reqType)
        {
            typ = FindTypicalByNameAndCatalog(catalogs, inst.TypicalName, inst.Catalog);
            reqType = GetMessageType(typ);
            return reqType;
        }

        /// <summary>
        /// Finds the metadata typical for an instance.
        /// </summary>
        /// <param name="catalogsAndTypicals">The catalogs.</param>
        /// <param name="typicalName">The name of the typical.</param>
        /// <param name="catalogName">the name of the catalogue.</param>
        /// <returns></returns>
        public static MetadataTypical FindTypicalByNameAndCatalog(Hashtable catalogsAndTypicals, string typicalName, string catalogName)
        {
            Hashtable typicalHash = (Hashtable)catalogsAndTypicals[catalogName];
            MetadataTypical typ = (MetadataTypical)typicalHash[typicalName];
            return typ;
        }

        #endregion
    }
}
