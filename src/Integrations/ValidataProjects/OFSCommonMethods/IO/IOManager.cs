﻿using System;
using System.Collections;
using System.Threading;
using OFSStringConvertor;

namespace OFSCommonMethods.IO
{
    public abstract class IOManager
    {
        #region Private Members

        /// <summary>
        /// This hashtable is used to record the response times of GLobus to all messages
        /// </summary>
        private Hashtable _MessageTimes = new Hashtable();

        #endregion

        #region Public Static Members

        /// <summary>
        /// The encoding used for reading Globus OFS 
        /// </summary>
        public static GlobusEncoding EncodingUsed = GlobusEncoding.UTF8;

        #endregion

        #region Protected Static Members

        /// <summary>
        /// The key is the message ID, the value is an AutoResetEvent
        /// </summary>
        protected static Hashtable _ThreadEvents = new Hashtable();

        #endregion

        #region Public Properties

        /// <summary>
        /// Returns the collection of repsonse times for messages
        /// </summary>
        public Hashtable MessageTimes
        {
            get { return _MessageTimes; }
        }

        /// <summary>
        /// Type of the io manager
        /// </summary>
        public abstract Settings.IOType Type
        {
            get;
        }

        /// <summary>
        /// tSS reconnection is required for T24 unit testing execution when authorizing transaction
        /// </summary>
        public bool RequireTSSReconnection { get; set; }

        #endregion

        /// <summary>
        /// Writes an OFS message to Globus and returns.
        /// </summary>
        /// <param name="isAuthorize">Is Authorize</param>
        /// <param name="gm">Globus Message</param>
        public abstract void WriteMessage(bool isAuthorize, GlobusMessage gm);

        /// <summary>
        /// Writes message and returns result
        /// </summary>
        /// <param name="inputT24Message"></param>
        /// <param name="outpuT24Message"></param>
        /// <param name="telnetComm"></param>
        public abstract void WriteMessage(GlobusMessage inputT24Message, GlobusMessage outpuT24Message, OFSCommonMethods.IO.Telnet.TSSTelnetComm telnetComm);
        

        /// <summary>
        /// Writes an OFS message to Globus and returns.
        /// </summary>
        /// <param name="isAuthorize">Is Authorize</param>
        /// <param name="gm">Globus Message</param>
        public abstract void WriteMessageBulk(bool isAuthorize, GlobusMessage gm);

        /// <summary>
        /// Gets the messages that were received for the thread with the
        /// given guid.
        /// </summary>
        /// <param name="guid">The guid of the thread that waits for the messages</param>
        /// <returns>A Hashtable of messages found in the message results.</returns>
        public abstract Hashtable GetMessages(string guid);

        /// <summary>
        /// Reads all files from the output Globus directory
        /// </summary>
        /// <param name="isAuthorize">If true read from the authorize path, else 
        /// read from the data path</param>
        public abstract void ReadResults(bool isAuthorize);

        /// <summary>
        /// Reads all files from the output Globus directory
        /// </summary>
        /// <param name="isAuthorize">If true read from the authorize path, else read from the data path</param>
        /// <param name="request"></param>
        /// <param name="requestGuid"></param>
        public abstract GlobusMessage GetDataResult(bool isAuthorize, GlobusRequestShort request, string requestGuid);

        #region Protected Methods

        protected void PushMessageTime(string key)
        {
            PushMessageTime(key, false);
        }

        protected void PushMessageTime(string key, bool isPhantoms)
        {
            lock (_MessageTimes.SyncRoot)
            {
                if (_MessageTimes.ContainsKey(key))
                {
                    if (_MessageTimes[key] is DateTime)
                    {
                        DateTime dtWrite = (DateTime)_MessageTimes[key];
                        DateTime dtRead = DateTime.Now;

                        _MessageTimes[key] = dtRead.Ticks - dtWrite.Ticks;
                    }
                }
                else
                {
                    if (isPhantoms)
                    {
                        //it theoretically possible in a multithreaded environment
                        //that reading the response can preceed the proper registering of the write
                        //in this case we simpli set 0 for response time
                        //  (for File Based Communication)
                        _MessageTimes[key] = (long)0;
                    }
                    else
                    {
                        _MessageTimes[key] = DateTime.Now;
                    }
                }
            }
        }

        protected static void GetMessageInfo(string msgId, out string guid, out string id)
        {
            int pos = msgId.IndexOf('_');
            if (pos < 0)
            {
                id = "0";
                guid = msgId;
            }
            else
            {
                guid = msgId.Substring(0, pos);
                id = msgId.Substring(pos + 1);
            }
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Creates a new guid.
        /// </summary>
        /// <returns>A string representing the created GUID.</returns>
        public static string CreateGUID()
        {
            return Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Creates a new guid and registers the thread using it as its identifier.
        /// </summary>
        /// <param name="threadEvent">The event that will be used to wake the thread</param>
        /// <returns>A string representing the created GUID.</returns>
        public static string RegisterThread(AutoResetEvent threadEvent)
        {
            string result;
            lock (typeof(FileBasedIOManager))
            {
                result = CreateGUID();

                //store the correspondence between the guid and the event
                //later, when a message is read for this guid the 
                _ThreadEvents[result] = threadEvent;
            }
            return result;
        }

        public static string RegisterThread()
        {
            string result = CreateGUID();
            
            return result;
        }

        #endregion
    }
}
