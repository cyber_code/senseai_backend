using System.Collections.Generic;

namespace OFSCommonMethods.IO
{
    public class Routine
    {
        public enum CallArgumentType
        {
            /// <summary>
            /// Directly writing the OFS message in the command line (legacy approach)
            /// </summary>
            UsingOFS,

            /// <summary>
            /// Using a file with the OFS command (the preferred method, since it doesn't have the limitation of 
            /// </summary>
            UsingFile
        }

        /// <summary>
        /// The sequences of actions 
        /// </summary>
        public enum SequenceToA
        {
            /// <summary>
            /// Explicat authorization is necessary
            /// </summary>
            A,

            /// <summary>
            /// Both explicit input and authorization are necessary (e.g. when the record has been created in IHLD)
            /// </summary>
            IA,

            /// <summary>
            /// No further action is necessary, the routine should have authorized the records
            /// </summary>
            Nothing
        }

        public string Name { get; set; }

        public bool EnableSmartOFS { get; set; }

        public CallArgumentType CallType { get; set; }

        public SequenceToA PostCallActions { get; set; }

        public Routine(string name, bool enableSmartOFS, CallArgumentType callType, SequenceToA postCallActions)
        {
            Name = name;
            EnableSmartOFS = enableSmartOFS;
            CallType = callType;
            PostCallActions = postCallActions;
        }

        public static CallArgumentType CallArgumentTypeFromString(string value)
        {
            if (value != null && value.ToUpper() == "OFS")
            {
                return CallArgumentType.UsingOFS;
            }

            return CallArgumentType.UsingFile;
        }
    }

    public class RoutinesSettings : Dictionary<string, Routine>
    {
        #region Class Lifecycle

        internal RoutinesSettings()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve name of T24 routine from application name
        /// </summary>
        /// <param name="applicationName">T24 Application Name</param>
        /// <returns>Name of routine if it is defined. Otherwise null</returns>
        public string GetRoutineNameByApplication(string applicationName)
        {
            return ContainsKey(applicationName) ? this[applicationName].Name : null;
        }

        /// <summary>
        /// Retrieve the T24 routine from application name
        /// </summary>
        /// <param name="applicationName">T24 Application Name</param>
        /// <returns>Name of routine if it is defined. Otherwise null</returns>
        public Routine GetRoutineByApplication(string applicationName)
        {
            return ContainsKey(applicationName) ? this[applicationName] : null;
        }

        #endregion
    }
}