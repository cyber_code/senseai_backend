﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods.IO
{
    public class OFSResultHandlingSettings
    {
        private List<string> _Actions = new List<string>();

        private List<string> _Versions = new List<string>();

        private List<EndOfResponseCondition> _EndOfResponseConditions =
            new List<EndOfResponseCondition>();

        private List<CustomCommandResult> _CommandResults = 
            new List<CustomCommandResult>();

        public int Count
        {
            get {
                System.Diagnostics.Debug.Assert(
                        _Actions.Count == _Versions.Count &&
                        _Versions.Count == _EndOfResponseConditions.Count &&
                        _EndOfResponseConditions.Count == _CommandResults.Count
                    );

                return _Actions.Count; 
            }
        }

        public void Add(string action, string version, CustomCommandResult commandResult, EndOfResponseCondition endOfResponseConditions)
        {
            _Actions.Add(action);
            _Versions.Add(version);
            _CommandResults.Add(commandResult);
            _EndOfResponseConditions.Add(endOfResponseConditions);
        }

        public void Clear()
        {
            _Actions = new List<string>();
            _Versions = new List<string>();
            _EndOfResponseConditions = new List<EndOfResponseCondition>();
            _CommandResults = new List<CustomCommandResult>();
        }

        public EndOfResponseCondition GetEndOfResponseCondition(string action, string version)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (_Actions[i] == action && _Versions[i] == version)
                {
                    return _EndOfResponseConditions[i];
                }
            }

            return null;
        }

        public CustomCommandResult GetCustomCommandResult(string action, string version)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (_Actions[i] == action && _Versions[i] == version)
                {
                    return _CommandResults[i];
                }
            }

            return null;
        }
    }
}
