﻿using System.Collections.Generic;
using System.Diagnostics;

namespace OFSCommonMethods.IO
{
    public static class JShellCommandHelper
    {
        // NOTE: Some commends are commented out to allow them being optional (i.e. if not specified in the environment set to consider they should not be used)
        private static readonly Dictionary<JShellCommandType, JShellCommand> _DefaultCommands
            = new Dictionary<JShellCommandType, JShellCommand>
                  {
                      {JShellCommandType.GetWorkingDirectory, new JShellCommand("pwd", null)},
                      {JShellCommandType.GetEnvironmentVariable, new JShellCommand("echo ${0}", null)},
                      {JShellCommandType.AnalyzeCompilation, new JShellCommand("JSHOW -c {0}", null)},
                      {JShellCommandType.Compile, new JShellCommand("BASIC -IGLOBUS.BP {0} {1}", ".*Source file .* compiled successfully")},
                      //{JShellCommandType.CompileAndCatalog, new JShellCommand("EB.COMPILE {0} {1}", ".*Source file .* compiled successfully")},
                      {JShellCommandType.Catalog, new JShellCommand("CATALOG {0} {1}", "Object {0} cataloged successfully")},
                      {JShellCommandType.Decatalog, new JShellCommand("DECATALOG {0} {1}", ".*decataloged successfully.*")},
                      {JShellCommandType.DeleteDollarFile, new JShellCommand("DELETE {0} ${1}", ".*deleted\\.")},
                      {JShellCommandType.CopyFileToCatalog, new JShellCommand("COPY FROM . TO {0} {1}", null)},
                      //{JShellCommandType.ListCatalog, new JShellCommand("LIST {0}", null)},
                      {JShellCommandType.ListVOCRecord, new JShellCommand("CT VOC {0}", null)},
                      {JShellCommandType.DeleteRecord, new JShellCommand("DELETE {0} {1}", "1 record\\(s\\) deleted\\.")},
                      {JShellCommandType.CopyRecord, new JShellCommand("COPY FROM {0} TO {1} {2},{3} OVERWRITING", "1 records copied")},
                      {JShellCommandType.JShow, new JShellCommand("jshow {0} {1}", null)}
                  };

        public static void FillMissingCommandsWithDefaultText(Dictionary<JShellCommandType, JShellCommand> commands)
        {
            foreach (KeyValuePair<JShellCommandType, JShellCommand> cmd in _DefaultCommands)
            {
                if(! commands.ContainsKey(cmd.Key) || (commands[cmd.Key] == null))
                {
                    Debug.WriteLine(string.Format("Set default value for {0} = {1}", cmd.Key, cmd.Value.Command));
                    commands[cmd.Key] = cmd.Value; 
                }
                else
                {
                    JShellCommand curCmd = commands[cmd.Key];
                    if (string.IsNullOrEmpty(curCmd.SuccessResponseRegex))
                    {
                        curCmd.SuccessResponseRegex = cmd.Value.SuccessResponseRegex;
                    }
                }
                
            }
        }
    }
}
