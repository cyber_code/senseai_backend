﻿using System.Text.RegularExpressions;

namespace OFSCommonMethods.IO
{
    public class CustomCommandResult
    {
        /// <summary>
        /// The name of the command (just for the UI)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Should apply regex verifications
        /// </summary>
        public bool IsRegEx { get; set; }

        /// <summary>
        /// Is the verification a success condition (or a failure condition)
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Is the verification based on the part of the text or on the entire text
        /// </summary>
        public bool IsPartial { get; set; }

        /// <summary>
        /// The verification text or regex text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// The type of command
        /// </summary>
        public ConsoleType Interface { get; set; }

        public string Summary
        {
            get
            {
                return string.Format("rule \"{0}\" {1} {2} : \"{3}\"",
                                     Name,
                                     IsSuccess ? " for" : " for not",
                                     TextMatchingMode,
                                     Text);
            }
        }

        private string TextMatchingMode
        {
            get
            {
                if (IsRegEx)
                {
                    return IsPartial ? "matching a regular expression with the pattern" : "fully matching a regular expression with the pattern";
                }
                else
                {
                    return IsPartial ? "containing the text" : "exactly matching the text";
                }
            }
        }

        private const string ALL_CUSTOM_COMMAND_RESULT_HANDLER = "[ALL]";

        public bool Matches(string command)
        {
            if (Name == ALL_CUSTOM_COMMAND_RESULT_HANDLER)
                return true;
            return command.StartsWith(Name);
        }

        private const string COMMAND_NAME_TO_REPLACE = "[*COMMAND*]";

        public bool Execute(string command, string response)
        {
            bool result;

            int spaceIndex = command.IndexOf(' ');
            if (Interface == ConsoleType.OFS)
                spaceIndex = command.IndexOf(',');
            string commandFirst = command;
            if (spaceIndex > 1)
                commandFirst = command.Substring(0, spaceIndex);

            string txt = Text.Replace(COMMAND_NAME_TO_REPLACE, commandFirst);

            if (IsRegEx)
            {
                var expression = new Regex(txt);
                Match match = expression.Match(response);
                result = match.Success;
                if (result && !IsPartial)
                {
                    if (match.Length != response.Length)
                        result = false;
                }
            }
            else
            {
                result = IsPartial
                             ? response.Contains(txt)
                             : response == txt;
            }

            return !IsSuccess ^ result;
        }

        public override string ToString()
        {
            return Summary;
        }
    }
}