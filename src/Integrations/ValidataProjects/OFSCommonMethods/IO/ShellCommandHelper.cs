﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace OFSCommonMethods.IO
{
    public static class ShellCommandHelper
    {
        private static readonly Dictionary<ShellCommandType, string> _DefaultCommands
            = new Dictionary<ShellCommandType, string>
                  {
                      {ShellCommandType.ChangeDirectory, "cd {0}"},
                      {ShellCommandType.Compress, "tar -cvf {0} {1};compress {0}"},
                      {ShellCommandType.Uncompress, "uncompress -c {0} | tar -xvf -"},
                      {ShellCommandType.DeleteDirectory, "rd /S /Q {0}"},
                      {ShellCommandType.DeleteFile, "rm {0}/{1}"},
                      {ShellCommandType.CreateTextFile, "cat >> {0}/{1}|4"}
                  };

        public static void FillMissingCommandsWithDefaultText(Dictionary<ShellCommandType, ShellCommand> commands)
        {
            foreach (KeyValuePair<ShellCommandType, string> cmd in _DefaultCommands)
            {
                if (!commands.ContainsKey(cmd.Key) || string.IsNullOrEmpty(commands[cmd.Key].Format))
                {
                    Debug.WriteLine(string.Format("Set default value for {0} = {1}", cmd.Key, cmd.Value));
                    commands[cmd.Key] = new ShellCommand(cmd.Value, null);
                }
            }
        }
    }
}
