namespace OFSCommonMethods.IO
{
    internal class SystemShellConfigSettings : ConnectionTypeSetting
    {
        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.Shell; }
        }
    }
}