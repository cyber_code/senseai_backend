﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods.IO.Telnet
{
    public class SkipLines
    {
        private List<Line> _enquiryLines = new List<Line>();
        public List<Line> EnquiryLines
        {
            get
            {
                return _enquiryLines;
            }
            set
            {
                _enquiryLines = value;
            }
        }
    }

    public enum Operations
    {
        Contains,
        StartsWith,
        EndsWith,
        Equals
    }

    public class Line
    {
        private Operations _operation = Operations.Contains;
        public Operations Operation
        {
            get
            {
                return _operation;
            }
            set
            {
                _operation = value;
            }
        }

        private string _name = String.Empty;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        private bool _startWithAdditionalCharacters = false;
        public bool StartWithAdditionalCharacters
        {
            get
            {
                return _startWithAdditionalCharacters;
            }
            set
            {
                _startWithAdditionalCharacters = value;
            }
        }

        private string _value = String.Empty;
        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }
    }

}
