﻿using System;
using System.Diagnostics;

namespace OFSCommonMethods.IO.Telnet
{
    public class TelnetCommException : Exception
    {
        public enum ExceptionType
        {
            Undefined = 0,
            TimeOut,
            UnexpectedCommunicationError
        }

        public ExceptionType Type { get; private set; }

        public TelnetCommException(string message, ExceptionType exceptionType)
            : base(message)
        {
            Debug.Assert(exceptionType != ExceptionType.Undefined);
            Type = exceptionType;
        }
    }
}