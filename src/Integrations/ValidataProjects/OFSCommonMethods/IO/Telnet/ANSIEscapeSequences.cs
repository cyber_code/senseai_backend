﻿using System.Text;
namespace OFSCommonMethods.IO.Telnet
{
    public class ANSIEscapeSequences
    {
        #region Public Static Properties

        public static string ClearScreen = ((char)27).ToString() + ((char)91).ToString() + "J";

        public static string PositionCursor11 = ((char)27).ToString() + ((char)91).ToString() + "H";

        public static string SometingElse = ((char)27).ToString() + ((char)91).ToString() + "K";

        public static string EraseLine = ((char)27).ToString() + ((char)91).ToString() + "K" + ((char)8).ToString();

        public static string MoveCursorRight = ((char)27).ToString() + ((char)91).ToString() + "C";

        public static string SometingElse2 = ((char)27).ToString() + ((char)91).ToString();

        #endregion

        #region Public Static Methods

        public static string RemoveTelnetSpecialCharSequences(string telnetResponse)
        {
            return telnetResponse
                            .Replace(ANSIEscapeSequences.ClearScreen, string.Empty)
                            .Replace(ANSIEscapeSequences.EraseLine, string.Empty)
                            .Replace(ANSIEscapeSequences.MoveCursorRight, string.Empty)
                            .Replace(ANSIEscapeSequences.PositionCursor11, string.Empty)
                            .Replace(ANSIEscapeSequences.SometingElse, string.Empty)
                            .Replace(ANSIEscapeSequences.SometingElse2, string.Empty);
        }

        public static string GetStringFromBytes(int codepage, byte[] buff)
        {
            byte[] cleanedBuff = new byte[buff.Length];
            int pos = 0;
            for (int i = 0; i < buff.Length; i++)
            {
                if (!CodeHelper.IsTelnetCommand(buff[i]))
                {
                    cleanedBuff[pos++] = buff[i];
                }
            }

            string result = Encoding.GetEncoding(codepage).GetString(cleanedBuff, 0, pos);
            result = ANSIEscapeSequences.RemoveTelnetSpecialCharSequences(result);

            return result;
        }

        #endregion
    }
}

