﻿using System.Text;

namespace OFSCommonMethods.IO.Telnet
{
    internal abstract class CommBase
    {
        /// <summary>
        /// Default UTF-8
        /// </summary>
        protected int _PageCode = 65001;

        internal abstract bool IsConnected { get; }

        internal CommBase(int pageCode)
        {
            _PageCode = pageCode;
        }

        internal abstract bool Connect(string url, int port);

        internal abstract string Disconnect();

        internal abstract void Send(string str, bool isLogoutStep);

        internal abstract byte[] GetNext();

        internal abstract string GetStringFromBytes(byte[] rowData, bool joinTerminalLines);

        #region Protected static methods

        protected byte[] GetBytesToSend(string str, bool skipCR = false)
        {
            str = str.Trim() + (skipCR ? "" : "\r");
            return Encoding.GetEncoding(_PageCode).GetBytes(str);
        }

        #endregion
    }
}