﻿using System;
using System.Diagnostics;
using System.Text;
using VSSH;

namespace OFSCommonMethods.IO.Telnet
{
    internal class SshComm : CommBase
    {
        #region Private Members

        private VSshBase _Ssh;
        private readonly Settings.IOType _SshLibImplementation;

        private readonly string _IdentityFile;

        private readonly string _Passphrase;

        private readonly string _UserName;

        private readonly string _Password;

        private int _Timeout = 10000;

        #endregion

        #region Class Lifecycle

        internal SshComm(Settings settings, Settings.IOType sshLib)
            : base(settings.PageCode)
        {
            _SshLibImplementation = sshLib;
            _IdentityFile = settings.IdentityFile;
            _Passphrase = settings.Passphrase;
            _UserName = settings.GlobusUserName;
            _Password = settings.GlobusPassword;
            if (!int.TryParse(settings.TelnetResponseTimeOut, out _Timeout))
            {
                _Timeout = 10000;
            }
        }

        #endregion

        #region CommBase Implementation

        internal override bool IsConnected
        {
            get { return _Ssh != null && _Ssh.ShellOpened; }
        }

        internal override bool Connect(string url, int port)
        {
            try
            {
                CreateAndConnectVSsh(url, port);
                
                return true;
            }
            catch
            {
                _Ssh = null;
                throw;
            }
        }

        internal override string Disconnect()
        {
            try
            {
                if (IsConnected)
                {
                    _Ssh.Close();
                }

                return null;
            }
            catch (Exception ex)
            {
                return _Ssh.ShellOpened ? ex.ToString() : null;
            }
            finally
            {
                _Ssh = null;
            }
        }

        internal override void Send(string str, bool isLogoutStep)
        {
            byte[] bytesToSend = GetBytesToSend(str, this._Ssh is VSSH.VShellSshNET);
            _Ssh.Write(bytesToSend, isLogoutStep);
        }

        internal override byte[] GetNext()
        {
            return _Ssh.Receive();
        }

        internal override string GetStringFromBytes(byte[] rowData, bool joinTerminalLines)
        {
            string str = Encoding.GetEncoding(_PageCode).GetString(rowData, 0, rowData.Length);
            return _Ssh.RemoveSSHEscSequences(str, joinTerminalLines);
        }

        #endregion

        #region Private Methods

        private void CreateAndConnectVSsh(string url, int port)
        {
            Debug.Assert(_Ssh == null, "Should be not initialized!");

            if (_SshLibImplementation == Settings.IOType.SharpSsH)
            {
                _Ssh = new VShellSharpSsh(url, _UserName);

                if (!string.IsNullOrEmpty(_Password))
                {
                    _Ssh.Password = _Password;
                }

                if (!string.IsNullOrEmpty(_IdentityFile))
                {
                    _Ssh.AddIdentityFile(_IdentityFile, _Passphrase);
                }

                _Ssh.Connect(port, _Timeout);

            }
            else
            {
                _Ssh = new VShellSshNET(url, _UserName, _Password);
                if (_IdentityFile != null)
                {
                    _Ssh.AddIdentityFile(_IdentityFile);
                }

                    (_Ssh as VShellSshNET).Connect(_Timeout);
            }
        }

        #endregion
    }
}