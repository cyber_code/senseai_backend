﻿using System;
using System.Diagnostics;
using VSSH;

namespace OFSCommonMethods.IO.Telnet
{
    internal class SHHComm : CommBase
    {
        #region Private Members

        private VShellSsh _Ssh;

        private readonly string _IdentityFile;

        private readonly string _GlobusServer;

        private readonly string _UserName;

        private readonly string _Password;

        #endregion

        #region Class Lifecycle

        internal SHHComm(Settings settings)
            : base(settings.PageCode)
        {
            Debug.Assert(settings.IsSSH);

            _IdentityFile = settings.IdentityFile;
            _GlobusServer = settings.GlobusServer;
            _UserName = settings.GlobusUserName;
            _Password = settings.GlobusPassword;
        }

        #endregion

        #region CommBase Implementation

        internal override bool IsConnected
        {
            get { return _Ssh != null && _Ssh.ShellOpened; }
        }

        internal override bool Connect(string url, int port)
        {
            try
            {
                CreateVSsh();
                _Ssh.Connect(port);
                return true;
            }
            catch (Exception ex)
            {
                // TODO log the exception
                _Ssh = null;
                return false;
            }
        }

        internal override string Disconnect()
        {
            try
            {
                if (IsConnected)
                {
                    _Ssh.Close();
                }

                return null;
            }
            catch (Exception ex)
            {
                return _Ssh.ShellOpened ? ex.ToString() : null;
            }
            finally
            {
                _Ssh = null;
            }
        }

        internal override void Send(string str)
        {
            byte[] bytesToSend = GetMessage(str);
            _Ssh.Write(bytesToSend);
        }

        internal override byte[] GetNext()
        {
            return _Ssh.Receive();
        }

        internal override string GetStringFromBytes(byte[] rowData)
        {
            string str = base.GetStringFromBytes(rowData);
            return VShellSsh.RemoveSSHEscSequences(str);
        }

        #endregion

        #region Private Methods

        private void CreateVSsh()
        {
            Debug.Assert(_Ssh == null, "Should be not initialized!");

            _Ssh = new VShellSsh(_GlobusServer, _UserName);

            if (_Password != null)
            {
                _Ssh.Password = _Password;
            }

            if (_IdentityFile != null)
            {
                _Ssh.AddIdentityFile(_IdentityFile);
            }
        }

        #endregion
    }
}