﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods.IO.Telnet
{
    public enum AsciiCodes : byte
    {
        /// <summary>
        ///     Null character (ASCII 0)
        /// </summary>
        NUL = 0,


        /// <summary>
        ///     Bell (ASCII 7)
        /// </summary>
        BEL = 7,


        /// <summary>
        ///     Backspace (ASCII 8)
        /// </summary>
        BS = 8,


        /// <summary>
        ///     Horizontal tab (ASCII 9)
        /// </summary>
        HT = 9,


        /// <summary>
        ///     Line Feed (ASCII 10)
        /// </summary>
        LF = 10,


        /// <summary>
        ///     Vertical Tab (ASCII 11)
        /// </summary>
        VT = 11,


        /// <summary>
        ///     Form Feed (ASCII 12)
        /// </summary>
        FF = 12,


        /// <summary>
        ///     Carriage Return (ASCII 13)
        /// </summary>
        CR = 13,


        /// <summary>
        ///     Escape (ASCII 27)
        /// </summary>
        ESC = 27
    }

    enum Options : int
    {
        SGA = 3
    }

    public enum TelnetCommands : byte
    {
        /// <summary>
        ///     End Subnegotiation (SE)
        /// </summary>
        /// <remarks>
        ///     This command indicates the end of subnegotiation data.
        /// </remarks>
        /// <seealso cref="Subnegotiation"/>
        EndSubnegotiation = 240,

        /// <summary>
        ///     No operation (NOP)
        /// </summary>
        NoOperation = 241,

        /// <summary>
        ///     Data Mark
        /// </summary>
        DataMark = 242,

        /// <summary>
        ///     Break (BRK)
        /// </summary>
        Break = 243,

        /// <summary>
        ///     Interrupt Process (IP)
        /// </summary>
        InterruptProcess = 244,

        /// <summary>
        ///     Abort output (AO)
        /// </summary>
        AbortOutput = 245,

        /// <summary>
        ///     Are You There (AYT)
        /// </summary>
        AreYouThere = 246,

        /// <summary>
        ///     Erase Character (EC)
        /// </summary>
        /// <seealso cref="EraseLine"/>
        EraseCharacter = 247,

        /// <summary>
        ///     Erase Line (EL)
        /// </summary>
        /// <seealso cref="EraseCharacter"/>
        EraseLine = 248,

        /// <summary>
        ///     Go Ahead (GA)
        /// </summary>
        GoAhead = 249,

        /// <summary>
        ///     Subnegotiation (SB)
        /// </summary>
        /// <seealso cref="EndSubnegotiation"/>
        Subnegotiation = 250,

        /// <summary>
        ///     Will do option (WILL)
        /// </summary>
        /// <seealso cref="Do"/>
        /// <seealso cref="Dont"/>
        /// <seealso cref="Wont"/>
        Will = 251,

        /// <summary>
        ///     Will not do option (WONT)
        /// </summary>
        /// <seealso cref="Do"/>
        /// <seealso cref="Dont"/>
        /// <seealso cref="Will"/>
        Wont = 252,

        /// <summary>
        ///     Do option (DO)
        /// </summary>
        /// <seealso cref="Dont"/>
        /// <seealso cref="Will"/>
        /// <seealso cref="Wont"/>
        Do = 253,

        /// <summary>
        ///     Don't do option (DONT)
        /// </summary>
        /// <seealso cref="Do"/>
        /// <seealso cref="Will"/>
        /// <seealso cref="Wont"/>
        Dont = 254,

        /// <summary>
        ///     IAC
        /// </summary>
        Iac = 255
    }

    public static class CodeHelper
    {
        public static bool IsTelnetCommand(byte b)
        {
            return (b >= (byte)TelnetCommands.EndSubnegotiation && b <= (byte)TelnetCommands.Iac);
        }
    }
}

