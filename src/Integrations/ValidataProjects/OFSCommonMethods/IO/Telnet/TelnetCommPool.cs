﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace OFSCommonMethods.IO.Telnet
{
    public static class TelnetCommPool
    {
        #region Helper Classes

        private struct LoginKey
        {
            public ConsoleType ConnectorType;

            public string URL;

            public int Port;

            public string AccountName;

            public string AccountPassword;

            public string ConnectionId;

            public string MinorId;

            public string TestStepID;
        }

        public class TSSComm
        {
            public TSSTelnetComm Comm;

            public string Error;

            public bool HasError { get { return !String.IsNullOrEmpty(this.Error); } }
        }

        #endregion

        #region Public Static Properties


        private static string logsFile = string.Empty;


        public static string LogsFolder
        {
            get
            {
                if (string.IsNullOrEmpty(logsFile))
                {
                    return Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"adapterfiles\Logs";
                }

                return logsFile;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    logsFile = value.Contains("%VALIDATA_ROOT%") ? Environment.ExpandEnvironmentVariables(value) : value;
                }
            }
        }

        public static int TimeOut
        {
            get { return _TimeOut; }
            set { _TimeOut = value; }
        }

        public static int CommunicatorsCount
        {
            get { return _Comms.Count; }
        }

        #endregion

        #region Private Constants

        private const int TimeOutDefault = 5000;

        #endregion

        #region Private Static members

        private static int _TimeOut = TimeOutDefault;

        private static Dictionary<LoginKey, TSSComm> _Comms;

        private static readonly Object _Sync = new Object();

        #endregion

        #region Public Static methods

        /// <summary>
        /// Initializes the telnet connections pool.
        /// </summary>
        /// <param name="settingsSets">The settings sets.</param>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public static List<int> InitializeTelnetConnectionsPool(List<SettingsSet> settingsSets, string id)
        {
            List<TSSTelnetComm> createdCommunicators;
            return InitializeTelnetConnectionsPool(settingsSets, id, out createdCommunicators);
        }

        /// <summary>
        /// Initializes the telnet connections pool.
        /// </summary>
        /// <param name="settingsSets">The settings sets.</param>
        /// <param name="id">The id.</param>
        /// <param name="createdCommunicators">Created communicators</param>
        /// <returns></returns>
        public static List<int> InitializeTelnetConnectionsPool(List<SettingsSet> settingsSets, string id, out List<TSSTelnetComm> createdCommunicators)
        {
            var connectors = new List<LoginKey>();

            AssureCommunicatorsMapIsInitialized();

            Dictionary<SettingsSet, List<ManualResetEvent>> setsEvents = new Dictionary<SettingsSet, List<ManualResetEvent>>(settingsSets.Count);
            List<int> initializedConnectorIndices = new List<int>();

            try
            {
                ManualResetEvent initializationTimeoutEvent = new ManualResetEvent(false);
                DateTime startTime = DateTime.Now;

                // start to creat all connections in parallel
                int currentThreadIndex = 0;
                for (int i = 0; i < settingsSets.Count; i++)
                {
                    setsEvents.Add(settingsSets[i], new List<ManualResetEvent>());

                    for (int j = 0; j < settingsSets[i].NumberOfThreads; j++)
                    {
                        ManualResetEvent finishEvent = new ManualResetEvent(false);
                        setsEvents[settingsSets[i]].Add(finishEvent);

                        Thread thread = new Thread(CreateOfsConnectorForPerformanceAndSignalEvent);
                        thread.Name = "OFS Login " + currentThreadIndex;

                        thread.Start(new ThreadStartData(settingsSets[i].Settings, id, currentThreadIndex, finishEvent, initializationTimeoutEvent));
                        var lk = GetLoginKeyForPerformance(settingsSets[i].Settings, id, currentThreadIndex);
                        connectors.Add(lk);



                        currentThreadIndex++;
                    }
                }

                // wait for the connections
                int currentEventIndex = 0;
                foreach (var setEvents in setsEvents)
                {
                    startTime = DateTime.Now;
                    foreach (var manualResetEvent in setEvents.Value)
                    {
                        ManualResetEvent finishEvent = manualResetEvent;

                        int milisecondsSpent = (int)(DateTime.Now - startTime).TotalMilliseconds;
                        if (milisecondsSpent > setEvents.Key.ThreadsInitializationTimeout)
                        {
                            //Debug.Fail("Should not happen");

                            if (finishEvent.WaitOne(0))
                            {
                                // completed
                                initializedConnectorIndices.Add(currentEventIndex);
                                setEvents.Key.InitializedConnectorIndices.Add(currentEventIndex);
                            }
                        }
                        else
                        {
                            if (finishEvent.WaitOne(setEvents.Key.ThreadsInitializationTimeout - milisecondsSpent))
                            {
                                initializedConnectorIndices.Add(currentEventIndex);
                                setEvents.Key.InitializedConnectorIndices.Add(currentEventIndex);
                            }
                        }

                        currentEventIndex++;
                    }
                }

                initializationTimeoutEvent.Set();

                // don't join the threads because it would block the further execution -> thus the import/export
                //  would stard long after the 'timeOutInMilliseconds' timeout expires
            }
            finally
            {
                // dispose the events
                foreach (var setEvents in setsEvents)
                {
                    foreach (var manualResetEvent in setEvents.Value)
                    {
                        manualResetEvent.Close();
                    }
                }
            }

            createdCommunicators = new List<TSSTelnetComm>();

            lock (_Sync)
            {
                foreach (var lk in connectors)
                {
                    var comm = FindCommunicator(lk);
                    if (comm != null)
                        createdCommunicators.Add(comm);
                }
            }

            Debug.WriteLine("Number of active connectors: " + CommunicatorsCount);

            return initializedConnectorIndices;
        }

        public static TSSTelnetComm GetOrCreateCommunicatorForPerformance(int pos, Settings settings, string telnetConnectionsGroupID)
        {
            string url;
            int port;
            SocketBasedIOManager.ParseServer(settings.GlobusServer, settings.Type, out url, out port);

            lock (_Sync)
            {
                TSSTelnetComm result = FindCommunicator(url, port, settings, ConsoleType.OFS, telnetConnectionsGroupID, pos.ToString());
                if (result != null)
                    return result;
            }

            TSSComm created = CreateOfsConnectorForPerformance(settings, telnetConnectionsGroupID, pos);
            if (created == null)
                throw new Exception(string.Format("Unable to create T24 connector for thread: {0}", pos));

            return created.Comm;
        }

        public static TSSTelnetComm GetCommunicator(string url, int port, Settings settings, string id, string testStepId = "")
        {
            return GetCommunicator(url, port, settings, id, null, testStepId);
        }

        public static TSSTelnetComm GetCommunicator(string url, int port, Settings settings, string id, TSSTelnetComm.LogMessageHandle logMessageHandle, string testStepId)
        {
            return GetOrCreateConnector(url, port, settings, id, ConsoleType.OFS, logMessageHandle, testStepId);
        }

        public static TSSTelnetComm GetShellCommunicator(string url, int port, Settings settings, string id)
        {
            return GetOrCreateConnector(url, port, settings, id, ConsoleType.Shell, null, null);
        }

        public static void Release(TSSTelnetComm comm)
        {
            bool shouldDisconnect = false;
            lock (_Sync)
            {
                if (_Comms == null)
                {
                    Debug.Fail("Why list with communicators is empty?");
                    return;
                }

                foreach (LoginKey key in _Comms.Keys)
                {
                    if (_Comms[key].Comm == comm)
                    {
                        shouldDisconnect = true;
                        _Comms.Remove(key);
                        break;
                    }
                }
            }

            if (shouldDisconnect)
                comm.Disconnect();
        }

        public static void Release(string url, int port, Settings settings, string commId)
        {
            List<TSSComm> lsComm;
            lock (_Sync)
            {
                lsComm = FindCommunicators(url, port, settings, ConsoleType.OFS, commId);
                lsComm.AddRange(FindCommunicators(url, port, settings, ConsoleType.Shell, commId));
            }

            if (lsComm.Count == 0)
                return; // nothing to release

            for (int i = 0; i < lsComm.Count; i++)
            {
                if (lsComm[i].Comm != null)
                {
                    Release(lsComm[i].Comm);
                }
                else
                {
                    Debug.Fail("Unexepected");
                }
            }
        }

        public static void ReleaseAll()
        {
            var toBeReleased = new List<TSSTelnetComm>();

            lock (_Sync)
            {
                if (_Comms != null)
                {
                    toBeReleased = _Comms.Values.Where(n => n.Comm != null).Select(n => n.Comm).ToList();
                    _Comms = null;
                }
            }

            toBeReleased.ForEach(n => n.Disconnect());
        }

        #endregion

        #region Private Static Methods

        private static void AssureCommunicatorsMapIsInitialized()
        {
            if (_Comms == null)
                _Comms = new Dictionary<LoginKey, TSSComm>();
        }

        private static LoginKey GetLoginKey(
            string url,
            int port,
            string accountName,
            string accountPassword,
            ConsoleType connectorType,
            string connId,
            string minorId,
            string testStepID = "")
        {
            LoginKey lk = new LoginKey();
            lk.URL = url;
            lk.Port = port;
            lk.AccountName = accountName;
            lk.AccountPassword = accountPassword;
            lk.ConnectorType = connectorType;

            Debug.Assert(!string.IsNullOrEmpty(connId));
            lk.ConnectionId = connId;

            lk.MinorId = minorId;

            lk.TestStepID = testStepID;

            return lk;
        }

        private static TSSComm CreateTelnetConnectorAndLogin(LoginKey lk,
                                                             Settings settings,
                                                             TSSTelnetComm.LogMessageHandle logMessageHandle, string testStepId = "")
        {
            TSSComm result = new TSSComm();

            LoadTimeOutValue(settings.PhantomSet, settings.TelnetThreadsConnectResponseTimeout > 0 ? settings.TelnetThreadsConnectResponseTimeout.ToString() : settings.TelnetResponseTimeOut);

            TSSTelnetComm tssTelnetComm = new TSSTelnetComm(
                lk.ConnectorType,
                lk.URL,
                lk.Port,
                TimeOut,
                settings.PhantomSet,
                (lk.MinorId == null) ? (lk.ConnectionId) : (lk.ConnectionId + "-" + lk.MinorId),
                settings,
                testStepId
                );

            if (logMessageHandle != null)
            {
                tssTelnetComm.OnLogMessage += logMessageHandle;
            }

            if (!tssTelnetComm.Connect())
            {
                result.Error = string.Format("Unable to connect to {0} server ({1}:{2})", settings.Type, lk.URL, lk.Port);

                string lastErr = tssTelnetComm.LastError;
                if (!string.IsNullOrEmpty(lastErr))
                {
                    result.Error += ": " + lastErr;
                }

                return result;
            }

            // TODO should we use actual user/pass from the connection settings
            if (
                settings.Type != Settings.IOType.TCServer &&
                settings.Type != Settings.IOType.HttpWebService &&
                settings.Type != Settings.IOType.HttpRestService
				&& !tssTelnetComm.IsLocalShell()
            )
            {
                try
                {
                    if (!tssTelnetComm.Login(lk.AccountName, lk.AccountPassword))
                    {
                        result.Error = string.Format("Unable to login to {0} server: {1}", settings.Type, tssTelnetComm.LastError);
                    }
                }
                catch (Exception ex)
                {
                    result.Error = string.Format("Unable to login to {0} server: {1}", settings.Type, ex.ToString());
                }

                if (!string.IsNullOrEmpty(result.Error))
                {
                    tssTelnetComm.Disconnect();
                    return result;
                }
            }

            result.Comm = tssTelnetComm;

            LoadTimeOutValue(settings.PhantomSet, settings.TelnetResponseTimeOut);

            tssTelnetComm.SetTimeOutPeriod(TimeOut);

            return result;
        }

        /// <summary>
        /// Load Time Out Value
        /// </summary>
        /// <param name="phantomSet">Indicate that the adapter have to use parameters from TelnetCommDef.xml file settings</param>
        /// <param name="timeOut">Time out in milliseconds</param>
        private static void LoadTimeOutValue(string phantomSet, string timeOut)
        {
            if (!String.IsNullOrEmpty(phantomSet))
            {
                if (!int.TryParse(timeOut, out _TimeOut))
                {
                    TimeOut = TimeOutDefault;
                }
            }
            else
            {
                TimeOut = 15000;
            }
        }

        private static List<TSSComm> FindCommunicators(string url, int port, Settings settings, ConsoleType consoleType, string commId, string testStepID = "")
        {
            List<TSSComm> result = new List<TSSComm>();

            if (_Comms == null)
                return result;

            LoginKey tempLK = GetLoginKey(url, port, settings.GlobusUserName, settings.GlobusPassword, consoleType, commId, null, testStepID);

            foreach (LoginKey ck in _Comms.Keys)
            {
                tempLK.MinorId = ck.MinorId;
                if (tempLK.Equals(ck))
                {
                    result.Add(_Comms[ck]);
                }
            }

            return result;
        }

        private static TSSTelnetComm FindCommunicator(LoginKey lk)
        {
            if (_Comms != null && _Comms.ContainsKey(lk))
                return _Comms[lk].Comm;

            return null;
        }

        private static TSSTelnetComm FindCommunicator(string url, int port, Settings settings, ConsoleType consoleType, string groupId, string minorId)
        {
            LoginKey lk = GetLoginKey(url, port, settings.GlobusUserName, settings.GlobusPassword, consoleType, groupId, minorId, null);

            return FindCommunicator(lk);
        }

        private static TSSTelnetComm GetOrCreateConnector(string url,
                                                          int port,
                                                          Settings settings,
                                                          string id,
                                                          ConsoleType connectorType,
                                                          TSSTelnetComm.LogMessageHandle logMessageHandle,
                                                          string testStepId)
        {
            TSSComm result = GetOrCreateConnectorA(url, port, settings, id, connectorType, logMessageHandle, testStepId);

            if (result == null)
            {
                throw new Exception("Unable to find or create communicator!");
            }

            if (result.Comm == null)
            {
                Debug.Assert(result.HasError);
                throw new Exception(result.Error);
            }

            result.Comm.InUse = true;
            return result.Comm;
        }

        private static TSSComm GetOrCreateConnectorA(string url,
                                                     int port,
                                                     Settings settings,
                                                     string id,
                                                     ConsoleType connectorType,
                                                     TSSTelnetComm.LogMessageHandle logMessageHandle,
                                                     string testStepId)
        {
            List<TSSComm> lsComms;
            lock (_Sync)
            {
                AssureCommunicatorsMapIsInitialized();
                lsComms = FindCommunicators(url, port, settings, connectorType, id, testStepId);
            }

            if (lsComms.Count == 0)
            {
                LoginKey lk = GetLoginKey(url, port, settings.GlobusUserName, settings.GlobusPassword, connectorType, id, null, testStepId);
                TSSComm tssc = CreateTelnetConnectorAndLogin(lk, settings, logMessageHandle, testStepId);   // todo - this call should be outside of the LOCK

                bool shouldReleaseNewlyCreated = false;
                TSSComm result;
                lock (_Sync)
                {
                    if (_Comms.ContainsKey(lk))
                        shouldReleaseNewlyCreated = true;
                    else
                        _Comms[lk] = tssc;

                    result = _Comms[lk];
                }

                if (shouldReleaseNewlyCreated)
                    tssc.Comm.Disconnect();

                return result;
            }

            if (lsComms.Count == 1)
                return lsComms[0];

            DateTime dtStart = DateTime.Now;
            do
            {
                var tssComm = GetFirstFreeCommunicator(lsComms);
                if (tssComm != null)
                    return tssComm;

                Thread.Sleep(200);
            }
            while ((DateTime.Now - dtStart).TotalMilliseconds < settings.TimeoutForFreeConnection);

            return null;
        }

        private static TSSComm GetFirstFreeCommunicator(List<TSSComm> lsComms)
        {
            if (lsComms == null || lsComms.Count == 0)
            {
                throw new Exception("Telnet connections for performance are not initialized!");
            }

            foreach (TSSComm tssComm in lsComms)
            {
                if (tssComm.Comm != null && !tssComm.Comm.InUse)
                    return tssComm;
            }

            return null;
        }

        private static TSSComm CreateOfsConnectorForPerformance(Settings settings, string groupId, int threadId)
        {
            LoginKey lk = GetLoginKeyForPerformance(settings, groupId, threadId);

            lock (_Sync)
            {
                if (FindCommunicator(lk) != null)
                    return _Comms[lk];
            }

            TSSComm tssc = CreateTelnetConnectorAndLogin(lk, settings, null);
            if (tssc == null || tssc.Comm == null || tssc.HasError)
                return null;

            bool shouldDisconnectNewlyCreated = false;
            TSSComm result;
            lock (_Sync)
            {
                if (_Comms.ContainsKey(lk))
                    shouldDisconnectNewlyCreated = true;
                else
                    _Comms[lk] = tssc;

                result = _Comms[lk];
            }

            if (shouldDisconnectNewlyCreated)
                tssc.Comm.Disconnect();

            return result;
        }

        private static LoginKey GetLoginKeyForPerformance(Settings settings, string groupId, int threadId)
        {
            string url;
            int port;
            SocketBasedIOManager.ParseServer(settings.GlobusServer, settings.Type, out url, out port);

            return GetLoginKey(url, port, settings.GlobusUserName, settings.GlobusPassword, ConsoleType.OFS, groupId, threadId.ToString());
        }

        private class ThreadStartData
        {
            public readonly Settings Settings;

            public readonly string Id;

            public readonly int MinorId;

            public readonly ManualResetEvent ManualResetEvent;

            public readonly ManualResetEvent InitializationTimeoutEvent;

            public ThreadStartData(Settings settings, string id, int i, ManualResetEvent manualResetEvent, ManualResetEvent initializationTimeoutEvent)
            {
                Settings = settings;
                Id = id;
                MinorId = i;
                ManualResetEvent = manualResetEvent;
                InitializationTimeoutEvent = initializationTimeoutEvent;
            }
        }

        private static void CreateOfsConnectorForPerformanceAndSignalEvent(object data)
        {
            ThreadStartData tsd = (ThreadStartData)data;
            CreateOfsConnectorForPerformanceAndSignalEvent(tsd.Settings, tsd.Id, tsd.MinorId, tsd.ManualResetEvent, tsd.InitializationTimeoutEvent);
        }

        private static void CreateOfsConnectorForPerformanceAndSignalEvent(Settings settings, string id, int minorId, EventWaitHandle manualResetEvent, EventWaitHandle initializationTimeoutEvent)
        {
            if (settings.Type == Settings.IOType.HttpWebService || settings.Type == Settings.IOType.HttpRestService)
            {
                manualResetEvent.Set();
                return;
            }

            TSSComm comm = null;
            while (comm == null
                && !initializationTimeoutEvent.WaitOne(0))
            {
                comm = CreateOfsConnectorForPerformance(settings, id, minorId);
            }

            try
            {
                manualResetEvent.Set();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        #endregion
    }
}