﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace OFSCommonMethods.IO.Telnet
{
    class TCServerComm : CommBase
    {
        #region Private Members

        private Socket _Socket;
        private const int BufferSize = 4096;

        #endregion

        #region Class Lifecycle

        internal TCServerComm(Settings settings)
            : base(settings.PageCode)
        {
            Debug.Assert(settings.Type == Settings.IOType.TCServer);
        }

        #endregion

        #region CommBase Implementation

        internal override bool IsConnected
        {
            get { return _Socket != null && _Socket.Connected; }
        }

        internal override bool Connect(string url, int port)
        {
            Debug.Assert(!IsConnected, "Connection is already established! ");

            IPAddress ipa;
            if(!IPAddress.TryParse(url, out ipa))
            {
                try
                {
                    var ipHostEntry = Dns.GetHostEntry(url);
                    foreach (IPAddress address in ipHostEntry.AddressList)
                    {
                        if (Connect(address, port))
                            return true;
                    }

                    return false;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return Connect(ipa, port);

        }

        internal override string Disconnect()
        {
            try
            {
                if (IsConnected)
                {
                    _Socket.Close();
                }

                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            finally
            {
                _Socket = null;
            }
        }

        internal override void Send(string str, bool isLogoutStepr)
        {
            byte[] bytesToSend = GetBytesToSend(str);
            _Socket.Send(bytesToSend, bytesToSend.Length, 0);
        }

        internal override byte[] GetNext()
        {
            if(_Socket.Available == 0)
                return new byte[0];

            var bytes = new byte[BufferSize];
            int nReceived = _Socket.Receive(bytes);

            if(nReceived < BufferSize)
            {
                var resBytes = new byte[nReceived];
                Array.Copy(bytes, resBytes, nReceived);
                return resBytes;
            }

            return bytes;
        }

        internal override string GetStringFromBytes(byte[] rowData, bool joinTerminalLines)
        {
            string str = Encoding.GetEncoding(_PageCode).GetString(rowData, 0, rowData.Length);
            return str;
        }

        #endregion

        #region Private Methods

        private bool Connect(IPAddress address, int port)
        {
            try
            {
                var ipe = new IPEndPoint(address, port);
                _Socket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                _Socket.Connect(ipe);
                if (_Socket.Connected)
                {
                    _Socket.Send(new byte[] { });
                    _Socket.Receive(new byte[4090]);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        #endregion
    }
}
