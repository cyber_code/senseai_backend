﻿using OFSCommonMethods.IO.HttpRestService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Xml;
using ValidataCommon;
using ValidataCrypto;
using static OFSCommonMethods.IO.Settings;

namespace OFSCommonMethods.IO.Telnet
{
    public class ExecutionStepContainer
    {
        public delegate void OnEvent(string msg);

        public OnEvent Event;

        public static string TelnetDefFileName =
            !string.IsNullOrEmpty(AdapterConfiguration.AdapterFilesRootPath)
            ? $"{AdapterConfiguration.AdapterFilesRootPath}\\Assemblies\\TelnetCommmDef.xml"
            : $"{Directory.GetCurrentDirectory()}\\TelnetCommDef.xml";


        public static string[] ListOfTelnetSets
        {
            get { return GetListOfTelnetSetNames().ToArray(); }
        }

        public List<CustomCommandResult> CommandResults;

        private readonly string _TelnetSet = String.Empty;
        private const string DEFAULT_AA_PRODUCT_ENQUIRY = "AA.PRODUCT.CATALOG-PRODUCTS";

        #region Lifecycle

        public ExecutionStepContainer(string telnetSet)
        {
            _TelnetSet = telnetSet;
        }

        #endregion

        #region Public Methods

        public static void LoadConnectionSettings(Settings settings)
        {
            if (!File.Exists(TelnetDefFileName))
            {
                throw new IOException(string.Format("File '{0}' does not exist", TelnetDefFileName));
            }

            try
            {
                var readerSettings = new XmlReaderSettings();
                readerSettings.IgnoreComments = true;
                var doc = new XmlDocument();

                using (XmlReader reader = XmlReader.Create(TelnetDefFileName, readerSettings))
                {
                    doc.Load(reader);
                }

                XmlElement docElement = doc.DocumentElement;
                if (docElement == null)
                {
                    throw new XmlException("Missing XML document element");
                }

                XmlNode setNode = docElement.SelectSingleNode(String.Format("//TelnetSets/TelnetSet[@Name='{0}']", settings.PhantomSet));
                if (setNode == null)
                {
                    if (string.IsNullOrEmpty(settings.PhantomSet))
                    {
                        throw new XmlException("No TelnetSet name is specified.");
                    }
                    else
                    {
                        throw new XmlException(string.Format("Cannot find a TelnetSet named '{0}' in the configuration file.", settings.PhantomSet));
                    }
                }

                ReadConnectionSection(setNode, settings);
                ReadSpecialSection(setNode, settings);
                ReadAATestGenerationSection(setNode, settings);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Invalid contents of " + TelnetDefFileName + ". Details: " + ex.Message);
            }
        }

        public CustomTelnetResponseProcessing LoadCustomProcessing()
        {
            if (!File.Exists(TelnetDefFileName))
            {
                throw new IOException(string.Format("File '{0}' does not exist", TelnetDefFileName));
            }

            CustomTelnetResponseProcessing customProcessing = new CustomTelnetResponseProcessing();

            #region ENQUIRY LINES
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(TelnetDefFileName);

                    XmlNodeList lsLineNodes = doc.SelectNodes(
                            String.Format("//TelnetSets/TelnetSet[@Name='{0}']/SkipLines/EnquiryLines/Line", _TelnetSet)
                        );

                    if (lsLineNodes != null)
                    {
                        foreach (XmlNode node in lsLineNodes)
                        {
                            Line line = ParseLineDefinition(node);
                            if (line != null)
                            {
                                customProcessing.EnquiryLines.Add(line);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // IN CASE OF ERROR LINE IS SKIPPED & IT IS LOGGED IN THE LOG FILE
                    //throw new InvalidOperationException(ex.Message);
                    Debug.WriteLine("Problem in 'EnquiryLines'" + ex);
                }
            }
            #endregion

            #region PROCESS LINES
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(TelnetDefFileName);

                    XmlNodeList lsLineNodes = doc.SelectNodes(
                            String.Format("//TelnetSets/TelnetSet[@Name='{0}']/SkipLines/ProcessLines/Line", _TelnetSet)
                        );

                    if (lsLineNodes != null)
                    {
                        foreach (XmlNode node in lsLineNodes)
                        {
                            Line line = ParseLineDefinition(node);
                            if (line != null)
                            {
                                customProcessing.ProcessLines.Add(line);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // IN CASE OF ERROR LINE IS SKIPPED & IT IS LOGGED IN THE LOG FILE
                    // throw new InvalidOperationException(ex.Message);         
                    Debug.WriteLine("Problem in 'SkipLines/ProcessLines/Line'" + ex);
                }
            }
            #endregion

            #region CUSTOM ACTIONS
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(TelnetDefFileName);

                    XmlNodeList lsActionNodes = doc.SelectNodes(
                            String.Format("//TelnetSets/TelnetSet[@Name='{0}']/SkipLines/ProcessLines/CustomAction", _TelnetSet)
                        );

                    if (lsActionNodes != null)
                    {
                        foreach (XmlNode node in lsActionNodes)
                        {
                            CustomAction ca = ParseCustomActionDefinition(node);
                            if (ca != null)
                            {
                                customProcessing.CustomActions.Add(ca);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // IN CASE OF ERROR LINE IS SKIPPED & IT IS LOGGED IN THE LOG FILE
                    // throw new InvalidOperationException(ex.Message);                    
                    Debug.WriteLine("Problem in 'SkipLines/ProcessLines/CustomAction'" + ex);
                }
            }
            #endregion

            return customProcessing;
        }

        #endregion

        #region Private Methods

        private static List<string> GetListOfTelnetSetNames()
        {
            if (!File.Exists(TelnetDefFileName))
            {
                throw new IOException(string.Format("File '{0}' does not exist", TelnetDefFileName));
            }

            XmlDocument doc = new XmlDocument(); // TODO maybe use XPathDocument
            using (FileStream fs = new FileStream(TelnetDefFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                doc.Load(fs);
            }

            XmlElement docElement = doc.DocumentElement;
            if (docElement == null)
            {
                throw new XmlException("Missing xml document element");
            }

            List<string> result = new List<string>();

            XmlNodeList lsNodes = docElement.SelectNodes("//TelnetSets/TelnetSet");
            if (lsNodes != null)
            {
                foreach (XmlNode n in lsNodes)
                {
                    result.Add(n.Attributes["Name"].Value);
                }
            }

            return result;
        }

        private static ExecutionStep GetExecutionStepFromXmlNode(XmlNode node)
        {
            string stepName = node.Attributes["name"].Value;

            string command = null;
            try
            {
                XmlNode cmd = node.SelectSingleNode("command");
                if (cmd != null)
                {
                    command = GetDecryptedValue(cmd);
                }
            }
            catch { }

            string waitFor = null;
            try
            {
                XmlNode wait = node.SelectSingleNode("waitFor");
                if (wait != null)
                {
                    waitFor = wait.InnerText;
                }
            }
            catch { }

            return new ExecutionStep(stepName, command, waitFor);
        }

        private Line ParseLineDefinition(XmlNode node)
        {
            Line result = new Line();

            if (node.Attributes["Operation"] == null)
            {
                FireErrorEvent("Line skipped: 'Operation' attribute not defined!");
                return null;
            }

            try
            {
                result.Operation = (Operations)Enum.Parse(typeof(Operations), node.Attributes["Operation"].Value);
            }
            catch
            {
                FireErrorEvent("Line skipped: Invalid value for 'Operation' attribute! Possible values are: Contains; StartsWith; EndsWith; Equals; ");
                return null;
            }

            if (node.Attributes["Application"] != null)
            {
                result.Application = node.Attributes["Application"].Value;
                if (result.Application == string.Empty)
                {
                    result.Application = null;
                }
            }

            if (node.Attributes["Command"] != null)
            {
                result.Command = node.Attributes["Command"].Value;
            }

            result.Value = node.InnerText;
            return result;
        }

        private CustomAction ParseCustomActionDefinition(XmlNode node)
        {
            try
            {
                CustomAction result = new CustomAction();

                // Application
                result.Application = (node.Attributes["Application"] != null) ? node.Attributes["Application"].Value : null;
                if (result.Application == string.Empty)
                {
                    result.Application = null;
                }

                result.Action = (node.Attributes["Action"] != null) ? node.Attributes["Action"].Value : null;
                if (result.Application == string.Empty)
                {
                    result.Application = null;
                }

                try
                {
                    result.Command = node.SelectSingleNode("command").InnerText;
                }
                catch
                {
                    result.Command = null;
                }

                try
                {
                    result.WaitMS = int.Parse(node.SelectSingleNode("waitMs").InnerText);
                }
                catch
                {
                    result.WaitMS = 0;
                }

                try
                {
                    result.WaitFor = node.SelectSingleNode("waitFor").InnerText;
                }
                catch
                {
                    result.WaitFor = null;
                }

                return result;
            }
            catch (Exception ex)
            {
                FireErrorEvent("Unable to parse custom cation node: " + ex.Message);
                return null;
            }
        }

        private void FireEvent(string msg)
        {
            if (Event != null)
            {
                Event(msg);
            }
        }

        private void FireErrorEvent(string msg)
        {
            FireEvent("ERRROR: " + msg);
        }

        private static void ReadConnectionSection(XmlNode telnetSetNode, Settings settings)
        {
            XmlNode connNode = telnetSetNode.SelectSingleNode("Connection");
            if (connNode == null)
            {
                throw new XmlException(string.Format("Missing section 'Connection' for set {0}", settings.PhantomSet));
            }

            //T24 credentials for telnet adapter
            settings.InputUser = FindRequiredNode(connNode, "InputUser").InnerText;
            settings.InputPassword = GetDecryptedValue(FindRequiredNode(connNode, "InputPassword"));
            settings.AuthorizationUser = FindRequiredNode(connNode, "AuthorizationUser").InnerText;
            settings.AuthorizationPassword = GetDecryptedValue(FindRequiredNode(connNode, "AuthorizationPassword"));

            //Telnet credentials for telnet adapter
            settings.GlobusServer = FindRequiredNode(connNode, "T24TelnetServer").InnerText;
            settings.GlobusUserName = FindRequiredNode(connNode, "T24LoginUserName").InnerText;
            settings.GlobusPassword = GetDecryptedValue(FindRequiredNode(connNode, "T24LoginPassword"));
            settings.TelnetResponseTimeOut = FindRequiredNode(connNode, "TelnetResponseTimeOut").InnerText;

            settings.OFSRequestStart = FindRequiredNode(connNode, "OFSRequestStart").InnerText;
            settings.OFSRequestEnd = FindRequiredNode(connNode, "OFSRequestEnd").InnerText;
            settings.OFSResponseStart = FindRequiredNode(connNode, "OFSResponseStart").InnerText;
            settings.OFSResponseEnd = FindRequiredNode(connNode, "OFSResponseEnd").InnerText;

            settings.OFSMLInstancesStart = FindRequiredNode(connNode, "OFSMLInstancesStart").InnerText;
            settings.OFSMLInstancesEnd = FindRequiredNode(connNode, "OFSMLInstancesEnd").InnerText;
            settings.OFSMLInstanceStart = FindRequiredNode(connNode, "OFSMLInstanceStart").InnerText;
            settings.OFSMLInstanceEnd = FindRequiredNode(connNode, "OFSMLInstanceEnd").InnerText;

            try
            {
                settings.OFSUseOldHistoryReadResponse = FindRequiredNode(connNode, "OFSUseOldHistoryReadResponse").InnerText;
            }
            catch
            {
            }

            settings.Objectdir = FindRequiredNode(connNode, "Objectdir").InnerText;
            settings.Exportname = FindRequiredNode(connNode, "Exportname").InnerText;
            settings.ObjectDelimiter = FindRequiredNode(connNode, "ObjectDelimiter").InnerText;
            settings.ObjectExtension = FindRequiredNode(connNode, "ObjectExtension").InnerText;
            settings.LibVersionDelimiter = FindRequiredNode(connNode, "LibVersionDelimiter").InnerText;
            settings.JSh = FindRequiredNode(connNode, "jSh").InnerText;

            // Code page
            settings.PageCode = Convert.ToInt32(FindRequiredNode(connNode, "PageCode").InnerText);

            // Connection Type
            try
            {
                string innerText = GetNodeInnerTextSave(connNode, "ConnectionType");
                if (!string.IsNullOrEmpty(innerText))
                {
                    settings.Type = (Settings.IOType)Enum.Parse(typeof(Settings.IOType), innerText.ToUpper(), true);
                }
            }
            catch { }

            // OFS Type
            try
            {
                string innerText = GetNodeInnerTextSave(connNode, "OfsType");
                if (!string.IsNullOrEmpty(innerText))
                {
                    settings.OfsType = (Settings.OFSType)Enum.Parse(typeof(Settings.OFSType), innerText.ToUpper(), true);
                }
            }
            catch { }

            // SSH Identity File & Passphrase 
            settings.IdentityFile = getSSHInfoFromNode(connNode, "IdentityFile");
            settings.Passphrase = getSSHInfoFromNode(connNode, "Passphrase");
         

            string t24InstancesImportMethod = GetNodeInnerTextSave(connNode, "T24InstancesImportMethod");

            if (String.IsNullOrEmpty(t24InstancesImportMethod) ||
                !Enum.TryParse(t24InstancesImportMethod, out settings.ExportToT24Method))
                settings.ExportToT24Method = Settings.T24InstancesImportMethod.GenericT24AdapterCall;

            #region Batch file Listener related settings

            settings.BatchFileListenerOfsRequestsFolderPath = GetNodeInnerTextSave(connNode, "BatchFileListenerOfsRequestsFolderPath");
            settings.BatchFileListenerOfsReresponsesFolderPath = GetNodeInnerTextSave(connNode, "BatchFileListenerOfsReresponsesFolderPath");

            string timeIntervalNodeText = GetNodeInnerTextSave(connNode, "BatchFileListenerRepeatableResponseCheckTimeInterval");
            if (!string.IsNullOrWhiteSpace(timeIntervalNodeText))
                int.TryParse(timeIntervalNodeText, out settings.BatchFileListenerRepeatableResponseCheckTimeInterval);

            string timeOutNodeText = GetNodeInnerTextSave(connNode, "BatchFileListenerResponseWaitTimeout");
            if (!string.IsNullOrWhiteSpace(timeOutNodeText))
                int.TryParse(timeOutNodeText, out settings.BatchFileListenerResponseWaitTimeout);

            settings.BatchFileListenerRequestNewLineCharacter = GetNodeInnerTextSave(connNode, "BatchFileListenerRequestNewLineCharacter");
            if (settings.BatchFileListenerRequestNewLineCharacter != null && settings.BatchFileListenerRequestNewLineCharacter.ToUpper() == "WIN")
            {
                settings.BatchFileListenerRequestNewLineCharacter = "\r\n";
            }
            else
            {
                settings.BatchFileListenerRequestNewLineCharacter = "\n";
            }

            var msgIdPrefixNode = connNode.SelectSingleNode("OfsRequestMessageIdPrefix");
            if (msgIdPrefixNode != null && msgIdPrefixNode.Attributes != null)
            {
                settings.OfsRequestMessageIdPrefix_Prefix = msgIdPrefixNode.Attributes["prefix"] != null
                    ? msgIdPrefixNode.Attributes["prefix"].Value
                    : "VAL";
                settings.OfsRequestMessageIdPrefix_RandomStringCharactersNumber = msgIdPrefixNode.Attributes["randomStringCharactersNumber"] != null
                    ? int.Parse(msgIdPrefixNode.Attributes["randomStringCharactersNumber"].Value)
                    : 4;
            }
            else
            {
                settings.OfsRequestMessageIdPrefix_Prefix = "VAL";
                settings.OfsRequestMessageIdPrefix_RandomStringCharactersNumber = 4;
            }

            #endregion

            #region PostOFS related settings

            settings.OFSPostSettingsActionType = GetNodeInnerTextSave(connNode, "OFSPostSettingsActionType");
            settings.OFSPostMessageRequestFileFtpLocation = GetNodeInnerTextSave(connNode, "OFSPostMessageRequestFileFtpLocation");
            settings.OFSPostMessageResponseFileFtpLocation = GetNodeInnerTextSave(connNode, "OFSPostMessageResponseFileFtpLocation");
            settings.OFSPostSettingsFileFtpLocation = GetNodeInnerTextSave(connNode, "OFSPostSettingsFileFtpLocation");
            settings.OFSPostMessageSource = GetNodeInnerTextSave(connNode, "OFSPostMessageSource");
            settings.OFSPostSettingsFileFtpLocation = GetNodeInnerTextSave(connNode, "OFSPostSettingsFileFtpLocation");
            settings.OFSPostMessageQueuePrefix = GetNodeInnerTextSave(connNode, "OFSPostMessageQueuePrefix");
            settings.OFSPostMessageServicesT24CheckCommand = GetNodeInnerTextSave(connNode, "OFSPostMessageServicesT24CheckCommand");
            settings.OFSPostMessageServicesT24CWakeUpCommand = GetNodeInnerTextSave(connNode, "OFSPostMessageServicesT24CWakeUpCommand");
            settings.OFSPostRequiredAgentsT24CheckCommand = GetNodeInnerTextSave(connNode, "OFSPostRequiredAgentsT24CheckCommand");
            settings.OFSPostMessageSourceT24CheckCommand = GetNodeInnerTextSave(connNode, "OFSPostMessageSourceT24CheckCommand");
            settings.OFSPostMessageTSMServiceT24CheckCommand = GetNodeInnerTextSave(connNode, "OFSPostMessageTSMServiceT24CheckCommand");
            settings.OFSPostMessageServicesCheckCommand = GetNodeInnerTextSave(connNode, "OFSPostMessageServicesCheckCommand");

            try
            {
                var node = connNode.SelectSingleNode("OFSPostMessageServicesT24CWakeUpCommand");
                if (node != null && node.Attributes != null)
                {
                    if (node.Attributes["retryPeriod"] != null)
                    {
                        settings.OFSPostMessageServicesT24CWakeUpCommand_RetryPeriod =
                            int.Parse(node.Attributes["retryPeriod"].Value);
                    }
                }
            }
            catch
            {
                settings.OFSPostMessageServicesT24CWakeUpCommand_RetryPeriod = 10000;
            }

            try
            {
                var node = connNode.SelectSingleNode("OFSPostMessageServicesCheckCommand");
                if (node != null && node.Attributes != null)
                {
                    if (node.Attributes["waitFor"] != null)
                    {
                        settings.OFSPostMessageServicesCheckCommand_WaitFor =
                            node.Attributes["waitFor"].Value;
                    }
                }
            }
            catch
            {
                settings.OFSPostMessageServicesCheckCommand_WaitFor = "jsh";
            }

            try
            {
                var node = connNode.SelectSingleNode("OFSPostMessageFailoverRetry");
                if (node != null && node.Attributes != null)
                {
                    if (node.Attributes["retryPeriod"] != null)
                    {
                        settings.OFSPostMessageFailoverRetry_RetryPeriod =
                            long.Parse(node.Attributes["retryPeriod"].Value);
                    }

                    if (node.Attributes["attemptEvery"] != null)
                    {
                        settings.OFSPostMessageFailoverRetry_AttemptEvery =
                            long.Parse(node.Attributes["attemptEvery"].Value);
                    }
                }
            }
            catch
            {
                settings.OFSPostMessageFailoverRetry_RetryPeriod = 0;
                settings.OFSPostMessageFailoverRetry_AttemptEvery = 180000;
            }

            string ofsPostMessageAsyncTimeout = GetNodeInnerTextSave(connNode, "OfsPostMessageAsyncCommandTimeout");
            if (!String.IsNullOrEmpty(ofsPostMessageAsyncTimeout))
            {
                Int32.TryParse(ofsPostMessageAsyncTimeout, out settings.OfsPostMessageAsyncCommandTimeout);
            }

            #endregion

            // Telnet Threads Connect Response Timeout
            XmlNode telnetThreadConnectResponseTimeoutNode = connNode.SelectSingleNode("TelnetThreadsConnectResponseTimeout");
            if (telnetThreadConnectResponseTimeoutNode != null
                && !String.IsNullOrEmpty(telnetThreadConnectResponseTimeoutNode.InnerText))
            {
                int.TryParse(telnetThreadConnectResponseTimeoutNode.InnerText, out settings.TelnetThreadsConnectResponseTimeout);
            }

            // Multithreaded execution
            XmlNode telnetExportToT24ThreadsCountNode = connNode.SelectSingleNode("TelnetExportToT24ThreadsCount");
            if (telnetExportToT24ThreadsCountNode != null
                && !String.IsNullOrEmpty(telnetExportToT24ThreadsCountNode.InnerText))
            {
                int.TryParse(telnetExportToT24ThreadsCountNode.InnerText, out settings.TelnetExportToT24ThreadsCount);

                if (telnetExportToT24ThreadsCountNode.Attributes != null
                    && telnetExportToT24ThreadsCountNode.Attributes["min"] != null)
                {
                    int.TryParse(telnetExportToT24ThreadsCountNode.Attributes["min"].Value, out settings.TelnetExportToT24MinThreadsCount);
                }

                if (telnetExportToT24ThreadsCountNode.Attributes != null
                    && telnetExportToT24ThreadsCountNode.Attributes["initializationTimeout"] != null)
                {
                    int.TryParse(telnetExportToT24ThreadsCountNode.Attributes["initializationTimeout"].Value, out settings.TelnetExportToT24ThreadsInitializationTimeout);
                }
            }

            XmlNode telnetImportToT24ThreadsCountNode = connNode.SelectSingleNode("TelnetMultythreadedImportFromT24");
            if (telnetImportToT24ThreadsCountNode != null
                && !String.IsNullOrEmpty(telnetImportToT24ThreadsCountNode.InnerText))
            {
                int.TryParse(telnetImportToT24ThreadsCountNode.InnerText, out settings.TelnetImportFromT24ThreadsCount);

                if (telnetImportToT24ThreadsCountNode.Attributes != null
                    && telnetImportToT24ThreadsCountNode.Attributes["min"] != null)
                {
                    int.TryParse(telnetImportToT24ThreadsCountNode.Attributes["min"].Value, out settings.TelnetImportFromT24MinThreadsCount);
                }

                if (telnetImportToT24ThreadsCountNode.Attributes != null
                    && telnetImportToT24ThreadsCountNode.Attributes["initializationTimeout"] != null)
                {
                    int.TryParse(telnetImportToT24ThreadsCountNode.Attributes["initializationTimeout"].Value, out settings.TelnetImportFromT24ThreadsInitializationTimeout);
                }
            }

            XmlNode telnetTelnetPoolSizeNode = connNode.SelectSingleNode("TelnetPoolSize");
            if (telnetTelnetPoolSizeNode != null
                && !String.IsNullOrEmpty(telnetTelnetPoolSizeNode.InnerText))
            {
                int.TryParse(telnetTelnetPoolSizeNode.InnerText, out settings.TelnetPoolSize);
            }
            if (settings.TelnetPoolSize <= 0)
                settings.TelnetPoolSize = 1;

            XmlNode xmlNodeTelnetRequestBufferSize = connNode.SelectSingleNode("TelnetRequestBufferSize");
            if (xmlNodeTelnetRequestBufferSize != null
                && !String.IsNullOrEmpty(xmlNodeTelnetRequestBufferSize.InnerText))
            {
                int.TryParse(xmlNodeTelnetRequestBufferSize.InnerText, out settings.TelnetRequestBufferSize);
            }
            if (settings.TelnetRequestBufferSize <= 64) // there is no point to have buffer size smaller than 64
                settings.TelnetRequestBufferSize = -1;


            XmlNode timeoutTimeoutForFreeConnectionNode = connNode.SelectSingleNode("TimeoutForFreeConnection");
            if (timeoutTimeoutForFreeConnectionNode != null
                && !String.IsNullOrEmpty(timeoutTimeoutForFreeConnectionNode.InnerText))
            {
                long.TryParse(timeoutTimeoutForFreeConnectionNode.InnerText, out settings.TimeoutForFreeConnection);
            }
            if (settings.TimeoutForFreeConnection <= 1000/*1 sec*/)
            {
                settings.TimeoutForFreeConnection = 2 * 60 * 1000;//2 min
            }

            XmlNode commonStepsNode = connNode.SelectSingleNode("CommonExecutionSteps");
            LoadSteps(commonStepsNode, settings.CommonExecutionSteps);

            XmlNode connectionTypesNode = connNode.SelectSingleNode("ConnnectionTypes");
            LoadConnectionTypes(connectionTypesNode, settings);

            XmlNode joinTerminalLinesNode = connNode.SelectSingleNode("JoinTerminalLines");
            if (joinTerminalLinesNode != null)
            {
                bool joinTerminalLines = false;
                bool.TryParse(joinTerminalLinesNode.InnerText, out joinTerminalLines);
                settings.JoinTerminalLines = joinTerminalLines;
            }

            ReadDBConnectionSection(connNode, settings);
       }

        private static string getSSHInfoFromNode(XmlNode connNode, string text)
        {
            string res = null;
            try
            {
                res = GetNodeInnerTextSave(connNode, text);
                if (string.IsNullOrWhiteSpace(res)) res = null;
            }
            catch
            {
            }

            return res;
        }

        private static void ReadDBConnectionSection(XmlNode connNode, Settings settings)
        {

            XmlNode dbNode = connNode.SelectSingleNode("DBConnection");
            if (dbNode == null)
            {
                settings.DBConnection = null;
                return;
            }
            var dbConn = settings.DBConnection = new DBConnSettings();

            // Connection Type
            try
            {
                string innerText = GetNodeInnerTextSave(dbNode, "ConnectionType");
                if (!string.IsNullOrEmpty(innerText))
                {
                    dbConn.ConnType = (Settings.IOType)Enum.Parse(typeof(Settings.IOType), innerText.ToUpper(), true);
                    //if (dbConn.ConnType == Settings.IOType.LocalScript) return;
                }
            }
            catch { dbConn.ConnType = Settings.IOType.Telnet; }

            dbConn.ServerIP = FindRequiredNode(dbNode, "DBTelnetServer").InnerText;
            dbConn.Username = FindRequiredNode(dbNode, "DBLoginUserName").InnerText;
            dbConn.Password = GetDecryptedValue(FindRequiredNode(dbNode, "DBLoginPassword"));
            dbConn.Timeout = FindRequiredNode(dbNode, "DBResponseTimeOut").InnerText;

         


            // SSH Identity File & Passphrase
            dbConn.IdentityFile = getSSHInfoFromNode(dbNode, "IdentityFile");
            dbConn.Passphrase = getSSHInfoFromNode(dbNode, "Passphrase");

            dbConn.LoginExecutionSteps = new ExecutionSteps();
            if (dbConn.ConnType != Settings.IOType.LocalShell)
                LoadSteps(dbNode.SelectSingleNode("Shell"), dbConn.LoginExecutionSteps);

            ConnectionTypeSetting cts = new DBShellConfigSettings();
            int val;
            if (int.TryParse(dbConn.Timeout, out val))
            {
                cts.TelnetResponseTimeOut = val;
            }
            cts.LoginSteps = dbConn.LoginExecutionSteps.LoginSteps;
            cts.ExitSteps = dbConn.LoginExecutionSteps.ExitSteps;
            cts.CommonSettings = settings;

            settings.ConnectionTypes.Add(cts);

        }

        private static void ReadAATestGenerationSection(XmlNode telnetSetNode, Settings settings)
        {
            XmlNode connNode = telnetSetNode.SelectSingleNode("AATestGenerationRoutineParameters");
            if (connNode == null)
            {
                Settings.AATestGenerationRoutineParameters defaultSettings = new Settings.AATestGenerationRoutineParameters()
                {
                    CallDelimiter = "*",
                    Delimiter = "|",
                    RoutineName = "AAA.PRE.DATA",
                    TAFJ_or_TAFC = "TAFC",
                    Timeout = "60000",
                    AaArrangementActivityEnquiry = "%VAL.AAA",
                    AaProductEnquiry = DEFAULT_AA_PRODUCT_ENQUIRY,
                    CallProdEnqByGroup = true
                };
                settings.AaTestGenerationRoutineParameters = defaultSettings;
                return;
            }

            settings.AaTestGenerationRoutineParameters = new Settings.AATestGenerationRoutineParameters()
            {
                RoutineName = FindRequiredNode(connNode, "RoutineName").InnerText,
                CallDelimiter = FindRequiredNode(connNode, "CallDelimiter").InnerText,
                Delimiter = FindRequiredNode(connNode, "Delimiter").InnerText,
                Timeout = FindRequiredNode(connNode, "Timeout").InnerText,
                TAFJ_or_TAFC = FindRequiredNode(connNode, "TAFJ_or_TAFC").InnerText,
                AaArrangementActivityEnquiry = FindRequiredNode(connNode, "AaArrangementActivityEnquiry").InnerText
            };

            try
            {
                var prdEnqNode = FindRequiredNode(connNode, "AaProductEnquiry");
                settings.AaTestGenerationRoutineParameters.AaProductEnquiry = prdEnqNode.InnerText;

                if (prdEnqNode.Attributes != null && prdEnqNode.Attributes["callByGroup"] != null)
                {
                    settings.AaTestGenerationRoutineParameters.CallProdEnqByGroup = bool.Parse(prdEnqNode.Attributes["callByGroup"].Value);
                }
            }
            catch
            {
                settings.AaTestGenerationRoutineParameters.AaProductEnquiry = DEFAULT_AA_PRODUCT_ENQUIRY;
                settings.AaTestGenerationRoutineParameters.CallProdEnqByGroup = true;
            }
            

        }

        private static bool IsEcnrypted(XmlNode node)
        {
            if (node == null || node.Attributes == null)
            {
                return false;
            }

            XmlAttribute xmlAttribute = node.Attributes["encrypted"];
            if (xmlAttribute == null)
            {
                return false;
            }

            return string.Compare(xmlAttribute.Value, "true", true) == 0;
        }

        private static string GetDecryptedValue(XmlNode node)
        {
            return IsEcnrypted(node) ? Encryptor.Default.DecryptText(node.InnerText) : node.InnerText;
        }

        private static void LoadConnectionTypes(XmlNode node, Settings settings)
        {
            IList<ConnectionTypeSetting> types = settings.ConnectionTypes;

            if (node == null)
            {
                return;
            }

            types.Clear();
            foreach (XmlNode connectionTypeNode in node.ChildNodes)
            {
                ConnectionTypeSetting connTypeSettings = LoadConnectionType(connectionTypeNode, settings);
                if (connTypeSettings != null)
                {
                    types.Add(connTypeSettings);
                }
            }
        }

        private static ConnectionTypeSetting LoadConnectionType(XmlNode nodeConnType, Settings commonSettings)
        {
            ConnectionTypeSetting connSettings;
            if (nodeConnType == null)
            {
                return null;
            }

            ConsoleType? consoleType = Settings.GetConsoleTypeFromConnectionHandlerName(nodeConnType.Name);
            switch (consoleType)
            {
                case ConsoleType.JShell:
                    connSettings = new JShellConfigSettings();
                    break;
                case ConsoleType.Shell:
                    connSettings = new SystemShellConfigSettings();
                    break;
                case ConsoleType.OFS:
                    connSettings = new OfsConfigSettings();
                    break;
                case ConsoleType.Classic:
                    ClassicSettings classicSettings = new ClassicSettings();
                    ReadClassicSection(nodeConnType, classicSettings);
                    connSettings = classicSettings;
                    break;
                case ConsoleType.HttpWebService:
                    HttpWebServiceConfigSettings httpWebServiceSettings = new HttpWebServiceConfigSettings();
                    ReadHttpWebServiceSection(nodeConnType, httpWebServiceSettings, commonSettings);
                    connSettings = httpWebServiceSettings;
                    break;
                case ConsoleType.HttpRestService:
                    var httpRestServiceConfigSettings = new HttpRestServiceConfigSettings();
                    ReadHttpRestServiceSection(nodeConnType, httpRestServiceConfigSettings, commonSettings);
                    connSettings = httpRestServiceConfigSettings;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // set common properties, in order to inherit from them
            connSettings.CommonSettings = commonSettings;

            XmlNode userNode = nodeConnType.SelectSingleNode("T24LoginUserName");
            if (userNode != null)
            {
                connSettings.GlobusUserName = userNode.InnerText;
            }

            XmlNode passNode = nodeConnType.SelectSingleNode("T24LoginPassword");
            if (passNode != null)
            {
                connSettings.GlobusPassword = GetDecryptedValue(passNode);
            }

            XmlNode pageNode = nodeConnType.SelectSingleNode("PageCode");
            if (pageNode != null)
            {
                int val;
                if (int.TryParse(pageNode.InnerText, out val))
                {
                    connSettings.PageCode = val;
                }
            }

            XmlNode timeoutNode = nodeConnType.SelectSingleNode("TelnetResponseTimeOut");
            if (timeoutNode != null)
            {
                int val;
                if (int.TryParse(timeoutNode.InnerText, out val))
                {
                    connSettings.TelnetResponseTimeOut = val;
                }
            }

            // Load login & logout execution steps
            XmlNode commonStepsNode = nodeConnType.SelectSingleNode("ExecutionSteps");
            LoadSteps(commonStepsNode, connSettings);

            return connSettings;
        }

        private static void LoadSteps(XmlNode node, ExecutionSteps steps)
        {
            if (node == null)
                return;

            steps.ClearAllSteps();
            foreach (XmlNode stepNode in node.ChildNodes)
            {
                ExecutionStep executionStep = GetExecutionStepFromXmlNode(stepNode);
                if (stepNode.Name == "Login")
                {
                    steps.LoginSteps.Add(executionStep);
                }

                if (stepNode.Name == "Exit")
                {
                    steps.ExitSteps.Add(executionStep);
                }
            }
        }

        private static void ReadSpecialSection(XmlNode telnetSetNode, Settings settings)
        {
            XmlNode specialNode = telnetSetNode.SelectSingleNode("Special");
            if (specialNode == null)
            {
                throw new XmlException(string.Format("Missing section 'Special' for set {0}", settings.PhantomSet));
            }

            XmlNode suffix = specialNode.SelectSingleNode("CommandPromptSuffix");
            if (suffix != null)
            {
                settings.CommandPromptSuffix = suffix.InnerText;
            }

            XmlNode updExistsNode = specialNode.SelectSingleNode("UpdaterPackExistsResponse");
            if (updExistsNode != null)
            {
                settings.UpdaterPackExistedString = updExistsNode.InnerText;
            }

            XmlNode xmlNodeFinancialReportPath = specialNode.SelectSingleNode("FinancialReportPath");
            if (xmlNodeFinancialReportPath != null)
            {
                settings.FinancialReportPath = xmlNodeFinancialReportPath.InnerText;
            }

            XmlNode xmlNodeLWorkDayToCvf = specialNode.SelectSingleNode("LWorkDayToCvf");
            if (xmlNodeLWorkDayToCvf != null)
            {
                settings.LWorkDayToCvf = xmlNodeLWorkDayToCvf.InnerText;
            }

            XmlNode rollBackForceDeleteNode = specialNode.SelectSingleNode("RollBackForceDelete");
            if (rollBackForceDeleteNode != null)
            {
                settings.RollBackForceDelete = rollBackForceDeleteNode.InnerText;
            }
            else
            {
                settings.RollBackForceDelete = string.Empty;
            }

            try
            {
                XmlNode xmlNode = specialNode.SelectSingleNode("SkipWhiteSpaceInFieldValue");
                settings.SkipWhiteSpaceInFieldValue = bool.Parse(xmlNode.Attributes["value"].Value);
            }
            catch
            {
                settings.SkipWhiteSpaceInFieldValue = false;
            }

            XmlNode xmlCuNode = specialNode.SelectSingleNode("CoreUpdatesPath");
            settings.CoreUpdatesPath = xmlCuNode != null ? xmlCuNode.InnerText : null;

            XmlNode xmlTuNode = specialNode.SelectSingleNode("TamUpdatesPath");
            settings.TamUpdatesPath = xmlTuNode != null ? xmlTuNode.InnerText : null;


            try
            {
                XmlNode xmlNode = specialNode.SelectSingleNode("DeployAllBinaryFiles");
                settings.DeployAllBinaryFiles = bool.Parse(xmlNode.Attributes["value"].Value);
            }
            catch
            {
                settings.DeployAllBinaryFiles = true;
            }

            try
            {
                XmlNode xmlNode = specialNode.SelectSingleNode("TreatOFSResponseSingleValueAsError");
                settings.TreatOFSResponseSingleValueAsError = bool.Parse(xmlNode.Attributes["value"].Value);
            }
            catch
            {
                settings.TreatOFSResponseSingleValueAsError = false;
            }

            settings.IgnoreSubroutineCharacters = new[] { "@" };
            try
            {
                XmlNode xmlNode = specialNode.SelectSingleNode("IgnoreSubroutineCharacters");
                if (xmlNode != null)
                {
                    settings.IgnoreSubroutineCharacters = xmlNode.InnerText.Split(';');
                }
            }
            catch (Exception ex)
            {
                Debug.Fail("Unexpected Exception" + ex);
            }

            try
            {
                string innerText = GetNodeInnerTextSave(specialNode, "CopyCommandTimeOut");
                settings.CopyCommandTimeOut = (innerText != null) ? int.Parse(innerText) : 5 * 60 * 1000;
            }
            catch
            {
                // 5 min
                settings.CopyCommandTimeOut = 5 * 60 * 1000;
            }

            try
            {
                string innerText = GetNodeInnerTextSave(specialNode, "BuildControlDeploymentPath");
                settings.BuildControlDeploymentPath = innerText;
            }
            catch
            {
            }

            try
            {
                XmlNode bconNode = specialNode.SelectSingleNode("BCONVersions");
                BCONVersions bCONVersion = new IO.BCONVersions();
                bCONVersion.Save = null;
                bCONVersion.Release = null;
                bCONVersion.See = null;
                if (bconNode != null)
                {
                    bCONVersion.Save = (bconNode.Attributes["save"] != null) ? bconNode.Attributes["save"].InnerText : null;
                    bCONVersion.Release = (bconNode.Attributes["release"] != null) ? bconNode.Attributes["release"].InnerText : null; 
                    bCONVersion.See = (bconNode.Attributes["see"] != null) ? bconNode.Attributes["see"].InnerText : null; 
                }
                settings.BCONVersion = bCONVersion;
            }
            catch
            {
            }

            try
            {
                string innerText = GetNodeInnerTextSave(specialNode, "BuildControlSavePath");
                settings.BuildControlSavePath = innerText;
            }
            catch
            {
            }

            try
            {
                string innerText = GetNodeInnerTextSave(specialNode, "ArchiveExt");
                settings.ArchiveExt = innerText;
            }
            catch
            {
            }

            try
            {
                string innerText = GetNodeInnerTextSave(specialNode, "BuildControlDeploymentFields");
                settings.BuildControlDeploymentFields = innerText;
            }
            catch
            {
            }

            try
            {
                string innerText = GetNodeInnerTextSave(specialNode, "BuildControlRecordVerifyTimeOut");
                settings.BuildControlRecordVerifyTimeOut = (innerText != null) ? int.Parse(innerText) : 2 * 60 * 1000;
            }
            catch
            {
                settings.BuildControlRecordVerifyTimeOut = 2 * 60 * 1000;
            }

            try
            {
                XmlNode xmlNode = specialNode.SelectSingleNode("SkipCheckSum");
                settings.SkipCheckSum = bool.Parse(xmlNode.Attributes["value"].Value);
            }
            catch
            {
                settings.SkipCheckSum = false;
            }
            ReadTimeDateCommand(settings, specialNode);

            ReadT24AppRoutines(settings, specialNode);

            ReadSkippedResponses(settings, specialNode);

            ReadCompanies(settings, specialNode);

            ReadCustomActionResults(settings, specialNode);

            ReadOFSResultHandling(settings, specialNode);

            ReadJshellCommands(settings, specialNode);

            ReadShellCommands(settings, specialNode);

            settings.DuplicationString = Settings.DefaultDuplicationString;
            XmlNode dupStringNode = specialNode.SelectSingleNode("DuplicationString");
            if (dupStringNode != null)
            {
                settings.DuplicationString = dupStringNode.InnerText;
            }

            XmlNode ftpHomeNode = specialNode.SelectSingleNode("Ftp/Home");
            if (ftpHomeNode != null)
            {
                settings.FtpHome = ftpHomeNode.InnerText;
                if (string.IsNullOrEmpty(settings.FtpHome))
                {
                    settings.FtpHome = null;
                }
            }
            XmlNode ftpHomeNodeFull = specialNode.SelectSingleNode("Ftp/HomeFullPath");
            if (ftpHomeNodeFull != null)
            {
                settings.FtpHomeFullPath = ftpHomeNodeFull.InnerText;
            }

            XmlNode ftpListLineRegex = specialNode.SelectSingleNode("Ftp/ListLineRegex");
            if (ftpListLineRegex != null)
            {
                settings.FtpListLineRegex = ftpListLineRegex.InnerText;
            }

            XmlNode ftpDirectoryGroupText = specialNode.SelectSingleNode("Ftp/DirectoryGroupText");
            if (ftpDirectoryGroupText != null)
            {
                settings.FtpDirectoryGroupText = ftpDirectoryGroupText.InnerText;
            }

            XmlNodeList ftpListSkippedLinesRegex = specialNode.SelectNodes("Ftp/ListSkippedLines/SkippedLine");
            if (ftpListSkippedLinesRegex != null)
            {
                settings.FtpListSkippedLinesRegex = new List<String>();
                foreach (XmlNode node in ftpListSkippedLinesRegex)
                {
                    settings.FtpListSkippedLinesRegex.Add(node.InnerText);
                }
            }

            XmlNode ftpUsername = specialNode.SelectSingleNode("Ftp/Username");
            if (ftpUsername != null)
            {
                settings.FtpUsername = ftpUsername.InnerText;
            }

            XmlNode ftpPassword = specialNode.SelectSingleNode("Ftp/Password");
            if (ftpPassword != null)
            {
                settings.FtpPassword = GetDecryptedValue(ftpPassword);
            }

            XmlNode ftpHostName = specialNode.SelectSingleNode("Ftp/HostNameAndPort");
            if (ftpHostName != null)
            {
                settings.FtpHostNameAndPort = ftpHostName.InnerText;
            }

            XmlNode ftpIsSFtp = specialNode.SelectSingleNode("Ftp/IsSFtp");
            if (ftpIsSFtp != null)
            {
                bool isSFtp = false;
                bool.TryParse(ftpIsSFtp.InnerText, out isSFtp);
                settings.IsSFtp = isSFtp;
            }

            XmlNode useSharpSshFtpNode = specialNode.SelectSingleNode("Ftp/UseSharpSshFtp");
            if (useSharpSshFtpNode != null)
            {
                bool useSharpSshFtp = false;

                if (!bool.TryParse(useSharpSshFtpNode.InnerText, out useSharpSshFtp))
                    useSharpSshFtp = false;

                settings.UseSharpSshFtp = useSharpSshFtp;
            }

            XmlNode ftpIsSharedDir = specialNode.SelectSingleNode("Ftp/IsSharedDir");
            if (ftpIsSharedDir != null)
            {
                bool isSharedDir = false;
                bool.TryParse(ftpIsSharedDir.InnerText, out isSharedDir);
                settings.IsSharedDir = isSharedDir;
            }

            XmlNode ftpSetAccessMethod = specialNode.SelectSingleNode("Ftp/FTPSetAccessMethod");
            if (ftpSetAccessMethod != null)
            {
                switch (ftpSetAccessMethod.InnerText)
                {

                    case "shell":
                        settings.FTPSetAccessMethod = Settings.SetPermissionsApproach.Shell;
                        break;

                    case "skip":
                        settings.FTPSetAccessMethod = Settings.SetPermissionsApproach.Skip;
                        break;

                    case "comm":
                    default:

                        settings.FTPSetAccessMethod = Settings.SetPermissionsApproach.Communicator;
                        break;
                }

            }
            else settings.FTPSetAccessMethod = Settings.SetPermissionsApproach.Communicator;


            XmlNode keepAliveDefault = specialNode.SelectSingleNode("Ftp/KeepAliveDefault");
            if (keepAliveDefault != null)
            {
                bool keepAliveDefaultBool = false;
                bool.TryParse(keepAliveDefault.InnerText, out keepAliveDefaultBool);
                settings.FtpKeepAliveDefault = keepAliveDefaultBool;
            }

            XmlNode ftpKeepAliveFirst = specialNode.SelectSingleNode("Ftp/KeepAliveFirst");
            if (ftpKeepAliveFirst != null)
            {
                bool ftpKeepAliveFirstBool = false;
                bool.TryParse(ftpKeepAliveFirst.InnerText, out ftpKeepAliveFirstBool);
                settings.FtpKeepAliveFirst = ftpKeepAliveFirstBool;
            }

            XmlNode usePassive = specialNode.SelectSingleNode("Ftp/UsePassive");
            if (usePassive != null)
            {
                bool usePassiveBool = false;
                bool.TryParse(usePassive.InnerText, out usePassiveBool);
                settings.FtpUsePassive = usePassiveBool;
            }

            XmlNode identityFilePath = specialNode.SelectSingleNode("Ftp/IdentityFile");
            if (identityFilePath != null)
            {
                settings.FtpIdentityFile = identityFilePath.InnerText;
            }

            XmlNode jbcDevLib = specialNode.SelectSingleNode("JBCDEV_LIB");
            if (jbcDevLib != null)
            {
                settings.JBCDEV_LIB = jbcDevLib.InnerText;
            }

            XmlNode jbcDevBin = specialNode.SelectSingleNode("JBCDEV_BIN");
            if (jbcDevBin != null)
            {
                settings.JBCDEV_BIN = jbcDevBin.InnerText;
            }

            XmlNode jbcReplaceText = specialNode.SelectSingleNode("OFSReplaceText");
            if (jbcReplaceText != null)
            {
                settings.OFSReplaceText = jbcReplaceText.InnerText;
            }

            XmlNode pathGlobusBP = specialNode.SelectSingleNode("PATH_GlobusBP");
            if (pathGlobusBP != null)
            {
                settings.PathGlobusBP = pathGlobusBP.InnerText;
            }

            XmlNode pathGlobusLib = specialNode.SelectSingleNode("PATH_GlobusLib");
            if (pathGlobusLib != null)
            {
                settings.PathGlobusLib = pathGlobusLib.InnerText;
            }

            XmlNode pathGlobusBin = specialNode.SelectSingleNode("PATH_GlobusBin");
            if (pathGlobusBin != null)
            {
                settings.PathGlobusBin = pathGlobusBin.InnerText;
            }

            XmlNodeList objectDirOverrideNodes = specialNode.SelectNodes("ObjectDirOverrides/Catalog");
            if (objectDirOverrideNodes != null)
            {
                foreach (XmlNode node in objectDirOverrideNodes)
                {
                    string name = ReadStringAttribute(node, "name", null);
                    string objectDir = ReadStringAttribute(node, "objectDir", null);
                    if (name == null || objectDir == null)
                    {
                        Debug.Fail("Invalid Node");
                        continue;
                    }

                    if (settings.ObjectDirOverrides.ContainsKey(name))
                    {
                        Debug.Fail("Duplicated Node");
                        continue;
                    }

                    settings.ObjectDirOverrides.Add(name, objectDir);
                }
            }

            ReadRegexForIgnoredExtraLinesInCatalogCommandResponse(settings, specialNode);

            ReadSpecialTypicalAttributes(settings, specialNode);

            ReadCustomEnquiryNames(settings, specialNode);

            ReadNotAuthorizableApplications(settings, specialNode);

            ReadRecordsRollbackSettings(settings, specialNode);

            ReadUnexpectedCommunicationErrors(settings, specialNode);

            ReadT24LocalTableEnquiryResultColumns(settings, specialNode);

            ReadT24ProductionImportSettings(settings, specialNode);
        }

        private static void ReadCustomActionResults(Settings settings, XmlNode specialNode)
        {
            try
            {
                settings.CommandResults.Results.Clear();

                XmlNodeList xmlNodeCommandResults = specialNode.SelectNodes("CustomCommandResults/CommandResult");
                if (xmlNodeCommandResults != null)
                {
                    foreach (XmlNode node in xmlNodeCommandResults)
                    {
                        var cmd = ReadCustomCommandResult(node, true);
                        if (cmd != null)
                        {
                            settings.CommandResults.Results.Add(cmd);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in ReadCustomActionResults " + ex); // TODO better handling
            }
        }

        private static EndOfResponseCondition ReadEndOfResponseCondition(XmlNode node)
        {
            try
            {
                var result = new EndOfResponseCondition();
                string strTimeOut = ReadStringAttribute(node, "timeOut", null);
                int timeOut;
                if (strTimeOut != null && int.TryParse(strTimeOut, out timeOut))
                {
                    result.TimeOut = timeOut;
                }
                else
                {
                    result.TimeOut = -1;
                }

                result.IsRegEx = ReadBoolAttribute(node, "isRegEx", false);

                result.WaitForText = node.InnerText;

                return result;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Bad 'CustomCommandResult' node: " + ex);
                return null;
            }
        }

        private static CustomCommandResult ReadCustomCommandResult(XmlNode node, bool readNameAndInterface)
        {
            try
            {
                if (node == null)
                {
                    return null;
                }

                var cmd = new CustomCommandResult();

                if (readNameAndInterface)
                {
                    cmd.Name = ReadStringAttribute(node, "name", null);
                    if (cmd.Name == null)
                    {
                        Debug.WriteLine("Bad 'CustomCommandResult' node: missing 'name' attribute");
                        return null;
                    }

                    string intf = ReadStringAttribute(node, "interface", null);
                    if (intf == null)
                    {
                        Debug.WriteLine("Bad 'CustomCommandResult' node: missing 'interface' attribute");
                        return null;
                    }

                    cmd.Interface = (ConsoleType)StringMethods.ReadEnumFromString<ConsoleType>(intf);
                }

                cmd.Text = node.InnerText;

                cmd.IsSuccess = ReadBoolAttribute(node, "isSuccess", true);
                cmd.IsRegEx = ReadBoolAttribute(node, "isRegEx", false);
                cmd.IsPartial = ReadBoolAttribute(node, "isPartial", false);

                return cmd;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Bad 'CustomCommandResult' node: " + ex);
                return null;
            }
        }

        private static void ReadOFSResultHandling(Settings settings, XmlNode specialNode)
        {
            /* Usage in TelnetCommmDef.xml
             * 
             * 
             * 
            <OFSResultHandling>
				<OFSCommand action="V" version="BUILD.CONTROL">
					<CommandResult isSuccess="true" isRegEx="false" isPartial="true">Transfer of records to data accounts complete.</CommandResult>
					<EndOfResponseCondition timeOut="100000" isRegEx="true">NORMAL TERMINATION</EndOfResponseCondition>
				</OFSCommand>
			    </OFSResultHandling>
                */
            try
            {
                settings.OFSResultHandling.Clear();

                XmlNodeList xmlNodeCommandResults = specialNode.SelectNodes("OFSResultHandling/OFSCommand");
                if (xmlNodeCommandResults != null)
                {
                    foreach (XmlNode node in xmlNodeCommandResults)
                    {
                        try
                        {
                            string action = ReadStringAttribute(node, "action", null);
                            if (action == null)
                            {
                                Debug.WriteLine("Bad 'OFSResultHandling/OFSCommand' node: missing 'action' attribute");
                                continue;
                            }

                            string version = ReadStringAttribute(node, "version", null);
                            if (version == null)
                            {
                                Debug.WriteLine("Bad 'OFSResultHandling/OFSCommand' node: missing 'version' attribute");
                                continue;
                            }

                            var commandResult = ReadCustomCommandResult(node.SelectSingleNode("CommandResult"), false);
                            if (commandResult == null)
                            {
                                Debug.WriteLine("Bad 'OFSResultHandling/OFSCommand/CommandResult'");
                                continue;
                            }

                            commandResult.Name = "";
                            commandResult.Interface = ConsoleType.OFS;

                            EndOfResponseCondition endOfResponseCondition =
                                ReadEndOfResponseCondition(node.SelectSingleNode("EndOfResponseCondition"));

                            settings.OFSResultHandling.Add(action, version, commandResult, endOfResponseCondition);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("Bad 'CustomCommandResult' node: " + ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in ReadOFSResultHandling " + ex); // TODO better handling
            }
        }

        private static void ReadJshellCommands(Settings settings, XmlNode specialNode)
        {
            try
            {
                settings.JShellCommands.Clear();

                XmlNodeList shellCommandsNodes = specialNode.SelectNodes("JShellCommands/Command");
                if (shellCommandsNodes != null)
                {
                    foreach (XmlNode node in shellCommandsNodes)
                    {
                        try
                        {
                            string id = ReadStringAttribute(node, "id", null);
                            if (id == null)
                            {
                                Debug.Fail("Bad 'Command' node: missing 'id' attribute");
                                continue;
                            }

                            string text = ReadStringAttribute(node, "text", null);
                            if (text == null)
                            {
                                Debug.Fail("Bad 'Command' node: missing 'text' attribute");
                                continue;
                            }

                            JShellCommandType commandType = (JShellCommandType)Enum.Parse(typeof(JShellCommandType), id, true);
                            if (settings.JShellCommands.ContainsKey(commandType))
                            {
                                Debug.Fail("There should not exist  'Command' nodes with duplicate 'id'");
                            }

                            string successResponse = ReadStringAttribute(node, "responseSuccess", null);
                            if (successResponse == null)
                            {
                                //Debug.Fail("Bad 'Command' node: missing 'text' attribute");
                                //continue;
                            }

                            settings.JShellCommands[commandType] = new JShellCommand(text, successResponse);
                        }
                        catch (Exception ex)
                        {
                            Debug.Fail("Bad 'Command' node: " + ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in ReadJshellCommands " + ex); // TODO better handling
            }
            finally
            {
                JShellCommandHelper.FillMissingCommandsWithDefaultText(settings.JShellCommands);
            }
        }

        private static void ReadShellCommands(Settings settings, XmlNode specialNode)
        {
            try
            {
                settings.ShellCommands.Clear();

                XmlNodeList shellCommandsNodes = specialNode.SelectNodes("ShellCommands/Command");
                if (shellCommandsNodes != null)
                {
                    foreach (XmlNode node in shellCommandsNodes)
                    {
                        try
                        {
                            string id = ReadStringAttribute(node, "id", null);
                            if (id == null)
                            {
                                Debug.Fail("Bad 'Command' node: missing 'id' attribute");
                                continue;
                            }

                            string text = ReadStringAttribute(node, "text", null);
                            if (text == null)
                            {
                                Debug.Fail("Bad 'Command' node: missing 'text' attribute");
                                continue;
                            }

                            ShellCommandType commandType = (ShellCommandType)Enum.Parse(typeof(ShellCommandType), id, true);
                            if (settings.ShellCommands.ContainsKey(commandType))
                            {
                                Debug.Fail("There should not exist 'Command' nodes with duplicate 'id'");
                            }

                            var commandResult = ReadCustomCommandResult(node.SelectSingleNode("CommandResult"), false);

                            settings.ShellCommands[commandType] = new ShellCommand(text, commandResult);
                        }
                        catch (Exception ex)
                        {
                            Debug.Fail("Bad 'Command' node: " + ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in ReadShellCommands " + ex); // TODO better handling
            }
            finally
            {
                ShellCommandHelper.FillMissingCommandsWithDefaultText(settings.ShellCommands);
            }
        }

        private static string ReadStringAttribute(XmlNode node, string attribute, string def)
        {
            return node.Attributes[attribute] == null
                              ? def
                              : node.Attributes[attribute].Value;
        }

        private static bool ReadBoolAttribute(XmlNode node, string attribute, bool def)
        {
            return node.Attributes[attribute] == null
                              ? def
                              : bool.Parse(node.Attributes[attribute].Value);
        }

        private static bool ReadBoolValue(XmlNode node, bool defaultValue)
        {
            return node.InnerText == ""
                              ? defaultValue
                              : bool.Parse(node.InnerText);
        }

        private static void ReadClassicSection(XmlNode classicNode, ClassicSettings settings)
        {
            if (classicNode == null)
            {
                Debug.WriteLine("Classic settings are not defined!");
                return;
            }

            XmlNode commandHanling = classicNode.SelectSingleNode("CommandHandling");
            if (commandHanling == null)
            {
                Debug.WriteLine("(Classic) CommandHandling settings are not defined!");
                return;
            }

            XmlNodeList lsResponse = commandHanling.SelectNodes("Response");
            if (lsResponse == null)
            {
                Debug.WriteLine("(Classic) Response settings are missing!");
                return;
            }

            foreach (XmlNode nodeResponse in lsResponse)
            {
                string screen = nodeResponse.Attributes["Screen"] == null ? null : nodeResponse.Attributes["Screen"].Value;
                string applyOn = nodeResponse.Attributes["applyOn"] == null ? null : nodeResponse.Attributes["applyOn"].Value;

                ClassicSettings.Response response = settings.AddResponseItem(screen, applyOn);

                XmlNode waitForNode = nodeResponse.SelectSingleNode("WaitFor");
                if (waitForNode == null)
                {
                    Debug.WriteLine("(Classic) WaitFor undefined - set to default!");
                }
                else
                {
                    response.WaitFor = waitForNode.InnerText.Trim();
                }

                XmlNode messageItemNode = nodeResponse.SelectSingleNode("MessageItem");
                if (messageItemNode == null)
                {
                    Debug.WriteLine("(Classic) MessageItem undefined - set to default!");
                }
                else
                {
                    response.SetMessageItems(messageItemNode.InnerText.Trim());
                }

                XmlNodeList isSuccessNodes = nodeResponse.SelectNodes("IsSuccess");
                if (isSuccessNodes != null)
                {
                    foreach (XmlNode successNode in isSuccessNodes)
                    {
                        string method = successNode.Attributes["method"] == null ? null : successNode.Attributes["method"].Value;
                        string messageItem = successNode.Attributes["messageItem"] == null ? null : successNode.Attributes["messageItem"].Value;
                        string stringToCompare = successNode.InnerText.Trim();
                        int iMsgItem = -1;
                        if (messageItem != null)
                        {
                            int.TryParse(messageItem, out iMsgItem);
                        }

                        response.AddSuccessCondition(method, stringToCompare, iMsgItem);
                    }
                }

                XmlNodeList isFailNodes = nodeResponse.SelectNodes("IsFail");
                if (isFailNodes != null)
                {
                    foreach (XmlNode failNode in isFailNodes)
                    {
                        string method = failNode.Attributes["method"] == null ? null : failNode.Attributes["method"].Value;
                        string messageItem = failNode.Attributes["messageItem"] == null ? null : failNode.Attributes["messageItem"].Value;
                        string stringToCompare = failNode.InnerText.Trim();
                        int iMsgItem = -1;
                        if (messageItem != null)
                        {
                            int.TryParse(messageItem, out iMsgItem);
                        }

                        response.AddFailCondition(method, stringToCompare, iMsgItem);
                    }
                }
            }
        }

        private static void ReadHttpWebServiceSection(XmlNode httpWebServiceNode, HttpWebServiceConfigSettings httpWebServiceConfigSettings, Settings settings)
        {
            if (settings.Type != Settings.IOType.HttpWebService)
                return;

            if (httpWebServiceNode == null)
            {
                throw new XmlException("HttpWebService config settings are not defined!");
            }

            XmlNode ofsWebServiceUrl = httpWebServiceNode.SelectSingleNode("OFSWebServiceUrl");
            if (ofsWebServiceUrl != null)
            {
                httpWebServiceConfigSettings.OFSWebServiceUrl = ofsWebServiceUrl.InnerText.Trim();
            }
            else
            {
                throw new XmlException("(HttpWebService) OFSWebServiceUrl settings are not defined!");
            }

            XmlNode subroutineInvokerWebServiceUrl = httpWebServiceNode.SelectSingleNode("SubroutineInvokerWebServiceUrl");
            if (subroutineInvokerWebServiceUrl != null)
            {
                httpWebServiceConfigSettings.SubroutineInvokerWebServiceUrl = subroutineInvokerWebServiceUrl.InnerText.Trim();
            }
            else
            {
                throw new XmlException("(HttpWebService) SubroutineInvokerWebServiceUrl settings are not defined!");
            }

            XmlNode webServiceResponseTimeOut = httpWebServiceNode.SelectSingleNode("WebServiceResponseTimeOut");
            if (webServiceResponseTimeOut != null)
            {
                int timeOutValue;
                if (int.TryParse(webServiceResponseTimeOut.InnerText.Trim(), out timeOutValue)) httpWebServiceConfigSettings.WebServiceResponseTimeOut = timeOutValue;
                else httpWebServiceConfigSettings.WebServiceResponseTimeOut = 5 * 60 * 1000;// 5 min
            }
            else
            {
                throw new XmlException("(HttpWebService) WebServiceResponseTimeOut settings are missing!");
            }
        }

        private static void ReadHttpRestServiceSection(XmlNode httpRestServiceNode, HttpRestServiceConfigSettings httpRestServiceConfigSettings, Settings settings)
        {
            if (settings.Type != Settings.IOType.HttpRestService)
                return;

            if (httpRestServiceNode == null)
            {
                throw new XmlException("HttpRestService config settings are not defined!");
            }

            XmlNode ofsWebServiceUrl = httpRestServiceNode.SelectSingleNode("OFSWebServiceUrl");
            if (ofsWebServiceUrl != null)
            {
                httpRestServiceConfigSettings.OFSWebServiceUrl = ofsWebServiceUrl.InnerText.Trim();
            }
            else
            {
                throw new XmlException("(HttpRestService) OFSWebServiceUrl settings are not defined!");
            }

            XmlNode subroutineInvokerWebServiceUrl = httpRestServiceNode.SelectSingleNode("SubroutineInvokerWebServiceUrl");
            if (subroutineInvokerWebServiceUrl != null)
            {
                httpRestServiceConfigSettings.SubroutineInvokerWebServiceUrl = subroutineInvokerWebServiceUrl.InnerText.Trim();
            }
            else
            {
                throw new XmlException("(HttpRestService) SubroutineInvokerWebServiceUrl settings are not defined!");
            }

            XmlNode webServiceResponseTimeOut = httpRestServiceNode.SelectSingleNode("WebServiceResponseTimeOut");
            if (webServiceResponseTimeOut != null)
            {
                int timeOutValue;
                if (int.TryParse(webServiceResponseTimeOut.InnerText.Trim(), out timeOutValue)) httpRestServiceConfigSettings.WebServiceResponseTimeOut = timeOutValue;
                else httpRestServiceConfigSettings.WebServiceResponseTimeOut = 5 * 60 * 1000;// 5 min
            }
            else
            {
                throw new XmlException("(HttpRestService) WebServiceResponseTimeOut settings are missing!");
            }

            var username = httpRestServiceNode.SelectSingleNode("UserName");

            if (username != null)
            {
                httpRestServiceConfigSettings.Username = username.InnerText.Trim();
            }
            else
            {
                throw new XmlException("(HttpRestService) UserName is not defined!");
            }

            var password = httpRestServiceNode.SelectSingleNode("Password");

            if (password != null)
            {
                httpRestServiceConfigSettings.Password = password.InnerText.Trim();
            }
            else
            {
                throw new XmlException("(HttpRestService) Password is not defined!");
            }

            var ofsJsonPath = httpRestServiceNode.SelectSingleNode("OfsJsonPath");

            if (ofsJsonPath != null)
            {
                httpRestServiceConfigSettings.OfsJsonPath = ofsJsonPath.InnerText.Trim();
            }
            else
            {
                throw new XmlException("(HttpRestService) OfsJsonPath is not defined!");
            }

            var ofsRoutineJsonPath = httpRestServiceNode.SelectSingleNode("OfsRoutineJsonPath");

            if (ofsRoutineJsonPath != null)
            {
                httpRestServiceConfigSettings.OfsRoutineJsonPath = ofsRoutineJsonPath.InnerText.Trim();
            }
            else
            {
                throw new XmlException("(HttpRestService) OfsRoutineJsonPath is not defined!");
            }

            var httpMethod = httpRestServiceNode.SelectSingleNode("HttpMethod");

            if (httpMethod != null)
            {
                httpRestServiceConfigSettings.HttpMethod =  new HttpMethod(httpMethod.InnerText.Trim());
            }
            else
            {
                throw new XmlException("(HttpRestService) HttpMethod is not defined!");
            }

            var parameters = httpRestServiceNode.SelectSingleNode("Parameters");

            if (parameters != null)
            {
                httpRestServiceConfigSettings.Parameters = parameters.InnerText.Trim();
            }

            var number_of_routine_parameter = httpRestServiceNode.SelectSingleNode("RoutineParams");

            if (number_of_routine_parameter != null)
            {
                httpRestServiceConfigSettings.Number_Of_Routine_Parameter = Convert.ToInt32(number_of_routine_parameter.InnerText.Trim());
            }
        }

        private static void ReadRegexForIgnoredExtraLinesInCatalogCommandResponse(Settings settings, XmlNode specialNode)
        {
            List<string> regexes = new List<string>();

            XmlNodeList xmlNodeListRegexes = specialNode.SelectNodes("IgnoredExtraLinesInCatalogResponse/LineRegex");
            if (xmlNodeListRegexes != null)
            {
                foreach (XmlNode node in xmlNodeListRegexes)
                {
                    regexes.Add(node.InnerText);
                }
            }

            settings.RegexesForIgnoredExtraLinesInCatalogCommandResponse = regexes;
        }

        private static void ReadUnexpectedCommunicationErrors(Settings settings, XmlNode specialNode)
        {
            try
            {
                settings.UnexpectedCommunicationErrors.Clear();

                XmlNodeList xmlNodes = specialNode.SelectNodes("UnexpectedCommunicationErrors/ErrorString");
                if (xmlNodes != null)
                {
                    foreach (XmlNode node in xmlNodes)
                    {

                        ConsoleType? consoleType = node.Attributes["consoleType"] != null ? Settings.GetConsoleTypeFromConnectionHandlerName(node.Attributes["consoleType"].Value) : null;

                        if (node.Attributes["text"] == null)
                            throw new Exception("'text' attribute is not defined");

                        string text = node.Attributes["text"].Value;

                        string sendText = node.Attributes["sendText"] == null
                                              ? null
                                              : node.Attributes["sendText"].Value;

                        int errorMessageLines = 1;
                        if (node.Attributes["errorMessageLines"] != null)
                            int.TryParse(node.Attributes["errorMessageLines"].Value, out errorMessageLines);

                        bool isError = true;
                        if (node.Attributes["isError"] != null)
                            bool.TryParse(node.Attributes["isError"].Value, out isError);

                        UnexpectedCommunicationError.EnabledWhen enabled = UnexpectedCommunicationError.EnabledWhen.AfterLogin;
                        if (node.Attributes["enabled"] != null)
                            Enum.TryParse<UnexpectedCommunicationError.EnabledWhen>(node.Attributes["enabled"].Value, out enabled);

                        settings.UnexpectedCommunicationErrors.AddItem(consoleType, text, sendText, isError, errorMessageLines, enabled);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Bad UnexpectedCommunicationErrors node: " + ex);
            }
        }
        private static void ReadTimeDateCommand(Settings settings, XmlNode specialNode)
        {
            var xmlNode = specialNode.SelectSingleNode("TimeDateCommand");
            if (xmlNode != null)
            {
                var dateNode = xmlNode.SelectSingleNode("DateCommand");
                var timeNode = xmlNode.SelectSingleNode("TimeCommand");
                var timeDateCommand = new TimeDateCommand();

                if (dateNode != null)
                {
                    timeDateCommand.DateCommand = dateNode.InnerText;
                    if (dateNode.Attributes != null && dateNode.Attributes["formatString"] != null)
                    {
                        timeDateCommand.DateFormat = dateNode.Attributes["formatString"].Value;
                    }
                    if (dateNode.Attributes != null && dateNode.Attributes["consoleType"] != null)
                    {
                        int consoleType = 0;
                        int.TryParse(dateNode.Attributes["consoleType"].Value, out consoleType);
                        timeDateCommand.DateConsoleType = (ConsoleType)consoleType;
                    }
                }

                if (timeNode != null)
                {
                    timeDateCommand.TimeCommand = timeNode.InnerText;
                    if (timeNode.Attributes != null && timeNode.Attributes["formatString"] != null)
                    {
                        timeDateCommand.TimeFormat = timeNode.Attributes["formatString"].Value;
                    }
                    if (timeNode.Attributes != null && timeNode.Attributes["consoleType"] != null)
                    {
                        int consoleType = 0;
                        int.TryParse(timeNode.Attributes["consoleType"].Value, out consoleType);
                        timeDateCommand.TimeConsoleType = (ConsoleType)consoleType;
                    }
                }

                settings.SpecialTimeDateCommand = timeDateCommand;
            }
            else
            {
                settings.SpecialTimeDateCommand = new TimeDateCommand();
            }

        }
        private static void ReadT24AppRoutines(Settings settings, XmlNode specialNode)
        {
            XmlNodeList xmlNodeListRoutines = specialNode.SelectNodes("T24Routines/AppRoutine");
            if (xmlNodeListRoutines != null)
            {
                foreach (XmlNode node in xmlNodeListRoutines)
                {
                    try
                    {
                        XmlNode app = node.SelectSingleNode("./Application");
                        XmlNode routine = node.SelectSingleNode("./Routine");
                        XmlNode enableSmartOFS = node.SelectSingleNode("./EnableSmartOFS");
                        XmlNode sta = node.SelectSingleNode("./SequenceToAuth");
                        Routine.SequenceToA seqToA = Routine.SequenceToA.Nothing;

                        if (sta != null)
                            switch (sta.InnerText)
                            {
                                case "IA":
                                    seqToA = Routine.SequenceToA.IA;
                                    break;
                                case "A":
                                    seqToA = Routine.SequenceToA.A;
                                    break;
                                default:
                                    seqToA = Routine.SequenceToA.Nothing;
                                    break;
                            }

                        bool smartOFS = ReadBoolValue(enableSmartOFS, false);
                        XmlNode routineArgumentType = node.SelectSingleNode("./RoutineArgumentType");
                        var callType = Routine.CallArgumentTypeFromString(routineArgumentType == null ? null : routineArgumentType.InnerText);
                        settings.T24Routines[app.InnerText] = new Routine(routine.InnerText, smartOFS, callType, seqToA);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Bad AppRoutine node: " + ex);
                    }
                }
            }
        }

        private static void ReadSkippedResponses(Settings settings, XmlNode specialNode)
        {
            try
            {
                XmlNodeList xmlNodeListSkip = specialNode.SelectNodes("SkipResponses/Command");
                if (xmlNodeListSkip != null)
                {
                    foreach (XmlNode node in xmlNodeListSkip)
                    {
                        try
                        {
                            string name = node.Attributes["name"] == null
                                              ? null
                                              : node.Attributes["name"].Value;

                            XmlNodeList lsRl = node.SelectNodes("./ResponseLine");
                            if (lsRl != null)
                            {
                                foreach (XmlNode n in lsRl)
                                {
                                    settings.SkipLines.Add(name, n.InnerText);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("Bad 'ResponseLine' node: " + ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in ReadSkippedResponses " + ex); // TODO better handling
            }
        }

        private static void ReadCompanies(Settings settings, XmlNode specialNode)
        {
            try
            {
                XmlNode xmlDefaultCompany = specialNode.SelectSingleNode("DefaultCompany");
                if (xmlDefaultCompany != null)
                {
                    settings.Companies.SetDefault(xmlDefaultCompany.InnerText.Trim());
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in 'DefaultCompany' " + ex);
            }

            try
            {
                XmlNodeList nodes = specialNode.SelectNodes("UseCompany");
                if (nodes != null)
                {
                    foreach (XmlNode useCompany in nodes)
                    {
                        if (useCompany.Attributes["application"] == null)
                        {
                            continue;
                        }

                        string application = useCompany.Attributes["application"].Value.Trim();

                        settings.Companies.SetCompany(application, useCompany.InnerText.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in 'UseCompany' " + ex);
            }
        }

        private static void ReadSpecialTypicalAttributes(Settings settings, XmlNode specialNode)
        {
            XmlNodeList taList = specialNode.SelectNodes("SpecialTypicalAttributes/TypicalAttribute");

            if (taList != null && taList.Count > 0)
            {
                settings.SpecialTypicalAttibutes = new Dictionary<string, List<string>>();
                foreach (XmlNode ta in taList)
                {
                    string typicalName = ta.Attributes["TypicalName"].Value;
                    string attribName = ta.Attributes["AttributeName"].Value;

                    if (!settings.SpecialTypicalAttibutes.ContainsKey(typicalName))
                        settings.SpecialTypicalAttibutes.Add(typicalName, new List<string> { attribName });
                    else
                        settings.SpecialTypicalAttibutes[typicalName].Add(attribName);
                }
            }
        }

        private static void ReadCustomEnquiryNames(Settings settings, XmlNode specialNode)
        {
            settings.CustomEnquiries = new Dictionary<string, string>();

            XmlNodeList taList = specialNode.SelectNodes("CustomEnquiries/CustomEnquiry");
            if (taList != null && taList.Count > 0)
            {
                foreach (XmlNode ta in taList)
                {
                    string application = ta.Attributes["Application"].Value;
                    string enquiry = ta.Attributes["Enquiry"].Value;

                    if (!settings.CustomEnquiries.ContainsKey(application))
                        settings.CustomEnquiries.Add(application, enquiry);
                    else
                        settings.CustomEnquiries[application] = enquiry;
                }
            }
        }

        private static void ReadNotAuthorizableApplications(Settings settings, XmlNode specialNode)
        {
            XmlNodeList taList = specialNode.SelectNodes("NotAuthorizableApplications/Application");

            if (taList != null && taList.Count > 0)
            {
                settings.NotAuthorizableApplications = new List<string>();
                foreach (XmlNode ta in taList)
                {
                    settings.NotAuthorizableApplications.Add(ta.InnerText);
                }
            }
        }

        private static void ReadRecordsRollbackSettings(Settings settings, XmlNode specialNode)
        {
            settings.RecordRollbackOptions.SkipDeletionApplications = GetRecordRollbackListOfStrings(specialNode, "RecordsRollback/SkipDeletion/Application");
            settings.RecordRollbackOptions.SkipRestoreApplications = GetRecordRollbackListOfStrings(specialNode, "RecordsRollback/SkipRestore/Application");
            settings.RecordRollbackOptions.ReverseOfsApplications = GetRecordRollbackListOfStrings(specialNode, "RecordsRollback/ReverseOFSRestore/Application");
            settings.RecordRollbackOptions.IHLDApplications = GetRecordRollbackListOfStrings(specialNode, "RecordsRollback/IHLDRestore/Application");
            settings.RecordRollbackOptions.OnlyLiveTableApplications = GetRecordRollbackListOfStrings(specialNode, "RecordsRollback/OnlyLiveTable/Application");
            settings.RecordRollbackOptions.ApplicationsLiveTableNames = ReadApplicationsLiveTableNames(specialNode);
        }

        private static Dictionary<string, string> ReadApplicationsLiveTableNames(XmlNode specialNode)
        {
            XmlNodeList taList = specialNode.SelectNodes("RecordsRollback/ApplicationTableMappings/Application");

            Dictionary<string, string> result = new Dictionary<string, string>();
            if (taList != null && taList.Count > 0)
            {
                foreach (XmlNode ta in taList)
                {
                    result.Add(ta.Attributes["name"].Value, ta.Attributes["table"].Value);
                }
            }

            return result;
        }

        private static void ReadT24LocalTableEnquiryResultColumns(Settings settings, XmlNode specialNode)
        {
            settings.LocalTableEnquiryResultColumnsMap = new Dictionary<string, int>();
            XmlNode localTableNode = specialNode.SelectSingleNode("T24LocalTableEnquiryResultColumns");
            if (localTableNode != null)
            {
                settings.LocalTableEnquiryName = localTableNode.Attributes["EnquiryName"] != null ? localTableNode.Attributes["EnquiryName"].Value : String.Empty;

                foreach (XmlNode ta in localTableNode.ChildNodes)
                {
                    if (ta.NodeType == XmlNodeType.Comment)
                        continue;

                    string columnName = ta.Attributes["name"].Value;
                    int columnPos;
                    int.TryParse(ta.Attributes["position"].Value, out columnPos);

                    if (!settings.LocalTableEnquiryResultColumnsMap.ContainsKey(columnName))
                        settings.LocalTableEnquiryResultColumnsMap.Add(columnName, columnPos);
                }
            }
        }

        private static void ReadT24ProductionImportSettings(Settings settings, XmlNode specialNode)
        {
            XmlNode prodImportNode = specialNode.SelectSingleNode("T24ProductionImport");
            if (prodImportNode != null)
            {
                settings.ProductionImportOFS = GetNodeInnerTextSave(prodImportNode, "ProductionImportOFS");
                settings.PathForProductionImport = GetNodeInnerTextSave(prodImportNode, "PathForProductionImport");
                settings.PathForProductionImportErrors = GetNodeInnerTextSave(prodImportNode, "PathForProductionImportErrors");
                settings.CSVMultiValueSeparator = GetNodeInnerTextSave(prodImportNode, "CSVMultiValueSeparator");
                settings.CSVSubValueSeparator = GetNodeInnerTextSave(prodImportNode, "CSVSubValueSeparator");
                settings.CSVMultiValueSeparatorEscape = GetNodeInnerTextSave(prodImportNode, "CSVMultiValueSeparatorEscape");
                settings.CSVSubValueSeparatorEscape = GetNodeInnerTextSave(prodImportNode, "CSVSubValueSeparatorEscape");
            }
        }

        private static List<string> GetRecordRollbackListOfStrings(XmlNode specialNode, string xpath)
        {
            XmlNodeList taList = specialNode.SelectNodes(xpath);

            List<string> result = new List<string>();
            if (taList != null && taList.Count > 0)
            {
                foreach (XmlNode ta in taList)
                {
                    result.Add(ta.InnerText);
                }
            }

            return result;
        }

        private static XmlNode FindRequiredNode(XmlNode parent, string nodeText)
        {
            XmlNode node = parent.SelectSingleNode(nodeText);
            if (node == null)
            {
                throw new Exception(string.Format("Missing XML node '{0}' that should be a child of node '{1}'", nodeText, parent.Name));
            }

            return node;
        }

        private static string GetNodeInnerTextSave(XmlNode parent, string nodeText)
        {
            XmlNode node = parent.SelectSingleNode(nodeText);
            return node == null ? null : node.InnerText;
        }

        #endregion
    }
}
