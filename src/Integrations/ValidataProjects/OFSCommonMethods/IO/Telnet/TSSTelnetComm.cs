﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Configuration;
using System.Xml;
using System.Text.RegularExpressions;

namespace OFSCommonMethods.IO.Telnet
{
    public class TSSTelnetComm
    {
        const string LOGOUT_STEP_NAME = "logout";

        public delegate void LogMessageHandle(string line);

        private enum ResponseStatus
        {
            Total,
            TimeOut
        }

        #region Constants

        private const string LOGGING_DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss.fff";

        #endregion

        #region Private Members

        private readonly CommBase _Comm;

        private bool _IsLoggedIn;

        private readonly string _IPAddress;

        private readonly string _TestStepId;

        private readonly int _Port;

        private int _TimeOut = 7000;

        private readonly int _ConnectTimeOut = 7000;

        private String _AccountName, _AccountPassword;

        private readonly string _TelnetSet = String.Empty;

        private readonly string _ConnectionID;

        private string _Error;

        private StreamWriter _Logger;

        private ExecutionStepContainer _ExecutionSteps;

        private CustomTelnetResponseProcessing _CustomProcessing = new CustomTelnetResponseProcessing();

        private static Settings _Settings;

        private ResponseStatus _LastStatus;

        private string _ShellPrompt;

        private readonly ConsoleType _ConnectionType;

        private readonly object _Sync = new object();

        private TelnetCommErrors _handledCommErrors;

        private bool _retryStarted = false;
        private int _retryCount;
        private long _retryTimeout;
        private long _retryStartTime;
        private long _retryCountPassed;

        #endregion

        #region Events

        public event LogMessageHandle OnLogMessage;

        #endregion

        #region Internal Properties

        internal bool IsConnected
        {
            get { return _Comm.IsConnected; }
        }

        internal bool IsLogged
        {
            get { return _IsLoggedIn; }
        }

        /// <summary>
        /// Gets the last error and cleans it up. 
        /// TODO should we support such confusing property?
        /// </summary>
        /// <value>The last error.</value>
        public string LastError
        {
            get
            {
                string r = _Error;
                _Error = null;
                return r;
            }
        }

        private bool _inUse;


        internal bool InUse
        {
            get { return _inUse; }
            set
            {
                _inUse = value;
                Log(String.Format("InUse set to {0}", value));
            }
        }



        #endregion

        #region Class Lifecycle

        public TSSTelnetComm(ConsoleType connectionType, string ipAddress, int port, int timeOut, string phantomSet, string connectionID, Settings settings, string testStepId)
        {
            _ConnectionType = connectionType;
            _IPAddress = ipAddress;
            _Port = port;
            _TimeOut = timeOut;
            _TelnetSet = phantomSet;
            _ConnectionID = connectionID;
            _Settings = settings;
            _TestStepId = testStepId;
            _handledCommErrors = TelnetCommErrors.FromXmlFile();

            // Select Comm
            switch (settings.Type)
            {
                case Settings.IOType.SharpSsH:
                case Settings.IOType.SSH:
                    _Comm = new SshComm(_Settings, settings.Type);
                    break;
                case Settings.IOType.TCServer:
                    _Comm = new TCServerComm(_Settings);
                    break;
                case Settings.IOType.HttpWebService:
                    _Comm = new TelnetComm(_Settings); // same as for default just using return to avoid CreateLogger() method.
                    return;
                case Settings.IOType.HttpRestService:
                    _Comm = new TelnetComm(_Settings); // same as for default just using return to avoid CreateLogger() method.
                    return;
                default:
                    _Comm = new TelnetComm(_Settings);
                    break;
            }

            CreateLogger();

            LoadExecutionSteps();
        }

        ~TSSTelnetComm()
        {
            Debug.Assert(!IsLogged,
                         "Forgot to disconnect? The finalizer of TSSTelnetComm is executed, while the user is still logged in T24. Thread id = " + _ConnectionID ?? "?");

            Debug.Assert(!IsConnected,
                         "Forgot to disconnect? The finalizer of TSSTelnetComm is executed, while the telnet socket is still open. Thread id = " + _ConnectionID ?? "?");

            Disconnect();
        }


        #endregion

        #region Public and Internal Methods

        /// <summary>
        /// Changes the initial timeout value of the telnet comm 
        /// </summary>
        /// <param name="value">New timeout period</param>
        public void SetTimeOutPeriod(int value)
        {
            _TimeOut = value;
        }

        internal bool IsLocalShell()
        {
            return this._ConnectionType == ConsoleType.DBShell && _Settings.DBConnection.ConnType == Settings.IOType.LocalShell;
        }
            internal bool Connect()
        {
            Disconnect();

            lock (_Sync)
            {
                return ConnectA(_IPAddress, _Port);
            }
        }

        internal bool Disconnect()
        {
            if (_Settings.Type == Settings.IOType.HttpWebService || _Settings.Type == Settings.IOType.HttpRestService || IsLocalShell())
                return true;

            Log("Disconnecting ...");
            lock (_Sync)
            {
                return DisconnectA();
            }
        }

        // TODO find out where does this items come from 
        internal bool Login(string accountName, string password)
        {
            lock (_Sync)
            {
                _AccountName = accountName;
                _AccountPassword = password;

                return LoginA(accountName, password);
            }
        }

        public void SendCommand(string command, out string response)
        {
            lock (_Sync)
            {
                try
                {

                    try
                    {
                        SendCommandA(command, out response);
                    }
                    catch (Exception ex)
                    {
                        if (_handledCommErrors.Count > 0)
                        {
                            LogError(ex.Message);
                            Log(ex.StackTrace);
                            HandledCommError handledCommError = _handledCommErrors.ContainsError(ex.Message);
                            if (handledCommError != null)
                            {
                                Log(
                                    String.Format(
                                        "Retry rule found '{0}' with retry {1} times for total of {2} miliseconds",
                                        handledCommError.ErrorMessage, handledCommError.RetryCount,
                                        handledCommError.RetryTimeout));

                                _retryTimeout = handledCommError.RetryTimeout;
                                _retryCount = handledCommError.RetryCount;
                                _retryStartTime = Environment.TickCount;
                                bool retryNotConnected = true;

                                while ((_retryCount > _retryCountPassed) && ((Environment.TickCount - _retryStartTime) < _retryTimeout))
                                {
                                    try
                                    {
                                        Log(String.Format("Retrying to connect for {0} times", _retryCountPassed + 1));
                                        if (Connect())
                                        {
                                            Log(String.Format("Connected on attempt {0}", _retryCountPassed + 1));
                                            if (Login(_AccountName, _AccountPassword))
                                            {
                                                Log(String.Format("Logged in on attempt {0}", _retryCountPassed + 1));
                                                retryNotConnected = false;
                                                break;
                                            }
                                            else
                                            {
                                                LogError("Unable to Login");
                                                LogError(_Error);
                                            }
                                        }
                                        else
                                        {
                                            LogError("Unable to Connect");
                                            LogError(_Error);
                                        }

                                        _retryCountPassed++;
                                    }
                                    catch (Exception ConnReconnectEx)
                                    {
                                        LogError(ConnReconnectEx.Message);
                                        Log(ConnReconnectEx.StackTrace);
                                    }
                                }
                                if (retryNotConnected)
                                {
                                    string error = "Unable to reestablish connection in the specified time and attempts.";
                                    LogError(error);
                                    SetError(error);
                                    throw new ApplicationException(error);
                                }
                            }
                            else
                            {
                                throw;
                            }
                            response = string.Empty;
                        }
                        else
                        {
                            throw;
                        }
                    }

                }
                finally
                {
                    _retryCount = 0;
                    _retryCountPassed = 0;
                    InUse = false;
                }
            }
        }

        public void ExecuteShellCommand(string command, out string response)
        {
            lock (_Sync)
            {
                ExecuteShellCommand(command, _ShellPrompt, out response);
            }
        }

        internal void Log(string line)
        {
            Log(line, false);
        }
        private void Log(string line, bool onlyPartial)
        {
            try
            {
                if (_Logger != null && _Logger.BaseStream != null && _Logger.BaseStream.CanWrite)
                {
                    if (onlyPartial)
                    {
                        _Logger.Write(line);
                    }
                    else
                    {
                        _Logger.WriteLine(DateTime.Now.ToString(LOGGING_DATE_TIME_FORMAT));
                        _Logger.WriteLine(line);
                    }
                    _Logger.Flush();
                }
            }
            catch { }

            try
            {
                if (OnLogMessage != null)
                {
                    OnLogMessage(line);
                }
            }
            catch { }
        }

        #endregion

        #region Private Methods

        private bool ConnectA(string url, int port)
        {
            if (_Settings.Type == Settings.IOType.HttpWebService || _Settings.Type == Settings.IOType.HttpRestService || IsLocalShell())
                return true;

            if (_Logger == null || _Logger.BaseStream == null) CreateLogger();

            Log(string.Format("Connecting to: {0}:{1}", url, port));

            string errMsg = null;
            bool res = false;
            try
            {
                res = _Comm.Connect(url, port);
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
                Log(ex.StackTrace);
                errMsg = ex.Message;
            }

            if (res)
            {
                Log("\tDone!");
            }
            else
            {
                SetError(string.Format("Unable to connect to: {0}:{1}", url, port));
                if (errMsg != null)
                {
                    SetError(errMsg);
                }

                LogError(_Error);
            }

            return res;
        }

        private bool DisconnectA()
        {
            try
            {
                if (IsLogged)
                {
                    Logout();
                }

                if (IsConnected)
                {
                    Log("Closing Socket...");
                    string res = _Comm.Disconnect();
                    if (res == null)
                    {
                        Log("Closing Socket - Done!");
                    }
                    else
                    {
                        Log("ERROR Closing Socket: " + res);
                    }

                    CloseLogger();
                }

                return true;

            }
            catch (Exception ex)
            {
                Debug.Fail(ex.ToString());
            }
            finally
            {
                _IsLoggedIn = false;
            }

            return true;
        }

        private bool LoginA(string accountName, string password)
        {
            Log("Login...");

            if (_IsLoggedIn)
            {
                Log("\tDone! - Already Logged");
                return true;
            }

            string lastResponseLine = string.Empty;
            List<ExecutionStep> loginSteps = _Settings.GetConnectionSettingsByType(_ConnectionType).GetAllLoginSteps();

            foreach (ExecutionStep es in loginSteps)
            {
                // some login steps need account or password
                ExecutionStep loginStep = (ExecutionStep)es.Clone();
                loginStep.SetAccountName(accountName);
                loginStep.SetPassword(password);

                if (!ExecuteStep(loginStep, "login", out lastResponseLine))
                {
                    return false;
                }
            }

            if (_ConnectionType == ConsoleType.JShell || _ConnectionType == ConsoleType.Shell || _ConnectionType == ConsoleType.DBShell)
            {
                _ShellPrompt = lastResponseLine;
                DetectOS();
            }

            Log("Login Done!");

            _IsLoggedIn = true;

            return true;
        }

        private void Logout()
        {
            Log("Logging Out...");

            try
            {
                List<ExecutionStep> exitSteps = _Settings.GetConnectionSettingsByType(_ConnectionType).GetAllExitSteps();

                foreach (ExecutionStep es in exitSteps)
                {
                    string lastResponseLine;
                    bool res = ExecuteStep(es, LOGOUT_STEP_NAME, out lastResponseLine);
                    if (!res)
                    {
                        // TODO more details: command + response
                        Log(string.Format("Unable to execute logout step: '{0}'", es.StepName));
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Log("Error when trying to logout: " + ex.Message);
            }
            finally
            {
                Log("Logging Out - Completed!");
                _IsLoggedIn = false;
            }
        }

        private void SendCommandA(string command, out string response)
        {
            Log(String.Format("Start of SendCommandA for command '{0}'", command));
            response = string.Empty;

            SetError(null);
            string messageResidual = String.Empty;
            DeflateReceiveBuffer();

            Send(command);

            string strError = string.Empty;

            string[] res = null;
            bool isEnquiry = command.StartsWith("ENQUIRY.SELECT") ||
                             command.StartsWith(_Settings.OFSRequestStart + "ENQUIRY.SELECT");

            if (_Settings.Type == Settings.IOType.TCServer)
            {
                string tcServerResponse = GetTCServerResponse();

                if (tcServerResponse != null)
                {
                    res = new string[2];
                    res[0] = command;
                    res[1] = tcServerResponse;

                    if (isEnquiry)
                    {
                        string enquiryResult = tcServerResponse;
                        int indexStartData = enquiryResult.IndexOf(",\"");
                        if (indexStartData != -1)
                            enquiryResult = enquiryResult.Substring(0, indexStartData + 1) +
                                            "\r\n" +
                                            enquiryResult.Substring(indexStartData + 1);

                        Log("TCSERVER ENQUIRY RESPONSE ROW BY ROW:\r\n" + enquiryResult.Replace("\",\"", "\"\r\n\""));
                    }
                }
            }
            else
            {
                if (isEnquiry)
                {
                    res = ReceiveEnquiryLines(command);
                }
                else if (string.IsNullOrEmpty(_Settings.OFSResponseEnd))
                {
                    string residual;
                    res = ReceiveLines(command, 2, out residual);
                    messageResidual = EmptyReceiveBuffer(residual);
                }
                else
                {
                    //If we are using an OFS tag we should not limit the response to 2 lines.
                    //Instead we should wait for the end tag 
                    res = ReceiveAll(command, _Settings.OFSResponseEnd, 100, false);
                    if (_LastStatus == ResponseStatus.TimeOut)
                    {
                        strError = GetAsError(res);
                        res = null;
                    }
                    else
                    {
                        bool containsRequest;
                        res = DivideOfsResponse(res, out containsRequest);
                    }
                }
            }

            if (res == null)
            {
                SetError("Time Out during execution of current request." + (string.IsNullOrEmpty(strError) ? string.Empty : ("\r\n" + strError)));
                LogError(_Error);
                throw new TelnetCommException(_Error, TelnetCommException.ExceptionType.TimeOut);
            }

            string sent = res[0];
            string received = res[1];

            if (res[1].Contains(command))
            {
                sent = res[1];
                received = res[0];
            }

            response = ProcessResponse(sent, received);

            if (!string.IsNullOrEmpty(messageResidual))
            {
                response = response.Trim();
                response += (char)0x02;
                response += messageResidual;
                Debug.Fail("Should occurs really rarely! So please check WHY?!?!?!");
            }

            Log(String.Format("End of SendCommandA for command '{0}'", command));
        }

        private string GetTCServerResponse()
        {
            Log("Waiting for TCServer response...");

            var response = new StringBuilder();

            DateTime dtStart = DateTime.Now;
            do
            {
                byte[] buff = GetNext();
                if (buff != null && buff.Length > 0)
                {
                    dtStart = DateTime.Now;
                }

                string stringFromBytes = GetStringFromBytes(buff, false);
                response.Append(stringFromBytes);

                if (stringFromBytes.EndsWith("\r\n"))
                {
                    string result = response.ToString();
                    string[] lines = result.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                    Log(string.Format("Received: {0} lines!", lines.Length));
                    Log("RESULT:\r\n" + result);

                    return result;
                }

                Thread.Sleep(50);
            }
            while ((DateTime.Now - dtStart).TotalMilliseconds < _TimeOut);

            LogError("TIME OUT\r\nNOT received end character(Carriage return character) !");
            return null;
        }

        private void DetectOS()
        {
            if (_Settings.OS != "WIN" && _Settings.OS != "AIX")
            {
                string pwdResponse;
                ExecuteShellCommand("pwd", out pwdResponse);
                _Settings.OS = DetectOSFromPwdResponse(pwdResponse);
            }
        }

        private void ExecuteShellCommand(string command, string prompt, out string response)
        {
            Debug.Assert(!string.IsNullOrEmpty(prompt));

            response = string.Empty;

            try
            {
                SetError(null);
                DeflateReceiveBuffer();

                Send(command);
                response = GetCleanResponse(command, prompt);
            }
            catch (Exception ex)
            {
                SetError(ex.Message);
            }
        }

        /// <summary>
        /// Gets the response, without the sent command and without the prompt
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="prompt">The prompt.</param>
        /// <returns></returns>
        private string GetCleanResponse(string command, string prompt)
        {
            string result = string.Empty;

            string errMessage;
            string singleRes = Receive(prompt, out errMessage);

            if (singleRes == null)
            {
                _LastStatus = ResponseStatus.TimeOut;
                throw new ApplicationException(errMessage ?? "Failed to receive a response to command: " + command);
            }

            result += singleRes;

            if (!result.EndsWith(prompt))
            {
                _LastStatus = ResponseStatus.TimeOut;
                throw new ApplicationException(string.Format("The expected shell prompt: '{0}' has not been received", prompt));
            }

            _LastStatus = ResponseStatus.Total;

            // remove the prompt (NOTE: the prompt should be on a new line, but in rare cases it might be sticked to the response)
            string cleanResult = result.Substring(0, result.Length - prompt.Length);

            // remove trailing new line, preceding the prompt (TODO might affect the results - we should trim only 1 or 2 chars)
            cleanResult = cleanResult.TrimEnd('\r', '\n');

            if (cleanResult.StartsWith(command))
            {
                cleanResult = cleanResult.Substring(command.Length);

                // remove trailing new line (TODO might affect the results - we should trim only 1 or 2 chars)
                cleanResult = cleanResult.TrimStart('\r', '\n');
            }
            else
            {
                Debug.Write("There seems to be no echo");
            }

            return cleanResult;
        }

        private bool ExecuteStep(ExecutionStep es, string stepType, out string lastResponseLine)
        {
            Log(string.Format("Executing {0} step '{1}'", stepType, es.StepName));

            lastResponseLine = string.Empty;

            if (es.Command != null)
            {
                Send(es.Command, stepType == LOGOUT_STEP_NAME);
            }

            if (!string.IsNullOrEmpty(es.WaitFor))
            {
                string errMessage;
                string response = Receive(es.WaitFor, out errMessage);

                if (response == null)
                {
                    string errMsg = string.Format(
                            "Problem with {0} connection: {1} step: '{2}' failed! Details: {3}.",
                            _ConnectionType,
                            stepType,
                            es.StepName,
                            errMessage
                        );

                    SetError(errMsg);

                    return false;
                }

                lastResponseLine = GetLastLine(response);
            }

            Log(string.Format("Execution of {0} step '{1}' - DONE!", stepType, es.StepName));
            return true;
        }

        private string Receive(string expectedText, out string errMessage)
        {
            errMessage = null;

            Log(string.Format("Waiting to recieve '{0}' ...", expectedText));

            string response = string.Empty;
            bool startedLogging = false;

            DateTime dtStart = DateTime.Now;
            do
            {
                byte[] buff = GetNext();
                if (buff != null && buff.Length > 0)
                {
                    // Reset Time - Out Counter
                    dtStart = DateTime.Now;
                }
                var bytesText = GetStringFromBytes(buff, false);

                if (!string.IsNullOrEmpty(bytesText))
                {
                    response += bytesText;

                    if (!startedLogging)
                    {
                        Log("\r\nRECEIVING PARTS: \r\n" + bytesText);
                        startedLogging = true;
                    }
                    else
                    {
                        Log(bytesText, true);
                    }

                }
                if (response.Contains(expectedText))
                {
                    Log("\r\n", true);
                    Log("Received expected: " + expectedText);
                    return response;
                }

                Thread.Sleep(20);

                var tst = System.Threading.Thread.CurrentThread.ThreadState;
                if (tst.HasFlag(System.Threading.ThreadState.AbortRequested))
                {
                    Log("\r\n", true);
                    errMessage = "Thread abort requested";
                    LogError(errMessage);
                    return null;
                }
            }
            while ((DateTime.Now - dtStart).TotalMilliseconds < _TimeOut);

            Log("\r\n", true);
            errMessage = string.Format("TIME OUT\r\nNOT Received: {0}\r\n", expectedText);
            LogError(errMessage);
            return null;
        }

        private string GetStringFromBytes(byte[] buff, bool joinTerminalLinesExplicitely, string toSplit = null)
        {

            // On a Bitvise SSH server connection, the response has been observed to be 
            // structured into 80 char length lines, because 80 is the terminal width
            // Lines need to be joined.

            // Since this behavior is observed for all other Console types (OFS, jsh, shell..)
            // the joining fix is now moved inside the GetStringFromBytes(byte[] buff) override below
            // Note: Only implemented for SSH

            var commBytes = _Comm.GetStringFromBytes(buff, _Settings.JoinTerminalLines);

            // OLD logic commented, obsolete:
            // if (joinTerminalLinesExplicitely) return joinTerminalResponsesIfneeded(commBytes, toSplit);

            return commBytes;

        }

        private string[] ReceiveLines(string sentCommand, int count, out string residual)
        {
            residual = null;
            Log(string.Format("Waiting to receive {0} lines...", count));

            int currLine = 0;
            string response = string.Empty;
            string[] result = new string[count];
            DateTime dtStart = DateTime.Now;
            do
            {
                byte[] buff = GetNext();
                if (buff != null && buff.Length > 0)
                {
                    // Reset Time - Out Counter
                    dtStart = DateTime.Now;
                }

                response += GetStringFromBytes(buff, false);
                response = IsAwaytingSomething(sentCommand, response);

                int crPos;
                while ((crPos = response.IndexOf((char)AsciiCodes.CR)) >= 0)
                {
                    result[currLine++] = response.Substring(0, crPos);
                    response = response.Substring(crPos + 1);

                    result = ProcessSpecialLines(sentCommand, result, ref currLine);

                    if (currLine >= count)
                    {
                        residual = response;

                        Log(string.Format("Received: {0} lines!", count));
                        StringBuilder sb = new StringBuilder();
                        sb.Append("RESULT:\r\n");
                        foreach (string s in result)
                        {
                            sb.Append(s + "\r\n");
                        }

                        Log(sb.ToString());
                        return result;
                    }
                }

                Thread.Sleep(50);
            }
            while ((DateTime.Now - dtStart).TotalMilliseconds < _TimeOut);

            LogError(string.Format("TIME OUT\r\nNOT Received: {0} lines", count));
            return null;
        }

        private string[] ReceiveAll(string currentCommand, string terminatingText, int count, bool limitResponse)
        {
            string oneLine = string.Empty;
            string[] result = new string[count];
            string save1l = string.Empty;
            int currLine = 0;
            char[] removeArr = { (char)AsciiCodes.LF, (char)AsciiCodes.CR };

            List<CustomAction> customActions = _CustomProcessing.GetCustomActions(_Settings.OFSRequestStart, currentCommand);
            ProcessCustomActions(ref customActions, null);

            DateTime dtStart = DateTime.Now;
            _LastStatus = ResponseStatus.TimeOut;
            int lastErr = 0;
            do
            {
                string temp = GetNextTelnetResponse();

                if (!string.IsNullOrEmpty(temp))
                {
                    // Reset Time - Out Counter
                    dtStart = DateTime.Now;


                    save1l += temp;
                }

                ProcessCustomActions(ref customActions, save1l);

                if ((save1l.TrimEnd(removeArr)).EndsWith(terminatingText) ||
                    oneLine.Contains(terminatingText))
                {
                    _LastStatus = ResponseStatus.Total;
                    break;
                }

                save1l = ProcessSpecialLines(currentCommand, save1l);

                // Currently limitResponse is only called with false,
                // so this doesnt apply
                string[] lines = save1l.Split('\n');
                if (lines.Length >= count && limitResponse)
                {
                    _LastStatus = ResponseStatus.Total;
                    break;
                }

                // Check for unexpected errors
                CheckForUnexpectedCommunicationErrors(save1l, ref lastErr);


                Thread.Sleep(20);
            }
            while ((DateTime.Now - dtStart).TotalMilliseconds < _TimeOut);

            if (_LastStatus == ResponseStatus.TimeOut)
            {
                LogError(string.Format("TIME OUT\r\nTerminating text NOT received: {0}", terminatingText));
            }

            save1l = save1l.Replace((char)AsciiCodes.LF, (char)AsciiCodes.CR) + (char)AsciiCodes.CR;
            oneLine = save1l;
            if (oneLine.IndexOf((char)AsciiCodes.CR) == -1)
            {
                result[currLine] += oneLine;
            }
            else
            {
                if (!limitResponse)
                {
                    string[] longresult = oneLine.Split(new char[] { (char)AsciiCodes.CR }, StringSplitOptions.RemoveEmptyEntries);
                    return longresult;
                }

                int crPos;
                while ((crPos = oneLine.IndexOf((char)AsciiCodes.CR)) >= 0)
                {
                    result[currLine] += oneLine.Substring(0, crPos);
                    oneLine = oneLine.Substring(crPos + 1);
                    if (++currLine >= count)
                    {
                        return result;
                    }
                }
            }

            return result;
        }

        [Obsolete]
        private string joinTerminalResponsesIfneeded(string msg, string toSplit)
        {
            string res = msg;

            // If this behavior is observed for some special telnet server as well, 
            // uncomment the SSH condition to apply the joining fix.

            if (_Settings.Type == Settings.IOType.SharpSsH || _Settings.Type == Settings.IOType.SSH)
                if (_Settings.JoinTerminalLines)
                {
                    res = string.Empty;
                    if (msg.Contains("\n"))
                    {
                        foreach (var terminalLine in msg.Split(new char[] { '\n' }))
                        {
                            res += terminalLine.Trim();
                        }
                    }

                    // Split by tags because DivideOfsResponse is not called in caes of enquiry OFS

                    if (!string.IsNullOrEmpty(toSplit))
                        res = res.Replace(toSplit, "\r\n" + toSplit);
                }

            return res;
        }

        private void CheckForUnexpectedCommunicationErrors(string response, ref int lastErr)
        {
            string errorDisplayMessage = string.Empty;

            try
            {
                UnexpectedCommunicationErrorList unexpectedCommunicationErrors =
                    _Settings.UnexpectedCommunicationErrors.GetFilteredByConsoleType(ConsoleType.OFS);

                if (unexpectedCommunicationErrors == null)
                {
                    return;
                }

                UnexpectedCommunicationError unexpectedCommunicationError =
                    unexpectedCommunicationErrors.FindForMessage(response, ref lastErr);

                if (unexpectedCommunicationError == null ||
                    unexpectedCommunicationError.LinesToSend.Length == 0)
                {
                    return;
                }

                if (unexpectedCommunicationError.IsError)
                {
                    errorDisplayMessage = unexpectedCommunicationError.GetDisplayErrorMessage(response);
                    LogError(errorDisplayMessage);
                }

                foreach (string command in unexpectedCommunicationError.LinesToSend)
                {
                    Send(command);

                    // todo - for real recovery implement receive instead of Sleep
                    //System.Threading.Thread.Sleep(5000);

                    // empty the buffer
                    DeflateReceiveBuffer();
                }

                if (!unexpectedCommunicationError.IsError)
                {
                    return;
                }
            }
            catch
            {
                return;
            }

            System.Diagnostics.Debug.Assert(!string.IsNullOrEmpty(errorDisplayMessage));
            throw new TelnetCommException(errorDisplayMessage, TelnetCommException.ExceptionType.UnexpectedCommunicationError);
        }

        private void ProcessCustomActions(ref List<CustomAction> customActions, string currentBuff)
        {
            bool hasProcessed = false;
            for (int i = 0; i < customActions.Count; i++)
            {
                if (string.IsNullOrEmpty(customActions[i].WaitFor))
                {
                    Thread.Sleep(customActions[i].WaitMS);
                    if (!string.IsNullOrEmpty(customActions[i].Command))
                    {
                        Send(customActions[i].Command);
                        customActions[i] = null;
                        hasProcessed = true;
                    }
                }
                else if (currentBuff != null)
                {
                    if (currentBuff.Contains(customActions[i].WaitFor))
                    {
                        Send(customActions[i].Command);
                        customActions[i] = null;
                        hasProcessed = true;
                    }
                }
            }

            if (hasProcessed)
            {
                List<CustomAction> newList = new List<CustomAction>();
                foreach (CustomAction ca in customActions)
                {
                    if (ca != null)
                    {
                        newList.Add(ca);
                    }
                }

                customActions = newList;
            }
        }

        private string ProcessSpecialLines(string sentCommand, string currMessage)
        {
            if (_CustomProcessing.ProcessLines == null ||
                _CustomProcessing.ProcessLines.Count == 0)
            {
                return currMessage;
            }

            try
            {
                string[] lines = currMessage.Split('\n');

                for (int i = 0; i < lines.Length; i++)
                {
                    string doAction;
                    if (_CustomProcessing.ShouldSkipProcessLine(lines[i].Trim(), _Settings.OFSRequestStart, sentCommand, out doAction))
                    {
                        if (doAction != null)
                        {
                            Send(doAction);
                        }

                        lines[i] = null;
                    }
                }

                string result = string.Empty;
                foreach (string s in lines)
                {
                    if (s == null)
                    {
                        continue;
                    }

                    result += s;
                    result += '\n';
                }

                if (currMessage[currMessage.Length - 1] != '\n')
                {
                    result.Remove(result.Length - 1);
                }

                return result;
            }
            catch
            {
                // Just in case...
                return currMessage;
            }
        }

        private string[] ProcessSpecialLines(string sentCommand, string[] result, ref int currLine)
        {
            if (_CustomProcessing.ProcessLines == null ||
                _CustomProcessing.ProcessLines.Count == 0)
            {
                return result;
            }

            for (int i = 0; i < currLine; i++)
            {
                string doAction;
                if (_CustomProcessing.ShouldSkipProcessLine(result[i], _Settings.OFSRequestStart, sentCommand, out doAction))
                {
                    if (doAction != null)
                    {
                        Send(doAction);
                    }

                    result[i] = null;
                    currLine--;
                    return result;
                }
            }

            return result;
        }

        private string IsAwaytingSomething(string sentCommand, string response)
        {
            string doAction;
            if (!_CustomProcessing.ShouldSkipProcessLine(response, _Settings.OFSRequestStart, sentCommand, out doAction))
            {
                return response;
            }

            if (doAction == null)
            {
                return response;
            }

            Send(doAction);
            return string.Empty;
        }

        /// <summary>
        /// Receive telnet enquiry response and process it even if there are more than 2 lines.
        /// </summary>
        /// <returns></returns>
        private string[] ReceiveEnquiryLines(string sentCommand)
        {
            char[] removeArr = { (char)AsciiCodes.LF, (char)AsciiCodes.CR };

            Log("Receiving enquiry lines, Awaiting for: 2 lines");

            Debug.Assert(!string.IsNullOrEmpty(_Settings.OFSResponseStart) && !string.IsNullOrEmpty(_Settings.OFSResponseEnd)
                , "This method for receiving enquiry results should be executed only when 'OFSResponseStart' and 'OFSResponseEnd' are defined!");

            bool endTagRecived = false;
            string response = string.Empty;
            DateTime dtStart = DateTime.Now;
            string[] splitDelimiter = new string[] { ((char)AsciiCodes.CR).ToString() + ((char)AsciiCodes.LF).ToString() };

            bool isFirstWaitIterration = true;

            do
            {
                if (!isFirstWaitIterration)
                {
                    Thread.Sleep(50);
                }
                else
                {
                    isFirstWaitIterration = false;
                }


                byte[] buff = GetNext();
                if (buff != null && buff.Length > 0)
                {
                    // Reset Time - Out Counter
                    dtStart = DateTime.Now;
                }



                response += GetStringFromBytes(buff, true, _Settings.OFSResponseStart);
                if (!enqResponseLinesContainSentCommand(response))
                {
                    response = string.Format("{0}{1}{2}", sentCommand, splitDelimiter, response);
                }

                List<string> allLines = new List<string>(response.Split(splitDelimiter, StringSplitOptions.None));
                List<string> goodLines = new List<string>();
                int lastErr = 0;
                CheckForUnexpectedCommunicationErrors(response, ref lastErr);



                if (allLines.Count >= 2)
                {
                    // Collect the lines that are considered "good";
                    // For logging purpose, collect all the response lines in a single string
                    StringBuilder allLinesBuilder = new StringBuilder();
                    foreach (string responseLine in allLines)
                    {
                        if (!_CustomProcessing.ShouldSkipEnquieryLine(responseLine))
                            goodLines.Add(responseLine);

                        if (responseLine.TrimEnd(removeArr).EndsWith(_Settings.OFSResponseEnd))
                            endTagRecived = true;

                        allLinesBuilder.Append(responseLine);
                    }

                    //If there is an end tag we should wait for it
                    if (!endTagRecived)
                        continue;

                    // The good lines must be at least numLinesRequired, in order to consider the response valid
                    if (goodLines.Count >= 2)
                    {
                        // Create the response data line by concatenating the recieved good lines
                        StringBuilder goodLinesBuilder = new StringBuilder();
                        for (int goodLinesCounter = 1; goodLinesCounter < goodLines.Count; goodLinesCounter++)
                        {
                            goodLinesBuilder.Append(goodLines[goodLinesCounter]);
                            goodLinesBuilder.Append(Environment.NewLine);
                        }

                        // If the response is only new lines, then we should continue listening for the data
                        if (goodLinesBuilder.Replace(Environment.NewLine, String.Empty).Length == 0)
                            continue;

                        Log(string.Format("Received: {0} lines!", allLines.Count));
                        LogAllLinesRichness(allLines);

                        Log("TELNET FULL ENQUIRY RESPONSE:\r\n" + allLinesBuilder);

                        string allLinesString = allLinesBuilder.ToString();
                        int indexStartData = allLinesString.IndexOf(",\"");
                        if (indexStartData != -1)
                            allLinesString = allLinesString.Substring(0, indexStartData + 1) + "\r\n" + allLinesString.Substring(indexStartData + 1);

                        Log("TELNET ENQUIRY RESPONSE ROW BY ROW:\r\n" + allLinesString.Replace(",\"", "\r\n\""));

                        string[] result = new string[2]
                        {
                            goodLines[0],
                            goodLinesBuilder.ToString()
                        };
                        return result;
                    }
                }


            }
            while ((DateTime.Now - dtStart).TotalMilliseconds < _TimeOut);

            LogError("TIME OUT\r\nNOT Received: 2 lines");

            return null;
        }

        private bool enqResponseLinesContainSentCommand(string response)
        {
            return
                !string.IsNullOrEmpty(response)
                && response.Length >= _Settings.OFSRequestStart.Length
                && response.StartsWith(_Settings.OFSRequestStart);
        }

        private byte[] GetNext()
        {
            //Log("Reading buffer");
            byte[] bytes = _Comm.GetNext();
            //Log("Reading buffer: DONE");
            return bytes;
        }

        private void Send(string str, bool isLogoutStep = false)
        {
            Log("Sending Command:\r\n" + str);
            _Comm.Send(str, isLogoutStep);
            Log("Sending Command: DONE!");
        }

        private void SetError(string message)
        {
            _Error = message;
        }

        private void LogError(string line)
        {
            Log("ERROR: " + line);
        }

        private void LogAllLinesRichness(List<string> allLines)
        {
            StringBuilder sbLines = new StringBuilder();
            for (int i = 0; i < allLines.Count; i++)
            {
                sbLines.AppendFormat("Line {0} is {1}", i, String.IsNullOrEmpty(allLines[i]) ? "empty!" : "not empty.");
                sbLines.Append(_CustomProcessing.ShouldSkipEnquieryLine(allLines[i]) ? " Line is skipped." : "");
                sbLines.AppendLine();
            }

            Log(sbLines.ToString());
        }

        private void CreateLogger()
        {
            bool bCreateTelnetLogFile = true;
            string createTelnetLogFile = "True";

            if (!String.IsNullOrEmpty(createTelnetLogFile))
            {
                bool.TryParse(createTelnetLogFile, out bCreateTelnetLogFile);
            }

            if (!bCreateTelnetLogFile)
            {
                return;
            }

            string path = Path.Combine(TelnetCommPool.LogsFolder,
                                       string.Format("Telnet{0}_{1}.{2}.txt",
                                           string.IsNullOrEmpty(_TestStepId) ? string.Empty : $"_{_TestStepId}", // to avoid double undescore
                                           DateTime.Now.Ticks, 
                                           _ConnectionID));

            CreateLogDirectory(TelnetCommPool.LogsFolder);

            try
            {
                _Logger = new StreamWriter(File.Open(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.ReadWrite), Encoding.GetEncoding(_Settings.PageCode));
                Debug.WriteLine("Started logging at " + path);
            }
            catch
            {
                Debug.WriteLine("Can not start logging at " + path);
                _Logger = null;
            }
        }

        private static void CreateLogDirectory(string outputFolder)
        {
            if (string.IsNullOrEmpty(outputFolder))
            {
                // whenever the 'outputFolder' is null, it means that the logger is disabled
                //      => don't need to try to create the ouptut folder
                return;
            }

            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
            }
        }

        private void CloseLogger()
        {
            try
            {
                if (_Logger != null)
                {
                    _Logger.Close();
                }
            }
            catch { }
            finally
            {
                _Logger = null;
            }
        }

        private void LoadExecutionSteps()
        {
            _ExecutionSteps = new ExecutionStepContainer(_TelnetSet);
            _ExecutionSteps.Event += Log;
            _CustomProcessing = _ExecutionSteps.LoadCustomProcessing();
        }

        private string GetNextTelnetResponse()
        {
            byte[] rawData = GetNext();
            string textData = GetStringFromBytes(rawData, true);

            if (textData.Length > 0)
            {
                // TODO probably log it in a separate file
                Log(string.Format("{0} characters read from telnet session:\r\n{1}", textData.Length, textData));
            }

            return textData;
        }

        private void DeflateReceiveBuffer()
        {
            try
            {
                byte[] rawData = GetNext();
                if (rawData == null || rawData.Length == 0)
                {
                    // OK: No responses, which are not processed!
                    return;
                }

                // FAILE: Not processed telnet responses!
                string str = GetStringFromBytes(rawData, false);
                Log(string.Format("WARNING: Unprocessed response(s) detected:\r\n{0}", str));
            }
            catch (Exception ex)
            {
                Log(string.Format("INTERNAL EXCEPTION (0):\r\n{0}", ex.Message));
                Debug.Fail("DeflateReceiveBuffer");
            }
        }

        private string EmptyReceiveBuffer(string residual)
        {
            try
            {
                for (int i = 0; i < 100/*MAX ITERATIONS*/; i++)
                {
                    Thread.Sleep(50);

                    byte[] buff = GetNext();
                    if (buff == null)
                        break;

                    if (buff.Length == 0)
                        break;

                    residual += GetStringFromBytes(buff, false);
                }

                residual = residual.Trim();
                if (!string.IsNullOrEmpty(residual))
                    Log(string.Format("WARNING: Unprocessed response residual:\r\n{0}", residual));

                return residual;
            }
            catch   // just in case
            {
                return null;
            }
        }

        #endregion

        #region Private Static Methods

        /// <summary>
        /// Process responce message and remove the unnecessary prefix from the responce
        /// </summary>
        /// <param name="sentMessage">Sent OFS message to the Globus</param>
        /// <param name="receivedMessage">Received OFS message from Globus</param>
        /// <returns></returns>
        private static string ProcessResponse(string sentMessage, string receivedMessage)
        {
            if (receivedMessage.IndexOf((char)AsciiCodes.ESC) > -1)
            {
                string appID = GetAppID(sentMessage);
                if (!string.IsNullOrEmpty(appID))
                {
                    int appIDIndex = receivedMessage.IndexOf(appID);
                    if (appIDIndex > -1)
                        return receivedMessage.Substring(appIDIndex);
                }
            }
            else if (string.IsNullOrEmpty(_Settings.OFSResponseEnd)
                && (sentMessage.Contains("/A/PROCESS") || sentMessage.Contains("/S/PROCESS")))
            {
                int possition = receivedMessage.IndexOf("//");
                if (possition < 0)
                    return receivedMessage;

                string appId = GetAppID(sentMessage);
                if (!string.IsNullOrEmpty(appId))
                {
                    int appIdPosition = receivedMessage.IndexOf(appId);
                    if (appIdPosition > -1)
                        return receivedMessage.Substring(appIdPosition);
                }
                else
                {
                    int i;
                    for (i = possition - 1; i >= 0; i--)
                    {
                        if (!char.IsLetterOrDigit(receivedMessage[i]))
                            break;
                    }

                    return receivedMessage.Substring(i);
                }
            }

            return receivedMessage;
        }

        private static string GetAppID(string sentMessage)
        {
            int lastCommaIndex = sentMessage.LastIndexOf(',');
            string result = null;
            if (lastCommaIndex > -1)
            {
                result = sentMessage.Substring(lastCommaIndex + 1).Trim();
            }

            if (string.IsNullOrEmpty(result))
            {
                return null;
            }

            if (result == "No Value")
            {
                return null;
            }

            return result;
        }

        private static string GetAsError(string[] lines)
        {
            try
            {
                if (lines == null)
                {
                    return string.Empty;
                }
                if (lines.Length == 0)
                {
                    return string.Empty;
                }

                StringBuilder sbResult = new StringBuilder();
                sbResult.AppendLine("Unexpected response!");
                foreach (string line in lines)
                {
                    if (line == "\0")
                    {
                        continue;
                    }
                    sbResult.AppendLine(line);
                }

                return sbResult.ToString();
            }
            catch//Just in case...
            {
                return string.Empty;
            }
        }

        private static string[] DivideOfsResponse(string[] res, out bool containsRequest)
        {
            // Devides the whole response into 2 strings:
            // One is between OFSRequestStart and OFSRequestEnd, and one is the rest
            string[] devidedRes = new string[2];

            string wholeResponse = "";
            foreach (string line in res)
            {
                if (!string.IsNullOrEmpty(line))
                    wholeResponse += line;
            }

            int startPos = wholeResponse.IndexOf(_Settings.OFSRequestStart);
            int endPos = wholeResponse.IndexOf(_Settings.OFSRequestEnd) + _Settings.OFSRequestEnd.Length;

            if (startPos == -1 || endPos == _Settings.OFSRequestEnd.Length - 1)
            {
                containsRequest = false;
                devidedRes[0] = wholeResponse;
                devidedRes[1] = wholeResponse;
            }
            else
            {
                containsRequest = true;

                devidedRes[0] = wholeResponse.Substring(startPos, endPos - startPos);

                int startPosResponse = wholeResponse.IndexOf(_Settings.OFSResponseStart);
                int endPosResponse = wholeResponse.IndexOf(_Settings.OFSResponseEnd) + _Settings.OFSResponseEnd.Length;

                if (startPosResponse == -1 || endPosResponse == _Settings.OFSResponseEnd.Length - 1)
                {
                    devidedRes[1] = wholeResponse.Substring(startPosResponse, endPosResponse - startPosResponse);
                }
                else
                {
                    if (!string.IsNullOrEmpty(devidedRes[0]))
                    {
                        devidedRes[1] = wholeResponse.Replace(devidedRes[0], "");
                    }
                    else
                    {
                        devidedRes[1] = wholeResponse;
                    }
                }
            }

            if (!string.IsNullOrEmpty(_Settings.OFSResponseStart))
            {
                int pos = devidedRes[1].IndexOf(_Settings.OFSResponseStart);
                if (pos > -1)
                {
                    devidedRes[1] = devidedRes[1].Substring(pos);
                }
            }

            if (!string.IsNullOrEmpty(_Settings.OFSResponseEnd))
            {
                int pos = devidedRes[1].IndexOf(_Settings.OFSResponseEnd);
                if (pos > -1)
                {
                    devidedRes[1] = devidedRes[1].Substring(0, pos + _Settings.OFSResponseEnd.Length);
                }
            }

            return devidedRes;
        }

        private static string DetectOSFromPwdResponse(string pwdResponse)
        {
            if (string.IsNullOrEmpty(pwdResponse))
            {
                throw new Exception("Could not determine OS (WIN or AIX). Could not get and analyze the home directory");
            }

            if (pwdResponse.Contains("'pwd' is not recognized as an internal or external command,"))
            {
                return "WIN";
            }

            if (pwdResponse.StartsWith("/"))
            {
                return "AIX";
            }

            Debug.Assert(pwdResponse.Length > 3 && pwdResponse[1] == ':' && pwdResponse[2] == '\\');
            return "WIN";
        }

        private static string GetLastLine(string response)
        {
            if (string.IsNullOrEmpty(response))
            {
                return "";
            }

            response = response.Replace("\r", "");

            string[] lines = response.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length == 0)
                return "";

            return lines[lines.Length - 1];
        }

        #endregion
    }
}
