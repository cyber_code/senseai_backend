﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods.IO.Telnet
{
    public enum Operations
    {
        Contains,
        StartsWith,
        EndsWith,
        Equals
    }

    public class Line
    {
        public Operations Operation
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }

        public string Application
        {
            get;
            set;
        }

        public string Command
        {
            get;
            set;
        }

        internal bool IsForApplication(string requestStart, string currentSentCommand)
        {
            if (string.IsNullOrEmpty(this.Application))
            {
                return true;
            }

            if (currentSentCommand == null)
            {
                System.Diagnostics.Debug.Assert(false);
                return true;
            }

            return currentSentCommand.StartsWith(string.Format("{0}{1}", requestStart, this.Application));
        }
    }

    public class CustomAction
    {
        public string Application
        {
            get;
            set;
        }

        /// <summary>
        /// I, A, S ...
        /// </summary>
        public string Action
        {
            get;
            set;
        }

        public int WaitMS
        {
            get;
            set;
        }

        public string WaitFor
        {
            get;
            set;
        }

        public string Command
        {
            get;
            set;
        }

        public bool IsForApplication(string requestStart, string currentSentCommand)
        {
            if (string.IsNullOrEmpty(this.Application))
            {
                return true;
            }

            if (currentSentCommand == null)
            {
                System.Diagnostics.Debug.Assert(false);
                return true;
            }

            if (string.IsNullOrEmpty(this.Command))
            {
                return currentSentCommand.StartsWith(string.Format("{0}{1}", requestStart, this.Application));
            }

            if (this.Action == "I")
            if (currentSentCommand.StartsWith(string.Format("{0}{1}//", requestStart, this.Application)))
            {
                return true;
            }

            return currentSentCommand.StartsWith(
                    string.Format("{0}{1}/{2}", requestStart, this.Application, this.Command)
                );
        }
    }

    public class CustomTelnetResponseProcessing
    {
        public List<Line> EnquiryLines
        {
            get;
            set;
        }

        public List<Line> ProcessLines
        {
            get;
            set;
        }

        public List<CustomAction> CustomActions
        {
            get;
            set;
        }

        #region Class Lifecycle

        public CustomTelnetResponseProcessing()
        {
            EnquiryLines = new List<Line>();
            ProcessLines = new List<Line>();
            CustomActions = new List<CustomAction>();
        }

        #endregion

        #region Public Methods

        public bool ShouldSkipEnquieryLine(string line)
        {
            foreach (Line skipLine in EnquiryLines)
            {
                if (skipLine.Operation == Operations.Contains)
                {
                    if (line.Contains(skipLine.Value))
                    {
                        return true;
                    }
                }
                else if (skipLine.Operation == Operations.StartsWith)
                {
                    if (line.StartsWith(skipLine.Value))
                    {
                        return true;
                    }
                }
                else if (skipLine.Operation == Operations.EndsWith)
                {
                    if (line.EndsWith(skipLine.Value))
                    {
                        return true;
                    }
                }
                else if (skipLine.Operation == Operations.Equals)
                {
                    if (line.Equals(skipLine.Value))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool ShouldSkipProcessLine(string line, string requestStart, string currentSentCommand, out string doAction)
        {
            foreach (Line processLine in ProcessLines)
            {
                if (!processLine.IsForApplication(requestStart, currentSentCommand))
                {
                    continue;
                }

                doAction = processLine.Command;

                switch (processLine.Operation)
                {
                    case Operations.Contains:
                        if (line.Contains(processLine.Value))
                            return true;
                        break;

                    case Operations.StartsWith:
                        if (line.StartsWith(processLine.Value))
                            return true;
                        break;

                    case Operations.EndsWith:
                        if (line.EndsWith(processLine.Value))
                            return true;
                        break;

                    case Operations.Equals:
                        if (line.Equals(processLine.Value))
                            return true;
                        break;

                    default:
                        System.Diagnostics.Debug.Assert(false);
                        break;
                }
            }

            doAction = null;
            return false;
        }

        public List<CustomAction> GetCustomActions(string requestStart, string currentSentLine)
        {
            List<CustomAction> result = new List<CustomAction>();
            foreach (CustomAction ca in CustomActions)
            {
                if (ca.IsForApplication(requestStart, currentSentLine))
                {
                    result.Add(ca);
                }
            }

            return result;
        }

        #endregion
    }
}
