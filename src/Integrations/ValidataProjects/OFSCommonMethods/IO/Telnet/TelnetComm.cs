﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;

namespace OFSCommonMethods.IO.Telnet
{
    internal class TelnetComm : CommBase
    {
        #region Private Members

        private Socket _Socket;

        private IPEndPoint _IPEndPoint;

        private readonly int TelnetRequestBufferSize = -1; // default - no restriction

        private readonly List<byte> _UnreceivedBytes = new List<byte>();

        #endregion

        internal TelnetComm(Settings settings)
            : base(settings.PageCode)
        {
            System.Diagnostics.Debug.Assert(settings.Type != Settings.IOType.SharpSsH || settings.Type != Settings.IOType.SSH);

            TelnetRequestBufferSize = settings.TelnetRequestBufferSize;
        }

        #region CommBase Implementation

        internal override bool IsConnected
        {
            get { return _Socket != null && _Socket.Connected; }
        }

        internal override bool Connect(string url, int port)
        {
            try
            {
                IPAddress ipa = IPAddress.Parse(url);
                IPEndPoint ipe = new IPEndPoint(ipa, port);
                _Socket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                _Socket.Connect(ipe);

                if (_Socket.Connected)
                {
                    _IPEndPoint = ipe;
                    return true;
                }
            }
            catch (Exception) // TODO catch more detailed one
            {
                try
                {
                    IPHostEntry hostEntry = Dns.Resolve(url);

                    foreach (IPAddress address in hostEntry.AddressList)
                    {
                        IPEndPoint ipe = new IPEndPoint(address, port);
                        _Socket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                        _Socket.Connect(ipe);
                        if (_Socket.Connected)
                        {
                            _IPEndPoint = ipe;
                            return true;
                        }
                    }
                }
                catch (Exception)
                {
                    // TODO why the exception is swallowed
                }
            }

            return false;
        }

        internal override string Disconnect()
        {
            try
            {
                if (IsConnected)
                {
                    // Close Socket
                    _Socket.Close();
                }

                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            finally
            {
                _Socket = null;
            }
        }

        internal override void Send(string str, bool isLogoutStep)
        {
            if (TelnetRequestBufferSize <= 0 || str.Length <= TelnetRequestBufferSize)
            {
                // default behavior w/o restriction - send whole string
                byte[] bytesToSend = GetBytesToSend(str);
                _Socket.Send(bytesToSend, bytesToSend.Length, 0);
            }
            else
            {
                // send strings part by part
                str = str.Trim();

                do
                {
                    string toSend;
                    if (str.Length > TelnetRequestBufferSize)
                    {
                        toSend = str.Substring(0, TelnetRequestBufferSize);
                        str = str.Substring(TelnetRequestBufferSize);
                    }
                    else
                    {
                        toSend = str;
                        str = null;
                    }

                    bool isLastPart = str == null;
                    byte[] bytesToSend = GetBytesToSend(toSend, isLastPart);
                    _Socket.Send(bytesToSend, bytesToSend.Length, 0);

                    byte[] currentRead;
                    do
                    {
                        System.Threading.Thread.Sleep(20);
                        currentRead = GetNext_Inner();
                        _UnreceivedBytes.AddRange(currentRead);
                    } while (currentRead.Length > 0);

                } while (str != null);
            }
        }

        private byte[] GetBytesToSend(string toSend, bool appendCR)
        {
            if (appendCR)   // last one
                toSend += "\r";

            return System.Text.Encoding.GetEncoding(_PageCode).GetBytes(toSend);
        }

        internal override byte[] GetNext()
        {
            if (_UnreceivedBytes.Count == 0)
                return GetNext_Inner();

            _UnreceivedBytes.AddRange(GetNext_Inner());
            var result = _UnreceivedBytes.ToArray();
            _UnreceivedBytes.Clear();
            return result;
        }

        internal byte[] GetNext_Inner()
        {
            if (_Socket.Available == 0)
            {
                return new byte[0];
            }

            List<byte> result = new List<byte>();

            while (_Socket.Available > 0)
            {
                byte[] buff = new byte[1];
                _Socket.Receive(buff, 1, SocketFlags.None);

                switch (buff[0])
                {
                    case (byte)TelnetCommands.Iac:
                        // interpret as command
                        _Socket.Receive(buff, 1, SocketFlags.None);
                        byte inputverb = buff[0];

                        switch (inputverb)
                        {
                            case (byte)TelnetCommands.Iac:
                                //literal IAC = 255 escaped, so append char 255 to string
                                result.Add(inputverb);
                                break;

                            case (byte)TelnetCommands.Do:
                            case (byte)TelnetCommands.Dont:
                            case (byte)TelnetCommands.Will:
                            case (byte)TelnetCommands.Wont:
                                // reply to all commands with "WONT", unless it is SGA (suppres go ahead)
                                _Socket.Receive(buff, 1, SocketFlags.None);
                                int inputoption = buff[0];
                                if (inputoption == -1)
                                {
                                    break;
                                }
                                _Socket.Send(new byte[] { (byte)TelnetCommands.Iac }, 1, SocketFlags.None);

                                if (inputoption == (int)Options.SGA)
                                {
                                    _Socket.Send(
                                        new byte[] { inputverb == (byte)TelnetCommands.Do ? (byte)TelnetCommands.Will : (byte)TelnetCommands.Do },
                                        1,
                                        SocketFlags.None);
                                }
                                else
                                {
                                    _Socket.Send(
                                        new byte[] { inputverb == (byte)TelnetCommands.Do ? (byte)TelnetCommands.Wont : (byte)TelnetCommands.Dont },
                                        1,
                                        SocketFlags.None);
                                }
                                _Socket.Send(new byte[] { (byte)inputoption }, 1, SocketFlags.None);

                                break;
                            default:
                                break;
                        }
                        break;

                    default:
                        result.Add(buff[0]);
                        break;
                }
            }

            return result.ToArray();
        }

        internal override string GetStringFromBytes(byte[] rowData, bool joinTerminalLines)
        {
            return ANSIEscapeSequences.GetStringFromBytes(_PageCode, rowData);
        }

        #endregion
    }
}
