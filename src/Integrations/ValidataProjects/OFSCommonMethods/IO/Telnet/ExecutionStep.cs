﻿using System;

namespace OFSCommonMethods.IO.Telnet
{
    public class ExecutionStep : ICloneable
    {
        public string StepName;
        public string Command;
        public string WaitFor;

        public ExecutionStep(string stepName, string command, string waitFor)
        {
            StepName = stepName;
            Command = command;
            WaitFor = waitFor;
        }

        public override string ToString()
        {
            return (
                        "NAME: " + StepName ?? "" + "\r\n" +
                        "COMMAND: " + Command ?? "" + "\r\n" +
                        "WAIT_FOR: " + WaitFor ?? "" + "\r\n"
                   );
        }

        public void SetAccountName(string accountName)
        {
            Subst("[ACCOUNT_NAME]", accountName);
        }

        public void SetPassword(string password)
        {
            Subst("[PASSWORD]", password);
        }

        private void Subst(string template, string actualValue)
        {
            if (Command != null)
            {
                Command = Command.Replace(template, actualValue);
            }

            if (WaitFor != null)
            {
                WaitFor = WaitFor.Replace(template, actualValue);
            }
        }

        public object Clone()
        {
            ExecutionStep step = new ExecutionStep(StepName, Command, WaitFor);
            return step;
        }
    }
}