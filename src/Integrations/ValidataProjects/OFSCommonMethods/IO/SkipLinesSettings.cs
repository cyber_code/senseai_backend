﻿using System;
using System.Collections.Generic;
using System.Text;
namespace OFSCommonMethods.IO
{
    public class SkipLinesSettings : Dictionary<string, List<string>>
    {
        #region Class Lifecycle

        internal SkipLinesSettings()
            : base()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Should skip/ignore line from response
        /// </summary>
        /// <param name="commandName">command</param>
        /// <param name="line">current line</param>
        /// <returns>True is line should be ignored</returns>
        public bool ShouldIgnore(string commandName, string line)
        {
            List<string> lsToSkip = null;
            if (this.ContainsKey(commandName))
            {
                lsToSkip = this[commandName];
            }
            else if(this.ContainsKey(string.Empty))// global
            {
                lsToSkip = this[string.Empty];
            }
            else
            {
                return false;
            }

            line = line.Trim();
            foreach (string toSkip in lsToSkip)
            {
                if (toSkip == line)
                {
                    return true;
                }
            }

            return false;
        }

        public void Add(string commandName, string ignoreRespnseLine)
        {
            if (string.IsNullOrEmpty(commandName))
            {
                commandName = string.Empty;
            }

            if (!this.ContainsKey(commandName))
            {
                this.Add(commandName, new List<string>());
            }

            ignoreRespnseLine = ignoreRespnseLine.Trim();

            if (!this[commandName].Contains(ignoreRespnseLine))
            {
                this[commandName].Add(ignoreRespnseLine);
            }
        }

        #endregion
    }
}
