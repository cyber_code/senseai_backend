﻿using System;
using System.Text.RegularExpressions;
using log4net;

namespace OFSCommonMethods.IO
{
    public class JShellCommand
    {
        public readonly string Command;
        public string _SuccessResponseRegex;
        private Regex _SuccessRegex;

        public string SuccessResponseRegex
        {
            get { return _SuccessResponseRegex; }
            set
            {
                if (_SuccessResponseRegex != value)
                {
                    _SuccessRegex = null;
                }
                _SuccessResponseRegex = value;
            }
        }

        internal JShellCommand(string command, string successResponseRegex)
        {
            Command = command;
            SuccessResponseRegex = successResponseRegex;
        }

        public static readonly ILog SystemLog = LogManager.GetLogger("Validata.SAS.System");

        private void InitializeRegexIfNecessary()
        {
            if (_SuccessRegex != null)
            {
                return;
            }

            try
            {
                _SuccessRegex = new Regex(SuccessResponseRegex);
            }
            catch (Exception ex)
            {
                string message = string.Format("Invalid success criteria regex {0} for command {1} (in telnetcommdef.xml)", SuccessResponseRegex ?? "",
                    Command ?? "");
                SystemLog.Warn(message);
                throw new ApplicationException(message, ex);
            }
        }

        public bool CheckResponseSuccess(string response)
        {
            InitializeRegexIfNecessary();
            return _SuccessRegex.IsMatch(response);
        }

        public void CreateRegex()
        {
            if (!string.IsNullOrEmpty(_SuccessResponseRegex))
                InitializeRegexIfNecessary();
        }
    }
}