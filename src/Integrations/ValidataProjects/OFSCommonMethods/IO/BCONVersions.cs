﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OFSCommonMethods.IO
{
    public class BCONVersions
    {
        public string Save { get; set; }
        public string Release { get; set; }
        public string See { get; set; }
    }
}
