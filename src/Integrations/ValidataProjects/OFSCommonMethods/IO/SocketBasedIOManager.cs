﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using OFSCommonMethods.IO.Telnet;

namespace OFSCommonMethods.IO
{
    public abstract class SocketBasedIOManager : IOManager
    {
        #region Private Members

        private readonly Hashtable _MessageResults = new Hashtable();

        #endregion

        #region Protected Members

        protected string _URL = string.Empty;

        protected int _Port = 23;

        protected string _ExecutionID = "0";

        protected string _TestStepId = "";

        protected Settings _Settings;

        #endregion

        #region Protected Methods

        protected bool Execute(string message, out string response, TSSTelnetComm comm)
        {
            try
            {
                comm.SendCommand(message, out response);
            }
            catch (TelnetCommException ex)
            {
                if (ex.Type == TelnetCommException.ExceptionType.TimeOut)
                {
                    // Force close the current connection
                    TelnetCommPool.Release(comm);
                }
                else if (ex.Type == TelnetCommException.ExceptionType.UnexpectedCommunicationError)
                {
                    // do nothing - just re-throw
                }

                throw;
            }

            if (response != null)
            {
                response = response.Trim();
                return true;
            }

            return false;
        }

        protected bool Execute(string message, out string response)
        {
            TSSTelnetComm comm = null;

            if (RequireTSSReconnection)
            {
                //Release the connection
                TelnetCommPool.Release(_URL, _Port, _Settings, _ExecutionID);

                //Create new connection
                comm = TelnetCommPool.GetCommunicator(_URL, _Port, _Settings, _ExecutionID, _TestStepId);
            }
            else
            {
                comm = TelnetCommPool.GetCommunicator(_URL, _Port, _Settings, _ExecutionID, _TestStepId);
            }

            return Execute(message, out response, comm);
        }

        #endregion

        #region Class Lifecycle

        internal SocketBasedIOManager(Settings settings, string executionId, string testStepId = "")
        {
            _Settings = settings;
            ParseServer(_Settings.GlobusServer, _Settings.Type, out _URL, out _Port);

            if (!string.IsNullOrEmpty(executionId))
            {
                _ExecutionID = executionId;
            }

            if (!string.IsNullOrEmpty(testStepId))
            {
                _TestStepId = testStepId;
            }
        }

        #endregion

        #region Public Overrides (IOManager)

        /// <summary>
        /// Gets the messages that were received for the thread with the
        /// given guid.
        /// </summary>
        /// <param name="guid">The guid of the thread that waits for the messages</param>
        /// <returns>A Hashtable of messages found in the message results.</returns>
        public override Hashtable GetMessages(string guid)
        {
            // The GlobusIOManager keeps an ArrayList of results for every thread GUID that was registered in it
            lock (_MessageResults.SyncRoot)
            {
                if (!_MessageResults.ContainsKey(guid))
                {
                    return new Hashtable();
                }

                Hashtable results = (Hashtable)_MessageResults[guid];
                _MessageResults.Remove(guid);

                Hashtable res = new Hashtable();
                foreach (string key in results.Keys)
                {
                    GlobusMessage msg = new GlobusMessage(key);
                    msg.Message = (GlobusMessage.OFSMessage)results[key];
                    res.Add(key, msg);
                }

                return res;
            }
        }

        /// <summary>
        /// Reads all files from the output Globus directory
        /// </summary>
        /// <param name="isAuthorize">If true read from the authorize path, else  read from the data path</param>
        public override void ReadResults(bool isAuthorize)
        {
            Dictionary<string, AutoResetEvent> threadList = new Dictionary<string, AutoResetEvent>();

            lock (_MessageResults.SyncRoot)
            {
                foreach (string guid in _MessageResults.Keys)
                {
                    lock (_ThreadEvents.SyncRoot)
                    {
                        threadList[guid] = (AutoResetEvent)_ThreadEvents[guid];
                    }
                }
            }

            foreach (AutoResetEvent threadEvent in threadList.Values)
            {
                threadEvent.Set();
            }
        }

        /// <summary>
        /// Reads all files from the output Globus directory
        /// </summary>
        /// <param name="isAuthorize">If true read from the authorize path, else read from the data path</param>
        /// <param name="request"></param>
        /// <param name="requestGuid"></param>
        public override GlobusMessage GetDataResult(bool isAuthorize, GlobusRequestShort request, string requestGuid)
        {
            GlobusMessage responseMessage = new GlobusMessage(requestGuid);
            responseMessage.Message = GetMessage(request.GUID);
            // todo - is it the right place for such replacement?
            responseMessage.Message.Text = responseMessage.Message.Text.Replace("\n", String.Empty);
            return responseMessage;
        }

        #region Protected Methods

        protected void AddMessageResult(bool isAuthorize, GlobusMessage gm, GlobusMessage.OFSMessage ofsMessage)
        {
            lock (_MessageResults.SyncRoot)
            {
                string msgID;
                string guid;
                GetMessageInfo(gm.ID, out guid, out msgID);

                if (!_MessageResults.ContainsKey(guid))
                {
                    _MessageResults[guid] = new Hashtable();
                }

                Hashtable resultList = (Hashtable)_MessageResults[guid];

                // Now that we don't delete the messages until the end, we have to protect against reading many times one and the same message. 
                // This can be done simply by using the hashtable behaviour
                if (!resultList.ContainsKey(msgID))
                {
                    //this is a new message
                    resultList[msgID] = ofsMessage;
                }
            }
        }

        #endregion

        #region Private Methods

        private GlobusMessage.OFSMessage GetMessage(string requestGuid)
        {
            string guid;
            string id;
            GetMessageInfo(requestGuid, out guid, out id);

            return (GlobusMessage.OFSMessage)(((Hashtable)_MessageResults[guid])[id]);
        }

        #endregion

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Parse network address
        /// </summary>
        /// <param name="serverUrlAndIP"></param>
        /// <param name="type"></param>
        /// <param name="url"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public static string ParseServer(string serverUrlAndIP, Settings.IOType type, out string url, out int port)
        {
            const string ipNotSpecified = "Server and IP not specified";
            if (string.IsNullOrEmpty(serverUrlAndIP))
            {
                url = "UNDEFINED";
                port = 0;
                return ipNotSpecified;
            }

            string[] parts = serverUrlAndIP.Split(new char[] { ':' });

            if (parts.Length == 1)
            {
                url = parts[0];
                port = SelectDefaultPort(type);
            }
            else
            {
                System.Diagnostics.Debug.Assert(parts.Length >= 2);
                url = parts[0];
                if (!int.TryParse(parts[1], out port))
                {
                    port = SelectDefaultPort(type);
                }
            }

            if (port == -1)
            {
                const string unexpectedFormat = "The expected format for the T24 server is: {IP|MachineName}:{port}, e.g. 127.0.0.1:23 or serverName:23";
                return unexpectedFormat;
            }

            return null;


            //if (IsValidIP(serverUrlAndIP))
            //{
            //    string[] res = serverUrlAndIP.Split(new[] { ':' });
            //    url = res[0];
            //    int.TryParse(res[1], out port);
            //    return null;
            //}

            //if (IsValidIPWithoutPort(serverUrlAndIP))
            //{
            //    string[] res = serverUrlAndIP.Split(new[] { ':' });
            //    url = res[0];
            //    switch (type)
            //    {
            //        case Settings.IOType.SSH:
            //            port = 22;
            //            break;
            //        case Settings.IOType.TCServer:
            //            break;
            //        default:
            //            port = 23;
            //            break;
            //    }
            //    return null;
            //}

            //const string unexpectedFormat = "The expected format for the T24 server is: {IP|MachineName}[:{port}], e.g. 127.0.0.1:23 or serverName:23 or serverName";
            //return unexpectedFormat;
        }

        private static int SelectDefaultPort(Settings.IOType type)
        {
            switch (type)
            {
                case Settings.IOType.SharpSsH:
                case Settings.IOType.SSH:
                    return 22;

                case Settings.IOType.TCServer:
                    return -1;

                case Settings.IOType.Telnet:
                default:
                    return 23;
            }
        }

        //private static bool IsValidIP(string addr)
        //{
        //    const string pattern = @"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,5}$";
        //    var check = new Regex(pattern);

        //    return check.IsMatch(addr, 0);
        //}

        //private static bool IsValidIPWithoutPort(string addr)
        //{
        //    const string pattern = @"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}";
        //    var check = new Regex(pattern);

        //    return check.IsMatch(addr, 0);
        //}

        public static string ValidateServer(string serverUrlAndIP)
        {
            string url;
            int port;
            return ParseServer(serverUrlAndIP, Settings.IOType.None, out url, out port);
        }

        /// <summary>
        /// Tests the connection.
        /// </summary>
        /// <param name="descriptiveError">The descriptive error.</param>
        /// <param name="adapterIdentity">The adapter identity.</param>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        public static bool TestConnection(StringBuilder descriptiveError, WindowsIdentity adapterIdentity, Settings settings)
        {
            adapterIdentity.Impersonate();

            try
            {
                string url;
                int port;
                ParseServer(settings.GlobusServer, settings.Type, out url, out port);

                TSSTelnetComm comm = TelnetCommPool.GetCommunicator(url, port, settings, "TestConnection");

                // TODO we might reuse the connections
                TelnetCommPool.Release(comm);

                return true;
            }
            catch (Exception ex)
            {
                descriptiveError.Append(ex.Message);
                return false;
            }
        }

        public static void ReleaseConnection(Settings settings, string uniqueId)
        {
            string url;
            int port;
            ParseServer(settings.GlobusServer, settings.Type, out url, out port);
            TelnetCommPool.Release(url, port, settings, uniqueId);
        }

        #endregion
    }
}
