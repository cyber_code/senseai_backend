﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods.IO
{
    public class CommandResultSettings
    {
        private List<CustomCommandResult> _Results = new List<CustomCommandResult>();
        public List<CustomCommandResult> Results
        {
            get
            {
                return _Results;
            }
        }
    }
}
