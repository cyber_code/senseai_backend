﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace OFSCommonMethods.IO
{
    public class UnexpectedCommunicationError
    {
        public readonly ConsoleType? ConsoleType;

        public readonly string Text;

        public readonly string TextToSend;

        [Flags]
        public enum EnabledWhen : ulong
        {
            Never = 0x00,
            DuringLogin = 0x01,
            AfterLogin = 0x02,
            
            Always = DuringLogin | AfterLogin
        }

        public readonly EnabledWhen Enabled = EnabledWhen.AfterLogin; 

        public string[] LinesToSend
        {
            get
            {
                return TextToSend == null
                    ? new string[] { }
                    : TextToSend.Split(new[] { "\\r\\n" }, StringSplitOptions.None);
            }
        }

        public readonly bool IsError;

        public readonly int ErrorMessageLinesCount;

        public UnexpectedCommunicationError(ConsoleType? consoleType, string text, string textToSend, bool isError, int errorMessageLinesCount, EnabledWhen enabled = EnabledWhen.AfterLogin)
        {
            ConsoleType = consoleType;
            Text = text;
            TextToSend = textToSend;
            IsError = isError;
            ErrorMessageLinesCount = errorMessageLinesCount;
            Enabled = enabled;
        }

        public string GetDisplayErrorMessage(string text)
        {
            Debug.Assert(IsError, "Should be called only for errors");

            string[] lines = text.Split(new char[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);

            int lineFrom = -1;
            int lineTo = -1;

            for (int i = lines.Length - 1; i >= 0; i--)
            {
                if (lines[i].Contains(Text))
                {
                    lineFrom = i - ErrorMessageLinesCount + 1;
                    if (lineFrom < 0)
                    {
                        lineFrom = 0;
                    }

                    lineTo = i;
                    break;
                }
            }

            if (lineTo >= 0 && lineFrom >= 0)
            {
                StringBuilder sbResult = new StringBuilder();
                for (int i = lineFrom; i <= lineTo; i++)
                {
                    sbResult.AppendLine(lines[i]);
                }

                return sbResult.ToString();
            }
            else
            {
                Debug.Fail("???");
                return "Unexpected Error: " + Text;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (! string.IsNullOrEmpty(Text))
            {
                sb.Append("[Text: ]").Append(Text);
            }
            if (! string.IsNullOrEmpty(TextToSend))
            {
                sb.Append("[TextToSend: ]").Append(TextToSend);
            }

            return sb.ToString();
        }
    }

    public class UnexpectedCommunicationErrorList : List<UnexpectedCommunicationError>
    {
        public void AddItem(ConsoleType? inConsoleType, string text, string textToSend, bool isError, int errorMessageLinesCount, UnexpectedCommunicationError.EnabledWhen enabled = UnexpectedCommunicationError.EnabledWhen.AfterLogin)
        {
            Add(new UnexpectedCommunicationError(inConsoleType, text, textToSend, isError, errorMessageLinesCount, enabled));
        }

        public UnexpectedCommunicationErrorList GetFilteredByConsoleType(ConsoleType consoleType)
        {
            var result = new UnexpectedCommunicationErrorList();
            foreach (var cn in from item in this
                             where item.ConsoleType == null || item.ConsoleType == consoleType
                             select item)
            {
                result.Add(cn);
            }

            return result ;

        }

        public UnexpectedCommunicationErrorList GetEnabledDuringLoginAndLogoff()
        {
            var result = new UnexpectedCommunicationErrorList();
            foreach (var itm in this)
            {
                if (itm.Enabled.HasFlag(UnexpectedCommunicationError.EnabledWhen.DuringLogin))
                    result.Add(itm);
            }

            return result;
        }

        public UnexpectedCommunicationErrorList GetEnabledAfterLogin()
        {
            var result = new UnexpectedCommunicationErrorList();
            foreach (var itm in this)
            {
                if (itm.Enabled.HasFlag(UnexpectedCommunicationError.EnabledWhen.AfterLogin))
                    result.Add(itm);
            }

            return result;
        }

        public UnexpectedCommunicationError FindForMessage(string text, ref int latestErrPos)
        {
            foreach (UnexpectedCommunicationError e in this)
            {
                if (latestErrPos >= text.Length - 1) return null;
                int pos = text.IndexOf(e.Text, latestErrPos + 1);

                if (pos != null && pos != -1 )
                {
                    latestErrPos = pos;
                    return e;
                }
            }

            return null;
        }

    }
}