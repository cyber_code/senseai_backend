﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods.IO
{
    public class CompaniesSettings
    {
        private Dictionary<string, string> _ByTypical = new Dictionary<string, string>();

        private string _Default = null;

        public string GetCompany(string typicalName)
        {
            if (_ByTypical.ContainsKey(typicalName))
            {
                return _ByTypical[typicalName];
            }

            return _Default;
        }

        public void SetDefault(string defaultCompany)
        {
            if (defaultCompany == string.Empty)
            {
                defaultCompany = null;
            }

            _Default = defaultCompany;
        }

        public void SetCompany(string typical, string company)
        {
            if (_ByTypical.ContainsKey(typical))
            {
                _ByTypical[typical] = company;
            }
            else
            {
                _ByTypical.Add(typical, company);
            }
        }
    }
}
