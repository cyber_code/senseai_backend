﻿using System;
using System.Collections.Generic;

namespace OFSCommonMethods.IO
{
    public class ClassicSettings : ConnectionTypeSetting
    {
        #region Classes

        public class Response
        {
            #region Private Helper Classes

            private class Condition
            {
                #region Enums

                public enum ConditionType
                {
                    IsSuccess,
                    IsFail
                }

                #endregion

                #region Properties

                public ConditionType Type
                {
                    get;
                    set;
                }

                public string Method
                {
                    get;
                    set;
                }

                public string StringToCompare
                {
                    get;
                    set;
                }

                public int MessageItem
                {
                    get;
                    set;
                }

                #endregion

                #region Internal Methods

                internal bool IsSuccess(string[] lines)
                {
                    if (Type != ConditionType.IsSuccess)
                    {
                        return false;
                    }

                    return IsConditionSatisfied(lines);
                }

                internal bool IsFail(string[] lines)
                {
                    if (Type != ConditionType.IsFail)
                    {
                        return false;
                    }

                    return IsConditionSatisfied(lines);
                }

                #endregion

                #region Private Methods

                private bool IsConditionSatisfied(string[] lines)
                {
                    if (this.MessageItem >= lines.Length)
                    {
                        return false;
                    }

                    string line;
                    if (this.MessageItem < 0)
                    {
                        line = string.Empty;
                        foreach (string l in lines)
                        {
                            line += l + "\r\n";
                        }
                    }
                    else
                    {
                        line = lines[this.MessageItem];
                    }

                    return FindInLine(line, Method, StringToCompare);
                }

                private static bool FindInLine(string line, string method, string stingToCompare)
                {
                    switch (method)
                    {
                        case "Contains":
                            return line.Contains(stingToCompare);
                        case "StartsWith":
                            return line.StartsWith(stingToCompare);
                        case "EndsWith":
                            return line.EndsWith(stingToCompare);
                        default:
                            System.Diagnostics.Debug.Fail("Unknown method: " + method);
                            return false;
                    }
                }

                #endregion
            }

            #endregion

            #region Members

            private List<Condition> _Conditions = new List<Condition>();

            private List<int> _MessageItems = new List<int>();

            private readonly object _SyncConditions = new object();

            #endregion

            #region Properties

            public string Screen
            {
                get;
                private set;
            }

            public string ApplyOn
            {
                get;
                private set;
            }

            public string WaitFor
            {
                get;
                set;
            }

            internal IEnumerable<int> MessageItems
            {
                get { return _MessageItems; }
            }

            #endregion

            #region Class Lifecycle

            internal Response(string screen, string applyOn)
            {
                Screen = screen;
                ApplyOn = applyOn;

                // Initialize Defaults
                WaitFor = null;
                _MessageItems.Add(3);
            }

            #endregion

            #region Internal Methods

            internal void AddSuccessCondition(string method, string stringToCompare, int messageItem)
            {
                lock(_SyncConditions )
                {
                    Condition c = new Condition
                                      {
                                          Type = Condition.ConditionType.IsSuccess,
                                          Method = method,
                                          StringToCompare = stringToCompare,
                                          MessageItem = messageItem
                                      };

                    _Conditions.Add(c);
                }
            }

            internal void AddFailCondition(string method, string stringToCompare, int messageItem)
            {
                lock(_SyncConditions )
                {
                    Condition c = new Condition
                                      {
                                          Type = Condition.ConditionType.IsFail,
                                          Method = method,
                                          StringToCompare = stringToCompare,
                                          MessageItem = messageItem
                                      };
                    _Conditions.Add(c);
                }
            }

            internal void CopyConditions(Response src)
            {
                lock(_SyncConditions )
                {
                    _Conditions = new List<Condition>();
                    foreach (Condition src_c in src._Conditions)
                    {
                        Condition c = new Condition
                                          {
                                              Type = src_c.Type,
                                              Method = src_c.Method,
                                              StringToCompare = src_c.StringToCompare,
                                              MessageItem = src_c.MessageItem,
                                          };

                        _Conditions.Add(c);
                    }
                }
            }

            internal void SetMessageItems(string str)
            {
                _MessageItems.Clear();
                if (!string.IsNullOrEmpty(str))
                {
                    string[] arr = str.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string a in arr)
                    {
                        int itm;
                        if (int.TryParse(a.Trim(), out itm))
                        if (!_MessageItems.Contains(itm))
                        {
                            _MessageItems.Add(itm);
                        }
                    }
                }

                if (_MessageItems.Count == 0)
                {
                    _MessageItems.Add(3);
                }
            }

            internal void SetMessageItems(IEnumerable<int> msgItems)
            {
                _MessageItems = new List<int>(msgItems);
            }

            #endregion

            #region Public Methods

            public bool IsSuccess(string[] telnetResponse)
            {
                bool hasSuccess = false;
                bool hasFail = false;
                bool hasDefinedSuccess = false;
                lock (_SyncConditions)
                {
                    foreach (Condition c in _Conditions)
                    {
                        if (c.IsSuccess(telnetResponse))
                        {
                            hasSuccess = true;
                        }

                        if (c.IsFail(telnetResponse))
                        {
                            hasFail = true;
                        }

                        if (c.Type == Condition.ConditionType.IsSuccess)
                        {
                            hasDefinedSuccess = true;
                        }
                    }
                }
                if (hasFail)
                {
                    return false;
                }

                if (hasSuccess)
                {
                    return true;
                }

                if (hasDefinedSuccess)
                {
                    if (_Conditions.Count == 0)
                    {
                        // No condition->everithing is ok!
                        return true;
                    }

                    return false;
                }
                else
                {
                    return true;
                }
            }

            public string[] GetMessageItemsArray(string[] telnetResponse)
            {
                List<string> result= new List<string>();
                foreach (int i in this._MessageItems)
                {
                    if (telnetResponse.Length > i)
                    {
                        result.Add(telnetResponse[i]);
                    }
                }

                return result.ToArray();
            }

            #endregion

            
        }

        #endregion

        #region Members

        private readonly Dictionary<string, Dictionary<string, Response>> _Responses =
            new Dictionary<string, Dictionary<string, Response>>();

        #endregion

        #region Class Lifecycle

        internal ClassicSettings()
        {
            InitializeDefaultResponseItem();
        }

        #endregion

        #region Methods

        internal Response AddResponseItem(string screen, string applyOn)
        {
            Response response = PushResponseItem(screen, applyOn);
            return response;
        }

        public Response GetResponseFor(string scrreen, string command)
        {
            Dictionary<string, Response> forScreen;
            if (_Responses.ContainsKey(scrreen))
            {
                forScreen = _Responses[scrreen];
            }
            else if (_Responses.ContainsKey(""))
            {
                forScreen = _Responses[""];
            }
            else
            {
                System.Diagnostics.Debug.Fail("Why?");
                return DefaultResponse;
            }

            if (forScreen.ContainsKey(command))
            {
                return forScreen[command];
            }

            foreach (string s in forScreen.Keys)
            {
                if (command.StartsWith(s))
                {
                    return forScreen[s];
                }
            }

            if (forScreen.ContainsKey(""))
            {
                return forScreen[""];
            }

            System.Diagnostics.Debug.Fail("Why?");
            return DefaultResponse;
        }

        #endregion

        #region Private Methods

        private static Response _DefaultResponse; // TODO why static

        private Response DefaultResponse
        {
            get
            {
                if (_DefaultResponse == null)
                {
                    _DefaultResponse = new Response("Main", string.Empty);
                    _DefaultResponse.SetMessageItems("3");
                    _DefaultResponse.WaitFor = "AWAITING APPLICATION";
                    _DefaultResponse.AddFailCondition("EndsWith", "APPLICATION MISSING", 3);
                }

                return _DefaultResponse;
            }
        }

        private void InitializeDefaultResponseItem()
        {
            // Currently only main is possible
            Response response = PushResponseItem(DefaultResponse.Screen, DefaultResponse.ApplyOn);
            response.SetMessageItems(DefaultResponse.MessageItems);
            response.WaitFor = DefaultResponse.WaitFor;
            response.CopyConditions(DefaultResponse);
        }

        private Response PushResponseItem(string screen, string applyOn)
        {
            if (screen == null)
            {
                screen = string.Empty;
            }
            if (applyOn == null)
            {
                applyOn = string.Empty;
            }

            if (!_Responses.ContainsKey(screen))
            {
                _Responses.Add(screen, new Dictionary<string,Response>());
            }

            if (!_Responses[screen].ContainsKey(applyOn))
            {
                _Responses[screen].Add(applyOn, null);
            }

            _Responses[screen][applyOn] = new Response(screen, applyOn);

            return _Responses[screen][applyOn];
        }
            

        #endregion

        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.Classic; }
        }
    }
}
