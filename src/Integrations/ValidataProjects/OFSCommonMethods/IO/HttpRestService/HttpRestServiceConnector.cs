﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using Common;
using System.Text;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Web;

namespace OFSCommonMethods.IO.HttpRestService
{
    public class HttpRestServiceConnector : IDisposable
    {
        private enum RequestType
        {
            OFS,
            Routine
        }

        private readonly Logger _Logger;
        protected bool IsDisposed;
        private readonly HttpClient _Client;
        protected readonly string _ClientID;
        private readonly string _ofsJsonPath;
        private readonly HttpMethod _httpMethod;

        public const string TESTING_CONNECTION = "Testing Connection";

        protected HttpRestServiceConnector(string username, string password, string ofsJsonPath, HttpMethod httpMethod)
        {
            _Client = new HttpClient();
            _ClientID = getRandomID();

            _ofsJsonPath = ofsJsonPath;
            _httpMethod = httpMethod;

            var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", username, password));
            var authHeaderValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            _Client.DefaultRequestHeaders.Authorization = authHeaderValue;
        }

        private string getRandomID()
        {
            int capA = 'A';
            int capZ = 'Z';

            Random rand = new Random();
            return $"{(char)rand.Next(capA, capZ)}{(char)rand.Next(capA, capZ)}{(char)rand.Next(capA, capZ)}";
        }

        public HttpRestServiceConnector(Logger logger, string username, string password, string ofsJsonPath, HttpMethod httpMethod)
            : this(username, password, ofsJsonPath, httpMethod)
        {
            _Logger = logger;
        }

        ~HttpRestServiceConnector()
        {
            if (!IsDisposed)
                Dispose();
        }

        protected virtual void LogInfo(string message)
        {
            if (_Logger != null)
            {
                if (string.IsNullOrEmpty(_ClientID))
                    _Logger.Info(message);
                else
                    _Logger.Info($"[Request: {_ClientID}] {message}");
            }
        }

        protected virtual void LogError(string message, Exception ex = null)
        {
            if (_Logger != null)
                _Logger.Error(message, ex);
        }

        public virtual void Dispose()
        {
            if (_Client != null && !IsDisposed)
            {
                _Client.Dispose();
                IsDisposed = true;
            }
        }

        public bool RunHttpWebServiceCommand(string command, string httpWebServiceUrl, int timeout, out string webRequestResult, out string webRequestError, string method = "GET")
        {
            webRequestResult = string.Empty;
            bool createLog = command != TESTING_CONNECTION;
            try
            {
                return SendWebRequest(command, httpWebServiceUrl, timeout, out webRequestResult, out webRequestError, createLog, method);
            }
            catch (AggregateException ex)
            {
                if (ex.InnerExceptions.Count == 1)
                {
                    Exception e = ex.InnerExceptions[0];
                    webRequestError = CreateErrorMessage(e, command);
                    if (createLog) LogError(webRequestError);
                    return false;
                }

                string errorMessage = string.Format("AggregateException occured while processing command: '{0}'\r\n", command);
                foreach (Exception e in ex.InnerExceptions)
                {
                    if (e is TaskCanceledException)
                    {
                        if (command == TESTING_CONNECTION)
                        {
                            errorMessage = "Timeout occured while testing connection.";
                            if (createLog) LogError("Timeout occured while testing connection.");
                        }
                        else
                        {
                            errorMessage = string.Format("Timeout occured while processing command: '{0}'\r\n", command);
                            if (createLog) LogError(string.Format("Due timeout failed to process command: '{0}'", command), e);
                        }
                    }
                    else
                    {
                        if (createLog) LogError(string.Format("Failed to process command '{0}'", command), e);
                    }
                }

                webRequestError = "Error: " + errorMessage;
                return false;
            }
            catch (Exception ex)
            {
                if (createLog) LogError(string.Format("Failed to process command '{0}'", command), ex);
                webRequestError = "Error: " + ex.Message;
                return false;
            }
        }

        public bool RunHttpWebServiceCommandUpdated(string command, string httpWebServiceUrl, int timeout, out string webRequestResult, out string webRequestError, string[] parameters, int Number_Of_Parameters)
        {
            webRequestResult = string.Empty;
            bool createLog = command != TESTING_CONNECTION;
            try
            {
                return SendWebRequestUpdated(command, httpWebServiceUrl, timeout, out webRequestResult, out webRequestError, parameters, Number_Of_Parameters, createLog);
            }
            catch (AggregateException ex)
            {
                if (ex.InnerExceptions.Count == 1)
                {
                    Exception e = ex.InnerExceptions[0];
                    webRequestError = CreateErrorMessage(e, command);
                    if (createLog) LogError(webRequestError);
                    return false;
                }

                string errorMessage = string.Format("AggregateException occured while processing command: '{0}'\r\n", command);
                foreach (Exception e in ex.InnerExceptions)
                {
                    if (e is TaskCanceledException)
                    {
                        if (command == TESTING_CONNECTION)
                        {
                            errorMessage = "Timeout occured while testing connection.";
                            if (createLog) LogError("Timeout occured while testing connection.");
                        }
                        else
                        {
                            errorMessage = string.Format("Timeout occured while processing command: '{0}'\r\n", command);
                            if (createLog) LogError(string.Format("Due timeout failed to process command: '{0}'", command), e);
                        }
                    }
                    else
                    {
                        if (createLog) LogError(string.Format("Failed to process command '{0}'", command), e);
                    }
                }

                webRequestError = "Error: " + errorMessage;
                return false;
            }
            catch (Exception ex)
            {
                if (createLog) LogError(string.Format("Failed to process command '{0}'", command), ex);
                webRequestError = "Error: " + ex.Message;
                return false;
            }
        }

        private bool SendWebRequestUpdated(string command, string httpWebServiceUrl, int timeout, out string result, out string error, string[] parameters, int Number_Of_Parameters, bool createLog = true)
        {
            var query = HttpUtility.ParseQueryString(string.Empty);

            RequestType requestType;

            //Ofs request
            if (httpWebServiceUrl.EndsWith("resources/ofs"))
            {
                if (_httpMethod == HttpMethod.Post)
                {
                    query["ofsRequest"] = command;
                }
                else
                {
                    query["Request"] = command;
                }

                requestType = RequestType.OFS;
            }
            //Routine call
            else if (httpWebServiceUrl.EndsWith("resources/callAt"))
            {
                var paramters = parameters.Length > 0 ? GetSpecificRoutineParamsUpdated(command, parameters, Number_Of_Parameters) : GetSpecificRoutineParams(command);

                foreach (var parameter in paramters)
                {
                    query.Add(parameter.Key, parameter.Value);
                }

                requestType = RequestType.Routine;
            }
            else
            {
                throw new ApplicationException("Unexpected web service Url!");
            }

            Func<HttpResponseMessage> action;

            _Client.Timeout = new TimeSpan(0, 0, timeout);

            string queryString = "?" + query.ToString();

            if (createLog)
            {
                LogInfo("Attempting to connect to Http Rest Service");
                LogInfo(string.Format("Http Rest Service URL: {0}", httpWebServiceUrl));
                LogInfo(string.Format("Http Rest Service Query String: {0}", queryString));
                LogInfo(string.Format("Timeout in seconds: {0}", timeout));
                LogInfo(string.Format("Command to execute: {0}", command)); // need to remove password from log file !!!
                LogInfo(string.Format("Sending request....."));
            }

            if (_httpMethod == HttpMethod.Post)
            {
                if (createLog)
                {
                    LogInfo(string.Format("Method is POST"));
                }

                var values = new List<KeyValuePair<string, string>>();

                foreach (var item in query.AllKeys)
                {
                    values.Add(new KeyValuePair<string, string>(item, query[item]));
                }

                var content = new FormUrlEncodedContent(values);

                action = () => _Client.PostAsync(httpWebServiceUrl, content).Result;
            }
            else
            {
                if (createLog)
                {
                    LogInfo(string.Format("Method is GET"));
                }

                action = () => _Client.GetAsync(httpWebServiceUrl + queryString).Result;
            }

            var response = action();

            if (createLog)
                LogInfo("Response received.");

            bool isTestingRequest = command == TESTING_CONNECTION;
            return ParseResponse(requestType, query.Count, response, isTestingRequest, out result, out error, createLog);
        }

        private List<KeyValuePair<string, string>> GetSpecificRoutineParamsUpdated(string command, string[] Parameters, int Number_Of_parameters, string subDelimiter = "|", string parameters_sub_delimiter = "=")
        {
            if (string.IsNullOrEmpty(command))
            {
                throw new ApplicationException("Wrong routine call! Empty command!");
            }

            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();

            if (Parameters.Length > 0)
            {
                var values = command.Trim().Split();
                string temp;
                if (Parameters.Length == values.Length)
                {
                    for (int j = 0; j < values.Length; j++)
                    {
                        var sub_values = values[j].Split(new[] { subDelimiter }, StringSplitOptions.None);
                        var sub_parameters = Parameters[j].Split(new[] { parameters_sub_delimiter }, StringSplitOptions.None);
                        if (sub_parameters.Length > 1)
                        {
                            temp = GenerateValueForParameter(sub_parameters[1], sub_values, Number_Of_parameters);
                            items.Add(new KeyValuePair<string, string>(sub_parameters[0], temp));
                        }
                        else
                        {
                            items.Add(new KeyValuePair<string, string>(Parameters[j], values[j]));
                        }
                    }
                }
            }

            return items;
        }

        private string GenerateValueForParameter(string param, string[] values, int params_number)
        {
            string temp = "";

            for (int i = 0; i < values.Length; i++)
            {
                if (i != 0)
                {
                    temp += $"&{param}={values[i]}";
                }
                else
                {
                    temp += $"{param}={values[i]}";
                }
            }

            for (int i = values.Length; i < params_number; i++)
            {
                temp += $"&{param}=";
            }

            //temp += new String("param=", params_number - values.Length);

            return temp;
        }

        private bool SendWebRequest(string command, string httpWebServiceUrl, int timeout, out string result, out string error, bool createLog = true, string method = "GET")
        {
            var query = HttpUtility.ParseQueryString(string.Empty);

            RequestType requestType;

            //Ofs request
            if (httpWebServiceUrl.EndsWith("resources/ofs"))
            {
                if (_httpMethod == HttpMethod.Post)
                {
                    query["ofsRequest"] = command;
                }
                else
                {
                    query["Request"] = command;
                }

                requestType = RequestType.OFS;
            }
            //Routine call
            else if (httpWebServiceUrl.EndsWith("resources/callAt"))
            {
                var paramters = method.Equals("GET") ? GetRoutineParams(command) : GetSpecificRoutineParams(command);

                foreach (var parameter in paramters)
                {
                    query.Add(parameter.Key, parameter.Value);
                }

                requestType = RequestType.Routine;
            }
            else
            {
                throw new ApplicationException("Unexpected web service Url!");
            }

            Func<HttpResponseMessage> action;

            _Client.Timeout = new TimeSpan(0, 0, timeout);

            string queryString = "?" + query.ToString();

            if (createLog)
            {
                LogInfo("Attempting to connect to Http Rest Service");
                LogInfo(string.Format("Http Rest Service URL: {0}", httpWebServiceUrl));
                LogInfo(string.Format("Http Rest Service Query String: {0}", queryString));
                LogInfo(string.Format("Timeout in seconds: {0}", timeout));
                LogInfo(string.Format("Command to execute: {0}", command)); // need to remove password from log file !!!
                LogInfo(string.Format("Sending request....."));
            }

            if (_httpMethod == HttpMethod.Post)
            {
                if (createLog)
                {
                    LogInfo(string.Format("Method is POST"));
                }

                var values = new List<KeyValuePair<string, string>>();

                foreach (var item in query.AllKeys)
                {
                    values.Add(new KeyValuePair<string, string>(item, query[item]));
                }

                var content = new FormUrlEncodedContent(values);

                action = () => _Client.PostAsync(httpWebServiceUrl, content).Result;
            }
            else
            {
                if (createLog)
                {
                    LogInfo(string.Format("Method is GET"));
                }

                action = () => _Client.GetAsync(httpWebServiceUrl + queryString).Result;
            }

            var response = action();

            if (createLog)
                LogInfo("Response received.");

            bool isTestingRequest = command == TESTING_CONNECTION;
            return ParseResponse(requestType, query.Count, response, isTestingRequest, out result, out error, createLog);
        }

        private IEnumerable<KeyValuePair<string, string>> GetRoutineParams(string command)
        {
            if (string.IsNullOrEmpty(command))
            {
                throw new ApplicationException("Wrong routine call! Empty command!");
            }

            var parts = command.Split(new[] { " " }, StringSplitOptions.None);
            yield return new KeyValuePair<string, string>("Subroutine", parts[0]);

            if (parts.Length > 1)
            {
                if (parts.Length == 2)
                {
                    var delimitedParams = parts[1].Split(new[] { "|" }, StringSplitOptions.None);

                    foreach (string parameter in delimitedParams)
                        yield return new KeyValuePair<string, string>("Param", parameter);
                }
                else
                {
                    for (int i = 1; i < parts.Length; i++)
                        yield return new KeyValuePair<string, string>("Param", parts[i]);
                }
            }
        }

        private IEnumerable<KeyValuePair<string, string>> GetSpecificRoutineParams(string command, string routineParamName = "routineName", string parametersName = "inParameters")
        {
            if (string.IsNullOrEmpty(command))
            {
                throw new ApplicationException("Wrong routine call! Empty command!");
            }
            string temp = "";
            var parts = command.Split(new[] { " " }, StringSplitOptions.None);
            yield return new KeyValuePair<string, string>(routineParamName, parts[0]);

            if (parts.Length > 1)
            {
                if (parts.Length == 2)
                {
                    var delimitedParams = parts[1].Split(new[] { "|" }, StringSplitOptions.None);

                    foreach (string parameter in delimitedParams)
                        yield return new KeyValuePair<string, string>(parametersName, "param=" + parameter);
                }
                else
                {
                    for (int i = 1; i < parts.Length; i++)
                    {
                        if (i == parts.Length - 1)
                        {
                            temp += "param=" + parts[i];
                        }
                        else
                        {
                            temp += "param=" + parts[i] + "&";
                        }

                        //yield return new KeyValuePair<string, string>("inParameters", "param=" + parts[i]);
                    }
                    yield return new KeyValuePair<string, string>("inParameters", temp);
                }
            }
        }

        private bool ParseResponse(RequestType requestType, int parametersCount, HttpResponseMessage httpResponse, bool isTestingRequest, out string response, out string error, bool createLog = true)
        {
            if (createLog) LogInfo("Parsing response.....");

            Task<string> data = httpResponse.Content.ReadAsStringAsync();
            response = data.Result;
            if (!httpResponse.IsSuccessStatusCode)
            {
                if (createLog) LogError("Request was unsuccessful.");
                if (string.IsNullOrWhiteSpace(response))
                {
                    if (createLog)
                    {
                        LogError(string.Format("Response is empty"));
                        LogError(string.Format("ReasonPhrase: {0}", httpResponse.ReasonPhrase));
                        LogError(string.Format("StatusCode: {0} - {1}", (int)httpResponse.StatusCode, httpResponse.StatusCode));
                        LogError(string.Format("Content: {0}", httpResponse.Content));
                    }
                    error = string.Format("ReasonPhrase: {0}, StatusCode: {1} - {2}, Content: {3}",
                        httpResponse.ReasonPhrase, (int)httpResponse.StatusCode, httpResponse.StatusCode, httpResponse.Content);
                }
                else
                {
                    error = ParseErrorMessage(requestType, parametersCount, response);
                    if (createLog) LogError(string.Format("Error message: {0}", error));
                }
                return false;
            } else if (isTestingRequest)
            {
                error = "";
                //Testing connection requet is OK, no need to check the response
                return true;
            }

            if (createLog) LogInfo("Request was successful.");

            return ParseResponseMessage(requestType, parametersCount, ref response, out error, createLog);
        }

        private bool ParseResponseMessage(RequestType requestType, int parametersCount, ref string response, out string error, bool createLog = true)
        {
            error = "";

            try
            {
                if (createLog && !string.IsNullOrEmpty(response))
                {
                    LogInfo(string.Format("Totaly retrieved {0} bytes of data from server.", response.Length));
                    LogInfo(string.Format("Server response: {0}", response));
                }
				
				var jToken = JToken.Parse(response);
                var ofsToken = jToken.SelectToken(_ofsJsonPath);

                if (ofsToken != null)
                {
                    response = ofsToken.Value<string>();
                }
                else
                {
                    error = string.Format("Unable to extract ofs message based on json path: '{0}'", _ofsJsonPath);
                    return false;
                }

                if (createLog)
                {
                    if (!string.IsNullOrEmpty(response))
                     LogInfo(string.Format("OFS message: {0}", response));
                    else
                        LogInfo("OFS message is empty");
                }

                //Routine call
                return true;
             
            }
            catch (Exception ex)
            {
                if (createLog)
                {
                    LogError(string.Format("Unable to process http web service response due to invalid format:\r\n{0}", response), ex);
                }
                error = ex.Message;

                return false;
            }
        }

        /// <summary>
        /// This method parse error message received from web service. This method is used in case when there is response from web service but it contain error.
        /// </summary>
        /// <param name="requestType"></param>
        /// <param name="parametersCount"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private static string ParseErrorMessage(RequestType requestType, int parametersCount, string errorMessage)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(errorMessage);

                if (xmlDoc.DocumentElement != null && (xmlDoc.DocumentElement.ChildNodes.Count == 1 && xmlDoc.DocumentElement.ChildNodes[0].Name == "ns:return"))
                {
                    errorMessage = xmlDoc.DocumentElement.ChildNodes[0].InnerText.Trim();
                }

                return errorMessage;
            }
            catch (Exception ex)
            {
                return errorMessage;
            }
        }

        private string CreateErrorMessage(Exception e, string command)
        {
            string errorMessage = "Error: ";
            if (e is TaskCanceledException)
            {
                if (command == TESTING_CONNECTION) errorMessage += "Timeout occured while testing connection.";
                else errorMessage += string.Format("Timeout occured while processing command: '{0}'", command);
            }
            else
            {
                errorMessage = PrepareErrorMessage(e, command);
            }
            return errorMessage;
        }

        /// <summary>
        /// This method parse error message received from web server. This method is used in case when there is not any response from web service
        /// </summary>
        /// <param name="e"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        private string PrepareErrorMessage(Exception e, string command)
        {
            string errorMessage = string.Format("Error Message: {0}", e.Message);
            if (e.InnerException != null)
            {
                errorMessage += string.Format("\r\nInner Exception Message: {0}", e.InnerException.Message);
                if (!string.IsNullOrWhiteSpace(e.InnerException.StackTrace)) errorMessage += string.Format("\r\nInner Exception Stack Trace: {0}", e.InnerException.StackTrace);
            }
            errorMessage = command == TESTING_CONNECTION 
                ? string.Format("Failed to testing connection due to following error:\r\n{0}\r\n", errorMessage) 
                : string.Format("Failed to process command:\r\'{0}'\r\ndue to following error:\r\n{1}", command, errorMessage);

            return errorMessage;
        }
    }
}
