﻿using System;
using System.Collections;
using System.Text;
using System.Threading;
using OFSCommonMethods.IO.HttpWebService;
using OFSCommonMethods.IO.Telnet;
using System.Net.Http;

namespace OFSCommonMethods.IO.HttpRestService
{
    public class HttpRestServiceIOManager : SocketBasedIOManager
    {
 
        #region Private Members

        private readonly string _HttpWebServiceUrl;
        private readonly int _Timeout;

        private readonly string _ConnectionId;
        private readonly string _testStepId;

        private string _WebRequestResult;
        private readonly int _PageCode;
        private readonly Settings _Settings;
        private readonly string _username;
        private readonly string _password;
        private readonly string _ofsJsonPath;
        private readonly HttpMethod _httpMethod;

        #endregion

        #region Public Members

        public override Settings.IOType Type
        {
            get { return Settings.IOType.HttpRestService; }
        }

        public HttpRestServiceIOManager(Settings settings, string id, string testStepId = "")
            : base(settings, id)
        {
            _Settings = settings;
            _ConnectionId = id;
            _testStepId = testStepId;

            var setting = (HttpRestServiceConfigSettings)settings.ConnectionTypes.Find(n => n.ConnectionType == ConsoleType.HttpRestService);
            _HttpWebServiceUrl = setting.OFSWebServiceUrl;
            _Timeout = setting.WebServiceResponseTimeOut;
            _PageCode = settings.PageCode;
            _username = setting.Username;
            _password = setting.Password;
            _ofsJsonPath = setting.OfsJsonPath;
            _httpMethod = setting.HttpMethod;
        }

        /// <summary>
        /// Tests the connection.
        /// </summary>
        /// <param name="descriptiveError">The descriptive error.</param>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        public static bool TestConnection(StringBuilder descriptiveError, Settings settings)
        {
            if (settings.Type != Settings.IOType.HttpRestService)
            {
                descriptiveError.Append("OFS communication chanel is not HttpRestService");
                return false;
            }

            var connectionTypeSettings = (HttpRestServiceConfigSettings)settings.ConnectionTypes.Find(ct => ct.ConnectionType == ConsoleType.HttpRestService);
            if (connectionTypeSettings == null)
            {
                descriptiveError.Append("There is missing connection type settings for HttpRestService");
                return false;
            }

            if (string.IsNullOrWhiteSpace(connectionTypeSettings.OFSWebServiceUrl))
            {
                descriptiveError.Append("There is missing or invalid settings for OFSWebServiceUrl");
                return false;
            }

            if (connectionTypeSettings.WebServiceResponseTimeOut < 1)
            {
                descriptiveError.Append("There is missing or invalid settings for WebServiceResponseTimeOut");
                return false;
            }

            try
            {
                using (var connector = new HttpRestServiceConnectorForATS(HttpRestServiceConnector.TESTING_CONNECTION, settings.PageCode, connectionTypeSettings.Username, connectionTypeSettings.Password, connectionTypeSettings.OfsJsonPath, string.Empty, connectionTypeSettings.HttpMethod))
                {
                    string webRequestResult;
                    string webRequestError;
                    bool result = connector.RunHttpWebServiceCommand(HttpRestServiceConnector.TESTING_CONNECTION, connectionTypeSettings.OFSWebServiceUrl, connectionTypeSettings.WebServiceResponseTimeOut, out webRequestResult, out webRequestError);
                    if (!result) 
                        descriptiveError.Append(webRequestError);
                    return result;
                }
                
            }
            catch (Exception ex)
            {
                descriptiveError.Append(ex.Message);
                return false;
            }
        }

        public override void WriteMessage(bool isAuthorize, GlobusMessage gm)
        {
            using (var connector = new HttpRestServiceConnectorForATS(_ConnectionId, _PageCode, _username, _password, _ofsJsonPath, _testStepId, _httpMethod))
            {
                string webRequestResult;
                string webRequestError;

                _WebRequestResult = connector.RunHttpWebServiceCommand(gm.Message.Text, _HttpWebServiceUrl, _Timeout, out webRequestResult, out webRequestError)
                ? webRequestResult
                : webRequestError;

                AddMessageResult(isAuthorize, gm, TelnetIOManager.GetOFSMessageWithoutTags(_Settings, _WebRequestResult));
            }

            lock (_ThreadEvents.SyncRoot)
            {
                foreach (var resetEvent in _ThreadEvents.Values)
                {
                    ((AutoResetEvent)resetEvent).Set();
                }
            }
        }

        public override void WriteMessage(GlobusMessage inputT24Message, GlobusMessage outpuT24Message, TSSTelnetComm telnetComm)
        {
            using (var connector = new HttpRestServiceConnectorForATS(_ConnectionId, _PageCode, _username, _password, _ofsJsonPath, _testStepId, _httpMethod))
            {
                string webRequestResult;
                string webRequestError;
                if (connector.RunHttpWebServiceCommand(inputT24Message.Message.Text, _HttpWebServiceUrl, _Timeout,
                    out webRequestResult, out webRequestError))
                {
                    outpuT24Message.Message.Text = webRequestResult;
                    _WebRequestResult = webRequestResult;
                }
                else
                {
                    outpuT24Message.Message.Text = _WebRequestResult = webRequestError;
                }
            }
        }

        public override void WriteMessageBulk(bool isAuthorize, GlobusMessage gm)
        {
            WriteMessage(isAuthorize, gm);
        }

        public override void ReadResults(bool isAuthorize)
        {

        }

        #endregion
    }
}

