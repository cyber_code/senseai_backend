﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using OFSCommonMethods.IO.Telnet;
using log4net;
using System.Net.Http;

namespace OFSCommonMethods.IO.HttpRestService
{
    public class HttpRestServiceConnectorForATS : HttpRestServiceConnector
    {
        private enum MessageType
        {
            Info,
            Error
        }

        private const string DefaultLoggerName = "Validata.SAS.HttpWebService";
        private const string LoggingDateTimeFormat = "dd.MM.yyyy HH:mm:ss.fff";

        private ILog _Log4NetLogger;

        private StreamWriter _Logger;
        private readonly string _ConnectionId;
        private readonly string _testStepId;
        private readonly int _PageCode;

        public HttpRestServiceConnectorForATS(string connectionId, int pageCode, string username, string password, string ofsJsonPath, string testStepId, HttpMethod httpMethod)
            : base(username, password, ofsJsonPath, httpMethod)
        {
            _ConnectionId = connectionId;
            _testStepId = testStepId;
            _PageCode = pageCode;
            InitializeLogger();
        }

        protected override void LogInfo(string message)
        {
            if (string.IsNullOrEmpty(_ConnectionId) && _Log4NetLogger != null)
            {
                if (string.IsNullOrEmpty(_ClientID))
                    _Log4NetLogger.Info(message);
                else
                    _Log4NetLogger.Info($"[Request: {_ClientID}] {message}");
                return;
            }

            try
            {
                if (_Logger != null && _Logger.BaseStream.CanWrite)
                {
                    _Logger.WriteLine("{0}: {1}", MessageType.Info, DateTime.Now.ToString(LoggingDateTimeFormat));
                    _Logger.WriteLine(message);
                    _Logger.Flush();
                }
            }
            catch { }
        }

        protected override void LogError(string message, Exception ex = null)
        {
            if (string.IsNullOrEmpty(_ConnectionId) && _Log4NetLogger != null)
            {                
                _Log4NetLogger.Error(message, ex);
                return;
            }

            try
            {
                if (_Logger != null && _Logger.BaseStream.CanWrite)
                {
                    _Logger.WriteLine("{0}: {1}", MessageType.Error, DateTime.Now.ToString(LoggingDateTimeFormat));
                    _Logger.WriteLine(message);

                    if (ex != null)
                        _Logger.WriteLine("Exception: {0}\r\nInner exception: {1}\r\nStack Trace:\r\n{2}", ex.Message, ex.InnerException, ex.StackTrace);

                    _Logger.Flush();
                }
            }
            catch { }
        }

        public override void Dispose()
        {
            if (_Logger != null && !IsDisposed)
            {
                base.Dispose();
                ReleaseLogger();
            }
        }

        private void InitializeLogger()
        {
            _Log4NetLogger = LogManager.Exists(DefaultLoggerName);
            if ((string.IsNullOrEmpty(_ConnectionId) || _ConnectionId == TESTING_CONNECTION) && _Log4NetLogger != null)
                return;

            if (!Directory.Exists(TelnetCommPool.LogsFolder))
            {
                Directory.CreateDirectory(TelnetCommPool.LogsFolder);
            }

            var path = Path.Combine(TelnetCommPool.LogsFolder, !string.IsNullOrEmpty(_ConnectionId)
                ? string.Format("HttpWebService_{0}.{1}.txt", DateTime.Now.Ticks, _testStepId)
                : string.Format("HttpWebService_{0}.txt", DateTime.Now.Ticks));

            try
            {
                _Logger = new StreamWriter(File.Open(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.ReadWrite), Encoding.GetEncoding(_PageCode));
                Debug.WriteLine("Started logging at " + path);
            }
            catch
            {
                Debug.WriteLine("Can not start logging at " + path);
                _Logger = null;
            }
        }

        private void ReleaseLogger()
        {
            try
            {
                if (_Logger != null)
                {
                    _Logger.Close();
                }
            }
            catch
            {

            }
            finally
            {
                _Logger = null;
            }
        }
    }
}
