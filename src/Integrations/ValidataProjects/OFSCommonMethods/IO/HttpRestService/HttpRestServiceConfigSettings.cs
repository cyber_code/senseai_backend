using System.Net.Http;

namespace OFSCommonMethods.IO.HttpRestService
{
    public class HttpRestServiceConfigSettings : ConnectionTypeSetting
    {
        public override ConsoleType ConnectionType
        {
            get
            {
                return ConsoleType.HttpRestService;
            }
        }

        public string Parameters { get; set; }
        public int Number_Of_Routine_Parameter { get; set; }
        public string Username { get; set; }

        public string Password { get; set; }

        public string OfsJsonPath { get; set; }

        public string OfsRoutineJsonPath { get; set; }

        public HttpMethod HttpMethod { get; set; }
    }
}
