﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace OFSCommonMethods.IO
{
    [Serializable()]
    public class TelnetCommErrors : List<HandledCommError>
    {

        public void ToXmlFile()
        {
            SerializeToXml();
        }

        public static TelnetCommErrors FromXmlFile()
        {
            return DeserializeFromXml();
        }

        public HandledCommError ContainsError(String errorMessage)
        {
            List<HandledCommError> list = this.Where(a => a.ErrorMessage == "*").ToList();
            if (list.Count == 0)
            {
                list = this.Where(a => errorMessage.Contains(a.ErrorMessage)).ToList();
            }

            if (list.Count > 0)
            {
                return list[0];
            }

            return null;
        }


        /// <summary>
        /// Actual serialization of the data
        /// </summary>
        /// <returns>false- cancel the save</returns>
        public bool SerializeToXml()
        {
            string filePath = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "TelnetCommErrors.xml");
            XmlSerializer serializator = new XmlSerializer(this.GetType());
            try
            {
                using (StreamWriter fileStream = File.CreateText(filePath))
                {
                    serializator.Serialize(fileStream, this);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) throw new ApplicationException(ex.InnerException.Message);
                throw;
            }
            return true;
        }

        public static TelnetCommErrors DeserializeFromXml()
        {
            string filePath = System.IO.Path.Combine(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"adapterfiles\Assemblies" , "TelnetCommErrors.xml");
            if (!File.Exists(filePath)) return new TelnetCommErrors();

            XmlSerializer deSerializator = new XmlSerializer(typeof(TelnetCommErrors));
            // Reading the XML report

            Stream readerObject = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            TelnetCommErrors telnetCommErrors = deSerializator.Deserialize(readerObject) as TelnetCommErrors;
            readerObject.Close();
            return telnetCommErrors;
        }
    }

    [Serializable()]
    public class HandledCommError
    {
        public String ErrorMessage;
        public int RetryCount;
        public long RetryTimeout;
    }
}
