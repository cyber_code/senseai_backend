﻿namespace OFSCommonMethods.IO
{
    internal class HttpWebServiceConfigSettings : ConnectionTypeSetting
    {
        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.HttpWebService; }
        }
    }
}
