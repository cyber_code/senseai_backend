﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using OFSCommonMethods.IO.Telnet;
using log4net;

namespace OFSCommonMethods.IO.HttpWebService
{
    public class HttpWebServiceConnectorForATS : HttpWebServiceConnector
    {
        private enum MessageType
        {
            Info,
            Error
        }

        private const string DefaultLoggerName = "Validata.SAS.HttpWebService";
        private const string LoggingDateTimeFormat = "dd.MM.yyyy HH:mm:ss.fff";

        private ILog _Log4NetLogger;

        private StreamWriter _Logger;
        private readonly string _ConnectionId;
        private readonly int _PageCode;
        private readonly string _testStepId;

        public HttpWebServiceConnectorForATS(string connectionId, int pageCode, string testStepId)
        {
            _ConnectionId = connectionId;
            _PageCode = pageCode;
            _testStepId = testStepId;
            InitializeLogger();
        }

        protected override void LogInfo(string message)
        {
            if (string.IsNullOrEmpty(_ConnectionId) && _Log4NetLogger != null)
            {
                _Log4NetLogger.Info(message);
                return;
            }

            try
            {
                if (_Logger != null && _Logger.BaseStream.CanWrite)
                {
                    _Logger.WriteLine("{0}: {1}", MessageType.Info, DateTime.Now.ToString(LoggingDateTimeFormat));
                    _Logger.WriteLine(message);
                    _Logger.Flush();
                }
            }
            catch { }
        }

        protected override void LogError(string message, Exception ex = null)
        {
            if (string.IsNullOrEmpty(_ConnectionId) && _Log4NetLogger != null)
            {
                _Log4NetLogger.Error(message, ex);
                return;
            }

            try
            {
                if (_Logger != null && _Logger.BaseStream.CanWrite)
                {
                    _Logger.WriteLine("{0}: {1}", MessageType.Error, DateTime.Now.ToString(LoggingDateTimeFormat));
                    _Logger.WriteLine(message);

                    if (ex != null)
                        _Logger.WriteLine("Exception: {0}\r\nInner exception: {1}\r\nStack Trace:\r\n{2}", ex.Message, ex.InnerException, ex.StackTrace);

                    _Logger.Flush();
                }
            }
            catch { }
        }

        public override void Dispose()
        {
            if (_Logger != null && !IsDisposed)
            {
                base.Dispose();
                ReleaseLogger();
            }
        }

        private void InitializeLogger()
        {
            _Log4NetLogger = LogManager.Exists(DefaultLoggerName);
            if (string.IsNullOrEmpty(_ConnectionId) && _Log4NetLogger != null)
                return;

            if (!Directory.Exists(TelnetCommPool.LogsFolder))
            {
                Directory.CreateDirectory(TelnetCommPool.LogsFolder);
            }

            var path = Path.Combine(TelnetCommPool.LogsFolder, !string.IsNullOrEmpty(_ConnectionId)
                ? string.Format("HttpWebService_{0}_{1}.{2}.txt",_testStepId, DateTime.Now.Ticks, _ConnectionId)
                : string.Format("HttpWebService_{0}_{1}.txt",_testStepId, DateTime.Now.Ticks));

            try
            {
                _Logger = new StreamWriter(File.Open(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.ReadWrite), Encoding.GetEncoding(_PageCode));
                Debug.WriteLine("Started logging at " + path);
            }
            catch
            {
                Debug.WriteLine("Can not start logging at " + path);
                _Logger = null;
            }
        }

        private void ReleaseLogger()
        {
            try
            {
                if (_Logger != null)
                {
                    _Logger.Close();
                }
            }
            catch
            {

            }
            finally
            {
                _Logger = null;
            }
        }
    }
}
