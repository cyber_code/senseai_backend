﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using Common;

namespace OFSCommonMethods.IO.HttpWebService
{
    public class HttpWebServiceConnector : IDisposable
    {
        private enum RequestType
        {
            OFS,
            Routine
        }

        private readonly Logger _Logger;
        protected bool IsDisposed;
        private readonly HttpClient _Client;

        public const string TESTING_CONNECTION = "Testing Connection";

        protected HttpWebServiceConnector()
        {
            _Client = new HttpClient();
        }

        public HttpWebServiceConnector(Logger logger)
        {
            _Logger = logger;
            _Client = new HttpClient();
        }

        ~HttpWebServiceConnector()
        {
            if (!IsDisposed)
                Dispose();
        }

        protected virtual void LogInfo(string message)
        {
            if (_Logger != null)
                _Logger.Info(message);
        }

        protected virtual void LogError(string message, Exception ex = null)
        {
            if (_Logger != null)
                _Logger.Error(message, ex);
        }

        public virtual void Dispose()
        {
            if (_Client != null && !IsDisposed)
            {
                _Client.Dispose();
                IsDisposed = true;
            }
        }

        public bool RunHttpWebServiceCommand(string command, string httpWebServiceUrl, int timeout, out string webRequestResult, out string webRequestError)
        {
            webRequestResult = string.Empty;
            bool createLog = command != TESTING_CONNECTION;
            try
            {
                return SendWebRequest(command, httpWebServiceUrl, timeout, out webRequestResult, out webRequestError, createLog);
            }
            catch (AggregateException ex)
            {
                if (ex.InnerExceptions.Count == 1)
                {
                    Exception e = ex.InnerExceptions[0];
                    webRequestError = CreateErrorMessage(e, command);
                    if (createLog) LogError(webRequestError);
                    return false;
                }

                string errorMessage = string.Format("AggregateException occured while processing command: '{0}'\r\n", command);
                foreach (Exception e in ex.InnerExceptions)
                {
                    if (e is TaskCanceledException)
                    {
                        if (command == TESTING_CONNECTION)
                        {
                            errorMessage = "Timeout occured while testing connection.";
                            if (createLog) LogError("Timeout occured while testing connection.");
                        }
                        else
                        {
                            errorMessage = string.Format("Timeout occured while processing command: '{0}'\r\n", command);
                            if (createLog) LogError(string.Format("Due timeout failed to process command: '{0}'", command), e);
                        }
                    }
                    else
                    {
                        if (createLog) LogError(string.Format("Failed to process command '{0}'", command), e);
                    }
                }

                webRequestError = "Error: " + errorMessage;
                return false;
            }
            catch (Exception ex)
            {
                if (createLog) LogError(string.Format("Failed to process command '{0}'", command), ex);
                webRequestError = "Error: " + ex.Message;
                return false;
            }
        }

        private bool SendWebRequest(string command, string httpWebServiceUrl, int timeout, out string result, out string error, bool createLog = true)
        {
            var values = new List<KeyValuePair<string, string>>();

            RequestType requestType;

            //Ofs request
            if (httpWebServiceUrl.EndsWith("OFSService/Invoke"))
            {
                values.Add(new KeyValuePair<string, string>("Request", command));
                requestType = RequestType.OFS;
            }
            //Routine call
            else if (httpWebServiceUrl.EndsWith("InvokerService/Invoke"))
            {
                values.AddRange(GetRoutineParams(command));
                requestType = RequestType.Routine;
            }
            else
            {
                throw new ApplicationException("What the f.. is this url!");
            }

            var content = new FormUrlEncodedContent(values);
            _Client.Timeout = new TimeSpan(0, 0, timeout);

            if (createLog)
            {
                LogInfo("Attempting to connect to Http Web Service");
                LogInfo(string.Format("Http Web Service URL: {0}", httpWebServiceUrl));
                LogInfo(string.Format("Timeout in seconds: {0}", timeout));
                LogInfo(string.Format("Command to execute: {0}", command)); // need to remove password from log file !!!
                LogInfo(string.Format("Sending request....."));
            }

            var response = _Client.PostAsync(httpWebServiceUrl, content).Result;

            if (createLog) LogInfo("Response received.");
            return ParseResponse(requestType, values.Count, response, out result, out error, createLog);
        }

        private IEnumerable<KeyValuePair<string, string>> GetRoutineParams(string command)
        {
            if (string.IsNullOrEmpty(command))
            {
                throw new ApplicationException("Wrong routine call! Empty command!");
            }

            var parts = command.Split(new[] { " " }, StringSplitOptions.None);
            yield return new KeyValuePair<string, string>("Subroutine", parts[0]);

            if (parts.Length > 1)
            {
                if (parts.Length == 2)
                {
                    var delimitedParams = parts[1].Split(new[] { "|" }, StringSplitOptions.None);

                    foreach (string parameter in delimitedParams)
                        yield return new KeyValuePair<string, string>("Param", parameter);
                }
                else
                {
                    for (int i = 1; i < parts.Length; i++)
                        yield return new KeyValuePair<string, string>("Param", parts[i]);
                }
            }
        }

        private bool ParseResponse(RequestType requestType, int parametersCount, HttpResponseMessage httpResponse, out string response, out string error, bool createLog = true)
        {
            if (createLog) LogInfo("Parsing response.....");

            Task<string> data = httpResponse.Content.ReadAsStringAsync();
            response = data.Result;
            if (!httpResponse.IsSuccessStatusCode)
            {
                if (createLog) LogError("Request was unsuccessful.");
                if (string.IsNullOrWhiteSpace(response))
                {
                    if (createLog)
                    {
                        LogError(string.Format("Response is empty"));
                        LogError(string.Format("ReasonPhrase: {0}", httpResponse.ReasonPhrase));
                        LogError(string.Format("StatusCode: {0} - {1}", (int)httpResponse.StatusCode, httpResponse.StatusCode));
                        LogError(string.Format("Content: {0}", httpResponse.Content));
                    }
                    error = string.Format("ReasonPhrase: {0}, StatusCode: {1} - {2}, Content: {3}",
                        httpResponse.ReasonPhrase, (int)httpResponse.StatusCode, httpResponse.StatusCode, httpResponse.Content);
                }
                else
                {
                    error = ParseErrorMessage(requestType, parametersCount, response);
                    if (createLog) LogError(string.Format("Error message: {0}", error));
                }
                return false;
            }

            if (createLog) LogInfo("Request was successful.");

            return ParseResponseMessage(requestType, parametersCount, ref response, out error, createLog);
        }

        private bool ParseResponseMessage(RequestType requestType, int parametersCount, ref string response, out string error, bool createLog = true)
        {
            error = "";

            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(response);

                if (requestType == RequestType.OFS)
                {
                    var responseNode = xmlDoc.GetElementsByTagName("ns:responses")[0];

                    var r2 = responseNode.InnerXml.Replace("&lt;", "<").Replace("&gt;", ">");

                    try
                    {
                        var xmlDoc2 = new XmlDocument();
                        xmlDoc2.LoadXml(r2);
                        responseNode = xmlDoc2;
                    }
                    catch
                    {
                        //
                    }

                    response = responseNode.InnerXml;

                    //return true; //Skip this return which will log the response bellow
                }
                else
                {
                    if (createLog) LogInfo("RequestType is not OFS");
                }

                if (createLog && !string.IsNullOrEmpty(response))
                {
                    LogInfo(string.Format("Totaly retrieved {0} bytes of data from server.", response.Length));
                    LogInfo(string.Format("Server response: {0}", response));
                }

                //Routine call
                return true;
             
            }
            catch (Exception ex)
            {
                if (createLog)
                {
                    LogError(string.Format("Unable to process http web service response due to invalid format:\r\n{0}", response), ex);
                }
                error = ex.Message;

                return false;
            }
        }

        /// <summary>
        /// This method parse error message received from web service. This method is used in case when there is response from web service but it contain error.
        /// </summary>
        /// <param name="requestType"></param>
        /// <param name="parametersCount"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private static string ParseErrorMessage(RequestType requestType, int parametersCount, string errorMessage)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(errorMessage);

                if (xmlDoc.DocumentElement != null && (xmlDoc.DocumentElement.ChildNodes.Count == 1 && xmlDoc.DocumentElement.ChildNodes[0].Name == "ns:return"))
                {
                    errorMessage = xmlDoc.DocumentElement.ChildNodes[0].InnerText.Trim();
                }

                return errorMessage;
            }
            catch (Exception ex)
            {
                return errorMessage;
            }
        }

        private string CreateErrorMessage(Exception e, string command)
        {
            string errorMessage = "Error: ";
            if (e is TaskCanceledException)
            {
                if (command == TESTING_CONNECTION) errorMessage += "Timeout occured while testing connection.";
                else errorMessage += string.Format("Timeout occured while processing command: '{0}'", command);
            }
            else
            {
                errorMessage = PrepareErrorMessage(e, command);
            }
            return errorMessage;
        }

        /// <summary>
        /// This method parse error message received from web server. This method is used in case when there is not any response from web service
        /// </summary>
        /// <param name="e"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        private string PrepareErrorMessage(Exception e, string command)
        {
            string errorMessage = string.Format("Error Message: {0}", e.Message);
            if (e.InnerException != null)
            {
                errorMessage += string.Format("\r\nInner Exception Message: {0}", e.InnerException.Message);
                if (!string.IsNullOrWhiteSpace(e.InnerException.StackTrace)) errorMessage += string.Format("\r\nInner Exception Stack Trace: {0}", e.InnerException.StackTrace);
            }
            errorMessage = command == TESTING_CONNECTION 
                ? string.Format("Failed to testing connection due to following error:\r\n{0}\r\n", errorMessage) 
                : string.Format("Failed to process command:\r\'{0}'\r\ndue to following error:\r\n{1}", command, errorMessage);

            return errorMessage;
        }
    }
}
