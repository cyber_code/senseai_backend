namespace OFSCommonMethods.IO
{
    internal class JShellConfigSettings : ConnectionTypeSetting
    {
        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.JShell; }
        }
    }
}