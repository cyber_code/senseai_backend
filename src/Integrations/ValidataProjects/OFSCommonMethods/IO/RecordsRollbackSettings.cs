﻿using System.Collections.Generic;

namespace OFSCommonMethods.IO
{
    public class RecordsRollbackSettings
    {
        private List<string> _SkipDeletionApplicationsSorted = new List<string>();
        private List<string> _SkipRestoreApplicationsSorted = new List<string>();
        private List<string> _ReverseOfsApplicationsSorted = new List<string>();
        private List<string> _OnlyLiveTableApplicationsSorted = new List<string>();
        private List<string> _IHLDApplicationsSorted = new List<string>();
        public string DefaultTablePrefix = "F."; // F. is usually for internal and FBNK. for external T24 application
        public Dictionary<string, string> ApplicationsLiveTableNames = new Dictionary<string, string>();

        public List<string> IHLDApplications
        {
            get { return _IHLDApplicationsSorted; }
            set { _IHLDApplicationsSorted = GetSorted(value); }
        }

        public List<string> OnlyLiveTableApplications
        {
            get { return _OnlyLiveTableApplicationsSorted; }
            set { _OnlyLiveTableApplicationsSorted = GetSorted(value); }
        }

        public List<string> SkipDeletionApplications
        {
            get { return _SkipDeletionApplicationsSorted; }
            set { _SkipDeletionApplicationsSorted = GetSorted(value); }
        }

        public List<string> SkipRestoreApplications
        {
            get { return _SkipRestoreApplicationsSorted; }
            set { _SkipRestoreApplicationsSorted = GetSorted(value); }
        }

        public List<string> ReverseOfsApplications
        {
            get { return _ReverseOfsApplicationsSorted; }
            set { _ReverseOfsApplicationsSorted = GetSorted(value); }
        }

        private static List<string> GetSorted(List<string> items)
        {
            items.Sort();
            return items;
        }

        public bool ShouldSkipDeletion(string application)
        {
            return CheckIfItemIsInSortedList(application, _SkipDeletionApplicationsSorted, true);
        }

        public bool ShouldSkipRestore(string application)
        {
            return CheckIfItemIsInSortedList(application, _SkipRestoreApplicationsSorted, true);
        }

        public bool ShouldRestoreViaReverseOFS(string application)
        {
            return CheckIfItemIsInSortedList(application, _ReverseOfsApplicationsSorted, true);
        }

        public bool HasApplicationOnlyLiveTable(string application)
        {
            return CheckIfItemIsInSortedList(application, _OnlyLiveTableApplicationsSorted, true);
        }

        public bool ShouldRestoreViaIHLD(string application)
        {
            return CheckIfItemIsInSortedList(application, _IHLDApplicationsSorted, true);
        }

        private static bool CheckIfItemIsInSortedList(string item, List<string> items, bool allowWildCard)
        {
            if (items.BinarySearch(item) >= 0)
            {
                return true;
            }

            if (allowWildCard && items.BinarySearch("*") >= 0)
            {
                // TODO support other wildcards, too
                return true;
            }

            return false;
        }
    }
}