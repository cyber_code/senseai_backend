﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using ValidataCommon;

namespace OFSCommonMethods.IO
{
    public enum EndOverride
    {
        True,
        False,
        Unspecified
    }

    [Serializable]
    public class EndOfResponseCondition
    {
        public bool PatientTimeout = true;

        public bool IsRegEx { get; set; }

        public string WaitForText { get; set; }

        public int WaitHowManyTimes = 1;

        public int FinishOnMaxLines = -1;

        public bool DontExpectCommandEcho;

        public int TimeOut = -1;

        public EndOverride MustBeEnd = EndOverride.Unspecified;

        public EndOfResponseCondition()
        {
        }

        public EndOfResponseCondition(string waitForText)
        {
            WaitForText = waitForText;
        }

        public EndOfResponseCondition(string waitForText, int howManyTimes)
        {
            WaitForText = waitForText;
            WaitHowManyTimes = howManyTimes;
        }

        public EndOfResponseCondition(string waitForText, bool isRegEx, bool mustBeEnd = false)
            : this(waitForText)
        {
            IsRegEx = isRegEx;
            MustBeEnd = mustBeEnd ? EndOverride.True :  EndOverride.False;
        }


        public static bool IsValid(EndOfResponseCondition еndOfResponseCondition)
        {
            return еndOfResponseCondition != null
                && !string.IsNullOrEmpty(еndOfResponseCondition.WaitForText);
        }

        public bool IsSatisfied(string str)
        {
            return IsSatisfied(str, false, -1);
        }

        public bool IsSatisfied(string str, bool atTheEnd, int currLinesEnd)
        {
            // SSH-31: the end tag has got cut:

            // Expect to read '<-/MSG>'
            // GB0010001:1:1,DEPT.CODE=1:1:1<
            // -/MSG>

            // solution is to join the response lines before examining
            str = joinLines(str);

            if (MustBeEnd == EndOverride.True)
                atTheEnd = true;
            if (MustBeEnd == EndOverride.False)
                atTheEnd = false;

            if (FinishOnMaxLines > 0
                && currLinesEnd > 0
                && currLinesEnd >= FinishOnMaxLines)
            {
                return true;
            }

            if (IsRegEx)
            {
                Debug.Assert(!atTheEnd, "meaningless");
                Debug.Assert(!string.IsNullOrEmpty(WaitForText));

                Regex regEx = new Regex(WaitForText);
                return regEx.IsMatch(str);
            }

            if (WaitForText == null)
                return true;

            int waitForMatches = StringHelper.GetNumberOfOccurrences(str, WaitForText);
            if (waitForMatches < WaitHowManyTimes)
                return false;

            if (atTheEnd)
                return str.EndsWith(WaitForText);

            return str.Contains(WaitForText);
        }

        private static string joinLines(string str)
        {
            var sb = new System.Text.StringBuilder(str.Length);

            foreach (char i in str)
                if (i != '\n' && i != '\r')
                    sb.Append(i);


            str = sb.ToString();
            return str;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(WaitForText))
                return "[Invalid Condition - WaitForText is empty!]";

            return IsRegEx
                ? "(RegEx)" + WaitForText
                : WaitForText;
        }

        public bool IsEqualTo(EndOfResponseCondition other)
        {
            return other != null
                && PatientTimeout == other.PatientTimeout
                && IsRegEx == other.IsRegEx
                && WaitForText == other.WaitForText
                && WaitHowManyTimes == other.WaitHowManyTimes
                && FinishOnMaxLines == other.FinishOnMaxLines
                && DontExpectCommandEcho == other.DontExpectCommandEcho
                && TimeOut == other.TimeOut
                && MustBeEnd == other.MustBeEnd;
        }

        public EndOfResponseCondition Clone()
        {
            return new EndOfResponseCondition
            {
                PatientTimeout = this.PatientTimeout,
                IsRegEx = this.IsRegEx,
                WaitForText = this.WaitForText,
                WaitHowManyTimes = this.WaitHowManyTimes,
                FinishOnMaxLines = this.FinishOnMaxLines,
                DontExpectCommandEcho = this.DontExpectCommandEcho,
                TimeOut = this.TimeOut,
                MustBeEnd = this.MustBeEnd
            };
        }
    }
}