﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods.IO
{
    public class ShellCommand
    {
        public readonly CustomCommandResult CommandResult;

        public readonly string Format;
        
        internal ShellCommand(string format, CustomCommandResult result)
        {
            this.Format = format;
            this.CommandResult = result;
        }
    }
}
