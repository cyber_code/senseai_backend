﻿namespace OFSCommonMethods.IO
{
    public enum JShellCommandType
    {
        GetWorkingDirectory,
        GetEnvironmentVariable,
        AnalyzeCompilation,
        Compile,
        CompileAndCatalog,
        Catalog,
        Decatalog,
        DeleteDollarFile,
        CopyFileToCatalog,
        ListCatalog,
        ListVOCRecord,
        CopyRecord,
        DeleteRecord,
        JShow
    }
}