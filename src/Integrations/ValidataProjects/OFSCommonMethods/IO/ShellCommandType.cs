﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods.IO
{
    public enum ShellCommandType
    {
        Uncompress,
        Compress,
        ChangeDirectory,
        DeleteDirectory,
        DeleteFile,
        CreateTextFile
    }
}
