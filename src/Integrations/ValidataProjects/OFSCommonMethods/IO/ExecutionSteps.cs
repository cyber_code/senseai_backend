﻿using System.Collections.Generic;
using OFSCommonMethods.IO.Telnet;

namespace OFSCommonMethods.IO
{
    public class ExecutionSteps
    {
        public List<ExecutionStep> LoginSteps = new List<ExecutionStep>();
        public List<ExecutionStep> ExitSteps = new List<ExecutionStep>();

        public void ClearAllSteps()
        {
            LoginSteps.Clear();
            ExitSteps.Clear();
        }
    }
}