﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Principal;
using OFSCommonMethods.IO.HttpRestService;

namespace OFSCommonMethods.IO
{
    public class IOFactory
    {
        public static IOManager Create(Settings settings, string id, string testStepId = "")
        {
            switch(settings.Type)
            {
                case Settings.IOType.FileBased:
                    return new FileBasedIOManager(
                            settings.GlobusInput,
                            settings.GlobusOutput,
                            settings.GlobusAuthInput,
                            settings.GlobusAuthOutput
                        );

                case Settings.IOType.SharpSsH:
                case Settings.IOType.SSH:
                case Settings.IOType.Telnet:
                    return new TelnetIOManager(settings, id, testStepId);

                case Settings.IOType.TCServer:
                    return new TCServerIOManager(settings, id,testStepId);
                case Settings.IOType.HttpWebService:
                    return new HttpWebServiceIOManager(settings, id,testStepId);
                case Settings.IOType.HttpRestService:
                    return new HttpRestServiceIOManager(settings, id, testStepId);

                default:
                    throw new Exception("Unknown IO Communication Method!");
            }
        }

        public static bool TestAccessibility(Settings settings, StringBuilder descriptiveError, WindowsIdentity adapterIdentity)
        {
            switch(settings.Type)
            {
                case Settings.IOType.FileBased:
                    return FileBasedIOManager.PhantomsPathsExistingAndAccessible(
                            settings.GlobusInput,
                            settings.GlobusOutput, 
                            settings.GlobusAuthInput,
                            settings.GlobusAuthOutput,
                            descriptiveError, 
                            adapterIdentity
                        );

                case Settings.IOType.SharpSsH:
                case Settings.IOType.SSH:
                case Settings.IOType.Telnet:
                case Settings.IOType.TCServer:
                    return SocketBasedIOManager.TestConnection(
                            descriptiveError,
                            adapterIdentity,
                            settings
                        );
                case Settings.IOType.HttpWebService:
                    return HttpWebServiceIOManager.TestConnection(descriptiveError, settings);
                case Settings.IOType.HttpRestService:
                    return HttpRestServiceIOManager.TestConnection(descriptiveError, settings);

                default:
                    System.Diagnostics.Debug.Assert(false);
                    descriptiveError.Append("Unknown IO Communication Method!");
                    return false;
            }
        }
    }
}
