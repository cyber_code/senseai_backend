using OFSCommonMethods.IO.Telnet;
using System.Collections.Generic;

namespace OFSCommonMethods.IO
{
    internal class DBShellConfigSettings : ConnectionTypeSetting
    {
        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.DBShell; }
        }

        public override List<ExecutionStep> GetAllLoginSteps()
        {
            return LoginSteps;
        }

        public override List<ExecutionStep> GetAllExitSteps()
        {
            return ExitSteps;
        }

    }
}