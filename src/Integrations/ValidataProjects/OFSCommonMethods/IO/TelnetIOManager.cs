﻿using System;
using OFSCommonMethods.IO.Telnet;
using System.Text;

namespace OFSCommonMethods.IO
{
    public class TelnetIOManager : SocketBasedIOManager
    {
        
        #region Public Properties

        public override Settings.IOType Type
        {
            get { return Settings.IOType.Telnet; }
        }

        public static bool _JoinMainAAAOFS
        {
            get
            {
                // TODO: research on different T24, maybe introduce a config setting.
                return true;
            }
        }

        #endregion

        #region Class Lifecycle

        internal TelnetIOManager(Settings settings, string executionId, string testStepId = "") :
            base(settings, executionId, testStepId)
        {
           
        }

        #endregion

        #region Public Overrides (IOManager)

        /// <summary>
        /// Writes an OFS message to Globus and returns.
        /// </summary>
        /// <param name="inputT24Message"></param>
        /// <param name="outpuT24Message"></param>
        /// <param name="telnetComm"></param>
        public override void WriteMessage(GlobusMessage inputT24Message, GlobusMessage outpuT24Message, TSSTelnetComm telnetComm)
        {
            string requestMessage = EncloseInStartEndTags(GetOFSMessageCompleteText(_Settings, inputT24Message.Message));

            string resultOfs;
            Execute(requestMessage, out resultOfs, telnetComm);

            outpuT24Message.Message = GetOFSMessageWithoutTags(_Settings, resultOfs);
        }

        /// <summary>
        /// Writes an OFS message to Globus and returns.
        /// </summary>
        /// <param name="isAuthorize">Is Authorize</param>
        /// <param name="gm">Globus Message</param>
        public override void WriteMessage(bool isAuthorize, GlobusMessage gm)
        {
            //Check why it's needed in multythreaded
            string requestMessage = EncloseInStartEndTags(GetOFSMessageCompleteText(_Settings, gm.Message));

            PushMessageTime(gm.ID);

            string resultOfs;
            Execute(requestMessage, out resultOfs);

            PushMessageTime(gm.ID);

            GlobusMessage.OFSMessage result = GetOFSMessageWithoutTags(_Settings, resultOfs);
            AddMessageResult(isAuthorize, gm, result);
        }

        /// <summary>
        /// Writes an OFS message to Globus and returns.
        /// </summary>
        /// <param name="isAuthorize">Is Authorize</param>
        /// <param name="gm">Globus Message</param>
        public override void WriteMessageBulk(bool isAuthorize, GlobusMessage gm)
        {
            string requestMessage = EncloseInStartEndTags(GetOFSMessageCompleteText(_Settings, gm.Message));

            PushMessageTime(gm.ID);

            string resultOfs;
            Execute(requestMessage, out resultOfs);

            PushMessageTime(gm.ID);

            GlobusMessage.OFSMessage result = GetOFSMessageWithoutTags(_Settings, resultOfs);
            AddMessageResult(isAuthorize, gm, result);
        }

        #endregion

        #region Private Static Methods

        private static string GetOFSMessageCompleteText(Settings settings, GlobusMessage.OFSMessage msg)
        {
            if (msg.SubItems == null || msg.SubItems.Count == 0)
            {
                return msg.Text;
            }

            StringBuilder sbResult = new StringBuilder();

            sbResult.Append(settings.OFSMLInstancesStart);

            sbResult.Append(settings.OFSMLInstanceStart);
            sbResult.Append(msg.Text);
            sbResult.Append(settings.OFSMLInstanceEnd);

            foreach (string subMsg in msg.SubItems)
            {
                sbResult.Append(settings.OFSMLInstanceStart);
                sbResult.Append(subMsg);
                sbResult.Append(settings.OFSMLInstanceEnd);
            }

            sbResult.Append(settings.OFSMLInstancesEnd);

            return sbResult.ToString();
        }

        public static GlobusMessage.OFSMessage GetOFSMessageWithoutTags(Settings settings, string result)
        {
            if (result == null)
            {
                System.Diagnostics.Debug.Assert(false);
                return new GlobusMessage.OFSMessage(result);
            }

            if (!string.IsNullOrEmpty(settings.OFSResponseStart))
            {
                int idx = result.IndexOf(settings.OFSResponseStart);
                if (idx >= 0)
                    result = result.Substring(idx + settings.OFSResponseStart.Length);
            }

            if (!string.IsNullOrEmpty(settings.OFSResponseEnd))
            {
                int idx = result.LastIndexOf(settings.OFSResponseEnd);
                if (idx >= 0)
                    result = result.Substring(0, idx);
            }

            return ExtractOFSMessagesWithInstancesTags(settings, result);
        }

        private static GlobusMessage.OFSMessage ExtractOFSMessagesWithInstancesTags(Settings settings, string inputString)
        {
            string[] instancesOFSs = checkTagsAndSplit(settings, inputString);

            if (instancesOFSs == null)
                return new GlobusMessage.OFSMessage(inputString);


            string mainOFS;
            int subitemsStartIdx = 1;

            // Handling for cases like VCONN01-12 on R16 tafj:
            // AAACT16113Q58NT4QS//1,<requests><request>ARRANGEMENT:1:1=AA16113CJ1M2,ACTIVITY:1:1=ACCOUNTS-TAKEOVER-ARRANGEMENT,...

            if (
                !inputString.StartsWith(settings.OFSMLInstancesStart)
                && instancesOFSs[0].Contains("//")
                && !instancesOFSs[1].Contains("//")
                && _JoinMainAAAOFS)
            {
                // join the 0th and 1st, and then skip the 1st
                subitemsStartIdx = 2;
                mainOFS = (instancesOFSs[0] + instancesOFSs[1]).Trim();
            }
            else
                mainOFS = instancesOFSs[0].Trim();


            GlobusMessage.OFSMessage result = new GlobusMessage.OFSMessage(mainOFS);

            for (int i = subitemsStartIdx; i < instancesOFSs.Length; i++)
                result.SubItems.Add(instancesOFSs[i].Trim());

            return result;
        }

        private static string[] checkTagsAndSplit(Settings settings, string inputString)
        {
            System.Diagnostics.Debug.Assert(inputString != null);

            var instancesTags = new string[]
            {
                settings.OFSMLInstancesStart,
                settings.OFSMLInstancesEnd,
                settings.OFSMLInstanceStart,
                settings.OFSMLInstanceEnd
            };

            foreach (string ofsTag in instancesTags)
            {
                if (string.IsNullOrEmpty(ofsTag) || !inputString.Contains(ofsTag))
                    return null;
            }

            string[] instancesOFSs = inputString.Split(instancesTags, StringSplitOptions.RemoveEmptyEntries);

            if (instancesOFSs.Length == 0)
            {
                System.Diagnostics.Debug.Fail("???");
                return null;
            }

            return instancesOFSs;
        }

        private static string[] joinMainOFSifRequired(ref string[] instancesOFSs, out int subitemsStartIdx)
        {
            // Handling for cases like VCONN01-12 on R16 tafj:
            // AAACT16113Q58NT4QS//1,<requests><request>ARRANGEMENT:1:1=AA16113CJ1M2,ACTIVITY:1:1=ACCOUNTS-TAKEOVER-ARRANGEMENT,...

            subitemsStartIdx = 1;

            bool shouldJoin =
                instancesOFSs[0].Contains("//")
                && !instancesOFSs[1].Contains("//")
                && _JoinMainAAAOFS;


            if (shouldJoin)
            {
                // join the 0th and 1st, and then skip the 1st
                subitemsStartIdx = 2;
                instancesOFSs[0] = instancesOFSs[0] + instancesOFSs[1];
            }

            return instancesOFSs;
        }

        private string EncloseInStartEndTags(string ofsMessage)
        {
            return EncloseInStartEndTags(_Settings, ofsMessage);
        }

        public static string EncloseInStartEndTags(Settings settings, string ofsMessage)
        {
            return settings.OFSRequestStart + ofsMessage + settings.OFSRequestEnd;
        }

        #endregion
    }
}