﻿using OFSCommonMethods.IO.Telnet;

namespace OFSCommonMethods.IO
{
    public class TCServerIOManager : SocketBasedIOManager
    {

        #region Class Lifecycle

        internal TCServerIOManager(Settings settings, string executionId, string testStepId = "") : 
            base(settings, executionId){}

        #endregion

        #region Public Overrides (IOManager)

        public override Settings.IOType Type
        {
            get { return Settings.IOType.TCServer; }
        }

        /// <summary>
        /// Writes an OFS message to Globus and returns.
        /// </summary>
        /// <param name="isAuthorize">Is Authorize</param>
        /// <param name="gm">Globus Message</param>
        public override void WriteMessage(bool isAuthorize, GlobusMessage gm)
        {
            // TODO - Add support for AA and multipart messages see: TelnetIOManager.WriteMessage
           string result;

           PushMessageTime(gm.ID);

           Execute(gm.Message.Text, out result);

           PushMessageTime(gm.ID);

           AddMessageResult(isAuthorize, gm, new GlobusMessage.OFSMessage(result));
        }

        /// <summary>
        /// Writes an OFS message to Globus and returns.
        /// </summary>
        /// <param name="inputT24Message"></param>
        /// <param name="outpuT24Message"></param>
        /// <param name="telnetComm"></param>
        public override void WriteMessage(GlobusMessage inputT24Message, GlobusMessage outpuT24Message, TSSTelnetComm telnetComm)
        {
            // TODO - Add support for AA and multipart messages see: TelnetIOManager.WriteMessage
            string result;
            Execute(inputT24Message.Message.Text, out result, telnetComm);

            outpuT24Message.Message.Text = result;
        }

        /// <summary>
        /// Writes an OFS message to Globus and returns.
        /// </summary>
        /// <param name="isAuthorize">Is Authorize</param>
        /// <param name="gm">Globus Message</param>
        public override void WriteMessageBulk(bool isAuthorize, GlobusMessage gm)
        {
            // TODO - Add support for AA and multipart messages see: TelnetIOManager.WriteMessageBulk

            string result;

            PushMessageTime(gm.ID);
            
            Execute(gm.Message.Text, out result);

            PushMessageTime(gm.ID);

            AddMessageResult(isAuthorize, gm, new GlobusMessage.OFSMessage(result));
        }

        #endregion

    }
}
