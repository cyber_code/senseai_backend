using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Security.Principal;
using System.Text;
using System.Threading;

using OFSStringConvertor;

namespace OFSCommonMethods.IO
{
    /// <summary>
    /// Globus File Based IO Manager
    /// </summary>
    public class FileBasedIOManager : IOManager
    {
        private const string FilePrefix = "ft";

        #region Private Members

        private string _globusInputPath = "";
        private string _globusOutputPath = "";
        private string _globusAuthInputPath = "";
        private string _globusAuthOutputPath = "";

        private string _fileRequestExtension = "";
        private string _fileResponseExtension = "";

        #region Static Members

        /// <summary>
        /// Here the message results are stored
        /// </summary>
        private static Hashtable _MessageResults = new Hashtable();

        /// <summary>
        /// Files To Be Deleted
        /// </summary>
        private static ArrayList _FilesToDelete = new ArrayList();

        #endregion

        #endregion

        #region Public properties

        public override Settings.IOType Type
        {
            get { return Settings.IOType.FileBased; }
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// This routine constructs the GlobusUIManager instance. At construction it 
        /// cleans the Globus output directories of any files that may habe been left over
        /// from a previous execution. This allows us to use freely a static variable for
        /// generating a message ID.
        /// </summary>
        /// <param name="t24InputPath">The normal globus input path.</param>
        /// <param name="t24OutputPath">The normal globus output path.</param>
        /// <param name="t24AuthInputPath">The authorization input path.</param>
        /// <param name="t24AuthOutputPath">The authorization output path.</param>
        internal FileBasedIOManager(string t24InputPath, string t24OutputPath,
                               string t24AuthInputPath, string t24AuthOutputPath)
        {
            _globusInputPath = t24InputPath;
            _globusOutputPath = t24OutputPath;
            _globusAuthInputPath = t24AuthInputPath;
            _globusAuthOutputPath = t24AuthOutputPath;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Generates a unigue message file name using a static variable for a counter.
        /// </summary>
        /// <param name="isAuthorized">True if the message is an authorization, false otherwise.</param>
        /// <param name="BeginsWith">A prefix used for all message file names.</param>
        /// <param name="msgID">The ID of the message.</param>
        /// <returns></returns>
        private string GenerateUniqueGlobusFilename(bool isAuthorized, string BeginsWith, string msgID)
        {
            string outputFolder = isAuthorized ? _globusAuthInputPath : _globusInputPath;
            if (!outputFolder.EndsWith("\\"))
            {
                outputFolder = outputFolder + "\\";
            }

            return (String.Format("{0}{1}{2}", outputFolder, BeginsWith, msgID) + _fileRequestExtension);
        }

        /// <summary>
        /// Gets the ID of the message from its file name.
        /// </summary>
        /// <param name="fileName">The filename of the message.</param>
        /// <returns>The ID of the message in this file or "0"</returns>
        private string GetMessageID(string fileName)
        {
            int pos = fileName.LastIndexOf("\\");
            string idPart = fileName.Substring(pos + 3); //skip the ft

            //Sometimes we try to process filenames that contains ".copy"
            //we just will trim result string in order to get a valid ID
            pos = idPart.IndexOf(".");
            if (pos > 0)
            {
                idPart = idPart.Substring(0, pos);
            }

            return idPart;
        }

        /// <summary>
        /// Gets the ID of the message from its file name.
        /// </summary>
        /// <param name="fileName">The filename of the message.</param>
        /// <param name="id">The ID of the message in this file or "0".</param>
        /// <param name="guid">The guid part of the name.</param>
        /// <returns>true if the file name is valid</returns>
        public static bool GetMessageIDAndGUID(string fileName, ref string id, ref string guid)
        {
            fileName = Path.GetFileName(fileName);

            if (fileName.Length < FilePrefix.Length)
            {
                return false;
            }

            if (fileName.Substring(0, FilePrefix.Length).ToLower() != FilePrefix.ToLower())
            {
                return false;
            }

            //remove the file prefix
            string idPart = fileName.Substring(FilePrefix.Length);

            // Sometimes we try to process filenames that contains ".copy"
            // we just will trim result string in order to get a valid ID
            int pos = idPart.LastIndexOf(".");
            if (pos > 0)
            {
                idPart = idPart.Substring(0, pos);
            }

            // get the part before the first underscore
            int endGuidPos = idPart.IndexOf("_");
            if (endGuidPos < 0)
            {
                return false;
            }
            else
            {
                guid = idPart.Substring(0, endGuidPos);

                // make sure that this is not the last character
                if (endGuidPos != idPart.Length - 1)
                {
                    id = idPart.Substring(endGuidPos + 1);
                }
                else
                {
                    return false;
                }

                return true;
            }
        }

        public static string GetFileName(string guid, string path)
        {
            return (path + "\\" + FilePrefix + guid);
        }

        public static void ClearFiles()
        {
            lock (FileBasedIOManager._FilesToDelete.SyncRoot)
            {
                foreach (string filePath in FileBasedIOManager._FilesToDelete)
                {
                    ValidataCommon.Utils.DeleteFile(filePath);
                }

                FileBasedIOManager._FilesToDelete.Clear();
            }
        }


        /// <summary>
        /// Test if a file can be opened exclusively, i.e. nobody is using it
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <returns>true if the file is has special characters that denote that writing is completed</returns>
        public static bool FileIsReady(string fileName)
        {
            try
            {
                using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read,
                                                          FileShare.ReadWrite | FileShare.Delete))
                {
                    byte[] buffer = new byte[512];
                    int count;
                    while ((count = stream.Read(buffer, 0, 512)) != 0)
                    {
                        char[] ourChars = GetChars(buffer, count);
                        for (int i = 0; i < ourChars.Length; i++)
                        {
                            if ((ourChars[i] == '\r') || (ourChars[i] == '\n'))
                            {
                                stream.Close();
                                return true;
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            return false;
        }

        #endregion

        #region Public Methods

        public void SetExtensions(string ofsRequestExtension, string ofsResponseExtension)
        {
            _fileRequestExtension = ofsRequestExtension;
            _fileResponseExtension = ofsResponseExtension;
        }

        /// <summary>
        /// Writes an OFS message to Globus and returns.
        /// </summary>
        /// <param name="isAuthorize"></param>
        /// <param name="gm"></param>
        public override void WriteMessage(bool isAuthorize, GlobusMessage gm)
        {
            // TODO - Add support for AA and multipart messages see: TelnetIOManager.WriteMessage
            string fileName = GenerateUniqueGlobusFilename(isAuthorize, FilePrefix, gm.ID);

            StreamWriter sw = File.CreateText(fileName);
            sw.Write(gm.Message.Text);
            sw.Flush();
            sw.Close();
        }

        
        public override void WriteMessage(GlobusMessage inputT24Message, GlobusMessage outpuT24Message, OFSCommonMethods.IO.Telnet.TSSTelnetComm telnetComm)
        {
        }

        /// <summary>
        /// Writes an OFS message to Globus and returns.
        /// </summary>
        /// <param name="isAuthorize"></param>
        /// <param name="gm"></param>
        public override void WriteMessageBulk(bool isAuthorize, GlobusMessage gm)
        {
            // TODO - Add support for AA and multipart messages see: TelnetIOManager.WriteMessageBulk
            string fileName = GenerateUniqueGlobusFilename(isAuthorize, FilePrefix, gm.ID);

            StreamWriter sw = File.CreateText(fileName);
            sw.Write(gm.Message.Text);
            sw.Flush();
            sw.Close();
        }

        /// <summary>
        /// Gets the messages that were received for the thread with the
        /// given guid.
        /// </summary>
        /// <param name="guid">The guid of the thread that waits for the messages</param>
        /// <returns>A Hashtable of messages found in the message results.</returns>
        public override Hashtable GetMessages(string guid)
        {
            // The GlobusIOManager keeps an ArrayList of results for every thread
            // GUID that was registered in it
            lock (_MessageResults.SyncRoot)
            {
                if (_MessageResults.ContainsKey(guid))
                {
                    Hashtable results = (Hashtable) _MessageResults[guid];
                    _MessageResults.Remove(guid);

                    return results;
                }
                else
                {
                    return new Hashtable();
                }
            }
        }

        /// <summary>
        /// Reads all files from the output Globus directory
        /// </summary>
        /// <param name="isAuthorize">If true read from the authorize path, else 
        /// read from the data path</param>
        public override void ReadResults(bool isAuthorize)
        {
            string path = isAuthorize ? _globusAuthOutputPath : _globusOutputPath;

            string[] globusFiles = Directory.GetFiles(path);
            if (globusFiles.Length == 0)
            {
                return;
            }

            string guid;
            string msgID;

            List<string> readyFilesCollection = new List<string>();

            //register response times
            foreach (string filePath in globusFiles)
            {
                //First we check if this is a message for us
                guid = string.Empty;
                msgID = string.Empty;
                bool isValid = GetMessageIDAndGUID(filePath, ref msgID, ref guid);
                if (! isValid)
                {
                    continue;
                }

                // Only messages with guids that are registered in this GlobusIOManager are handles.
                // Other messages could be the result of other Globus adapters contacting the same Globus
                lock (IOManager._ThreadEvents.SyncRoot)
                {
                    if (!IOManager._ThreadEvents.ContainsKey(guid))
                    {
                        //this message is not for us
                        continue;
                    }
                }

                if (! FileIsReady(filePath))
                {
                    //file is not ready yet
                    continue;
                }

                //register response times
                //we don't need to care for locking the hashtable because our message
                //identification scheme guarantees that the key is unique so we don't
                //have a problem if many threads work with the hashtable in the same time
                //save the response time
                int slashPos = filePath.LastIndexOf("\\");
                string fileName = filePath.Substring(slashPos + 1);

                PushMessageTime(fileName, true);

                readyFilesCollection.Add(filePath);
            }

            Dictionary<string, AutoResetEvent> threadList = new Dictionary<string, AutoResetEvent>();

            foreach (string filePath in readyFilesCollection)
            {
                //First we check if this is a message for us
                guid = string.Empty;
                msgID = string.Empty;
                bool isValid = GetMessageIDAndGUID(filePath, ref msgID, ref guid);
                if (!isValid)
                {
                    continue;
                }

                StringBuilder sb = new StringBuilder();
                try
                {
                    FileStream s2 = new FileStream(filePath, FileMode.Open, FileAccess.Read,
                                                   FileShare.ReadWrite | FileShare.Delete);

                    byte[] buffer = new byte[512];
                    int offset = 0;
                    int count;
                    while ((count = s2.Read(buffer, offset, 512)) != 0)
                    {
                        char[] ourChars = GetChars(buffer, count);

                        sb.Append(ourChars);

                        //offset += count;
                    }

                    buffer = null;
                    s2.Flush();
                    s2.Close();
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvents(isAuthorize ? "Listen Authorize Thread" : "Listen Data Thread",
                                          GlobusEventType.Crash, ex.Message);
                    continue;
                }

                GlobusMessage responseMessage;
                responseMessage = new GlobusMessage(msgID);
                responseMessage.Message.Text = sb.ToString();

                //log Globus response
                EventLogger.LogEvents(isAuthorize ? "Listen Authorize Thread" : "Listen Data Thread",
                                      GlobusEventType.DataLog, responseMessage.Message.Text);

                lock (_MessageResults.SyncRoot)
                {
                    if (!_MessageResults.ContainsKey(guid))
                    {
                        _MessageResults[guid] = new Hashtable(); 
                    }

                    Hashtable resultList = (Hashtable)_MessageResults[guid];

                    //Now that we don't delete the messages until the end, we have to protect
                    //against reading many times one and the same message. This can be done
                    //simply by using the hashtable behaviour
                    if (!resultList.ContainsKey(msgID))
                    {
                        //this is a new message
                        resultList[msgID] = responseMessage;

                        // Delete from the collection the instance, whose response we have received
                        lock (_FilesToDelete.SyncRoot)
                        {
                            _FilesToDelete.Add(filePath);
                        }

                        //find the event for that controls the thread that has this guid
                        lock (IOManager._ThreadEvents.SyncRoot)
                        {
                            threadList[guid] = (AutoResetEvent) IOManager._ThreadEvents[guid];
                        }
                    }
                }
            }

            /*unleash all threads that have results to receive*/
            foreach (AutoResetEvent threadEvent in threadList.Values)
            {
                //wake the processing thread
                threadEvent.Set();
            }
        }

        /// <summary>
        /// Reads all files from the output Globus directory
        /// </summary>
        /// <param name="isAuthorize">If true read from the authorize path, else read from the data path</param>
        /// <param name="request"></param>
        /// <param name="requestGUID"></param>
        public override GlobusMessage GetDataResult(bool isAuthorize, GlobusRequestShort request, string requestGUID)
        {
            GlobusMessage responseMessage = null;

            string path = isAuthorize ? _globusAuthOutputPath : _globusOutputPath;

            string fileName = GetFileName(requestGUID, path);
            fileName += _fileResponseExtension;

            if (fileName.Length > 0)
            {
                FileInfo messageFileInfo = new FileInfo(fileName);
                if (messageFileInfo.Exists)
                {
                    if (FileIsReady(fileName))
                    {
                        StringBuilder sb = new StringBuilder();
                        try
                        {
                            GetMessageText(fileName, sb);
                            responseMessage = new GlobusMessage(requestGUID);
                            responseMessage.Message.Text = sb.ToString().Replace("\n", String.Empty);
                            File.Delete(fileName);
                        }
                        catch (Exception ex)
                        {
                            EventLogger.LogEvents(isAuthorize ? "Listen Authorize Thread" : "Listen Data Thread",
                                                  GlobusEventType.Crash, ex.Message);
                        }
                    }
                }
            }

            return responseMessage;
        }

        /// <summary>
        /// Check Existance and Accessibility of Phantoms
        /// </summary>
        /// <param name="inputInPath"></param>
        /// <param name="inputOutPath"></param>
        /// <param name="authInPath"></param>
        /// <param name="authOutPath"></param>
        /// <param name="descriptiveError"></param>
        /// <param name="adapterIdentity"></param>
        /// <returns></returns>
        internal static bool PhantomsPathsExistingAndAccessible(string inputInPath, string inputOutPath, 
            string authInPath, string authOutPath, StringBuilder descriptiveError, WindowsIdentity adapterIdentity)
        {
            adapterIdentity.Impersonate();

            string testFileName = string.Format("TestFile_{0}.txt", Guid.NewGuid());
            string testFileContent = "test";
            bool result = true;

            if (!CheckPhantom(inputInPath, inputOutPath, descriptiveError, testFileName, testFileContent))
            {
                result = false;
            }

            if (!CheckPhantom(authInPath, authOutPath, descriptiveError, testFileName, testFileContent))
            {
                result = false;
            }

            return result;
        }

        #endregion

        #region Private Static Methods

        /// <summary>
        /// Add Exception Message
        /// </summary>
        /// <param name="inputInPath"></param>
        /// <param name="descriptiveError"></param>
        /// <param name="testFileName"></param>
        /// <param name="message"></param>
        private static void AddExceptionMessage(string inputInPath, StringBuilder descriptiveError, string testFileName,
                                                string message)
        {
            descriptiveError.AppendFormat("Error: {0} for folder \"{1}\"\r\n",
                message.Replace("\\" + testFileName, String.Empty),
                inputInPath);
        }

        /// <summary>
        /// Decodes the OFS message using the selected encoding
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        private static char[] GetChars(byte[] bytes, int count)
        {
            switch (EncodingUsed)
            {
                case GlobusEncoding.UTF8:
                    return UTF8Encoding.UTF8.GetChars(bytes, 0, count);
                case GlobusEncoding.ASCII:
                    return ASCIIEncoding.ASCII.GetChars(bytes, 0, count);
                case GlobusEncoding.UTF16:
                    return UnicodeEncoding.Unicode.GetChars(bytes, 0, count);
                case GlobusEncoding.UTF32:
                    return UTF32Encoding.UTF32.GetChars(bytes, 0, count);
                default:
                    return UTF8Encoding.UTF8.GetChars(bytes, 0, count);
            }
        }

        /// <summary>
        /// Retrieve Message Text
        /// </summary>
        /// <param name="filePath">File Path</param>
        /// <param name="sb">Output String Bulder</param>
        private static void GetMessageText(string filePath, StringBuilder sb)
        {
            FileStream s2 = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite | FileShare.Delete);

            byte[] buffer = new byte[512];
            int offset = 0;
            int count;
            while ((count = s2.Read(buffer, offset, 512)) != 0)
            {
                char[] ourChars = GetChars(buffer, count);
                sb.Append(ourChars);
            }

            buffer = null;
            s2.Flush();
            s2.Close();
        }

        /// <summary>
        /// Check Phantom
        /// </summary>
        /// <param name="inputInPath"></param>
        /// <param name="inputOutPath"></param>
        /// <param name="descriptiveError"></param>
        /// <param name="testFileName"></param>
        /// <param name="testFileContent"></param>
        /// <returns></returns>
        private static bool CheckPhantom(string inputInPath, string inputOutPath, StringBuilder descriptiveError,
                                         string testFileName, string testFileContent)
        {
            bool result = CheckPathExistanceAndAddErrorMessage(inputInPath, descriptiveError);
            if (result)
            {
                try
                {
                    string fullInputTestPath = inputInPath + "\\" + testFileName;
                    string fullOutputTestPath = inputOutPath + "\\" + testFileName;

                    File.WriteAllText(fullInputTestPath, testFileContent);
                    result = CheckPathExistanceAndAddErrorMessage(inputOutPath, descriptiveError);
                    if (result)
                    {
                        try
                        {
                            Thread.Sleep(8000); //TODO move this constant in the XML configuration
                            if (File.Exists(fullOutputTestPath))
                            {
                                // TODO why do we do that?
                                using (StreamReader sr = new StreamReader(fullOutputTestPath))
                                {
                                    sr.ReadToEnd();
                                }

                                ValidataCommon.Utils.DeleteFile(fullOutputTestPath);
                            }
                            else
                            {
                                result = false;
                                descriptiveError.AppendFormat("T24 Phantom for folders \"{0}\" and \"{1}\" is not active.\r\n", inputInPath, inputOutPath);
                            }
                        }
                        catch (Exception ex)
                        {
                            result = false;
                            AddExceptionMessage(inputOutPath, descriptiveError, testFileName, ex.Message);
                        }
                    }
                }
                catch (Exception exc)
                {
                    result = false;
                    AddExceptionMessage(inputInPath, descriptiveError, testFileName, exc.Message);
                    CheckPathExistanceAndAddErrorMessage(inputOutPath, descriptiveError);
                }
            }
            else
            {
                CheckPathExistanceAndAddErrorMessage(inputOutPath, descriptiveError);
            }
            return result;
        }

        /// <summary>
        /// Check Path Existance And Add Error Message
        /// </summary>
        /// <param name="inputInPath"></param>
        /// <param name="descriptiveError"></param>
        /// <returns></returns>
        private static bool CheckPathExistanceAndAddErrorMessage(string inputInPath, StringBuilder descriptiveError)
        {
            bool result = true;
            if (!Directory.Exists(inputInPath))
            {
                result = false;
                descriptiveError.AppendFormat("Directory \"{0}\" does not exist\r\n", inputInPath);
            }
            return result;
        }

        #endregion
    }
}
