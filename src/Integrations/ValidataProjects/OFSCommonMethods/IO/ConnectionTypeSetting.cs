﻿using System.Collections.Generic;
using OFSCommonMethods.IO.Telnet;

namespace OFSCommonMethods.IO
{
    public abstract class ConnectionTypeSetting : ExecutionSteps
    {
        public abstract ConsoleType ConnectionType { get; }

        public Settings CommonSettings;

        private string _GlobusUserName;
        private string _GlobusPassword;
        private int? _TelnetResponseTimeOut;
        private int? _PageCode;

        public string GlobusUserName
        {
            get { return _GlobusUserName ?? CommonSettings.GlobusUserName; }
            set { _GlobusUserName = value; }
        }

        public string GlobusPassword
        {
            get { return _GlobusPassword ?? CommonSettings.GlobusPassword; }
            set { _GlobusPassword = value; }
        }

        public int TelnetResponseTimeOut
        {
            get { return _TelnetResponseTimeOut ?? int.Parse(CommonSettings.TelnetResponseTimeOut); }
            set { _TelnetResponseTimeOut = value; }
        }
        
        public int PageCode
        {
            get { return _PageCode ?? CommonSettings.PageCode; }
            set { _PageCode = value; }
        }

        public virtual List<ExecutionStep> GetAllLoginSteps()
        {
            List<ExecutionStep> steps = new List<ExecutionStep>();
            steps.AddRange(CommonSettings.CommonExecutionSteps.LoginSteps);
            steps.AddRange(LoginSteps);
            return steps;
        }

        public virtual List<ExecutionStep> GetAllExitSteps()
        {
            List<ExecutionStep> steps = new List<ExecutionStep>();
            steps.AddRange(ExitSteps);
            steps.AddRange(CommonSettings.CommonExecutionSteps.ExitSteps);
            return steps;
        }

        public string OFSWebServiceUrl { get; set; }

        public string SubroutineInvokerWebServiceUrl { get; set; }

        public int WebServiceResponseTimeOut { get; set; }
    }
}
