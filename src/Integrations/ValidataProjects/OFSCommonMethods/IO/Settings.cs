﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace OFSCommonMethods.IO
{
    public class DBConnSettings
    { 
        public string ServerIP { get; internal set; }

        public Settings.IOType ConnType = Settings.IOType.Telnet;
        public string IdentityFile { get; internal set; }
        public string Passphrase { get; internal set; }
        public ExecutionSteps LoginExecutionSteps { get; internal set; }
        public string Timeout { get; internal set; }
        public string Password { get; internal set; }
        public string Username { get; internal set; }
    }
    public class Settings
    {
        public const string JShellSectionName = "JShell";

        public const string SystemShellSectionName = "Shell";

        public const string OfsSectonName = "OFS";

        public const string GlobusClassicSectionName = "GlobusClassic";

        public const string HttpWebService = "HttpWebService";

        public const string HttpRestService = "HttpRestService";

        public static string GetNodeNameForConnectionHandler(ConsoleType consoleType)
        {
            switch (consoleType)
            {
                case ConsoleType.JShell:
                    return JShellSectionName;
                case ConsoleType.Shell:
                    return SystemShellSectionName;
                case ConsoleType.OFS:
                    return OfsSectonName;
                case ConsoleType.Classic:
                    return GlobusClassicSectionName;
                case ConsoleType.HttpWebService:
                    return HttpWebService;
                case ConsoleType.HttpRestService:
                    return HttpRestService;
                default:
                    throw new ArgumentOutOfRangeException("consoleType");
            }
        }

        public static ConsoleType? GetConsoleTypeFromConnectionHandlerName(string consoleType)
        {
            if (string.IsNullOrEmpty(consoleType)) return null;

            switch (consoleType)
            {
                case JShellSectionName:
                    return ConsoleType.JShell;
                case SystemShellSectionName:
                    return ConsoleType.Shell;
                case OfsSectonName:
                    return ConsoleType.OFS;
                case GlobusClassicSectionName:
                    return ConsoleType.Classic;
                case HttpWebService:
                    return ConsoleType.HttpWebService;
                case HttpRestService:
                    return ConsoleType.HttpRestService;
                default:
                    throw new ArgumentOutOfRangeException("consoleType");
            }
        }

        public enum SetPermissionsApproach
        { 
            Skip,
            Shell,
            Communicator
        }

        public enum IOType
        {
            None = 0,
            FileBased,
            Telnet,
            SharpSsH,
            TCServer,
            HttpWebService,
            SSH, // to be used with new SSH library - Renci.SshNet            
            HttpRestService,
            LocalShell
        }

        public enum OFSType
        {
            OFS,
            OFSML
        }

        public enum T24InstancesImportMethod
        {
            GenericT24AdapterCall,
            PostOfsMessage,
            BatchFileListener
        }

        public IOType Type = IOType.Telnet;

        public OFSType OfsType = OFSType.OFS;

        public string IdentityFile;

        public string Passphrase;

        public string PhantomSet = string.Empty;

        public string AdditionalPhantomSet = string.Empty;

        /// <summary>
        /// To be used, if we want to have several concurrent connection to the same environment with the same user/password
        /// </summary>
        public string UniqueID;

        #region Constants

        public const string DefaultDuplicationString = "(DUP!!)";

        #endregion

        #region File Based Settings

        public string GlobusInput = string.Empty;

        public string GlobusOutput = string.Empty;

        public string GlobusAuthInput = string.Empty;

        public string GlobusAuthOutput = string.Empty;

        #endregion


        #region  DBConnSettings

        public DBConnSettings DBConnection;


        #endregion

        #region Telnet Settings

        public string InputUser = string.Empty;

        public string InputPassword = string.Empty;

        public string AuthorizationUser = string.Empty;

        public string AuthorizationPassword = string.Empty;

        public string Version = string.Empty;

        public string GlobusServer = string.Empty;

        public string GlobusUserName = string.Empty;

        public string GlobusPassword = string.Empty;

        public string TelnetResponseTimeOut = string.Empty;

        public string OFSRequestStart = string.Empty;

        public string OFSRequestEnd = string.Empty;

        public string OFSResponseStart = string.Empty;

        public string OFSResponseEnd = string.Empty;

        public string OFSMLInstancesStart = string.Empty;

        public string OFSMLInstancesEnd = string.Empty;

        public string OFSMLInstanceStart = string.Empty;

        public string OFSMLInstanceEnd = string.Empty;

        public string OFSUseOldHistoryReadResponse = string.Empty;

        public int TelnetExportToT24ThreadsCount = 1;

        public int TelnetExportToT24MinThreadsCount = -1;

        public int TelnetExportToT24ThreadsInitializationTimeout = 300000;  // 5 min default

        public int TelnetThreadsConnectResponseTimeout = 0;

        public int TelnetImportFromT24ThreadsCount = 1;

        public int TelnetImportFromT24MinThreadsCount = -1;

        public int TelnetImportFromT24ThreadsInitializationTimeout = 300000;  // 5 min default

        public T24InstancesImportMethod ExportToT24Method;

        #region Batch file Listener related settings

        public string BatchFileListenerOfsRequestsFolderPath;

        public string BatchFileListenerOfsReresponsesFolderPath;

        public int BatchFileListenerRepeatableResponseCheckTimeInterval;

        public int BatchFileListenerResponseWaitTimeout;

        public string OfsRequestMessageIdPrefix_Prefix;

        public string BatchFileListenerRequestNewLineCharacter;

        public int OfsRequestMessageIdPrefix_RandomStringCharactersNumber;

        #endregion

        #region PostOFS related settings

        public int OfsPostMessageAsyncCommandTimeout;

        public string OFSPostSettingsActionType = string.Empty;

        public string OFSPostSettingsFileFtpLocation = string.Empty;

        public string OFSPostMessageQueuePrefix = string.Empty;

        public string OFSPostMessageServicesT24CheckCommand = string.Empty;

        public string OFSPostMessageServicesT24CWakeUpCommand = string.Empty;

        public int OFSPostMessageServicesT24CWakeUpCommand_RetryPeriod = 0;

        public string OFSPostRequiredAgentsT24CheckCommand = string.Empty;

        public string OFSPostMessageSourceT24CheckCommand = string.Empty;

        public string OFSPostMessageTSMServiceT24CheckCommand = string.Empty;

        public string OFSPostMessageServicesCheckCommand = string.Empty;

        public string OFSPostMessageServicesCheckCommand_WaitFor = string.Empty;

        public long OFSPostMessageFailoverRetry_RetryPeriod = 0;

        public long OFSPostMessageFailoverRetry_AttemptEvery = 180000;

        public string OFSPostMessageRequestFileFtpLocation = string.Empty;

        public string OFSPostMessageResponseFileFtpLocation = string.Empty;

        public string OFSPostMessageSource = string.Empty;

        #endregion

        public int TelnetPoolSize = 12;

        public int TelnetRequestBufferSize = -1;

        public long TimeoutForFreeConnection = 2 * 60 * 1000;   // 2 min

        public string CommandPromptSuffix = ">";

        public string CoreUpdatesPath = "";

        public string TamUpdatesPath = "";

       

        public string FinancialReportPath = "";

        public string LWorkDayToCvf = "";

        public string RollBackForceDelete = "";

        public string Objectdir = string.Empty;

        public string Exportname = string.Empty;

        public string DuplicationString = DefaultDuplicationString;

        public string FtpHome;

        public string FtpHomeFullPath;

        public string FtpListLineRegex;

        public List<string> FtpListSkippedLinesRegex;

        public string FtpDirectoryGroupText;

        public string FtpUsername;

        public string FtpPassword;

        public string FtpIdentityFile;

        public bool IsSFtp;

        public bool UseSharpSshFtp;

        public bool IsSharedDir;

        public SetPermissionsApproach FTPSetAccessMethod;

        public bool FtpKeepAliveFirst;

        public bool FtpKeepAliveDefault;

        public bool FtpUsePassive;

        public string FtpHostNameAndPort;

        public bool KeepAliveDefault;

        public bool KeepAliveFirst;

        public string ObjectDelimiter = string.Empty;

        public string ObjectExtension = string.Empty;

        public string[] IgnoreSubroutineCharacters;

        public string LibVersionDelimiter = string.Empty;

        public string JSh = string.Empty;

        public bool SkipWhiteSpaceInFieldValue;

        public bool TreatOFSResponseSingleValueAsError;

        public int CopyCommandTimeOut = 300000; // 5 min

        public bool SkipCheckSum;

        public string JBCDEV_LIB;

        public string JBCDEV_BIN;

        public string PathGlobusLib;
        public string PathGlobusBin;
        public string PathGlobusBP;

        public string OFSReplaceText;

        public string RecordCopySuccessResponce;

        public Dictionary<string, string> ObjectDirOverrides = new Dictionary<string, string>();

        public List<string> RegexesForIgnoredExtraLinesInCatalogCommandResponse = new List<string>();

        public readonly UnexpectedCommunicationErrorList UnexpectedCommunicationErrors = new UnexpectedCommunicationErrorList();

        /// <summary>
        /// Default UTF-8
        /// </summary>
        public int PageCode = 65001;

        private readonly List<ConnectionTypeSetting> _ConnectionTypes = new List<ConnectionTypeSetting>();

        public List<ConnectionTypeSetting> ConnectionTypes
        {
            get { return _ConnectionTypes; }
        }

        public ConnectionTypeSetting GetConnectionSettingsByType(ConsoleType type)
        {
            ConnectionTypeSetting connectionTypeSetting = ConnectionTypes.Find(cts => cts.ConnectionType == type);
            if (connectionTypeSetting != null)
            {
                return connectionTypeSetting;
            }

            throw new ApplicationException(string.Format(
                                               "The section '{0}' in TelnetCommmDef.xml is missing or invalid ",
                                               GetNodeNameForConnectionHandler(type)));
        }

        private readonly ExecutionSteps _CommonExecutionSteps = new ExecutionSteps();

        public ExecutionSteps CommonExecutionSteps
        {
            get { return _CommonExecutionSteps; }
        }

        public string UpdaterPackExistedString { get; set; }

        public RoutinesSettings T24Routines = new RoutinesSettings();

        public SkipLinesSettings SkipLines = new SkipLinesSettings();

        public CompaniesSettings Companies = new CompaniesSettings();

        public CommandResultSettings CommandResults = new CommandResultSettings();

        public OFSResultHandlingSettings OFSResultHandling = new OFSResultHandlingSettings();

        public Dictionary<JShellCommandType, JShellCommand> JShellCommands = new Dictionary<JShellCommandType, JShellCommand>();

        public Dictionary<ShellCommandType, ShellCommand> ShellCommands = new Dictionary<ShellCommandType, ShellCommand>();

        public string BuildControlDeploymentPath;

        public string BuildControlSavePath;
        public BCONVersions BCONVersion = null;

        public string ArchiveExt;

        public string BuildControlDeploymentFields;

        public int BuildControlRecordVerifyTimeOut;

        public string OS = string.Empty;

        public ClassicSettings Classic = new ClassicSettings();

        public bool DeployAllBinaryFiles;

        public Dictionary<string, List<string>> SpecialTypicalAttibutes;

        public Dictionary<string, string> CustomEnquiries;

        public List<string> NotAuthorizableApplications;

        public string LocalTableEnquiryName;

        public Dictionary<string, int> LocalTableEnquiryResultColumnsMap;

        public bool JoinTerminalLines;
		
        #region T24 Production Import
        public string ProductionImportOFS;

        public string PathForProductionImport;

        public string PathForProductionImportErrors;

        public string CSVMultiValueSeparator;

        public string CSVSubValueSeparator;

        public string CSVMultiValueSeparatorEscape;

        public string CSVSubValueSeparatorEscape;
        #endregion

        #endregion
         
        
       

        public AATestGenerationRoutineParameters AaTestGenerationRoutineParameters;

        public static IOType GetTypeFromString(string str)
        {
            try
            {
                if (string.IsNullOrEmpty(str))
                    return IOType.Telnet;

                switch (str.ToUpper())
                {
                    case "TELNET":
                        return IOType.Telnet;
                    case "SSH":
                        return IOType.SSH;
                    case "SHARPSSH":
                        return IOType.SharpSsH;
                    case "HTTPWEBSERVICE":
                        return IOType.HttpWebService;
                    case "HTTPRESTSERVICE":
                        return IOType.HttpRestService;
                    case "FILE":
                        return IOType.FileBased;
                    default:
                        return IOType.FileBased;
                }
            }
            catch
            {
                return IOType.Telnet;
            }
        }

        public List<JShellCommandType> GetJShellCommandErrors()
        {
            var result = new List<JShellCommandType>();

            foreach (JShellCommandType type in JShellCommands.Keys)
            {
                JShellCommand command = JShellCommands[type];

                try
                {
                    command.CreateRegex();
                }
                catch (Exception)
                {
                    result.Add(type);
                }
            }

            return result;
        }

        public RecordsRollbackSettings RecordRollbackOptions = new RecordsRollbackSettings();

        public TimeDateCommand SpecialTimeDateCommand = new TimeDateCommand();

        public string GetEnquiryForApplication(string application)
        {
            return CustomEnquiries.ContainsKey(application)
                ? CustomEnquiries[application]
                : "%" + application;
        }

        [Serializable]
        public class AATestGenerationRoutineParameters
        {
            public string RoutineName { get; set; }
            public string CallDelimiter { get; set; }
            public string Delimiter { get; set; }
            public string TAFJ_or_TAFC { get; set; }
            public string Timeout { get; set; }
            public string AaArrangementActivityEnquiry { get; set; }
            public string AaProductEnquiry { get; set; }
            public bool CallProdEnqByGroup { get; set; }
        }
        [Serializable]
        public class TimeDateCommand
        {
            public TimeDateCommand()
            {
                TimeCommand = "";
                DateCommand = "TIME";
                TimeFormat = "";
                DateFormat = "";
                TimeConsoleType = ConsoleType.JShell;
                DateConsoleType = ConsoleType.JShell;
            }
            public string TimeCommand { get; set; }
            public string DateCommand { get; set; }
            public string TimeFormat { get; set; }
            public string DateFormat { get; set; }

            public ConsoleType TimeConsoleType { get; set; }
            public ConsoleType DateConsoleType { get; set; }
        }

    }
}
