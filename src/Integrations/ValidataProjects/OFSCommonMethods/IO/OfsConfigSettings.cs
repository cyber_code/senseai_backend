﻿namespace OFSCommonMethods.IO
{
    internal class OfsConfigSettings : ConnectionTypeSetting
    {
        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.OFS; }
        }
    }
}