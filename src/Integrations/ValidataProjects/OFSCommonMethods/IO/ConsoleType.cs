﻿namespace OFSCommonMethods.IO
{
    public enum ConsoleType
    {
        /// <summary>
        /// The JShell is what appears after clicking N when you start Globus
        /// </summary>
        JShell,

        /// <summary>
        /// The system shell may be windows/unix based
        /// </summary>
        Shell,

        /// <summary>
        /// This could be TSS or G12/G13
        /// </summary>
        OFS,

        /// <summary>
        /// Globus Classis is the old colored interface that appears if you click Y when you start Globus
        /// </summary>
        Classic,

        /// <summary>
        /// Http Web Service for use with R16
        /// </summary>
        HttpWebService,

        /// <summary>
        /// Http Rest Service
        /// </summary>
        HttpRestService,

        /// <summary>
        /// System shell of a db machine different from the environment host
        /// </summary>
        DBShell
    }
}