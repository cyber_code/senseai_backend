﻿namespace OFSCommonMethods
{
    public class ValidataOFSMessage
    {
        private string _GeneratorID;
        private string _RequestID;
        private string _GlobusID;
        private string _ExecutionState;
        private string _OfsMessage;

        public string GeneratorID
        {
            get { return _GeneratorID; }
            set { _GeneratorID = value; }
        }

        public string RequestID
        {
            get { return _RequestID; }
            set { _RequestID = value; }
        }

        public string GlobusID
        {
            get { return _GlobusID; }
            set { _GlobusID = value; }
        }

        public string RequestExeState
        {
            get { return _ExecutionState; }
            set { _ExecutionState = value; }
        }

        public string OfsMessage
        {
            get { return _OfsMessage; }
            set { _OfsMessage = value; }
        }

        public override string ToString()
        {
            return OfsMessage;
        }
    }
}