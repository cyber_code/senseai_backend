﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods
{
    public class GlobusVersionInfo
    {
        #region Private Members

        private List<string> _RekeyAttributes = new List<string>();

        private List<string> _AssocVersions = new List<string>();

        private string _GbDescription = null;

        #endregion

        #region Public Properties

        public List<string> RekeyAttributes
        {
            get { return _RekeyAttributes; }
        }

        public int MaxTabs
        {
            get { 
                return (
                        1 /*Full View*/ +
                        _AssocVersions.Count +
                        1
                        /*(string.IsNullOrEmpty(_GbDescription)? 0 : 1)*/
                    );
            }
        }

        #endregion

        #region Public Methods

        public void AddRekey(string attributeName)
        {
            _RekeyAttributes.Add(attributeName);
        }

        public void SetGbDescription(string description)
        {
            _GbDescription = null;

            if(string.IsNullOrEmpty(description))
            {
                return;
            }

            if(description == "No Value")
            {
                return;
            }

            _GbDescription = description;
        }

        public void AddAssocVersion(string versionName)
        {
            _AssocVersions.Add(versionName);
        }

        #endregion
    }
}
