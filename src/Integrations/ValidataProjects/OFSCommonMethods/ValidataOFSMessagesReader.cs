using System.Collections.Generic;
using System.IO;
using System.Xml;
using AXMLEngine;

namespace OFSCommonMethods
{
    public static class ValidataOFSMessagesReader
    {
        public static ValidataOFSMessageList ReadAllItems(string file)
        {
            return new ValidataOFSMessageList(EnumerateAllItems(file));
        }

        public static IEnumerable<ValidataOFSMessage> EnumerateAllItems(string file)
        {
            if (!File.Exists(file))
            {
                yield break;
            }

            using (XmlReader xmlReaderTempErrors = XmlReader.Create(file, XmlFragmentLogHelper.GetSettings()))
            {
                if (xmlReaderTempErrors.Read())
                {
                    while (!xmlReaderTempErrors.EOF)
                    {
                        xmlReaderTempErrors.MoveToContent();
                        ValidataOFSMessage msg = ReadMessage(xmlReaderTempErrors);
                        yield return msg;
                    }
                }
            }
        }

        private static ValidataOFSMessage ReadMessage(XmlReader reader)
        {
            ValidataOFSMessage msg = new ValidataOFSMessage();

            msg.GeneratorID = reader.GetAttribute(AXMLConstants.GENERATOR_ID_TAG);
            msg.RequestID = reader.GetAttribute(AXMLConstants.REQUEST_ATTRIBUTE_ID);
            msg.RequestExeState = reader.GetAttribute(AXMLConstants.REQUEST_EXECUTION_STATE);
            msg.GlobusID = reader.GetAttribute(AXMLConstants.OFS_GLOBUS_ID);

            msg.OfsMessage = reader.ReadInnerXml();

            return msg;
        }
    }
}