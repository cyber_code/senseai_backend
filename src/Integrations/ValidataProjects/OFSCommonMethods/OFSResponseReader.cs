using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using AXMLEngine;
using OFSCommonMethods.IO.Telnet;
using Validata.Common;
using ValidataCommon;
using OFSCommonMethods.OFSEnquiry;

namespace OFSCommonMethods
{
    /// <summary>
    /// Parser of OFS Response messages
    /// </summary>
    public class OFSResponseReader : OFSResponseReaderBase
    {
        #region Properties

        public bool IsAAProcessDetails { get; set; }

        #endregion

        #region Life cycle

        public OFSResponseReader()
        {
        }

        public OFSResponseReader(Dictionary<string, List<string>> specialTypicalAttributes, string validataID)
            : base (specialTypicalAttributes, validataID)
        {
        }

        public OFSResponseReader(string phantomSet, Dictionary<string, List<string>> specialTypicalAttributes)
            : base(phantomSet, specialTypicalAttributes)
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Parse the enquiry with the help of the expected fields in the response typical
        /// </summary>
        /// <param name="enquiryName"></param>
        /// <param name="msgText"> OFS response message </param>
        /// <param name="axmlWriter">The VXML writer for the response converted to XML instances</param>
        /// <param name="responseTypical"> Response Typical </param>
        /// <param name="logger"></param>
        /// <param name="treatOFSResponseSingleValueAsError"></param>
        /// <param name="enquiryResultParsingRoutine">Enquiry result parting routine</param>
        /// <returns> The OFS error message if any</returns>
        public string ReadEnquiryResponse(string enquiryName,
                                          string msgText,
                                          MetadataTypical responseTypical,
                                          XmlWriter axmlWriter,
                                          TSSTelnetComm.LogMessageHandle logger,
                                          bool treatOFSResponseSingleValueAsError,
                                          EnquiryResultParsingRotine enquiryResultParsingRoutine,
                                          List<Filter> filters = null)
        {
            System.Diagnostics.Debug.Assert(enquiryResultParsingRoutine != null);

            // TODO: add XML Writer to write instances in OUTPUT AXML if we stop here
            // Create Short Requests for further proccesing if needed
            try
            {
                if (msgText == "SECURITY.VIOLATION" || msgText == "SECURITY VIOLATION")
                {
                    const string errorMsg = "Security violation: No permissions to perform the query";
                    throw new EnquiryConfigurationException(errorMsg, EnquiryExceptionType.Undefined);
                }

                _HasError = false;
                StringBuilder instanceXML = new StringBuilder();

                int readPos;
                int firstCommaPos = msgText.IndexOf(",");
                if (firstCommaPos >= 0 && !msgText.StartsWith("Error"))
                {
                    if ((firstCommaPos + 1) < msgText.Length)
                    {
                        if (msgText[firstCommaPos + 1] == ',')
                        {
                            if ((firstCommaPos + 2) < msgText.Length)
                            {
                                if (msgText[firstCommaPos + 2] != '\"')
                                {
                                    firstCommaPos++;
                                }
                            }
                        }
                    }
                    readPos = firstCommaPos + 1;
                }
                else
                {
                    // if there is no comma this means we received an error
                    _HasError = true;

                    if (String.IsNullOrEmpty(msgText))
                        msgText = "Enquiry response is empty text. Equiry name: " + enquiryName;

                    throw new EnquiryConfigurationException(msgText, EnquiryExceptionType.Undefined);
                }

                int secondCommaPos = msgText.IndexOf(",", readPos);

                // If there is only one text value after the second comma then it is an error  message.
                // This means that if there is no comma and only one pair of quotation marks in the string then it is an error message. 
                string testMessage = msgText.Substring(secondCommaPos + 1);
                if (testMessage.IndexOf(",") < 0)
                {
                    // counts number of quotes in the rest of the message
                    int quotes = 0;
                    for (int i = 0; i < testMessage.Length; i++)
                    {
                        if (testMessage[i] == '"')
                        {
                            quotes++;
                            if (quotes > 2)
                            {
                                break;
                            }
                        }
                    }

                    if (quotes == 2 && treatOFSResponseSingleValueAsError)
                    {
                        // if there are only two quotes by the end of the message - what is in the quotes is an Error message
                        _HasError = true;
                        throw new EnquiryConfigurationException(msgText, EnquiryExceptionType.Undefined);
                    }
                }

                List<string> headers = new List<string>();
                for (int i = 0; i < responseTypical.Attributes.Count; i++)
                {
                    TypicalAttribute attr = responseTypical.Attributes[i];
                    if (attr.Name != "ID")
                        headers.Add(XmlConvert.DecodeName(attr.Name));
                }

                var levelStructure = enquiryResultParsingRoutine.LevelStructure;

                if (levelStructure == null)
                {
                    // not defined by the execution call parameters -> try to get it from global configuration xml
                    bool hasCustomTemplate = !String.IsNullOrEmpty(enquiryResultParsingRoutine.TemplateName);
                    levelStructure = EnquiryResponseRule.GetRuleFromXML(
                            _PhantomSet,
                            hasCustomTemplate ? enquiryResultParsingRoutine.TemplateName : enquiryName,
                            "T24OFSAdapter",
                            hasCustomTemplate
                        );
                }

                string actualResult = msgText.Substring(secondCommaPos + 1);
                EnquiryResponse rows;

                if (levelStructure == null)
                {
                    // there is not configuration settings for this enquiry in EnquiryConfiguration.xml file
                    rows = EnquiryResponseResultParser.GetResult(actualResult);
                }
                else
                {
                    EnquiryResponse parsedActualResult = EnquiryResponseResultParser.GetResult(actualResult);
                    rows = GetEnquiryRequestResult(levelStructure, parsedActualResult);
                    if (rows.Rows == null || rows.Rows.Count == 0)
                    {
                        if (actualResult.Length > 255)
                            actualResult = actualResult.Substring(0, 254) + "...";

                        string errorMsg = string.Format("Configuration rule for '{0}' enquiry do not match with any record from enquiry result: {1}", enquiryName, actualResult);
                        throw new EnquiryConfigurationException(errorMsg, EnquiryExceptionType.NoRuleMatch);
                    }
                }

                if (rows != null)
                {
                    if ((rows.Rows.Count > 0) &&
                        (rows.Rows[0].Cells.Count > 0) &&
                        (rows.Rows[0].Cells[0].Value == NO_RECORDS_STRING))
                    {
                        throw new EnquiryConfigurationException(NO_RECORDS_STRING, EnquiryExceptionType.Undefined);
                    }

                    List<TypicalAttribute> attributesWithoutID =
                        responseTypical.Attributes.FindAll(attr => attr.Name != ID_ATTRIBUTE_NAME);

                    foreach (EnquiryResponseRow row in rows.Rows)
                    {
                        for (int fieldPos = 0; fieldPos < row.Cells.Count; fieldPos++)
                        {
                            if (attributesWithoutID.Count <= fieldPos)
                                continue;

                            string fieldValue = Utils.GetHtmlEncodedValue(row.Cells[fieldPos].Value);
                            AttributeDataType dataType = attributesWithoutID[fieldPos].Type;
                            fieldValue = ValidataConverter.GetStringValueForDB(fieldValue, dataType);
                            fieldValue = SafeXmlTextWriter.GetValidXmlString(fieldValue);

                            instanceXML.Append(EncloseInXmlTag(fieldValue, headers[fieldPos], ""));
                        }

                        string attributes = string.Format(" Catalog=\"{0}\"", responseTypical.CatalogName);

                        // hack for parallel execution of OBJECT.MAP.ENQ
                        if(responseTypical != null && responseTypical.Name == "OBJECT.MAP.ENQ"
                            && filters != null && filters.Count == 1 && filters[0].AttributeName == "APP.VERSION")
                        {
                            attributes += string.Format(" t24VersionName=\"{0}\"", filters[0].TestValue);

                            if (rows.Rows.Last() == row)
                                attributes += " lastRecord=\"true\"";
                        }

                        WriteTag(instanceXML.ToString(), responseTypical.Name, attributes, axmlWriter);
                        instanceXML = new StringBuilder();
                    }
                }
            }
            catch (Exception ex)
            {
                if (logger != null)
                {
                    logger.Invoke(ex.Message);
                }
                throw;
            }

            return String.Empty;
        }

        /// <summary>
        /// Read from the response file all IDs of the 'live' records. Reads the IDs only !!!
        /// </summary>
        /// <param name="msgText">Response message </param>
        /// <returns>List of all ID's from the response message </returns>
        public List<string> ReadHistoryResponseOld(string msgText)
        {
            if (msgText.Contains(NO_RECORDS_STRING))
            {
                // no items where found
                return new List<string>(0);
            }

            List<string> results = new List<string>();
            int readPos;
            int firstCommaPos = msgText.IndexOf(",");
            if (firstCommaPos >= 0)
            {
                if ((firstCommaPos + 1) < msgText.Length)
                    if (msgText[firstCommaPos + 1] == ',')
                    {
                        firstCommaPos++;
                    }

                readPos = firstCommaPos + 1;
            }
            else
            {
                _HasError = true;
                string errorMessage = Utils.GetHtmlEncodedValue(msgText);
                results.Add(errorMessage);
                return results;
            }

            char[] msgChars = msgText.ToCharArray();
            List<string> headers = GetHeaderNames(msgChars, ref readPos);
            // if headers are not presented in the response, get number of columns
            if (headers.Count == 0)
            {
                if (msgChars[readPos] == '"')
                    headers = CountColumns(msgChars, readPos);
                else
                    headers = CountColumns(msgChars, readPos + 1);
            }
            int headerCount = headers.Count;
            int headerPos = 0;

            while (readPos < msgChars.Length)
            {
                string fieldValue = GetFieldValue(msgChars, ref readPos);

                // break the suite if we don't have more values to read
                if (fieldValue.Length == 0)
                {
                    SkipEmpty(msgChars, ref readPos);
                }

                if (headerPos == 0 && fieldValue.Trim() != String.Empty)
                {
                    results.Add(fieldValue);
                    //Create fake fake GlobusRequest(with fake instance only with @ID) --> ShortGlobusRequest 
                }

                headerPos++; /*get to the next header or if at the end of listagain to the first one*/
                if (headerPos >= headerCount)
                {
                    headerPos = 0;
                }
                SkipEmpty(msgChars, ref readPos);
            }
            return results;
        }

        /// <summary>
        /// Read from the response file all IDs of the 'live' records. Reads the IDs only !!!
        /// </summary>
        /// <param name="msgText">Response message </param>
        /// <returns>List of all ID's from the response message </returns>
        internal List<string> ReadHistoryResponse(string msgText)
        {
            if (msgText.Contains(NO_RECORDS_STRING))
            {
                // no items where found
                return new List<string>(0);
            }

            List<string> results = new List<string>();
            int readPos;
            int firstCommaPos = msgText.IndexOf(",");
            if (firstCommaPos >= 0)
            {
                if ((firstCommaPos + 1) < msgText.Length)
                    if (msgText[firstCommaPos + 1] == ',')
                    {
                        firstCommaPos++;
                    }

                readPos = firstCommaPos + 1;
            }
            else
            {
                _HasError = true;
                string errorMessage = Utils.GetHtmlEncodedValue(msgText);
                results.Add(errorMessage);
                return results;
            }

            
            readPos = msgText.IndexOf(',', readPos + 1); // skip headers
            int seekPos = readPos + 1;

            while (seekPos < msgText.Length)
            {
                int tmpPos = msgText.IndexOf(',', seekPos);
                if (tmpPos == -1)
                { // last line
                    string line = msgText.Substring(readPos);
                    string id = GetIDValue(line);
                    if (string.IsNullOrEmpty(id))
                    {
                        break;
                    }
                    results.Add(id);
                    break;
                }

                if (tmpPos >= msgText.Length - 1)
                { // last line
                    string line = msgText.Substring(readPos);
                    string id = GetIDValue(line);
                    if (string.IsNullOrEmpty(id))
                    {
                        break;
                    }
                    results.Add(id);
                    break;
                }

                if (msgText[tmpPos - 1] == '\"' && msgText[tmpPos + 1] == '\"')
                { // readPos to tmpPos is a line
                    seekPos = tmpPos + 1;
                    string line = msgText.Substring(readPos, seekPos - readPos).Trim();
                    // the ID we are interested in is in between quotas in the begining of the line
                    string id = GetIDValue(line);
                    readPos = seekPos;
                    if (string.IsNullOrEmpty(id))
                    {
                        // this is error in format
                        continue;
                    }
                    results.Add(id);
                }
                else
                {
                    seekPos = tmpPos + 1; // this comma is in middle of a text,not delimiter of lnes
                    continue;
                }

            }
            
            return results;
        }

        /// <summary>
        /// Returns first value in the line between in quotas
        /// </summary>
        /// <param name="line">input line</param>
        /// <returns>the value</returns>
        private string GetIDValue(string line)
        {
            int tmpPos = line.IndexOf('\"');
            if (tmpPos == -1)
            {
                // this is error in format
                return null;
            }
            int endId = line.IndexOf('\"', tmpPos + 1);
            if (endId == -1)
            {
                // this is error in format
                return null;
            }
            return line.Substring(tmpPos + 1, endId - tmpPos - 1);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Writes the tag.
        /// </summary>
        /// <param name="val">The val.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="attributes">The attributes.</param>
        /// <param name="axmlWriter">The axml writer.</param>
        private static void WriteTag(String val, String tag, string attributes, XmlWriter axmlWriter)
        {
            string element = EncloseInXmlTag(val, tag, attributes);
            axmlWriter.WriteRaw(element);
        }
        #endregion

        #region Overrides

        protected override OFSResponseReaderBase CreateClonedOFSResponseReader()
        {
            return new OFSResponseReader(_PhantomSet, _SpecialTypicalAttributes)
            {
                _ResponseTypical = _ResponseTypical,
                _ValidataID = _ValidataID
            };
        }

        protected override string GenerateFieldsAndValuesXml()
        {
            if (IsAAProcessDetails)
                return base.GenerateFieldsAndValuesXml();

            var multiValueShrinkStretch = MultiValueShrinkStretchCache.GetByTypical(_ResponseTypical);

            if (multiValueShrinkStretch == null || !multiValueShrinkStretch.HasMVToParse)
                return base.GenerateFieldsAndValuesXml();

            Instance instance = new Instance(_ResponseTypical.Name, _ResponseTypical.CatalogName);


            foreach (string attributeName in _FieldsAndValues.AllKeys)
            {
                string attributeValue = _FieldsAndValues[attributeName];
                instance.AddAttribute(attributeName, attributeValue);
            }

            instance = multiValueShrinkStretch.MergeMultiValues(instance);

            if (instance.FindAttribute("@ID")==null)
                instance.AddAttribute("@ID", _TransactionID);

            

            return instance.ToXmlString(true);
        }

        #endregion

    }
}
