﻿using System.Text;
using OFSStringConvertor;

namespace OFSCommonMethods
{
    internal class OFSStringBuilder
    {
        private readonly StringBuilder _StringBuilder = new StringBuilder();

        internal void AppendID(string id)
        {
            _StringBuilder.Append(GlobusRequestBase.GetEscapedID(id));
        }

        internal void Append(string textToEscape)
        {
            _StringBuilder.Append(OFSMessageConverter.ImportMsgSubstChars(textToEscape));
        }

        internal void Append(char c)
        {
            _StringBuilder.Append(c);
        }

        internal void Append(int i)
        {
            _StringBuilder.Append(i);
        }

        public override string ToString()
        {
            return _StringBuilder.ToString();
        }
    }
}
