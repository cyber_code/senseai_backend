﻿namespace OFSCommonMethods
{
    public class OFSDetailedError
    {
        private string _ErrorID;
        private string _GeneratorID;
        private string _RequestID;
        private string _RequestExeState;
        private string _RequestAfterExeState;
        private string _RequestErrorState;
        private string _GlobusID;
        private string _ErrorMessage;

        public string ErrorID
        {
            get { return _ErrorID; }
            set { _ErrorID = value; }
        }

        public string GeneratorID
        {
            get { return _GeneratorID; }
            set { _GeneratorID = value; }
        }

        public string RequestID
        {
            get { return _RequestID; }
            set { _RequestID = value; }
        }

        public string RequestExeState
        {
            get { return _RequestExeState; }
            set { _RequestExeState = value; }
        }

        public string RequestAfterExeState
        {
            get { return _RequestAfterExeState; }
            set { _RequestAfterExeState = value; }
        }

        public string RequestErrorState
        {
            get { return _RequestErrorState; }
            set { _RequestErrorState = value; }
        }

        public string GlobusID
        {
            get { return _GlobusID; }
            set { _GlobusID = value; }
        }

        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }

        public string ErrorCategory
        {
            get { return GetErrorCategory(_ErrorMessage); }
        }

        public static string GetErrorCategory(string error)
        {
            int pos = error.IndexOf(',');
            if (pos > 0)
            {
                return error.Substring(pos + 1);
            }
            else
            {
                return "";
            }
        }
    }
}