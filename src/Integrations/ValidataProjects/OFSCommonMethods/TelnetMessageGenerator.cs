using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Web;
using System.Xml;
using AXMLEngine;
using OFSCommonMethods.IO;
using OFSCommonMethods.IO.Telnet;
using System.Configuration;
using ValidataCommon;

namespace OFSCommonMethods
{
    /// <summary>
    /// Generator of OFS Messages for Telnet
    /// </summary>
    public class TelnetMessageGenerator : OFSMessageGeneratorBase
    {
        #region Private Members

        /// <summary>
        /// Telnet communicator
        /// </summary>
        private TSSTelnetComm _TelnetComm;

        /// <summary>
        /// Collection of GlobusReques objects processed by this message generator
        /// </summary>
        private GlobusRequestCollection _GlobusRequests = new GlobusRequestCollection();

        private Instance _Instance;

        private readonly bool _IsValidate;

        private ManualResetEvent _EndEvent;

        private int _MultithreadedCommunicatorIdx = -1;

        private bool _CancelExecution;

        private GlobusImportType _ImportType;

        private List<string> _EnquiryResults;

        private XmlWriter _VxmlWriter;

        private DateTime _OperationStart;

        private DateTime _OperationEnd;

        private const string _SkipInstancesError = "Load not attempted";

        /// <summary>
        /// Determine wheather to import authorised records only
        /// </summary>
        private bool _ImportAuthorisedRecordsOnly;


        private Settings _Settings;

        /// <summary>
        /// Contains list of typicals attribute names that starts with digit
        /// </summary>
        private Dictionary<string, List<string>> _SpecialTypicalAttributes;

        public Dictionary<string, string> CustomEnquiries;

        public bool SkipInstance { get; set; }

        #endregion

        #region Public Members 

        public int InstanceIndex { get; set; }

        public XmlWriter VxmlWriter
        {
            get { return _VxmlWriter; }
            set { _VxmlWriter = value; }
        }

        public GlobusRequestCollection GlobusRequests
        {
            get { return _GlobusRequests; }
            set { _GlobusRequests = value; }
        }

        public bool IsBusy { get; set; }

        public Instance Instance
        {
            get { return _Instance; }
            set { _Instance = value; }
        }

        public EventWaitHandle GoEvent { get; set; }

        public ManualResetEvent EndEvent
        {
            get { return _EndEvent; }
            set { _EndEvent = value; }
        }

        public int MultithreadedCommunicatorIdx
        {
            get { return _MultithreadedCommunicatorIdx; }
            set { _MultithreadedCommunicatorIdx = value; }
        }

        public string TelnetConnectionsGroupID { get; set; }

        public bool CancelExecution
        {
            get { return _CancelExecution; }
            set { _CancelExecution = value; }
        }

        public GlobusImportType ImportType
        {
            get { return _ImportType; }
            set { _ImportType = value; }
        }

        public List<string> EnquiryResults
        {
            get { return _EnquiryResults; }
            set { _EnquiryResults = value; }
        }

        public DateTime OperationStart
        {
            get { return _OperationStart; }
        }

        public DateTime OperationEnd
        {
            get { return _OperationEnd; }
        }

        public bool IsInitialized
        {
            get
            {
                // TODO is there a better criteria ???
                return Instance != null;
            }
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Creates an instance of the TelnetMessageGenerator class
        /// </summary>
        public TelnetMessageGenerator(MessageQueueManager messageQueue,
                                      AXMLEngine.AXMLEngine engine,
                                      Settings settings,
                                      OFSParameters ofsParam,
                                      TSSTelnetComm telnetComm,
                                      bool alwaysUseAuthentication,
                                      bool isValidate)
        {
            _Settings = settings;
            _PhantomSet = settings.PhantomSet;
            _CommandUser = settings.InputUser;
            _CommandPassword = settings.InputPassword;
            _AuthorizeUser = settings.AuthorizationUser;
            _AuthorizePassword = settings.AuthorizationPassword;
            _TreatOFSResponseSingleValueAsError = settings.TreatOFSResponseSingleValueAsError;
            _OfsType = settings.OfsType;
            _OFSUseOldHistoryReadResponse = false;
            bool.TryParse(settings.OFSUseOldHistoryReadResponse, out _OFSUseOldHistoryReadResponse);
            _MessageQueue = messageQueue;
            _Filters = engine.DefaultRequest.Filters.FiltersCollection;
            _OFSParameters = ofsParam;
            _TelnetComm = telnetComm;
            _AlwaysUseAuthentication = alwaysUseAuthentication;
            _IsValidate = isValidate;
            _GUID = Guid.NewGuid().ToString();

            _OFSParameters.AdapterIdentity.Impersonate();
            _SpecialTypicalAttributes = settings.SpecialTypicalAttibutes;

            //organize typicals in catalogs
            BuildTypicalCatalogs(engine.DefaultRequest.RequestMetadata.Typicals);

            _ImportAuthorisedRecordsOnly = true;
        }

        #endregion

        #region Public methods 

        public void DoEnquiry()
        {
            GoEvent.WaitOne();

            _OperationStart = DateTime.Now;
            try
            {
                if (_CancelExecution)
                {
                    return;
                }

                DoEnquiryRequest();
            }
            catch (Exception ex)
            {
                _OFSParameters.ResultInstances = "";
                _OFSParameters.ResultErrors = "<error>" + Utils.GetHtmlEncodedValue(ex.Message) + "</error>";

                if (_ImportType == GlobusImportType.MULTI_ENQ && _VxmlWriter != null)
                {
                    var data = string.Format("<AppError catalog=\"NavigatorCatalog\" t24VersionName=\"{0}\" lastRecord=\"true\" >"
                        + "<Error_x0020_Message>{1}</Error_x0020_Message></AppError>"
                        , _Filters[0].TestValue
                        , Utils.GetHtmlEncodedValue(ex.Message));

                    _VxmlWriter.WriteRaw(data);
                }

                if ((ex is TelnetCommException) && (ex as TelnetCommException).Type == TelnetCommException.ExceptionType.TimeOut
                    && !_TelnetComm.IsConnected
                    && _ImportType == GlobusImportType.MULTI_ENQ)
                {
                    // reconnect the connector 

                    // TelnetCommPool.Release(_TelnetComm);
                    _TelnetComm = TelnetCommPool.GetOrCreateCommunicatorForPerformance(this.MultithreadedCommunicatorIdx
                        , this._Settings, this.TelnetConnectionsGroupID);
                }
            }
            finally
            {
                IsBusy = false;
                _OperationEnd = DateTime.Now;
                _EndEvent.Set();
            }
        }

        private void DoEnquiryRequest()
        {
            bool typicalMetadataQueryMode = GlobusMetadataReader.IsExpectedTypical(_OFSParameters.RequestTypical);

            Instance instance = null;
            DataProcessType dataProcessType = DataProcessType.None;
            GlobusRequest.RequestType reqType = GlobusRequest.RequestType.Enquiry;
            if (_ImportType != GlobusImportType.TESTDATA && _ImportType != GlobusImportType.MULTI_ENQ)
            {
                reqType = GlobusRequest.RequestType.History;
            }
            else if (typicalMetadataQueryMode) //HACK this is not really an enquiry
            {
                reqType = GlobusRequest.RequestType.DataProcess;
                dataProcessType = GlobusRequest.GetDataProcessType(_OFSParameters.Configuration);
                if (_OFSParameters.Instances != null && _OFSParameters.Instances.Count > 0)
                {
                    instance = _OFSParameters.Instances[0];
                }
            }

            _GlobusRequests = new GlobusRequestCollection();

            GlobusRequest request = new GlobusRequest(_OFSParameters.RequestTypical,
                                                      instance,
                                                      reqType,
                                                      dataProcessType,
                                                      _OFSParameters.Version,
                                                      _OFSParameters.Company, 
                                                      _PhantomSet);

            request.ResponseTypical = _OFSParameters.ResponseTypical;
            request.SpecialTypicalAttributes = _SpecialTypicalAttributes;
            request.CustomEnquiries = CustomEnquiries;

            _GlobusRequests.Add(request);

            bool isImpTestData = (_ImportType == GlobusImportType.TESTDATA);

            CreateAndWriteCommand(_OFSParameters.Configuration, _IsValidate, isImpTestData);
        }

        public void DoProduction()
        {
            GoEvent.WaitOne();

            _OperationStart = DateTime.Now;
            try
            {
                if (_CancelExecution)
                {
                    return;
                }

                DoProductionRequest();
            }
            catch (Exception ex)
            {
                _OFSParameters.ResultInstances = "";
                _OFSParameters.ResultErrors = "<error>" + Utils.GetHtmlEncodedValue(ex.Message) + "</error>";
            }
            finally
            {
                IsBusy = false;
                _OperationEnd = DateTime.Now;
                _EndEvent.Set();
            }
        }

        private bool DoProductionRequest()
        {
            //Globus Request should be only one
            _GlobusRequests = new GlobusRequestCollection();

            if (_Instance != null)
            {
                GlobusRequest tempRequest = new GlobusRequest(
                        _OFSParameters.ResponseTypical,
                        _Instance,
                        GlobusRequest.RequestType.DataProcess,
                        DataProcessType.Select,
                        _OFSParameters.Version,
                        _OFSParameters.Company
                    );

                tempRequest.CustomEnquiries = CustomEnquiries;
                tempRequest.SpecialTypicalAttributes = _SpecialTypicalAttributes;
                _GlobusRequests.Add(tempRequest);

                bool isImpTestData = (_ImportType == GlobusImportType.TESTDATA);

                CreateAndWriteCommand(_OFSParameters.Configuration, _IsValidate, isImpTestData);

                GlobusRequest lastRequest = _GlobusRequests[0] as GlobusRequest;

                if (!lastRequest.Failed)
                {
                    if (lastRequest.State == GlobusRequest.ExecutionState.Processed)
                    {
                        lastRequest.State = GlobusRequest.ExecutionState.Finished;

                        if (_ImportType != GlobusImportType.HIS && _ImportAuthorisedRecordsOnly)
                        {
                            Instance firstInst = Instance.FromXmlString(lastRequest.Result);
                            InstanceAttribute recordStatus = firstInst.GetAttributeByShortName("RECORD.STATUS");

                            if (recordStatus != null)
                            {
                                DoProductionRecursive(lastRequest);
                            }
                        }
                        else
                        {
                            int numberOfVersions;
                            if (int.TryParse(lastRequest.CURRNO, out numberOfVersions) && numberOfVersions > 1)
                            {
                                DoHistory(numberOfVersions);
                            }
                        }
                    }
                    else
                    {
                        Debug.Fail("Should not happend");
                    }
                }
                else
                {
                    //TODO: Check error should be added to GlobusRequest
                    if (_ImportAuthorisedRecordsOnly)
                    {
                        if (!String.IsNullOrEmpty(lastRequest.Result) && lastRequest.Result.Contains("HISTORY RECORD MISSING"))
                        {
                            DoProductionRecursive(lastRequest);
                        }
                    }
                }
            }
            else
            {
                // if _Instance is not initialized, get the EnquiryResults by calling DoEnquiry instead
                DoEnquiryRequest();
            }

            return true;
        }

        /// <summary>
        /// Decrement CURR.NO and do production recursive in order to reach record version that is authorized
        /// </summary>
        /// <param name="lastRequest">Latest T24 request</param>
        private void DoProductionRecursive(GlobusRequest lastRequest)
        {
            int currNo;
            if (int.TryParse(lastRequest.CURRNO, out currNo))
            {
                if (currNo > 1)
                {
                    string ID = string.Format("{0};{1}", lastRequest.TransactionID, currNo - 1);
                    _Instance = OFSMessageGeneratorBase.CreateFakeInstance(ID);
                    DoProductionRequest();
                }
                else
                {
                    _GlobusRequests.Clear();
                }
            }
        }

        private void DoHistory(int numberOfVersions)
        {
            for (int j = 1; j < numberOfVersions; j++)
            {
                string ID = string.Format("{0};{1}", ((GlobusRequest) _GlobusRequests[0]).TransactionID, j);
                Instance inst = OFSMessageGeneratorBase.CreateFakeInstance(ID);

                GlobusRequest tempRequest = new GlobusRequest(
                    _OFSParameters.ResponseTypical,
                    inst,
                    GlobusRequest.RequestType.DataProcess,
                    DataProcessType.Select,
                    _OFSParameters.Version,
                    _OFSParameters.Company);

                tempRequest.SpecialTypicalAttributes = _SpecialTypicalAttributes;
                _GlobusRequests.Add(tempRequest);

                bool isImpTestData = (_ImportType == GlobusImportType.TESTDATA);

                CreateAndWriteCommand(_OFSParameters.Configuration, _IsValidate, isImpTestData);

                int requestNumber = _GlobusRequests.Count - 1;

                GlobusRequest lastRequest = (GlobusRequest)_GlobusRequests[requestNumber];

                if (!lastRequest.Failed)
                {
                    if (lastRequest.State == GlobusRequest.ExecutionState.Processed)
                    {
                        lastRequest.State = GlobusRequest.ExecutionState.Finished;
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(lastRequest.Result) && lastRequest.Result.Contains("HISTORY RECORD MISSING"))
                    {
                        lastRequest.IsWarning = true;
                    }
                }
            }
        }

        /// <summary>
        /// This is the main working thread routine that calls the real DoOfsRequest routine.
        /// The real DoOFS routine organizes the creation of OFS messages,  sends them to Globus and processes OFS responses
        /// </summary>
        public void DoOFS()
        {
            GoEvent.WaitOne();

            _OperationStart = DateTime.Now;
            try
            {
                if (_CancelExecution)
                {
                    return;
                }

                InstanceCollection instanceCollection = new InstanceCollection() { _Instance };

        
                DoOfsRequest(instanceCollection,
                             _OFSParameters.RequestTypical,
                             _OFSParameters.ResponseTypical,
                             _OFSParameters.Configuration,
                             _OFSParameters.Version,
                             _OFSParameters.Company);
                string instances, errors, firstMessage, result;
                bool writeInstances;
                GetResult(out instances, out errors, out writeInstances, out firstMessage, out result);

                _OFSParameters.ResultInstances = instances;
                _OFSParameters.ResultErrors = errors;
            }
            catch (Exception ex)
            {
                _OFSParameters.ResultInstances = "";
                _OFSParameters.ResultErrors = "<error>" + Utils.GetHtmlEncodedValue(ex.Message) + "</error>";
            }
            finally
            {
                IsBusy = false;
                _OperationEnd = DateTime.Now;
                _EndEvent.Set();
            }
        }

        /// <summary>
        /// Loads all instances and generates input files for GLOBUS
        /// </summary>
        /// <param name="axmlInstances"> AXML instances collection </param>
        /// <param name="requestTypical">The request type</param>
        /// <param name="responseTypical">The response</param>
        /// <param name="configuration">The configuration specified in the AXML request.</param>
        /// <param name="version">The version specified in the AXML request.</param>
        /// <param name="company">The name of the company if any.</param>
        private void DoOfsRequest(InstanceCollection axmlInstances,
                          MetadataTypical requestTypical,
                          MetadataTypical responseTypical,
                          string configuration,
                          string version,
                          string company)
        {
            //Create the thread event that controls the execution of this thread
            //Later the threads that read Globus output will wake this thread by setting this event

            _GUID = IOManager.RegisterThread();

            if (axmlInstances.Count > 0)
            {
                //TODO: There is only one Globus Request
                bool needAuthorization;
                _GlobusRequests = GlobusRequest.CreateGlobusRequests(axmlInstances,
                                                                     requestTypical,
                                                                     responseTypical,
                                                                     configuration,
                                                                     version,
                                                                     company,
                                                                     _Catalogs,
                                                                     out needAuthorization);

                if (!SkipInstance)
                {
                    if (configuration == "A") // only Authorization is needed
                    {
                        //In this case only authorization must be carried out
                        CreateAndWriteCommand(configuration, _IsValidate, true);
                        _GlobusRequests.SetResultsForNotPassedMessages();
                    }
                    else
                    {
                        //creates the OFS messages and sends them to Globus
                        CreateAndWriteCommand(configuration, _IsValidate, true);
                        _GlobusRequests.SetResultsForNotPassedMessages();

                        if (needAuthorization)
                        {
                            CreateAndWriteCommand(configuration, _IsValidate, true);
                            _GlobusRequests.SetResultsForNotPassedMessages();
                        }
                    }
                }
                else
                {
                    CreateAndJustLogCommand(configuration, _IsValidate, true);
                    _GlobusRequests.SetResultsForFailed(_SkipInstancesError);
                }
            }
        }


        /// <summary>
        /// Creates and saves the input files in the Globus input directory (IN)
        /// </summary>
        private void CreateAndJustLogCommand(string configuration, bool isValidate, bool isImpTestData)
        {
            bool waitingForAuthorization = false;
            //There is only one GlobusRequest for Telnet execution
            foreach (GlobusRequest request in _GlobusRequests)
            {
                if (!request.IsFinished)
                {
                    if ((configuration == "A")
                        || (request.NeedsAuthorize && request.ReadyForAuthorization))
                    {
                        waitingForAuthorization = true;
                    }

                    string firstCommand = request.GenerateMessageText(
                                _CommandUser,
                                _CommandPassword,
                                _AuthorizeUser,
                                _AuthorizePassword,
                                _Filters,
                                waitingForAuthorization,
                                isValidate,
                                isImpTestData,
                                _AlwaysUseAuthentication,
                                _OfsType);

                    GlobusMessage inputT24Message = new GlobusMessage("");
                    inputT24Message.Message.Text = firstCommand;

                    if (request.TypeOfRequest == GlobusRequest.RequestType.AAProduct &&
                        request.ProcessingType == DataProcessType.Select)
                    {
                        UpdateMultipartAAMessage(request, inputT24Message);
                        if (request.Failed)
                        {
                            // Finalize
                            if (waitingForAuthorization)
                            {
                                request.WaitingForAuthorization = true;
                            }
                            else
                            {
                                request.StartWaitingForProcessing();
                            }

                            _GlobusRequests.RegisterAsBeingProcessed(request);
                            continue;
                        }
                    }

                    GlobusMessage outputT24Message = new GlobusMessage("");

                    if (waitingForAuthorization)
                    {
                        request.WaitingForAuthorization = true;
                    }
                    else
                    {
                        request.StartWaitingForProcessing();
                    }

                    _GlobusRequests.RegisterAsBeingProcessed(request);

                    var enqResultTemplate = (request.EnquiryLevelStructure != null) ?
                        new EnquiryResultParsingRotine(request.EnquiryLevelStructure) :
                        GetEnquiryResultParsingTemplate();

                    //Enquiry results are needed only for production data, XML Writer should be null for Exports and Production(History) Imports be null if
                    outputT24Message.Message.Text = request.FirstMessage;
                    _EnquiryResults = request.SetResponse(
                            outputT24Message.Message,
                            request.ResponseTypical,
                            _VxmlWriter,
                            null,
                            _TreatOFSResponseSingleValueAsError,
                            enqResultTemplate,
                            _OfsType
                        );
                 
                }
            }
        }

        /// <summary>
        /// Creates and saves the input files in the Globus input directory (IN)
        /// </summary>
        private void CreateAndWriteCommand(string configuration, bool isValidate, bool isImpTestData)
        {
            bool waitingForAuthorization = false;
            //There is only one GlobusRequest for Telnet execution
            foreach (GlobusRequest request in _GlobusRequests)
            {
                if (!request.IsFinished)
                {
                    if ((configuration == "A")
                        || (request.NeedsAuthorize && request.ReadyForAuthorization))
                    {
                        waitingForAuthorization = true;
                    }

                    string firstCommand = request.GenerateMessageText(
                                _CommandUser,
                                _CommandPassword,
                                _AuthorizeUser,
                                _AuthorizePassword,
                                _Filters,
                                waitingForAuthorization,
                                isValidate,
                                isImpTestData,
                                _AlwaysUseAuthentication,
                                _OfsType);

                    GlobusMessage inputT24Message = new GlobusMessage("");
                    inputT24Message.Message.Text = firstCommand;

                    if (request.TypeOfRequest == GlobusRequest.RequestType.AAProduct &&
                        request.ProcessingType == DataProcessType.Select)
                    {
                        UpdateMultipartAAMessage(request, inputT24Message);
                        if (request.Failed)
                        {
                            // Finalize
                            if (waitingForAuthorization)
                            {
                                request.WaitingForAuthorization = true;
                            }
                            else
                            {
                                request.StartWaitingForProcessing();
                            }

                            _GlobusRequests.RegisterAsBeingProcessed(request);
                            continue;
                        }
                    }

                    GlobusMessage outputT24Message = new GlobusMessage("");

                    try
                    {
                        _MessageQueue.ProcessMessage_Bulk(_GUID, inputT24Message, outputT24Message, _TelnetComm);
                    }
                    catch (TelnetCommException ex)
                    {
                        request.SetErrorResponse(ex);
                        throw;
                    }

                    if (waitingForAuthorization)
                    {
                        request.WaitingForAuthorization = true;
                    }
                    else
                    {
                        request.StartWaitingForProcessing();
                    }

                    _GlobusRequests.RegisterAsBeingProcessed(request);

                    var enqResultTemplate = (request.EnquiryLevelStructure != null) ?
                        new EnquiryResultParsingRotine(request.EnquiryLevelStructure) :
                        GetEnquiryResultParsingTemplate();

                    //Enquiry results are needed only for production data, XML Writer should be null for Exports and Production(History) Imports be null if
                    _EnquiryResults = request.SetResponse(
                            outputT24Message.Message,
                            request.ResponseTypical,
                            _VxmlWriter,
                            _TelnetComm.Log,
                            _TreatOFSResponseSingleValueAsError,
                            enqResultTemplate,
                            _OfsType,
                            _OFSUseOldHistoryReadResponse,
                            _Filters);
                }
            }
        }

        private void UpdateMultipartAAMessage(GlobusRequest request, GlobusMessage resultMessage)
        {
            base.UpdateMultipartAAMessage(request, resultMessage, _TelnetComm, _VxmlWriter);
        }

        public void ClearData()
        {
            foreach (GlobusRequest request in _GlobusRequests)
            {
                if (request.Instance != null)
                {
                    request.Instance = null;
                }

                if (!String.IsNullOrEmpty(request.Result))
                {
                    request.Result = String.Empty;
                }

                if (!String.IsNullOrEmpty(request.FirstMessage))
                {
                    request.FirstMessage = String.Empty;
                }
            }

            if (_Instance != null)
            {
                _Instance = null;
            }

            if (_EnquiryResults != null)
            {
                _EnquiryResults = new List<string>();
            }
        }


        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <param name="instances">The instances.</param>
        /// <param name="errors">The errors.</param>
        /// <param name="writeInstances">if set to <c>true</c> [write instances].</param>
        /// <param name="firstMessage">The first message.</param>
        /// <param name="result">The result.</param>
        public void GetResult(out string instances, out string errors, out bool writeInstances, out string firstMessage, out string result)
        {
            firstMessage = String.Empty;
            result = String.Empty;
            writeInstances = false;

            StringBuilder sbInst = new StringBuilder();
            StringBuilder sbError = new StringBuilder();

            //TODO: There should be only one GlobusRequest
            foreach (GlobusRequest request in _GlobusRequests)
            {
                if (OFSMessageGenerator.IsForVXMLWriting(request))
                {
                    writeInstances = true;
                }

                if (!String.IsNullOrEmpty(request.FirstMessage))
                {
                    firstMessage += request.FirstMessage;
                }

                if (!String.IsNullOrEmpty(request.Result))
                {
                    result += request.Result;
                }

                if (request.Failed)
                {
                    if (request.IsWarning)
                        sbError.Append("<error type=\"Warning\">");
                    else
                        sbError.Append("<error>");

                    string globusId = (request.Instance != null) ? request.Instance.GetAttributeValue("@ID") : "None";

                    // @ID if present
                    sbError.Append("@ID=[");
                    sbError.Append(Utils.GetHtmlEncodedValue(globusId));
                    sbError.Append("] ");

                    // Error message (it is already HTML encoded)
                    sbError.Append("Error=[");
                    if (!SkipInstance)
                    {
                        sbError.Append(request.Result.Replace("\n", String.Empty));
                    }
                    else
                    {
                        sbError.Append(_SkipInstancesError);
                    }
                    sbError.Append("] ");

                    // OFS Message
                    sbError.Append("OFS=[");
                    sbError.Append(Utils.GetHtmlEncodedValue(request.FirstMessage));
                    sbError.Append("]");

                    sbError.Append("</error>");
                }
                else
                {
                    sbInst.Append(request.Result);
                }
            }

            //errors and instances are not enclosed in root tags
            instances = sbInst.ToString();
            errors = sbError.ToString();
        }

        #endregion
    }
}
