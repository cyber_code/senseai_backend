﻿using System.Collections.Generic;
using System.Diagnostics;
using AXMLEngine;
using ValidataCommon;

namespace OFSCommonMethods
{
    public class GlobusTypeMetadata
    {
        public string Name;

        public GlobusAttributeMetadataCollection Attributes = new GlobusAttributeMetadataCollection();

        public static GlobusTypeMetadata FromInstance(Instance instance)
        {
            GlobusTypeMetadata result = new GlobusTypeMetadata();
            result.Name = instance.GetAttributeValue("@ID");

            // process and append sys fields
            result.Attributes.AddRange(ProcessAttributes(instance, false));

            // process and append user fields
            result.Attributes.AddRange(ProcessAttributes(instance, true));

            return result;
        }

        private static IEnumerable<GlobusAttributeMetadata> ProcessAttributes(Instance instance, bool isUserDefined)
        {
            string fieldNamePrefix = isUserDefined ? "USR." : "SYS.";

            Dictionary<string, GlobusAttributeMetadata> attributeMetadatasByName = 
                new Dictionary<string, GlobusAttributeMetadata>();

            List<InstanceAttribute> atttibuteNames = instance.GetMultivalueAttributesByShortFieldName(
                    fieldNamePrefix + "FIELD.NAME"
                );

            foreach (InstanceAttribute attrName in atttibuteNames)
            {
                if(attributeMetadatasByName.ContainsKey(attrName.Value))
                {
                    Debug.WriteLine("Duplicate attribute name " + attrName.Value);
                    continue;
                }

                string fieldNameSuffix = attrName.Name.Substring((fieldNamePrefix + "FIELD.NAME").Length);

                GlobusAttributeMetadata attributeMetadata = new GlobusAttributeMetadata
                {
                    IsUserDefined = isUserDefined,
                    FieldName = attrName.Value,
                    Type = GetInstanceAttributeValue(instance, fieldNamePrefix, "TYPE", fieldNameSuffix, ""),
                    FieldNo = GetInstanceAttributeValue(instance, fieldNamePrefix, "FIELD.NO", fieldNameSuffix, ""),
                    ValProg = GetInstanceAttributeValue(instance, fieldNamePrefix, "VAL.PROG", fieldNameSuffix, ""),
                    Conversion = GetInstanceAttributeValue(instance, fieldNamePrefix, "CONVERSION", fieldNameSuffix, ""),
                    DisplayFmt = GetInstanceAttributeValue(instance, fieldNamePrefix, "DISPLAY.FMT", fieldNameSuffix, ""),
                    AltIndex = GetInstanceAttributeValue(instance, fieldNamePrefix, "ALT.INDEX", fieldNameSuffix, ""),
                    IdxFile = GetInstanceAttributeValue(instance, fieldNamePrefix, "IDX.FILE", fieldNameSuffix, ""),
                    IndexNulls = GetInstanceAttributeValue(instance, fieldNamePrefix, "INDEX.NULLS", fieldNameSuffix, ""),
                    SingleMult = GetInstanceAttributeValue(instance, fieldNamePrefix, "SINGLE.MULT", fieldNameSuffix, ""),
                    LangField = GetInstanceAttributeValue(instance, fieldNamePrefix, "LANG.FIELD", fieldNameSuffix, ""),
                    Generated = GetInstanceAttributeValue(instance, fieldNamePrefix, "GENERATED", fieldNameSuffix, ""),
                    CnvType = GetInstanceAttributeValue(instance, fieldNamePrefix, "CNV.TYPE", fieldNameSuffix, ""),
                    RelFile = GetInstanceAttributeValue(instance, fieldNamePrefix, "REL.FILE", fieldNameSuffix, ""),
                    DisplayType = GetInstanceAttributeValue(instance, fieldNamePrefix, "DISPLAY.TYPE", fieldNameSuffix, "")
                };

                attributeMetadatasByName[attributeMetadata.FieldName] = attributeMetadata;
            }

            return attributeMetadatasByName.Values;
        }

        private static string GetInstanceAttributeValue(Instance instance, string prefix, string fieldName, string suffix, string defaultValue)
        {
            string fullFieldName = string.Format("{0}{1}{2}", prefix, fieldName, suffix);
            InstanceAttribute attribute = instance.AttributeByName(fullFieldName);
            if (attribute == null || attribute.Value == null)
            {
                Debug.WriteLine("Missing field: " + fullFieldName);
                return defaultValue;
            }

            return attribute.Value;
        }

        public static VersionDefinition ConvertToVersionDefinition(GlobusTypeMetadata t24Metadata)
        {
            VersionDefinition result = new VersionDefinition();
            result.InitializeFromFullVersionName(t24Metadata.Name);

            // append fields
            result.AddFieldsRange(GetFieldInfos(t24Metadata));

            return result;
        }

        /// <summary>
        /// Generates display text for T24 Field
        /// </summary>
        /// <param name="fieldName">Field Name</param>
        /// <returns>Generated display text</returns>
        public static string GenerateT24FieldDisplayName(string fieldName)
        {
            if (fieldName == "@ID")
            {
                // Don't modify @ID
                return fieldName;
            }

            char[] result = new char[fieldName.Length];

            bool prevCharIsDelimiter = true;
            for (int i = 0; i < fieldName.Length; i++)
            {
                if (fieldName[i] == '.')
                {
                    result[i] = ' ';
                    prevCharIsDelimiter = true;
                }
                else
                {
                    result[i] = prevCharIsDelimiter ?
                        char.ToUpper(fieldName[i]) :
                        char.ToLower(fieldName[i]);

                    prevCharIsDelimiter = false;
                }
            }

            return new string(result);
        }

        public static List<FieldInfo> GetFieldInfos(GlobusTypeMetadata t24Metadata)
        {
            List<FieldInfo> result = new List<FieldInfo>();

            foreach(var attribute in t24Metadata.Attributes)
            {
                var fieldInfo = new FieldInfo
                {
                    FieldName = attribute.FieldName,
                    FieldNo = attribute.FieldNo,
                    DisplayName = GenerateT24FieldDisplayName(attribute.FieldName),
                    HasMultiValues = attribute.IsMultiValue,
                    IsMandatory = false,     // no information about 'attribute.IsMandatory'
                    IsSynchronizable = IsSynchronizable(attribute.ValProg), // true, // no information about 'attribute.IsSyncronizable'
                    ValProg = attribute.ValProg,
                    DisplayFmt = attribute.DisplayFmt,
                    SingleMult = attribute.SingleMult,
                    LangField = attribute.LangField,
                    RelFile = attribute.RelFile,
                    DropDown = attribute.DropDown,
                    DisplayType = attribute.DisplayType
                };

                result.Add(fieldInfo);
            }

            return result;
        }

        private static bool IsSynchronizable(string valProgValue)
        {
            string[] synchronizableCheckValues = new string[] { "NOINPUT", "EXTERN" };
            
            for (int i = 0; i < synchronizableCheckValues.Length; i++)
            {
                if (valProgValue.Contains(synchronizableCheckValues[i]))
                {
                    return false;
                }
            }

            return true;
        }

        private static void AppendFields(VersionDefinition result, IEnumerable<FieldInfo> fieldInfos)
        {
            foreach (FieldInfo fi in fieldInfos)
            {
                Debug.Assert(fi.FieldName != "*");

                if (!result.Fields.Contains(fi.FieldName))
                {
                    result.Fields.Add(fi);
                }
            }
        }
    }
}