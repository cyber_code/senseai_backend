﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using AXMLEngine;

namespace OFSCommonMethods
{
    public static class OFSDetailedErrorReader
    {
        public static IEnumerable<OFSDetailedError> EnumerateAllItems(string file)
        {
            if(! File.Exists(file))
            {
                yield break;
            }

            using (XmlReader xmlReaderTempErrors = XmlReader.Create(file, XmlFragmentLogHelper.GetSettings()))
            {
                if (xmlReaderTempErrors.Read())
                {
                    while (!xmlReaderTempErrors.EOF)
                    {
                        xmlReaderTempErrors.MoveToContent();
                        OFSDetailedError error = ReadError(xmlReaderTempErrors);
                        yield return error;
                    }
                }
            }
        }

        private static OFSDetailedError ReadError(XmlReader reader)
        {
            OFSDetailedError error = new OFSDetailedError();

            error.ErrorID = reader.GetAttribute(AXMLConstants.ERRORSET_ATTRIBUTE_ID);
            error.GeneratorID = reader.GetAttribute(AXMLConstants.GENERATOR_ID_TAG);
            error.RequestID = reader.GetAttribute(AXMLConstants.REQUEST_ATTRIBUTE_ID);
            error.RequestExeState = reader.GetAttribute(AXMLConstants.REQUEST_EXECUTION_STATE);
            error.RequestAfterExeState = reader.GetAttribute(AXMLConstants.REQUEST_AFTER_EXECUTION_STATE);
            error.RequestErrorState = reader.GetAttribute(AXMLConstants.REQUEST_ERROR_STATE);
            error.GlobusID = reader.GetAttribute(AXMLConstants.OFS_GLOBUS_ID);

            error.ErrorMessage = reader.ReadInnerXml();

            return error;
        }
    }
}