using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Xml;
using AXMLEngine;
using System.Configuration;

namespace OFSStringConvertor
{
    /// <summary>
    /// 	Types of encoding used to read Globus Adapter messages
    /// </summary>
    public enum GlobusEncoding
    {
        ASCII,
        UTF8,
        UTF16,
        UTF32
    }

    /// <summary>
    /// Escapes outgoing OFS requests and incomming OFS responses, by applying configurable rules
    /// </summary>
    public static class OFSMessageConverter
    {
        #region Private Members

        /// <summary>
        /// 	Import Mappings
        /// </summary>
        private static Dictionary<string, string> _ImportMappings;

        /// <summary>
        /// 	Export Mappings
        /// </summary>
        private static Dictionary<string, string> _ExportMappings;

        private static bool _ImportBalanceDoubleQuotesInAttributeValue;

        #endregion

        #region Protected Methods

        /// <summary>
        /// 	Loads Rules From File
        /// </summary>
        private static void LoadRules(string xmlFileName)
        {
            // These Has To Be Created Only Once During Adapter Life
            if (_ImportMappings != null)
            {
                Debug.Assert(_ExportMappings != null);
                return;
            }

            try
            {
                _ImportMappings = new Dictionary<string, string>();
                _ExportMappings = new Dictionary<string, string>();

                XmlDocument doc = new XmlDocument();
                doc.Load(xmlFileName);

                XmlNode import = doc.SelectSingleNode("/ReplacementRules/Import");

                if (import != null && import.Attributes != null && import.Attributes["balanceDoubleQuotesInAttributeValue"] != null)
                {
                    bool.TryParse(import.Attributes["balanceDoubleQuotesInAttributeValue"].Value,
                                  out _ImportBalanceDoubleQuotesInAttributeValue);
                }

                XmlNodeList nodesImport = doc.SelectNodes("/ReplacementRules/Import/Rule");
                LoadMappings(_ImportMappings, nodesImport);

                XmlNodeList nodesExport = doc.SelectNodes("/ReplacementRules/Export/Rule");
                LoadMappings(_ExportMappings, nodesExport);
            }
            catch
            {
                // do nothing - no rules
            }
        }

        /// <summary>
        /// 	Loads Concrete Operation Mappings
        /// </summary>
        /// <param name="mappings"> The Mapping </param>
        /// <param name="nodes"> Xml Nodes </param>
        private static void LoadMappings(Dictionary<string, string> mappings, XmlNodeList nodes)
        {
            foreach (XmlNode node in nodes)
            {
                string original = null;
                string substitute = null;

                foreach (XmlNode cn in node.ChildNodes)
                {
                    if (cn.Name == "Original")
                    {
                        original = cn.InnerText;
                    }
                    else if (cn.Name == "Substitute")
                    {
                        substitute = cn.InnerText;
                    }
                }

                if (string.IsNullOrEmpty(original)) 
                    continue;
                
                if (mappings.ContainsKey(original))
                {
                    throw new Exception(string.Format("Duplicate key '{0}' in the OFSCharactersReplacementRules.xml file", original));
                }

                mappings.Add(original, substitute);
            }
        }

        #endregion

        #region Public Methods

        public static readonly string DefaultPath = String.IsNullOrEmpty(ValidataCommon.AdapterConfiguration.AdapterFilesRootPath)
            ? Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\Assemblies\OFSCharactersReplacementRules.xml" :
            Environment.ExpandEnvironmentVariables(ValidataCommon.AdapterConfiguration.AdapterFilesRootPath) +  @"\Assemblies\OFSCharactersReplacementRules.xml";

        public static void MakeSureReplaceRulesAreLoaded()
        {
            if (_ImportMappings == null)
            {
                LoadRules(DefaultPath);
            }
        }

        /// <summary>
        /// 	Modifies the field values that will go in the OFS message, in order to escape reserved T24 symbols. 
        ///   Currently we know that ',' should be escaped as '?', but we have this configurable in a file
        ///   See http://www.t24all.com/discussions/31-t24-interfaces/9895-reserved-characters-in-ofs-message.html and Natasha e-mail from 14 August 2012 2:47 PM
        /// </summary>
        /// <param name="msg"> </param>
        /// <param name="convertValue"> </param>
        /// <returns> </returns>
        public static string ImportMsgSubstChars(string msg, bool convertValue = false)
        {
            MakeSureReplaceRulesAreLoaded();

            Debug.Assert(_ImportMappings != null, "Call 'Initialize' method before using me!");

            foreach (string s in _ImportMappings.Keys)
            {
                msg = msg.Replace(s, _ImportMappings[s]);
            }

            if (convertValue && _ImportBalanceDoubleQuotesInAttributeValue)
            {
                int count = 0;
                int lastPosition = 0;

                for (int i = 0; i < msg.Length; i++ )
                {
                    if(msg[i] == '"')
                    {
                        count++;
                        lastPosition = i;
                    }
                }

                if(count != 0)
                {
                    if(count % 2 != 0 )
                    {
                        var s = new StringBuilder(msg);
                        s[lastPosition] = '\'';
                        msg = s.ToString();
                    }
                }
            }

            return msg;
        }

        /// <summary>
        /// 	Modifies the field values that are read from the OFS message, in order to restore unusable T24 symbols. 
        ///    Currently we are sure e.g. that '?' actually means ',', but we have this configurable in a file.
        /// </summary>
        /// <param name="msg"> </param>
        /// <returns> </returns>
        public static string ExportMsgSubstChars(string msg)
        {
            MakeSureReplaceRulesAreLoaded();

            Debug.Assert(_ExportMappings != null, "Call 'Initialize' method before using me!");

            foreach (string s in _ExportMappings.Keys)
            {
                msg = msg.Replace(s, _ExportMappings[s]);
            }

            return msg;
        }

        public static Instance ConvertInstanceForHAMode(Instance instance)
        {
            Instance result = new Instance(instance.TypicalName, instance.Catalog);
            result.AddAttribute("ID", instance.GetAttributeValue("ID"));
            result.AddAttribute("@ID", instance.GetAttributeValue("@ID"));

            return result;
        }

        #endregion
    }
}