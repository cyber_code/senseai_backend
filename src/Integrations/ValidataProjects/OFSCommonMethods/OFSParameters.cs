﻿using System;
using System.Collections.Generic;
using System.Text;
using AXMLEngine;
using System.Threading;
using System.Security.Principal;

namespace OFSCommonMethods
{
    /// <summary>
    /// Contains the parameters for one Globus task
    /// </summary>
    public class OFSParameters
    {
        #region Private Members

        private InstanceCollection _Instances;

        private MetadataTypical _RequestTypical;

        private MetadataTypical _ResponseTypical;

        private string _Configuration;

        private string _Version;

        private ManualResetEvent _FinishEvent;

        private ManualResetEvent _StartEvent;

        private string _ResultInstances;

        private string _ResultErrors;

        private WindowsIdentity _AdapterIdentity;

        private int _DatasetID;

        private string _Company;

        private LevelStructureHierarchy _EnquiryLevelStructure;

        #endregion

        #region Class Lifecycle

        public OFSParameters(AXMLEngine.InstanceCollection instances, MetadataTypical requestTypical,
                             MetadataTypical responseTypical, string configuration, string version,
                             WindowsIdentity adapterIdentity, int datasetID, string company, LevelStructureHierarchy enquiryLevelStructure)
        {
            Instances = instances;
            _RequestTypical = requestTypical;
            ResponseTypical = responseTypical;
            Configuration = ((configuration == "No Value") ? string.Empty : configuration);
            Version = version;
            AdapterIdentity = adapterIdentity;
            DatasetID = datasetID;
            Company = company;

            _EnquiryLevelStructure = enquiryLevelStructure;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the result instances XML for this Globus task
        /// </summary>
        public string ResultInstances
        {
            get { return _ResultInstances; }
            set { _ResultInstances = value; }
        }

        /// <summary>
        /// Gets or sets the result errors XML for this Globus task
        /// </summary>
        public string ResultErrors
        {
            get { return _ResultErrors; }
            set { _ResultErrors = value; }
        }

        public ManualResetEvent FinishEvent
        {
            get { return _FinishEvent; }
            set { _FinishEvent = value; }
        }

        public ManualResetEvent StartEvent
        {
            get { return _StartEvent; }
            set { _StartEvent = value; }
        }

        public int DatasetID
        {
            get { return _DatasetID; }
            set { _DatasetID = value; }
        }

        public WindowsIdentity AdapterIdentity
        {
            get { return _AdapterIdentity; }
            set { _AdapterIdentity = value; }
        }

        public InstanceCollection Instances
        {
            get { return _Instances; }
            set { _Instances = value; }
        }

        public MetadataTypical RequestTypical
        {
            get { return _RequestTypical; }
        }

        public MetadataTypical ResponseTypical
        {
            get { return _ResponseTypical; }
            set { _ResponseTypical = value; }
        }

        public string Configuration
        {
            get { return _Configuration; }
            set { _Configuration = value; }
        }

        public string Version
        {
            get { return _Version; }
            set { _Version = value; }
        }

        public string Company
        {
            get { return _Company; }
            set { _Company = value; }
        }

        public LevelStructureHierarchy EnquiryLevelStructure
        {
            get { return _EnquiryLevelStructure; }
        }

        #endregion
    }
}
