﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using OFSCommonMethods.IO;
using System.Collections;
using AXMLEngine;
using System.Threading;
using System.Configuration;
using System.Web;
using ValidataCommon;

namespace OFSCommonMethods
{
    public abstract class OFSMessageGeneratorBase
    {
        #region Constants

        /// <summary>
        /// The name of the Globus unique ID attribute
        /// </summary>
        protected const string T24_ID_ATTRIBUTE_NAME = "@ID";

        public const string ADAPTER_REQUEST_METADATA_MISMATCH = "ADAPTER_REQUEST_METADATA_MISMATCH";

        #endregion

        #region Protected Members

        /// <summary>
        /// Contains the user used for commands and enquiries
        /// </summary>
        protected string _CommandUser = "INPUTT";

        /// <summary>
        /// Contains the password for the user used for commands and enquiries
        /// </summary>
        protected string _CommandPassword = "234567";

        /// <summary>
        /// Contains the user used for authorizationa
        /// </summary>
        protected string _AuthorizeUser = "OUTPUTT";

        /// <summary>
        /// Contains the password for the user used for authorizations
        /// </summary>
        protected string _AuthorizePassword = "234567";

        /// <summary>
        /// Treat OFS Response Single Value As Error
        /// </summary>
        protected bool _TreatOFSResponseSingleValueAsError;

        /// <summary>
        /// OFS or OFSML
        /// </summary>
        protected Settings.OFSType _OfsType;


        /// <summary>
        /// Flag if old HistoryResponseReader has to be used - it has known error if in any value there is comma and quotation mark, cannot extract IDs
        /// </summary>
        protected bool _OFSUseOldHistoryReadResponse;

        /// <summary>
        /// Array of filter objects
        /// </summary>
        protected List<Filter> _Filters;

        /// <summary>
        /// Array of post filter objects
        /// </summary>
        protected List<Filter> _PostFilters;

        /// <summary>
        /// Hashtable of catalogs that contain arrays of typicals
        /// </summary>
        protected Hashtable _Catalogs = new Hashtable();

        /// <summary>
        /// The message queue manager
        /// </summary>
        protected MessageQueueManager _MessageQueue = null;

        /// <summary>
        /// The parameters that have to be passed to the DoOFS or DoHistory routines
        /// </summary>
        protected OFSParameters _OFSParameters;

        /// <summary>
        /// The event used to activate the current thread when response processing must be carried out
        /// </summary>
        protected AutoResetEvent _ThreadEvent;

        /// <summary>
        /// The GUID of the current thread
        /// </summary>
        public string _GUID;

        /// <summary>
        /// Contains the current message index for this this thread
        /// </summary>
        protected uint _MessageIndex;

        /// <summary>
        /// Always autenticate in OFS messages
        /// </summary>
        protected bool _AlwaysUseAuthentication = false;

        protected static int TimeoutStep1
        {
            get {
                return 300000; 
            }
        }


        protected string _PhantomSet = string.Empty;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the parameters of the Globus task
        /// </summary>
        public OFSParameters Parameters
        {
            get { return this._OFSParameters; }
            set { this._OFSParameters = value; }
        }

        public bool AlwaysUseAuthentication
        {
            get { return _AlwaysUseAuthentication; }
            set { _AlwaysUseAuthentication = value; }
        }

        /// <summary>
        /// Get/Set filters
        /// </summary>
        public List<Filter> Filters { get { return _Filters; } set { _Filters = value; } }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Creates a fake instance for the needes of the history feature
        /// </summary>
        /// <param name="id">The value of the ID</param>
        /// <returns></returns>
        public static Instance CreateFakeInstance(string id)
        {
            Instance inst = new Instance();
            inst.AddAttribute(T24_ID_ATTRIBUTE_NAME, id);

            return inst;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Finds the metadata typical for an instance.
        /// </summary>
        /// <param name="typicalName">The name of the typical.</param>
        /// <param name="catalogName">the name of the catalogue.</param>
        /// <returns></returns>
        protected MetadataTypical FindTypicalByNameAndCatalog(string typicalName, string catalogName)
        {
            Hashtable typicalHash = (Hashtable)_Catalogs[catalogName];
            MetadataTypical typ = (MetadataTypical)typicalHash[typicalName];
            return typ;
        }

        /// <summary>
        /// Organizes the typicals in this AXML request by their catalog name. Later, this collection
        /// will be used to find the typical by catalog name and typical name
        /// </summary>
        /// <param name="typicals">The collection of all typicals in the request</param>
        protected void BuildTypicalCatalogs(TypicalCollection typicals)
        {
            for (int i = 0; i < typicals.Count; i++)
            {
                MetadataTypical tmpTypical = typicals[i];

                Hashtable typicalHash;
                if (_Catalogs.ContainsKey(tmpTypical.CatalogName))
                {
                    typicalHash = (Hashtable)_Catalogs[tmpTypical.CatalogName];
                    typicalHash[tmpTypical.Name] = tmpTypical;
                }
                else
                {
                    typicalHash = new Hashtable();
                    _Catalogs[tmpTypical.CatalogName] = typicalHash;
                    typicalHash[tmpTypical.Name] = tmpTypical;
                }
            }
        }

        protected EnquiryResultParsingRotine GetEnquiryResultParsingTemplate()
        {
            var enqResFilter = _Filters.FirstOrDefault(n => n.Type == FilterType.TemplateName);
            
            EnquiryResultParsingRotine routine =
                enqResFilter != null
                ? EnquiryResultParsingRotine.FromString(enqResFilter.AttributeFilter)
                : EnquiryResultParsingRotine.None;

            routine.PostFilters = _PostFilters;

            return routine;
        }

        protected void UpdateMultipartAAMessage(GlobusRequestBase request, GlobusMessage resultMessage,
            OFSCommonMethods.IO.Telnet.TSSTelnetComm telnetComm, System.Xml.XmlWriter axmlWriter)
        {
            MetadataTypical metadataTypical =
                new MetadataTypical() { Name = "AA.PROCESS.DETAILS", CatalogName = "DummyCatalog" };
            metadataTypical.AddAttribute(new TypicalAttribute("@ID", Validata.Common.AttributeDataType.String, "", 1000));

            GlobusRequest rqAA_PROCESS_DETAILS = new GlobusRequest(
                    metadataTypical,
                    request.Instance,
                    GlobusRequest.RequestType.DataProcess,
                    DataProcessType.Select,
                    _OFSParameters.Version,
                    _OFSParameters.Company,
                    this._PhantomSet
                );

            rqAA_PROCESS_DETAILS.State = GlobusRequest.ExecutionState.WaitingForProcessing;

            string seeRecordCommand_AA_PROCESS_DETAILS = rqAA_PROCESS_DETAILS.GenerateMessageText(
                    _CommandUser,
                    _CommandPassword,
                    _AuthorizeUser,
                    _AuthorizePassword,
                    _Filters,
                    false,
                    false,
                    false,
                    _AlwaysUseAuthentication,
                    _OfsType);

            GlobusMessage outputT24Message = new GlobusMessage("");
            if (telnetComm != null)
            {
                GlobusMessage inputT24Message = new GlobusMessage("");
                inputT24Message.Message.Text = seeRecordCommand_AA_PROCESS_DETAILS;
                _MessageQueue.ProcessMessage_Bulk(_GUID, inputT24Message, outputT24Message, telnetComm);
            }
            else
            {
                GlobusMessage inputT24Message = new GlobusMessage(_GUID, 0);
                inputT24Message.Message.Text = seeRecordCommand_AA_PROCESS_DETAILS;
                request.MessageFileID = _MessageQueue.ProcessMessage_Bulk(false, inputT24Message);
                ArrayList messages = _MessageQueue.GetMessages(_GUID);
                System.Diagnostics.Debug.Assert(messages.Count == 1);
                if (messages.Count == 0)
                {
                    outputT24Message.Message.Text = "No result retrieved";
                }
                else
                {
                    outputT24Message.Message = ((GlobusMessage)messages[0]).Message;
                }
            }

            var enqResultTemplate = (request.EnquiryLevelStructure != null) ?
                new EnquiryResultParsingRotine(request.EnquiryLevelStructure) :
                GetEnquiryResultParsingTemplate();

            rqAA_PROCESS_DETAILS.IsAAProcessDetails = true;
            rqAA_PROCESS_DETAILS.SetResponse(
                            outputT24Message.Message,
                            request.ResponseTypical,
                            axmlWriter,
                            ((telnetComm != null) ? telnetComm.Log : (IO.Telnet.TSSTelnetComm.LogMessageHandle)null),
                            _TreatOFSResponseSingleValueAsError,
                            enqResultTemplate,
                            _OfsType
                        );

            if (rqAA_PROCESS_DETAILS.Failed)
            {
                // Set error on original request and exit
                request.Result = string.Format("{0} ({1})", rqAA_PROCESS_DETAILS.Result, seeRecordCommand_AA_PROCESS_DETAILS);
                request.Failed = true;
                request.State = GlobusRequest.ExecutionState.Finished;
                return;
            }

            Instance instanceAA_PROCESS_DETAILS = Instance.FromXmlString(rqAA_PROCESS_DETAILS.Result);

            AAProcessDetailsPropertyList lsProperties =
                AAProcessDetailsPropertyList.FromInstance(instanceAA_PROCESS_DETAILS);

            foreach (AAProcessDetailsProperty property in lsProperties)
            {
                if (property.Action != "I")
                {
                    // TODO - Are only I are possible.
                    // Currently it is known that 'M' is not. But others?
                    continue;
                }

                MetadataTypical metadataTypical1 =
                    new MetadataTypical() { Name = property.Application, CatalogName = "DummyCatalog" };
                metadataTypical1.AddAttribute(new TypicalAttribute("@ID", Validata.Common.AttributeDataType.String, "", 1000));

                GlobusRequest rqProperty = new GlobusRequest(
                        metadataTypical1,
                        OFSMessageGeneratorBase.CreateFakeInstance(property.ID),
                        GlobusRequest.RequestType.DataProcess,
                        DataProcessType.Select,
                        property.Version,
                        _OFSParameters.Company,
                        this._PhantomSet
                    );

                string seeCommand = rqProperty.GenerateMessageText(
                        _CommandUser,
                        _CommandPassword,
                        _AuthorizeUser,
                        _AuthorizePassword,
                        _Filters,
                        false,
                        false,
                        false,
                        _AlwaysUseAuthentication,
                        _OfsType);

                resultMessage.Message.SubItems.Add(seeCommand);
            }
        }

        #endregion
    }
}
