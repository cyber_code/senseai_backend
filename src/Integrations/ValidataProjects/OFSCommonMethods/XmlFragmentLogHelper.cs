﻿using System.Xml;

namespace OFSCommonMethods
{
    internal class XmlFragmentLogHelper
    {
        public static XmlReaderSettings GetSettings()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            settings.IgnoreWhitespace = true;
            settings.IgnoreComments = true;
            return settings;
        }
    }
}