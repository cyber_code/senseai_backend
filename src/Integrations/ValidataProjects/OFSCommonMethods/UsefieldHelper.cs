﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AXMLEngine;

namespace OFSCommonMethods
{
    internal static class UsefieldHelper
    {
        #region Constants

        /// <summary>
        /// Prefix for company in case it has to be obtained from test data attribute value
        /// </summary>
        internal const string UseFieldCommandParamPreffix = "USEFIELD:";

        /// <summary>
        /// Prefix for company in case it has to be obtained from test data attribute value (Move operation - erase company attribute value)
        /// </summary>
        internal const string UseFieldCommandParamPreffix_Move = "USEFIELD(MOVE):";

        /// <summary>
        /// Prefix for company in case it has to be obtained from test data attribute value (Copy operation - preserve company attribute value)
        /// </summary>
        internal const string UseFieldCommandParamPreffix_Copy = "USEFIELD(COPY):";

        #endregion

        internal static string GetSignonUser(string user, Instance instance)
        {
            if (instance == null)
                return user;

            return GetActualCommandParam(user, instance);
        }

        internal static string GetSignonPassword(string password, Instance instance)
        {
            if (instance == null)
                return password;

            return GetActualCommandParam(password, instance);
        }

        internal static string GetCompany(string company, Instance instance)
        {
            return GetActualCommandParam(company, instance);
        }

        private static string GetActualCommandParam(string commandParamValue, Instance instance)
        {
            if (string.IsNullOrEmpty(commandParamValue))
                return commandParamValue;

            string prefix;
            bool isMove = true;
            if (commandParamValue.StartsWith(UseFieldCommandParamPreffix, StringComparison.OrdinalIgnoreCase))
            {
                prefix = UseFieldCommandParamPreffix;
            }
            else if (commandParamValue.StartsWith(UseFieldCommandParamPreffix_Move, StringComparison.OrdinalIgnoreCase))
            {
                prefix = UseFieldCommandParamPreffix_Move;
            }
            else if (commandParamValue.StartsWith(UseFieldCommandParamPreffix_Copy, StringComparison.OrdinalIgnoreCase))
            {
                prefix = UseFieldCommandParamPreffix_Copy;
                isMove = false;
            }
            else
            {
                // not USEFIELD:XXX
                return commandParamValue;
            }

            string companyAttributeName = commandParamValue.Substring(prefix.Length).Trim();
            string result = GetCompanyFromInstanceAttribute(instance, companyAttributeName);

            if (isMove)
            {
                // clear value from the used field
                RemoveInstanceAttributeValue(instance, companyAttributeName);
            }

            return result;
        }

        private static string GetCompanyFromInstanceAttribute(Instance instance, string attributeName)
        {
            if (instance == null)
                throw new InvalidOperationException("Error getting company from instance attribute: Data instance not presented!");

            var instanceAttribute = instance.GetAttributeByShortName(attributeName);
            return (instanceAttribute == null || instanceAttribute.Value == "No Value") 
                ? string.Empty 
                : instanceAttribute.Value;
        }

        private static void RemoveInstanceAttributeValue(Instance instance, string attributeName)
        {
            if (instance == null)
            {
                string errorMessage = string.Format("Error removing company attribute '{0}' from instance: Data instance not presented!", attributeName);
                throw new InvalidOperationException(errorMessage);
            }

            instance.RemoveAttribute(attributeName);
        }
    }
}
