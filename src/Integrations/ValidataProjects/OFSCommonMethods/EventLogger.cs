using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;

namespace OFSCommonMethods
{
    public enum GlobusEventType
    {
        Crash,
        Warning,
        DataLog,
        ExecPointReached
    }

    public class FileManager
    {
        private static object _obLock = new object();
        private static Dictionary<string, TextWriter> _OpenedFiles = new Dictionary<string, TextWriter>();

        public static void CloseAllOpenFiles()
        {
            lock (_obLock)
            {
                foreach (KeyValuePair<string, TextWriter> file in _OpenedFiles)
                {
                    try
                    {
                        file.Value.Close();
                    }
                    catch { }
                }
                _OpenedFiles.Clear();
            }
        }

        public static bool IsOpen(string filePath)
        {
            return _OpenedFiles.ContainsKey(filePath);
        }

        public static TextWriter GetOpenedFileTextWriter(string filePath)
        {
            lock (_obLock)
            {
                if (!_OpenedFiles.ContainsKey(filePath))
                {
                    FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate);
                    TextWriter streamWriter = new StreamWriter(fs, Encoding.ASCII);
                    _OpenedFiles.Add(filePath, streamWriter);
                    return streamWriter;
                }
                else
                {
                    return _OpenedFiles[filePath];
                }
            }
        }

        public static void WriteToFile(string filePath, string data, bool flush)
        {
            TextWriter textWriter = GetOpenedFileTextWriter(filePath);
            textWriter.Write(data);

            if (flush)
            {
                textWriter.Flush();
            }
        }

        public static void FlushAndCloseFile(string filePath)
        {
            TextWriter textWriter = GetOpenedFileTextWriter(filePath);
            textWriter.Flush();
            textWriter.Close();
        }
    }

    #region New Code

    public class EventLogger
    {
        private static string _LogFilesPath = Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\Logs\T24AdapterLog\";

        public static void StartLogging(bool erasePrevious)
        {
            try
            {
                if (erasePrevious && Directory.Exists(_LogFilesPath))
                {
                    Directory.Delete(_LogFilesPath, true);
                }

                Directory.CreateDirectory(_LogFilesPath);
            }
            catch
            {
            }
        }

        public static void LogEvents(string threadGuid, GlobusEventType eventType, object eventParam)
        {
            try
            {
                if (eventType == GlobusEventType.DataLog)
                    return;

                System.DateTime logTime = DateTime.Now;

                string filePath = string.Format("{0}{1}.txt", _LogFilesPath, threadGuid);
                bool writeStartUp = !FileManager.IsOpen(filePath);

                TextWriter writer = FileManager.GetOpenedFileTextWriter(filePath);

                if (writeStartUp)
                {
                    writer.WriteLine("{0}: Logging Started...", logTime.ToString("dd-MM-yyyy HH:mm:ss"));
                }

                // WRITE CURRENT LOG...
                writer.Write(logTime.ToLongTimeString());
                writer.Write(": ");
                writer.Write(GetGlobusEventTypeText(eventType));

                if (eventParam != null)
                {
                    writer.Write(eventParam);
                }
                writer.WriteLine();
                writer.Flush();
            }
            catch
            {
            }
        }

        private static string GetGlobusEventTypeText(GlobusEventType eventType)
        {
            switch (eventType)
            {
                case GlobusEventType.Crash:
                    return "CRASH   : ";
                case GlobusEventType.DataLog:
                    return "DATALOG : ";
                case GlobusEventType.Warning:
                    return "WARNING : ";
                case GlobusEventType.ExecPointReached:
                    return "EXEC    : ";
                default:
                    return "";
            }
        }

        public static void EndLogging()
        {
            FileManager.CloseAllOpenFiles();
        }
    }

    #endregion
}