﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Threading;
using AXMLEngine;
using ValidataCommon;
using Validata.Common;
using OFSStringConvertor;
using OFSCommonMethods.OFSML;
using OFSCommonMethods.OFSML.Common;
using OFSCommonMethods.OFSML.OFSML130;
using OFSCommonMethods.OFSEnquiry;

namespace OFSCommonMethods
{
    public abstract class GlobusRequestBase
    {
        #region Constants

        /// <summary>
        /// The name of the loan and deposits application
        /// </summary>
        public const string LD_LOAN = "LD.LOANS.AND.DEPOSITS";

        /// <summary>
        /// The name of the schedule application
        /// </summary>
        public const string LD_SCHEDULE = "LD.SCHEDULE.DEFINE";

        /// <summary>
        /// The prefix used to write before the LD schedule attributes
        /// </summary>
        public const string LD_SCHEDULE_PREFIX = "LD.SCHEDULE.DEFINE.";

        /// <summary>
        /// The delimiter used to separate LD Loan attributes from the schedule attributes
        /// </summary>
        public const string LD_DELIM = "//";

        /// <summary>
        /// The field that controls whether an LD schedule should be define
        /// </summary>
        protected const string SCHEDULE_FLAG = "DEFINE.SCHEDS";

        /// <summary>
        /// The values that determines that an LD schedule is defined
        /// </summary>
        private readonly static List<string> SCHEDULE_INDICATOR_VALUES = new List<string> { "YES", "Y" };

        /// <summary>
        /// The name of the loan and deposits application
        /// </summary>
        public const string FX_SWAP = "FOREX";

        /// <summary>
        /// The string that precedes leg 1 attributes
        /// </summary>
        public const string FX_FIRST_LEG = "LEG.1.";

        /// <summary>
        /// The string that precedes leg 2 attributes
        /// </summary>
        public const string FX_SECOND_LEG = "LEG.2.";

        /// <summary>
        /// The name of the Globus attribute that contains the @ID of the second leg
        /// in the response received on the input stage of an fx swap. This @ID
        /// must be used for the Authorize command
        /// </summary>
        public const string FX_SECOND_LEG_ID = "SWAP.REF.NO";

        /// <summary>
        /// Live Record Not Changes String
        /// </summary>
        public const string LIVE_RECORD_NOT_CHANGED = "LIVE RECORD NOT CHANGED";

        /// <summary>
        /// The base class for typicals that denote globus enquiries
        /// </summary>
        public const string T24_Enquiry = "T24_Enquiry";

        /// <summary>
        /// The base class for typicals that denote globus enquiry results
        /// </summary>
        public const string T24_Enquiry_Result = "T24_Enquiry_Result";

        /// <summary>
        /// The base class for typicals that denote globus data tables
        /// </summary>
        public const string T24_DataProcess = "T24_DataProcess";

        /// <summary>
        /// The base class for typicals that denote globus applications
        /// </summary>
        public const string T24_Application = "T24_Application";

        /// <summary>
        /// The base class for typicals that denote T24 AA Product
        /// </summary>
        public const string T24_AAProduct = "T24_AAProduct";

        /// <summary>
        /// The base class for typicals that denote multi apps (from BROWSER.MULTI.APP.PARAM)
        /// </summary>
        public const string T24_BrowserMultiApp = "T24_BrowserMultiApp";

        /// <summary>
        /// Prefix for company in case it has to be obtained from test data attribute value
        /// </summary>
        public const string CompanyForAttributePreffix = "USEFIELD:";

        /// <summary>
        /// Prefix for company in case it has to be obtained from test data attribute value (Move operation - erase company attribute value)
        /// </summary>
        public const string CompanyForAttributePreffix_Move = "USEFIELD(MOVE):";

        /// <summary>
        /// Prefix for company in case it has to be obtained from test data attribute value (Copy operation - preserve company attribute value)
        /// </summary>
        public const string CompanyForAttributePreffix_Copy = "USEFIELD(COPY):";

        #endregion

        #region Enumerations

        /// <summary>
        /// Constraint types supported by Globus (see http://www.t24all.com/forum/8-t24-technical/3918-ofsml-selection-operands.html)
        /// </summary>
        public enum GlobusConstraint
        {
            /// <summary>
            /// Equal
            /// </summary>
            EQ,
            /// <summary>
            /// Greater than
            /// </summary>
            GT,
            /// <summary>
            /// Less Than
            /// </summary>
            LT,
            /// <summary>
            /// Greater or equal
            /// </summary>
            GE,
            /// <summary>
            /// Less or equal
            /// </summary>
            LE,
            /// <summary>
            /// Not equal
            /// </summary>
            NE,
            /// <summary>
            /// Like
            /// </summary>
            LK,
            /// <summary>
            /// Not like (unlike)
            /// </summary>
            UL,
            /// <summary>
            /// In Range 
            /// </summary>
            RG,
            /// <summary>
            /// Not In Range
            /// </summary>
            NR,
            /// <summary>
            /// Contains
            /// </summary>
            CT,
            /// <summary>
            /// Not Containing
            /// </summary>
            NC,
            /// <summary>
            /// Begins With
            /// </summary>
            BW,
            /// <summary>
            /// Ends With
            /// </summary>
            EW,
            /// <summary>
            /// Does Not Begin With
            /// </summary>
            DNBW,
            /// <summary>
            /// Does Not End With
            /// </summary>
            DNEW,
            /// <summary>
            /// Sounds Like
            /// </summary>
            SAID,
            /// <summary>
            /// Between
            /// </summary>
            BT
        }

        /// <summary>
        /// Enumerates the execution states for GlobusRequest objects
        /// </summary>
        public enum ExecutionState
        {
            Initial,
            WaitingForProcessing,
            Processed,
            WaitingForAuthorization,
            Finished
        }

        /// <summary>
        /// Enumerates the request types supported by this adapter
        /// </summary>
        public enum RequestType
        {
            Enquiry,
            DataProcess,
            Application,
            History,
            AAProduct
        }

        #endregion

        #region Private Members

        /// <summary>
        /// Determine if the FOREX is flat - with one LEG ot it is not flat - with two LEGs
        /// </summary>
        private readonly bool _FxIsFlat = true;

        /// <summary>
        /// Determines whether to authorize first or second leg (for IA OFS commands)
        /// </summary>
        private readonly bool _FxAuthorizeFirstLeg = true;

        /// <summary>
        /// The AXML instance that this request is based on
        /// </summary>
        protected Instance _Instance;

        /// <summary>
        /// The request typical
        /// </summary>
        private readonly MetadataTypical _RequestTypical;

        /// <summary>
        /// The response typical
        /// </summary>
        protected MetadataTypical _ResponseTypical;

        /// <summary>
        /// The text of the first message of this request
        /// </summary>
        protected string _FirstMessage = "";

        /// <summary>
        /// The message file id that was created for this GlobusRequest
        /// </summary>
        protected string _MessageFileID = "";

        /// <summary>
        /// True if this request needs authorization processing
        /// </summary>
        protected bool _NeedsAuthorize;

        /// <summary>
        /// The version for this request
        /// </summary>
        protected readonly string _Version = "";

        /// <summary>
        /// The type of the request
        /// </summary>
        protected RequestType _RequestType;

        /// <summary>
        /// The data processing type. Valid only if the request is of type data processing.
        /// </summary>
        protected DataProcessType _DataProcessType;

        /// <summary>
        /// The current execution state of this request
        /// </summary>
        protected ExecutionState _State;

        /// <summary>
        /// The result of the request execution
        /// </summary>
        protected string _Result;

        /// <summary>
        /// The transaction ID.
        /// </summary>
        protected string _TransactionID;

        /// <summary>
        /// True if the request has failed
        /// </summary>
        protected bool _Failed;

        /// <summary>
        /// The Globus version number. Used for history processing.
        /// </summary>
        protected string _CURRNO = "";

        /// <summary>
        /// The company
        /// </summary>
        protected readonly string _Company;

        /// <summary>
        /// PhantomSet
        /// </summary>
        protected string _PhantomSet = string.Empty;

        /// <summary>
        /// Contains list of typicals attribute names that starts with digit
        /// </summary>
        protected Dictionary<string, List<string>> _SpecialTypicalAttributes;

        /// <summary>
        /// True if the order of instance attributes have to be like the order of typical attributes
        /// , false - simple alphabetic order
        /// </summary>
        protected readonly bool _UseTypicalAttributeOrder;

        /// <summary>
        /// The ValidataID.
        /// </summary>
        protected string _ValidataID;

        #endregion

        #region Public Properties

        /// <summary>
        /// The ID of the message file that was sent to Globus based on this request.
        /// </summary>
        public string MessageFileID
        {
            get { return _MessageFileID; }
            set { _MessageFileID = value; }
        }

        /// <summary>
        /// True if the request needs also to be authorized in Globus.
        /// </summary>
        public bool NeedsAuthorize
        {
            get { return _NeedsAuthorize; }
            set { _NeedsAuthorize = value; }
        }

        /// <summary>
        /// The XML string that is the result of this request.
        /// </summary>
        public string Result
        {
            get { return _Result; }
            set { _Result = value; }
        }

        /// <summary>
        /// True if this request is in a state of waiting to be processed
        /// </summary>
        public bool WaitingForProcessing
        {
            get { return _State == ExecutionState.WaitingForProcessing; }
        }

        public void StartWaitingForProcessing()
        {
            if (_State == ExecutionState.Initial)
            {
                _State = ExecutionState.WaitingForProcessing;
            }
        }

        /// <summary>
        /// True if this request is in a state of waiting to be authorized
        /// </summary>
        public bool WaitingForAuthorization
        {
            get { return _State == ExecutionState.WaitingForAuthorization; }
            set
            {
                if (!value)
                {
                    throw new ApplicationException("Trying to set WaitingForAuthorization to sth other than true is illegal");
                }

                _State = ExecutionState.WaitingForAuthorization;
            }
        }

        /// <summary>
        /// True if this request is in a state of ready to be authorized
        /// </summary>
        public bool ReadyForAuthorization
        {
            get { return _State == ExecutionState.Processed; }
        }

        /// <summary>
        /// True if this request should not be processed anymore
        /// </summary>
        public bool IsFinished
        {
            get { return (_State == ExecutionState.Finished) || (_Failed); }
        }

        /// <summary>
        /// True if the request has failed.
        /// </summary>
        public bool Failed
        {
            get { return _Failed; }
            set { _Failed = value; }
        }

        /// <summary>
        /// The name of the typical ( Globus command,application, data table) 
        /// </summary>
        public string RequestTypicalName
        {
            get { return _RequestTypical.Name; }
        }

        /// <summary>
        /// The Response typical.
        /// </summary>
        public MetadataTypical ResponseTypical
        {
            get { return _ResponseTypical; }
            set { _ResponseTypical = value; }
        }

        /// <summary>
        /// The Globus record version number
        /// </summary>
        public string CURRNO
        {
            get { return _CURRNO; }
        }

        /// <summary>
        /// The type of the request
        /// </summary>
        public RequestType TypeOfRequest
        {
            get { return _RequestType; }
        }

        /// <summary>
        /// 	The data processing of the request. Valid only if the request is of type data processing.
        /// </summary>
        public DataProcessType ProcessingType
        {
            get { return _DataProcessType; }
        }

        /// <summary>
        /// Gets the first message.
        /// </summary>
        /// <value>The first message.</value>
        public string FirstMessage
        {
            get { return _FirstMessage; }
            set { _FirstMessage = value; }
        }

        /// <summary>
        /// List with weird named typical attributes
        /// </summary>
        public Dictionary<string, List<string>> SpecialTypicalAttributes
        {
            get { return _SpecialTypicalAttributes; }
            set { _SpecialTypicalAttributes = value; }
        }

        /// <summary>
        /// The instance on which this request is based.
        /// </summary>
        public Instance Instance
        {
            get { return _Instance; }
            set
            {
                _Instance = value;
                PrepareInstanceAttributeValues(_Instance, _RequestTypical);
            }
        }

        /// <summary>
        /// Enquiry mask (level structure hierarchy)
        /// </summary>
        public readonly LevelStructureHierarchy EnquiryLevelStructure;

        /// <summary>
        /// Sets/Gets the ValidataID if applicable(only in export).
        /// </summary>
        /// <value>The ValidataID.</value>
        public string ValidataID
        {
            get { return _ValidataID; }
            set { _ValidataID = value; }
        }

        public ExecutionState State
        {
            get { return _State; }
            set { _State = value; }
        }

        /// <summary>
        /// Used to override the enquiry name if different from %APPLICATION
        /// </summary>
        public Dictionary<string, string> CustomEnquiries;

        #endregion

        #region Class Lifecycle

        public GlobusRequestBase(MetadataTypical typical, Instance instance, RequestType reqType,
                                 DataProcessType dpType, string version, string company,
                                 LevelStructureHierarchy enquiryLevelStructure)
        {
            _RequestTypical = typical;
            _ResponseTypical = typical;
            _RequestType = reqType;
            _DataProcessType = dpType; //Valid only if the request is of type data processing.
            _State = ExecutionState.Initial;
            _Version = version;

            if (instance != null)
            {
                _Company = UsefieldHelper.GetCompany(company, instance);
            }

            Instance = PreProcessInstance(_RequestTypical, instance);

            EnquiryLevelStructure = enquiryLevelStructure;

            if (instance != null && instance.ID != null)
                _ValidataID = instance.ID;

            _UseTypicalAttributeOrder = true;
            _FxIsFlat = false;
            _FxAuthorizeFirstLeg = true;

            UpdateRequestTypeIfAA();
        }

        protected virtual Instance PreProcessInstance(MetadataTypical requestTypical, Instance instance)
        {
            return instance;
        }

        #endregion

        #region Public Methods

        public void SetOfsmlTransactionResponse(string response)
        {
            try
            {
                if (_State == ExecutionState.WaitingForProcessing ||
                    _State == ExecutionState.WaitingForAuthorization)
                {
                    var ofsmlManager = new OfsmlManager(new Ofsml130Commands(), RequestTypicalName);

                    bool hasError = false;
                    string resMessage = ofsmlManager.ReadOfsmlTransactionResponse(
                        response, _ResponseTypical, ref hasError, ref _TransactionID);

                    if (hasError)
                    {
                        _Result = Utils.GetHtmlEncodedValue(resMessage);
                        _State = ExecutionState.Finished;
                        _Failed = true;
                    }
                    else
                    {
                        _Result = resMessage;
                        _State = _State == ExecutionState.WaitingForAuthorization
                            ? ExecutionState.Finished
                            : ExecutionState.Processed;
                    }
                }
            }
            catch (Exception e)
            {
                LogParsingExceptionInfo(response, e);
            }
        }

        public List<string> SetOfsmlEnquiryResponse(string response, EnquiryResultParsingRotine enquiryResultParsingRoutine)
        {
            var results = new List<string>();

            try
            {
                if (_State == ExecutionState.WaitingForProcessing)
                {
                    var ofsmlManager = new OfsmlManager(new Ofsml130Commands(), RequestTypicalName);

                    if (_RequestType == RequestType.History)
                    {
                        results = ofsmlManager.ReadOfsmlHistoryResponse(response);
                    }
                    else if (_RequestType == RequestType.Enquiry)
                    {
                        if (EnquiryLevelStructure != null)
                        {
                            // override default one directly passed is with high priority
                            enquiryResultParsingRoutine = new EnquiryResultParsingRotine(EnquiryLevelStructure);
                        }

                        if (enquiryResultParsingRoutine == null)
                        {
                            enquiryResultParsingRoutine = EnquiryResultParsingRotine.None;
                        }

                        _Result = ofsmlManager.ReadOfsmlEnquiryResponse(
                                _PhantomSet,
                                RequestTypicalName,
                                ResponseTypical,
                                response,
                                enquiryResultParsingRoutine
                            );
                    }

                    _State = ExecutionState.Finished;
                }
            }
            catch (EnquiryConfigurationException ex)
            {
                _Result = ex.Message;
                _State = ExecutionState.Finished;
                _Failed = true;
            }
            catch (Exception e)
            {
                LogParsingExceptionInfo(response, e);
            }

            return results;
        }

        public string GenerateOfsmlRequest(string user, string password, string authorizeUser, string authorizePassword, List<Filter> filters, bool validate)
        {
            string messageText = "";

            var ofsmlManager = new OfsmlManager(new Ofsml130Commands(), RequestTypicalName);

            switch (_RequestType)
            {
                case RequestType.Enquiry:
                case RequestType.History:
                    messageText = ofsmlManager.GenerateOfsmlEnquiryRequest(user, password, filters, _RequestType);

                    break;

                case RequestType.Application:
                case RequestType.DataProcess:
                    {
                        user = UsefieldHelper.GetSignonUser(user, _Instance);
                        password = UsefieldHelper.GetSignonPassword(password, _Instance);

                        authorizeUser = UsefieldHelper.GetSignonUser(authorizeUser, _Instance);
                        authorizePassword = UsefieldHelper.GetSignonPassword(authorizePassword, _Instance);

                        OperationCode operationCode = validate ? OperationCode.VALIDATE : OperationCode.PROCESS;
                        messageText = ofsmlManager.GenerateOfsmlTransactionRequest(user, password
                            , authorizeUser, authorizePassword, _Version, _Company, operationCode, _DataProcessType, _Instance);
                        break;
                    }
            }

            _FirstMessage = messageText;

            return messageText;
        }

        /// <summary>
        /// Marks the request as finished
        /// </summary>
        public void SetFinished()
        {
            _State = ExecutionState.Finished;
        }

        /// <summary>
        /// Generates the text of an OFS message based on the type and
        /// settings of this GlobusRequest instance.
        /// </summary>
        /// <param name="user">The name of the Globus user to be used in the OFS message.</param>
        /// <param name="password">The password for the user.</param>
        /// <param name="authorizeUser">Authorization user name</param>
        /// <param name="authorizePassword">Authorization user password</param>
        /// <param name="filters">The colection of filtering rules that must be added to an enquiry request.</param>
        /// <param name="authenticateAlways">Authenticate always</param>
        /// <param name="isProductionDataImport">Is production data import action</param>
        /// <param name="isValidate">is validate method</param>
        /// <param name="waitingForAuthorization">Is waiting for authorization</param>
        /// <param name="msgId">Message ID. Required in case of use of Post OFS and Batch file listener adapters</param>
        /// <returns>The text of the OFS message.</returns>
        public string GenerateOfsRequest(string user, string password,
                                          string authorizeUser, string authorizePassword,
                                          List<Filter> filters, bool authenticateAlways,
                                          bool isProductionDataImport, bool isValidate, bool waitingForAuthorization, string msgId = "")
        {
            string messageText = "";

            switch (_RequestType)
            {
                case RequestType.Enquiry:
                case RequestType.History:
                    messageText = authenticateAlways ?
                        GenerateEnquiryMessage(user, password, filters, isProductionDataImport) :
                        GenerateEnquiryMessage(filters, isProductionDataImport);
                    break;

                case RequestType.Application:
                    user = UsefieldHelper.GetSignonUser(user, _Instance);
                    password = UsefieldHelper.GetSignonPassword(password, _Instance);
                    messageText = GenerateApplicationCallMessage(user, password, isValidate);
                    break;

                case RequestType.DataProcess:
                    if (_Instance == null)
                    {
                        //check for a common source of errors 
                        throw new ApplicationException("Instance not set for a data process command!");
                    }

                    _Instance.ReorderAttributes(_RequestTypical, _UseTypicalAttributeOrder);

                    if (_DataProcessType == DataProcessType.Authorize || waitingForAuthorization)
                    {
                        if (_TransactionID == null || _TransactionID.Trim() == String.Empty)
                            _TransactionID = _Instance.GetAttributeValue("@ID");

                        authorizeUser = UsefieldHelper.GetSignonUser(authorizeUser, _Instance);
                        authorizePassword = UsefieldHelper.GetSignonPassword(authorizePassword, _Instance);

                        messageText = string.Format("{0},{1}", RequestTypicalName
                            , GenerateAuthorizeMessageText(authorizeUser, authorizePassword, isValidate, false));
                    }
                    else
                    {
                        user = UsefieldHelper.GetSignonUser(user, _Instance);
                        password = UsefieldHelper.GetSignonPassword(password, _Instance);
                        messageText = GenerateDataCreateOrUpdateMessage(user, password, isValidate, authenticateAlways, msgId);
                    }
                    break;

                case RequestType.AAProduct:

                    if (_Instance == null)
                    {
                        //check for a common source of errors 
                        throw new ApplicationException("Instance not set for a data process command!");
                    }

                    _Instance = PreProcessInstance(_RequestTypical, _Instance);
                    _Instance.ReorderAttributes(_RequestTypical, _UseTypicalAttributeOrder);

                    if (_DataProcessType == DataProcessType.Authorize || waitingForAuthorization)
                    {
                        if (_TransactionID == null || _TransactionID.Trim() == String.Empty)
                            _TransactionID = _Instance.GetAttributeValue("@ID");

                        Instance instance = new Instance(_Instance.TypicalName, _Instance.Catalog);
                        instance.AddAttribute("@ID", _TransactionID);

                        messageText = AAProductOFSMessageGenerator.GenerateRequest(
                            DataProcessType.Authorize, authorizeUser, authorizePassword, _Company, _Version, isValidate, instance);
                    }
                    else
                    {
                        DataProcessType dataProcessType = (_DataProcessType == DataProcessType.CreateAndAuthorize)
                            ? DataProcessType.Create 
                            : _DataProcessType;

                        messageText = AAProductOFSMessageGenerator.GenerateRequest(
                                dataProcessType, user, password, _Company, _Version, isValidate, _Instance, msgId);
                    }

                    break;
            }

            _FirstMessage = messageText;

            return messageText;
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Generates authorize message for a given message
        /// </summary>
        /// <param name="authorizePassword"></param>
        /// <param name="authorizeUser"></param>
        /// <param name="isValidate">Validate mode</param>
        /// <param name="useApplicationAndVersionFromFirstMessage"></param>
        public string GenerateAuthorizeMessageText(string authorizeUser, string authorizePassword, bool isValidate, bool useApplicationAndVersionFromFirstMessage)
        {
            string version = _Version;
            if (useApplicationAndVersionFromFirstMessage)
            {
                int authIntPos = _FirstMessage.IndexOf("//");
                if (authIntPos < 0)
                    authIntPos = _FirstMessage.IndexOf("/I/");

                Debug.Assert(authIntPos >= 0);

                version = _FirstMessage.Substring(0, authIntPos);
            }

            string secondCommand = version + "/A";
            secondCommand += isValidate ? "/VALIDATE," : "/PROCESS,";

            if (authorizeUser != "" && authorizePassword != "")
            {
                secondCommand += UsefieldHelper.GetSignonUser(authorizeUser, Instance)
                    + "/"
                    + UsefieldHelper.GetSignonPassword(authorizePassword, Instance);

                if (_Company != String.Empty)
                {
                    //if company is specified - add it to the OFS request
                    secondCommand += "/" + _Company;
                }
            }
            else
            {
                if (_Company != String.Empty)
                {
                    //if company is specified - add it to the OFS request
                    secondCommand += "//" + _Company;
                }
            }
            secondCommand += ",";
            secondCommand += GetEscapedID(_TransactionID);

            return secondCommand;
        }

        /// <summary>
        /// Gets the escaped @ID.
        /// </summary>
        /// <param name="transactionID">The transaction ID.</param>
        /// <returns></returns>
        public static string GetEscapedID(string transactionID)
        {
            return OFSMessageConverter.ImportMsgSubstChars(transactionID.Replace("/", "^"));
        }

        /// <summary>
        /// Determine Request type according to typical's base class
        /// </summary>
        /// <param name="typical">Metadata for the typical that is used to determine the message type.</param>
        /// <returns> Request type </returns>
        public static RequestType GetMessageType(MetadataTypical typical)
        {
            switch (typical.BaseClass)
            {
                case T24_Enquiry:
                    return RequestType.Enquiry;
                case T24_Application:
                case T24_Enquiry_Result:
                    return RequestType.Application;
                case T24_AAProduct:
                    return RequestType.AAProduct;
                case T24_DataProcess:
                    return RequestType.DataProcess;
                default:
                    return RequestType.DataProcess;
            }
        }

        /// <summary>
        /// Determine Request type according to typical's base class
        /// </summary>
        /// <param name="typical">Metadata for the typical that is used to determine the message type.</param>
        /// <param name="dpType">Type of the dp.</param>
        /// <returns>Request type</returns>
        public static RequestType GetMessageType(MetadataTypical typical, DataProcessType dpType)
        {
            switch (typical.BaseClass)
            {
                case T24_Enquiry:
                    return RequestType.Enquiry;
                case T24_Application:
                case T24_Enquiry_Result:
                    return RequestType.Application;
                case T24_AAProduct:
                    return RequestType.AAProduct;
                case T24_DataProcess:
                    return RequestType.DataProcess;
                default:
                    return (dpType == DataProcessType.None) ?
                            RequestType.Enquiry :
                            RequestType.DataProcess;
            }
        }

        /// <summary>
        /// Retrieve data prodess from configuration parameter
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static DataProcessType GetDataProcessType(string configuration)
        {
            DataProcessType dpType = DataProcessType.None;
            switch (configuration)
            {
                case "IA": // Create and Authorize
                    dpType = DataProcessType.CreateAndAuthorize;
                    break;
                case "I": // Create only
                    dpType = DataProcessType.Create;
                    break;
                case "R": // Reverse only
                    dpType = DataProcessType.Reverse;
                    break;
                case "S": // See (no Authorization)
                    dpType = DataProcessType.Select;
                    break;
                case "A": // Authorize only
                    dpType = DataProcessType.Authorize;
                    break;
                case "D": // Delete
                    dpType = DataProcessType.Delete;
                    break;
                case "VP": // Verify
                    dpType = DataProcessType.Verify;
                    break;
                case "V": // Validate
                    dpType = DataProcessType.Validate;
                    break;
                case "VI": // Validata and Create
                    dpType = DataProcessType.Create;
                    break;
            }

            return dpType;
        }

        /// <summary>
        /// Generates the search constraint part of an enquiry message.
        /// </summary>
        /// <param name="sb">The StringBuilder object where output is written.</param>
        /// <param name="filters">The array of filters to be applied.</param>
        /// <param name="useQuotesForFields">Specifies if filter criteria will be surrounded by quotes</param>
        public static void GenerateSearchConstraint(StringBuilder sb, IEnumerable<Filter> filters, bool useQuotesForFields = true)
        {
            GenerateSearchConstraint(sb, filters.Select(n => n as ValidataCommon.Interfaces.IFilteringConstraint), useQuotesForFields);
        }

        /// <summary>
        /// Generates the search constraint part of an enquiry message.
        /// </summary>
        /// <param name="sb">The StringBuilder object where output is written.</param>
        /// <param name="filters">The array of filters to be applied.</param>
        /// <param name="useQuotesForFields">Specifies if filter criteria will be surrounded by quotes</param>
        public static void GenerateSearchConstraint(StringBuilder sb, IEnumerable<ValidataCommon.Interfaces.IFilteringConstraint> filters, bool useQuotesForFields = true)
        {
            foreach (ValidataCommon.Interfaces.IFilteringConstraint filter in filters)
            {
                //It is not real filter
                if (filter.Operator == FilterType.TemplateName)
                    continue;

                string filterValue = filter.TestValue;
                GlobusConstraint gcons = GetGlobusConstraint(filter.Operator);
                UpdateGlobusConstraintSpecialCases(ref gcons, ref filterValue);
                filterValue = EvaluateDateTimeVariables(filterValue);

                if(useQuotesForFields)
                {
                    filterValue = OFSMessageConverter.ImportMsgSubstChars(filterValue);
                }

                // If there are spaces - put the value in quotes
                if (filterValue.IndexOf(' ') >= 0 && gcons != GlobusConstraint.RG && gcons != GlobusConstraint.NR && useQuotesForFields)
                    filterValue = string.Format("\"{0}\"", filterValue);

                string attributeName = StripMultiValueNames(filter.AttributeName);

                // append to the message
                sb.AppendFormat(",{0}:{1}={2}", attributeName, gcons.ToString(), filterValue);
            }
        }

        #endregion

        #region Private Methods

        private void UpdateRequestTypeIfAA()
        {
            if (_RequestType == RequestType.DataProcess)
                if (_ResponseTypical != null)
                    if (_ResponseTypical.BaseClass == T24_AAProduct)
                    {
                        _RequestType = RequestType.AAProduct;
                    }
        }

        /// <summary>
        /// Generates the first part of an enquiry OFS message.
        /// </summary>
        /// <param name="sb">The StringBuilder object where to write the output.</param>
        /// <param name="typicalName">The name of the typical for this enquiry.</param>
        /// <param name="isProductionDataImport">Is import data test data mode</param>
        private void GenerateEnquiryStart(StringBuilder sb, string typicalName, bool isProductionDataImport)
        {
            sb.Append("ENQUIRY.SELECT,,");

            if (_Company != String.Empty)
            {
                //if company is specified - add it to the OFS request
                sb.Append("//");
                sb.Append(_Company);
            }

            sb.Append(",");

            string enquiryName = GetEnquiryForApplication(typicalName, isProductionDataImport);
            sb.Append(OFSMessageConverter.ImportMsgSubstChars(enquiryName));
        }

        private string GetEnquiryForApplication(string application, bool isProductionDataImport)
        {
            if (CustomEnquiries != null && CustomEnquiries.ContainsKey(application))
                return CustomEnquiries[application];

            if (isProductionDataImport && !application.StartsWith("%") && application != "OBJECT.MAP.ENQ")
            {
                application = "%" + application;
            }

            return application;
        }

        /// <summary>
        /// Generates the first part of an enquiry OFS message.
        /// </summary>
        /// <param name="sb">The StringBuilder object where to write the output.</param>
        /// <param name="typicalName">The name of the typical for this enquiry.</param>
        /// <param name="isProductionDataImport">Is import data test data mode</param>
        /// <param name="user">T24 User </param>
        /// <param name="password">Password for T24 user</param>
        private void GenerateEnquiryStart(StringBuilder sb, string typicalName, bool isProductionDataImport, string user, string password)
        {
            sb.Append("ENQUIRY.SELECT,,");
            if (!string.IsNullOrEmpty(user))
            {
                sb.Append(user);
            }
            sb.Append("/");
            if (!string.IsNullOrEmpty(password))
            {
                sb.Append(password);
            }
            sb.Append("/");

            if (!string.IsNullOrEmpty(_Company))
            {
                sb.Append(_Company);
            }

            sb.Append(",");

            string enquiryName = GetEnquiryForApplication(typicalName, isProductionDataImport);
            sb.Append(OFSMessageConverter.ImportMsgSubstChars(enquiryName));
        }

        /// <summary>
        /// Translates the AXML constraint to the globus constraint types.
        /// </summary>
        /// <param name="filterType">The AXML filter type.</param>
        /// <returns>The GlobusConstraint that corresponds to the constraint specified in AXML.</returns>
        public static GlobusConstraint GetGlobusConstraint(FilterType filterType)
        {
            switch (filterType)
            {
                case FilterType.GreaterThan:
                    return GlobusConstraint.GT;
                case FilterType.GreaterOrEqual:
                    return GlobusConstraint.GE;
                case FilterType.Equal:
                    return GlobusConstraint.EQ;
                case FilterType.LessOrEqual:
                    return GlobusConstraint.LE;
                case FilterType.Less:
                    return GlobusConstraint.LT;
                case FilterType.Like:
                    return GlobusConstraint.LK;
                case FilterType.NotEqual:
                    return GlobusConstraint.NE;
                case FilterType.NotLike:
                    return GlobusConstraint.UL;
                case FilterType.Between:
                    return GlobusConstraint.BT;
                default:
                    throw new NotSupportedException("The filter " + filterType + " is not supported by T24");
            }
        }

        /// <summary>
        /// Updates the Globus Constraint for special cases.
        /// </summary>
        /// <param name="globusConstraint">The Globus Constraint.</param>
        /// <param name="filterValue">The value of the filter.</param>
        /// <returns>True is the constraint is changed</returns>
        private static bool UpdateGlobusConstraintSpecialCases(ref GlobusConstraint globusConstraint, ref string testValue)
        {
            // TODO: Combine with the one in T24DesktopExecutor.UpdateGlobusConstraintSpecialCases();
            const char specialCasePrefix = '#';

            if (globusConstraint != GlobusConstraint.LK && globusConstraint != GlobusConstraint.UL)
                return false; // do nothing, only constraints with LK/UL operator can be 'special'

            if (String.IsNullOrEmpty(testValue))
                return false;    // specials has special syntax

            // Old way of declaring regions 'RG'
            if (testValue.StartsWith("[") && testValue.EndsWith("]") && testValue.Contains(" "))
            {
                // override the operator to be 'RG'
                globusConstraint = globusConstraint == GlobusConstraint.LK ? GlobusConstraint.RG : GlobusConstraint.NR;
                // extract the region from the original value and put it as test value
                testValue = testValue.Substring(1, testValue.Length - 2);
                return true;
            }

            // Generic T24 operator overrides
            if (testValue[0] == specialCasePrefix)
            {
                if (testValue.Length >= 2 && testValue[1] == specialCasePrefix)
                {
                    // this is escape sequence denoting that this is not special case => remove one of the characters & exit
                    testValue = testValue.Substring(1);
                    return false;
                }

                int idxOfFirstSpace = testValue.IndexOf(' ');
                if (idxOfFirstSpace < 0)
                    return false; // not recognized as special sequence

                GlobusConstraint overrideOperator;
                var overrideOperatorStr = testValue.Substring(1, idxOfFirstSpace - 1);
                if (!Enum.TryParse<GlobusConstraint>(overrideOperatorStr, true, out overrideOperator))
                    return false; // not recognized as special sequence

                // override the operator and the test value
                globusConstraint = overrideOperator;
                testValue = testValue.Substring(idxOfFirstSpace).Trim();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Evaluates the globus date time variables used for filters like {GLOBUS_TODAY} etc.
        /// </summary>
        /// <param name="originalValue">The original value</param>
        /// <returns>The value with the evaluated variables</returns>
        private static string EvaluateDateTimeVariables(string originalValue)
        {
            if (originalValue.Contains("{GLOBUS_TODAY}")
                || originalValue.Contains("{GLOBUS_THIS_MONTH}")
                || originalValue.Contains("{GLOBUS_THIS_YEAR}"))
            {
                DateTime today = DateTime.Today;

                string globusToday = today.Year.ToString().Substring(today.Year.ToString().Length - 2) +
                                     String.Format("{0:D2}", today.Month) +
                                     String.Format("{0:D2}", today.Day) +
                                     "...";

                string globusThisMonth = today.Year.ToString().Substring(today.Year.ToString().Length - 2) +
                                     String.Format("{0:D2}", today.Month) +
                                     "...";

                string globusThisYear = today.Year.ToString().Substring(today.Year.ToString().Length - 2) +
                                     "...";

                originalValue = originalValue.Replace("{GLOBUS_TODAY}", globusToday);
                originalValue = originalValue.Replace("{GLOBUS_THIS_MONTH}", globusThisMonth);
                originalValue = originalValue.Replace("{GLOBUS_THIS_YEAR}", globusThisYear);

                return originalValue;
            }
            else
            {
                return originalValue;
            }
        }

        /// <summary>
        /// Strip "-X~Y"
        /// </summary>
        /// <param name="attrName">attribute name</param>
        /// <returns>Clean attribute name</returns>
        private static string StripMultiValueNames(string attrName)
        {
            if (string.IsNullOrEmpty(attrName))
            {
                return string.Empty;
            }

            return AttributeNameParser.GetShortFieldName(attrName);
        }

        /// <summary>
        /// Generates the start of an input processing OFS message.
        /// </summary>
        /// <param name="sb">The StringBuilder object where output is written.</param>
        /// <param name="commandUser">The user to be specified in the comand.</param>
        /// <param name="commandPassword">The password for this user.</param>
        /// <param name="isValidate">Validate mode</param>
        /// <param name="authenticateAlways">Always authenticate</param>
        private void GenerateInputStart(StringBuilder sb, string commandUser, string commandPassword, bool isValidate, bool authenticateAlways)
        {
            sb.Append(RequestTypicalName);
            sb.Append(",");
            sb.Append(_Version);
            sb.Append("/");
            if (_DataProcessType == DataProcessType.Select)
            {
                sb.Append("S");
            }
            else if (_DataProcessType == DataProcessType.Delete)
            {
                sb.Append("D");
            }
            else if (_DataProcessType == DataProcessType.Reverse)
            {
                sb.Append("R");
            }
            else if (_DataProcessType == DataProcessType.Verify)
            {
                sb.Append("V");
            }

            if (isValidate || _DataProcessType == DataProcessType.Validate)
            {
                sb.Append("/VALIDATE");
            }
            else
            {
                sb.Append("/PROCESS");
            }
            

            sb.Append(",");
            if (_DataProcessType == DataProcessType.Select ||
                _DataProcessType == DataProcessType.Delete ||
                _DataProcessType == DataProcessType.Reverse ||
                _DataProcessType == DataProcessType.Verify 
                //|| _DataProcessType == DataProcessType.Create
                )
            {
                if (authenticateAlways)
                {
                    sb.Append(commandUser);
                    sb.Append("/");
                    sb.Append(commandPassword);
                    if (_Company != String.Empty)
                    {
                        //if company is specified - add it to the OFS request
                        sb.Append("/");
                        sb.Append(_Company);
                    }
                }
                else
                {
                    //for select,delete and reverse we do not apply user and password
                    if (_Company != String.Empty)
                    {
                        //if company is specified - add it to the OFS request
                        sb.Append("//");
                        sb.Append(_Company);
                    }
                }
            }
            else
            {
                sb.Append(commandUser);
                sb.Append("/");
                sb.Append(commandPassword);
                if (_Company != String.Empty)
                {
                    //if company is specified - add it to the OFS request
                    sb.Append("/");
                    sb.Append(_Company);
                }
            }
            sb.Append(",");
        }

        /// <summary>
        /// Generates the start of an application call OFS message.
        /// </summary>
        /// <param name="applicationName">The name of the application.</param>
        /// <param name="sb">The StringBuilder object where output is written.</param>
        /// <param name="commandUser">The name of the command user.</param>
        /// <param name="commandPassword">The password for this user.</param>
        /// <param name="isValidate">Validate mode</param>
        private static void GenerateApplicationStart(string applicationName, StringBuilder sb,
                                                     string commandUser, string commandPassword, bool isValidate)
        {
            sb.Append(applicationName);
            sb.Append(",");
            if (isValidate)
            {
                sb.Append("//VALIDATE");
            }
            else
            {
                sb.Append("//PROCESS");
            }
            sb.Append(commandUser);
            sb.Append("/");
            sb.Append(commandPassword);
            sb.Append(",,");
        }

        /// <summary>
        /// Generates an enquiry OFS message
        /// </summary>
        /// <param name="filters">The filters to be used as constraints.</param>
        /// <returns>The text of the OFS enquiry message.</returns>
        /// <param name="isProductionDataImport">Is in import Test Data mode</param>
        private string GenerateEnquiryMessage(IEnumerable<Filter> filters, bool isProductionDataImport)
        {
            return GenerateEnquiryMessage(null, null, filters, isProductionDataImport);
        }

        /// <summary>
        /// Generates the enquiry message.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        /// <param name="filters">The filters.</param>
        /// <param name="isProductionDataImport">if set to <c>true</c> [is imp test data].</param>
        /// <returns></returns>
        private string GenerateEnquiryMessage(string user, string password, IEnumerable<Filter> filters, bool isProductionDataImport)
        {
            StringBuilder ofsCommand = new StringBuilder();

            string requestTypicalName = RequestTypicalName;
            if (_ResponseTypical.BaseClass == T24_AAProduct)
            {
                requestTypicalName = "%AA.ARRANGEMENT.ACTIVITY";
                List<Filter> filters1 = new List<Filter>(filters);
                if (filters1.Find(n => n.AttributeName == "PRODUCT") == null)
                {
                    // no filtering constraint added for PRODUCT, thus we will use the typical name which is equal to the product name
                    filters1.Add(new Filter("PRODUCT", _ResponseTypical.Name, AttributeDataType.String, FilterType.Equal));
                }

                filters = filters1;
            }

            StringBuilder sbc = new StringBuilder();
            if (user == null && password == null)
            {
                GenerateEnquiryStart(sbc, requestTypicalName, isProductionDataImport);
            }
            else
            {
                GenerateEnquiryStart(sbc, requestTypicalName, isProductionDataImport, user, password);
            }

            GenerateSearchConstraint(sbc, filters);
            ofsCommand.Append(sbc.ToString());

            return ofsCommand.ToString();
        }

        /// <summary>
        /// Generates an OFS message that calls a Globus application.
        /// </summary>
        /// <param name="commandUser">The name of the user to be specified in the OFS message.</param>
        /// <param name="commandPassword">The password for the user.</param>
        /// <param name="isValidate">Is Validate command.</param>
        /// <returns>The text of the OFS message.</returns>
        private string GenerateApplicationCallMessage(string commandUser, string commandPassword, bool isValidate)
        {
            StringBuilder ofsCommand = new StringBuilder();

            GenerateApplicationStart(RequestTypicalName, ofsCommand, commandUser, commandPassword, isValidate);

            if (Instance == null &&
                _RequestTypical.BaseClass == T24_Enquiry_Result &&
                _RequestType == RequestType.Application)
            {
                throw new Exception("T24 Enquiry not defined for 'T24_Enquiry_Result' typical");
            }

            // Add values from the instance to the OFS
            foreach (InstanceAttribute attr in Instance.Attributes)
            {
                if (attr.Name != "ID" && !(attr.Value == "No Value" || attr.Value == String.Empty))
                {
                    ofsCommand.Append(",");
                    FormatFieldNameAndValue(ofsCommand, attr.Name, attr.Value);
                }
            }

            return ofsCommand.ToString();
        }

        /// <summary>
        /// Determines if a string value is equivalent to the special Validata value 'No Value'
        /// </summary>
        /// <param name="fieldValue">The string value.</param>
        /// <returns>True if the string is equivalent to 'No Value'</returns>
        private static bool IsNoValue(string fieldValue)
        {
            //NOTE: Trimming for safety
            return fieldValue == null ||
                    fieldValue.Trim() == string.Empty ||
                    string.Compare(fieldValue.Trim(), "No Value", true) == 0;
        }

        /// <summary>
        /// Generates a data create or update OFS message.
        /// </summary>
        /// <param name="commandUser">The name of the user to be specified in the OFS message.</param>
        /// <param name="commandPassword">The password for the user.</param>
        /// <param name="isValidate">Validate mode</param>
        /// <param name="authenticateAlways">Always authenticate</param>
        /// <param name="msgId">Message ID. Required in case of use of Post OFS and Batch file listener adapters</param>
        /// <returns>The text of the OFS message.</returns>
        private string GenerateDataCreateOrUpdateMessage(string commandUser, string commandPassword, bool isValidate, bool authenticateAlways, string msgId = "")
        {
            if (_Instance == null)
            {
                throw new ApplicationException("Instance not set for a data process command!");
            }

            StringBuilder ofsCommand = new StringBuilder();

            GenerateInputStart(ofsCommand, commandUser, commandPassword, isValidate, authenticateAlways);

            // add ID for see and delete commands
            if (_DataProcessType == DataProcessType.Select ||
                _DataProcessType == DataProcessType.Delete ||
                _DataProcessType == DataProcessType.Reverse ||
                _DataProcessType == DataProcessType.Verify)
            {
                string id = _Instance.GetAttributeValue("@ID").Trim();
                ofsCommand.Append(GetEscapedID(id));
            }
            else
            {
                if (RequestTypicalName == LD_LOAN)
                {
                    // this is an LD loand and not a SEE, DELETE or REVERSE
                    return GenerateLDLoan(ofsCommand);
                }
                else if (RequestTypicalName == FX_SWAP && !_FxIsFlat)
                {
                    // special handling for FX SWAP -> not a traditional SEE, DELETE or REVERSE
                    return GenerateFXSwap(ofsCommand);
                }

                if ((_Instance.GetAttributeValue("@ID") != null) &&
                    (_Instance.GetAttributeValue("@ID").Trim().Length > 0))
                {
                    //If ID is specified use it for input commands
                    string id = _Instance.GetAttributeValue("@ID").Trim();
                    if (!IsNoValue(id))
                    {
                        ofsCommand.Append(GetEscapedID(id));
                    }
                }

                if(!string.IsNullOrEmpty(msgId))
                {
                    ofsCommand.Append("/" + msgId);
                }

                // In case of record definition and application call, we need to add values to the OFS message
                foreach (InstanceAttribute attr in _Instance.Attributes)
                {
                    if (attr.Name == "ID" || attr.Name == "@ID" || (IsNoValue(attr.Value)))
                    {
                        continue;
                    }

                    ofsCommand.Append(",");
                    FormatFieldNameAndValue(ofsCommand, attr.Name, attr.Value);
                }
            }

            return ofsCommand.ToString();
        }

        /// <summary>
        /// Generates an LD Loans OFS command
        /// </summary>
        /// <param name="ofsCommand">A string builder to be used to create this OFS message. </param>
        /// <returns>The OFS message text</returns>
        private string GenerateLDLoan(StringBuilder ofsCommand)
        {
            // add ID for see and delete commands
            if ((_Instance.GetAttributeValue("@ID") != null) &&
                (_Instance.GetAttributeValue("@ID").Trim().Length > 0))
            {
                //If ID is specified use it for input commands
                string id = _Instance.GetAttributeValue("@ID").Trim();
                if (!IsNoValue(id))
                {
                    ofsCommand.Append(GetEscapedID(id));
                }
            }

            InstanceAttributesCollection scheduleAttributes = new InstanceAttributesCollection();

            bool hasSchedule = false;
            // In case of record definition and application call , some
            // values need to be specified
            foreach (InstanceAttribute attr in _Instance.Attributes)
            {
                if (attr.Name.StartsWith(LD_SCHEDULE))
                {
                    //collect the attributes of the schedule and first add all ld loan attributes
                    scheduleAttributes.Add(attr);
                    continue;
                }
                if (attr.Name != "ID" && attr.Name != "@ID" && (!IsNoValue(attr.Value)))
                {
                    if (attr.Name == SCHEDULE_FLAG)
                    {
                        // this is the field that controls whether a schedule should be added
                        if (SCHEDULE_INDICATOR_VALUES.Contains(attr.Value.Trim().ToUpper()))
                        {
                            //if the attribute has the special value that schedule must be defined
                            hasSchedule = true;
                        }
                    }
                    // all typical attributes should have either full name i.e. NAME-x~y or only NAME
                    // if the attribute has only fieldNum i.e. Name-x then it is skipped						
                    ofsCommand.Append(",");
                    FormatFieldNameAndValue(ofsCommand, attr.Name, attr.Value);
                }
            }

            if (hasSchedule)
            {
                //this is the delimiter for the schedule section
                ofsCommand.Append(LD_DELIM);
                bool firstAttribute = true;
                foreach (InstanceAttribute attr in scheduleAttributes)
                {

                    string attrScheduleName = attr.Name.Remove(0, LD_SCHEDULE.Length + 1);
                    if (attrScheduleName != "ID" && attrScheduleName != "@ID" && (!IsNoValue(attr.Value)))
                    {
                        if (firstAttribute)
                        {
                            firstAttribute = false;
                        }
                        else
                        {
                            ofsCommand.Append(",");
                        }

                        //remove the prefix LD.SCHEDULE.DEFINE.
                        FormatFieldNameAndValue(ofsCommand, attrScheduleName, attr.Value);
                    }
                }
            }


            return ofsCommand.ToString();
        }

        /// <summary>
        /// Generates an FX Swap OFS command
        /// </summary>
        /// <param name="ofsCommand">A string builder to be used to create this OFS message. </param>
        /// <returns>The OFS message text</returns>
        private string GenerateFXSwap(StringBuilder ofsCommand)
        {
            // add ID for see and delete commands
            if ((this._Instance.GetAttributeValue("@ID") != null) &&
                (this._Instance.GetAttributeValue("@ID").Trim().Length > 0))
            {
                //If ID is specified use it for input commands
                string id = this._Instance.GetAttributeValue("@ID").Trim();
                if (!IsNoValue(id))
                {
                    ofsCommand.Append(GetEscapedID(id));
                }
            }

            Dictionary<string, string> firstLegAttributes = new Dictionary<string, string>();
            Dictionary<string, string> secondLegAttributes = new Dictionary<string, string>();

            // In case of record definition and application call , some values need to be specified

            //_Instance.ReorderAttributes(this.RequestTypical);

            foreach (InstanceAttribute attr in _Instance.Attributes)
            {
                if (attr.Name.StartsWith(FX_FIRST_LEG))
                {    //collect the attributes of the first leg
                    string realName = attr.Name.Remove(0, FX_FIRST_LEG.Length);
                    if (realName != "ID" && realName != "@ID" && (!IsNoValue(attr.Value)))
                    {
                        firstLegAttributes[realName] = attr.Value;
                    }
                    continue;
                }
                else if (attr.Name.StartsWith(FX_SECOND_LEG))
                {    //collect the attributes of the first leg
                    string realName = attr.Name.Remove(0, FX_SECOND_LEG.Length);
                    if (realName != "ID" && realName != "@ID" && (!IsNoValue(attr.Value)))
                    {
                        secondLegAttributes[realName] = attr.Value;
                    }
                    continue;
                }

                if (attr.Name != "ID" && attr.Name != "@ID" && (!IsNoValue(attr.Value)))
                {
                    //for fx swap it is expected that all attributes are either part of leg1 or leg2
                    ofsCommand.Append(",");
                    string validataName = attr.Name;
                    string fieldValue = attr.Value;
                    FormatFieldNameAndValue(ofsCommand, validataName, fieldValue);
                }
            }

            //first we select all attribute values for first leg and group them with second leg
            foreach (string attrName in firstLegAttributes.Keys)
            {
                string attrFirstLegValue = firstLegAttributes[attrName];
                if (attrName != "ID" && attrName != "@ID" && (!IsNoValue(attrFirstLegValue)))
                {
                    ofsCommand.Append(",");
                    if (secondLegAttributes.ContainsKey(attrName))
                    {
                        //we have values for this attribute in both legs, so we have to merge the values
                        string attrSecondLegValue = secondLegAttributes[attrName];

                        string fieldValue = attrFirstLegValue + "_" + attrSecondLegValue;
                        FormatFieldNameAndValue(ofsCommand, attrName, fieldValue);
                    }
                    else
                    {
                        //we have values for this attribute only for the first leg
                        FormatFieldNameAndValue(ofsCommand, attrName, attrFirstLegValue);
                    }
                }
            }

            //second we add all attributes of the second leg that are not part of the first leg
            foreach (string attrName in secondLegAttributes.Keys)
            {
                string attrSecondLegValue = secondLegAttributes[attrName];
                if (attrName != "ID" && attrName != "@ID" && (!IsNoValue(attrSecondLegValue)))
                {
                    if (!firstLegAttributes.ContainsKey(attrName))
                    {
                        //we are adding only attributes that exist only in the second leg, the other attributes have been added on the previous step
                        ofsCommand.Append(",");
                        //these are values that exist only in the second leg
                        string fieldValue = "_" + attrSecondLegValue;
                        FormatFieldNameAndValue(ofsCommand, attrName, fieldValue);
                    }

                }
            }

            return ofsCommand.ToString();
        }

        private static void PrepareInstanceAttributeValues(Instance instance, MetadataTypical typical)
        {
            if (instance == null || typical == null)
            {
                return;
            }

            Dictionary<string, TypicalAttribute> attributesDict = typical.AttributesByName;
            List<string> missingAttributes = new List<string>();

            foreach (InstanceAttribute attribute in instance.Attributes)
            {
                TypicalAttribute typicalAttribute;
                if (attributesDict.TryGetValue(attribute.Name, out typicalAttribute))
                {
                    if (typicalAttribute.Type == AttributeDataType.DateTime)
                    {
                        DateTime dt = ValidataConverter.GetDateTime(attribute.Value);
                        if (dt == DateTime.MinValue)
                        {
                            attribute.Value = "No Value";
                        }
                        else
                        {
                            attribute.Value = dt.ToString("yyyyMMdd");
                        }
                    }
                }
                else
                {
                    missingAttributes.Add(attribute.Name);
                }
            }

            if (missingAttributes.Count > 0)
            {
                Debug.WriteLine(string.Format("Can't find {0} attributes in '{1}': {2}",
                    missingAttributes.Count, typical.Name, string.Join(",", missingAttributes.ToArray())));
                //Debug.Fail(string.Format("Can't find {0} attributes in '{1}': {2}",
                //    missingAttributes.Count, typical.Name, string.Join(",", missingAttributes.ToArray())));
            }
        }

        /// <summary>
        /// Formats a Validata field name as a Globus OFS field selector
        /// </summary>
        /// <param name="ofsCommand">The string builder where the results must be stored</param>
        /// <param name="validataAttributeName">The name of the attribute in Validata. It could be simple as NAME or complex as NAME-1~1</param>
        /// <param name="fieldValue">The value of the field</param>
        private static void FormatFieldNameAndValue(StringBuilder ofsCommand, string validataAttributeName, string fieldValue)
        {
            string fieldName;
            int multiValueIndex, subValueIndex;
            bool isMultivalue = AttributeNameParser.ParseComplexFieldName(validataAttributeName, out fieldName, out multiValueIndex, out subValueIndex);

            // prepare the field name
            ofsCommand.Append(fieldName);
            ofsCommand.Append(':');
            if (isMultivalue)
            {
                ofsCommand.Append(multiValueIndex);
            }
            ofsCommand.Append(':');
            if (isMultivalue)
            {
                ofsCommand.Append(subValueIndex);
            }

            ofsCommand.Append("=");
            ofsCommand.Append(OFSMessageConverter.ImportMsgSubstChars(fieldValue, true));
        }

        private static string MergeFxSwapLegs(string firstLeg, string secondLeg)
        {
            //string startTag = "<" + _ResponseTypical.Name + ">";
            //string endTag = "<" + _ResponseTypical.Name + "/>";
            //this._Result = firstLeg.Replace(endTag, string.Empty);
            //secondLeg = secondLeg.Replace(startTag, string.Empty);
            ////now connect the two strings
            //this._Result += secondLeg;

            Instance instanceFirstLeg = Instance.FromXmlString(firstLeg);
            Instance instanceSecondLeg = Instance.FromXmlString(secondLeg);

            foreach (InstanceAttribute ia in instanceSecondLeg.Attributes)
            {
                if (ia.Name == "@ID")
                {
                    continue;
                }

                instanceFirstLeg.AddAttribute(ia);
            }

            return instanceFirstLeg.ToXmlString();
        }

        #endregion

        #region Protected Methods

        protected void LogParsingExceptionInfo(string response, Exception ex)
        {
            LogExceptionInfo("Failed parsing T24 response!", response, ex);
        }

        protected void LogExceptionInfo(string prefix, string response, Exception ex)
        {
            _Result = Utils.GetHtmlEncodedValue(prefix);

            if (response != null)
            {
                _Result +=
                    Utils.GetHtmlEncodedValue(
                        ((response.Length < 500) ? response : (response.Substring(0, 497) + "..."))
                        );
            }
            else if (string.IsNullOrEmpty(prefix))
            {
                System.Diagnostics.Debug.Fail("Why we have no prefix and the response message is empty? Nothing to tell to user!!!");
                _Result += Utils.GetHtmlEncodedValue("Unknown error during communication");
            }

            _State = ExecutionState.Finished;
            _Failed = true;

            EventLogger.LogEvents(Thread.CurrentThread.Name,
                                  GlobusEventType.Crash,
                                  string.Format("SetResponse Failure: {0} Response: [{1}] Response Typical: [{2}] ",
                                                ex,
                                                response,
                                                ResponseTypical == null ? "<none>" : ResponseTypical.Name));
        }

        protected ExecutionState SetOfsDataProcessResponse(OFSResponseReaderBase reader, GlobusMessage.OFSMessage responseOfsMessage, MetadataTypical responseTypical)
        {
            ExecutionState state;

            //this is the reponse of a processing command
            string resMessage = reader.ReadOFS(responseOfsMessage, responseTypical, false, null);

            _CURRNO = reader.CURRNO;

            if (reader.HasError) // error
            {
                _Result = Utils.GetHtmlEncodedValue(resMessage);
                _Failed = true;

                state = ExecutionState.Finished;
            }
            else
            {
                _Result = resMessage;
                _TransactionID = reader.TransactionID;

                //Teo was wrong - we don't need the @ID of the second leg
                //for the authorize message
                if (responseTypical.Name == FX_SWAP)
                    if (_DataProcessType == DataProcessType.CreateAndAuthorize)
                        if (_FxAuthorizeFirstLeg)
                        {
                            _TransactionID = reader.FxSecondLegID;
                        }

                state = ExecutionState.Processed;
            }

            return state;
        }

        protected void SetOfsAuthorizationResponse(OFSResponseReaderBase reader, GlobusMessage.OFSMessage responseOfsMessage)
        {
            bool isAuthorize = (_DataProcessType == DataProcessType.Authorize) ||
                               (_DataProcessType == DataProcessType.CreateAndAuthorize);

            string resMessage = reader.ReadOFS(responseOfsMessage, _ResponseTypical, isAuthorize, null);

            if (reader.HasError) // error
            {
                _Result = (reader.TransactionID != null) ?
                    string.Format("{0} ID={1}", resMessage, reader.TransactionID) :
                    resMessage;


                _Failed = true;
            }
            else
            {
                _Result = resMessage;

                if (_ResponseTypical.Name == FX_SWAP && !_FxIsFlat)
                {
                    //this is an authorize command of an fx swap 
                    //check if there was an insert before that 
                    if (_DataProcessType == DataProcessType.CreateAndAuthorize)
                    {
                        //we have to merge the result of the second leg with the 
                        //result of the first leg
                        _Result = MergeFxSwapLegs(_Result, resMessage);
                    }
                    else //this was only an authorize command
                    {
                        //we will have only the second leg filled but we anyway don't have 
                        //information for the first leg
                        _Result = resMessage;
                    }
                }
            }
        }

        #endregion
    }
}
