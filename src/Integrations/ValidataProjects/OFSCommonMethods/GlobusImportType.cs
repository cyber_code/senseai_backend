﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods
{
    public enum GlobusImportType
    {
        TESTDATA,
        LIVE,
        HIS,
        MULTI_ENQ
    }
}
