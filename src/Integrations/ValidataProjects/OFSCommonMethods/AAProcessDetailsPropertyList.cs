﻿using System;
using System.Collections.Generic;
using System.Text;
using AXMLEngine;

namespace OFSCommonMethods
{
    internal class AAProcessDetails
    {
        internal readonly string VersionFull;

        internal readonly string Action;

        internal readonly string ID;

        internal readonly string Unknown;

        internal AAProcessDetails(string versionFull, string action, string id, string unknown)
        {
            VersionFull = versionFull;
            Action = action;
            ID = id;
            Unknown = unknown;
        }

        internal AAProcessDetails(AAProcessDetails src)
        {
            VersionFull = src.VersionFull;
            Action = src.Action;
            ID = src.ID;
            Unknown = src.Unknown;
        }

        internal static List<AAProcessDetails> SplitProcessesDetails(string processDetails)
        {
            List<AAProcessDetails> result = new List<AAProcessDetails>();

            string[] arr = processDetails.Split(new char[] { ' ' });

            for(int i = 0; i < arr.Length; i+=4)
            {
                int currIdx = i;
                string version = arr.Length > currIdx ? arr[currIdx].Trim() : null;

                currIdx++;
                string action = arr.Length > currIdx ? arr[currIdx].Trim() : null;

                currIdx++;
                string id = arr.Length > currIdx ? arr[currIdx].Trim() : null;

                currIdx++;
                string unknown = arr.Length > currIdx ? arr[currIdx].Trim() : null;

                result.Add(new AAProcessDetails(version, action, id, unknown));
            }

            return result;
        }
    }

    internal class AAProcessDetailsProperty : AAProcessDetails
    {
        internal readonly int IndexMV;

        internal readonly int IndexSV;

        internal readonly string Property;

        internal string Application
        {
            get
            {
                if (VersionFull == null)
                {
                    return "";
                }
                int pos = VersionFull.IndexOf(',');
                return pos < 0 ? VersionFull : VersionFull.Substring(0, pos);
            }
        }

        internal string Version
        {
            get
            {
                if (VersionFull == null)
                {
                    return "";
                }
                int pos = VersionFull.IndexOf(',');
                return pos < 0 ? "" : VersionFull.Substring(pos + 1);
            }
        }

        internal AAProcessDetailsProperty(int mvIndex, int svIndex, string property, AAProcessDetails processDetails)
            : base (processDetails)
        {
            this.IndexMV = mvIndex;
            this.IndexSV = svIndex;
            this.Property = property;
        }

        public override string ToString()
        {
            return string.Format("{0}.{1} {2} {3} {4} {5} {6}", IndexMV, IndexSV, Property, VersionFull, Action, ID, Unknown);
        }
    }

    internal class AAProcessDetailsPropertyList : List<AAProcessDetailsProperty>
    {
        internal static AAProcessDetailsPropertyList FromInstance(Instance instance)
        {
            AAProcessDetailsPropertyList result = new AAProcessDetailsPropertyList();

            SortedDictionary<int, SortedDictionary<int, string>> propertyes =
                new SortedDictionary<int, SortedDictionary<int, string>>();

            SortedDictionary<int, SortedDictionary<int, string>> processDetails =
                new SortedDictionary<int, SortedDictionary<int, string>>();

            foreach (InstanceAttribute ia in instance.Attributes)
            {
                string shortName;
                int mvIndex, svIndex;
                ValidataCommon.AttributeNameParser.ParseComplexFieldName(ia.Name, out shortName, out mvIndex, out svIndex);

                if (shortName == "PROPERTY")
                {
                    if(!propertyes.ContainsKey(mvIndex))
                    {
                        propertyes[mvIndex] = new SortedDictionary<int, string>();
                    }

                    propertyes[mvIndex][svIndex] = ia.Value;
                }

                if (shortName == "PROCESS.DETAILS")
                {
                    if (!processDetails.ContainsKey(mvIndex))
                    {
                        processDetails[mvIndex] = new SortedDictionary<int, string>();
                    }

                    processDetails[mvIndex][svIndex] = ia.Value;
                }
            }

            foreach (int iMV in propertyes.Keys)
            {
                if (!processDetails.ContainsKey(iMV))
                {
                    System.Diagnostics.Debug.Fail("???? - Just discard?");
                    continue;
                }

                foreach (int iSV in processDetails[iMV].Keys)
                {
                    List<AAProcessDetails> lsProcessesDetails =
                        AAProcessDetails.SplitProcessesDetails(processDetails[iMV][iSV]);

                    foreach (AAProcessDetails aapd in lsProcessesDetails)
                    {
                        string propertyName = string.Empty;
                        if (propertyes[iMV].ContainsKey(iSV))
                        {
                            propertyName = propertyes[iMV][iSV];
                        }
                        else
                        {
                            foreach (var val in propertyes[iMV].Values)
                            {
                                propertyName = val;
                                break;
                            }
                        }
                        result.Add(new AAProcessDetailsProperty(iMV, iSV, propertyName, aapd));
                    }
                }
            }

            return result;
        }
    }
}
