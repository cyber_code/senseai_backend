using System;
using AXMLEngine;
using OFSCommonMethods.IO;

namespace OFSCommonMethods
{
    /// <summary>
    /// Represents the state of a Globus request
    /// </summary>
    public class GlobusRequestShort
    {

        #region Private Members

        /// <summary>
        /// The GUID.
        /// </summary>
        private string _GUID = String.Empty;

        /// <summary>
        /// The Globus version number. Used for history processing.
        /// </summary>
        private string _CURRNO = String.Empty;

        /// <summary>
        /// The company
        /// </summary>
        private string _GlobusID = String.Empty;

        /// <summary>
        /// The ValidataID
        /// </summary>
        private string _ValidataID = String.Empty;

        /// <summary>
        /// Autorization is needed
        /// </summary>
        private bool _NeedsAuthorize = false;

        /// <summary>
        /// State of the request
        /// </summary>
        private GlobusRequest.ExecutionState _ExecutionState = GlobusRequest.ExecutionState.Initial;

        private GlobusRequest.RequestType _RequestType = GlobusRequest.RequestType.DataProcess;

        private DataProcessType _DataProcessType = DataProcessType.None;

        private string _TransactionID = String.Empty;

        private bool _Failed = false;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobusRequestShort"/> class.
        /// </summary>
        /// <param name="instance">The AXML instance that this request was created for.</param>
        public GlobusRequestShort(Instance instance)
        {
            _ValidataID = instance.ID;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobusRequestShort"/> class.
        /// </summary>
        /// <param name="validataUID">The validata UID.</param>
        public GlobusRequestShort(int validataUID)
        {
            _ValidataID = validataUID.ToString();
        }

        /// <summary>
        /// Create a GlobusRequest short object instance.
        /// </summary>
        /// <param name="request">Common Globus Request.</param>
        public GlobusRequestShort(GlobusRequest request)
        {
            if (request.Instance != null && request.Instance.ID != String.Empty)
            {
                _ValidataID = request.Instance.ID;
            }

            SetShortResponseProperties(request);
        }

        #endregion

        #region Public methods

        public void SetShortResponseProperties(GlobusRequest globusRequest)
        {
            _CURRNO = globusRequest.CURRNO;
            _DataProcessType = globusRequest.ProcessingType;
            _ExecutionState = globusRequest.State;
            _TransactionID = globusRequest.TransactionID;
            _RequestType = globusRequest.TypeOfRequest;
            _Failed = globusRequest.Failed;

            _NeedsAuthorize = globusRequest.NeedsAuthorize;            

            if (globusRequest.Instance != null)
            {
                _GlobusID = globusRequest.Instance.GetAttributeValue("@ID");

                if (_ValidataID == String.Empty)
                {
                    _ValidataID = globusRequest.Instance.GetAttributeValue("ID");
                }
            }
        }

        public string SetResponse(GlobusRequest globusRequest, GlobusMessage resultMessage, bool treatOfsResponseSingleValueAsError, Settings.OFSType ofsType)
        {
            globusRequest.FirstMessage = resultMessage.Message.Text;

            globusRequest.NeedsAuthorize = _NeedsAuthorize;

            globusRequest.SetResponse(
                    resultMessage.Message, 
                    globusRequest.ResponseTypical, 
                    null, 
                    null, 
                    treatOfsResponseSingleValueAsError,
                    EnquiryResultParsingRotine.None,
                    ofsType
                );

            SetShortResponseProperties(globusRequest);

            return globusRequest.Result;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The Globus version number. Used for history processing.
        /// </summary>
        public string GUID
        {
            get { return _GUID; }
            set { _GUID = value; }
        }

        /// <summary>
        /// The Globus version number. Used for history processing.
        /// </summary>
        public string CURRNO
        {
            get { return _CURRNO; }
            set { _CURRNO = value; }
        }

        /// <summary>
        /// The Globus @ID
        /// </summary>
        public string GlobusID
        {
            get { return _GlobusID; }
            set { _GlobusID = value; }
        }

        /// <summary>
        /// The execution state
        /// </summary>
        public GlobusRequest.ExecutionState ExecutionState
        {
            get { return _ExecutionState; }
            set { _ExecutionState = value; }
        }

        /// <summary>
        /// The Request Type
        /// </summary>
        public GlobusRequest.RequestType RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }

        /// <summary>
        /// The Request Type
        /// </summary>
        public DataProcessType DataProcessType
        {
            get { return _DataProcessType; }
            set { _DataProcessType = value; }
        }

        public string TransactionID
        {
            get { return _TransactionID; }
            set { _TransactionID = value; }
        }

        public bool Failed
        {
            get { return _Failed; }
            set { _Failed = value; }
        }

        /// <summary>
        /// The Validata ID
        /// </summary>
        public string ValidataID
        {
            get { return _ValidataID; }
            set { _ValidataID = value; }
        }

        /// <summary>
        /// True if the request needs also to be authorized in Globus.
        /// </summary>
        public bool NeedsAuthorize
        {
            get { return _NeedsAuthorize; }
            set { _NeedsAuthorize = value; }
        }

        #endregion

        public override string ToString()
        {
            // for the debugger

            string res = RequestType + " " + DataProcessType;

            if (!string.IsNullOrEmpty(GlobusID))
                res += " @ID=" + GlobusID;

            if (!string.IsNullOrEmpty(ValidataID))
                res += " V_ID=" + ValidataID;            
            
            if (!string.IsNullOrEmpty(CURRNO))
                res += " CurrNo=" + CURRNO;

            if (_Failed)
                res += " (Failed)";

            return res;
        }
    }
}