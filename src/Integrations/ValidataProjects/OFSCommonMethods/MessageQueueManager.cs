using System;
using System.Collections;
using System.Security.Principal;
using System.Threading;
using OFSCommonMethods.IO;
using System.Configuration;

namespace OFSCommonMethods
{
    /// <summary>
    /// MessageQueueManager is designed to manage the process of writing OFS
    /// message to Globus and retrieving their responses. The thread-safety is not perfect
    /// but there is no practical case for having competing Globus Adapters messing with one 
    /// another
    /// </summary>
    public class MessageQueueManager
    {
        #region Private Members

        /// <summary>
        /// A reference to a GlobusIOManager instance. 
        /// </summary>
        private IOManager _IOManager = null;

        /// <summary>
        /// The windows identity to be used when executing the listening data and authorization threads.
        /// </summary>
        private WindowsIdentity _AdapterIdentity;

        /// <summary>
        /// Stop threads flag
        /// </summary>
        private bool _StopThreads = false;

        /// <summary>
        /// Gap Interval Between Write & Read (in mS)
        /// </summary>
        private int _ListenSleepDuration = -1;

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets Gap Interval Between Write & Read (in mS)
        /// </summary>
        private int ListenSleepDuration
        {
            get
            {
                if (_ListenSleepDuration == -1)
                {
                        _ListenSleepDuration = 100; // default
                }

                return _ListenSleepDuration;
            }
        }

        #endregion

        #region Public Properties

        public bool StopThreads
        {
            set { _StopThreads = value; }
        }

        /// <summary>
        /// Defines whether we are executing T24 unit test step
        /// </summary>
        public bool IsT24UnitTestExecution { get; set; }

        /// <summary>
        /// Settings
        /// </summary>
        public readonly Settings Settings = null;

        #endregion

        #region Class Lifecycle

        public MessageQueueManager(Settings settings, WindowsIdentity adapterIdentity, string id)
            : this(settings, adapterIdentity, id, string.Empty)
        {
        }

        public MessageQueueManager(Settings settings, WindowsIdentity adapterIdentity, string id, string testStepId = "")
        {
            //Create the Input/Output Manager
            _IOManager = IOFactory.Create(settings, id, testStepId);

            //Set the adapter impersonation identity
            _AdapterIdentity = adapterIdentity;

            _StopThreads = false;

            this.Settings = settings;
        }

        public MessageQueueManager(Settings settings, WindowsIdentity adapterIdentity, string id,
                                   string ofsRequestExtension, string ofsResponseExtension, string testStepId = "")
            :this(settings, adapterIdentity, id, testStepId)
		{

            System.Diagnostics.Debug.Assert(_IOManager != null, 
                    "Should be initialized in executed constructor"
                );
            if (_IOManager.Type == Settings.IOType.FileBased)
            {
                ((FileBasedIOManager)_IOManager).SetExtensions(ofsRequestExtension, ofsResponseExtension);
            }
		}

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieves the response times for all messages sent to Globus. 
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="average"></param>
        /// <param name="unanswered"></param>
        /// <returns>An ArrayList with long integers representing response times in milliseconds.</returns>
        public ArrayList GetMessageResponseTimes(ref long min, ref long max, ref long average, ref int unanswered)
        {
            ArrayList responseTimes = new ArrayList();

            min = long.MaxValue;
            max = long.MinValue;
            long sum = 0;
            average = 0;

            foreach (object timeVal in this._IOManager.MessageTimes.Values)
            {
                if (timeVal is long)
                {
                    long ticksForMessage = (long)timeVal;

                    TimeSpan ts = new TimeSpan(ticksForMessage);
                    //add the number of milliseconds for this message
                    long mills = (long)ts.TotalMilliseconds;
                    responseTimes.Add(mills);

                    if (mills > max)
                    {
                        max = mills;
                    }

                    if (mills < min)
                    {
                        min = mills;
                    }

                    sum += mills;
                }
                else
                {
                    //we don't have a valid entry for this message or it has only 
                    //write time (DateTime object) for this message
                    unanswered++;
                }
            }

            if (max == long.MinValue)
            {
                //there was not a valid entry so the start value was preserved
                max = 0;
            }

            if (min == long.MaxValue)
            {
                //there was not a valid entry so the start value was preserved
                min = 0;
            }

            if (responseTimes.Count > 0)
            {
                average = sum / responseTimes.Count;
            }
            else
            {
                //it may happen due to some error condition that no messages were successfully written and read to Globus
                average = 0;
            }

            return responseTimes;
        }

        /// <summary>
        /// Reads Globus responses on the authorize phantom
        /// </summary>
        public void AuthorizeMessageReader()
        {
            _AdapterIdentity.Impersonate();

            try
            {
                EventLogger.LogEvents("Listen Authorize Thread", GlobusEventType.ExecPointReached, "Authorize Thread Started");

                while (!_StopThreads)
                {
                    this._IOManager.ReadResults(true);
                    Thread.Sleep(ListenSleepDuration);
                }

                EventLogger.LogEvents("Listen Authorize Thread", GlobusEventType.ExecPointReached, "Authorize Thread Finished");
            }
            catch (Exception ex)
            {
                EventLogger.LogEvents("Listen Authorize Thread", GlobusEventType.Crash, ex.Message);
            }
        }

        /// <summary>
        /// Reads Globus Responses on the Data Phantom
        /// </summary>
        public void DataMessageReader()
        {
            _AdapterIdentity.Impersonate();

            try
            {
                EventLogger.LogEvents("Listen Data Thread", GlobusEventType.ExecPointReached, "Data Thread Started");

                while (!_StopThreads)
                {
                    this._IOManager.ReadResults(false);
                    Thread.Sleep(ListenSleepDuration);
                }

                EventLogger.LogEvents("Listen Data Thread", GlobusEventType.ExecPointReached, "Data Thread Finished");

            }
            catch (Exception ex)
            {
                EventLogger.LogEvents("Listen Data Thread", GlobusEventType.Crash, ex.Message);
            }
        }

        /// <summary>
        /// Reads Globus Responses on the Data Phantom
        /// </summary>
        public GlobusMessage DataMessageRead(GlobusRequestShort request, string requestGUID)
        {
            GlobusMessage globusMessage = null;
            try
            {
                globusMessage = _IOManager.GetDataResult(false, request, requestGUID);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvents("Listen Data Thread", GlobusEventType.Crash, ex.Message);
            }

            return globusMessage;
        }

        public GlobusMessage AuthorizeMessageRead(GlobusRequestShort request, string requestGUID)
        {
            GlobusMessage globusMessage = null;
            try
            {
                globusMessage = _IOManager.GetDataResult(true, request, requestGUID);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvents("Listen Authorize Thread", GlobusEventType.Crash, ex.Message);
            }

            return globusMessage;
        }

        /// <summary>
		/// Gets the messages that the GlobusIOManager has for this thread.
		/// </summary>
		/// <param name="guid"></param>
		/// <returns></returns>
		public ArrayList GetMessages( string guid )
		{
            //Writes the contents of the hashtable to array list
            //for easier processing
            Hashtable msgHash = this._IOManager.GetMessages( guid );
            ArrayList arrMsg = new ArrayList();

            foreach (object globusMessage in msgHash.Values)
            {
                arrMsg.Add(globusMessage);
            }                

            return arrMsg;
		}

        /// <summary>
		/// Writes the passed message to 'in' Globus directory
		/// </summary>
		/// <param name="isAuthorize"> Flag - Is Authorized - not used !!!</param>
		/// <param name="message"> Message to be saved </param>
        /// <param name="guid"></param>
        /// <param name="messageIndex"></param>
		/// <returns> ID of the generated filename</returns>
        public string ProcessMessage_Bulk(bool isAuthorize, GlobusMessage inputT24Message)
		{
            _IOManager.RequireTSSReconnection = IsT24UnitTestExecution && isAuthorize;
            _IOManager.WriteMessage(isAuthorize, inputT24Message);

            return inputT24Message.MessageIndex;
		}

        /// <summary>
        /// Send messages using Telnet
        /// </summary>
        public void ProcessMessage_Bulk(string guid, GlobusMessage inputT24Message, GlobusMessage outputT24Message, OFSCommonMethods.IO.Telnet.TSSTelnetComm telnetComm)
        {
            _IOManager.WriteMessage(inputT24Message, outputT24Message, telnetComm);
        }

        /// <summary>
        /// Writes the passed message to 'in' Globus directory
        /// </summary>
        /// <param name="isAuthorize"> Flag - Is Authorized - not used !!!</param>
        /// <param name="message"> Message to be saved </param>
        /// <param name="guid"></param>
        /// <returns> ID of the generated filename</returns>
        public GlobusMessage ProcessMessage_Bulk(bool isAuthorize, string message, string guid)
        {
            GlobusMessage gm = new GlobusMessage(guid);

            gm.Message.Text = message;

            _IOManager.RequireTSSReconnection = IsT24UnitTestExecution && isAuthorize;
            _IOManager.WriteMessageBulk(isAuthorize, gm);
            return gm;
        }

        #endregion
    }
}
