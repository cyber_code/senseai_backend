﻿using System.Collections.Generic;

namespace OFSCommonMethods
{
    public class OFSDetailedErrorList : List<OFSDetailedError>
    {
        public OFSDetailedErrorList(IEnumerable<OFSDetailedError> collection) : base(collection)
        {
        }

        public OFSDetailedErrorList(int capacity) : base(capacity)
        {
        }

        public OFSDetailedErrorList()
        {
        }
    }
}