﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods
{
    /// <summary>
    /// Enumerates the type of the data processing operations.
    /// </summary>
    public enum DataProcessType
    {
        None,

        Create,

        Select,

        Delete,

        Reverse,

        Authorize,

        CreateAndAuthorize,

        Verify,

        Validate
    }
}
