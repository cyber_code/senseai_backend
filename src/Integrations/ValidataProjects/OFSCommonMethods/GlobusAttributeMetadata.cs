﻿using System.Collections.Generic;

namespace OFSCommonMethods
{
    public class GlobusAttributeMetadata
    {
        #region T24 Fields

        /// <summary>
        /// Is user defined attribute
        /// Whether it is USR.xxxx field (or SYS.xxxx field)
        /// </summary>
        public bool IsUserDefined;

        /// <summary>
        /// Attribute Name
        /// USR.FIELD.NAME or SYS.FIELD.NAME
        /// </summary>
        public string FieldName;

        /// <summary>
        /// Attribute Type
        /// USR.TYPE or SYS.TYPE
        /// </summary>
        public string Type;

        /// <summary>
        /// Attribute Field No
        /// USR.FIELD.NO or SYS.FIELD.NO
        /// </summary>
        public string FieldNo;

        /// <summary>
        /// Attribute Val Prog
        /// USR.VAL.PROG or SYS.VAL.PROG
        /// </summary>
        public string ValProg;

        /// <summary>
        /// Attribute Conversion
        /// USR.CONVERSION or SYS.CONVERSION
        /// </summary>
        public string Conversion;

        /// <summary>
        /// Attribute Display Format
        /// USR.DISPLAY.FMT or SYS.DISPLAY.FMT
        /// </summary>
        public string DisplayFmt;

        /// <summary>
        /// Attribute DAlt Index
        /// USR.ALT.INDEX or SYS.ALT.INDEX
        /// </summary>
        public string AltIndex;

        /// <summary>
        /// Attribute Alt Index
        /// USR.IDX.FILE or SYS.IDX.FILE
        /// </summary>
        public string IdxFile;

        /// <summary>
        /// Attribute Index Nulls
        /// USR.INDEX.NULLS or SYS.INDEX.NULLS
        /// </summary>
        public string IndexNulls;

        /// <summary>
        /// Attribute SingleMult
        /// SYS.SINGLE.MULT or USR.SINGLE.MULT
        /// </summary>
        public string SingleMult;

        /// <summary>
        /// Attribute Language Field
        /// SYS.LANG.FIELD or USR.LANG.FIELD
        /// </summary>
        public string LangField;

        /// <summary>
        /// Attribute Generated
        /// SYS.GENERATED
        /// </summary>
        public string Generated;

        /// <summary>
        /// Attribute Cnv Type
        /// SYS.CNV.TYPE or USR.CNV.TYPE
        /// </summary>
        public string CnvType;

        /// <summary>
        /// Attribute Rel File
        /// SYS.REL.FILE or USR.REL.FILE
        /// </summary>
        public string RelFile;

        /// <summary>
        /// Attribute Drop Down
        /// DROP.DOWN
        /// </summary>
        public string DropDown;

        /// <summary>
        /// Attribute Display Type
        /// "NODISPLAY", "COMBOBOX", "TOGGLE" and "VERTICAL.OPTIONS"
        /// </summary>
        public string DisplayType;
        #endregion

        #region Properties

        /// <summary>
        /// Is attribute defines multi-values
        /// </summary>
        public bool IsMultiValue
        {
            get { return SingleMult == "M" || IsMultiline; }
        }

        /// <summary>
        /// Is multiline (TEXT multi value defined)
        /// </summary>
        public bool IsMultiline
        {
            get
            {
                ValidataCommon.ValProg vp = new ValidataCommon.ValProg(this.ValProg);
                return vp.IsMultiline;
            }
        }

        /// <summary>
        /// Is attribute defines sub-values
        /// </summary>
        public bool IsSubValue
        {
            get
            {
                if (!IsMultiValue)
                {
                    // TODO - Check if we can have sub/values without multi values
                    return false;
                }

                switch (LangField)
                {
                    case "Y":
                        return false;
                    case "S":
                        return true;
                    case "N":
                    default:
                        return false;
                }
            }
        }

        #endregion

        #region Object Overrides

        public override string ToString()
        {
            return string.Format("{0}{1}{2}{3}",
                    FieldName,
                    IsMultiValue ? "[MV]" : string.Empty,
                    IsSubValue ? "[SV]" : string.Empty,
                    IsUserDefined ? "[USER]" : string.Empty
                );
        }

        #endregion
    }

    public class GlobusAttributeMetadataCollection : List<GlobusAttributeMetadata>
    {
        public IEnumerable<GlobusAttributeMetadata> UsrFields
        {
            get { return this.FindAll(n => n.IsUserDefined); }
        }

        public GlobusAttributeMetadata FindByName(string fieldName)
        {
            // todo - may be implement dictionary
            foreach (var gam in this)
            {
                if (gam.FieldName == fieldName)
                {
                    return gam;
                }
            }

            return null;
        }

        public GlobusAttributeMetadata FindByLocalRef(string fieldName)
        {
            try
            {
                System.Diagnostics.Debug.Assert(fieldName.StartsWith("LOCAL.REF-"));
                int pos = fieldName.IndexOf('-');
                if (pos < 0)
                {
                    return null;
                }

                fieldName = fieldName.Substring(pos + 1);

                int mv = -1;
                int sv = -1;
                pos = fieldName.IndexOf('.');
                if (pos > 0)
                {
                    int.TryParse(fieldName.Substring(0, pos), out mv);
                    int.TryParse(fieldName.Substring(pos + 1), out sv);
                }
                else
                {
                    int.TryParse(fieldName, out mv);
                }

                // todo - may be implement dictionary
                GlobusAttributeMetadata bestMatch = null;
                foreach (var gam in this)
                {
                    if (string.IsNullOrEmpty(gam.FieldNo))
                    {
                        continue;
                    }

                    string fn = gam.FieldNo.Trim();

                    if (!fn.StartsWith("LOCAL.REF"))
                    {
                        continue;
                    }

                    string[] parts = fn
                        .Replace("LOCAL.REF", "")
                        .Replace("<", "")
                        .Replace(">", "")
                        .Split(new char[] { ',' });

                    if (parts.Length != 2)
                    {
                        continue;
                    }

                    int curMv = -1;
                    int curSv = -1;
                    int.TryParse(parts[1], out curMv);
                    int.TryParse(parts[0], out curSv);

                    if (curMv == mv)
                    {
                        if (curSv == sv || (sv < 0 && curSv == 1))
                        {
                            return gam;
                        }
                        else if (curSv <= 1)
                        {
                            bestMatch = gam;
                        }
                    }
                }

                return bestMatch;
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Debug.Fail(ex.Message);
                return null;
            }
        }

        public void RemoveRange(IEnumerable<GlobusAttributeMetadata> attributes)
        {
            if (attributes != null)
            {
                foreach (var attr in attributes)
                {
                    this.Remove(attr);
                }
            }
        }
    }
}