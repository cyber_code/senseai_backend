﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace OFSCommonMethods
{
    public struct AdditionalRecInfo
    {
        public string RecID;
        public string T24App;
    }

    public struct ErrorObjectDetails
    {
        public string Object;
        public string Details;
    }
    /// <summary>
    /// 	Parses OFS Responses based on a configurable list of regular expressions
    /// </summary>
    public static class OfsResponseErrorParser
    {
        #region Private Members

        /// <summary>
        /// 	A path to the parsing rules regexp file
        /// </summary>
        private static readonly string RulesFilePath = String.IsNullOrEmpty(ValidataCommon.AdapterConfiguration.AdapterFilesRootPath) ?
            Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\Assemblies\OfsErrorsParsingRules.xml" :
            Environment.ExpandEnvironmentVariables(ValidataCommon.AdapterConfiguration.AdapterFilesRootPath) + @"\Assemblies\OfsErrorsParsingRules.xml";

        #endregion

        #region Public Members

        /// <summary>
        /// 	A container for the parsing rules from the Xml configuration file
        /// </summary>
        private static List<OfsParseRule> _ParseRules = new List<OfsParseRule>();

        /// <summary>
        /// 	Gets a list with parsing rules from the Xml document
        /// </summary>
        internal static List<OfsParseRule> ParseRules
        {
            get
            {
                if (_ParseRules.Count == 0)
                {
                    ReadRulesFromXml();
                }

                return _ParseRules;
            }
        }

     
        #endregion

        #region Public Members
        /// <summary>
        /// 	Reads the rules from XML.
        /// </summary>
        public static void ReadRulesFromXml()
        {
            if (!new FileInfo(RulesFilePath).Exists)
                return;

            var rules = XDocument.Load(RulesFilePath);
            _ParseRules = (from r in rules.Descendants("Rule")
                           select new OfsParseRule
                           {
                               Description = r.Element("Description").Value,
                               RegExp = r.Element("RegExp").Value,
                               SearchForRecordData = bool.Parse(r.Element("SearchForRecordData").Value),
                               RecordInfo = (r.Element("RecInfo") != null) ? r.Element("RecInfo").Value : ""
                           }).ToList();
        }

        /// <summary>
        /// 	Parses each error individualy from the &lt;MSG&gt;OFS Response&lt;/MSG&gt;
        /// </summary>
        /// <param name = "ofsResponse">OFS response</param>
        /// <returns>A "error name->error" pairs from the OFS Response </returns>
        public static List<ErrorObjectDetails> ParseErrors(string ofsResponse, AdditionalRecInfo recordInfo )
        {
            var errors = new List<ErrorObjectDetails>();

            
            foreach (var rule in ParseRules)
            {
                string recInfo = "";
                if (rule.RecordInfo != "")
                {
                    var recRgx = new Regex(rule.RecordInfo.Trim());
                    var mtch = recRgx.Match(ofsResponse);                    
                    if (mtch != null) recInfo = mtch.Value ;

                }
                var regexMatch = new Regex(rule.RegExp.Trim());
                var matches = regexMatch.Matches(ofsResponse);
                foreach (Match match in matches)
                {
                    string errorKey = match.Value;
                    if (recInfo != "")
                    {
                        errorKey = recInfo;
                    }

                    if (rule.SearchForRecordData)
                    {
                        errors.Add(new ErrorObjectDetails() {Object = errorKey, Details = String.Format(rule.Description, match.Value)});
                    }
                    else
                        errors.Add(new ErrorObjectDetails() { Object = match.Value, Details = rule.Description });
                }
            }

            return errors;
        }

        #endregion
    }

    internal struct OfsParseRule
    {
        public string Description { get; set; }
        public string RegExp { get; set; }
        public bool SearchForRecordData { get; set; }
        public string RecordInfo { get; set; }
    }
}