﻿using System.Collections.Generic;

namespace OFSCommonMethods
{
    public class GlobusDMDAttributeMetadata
    {
        #region T24 Fields

        /// <summary>
        /// Attribute Name
        /// APPL.FIELD.NAME
        /// </summary>
        public string FieldName;

        /// <summary>
        /// Attribute Type
        /// FIELD.POSITION
        /// </summary>
        public int FieldPosition;
        
        #endregion
    }
}