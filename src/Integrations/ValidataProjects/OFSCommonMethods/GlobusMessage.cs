using System.Collections.Generic;
using System.Text;

namespace OFSCommonMethods
{
    /// <summary>
    /// Represents a single Globus request or response.
    /// </summary>
    public class GlobusMessage
    {
        #region Classes

        /// <summary>
        /// OFS Multipart Message
        /// </summary>
        public class OFSMessage
        {
            /// <summary>
            /// Message Text - Main OFS Message. Usually it is the only message, unless it is a multipart message
            /// </summary>
            public string Text = "";

            /// <summary>
            /// OFS messages for sub items (applicable only to multipart OFS messages)
            /// </summary>
            public readonly List<string> SubItems = new List<string>();

            /// <summary>
            /// Constructor
            /// </summary>
            public OFSMessage() {}

            /// <summary>
            /// Constructs
            /// </summary>
            /// <param name="text"></param>
            public OFSMessage(string text)
            {
                Text = text;
            }

            /// <summary>
            /// Returns a <see cref="System.String"/> that represents this instance.
            /// </summary>
            /// <returns>
            /// A <see cref="System.String"/> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                return Text;
            }

            /// <summary>
            /// Returns either the message or if there are multipart message items, returns them as well in a XML fragment
            /// </summary>
            /// <returns></returns>
            public string ToLongString()
            {
                if (SubItems.Count == 0)
                {
                    return ToString();
                }

                StringBuilder sbResult = new StringBuilder();

                sbResult.Append("<request>");
                sbResult.Append(Text);
                sbResult.Append("</request>");

                foreach (string subItem in SubItems)
                {
                    sbResult.AppendLine();
                    sbResult.Append("<request>");
                    sbResult.Append(subItem);
                    sbResult.Append("</request>");
                }

                return sbResult.ToString();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The ID of the message. External ID or generated ID = thread GUID concatenated with the message index
        /// </summary>
        public readonly string ID;

        /// <summary>
        /// The index of this message
        /// </summary>
        public readonly string MessageIndex = "0";

        /// <summary>
        /// The text of this message
        /// </summary>
        public OFSMessage Message = new OFSMessage();

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// The other constructor of the GlobusMessage file is used when the Globus response is read so the ID of the message is already assigned.
        /// </summary>
        /// <param name="id">The already known ID of the Globus message</param>
        public GlobusMessage(string id)
        {
            ID = id;
        }

        /// <summary>
        /// Creates a GlobusMessage instance using a 
        /// </summary>
        /// <param name="guid">The GUID to be used to form the message ID from the other constructor.</param>
        /// <param name="messageIndex"></param>
        public GlobusMessage(string guid, uint messageIndex)
        {
            MessageIndex = messageIndex.ToString();
            ID = guid + "_" + MessageIndex;
        }

        #endregion
    }
}