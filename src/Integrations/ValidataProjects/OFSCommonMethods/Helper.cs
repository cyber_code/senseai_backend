﻿using OFSCommonMethods.IO;
using ValidataCommon;

namespace OFSCommonMethods
{
    public class Helper
    {
        /// <summary>
        /// Possible combinations for the configuration field:
        /// Command - 1 
        /// Command,User,Password - 3
        /// Command,User,Password,AuthUser,AuthPassword - 5
        /// Version,Command - 2
        /// Version,Command,User,Password - 4
        /// Version,Command,User,Password,AuthUser,AuthPassword - 6
        ///					  
        /// Note: If the Command is 'A', both parameters AuthUser and AuthPassword might follow the command
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="settings"></param>
        /// <param name="company">Company</param>
        public static string ParseConfiguration(string configuration, ref Settings settings, ref string company)
        {
            #region Obsolete

            //if (configuration == null)
            //{
            //    configuration = string.Empty;
            //}

            //string[] param = configuration.Split(',');

            //if (param.Length > 1)
            //{
            //    if (param.Length == 2 || param.Length == 4 || param.Length == 6)
            //    {
            //        settings.Version = param[0]; // Version, Command,....
            //        configuration = param[1];
            //        if (param.Length == 4 || param.Length == 6)
            //        {
            //            settings.InputUser = param[2]; // Version, Command, User, Password
            //            settings.InputPassword = param[3];
            //            if (param.Length == 6) // Version, Command, User, Password, AuthUser, AuthPassword
            //            {
            //                settings.AuthorizationUser = param[4];
            //                settings.AuthorizationPassword = param[5];
            //            }
            //        }
            //    }
            //    else if (param.Length == 3 || param.Length == 5)
            //    {
            //        configuration = param[0]; // Command,...
            //        settings.InputUser = param[1]; //  Command, User, Password
            //        settings.InputPassword = param[2];
            //        if (configuration.ToUpper() == "A")
            //        {
            //            settings.AuthorizationUser = settings.InputUser;
            //            settings.AuthorizationPassword = settings.InputPassword;
            //        }
            //        if (param.Length == 5) // Command, User, Password, AuthUser, AuthPassword
            //        {
            //            settings.AuthorizationUser = param[3];
            //            settings.AuthorizationPassword = param[4];
            //        }
            //    }
            //}

            //return configuration;

            #endregion

            T24AdapterConfiguration t24AdapterConfiguration =
                T24AdapterConfiguration.Parse(configuration);

            // Copy to Settings
            if (t24AdapterConfiguration.Version != null)
            {
                settings.Version = t24AdapterConfiguration.Version;
            }

            if (t24AdapterConfiguration.InputUser != null)
            {
                settings.InputUser = t24AdapterConfiguration.InputUser;
            }

            if (t24AdapterConfiguration.InputPassword != null)
            {
                settings.InputPassword = t24AdapterConfiguration.InputPassword;
            }

            if (t24AdapterConfiguration.AuthorizationUser != null)
            {
                settings.AuthorizationUser = t24AdapterConfiguration.AuthorizationUser;
            }

            if (t24AdapterConfiguration.AuthorizationPassword != null)
            {
                settings.AuthorizationPassword = t24AdapterConfiguration.AuthorizationPassword;
            }

            if (!string.IsNullOrEmpty(t24AdapterConfiguration.Company))
            {
                company = t24AdapterConfiguration.Company;
            }

            return t24AdapterConfiguration.Configuration;
        }
    }
}