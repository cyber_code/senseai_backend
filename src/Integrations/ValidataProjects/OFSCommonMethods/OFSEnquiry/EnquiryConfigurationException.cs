﻿using System;
using System.Runtime.Serialization;

namespace OFSCommonMethods.OFSEnquiry
{
    public enum EnquiryExceptionType
    {
        Undefined,
        NoRuleMatch,
        InvalidXML
    }

    [Serializable]
    public class EnquiryConfigurationException : Exception
    {
        public EnquiryConfigurationException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public EnquiryExceptionType ExceptionType { get; set; }

        public EnquiryConfigurationException(EnquiryExceptionType exceptionType) {ExceptionType = exceptionType; }

        public EnquiryConfigurationException(string message, EnquiryExceptionType exceptionType) : base(message) { ExceptionType = exceptionType; }

        public EnquiryConfigurationException(string message, Exception innerException, EnquiryExceptionType exceptionType) : base(message, innerException) { ExceptionType = exceptionType; }

    }
}
