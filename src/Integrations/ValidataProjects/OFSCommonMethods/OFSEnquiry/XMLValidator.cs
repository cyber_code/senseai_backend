﻿using System;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace OFSCommonMethods.OFSEnquiry
{
    internal class XMLValidator
    {
        private readonly string _XSDFilePath = String.IsNullOrEmpty(ValidataCommon.AdapterConfiguration.AdapterFilesRootPath)
            ? Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\Assemblies\EnquiryConfiguration.xsd" 
            : Environment.ExpandEnvironmentVariables(ValidataCommon.AdapterConfiguration.AdapterFilesRootPath) + @"\Assemblies\EnquiryConfiguration.xsd";

        /// <summary>
        /// Validation Error Count
        /// </summary>
        public int ErrorsCount;

        /// <summary>
        /// Validation Error Message
        /// </summary>
        public string ErrorMessage = "";

        private void ValidationHandler(object sender, ValidationEventArgs args)
        {
            ErrorMessage = ErrorMessage + args.Message + "\r\n";
            ErrorsCount++;
        }

        public void Validate(string strXMLDoc)
        {
            if (!File.Exists(_XSDFilePath))
            {
                return;
            }

            var settings = new XmlReaderSettings { NameTable = new NameTable() };
            var mgr = new XmlNamespaceManager(settings.NameTable);
            mgr.AddNamespace("", "");

            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(null, _XSDFilePath);
            settings.ValidationFlags = settings.ValidationFlags & XmlSchemaValidationFlags.ReportValidationWarnings;
            settings.ValidationEventHandler += ValidationHandler;

            var ctxt = new XmlParserContext(settings.NameTable, mgr, "", XmlSpace.Default);

            using (XmlReader reader = XmlReader.Create(new StringReader(strXMLDoc), settings, ctxt))
            {
                while (reader.Read()) { }
            }
        }
    }
}