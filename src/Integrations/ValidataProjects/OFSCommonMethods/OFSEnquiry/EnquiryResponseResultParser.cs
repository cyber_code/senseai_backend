﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OFSCommonMethods.OFSML.OFSML130;

namespace OFSCommonMethods.OFSEnquiry
{
    /// <summary>
    /// </summary>
    public class EnquiryResponseRow
    {
        ///<summary>
        ///	List of cells
        ///</summary>
        public List<EnquiryResponseCell> Cells = new List<EnquiryResponseCell>();

        /// <summary>
        /// 	Name of the row template describing the row
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        /// Determine whether any cell value is mapped to an enquiry result attribute
        /// </summary>
        public bool IsAnyValueMapped
        {
            get
            {
                return Cells != null && Cells.Any(c => c.Name != null);
            }
        }
    }

    public class EnquiryResponseCell
    {
        /// <summary>
        /// Enquiry result field name, defined in the enquiry mask
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Enquiry result field value
        /// </summary>
        public string Value { get; set; }

        public EnquiryResponseCell(string cellValue)
        {
            Value = cellValue;
        }
    }

    /// <summary>
    /// </summary>
    public class EnquiryResponse
    {
        ///<summary>
        ///	List of rows
        ///</summary>
        public List<EnquiryResponseRow> Rows = new List<EnquiryResponseRow>();

        /// <summary>
        /// Re-order response columns in a same order as the attributes in the typical
        /// </summary>
        /// <param name="attributes"></param>
        /// <returns></returns>
        public EnquiryResponse ReOrder(List<string> attributes, List<string> columns)
        {
            //This is a hack, we need to modify xpath better and not to take headers and values shifted
            if(columns.Any() && columns.First() == "")
            {
                columns.RemoveAt(0);
            }

            List<KeyValuePair<string, int>> fromTo = new List<KeyValuePair<string, int>>();

            if (Rows.Any())
            {
                foreach (var attr in attributes)
                {
                    fromTo.Add(new KeyValuePair<string, int>(attr, columns.FindIndex(n => n.ToLower() == attr.ToLower())));
                }

                EnquiryResponse resultRows = new EnquiryResponse();
                foreach (var row in Rows)
                {
                    EnquiryResponseRow newRow = new EnquiryResponseRow();

                    foreach (var pair in fromTo)
                    {
                        EnquiryResponseCell newCell = new EnquiryResponseCell("") { Name = pair.Key };

                        if (pair.Value != -1 && row.Cells.Count > pair.Value)
                        {
                            newCell.Value = row.Cells[pair.Value].Value;
                        }

                        newRow.Cells.Add(newCell);
                    }

                    resultRows.Rows.Add(newRow);
                }

                return resultRows;
            }

            return this;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (Rows.Any())
            {
                sb.AppendLine(string.Join("\t", Rows[0].Cells.Select(n => string.Format("\"{0}\"", n.Name))));

                foreach(var row in Rows)
                {
                    sb.AppendLine(string.Join("\t", row.Cells.Select(n => string.Format("\"{0}\"", n.Value))));
                }
            }

            return sb.ToString();
        } 
    }

    ///<summary>
    ///	Enquiry response parser
    ///</summary>
    public static class EnquiryResponseResultParser
    {
        /// <summary>
        /// 	Split response result by rows
        /// </summary>
        /// <param name="response"> </param>
        /// <returns> </returns>
        public static EnquiryResponse GetResult(string response)
        {
            return GetResult(response, char.MinValue);
        }

        /// <summary>
        /// 	Split response result by rows
        /// </summary>
        /// <param name="response"> </param>
        /// <param name="doubleQuotesReplacement"></param>
        /// <returns> </returns>
        public static EnquiryResponse GetResult(string response, char doubleQuotesReplacement)
        {
            if(response.Trim().Length == 0)
            {
                return new EnquiryResponse();
            }

            string rowByRow = response.Replace("\",\"", "\"\r\n\"");
            string[] splitters = {"\r\n"};
            string[] lines = rowByRow.Split(splitters, StringSplitOptions.None);

            return GetResult(lines, doubleQuotesReplacement);
        }

        /// <summary>
        /// </summary>
        /// <param name="lines"> </param>
        /// <returns> </returns>
        public static EnquiryResponse GetResult(IEnumerable<string> lines, char doubleQuotesReplacement)
        {
            var res = new EnquiryResponse();

            foreach (string line in lines)
            {
                var row = new EnquiryResponseRow { Cells = GetSingleLineArray(line, doubleQuotesReplacement) };
                res.Rows.Add(row);
            }

            return res;
        }

        /// <summary>
        /// </summary>
        /// <param name="response"> </param>
        /// <returns> </returns>
        public static EnquiryResponse GetResult(Ofsml130StandardEnquiryResponse response)
        {
            var res = new EnquiryResponse();
            foreach (var record in response.EnquiryRecords)
            {
                var row = new EnquiryResponseRow {Cells = record.Columns};
                res.Rows.Add(row);
            }

            return res;
        }

        private static List<EnquiryResponseCell> GetSingleLineArray(IEnumerable<char> line, char doubleQuotesReplacement)
        {
            var lineArr = new List<EnquiryResponseCell>();

            bool begin = false;
            var word = new StringBuilder();
            foreach (char c in line)
            {
                if (c == '"' && !begin)
                {
                    begin = true;
                    continue;
                }

                if (c == '"' && begin)
                {
                    begin = false;
                    string fieldValue = word.ToString();
                    
                    var outputValue = (fieldValue.Trim() != string.Empty) ? fieldValue.Trim() : word.ToString();
                    if (outputValue != null && doubleQuotesReplacement != char.MinValue)
                        outputValue = outputValue.Replace(doubleQuotesReplacement, '\"');
                        
                    lineArr.Add(new EnquiryResponseCell(outputValue));

                    word.Remove(0, word.Length);
                    continue;
                }

                if (begin && c != '"')
                    word.Append(c);
            }

            return lineArr;
        }
    }
}