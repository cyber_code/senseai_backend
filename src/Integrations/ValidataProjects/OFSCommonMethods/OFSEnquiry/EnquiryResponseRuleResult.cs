﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using AXMLEngine;

namespace OFSCommonMethods.OFSEnquiry
{
    /// <summary>
    /// 
    /// </summary>
    public static class EnquiryResponseRuleResult
    {

        #region Public Methods

        /// <summary>
        /// Return requested result fulfilled the rule
        /// </summary>
        /// <param name="response"></param>
        /// <param name="rule"></param>
        /// <returns></returns>
        public static EnquiryResponse GetResult(EnquiryResponse response, LevelStructureHierarchy rule)
        {
            var allResult = new EnquiryResponse();
            var result = new EnquiryResponse();
            LevelStructureHierarchy lastPassedRule = rule;

            bool finishWithSuccess;
            bool addRow;

            foreach (EnquiryResponseRow row in response.Rows)
            {
                lastPassedRule = LastPassedRule(lastPassedRule, row, out finishWithSuccess, out addRow, result);

                if (finishWithSuccess)
                {
                    allResult.Rows.AddRange(result.Rows);

                    result.Rows.Clear();
                    lastPassedRule = LastPassedRule(rule, row, out finishWithSuccess, out addRow, result);
                }

                if (lastPassedRule != null)
                {
                    if (lastPassedRule.HaveSublevels())
                        result.Rows.Add(row);
                    else
                    {
                        if (addRow)
                            result.Rows.Add(row);
                    }
                }
                else
                {
                    result.Rows.Clear();
                    lastPassedRule = rule;
                }
            }

            if (!lastPassedRule.HaveSublevels())
                allResult.Rows.AddRange(result.Rows);

            return GetResultByOccurrence(allResult, LastLevelOccurrence(rule));
        }

        private static string LastLevelOccurrence(LevelStructureHierarchy rule)
        {
            if (rule.HaveSublevels())
                return LastLevelOccurrence(rule.SubLevels[0]);

            return rule.Occurrence;
        }

        #endregion

        #region Private Methods

        private static LevelStructureHierarchy LastPassedRule(LevelStructureHierarchy lastPassedRule,
                                                              EnquiryResponseRow row,
                                                              out bool finishWithSuucess,
                                                              out bool addRow,
                                                              EnquiryResponse resultRows)
        {
            finishWithSuucess = false;

            if (ValidateRow(row, lastPassedRule.RowTemplates, out addRow))
            {
                if (addRow || !lastPassedRule.HaveSublevels())
                    return lastPassedRule;

                if (resultRows.Rows.Count > 0 &&
                    lastPassedRule.HaveSublevels())
                {
                    resultRows.Rows.Clear();
                    return LastPassedRule(lastPassedRule.SubLevels[0], row, out finishWithSuucess, out addRow, resultRows);
                }
            }
            else
            {
                if (resultRows.Rows.Count > 0 &&
                    lastPassedRule.HaveSublevels())
                {
                    resultRows.Rows.Clear();
                    return LastPassedRule(lastPassedRule.SubLevels[0], row, out finishWithSuucess, out addRow, resultRows);
                }
            }

            if (!lastPassedRule.HaveSublevels() && resultRows.Rows.Count > 0)
            {
                finishWithSuucess = true;
            }
            return null;
        }

        private static bool ValidateRow(EnquiryResponseRow row, IEnumerable<LevelRowTemplate> rowTemplates, out bool addRow)
        {
            bool validRowStructure = false;
            foreach (LevelRowTemplate rowTemplate in rowTemplates)
            {

                if (ValidateRow(row, rowTemplate, out addRow))
                {
                    if (addRow)
                        return true;

                    validRowStructure = true;
                }
            }

            addRow = false;
            if (validRowStructure)
                return true;

            return false;
        }

        /// <summary>
        /// Check if row structure is same as RowTemplate structure. 
        /// I mean number and type of columns, without search creterias
        /// </summary>
        /// <param name="row">Row returned from OFS</param>
        /// <param name="rowTemplate">RowTemplate structure</param>
        /// <param name="addRow">If row fulfill search creterias true, else false.</param>
        /// <returns>True, if row structure match with row templatfe, else false</returns>
        private static bool ValidateRow(EnquiryResponseRow row, LevelRowTemplate rowTemplate, out bool addRow)
        {
            decimal currencyCellValue;
            int integerCellValue;
            DateTime dateCellValue = new DateTime();

            addRow = true;

            var nfi = new NumberFormatInfo
            {
                CurrencyDecimalSeparator = ".",
                NumberGroupSeparator = ",",
                NumberDecimalDigits = 3
            };

            if (row.Cells.Count != rowTemplate.Fields.Count)
                return false;

            for (int i = 0; i < rowTemplate.Fields.Count; i++)
            {
                Field fieldFromConfig = rowTemplate.Fields[i];
                string valueOfCell = row.Cells[i].Value;

                if (fieldFromConfig.FieldName != null)
                {
                    row.Cells[i].Name = fieldFromConfig.FieldName;
                }

                bool lastSimbolIsDash = valueOfCell.EndsWith("-");

                switch (fieldFromConfig.Type)
                {
                    case AXMLEngine.ValueType.String:
                        if (fieldFromConfig.Value != null &&
                            !CompareTwoStringValuesWithOperand(fieldFromConfig.Value,
                                                               valueOfCell,
                                                               fieldFromConfig.Operand))
                            addRow = false;
                        break;
                    case AXMLEngine.ValueType.Integer:
                        if (Int32.TryParse(valueOfCell, out integerCellValue)) //integer
                        {
                            if (fieldFromConfig.Value != null &&
                                !CompareTwoNumericValuesWithOperand(fieldFromConfig.Value,
                                                                    integerCellValue,
                                                                    fieldFromConfig.Operand))
                                addRow = false;
                        }
                        else
                            return false;
                        break;
                    case AXMLEngine.ValueType.Currency:
                        if (Decimal.TryParse(valueOfCell,
                                             NumberStyles.Any,
                                             nfi,
                                             out currencyCellValue)) //decimal
                        {
                            if (lastSimbolIsDash)
                                currencyCellValue = -currencyCellValue;
                            if (fieldFromConfig.Value != null &&
                                !CompareTwoNumericValuesWithOperand(fieldFromConfig.Value,
                                                                    currencyCellValue,
                                                                    fieldFromConfig.Operand))
                                addRow = false;
                        }
                        else
                            return false;
                        break;
                    case AXMLEngine.ValueType.Date:
                        if (DateTime.TryParse(valueOfCell, out dateCellValue)) //date
                        {
                            if (fieldFromConfig.Value != null &&
                                !CompareTwoDateValuesWithOperand(fieldFromConfig.Value,
                                                                 dateCellValue,
                                                                 fieldFromConfig.Operand))
                                addRow = false;
                        }
                        else
                            return false;
                        break;
                }
            }

            return true;
        }

        private static bool CompareTwoNumericValuesWithOperand(string configValue, decimal cellValue, ValueOperand valueOperand)
        {
            decimal configNumberValue;
            if (decimal.TryParse(configValue, out configNumberValue))
            {
                switch (valueOperand)
                {
                    case ValueOperand.EQ:
                        return cellValue == configNumberValue;
                    case ValueOperand.NE:
                        return cellValue != configNumberValue;
                    case ValueOperand.GE:
                        return cellValue >= configNumberValue;
                    case ValueOperand.GT:
                        return cellValue > configNumberValue;
                    case ValueOperand.LE:
                        return cellValue <= configNumberValue;
                    case ValueOperand.LT:
                        return cellValue < configNumberValue;
                    default:
                        return false;
                }
            }

            return false;
        }

        private static bool CompareTwoDateValuesWithOperand(string configValue, DateTime cellValue, ValueOperand valueOperand)
        {
            DateTime configDateValue;
            if (DateTime.TryParse(configValue, out configDateValue))
            {
                switch (valueOperand)
                {
                    case ValueOperand.EQ:
                        return cellValue == configDateValue;
                    case ValueOperand.NE:
                        return cellValue != configDateValue;
                    case ValueOperand.GE:
                        return cellValue >= configDateValue;
                    case ValueOperand.GT:
                        return cellValue > configDateValue;
                    case ValueOperand.LE:
                        return cellValue <= configDateValue;
                    case ValueOperand.LT:
                        return cellValue < configDateValue;
                    default:
                        return false;
                }
            }

            return false;
        }

        private static bool CompareTwoStringValuesWithOperand(string configValue, string cellValue, ValueOperand valueOperand)
        {
            switch (valueOperand)
            {
                case ValueOperand.EQ:
                    return configValue == cellValue;
                case ValueOperand.NE:
                    return configValue != cellValue;
                case ValueOperand.LIKE:
                    return cellValue.Contains(configValue);
                case ValueOperand.STARTSWITH:
                    return cellValue.StartsWith(configValue);
                case ValueOperand.ENDSWITH:
                    return cellValue.EndsWith(configValue);
                case ValueOperand.REGEX:
                    return Regex.IsMatch(cellValue, configValue);
                default:
                    return false;
            }
        }

        private static EnquiryResponse GetResultByOccurrence(EnquiryResponse allResult, string occurrence)
        {
            if (allResult.Rows.Count == 0)
                return allResult;

            if (occurrence == null)
                return allResult;


            if (string.Compare(occurrence, "Last", true) == 0)
            {
                allResult.Rows.RemoveRange(0, allResult.Rows.Count - 1);
            }
            else if (string.Compare(occurrence, "First", true) == 0)
            {
                if (allResult.Rows.Count > 1)
                    allResult.Rows.RemoveRange(1, allResult.Rows.Count - 1);
            }
            else
            {
                int ocurrencyPos;
                if (int.TryParse(occurrence, out ocurrencyPos))
                {
                    if (ocurrencyPos == 0 || allResult.Rows.Count < ocurrencyPos)
                    {
                        allResult.Rows.Clear();
                    }
                    else
                    {
                        EnquiryResponseRow rowAtPos = allResult.Rows[ocurrencyPos - 1];
                        allResult.Rows.Clear();
                        allResult.Rows.Add(rowAtPos);
                    }
                }
            }

            return allResult;
        }

        #endregion
    }
}