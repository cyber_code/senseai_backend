﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using AXMLEngine;

namespace OFSCommonMethods.OFSEnquiry
{
    ///<summary>
    ///Enquiry response rule helper
    ///</summary>
    public static class EnquiryResponseRule
    {
        #region public Methods

        ///<summary>
        /// Extract enquiry response rule from XML configuration file, using default xml configuration file
        ///     (defined by: OFSResponseReaderBase.EnquiryConfigurationFileName)
        ///</summary>
        ///<param name="environment"></param>
        ///<param name="enquiry">Enquiry name</param>
        ///<param name="adapter">Adpter name</param>
        ///<returns></returns>
        public static LevelStructureHierarchy GetRuleFromXML(string environment, string enquiry, string adapter, bool isCustomTemplate)
        {
            return GetRuleFromXML(OFSResponseReaderBase.EnquiryConfigurationFileName, environment, enquiry, adapter, isCustomTemplate);
        }

        ///<summary>
        ///Extract enquiry response rule from XML configuration file
        ///</summary>
        ///<param name="path">XML file destination path</param>
        ///<param name="environment"></param>
        ///<param name="enquiry">Enquiry name</param>
        ///<param name="adapter">Adpter name</param>
        ///<returns></returns>
        public static LevelStructureHierarchy GetRuleFromXML(string path, string environment, string enquiry, string adapter, bool isCustomTemplate)
        {
            string node;
            try
            {
                if (!File.Exists(path))
                    throw new IOException(string.Format("File '{0}' does not exist", path));

                var doc = new XmlDocument();
                doc.Load(path);

                XmlElement docElement = doc.DocumentElement;
                if (docElement == null)
                    throw new XmlException("Missing xml document element");

                node  = "//Sets/Set[@name='" + environment + "']";
                XmlNode setNode = docElement.SelectSingleNode(node);
                if (setNode == null)
                {
                    if (isCustomTemplate)
                        throw new InvalidDataException(String.Format("Unable to apply custom enquiry result template '{0}' because enquiry configuration set name '{1}' is missing.", enquiry, environment));
                    else
                        return null;
                }

                node = "Enquiries/Enquiry[@name='" + enquiry + "']";
                XmlNodeList enquiryNodes = setNode.SelectNodes(node);
                if (enquiryNodes == null || enquiryNodes.Count == 0)
                {
                    if (isCustomTemplate)
                        throw new InvalidDataException(String.Format("Custom enquiry result template '{0}' is missing for enquiry configuration set name '{1}'.", enquiry, environment));
                    else
                        return null;
                }

                XmlNode enquiryNode = null;
                foreach (XmlNode enqNode in enquiryNodes)
                {
                    node = "Adapters/Adapter[@name='" + adapter + "']";
                    XmlNode adapterNode = enqNode.SelectSingleNode(node);
                    if (adapterNode != null)
                    {
                        enquiryNode = enqNode;
                        break;
                    }
                }

                if (enquiryNode == null)
                    return null;

                LevelStructureHierarchy.PreformatEnquiryNode(enquiryNode);

                //var doc1 = new XmlDocument {InnerXml = enquiryNode.InnerXml};
                //doc1.Save(@"C:\test.xml");

                var validator = new XMLValidator();
                validator.Validate(enquiryNode.SelectSingleNode("Response").OuterXml);

                if (validator.ErrorsCount > 0)
                    throw new InvalidOperationException(validator.ErrorMessage);

                XmlNode levelNode = enquiryNode.SelectSingleNode("Response/Level");
                if (levelNode == null)
                    throw new InvalidOperationException(String.Format("Response/Level element does not exist for {0} enquiry", enquiry));

                return new LevelStructureHierarchy(levelNode);

            }
            catch(IOException ex)
            {
                string message = string.Format(
                        "Invalid content of {0} file. Details: {1}", System.IO.Path.GetFileName(path), ex.Message
                    );
                throw new EnquiryConfigurationException(message, ex, EnquiryExceptionType.InvalidXML);
            }
            catch (InvalidOperationException ex)
            {
                string message = string.Format(
                        "Invalid content of {0} file. Details: {1}", System.IO.Path.GetFileName(path), ex.Message
                    );
                throw new EnquiryConfigurationException(message, ex, EnquiryExceptionType.InvalidXML);
            }
            catch (InvalidDataException ex)
            {
                throw new EnquiryConfigurationException(ex.Message, ex, EnquiryExceptionType.InvalidXML);
            }
            catch (Exception ex)
            {
                string message = string.Format(
                        "Invalid content of {0} file. Details: {1}", System.IO.Path.GetFileName(path), ex.Message
                    );
                throw new EnquiryConfigurationException(message, ex, EnquiryExceptionType.InvalidXML);
            }
        }

        /// <summary>
        /// Return list of levels with only one sub level in hierarchy
        /// </summary>
        /// <param name="multipleBranchHierarchy">Level with multiple sub levels in hierarchy</param>
        /// <returns></returns>
        public static List<LevelStructureHierarchy> GetSingleBranchLevels(LevelStructureHierarchy multipleBranchHierarchy)
        {
            var lastLevels = new List<LevelStructureHierarchy>();
            getLastLevels(multipleBranchHierarchy, lastLevels);

            var singleBranchLevels = new List<LevelStructureHierarchy>();

            foreach (LevelStructureHierarchy lastLevel in lastLevels)
            {
                LevelStructureHierarchy resultLevel = getSingleBranchLevelHierarchy(lastLevel.ShadowCopy());
                singleBranchLevels.Add(resultLevel);
            }

            return singleBranchLevels;
        }

        #endregion

        #region Private Methods

        private static LevelStructureHierarchy getSingleBranchLevelHierarchy(LevelStructureHierarchy lastLevel)
        {
            if (lastLevel.ParentLevel == null) return lastLevel;

            LevelStructureHierarchy parentLevel = lastLevel.ParentLevel.ShadowCopy();
            parentLevel.AddSubLevel(lastLevel);
            lastLevel.ParentLevel = parentLevel;

            return getSingleBranchLevelHierarchy(parentLevel);
        }

        /// <summary>
        /// Get leaf nodes for hierarchy structure of Levels
        /// </summary>
        /// <param name="multipleBranchHierarchy"></param>
        /// <param name="lastLevels"></param>
        private static void getLastLevels(LevelStructureHierarchy multipleBranchHierarchy, ICollection<LevelStructureHierarchy> lastLevels)
        {
            if (multipleBranchHierarchy.SubLevels == null || multipleBranchHierarchy.SubLevels.Count == 0)
                lastLevels.Add(multipleBranchHierarchy);
            else
            {
                foreach (LevelStructureHierarchy subLevel in multipleBranchHierarchy.SubLevels)
                {
                    getLastLevels(subLevel, lastLevels);
                }
            }
        }

        

        #endregion
    }
}
