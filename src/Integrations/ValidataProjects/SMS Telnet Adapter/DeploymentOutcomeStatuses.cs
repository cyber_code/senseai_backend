﻿using System;
using System.Collections.Generic;
using AXMLEngine;
using ValidataCommon;

namespace T24TelnetAdapter
{

    public enum DeploymentOutcome
    {
        Successful,
        Failed,
        Skipped
    }

    public class DeploymentOutcomeStatuses
    {
        /// <summary>
        /// 	String identifying sucessful execution
        /// </summary>
        public const string ExecutionStatusSuccessful = "Successful";

        /// <summary>
        /// 	String identifying failed execution
        /// </summary>
        public const string ExecutionStatusFailed = "Failed";

        /// <summary>
        /// 	String identifying aborted execution
        /// </summary>
        public const string ExecutionStatusAborted = "Aborted";

        /// <summary>
        /// 	String identifying execution in progress
        /// </summary>
        public const string ExecutionStatusInProgress = "In Progress";

        /// <summary>
        /// 	String for marking an execution resulted sucessful with issues
        /// </summary>
        public const string ExecutionStatusSucessfulWithIssues = "Successful With Issues";

    }
}
