﻿using System;

namespace T24TelnetAdapter
{
    internal class HttpRestServiceConnection : TerminalConnection
    {
        #region Private Members

        private readonly string _HttpWebServiceUrl;
        private int _Timeout;

        #endregion

        public override TerminalConnectionType ConnectionType
        {
            get { return TerminalConnectionType.HttpRestService; }
        }

        public HttpRestServiceConnection(string host, int port, int pageCode, double timeout) : base(host, port, pageCode)
        {
            _HttpWebServiceUrl = host;
            _Timeout = (int)timeout;
        }

        protected override Exception EstablishConnection()
        {
            throw new NotImplementedException();
        }

        public override bool IsConnected
        {
            get { return true; }
        }

        protected override string GetTextFromResponseBytes(byte[] buff)
        {
            throw new NotImplementedException("NotImplementedException - GetTextFromResponseBytes");
        }

        protected override byte[] ReadAvailableBytes()
        {
            throw new NotImplementedException("NotImplementedException - ReadAvailableBytes");
        }

        protected override byte[] SendCommandText(string commandText)
        {
            throw new NotImplementedException("NotImplementedException - SendCommandText");
        }

        public override void Disconnect()
        {
            
        }
    }
}
