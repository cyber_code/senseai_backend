﻿using System.Collections.Generic;
using System.Diagnostics;
using Common;
using OFSCommonMethods.IO;

namespace T24TelnetAdapter
{
    public class HttpWebServiceConnector : T24TelnetConnector
    {
        private string[] _ResponseBuffer;

        private const string DefaultLoggerName = "Validata.SAS.HttpWebService"; // TODO might rename it to include SSH, too

        private new static readonly Logger Logger = Logger.UniqueInstance[DefaultLoggerName];

        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.HttpWebService; }
        }

        public bool RunHttpWebServiceCommand(string command, string httpWebServiceUrl, int timeout)
        {
            using (var connector = new OFSCommonMethods.IO.HttpWebService.HttpWebServiceConnector(Logger))
            {
                string result;
                string error;
                bool webRequestSucceed = connector.RunHttpWebServiceCommand(command, httpWebServiceUrl, timeout,
                    out result, out error);
                _ResponseBuffer = new string[] { command, result };

                return webRequestSucceed;
            }
        }

        public override bool Login()
        {
            _IsLogged = true;
            return true;
        }

        protected override bool Logout()
        {
            _IsLogged = false;
            return true;
        }

        protected override bool Reconnect()
        {
            return true;
        }

        public Queue<string> ReadResponse()
        {
            Queue<string> res = new Queue<string>();
            if (_ResponseBuffer != null)
            {
                foreach (string line in _ResponseBuffer)
                {
                    if (line != null && !line.Equals("") && !line.Equals("\n"))
                    {
                        res.Enqueue(line);
                    }
                }
            }
            else
            {
                Debug.Fail("Please check why response buffer is null!");
            }

            return res;
        }
    }
}
