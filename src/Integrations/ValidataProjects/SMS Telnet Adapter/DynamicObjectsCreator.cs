﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;
using AXMLEngine;

namespace T24TelnetAdapter
{
    public class DynamicObjectsCreator
    {
        private Type _DynamicType;

        public List<object> ConstructDynamicObjects(string ofsResponse)
        {
            try
            {
                string[] rows = ofsResponse.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                string headerRow = rows[0];

                var headersList = new List<string>();

                try
                {
                    string[] headers = headerRow.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (var header in headers)
                    {
                        string[] parts = header.Split(new[] { "::", ":" }, StringSplitOptions.RemoveEmptyEntries);
                        headersList.Add(parts[1]);
                    }
                }
                catch (Exception)
                {

                    //Header row is not formatted as expected
                    headersList = new List<string>(Enumerable.Range(1, 9).Select(n => "Field " + n.ToString()));
                }

                string[,] grid = new string[headersList.Count(), rows.Count() - 1];

                for (int i = 1; i < rows.Count(); i++)
                {
                    string resultLine = rows[i];
                    resultLine = removeNonTabs(resultLine);

                    string[] fieldValues = resultLine.Split(new[] { "\"", "\t" }, StringSplitOptions.RemoveEmptyEntries);

                    if (i == 1 && fieldValues.Any() && fieldValues[0] == "No records were found that matched the selection criteria")
                    {
                        return new List<object>();
                    }

                    if (fieldValues.Count() != headersList.Count())
                    {
                        //Debug.Assert(false, "Result values count is different than headers count.");
                    }

                    int min = Math.Min(headersList.Count(), fieldValues.Count());

                    for (int j = 0; j < min; j++)
                    {
                        grid[j, i - 1] = fieldValues[j].Trim(); // there are usually unnecessary trailing & leading spaces
                    }
                }

                return ConstructDynamicObjects(headersList, grid);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("OFS response parsing failed: {0}", ofsResponse), ex);
            }
        }

        private string removeNonTabs(string resultLine)
        {
            string result = resultLine;

            if (!string.IsNullOrEmpty(resultLine))
            {
                int quoteCnt = resultLine.Count(ch => ch == '\"');
                int tabCnt = resultLine.Count(ch => ch == '\t');

                string respBuffer = "";
                if (tabCnt == 0 && quoteCnt > 0 && quoteCnt > 0 && quoteCnt % 2 == 0)
                {
                    bool isInTab = true;
                    for (int ccc = 0; ccc < resultLine.Count(); ccc++)
                    {
                        char currChar = resultLine[ccc];
                        bool wasInTab = isInTab;

                        if (currChar == '\"')
                            isInTab = !isInTab;

                        if (!wasInTab || !isInTab)
                            respBuffer += currChar;

                    }
                    result = respBuffer;
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columnsNames"></param>
        /// <param name="values">Two dimensionals array. X-columns, Y-rows()</param>
        /// <returns></returns>
        public List<object> ConstructDynamicObjects(List<string> columnsNames, string[,] values)
        {
            if (columnsNames.Count != values.GetLength(0))
                Debug.Assert(false, "Header columns count do not match with the corresponding two dimensional array values.");

            var res = new List<object>();

            //Loop by rows
            for (int j = values.GetLowerBound(1); j <= values.GetUpperBound(1); j++)
            {
                var obj = CreateDynamicObject(columnsNames);

                PropertyInfo[] properties = obj.GetType().GetProperties();

                //Loop by columns
                for (int i = values.GetLowerBound(0); i <= values.GetUpperBound(0); i++)
                {
                    properties.First(n => n.Name == columnsNames[i]).SetValue(obj, values[i, j], null);
                }

                res.Add(obj);
            }

            return res;
        }

        public List<object> ConstructDynamicObjectsFromInstancesList(List<Instance> instances)
        {
            List<string> uniqueColumnsNames = GetUniqueColumnNames(instances);
            uniqueColumnsNames.Sort();

            var res = new List<object>();

            foreach (Instance instance in instances)
            {
                var obj = CreateDynamicObject(uniqueColumnsNames);
                foreach (var attr in instance.Attributes)
                {
                    SetPropValue(obj, attr.Name, attr.Value);
                }

                res.Add(obj);
            }

            return res;
        }

        public static object GetPropValue(object src, int propIndex)
        {
            return src.GetType().GetProperties()[propIndex].GetValue(src, null);
        }

        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static void SetPropValue(object src, string propName, object value)
        {
            src.GetType().GetProperty(propName).SetValue(src, value, null);
        }

        public static List<string> ValuesListByVieldName(List<object> objList, string propertyName)
        {
            return objList.Select(obj => GetPropValue(obj, propertyName)).Select(value => value.ToString()).ToList();
        }

        private Type GetDynamicType(IEnumerable<string> columnsNames)
        {
            //We expect different instances of the 'Creator' for different applications/enquiries
            return _DynamicType ?? (_DynamicType = CreateDynamicObjectType(columnsNames));
        }

        private object CreateDynamicObject(IEnumerable<string> columnsNames)
        {
            Type dynamicType = GetDynamicType(columnsNames);
            object generetedObject = Activator.CreateInstance(dynamicType);

            return generetedObject;
        }

        private Type CreateDynamicObjectType(IEnumerable<string> columnNames)
        {
            // create a dynamic assembly and module 
            var assemblyName = new AssemblyName { Name = "tmpAssembly" };

            AssemblyBuilder assemblyBuilder = Thread.GetDomain().DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            ModuleBuilder module = assemblyBuilder.DefineDynamicModule("tmpModule");

            // create a new type builder
            TypeBuilder typeBuilder = module.DefineType("BindableRow", TypeAttributes.Public | TypeAttributes.Class);

            foreach (var header in columnNames)
            {
                AddProperty(typeBuilder, header, typeof(string));
            }

            // Generate our type
            Type generetedType = typeBuilder.CreateType();

            return generetedType;
        }

        private void AddProperty(TypeBuilder typeBuilder, string propertyName, Type type)
        {
            FieldBuilder field = typeBuilder.DefineField("_" + propertyName, typeof(string), FieldAttributes.Private);
            // Generate a public property
            PropertyBuilder property = typeBuilder.DefineProperty(propertyName, PropertyAttributes.HasDefault, type, null);

            // The property set and property get methods require a special set of attributes:

            const MethodAttributes getSetAttr = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig;

            // Define the "get" accessor method for current private field.
            MethodBuilder currGetPropMthdBldr = typeBuilder.DefineMethod("get_" + property.Name, getSetAttr, type, Type.EmptyTypes);

            // Intermediate Language stuff...
            ILGenerator currGetIL = currGetPropMthdBldr.GetILGenerator();
            currGetIL.Emit(OpCodes.Ldarg_0);
            currGetIL.Emit(OpCodes.Ldfld, field);
            currGetIL.Emit(OpCodes.Ret);

            // Define the "set" accessor method for current private field.
            MethodBuilder currSetPropMthdBldr = typeBuilder.DefineMethod("set_" + property.Name, getSetAttr, null, new Type[] { type });

            // Again some Intermediate Language stuff...
            ILGenerator currSetIL = currSetPropMthdBldr.GetILGenerator();
            currSetIL.Emit(OpCodes.Ldarg_0);
            currSetIL.Emit(OpCodes.Ldarg_1);
            currSetIL.Emit(OpCodes.Stfld, field);
            currSetIL.Emit(OpCodes.Ret);

            // Last, we must map the two methods created above to our PropertyBuilder to 
            // their corresponding behaviors, "get" and "set" respectively. 
            property.SetGetMethod(currGetPropMthdBldr);
            property.SetSetMethod(currSetPropMthdBldr);
        }

        private List<string> GetUniqueColumnNames(IEnumerable<Instance> instances)
        {
            var columns = new Dictionary<string, string>();

            foreach (Instance instance in instances)
            {
                foreach (var attr in instance.Attributes)
                {
                    if (!columns.ContainsKey(attr.Name))
                    {
                        columns.Add(attr.Name, null);
                    }
                }
            }

            return columns.Keys.ToList();
        }
    }
}
