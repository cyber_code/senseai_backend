namespace T24TelnetAdapter
{
    public enum CompileOutcome
    {
        Success,
        LinkError,
        Warnings,
        Errors,
        MissingInclude,
        MissingFile,
        TelnetError,
        UnexpectedError
    }
}