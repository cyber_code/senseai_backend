﻿using System;
using System.Linq;
using log4net;
using System.Collections.Generic;

namespace T24TelnetAdapter
{
    public enum TerminalConnectionType
    {
        Telnet,
        SharpSsh,
        TCServer,
        HttpWebService,
        Ssh,
        HttpRestService
    }

    public abstract class TerminalConnection
    {
        internal string LastCommand;
        #region Events

        public event CommEvent OnSend;

        public event CommEvent OnReceive;

        private void FireSendEvent(string text, byte[] bytes)
        {
            if (OnSend != null)
            {
                OnSend(text, bytes);
            }
        }

        private void FireReceiveEvent(string text, byte[] bytes)
        {
            if (OnReceive != null)
            {
                OnReceive(text, bytes);
            }
        }

        #endregion

        private Exception _ConnectionException;

        private string ConnectionTypeFriendlyName
        {
            get
            {
                switch(ConnectionType)
                {
                    case TerminalConnectionType.Telnet:
                        return "telnet";
                    case TerminalConnectionType.SharpSsh:
                        return "SSH (with SharpSSH lib)";
                    case TerminalConnectionType.Ssh:
                        return "SSH";
                    case TerminalConnectionType.TCServer:
                        return "TCServer";
                    case TerminalConnectionType.HttpWebService:
                        return "HttpWebService";
                    case TerminalConnectionType.HttpRestService:
                        return "HttpRestService";
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        #region Abstract methods

        public abstract bool IsConnected { get; }

        protected abstract Exception EstablishConnection();

        protected abstract byte[] SendCommandText(string commandText);

        protected virtual byte[] SendCommandText(string commandText, bool isLastLogoutStep)
        {
           return  SendCommandText(commandText);
        }

        protected abstract byte[] ReadAvailableBytes();

        protected abstract string GetTextFromResponseBytes(byte[] buff);

        public abstract void Disconnect();

        public abstract TerminalConnectionType ConnectionType { get; }

        #endregion

        protected TerminalConnection(string host, int port, int pageCode)
        {
            _Host = host;
            _Port = port;
            _PageCode = pageCode;
        }

        protected readonly string _Host;
        protected readonly int _Port;
        protected readonly int _PageCode = 65001 /*DEFAULT UNICODE*/;

        public string Host
        {
            get { return _Host; }
        }

        public int Port
        {
            get { return _Port; }
        }

        public int PageCode
        {
            get { return _PageCode; }
        }

        public Exception SocketConnectionException
        {
            get { return _ConnectionException; }
        }
        
        internal Common.Logger Log { get; set; }

        protected void VerifyConnectionEstablished(string operationToHost)
        {
            if (!IsConnected)
            {
                string errorMessage = string.Format("Cannot {2} {0}:{1} since there is no established {3} connection to it", _Host, _Port, operationToHost, ConnectionTypeFriendlyName);
                throw new ApplicationException(errorMessage, _ConnectionException);
            }
            
        }

        private string _OFSBuffer = "";
        private string _OfsRequestStart = "<MSG>";
        private string _OfsRequestEnd = "</MSG>";
        public bool AttempCorrectionForSentCmd = false; // TODO: make configurable

        public string GetAvailableText(out bool hasBytes, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            VerifyConnectionEstablished("read data from");
            byte[] rawData = ReadAvailableBytes();
            hasBytes = (rawData != null && rawData.Length > 0);

            string result = GetTextFromResponseBytes(rawData);

            string remainder = result;
            string correctedResult = result;

            while (!string.IsNullOrEmpty(remainder))
                remainder = logTextData(LastCommand, remainder, out correctedResult, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);

            // do not perform correction for OFS, the algoritham is not suitable
            return AttempCorrectionForSentCmd  ? correctedResult : result;
        }

        private string logTextData(string sentCommand, string textData, out string correctedtextData, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            if (sentCommand != null)
            {
                if (INPUT_USER.Trim() != "" && INPUT_PASSWORD.Trim() != "")
                {
                    sentCommand = sentCommand.Replace(INPUT_USER + "/" + INPUT_PASSWORD, INPUT_USER + "/******");
                }
                if (AUTHORIZATION_USER.Trim() != "" && AUTHORIZATION_PASSWORD.Trim() != "")
                {
                    sentCommand = sentCommand.Replace(AUTHORIZATION_USER + "/" + AUTHORIZATION_PASSWORD, AUTHORIZATION_USER + "/******");
                }
            }
            if (textData != null)
            {
                if (INPUT_USER.Trim() != "" && INPUT_PASSWORD.Trim() != "")
                {
                    textData = textData.Replace(INPUT_USER + "/" + INPUT_PASSWORD, INPUT_USER + "/******");
                }
                if (AUTHORIZATION_USER.Trim() != "" && AUTHORIZATION_PASSWORD.Trim() != "")
                {
                    textData = textData.Replace(AUTHORIZATION_USER + "/" + AUTHORIZATION_PASSWORD, AUTHORIZATION_USER + "/******");
                }
            }

            string remainder = "";
            correctedtextData = textData;

            if (textData.Length > 0)
            {
                // try to initialize or update the OFS buffer
                bool newOFS = textData.StartsWith(_OfsRequestStart)
                    ||
                    (textData.Length < _OfsRequestStart.Length && textData == _OfsRequestStart.Substring(0, textData.Length));
                
                bool oldOFS = !string.IsNullOrEmpty(_OFSBuffer);

                if ( newOFS || oldOFS)
                    _OFSBuffer += textData.EndsWith(_OfsRequestEnd) ? 
                                    textData :                          // finished ofs - go in new line
                                    textData.TrimEnd('\r', '\n', ' ') ;  // incomplete OFS - remain in same line


                // not OFS - normal operation
                if (string.IsNullOrEmpty(_OFSBuffer))
                {
                    if (AttempCorrectionForSentCmd)
                    {
                        var tempResult = checkForSplitSentCommand(sentCommand, textData.Split(NEW_LINE_CHARS));
                        if (tempResult.Count > 0)
                        {
                            var nlSep = textData.Contains("\r") ? "\r\n" : "\n";
                            correctedtextData = string.Join(nlSep, tempResult);
                        }
                    }
                    if (textData != null)
                    {
                        if (INPUT_USER.Trim() != "" && INPUT_PASSWORD.Trim() != "")
                        {
                            textData = textData.Replace(INPUT_USER + "/" + INPUT_PASSWORD, INPUT_USER + "/******");
                        }
                        if (AUTHORIZATION_USER.Trim() != "" && AUTHORIZATION_PASSWORD.Trim() != "")
                        {
                            textData = textData.Replace(AUTHORIZATION_USER + "/" + AUTHORIZATION_PASSWORD, AUTHORIZATION_USER + "/******");
                        }
                    }
                    Log.Debug(string.Format("{0} characters read: '{1}'", textData.Length, textData));
                    FireReceiveEvent(correctedtextData, null);
                }

                // BUG - what if the buffer contains </msg> and also  <-/msg>
                if (_OFSBuffer.Contains(_OfsRequestEnd))
                {

                    var enryptedOFS = _OFSBuffer.Substring(_OfsRequestStart.Length);
                    enryptedOFS = enryptedOFS.Substring(0, enryptedOFS.IndexOf(_OfsRequestEnd));
                    remainder = _OFSBuffer.Substring(_OfsRequestStart.Length + enryptedOFS.Length + _OfsRequestEnd.Length); // the part after the sent ofs

                    // ENQUIRY.SELECT,/I/PROCESS,GEORGE001/1234567,%STANDARD.SELECTION'
                    // ENQUIRY.SELECT,          ,GEORGE001/1234567,%STANDARD.SELECTION'
                    // app        ,      func   , user/pass       , @ID , fields...

                    var ofsParts = enryptedOFS.Split(',');

                    if (ofsParts.Length >= 3)
                    {
                        if (ofsParts[2].Contains('/'))
                        {
                            string[] tmp = ofsParts[2].Split('/');
                            if (tmp != null && tmp.Length > 1)
                            tmp[1] = "******";
                            ofsParts[2] = string.Join("/", tmp);
                        }

                        enryptedOFS = string.Concat(_OfsRequestStart, string.Join(",", ofsParts), _OfsRequestEnd);
                    }

                    if (enryptedOFS != null)
                    {
                        if (INPUT_USER.Trim() != "" && INPUT_PASSWORD.Trim() != "")
                        {
                            enryptedOFS = enryptedOFS.Replace(INPUT_USER + "/" + INPUT_PASSWORD, INPUT_USER + "/******");
                        }
                        if (AUTHORIZATION_USER.Trim() != "" && AUTHORIZATION_PASSWORD.Trim() != "")
                        {
                            enryptedOFS = enryptedOFS.Replace(AUTHORIZATION_USER + "/" + AUTHORIZATION_PASSWORD, AUTHORIZATION_USER + "/******");
                        }
                    }

                    Log.Debug(string.Format("{0} characters read: '{1}'", enryptedOFS.Length, enryptedOFS));
                    FireReceiveEvent(_OfsRequestStart + enryptedOFS + _OfsRequestEnd + "\r\n", null);
                    _OFSBuffer = "";
                }

            }
            return remainder;
        }

        private static readonly char[] CHARS_TO_SKIP_IN_SENT_COMMAND = { ' ', '\t', '\r', '\n' };
        private static readonly char[] NEW_LINE_CHARS = { '\r', '\n' };
        private List<string> checkForSplitSentCommand(string sentCommand, string[] result)
        {
            List<string> tempResult = new List<string>();

            var reducedSent = reduceString(sentCommand);
            if (reducedSent == string.Empty) return tempResult;

            var templine = string.Empty;

            bool foundSent = false;
            for (int i = 0; i < result.Length; i++)
            {
                if (foundSent)
                    tempResult.Add(result[i]);
                else
                {
                    templine += reduceString(result[i]);

                    if (!string.IsNullOrEmpty(templine))
                    {
                        // there is something, but sent doesnt start with it
                        // no need to check further
                        if (!reducedSent.StartsWith(templine)) break;

                        if (templine == reducedSent)
                        {
                            foundSent = true;
                            tempResult.Add(sentCommand);
                        }
                    }
                }
            }

            return tempResult;
        }

        private string reduceString(string line)
        {
            var templine = string.Empty;

            if (!string.IsNullOrEmpty(line))
                for (int ccc = 0; ccc < line.Length; ccc++)
                {
                    char lnChar = line[ccc];
                    if (!CHARS_TO_SKIP_IN_SENT_COMMAND.Contains(lnChar))
                        templine += lnChar;
                }

            return templine;
        }



        public bool Connect()
        {
            Disconnect();

            Log.Info(string.Format("Trying to establish a {0} connection to {1}:{2}", ConnectionTypeFriendlyName, _Host, _Port));
            _ConnectionException = EstablishConnection();

            if (_ConnectionException == null)
            {
                Log.Info(string.Format("A {0} connection to {1}:{2} established", ConnectionTypeFriendlyName, _Host, _Port));
            }
            else
            {
                Log.Warn(string.Format("Failed to establish {0} connection to {1}:{2}", ConnectionTypeFriendlyName, _Host, _Port), _ConnectionException);

                if (_ConnectionException.Data != null)
                {
                    if (_ConnectionException.Data.Contains("algneg"))
                    {
                        Log.Warn(_ConnectionException.Data["algneg"]);
                    }

                }
            }

            return _ConnectionException != null;
        }

        public void Send(string commandText, bool isLastLogoutStep)
        {
            VerifyConnectionEstablished("write data to");

            // we should always clear the current available text in the active telnet session, so that it does not interfere with the processing of the new command
            // NOTE: Unfortunately there is no 100% guarantee that all text from previous command execution is fully available at this point
            CleanUnprocessedText();

            byte[] bytesToSend = SendCommandText(commandText, isLastLogoutStep);

            FireSendEvent(commandText + "\r", bytesToSend);
        }

        private void CleanUnprocessedText()
        {
            bool hasBytes;
            string txtLeft = GetAvailableText(out hasBytes);
            if (txtLeft.Length > 0)
            {
                Log.Debug(string.Format("Cleared {0} unprocessed characters: {1}", txtLeft.Length, txtLeft));
            }
        }

        public override string ToString()
        {
            return string.Format("Host: {0}:{1} Page Code: {2} Type: {3}", _Host, _Port, _PageCode, ConnectionTypeFriendlyName);
        }
    }
}