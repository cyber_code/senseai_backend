using System;

namespace T24TelnetAdapter
{
    /// <summary>
    /// Holder of values for a field (the values are gathered before and after deployment)
    /// </summary>
    [Serializable]
    public class DeploymentFieldValues
    {
        private string _FieldName;
        private string _OriginalValue;
        private string _TargetValue;
        private string _FinalValue;

        /// <summary>
        /// Gets or sets the name of the field.
        /// </summary>
        /// <value>The name of the field.</value>
        public string FieldName
        {
            get { return _FieldName; }
            set { _FieldName = value; }
        }

        /// <summary>
        /// Gets or sets the original value (the value on the existing T24 record)
        /// </summary>
        /// <value>The original value.</value>
        public string OriginalValue
        {
            get { return _OriginalValue; }
            set { _OriginalValue = value; }
        }

        /// <summary>
        /// Gets or sets the target value (the value of the deployed SAS record)
        /// </summary>
        /// <value>The target value.</value>
        public string TargetValue
        {
            get { return _TargetValue; }
            set { _TargetValue = value; }
        }

        /// <summary>
        /// Gets or sets the final value (the value of the T24 record after deployment)
        /// </summary>
        /// <value>The final value.</value>
        public string FinalValue
        {
            get { return _FinalValue; }
            set { _FinalValue = value; }
        }
    }
}