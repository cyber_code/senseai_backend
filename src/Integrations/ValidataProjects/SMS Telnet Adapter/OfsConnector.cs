using OFSCommonMethods.IO;

namespace T24TelnetAdapter
{
    public class OfsConnector : T24TelnetConnector
    {
        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.OFS; }
        }
    }
}