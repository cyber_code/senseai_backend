using System;
using System.Collections.Generic;

namespace T24TelnetAdapter
{
    public enum T24FileLocation
    {
        NotExisting,
        ExistingInDEV,
        ExistingInGlobus, // System
    }

    [Serializable]
    public class CompilationAnalysisResult
    {
        public List<string> AffectedFiles = new List<string>();

        public List<string> AffectedObjectFiles = new List<string>();

        public List<string> DuplicateFiles = new List<string>();

        public string SourceDir = "";

        public string ErrorMessage;

        public T24FileLocation CurrentState = T24FileLocation.NotExisting;

        public bool HasFailed
        {
            get { return ErrorMessage != null; }
        }
    }
}