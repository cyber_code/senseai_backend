using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using OFSCommonMethods.IO;
using OFSCommonMethods.IO.Telnet;

namespace T24TelnetAdapter
{
    public class GlobusClassicConnector : T24TelnetConnector
    {
        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.Classic; }
        }

        #region Classes

        private class UnableToExitException : Exception
        {
            internal UnableToExitException(string message) : base(message) { }
        }

        #endregion

        private bool _IsExecutedSuccessfully;

        public bool IsExecutedSuccessfully
        {
            get { return _IsExecutedSuccessfully; }
        }

        #region TSSTelnetComm Overrides

        public override bool Login()
        {
            // TODO should we try/catch
            if (base.Login())
            {
                return true;
            }

            try
            {
                // TODO might need to change
                RecoverFromLoginFail(_LastResponse);
            }
            catch (UnableToExitException ex)
            {
                Log.Debug(string.Format("ERROR ({0})\r\n", ex.GetType().ToString()) + ex.Message + "\r\nCalling: ExitFromEverywhere\r\n");
                ExitFromEverywhere();
            }

            return false;
        }

        protected override bool Logout()
        {
            // TODO:
            //  exit from main screen of Classic: 'BK'
            //  exit from other screen (go 1 screen back): 'CTRL+U'
            if (base.Logout())
            {
                return true;
            }

            // Unsuccessfull logout
           
            Log.Debug(string.Format(
                    "ERROR (Unable to exit from Classic)\r\n{0}\r\nCalling: ExitFromEverywhere\r\n",
                    _LastResponse
                    ));
            ExitFromEverywhere();
            return true;
        }

        private ClassicSettings GetClassicSetttings()
        {
            return (ClassicSettings)_ConnectionSettings;
        }

        public override bool RunTelnetCommand(string command, EndOfResponseCondition endOfResponseCondition, UnexpectedCommunicationErrorList unexpectedCommunicationErrors, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            _IsExecutedSuccessfully = false;
            string stringForDebug = EmptyReceiveBuffer(string.Empty);
            Debug.Assert(string.IsNullOrEmpty(stringForDebug));

            try
            {
                VerifyIsLoggedIn();

                Send(command);

                ClassicSettings.Response classicResponse = GetClassicSetttings().GetResponseFor("Main", command);

                if (!EndOfResponseCondition.IsValid(endOfResponseCondition))
                {
                    endOfResponseCondition = new EndOfResponseCondition(classicResponse.WaitFor);
                }

                string dummyErrMsg;
                string result = Receive(endOfResponseCondition, unexpectedCommunicationErrors, out dummyErrMsg);

                LastStatus = (result == null) ? ResponseStatus.TimeOut : ResponseStatus.Total;
                string completeResponse = EmptyReceiveBuffer(_LastResponse);

                if (LastStatus == ResponseStatus.TimeOut)
                {
                    string errMsg = string.Format(
                            "ERROR ({0}) NOT RECEIVED\r\n{1}\r\nCalling: ExitFromEverywhere\r\n",
                            endOfResponseCondition,
                            completeResponse
                        );

                    Log.Debug(errMsg);

                    SetError(errMsg);
                    ResponseBuffer = new string[] { errMsg };
                    ExitFromEverywhere();
                    return false;
                }

                TelnetResponseParser parser = new TelnetResponseParser();
                ResponseBuffer = parser.GetGoodStringsTrim(completeResponse).ToArray();
                
                Log.Debug(string.Format("RECEIVED MESSAGE PARTS FROM CLASSIC:\r\n{0}\r\n", StrArr2Str(ResponseBuffer)));

                _IsExecutedSuccessfully = classicResponse.IsSuccess(ResponseBuffer);
                // TODO??????? string[] realResult = classicResponse.GetMessageItemsArray(ResponseBuffer);
                // ResponseBuffer = realResult;

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Failed to process command '{0}'", command), ex);
                SetError(ex.Message);
                ResponseBuffer = new string[] { "Error: " + ex.Message };
                return false;
            }
        }

        private string Receive(EndOfResponseCondition endOfResponseCondition, UnexpectedCommunicationErrorList unexpectedCommunicationErrors, out string errMsg)
        {
            errMsg = null;
            _LastResponse = null;
            string response = string.Empty;
            DateTime dtStart = DateTime.Now;
            int lastErr = 0;
            do
            {
                bool hasBytes;
                string partialResponse = GetAvailableText(out hasBytes);
                if (hasBytes)
                {
                    dtStart = DateTime.Now;
                }

                response += partialResponse;
                _LastResponse = response;

                if (unexpectedCommunicationErrors != null)
                {
                    // NOTE: This has been implmented only for the special case of accepting overrides when verifying BCON packages, so no guarantee it would work for other scenarios
                    UnexpectedCommunicationError communicationInterruption = unexpectedCommunicationErrors.FindForMessage( response, ref lastErr);

                    if (communicationInterruption != null && communicationInterruption.LinesToSend.Length > 0)
                    {
                        // set more user friendly error
                        if (communicationInterruption.IsError)
                        {
                            SetError(communicationInterruption.GetDisplayErrorMessage(response));
                        }

                        // make sure everything is read
                        EmptyReceiveBuffer("");

                        // try to recover from the error
                        foreach (string command in communicationInterruption.LinesToSend)
                        {
                            Send(command);

                            // todo - for real recovery implement receive instead of Sleep
                            //Thread.Sleep(3000);

                            // log out since on some environment, the connection to the Globus Classic is dropped and needs to be reestablished from scratch
                           // _IsLogged = false;
                            
                            this.Disconnect();
                            return response;
                        }

                        // most likely there will be 
                        if (communicationInterruption.IsError)
                        {
                            LastStatus = ResponseStatus.UnexpectedError;
                        }
                        else
                        {
                            LastStatus = ResponseStatus.Total;
                        }
                    }
                }

                if (endOfResponseCondition.IsSatisfied(response))
                {
                    return response;
                }

                Thread.Sleep(20);
            }
            while ((DateTime.Now - dtStart).TotalMilliseconds < _TimeOut);

            errMsg = string.Format("Did not receive expected text '{0}' in {1} miliseconds", endOfResponseCondition, _TimeOut);
            Log.Warn(_Error);

            return null;
        }


        #endregion

        #region Private Methods

        private void RecoverFromLoginFail(string lastResponse)
        {
            TelnetResponseParser parser = new TelnetResponseParser();
            List<string> strings = parser.GetGoodStringsTrim(lastResponse);

            int loginStage = -1;

            if (strings.Contains("PLEASE ENTER YOUR SIGN ON NAME"))
            {
                loginStage = 1;
            }
            else if (strings.Contains("PLEASE ENTER YOUR PASSWORD"))
            {
                loginStage = 2;
            }
            else
            {
                foreach (string s in strings)
                {
                    if (s.StartsWith("CONTINUE (Y)"))
                    {
                        loginStage = 3;
                        break;
                    }
                }
            }

            if (loginStage < 0)
            {
                // Failed somewhere elese!
                return;
            }

            //  3* Iterations:
            //      (USER, PASS, Y)
            //  ==> HERE WE ARE IN J-SHELL
            for (int iter = 0; iter < 3; iter++)
            {
                for (int curr = loginStage; curr < 4; curr++)
                {
                    string waitFor = null;
                    switch (curr)
                    {
                        case 1:
                            waitFor = "PLEASE ENTER YOUR PASSWORD";
                            break;
                        case 2:
                            waitFor = "SECURITY.VIOLATION";
                            break;
                        case 3:
                            waitFor = (iter < 2) ? "PLEASE ENTER YOUR SIGN ON NAME" : "-->";
                            break;
                        default:
                            Debug.Fail("Impossible!");
                            break;
                    }

                    ExecutionStep es = new ExecutionStep("RECOVER FROM UNSUCCESSFULL LOGIN", "Y", waitFor);

                    if (!ExecuteStep(es))
                    {
                        throw new UnableToExitException(_LastResponse);
                    }
                }

                loginStage = 1;
            }

            {
                ExecutionStep es = new ExecutionStep("RECOVER FROM UNSUCCESSFULL LOGIN - EXIT FROM J-SHELL", "exit", null);

                if (!ExecuteStep(es))
                {
                    throw new UnableToExitException(_LastResponse);
                }
            }

            _IsLogged = false;
        }

        private void ExitFromEverywhere()
        {
            try
            {
                {
                    string exitAllCommand = new string(new char[] { (char)0x00, (char)0x03 });
                    ExecutionStep es = new ExecutionStep("EXIT FROM EVERYWHERE", exitAllCommand, null);
                    ExecuteStep(es);
                    Thread.Sleep(200);
                }

                {
                    ExecutionStep es = new ExecutionStep("EXIT FROM DEBUGGER", "q", null);
                    ExecuteStep(es);
                    Thread.Sleep(200);
                }

                {
                    ExecutionStep es = new ExecutionStep("EXIT FROM DEBUGGER (y)", "y", null);
                    ExecuteStep(es);
                    Thread.Sleep(200);
                }

                {
                    ExecutionStep es = new ExecutionStep("EXIT FROM BATCH JOB (y)", "y", null);
                    ExecuteStep(es);
                    Thread.Sleep(200);
                }

                // TODO in some environment,  the last step is not "exit", but 'X' or something else... This code will not handle it well

                const int maxExits = 10;
                for (int i = 0; i < maxExits && IsConnected; i++)
                {
                    ExecutionStep es = new ExecutionStep("EXIT FROM J-SHELL", "exit", null);
                    try
                    {
                        ExecuteStep(es);
                    }
                    catch { }

                    Thread.Sleep(200);
                }


            }
            catch (Exception)
            {
                // Do nothing
            }
            finally
            {
                _IsLogged = false;
            }
        }

        private string EmptyReceiveBuffer(string residual)
        {
            // TODO reuse base class method
            try
            {
                for (int i = 0; i < 100 /*MAX ITERATIONS*/; i++)
                {
                    Thread.Sleep(50);

                    bool hasBytes;
                    string temp = GetAvailableText(out hasBytes);
                    if (!hasBytes)
                    {
                        break;
                    }

                    residual += temp;
                }

                return residual;
            }
            catch // just in case
            {
                return null;
            }
        }

        private static string StrArr2Str(string[] arr)
        {
            StringBuilder sbResult = new StringBuilder();
            foreach (string s in arr)
            {
                sbResult.AppendLine(s);
            }

            return sbResult.ToString();
        }




        #endregion
    }
}