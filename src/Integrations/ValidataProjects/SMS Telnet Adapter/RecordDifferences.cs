﻿using System.Collections.Generic;

namespace T24TelnetAdapter
{
    public class FieldValueDifference
    {
        public string FieldName;
        public string Value1;
        public string Value2;

        public FieldValueDifference(string fieldName, string value1, string value2)
        {
            FieldName = fieldName;
            Value1 = value1;
            Value2 = value2;
        }
    }

    public class RecordDifferences
    {
        private readonly List<FieldValueDifference> _Diffs = new List<FieldValueDifference>();

        public bool Match
        {
            get { return _Diffs.Count == 0 && string.IsNullOrEmpty(ErrorText); }
        }

        public int NumDifferences
        {
            get { return _Diffs.Count; }
        }

        public readonly string ID;
        public readonly string DeploymentPack;
        public readonly string Version;
        public readonly string ItemType;

        public string ErrorText;

        public RecordDifferences(string id, string packName, string itemType, string version)
        {
            ID = id;
            DeploymentPack = packName;
            ItemType = itemType;
            Version = version;
        }

        public void AddDifference(string field, string sasValue, string envValue)
        {
            _Diffs.Add(new FieldValueDifference(field, sasValue, envValue));
        }

        public void Get1Difference(int index, out string field, out string sasValue, out string envValue)
        {
            FieldValueDifference diff = _Diffs[index];
            field = diff.FieldName;
            sasValue = diff.Value1;
            envValue = diff.Value2;
        }
    }
}