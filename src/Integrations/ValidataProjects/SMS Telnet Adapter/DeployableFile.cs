using System;
using SCApiInterface;
using ValidataCommon;

namespace T24TelnetAdapter
{
    [Serializable]
    public class DeployableFile
    {
        public DeploymentType DeploymentType;

        public readonly IFile IFile;

        public readonly string DeploymentPack;

        public bool IsCatalog;

        public string ServerDeploymentFolder;

        public DeployableFile()
        {
            // empty constructor necessary for serialization
        }

        public DeployableFile(IFile iFile, string deploymentPack, string deploymentFolder)
        {
            IFile = iFile;
            DeploymentPack = deploymentPack;
            ServerDeploymentFolder = deploymentFolder;
        }

        public string Name
        {
            get { return IFile.Name; }
        }

        public string RoutineName
        {
            get { return !IsBinaryDollarFile ? Name : T24FilesAnalyzer.GetRoutineNameFromDollarFile(Name); }
        }

        public bool IsBinaryDollarFile
        {
            get { return IFile.IsBinary & T24FilesAnalyzer.IsDollarFile(Name); }
        }
    }
}