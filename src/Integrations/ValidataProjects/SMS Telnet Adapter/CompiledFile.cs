using System;
using System.Collections.Generic;
using System.Linq;

namespace T24TelnetAdapter
{
    [Serializable]
    public class CompiledFile
    {
        public DeployableFile File;

        public DeploymentMethod? DeployMethod;

        public CompileOutput Output;

        public CompilationAnalysisResult AnalysisBeforeCompilation;

        public CompilationAnalysisResult AnalysisAfterCompilation;

        public string[] GetAffectedServerFilesPaths()
        {
            List<string> result = new List<string>();

            if (AnalysisBeforeCompilation != null)
            {
                result.AddRange(AnalysisBeforeCompilation.AffectedFiles);
            }

            if (AnalysisAfterCompilation != null)
            {
                result.AddRange(AnalysisAfterCompilation.AffectedFiles);
            }

            result = result.Distinct().ToList();

            return result.ToArray();
        }

        public string[] GetNewlyCreatedServerFilesPaths()
        {
            List<string> result = new List<string>();

            if (AnalysisAfterCompilation != null)
            {
                foreach (string filePath in AnalysisAfterCompilation.AffectedFiles)
                {
                    if (! AnalysisBeforeCompilation.AffectedFiles.Contains(filePath))
                    {
                        result.Add(filePath);
                    }
                }
            }

            return result.ToArray();
        }

        public string FindAffectedFilesErrorMessage
        {
            get
            {
                if (AnalysisBeforeCompilation == null)
                {
                    return null;
                }

                return AnalysisBeforeCompilation.ErrorMessage;
            }
        }

        public string Name
        {
            get { return File.Name; }
        }

        /// <summary>
        /// Gets the name of the routine (it might be different for $ files only)
        /// </summary>
        /// <value>The name of the routine.</value>
        public string RoutineName
        {
            get { return File.RoutineName; }
        }

        public string Version
        {
            get { return File.IFile.Version; }
        }

        public bool IsNewlyCreated
        {
            get { return (AnalysisBeforeCompilation != null && AnalysisBeforeCompilation.AffectedFiles.Count == 0); }
        }

        public bool IsSuccessful
        {
            get
            {
                return Output != null
                       && ! IsSkipped
                       && (Output.Outcome == CompileOutcome.Success || Output.Outcome == CompileOutcome.Warnings);
            }
        }

        public bool IsFailed
        {
            get { return !IsSuccessful && !IsSkipped; }
        }

        public bool IsSkipped { get; set; }

        public string SkippedMessage { get; set; }
    }
}