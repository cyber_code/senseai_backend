﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.WinControls.UI;


namespace SMS_Telnet_Adapter
{
    /*
    public static class GridHelper
    {
        public static void InitCompileGrid(RadGridView Grid)
        {
            Grid.MasterGridViewTemplate.AutoExpandGroups = true;
            Grid.MasterGridViewTemplate.EnableGrouping = true;
            Grid.MasterGridViewTemplate.AllowDragToGroup = false;
            Grid.ReadOnly = true;

            // The master

            GridViewTextBoxColumn textColumn =
                new GridViewTextBoxColumn("Source Name");
            textColumn.Width = 150;
            Grid.MasterGridViewTemplate.Columns.Add(textColumn);

            textColumn = new GridViewTextBoxColumn("Outcome");
            textColumn.Width = 150;
            Grid.MasterGridViewTemplate.Columns.Add(textColumn);

            textColumn = new GridViewTextBoxColumn("Deployment Pack");
            textColumn.Width = 150;
            Grid.MasterGridViewTemplate.Columns.Add(textColumn);


            // The child template

            GridViewTemplate template = new GridViewTemplate();

            GridViewTextBoxColumn TempTextColumn = new GridViewTextBoxColumn("Source Name");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);

            TempTextColumn = textColumn = new GridViewTextBoxColumn("Issue");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);

            TempTextColumn = new GridViewTextBoxColumn("Description");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);

            Grid.MasterGridViewTemplate.ChildGridViewTemplates.Add(template);


            // The relation

            GridViewRelation relation = new GridViewRelation(Grid.MasterGridViewTemplate);

            relation.ChildTemplate = template;
            relation.RelationName = "Source Name";

            relation.ParentColumnNames.Add("Source Name");
            relation.ChildColumnNames.Add("Source Name");


            //Grid.Relations.Clear();
            Grid.Relations.Add(relation);


        }
        public static void AddColumns(RadGridView Grid, List<CompileOutput> Outputs)
        {

            foreach (CompileOutput Output in Outputs)
            {
                string SourceName = Output.SourceName;

                Grid.MasterGridViewTemplate.Rows.Add(SourceName, Output.Outcome, "Pack1");
                GridViewTemplate template = Grid.MasterGridViewTemplate.ChildGridViewTemplates[0];

                foreach (string Etext in Output.ParseErrors)
                {
                    template.Rows.Add(SourceName, "Error", Etext);
                    template.Rows[template.Rows.Count - 1].Height = Etext.Split('\n').Count<string>() * 20;
                    template.Rows[template.Rows.Count - 1].IsExpanded = true;
                    
                      //  template.MasterGridViewTemplate.ChildGridViewTemplates[0].Columns[1].Width = smsvalue.Length * 25;
                    //    template.MasterGridViewTemplate.ChildGridViewTemplates[0].Columns[2].Width = envvalue.Length * 25;
                }
                foreach (string Wtext in Output.Warnings)
                {
                    template.Rows.Add(SourceName, "Warning", Wtext);

                    template.Rows[template.Rows.Count - 1].Height = Wtext.Split('\n').Count<string>() * 20;
                    template.Rows[template.Rows.Count - 1].IsExpanded = true;
                }
                foreach (string Ltext in Output.LinkError)
                {
                    template.Rows.Add(SourceName, "Linker Error", Ltext);

                    template.Rows[template.Rows.Count - 1].Height = Ltext.Split('\n').Count<string>() * 20;
                    template.Rows[template.Rows.Count - 1].IsExpanded = true;
                }
                if (!String.IsNullOrEmpty(Output.Include))
                    template.Rows.Add(SourceName, "Missing include file", Output.Include);

                template.BestFitColumns();

            }
            Grid.Visible = true;

        }

        /*
        public static void FillCompareGrid(RadGridView Grid, List<RecordDifferences> Outputs)
        {
            Grid.MasterGridViewTemplate.AutoExpandGroups = true;
            Grid.ReadOnly = true;


            // The master

            GridViewTextBoxColumn textColumn =
                new GridViewTextBoxColumn("Record Name");
            textColumn.Width = 150;
            Grid.MasterGridViewTemplate.Columns.Add(textColumn);

            textColumn = new GridViewTextBoxColumn("Outcome");
            textColumn.Width = 150;
            Grid.MasterGridViewTemplate.Columns.Add(textColumn);

            textColumn = new GridViewTextBoxColumn("Deployment Pack");
            textColumn.Width = 150;
            Grid.MasterGridViewTemplate.Columns.Add(textColumn);


            // The child template

            GridViewTemplate template = new GridViewTemplate();

            GridViewTextBoxColumn TempTextColumn = new GridViewTextBoxColumn("Record Name");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);
                
            TempTextColumn = new GridViewTextBoxColumn("Field");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);

            TempTextColumn = new GridViewTextBoxColumn("Value in SAS");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);

            TempTextColumn = new GridViewTextBoxColumn("Value on T24 env");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);

            Grid.MasterGridViewTemplate.ChildGridViewTemplates.Add(template);

            // The relation

            GridViewRelation relation = new GridViewRelation(Grid.MasterGridViewTemplate);

            relation.ChildTemplate = template;
            relation.RelationName = "Record Name";

            
            relation.ParentColumnNames.Add("Record Name");
            relation.ChildColumnNames.Add("Record Name");

            
            //Grid.Relations.Clear();
            Grid.Relations.Add(relation);


            foreach (RecordDifferences Output in Outputs)
            {
                if (Output == null)
                {
                    Grid.MasterGridViewTemplate.Rows.Add("-", "Record not found", "Pack1");
                }

                Grid.MasterGridViewTemplate.Rows.Add(Output.ID, (Output.Match) ? "MATCH" : "MISSMATCH", "Pack1");


                GridViewTemplate template0 = Grid.MasterGridViewTemplate.ChildGridViewTemplates[0];

                for (int i = 0; i < Output.NumDifferences; i++)
                {
                    string field, smsvalue, envvalue;
                    Output.Get1Difference(i, out field, out smsvalue, out envvalue);

                    template0.Rows.Add(Output.ID, field, smsvalue, envvalue);
                    
                     template.Rows[template.Rows.Count - 1].Height = 30;
                        template.Rows[template.Rows.Count - 1].IsExpanded = true;

                        template.MasterGridViewTemplate.ChildGridViewTemplates[0].Columns[1].Width = smsvalue.Length * 25;
                        template.MasterGridViewTemplate.ChildGridViewTemplates[0].Columns[2].Width = envvalue.Length * 25;

                }

            }

        }


        public static void InitCompareGrid(RadGridView Grid)
        {
            Grid.MasterGridViewTemplate.AutoExpandGroups = true;
            Grid.MasterGridViewTemplate.EnableGrouping = true;
            Grid.MasterGridViewTemplate.AllowDragToGroup = false;
            Grid.ReadOnly = true;


            // The master

            GridViewTextBoxColumn textColumn =
                new GridViewTextBoxColumn("Record Name");
            textColumn.Width = 150;
            Grid.MasterGridViewTemplate.Columns.Add(textColumn);

            textColumn = new GridViewTextBoxColumn("Outcome");
            textColumn.Width = 150;
            Grid.MasterGridViewTemplate.Columns.Add(textColumn);

            textColumn = new GridViewTextBoxColumn("Deployment Pack");
            textColumn.Width = 150;
            Grid.MasterGridViewTemplate.Columns.Add(textColumn);


            // The child template

            GridViewTemplate template = new GridViewTemplate();

            GridViewTextBoxColumn TempTextColumn = new GridViewTextBoxColumn("Record Name");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);

            TempTextColumn = new GridViewTextBoxColumn("Field");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);

            TempTextColumn = new GridViewTextBoxColumn("Value in SAS");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);

            TempTextColumn = new GridViewTextBoxColumn("Value on T24 env");
            TempTextColumn.Width = 150;
            template.Columns.Add(TempTextColumn);

            Grid.MasterGridViewTemplate.ChildGridViewTemplates.Add(template);

            // The relation

            GridViewRelation relation = new GridViewRelation(Grid.MasterGridViewTemplate);

            relation.ChildTemplate = template;
            relation.RelationName = "Record Name";


            relation.ParentColumnNames.Add("Record Name");
            relation.ChildColumnNames.Add("Record Name");


            //Grid.Relations.Clear();
            Grid.Relations.Add(relation);
        }
        public static void AddColumns(RadGridView Grid, List<RecordDifferences> Outputs)
        {

            foreach (RecordDifferences Output in Outputs)
            {
                if (Output == null)
                {
                    Grid.MasterGridViewTemplate.Rows.Add("-", "Record not found", "Pack1");
                }

                Grid.MasterGridViewTemplate.Rows.Add(Output.ID, (Output.Match) ? "MATCH" : "MISSMATCH", "Pack1");


                GridViewTemplate template = Grid.MasterGridViewTemplate.ChildGridViewTemplates[0];

                for (int i = 0; i < Output.NumDifferences; i++)
                {
                    string field, smsvalue, envvalue;
                    Output.Get1Difference(i, out field, out smsvalue, out envvalue);

                    template.Rows.Add(Output.ID, field, smsvalue, envvalue);
                    template.Columns[0].TextAlignment = 
                }
                template.BestFitColumns();
            }
            
            Grid.Visible = true;

        }


    }
*/
}
