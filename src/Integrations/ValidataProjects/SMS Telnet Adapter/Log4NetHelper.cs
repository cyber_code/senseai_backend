﻿using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace T24TelnetAdapter
{
    internal class Log4NetHelper
    {
        // Set the level for a named logger
        public static void SetLevel(string loggerName, string levelName)
        {
            ILog log = LogManager.GetLogger(loggerName);
            Logger l = (Logger) log.Logger;

            l.Level = l.Hierarchy.LevelMap[levelName];
        }

        // Add an appender to a logger
        public static void AddAppender(string loggerName, IAppender appender)
        {
            ILog log = LogManager.GetLogger(loggerName);
            Logger l = (Logger) log.Logger;

            l.AddAppender(appender);
        }

        public static bool HasDebugAppender(string loggerName)
        {
            ILog log = LogManager.GetLogger(loggerName);
            Logger l = (Logger) log.Logger;

            foreach (IAppender appender in  l.Appenders)
            {
                if (appender.GetType() == typeof (DebugAppender))
                {
                    return true;
                }
            }

            return false;
        }

        public static IAppender CreateDebugAppender(string name)
        {
            DebugAppender appender = new DebugAppender();
            appender.Name = name;

            appender.Layout = GetDefaultLayout();
            appender.ActivateOptions();

            return appender;
        }

        // Create a new file appender
        public static IAppender CreateFileAppender(string name, string fileName)
        {
            FileAppender appender = new FileAppender();
            appender.Name = name;
            appender.File = fileName;
            appender.AppendToFile = true;

            appender.Layout = GetDefaultLayout();
            appender.ActivateOptions();

            return appender;
        }

        private static PatternLayout GetDefaultLayout()
        {
            PatternLayout layout = new PatternLayout();
            layout.ConversionPattern = "%d [%t] %-5p %c [%x] - %m%n";
            layout.ActivateOptions();
            return layout;
        }
    }
}