﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using AXMLEngine;
using Common;
using OFSCommonMethods.IO;
using SCApiInterface;
using ValidataCommon;
using ValidataFtp;
using OperationLog = Common.OperationLog;

namespace T24TelnetAdapter
{
    public class BuildControlDeployment
    {
        private string TargetPathForArchive
        {
            get { return _RequestManager.Settings.BuildControlDeploymentPath; }
        }

        public delegate void DeploymentEventHandler(string message);

        public event DeploymentEventHandler OnDeploymentEvent;

        public delegate bool BconDeploymentStepExecutionEventHandler(DeploymentOutput output);

        public event BconDeploymentStepExecutionEventHandler OnDeploymentStepExecuted;

        private readonly IABCFileConnector _FileConnector;

        private readonly RequestManager _RequestManager;

        public bool VerifyUsingGlobusClassic;

        private List<AttributeDefinition> _BconNormalizedFields;
        private TypicalDefinition _BuildControlTypicalMetadata;

        private bool _WasAborted;

        public bool WasAborted
        {
            get { return _WasAborted; }
        }

        public TypicalDefinition BuildControlTypicalMetadata
        {
            get { return _BuildControlTypicalMetadata; }
            set
            {
                _BuildControlTypicalMetadata = value;
                _BconNormalizedFields = value == null ? null : _BuildControlTypicalMetadata.GetNormalizedFields();
            }
        }

        public BuildControlDeployment(IABCFileConnector fileConnector, RequestManager requestManager)
        {
            Debug.Assert(fileConnector != null);
            _FileConnector = fileConnector;

            Debug.Assert(requestManager != null);
            _RequestManager = requestManager;
        }

        private static string GetStatusString(DeploymentOutput deploymentOutput)
        {
            return deploymentOutput.Outcome.ToString().ToUpper();
        }

        public bool DeployPackage(IDeployPackage deployPackage, bool deployUncompressed, IFile file, IRecord bconRecord,
            out List<DeploymentOutput> output, out DeploymentOutput deploymentResultRecordOutput, string version = null)
        {
            OperationLog bconDeploymentLog = new OperationLog("Build Control Deployment of package " + deployPackage.Name, false, true);

            output = new List<DeploymentOutput>();
            deploymentResultRecordOutput = null;

            bool hasFatalError;

            // TODO: Get affected records for merge on T24

            #region Uploading of file/directory

            if (deployUncompressed)
            {
                string directoryName = deployPackage.BConId;
                string tempDir = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["TempFolderForDLPackages"]) ?? "c:\\temp";
                string fullDirectoryPath = tempDir + @"\" + directoryName;

                FireDeploymentEvent("Uploading directory '{0}' to '{1}'.", fullDirectoryPath, TargetPathForArchive);
                DeploymentOutput deploymentOutput = UploadUncompressedDirectory(deployPackage.Name, fullDirectoryPath, directoryName, false);

                hasFatalError = deploymentOutput.Outcome != DeploymentOutcome.Successful;

                FireDeploymentEvent("Uploading directory '{0}' to '{1}': {2}.",
                                    fullDirectoryPath,
                                    TargetPathForArchive,
                                    GetStatusString(deploymentOutput));

                output.Add(deploymentOutput);
            }
            else
            {
                string deplPackInfo = deployPackage.Name;
                if (deployPackage.BConId != deployPackage.Name)
                {
                    deplPackInfo += " with @ID=" + deployPackage.BConId;
                }

                string rootToArchivePath;
                {
                    OperationLog uploadLog = new OperationLog("Uploading BCON package", false, false);
                    bconDeploymentLog.SubOperations.Add(uploadLog);

                    FireDeploymentEvent("Uploading archive '{0}' to directory '{1}'.", deplPackInfo, TargetPathForArchive);
                    DeploymentOutput deploymentOutput = UploadArchiveFile(deployPackage.Name, file, false, out rootToArchivePath);
                    hasFatalError = deploymentOutput.Outcome != DeploymentOutcome.Successful;
                    FireDeploymentEvent("Uploading archive '{0}' to directory '{1}' : {2}.", deployPackage.BConId, TargetPathForArchive, GetStatusString(deploymentOutput));
                    output.Add(deploymentOutput);
                    uploadLog.End();

                    if (hasFatalError)
                    {
                        return false;
                    }

                    if (!FireDeploymentStepExecutedEvent(deploymentOutput))
                    {
                        return false;
                    }
                }

                {
                    OperationLog uncompressLog = new OperationLog("Uncompress uploaded BCON package", false, false);
                    bconDeploymentLog.SubOperations.Add(uncompressLog);
                    FireDeploymentEvent("Uncompressing archive '{0}'.", deplPackInfo);
                    DeploymentOutput deploymentOutput = UncompressArchiveOnT24Environment(deployPackage.Name, file.Name, rootToArchivePath, hasFatalError);
                    hasFatalError = deploymentOutput.Outcome != DeploymentOutcome.Successful;
                    FireDeploymentEvent("Uncompressing archive '{0}': {1}.", deployPackage.BConId, GetStatusString(deploymentOutput));
                    output.Add(deploymentOutput);
                    uncompressLog.End();

                    if (!FireDeploymentStepExecutedEvent(deploymentOutput))
                    {
                        return false;
                    }
                }
            }

            #endregion

            #region I,V, V, S

            string buildControlRecordID;
            {
                OperationLog relogLog = new OperationLog("Re-logging to environment", false, false);
                bconDeploymentLog.SubOperations.Add(relogLog);

                FireDeploymentEvent("Re-logging to jShell.");
                DeploymentOutput deploymentOutput = RelogInJshell(deployPackage.Name, hasFatalError);
                hasFatalError = deploymentOutput.Outcome != DeploymentOutcome.Successful;
                FireDeploymentEvent("Re-logging to jShell: {0}.", GetStatusString(deploymentOutput));
                output.Add(deploymentOutput);
                relogLog.End();

                if (hasFatalError)
                {
                    return false;
                }

                if (!FireDeploymentStepExecutedEvent(deploymentOutput))
                {
                    return false;
                }
            }

            {
                FireDeploymentEvent("Inputting BCON record.");

                OperationLog inputBConLog = new OperationLog("Input of BCON record", false, false);
                bconDeploymentLog.SubOperations.Add(inputBConLog);

                DeploymentOutput deploymentOutput = CreateBuildControlRecord(deployPackage.Name, deployPackage.BConId, hasFatalError, bconRecord, version);
                hasFatalError = deploymentOutput.Outcome != DeploymentOutcome.Successful;
                FireDeploymentEvent("Inputting BCON record: {0}", GetStatusString(deploymentOutput));
                output.Add(deploymentOutput);
                inputBConLog.End();

                if (hasFatalError)
                {
                    return false;
                }

                buildControlRecordID = deploymentOutput.UpdatedInstanceOnT24.GetAttributeValue("@ID");

                if (!FireDeploymentStepExecutedEvent(deploymentOutput))
                {
                    return false;
                }
            }

            {
                OperationLog relogLog = new OperationLog("Re-logging to environment", false, false);
                bconDeploymentLog.SubOperations.Add(relogLog);

                FireDeploymentEvent("Re-logging to jShell.");
                DeploymentOutput deploymentOutput = RelogInJshell(deployPackage.Name, hasFatalError);
                hasFatalError = deploymentOutput.Outcome != DeploymentOutcome.Successful;
                FireDeploymentEvent("Re-logging to jShell: {0}.", GetStatusString(deploymentOutput));
                output.Add(deploymentOutput);
                relogLog.End();

                if (hasFatalError)
                {
                    return false;
                }

                if (!FireDeploymentStepExecutedEvent(deploymentOutput))
                {
                    return false;
                }
            }

            {
                OperationLog verify1Log = new OperationLog("First Verify of the BCON record", false, false);
                bconDeploymentLog.SubOperations.Add(verify1Log);

                FireDeploymentEvent("First Verify of the BCON record {0}.", buildControlRecordID);
                DeploymentOutput deploymentOutput = VerifyBuildControlRecord("First", deployPackage.Name, buildControlRecordID, bconRecord, hasFatalError, version);
                if (deploymentOutput.Outcome != DeploymentOutcome.Successful)
                    deploymentOutput.Outcome = DeploymentOutcome.Skipped;

                // ignore the result of the first first verify, the second is what's important
                FireDeploymentEvent("First Verify of the BCON record {0}: {1}.", buildControlRecordID, GetStatusString(deploymentOutput));
                output.Add(deploymentOutput);
                verify1Log.End();

                if (!FireDeploymentStepExecutedEvent(deploymentOutput))
                {
                    return false;
                }
            }

            {
                OperationLog relogLog = new OperationLog("Re-logging to environment", false, false);
                bconDeploymentLog.SubOperations.Add(relogLog);

                FireDeploymentEvent("Re-logging to jShell.");
                DeploymentOutput deploymentOutput = RelogInJshell(deployPackage.Name, hasFatalError);
                hasFatalError = deploymentOutput.Outcome != DeploymentOutcome.Successful;
                FireDeploymentEvent("Re-logging to jShell: {0}.", GetStatusString(deploymentOutput));
                output.Add(deploymentOutput);
                relogLog.End();

                if (hasFatalError)
                {
                    return false;
                }

                if (!FireDeploymentStepExecutedEvent(deploymentOutput))
                {
                    return false;
                }
            }

            {
                OperationLog verify2Log = new OperationLog("Second Verify of BCON record", false, false);
                bconDeploymentLog.SubOperations.Add(verify2Log);

                FireDeploymentEvent("Second Verify of the BCON record {0}.", buildControlRecordID);
                DeploymentOutput deploymentOutput = VerifyBuildControlRecord("Second", deployPackage.Name, buildControlRecordID, bconRecord, hasFatalError, version);
                hasFatalError = deploymentOutput.Outcome != DeploymentOutcome.Successful;
                if (_RequestManager.AbortDeployment)
                {
                    hasFatalError = true;
                    deploymentOutput.Outcome = DeploymentOutcome.Failed;
                    output[output.Count - 2].Outcome = DeploymentOutcome.Failed;
                }

                FireDeploymentEvent("Second Verify of the BCON record {0}: {1}.", buildControlRecordID, GetStatusString(deploymentOutput));
                output.Add(deploymentOutput);

                verify2Log.End();

                if (hasFatalError)
                {
                    return false;
                }

                if (!FireDeploymentStepExecutedEvent(deploymentOutput))
                {
                    return false;
                }
            }

            {
                OperationLog relogLog2 = new OperationLog("Re-logging to environment again", false, false);
                bconDeploymentLog.SubOperations.Add(relogLog2);

                FireDeploymentEvent("Re-logging to jShell.");
                DeploymentOutput deploymentOutput = RelogInJshell(deployPackage.Name, hasFatalError);
                hasFatalError = deploymentOutput.Outcome != DeploymentOutcome.Successful;
                FireDeploymentEvent("Re-logging to jShell: {0}.", GetStatusString(deploymentOutput));
                output.Add(deploymentOutput);
                relogLog2.End();

                if (hasFatalError)
                {
                    return false;
                }

                if (!FireDeploymentStepExecutedEvent(deploymentOutput))
                {
                    return false;
                }
            }

            // TODO?
            //{
            //    DeploymentOutput deploymentOutput = CheckBuildControlResults(packageName, fail);
            //    fail = deploymentOutput.Outcome != DeploymentOutcome.Successful;
            //    output.Add(deploymentOutput);
            //}

            #endregion

            OperationLog getDeployResultOutput = new OperationLog("Re-logging to environment again", false, false);
            bconDeploymentLog.SubOperations.Add(getDeployResultOutput);
            deploymentResultRecordOutput = GetDeployResultRecordOutput(
                (_BuildControlTypicalMetadata != null && !string.IsNullOrEmpty(_BuildControlTypicalMetadata.CatalogName)) ?
                                    _BuildControlTypicalMetadata.CatalogName : "Dummy"
                , deployPackage.Name, deployPackage.BConId);

            getDeployResultOutput.End();
            bconDeploymentLog.End();

            return true;
        }

        private DeploymentOutput GetDeployResultRecordOutput(string catalogName, string deployPackageName, string bconRecordId)
        {
            DeploymentOutput deploymentOutput = GetDefaultDeploymentOutput(deployPackageName, deployPackageName);
            var metadata = new MetadataTypical
            {
                CatalogName = catalogName,
                Name = deploymentOutput.TypicalName
            };

            if (deploymentOutput.TypicalName == "BUILD.CONTROL")
                _RequestManager.OFSParser_CustomAttributeAppender = BuildControlParseOFSAttributeAppender;

            string errorMessage;

            try
            {
                RequestManager.OFSResponseErrorType errorType;
                deploymentOutput.UpdatedInstanceOnT24 = _RequestManager.GetRecordDataByID(metadata, bconRecordId, true, out errorType, out errorMessage, _RequestManager.Settings.BCONVersion.See);
            }
            finally
            {
                _RequestManager.OFSParser_CustomAttributeAppender = null;
            }

            deploymentOutput.Details = errorMessage;

            return deploymentOutput;
        }

        private void BuildControlParseOFSAttributeAppender(NameValueCollection currentFieldValues, string attributeName, string attributeValue)
        {
            List<string> buildControlFieldsOrder = _BconNormalizedFields != null
                ? _BconNormalizedFields.Select(n => n.Name).ToList()
                : null;

            bool shoudlAppendAsValueOfTheLastField = false;

            var lastAddedAttributeName = currentFieldValues.Count > 0
                ? AttributeNameParser.GetShortFieldName(currentFieldValues.AllKeys[currentFieldValues.AllKeys.Length - 1])
                : null;

            // The custom logic for PROCESS.ERR is necessary, since it contains OFS-like values.
            if (buildControlFieldsOrder != null && lastAddedAttributeName == "PROCESS.ERR")
            {
                int processErrIndex = buildControlFieldsOrder.IndexOf("PROCESS.ERR");
                int currentAttributeIndex = buildControlFieldsOrder.IndexOf(AttributeNameParser.GetShortFieldName(attributeName));

                if (currentAttributeIndex < processErrIndex)
                    shoudlAppendAsValueOfTheLastField = true;
            }
            else if (currentFieldValues[attributeName] != null && lastAddedAttributeName == "PROCESS.ERR")
            {
                shoudlAppendAsValueOfTheLastField = true;
            }

            // the default logic
            if (!shoudlAppendAsValueOfTheLastField)
            {
                currentFieldValues.Add(attributeName, attributeValue);
            }
            else
            {
                // add the field name and value to the last "PROCESS.ERR"
                var parseResult = AttributeNameParser.ParseComplexFieldName(attributeName);
                string ofsAttributeName = parseResult.IsComplex
                    ? string.Format("{0}:{1}:{2}", parseResult.ShortFieldName, parseResult.MultiValueIndex, parseResult.SubValueIndex)
                    : attributeName;

                var previousKey = currentFieldValues.AllKeys[currentFieldValues.AllKeys.Length - 1];
                currentFieldValues[previousKey] = currentFieldValues[previousKey] + string.Format(",{0}={1}", ofsAttributeName, attributeValue);
            }
        }

        private void FireDeploymentEvent(string format, params object[] args)
        {
            FireDeploymentEvent(string.Format(format, args));
        }

        private void FireDeploymentEvent(string message)
        {
            if (OnDeploymentEvent != null)
            {
                OnDeploymentEvent(message);
            }
        }

        private bool FireDeploymentStepExecutedEvent(DeploymentOutput output)
        {
            if (OnDeploymentStepExecuted != null)
            {
                _WasAborted = !OnDeploymentStepExecuted(output);
                return !_WasAborted;
            }
            return true;
        }

        private DeploymentOutput GetDefaultDeploymentOutput(string packageName, string format, params object[] args)
        {
            return GetDefaultDeploymentOutput(packageName, string.Format(format, args));
        }

        private DeploymentOutput GetDefaultDeploymentOutput(string packageName, string message)
        {
            return new DeploymentOutput
            {
                PackName = packageName,
                UnitName = message,
                DeploymentType = DeploymentTypes.BuildControl,
                Method = DeploymentMethod.DlDefinePackage_Deploy,
                IsTargetCatalog = false,
                TargetDirectory = TargetPathForArchive,
                TypicalName = "BUILD.CONTROL",
                UnitType = UnitTypes.SourceFile,
            };
        }

        private DeploymentOutput UploadUncompressedDirectory(string packageName, string directoryPath, string directoryName, bool skipAction)
        {
            DeploymentOutput output = GetDefaultDeploymentOutput(packageName, "Uploading directory: {0}", directoryName);
            if (skipAction)
            {
                output.Outcome = DeploymentOutcome.Skipped;
                return output;
            }

            output.Outcome = DeploymentOutcome.Successful;

            if (Directory.GetFiles(directoryPath).Length == 0 && Directory.GetDirectories(directoryPath).Length == 0)
            {
                output.Outcome = DeploymentOutcome.Failed;
                output.Details = "Deployment package is empty";
                return output;
            }

            try
            {
                //FtpPath.BuildX(_RequestManager.HomeDir, TargetPathForArchive)
                //FtpPath.BuildHomeRelative(_RequestManager.Settings.FtpHome, new FolderMapList(), FtpPath.BuildX(_RequestManager.HomeDir, TargetPathForArchive), out isCatalog)
                //FtpPath.MakeRelative(_RequestManager.HomeDir, FtpPath.BuildX(_RequestManager.HomeDir, TargetPathForArchive))
                bool isCatalog;
                string dirFtpRelativePath = FtpPath.BuildHomeRelative(_RequestManager.FtpWorkingDirectory, new FolderMapList(), TargetPathForArchive, out isCatalog, _RequestManager.FtpLogsSubdir);
                var shConn = (_FileConnector as ValidataSharedDirs.SharedConnector);
                if (shConn != null)
                {
                    shConn.ParentPermissionsDir = dirFtpRelativePath;
                }

                _FileConnector.UseBinary = true;

                string serverDirectoryPath = FtpPath.Concat(dirFtpRelativePath, directoryName);
                _FileConnector.CreateDirectoryRecursively(serverDirectoryPath);

                // Uploading complete directory contents
                {
                    UploadFilesInDirectory(directoryPath, serverDirectoryPath);

                    foreach (string dirName in Directory.GetDirectories(directoryPath))
                    {
                        string serverDirectoryPath1 = FtpPath.Concat(serverDirectoryPath, Path.GetFileName(dirName));

                        _FileConnector.CreateDirectoryRecursively(serverDirectoryPath1);

                        UploadFilesInDirectory(dirName, serverDirectoryPath1);
                    }
                }
            }
            catch (WebException ex)
            {
                output.Outcome = DeploymentOutcome.Failed;

                string message = ex.Message;

                if (ex.Response != null)
                {
                    message += String.Format("{0} {1}", ((FtpWebResponse)ex.Response).StatusDescription, ex.Response.ResponseUri);
                }

                output.Details = message;
            }
            catch (Exception exc)
            {
                output.Outcome = DeploymentOutcome.Failed;
                output.Details = exc.Message;
            }

            return output;
        }

        private DeploymentOutput UploadArchiveFile(string packageName, IFile file, bool skipAction, out string rootToArchivePath)
        {
            bool isCatalog;
            rootToArchivePath = FtpPath.BuildHomeRelative(_RequestManager.FtpWorkingDirectory, new FolderMapList(), TargetPathForArchive, out isCatalog, _RequestManager.FtpLogsSubdir);

            DeploymentOutput output = GetDefaultDeploymentOutput(packageName, "Uploading archive: {0}", file.Name);
            if (skipAction)
            {
                output.Outcome = DeploymentOutcome.Skipped;
                return output;
            }

            output.Outcome = DeploymentOutcome.Successful;

            try
            {
                //FtpPath.BuildX(_RequestManager.HomeDir, TargetPathForArchive)
                //FtpPath.BuildHomeRelative(_RequestManager.Settings.FtpHome, new FolderMapList(), FtpPath.BuildX(_RequestManager.HomeDir, TargetPathForArchive), out isCatalog)
                //FtpPath.MakeRelative(_RequestManager.HomeDir, FtpPath.BuildX(_RequestManager.HomeDir, TargetPathForArchive))

                if (!string.IsNullOrEmpty(rootToArchivePath))
                {
                    var shConn = (_FileConnector as ValidataSharedDirs.SharedConnector);
                    if (shConn != null)
                    {
                        shConn.ParentPermissionsDir = rootToArchivePath;
                    }
                    _FileConnector.CreateDirectoryRecursively(rootToArchivePath);
                }

                string filePath = FtpPath.Concat(rootToArchivePath, file.Name);

                _FileConnector.UseBinary = true;

                Debug.Assert(file.Data != null, "Contents should be loaded!");

                _FileConnector.UploadFile(filePath, file.Data.ToBinaryArray(), false);
            }
            catch (WebException ex)
            {
                output.Outcome = DeploymentOutcome.Failed;

                string message = ex.Message;

                if (ex.Response != null)
                {
                    message += String.Format("{0} {1}", ((FtpWebResponse)ex.Response).StatusDescription, ex.Response.ResponseUri);
                }

                output.Details = message;
            }
            catch (Exception exc)
            {
                output.Outcome = DeploymentOutcome.Failed;
                output.Details = exc.Message;
            }

            return output;
        }

        private DeploymentOutput UncompressArchiveOnT24Environment(string packageName, string archivedFileName, string rootToArchivePath, bool skipAction)
        {
            DeploymentOutput output = GetDefaultDeploymentOutput(packageName, "Uncompressing archive: {0}", archivedFileName);
            if (skipAction)
            {
                output.Outcome = DeploymentOutcome.Skipped;
                return output;
            }

            output.Outcome = DeploymentOutcome.Successful;

            CustomCommandResult originalCustomCommandResult = _RequestManager.CustomCommandDefinition;

            try
            {
                ShellCommand changeDirectoryShellCommand = _RequestManager.Settings.ShellCommands[ShellCommandType.ChangeDirectory];

                string commandMoveToTargetDirectory = string.Format(changeDirectoryShellCommand.Format, TargetPathForArchive);

                string errorText;
                Queue<string> res;

                bool customCheck = false;
                if (changeDirectoryShellCommand.CommandResult != null)
                {
                    _RequestManager.CustomCommandDefinition = changeDirectoryShellCommand.CommandResult;
                    customCheck = true;
                }

                if (_RequestManager.ExecuteShellCommand(commandMoveToTargetDirectory, out res, out errorText, customCheck, null, -1) != ExecuteResult.Success)
                {
                    throw new Exception(errorText);
                }

                // Delete directory
                if (_RequestManager.Settings.ShellCommands.ContainsKey(ShellCommandType.DeleteDirectory))
                {
                    ShellCommand deleteDirectoryShellCommand = _RequestManager.Settings.ShellCommands[ShellCommandType.DeleteDirectory];
                    string shellCommand = string.Format(deleteDirectoryShellCommand.Format,
                                                        archivedFileName.EndsWith(".zip")
                                                            ? archivedFileName.Substring(0, archivedFileName.IndexOf(".zip"))
                                                            : archivedFileName.Substring(0, archivedFileName.IndexOf(".tar")));

                    if (_RequestManager.ExecuteShellCommand(shellCommand, out res, out errorText, false, null, -1) != ExecuteResult.Success)
                    {
                        throw new Exception(errorText);
                    }
                }

                // Uncompress
                ShellCommand uncompressShellCommand = _RequestManager.Settings.ShellCommands[ShellCommandType.Uncompress];

                string ucompressCmdText;

                // full ftp root + (ftp/telnet offset + telnet/bcon release offset) = full ftp root + rootToArchivePath

                string setHomeFull = _RequestManager.Settings.FtpHomeFullPath;
                string fullTargetArchiveDir = string.IsNullOrEmpty(setHomeFull) ?
                    FtpPath.BuildFullPath(rootToArchivePath, ""):
                    FtpPath.BuildFullPath(setHomeFull, rootToArchivePath);
                string fullArchivePath = String.Format("\"{0}\"", FtpPath.BuildFullPath(fullTargetArchiveDir, archivedFileName));

                // In case of zip the comman should contain full paths for the archive file and the target dir.
                if (archivedFileName.ToLower().EndsWith(".zip"))
                {
                    string destinationFolder = String.Format("\"{0}\"", fullTargetArchiveDir);
                    ucompressCmdText = string.Format(uncompressShellCommand.Format, fullArchivePath, destinationFolder);
                }
                else
                {
                    if (archivedFileName.ToLower().EndsWith(".tar.z"))
                    {
                        // .tar.z arhives are archive in archive. when uncompress tar.z archive result is .tar arhcive which don't have any compression
                        // so we need additional step to unpack .tar archive
                        string destinationFolder = String.Format("\"{0}\"", fullTargetArchiveDir);
                        string tmpTarArchive = fullArchivePath;
                        if (tmpTarArchive.EndsWith(".z\"")) tmpTarArchive = tmpTarArchive.Substring(0, tmpTarArchive.Length - 3) + "\"";
                        ucompressCmdText = string.Format(uncompressShellCommand.Format, fullArchivePath, destinationFolder, tmpTarArchive);
                    }
                    // otherwise just the archive path.
                    else
                    {
                        ucompressCmdText = string.Format(uncompressShellCommand.Format, fullArchivePath);
                    }
                }

                customCheck = false;
                _RequestManager.CustomCommandDefinition = originalCustomCommandResult;
                if (uncompressShellCommand.CommandResult != null)
                {
                    _RequestManager.CustomCommandDefinition = uncompressShellCommand.CommandResult;
                    customCheck = true;
                }

                string[] uncompresComands = ucompressCmdText.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
                ExecuteResult executeResult = new ExecuteResult();
                foreach (string uncompressComand in uncompresComands)
                {
                    executeResult = _RequestManager.ExecuteShellCommand(uncompressComand, out res, out errorText, customCheck, null, -1);
                    if (executeResult != ExecuteResult.Success) break;
                }

                if (executeResult != ExecuteResult.Success)
                {
                    if (string.IsNullOrEmpty(errorText) && res != null)
                    {
                        if (errorText == null)
                        {
                            errorText = "";
                        }

                        foreach (string s in res)
                        {
                            errorText += s + "\r\n";
                        }

                        if (errorText.Length > 2)
                        {
                            errorText = errorText.Remove(errorText.Length - 2);
                        }
                    }

                    throw new Exception(errorText);
                }
            }
            catch (Exception ex)
            {
                output.Outcome = DeploymentOutcome.Failed;
                output.Details = ex.Message + "\r\n" + ex.StackTrace;
            }
            finally
            {
                _RequestManager.CustomCommandDefinition = originalCustomCommandResult;
            }

            return output;
        }

        private DeploymentOutput CreateBuildControlRecord(string packageName, string bconId, bool skipAction, IRecord bconRecord, string version = null)
        {
            DeploymentOutput output = GetDefaultDeploymentOutput(packageName, "Input BUILD.CONTROL record: '{0}'", packageName);
            if (skipAction)
            {
                output.Outcome = DeploymentOutcome.Skipped;
                return output;
            }

            output.Outcome = DeploymentOutcome.Successful;

            try
            {
                string fields = _RequestManager.Settings.BuildControlDeploymentFields;
                string[] fieldsList = null;
                if (fields != null)
                    fieldsList = fields.Split(',');

                DeploymentRecord record = GetDeploymentRecord(TargetPathForArchive, packageName, bconId, TargetPathForArchive, fieldsList, bconRecord, _BuildControlTypicalMetadata.CatalogName);

                List<DeploymentOutput> result = _RequestManager.DeployRecords(new List<DeploymentRecord> { record }, false, false, version);

                if (result.Count == 0)
                {
                    throw new Exception("Unable to deploy BUILD.CONTROL record!");
                }

                return result[0];
            }
            catch (Exception ex)
            {
                output.Outcome = DeploymentOutcome.Failed;
                output.Details = ex.Message + "\r\n" + ex.StackTrace;
            }

            return output;
        }

        private const string BUILD_CONTROL = "BUILD.CONTROL";

        private static DeploymentRecord GetDeploymentRecord(string releasePath, string packageName, string bconId, string deploymentPath, string[] deploymentFields, IRecord bconRecord, string catalogName)
        {
            Instance instance = new Instance(BUILD_CONTROL, catalogName);
            instance.AddAttribute("@ID", bconId);
            instance.AddAttribute("ACTION", "RELEASE");
            instance.AddAttribute("RELEASE.PATH", releasePath);
            instance.AddAttribute("OFS.SOURCE.ID", BUILD_CONTROL);
            instance.AddAttribute("CREATE.LIB", "Y");
            //instance.AddAttribute("DESC", packageName);
            string version = string.Empty;

            if (bconRecord != null)
            {
                version = bconRecord.VersionName;               

                if (deploymentFields != null)
                {
                    try
                    {
                        Instance bconInstance = Instance.FromXmlString(bconRecord.Data.ToXml());

                        foreach (string field in deploymentFields)
                        {
                            InstanceAttribute attrib = bconInstance.GetAttributeByShortName(field);
                            if (attrib != null)
                            {
                                string attrVal = attrib.Value;
                                if (attrVal.Contains("{PACKAGE_NAME_NO_PREFIX}"))
                                {
                                    // TODO: This is temporary workaround
                                    attrVal = packageName;
                                }

                                if (attrVal.Contains("{PACKAGE_NAME}"))
                                {
                                    // TODO: This is temporary workaround
                                    attrVal = packageName;
                                }

                                instance.AddAttribute(field, attrVal);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // TODO log it
                    }
                }
            }

            DeploymentRecord result = new DeploymentRecord
            {
                RecordInstance = instance,
                RecordName = bconId, // "BUILD.CONTROL",
                DeploymentPack = packageName,
                Company = "",
                DeploymentMethod = DeploymentMethod.Record_I,
                SkipValueDeletions = true,
                InputSameValues = true,
                UseReplaceOption = false
            };

            if (!string.IsNullOrEmpty(version))
            {
                result.DeploymentVersion = new VersionDefinition { ApplicationName = BUILD_CONTROL, VersionName = version };
            }

            return result;
        }

        private DeploymentOutput VerifyBuildControlRecord(string operationPrefix, string packageName, string buildControlRecordID, IRecord bconRecord, bool skipAction, string version = null)
        {
            DeploymentOutput output = GetDefaultDeploymentOutput(packageName, operationPrefix + " verifying BCON record: '{0}'", buildControlRecordID);
            if (skipAction)
            {
                output.Outcome = DeploymentOutcome.Skipped;
                return output;
            }

            output.Outcome = DeploymentOutcome.Successful;

            try
            {
                DeploymentRecord record = GetDeploymentRecord(TargetPathForArchive, packageName, buildControlRecordID, TargetPathForArchive, null, bconRecord, _BuildControlTypicalMetadata.CatalogName);
                record.RecordInstance = new Instance("BUILD.CONTROL", _BuildControlTypicalMetadata.CatalogName);
                record.RecordInstance.AddAttribute("@ID", buildControlRecordID);

                // todo: for feature release make V/PROCESS accessible for deployment methods of records
                record.DeploymentMethod = DeploymentMethod.Record_V;

                List<DeploymentOutput> result = new List<DeploymentOutput>();
                if (VerifyUsingGlobusClassic)
                {
                    DeploymentOutput res = _RequestManager.ExecuteClassicRecordAction(record, version);

                    if (res.Outcome == DeploymentOutcome.Failed && res.Details.StartsWith("An established connection was aborted"))
                    {
                        res.Outcome = DeploymentOutcome.Successful;
                        res.Details = "Accepted overrides";

                        // recover from closing of telnet connection
                        string error;
                        _RequestManager.AbortDeployment = false;
                        _RequestManager.Release();
                        _RequestManager.LoginToJShell(out error);
                    }

                    result.Add(res);
                }
                else
                {
                    var res = _RequestManager.DeployRecords(new List<DeploymentRecord> { record }, false, false, version);
                    result.AddRange(res);
                }

                if (result.Count == 0)
                    throw new Exception("Unable to deploy BCON record!");

                result[0].Outcome = AnalyzeResult(result[0]);
                return result[0];
            }
            catch (Exception ex)
            {
                output.Outcome = DeploymentOutcome.Failed;
                output.Details = ex.Message + "\r\n" + ex.StackTrace;
            }

            return output;
        }

        private DeploymentOutcome AnalyzeResult(DeploymentOutput deploymentOutput)
        {
            //TODO - make errors list configurable in telnetCommmDef.xml file
            string[] errors = { "No such file or directory" };

            return errors.Any(error => deploymentOutput.Details.Contains(error))
                ? DeploymentOutcome.Failed
                : DeploymentOutcome.Successful;
        }

        private DeploymentOutput RelogInJshell(string packageName, bool skipAction)
        {
            DeploymentOutput output = GetDefaultDeploymentOutput(packageName, "Re-logging in jShell");
            if (skipAction)
            {
                output.Outcome = DeploymentOutcome.Skipped;
                return output;
            }

            output.Outcome = DeploymentOutcome.Successful;

            try
            {
                // release all of the connections. It is from jshell, but others are also on jshell
                _RequestManager.Release();
            }
            catch (Exception ex)
            {
                output.Outcome = DeploymentOutcome.Failed;
                output.Details = ex.Message + "\r\n" + ex.StackTrace;
            }

            return output;
        }

        private void UploadFilesInDirectory(string directoryPath, string serverDirectoryPath)
        {
            foreach (string file in Directory.GetFiles(directoryPath))
            {
                string serverFilePath = FtpPath.Concat(serverDirectoryPath, Path.GetFileName(file));
                _FileConnector.UploadFile(serverFilePath, File.ReadAllBytes(file), false);
            }
        }
    }
}