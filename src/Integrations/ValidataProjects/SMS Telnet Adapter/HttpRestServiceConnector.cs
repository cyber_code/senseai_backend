﻿using System.Collections.Generic;
using System.Diagnostics;
using Common;
using OFSCommonMethods.IO;
using System.Net.Http;
using System.Linq;

namespace T24TelnetAdapter
{
    public class HttpRestServiceConnector : T24TelnetConnector
    {
        private string[] _ResponseBuffer;

        private const string DefaultLoggerName = "Validata.SAS.HttpWebService"; // TODO might rename it to include SSH, too

        private new static readonly Logger Logger = Logger.UniqueInstance[DefaultLoggerName];

        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.HttpRestService; }
        }

        public bool RunHttpWebServiceCommand(string command, string httpWebServiceUrl, int timeout, string username, string password, string ofsJsonPath, HttpMethod httpMethod)
        {
            using (var connector = new OFSCommonMethods.IO.HttpRestService.HttpRestServiceConnector(Logger, username, password, ofsJsonPath, httpMethod))
            {
                string result;
                string error;
                bool webRequestSucceed = connector.RunHttpWebServiceCommand(command, httpWebServiceUrl, timeout,
                    out result, out error, httpMethod.Method.ToString());
                _ResponseBuffer = new string[] { command, webRequestSucceed ? result : error };

                return webRequestSucceed;
            }
        }

        public bool RunHttpWebServiceCommandUpdated(string command, string httpWebServiceUrl, int timeout, string username, string password, string OFSJsonPath, HttpMethod httpmethod, string parameters, int Number_Of_Parameters)
        {
            string[] url_parameters = parameters != null ? parameters.Split(new[] { "," }, System.StringSplitOptions.None).Select(x => x.Trim()).ToArray() : new string[] { };
            using (var connector = new OFSCommonMethods.IO.HttpRestService.HttpRestServiceConnector(Logger, username, password, OFSJsonPath, httpmethod))
            {
                string result;
                string error;
                bool webRequestSucceed = connector.RunHttpWebServiceCommandUpdated(command, httpWebServiceUrl, timeout,
                    out result, out error, url_parameters, Number_Of_Parameters);
                _ResponseBuffer = new string[] { command, webRequestSucceed ? result : error };

                return webRequestSucceed;
            }
        }

        public override bool Login()
        {
            _IsLogged = true;
            return true;
        }

        protected override bool Logout()
        {
            _IsLogged = false;
            return true;
        }

        protected override bool Reconnect()
        {
            return true;
        }

        public Queue<string> ReadResponse()
        {
            Queue<string> res = new Queue<string>();
            if (_ResponseBuffer != null)
            {
                foreach (string line in _ResponseBuffer)
                {
                    if (line != null && !line.Equals("") && !line.Equals("\n"))
                    {
                        res.Enqueue(line);
                    }
                }
            }
            else
            {
                Debug.Fail("Please check why response buffer is null!");
            }

            return res;
        }
    }
}
