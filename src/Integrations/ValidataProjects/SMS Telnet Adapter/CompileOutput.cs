﻿using System;
using System.Collections.Generic;
using System.Linq;
using OFSCommonMethods.IO;

namespace T24TelnetAdapter
{
    [Serializable]
    public class CompileOutput
    {
        public enum CompilationResult
        {
            TelnetError,
            MissingSource,
            MissingDir,
            MissingInclude,
            ParseError,
            Warning1,
            Warning2,
            End,
            None
        }

        private Queue<string> _Text;
        private List<string> _EndStrings;
        private List<string> _ParseErrors;
        private List<string> _LinkErrors;
        private List<string> _Warnings;
        private string _MissingInclude = "";
        private string _MissingFileOrDir = "";
        private readonly string _ShellPrompt;
        private readonly string _SourceName;
        private string _UnexpectedError;
        private CompileOutcome _Outcome;

        public string DeploymentPack;

        public List<string> ParseErrors
        {
            get { return _ParseErrors; }
        }

        public List<string> Warnings
        {
            get { return _Warnings; }
        }

        public string UnexpectedError
        {
            get { return _UnexpectedError; }
        }

        public string MissingInclude
        {
            get { return _MissingInclude; }
        }

        public String MissingFileOrDirectory
        {
            get { return _MissingFileOrDir; }
        }

        public CompileOutcome Outcome
        {
            get { return _Outcome; }
        }

        public string ErrorsMessage
        {
            get
            {
                switch (Outcome)
                {
                    case CompileOutcome.Errors:
                        return String.Join(Environment.NewLine, ParseErrors.ToArray());
                    case CompileOutcome.LinkError:
                        return String.Join(Environment.NewLine, LinkErrors.ToArray());
                    case CompileOutcome.Warnings:
                        return String.Join(Environment.NewLine, Warnings.ToArray());
                    case CompileOutcome.MissingFile:
                        return MissingFileOrDirectory;
                    case CompileOutcome.MissingInclude:
                        return MissingInclude;
                    case CompileOutcome.UnexpectedError:
                        return UnexpectedError;
                    default:
                        break;
                }
                return "No Errors";
            }
        }

        public List<string> LinkErrors
        {
            get { return _LinkErrors; }
        }

        public string SourceName
        {
            get { return _SourceName; }
        }

        internal CompileOutput(string sourceFileName, string shellPrompt)
        {
            _SourceName = sourceFileName;
            _ShellPrompt = shellPrompt;
            Initialize();
        }

        private void Initialize()
        {
            _ParseErrors = new List<string>();
            _LinkErrors = new List<string>();
            _Warnings = new List<string>();
            _EndStrings = new List<string>();

            _EndStrings.Add("Source file");
            _EndStrings.Add("errors were found");
            _EndStrings.Add("error was found");
            _EndStrings.Add(_ShellPrompt);
        }

        private CompilationResult Match(string line)
        {
            if (line.Contains("File not located"))
            {
                return CompilationResult.MissingInclude;
            }

            foreach (string es in _EndStrings)
            {
                if (line.Contains(es))
                {
                    return CompilationResult.End;
                }
            }

            if (line.Contains("[warning\t") || (line.Contains("[jpp error") && line.Contains("line") ))
            {
                return CompilationResult.Warning1;
            }

            if (line.Contains("Warning:"))
            {
                return CompilationResult.Warning2;
            }

            if (((line.StartsWith("\"") && line.Contains("\",")) || line.Contains(SourceName))
                && line.Contains("(offset"))
            {
                return CompilationResult.ParseError;
            }
            if (line.Contains("** Error [ 201 ] **"))
            {
                return CompilationResult.MissingDir;
            }
            if (line.Contains("** Error [ 202 ] **"))
            {
                return CompilationResult.MissingSource;
            }
            return CompilationResult.None;
        }

        internal void Scan()
        {
            string paragraph = "";
            CompilationResult prevOcc = CompilationResult.None;

            foreach (string line in _Text)
            {
                CompilationResult compilationResult = Match(line);
                if (compilationResult == CompilationResult.None)
                {
                    paragraph = paragraph + line + '\n';
                }
                else
                {
                    if (prevOcc != CompilationResult.None)
                    {
                        AddOccurence(prevOcc, paragraph);
                    }
                    if (compilationResult == CompilationResult.End)
                    {
                        break;
                    }
                    paragraph = line + '\n';
                    prevOcc = compilationResult;
                }
            }

            if (_MissingInclude != String.Empty)
            {
                _Outcome = CompileOutcome.MissingInclude;
            }
            else if (_ParseErrors.Count > 0)
            {
                _Outcome = CompileOutcome.Errors;
            }
            else if (_Warnings.Count > 0)
            {
                _Outcome = CompileOutcome.Warnings;
            }
            else if (_MissingFileOrDir != String.Empty)
            {
                _Outcome = CompileOutcome.MissingFile;
            }
            else
            {
                _Outcome = CompileOutcome.Success;
            }
        }

        private void AddOccurence(CompilationResult compilationResult, string paragraph)
        {
            switch (compilationResult)
            {
                case CompilationResult.Warning1:
                case CompilationResult.Warning2:
                    if (paragraph.TrimEnd('\n').Split('\n').Last().StartsWith("BASIC_") &&
                        paragraph.TrimEnd('\n').Split('\n').Last().EndsWith(".c"))
                    {
                        paragraph = paragraph.TrimEnd('\n').Remove(paragraph.TrimEnd('\n').LastIndexOf('\n'));
                    }
                    _Warnings.Add(paragraph);

                    break;
                case CompilationResult.ParseError:
                    _ParseErrors.Add(paragraph);
                    break;

                case CompilationResult.MissingInclude:
                    _MissingInclude = paragraph;
                    break;

                case CompilationResult.MissingSource:
                case CompilationResult.MissingDir:
                    _MissingFileOrDir = paragraph.Replace('\n', ' ');

                    break;
            }
        }

        internal void SetText(Queue<string> text)
        {
            _Text = text;
        }

        internal void SetTelnetError()
        {
            _Outcome = CompileOutcome.TelnetError;
        }

        internal void AddLinkError(string line)
        {
            _LinkErrors.Add(line);
            _Outcome = CompileOutcome.LinkError;
        }

        internal void AddBasicError(string line)
        {
            _UnexpectedError = line;
            _Outcome = CompileOutcome.UnexpectedError;
        }

        internal void AddWarning(string line)
        {
            _Warnings.Add(line);
            _Outcome = CompileOutcome.Warnings;
        }

        /// <summary>
        /// Gets a value indicating whether this compilation is successful, by looking for a predefined "success line"
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is compilation successful; otherwise, <c>false</c>.
        /// </value>
        public bool IsCompilationSuccessful(JShellCommand compileCommand)
        {
            foreach (string outputLine in _Text)
            {
                if (compileCommand.CheckResponseSuccess(outputLine))
                {
                    return true;
                }
            }

            return false;
        }
    }
}