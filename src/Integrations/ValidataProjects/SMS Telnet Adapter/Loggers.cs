﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T24TelnetAdapter
{
    public enum LoggerType
    {
        Default,
        Actions,
        Como,
        Monitoring
    }

    public class Loggers
    {
        private const string DefaultLoggerName = "Validata.SAS.Telnet"; // TODO might rename it to include SSH, too
        private const string ActionsLoggerName = "Validata.SAS.Actions";
        private const string ComoLoggerName = "Validata.SAS.Como";
        private const string MonitoringLoggerName = "Validata.SAS.Monitoring";

        private static Common.Logger _ActionsLogger;
        private static Common.Logger _ComoLogger;
        private static Common.Logger _MonitoringLogger;

        public static Common.Logger ActionsLogger
        {
            get
            {
                if (_ActionsLogger == null)
                    _ActionsLogger = Common.Logger.UniqueInstance[ActionsLoggerName];
                return _ActionsLogger;
            }
        }
        public static Common.Logger ComoLogger
        {
            get
            {
                if (_ComoLogger == null)
                    _ComoLogger = Common.Logger.UniqueInstance[ComoLoggerName];
                return _ComoLogger;
            }
        }
        public static Common.Logger MonitoringLogger
        {
            get
            {
                if (_MonitoringLogger == null)
                    _MonitoringLogger = Common.Logger.UniqueInstance[MonitoringLoggerName];
                return _MonitoringLogger;
            }
        }

        public static Common.Logger GetLogger(LoggerType TypeOfLogger)
        {
            switch (TypeOfLogger)
            {
                case LoggerType.Actions:
                    return Loggers.ActionsLogger;
                case LoggerType.Como:
                    return Loggers.ComoLogger;
                case LoggerType.Monitoring:
                    return Loggers.MonitoringLogger;
                case LoggerType.Default:
                default:
                    return Common.Logger.UniqueInstance[DefaultLoggerName];

            }
        }
    }
}
