using OFSCommonMethods.IO;

namespace T24TelnetAdapter
{
    public class JShellConnector:T24TelnetConnector
    {
        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.JShell; }
        }
    }
}