﻿using System;
using System.Collections.Generic;
using Common;
using NUnit.Framework;
using OFSCommonMethods.IO;
using ValidataFtp;

namespace T24TelnetAdapter.UnitTests
{
    [TestFixture]
    public class BulkCompiler_Test
    {
        private const string BackupFolderName = "SAS090909";

        private const string AIXHome = @"/home/jbaseadm/MIGVB01/bnk.run/";

        private const string WINHome = @"F:\temenos\PSGMB-R08-000\bnk.run\";

        [Test]
        public void Test_GetFilePathFromBackupDirectory()
        {
            // Standart case
            TestGetFilePathFromBackupDirectory_AnyOS(
                "./lib",
                "./bin",
                "obj",
                "./globuslib",
                "./globusbin",
                "./lib/mylib.o.1",
                "./" + BackupFolderName + "/lib/mylib.o.1"
                );

            TestGetFilePathFromBackupDirectory_AnyOS(
                "./lib",
                "./bin",
                "obj",
                "./globuslib",
                "./globusbin",
                "./bin/mybin.f",
                "./" + BackupFolderName + "/bin/mybin.f"
                );

            TestGetFilePathFromBackupDirectory_AnyOS(
                "./lib",
                "./bin",
                "obj",
                "./globuslib",
                "./globusbin",
                "./lib/obj/myo.o",
                "./" + BackupFolderName + "/lib/obj/myo.o"
                );

            TestGetFilePathFromBackupDirectory_AnyOS(
                "./lib",
                "./bin",
                "obj",
                "./globuslib",
                "./globusbin",
                "./globuslib/syslib.y",
                "./" + BackupFolderName + "/globuslib/syslib.y"
                );

            TestGetFilePathFromBackupDirectory_AnyOS(
                "./lib",
                "./bin",
                "obj",
                "./globuslib",
                "./globusbin",
                "./globuslib/obj/myo.o",
                "./" + BackupFolderName + "/globuslib/obj/myo.o"
                );

            TestGetFilePathFromBackupDirectory_AnyOS(
                "./lib",
                "./bin",
                "obj",
                "./globuslib",
                "./globusbin",
                "./XXX/ZZZ/myo.o",
                "./XXX/ZZZ/myo.o"
                );

            TestGetFilePathFromBackupDirectory_AnyOS(
                "./lib",
                "./bin",
                "obj",
                "./globuslib",
                "./globusbin",
                "../XXX/ZZZ/myo.o",
                "../XXX/ZZZ/myo.o"
                );

            // Test Strange paths
            TestGetFilePathFromBackupDirectory_AnyOS(
                "./lib1",
                "./bin1",
                "obj1",
                "./globuslib1",
                "./globusbin1",
                "./bin1/mylib1.o.1",
                "./" + BackupFolderName + "/bin1/mylib1.o.1"
                );

            TestGetFilePathFromBackupDirectory_AnyOS(
                "../lib",
                "../bin",
                "obj",
                "../globuslib",
                "../globusbin",
                "../lib/mylib.o.1",
                "./" + BackupFolderName + "/lib/mylib.o.1"
                );

            TestGetFilePathFromBackupDirectory_AnyOS(
                "../lib",
                "../bin",
                "obj",
                "../globuslib",
                "../globusbin",
                "../globuslib/obj/x",
                "./" + BackupFolderName + "/globuslib/obj/x"
                );

            TestGetFilePathFromBackupDirectory_AnyOS(
                "../someotherfolder/lib",
                "../someotherfolder/bin",
                "obj",
                "../someotherfolder/globuslib",
                "../someotherfolder/globusbin",
                "../someotherfolder/bin/x",
                "./" + BackupFolderName + "/someotherfolder/bin/x"
                );
        }

        [Test]
        [Explicit]
        public void Test_FillFileAnalysis()
        {
            TestFillFileAnalysis_AnyOS_AnyDeplFolder(
                "CONV.ACCOUNT.AC.OFF",
                new List<string>
                    {
                        "/home/jbaseadm/MIGVB01/bnk.run/lib/lib.el",
                        "/home/jbaseadm/MIGVB01/bnk.run/lib/lib50.so.524",
                        "/home/jbaseadm/MIGVB01/bnk.run/CONV.ACCOUNT.AC.OFF",
                        "/home/jbaseadm/MIGVB01/bnk.run/lib/obj/CONV_2EACCOUNT_2EAC_2EOFF.o"
                    },
                new List<string>
                    {
                        "/home/jbaseadm/MIGVB01/bnk.run/globuslib/lib.el",
                        "/home/jbaseadm/MIGVB01/bnk.run/globuslib/lib86.so.1"
                    },
                T24FileLocation.ExistingInDEV
                );

            TestFillFileAnalysis_AnyOS_AnyDeplFolder(
                "CONV.ACCOUNT.200512",
                new List<string>
                    {
                        "/home/jbaseadm/MIGVB01/bnk.run/globuslib/lib.el",
                        "/home/jbaseadm/MIGVB01/bnk.run/globuslib/lib86.so.1",
                        "/home/jbaseadm/MIGVB01/bnk.run/CONV.ACCOUNT.200512"
                    },
                new List<string>(),
                T24FileLocation.ExistingInGlobus
                );

            TestFillFileAnalysis_AnyOS_AnyDeplFolder(
                "S.GORDAN.NF",
                new List<string>
                    {
                        "/home/jbaseadm/MIGVB01/bnk.run/lib/lib.el",
                        "/home/jbaseadm/MIGVB01/bnk.run/lib/lib50.so.524",
                        "/home/jbaseadm/MIGVB01/bnk.run/lib/obj/S_2EGORDAN_2ENF.o"
                    },
                new List<string>(),
                T24FileLocation.ExistingInDEV
                );

            TestFillFileAnalysis_AnyOS_AnyDeplFolder(
                "S.GORDAN.NF",
                new List<string>(),
                new List<string>(),
                T24FileLocation.NotExisting
                );
        }

        private static void TestFillFileAnalysis_AnyOS_AnyDeplFolder(string routineName,
                                                              List<string> affected,
                                                              List<string> duplicated,
                                                              T24FileLocation existingIn)
        {
            RequestManager aix1 = CreateRequestManager_NoConnection(
                OperatingSystem.AIX,
                AIXHome,
                BackupFolderName,
                "./lib",
                "./bin",
                "obj",
                "./globuslib",
                "./globusbin",
                "./GLOBUS.BP"
                );

            RequestManager aix2 = CreateRequestManager_NoConnection(
                OperatingSystem.AIX,
                AIXHome,
                BackupFolderName,
                "../XXX/lib",
                "../XXX/bin",
                "obj",
                "../XXX/globuslib",
                "../XXX/globusbin",
                "./GLOBUS.BP"
                );

            RequestManager win1 = CreateRequestManager_NoConnection(
                OperatingSystem.Win,
                WINHome,
                BackupFolderName,
                ".\\lib",
                ".\\bin",
                "obj",
                ".\\globuslib",
                ".\\globusbin",
                "./GLOBUS.BP"
                );

            RequestManager win2 = CreateRequestManager_NoConnection(
                OperatingSystem.Win,
                WINHome,
                BackupFolderName,
                "..\\XXX\\lib",
                "..\\XXX\\bin",
                "obj",
                "..\\XXX\\globuslib",
                "..\\XXX\\globusbin",
                "./GLOBUS.BP"
                );

            string[] DeploymentFolders = new string[] {"GLOBUS.BP/", "GLOBUS.BP", "", "NIKI/123", "NIKI/123/"};

            List<string> winAffected = GetForWin(affected);
            List<string> winDuplicated = GetForWin(duplicated);

            foreach (string deplFolder in DeploymentFolders)
            {
                TestFillFileAnalysis_AnyOS(aix1, deplFolder, routineName, affected, duplicated, existingIn);
                TestFillFileAnalysis_AnyOS(aix2, deplFolder, routineName, ChangeBnkRun2XXX(affected), ChangeBnkRun2XXX(duplicated), existingIn);
                TestFillFileAnalysis_AnyOS(win1, deplFolder.Replace("/", "\\"), routineName, winAffected, winDuplicated, existingIn);
                TestFillFileAnalysis_AnyOS(win2,
                                           deplFolder.Replace("/", "\\"),
                                           routineName,
                                           ChangeBnkRun2XXX(winAffected),
                                           ChangeBnkRun2XXX(winDuplicated),
                                           existingIn);
            }
        }

        private static List<string> ChangeBnkRun2XXX(IEnumerable<string> files)
        {
            List<string> result = new List<string>();

            foreach (string s in files)
            {
                result.Add(s.Replace("bnk.run", "XXX"));
            }

            return result;
        }

        private static List<string> GetForWin(IEnumerable<string> files)
        {
            List<string> result = new List<string>();

            foreach (string s in files)
            {
                string t = s.Replace(AIXHome, WINHome).Replace("/", "\\");
                result.Add(t);
            }

            return result;
        }

        private static void TestFillFileAnalysis_AnyOS(RequestManager requestManager,
                                                string deployFolder,
                                                string routineName,
                                                List<string> affected,
                                                List<string> duplicated,
                                                T24FileLocation existingIn)
        {
            BulkCompiler bulkCompiler = new BulkCompiler(requestManager, new FolderMapList());

            CompilationAnalysisResult compilationAnalysisResult = new CompilationAnalysisResult
                                                                      {
                                                                          AffectedFiles = affected,
                                                                          DuplicateFiles = duplicated,
                                                                          SourceDir = deployFolder
                                                                      };

            bulkCompiler.PerformAdditionalImpactAnalysis(compilationAnalysisResult, deployFolder, false, routineName);

            bool tryingInBP = deployFolder.Contains("GLOBUS.BP");
            switch (existingIn)
            {
                case T24FileLocation.ExistingInDEV:
                    Assert.IsTrue(compilationAnalysisResult.CurrentState == T24FileLocation.ExistingInDEV);
                    if (tryingInBP)
                    {
                        Assert.IsTrue(compilationAnalysisResult.HasFailed);
                    }
                    else
                    {
                        Assert.IsTrue(!compilationAnalysisResult.HasFailed);
                    }
                    break;
                case T24FileLocation.ExistingInGlobus:
                    Assert.IsTrue(compilationAnalysisResult.CurrentState == T24FileLocation.ExistingInGlobus);
                    if (tryingInBP)
                    {
                        Assert.IsTrue(!compilationAnalysisResult.HasFailed);
                    }
                    else
                    {
                        Assert.IsTrue(compilationAnalysisResult.HasFailed);
                    }

                    break;
                case T24FileLocation.NotExisting:
                    Assert.IsTrue(!compilationAnalysisResult.HasFailed);
                    Assert.IsTrue(compilationAnalysisResult.CurrentState == T24FileLocation.NotExisting);
                    break;
            }
        }

        #region Private Helper

        private static void TestGetFilePathFromBackupDirectory_AnyOS(
            string jLibDir,
            string jBinDir,
            string objectDir,
            string jSysLibDir,
            string jSysBinDir,
            string homeRelativeFilePath,
            string expectedResultPathHomeRelative
            )
        {
            // For AIX
            {
                string originalFilePath = FtpPath.BuildX(AIXHome, homeRelativeFilePath, "");
                originalFilePath = originalFilePath.Replace("\\", "/");
                string expectedResultPath = FtpPath.BuildX(AIXHome, expectedResultPathHomeRelative, "");
                expectedResultPath = expectedResultPath.Replace("\\", "/");
                TestGetFilePathFromBackupDirectory(AIXHome,
                                                   BackupFolderName,
                                                   jLibDir,
                                                   jBinDir,
                                                   objectDir,
                                                   jSysLibDir,
                                                   jSysBinDir,
                                                   originalFilePath,
                                                   expectedResultPath
                    );
            }

            // For WIN
            {
                string originalFilePath = FtpPath.BuildX(WINHome.Substring(2), homeRelativeFilePath.Replace("/", "\\"), "");
                originalFilePath = originalFilePath.Replace("/", "\\");
                originalFilePath = "F:" + originalFilePath;
                string expectedResultPath = FtpPath.BuildX(WINHome.Substring(2), expectedResultPathHomeRelative.Replace("/", "\\"), "");
                expectedResultPath = expectedResultPath.Replace("/", "\\");
                expectedResultPath = "F:" + expectedResultPath;
                TestGetFilePathFromBackupDirectory(WINHome,
                                                   BackupFolderName,
                                                   jLibDir.Replace("/", "\\"),
                                                   jBinDir.Replace("/", "\\"),
                                                   objectDir.Replace("/", "\\"),
                                                   jSysLibDir.Replace("/", "\\"),
                                                   jSysBinDir.Replace("/", "\\"),
                                                   originalFilePath,
                                                   expectedResultPath
                    );
            }
        }

        private static void TestGetFilePathFromBackupDirectory(
            string homeDir,
            string backUpFolder,
            string jLibDir,
            string jBinDir,
            string objectDir,
            string jSysLibDir,
            string jSysBinDir,
            string originalFilePath,
            string expectedResultPath
            )
        {
            OperatingSystem os = originalFilePath.StartsWith("/")
                                     ?
                                         OperatingSystem.AIX
                                     :
                                         OperatingSystem.Win;

            Console.WriteLine("--------------------------------------------");
            Console.WriteLine("OS:            " + os);
            Console.WriteLine("HOME DIR:      " + homeDir);
            Console.WriteLine("BACKUP FOLDER: " + backUpFolder);
            Console.WriteLine("JBCDEV_LIB:    " + jLibDir);
            Console.WriteLine("JBCDEV_BIN:    " + jBinDir);
            Console.WriteLine("OBJECT DIR:    " + objectDir);
            Console.WriteLine("SYS LIB DIR:   " + jSysLibDir);
            Console.WriteLine("SYS BIN DIR:   " + jSysBinDir);
            Console.WriteLine("");
            Console.WriteLine("FILE PATH:     " + originalFilePath);

            RequestManager requestManager = CreateRequestManager_NoConnection(
                os,
                homeDir,
                backUpFolder,
                jLibDir,
                jBinDir,
                objectDir,
                jSysLibDir,
                jSysBinDir
                );

            BulkCompiler bulkCompiler = new BulkCompiler(requestManager, new FolderMapList());

            string builtPath = bulkCompiler.GetFilePathFromBackupDirectory(originalFilePath);

            Console.WriteLine("RESULT PATH:   " + builtPath);

            Assert.AreEqual(expectedResultPath, builtPath);

            Console.WriteLine("");
        }

        /// <summary>
        /// This request manager is used for testing of methods 
        /// which require no connection to environemnt
        /// </summary>
        /// <returns>Request manager instance</returns>
        private static RequestManager CreateRequestManager_NoConnection(
            OperatingSystem os,
            string homeDir,
            string backUpFolder,
            string jLibDir,
            string jBinDir,
            string objectDir,
            string jSysLibDir,
            string jSysBinDir
            )
        {
            return CreateRequestManager_NoConnection(
                os,
                homeDir,
                backUpFolder,
                jLibDir,
                jBinDir,
                objectDir,
                jSysLibDir,
                jSysBinDir,
                null
                );
        }

        /// <summary>
        /// This request manager is used for testing of methods 
        /// which require no connection to environemnt
        /// </summary>
        /// <returns>Request manager instance</returns>
        private static RequestManager CreateRequestManager_NoConnection(
            OperatingSystem os,
            string homeDir,
            string backUpFolder,
            string jLibDir,
            string jBinDir,
            string objectDir,
            string jSysLibDir,
            string jSysBinDir,
            string jSysBPDir
            )
        {
            Settings settings = new Settings();
            settings.Objectdir = objectDir;
            settings.JBCDEV_LIB = jLibDir;
            settings.JBCDEV_BIN = jBinDir;
            settings.PathGlobusLib = jSysLibDir;
            settings.PathGlobusBin = jSysBinDir;
            settings.PathGlobusBP = jSysBPDir;

            RequestManager requestManager = new RequestManager(os, settings.PhantomSet, null, null, null, null, false);
            requestManager.BackupFolder = backUpFolder;
            requestManager.HomeDir = homeDir;

            return requestManager;
        }

        #endregion
    }
}