﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;
using OFSCommonMethods.IO;
using OFSCommonMethods.IO.Telnet;
using SCApiInterface;

namespace T24TelnetAdapter.UnitTests
{
    [TestFixture]
    public class RequestManager_Test
    {
        [Test]
        public void TestSplitAndEscapeTextInPortionsForEchoingInUnixShell()
        {
            VerifySplitAndEscapeTextInPortionsForUnix(2, "", new List<string>());
            VerifySplitAndEscapeTextInPortionsForUnix(2, "1", new List<string>() {"1"});
            VerifySplitAndEscapeTextInPortionsForUnix(2, "12", new List<string>() {"12"});
            VerifySplitAndEscapeTextInPortionsForUnix(2, "123", new List<string>() {"12", "3"});
            VerifySplitAndEscapeTextInPortionsForUnix(2, "1234", new List<string>() {"12", "34"});
            VerifySplitAndEscapeTextInPortionsForUnix(2, "12345", new List<string>() {"12", "34", "5"});

            VerifySplitAndEscapeTextInPortionsForUnix(2, @"\n", new List<string>() { @"\\\n" });
            VerifySplitAndEscapeTextInPortionsForUnix(2, @"1\2", new List<string>() { @"1\\\2" });
            VerifySplitAndEscapeTextInPortionsForUnix(2, @"1\23", new List<string>() { @"1\\\2", "3" });
            VerifySplitAndEscapeTextInPortionsForUnix(2, @"\\23", new List<string>() { @"\\\\\\2", "3" });
            VerifySplitAndEscapeTextInPortionsForUnix(2, @"1\\23", new List<string>() { @"1\\\\\\2", "3" });

            VerifySplitAndEscapeTextInPortionsForUnix(2, "\"1\"", new List<string>() { "\\\"", "1\\\"" });
        }


        [Test]
        public void TestSplitAndEscapeTextInPortionsUsingCommaForEchoingInMsDos()
        {
            const string sampleOfs = @"ENQUIRY,/I/PROCESS,INPUTT/123456,OBJECT.MAP.ENQ,BREAK.FIELDS:1:1=0,COLUMN:2:1=1,";
            List<string> values = new List<string>(RequestManager.SplitByCommaAndEscapeOfsCommandForEchoingInMsDos(sampleOfs, 200));

            Assert.AreEqual(6, values.Count);
            Assert.AreEqual(@"ENQUIRY,", values[0]);
            Assert.AreEqual(@"/I/PROCESS,", values[1]);
            Assert.AreEqual(@"INPUTT/123456,", values[2]);
            Assert.AreEqual(@"OBJECT.MAP.ENQ,", values[3]);
            Assert.AreEqual(@"BREAK.FIELDS:1:1=0,", values[4]);
            Assert.AreEqual(@"COLUMN:2:1=1,", values[5]);
        }

        [Test]
        public void TestSplitAndEscapeTextInPortionsForEchoingInMsDos()
        {
            VerifySplitAndEscapeTextInPortionsForMsDos(2, "", new List<string>());
            VerifySplitAndEscapeTextInPortionsForMsDos(2, "1", new List<string>() { "1" });
            VerifySplitAndEscapeTextInPortionsForMsDos(2, "12", new List<string>() { "12" });
            VerifySplitAndEscapeTextInPortionsForMsDos(2, "123", new List<string>() { "12", "3" });
            VerifySplitAndEscapeTextInPortionsForMsDos(2, "1234", new List<string>() { "12", "34" });
            VerifySplitAndEscapeTextInPortionsForMsDos(2, "12345", new List<string>() { "12", "34", "5" });

            VerifySplitAndEscapeTextInPortionsForMsDos(2, ")n", new List<string>() { "^)" ,"n" });
            VerifySplitAndEscapeTextInPortionsForMsDos(2, "1(2", new List<string>() { "1^(", "2" });
            VerifySplitAndEscapeTextInPortionsForMsDos(2, "1|23", new List<string>() { "1^|", "23" });
            VerifySplitAndEscapeTextInPortionsForMsDos(2, "<23", new List<string>() { "^<", "23" });
            VerifySplitAndEscapeTextInPortionsForMsDos(2, "1>23", new List<string>() {"1^>", "23" });

            VerifySplitAndEscapeTextInPortionsForMsDos(2, "^1^", new List<string>() { "^^1", "^^" });
        }

        private static void VerifySplitAndEscapeTextInPortionsForUnix(int portionLength, string text, List<string> expected)
        {
            VerifySplitAndEscapeTextInPortionsForEchoing(false, portionLength, text, expected);
        }

        private static void VerifySplitAndEscapeTextInPortionsForMsDos(int portionLength, string text, List<string> expected)
        {
            VerifySplitAndEscapeTextInPortionsForEchoing(true, portionLength, text, expected);
        }

        private static void VerifySplitAndEscapeTextInPortionsForEchoing(bool isMsDos, int portionLength, string text, List<string> expected)
        {
            List<string> actual = new List<string>(RequestManager.SplitAndEscapeTextInPortionsForEchoing(text, portionLength, isMsDos));
            Assert.AreEqual(expected.Count, actual.Count, text);
            for (int i = 0; i < actual.Count; i++)
            {
                string expectedRes = isMsDos ? expected[i] : "\"" + expected[i] + "\"";
                Assert.AreEqual(expectedRes, actual[i], text);
            }
        }

        [Test]
        [Explicit]
        public void Test_CT_VOC()
        {
            RequestManager requestManager = null;
            try
            {
                requestManager = CreateRequestManager(OperatingSystem.AIX, "AIX-migvb02");

                string errMsg;
                string result;

                // Creating VOC Entry:
                // JED VOC [Entry Name]
                //  Fill data
                //  Press 'Esc'
                //  Type  'fi'
                //  Press 'Enter'

                #region TEST Gxx (Simulation)

                {
                    // TESTCAT catalog should be existing
                    // TESTCAT.O VOC Entry should be existing
                    //  TESTCAT.O
                    //  001 F
                    //  002 ../bnk.data/NIKI.O
                    //  003 ../bnk.data/NIKI.O[D
                    // BUILD_2ECONTROL.o should be placed in ../bnk.data/NIKI.O
                    result = ConstructObjectFileName(
                        requestManager,
                        "BUILD.CONTROL",
                        false,
                        "TESTCAT",
                        "UniVerse",
                        new Dictionary<string, string>(),
                        out errMsg
                        );

                    Assert.AreEqual(result, "/home/jbaseadm/MIGVB02/bnk.data/NIKI.O/BUILD_2ECONTROL.o");
                    Assert.IsNull(errMsg);
                }

                {
                    result = ConstructObjectFileName(
                        requestManager,
                        "FAKE",
                        false,
                        "ASDASDASDASDASDASDA",
                        "UniVerse",
                        new Dictionary<string, string>(),
                        out errMsg
                        );

                    Assert.IsNull(result);
                    Assert.IsNotNull(errMsg);
                }

                {
                    result = ConstructObjectFileName(
                        requestManager,
                        "FAKE",
                        false,
                        "",
                        "UniVerse",
                        new Dictionary<string, string>(),
                        out errMsg
                        );

                    Assert.IsNull(result);
                    Assert.IsNotNull(errMsg);
                }

                {
                    result = ConstructObjectFileName(
                        requestManager,
                        "FAKE",
                        false,
                        null,
                        "UniVerse",
                        new Dictionary<string, string>(),
                        out errMsg
                        );

                    Assert.IsNull(result);
                    Assert.IsNotNull(errMsg);
                }

                #endregion

                #region Test Rxx (Real)

                {
                    result = ConstructObjectFileName(
                        requestManager,
                        "BUILD.CONTROL",
                        false,
                        "NoMatterOfThisFolder/AndSubFolders",
                        "",
                        new Dictionary<string, string>(),
                        out errMsg
                        );

                    Assert.AreEqual(result, "/home/jbaseadm/MIGVB02/bnk.run/lib/obj/BUILD_2ECONTROL.o");
                    Assert.IsNull(errMsg);
                }

                {
                    result = ConstructObjectFileName(
                        requestManager,
                        "BUILD.CONTROL.WHICH.IS.NOT.EXISTING",
                        false,
                        "NoMatterOfThisFolder/AndSubFolders",
                        "",
                        new Dictionary<string, string>(),
                        out errMsg
                        );

                    Assert.IsNull(result);
                    Assert.IsNotNull(errMsg);
                }

                #endregion

                #region Test Object Dir Overrides: Gxx & Rxx

                {
                    // Catalog and file inside should be existing
                    // /home/jbaseadm/MIGVB02/bnk.run/TESTCAT/XXX/BUILD_2ECONTROL.o

                    Dictionary<string, string> overrideMap = new Dictionary<string, string>();
                    overrideMap.Add("SomeFolder", "./TESTCAT/XXX/");
                    result = ConstructObjectFileName(
                        requestManager,
                        "BUILD.CONTROL",
                        true,
                        "SomeFolder",
                        "uNiVersE",
                        overrideMap,
                        out errMsg
                        );

                    Assert.AreEqual(result, "/home/jbaseadm/MIGVB02/bnk.run/TESTCAT/XXX/BUILD_2ECONTROL.o");
                    Assert.IsNull(errMsg);
                }

                {
                    // Folder and file inside should be existing
                    // /home/jbaseadm/MIGVB02/DL.BP.O/BUILD_2ECONTROL.o

                    Dictionary<string, string> overrideMap = new Dictionary<string, string>();
                    overrideMap.Add("SomeFolder1", "../DL.BP.O");
                    result = ConstructObjectFileName(
                        requestManager,
                        "BUILD.CONTROL",
                        true,
                        "SomeFolder1",
                        "",
                        overrideMap,
                        out errMsg
                        );

                    Assert.AreEqual(result, "/home/jbaseadm/MIGVB02/DL.BP.O/BUILD_2ECONTROL.o");
                    Assert.IsNull(errMsg);
                }

                #endregion
            }
            finally
            {
                ReleaseRequestManager(requestManager);
            }
        }

        [Test]
        [Explicit]
        public void TEST_DELME()
        {
            RequestManager requestManager = null;
            try
            {
                //
                // NOTE:
                // All of tests bellow are available on migvb01 (currently)!!!
                //
                requestManager = CreateRequestManager(OperatingSystem.AIX, "AIX-migvb02");
                requestManager.CheckMaxLengthOfCommandLineArgs();
            }
            finally
            {
                ReleaseRequestManager(requestManager);
            }
        }

        [Test]
        [Explicit]
        public void Test_FindAffectedFiles()
        {
            RequestManager requestManager = null;
            try
            {
                //
                // NOTE:
                // All of tests bellow are available on migvb01 (currently)!!!
                //
                string errMsg;
                requestManager = CreateRequestManager(OperatingSystem.AIX, "AIX-migvb02");

                // Duplicates (in lib & in globuslib):
                //      jshow -c CONV.ACCOUNT.AC.OFF
                {
                    List<string> affectedFiles;
                    List<string> duplicatedFiles;
                    bool res = FindAffectedFiles(
                        requestManager,
                        "CONV.ACCOUNT.AC.OFF",
                        ".",
                        out affectedFiles,
                        out duplicatedFiles,
                        out errMsg
                        );

                    Assert.IsTrue(res);
                    Assert.IsTrue(affectedFiles.Count == 4 /*lib.el; libX; .o; Source*/);
                    Assert.IsTrue(duplicatedFiles.Count == 2 /*lib.el; libX*/);
                }

                // Only in globuslib:
                //      jshow -c CONV.ACCOUNT.200512
                {
                    List<string> affectedFiles;
                    List<string> duplicatedFiles;
                    bool res = FindAffectedFiles(
                        requestManager,
                        "CONV.ACCOUNT.200512",
                        ".",
                        out affectedFiles,
                        out duplicatedFiles,
                        out errMsg
                        );

                    Assert.IsTrue(res);
                    Assert.IsTrue(affectedFiles.Count == 3 /*lib.el; libX; .o*/);
                    Assert.IsTrue(duplicatedFiles.Count == 0);
                }

                // Only in lib: 
                //      jshow -c S.GORDAN.NF
                {
                    List<string> affectedFiles;
                    List<string> duplicatedFiles;
                    bool res = FindAffectedFiles(
                        requestManager,
                        "S.GORDAN.NF",
                        ".",
                        out affectedFiles,
                        out duplicatedFiles,
                        out errMsg
                        );

                    Assert.IsTrue(res);
                    Assert.IsTrue(affectedFiles.Count == 3 /*lib.el; libX; .o*/);
                    Assert.IsTrue(duplicatedFiles.Count == 0);
                }

                // Nothing: 
                //      jshow -c TEST.PROC
                {
                    List<string> affectedFiles;
                    List<string> duplicatedFiles;
                    bool res = FindAffectedFiles(
                        requestManager,
                        "TEST.PROC",
                        ".",
                        out affectedFiles,
                        out duplicatedFiles,
                        out errMsg
                        );

                    Assert.IsTrue(res);
                    Assert.IsTrue(affectedFiles.Count == 0);
                    Assert.IsTrue(duplicatedFiles.Count == 0);
                }
            }
            finally
            {
                ReleaseRequestManager(requestManager);
            }
        }

#if DEBUG
        [Test]
        [Explicit]
        public void Test_BackupDirectoryOnServer()
        {
            RequestManager requestManagerAIX = null;
            RequestManager requestManagerWIN = null;

            try
            {
                string errText;

                requestManagerAIX = CreateRequestManager(OperatingSystem.AIX, "AIX-migvb02");


                bool res = requestManagerAIX.CreateBackupDirectory("/home/jbaseadm/MIGVB01/bnk.run/XXX_BACK_UP", out errText);
                Assert.IsTrue(res);

                errText = requestManagerAIX.BackupDirectoryOnServer_Test("/home/jbaseadm/MIGVB01/bnk.run/XXX/", false);
                Assert.IsTrue(errText == null);

                errText = requestManagerAIX.BackupDirectoryOnServer_Test("/home/jbaseadm/MIGVB01/bnk.run/XXX/", true);
                Assert.IsTrue(errText == null);


                requestManagerWIN = CreateRequestManager(OperatingSystem.Win, "WINR8-58");

                res = requestManagerWIN.CreateBackupDirectory(@"F:\temenos\PSGMB-R08-000\bnk.run\XXX_BACK_UP", out errText);
                Assert.IsTrue(res);

                errText = requestManagerWIN.BackupDirectoryOnServer_Test(@"F:\temenos\PSGMB-R08-000\bnk.run\XXX\", false);
                Assert.IsTrue(errText == null);

                errText = requestManagerWIN.BackupDirectoryOnServer_Test(@"F:\temenos\PSGMB-R08-000\bnk.run\XXX\", true);
                Assert.IsTrue(errText == null);

            }
            finally
            {
                ReleaseRequestManager(requestManagerAIX);
                ReleaseRequestManager(requestManagerWIN);
            }
        }

#endif

        #region Private Static Helper Methods

        private static RequestManager CreateRequestManager(OperatingSystem os, string telnetSetName)
        {
            Settings settings = new Settings();
            settings.PhantomSet = telnetSetName;
            ExecutionStepContainer.LoadConnectionSettings(settings);

            var rm = new RequestManager(
                os,
                settings.PhantomSet,
                ",",
                settings.GlobusServer,
                settings.GlobusUserName,
                settings.GlobusPassword,
                true
                );

            string errorMessage;

            if (!rm.LoginToJShell(out errorMessage))
            {
                throw new Exception(errorMessage);
            }

            // Now it is called from 'Login'
            //if (!rm.LocateLibBinDir(out errorMessage))
            //{
            //    throw new Exception(errorMessage);
            //}

            return rm;
        }

        private static void ReleaseRequestManager(RequestManager requestManager)
        {
            requestManager.Release();
        }

        private static string ConstructObjectFileName(
            RequestManager requestManager,
            string routineName,
            bool isCatalog,
            string serverDeploymentFolder,
            string jSh,
            Dictionary<string, string> objectDirOverrides,
            out string errMsg)
        {
            IFile file = new File();

            string origJSh = requestManager.Settings.JSh;
            requestManager.Settings.JSh = jSh;

            Dictionary<string, string> origObjectDirOverrides = requestManager.Settings.ObjectDirOverrides;
            requestManager.Settings.ObjectDirOverrides = objectDirOverrides;

            try
            {
                CompiledFile compiledFile = new CompiledFile();
                file.Name = routineName;
                compiledFile.File = new DeployableFile(file, "", "");
                compiledFile.File.IsCatalog = isCatalog;
                compiledFile.File.ServerDeploymentFolder = serverDeploymentFolder;

                string r = requestManager.ConstructObjectFileName(compiledFile, new List<string>(), false, out errMsg);
                return string.IsNullOrEmpty(r) ? null : r;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return null;
            }
            finally
            {
                requestManager.Settings.JSh = origJSh;
                requestManager.Settings.ObjectDirOverrides = origObjectDirOverrides;
            }
        }

        private static bool FindAffectedFiles(
            RequestManager requestManager,
            string routineName,
            string serverDeploymentFolder,
            out List<string> affectedFiles,
            out List<string> duplicatedFiles,
            out string errMsg)
        {
            affectedFiles = null;
            duplicatedFiles = null;

            IFile file = new File();

            try
            {
                CompiledFile compiledFile = new CompiledFile();
                file.Name = routineName;
                compiledFile.File = new DeployableFile(file, "", "");
                compiledFile.File.ServerDeploymentFolder = serverDeploymentFolder;

                CompilationAnalysisResult res = requestManager.AnalyzeFile(compiledFile, false);
                affectedFiles = res.AffectedFiles;
                duplicatedFiles = res.DuplicateFiles;
                errMsg = res.ErrorMessage;

                Display(routineName, affectedFiles, duplicatedFiles);

                return !res.HasFailed;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }

        private static void Display(string routineName, IEnumerable<string> affectedFiles, IEnumerable<string> duplicatedFiles)
        {
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("ROUTINE NAME: " + routineName);
            Console.WriteLine("");

            Console.WriteLine("AFFECTED:");
            foreach (string name in affectedFiles)
            {
                Console.WriteLine("\t" + name);
            }
            Console.WriteLine("");

            Console.WriteLine("DUPLICATED:");
            foreach (string name in duplicatedFiles)
            {
                Console.WriteLine("\t" + name);
            }
            Console.WriteLine("");
        }

        #endregion
    }
}