﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T24TelnetAdapter.UnitTests
{
    public class File : SCApiInterface.IFile
    {
        #region IFile Members

        public bool IsBinary
        {
            get;
            set;
        }

        public bool IsDLPackage
        {
            get
            {
                return ((ContentType & SCApiInterface.ContentType.DLPackage) != SCApiInterface.ContentType.Unknown);
            }
        }

        #endregion

        #region IDataSourceUnit Members

        public SCApiInterface.ContentType ContentType
        {
            get;
            set;
        }

        public int OrderPlace
        {
            get;
            set;
        }

        #endregion

        #region ISourceUnit Members

        public string RelationalPath
        {
            get;
            set;
        }
        public bool IsReleaseValid
        {
            get { throw new NotImplementedException(); }
        }
        public string RelationalNameFull
        {
            get { return RelationalPath + "\\" + Name; }
        }

        #endregion

        #region IBranchableUnit Members

        public string Version
        {
            get;
            set;
        }

        public SCApiInterface.UnitState State
        {
            get;
            set;
        }

        public SCApiInterface.UnitAction Action
        {
            get;
            set;
        }

        public string Comments
        {
            get;
            set;
        }

        public SCApiInterface.IBranchableUnit Predecessor
        {
            get { throw new NotImplementedException(); }
        }

        public SCApiInterface.IBranchableUnit SecondPredecessor
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IEnumerable<SCApiInterface.IBranchableUnit> Successors
        {
            get { throw new NotImplementedException(); }
        }

        public bool HistoryLoaded
        {
            get { throw new NotImplementedException(); }
        }

        public ulong PredecessorNoderef
        {
            get { throw new NotImplementedException(); }
        }

        public ulong SecondPredecessorNoderef
        {
            get { throw new NotImplementedException(); }
        }

        public ulong[] SuccessorsNoderefs
        {
            get { throw new NotImplementedException(); }
        }

        public ulong Thread
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region IHierUnit Members

        public SCApiInterface.IUnit Parent
        {
            get { throw new NotImplementedException(); }
        }

        public ulong ParentNoderef
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerable<SCApiInterface.IUnit> Children
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region IOwnedUnit Members

        public SCApiInterface.IUser Owner
        {
            get { throw new NotImplementedException(); }
        }

        public SCApiInterface.IRelease Release
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DateTime CreateDate
        {
            get { throw new NotImplementedException(); }
        }

        public DateTime UpdateDate
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region IUnit Members

        public ulong Noderef
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public SCApiInterface.UnitAttribute Name
        {
            get;
            set;
        }

        public SCApiInterface.IUnit Clone(SCApiInterface.IUser user)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IInfant<IUnit> Members

        public SCApiInterface.IUnit InfantParent
        {
            get { throw new NotImplementedException(); }
        }

        public bool EquivalentWithDiffs(SCApiInterface.IUnit item)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IComparable<IUnit> Members

        public int CompareTo(SCApiInterface.IUnit other)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IData Members

        public SCApiInterface.FileData Data
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
