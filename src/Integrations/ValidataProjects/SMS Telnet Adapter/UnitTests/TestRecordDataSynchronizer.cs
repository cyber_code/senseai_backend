﻿using System.Collections.Generic;
using System.Diagnostics;
using AXMLEngine;
using NUnit.Framework;
using ValidataCommon;

namespace T24TelnetAdapter.UnitTests
{
    [TestFixture]
    public class TestRecordDataSynchronizer
    {
        [Test]
        public void Test_SkipSame_DeleteEmpty_Default()
        {
            RecordDataSynchronizer syncer = new RecordDataSynchronizer();
            List<DeploymentFieldValues> result = syncer.GetAttributeValuesSynchronized(GetFieldInfosDefault(),
                                                                                               GetTargetInstance(),
                                                                                               GetOriginalInstance(),
                                                                                               false,
                                                                                               false,
                                                                                               false);
            OutputList(result);
            
            Assert.AreEqual(7, result.Count);

            Assert.AreEqual("OnlyInOriginalNonEmpty", result[0].FieldName);
            Assert.AreEqual("NULL", result[0].TargetValue);

            Assert.AreEqual("OnlyInTargetNonEmpty", result[1].FieldName);
            Assert.AreEqual("Some Value", result[1].TargetValue);

            Assert.AreEqual("InBothEmptyInOriginal", result[2].FieldName);
            Assert.AreEqual("Some Value", result[2].TargetValue);

            Assert.AreEqual("InBothEmptyInTarget", result[3].FieldName);
            Assert.AreEqual("NULL", result[3].TargetValue);

            Assert.AreEqual("InBothButDifferent", result[4].FieldName);
            Assert.AreEqual("Target Value", result[4].TargetValue);

            Assert.AreEqual("InBothMulti-2~1", result[5].FieldName);
            Assert.AreEqual("Target Value", result[5].TargetValue);

            Assert.AreEqual("InBothMulti-3~1", result[6].FieldName);
            Assert.AreEqual("-", result[6].TargetValue);
        }

        [Test]
        public void Test_SkipSame_DeleteEmpty_Override()
        {
            RecordDataSynchronizer syncer = new RecordDataSynchronizer();
            List<DeploymentFieldValues> result = syncer.GetAttributeValuesSynchronized(GetFieldInfosOverride(),
                                                                                               GetTargetInstance(),
                                                                                               GetOriginalInstance(),
                                                                                               false,
                                                                                               false,
                                                                                               false);
            OutputList(result);

            Assert.AreEqual(7, result.Count);

            Assert.AreEqual("OnlyInTargetNonEmpty", result[0].FieldName);
            Assert.AreEqual("Some Value", result[0].TargetValue);

            Assert.AreEqual("InBothEmptyInOriginal", result[1].FieldName);
            Assert.AreEqual("Some Value", result[1].TargetValue);

            Assert.AreEqual("InBothEmptyInTarget", result[2].FieldName);
            Assert.AreEqual("NULL", result[2].TargetValue);

            Assert.AreEqual("InBothButDifferent", result[3].FieldName);
            Assert.AreEqual("Target Value", result[3].TargetValue);

            Assert.AreEqual("InBothMulti-1~1", result[4].FieldName);
            Assert.AreEqual("Some Value", result[4].TargetValue);

            Assert.AreEqual("InBothMulti-2~1", result[5].FieldName);
            Assert.AreEqual("Target Value", result[5].TargetValue);

            Assert.AreEqual("InBothMulti-3~1", result[6].FieldName);
            Assert.AreEqual("-", result[6].TargetValue);
        }

        [Test]
        public void Test_DontSkipSame_DontDeleteEmpty_Default()
        {
            RecordDataSynchronizer syncer = new RecordDataSynchronizer();
            List<DeploymentFieldValues> result = syncer.GetAttributeValuesSynchronized(GetFieldInfosDefault(),
                                                                                               GetTargetInstance(),
                                                                                               GetOriginalInstance(),
                                                                                               false,
                                                                                               true,
                                                                                               true);
            
            OutputList(result);

            Assert.AreEqual(6, result.Count);

            Assert.AreEqual("OnlyInTargetNonEmpty", result[0].FieldName);
            Assert.AreEqual("Some Value", result[0].TargetValue);

            Assert.AreEqual("InBothEmptyInOriginal", result[1].FieldName);
            Assert.AreEqual("Some Value", result[1].TargetValue);

            Assert.AreEqual("InBothButDifferent", result[2].FieldName);
            Assert.AreEqual("Target Value", result[2].TargetValue);

            Assert.AreEqual("InBothSame", result[3].FieldName);
            Assert.AreEqual("Some Value", result[3].TargetValue);

            Assert.AreEqual("InBothMulti-1~1", result[4].FieldName);
            Assert.AreEqual("Some Value", result[4].TargetValue);

            Assert.AreEqual("InBothMulti-2~1", result[5].FieldName);
            Assert.AreEqual("Target Value", result[5].TargetValue);
        }

        [Test]
        public void Test_DontSkipSame_DontDeleteEmpty_Override()
        {
            RecordDataSynchronizer syncer = new RecordDataSynchronizer();
            List<DeploymentFieldValues> result = syncer.GetAttributeValuesSynchronized(GetFieldInfosOverride(),
                                                                                               GetTargetInstance(),
                                                                                               GetOriginalInstance(),
                                                                                               false,
                                                                                               true,
                                                                                               true);

            OutputList(result);

            Assert.AreEqual(6, result.Count);

            Assert.AreEqual("OnlyInTargetNonEmpty", result[0].FieldName);
            Assert.AreEqual("Some Value", result[0].TargetValue);

            Assert.AreEqual("InBothEmptyInOriginal", result[1].FieldName);
            Assert.AreEqual("Some Value", result[1].TargetValue);

            Assert.AreEqual("InBothButDifferent", result[2].FieldName);
            Assert.AreEqual("Target Value", result[2].TargetValue);

            Assert.AreEqual("InBothMulti-1~1", result[3].FieldName);
            Assert.AreEqual("Some Value", result[3].TargetValue);

            Assert.AreEqual("InBothMulti-2~1", result[4].FieldName);
            Assert.AreEqual("Target Value", result[4].TargetValue);

            Assert.AreEqual("InBothMulti-3~1", result[5].FieldName);
            Assert.AreEqual("-", result[5].TargetValue);
        }


        [Test]
        public void Test_DontSkipSame_DeleteEmpty_Default()
        {
            RecordDataSynchronizer syncer = new RecordDataSynchronizer();
            List<DeploymentFieldValues> result = syncer.GetAttributeValuesSynchronized(GetFieldInfosDefault(),
                                                                                               GetTargetInstance(),
                                                                                               GetOriginalInstance(),
                                                                                               false,
                                                                                               false,
                                                                                               true);
            OutputList(result);

            Assert.AreEqual(9, result.Count);

            Assert.AreEqual("OnlyInOriginalNonEmpty", result[0].FieldName);
            Assert.AreEqual("NULL", result[0].TargetValue);

            Assert.AreEqual("OnlyInTargetNonEmpty", result[1].FieldName);
            Assert.AreEqual("Some Value", result[1].TargetValue);

            Assert.AreEqual("InBothEmptyInOriginal", result[2].FieldName);
            Assert.AreEqual("Some Value", result[2].TargetValue);

            Assert.AreEqual("InBothEmptyInTarget", result[3].FieldName);
            Assert.AreEqual("NULL", result[3].TargetValue);

            Assert.AreEqual("InBothButDifferent", result[4].FieldName);
            Assert.AreEqual("Target Value", result[4].TargetValue);

            Assert.AreEqual("InBothSame", result[5].FieldName);
            Assert.AreEqual("Some Value", result[5].TargetValue);

            Assert.AreEqual("InBothMulti-1~1", result[6].FieldName);
            Assert.AreEqual("Some Value", result[6].TargetValue);

            Assert.AreEqual("InBothMulti-2~1", result[7].FieldName);
            Assert.AreEqual("Target Value", result[7].TargetValue);

            Assert.AreEqual("InBothMulti-3~1", result[8].FieldName);
            Assert.AreEqual("-", result[8].TargetValue);
        }

        [Test]
        public void Test_DontSkipSame_DeleteEmpty_Override()
        {
            RecordDataSynchronizer syncer = new RecordDataSynchronizer();
            List<DeploymentFieldValues> result = syncer.GetAttributeValuesSynchronized(GetFieldInfosOverride(),
                                                                                               GetTargetInstance(),
                                                                                               GetOriginalInstance(),
                                                                                               false,
                                                                                               false,
                                                                                               true);
            OutputList(result);

            Assert.AreEqual(7, result.Count);

            Assert.AreEqual("OnlyInTargetNonEmpty", result[0].FieldName);
            Assert.AreEqual("Some Value", result[0].TargetValue);

            Assert.AreEqual("InBothEmptyInOriginal", result[1].FieldName);
            Assert.AreEqual("Some Value", result[1].TargetValue);

            Assert.AreEqual("InBothEmptyInTarget", result[2].FieldName);
            Assert.AreEqual("NULL", result[2].TargetValue);

            Assert.AreEqual("InBothButDifferent", result[3].FieldName);
            Assert.AreEqual("Target Value", result[3].TargetValue);

            Assert.AreEqual("InBothMulti-1~1", result[4].FieldName);
            Assert.AreEqual("Some Value", result[4].TargetValue);

            Assert.AreEqual("InBothMulti-2~1", result[5].FieldName);
            Assert.AreEqual("Target Value", result[5].TargetValue);

            Assert.AreEqual("InBothMulti-3~1", result[6].FieldName);
            Assert.AreEqual("-", result[6].TargetValue);
        }

        [Test]
        public void Test_SkipSame_DontDeleteEmpty_Default()
        {
            RecordDataSynchronizer syncer = new RecordDataSynchronizer();
            List<DeploymentFieldValues> result = syncer.GetAttributeValuesSynchronized(GetFieldInfosDefault(),
                                                                                               GetTargetInstance(),
                                                                                               GetOriginalInstance(),
                                                                                               false,
                                                                                               true,
                                                                                               false);
            OutputList(result);

            Assert.AreEqual(4, result.Count);

            Assert.AreEqual("OnlyInTargetNonEmpty", result[0].FieldName);
            Assert.AreEqual("Some Value", result[0].TargetValue);

            Assert.AreEqual("InBothEmptyInOriginal", result[1].FieldName);
            Assert.AreEqual("Some Value", result[1].TargetValue);

            Assert.AreEqual("InBothButDifferent", result[2].FieldName);
            Assert.AreEqual("Target Value", result[2].TargetValue);

            Assert.AreEqual("InBothMulti-2~1", result[3].FieldName);
            Assert.AreEqual("Target Value", result[3].TargetValue);
        }

        [Test]
        public void Test_SkipSame_DontDeleteEmpty_Override()
        {
            RecordDataSynchronizer syncer = new RecordDataSynchronizer();
            List<DeploymentFieldValues> result = syncer.GetAttributeValuesSynchronized(GetFieldInfosOverride(),
                                                                                               GetTargetInstance(),
                                                                                               GetOriginalInstance(),
                                                                                               false,
                                                                                               true,
                                                                                               false);
            OutputList(result);

            Assert.AreEqual(6, result.Count);

            Assert.AreEqual("OnlyInTargetNonEmpty", result[0].FieldName);
            Assert.AreEqual("Some Value", result[0].TargetValue);

            Assert.AreEqual("InBothEmptyInOriginal", result[1].FieldName);
            Assert.AreEqual("Some Value", result[1].TargetValue);

            Assert.AreEqual("InBothButDifferent", result[2].FieldName);
            Assert.AreEqual("Target Value", result[2].TargetValue);

            Assert.AreEqual("InBothMulti-1~1", result[3].FieldName);
            Assert.AreEqual("Some Value", result[3].TargetValue);

            Assert.AreEqual("InBothMulti-2~1", result[4].FieldName);
            Assert.AreEqual("Target Value", result[4].TargetValue);

            Assert.AreEqual("InBothMulti-3~1", result[5].FieldName);
            Assert.AreEqual("-", result[5].TargetValue);
        }

        [Test]
        public void Test_HasInstanceNonEmptyMultivaluesAfterField()
        {
            RecordDataSynchronizer syncer = new RecordDataSynchronizer();
            Assert.AreEqual(true,
                            syncer.HasInstanceNonEmptyMultivaluesAfterField(GetTargetInstanceMultiValues(),
                                                                            "EmptyMiddleMultiValues-3~1"));
            Assert.AreEqual(true,
                            syncer.HasInstanceNonEmptyMultivaluesAfterField(GetTargetInstanceMultiValues(),
                                                                            "EmptyMiddleSubValues-2~2"));
            Assert.AreEqual(false,
                            syncer.HasInstanceNonEmptyMultivaluesAfterField(GetTargetInstanceMultiValues(),
                                                                            "NonEmptyMiddleValues-1~2"));
            Assert.AreEqual(false,
                            syncer.HasInstanceNonEmptyMultivaluesAfterField(GetTargetInstanceMultiValues(),
                                                                            "NonEmptyMiddleValues-2~2"));
            Assert.AreEqual(false,
                            syncer.HasInstanceNonEmptyMultivaluesAfterField(GetTargetInstanceMultiValues(),
                                                                            "NonEmptyMiddleValues-3~1"));
            Assert.AreEqual(true,
                            syncer.HasInstanceNonEmptyMultivaluesAfterField(GetTargetInstanceMultiValues(),
                                                                            "SingleMultivalue-1~1"));
        }

        private static void OutputList(List<DeploymentFieldValues> deplFieldValues)
        {
            Trace.WriteLine("Number of fields: " + deplFieldValues.Count);
            Trace.WriteLine("< Field Name | Original Value | Target Value >");
            foreach (DeploymentFieldValues fieldvalue in deplFieldValues)
            {
                Trace.WriteLine(fieldvalue.FieldName + " : " + fieldvalue.OriginalValue + " | " + fieldvalue.TargetValue);
            }
        }

        private static List<FieldInfo> GetFieldInfosDefault()
        {
            return new List<FieldInfo>
                       {
                           new FieldInfo
                               {
                                   FieldName = "OnlyInOriginalNonEmpty",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "OnlyInOriginalEmpty",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "OnlyInTargetNonEmpty",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "OnlyInTargetEmpty",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "InBothEmptyInOriginal",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "InBothEmptyInTarget",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "InBothButDifferent",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "InBothSame",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "InBothMulti",
                                   HasMultiValues = true,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "NonSynchronizable",
                                   HasMultiValues = true,
                                   IsSynchronizable = false,
                               }
                       };
        }

        private static List<FieldInfo> GetFieldInfosOverride()
        {
            return new List<FieldInfo>
                       {
                           new FieldInfo
                               {
                                   FieldName = "OnlyInOriginalNonEmpty",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                                   EmptyValueHandling = EmptyValueDeploymentHandling.SkipIfEmpty
                               },
                           new FieldInfo
                               {
                                   FieldName = "OnlyInOriginalEmpty",
                                   HasMultiValues = false,
                                   IsSynchronizable = true
                               },
                           new FieldInfo
                               {
                                   FieldName = "OnlyInTargetNonEmpty",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "OnlyInTargetEmpty",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "InBothEmptyInOriginal",
                                   HasMultiValues = false,
                                   IsSynchronizable = true
                               },
                           new FieldInfo
                               {
                                   FieldName = "InBothEmptyInTarget",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "InBothButDifferent",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                               },
                           new FieldInfo
                               {
                                   FieldName = "InBothSame",
                                   HasMultiValues = false,
                                   IsSynchronizable = true,
                                   UnchangedValueHandling = UnchangedValueDeploymentHandling.SkipIfNotChanged
                               },
                           new FieldInfo
                               {
                                   FieldName = "InBothMulti",
                                   HasMultiValues = true,
                                   IsSynchronizable = true,
                                   EmptyValueHandling = EmptyValueDeploymentHandling.DeleteIfEmpty,
                                   UnchangedValueHandling = UnchangedValueDeploymentHandling.InputIfNotChanged
                               },
                           new FieldInfo
                               {
                                   FieldName = "NonSynchronizable",
                                   HasMultiValues = true,
                                   IsSynchronizable = false,
                               }
                       };
        }

        private static Instance GetOriginalInstance()
        {
            Instance inst = new Instance("typical", "catalog");
            inst.AddAttribute("OnlyInOriginalNonEmpty", "Some Value");
            inst.AddAttribute("OnlyInOriginalEmpty", "No Value");
            inst.AddAttribute("InBothEmptyInOriginal", "No Value");
            inst.AddAttribute("InBothEmptyInTarget", "Some Value");
            inst.AddAttribute("InBothButDifferent", "Original Value");
            inst.AddAttribute("InBothSame", "Some Value");
            inst.AddAttribute("InBothMulti-1~1", "Some Value");
            inst.AddAttribute("InBothMulti-2~1", "No Value");
            inst.AddAttribute("InBothMulti-3~1", "Original Value");

            return inst;
        }

        private static Instance GetTargetInstance()
        {
            Instance inst = new Instance("typical", "catalog");
            inst.AddAttribute("OnlyInTargetNonEmpty", "Some Value");
            inst.AddAttribute("OnlyInTargetEmpty", "No Value");
            inst.AddAttribute("InBothEmptyInOriginal", "Some Value");
            inst.AddAttribute("InBothEmptyInTarget", "No Value");
            inst.AddAttribute("InBothButDifferent", "Target Value");
            inst.AddAttribute("InBothSame", "Some Value");
            inst.AddAttribute("InBothMulti-1~1", "Some Value");
            inst.AddAttribute("InBothMulti-2~1", "Target Value");
            inst.AddAttribute("InBothMulti-3~1", "No Value");
            inst.AddAttribute("NonSynchronizable", "Some Value");

            return inst;
        }

        private static Instance GetTargetInstanceMultiValues()
        {
            Instance inst = new Instance("typical", "catalog");
            inst.AddAttribute("SingleMultivalue-1~1", "No Value");
            inst.AddAttribute("EmptyMiddleMultiValues-1~1", "Some Value");
            inst.AddAttribute("EmptyMiddleMultiValues-2~1", "Some Value");
            inst.AddAttribute("EmptyMiddleMultiValues-3~1", "No Value");
            inst.AddAttribute("EmptyMiddleMultiValues-4~1", "Some Value");
            inst.AddAttribute("EmptyMiddleMultiValues-5~1", "Some Value");
            inst.AddAttribute("EmptyMiddleSubValues-1~1", "Some Value");
            inst.AddAttribute("EmptyMiddleSubValues-2~1", "Some Value");
            inst.AddAttribute("EmptyMiddleSubValues-2~2", "No Value");
            inst.AddAttribute("EmptyMiddleSubValues-2~3", "Some Value");
            inst.AddAttribute("EmptyMiddleSubValues-3~1", "Some Value");
            inst.AddAttribute("NonEmptyMiddleValues-1~1", "Some Value");
            inst.AddAttribute("NonEmptyMiddleValues-1~2", "No Value");
            inst.AddAttribute("NonEmptyMiddleValues-2~1", "Some Value");
            inst.AddAttribute("NonEmptyMiddleValues-2~2", "No Value");
            inst.AddAttribute("NonEmptyMiddleValues-3~1", "No Value");

            return inst;
        }
    }
}