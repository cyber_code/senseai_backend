﻿using System;

namespace T24TelnetAdapter
{
    public enum DeploymentMethod
    {
        #region Records Deployment

        [Obsolete("This enum value is obsolete; use Record_I instead")]
        Record_UsingVersion = 0,

        Record_IA = 1,

        Record_I,

        Record_A,

        Record_V,

        Record_Ignore,

        #endregion
        
        #region File Deployment

        File_DeployAndCompile,

        File_Deploy,

        File_Compile,

        DlDefinePackage_Deploy

        #endregion

    }
}