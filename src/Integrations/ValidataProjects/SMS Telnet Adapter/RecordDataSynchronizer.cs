﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AXMLEngine;
using ValidataCommon;

namespace T24TelnetAdapter
{
    internal class RecordDataSynchronizer
    {
        private Instance _TargetInstance;
        private Instance _ExistingInstance;
        private bool _SkipWhiteSpaceInFieldValue;
        private bool _SkipEmptyValuesDeletions;
        private bool _IncludeUnchangedValues;
        private List<DeploymentFieldValues> _AttributesToSync;

        public List<DeploymentFieldValues> GetAttributeValuesSynchronized(List<FieldInfo> allFields,
                                                                                  Instance instance,
                                                                                  Instance existingInstance,
                                                                                  bool skipWhiteSpaceInFieldValue,
                                                                                  bool skipEmptyValuesDeletions,
                                                                                  bool includeUnchangedValues)
        {
            _TargetInstance = instance;
            _ExistingInstance = existingInstance;
            _SkipWhiteSpaceInFieldValue = skipWhiteSpaceInFieldValue;
            _SkipEmptyValuesDeletions = skipEmptyValuesDeletions;
            _IncludeUnchangedValues = includeUnchangedValues;
            _AttributesToSync = new List<DeploymentFieldValues>();

            List<FieldInfo> significantFields = allFields.FindAll(f => f.IsSynchronizable);
            foreach (FieldInfo field in significantFields)
            {
                AddValuesToSyncInstances(field);
            }

            return _AttributesToSync;
        }
         
        public List<DeploymentFieldValues> GetAttributeValues(List<FieldInfo> allFields,
                                                              Instance instance,
                                                              Instance existingInstance)
        {
            _TargetInstance = instance;
            _ExistingInstance = existingInstance;
            _AttributesToSync = new List<DeploymentFieldValues>();

            List<FieldInfo> significantFields = allFields; //.FindAll(f => f.IsSynchronizable);
            foreach (FieldInfo field in significantFields)
            {
                AddValuesDirectly(field);
            }

            return _AttributesToSync;
        }

        private void AddValuesDirectly(FieldInfo field)
        {
            string fieldName = field.FieldName;

            if (fieldName == "ID")
            {
                // ignore the Validata ID, it is not a T24 field
                return;
            }

            if (fieldName == "@ID")
            {
                // the @ID is processed elsewhere, so ignore it
                return;
            }

            if (!field.HasMultiValues)
            {
                ProcessFieldDirectly(fieldName);
            }
            else
            {
                foreach (string multiValueFieldName in GetDistinctMultiValueFieldsFromInstances(fieldName, _TargetInstance, _ExistingInstance))
                {
                    ProcessFieldDirectly(multiValueFieldName);
                }
            }
        }

        private void ProcessFieldDirectly(string fieldName)
        {
            string targetValue = GetCanonicalFieldValue(_TargetInstance.GetAttributeValue(fieldName));

            string existingValue = _ExistingInstance == null ? "" : _ExistingInstance.GetAttributeValue(fieldName);
            if (existingValue == "No Value")
            {
                existingValue = "";
            }

            if (targetValue.Length > 0)
                _AttributesToSync.Add(new DeploymentFieldValues { FieldName = fieldName, TargetValue = targetValue, OriginalValue = existingValue });
        }

        private void AddValuesToSyncInstances(FieldInfo field)
        {
            string fieldName = field.FieldName;

            if (fieldName == "ID")
            {
                // ignore the Validata ID, it is not a T24 field
                return;
            }

            if (fieldName == "@ID")
            {
                // the @ID is processed elsewhere, so ignore it
                return;
            }

            if (!field.HasMultiValues)
            {
                ProcessSingleField(fieldName, field.EmptyValueHandling, field.UnchangedValueHandling, false);
            }
            else
            {
                foreach (string multiValueFieldName in GetDistinctMultiValueFieldsFromInstances(fieldName, _TargetInstance, _ExistingInstance))
                {
                    ProcessSingleField(multiValueFieldName, field.EmptyValueHandling, field.UnchangedValueHandling, true);
                }
            }
        }

        private static IEnumerable<string> GetDistinctMultiValueFieldsFromInstances(string fieldName, Instance instance, Instance existingInstance)
        {
            List<InstanceAttribute> attributes = instance.GetMultivalueAttributesByShortFieldName(fieldName);
            if (existingInstance != null)
            {
                List<InstanceAttribute> existingAttributes = existingInstance.GetMultivalueAttributesByShortFieldName(fieldName);
                attributes.AddRange(existingAttributes);
            }

            List<string> distinctAttrNames = new List<string>((from attr in attributes select attr.Name).Distinct());
            distinctAttrNames.Sort();
            return distinctAttrNames;
        }

        private string GetCanonicalFieldValue(string targetValue)
        {
            if (_SkipWhiteSpaceInFieldValue)
            {
                targetValue = targetValue.Trim();
            }
            if (targetValue == "No Value")
            {
                targetValue = "";
            }

            return targetValue;
        }

        private void ProcessSingleField(string fieldName, EmptyValueDeploymentHandling emptyHandling, UnchangedValueDeploymentHandling unchangedHandling, bool isMultivalue)
        {
            string targetValue = GetCanonicalFieldValue(_TargetInstance.GetAttributeValue(fieldName));

            // TODO check if the value is NONE or - (used for value removal)

            string existingValue = _ExistingInstance == null ? "" : _ExistingInstance.GetAttributeValue(fieldName);
            if (existingValue == "No Value")
            {
                existingValue = "";
            }

            if (unchangedHandling == UnchangedValueDeploymentHandling.SkipIfNotChanged
                || (unchangedHandling == UnchangedValueDeploymentHandling.Default && !_IncludeUnchangedValues))
            {
                if (targetValue == existingValue)
                {
                    // no need to update anything if values are the same
                    return;
                }
            }

            if (emptyHandling == EmptyValueDeploymentHandling.DeleteIfEmpty ||
                (emptyHandling == EmptyValueDeploymentHandling.Default && !_SkipEmptyValuesDeletions))
            {
                if (existingValue != "" && targetValue == "")
                {
                    // we need to delete the field value (using a special T24 keyword)
                    targetValue = isMultivalue ? GetMultiValueDeletionOfsKeyword(fieldName): "NULL";
                }
            }

            if (targetValue == "")
            {
                // just skip it empty values
                return;
            }

            _AttributesToSync.Add(new DeploymentFieldValues { FieldName = fieldName, TargetValue = targetValue, OriginalValue = existingValue });
        }

        private string GetMultiValueDeletionOfsKeyword(string fieldName)
        {
            return HasInstanceNonEmptyMultivaluesAfterField(_TargetInstance, fieldName) ? "NULL" : "-";
        }

        internal bool HasInstanceNonEmptyMultivaluesAfterField(Instance instance, string fieldName)
        {
            string shortName;
            int multiValueIndex, subValueIndex;
            bool isMultiValue = AttributeNameParser.ParseComplexFieldName(fieldName, out shortName, out multiValueIndex, out subValueIndex);
            Debug.Assert(isMultiValue);

            List<InstanceAttribute> attributes = instance.GetMultivalueAttributesByShortFieldName(shortName);
            foreach (InstanceAttribute attr in attributes)
            {
                if (GetCanonicalFieldValue(attr.Value).Length == 0)
                {
                    // we are interested only in non-empty field values
                    continue;
                }

                int currentMultiValueIndex = AttributeNameParser.GetMultiValueIndex(attr.Name);
                if (currentMultiValueIndex == multiValueIndex && AttributeNameParser.GetSubValueIndex(attr.Name) > subValueIndex)
                {
                    // if within the same multivalue group there are non-empty subvalues
                    return true;
                }

                if ((currentMultiValueIndex > multiValueIndex) && subValueIndex == 1)
                {
                    // we are in a new multivalue group, but this matters only for first subvalue in the group
                    return true;
                }
            }

            return (multiValueIndex == 1 && subValueIndex == 1);
        }
    }
}
