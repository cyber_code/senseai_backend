using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace T24TelnetAdapter
{
    internal class TCServerConnection : TerminalConnection
    {
        private Socket _Socket;
        private readonly double _TimeOutMilliseconds = double.MaxValue;

        private const int BufferSize = 4096;

        public TCServerConnection(string host, int port, int pageCode, double timeout) : base(host, port, pageCode)
        {
            _TimeOutMilliseconds = timeout;
        }

        public override TerminalConnectionType ConnectionType
        {
            get { return TerminalConnectionType.TCServer; }
        }

        public override bool IsConnected
        {
            get { return _Socket != null && _Socket.Connected; }
        }

        protected override byte[] ReadAvailableBytes()
        {
            List<byte> fullResponseBytes = new List<byte>();

            DateTime dtStart;
            do
            {
                dtStart = DateTime.Now;

                byte[] portion = GetNext();
                if (portion == null || portion.Length <= 0)
                {
                    return fullResponseBytes.ToArray();
                }
                
                fullResponseBytes.AddRange(portion);

                // if it ends on "\r\n" then the command should be complete, otherwise continue reading
                if (DoesResponseEndWIthNewLine(fullResponseBytes))
                    return fullResponseBytes.ToArray();

                Thread.Sleep(50);
            } while ((DateTime.Now - dtStart).TotalMilliseconds < _TimeOutMilliseconds);

            return new byte[0];
        }

        private static bool DoesResponseEndWIthNewLine(List<byte> responseBytes)
        {
            int count = responseBytes.Count;
            if (count > 2
                && responseBytes[count - 2] == '\r'
                && responseBytes[count - 1] == '\n')
                return true;

            return false;
        }

        private byte[] GetNext()
        {
            if (_Socket.Available == 0)
                return new byte[0];

            var bytes = new byte[BufferSize];
            int nReceived = _Socket.Receive(bytes);

            if (nReceived < BufferSize)
            {
                var resBytes = new byte[nReceived];
                Array.Copy(bytes, resBytes, nReceived);
                return resBytes;
            }

            return bytes;
        }

        protected override byte[] SendCommandText(string commandText)
        {
            byte[] bytesToSend = Encoding.GetEncoding(_PageCode).GetBytes(commandText.Trim() + "\r");
            _Socket.Send(bytesToSend, bytesToSend.Length, 0);
            return bytesToSend;
        }

        protected override string GetTextFromResponseBytes(byte[] buff)
        {
            string str = Encoding.GetEncoding(_PageCode).GetString(buff, 0, buff.Length);
            return str;
        }

        protected override Exception EstablishConnection()
        {
            try
            {
                IEnumerable<IPAddress> addresses = GetHostIPAddresses();
                if (addresses == null)
                {
                    return new Exception(string.Format("The host '{0}' does not seem a valid IP address or domain", _Host));
                }

                foreach (IPAddress address in addresses)
                {
                    IPEndPoint ipe = new IPEndPoint(address, _Port);
                    _Socket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    _Socket.Connect(ipe);
                    if (_Socket.Connected)
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return ex;
            }

            return new Exception("Can't connect to any of the host IP addresses");
        }

        /// <summary>
        /// 	Gets the host IP addresses.
        /// </summary>
        /// <returns> </returns>
        private IEnumerable<IPAddress> GetHostIPAddresses()
        {
            IPAddress ipa;
            if (IPAddress.TryParse(_Host, out ipa))
            {
                return new List<IPAddress> {ipa};
            }

            try
            {
                IPHostEntry hostEntry = Dns.Resolve(_Host);
                return hostEntry.AddressList;
            }
            catch (Exception ex)
            {
                Log.Debug(string.Format("The host '{0}' does not seem to be a valid IP address or domain", _Host), ex);
            }

            try
            {
                IPHostEntry iphost = Dns.GetHostEntry(_Host);
                return iphost.AddressList;
            }
            catch (Exception ex)
            {
                Log.Debug(string.Format("The host '{0}' does not seem to be a valid IP address or domain", _Host), ex);
            }

            return null;
        }

        public override void Disconnect()
        {
            try
            {
                if (IsConnected)
                {
                    CloseConnection();
                }

                _Socket = null;
            }
            catch (Exception ex)
            {
                Log.Error("Failed to close TCServer connection", ex);
            }
            finally
            {
                _Socket = null;
            }
        }

        private void CloseConnection()
        {
            Log.Info("Closing TCServer socket...");
            try
            {
                _Socket.Close();
                Log.Info("TCServer socket closed");
            }
            catch (Exception ex)
            {
                Log.Error("Failed to close TCServer socket", ex);
            }
        }
    }
}