using OFSCommonMethods.IO;

namespace T24TelnetAdapter
{
    public class SystemShellConnector : T24TelnetConnector
    {
        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.Shell; }
        }
    }

    public class DBShellConnector : T24TelnetConnector
    {
        public override ConsoleType ConnectionType
        {
            get { return ConsoleType.DBShell; }
        }
    }
}