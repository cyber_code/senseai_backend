﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace T24TelnetAdapter
{
    internal class TelnetConnection : TerminalConnection
    {
        private Socket _Socket;

        public TelnetConnection(string host, int port, int pageCode) : base(host, port, pageCode) {}
        
        public override TerminalConnectionType ConnectionType
        {
            get { return TerminalConnectionType.Telnet; }
        }

        public override bool IsConnected
        {
            get { return _Socket != null && _Socket.Connected; }
        }

        protected override byte[] ReadAvailableBytes()
        {
            if (_Socket.Available == 0)
            {
                return new byte[0];
            }

            List<byte> result = new List<byte>();

            while (_Socket.Available > 0)
            {
                byte[] buff = new byte[1];
                _Socket.Receive(buff, 1, SocketFlags.None);

                switch (buff[0])
                {
                    case (byte) TelnetCommands.Iac:
                        // interpret as command
                        _Socket.Receive(buff, 1, SocketFlags.None);
                        byte inputverb = buff[0];

                        switch (inputverb)
                        {
                            case (byte) TelnetCommands.Iac:
                                //literal IAC = 255 escaped, so append char 255 to string
                                result.Add(inputverb);
                                break;

                            case (byte) TelnetCommands.Do:
                            case (byte) TelnetCommands.Dont:
                            case (byte) TelnetCommands.Will:
                            case (byte) TelnetCommands.Wont:
                                // reply to all commands with "WONT", unless it is SGA (suppress go ahead)
                                _Socket.Receive(buff, 1, SocketFlags.None);
                                int inputoption = buff[0];
                                if (inputoption == -1)
                                {
                                    break;
                                }
                                _Socket.Send(new byte[] {(byte) TelnetCommands.Iac}, 1, SocketFlags.None);

                                if (inputoption == (int) Options.SGA)
                                {
                                    _Socket.Send(
                                        new byte[] {inputverb == (byte) TelnetCommands.Do ? (byte) TelnetCommands.Will : (byte) TelnetCommands.Do},
                                        1,
                                        SocketFlags.None);
                                }
                                else
                                {
                                    _Socket.Send(
                                        new byte[] {inputverb == (byte) TelnetCommands.Do ? (byte) TelnetCommands.Wont : (byte) TelnetCommands.Dont},
                                        1,
                                        SocketFlags.None);
                                }
                                _Socket.Send(new byte[] {(byte) inputoption}, 1, SocketFlags.None);

                                break;
                            default:
                                break;
                        }
                        break;

                    default:
                        result.Add(buff[0]);
                        break;
                }
            }

            return result.ToArray();
        }

        protected override byte[] SendCommandText(string commandText)
        {
            byte[] bytesToSend = Encoding.UTF8.GetBytes(commandText + "\r");
            SendBytes(bytesToSend);
            return bytesToSend;
        }

        public void ToggleTelnetEcho(bool echoOn)
        {
            // this function does not appear to work, use it with caution
            // see http://www.faqs.org/rfcs/rfc857.html  for reference
            if (echoOn)
            {
                SendBytes(new byte[] {(byte) TelnetCommands.Iac, (byte) TelnetCommands.Will, 1});
                SendBytes(new byte[] {(byte) TelnetCommands.Iac, (byte) TelnetCommands.Do, 1});
            }
            else
            {
                SendBytes(new byte[] {(byte) TelnetCommands.Iac, (byte) TelnetCommands.Wont, 1});
                SendBytes(new byte[] {(byte) TelnetCommands.Iac, (byte) TelnetCommands.Dont, 1});
            }
        }

        private void SendBytes(byte[] bytesToSend)
        {
            _Socket.Send(bytesToSend, bytesToSend.Length, 0);
        }

        /// <summary>
        /// Gets the text from telnet response. Strip off the telnet bytes
        /// </summary>
        /// <param name="buff">The buff.</param>
        /// <returns></returns>
        protected override string GetTextFromResponseBytes(byte[] buff)
        {
            byte[] cleanedBuff = new byte[buff.Length];
            int pos = 0;
            for (int i = 0; i < buff.Length; i++)
            {
                if (!CodeHelper.IsTelnetCommand(buff[i]))
                {
                    cleanedBuff[pos++] = buff[i];
                }
            }

            string result = Encoding.GetEncoding(PageCode).GetString(cleanedBuff, 0, pos);
            // there are also other sequences that we should ignore
            result = RemoveTelnetSpecialCharSequences(result);

            return result;
        }
        //static
        private  string RemoveTextAffectedByBackSpace(string text)
        {
            int posBS = text.IndexOf('\b'); // this is the backspace character

            if (posBS < 0)
            {
                // no backspace
                return text;
            }
            else if (posBS > 0)
            {
                string textToRemove = text.Substring(posBS - 1, 2);
                Log.Debug(string.Format("Removed '{0}' from the read data due to backspace", textToRemove));

                string newText = text.Substring(0, posBS - 1) + text.Substring(posBS + 1);
                return RemoveTextAffectedByBackSpace(newText);
            }
            else
            {
                Debug.Assert(posBS == 0);

                string newText = text.Substring(1);
                Log.Debug(string.Format("Removed single backspace preceeding '{0}'", newText));
                return RemoveTextAffectedByBackSpace(newText);
            }
        }
        //static
        private string RemoveTelnetSpecialCharSequences(string telnetResponse)
        {
            string result = telnetResponse.
                Replace(ANSI_Escape_Sequences.ClearScreen, String.Empty).
                Replace(ANSI_Escape_Sequences.EraseLine, String.Empty).
                Replace(ANSI_Escape_Sequences.MoveCursorRight, String.Empty).
                Replace(ANSI_Escape_Sequences.PositionCursor11, String.Empty).
                Replace(ANSI_Escape_Sequences.SometingElse, String.Empty).
                Replace(ANSI_Escape_Sequences.Null, String.Empty).
                Replace(ANSI_Escape_Sequences.Bel, String.Empty).
                Replace(ANSI_Escape_Sequences.FormFeed, String.Empty);


            // remove from the response backspaces that appear on some environment when writing special symbols like "_"
            return RemoveTextAffectedByBackSpace(result);
        }

        protected override Exception EstablishConnection()
        {
            try
            {
                IEnumerable<IPAddress> addresses = GetHostIPAddresses();
                if (addresses == null)
                {
                    return new Exception(string.Format("The host '{0}' does not seem a valid IP address or domain", _Host));
                }

                foreach (IPAddress address in addresses)
                {
                    IPEndPoint ipe = new IPEndPoint(address, _Port);
                    _Socket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    _Socket.Connect(ipe);
                    if (_Socket.Connected)
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return ex;
            }

            return new Exception("Can't connect to any of the host IP addresses");
        }

        private IEnumerable<IPAddress> GetHostIPAddresses()
        {
            IPAddress ipa;
            if (IPAddress.TryParse(_Host, out ipa))
            {
                return new List<IPAddress> { ipa };
            }

            try
            {
                IPHostEntry hostEntry = Dns.Resolve(_Host);
                return hostEntry.AddressList;
            }
            catch (Exception ex)
            {
                Log.Debug(string.Format("The host '{0}' does not seem to be a valid IP address or domain", _Host), ex);
            }

            try
            {
                IPHostEntry iphost = Dns.GetHostEntry(_Host);
                return iphost.AddressList;
            }
            catch (Exception ex)
            {
                Log.Debug(string.Format("The host '{0}' does not seem  to be a valid IP address or domain", _Host), ex);
            }

            return null;
        }

        public override void Disconnect()
        {
            try
            {
                if (IsConnected)
                {
                    CloseConnection();
                }

                _Socket = null;
            }
            catch (Exception ex)
            {
                Log.Error("Failed to close connection", ex);
            }
            finally
            {
                _Socket = null;
            }
        }

        private void CloseConnection()
        {
            Log.Info("Closing telnet socket...");
            try
            {
                _Socket.Close();
                Log.Info("Telnet socket closed");
            }
            catch (Exception ex)
            {
                Log.Error("Failed to close telent socket", ex);
            }
        }
    }
}