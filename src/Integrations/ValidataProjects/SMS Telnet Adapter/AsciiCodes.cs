﻿using System;
using System.Text;

namespace T24TelnetAdapter
{
    public enum AsciiCodes : byte
    {
        /// <summary>
        ///     Null character (ASCII 0)
        /// </summary>
        NUL = 0,

        /// <summary>
        ///     Bell (ASCII 7)
        /// </summary>
        BEL = 7,

        /// <summary>
        ///     Backspace (ASCII 8)
        /// </summary>
        BS = 8,

        /// <summary>
        ///     Horizontal tab (ASCII 9)
        /// </summary>
        HT = 9,

        /// <summary>
        ///     Line Feed (ASCII 10) Moves the printer to next line (keeping the same horizontal position).
        /// </summary>
        LF = 10,

        /// <summary>
        ///     Vertical Tab (ASCII 11)
        /// </summary>
        VT = 11,

        /// <summary>
        ///     Form Feed (ASCII 12)
        /// </summary>
        FF = 12,

        /// <summary>
        ///     Carriage Return (ASCII 13) Moves the printer to the left margin of the current line.
        /// </summary>
        CR = 13
    }

    public enum TelnetCommands : byte
    {
        /// <summary>
        ///     End Subnegotiation (SE)
        /// </summary>
        /// <remarks>
        ///     This command indicates the end of subnegotiation data.
        /// </remarks>
        /// <seealso cref="Subnegotiation"/>
        EndSubnegotiation = 240,

        /// <summary>
        ///     No operation (NOP)
        /// </summary>
        NoOperation = 241,

        /// <summary>
        ///     Data Mark
        /// </summary>
        DataMark = 242,

        /// <summary>
        ///     Break (BRK)
        /// </summary>
        Break = 243,

        /// <summary>
        ///     Interrupt Process (IP)
        /// </summary>
        InterruptProcess = 244,

        /// <summary>
        ///     Abort output (AO)
        /// </summary>
        AbortOutput = 245,

        /// <summary>
        ///     Are You There (AYT)
        /// </summary>
        AreYouThere = 246,

        /// <summary>
        ///     Erase Character (EC)
        /// </summary>
        /// <seealso cref="EraseLine"/>
        EraseCharacter = 247,

        /// <summary>
        ///     Erase Line (EL)
        /// </summary>
        /// <seealso cref="EraseCharacter"/>
        EraseLine = 248,

        /// <summary>
        ///     Go Ahead (GA)
        /// </summary>
        GoAhead = 249,

        /// <summary>
        ///     Subnegotiation (SB)
        /// </summary>
        /// <seealso cref="EndSubnegotiation"/>
        Subnegotiation = 250,

        /// <summary>
        ///     Will do option (WILL)
        /// </summary>
        /// <seealso cref="Do"/>
        /// <seealso cref="Dont"/>
        /// <seealso cref="Wont"/>
        Will = 251,

        /// <summary>
        ///     Will not do option (WONT)
        /// </summary>
        /// <seealso cref="Do"/>
        /// <seealso cref="Dont"/>
        /// <seealso cref="Will"/>
        Wont = 252,

        /// <summary>
        ///     Do option (DO)
        /// </summary>
        /// <seealso cref="Dont"/>
        /// <seealso cref="Will"/>
        /// <seealso cref="Wont"/>
        Do = 253,

        /// <summary>
        ///     Don't do option (DONT)
        /// </summary>
        /// <seealso cref="Do"/>
        /// <seealso cref="Will"/>
        /// <seealso cref="Wont"/>
        Dont = 254,

        /// <summary>
        ///     IAC
        /// </summary>
        Iac = 255
    }

    internal enum Options 
    {
        SGA = 3
    }

    public static class ANSI_Escape_Sequences
    {
        public static string ClearScreen = ((char) 27).ToString() + ((char) 91).ToString() + "J";

        public static string PositionCursor11 = ((char) 27).ToString() + ((char) 91).ToString() + "H";

        public static string SometingElse = ((char) 27).ToString() + ((char) 91).ToString() + "K";

        public static string EraseLine = ((char) 27).ToString() + ((char) 91).ToString() + "K" + ((char) 8).ToString();

        public static string MoveCursorRight = ((char) 27).ToString() + ((char) 91).ToString() + "C";

        public static string Null = ((char)0).ToString();

        public static string Bel = ((char)7).ToString(); // \a

        public static string FormFeed = ((char)12).ToString(); // \f
    }

    public static class CodeHelper
    {
        internal static string CTRL_U { get { return "" + CTRL_U_CHAR; } }

        internal static string CTRL_V { get { return "" + CTRL_V_CHAR; } }

        internal static char CTRL_U_CHAR = (char)21;

        internal static char CTRL_V_CHAR = (char)22;

        internal static char CTRL_B_CHAR = (char)2;

        internal static char CTRL_F_CHAR = (char)6;


        const char CARRET_ESCAPE = '^';
        public static string TranslateClassicTerminalSigns(string command)
        {
            // TODO: support various notations: caret ^V^M, 
            // or some escaping

            StringBuilder sb = new StringBuilder("");
            bool isInSequence = false;

            for (int ccc = 0; ccc < command.Length; ccc++)
            {
                var currChar = command[ccc];
                if (currChar == CARRET_ESCAPE)
                {
                    isInSequence = true;
                    continue;
                }
                if (isInSequence)
                {
                    var esc = getCarretEscapedChar(currChar);
                    if (esc != null)
                    {
                        if (esc != '\n') // TODO: would it work to send newline as part of the command? ; could be useful in some cases
                            sb.Append(esc);
                    }
                    else
                    {
                        // no escaping supoprted, bring back the carret
                        sb.Append(CARRET_ESCAPE);
                        sb.Append(currChar);
                    }

                    isInSequence = false;
                    continue;
                }
                sb.Append(currChar);
            }

            return sb.ToString();
        }

        private static char? getCarretEscapedChar(char currChar)
        {
            switch (currChar)
            {
                case 'V':
                    return CTRL_V_CHAR;
                case 'U':
                    return CTRL_U_CHAR;
                case 'B':
                    return CTRL_B_CHAR;
                case 'F':
                    return CTRL_F_CHAR;
                case 'M':
                    return '\n';
                default:
                    return null;
            }
        }

        public static bool IsTelnetCommand(byte b)
        {
            return (b >= (byte) TelnetCommands.EndSubnegotiation && b <= (byte) TelnetCommands.Iac);
        }
    }
}