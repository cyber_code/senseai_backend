﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Common;
using SCApiInterface;
using ValidataCommon;
using ValidataFtp;

namespace T24TelnetAdapter
{
    public class BulkCompiler
    {
        #region Private Members

        private readonly FolderMapList _FolderMapList;

        private readonly RequestManager _RequestManager;

        private readonly List<CompiledFile> _Files = new List<CompiledFile>();

        private readonly Dictionary<string, string> _AffectedFilesBackupErrors = new Dictionary<string, string>();

        private readonly Dictionary<string, byte[]> _BackupOfAffectedServerFiles = new Dictionary<string, byte[]>();

        private List<string> _AllAffectedFilePaths = new List<string>();

        private List<string> _AffectedFilesFromPreAnalysis = new List<string>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets of all files (some might be not be compiled)
        /// </summary>
        public List<CompiledFile> AllFiles
        {
            get { return _Files; }
        }

        /// <summary>
        /// Gets the files that will be compiled
        /// </summary>
        /// <value>The not skipped files to compile.</value>
        public List<CompiledFile> NotSkippedFiles
        {
            get { return _Files.FindAll(cf => !cf.IsSkipped); }
        }

        /// <summary>
        /// Gets the errors when backing up the affected files.
        /// </summary>
        /// <value>The affected files backup errors.</value>
        public Dictionary<string, string> AffectedFilesBackupErrors
        {
            get { return _AffectedFilesBackupErrors; }
        }

        /// <summary>
        /// The contents of the affected files that need to be backuped
        /// </summary>
        /// <value>The backup of affected server files.</value>
        public Dictionary<string, byte[]> BackupOfAffectedServerFiles
        {
            get { return _BackupOfAffectedServerFiles; }
        }

        #endregion

        #region Class Lifecycle

        public BulkCompiler(RequestManager reqManager, FolderMapList folderMapList)
        {
            _RequestManager = reqManager;
            _FolderMapList = folderMapList;
        }

        #endregion

        public CompiledFile FindCompiledFileByFile(IFile file)
        {
            return _Files.Find(cf => cf.File == file);
        }

        public void AddFileForProcessing(IFile file, string deploymentPack, DeploymentMethod method)
        {
            CompiledFile fileToCompile = CreateCompiledFile(file, deploymentPack, method);
            _Files.Add(fileToCompile);
        }

        private bool IsFileAlreadyCompiled(CompiledFile fileToCompile)
        {
            return _Files.Any(
                cf => cf != fileToCompile
                      && cf.IsSuccessful
                      && cf.RoutineName == fileToCompile.RoutineName);
        }

        public void CompileAllFilesInPackage(string packageName)
        {
            foreach (CompiledFile file in NotSkippedFiles.FindAll(cf => cf.File.DeploymentPack == packageName))
            {
                CompileSingleFile(file);
            }
        }

        public void CompileSingleFile(CompiledFile file)
        {
            // We have to make sure that no files with the same name will be compiled within the same session.
            // since the compilation of one of them will overwrite the compilation of the previous ones
            if (IsFileAlreadyCompiled(file))
            {
                file.IsSkipped = true;
                file.SkippedMessage =
                    "The compilation was skipped since a routine with the same name has already been successfully compiled in this session";
            }
            else
            {

                DeploymentMethod method = DeploymentMethod.File_DeployAndCompile;
                if (file.DeployMethod != null)
                    method = (DeploymentMethod) file.DeployMethod;
                file.Output = _RequestManager.CompileFile(file.File, method);
            }
        }

        public string[] FindAffectedFilesMissingInEnvironment()
        {
            List<string> missingFiles = new List<string>();

            // find which are the affected files from the two jshows (pre and post deployment)
            List<string> affectedFilesFromPreAnalysis = new List<string>();
            foreach (CompiledFile file in NotSkippedFiles.FindAll(cf => cf.AnalysisBeforeCompilation != null))
            {
                affectedFilesFromPreAnalysis.AddRange(file.AnalysisBeforeCompilation.AffectedFiles);
            }
            affectedFilesFromPreAnalysis = affectedFilesFromPreAnalysis.Distinct().ToList();

            foreach (string filePath in affectedFilesFromPreAnalysis)
            {
                if (!_RequestManager.DoesFileExist(filePath))
                {
                    missingFiles.Add(filePath);
                }
            }

            return missingFiles.ToArray();
        }

        public void FindAndDownloadAffectedFilesAfterCompilation(FtpConnector ftpManager)
        {
            List<CompiledFile> successfullyCompiledFiles = _Files.FindAll(cf => cf.IsSuccessful);
            foreach (CompiledFile file in successfullyCompiledFiles)
            {
                file.AnalysisAfterCompilation = _RequestManager.AnalyzeFile(file, false);
                file.AnalysisAfterCompilation.CurrentState = GetFileExistanceState(file.AnalysisAfterCompilation.AffectedFiles);
            }

            // find which are the affected files from the two jshows (pre and post deployment)
            List<string> allAffectedFiles = new List<string>();
            List<string> affectedFilesFromPreAnalysis = new List<string>();
            foreach (CompiledFile file in successfullyCompiledFiles)
            {
                allAffectedFiles.AddRange(file.GetAffectedServerFilesPaths());
                affectedFilesFromPreAnalysis.AddRange(file.AnalysisBeforeCompilation.AffectedFiles);
            }

            _AllAffectedFilePaths = allAffectedFiles.Distinct().ToList();
            _AffectedFilesFromPreAnalysis = affectedFilesFromPreAnalysis.Distinct().ToList();

            DownloadAffectedFiles(ftpManager);
        }
        
        public RequestManager.EnvironmentFolders GetAffectedFolders()
        {
            RequestManager.EnvironmentFolders folders = RequestManager.EnvironmentFolders.None;

            // analyze affected folders
            foreach (CompiledFile file in NotSkippedFiles)
            {
                if (_RequestManager.IsGlobusBPFolder(file.File.ServerDeploymentFolder, file.File.IsCatalog))
                {
                    folders = folders | RequestManager.EnvironmentFolders.GlobusSystem;
                }
                else
                {
                    folders = folders | RequestManager.EnvironmentFolders.Development;
                }
            }

            return folders;
        }

        public static List<string> GetAffectedObjectFiles(List<CompiledFile> notSkippedFiles, bool includeAfterCompilation)
        {
            List<string> result = new List<string>();

            foreach (CompiledFile file in notSkippedFiles)
            {
                foreach (string fileName in file.AnalysisBeforeCompilation.AffectedObjectFiles)
                {
                    if (!result.Contains(fileName))
                    {
                        result.Add(fileName);
                    }
                }

                if(includeAfterCompilation)
                {
                    foreach (string fileName in file.AnalysisAfterCompilation.AffectedObjectFiles)
                    {
                        if (!result.Contains(fileName))
                        {
                            result.Add(fileName);
                        }
                    }
                }
            }

            return result;
        }

        public List<string> GetAffectedObjectFiles(bool includeAfterCompilation)
        {
            return BulkCompiler.GetAffectedObjectFiles(NotSkippedFiles, includeAfterCompilation);
        }

        public List<string> GetAffectedObjectFiles()
        {
            return BulkCompiler.GetAffectedObjectFiles(NotSkippedFiles, false);
        }

        public List<string> AnalyzeImpactBeforeCompilation()
        {
            List<string> errorMessages = new List<string>();

            List<CompiledFile> notSkippedFiles = NotSkippedFiles;
            foreach (CompiledFile file in notSkippedFiles)
            {
                file.AnalysisBeforeCompilation = _RequestManager.AnalyzeFile(file, true);
            }

            // analyse results
            foreach (CompiledFile file in notSkippedFiles)
            {
                string errMsg = PerformAdditionalImpactAnalysis(file.AnalysisBeforeCompilation, file.File.ServerDeploymentFolder, file.File.IsCatalog, file.File.Name);
                if (errMsg != null)
                {
                    errorMessages.Add(errMsg);
                }
            }

            return errorMessages;
        }

        private T24FileLocation GetFileExistanceState(IEnumerable<string> affectedFileNames)
        {
            bool isExistingInSys = IsOneOfTheFilesIsInPath(affectedFileNames, _RequestManager.FullJGlobusLibDir, _RequestManager.OS) ||
                IsOneOfTheFilesIsInPath(affectedFileNames, _RequestManager.FullJGlobusBinDir, _RequestManager.OS);
            return (isExistingInSys ? T24FileLocation.ExistingInGlobus :T24FileLocation.ExistingInDEV);
        }

        internal string PerformAdditionalImpactAnalysis(CompilationAnalysisResult analysisResult, string serverDeploymentFolder, bool isCatalog, string fileName)
        {
            if (analysisResult.AffectedFiles.Count == 0)
            {
                // Nothing affected
                analysisResult.CurrentState = T24FileLocation.NotExisting;
                return null;
            }

            bool isDeployingToSys = _RequestManager.IsGlobusBPFolder(serverDeploymentFolder, isCatalog);

            analysisResult.CurrentState = GetFileExistanceState(analysisResult.AffectedFiles);
            bool isExistingInSys = (analysisResult.CurrentState == T24FileLocation.ExistingInGlobus);

            if (isExistingInSys == isDeployingToSys)
            {
                // OK
                return null;
            }

            // FAIL
            string tryingToDeployIn = isDeployingToSys ? _RequestManager.FullJGlobusLibDir : _RequestManager.FullJLibDir;
            string existingIn = isExistingInSys ? _RequestManager.FullJGlobusLibDir : _RequestManager.FullJLibDir;
            string msg = string.Format(
                "Unable to deploy/compile file: '{0}' in '{1}' directory! " +
                "The previous version of the file is compiled in '{2}' directory ",
                fileName,
                tryingToDeployIn,
                existingIn
                );

            analysisResult.ErrorMessage = msg;

            return msg;
        }

        public void AddSkippedFile(IFile file, string deploymentPack, string skippedMessage)
        {
            CompiledFile skippedFile = CreateCompiledFile(file, deploymentPack, null);
            skippedFile.IsSkipped = true;
            skippedFile.SkippedMessage = skippedMessage;

            _Files.Add(skippedFile);
        }

        private CompiledFile CreateCompiledFile(IFile file, string deploymentPack, DeploymentMethod? method)
        {
            CompiledFile compiledFile = new CompiledFile();

            bool isCatalog;
            string deploymentFolder = FtpPath.GetDeploymentPath(_FolderMapList, file.RelationalPath, out isCatalog);
            compiledFile.File = new DeployableFile(file, deploymentPack, deploymentFolder);
            compiledFile.File.IsCatalog = isCatalog;
            compiledFile.DeployMethod = method;
            return compiledFile;
        }

        private void DownloadAffectedFiles(FtpConnector ftpManager)
        {
            foreach (string affectedFilePath in _AllAffectedFilePaths)
            {
                DownloadAffectedFile(ftpManager, affectedFilePath);
            }
        }

        private void DownloadAffectedFile(FtpConnector ftpManager, string affectedFilePath)
        {
            if (_BackupOfAffectedServerFiles.ContainsKey(affectedFilePath))
            {
                Debug.Fail("Strange, should have been unique");
                return;
            }

            bool useBinary = ftpManager.UseBinary;
            ftpManager.UseBinary = true;

            try
            {
                string filePathFromBackupDirectory = GetFilePathFromBackupDirectory(affectedFilePath);
                if (!_RequestManager.DoesFileExist(filePathFromBackupDirectory))
                {
                    bool wasExistingFile = _AffectedFilesFromPreAnalysis.Contains(affectedFilePath);
                    if (!wasExistingFile)
                    { 
                        // Some of the files might be newly created, so they will not exist in the backup. Just ignore them
                        return;
                    }
                    else
                    {
                        // just notify that this is a strange situation and hope that the logic is wrong and the FTP will work
                        Debug.Fail("The file should have exist " + filePathFromBackupDirectory);
                    }
                }

                string fileName = FtpPath.Build(_RequestManager.HomeDir, _RequestManager.FtpWorkingDirectory, filePathFromBackupDirectory, _RequestManager.FtpLogsSubdir);
                byte[] contents = ftpManager.DownloadFile(fileName);
                _BackupOfAffectedServerFiles.Add(affectedFilePath, contents);
            }
            catch (Exception ex)
            {
                _AffectedFilesBackupErrors[affectedFilePath] = ex.Message;
                return;
            }
            finally
            {
                // restore the old value
                ftpManager.UseBinary = useBinary;
            }
        }

        internal string GetFilePathFromBackupDirectory(string filePath)
        {
            //  Need to test the 'filePath':
            //  if (filePath.LastDir == _JLibDir) ==> take it from SASXXXXX\_JLibDir\*
            //  if (filePath.LastDir == _JBinDir) ==> take it from SASXXXXX\_JBinDir\*
            //  if (filePath.LastDir == _JSysLibDir) ==> take it from SASXXXXX\_JSysLibDir\*
            //  if (filePath.LastDir == _JSysBinDir) ==> take it from SASXXXXX\_JSysBinDir\*
            //  if (filePath.LastDir == ObjDir) ==> take it as it is 
            //      (see: ConstructObjectFileName, objFullName variable) merge with its logic to prevent duplicated logic
            //  DEFAULT: Verify that file is source file!!!

            string fileName;
            string folder;
            if (!SplitPath(filePath, out fileName, out folder))
            {
                Debug.Fail("???");
                return filePath;
            }

            if (_RequestManager.AreFoldersEqual(folder, _RequestManager.FullJLibDir))
            {
                return GetFullFilePathInBackupDirectory(GetPathRelativeToHomeDir(_RequestManager.FullJLibDir), fileName);
            }
            else if (_RequestManager.AreFoldersEqual(folder, _RequestManager.FullJBinDir))
            {
                return GetFullFilePathInBackupDirectory(GetPathRelativeToHomeDir(_RequestManager.FullJBinDir), fileName);
            }
            else if (_RequestManager.AreFoldersEqual(folder, _RequestManager.FullJGlobusLibDir))
            {
                return GetFullFilePathInBackupDirectory(GetPathRelativeToHomeDir(_RequestManager.FullJGlobusLibDir), fileName);
            }
            else if (_RequestManager.AreFoldersEqual(folder, _RequestManager.FullJGlobusBinDir))
            {
                return GetFullFilePathInBackupDirectory(GetPathRelativeToHomeDir(_RequestManager.FullJGlobusBinDir), fileName);
            }
            else if (_RequestManager.AreFoldersEqual(folder, _RequestManager.FullObjectDevDir))
            {
                return GetFullFilePathInBackupDirectory(GetPathRelativeToHomeDir(_RequestManager.FullObjectDevDir), fileName);
            }
            else if (_RequestManager.AreFoldersEqual(folder, _RequestManager.FullObjectGlobusDir))
            {
                return GetFullFilePathInBackupDirectory(GetPathRelativeToHomeDir(_RequestManager.FullObjectGlobusDir), fileName);
            }

            // Return original file path
            //Debug.Fail("Should happen only for source files!");

            return filePath;
        }

        #region Private helper

        private string GetPathRelativeToHomeDir(string path)
        {
            return FtpPath.MakeRelative(_RequestManager.HomeDir, path);
        }

        private string GetFullFilePathInBackupDirectory(string relativeDirPath, string fileName)
        {
            string backupFolder = _RequestManager.FullBackupFolderPath;
            if (!backupFolder.EndsWith("\\") && !backupFolder.EndsWith("/"))
            {
                backupFolder += _RequestManager.PathDelimiter;
            }

            // TODO why is this "i"?
            int i = 0;
            for (; i < relativeDirPath.Length; i++)
            {
                if (relativeDirPath[i] != '.')
                {
                    if (relativeDirPath[i] != '/')
                    {
                        if (relativeDirPath[i] != '\\')
                        {
                            break;
                        }
                    }
                }
            }
            relativeDirPath = relativeDirPath.Substring(i);

            backupFolder += relativeDirPath;
            backupFolder += _RequestManager.PathDelimiter;

            return FtpPath.BuildX(backupFolder, fileName, _RequestManager.FtpLogsSubdir);
        }

        #endregion

        #region Private Static Helpers

        private bool IsOneOfTheFilesIsInPath(IEnumerable<string> fileNames, string thePath, OperatingSystem os)
        {
            foreach (string name in fileNames)
            {
                string fileName;
                string filePath;
                if (!SplitPath(name, out fileName, out filePath))
                {
                    continue;
                }

                if (_RequestManager.AreFoldersEqual(thePath, filePath))
                {
                    return true;
                }
            }

            return false;
        }

        internal static bool SplitPath(string fullPath, out string fileName, out string path)
        {
            fileName =
                path = null;

            fullPath = fullPath.Trim();

            if (string.IsNullOrEmpty(fullPath))
            {
                Debug.Fail("What a path!?!?!?");
                return false;
            }

            string[] items = fullPath.Split(new char[] {'\\', '/'}, StringSplitOptions.RemoveEmptyEntries);

            if (items.Length < 2)
            {
                Debug.Fail("Impossible path!");
                return false;
            }

            fileName = items[items.Length - 1].Trim();
            path = fullPath.Substring(0, fullPath.Length - items[items.Length - 1].Length);
            return true;
        }

        #endregion
    }
}