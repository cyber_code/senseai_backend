using System;
using System.Diagnostics;
using System.Text;
using VSSH;

namespace T24TelnetAdapter
{
    internal class SshConnection : TerminalConnection
    {
        #region Private Members

        private VSshBase _Ssh;

        private readonly string _IdentityFile;
        private readonly string _UserName;
        private readonly string _Password;
        private int _Timeout;
        private readonly bool _JoinTerminalLines;
        private TerminalConnectionType _TerminalConnectionType;

        #endregion

        public SshConnection(string host, int port, string identityFile,
            string userName, string password, int timeout, int pageCode, 
            bool joinTerminalLines, TerminalConnectionType terminalConnectionType)
            : base(host, port, pageCode)
        {
            _UserName = userName;
            _Password = password;
            _IdentityFile = identityFile;
            _Timeout = timeout;
            _JoinTerminalLines = joinTerminalLines;
            _TerminalConnectionType = terminalConnectionType;
        }

        public override TerminalConnectionType ConnectionType
        {
            get { return _TerminalConnectionType; }
        }

        public override bool IsConnected
        {
            get { return _Ssh != null && _Ssh.ShellOpened; }
        }

        public override string ToString()
        {
            return string.Format("Host: {0}:{1} User: {2}/{3}", _Host, _Port, _UserName, (_Password ?? ""));
        }

        protected override Exception EstablishConnection()
        {
            try
            {
                Debug.Assert(_Ssh == null, "Should be not initialized!");

                if (_TerminalConnectionType == TerminalConnectionType.SharpSsh)
                {
                    _Ssh = new VShellSharpSsh(_Host, _UserName);
                    if (_Password != null)
                    {
                        _Ssh.Password = _Password;
                    }
                    if (_IdentityFile != null)
                    {
                        _Ssh.AddIdentityFile(_IdentityFile);
                    }

                    _Ssh.Connect(_Port, _Timeout);
                }
                else
                {
                    _Ssh = new VShellSshNET(_Host, _UserName, _Password);
                    if (_IdentityFile != null)
                    {
                        _Ssh.AddIdentityFile(_IdentityFile);
                    }

                    (_Ssh as VShellSshNET).Connect( _Timeout);
                }

                

                return null;
            }
            catch (Exception ex)
            {
                _Ssh = null;
                return ex;
            }
        }

        protected override byte[] SendCommandText(string commandText)
        {
            return SendCommandText(commandText, false);
        }

        protected override byte[] SendCommandText(string commandText, bool isLastLogoutStep)
        {
            byte[] bytesToSend = Encoding.GetEncoding(PageCode).GetBytes(
                commandText + (_Ssh is VShellSshNET ? "" : "\r")
                );
            _Ssh.Write(bytesToSend, isLastLogoutStep);
            return bytesToSend;
        }

        protected override byte[] ReadAvailableBytes()
        {
            return _Ssh.Receive();
        }

        protected override string GetTextFromResponseBytes(byte[] buff)
        {
            string str = Encoding.GetEncoding(PageCode).GetString(buff, 0, buff.Length);
            return _Ssh.RemoveSSHEscSequences(str, _JoinTerminalLines);
        }

        public override void Disconnect()
        {
            try
            {
                if (_Ssh != null)
                {
                    CloseConnection();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to close SSH connection", ex);
            }
            finally
            {
                _Ssh = null;
            }
        }

        private void CloseConnection()
        {
            Log.Info("Closing SSH connection...");
            try
            {
                _Ssh.Close();
                Log.Info("SSH connection closed");
            }
            catch (Exception ex)
            {
                Log.Error("Failed to close SSH connection", ex);
            }
        }
    }
}