﻿using System.Collections.Generic;
using AXMLEngine;
using ValidataCommon;

namespace T24TelnetAdapter
{
    public class TypicalFieldsAnalyzer
    {
        public static FieldInfoList DeduceApplicationFieldsFromFieldNames(IEnumerable<string> attributeNames)
        {
            SortedDictionary<string, bool> normalizedAttributesNames = GetNormalizedAttributeNames(attributeNames);
            return BuildFieldInfosFromNormalizedNames(normalizedAttributesNames);
        }

        public static FieldInfoList DeduceApplicationFieldsFromInstance(Instance instance)
        {
            string[] attributeNames = instance.GetAttributeNames();
            return DeduceApplicationFieldsFromFieldNames(attributeNames);
        }

        public static SortedDictionary<string, /* isMultivalue */ bool> GetNormalizedAttributeNames(IEnumerable<string> attributeNames)
        {
            SortedDictionary<string, bool> attributes = new SortedDictionary<string, bool>();

            foreach (string attrName in attributeNames)
            {
                string shortAttrName = AttributeNameParser.GetShortFieldName(attrName);

                if (attributes.ContainsKey(shortAttrName))
                {
                    continue;
                }

                attributes.Add(shortAttrName, attrName != shortAttrName);
            }

            return attributes;
        }

        private static FieldInfoList BuildFieldInfosFromNormalizedNames(SortedDictionary<string, bool> normalizedAttributesNames)
        {
            FieldInfoList fieldInfos = new FieldInfoList();
            foreach (KeyValuePair<string, bool> attr in normalizedAttributesNames)
            {
                FieldInfo field = new FieldInfo();
                
                field.FieldName = attr.Key;
                field.HasMultiValues = attr.Value;
                fieldInfos.Add(field);
                field.IsSynchronizable = true;
            }

            MarkSystemFieldsAsNonSychronizable(fieldInfos, RequestManager.RecordsSystemFields);

            return fieldInfos;
        }

        public static void MarkSystemFieldsAsNonSychronizable(IEnumerable<FieldInfo> fieldInfo, string[] systemFields)
        {
            foreach (FieldInfo field in fieldInfo)
            {
                if (StringHelper.IsStringInArray(field.FieldName, systemFields))
                    field.IsSynchronizable = false;
            }
        }
    }
}