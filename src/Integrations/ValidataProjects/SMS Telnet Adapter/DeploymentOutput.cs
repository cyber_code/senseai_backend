﻿using System;
using System.Collections.Generic;
using AXMLEngine;
using ValidataCommon;

namespace T24TelnetAdapter
{
    [Serializable]
    public class DeploymentOutput
    {
        [NonSerialized()] 
        
        public DeploymentRecord Record;

        public string PackName { set; get; }

        public string UnitName { set; get; }

        public string UnitVersion { set; get; }

        public DeploymentOutcome Outcome { set; get; }

        public string Details { set; get; }

        public DeploymentTypes DeploymentType { set; get; }

        public DeploymentMethod Method { set; get; }
        
        #region Only for files

        public bool IsTargetCatalog { get; set; }

        public string TargetDirectory { get; set; }

        #endregion

        #region Only for records

        public string TypicalName { set; get; }
        public FieldInfoList ApplicationFields { set; get; }
        public UnitTypes UnitType { set; get; }
        public string DeploymentVersionName { set; get; }
        public string CompanyName { set; get; }

        public Instance InitialInstanceOnT24 { set; get; }
        public Instance DeployableInstanceToT24 { set; get; }
        public Instance UpdatedInstanceOnT24 { set; get; }

        public List<DeploymentFieldValues> FieldsUpdatedInRecord { set; get; }

        public bool DoesNotAffectDeploymentStatus { get; set; }

        public bool IsSuccessful
        {
            get { return Outcome == DeploymentOutcome.Successful; }
        }

        public bool IsSkipped
        {
            get { return Outcome == DeploymentOutcome.Skipped; }
        }

        public bool IsSkippedProblematically
        {
            get { return Outcome == DeploymentOutcome.Skipped && !  DoesNotAffectDeploymentStatus; }
        }

        public bool IsFailed
        {
            get { return Outcome == DeploymentOutcome.Failed; }
        }

        public bool IsAffectedRecord { get; set; }

        public string FullRecordName
        {
            get
            {
                string recordName = string.IsNullOrEmpty(UnitName) ? "<?>" : UnitName;
                return string.Format("[{0}] {1}", TypicalName, recordName);
            }
        }

        #endregion
    }

    public enum DeploymentTypes
    {
        Toolbox,
        Direct,
        BuildControl
    }


    public enum UnitTypes
    {
        Record,
        SourceFile
    }
}