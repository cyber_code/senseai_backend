namespace T24TelnetAdapter
{
    public enum ComparisonType
    {
        AllFields,
        SkipSystemFields,
        ListedFieldsOnly
    }
}