using System.Collections.Generic;
using AXMLEngine;
using ValidataCommon;

namespace T24TelnetAdapter
{
    /// <summary>
    /// 
    /// </summary>
    public class DeploymentRecord
    {
        /// <summary>
        /// The deployment package of the record
        /// </summary>
        public string DeploymentPack;

        public bool UseReplaceOption = true;

        /// <summary>
        /// The record to deploy
        /// </summary>
        public Instance RecordInstance;

        /// <summary>
        /// A record with the same @ID that exists on the T24 environment 
        /// </summary>
        public Instance ExistingRecordInstance;

        /// <summary>
        /// If an error has occured while retrieving the instance, this is where it is stored
        /// </summary>
        public string ExistingRecordRetrievalError;
        
        /// <summary>
        /// The version definition used for the deployment
        /// </summary>
        public VersionDefinition DeploymentVersion;

        /// <summary>
        /// The user friendly record name
        /// </summary>
        public string RecordName;

        /// <summary>
        /// The SAS version number of the record
        /// </summary>
        public string RecordVersion;

        /// <summary>
        /// Gets the record @ID.
        /// </summary>
        /// <value>The record ID.</value>
        public string RecordID
        {
            get { return RecordInstance == null ? "" : RecordInstance.GetAttributeValue("@ID"); }
        }

        /// <summary>
        /// The Company of the record
        /// </summary>
        public string Company;

        public DeploymentMethod DeploymentMethod;

        public string TypicalName
        {
            get { return RecordInstance.TypicalName; }
        }

        public string CatalogName
        {
            get { return RecordInstance.Catalog; }
        }

        public string FullVersionName
        {
            get { return DeploymentVersion == null ? TypicalName : DeploymentVersion.FullName; }
        }

        public string VersionName
        {
            get
            {
                if (DeploymentVersion == null)
                {
                    return "";
                }

                return DeploymentVersion.VersionName ?? "";
            }
        }

        public bool HasSpecifiedVersion
        {
            get
            {
                if (DeploymentVersion == null)
                {
                    return false;
                }

                return !string.IsNullOrEmpty(DeploymentVersion.VersionName);
            }
        }

        public bool? SkipValueDeletions;

        public bool? InputSameValues;

        public bool SkipChecksForAuthorization;

        private readonly List<Instance> _AffectedRecords = new List<Instance>();

        public List<Instance> AffectedRecords
        {
            get
            {
                return _AffectedRecords;
            }
        }

        public bool IgnoreSpecialRoutine = false;
        public bool AllowDifferentResponseID  = false;
    }
}