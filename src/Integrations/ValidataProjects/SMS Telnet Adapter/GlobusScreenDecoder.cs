﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T24TelnetAdapter
{

    public class GlobusScreenDecoder
    {
        public const char ESC = (char)27;

        const int GLOBUS_SCREEN_HEIGHT = 26;
        const int GLOBUS_SCREEN_WIDTH = 133;


        public static string SymbolsToStrings(string terminalLine, Encoding enc)
        {
            return SymbolsToStrings(terminalLine, enc, GLOBUS_SCREEN_WIDTH, GLOBUS_SCREEN_HEIGHT);
        }

        private static string SymbolsToStrings(string terminalLine, Encoding enc, int scWidth, int scHeight)
        {
            if (!terminalLine.Contains(ESC))
            {
                return terminalLine;
            }


            return terminalLine.Trim();

        }
    }
}
