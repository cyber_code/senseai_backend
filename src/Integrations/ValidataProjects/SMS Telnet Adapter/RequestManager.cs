﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using AXMLEngine;
using OFSCommonMethods;
using OFSCommonMethods.IO.Telnet;
using OFSCommonMethods.OFSEnquiry;
using OFSStringConvertor;
using SCApiInterface;
using OFSCommonMethods.IO;
using Validata.Common;
using ValidataCommon;
using ValidataFtp;
using Logger = Common.Logger;
using OFSCommonMethods.IO.HttpRestService;
using System.IO;
using System.Threading;
using Common;

namespace T24TelnetAdapter
{
    #region Delegates and Event Classes

    public class CommandExecutionEventArgs : EventArgs
    {
        public enum Action
        {
            Abort = 0,
            Retry,
            Ignore
        }

        public readonly string Message;
        public readonly string Section;
        public Action ErrorTreatAction;

        internal CommandExecutionEventArgs(string section, string message)
        {
            Section = section;
            Message = message;
            ErrorTreatAction = Action.Abort;
        }
    }

    public enum ExecuteResult
    {
        Success,
        Error,
        Ignored,
        Skipped
    }

    public delegate void CommandExecutionError(object sender, CommandExecutionEventArgs args);

    /// <summary>
    /// Used for functions in UI that update status info
    /// </summary>
    /// <param name="statusMessage">The actual message that is to be displayed in UI</param>
    public delegate bool UpdateStatusDelegate(string statusMessage);

    #endregion

    /// <summary>
    /// Manages T24 Request using all kinds of communication means
    /// </summary>
    public class RequestManager : IShellCommunicator
    {
        #region Enums

        [Flags]
        public enum EnvironmentFolders
        {
            None = 0,
            GlobusSystem = 1,
            Development = 2,
            All = GlobusSystem | Development,
        }

        public enum OFSResponseErrorType
        {
            Success = 0,

            UnableToExecute,

            SecurityViolation,

            NoRecords,

            UnexpectedOFS
        }


        #endregion

        #region Constants


        public const string NoVersionReservedName = "[No Version]";

        /// <summary>
        /// Carriage Return (ASCII 13)
        /// </summary>
        private const byte CR = 13;

        /// <summary>
        /// Line Feed (ASCII 10)
        /// </summary>
        private const byte LF = 10;

        public static readonly string[] RecordsSystemFields = {
                                                  "INPUTTER",
                                                  "AUTHORISER",
                                                  "OVERRIDE",
                                                  "DATE.TIME",
                                                  "RECORD.STATUS",
                                                  "CURR.NO",
                                                  "DEPT.CODE",
                                                  "AUDITOR.CODE",
                                                  "AUDIT.DATE.TIME",
                                                  "CO.CODE", // TODO probably should be taken in account in comparison
                                              };

        private const string BackupFolderPrefix = "SAS";

        private const string BackupFolderDateFormat = "yyMMdd";

        private const string SELECTION_PROMPT = ">";

        #endregion

        #region Private Members

        private readonly OfsConnector _OfsConnector;
        private readonly JShellConnector _JShellConnector;
        private readonly DBShellConnector _DbShellConnector;
        private readonly SystemShellConnector _SystemShellConnector;
        private readonly GlobusClassicConnector _GlobusClassicConnector;
        private readonly HttpWebServiceConnector _HttpWebServiceConnector;
        private readonly HttpRestServiceConnector _HttpRestServiceConnector;

        private readonly OperatingSystem _OS;

        private readonly char _PathDelimiter;

        private readonly Settings _Settings;

        private List<CustomCommandResult> CustomCommandResults
        {
            get { return _Settings.CommandResults.Results; }
        }

        private string _OFSReplaceText = "";

        private string _BackupFolder = "";

        private string _JLibDir = "";

        private string _JBinDir = "";

        private string _JGlobusLibDir = "";

        private string _JGlobusBinDir = "";

        private string _JGlobusBPDir = "";

        private string _HomeDir;

        private static readonly Regex OfsNewStatementRegex = new Regex(@"(^\s*[A-Za-z][\w.\d]+:\d+:\d+=.*$)");

        private bool _RecordDeploymentPreventDeletionDefault;

        private bool _RecordDeploymentInputUnchangedDefault;

        internal Common.Logger Log
        {
            get
            {
                return Loggers.GetLogger(_TypeOfLogger);
            }
        }

        #endregion

        #region Public Properties

        public ConsoleType LatestConsoleType;

        public LoggerType TypeOfLogger
        {
            get { return _TypeOfLogger; }
            set
            {
                _TypeOfLogger = value;
                setConnectorsLogs();
            }
        }

        private LoggerType _TypeOfLogger;

        public string FtpWorkingDirectory
        {
            get
            {
                string dir;
                if (_Settings == null)
                {
                    Debug.Fail("The settings are empty");
                    dir = null;
                }
                else
                {
                    dir = _Settings.FtpHome;
                }

                return dir ?? ""; // TODO should it be emty string or something else
            }
        }


        public Settings Settings
        {
            get { return _Settings; }
        }

        public OperatingSystem OS
        {
            get { return _OS; }
        }

        public bool IsCalledByDllAdapter;

        public string HomeDir
        {
            get { return _HomeDir; }
            internal set { _HomeDir = value; }
        }

        public string ExportName
        {
            get
            {
                return _Settings.Exportname;
            }
        }

        public bool DeployAllBinaryFiles
        {
            get
            {
                return _Settings.DeployAllBinaryFiles;
            }
        }

        public string BackupFolder
        {
            get { return _BackupFolder ?? ""; }
            internal set { _BackupFolder = value; }
        }

        public SystemShellConnector SystemShellConnector
        {
            get { return _SystemShellConnector; }
        }

        public GlobusClassicConnector GlobusClassicConnector
        {
            get { return _GlobusClassicConnector; }
        }

        public JShellConnector JShellConnector
        {
            get { return _JShellConnector; }
        }

        public DBShellConnector DbShellConnector
        {
            get { return _DbShellConnector; }
        }

        public OfsConnector OfsConnector
        {
            get { return _OfsConnector; }
        }

        public HttpWebServiceConnector HttpWebServiceConnector
        {
            get { return _HttpWebServiceConnector; }
        }

        public string FullJLibDir
        {
            get { return GetFullPath(_JLibDir); }
        }

        public string FullJBinDir
        {
            get { return GetFullPath(_JBinDir); }
        }

        public string FullJGlobusLibDir
        {
            get { return GetFullPath(_JGlobusLibDir); }
        }

        public string FullJGlobusBinDir
        {
            get { return GetFullPath(_JGlobusBinDir); }
        }

        public string FullObjectDevDir
        {
            get { return FullJLibDir + _PathDelimiter + _Settings.Objectdir; }
        }

        public string FullJGlobusBPDir
        {
            get { return GetFullPath(_JGlobusBPDir); }
        }

        public string GlobusBPCatalogName
        {
            get
            {
                string temp = _JGlobusBPDir;
                if (temp.EndsWith("\\") || temp.EndsWith("/"))
                {
                    temp = temp.Substring(0, temp.Length - 1);
                }
                int pos1 = _JGlobusBPDir.LastIndexOf("\\");
                int pos2 = _JGlobusBPDir.LastIndexOf("/");
                pos1 = pos1 > pos2 ? pos1 : pos2;
                temp = temp.Substring(pos1 + 1);

                return temp;
            }
        }

        public string FullObjectGlobusDir
        {
            get { return FullJGlobusLibDir + _PathDelimiter + _Settings.Objectdir; }
        }

        public string FullBackupFolderPath
        {
            get { return FtpPath.BuildX(HomeDir, BackupFolder, FtpLogsSubdir); }
        }

        public char PathDelimiter
        {
            get { return _PathDelimiter; }
        }

        public bool IsUniVerse
        {
            get { return (string.Compare(_Settings.JSh, "UniVerse", true) == 0); }
        }

        public bool AbortDeployment { get; set; }

        public CustomAttributeAppenderHandler OFSParser_CustomAttributeAppender;
        public string FtpLogsSubdir;

        #endregion

        #region Events

        public event CommEvent OnReceive;
        public event CommEvent OnSend;
        public event CommandExecutionError OnCommandExecutionError;
        public event Logger.LogEventHandle OnLogEvent;
        //public event Loggers.ActionsLogger.LogEventHandle OnLogEvent;


        #endregion

        #region Class Lifecycle

        private RequestManager(OperatingSystem operatingSystem,
                               Settings settings,
                               string inputVersion,
                               string serverName,
                               string serverUser,
                               string serverPassword,
                               bool validataOfsTags,
                               string ftpLogsSubdir,
                               bool initializeConnections,
                               string uniqueID,
                               LoggerType logType)

        {
            _TypeOfLogger = logType;
            FtpLogsSubdir = ftpLogsSubdir;
            _Settings = settings;
            _Settings.UniqueID = uniqueID;

            _OS = operatingSystem;
            _PathDelimiter = (operatingSystem == OperatingSystem.AIX) ? '/' : '\\';

            _OFSReplaceText = _Settings.OFSReplaceText ?? "////1";

            _JLibDir = _Settings.JBCDEV_LIB ?? "";
            if (_JLibDir.Length > 0)
            {
                Log.Debug("JBCDEV_LIB from telnet settings will be used. Value = " + _JLibDir);
            }

            _JBinDir = _Settings.JBCDEV_BIN ?? "";
            if (_JBinDir.Length > 0)
            {
                Log.Debug("JBCDEV_BIN from telnet settings will be used. Value = " + _JBinDir);
            }

            _JGlobusBinDir = _Settings.PathGlobusBin ?? "";
            if (_JGlobusBinDir.Length > 0)
            {
                Log.Debug("GlobusBinDir from telnet settings will be used. Value = " + _JGlobusBinDir);
            }

            _JGlobusLibDir = _Settings.PathGlobusLib ?? "";
            if (_JGlobusLibDir.Length > 0)
            {
                Log.Debug("GlobusLibDir from telnet settings will be used. Value = " + _JGlobusLibDir);
            }


            _JGlobusBPDir = _Settings.PathGlobusBP ?? "";
            if (_JGlobusBPDir.Length > 0)
            {
                Log.Debug("GlobusBPDir from telnet settings will be used. Value = " + _JGlobusBPDir);
            }

            if (validataOfsTags)
            {
                if (string.IsNullOrEmpty(_Settings.OFSRequestStart) ||
                    string.IsNullOrEmpty(_Settings.OFSRequestEnd) ||
                    string.IsNullOrEmpty(_Settings.OFSResponseStart) ||
                    string.IsNullOrEmpty(_Settings.OFSResponseEnd))
                {
                    throw new Exception(
                            "OFSRequestStart, OFSRequestEnd, OFSResponseStart, OFSResponseEnd values should be defined!\r\n" +
                            "Please, fill them in \"TelnetCommDef.xml\" file for Telnet Set: \"" + _Settings.PhantomSet + "\"."
                        );
                }
            }

            if (!string.IsNullOrEmpty(serverName))
            {
                _Settings.GlobusServer = serverName;
            }
            if (!string.IsNullOrEmpty(serverUser))
            {
                _Settings.GlobusUserName = serverUser;
            }
            if (!string.IsNullOrEmpty(serverPassword))
            {
                _Settings.GlobusPassword = serverPassword;
            }
            if (!string.IsNullOrEmpty(inputVersion))
            {
                _Settings.Version = inputVersion;
            }

            if (initializeConnections)
            {
                if (_Settings.DBConnection != null)
                {
                    _DbShellConnector = TelnetCommPool.GetDBShellConnector(_Settings);
                    AttachToSendReceive(_DbShellConnector);
                }

                _JShellConnector = TelnetCommPool.GetJShellConnector(_Settings);
                AttachToSendReceive(_JShellConnector);

                _SystemShellConnector = TelnetCommPool.GetSystemShellConnector(_Settings);
                AttachToSendReceive(_SystemShellConnector);

                _OfsConnector = TelnetCommPool.GetOfsConnector(_Settings);
                AttachToSendReceive(_OfsConnector);

                _GlobusClassicConnector = TelnetCommPool.GetGlobusClassicConnector(_Settings);

                AttachToSendReceive(_GlobusClassicConnector);

                _HttpWebServiceConnector = TelnetCommPool.GetHttpWebServiceConnector(_Settings);
                if (_HttpWebServiceConnector != null)
                {
                    AttachToSendReceive(_HttpWebServiceConnector);
                }

                _HttpRestServiceConnector = TelnetCommPool.GetHttpRestServiceConnector(_Settings);
                if (_HttpRestServiceConnector != null)
                    AttachToSendReceive(_HttpRestServiceConnector);

                setConnectorsLogs();

            }

            //Log.OnLogEvent += Log_OnLogEvent;
            Log.OnLogEvent += Log_OnLogEvent;
        }

        private void setConnectorsLogs()
        {
            _JShellConnector.Log = this.Log;
            _SystemShellConnector.Log = this.Log;
            _OfsConnector.Log = this.Log;
            _GlobusClassicConnector.Log = this.Log;

            if (_HttpWebServiceConnector != null)
                _HttpWebServiceConnector.Log = this.Log;

            if (_DbShellConnector != null)
                _DbShellConnector.Log = this.Log;

        }


        // This constructor should be used when an actual unique ID is provided for parallel connections
        public RequestManager(string uniqueID, OperatingSystem operatingSystem,
                                string telnetSetName,
                                string inputVersion,
                                string serverName,
                                string serverUser,
                                string serverPassword,
                                bool validataOfsTags,
                                LoggerType logType = LoggerType.Default
                                ) : this(
                operatingSystem,
                GetSettings(telnetSetName),
                inputVersion,
                serverName,
                serverUser,
                serverPassword,
                validataOfsTags,
                "",
                true,
                uniqueID,
                logType
                )
        { }


        public RequestManager(OperatingSystem operatingSystem,
                                string telnetSetName,
                                string inputVersion,
                                string serverName,
                                string serverUser,
                                string serverPassword,
                                bool validataOfsTags,
                                LoggerType logType = LoggerType.Default) : this(
                operatingSystem,
                GetSettings(telnetSetName),
                inputVersion,
                serverName,
                serverUser,
                serverPassword,
                validataOfsTags, "", true, null, logType
                )
        { }


        public void UnsubscribeAllOnReceiveDelegates(Type listenerType)
        {
            if (OnReceive != null)
            {
                foreach (Delegate d in OnReceive.GetInvocationList())
                {
                    if (listenerType == null
                        || (d.Target != null && d.Target.GetType() == listenerType))
                    {
                        OnReceive -= (CommEvent)d;
                    }
                }
            }
        }

        public void UnsubscribeAllOnSendDelegates(Type listenerType)
        {
            if (OnSend != null)
            {
                foreach (Delegate d in OnSend.GetInvocationList())
                {
                    if (listenerType == null
                        || (d.Target != null && d.Target.GetType() == listenerType))
                    {
                        OnSend -= (CommEvent)d;
                    }
                }
            }
        }

        void Log_OnLogEvent(Logger.LogEventType logEventType, string message)
        {
            if (OnLogEvent != null)
                OnLogEvent(logEventType, message);
        }

        private void AttachToSendReceive(T24TelnetConnector connector)
        {
            connector.OnReceive += new CommEvent(ReceiveHandler);
            connector.OnSend += new CommEvent(SendHandler);
        }

        void SendHandler(string text, byte[] bytes)
        {
            // TODO: add communication type
            if (OnSend != null)
                OnSend(text, bytes);
        }

        void ReceiveHandler(string text, byte[] bytes)
        {
            // TODO: add communication type
            if (OnReceive != null)
                OnReceive(text, bytes);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// To be used, if we want to have several concurrent connection to the same environment with the same user/password
        /// </summary>
        public string UniqueID
        {
            get { return _Settings.UniqueID; }
        }

        public bool LoginToJShell(out string result)
        {
            if (_Settings.Type == Settings.IOType.HttpWebService || _Settings.Type == Settings.IOType.HttpRestService)
            {
                result = "Connected!\n";
                return true;
            }

            try
            {
                Log.Debug("Start login to JShell.");
                if (!_JShellConnector.Login())
                {
                    result = _JShellConnector.LastError;
                    if (string.IsNullOrEmpty(result))
                    {
                        result = "Cannot login to Jshell.\n ";
                    }
                    return false;
                }

                result = "Connected!\n";

                string errorMessage;
                if (!InitializeHomeDir(out errorMessage))
                {
                    result = errorMessage;
                    return false;
                }

                if (!LocateLibBinDir(out errorMessage))
                {
                    result = errorMessage;
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                result = ex.Message;
                return false;
            }
        }

        public void Release(ConsoleType consoleType, bool withRemove = true)
        {
            switch (consoleType)
            {
                case ConsoleType.Classic:
                    ReleaseSafelyConnector(_GlobusClassicConnector, withRemove);
                    break;
                case ConsoleType.JShell:
                    ReleaseSafelyConnector(_JShellConnector, withRemove);
                    break;
                case ConsoleType.DBShell:
                    ReleaseSafelyConnector(_DbShellConnector, withRemove);
                    break;
                case ConsoleType.OFS:
                    ReleaseSafelyConnector(_OfsConnector, withRemove);
                    break;
                case ConsoleType.Shell:
                    ReleaseSafelyConnector(_SystemShellConnector, withRemove);
                    break;
                case ConsoleType.HttpWebService:
                    ReleaseSafelyConnector(_HttpWebServiceConnector, withRemove);
                    break;
                case ConsoleType.HttpRestService:
                    ReleaseSafelyConnector(_HttpRestServiceConnector, withRemove);
                    break;
                default:
                    Debug.Fail("Did you miss to implement something?");
                    break;
            }
        }

        public void Release()
        {
            // TODO we can parallelize this execution (but this can mess up the logging if it is in the same file)
            ReleaseSafelyConnector(_JShellConnector);
            ReleaseSafelyConnector(_SystemShellConnector);
            ReleaseSafelyConnector(_DbShellConnector);
            ReleaseSafelyConnector(_OfsConnector);
            ReleaseSafelyConnector(_GlobusClassicConnector);
            ReleaseSafelyConnector(_HttpWebServiceConnector);
        }

        private static void ReleaseSafelyConnector(T24TelnetConnector connector, bool withRemove = true)
        {
            try
            {
                TelnetCommPool.Release(connector, withRemove);
            }
            catch (Exception ex)
            {
                //Log.Warn("Error when releasing connection", ex);
                Loggers.ActionsLogger.Warn("Error when releasing connection", ex);
            }
        }

        public T24TelnetConnector ActivateOfsConnector()
        {
            if (_Settings.Type == Settings.IOType.HttpWebService)
            {
                return GetActivedConnectorByType(ConsoleType.HttpWebService);
            }
            else if (_Settings.Type == Settings.IOType.HttpRestService)
            {
                return GetActivedConnectorByType(ConsoleType.HttpRestService);
            }

            return GetActivedConnectorByType(ConsoleType.OFS);
        }

        public void TestOfsConnectivity()
        {
            if (_Settings.Type == Settings.IOType.HttpWebService || _Settings.Type == Settings.IOType.HttpRestService)
                return;

            T24TelnetConnector connector = null;

            try
            {
                connector = GetActivedConnectorByType(ConsoleType.OFS);

                if (connector == null)
                {
                    throw new Exception("Unable to establish OFS connection");
                }

                if (!connector.IsConnected)
                {
                    throw new Exception("Unable to establish OFS connection: " + connector.LastError);
                }

                TelnetCommPool.Release(connector);
            }
            finally
            {
                ReleaseSafelyConnector(connector);
            }
        }

        public List<RecordDifferences> CompareRecords(List<DeploymentRecord> records,
                                               ComparisonType comparisonType,
                                               bool skipEmptyFields,
                                               List<string> paramFields,
                                               out string errorText)
        {
            errorText = null;

            List<RecordDifferences> results = new List<RecordDifferences>();

            foreach (DeploymentRecord record in records)
            {
                Dictionary<string, string> sasAttributes = record.RecordInstance.Attributes.AsDictionary;

                RecordDifferences recDiffs = new RecordDifferences(sasAttributes["@ID"], record.DeploymentPack, "Record", record.RecordVersion);

                string lastResponseLine;
                Queue<string> res;

                #region Send OFS message

                string command = record.RecordInstance.TypicalName + ",/S/PROCESS," + _Settings.InputUser + "/" + _Settings.InputPassword + "," + GlobusRequestBase.GetEscapedID(sasAttributes["@ID"]);
                if (RunOfsCommand(command, out lastResponseLine, out errorText, -1) == ExecuteResult.Error)
                {
                    recDiffs.ErrorText = errorText;
                }
                else
                {
                    // TODO we should better reuse OFSResponseReader.ReadOFS()

                    bool found = false;
                    string[] ofsOriginalStatements = lastResponseLine.Split(',');

                    bool ofsSuccess = false;
                    string[] slashSlash = { "//" };
                    string[] ofsSplit = lastResponseLine.Split(slashSlash, StringSplitOptions.None);
                    if (ofsSplit.Length >= 2)
                    {
                        if (ofsSplit[1].StartsWith("1"))
                            ofsSuccess = true;
                    }

                    List<String> ofsParsedStatements = GetParsedStatements(ofsOriginalStatements);

                    if (ofsParsedStatements.Count > 1)
                    {
                        if (ofsSuccess)
                        {
                            found = true;

                            foreach (string sasFieldName in sasAttributes.Keys)
                            {
                                if (sasFieldName == "FinancialObjects")
                                {
                                    // TODO what is this used for ???
                                    continue;
                                }

                                if (sasFieldName == "ID")
                                {
                                    continue;
                                }

                                if (comparisonType == ComparisonType.SkipSystemFields)
                                {
                                    if (RecordsSystemFields.Contains(AttributeNameParser.GetShortFieldName(sasFieldName)))
                                    {
                                        continue;
                                    }
                                }
                                else if (comparisonType == ComparisonType.ListedFieldsOnly)
                                {
                                    if (!paramFields.Contains(AttributeNameParser.GetShortFieldName(sasFieldName)))
                                    {
                                        continue;
                                    }
                                }

                                if (skipEmptyFields)
                                {
                                    if (sasAttributes[sasFieldName] == "No Value")
                                    {
                                        continue;
                                    }
                                }

                                if (sasFieldName == "@ID")
                                {
                                    continue;
                                }

                                string t24value;
                                if (!IsMatched(sasFieldName, sasAttributes[sasFieldName], ofsParsedStatements.ToArray(), out t24value))
                                {
                                    recDiffs.AddDifference(sasFieldName, sasAttributes[sasFieldName], t24value);
                                }
                            }
                        }
                        else
                        {
                            recDiffs.ErrorText = lastResponseLine;
                        }

                        #endregion
                    }

                    if (!found)
                    {
                        recDiffs.ErrorText = lastResponseLine;
                    }
                }

                results.Add(recDiffs);
            }

            return results;
        }

        public List<RecordDifferences> CompareFiles(List<DeployableFile> filesToCompare, out string errorText, out bool skipChecksum)
        {
            errorText = "";
            List<RecordDifferences> recDiffs = new List<RecordDifferences>();

            skipChecksum = _Settings.SkipCheckSum;
            if (skipChecksum)
            {
                return recDiffs;
            }

            foreach (DeployableFile file in filesToCompare)
            {
                IFile iFile = file.IFile;
                RecordDifferences recordDifference = new RecordDifferences(iFile.RelationalNameFull, file.DeploymentPack, "File", iFile.Version);
                string folder = "";
                if (file.ServerDeploymentFolder != "")
                {
                    folder = file.ServerDeploymentFolder.
                                 Replace('\\', _PathDelimiter).
                                 Replace('/', _PathDelimiter) +
                             _PathDelimiter;
                    //folder = folder.TrimStart(new char[] { '/', '\\' });
                }
                string csEnv;

                if (!GetChecksum(folder + iFile.Name, out csEnv, out errorText, out skipChecksum))
                {
                    if (!string.IsNullOrEmpty(errorText))
                    {
                        recordDifference.ErrorText = errorText;
                    }
                    else
                    {
                        recordDifference.ErrorText = "File not found: " + folder + iFile.Name;
                    }
                }
                else
                {
                    MD5HashManager hm = new MD5HashManager();

                    byte[] hashBytes = new byte[iFile.Data.ToBinaryArray().Length + 2];
                    /*if (iFile.ContentType == ContentType.Binary)
                    {
                        hashBytes = iFile.Data.ToBinaryArray();
                    }*/
                    //To match the way the file was uploaded
                    byte[] iFileBytes = iFile.Data.ToBinaryArray();
                    if (iFile.ContentType != ContentType.Binary && file.DeploymentType == DeploymentType.Direct)
                    {
                        if (iFileBytes.Length >= 2)
                            if (iFileBytes[iFileBytes.Length - 1] != LF || iFileBytes[iFileBytes.Length - 2] != CR)
                            {
                                int cc = 0;
                                foreach (byte b in iFileBytes)
                                {
                                    hashBytes[cc] = b;
                                    cc++;
                                }
                                hashBytes[hashBytes.Length - 2] = CR;
                                hashBytes[hashBytes.Length - 1] = LF;
                            }
                    }
                    else
                    {
                        hashBytes = iFileBytes;
                    }


                    //Normal hash
                    string csSAS = hm.ComputeFileHash(hashBytes);

                    //Remove CR's and compute hash for AIX compatibility
                    string csSASNOCR = hm.RemoveCRComputeFileHash(hashBytes);

                    //Add CRLF to every line before computing hash, for Temenos Toolbox compatibility 
                    //(Deployed on WIN)
                    string csSASCRLF = hm.AddCRLFComputeFileHash(iFileBytes, false);

                    //Add LF and remove CR to every line, before computing hash, for Temenos Toolbox compatibility 
                    //(Deployed on AIX)
                    // Not tested yet
                    //csSASLF = hm.AddCRLFComputeFileHash(iFile.Data.ToBinaryArray(),true);

                    if (csSASCRLF != csEnv && csEnv != csSAS && csEnv != csSASNOCR)
                    {
                        recordDifference.AddDifference("CHECKSUM", csSAS, csEnv);
                    }
                }

                recDiffs.Add(recordDifference);
            }

            return recDiffs;
        }

        public DeploymentOutput DeployRecord(DeploymentRecord record, bool inputUnchangedValues, bool preventValueDeletions)
        {
            List<DeploymentOutput> outputs = DeployRecords(new List<DeploymentRecord> { record }, inputUnchangedValues, preventValueDeletions);
            return outputs[0];
        }

        public List<DeploymentOutput> DeployRecords(List<DeploymentRecord> recordsList, bool inputUnchangedValues, bool preventValueDeletions, UpdateStatusDelegate setStatus, string Version = null)
        {
            _RecordDeploymentInputUnchangedDefault = inputUnchangedValues;
            _RecordDeploymentPreventDeletionDefault = preventValueDeletions;

            List<DeploymentOutput> deploymentResults = new List<DeploymentOutput>();
            int currentRec = 1;
            foreach (DeploymentRecord dRecord in recordsList)
            {
                if (setStatus != null)
                {
                    bool shouldResume = setStatus(string.Format("Deploying record {0}:{1} [{2}/{3}] ...", dRecord.TypicalName, dRecord.RecordID, currentRec++, recordsList.Count));
                    if (!shouldResume)
                    {
                        break;
                    }
                }

                DeploymentOutput result = DeploySingleRecord(dRecord, Version);
                deploymentResults.Add(result);
            }

            return deploymentResults;
        }

        public List<DeploymentOutput> DeployRecords(List<DeploymentRecord> recordsList, bool inputUnchangedValues, bool preventValueDeletions, string Version = null)
        {
            return DeployRecords(recordsList, inputUnchangedValues, preventValueDeletions, null, Version);
        }

        public Instance GetRecordDataByID(MetadataTypical typical, string id, bool readAttributesNotInTypical, out OFSResponseErrorType errorType, out string errorText, string _Version = null)
        {
            return GetRecordDataByCompanyAndID(typical, null, id, readAttributesNotInTypical, out errorType, out errorText, _Version);
        }

        public Instance GetRecordDataByCompanyAndID(MetadataTypical typical, string company, string id, bool readAttributesNotInTypical, out OFSResponseErrorType errorType, out string errorText, string _Version = null)
        {
            if (typical.Name.Contains(","))
            {
                throw new ArgumentException("Invalid typical name containing comma: " + typical.Name);
            }

            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("Invalid attempt to fetch record data without specifying @ID");
            }

            if (id.StartsWith("@"))
            {
                id = id.Substring(1);
            }

            if (string.IsNullOrEmpty(company))
            {
                company = _Settings.Companies.GetCompany(typical.Name);
            }
            string version = (_Version != null) ? _Version : ",";
            if (!version.StartsWith(","))
                version = "," + version;
            string seeCommand =
                typical.Name + version +
                "/S/PROCESS," +
                _Settings.InputUser + "/" + _Settings.InputPassword +
                (string.IsNullOrEmpty(company) ? "" : "/" + company) +
                "," + GlobusRequestBase.GetEscapedID(id);

            return getRecordByOFS(seeCommand, typical, id, out errorType, out errorText);
        }

        public Instance GetRecordDataByProvidedOFS(string seeCommand, bool readAttributesNotInTypical, out OFSResponseErrorType errorType, out string errorText)
        {
            if (string.IsNullOrEmpty(seeCommand))
            {
                errorType = OFSResponseErrorType.UnableToExecute;
                errorText = "Null or empty input OFS message";
                return null;
            }
            var parts = seeCommand.Split(new char[] { ',' });
            if (parts.Length < 4)
            {
                errorType = OFSResponseErrorType.UnableToExecute;
                errorText = "OFS message should contain 4 parts separated by \',\'. Could not parse typical and @ID.";
                return null;
            }

            MetadataTypical typical = new MetadataTypical() { Name = parts[0], BaseClass = "T24_DataProcess", CatalogName = "Dummy4" };
            string id = parts[3];

            return getRecordByOFS(seeCommand, typical, id, out errorType, out errorText);
        }

        private Instance getRecordByOFS(string seeCommand, MetadataTypical typical, string id, out OFSResponseErrorType errorType, out string errorText)
        {
            string ofsResponseLine;
            //Queue<string> res;
            if (RunOfsCommand(seeCommand, out ofsResponseLine, out errorText, -1) == ExecuteResult.Error)
            {
                errorType = OFSResponseErrorType.UnableToExecute;
                return null;
            }

            if (ofsResponseLine == "SECURITY.VIOLATION" || ofsResponseLine == "SECURITY VIOLATION")
            {
                errorText = ofsResponseLine;
                errorType = OFSResponseErrorType.SecurityViolation;
                return null;
            }

            OFSResponseReader responseReader = new OFSResponseReader
            {
                CustomAttributeAppender = OFSParser_CustomAttributeAppender
            };

            string vxml = responseReader.ReadOFS(new GlobusMessage.OFSMessage(ofsResponseLine), typical, false, id);
            if (string.IsNullOrEmpty(vxml) || vxml.EndsWith("@ID:1:1=RECORD MISSING"))
            {
                errorText = "No record found with the specified ID"; // TODO better message
                errorType = OFSResponseErrorType.NoRecords;
                return null;
            }

            try
            {
                Instance instance = Instance.FromXmlString(vxml);
                errorType = OFSResponseErrorType.Success;
                return instance;
            }
            catch (XmlException)
            {
                errorText = string.Format("Unexpected OFS response when retrieving {0} @ID={1}\r\n{2}", typical.Name, id, ofsResponseLine);
                errorType = OFSResponseErrorType.UnexpectedOFS;
                return null;
            }
        }

        public void DeleteRecordByID(string typicalName, string id, out string errorText)
        {
            DeleteRecordByCompanyAndID(typicalName, null, id, out errorText);
        }

        public void DeleteRecordByCompanyAndID(string typicalName, string company, string id, out string errorText)
        {
            if (typicalName.Contains(","))
            {
                throw new ArgumentException("Invalid typical name containing comma: " + typicalName);
            }

            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("Invalid attempt to delete record without specifying @ID");
            }

            if (id.StartsWith("@"))
            {
                id = id.Substring(1);
            }

            if (string.IsNullOrEmpty(company))
            {
                company = _Settings.Companies.GetCompany(typicalName);
            }

            string deleteCommand =
                typicalName +
                ",/D/PROCESS," +
                _Settings.InputUser + "/" + _Settings.InputPassword +
                (string.IsNullOrEmpty(company) ? "" : "/" + company) +
                "," + GlobusRequestBase.GetEscapedID(id);

            string ofsResponseLine;
            //Queue<string> res;
            if (RunOfsCommand(deleteCommand, out ofsResponseLine, out errorText, -1) == ExecuteResult.Error)
            {
                return;
            }

            // Process OFS response -> might contain errors
            if (ofsResponseLine.Contains("UNAUTH. RECORD MISSING"))
            {
                errorText = string.Format("There is no unauthorised {0} record with @ID = {1}", typicalName, id);
            }
        }

        public Instance GetStandardSelection(string applicationName, out string errorMesssage)
        {
            MetadataTypical metadata = new MetadataTypical();
            metadata.CatalogName = "Dummy";
            metadata.Name = "STANDARD.SELECTION";

            OFSResponseErrorType errorType;
            Instance instance = GetRecordDataByID(metadata, applicationName, true, out errorType, out errorMesssage);

            return instance;
        }

        public string[] GetResultsFromSingleFieldEnquiry(string enquiryName, out string errorText)
        {
            return GetResultsFromSingleFieldEnquiry(enquiryName, "Name", true, out errorText);
        }

        public string[] GetResultsFromSingleFieldEnquiry(string enquiryName, string fieldName, bool sortByFieldValue, out string errorText)
        {
            return GetResultsFromSingleFieldEnquiry(enquiryName, fieldName, sortByFieldValue, "", out errorText);
        }

        public string[] GetResultsFromSingleFieldEnquiry(string enquiryName, int resultColumnIndex, bool sortByFieldValue, IEnumerable<Filter> filters, out string errorText)
        {
            MetadataTypical typical = new MetadataTypical();
            typical.CatalogName = "Dummy";
            typical.Name = enquiryName;

            // create fake columns equal to requested column index
            for (int i = 1; i <= resultColumnIndex; i++)
            {
                typical.AddAttribute(new TypicalAttribute(i.ToString(), AttributeDataType.String, "", 0));
            }

            InstanceCollection instances = GetResultsFromEnquiry(typical, enquiryName, filters, out errorText);
            if (instances == null)
            {
                return new string[0];
            }

            string fieldName = resultColumnIndex.ToString();

            List<string> results = new List<string>(instances.Count);
            foreach (Instance instance in instances)
            {
                string value = instance.GetAttributeValue(fieldName);
                if (value.Length > 0)
                {
                    results.Add(value);
                }
            }

            if (sortByFieldValue)
            {
                results.Sort();
            }

            return results.ToArray();
        }

        public string[] GetResultsFromSingleFieldEnquiryWithCompany(string Company, string enquiryName, int resultColumnIndex, bool sortByFieldValue, IEnumerable<Filter> filters, out string errorText)
        {
            MetadataTypical typical = new MetadataTypical();
            typical.CatalogName = "Dummy";
            typical.Name = enquiryName;

            // create fake columns equal to requested column index
            for (int i = 1; i <= resultColumnIndex; i++)
            {
                typical.AddAttribute(new TypicalAttribute(i.ToString(), AttributeDataType.String, "", 0));
            }

            InstanceCollection instances = GetResultsFromEnquiryWithCompany(typical, Company, enquiryName, filters, out errorText);
            if (instances == null)
            {
                return new string[0];
            }

            string fieldName = resultColumnIndex.ToString();

            List<string> results = new List<string>(instances.Count);
            foreach (Instance instance in instances)
            {
                string value = instance.GetAttributeValue(fieldName);
                if (value.Length > 0)
                {
                    results.Add(value);
                }
            }

            if (sortByFieldValue)
            {
                results.Sort();
            }

            return results.ToArray();
        }

        public string[] GetResultsFromSingleFieldEnquiry(string enquiryName, string fieldName, bool sortByFieldValue, IEnumerable<Filter> filters, out string errorText)
        {
            return GetResultsFromSingleFieldEnquiry(enquiryName, fieldName, sortByFieldValue, PrepareEnquiryFiltersOfsText(filters), out errorText);
        }

        public string[] GetResultsFromSingleFieldEnquiry(string enquiryName, string fieldName, bool sortByFieldValue, string filtersText, out string errorText)
        {
            MetadataTypical typical = new MetadataTypical();
            typical.CatalogName = "Dummy";
            typical.Name = enquiryName;
            typical.AddAttribute(new TypicalAttribute(fieldName, AttributeDataType.String, "", 0));

            InstanceCollection instances = GetResultsFromEnquiry(typical, enquiryName, filtersText, out errorText);
            if (instances == null)
            {
                return new string[0];
            }

            List<string> results = new List<string>(instances.Count);
            foreach (Instance instance in instances)
            {
                string value = instance.GetAttributeValue(fieldName);
                if (value.Length > 0)
                {
                    results.Add(value);
                }
            }

            if (sortByFieldValue)
            {
                results.Sort();
            }

            return results.ToArray();
        }

        public InstanceCollection GetResultsFromEnquiry(MetadataTypical typical, string enquiryName, IEnumerable<Filter> filters, out string errorText)
        {
            string filtersText = PrepareEnquiryFiltersOfsText(filters);
            return GetResultsFromEnquiry(typical, enquiryName, filtersText, out errorText);
        }

        public InstanceCollection GetResultsFromEnquiryWithCompany(MetadataTypical typical, string Company, string enquiryName, IEnumerable<Filter> filters, out string errorText)
        {
            string filtersText = PrepareEnquiryFiltersOfsText(filters);
            return GetResultsFromEnquiryWithCompany(typical, Company, enquiryName, filtersText, out errorText);
        }

        private static string PrepareEnquiryFiltersOfsText(IEnumerable<Filter> filters)
        {
            string filtersText = string.Empty;
            if (filters != null)
            {
                var sb = new StringBuilder();
                GlobusRequestBase.GenerateSearchConstraint(sb, filters);
                filtersText = sb.ToString();
            }
            return filtersText;
        }

        public InstanceCollection GetResultsFromEnquiry(MetadataTypical typical, string enquiryName, string filtersText, out string errorText)
        {
            Debug.Assert(!typical.Name.Contains(","));


            string enquriyCommand = "ENQUIRY.SELECT,,"
                                    + _Settings.InputUser
                                    + "/"
                                    + _Settings.InputPassword
                                    + ","
                                    + OFSMessageConverter.ImportMsgSubstChars(enquiryName)
                                    + filtersText;

            string ofsResponseLine;
            //Queue<string> res;
            if (RunOfsCommand(enquriyCommand, out ofsResponseLine, out errorText, -1) == ExecuteResult.Error)
            {
                return null;
            }

            OFSResponseReader responseReader = new OFSResponseReader();
            StringBuilder enquiryResponse = new StringBuilder();
            string errorInEnquiryResponse = null;
            using (XmlWriter xmlWriter = XmlWriter.Create(enquiryResponse))
            {
                try
                {
                    errorInEnquiryResponse = responseReader.ReadEnquiryResponse(enquiryName,
                                                                                ofsResponseLine,
                                                                                typical,
                                                                                xmlWriter,
                                                                                null,
                                                                                _Settings.TreatOFSResponseSingleValueAsError,
                                                                                EnquiryResultParsingRotine.None);
                }
                catch (EnquiryConfigurationException ex)
                {
                    if (ex.Message == OFSResponseReaderBase.NO_RECORDS_STRING)
                    {
                        // this is just a normal case of having no records, don't handle it as exception
                        errorText = null;
                        return new InstanceCollection();
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            if (!string.IsNullOrEmpty(errorInEnquiryResponse))
            {
                errorText = errorInEnquiryResponse;
                return null;
            }

            try
            {
                InstanceCollection records = InstanceCollection.FromXmlString(enquiryResponse.ToString(), InstanceCollection.GetXmlFragmentReadingSettings());
                return records;
            }
            catch (XmlException)
            {
                errorText = "Can not process OFS Enquiry response";
                return null;
            }
        }

        public InstanceCollection GetResultsFromEnquiryWithCompany(MetadataTypical typical, string Company, string enquiryName, string filtersText, out string errorText)
        {
            Debug.Assert(!typical.Name.Contains(","));


            string enquriyCommand = "ENQUIRY.SELECT,,"
                                    + _Settings.InputUser
                                    + "/"
                                    + _Settings.InputPassword
                                    + "/"
                                    + OFSMessageConverter.ImportMsgSubstChars(Company)
                                    + ","
                                    + OFSMessageConverter.ImportMsgSubstChars(enquiryName)
                                    + filtersText;

            string ofsResponseLine;
            Queue<string> res;
            if (RunOfsCommand(enquriyCommand, out ofsResponseLine, out errorText, -1) == ExecuteResult.Error)
            {
                return null;
            }

            OFSResponseReader responseReader = new OFSResponseReader();
            StringBuilder enquiryResponse = new StringBuilder();
            string errorInEnquiryResponse = null;
            using (XmlWriter xmlWriter = XmlWriter.Create(enquiryResponse))
            {
                try
                {
                    errorInEnquiryResponse = responseReader.ReadEnquiryResponse(enquiryName,
                                                                                ofsResponseLine,
                                                                                typical,
                                                                                xmlWriter,
                                                                                null,
                                                                                _Settings.TreatOFSResponseSingleValueAsError,
                                                                                EnquiryResultParsingRotine.None);
                }
                catch (EnquiryConfigurationException ex)
                {
                    if (ex.Message == OFSResponseReaderBase.NO_RECORDS_STRING)
                    {
                        // this is just a normal case of having no records, don't handle it as exception
                        errorText = null;
                        return new InstanceCollection();
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            if (!string.IsNullOrEmpty(errorInEnquiryResponse))
            {
                errorText = errorInEnquiryResponse;
                return null;
            }

            try
            {
                InstanceCollection records = InstanceCollection.FromXmlString(enquiryResponse.ToString(), InstanceCollection.GetXmlFragmentReadingSettings());
                return records;
            }
            catch (XmlException)
            {
                errorText = "Can not process OFS Enquiry response";
                return null;
            }
        }


        public InstanceCollection GetResultsFromEnquiry(MetadataTypical typical, string enquiryName, out string errorText)
        {
            return GetResultsFromEnquiry(typical, enquiryName, "", out errorText);
        }

        public ExecuteResult ExecuteShellCommand(string command, out Queue<string> res, out string errorText, int timeout = -1, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            return ExecuteShellCommand(command, out res, out errorText, false, null, timeout, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
        }
        //here need to add timeout parameter
        public ExecuteResult ExecuteShellCommand(string command, out Queue<string> res, out string errorText, bool useCustomResultVerification, EndOfResponseCondition endOfResponseCondition, int timeout = -1, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            if (RunCommand(ConsoleType.DBShell, command, out res, out errorText, endOfResponseCondition, timeout, useCustomResultVerification, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD) == ExecuteResult.Error)
            {
                if (errorText == null)
                    errorText = _SystemShellConnector.LastError;
                return ExecuteResult.Error;
            }

            return ExecuteResult.Success;
        }
        public ExecuteResult ExecuteDBShellCommand(string command, out Queue<string> res, out string errorText, bool useCustomResultVerification, EndOfResponseCondition endOfResponseCondition, int timeout = -1, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            if (RunCommand(ConsoleType.DBShell, command, out res, out errorText, endOfResponseCondition, timeout, useCustomResultVerification) == ExecuteResult.Error)
            {
                if (errorText == null)
                    errorText = _DbShellConnector.LastError;
                return ExecuteResult.Error;
            }

            return ExecuteResult.Success;
        }

        public ExecuteResult ExecuteJShellCommand(string command, out Queue<string> res, out string errorText, int timeout, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            return ExecuteJShellCommand(command, out res, out errorText, false, null, timeout, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
        }
        //here need to add timeout parameter
        public ExecuteResult ExecuteJShellCommand(string command, out Queue<string> res, out string errorText, bool useCustomResultVerification, EndOfResponseCondition endOfResponseCondition, int timeout, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            return RunCommand(ConsoleType.JShell, command, out res, out errorText, endOfResponseCondition, timeout, useCustomResultVerification, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
        }

        public ExecuteResult ExecuteOfsCommand(string command, out Queue<string> res, out string errorText, int timeout, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            return ExecuteOfsCommand(command, out res, out errorText, false, null, timeout, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
        }
        //here need to add timeout parameter
        public ExecuteResult ExecuteOfsCommand(string command, out Queue<string> res, out string errorText, bool useCustomResultVerification, EndOfResponseCondition endOfResponseCondition, int timeout, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            res = new Queue<string>();
            string lastResponseLine;

            ExecuteResult bResult = RunOfsCommand(command, out lastResponseLine, out errorText, useCustomResultVerification, endOfResponseCondition, timeout);

            if (!string.IsNullOrEmpty(lastResponseLine))
            {
                res.Enqueue(lastResponseLine);
            }

            return bResult;
        }

        public bool CreateBackupDirectory(string dirName, out string errorText)
        {
            if (CreateDirectory(dirName, out errorText))
            {
                _BackupFolder = dirName;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Increase the Maximum Length of the Command Line (only for AIX)
        /// The ncargs attribute determines maximum length of the command line,  including environment variables. 
        /// On the AIX operating system,  the default value of the ncargs attribute is four, 4–Kbyte blocks. 
        /// To ensure that Application Server commands do not exceed the maximum length of the command line,
        /// increase this value to 16 4–Kbyte blocks.
        /// </summary>
        internal void CheckMaxLengthOfCommandLineArgs()
        {
            if (_OS != OperatingSystem.AIX)
            {
                // this logic is only for AIX
                return;
            }

            try
            {
                string errorText;
                Queue<string> res;
                if (ExecuteShellCommand("lsattr -EH -l sys0 | grep ncargs", out res, out errorText, -1) != ExecuteResult.Success)
                {
                    // Do nothing
                    Debug.Fail("FAILED: Detection of Maximum Length of the Command Line!");
                    return;
                }

                if (res == null || res.Count != 3)
                {
                    // Do nothing
                    Debug.Fail("FAILED: Detection of Maximum Length of the Command Line!");
                    return;
                }

                // SAMPLE RESOINSE:
                // ncargs          16                    ARG/ENV list size in 4K byte blocks               True
                //
                string resLine = res.ToArray()[1];
                var arr = resLine.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                if (int.Parse(arr[1]) >= 16)
                {
                    // enough
                    return;
                }

                if (ExecuteShellCommand("chdev -l sys0 -a ncargs=16", out res, out errorText, -1) != ExecuteResult.Success)
                {
                    // Do nothing
                    Debug.Fail("FAILED: Detection of Maximum Length of the Command Line!");
                    return;
                }

            }
            catch (Exception ex)
            {
                Debug.Fail(ex.Message);
                return;
            }
        }

        public bool CreateDirectory(string dirName, out string errorText)
        {
            Queue<string> res;
            return ExecuteShellCommand("mkdir " + dirName, out res, out errorText, -1) == ExecuteResult.Success;
        }

        public ExecuteResult ExecuteGlobusClassicCommand(string command, out Queue<string> res, out string errorText, int timeout = -1, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            return ExecuteGlobusClassicCommand(command, out res, out errorText, false, null, timeout, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
        }
        //here need to add timeout parameter
        public ExecuteResult ExecuteGlobusClassicCommand(string command, out Queue<string> res, out string errorText, bool useCustomResultVerification, EndOfResponseCondition endOfResponseCondition, int timeout = -1, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            command = CodeHelper.TranslateClassicTerminalSigns(command);
            ExecuteResult execResult = RunCommand(ConsoleType.Classic, command, out res, out errorText, endOfResponseCondition, timeout, useCustomResultVerification, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
            if (execResult != ExecuteResult.Success)
            {
                if (string.IsNullOrEmpty(errorText))
                {
                    errorText = _GlobusClassicConnector != null ? _GlobusClassicConnector.LastError : "Unspecified error";
                }

                return execResult;
            }

            return ExecuteResult.Success;
        }



        public bool CopyLibAndBin(EnvironmentFolders folders, IEnumerable<string> envCatalogs, bool createCompleteCopy, out string errorText)
        {
            List<string> catalogsForBackup = new List<string>(envCatalogs);
            if (!IsUniVerse)
            {
                // Backup of catalogs .O is available only for universe
                catalogsForBackup = new List<string>();
            }

            if (!LocateLibBinDir(out errorText))
            {
                return false;
            }

            if ((folders & EnvironmentFolders.Development) == EnvironmentFolders.Development)
            {
                string jLibBackupError = BackupDirectoryOnServer(FullJLibDir, createCompleteCopy);
                if (jLibBackupError != null)
                {
                    errorText = "Failed to backup directory: " + FullJLibDir + ". Reason: " + jLibBackupError;
                    return false;
                }

                string jBinBackupError = BackupDirectoryOnServer(FullJBinDir, createCompleteCopy);
                if (jBinBackupError != null)
                {
                    errorText = "Failed to backup directory: " + FullJBinDir + ". Reason: " + jBinBackupError;
                    return false;
                }
            }

            if ((folders & EnvironmentFolders.GlobusSystem) == EnvironmentFolders.GlobusSystem)
            {
                // backup GlobusLib and GlobusBin
                string jGlobusLibBackupError = BackupDirectoryOnServer(FullJGlobusLibDir, createCompleteCopy);
                if (jGlobusLibBackupError != null)
                {
                    errorText = "Failed to backup directory: " + FullJGlobusLibDir + ". Reason: " + jGlobusLibBackupError;
                    return false;
                }

                string jGlobusBinBackupError = BackupDirectoryOnServer(FullJGlobusBinDir, createCompleteCopy);
                if (jGlobusBinBackupError != null)
                {
                    errorText = "Failed to backup directory: " + FullJGlobusBinDir + ". Reason: " + jGlobusBinBackupError;
                    return false;
                }
            }

            foreach (string catalog in catalogsForBackup)
            {
                string path = GetGEnvObjectDir(catalog, out errorText);
                if (path != null)
                {
                    string err = BackupDirectoryOnServer(path, createCompleteCopy);
                    if (err != null)
                    {
                        errorText = "Failed to backup directory: " + path + ". Reason: " + err;
                        Debug.Fail("To fail or not to fail?");
                    }
                }
                else
                {
                    Debug.Fail("To fail or not to fail?");
                }
            }

            return true;
        }

        public bool RestoreLibBinDir(EnvironmentFolders folders, IEnumerable<string> envCatalogs, out string errorText)
        {
            List<string> catalogsForBackup = new List<string>(envCatalogs);
            if (!IsUniVerse)
            {
                // Backup of catalogs .O is available only for universe
                catalogsForBackup = new List<string>();
            }

            int originalTimeout = _SystemShellConnector.Timeout;
            _SystemShellConnector.Timeout = _Settings.CopyCommandTimeOut;

            try
            {
                if (!LocateLibBinDir(out errorText))
                    return false;

                if ((folders & EnvironmentFolders.Development) == EnvironmentFolders.Development)
                {
                    if (!RestoreDirectory(FullJBinDir, out errorText))
                        return false;

                    if (!RestoreDirectory(FullJLibDir, out errorText))
                        return false;
                }

                if ((folders & EnvironmentFolders.GlobusSystem) == EnvironmentFolders.GlobusSystem)
                {
                    if (!RestoreDirectory(FullJGlobusBinDir, out errorText))
                        return false;

                    if (!RestoreDirectory(FullJGlobusLibDir, out errorText))
                        return false;
                }

                foreach (string catalog in catalogsForBackup)
                {
                    string path = GetGEnvObjectDir(catalog, out errorText);
                    if (path == null)
                    {
                        Debug.Fail("To fail or not to fail?");
                        continue;
                    }

                    if (!RestoreDirectory(path, out errorText))
                    {
                        Debug.Fail("To fail or not to fail?");
                    }
                }

                return true;
            }
            finally
            {
                _SystemShellConnector.Timeout = originalTimeout;
            }
        }

        internal bool AreFoldersEqual(string a, string b)
        {
            return AreFoldersEqual(a, b, _OS);
        }

        private static bool AreFoldersEqual(string a, string b, OperatingSystem os)
        {
            // TODO???
            if (a.EndsWith("\\") || a.EndsWith("/"))
            {
                a = a.Substring(0, a.Length - 1);
            }

            if (b.EndsWith("\\") || b.EndsWith("/"))
            {
                b = b.Substring(0, b.Length - 1);
            }

            if (os == OperatingSystem.AIX)
            {
                return (a == b);
            }
            else
            {
                return (string.Compare(a, b, true) == 0);
            }
        }

        public bool CopyObjFiles(IEnumerable<string> objectFilesNames, out string errorText)
        {
            char[] pathDelimiters = new char[] { '\\', '/' };

            errorText = string.Empty;

            foreach (string sourceFileName in objectFilesNames)
            {
                string fileName = sourceFileName.Split(pathDelimiters, StringSplitOptions.RemoveEmptyEntries).Last();
                string sourceFilePath = sourceFileName.TrimEnd(pathDelimiters);
                sourceFilePath = sourceFilePath.Substring(0, sourceFilePath.Length - fileName.Length);

                string targetDirectoryName = _BackupFolder;
                targetDirectoryName = targetDirectoryName.TrimEnd(pathDelimiters);
                targetDirectoryName += _PathDelimiter;

                if (AreFoldersEqual(sourceFilePath, FullObjectDevDir))
                {
                    targetDirectoryName += _JLibDir.Split(pathDelimiters).Last();
                }
                else if (AreFoldersEqual(sourceFilePath, FullObjectGlobusDir))
                {
                    targetDirectoryName += _JGlobusLibDir.Split(pathDelimiters).Last();
                }
                else
                {
                    Debug.Fail("TODO");
                    continue;
                }

                targetDirectoryName += _PathDelimiter;
                targetDirectoryName += _Settings.Objectdir.Trim(pathDelimiters);
                targetDirectoryName += _PathDelimiter;

                string tempErrorText;

                if (!CreateDirectory(targetDirectoryName, out tempErrorText))
                {
                    errorText += tempErrorText + "\r\n";
                    break;
                }

                if (!CopyFile(sourceFileName, targetDirectoryName, out tempErrorText))
                {
                    errorText += tempErrorText + "\r\n";
                }
            }

            return (errorText == string.Empty);
        }

        private bool CopyFile(string sourceFileName, string targetDirectoryName, out string errorText)
        {
            try
            {
                if (sourceFileName.Contains(" "))
                {
                    sourceFileName = "\"" + sourceFileName + "\"";
                }

                if (targetDirectoryName.Contains(" "))
                {
                    targetDirectoryName = "\"" + targetDirectoryName + "\"";
                }

                string command = string.Format(
                    _OS == OperatingSystem.AIX ? "cp {0} {1}" : "xcopy {0} {1}",
                    sourceFileName,
                    targetDirectoryName);

                Queue<string> output;
                if (RunCommand(ConsoleType.Shell, command, out output, out errorText) == ExecuteResult.Error)
                {
                    errorText = string.Format("Could not create local copy of file '{0}' in directory '{1}': {2}", sourceFileName, targetDirectoryName, errorText);
                    return false;
                }

                bool res = false;

                if (_OS == OperatingSystem.AIX)
                {
                    res = output.Count == 2 && output.Last().Trim().EndsWith("$");
                }
                else
                {
                    foreach (string line in output)
                    {
                        if (line.Contains("1 File(s) copied"))
                        {
                            res = true;
                            break;
                        }
                    }
                }

                if (!res)
                {
                    errorText = string.Format("Could not create local copy of file '{0}' in directory '{1}': \r\n", sourceFileName, targetDirectoryName);
                    foreach (string line in output)
                    {
                        errorText += line + "\r\n";
                    }
                }

                return res;
            }
            catch (Exception ex)
            {
                errorText = ex.Message;
                return false;
            }
        }

        public bool DeleteBackupFolder(out string errorText)
        {
            if (string.IsNullOrEmpty(_BackupFolder))
            {
                errorText = "No backup has been created in this deployment session";
                Debug.Fail("Don't call it if backup-directory is not created!");
                return true;
            }

            return DeleteDirectory(_BackupFolder, out errorText);
        }

        public bool GetNextAvailDir(out string nextDir, out string errorText)
        {
            nextDir = "";
            int maxIndex;
            if (!FindMaxIndexInBackupFolders(out errorText, out maxIndex))
            {
                return false;
            }

            nextDir = string.Format("{0}{1}{2}", BackupFolderPrefix, DateTime.Now.ToString(BackupFolderDateFormat), maxIndex + 1);

            return true;
        }

        public bool DoesFileExist(string filePath)
        {
            string command;
            switch (_OS)
            {
                case OperatingSystem.AIX:
                    command = string.Format("ls -l {0}", filePath);
                    break;
                case OperatingSystem.Win:
                    command = string.Format("dir \"{0}\"", filePath);
                    break;
                default:
                    Debug.Fail("Unknown environment type!");
                    return false;
            }

            Queue<string> response;
            string errorText;
            if (RunCommand(ConsoleType.Shell, command, out response, out errorText) == ExecuteResult.Error)
            {
                Debug.Fail("Check why this command failed");
                return false;
            }

            switch (_OS)
            {
                case OperatingSystem.AIX:
                    return !response.Any(line => line.Contains("does not exist."));
                case OperatingSystem.Win:
                    return response.Any(line => line.Contains("1 File(s)"));
                default:
                    Debug.Fail("Unknown environment type!");
                    return false;
            }
        }

        /// <summary>
        /// Finds the affected files.
        /// </summary>
        /// <param name="compiledFile">The compiled file</param>
        /// <param name="isBeforeCompilation">Is analysis before compilation</param>
        /// <remarks> Examples of jshow usage:
        /// 
        /// jshow R.UPLOAD.DATA
        /// Executable:          /home/jbaseadm/MIGVB2008/bnk.run/bin/R.UPLOAD.DATA.so
        /// Executable (DUP!!):  ./R.UPLOAD.DATA
        /// Executable (DUP!!):  /home/jbaseadm/MIGVB2008/bnk.run/bin/R.UPLOAD.DATA
        /// 
        /// jshow DEC.TEST.GOR
        /// Subroutine:          /home/jbaseadm/MIGVB2008/bnk.run/lib/lib53.so.154
        /// 
        /// </remarks> 
        /// <returns></returns>
        public CompilationAnalysisResult AnalyzeFile(CompiledFile compiledFile, bool isBeforeCompilation)
        {
            CompilationAnalysisResult caResult = new CompilationAnalysisResult();

            string fileName = compiledFile.RoutineName;

            string eText;
            Queue<string> res;
            string cmd = string.Format(GetJShellCommand(JShellCommandType.AnalyzeCompilation).Command, fileName);
            if (RunCommand(ConsoleType.JShell, cmd, out res, out eText) == ExecuteResult.Error)
            {
                caResult.ErrorMessage = "Error analyzing libraries.\nReason: " + eText;
                return caResult;
            }

            foreach (string line in res)
            {
                if (line.StartsWith("Executable") || line.StartsWith("Subroutine"))
                {
                    string file = line.Split(' ').Last();

                    bool isDuplicate = line.Contains(_Settings.DuplicationString);

                    if (line.StartsWith("Subroutine"))
                    {
                        string libdef = file.Substring(0, file.LastIndexOf(_PathDelimiter) + 1) + _Settings.Exportname;
                        if (isDuplicate)
                        {
                            caResult.DuplicateFiles.Add(libdef);
                        }
                        else
                        {
                            caResult.AffectedFiles.Add(libdef);
                        }
                    }

                    // we need only the files from the Home directory, the others are useless relative paths
                    if (file.ToUpperInvariant().StartsWith(HomeDir.ToUpperInvariant()))
                    {
                        if (isDuplicate)
                        {
                            caResult.DuplicateFiles.Add(file);
                        }
                        else
                        {
                            caResult.AffectedFiles.Add(file);
                        }
                    }
                    else
                    {
                        //Debug.WriteLine("Ignoring: " + file + " since it is not in the home directory");
                        string homeDir = this.HomeDir;
                        if (!string.IsNullOrEmpty(homeDir))
                            if (homeDir[homeDir.Length - 1] != _PathDelimiter)
                            {
                                homeDir += _PathDelimiter;
                            }

                        try
                        {
                            string fullFileName = FtpPath.BuildX(homeDir, file, FtpLogsSubdir);
                            if (isDuplicate)
                            {
                                caResult.DuplicateFiles.Add(fullFileName);
                            }
                            else
                            {
                                caResult.AffectedFiles.Add(fullFileName);
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.Fail(ex.Message);
                        }
                    }
                }
                if (line.Contains("source file"))
                {
                    caResult.SourceDir = line.Substring(line.LastIndexOf(" ") + 1);
                }
            }

            if (caResult.DuplicateFiles.Count != 0)
            {
                // TODO - Revise everything in this if
                bool hasDevFolderInResult = false;
                foreach (string s in caResult.AffectedFiles)
                {
                    if (s.ToUpperInvariant().StartsWith(FullJBinDir.ToUpperInvariant()) ||
                        s.ToUpperInvariant().StartsWith(FullJLibDir.ToUpperInvariant()))
                    {
                        hasDevFolderInResult = true;
                        break;
                    }
                }

                bool hasDevFolderInDuplicateResult = false;
                foreach (string s in caResult.DuplicateFiles)
                {
                    if (s.ToUpperInvariant().StartsWith(_JBinDir.ToUpperInvariant()) ||
                        s.ToUpperInvariant().StartsWith(_JLibDir.ToUpperInvariant()))
                    {
                        hasDevFolderInDuplicateResult = true;
                        break;
                    }
                }

                if (hasDevFolderInDuplicateResult && !hasDevFolderInResult)
                {
                    // Do nothing
                }
                else if (!hasDevFolderInDuplicateResult && hasDevFolderInResult)
                {
                    // Do nothing
                }
                else
                {
                    foreach (string s in caResult.DuplicateFiles)
                    {
                        string ss = s.Replace("\\\\", "\\");

                        if (caResult.AffectedFiles.Contains(ss))
                        {
                            continue;
                        }

                        caResult.AffectedFiles.Add(ss);
                    }
                }
            }

            // remove duplicate entries
            caResult.AffectedFiles = caResult.AffectedFiles.Distinct().ToList();
            caResult.DuplicateFiles = caResult.DuplicateFiles.Distinct().ToList();

            // Get object file
            string tempErrMsg;
            string objName = ConstructObjectFileName(compiledFile, caResult.AffectedFiles, isBeforeCompilation, out tempErrMsg);
            if (objName != "")
            {
                caResult.AffectedFiles.Add(objName);
                caResult.AffectedObjectFiles.Add(objName);
            }

            return caResult;
        }

        /// <summary>
        /// Calculate checksums
        /// </summary>
        /// <param name="folders"></param>
        /// <param name="objectFiles"></param>
        /// <param name="recursive"></param>
        /// <param name="checksums"></param>
        /// <param name="failures"></param>
        /// <param name="errorText"></param>
        /// <param name="skipChecksum">Identifies that the skip checksum option is specified in the configuration file. 
        /// If True, no calculation of checksums was performed </param>
        /// <returns></returns>
        public bool GetCheckSumsOfLibAndBin(EnvironmentFolders folders, IEnumerable<string> objectFiles, bool recursive,
            out Dictionary<string, string> checksums, out Dictionary<string, string> failures, out string errorText, out bool skipChecksum)
        {
            checksums = new Dictionary<string, string>();
            failures = new Dictionary<string, string>();
            errorText = String.Empty;
            skipChecksum = false;

            // Skip the check sum processing if the configuration SkipCheckSum is true for the current TelnetSet
            if (_Settings.SkipCheckSum)
            {
                skipChecksum = true;
                return true;
            }

            if (!LocateLibBinDir(out errorText))
            {
                return false;
            }

            var libCheckSums = new Dictionary<string, string>();
            var libFailures = new Dictionary<string, string>();
            bool libResult = true;

            var binCheckSums = new Dictionary<string, string>();
            var binFailures = new Dictionary<string, string>();
            bool binResult = true;

            if ((folders & EnvironmentFolders.Development) == EnvironmentFolders.Development)
            {
                libResult = FillChecksums(FullJLibDir, recursive, ref errorText, out libCheckSums, out libFailures);
                binResult = FillChecksums(FullJBinDir, recursive, ref errorText, out binCheckSums, out binFailures);
            }

            // getting Checksums for globusLib and globusBin folders
            var globusLibCheckSums = new Dictionary<string, string>();
            var globusLibFailures = new Dictionary<string, string>();
            bool globusLibResult = true;

            var globusBinCheckSums = new Dictionary<string, string>();
            var globusBinFailures = new Dictionary<string, string>();
            bool globusBinResult = true;

            if ((folders & EnvironmentFolders.GlobusSystem) == EnvironmentFolders.GlobusSystem)
            {
                globusLibResult = FillChecksums(FullJGlobusLibDir, recursive, ref errorText, out globusLibCheckSums, out globusLibFailures);
                globusBinResult = FillChecksums(FullJGlobusBinDir, recursive, ref errorText, out globusBinCheckSums, out globusBinFailures);
            }


            var objectFilesCheckSums = new Dictionary<string, string>();
            var objectFilesFailures = new Dictionary<string, string>();
            bool objectFilesResult = true;

            foreach (string objFilePath in objectFiles)
            {
                string objCheckSum;
                string tempErrText;
                bool skipCheckSum;
                if (!GetChecksum(objFilePath, out objCheckSum, out tempErrText, out skipCheckSum))
                {
                    objectFilesResult = false;
                    objectFilesFailures.Add(objFilePath, errorText);
                    errorText += tempErrText;
                }
                else if (!skipCheckSum)
                {
                    objectFilesCheckSums.Add(objFilePath, objCheckSum);
                }
            }

            checksums = Merge(new List<Dictionary<string, string>> { libCheckSums, binCheckSums, globusLibCheckSums, globusBinCheckSums, objectFilesCheckSums });
            failures = Merge(new List<Dictionary<string, string>> { libFailures, binFailures, globusLibFailures, globusBinFailures, objectFilesFailures });

            return libResult && binResult && globusLibResult && globusBinResult && objectFilesResult;
        }

        private bool FillChecksums(string checksumsDir, bool recursive, ref string errorText, out Dictionary<string, string> checkSums, out Dictionary<string, string> failures)
        {
            string currentErrorMsg;
            bool libResult = GetCheckSumsOfFilesInDirectory(checksumsDir, recursive, out checkSums, out failures, out currentErrorMsg);

            if (!String.IsNullOrEmpty(currentErrorMsg))
            {
                errorText += currentErrorMsg;
                errorText += Environment.NewLine;
            }

            return libResult;
        }

        public bool GetCheckSumsOfFilesInDirectory(string directory,
                                                   bool recursive,
                                                   out Dictionary<string, string> checksums,
                                                   out Dictionary<string, string> failures,
                                                   out string errorText)
        {
            // NOTE: The event handlers are not used, because now the processing is done server-side and it is hard to notify for progress
            //string operationName = "Calculating checksums for all files in " + directory;

            checksums = new Dictionary<string, string>();
            failures = new Dictionary<string, string>();

            try
            {
                string dirChecksumsCmd;
                if (directory.Contains(" "))
                {
                    dirChecksumsCmd = String.Format("ChecksumToolSMS {0}\"{1}\"", recursive ? "" : "-f ", directory);
                }
                else
                {
                    dirChecksumsCmd = String.Format("ChecksumToolSMS {0}{1}", recursive ? "" : "-f ", directory);
                }

                const string terminatingText = ":END:";

                Queue<string> response;
                // TODO we could easily wait for the shell
                if (RunCommand(ConsoleType.Shell, dirChecksumsCmd, out response, out errorText) == ExecuteResult.Error)
                {
                    errorText = string.Format("Error encountered while executing command '{0}'\nReason: {1}", dirChecksumsCmd, errorText);
                    return false;
                }

                if (response.Count == 4 && response.ToArray()[1].Contains("Usage: checksum [path]"))
                {
                    errorText = string.Format("Obsolete version of ChecksumToolSMS detected (required version 1.1)!");
                    return false;
                }

                List<string> filesChecksums = new List<string>(response);
                for (int i = 0; i < filesChecksums.Count; i++)
                {
                    filesChecksums[i] = filesChecksums[i].Trim();
                }

                filesChecksums.Remove(dirChecksumsCmd); // the first item is the command itself, so skip it
                filesChecksums.Remove(_SystemShellConnector.ShellPrompt); // the last item is the prompt, so skip it

                foreach (string checksumAndFile in filesChecksums)
                {
                    if (checksumAndFile == terminatingText || checksumAndFile.Trim() == _SystemShellConnector.ShellPrompt.Trim())
                    {
                        continue;
                    }

                    int pos = checksumAndFile.IndexOf(' ');
                    if (!(checksumAndFile.Length >= 32 && IsValidHash(checksumAndFile.Substring(0, 32))))
                    {
                        if (checksumAndFile.Trim() != _SystemShellConnector.ShellPrompt.Trim())
                        {
                            Debug.Fail("Unexpected checkum row: " + checksumAndFile);
                        }
                        continue;
                    }

                    if (pos > -1)
                    {
                        string checkSum = checksumAndFile.Substring(0, pos);
                        Debug.Assert(IsValidHash(checkSum));
                        string relativeFileName = checksumAndFile.Substring(pos + 1);
                        checksums[relativeFileName] = checkSum;
                    }
                }
            }
            finally
            {
                Debug.WriteLine("Success" + checksums.Count + " Failure: " + failures.Count());
            }

            return true;
        }

        public bool Decatalog(string progName, string directory, bool isCatalog, out string errorText)
        {
            char[] removeArr = { '/', '\\' };
            directory = directory.Trim(removeArr);
            if (directory.Trim() == "")
            {
                directory = ". ";
            }

            JShellCommand jshDecatalog = GetJShellCommand(JShellCommandType.Decatalog);

            string cmd = string.Format(jshDecatalog.Command, directory, progName);
            Queue<string> res = null;
            errorText = "";
            bool success = true;

            if (IsGlobusBPFolder(directory, isCatalog))
            {
                //A command to set the env. variable to globuslib
                string envCommand = ((this.OS == OperatingSystem.Win) ? "set " : "export ") +
                    "JBCDEV_LIB=" + FullJGlobusLibDir;

#if DEBUG // Test old code
                Debug.Assert(
                        FullJGlobusLibDir ==
                        (this.HomeDir.TrimEnd(this._PathDelimiter) + this._PathDelimiter + "globuslib")
                    );
#endif

                success = RunCommand(ConsoleType.JShell, envCommand, out res, out errorText) != ExecuteResult.Error;

            }

            if (success)
            {
                success = RunCommand(ConsoleType.JShell, cmd, out res, out errorText) != ExecuteResult.Error;

                if (IsGlobusBPFolder(directory, isCatalog))
                {
                    //reset the env. variable to lib
                    string envCommand = ((this.OS == OperatingSystem.Win) ? "set " : "export ") +
                        "JBCDEV_LIB=" + this._JLibDir;

                    Queue<string> dummyRes;
                    string dummyErrorText;
                    //No need to check for success since we had already set it successfully above
                    //Even if the connection is lost the var will be reset automaticaly during next login
                    RunCommand(ConsoleType.JShell, envCommand, out dummyRes, out dummyErrorText);

                }
            }
            if (success)
            {
                StringBuilder sbError = new StringBuilder();
                foreach (string line in res)
                {
                    sbError.Append(line);
                    sbError.Append("\r\n");

                    //if (line.Contains("decataloged successfully"))
                    if (jshDecatalog.CheckResponseSuccess(line))
                    {
                        DeleteDollarFile(directory, progName);
                        return true;
                    }
                }

                errorText = sbError.ToString();
                errorText = errorText.Replace(cmd, "");
            }
            else
            {
                StringBuilder sbError = new StringBuilder();
                sbError.Append(errorText);
                sbError.Append("\r\n");

                if (res != null)
                {
                    foreach (string line in res)
                    {
                        sbError.Append(line);
                        sbError.Append("\r\n");
                    }
                }

                errorText = sbError.ToString();
                errorText = errorText.Replace(cmd, "");
            }

            return false;
        }

        public bool SetFullAccessRights(string fileOrDirectoryName, out string errorText)
        {
            if (_OS == OperatingSystem.Win)
            {
                // TODO remove readonly flag
                errorText = "Not Implemented in DOS";
                return false;
            }
            else
            {
                return ChangeAccessRights(fileOrDirectoryName, "777", out errorText);
            }
        }

        private bool ChangeAccessRights(string fileOrDirectoryName, string rights, out string errorText)
        {
            string command = string.Format("chmod {0} {1}", rights, fileOrDirectoryName);
            Queue<string> res;
            if (ExecuteShellCommand(command, out res, out errorText, -1) == ExecuteResult.Error)
            {
                return false;
            }

            if (res.Count != 2)
            {
                Debug.WriteLine("Setting access rights '" + rights + "' for '" + fileOrDirectoryName
                                + "' did not work, but this might be because the item was missing!" +
                                " Continue, we do not want to stop now!");
            }

            return true;
        }

        public List<CompileOutput> CompileFiles(List<DeployableFile> filesList)
        {
            List<CompileOutput> coList = new List<CompileOutput>();

            foreach (DeployableFile file in filesList)
            {
                CompileOutput compileOutput = CompileFile(file, DeploymentMethod.File_DeployAndCompile);
                coList.Add(compileOutput);
            }

            return coList;
        }

        public CompileOutput CompileFile(DeployableFile file, DeploymentMethod deploymentMethod)
        {
            string sourceName = file.IFile.Name;
            CompileOutput co = new CompileOutput(sourceName, _JShellConnector.ShellPrompt);
            co.DeploymentPack = file.DeploymentPack;

            string folder = GetTargetFolderInNormalizedForm(file.ServerDeploymentFolder, file.IsCatalog);

            if (file.IsBinaryDollarFile)
            {
                CatalogDollarFile(file.RoutineName, folder, co);
            }
            else
            {
                JShellCommand compileCatalogCmd = null;
                JShellCommand command = GetJShellCommand(JShellCommandType.CompileAndCatalog);

                if (command != null && command.SuccessResponseRegex != null)
                {
                    compileCatalogCmd = command;
                }

                if (compileCatalogCmd == null)
                {
                    if (BasicCompile(sourceName, folder, co))
                        CatalogDollarFile(file.RoutineName, folder, co);
                }
                else
                {
                    CompileAndCatalogFile(sourceName, file.RoutineName, folder, co);
                }

                if (co.Outcome == CompileOutcome.Success)
                {
                    CheckIsFileRespondOnJShow(sourceName, folder, co);
                }
            }

            return co;
        }

        private void CheckIsFileRespondOnJShow(string sourceName, string folder, CompileOutput co)
        {
            Queue<string> cmdOutput;
            string errorText;
            string lastLine;
            JShellCommand command = GetJShellCommand(JShellCommandType.JShow);

            string cmd = string.Format(command.Command, folder, sourceName);
            if (RunAndGetLastLine(ConsoleType.JShell, cmd, out cmdOutput, out errorText, out lastLine) == ExecuteResult.Success)
            {
                if (lastLine == "")
                {
                    co.AddBasicError(string.Format("Compilation failed. File {0} was not found with 'jshow' command.", sourceName));
                }
            }
            else
            {
                co.AddBasicError(errorText);
            }
        }

        public bool IsCatalogExists(string listCmd)
        {
            string errorText;
            string lastLine;
            Queue<string> cmdOutput;

            EndOfResponseCondition endOfResponseCondition = new EndOfResponseCondition(_JShellConnector.ShellPrompt)
            {
                FinishOnMaxLines = 20
            };

            if (RunAndGetLastLine(ConsoleType.JShell, listCmd, out cmdOutput, out errorText, out lastLine, false, endOfResponseCondition) == ExecuteResult.Error)
            {
                return false;
            }

            if (cmdOutput.Count > 0)
            {
                if (cmdOutput.ToList().Last() != _JShellConnector.ShellPrompt)
                {
                    endOfResponseCondition = new EndOfResponseCondition(_JShellConnector.ShellPrompt)
                    {
                        DontExpectCommandEcho = true
                    };

                    if (RunAndGetLastLine(ConsoleType.JShell, "q", out cmdOutput, out errorText, out lastLine, false, endOfResponseCondition) == ExecuteResult.Error)
                    {
                        return false;
                    }
                }
            }

            // TODO - Make it configurable
            bool res = (lastLine != "No file name could be found for your query")
                       && (!lastLine.EndsWith("No such file or directory"));
            return res;
        }

        public void CopyFileToCatalog(string fileName, string catalogName)
        {
            catalogName = catalogName.Replace('\\', '/'); // '/' works for WIN and AIX.
            string copyCommand = string.Format(GetJShellCommand(JShellCommandType.CopyFileToCatalog).Command, catalogName, fileName);

            string errorText;
            string lastLine;
            Queue<string> cmdOutput;
            if (RunAndGetLastLine(ConsoleType.JShell, copyCommand, out cmdOutput, out errorText, out lastLine) == ExecuteResult.Error)
            {
                throw new Exception(errorText);
            }

            // TODO make response processing configurable
            if (!lastLine.EndsWith("copied"))
            {
                throw new Exception(
                    string.Format(
                        "ERROR: Copying to catalog. Command: {0}. Last response line: {1}",
                        copyCommand,
                        lastLine
                        )
                    );
            }
        }

        public bool DeleteFileForRollBack(string svrFileName, out string errorText)
        {
            bool match = StringMethods.Path.CompareWildcards(svrFileName, _Settings.RollBackForceDelete, true);

            if (_Settings.LibVersionDelimiter != string.Empty)
            {
                int lastPathIndex = svrFileName.LastIndexOf(_PathDelimiter);
                if (lastPathIndex >= 0)
                {
                    string lastDir = svrFileName.Substring(lastPathIndex - 4, 4);
                    if (lastDir == _PathDelimiter + "lib")
                    {
                        string libName = svrFileName.Substring(lastPathIndex + 1);
                        DeleteOldLib(libName);
                    }
                }
            }

            if (!match)
            {
                errorText = "INFO: Delete command is not executed, because of the filter!";
                return true;
            }

            return DeleteFile(svrFileName, out errorText);
        }

        public bool DeleteOldLib(string libFileName)
        {
            string errorText;

            if (libFileName == _Settings.Exportname)
                return true;
            libFileName = libFileName.Substring(0, libFileName.LastIndexOf(_Settings.LibVersionDelimiter));

            Queue<string> response;
            string command;
            if (_OS == OperatingSystem.AIX)
            {
                command = string.Format("find ./lib -name \"{0}*\" -exec rm {1} \\;", libFileName, "{}");
            }
            else
            {
                command = string.Format("del lib\\{0}*", libFileName);
            }

            if (RunCommand(ConsoleType.Shell, command, out response, out errorText) == ExecuteResult.Error)
            {
                return false;
            }

            return true;
        }

        public bool DeleteFile(string svrFileName, out string errorText)
        {
            string rmCmd;
            switch (_OS)
            {
                case OperatingSystem.AIX:
                    rmCmd = "rm -f";    // One For AIX
                    break;
                case OperatingSystem.Win:
                    rmCmd = "del /F";
                    break;
                default:
                    Debug.Fail("Unknown environment type!");
                    errorText = "Unknown environment type!";
                    return false;
            }

            string command = (svrFileName.Contains(" ") ?
                    string.Format("{0} \"{1}\"", rmCmd, svrFileName) :
                    string.Format("{0} {1}", rmCmd, svrFileName)
                );

            Queue<string> response;
            if (RunCommand(ConsoleType.Shell, command, out response, out errorText) == ExecuteResult.Error)
            {
                return false;
            }

            return true;
        }

        private bool DeleteDirectory(string directory, out string errorText)
        {
            string rdCommand;
            switch (_OS)
            {
                case OperatingSystem.AIX:
                    rdCommand = "rm -f -r";
                    break;
                case OperatingSystem.Win:
                    rdCommand = "rd /S /Q";
                    break;
                default:
                    Debug.Fail("Unknown environment type!");
                    errorText = "Unknown environment type!";
                    return false;
            }

            string command = (directory.Contains(" ") ?
                    string.Format("{0} \"{1}\"", rdCommand, directory) :
                    string.Format("{0} {1}", rdCommand, directory)
                );

            Queue<string> response;
            if (RunCommand(ConsoleType.Shell, command, out response, out errorText) == ExecuteResult.Error)
            {
                return false;
            }

            return true;
        }

        public bool DirectlyCopyRecordUsingJsh(string relativeDirPath,
                                               string sourceFileName,
                                               string targetID,
                                               string targetTable,
                                               out string errorText)
        {
            string lastLine;
            Queue<string> cmdResponse;

            //_Settings.

            JShellCommand cmd = GetJShellCommand(JShellCommandType.CopyRecord);
            string copyCommand = string.Format(cmd.Command,
                relativeDirPath, targetTable, sourceFileName, targetID);

            if (RunAndGetLastLine(ConsoleType.JShell, copyCommand, out cmdResponse, out errorText, out lastLine) == ExecuteResult.Error)
            {
                return false;
            }

            if (cmd.CheckResponseSuccess(lastLine))
            {
                return true;
            }

            errorText = QueueToString(cmdResponse, true);

            return false;
        }

        public bool DeleteHistoryRecordUsingJsh(string liveTable, string targetID, out string errorText)
        {
            string lastLine;
            Queue<string> cmdResponse;

            string hisTable = liveTable;
            if (hisTable.Contains("$"))
            {
                hisTable = hisTable.Substring(0, hisTable.IndexOf('$')) + "$HIS";
            }
            else
            {
                hisTable += "$HIS";
            }

            string selectCommand = "SELECT " + hisTable + " WITH @ID LIKE " + targetID + "...";

            if (RunAndGetLastLine(ConsoleType.JShell,
                           selectCommand,
                           out cmdResponse,
                           out errorText,
                           out lastLine,
                           false,
                           new EndOfResponseCondition(SELECTION_PROMPT)) == ExecuteResult.Error)
            {
                return false;
            }
            if (lastLine == SELECTION_PROMPT)
            {
                if (RunAndGetLastLine(ConsoleType.JShell, "DELETE " + hisTable, out cmdResponse, out errorText, out lastLine) == ExecuteResult.Error)
                {
                    return false;
                }
            }
            return true;
        }

        public bool DeleteRecordUsingJsh(string targetTable, string targetID, out string errorText)
        {
            DeleteHistoryRecordUsingJsh(targetTable, targetID, out errorText);

            string lastLine;
            Queue<string> cmdResponse;

            JShellCommand cmd = GetJShellCommand(JShellCommandType.DeleteRecord);

            string deleteCommand = string.Format(cmd.Command, targetTable, targetID);

            if (RunAndGetLastLine(ConsoleType.JShell, deleteCommand, out cmdResponse, out errorText, out lastLine) == ExecuteResult.Error)
            {
                return false;
            }

            if (cmd.CheckResponseSuccess(lastLine))
            {
                return true;
            }

            errorText = QueueToString(cmdResponse, true);
            return false;
        }

        public bool GetChecksum(string sourcePath, out string checkSum, out string errorText, out bool skipChecksum)
        {
            checkSum = "";
            errorText = String.Empty;

            skipChecksum = _Settings.SkipCheckSum;
            // Skip the check sum processing if the configuration SkipCheckSum is true for the current TelnetSet
            if (skipChecksum)
                return true;

            string cmdText;
            //Workaround: The only way to figure out if this is a unix environment is by the path delimiter for a relative path.
            if (sourcePath.Contains("/"))
                sourcePath = EscapeCommandForUnixShell(sourcePath);
            if (sourcePath.Contains(" "))
            {
                cmdText = String.Format("ChecksumToolSMS \"{0}\"", sourcePath);
            }
            else
            {
                cmdText = "ChecksumToolSMS " + sourcePath;
            }

            Queue<string> checkSumToolResponse;
            if (RunCommand(ConsoleType.Shell, cmdText, out checkSumToolResponse, out errorText) == ExecuteResult.Error)
            {
                return false;
            }

            // check for message that the tool is missing
            foreach (string line in checkSumToolResponse)
            {
                if (T24TelnetConnector.IsResponseIndicatingThatCheckSumToolIsMissing(line))
                {
                    errorText = string.Format("ChecksumToolSMS was not found on target environment.");
                    return false;
                }
            }

            List<string> responseLines = new List<string>(checkSumToolResponse);

            int indexOfCommand = responseLines.IndexOf(cmdText);
            if (indexOfCommand == -1)
            {
                int curr = 0;
                string tempCmdText = cmdText;
                while (curr < responseLines.Count && curr < 10)
                {
                    if (tempCmdText.StartsWith(responseLines[curr]))
                    {
                        tempCmdText = tempCmdText.Substring(responseLines[curr].Length);
                    }
                    if (tempCmdText == string.Empty)
                    {
                        break;
                    }
                    curr++;
                }

                if (tempCmdText == string.Empty)
                {
                    indexOfCommand = curr;
                }
                else
                {
                    Debug.Fail("tempCmdText is not empty");
                }
            }
            else
            {
                Debug.Assert(indexOfCommand == 0);
            }

            if (indexOfCommand < 0)
            {
                errorText = "The response does not contain the expected line: " + cmdText;
                return false;
            }

            int indexOfHash = indexOfCommand + 1;
            if (responseLines.Count <= indexOfHash)
            {
                errorText = "The computation of file hash returned no result";
                return false;
            }

            checkSum = responseLines[indexOfHash] ?? ""; //the next item after the command should be the hash
            {
                string[] ha = checkSum.Split(new char[] { ' ' });
                Debug.Assert(ha.Length == 2);
                Debug.Assert(ha[1] == sourcePath);
                if (ha.Length > 0)
                {
                    checkSum = ha[0];
                }
            }

            if (!IsValidHash(checkSum))
            {
                errorText = string.Format("Invalid check sum = {0}", checkSum);
                return false;
            }

            return true;
        }

        #endregion

        #region Public Static Methods


        public bool RunLocalShellCommand(string command, EndOfResponseCondition endOfResponseCondition, out Queue<string> res, out string errorText, string workingDirectory = "")
        {

            res = new Queue<string>();
            errorText = "";

            bool success;
            string executablePath, commandArguments;
            splitCmdArgs(command, out executablePath, out commandArguments);

            if (string.IsNullOrEmpty(executablePath))
            {
                Log.Error("ExecuteExternalProgram: No executablePath provided.");
                return true;
            }

            ProcessStartInfo startInfo = getStartInfo(workingDirectory, executablePath, commandArguments);

            Log.Info(string.Format("Starting execution of {0} {1}...", startInfo.FileName, startInfo.Arguments));

            using (var process = new Process { StartInfo = startInfo })
            {
                var outputDataReceived = new List<string>();
                var errorDataReceived = new List<string>();

                using (var outputWaitHandle = new AutoResetEvent(false))
                using (var errorWaitHandle = new AutoResetEvent(false))
                {
                    process.OutputDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            try
                            {
                                outputWaitHandle.Set();
                            }
                            catch (Exception) { }

                        }
                        else
                        {
                            Log.Info(e.Data);
                            outputDataReceived.Add(e.Data);
                        }
                    };
                    process.ErrorDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            try
                            {
                                errorWaitHandle.Set();
                            }
                            catch (Exception) { }
                        }
                        else
                        {
                            Log.Error(e.Data);
                            errorDataReceived.Add(e.Data);
                        }
                    };

                    process.Start();

                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();

                    success = AwaitProcessOnCondition(endOfResponseCondition, res, ref errorText, process, errorDataReceived, outputWaitHandle, errorWaitHandle);

                    res = new Queue<string>(outputDataReceived);
                }
            }

            return success;
        }

        private bool AwaitProcessOnCondition(EndOfResponseCondition endOfResponseCondition, Queue<string> res, ref string errorText, Process process, List<string> errorDataReceived, AutoResetEvent outputWaitHandle, AutoResetEvent errorWaitHandle)
        {
            bool success;
            bool waitFor = false, noEorc = true; // not supported endOfResponseCondition
            int completionTimeout = 1000;

            if (endOfResponseCondition != null)
            {
                if (!string.IsNullOrEmpty(endOfResponseCondition.WaitForText))
                {
                    waitFor = true; // Wait for some string
                    noEorc = false;
                }

                else if (endOfResponseCondition.TimeOut > 0)
                {
                    completionTimeout = endOfResponseCondition.TimeOut;   // Wait for timeout
                    noEorc = false;
                }
                else
                {
                    // really no eorc, but..
                    try
                    {
                        completionTimeout = int.Parse(_Settings.DBConnection.Timeout);
                        noEorc = false;
                    }
                    catch { }
                }
            }

            success = true;

            do
            {
                if (process.WaitForExit(completionTimeout) &&
                    outputWaitHandle.WaitOne(completionTimeout) &&
                    errorWaitHandle.WaitOne(completionTimeout))

                    break;
                // recived or not the expected, but exited anyways

                if (noEorc) continue;

                if (waitFor)
                {
                    if (endOfResponseCondition.IsSatisfied(QueueToString(res))) break;
                }
                else
                {
                    // Timed out.
                    process.Kill();
                    errorText = "Timeout: process did not exit in " + completionTimeout + " miliseconds. " + string.Join("\r\n", errorDataReceived);

                    success = false;
                    break;
                }
            }
            while (true);
            return success;
        }

        private ProcessStartInfo getStartInfo(string workingDirectory, string executablePath, string commandArguments)
        {
            ProcessStartInfo startInfo = string.IsNullOrEmpty(commandArguments)
                                                         ? new ProcessStartInfo(executablePath)
                                                         : new ProcessStartInfo(executablePath, commandArguments);

            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;

            if (!string.IsNullOrEmpty(workingDirectory))
            {
                if (!Directory.Exists(workingDirectory))
                {
                    Log.Error(string.Format("This working directory '{0}' is invalid. Will use the path of the command.", workingDirectory));
                    startInfo.WorkingDirectory = Path.GetDirectoryName(executablePath);
                }
                else
                    startInfo.WorkingDirectory = workingDirectory;
            }
            else
            {
                startInfo.WorkingDirectory = Path.GetDirectoryName(executablePath);
            }

            return startInfo;
        }

        public static Settings GetSettings(string telnetSet)
        {
            Settings settings = new Settings();
            settings.PhantomSet = telnetSet;
            ExecutionStepContainer.LoadConnectionSettings(settings);

            return settings;
        }

        #endregion

        #region Private Methods

        public bool SendShellCommand(string command, out string errorMessage)
        {
            Queue<string> dummy;
            RunCommand(ConsoleType.Shell, command, out dummy, out errorMessage);
            return true;
        }

        public int GetTimeoutOfConnector(ConsoleType consoleType)
        {
            return GetConnectorByType(consoleType).Timeout;
        }

        private T24TelnetConnector GetActivedConnectorByType(ConsoleType consoleType)
        {
            T24TelnetConnector communicator = GetConnectorByType(consoleType);
            if (!IsLocalShell(consoleType) && !communicator.IsLogged)
            {
                if (communicator.Login())
                {
                    OnCommunicatorLoggedIn(consoleType);
                }
            }

            return communicator;
        }

        private void OnCommunicatorLoggedIn(ConsoleType consoleType)
        {
            switch (consoleType)
            {
                case ConsoleType.Shell:
                    CheckMaxLengthOfCommandLineArgs();
                    break;
            }
        }

        private T24TelnetConnector GetConnectorByType(ConsoleType consoleType)
        {
            switch (consoleType)
            {
                case ConsoleType.JShell:
                    return _JShellConnector;
                case ConsoleType.DBShell:
                    return _DbShellConnector;
                case ConsoleType.Shell:
                    return _SystemShellConnector;
                case ConsoleType.OFS:
                    return _OfsConnector;
                case ConsoleType.Classic:
                    return _GlobusClassicConnector;
                case ConsoleType.HttpWebService:
                    return _HttpWebServiceConnector;
                case ConsoleType.HttpRestService:
                    return _HttpRestServiceConnector;
                default:
                    throw new InvalidOperationException("consoleType");
            }
        }

        public ExecuteResult RunCommand(ConsoleType consoleType, string command,
            out Queue<string> res, out string errorText, EndOfResponseCondition endOfResponseCondition = null, int predefinedTimeOut = -1,
            bool useCustomResultVerification = false, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            LatestConsoleType = consoleType;
            bool shouldRetry = false;
            ExecuteResult execResult;
            errorText = string.Empty;
            res = new Queue<string>();

            do
            {
                bool success;
                bool isJShell = false;

                //We run the routines through web service
                if (_Settings.Type == Settings.IOType.HttpWebService && consoleType == ConsoleType.JShell)
                {
                    consoleType = ConsoleType.HttpWebService;
                    isJShell = true;
                }

                if (_Settings.Type == Settings.IOType.HttpRestService && consoleType == ConsoleType.JShell)
                {
                    consoleType = ConsoleType.HttpRestService;
                    isJShell = true;
                }

                T24TelnetConnector connector = GetActivedConnectorByType(consoleType);

                switch (consoleType)
                {
                    case ConsoleType.Classic:
                    case ConsoleType.JShell:
                    case ConsoleType.OFS:
                    case ConsoleType.Shell:
                    case ConsoleType.DBShell:
                        {
                            if (!connector.IsLogged && consoleType != ConsoleType.HttpWebService && !IsLocalShell(consoleType))
                            {
                                errorText = connector.LastError;
                                return ExecuteResult.Error;
                            }

                            int originalTimeout = connector.Timeout;
                            if (predefinedTimeOut > 0)
                            {
                                connector.Timeout = predefinedTimeOut;
                            }

                            UnexpectedCommunicationErrorList unexpectedCommunicationErrors =
                                _Settings.UnexpectedCommunicationErrors.GetFilteredByConsoleType(consoleType);

                            if (consoleType == ConsoleType.DBShell && (unexpectedCommunicationErrors == null || unexpectedCommunicationErrors.Count == 0))
                                unexpectedCommunicationErrors =
                                _Settings.UnexpectedCommunicationErrors.GetFilteredByConsoleType(ConsoleType.Shell);

                            try
                            {
                                if (IsLocalShell(consoleType))
                                {
                                    success = RunLocalShellCommand(command, endOfResponseCondition, out res, out errorText);
                                }
                                else
                                {
                                    success = connector.RunTelnetCommand(
                                          command,
                                          endOfResponseCondition,
                                          unexpectedCommunicationErrors,
                                          INPUT_USER,
                                          INPUT_PASSWORD,
                                          AUTHORIZATION_USER,
                                          AUTHORIZATION_PASSWORD);

                                    errorText = connector.LastError;
                                    res = connector.ReadResponseFromTelnetBuffer();
                                }
                            }
                            finally
                            {
                                if (predefinedTimeOut > 0)
                                {
                                    connector.Timeout = originalTimeout;
                                }
                            }
                            break;
                        }
                    case ConsoleType.HttpWebService:
                        {
                            HttpWebServiceConnector httpWebServiceConnector = (HttpWebServiceConnector)connector;

                            ConnectionTypeSetting cts = _Settings.ConnectionTypes.Find(ct => ct.ConnectionType == ConsoleType.HttpWebService);

                            success = httpWebServiceConnector.RunHttpWebServiceCommand(
                                command,
                                isJShell
                                    ? cts.SubroutineInvokerWebServiceUrl
                                    : cts.OFSWebServiceUrl,
                                cts.WebServiceResponseTimeOut);

                            res = httpWebServiceConnector.ReadResponse();
                            if (!success)
                            {
                                errorText = res.Last();
                            }
                            break;
                        }
                    case ConsoleType.HttpRestService:
                        {
                            var httpRestServiceConnector = (HttpRestServiceConnector)connector;

                            var cts = (HttpRestServiceConfigSettings)_Settings.ConnectionTypes.Find(ct => ct.ConnectionType == ConsoleType.HttpRestService);

                            success = httpRestServiceConnector.RunHttpWebServiceCommandUpdated(
                                command,
                                isJShell
                                    ? cts.SubroutineInvokerWebServiceUrl
                                    : cts.OFSWebServiceUrl,
                                cts.WebServiceResponseTimeOut,
                                cts.Username,
                                cts.Password,
                                isJShell
                                    ? cts.OfsRoutineJsonPath
                                    : cts.OfsJsonPath,
                                cts.HttpMethod,
                                cts.Parameters,
                                cts.Number_Of_Routine_Parameter
                            );

                            res = httpRestServiceConnector.ReadResponse();
                            if (!success)
                            {
                                errorText = res.Last();
                            }
                            break;
                        }
                    default:
                        throw new InvalidOperationException("Unkown value for consoleType");
                }

                string strippedResponse = ResponseToStr(res, command != string.Empty && !IsLocalShell(consoleType), false);
                strippedResponse = StripResponseIfOFS(consoleType, strippedResponse);

                // SSH chops the response returning newline chars in the res queue.
                // At least for OFS it is safe to join them
                if (consoleType == ConsoleType.OFS)
                {
                    res = new Queue<string>();
                    res.Enqueue(command);
                    if (!success && !string.IsNullOrEmpty(errorText))
                        res.Enqueue(errorText);
                    else
                        res.Enqueue($"{_Settings.OFSResponseStart}{ strippedResponse.Replace("\r", "").Replace("\n", "")}{_Settings.OFSResponseEnd}");
                }

                if (success && useCustomResultVerification)
                {

                    string strippedCommand = StripCommandIfOFS(consoleType, command);

                    CustomCommandResult cmdResult;
                    success = CustomResultCheck(consoleType, strippedCommand, strippedResponse, out cmdResult);
                    if (!success)
                    {
                        errorText = string.Format("Response indicated error according to {0}{1}: \n{2}",
                            cmdResult.Summary,
                            CustomCommandDefinition == null ? " (see TelnetCommDef.xml, Special/CustomCommandResults section)" : string.Empty,
                            ResponseToStr(res, true, false));
                    }
                }

                if (success)
                {
                    shouldRetry = false;
                    execResult = ExecuteResult.Success;
                }
                else
                {
                    execResult = ExecuteResult.Error;
                    if (!IsCalledByDllAdapter)
                    {
                        execResult = AskUserToRetryCommand(command, errorText, out shouldRetry);
                        // TODO probably try to relogin?
                    }
                }

            } while (shouldRetry);

            return execResult;
        }

        private static void splitCmdArgs(string command, out string exePath, out string args)
        {
            exePath = command;
            args = "";

            int exeEndQuote = 0;
            if (command[0] == '\"')
            {
                exeEndQuote = command.IndexOf("\"", 1);
                if (exeEndQuote <= 1 || command.Length <= exeEndQuote + 2 && command[exeEndQuote + 1] != ' ')
                {
                    // forget about it
                    return;
                }
            }

            int spaceIdx = command.IndexOf(' ', exeEndQuote);
            if (spaceIdx > 0)
            {
                exePath = command.Substring(0, spaceIdx).Trim('"');
                args = command.Substring(spaceIdx + 1);
            }
        }

        private bool IsLocalShell(ConsoleType consoleType)
        {
            return consoleType == ConsoleType.DBShell && _Settings.DBConnection.ConnType == Settings.IOType.LocalShell;
        }

        private string StripResponseIfOFS(ConsoleType type, string response)
        {
            if (type == ConsoleType.OFS)
            {
                if (!string.IsNullOrEmpty(_Settings.OFSResponseStart) && !string.IsNullOrEmpty(_Settings.OFSResponseEnd))
                {
                    int poss = response.IndexOf(_Settings.OFSResponseStart);
                    int pose = response.IndexOf(_Settings.OFSResponseEnd);
                    if (poss > -1 && pose > -1 && pose >= poss + _Settings.OFSResponseStart.Length + _Settings.OFSResponseEnd.Length)
                    {
                        response = response.Substring(0, pose).Substring(poss + _Settings.OFSResponseStart.Length);
                    }
                }
            }

            return response;
        }

        private string StripCommandIfOFS(ConsoleType type, string command)
        {
            if (type == ConsoleType.OFS)
            {
                if (!string.IsNullOrEmpty(_Settings.OFSResponseStart) && !string.IsNullOrEmpty(_Settings.OFSResponseEnd))
                {
                    command = command.Substring(_Settings.OFSRequestStart.Length);
                    command = command.Substring(0, command.Length - _Settings.OFSRequestEnd.Length);
                }
            }

            return command;
        }

        private static string ResponseToStr(Queue<string> response, bool stripFirstLine, bool stripLastLine)
        {
            return StringMethods.ResponseToStr(response, stripFirstLine, stripLastLine);
        }

        private bool CustomResultCheck(ConsoleType consoleType, string command, string response, out CustomCommandResult cmdResult)
        {
            // Check if command has explicit defition
            if (CustomCommandDefinition != null)
            {
                cmdResult = CustomCommandDefinition;
                return CustomCommandDefinition.Execute(command, response);
            }

            // Check for command defitions in telnetcommdef.xml
            cmdResult = null;
            foreach (CustomCommandResult cmdRes in CustomCommandResults)
            {
                if (cmdRes.Interface == consoleType)
                {
                    if (cmdRes.Matches(command))
                    {
                        cmdResult = cmdRes;
                        return cmdRes.Execute(command, response);
                    }
                }
            }

            // TODO: If no response is defined for this command, default is true, maybe add more complex logic for this
            return true;
        }

        private ExecuteResult RunAndGetLastLine(ConsoleType consoleType, string command, out Queue<string> res, out string errorText, out string lastline, bool useCustomResultVerification = false, EndOfResponseCondition endOfResponseCondition = null, int timeout = -1)
        {
            ExecuteResult success = RunCommand(consoleType, command, out res, out errorText, endOfResponseCondition, timeout, useCustomResultVerification);

            if (consoleType != ConsoleType.HttpWebService && consoleType != ConsoleType.HttpRestService)
            {
                T24TelnetConnector connector = GetActivedConnectorByType(consoleType);
                lastline = connector.GetLastLineOfResponse(res);
            }
            else
            {
                lastline = res.Last();
            }
            return success;
        }

        private ExecuteResult AskUserToRetryCommand(string command, string errorText, out bool shouldRetry)
        {
            shouldRetry = false;

            CommandExecutionEventArgs.Action doAction;
            FireError(command, errorText, out doAction);
            switch (doAction)
            {
                case CommandExecutionEventArgs.Action.Abort:
                    this.AbortDeployment = true;
                    return ExecuteResult.Error;

                case CommandExecutionEventArgs.Action.Ignore:
                    return ExecuteResult.Ignored;

                case CommandExecutionEventArgs.Action.Retry:
                    shouldRetry = true;
                    return ExecuteResult.Error;

                default:
                    Debug.Fail("Unknown action");
                    return ExecuteResult.Error;
            }
        }

        private void FireError(string command, string errorMessage, out CommandExecutionEventArgs.Action doAction)
        {
            if (OnCommandExecutionError == null)
            {
                doAction = CommandExecutionEventArgs.Action.Abort;
                return;
            }

            CommandExecutionEventArgs evArgs = new CommandExecutionEventArgs("Error executing: " + command ?? "", errorMessage);
            OnCommandExecutionError(this, evArgs);

            doAction = evArgs.ErrorTreatAction;
        }

#if DEBUG
        public string BackupDirectoryOnServer_Test(string folder, bool recursive)
        {
            return BackupDirectoryOnServer(folder, recursive);
        }
#endif

        private string BackupDirectoryOnServer(string folder, bool recursive)
        {
            string errorText;

            string copyCommand;
            if (_OS == OperatingSystem.Win)
            {
                copyCommand = "xcopy ";
                copyCommand += recursive ? "/SIQY " : "/IQY ";
            }
            else
            {
                copyCommand = "cp ";
                copyCommand += recursive ? "-r " : "";
            }

            if (!recursive && _OS != OperatingSystem.Win)
            {
                copyCommand += folder;
                if (!copyCommand.EndsWith("/"))
                {
                    copyCommand += "/";
                }

                copyCommand += "* " + _BackupFolder;
            }
            else if (_OS == OperatingSystem.Win)
            {
                string tempFolder = folder;
                if (tempFolder.EndsWith("\\") || tempFolder.EndsWith("/"))
                {
                    tempFolder = tempFolder.Substring(0, tempFolder.Length - 1);
                }
                copyCommand += (tempFolder + " " + _BackupFolder);
            }
            else
            {
                copyCommand += (folder + " " + _BackupFolder);
            }

            if (_OS == OperatingSystem.Win)
            {
                string lastdir = folder.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries).Last();
                copyCommand += ("\\" + lastdir);
            }

            if (!recursive)
            {
                string[] arr = folder.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
                string lastDir = arr[arr.Length - 1];
                string innerBackupDirectory = FtpPath.Concat(_BackupFolder, lastDir);
                if (_OS == OperatingSystem.Win)
                {
                    innerBackupDirectory = innerBackupDirectory.Replace("/", "\\");
                }

                if (!CreateDirectory(innerBackupDirectory, out errorText))
                {
                    return "Unable to execute copy command: Cannot create target directory. " + errorText;
                }

                if (!copyCommand.EndsWith("\\") && !copyCommand.EndsWith("/"))
                {
                    copyCommand += (_OS == OperatingSystem.Win) ? "\\" : "/";
                }

                if (_OS != OperatingSystem.Win)
                {
                    copyCommand += lastDir;
                    copyCommand += (_OS == OperatingSystem.Win) ? "\\" : "/";
                }
            }

            Queue<string> copyCmdOutput;
            if (RunCommand(ConsoleType.Shell, copyCommand, out copyCmdOutput, out errorText, null, _Settings.CopyCommandTimeOut) == ExecuteResult.Error)
            {
                return errorText;
            }

            if (_OS == OperatingSystem.AIX)
            {
                if (copyCmdOutput.Count > 1)
                {
                    string[] response = copyCmdOutput.ToArray();

                    if (response.Length > 2)
                    {
                        List<string> tempResponse = new List<string>();
                        for (int i = 0; i < response.Length; i++)
                        {
                            if (_Settings.SkipLines.ShouldIgnore("cp", response[i]))
                            {
                                continue;
                            }

                            tempResponse.Add(response[i]);
                        }

                        response = tempResponse.ToArray();
                    }

                    if (response.Length == 2)
                    {
                        if (!response[1].Trim().EndsWith("$"))
                        {
                            return GenerateBackupErrorMessage(response);
                        }
                    }
                    else if (!recursive)
                    {
                        if (response.Length == 3)
                        {
                            if (response[1].Contains("The parameter list is too long"))
                            {
                                return GenerateBackupErrorMessage(response) + "Hint: increase the systems ARG/ENV list";
                            }
                        }
                        if (!response[response.Length - 1].Trim().EndsWith("$"))
                        {
                            return GenerateBackupErrorMessage(response);
                        }
                    }
                    else
                    {
                        return GenerateBackupErrorMessage(response);
                    }
                }
            }
            else if (_OS == OperatingSystem.Win)
            {
                if (copyCmdOutput.Count > 3)
                {
                    return GenerateBackupErrorMessage(copyCmdOutput.ToArray());
                }
            }

            return null;
        }

        private bool RestoreDirectory(string folder, out string errorText)
        {
            string copyCommand;
            string binSubdir = folder.Split(_PathDelimiter).Last();

            if (_OS == OperatingSystem.Win)
            {
                copyCommand = "xcopy /SIQY " + _BackupFolder + _PathDelimiter + binSubdir + " " + folder;
            }
            else
            {
                copyCommand = "cp -rf " + _BackupFolder + _PathDelimiter + binSubdir + " " +
                              folder.Remove(folder.LastIndexOf(_PathDelimiter));
            }

            Queue<string> copyCommandOutput;
            if (RunCommand(ConsoleType.Shell, copyCommand, out copyCommandOutput, out errorText) == ExecuteResult.Error)
            {
                errorText = string.Format("Could not restore {0}.\n{1}", folder, errorText);
                return false;
            }

            return true;
        }

        private bool GetEnvironmentVariableValue(string variableName, out string variableValue, out string errorText)
        {
            variableValue = "";

            Queue<string> res;
            string cmd = string.Format(GetJShellCommand(JShellCommandType.GetEnvironmentVariable).Command, variableName);
            if (RunCommand(ConsoleType.JShell, cmd, out res, out errorText) == ExecuteResult.Error)
            {
                return false;
            }

            List<string> resList = res.ToList();
            if (string.IsNullOrEmpty(resList[resList.Count - 2].Trim()))
            {
                errorText = string.Format("${0} is not defined.", variableName);
                return false;
            }

            variableValue = resList[resList.Count - 2].Trim();
            return true;
        }

        private bool LocateLibBinDir(out string errorText)
        {
            var hdCmd = GetJShellCommand(JShellCommandType.GetEnvironmentVariable).Command;
            if (string.IsNullOrEmpty(hdCmd))
            {
                Log.Info("Skipping locating bin and lib due to empty GetEnvironmentVariable");
                errorText = "";
                return true;
            }

            // once they are loaded, we don't need to query again
            if (!string.IsNullOrEmpty(_JLibDir) && !string.IsNullOrEmpty(_JBinDir) && !string.IsNullOrEmpty(_JGlobusLibDir))
            {
                errorText = null;
                return true;
            }

            bool resLib, resBin, resHome = true;
            string errorMsgLib = null, errorMsgBin = null, errorMsgHome = null;

            if (string.IsNullOrEmpty(_JLibDir))
            {
                resLib = GetEnvironmentVariableValue("JBCDEV_LIB", out _JLibDir, out errorMsgLib);
            }
            else
            {
                resLib = true;
            }

            if (string.IsNullOrEmpty(_JBinDir))
            {
                resBin = GetEnvironmentVariableValue("JBCDEV_BIN", out _JBinDir, out errorMsgBin);
            }
            else
            {
                resBin = true;
            }

            if (string.IsNullOrEmpty(_JGlobusBinDir) || string.IsNullOrEmpty(_JGlobusLibDir) ||
                string.IsNullOrEmpty(_JGlobusBPDir))
            {
                string envHomeDir;
                resHome = GetEnvironmentVariableValue("HOME", out envHomeDir, out errorMsgHome);

                if (string.IsNullOrEmpty(_JGlobusBinDir))
                {
                    _JGlobusBinDir = ConcatEnvPath(envHomeDir, "globusbin");
                }

                if (string.IsNullOrEmpty(_JGlobusLibDir))
                {
                    _JGlobusLibDir = ConcatEnvPath(envHomeDir, "globuslib");
                }

                if (string.IsNullOrEmpty(_JGlobusBPDir))
                {
                    _JGlobusBPDir = ConcatEnvPath(GetParentEnvPath(envHomeDir), "GLOBUS.BP");
                }
            }

            errorText = (errorMsgLib ?? "") + (errorMsgBin ?? "") + (errorMsgHome ?? "");
            return resLib && resBin && resHome;
        }

        private string GetParentEnvPath(string home)
        {
            StripLastPathDelimiter(ref home);
            int lastDelimiter = home.LastIndexOf(_PathDelimiter);
            if (lastDelimiter >= 0)
            {
                return home.Substring(0, lastDelimiter);
            }
            return null;
        }

        private void StripLastPathDelimiter(ref string home)
        {
            if (string.IsNullOrEmpty(home))
            {
                home = string.Empty;
                return;
            }

            int lastIndex = home.Length - 1;
            while (home[lastIndex] == _PathDelimiter)
            {
                lastIndex--;
            }

            if (lastIndex != (home.Length - 1))
            {
                home = home.Substring(0, lastIndex + 1);
            }
        }

        private string ConcatEnvPath(string firstDir, string subDir)
        {
            StripLastPathDelimiter(ref firstDir);

            return string.Concat(firstDir, _PathDelimiter, subDir);
        }

        private bool FindMaxIndexInBackupFolders(out string errorText, out int maxIndex)
        {
            string listFoldersCommand = (_OS == OperatingSystem.AIX)
                                     ?
                                         "ls -1d .*/ */"
                                     :
                                         "dir /B /AD";

            maxIndex = 0;
            Queue<string> res;
            if (RunCommand(ConsoleType.Shell, listFoldersCommand, out res, out errorText) == ExecuteResult.Error)
            {
                return false;
            }

            string folderPrefix = string.Format("{0}{1}", BackupFolderPrefix, DateTime.Now.ToString(BackupFolderDateFormat));
            int backupFullPrefixLength = BackupFolderPrefix.Length + 6; // 6 places for the date

            foreach (string dir in res.Take(res.Count - 1))
            {
                if (!dir.StartsWith(folderPrefix))
                {
                    continue;
                }

                string postfix = dir.Substring(backupFullPrefixLength).Trim();
                if (postfix.Length == 0)
                {
                    continue;
                }

                if (postfix.EndsWith("/") || postfix.EndsWith("\\"))
                {
                    postfix = postfix.Substring(0, postfix.Length - 1);
                }

                int index;
                if (int.TryParse(postfix, out index))
                {
                    if (index > maxIndex)
                    {
                        maxIndex = index;
                    }
                }
            }

            return true;
        }

        internal string ConstructObjectFileName(CompiledFile compiledFile, List<string> affectedFiles, bool isBeforeCompilation, out string errorText)
        {
            errorText = null;

            string objName = compiledFile.RoutineName.Replace(".", _Settings.ObjectDelimiter) +
                             "." +
                             _Settings.ObjectExtension;

            if (compiledFile.File.IsCatalog)
            {
                // First try with overrides in TelnetCommmDef.xml file
                if (compiledFile.File.ServerDeploymentFolder != null)
                    if (_Settings.ObjectDirOverrides.ContainsKey(compiledFile.File.ServerDeploymentFolder))
                    {
                        string foundPath = _Settings.ObjectDirOverrides[compiledFile.File.ServerDeploymentFolder];

                        if (foundPath.StartsWith("."))
                        {
                            foundPath = FtpPath.BuildX(HomeDir + _PathDelimiter, foundPath, FtpLogsSubdir);
                        }

                        if (!string.IsNullOrEmpty(foundPath))
                            if (foundPath[foundPath.Length - 1] != _PathDelimiter)
                            {
                                foundPath += _PathDelimiter;
                            }

                        return foundPath + objName;
                    }
            }

            string objFullName = GetObjFullName(compiledFile, objName, affectedFiles, isBeforeCompilation, out errorText);

            // Verify that the object really exists where we expect
            string listCommand = _OS == OperatingSystem.Win ? "dir /B " : "ls ", lastline;
            listCommand += objFullName;

            Queue<string> res;
            RunAndGetLastLine(ConsoleType.Shell, listCommand, out res, out errorText, out lastline);
            if (lastline != objName)
                if (lastline != objFullName)
                {
                    if (string.IsNullOrEmpty(errorText))
                    {
                        errorText = lastline;
                    }

                    objFullName = "";
                }

            return objFullName;
        }

        private string GetObjFullName(CompiledFile compiledFile, string objName, List<string> affectedFiles, bool isBeforeCompilation, out string errorText)
        {
            errorText = null;

            if (IsUniVerse)
            {
                string path = GetGEnvObjectDir(compiledFile.File.ServerDeploymentFolder, out errorText);
                if (path == null)
                {
                    return string.Empty;
                }

                return path + _PathDelimiter + objName;
            }

            {
                string dir = FullObjectDevDir;

                if (isBeforeCompilation)
                {
                    if (affectedFiles != null)
                        if (affectedFiles.Count > 0)
                        {
                            foreach (string s in affectedFiles)
                            {
                                string fileName;
                                string filePath;
                                BulkCompiler.SplitPath(s, out fileName, out filePath);
                                if (AreFoldersEqual(filePath, FullJGlobusLibDir))
                                {
                                    dir = FullObjectGlobusDir;
                                    break;
                                }
                            }
                        }
                }
                else
                {
                    if (IsGlobusBPFolder(compiledFile.File.ServerDeploymentFolder, compiledFile.File.IsCatalog))
                    {
                        dir = FullObjectGlobusDir;
                    }
                }

                return dir + _PathDelimiter + objName;
            }
        }

        internal bool IsGlobusBPFolder(string folder, bool isCatalog)
        {
            if (isCatalog)
            {
                int i = 0;
                for (; i < folder.Length; i++)
                {
                    if (folder[i] != '.')
                    {
                        break;
                    }
                }

                string temp = folder.Substring(i).Replace("/", "").Replace("\\", "");
                return (temp == GlobusBPCatalogName);
            }

            string a = FullJGlobusBPDir;
            if (a.EndsWith("/") || a.EndsWith("\\"))
            {
                a = a.Substring(0, a.Length - 1);
            }

            string b = FtpPath.BuildX(HomeDir, folder, FtpLogsSubdir);
            if (b.EndsWith("/"))
            {
                b = b.Substring(0, b.Length - 1);
            }

            return (a.Replace("\\", "/").ToUpperInvariant() == b.ToUpperInvariant());
        }

        private string GetGEnvObjectDir(string catalogName, out string errorMessage)
        {
            if (string.IsNullOrEmpty(catalogName) || catalogName.Trim() == string.Empty)
            {
                errorMessage = "Unable to obtain object directory: catalog name is blank!";
                return null;
            }

            Debug.Assert(IsUniVerse);

            catalogName += ".O";

            string[] result = ReadVOCRecord(catalogName, out errorMessage);
            if (result == null)
            {
                return null;
            }

            string name = "";
            if (result.Length >= 1)
            {
                name = result[0].Trim();
            }

            if (name.EndsWith("record not found"))
            {
                errorMessage = BuildErrMsg("Unable to obtain object directory [1]:\r\n", new string[] { name });
                return null;
            }

            if (result.Length < 3)
            {
                errorMessage = BuildErrMsg("Unable to obtain object directory [2]:\r\n", result);
                return null;
            }

            string entryType = result[1].Trim();
            if (!entryType.EndsWith(" F"))
            {
                errorMessage = BuildErrMsg("Entry type is not 'F':\r\n", result);
                return null;
            }

            string directory = result[2].Trim();
            int dirStartPos = directory.IndexOf(" ");
            if (dirStartPos < 0)
            {
                errorMessage = BuildErrMsg("Unable to obtain object directory [3]:\r\n", result);
                return null;
            }

            directory = directory.Substring(dirStartPos + 1);

            string homeDir = this.HomeDir;
            if (!string.IsNullOrEmpty(homeDir))
                if (homeDir[homeDir.Length - 1] != _PathDelimiter)
                {
                    homeDir += _PathDelimiter;
                }

            return FtpPath.BuildX(homeDir, directory, FtpLogsSubdir);
        }

        private static string BuildErrMsg(string msg, string[] result)
        {
            foreach (string s in result)
            {
                msg += s + "\r\n";
            }

            return msg;
        }

        private string[] ReadVOCRecord(string recordName, out string errorMessage)
        {
            if (string.IsNullOrEmpty(recordName) || recordName.Trim() == string.Empty)
            {
                errorMessage = "Unable to read from VOC - record name is blank!";
                return null;
            }

            string command = string.Format(GetJShellCommand(JShellCommandType.ListVOCRecord).Command, recordName);

            Queue<string> result;
            if (ExecuteJShellCommand(command, out result, out errorMessage, -1) == ExecuteResult.Error)
            {
                return null;
            }

            if (result.FirstOrDefault().Trim().EndsWith("record not found"))
            {
                //'ACCIASDASD' record not found
                //jsh migvb02 ~ -->
                errorMessage = result.FirstOrDefault();
                return null;
            }

            // Remove command echo
            List<string> tempRes = new List<string>(result);
            if (tempRes.Count > 0 && tempRes[0] == command)
            {
                tempRes.RemoveAt(0);
            }
            else
            {
                Debug.Fail("????");
            }

            // Remove j shell prompt
            if (tempRes.Count > 0 && tempRes[tempRes.Count - 1] == _JShellConnector.ShellPrompt)
            {
                tempRes.RemoveAt(tempRes.Count - 1);
            }
            else
            {
                Debug.Fail("????");
            }

            return tempRes.ToArray();
        }

        public CustomCommandResult CustomCommandDefinition
        {
            get; set;
        }

        public JShellCommand GetJShellCommand(JShellCommandType commandType)
        {
            if (_Settings.JShellCommands.ContainsKey(commandType))
            {
                return _Settings.JShellCommands[commandType];
            }
            else
                return null;
        }

        public bool InitializeHomeDir(out string errorText)
        {
            Queue<string> res;
            string lastResponseLine;

            var hdCmd = GetJShellCommand(JShellCommandType.GetWorkingDirectory).Command;
            if (string.IsNullOrEmpty(hdCmd))
            {
                Log.Info("Skipping homedir init due to empty GetWorkingDirectory");
                errorText = "";
                return true;
            }
            // this should work on Windows environment, too
            if (!_JShellConnector.RunTelnetCommandWithoutTerminatingString(hdCmd, out res, out errorText, out lastResponseLine))
            {
                return false;
            }

            Debug.Assert(!string.IsNullOrEmpty(lastResponseLine));

            if (string.IsNullOrEmpty(HomeDir))
            {
                _HomeDir = lastResponseLine;
            }
            else if (HomeDir != lastResponseLine)
            {
                // TODO - may be set a new home directory?
                Debug.Fail("Home directory has changed from the previous time! Old: " + HomeDir + " New: " + lastResponseLine);
            }

            return true;
        }

        private void CatalogDollarFile(string nameWithoutDollarPrefix, string folder, CompileOutput co)
        {
            string lastLine;
            Queue<string> cmdOutput;
            string errorText;
            JShellCommand jshCatalog = GetJShellCommand(JShellCommandType.Catalog);
            string catalogCommand = string.Format(jshCatalog.Command, folder, nameWithoutDollarPrefix);

            if (RunAndGetLastLine(ConsoleType.JShell, catalogCommand, out cmdOutput, out errorText, out lastLine) == ExecuteResult.Error)
            {
                // TODO this should be more detailed error
                co.SetTelnetError();
            }
            else
            {
                co.SetText(cmdOutput); // not sure if it is needed

                string successIndicatorLine = string.Format(jshCatalog.SuccessResponseRegex, nameWithoutDollarPrefix);

                Regex re = new Regex(successIndicatorLine);

                bool hasFoundSuccessIndicator = false;
                List<string> unexpectedLinesAfterSuccessIndicator = new List<string>();
                foreach (string line in cmdOutput)
                {
                    if (re.Match(line).Success)
                    {
                        hasFoundSuccessIndicator = true;
                    }
                    else
                    {
                        if (hasFoundSuccessIndicator)
                        {
                            if (line.EndsWith("rebuild okay"))
                            {
                                // we expect lines like 'Library C:\Temenos\MB\bnk\bnk.run\lib\lib2.dll rebuild okay' for subroutines
                                continue;
                            }

                            if (line == _JShellConnector.ShellPrompt)
                            {
                                // ignore prompt
                                continue;
                            }

                            unexpectedLinesAfterSuccessIndicator.Add(line);
                        }
                    }
                }

                if (!hasFoundSuccessIndicator)
                {
                    // We will have difficult parsing an error response - just attach it as a basic error
                    co.AddBasicError(QueueToString(cmdOutput));
                }
                else if (unexpectedLinesAfterSuccessIndicator.Count > 0)
                {
                    // Strangely, even if a success line is detected, some lines after that might indicate an error. Filter out the expected ones.
                    foreach (string unexpectedLine in unexpectedLinesAfterSuccessIndicator)
                    {
                        if (IsCatalogResponseLineExpected(unexpectedLine))
                        {
                            co.AddBasicError(unexpectedLine);
                        }
                    }
                }
            }
        }

        private bool IsCatalogResponseLineExpected(string line)
        {
            foreach (string lineRegex in _Settings.RegexesForIgnoredExtraLinesInCatalogCommandResponse)
            {
                if (lineRegex.Trim().Length == 0)
                {
                    continue;
                }

                Regex regex = new Regex(lineRegex);
                if (regex.IsMatch(line))
                {
                    return true;
                }
            }

            return false;
        }

        private void CompileAndCatalogFile(string sourceName, string routineName, string folder, CompileOutput co)
        {
            string lastLine;
            Queue<string> cmdOutput;
            string errorText;
            JShellCommand command = GetJShellCommand(JShellCommandType.CompileAndCatalog);

            if (command == null)
            {
                throw new Exception("Should have been handled already");
            }

            string cmd = string.Format(command.Command, folder, sourceName);
            if (RunAndGetLastLine(ConsoleType.JShell, cmd, out cmdOutput, out errorText, out lastLine) == ExecuteResult.Error)
            {
                // TODO this should be more detailed error
                co.SetTelnetError();
            }
            else
            {
                ProcessExtendedCompileResponse(cmdOutput, sourceName, co, command);
            }
        }

        private static void ProcessExtendedCompileResponse(Queue<string> response, string sourceName, CompileOutput co, JShellCommand compileCommand)
        {
            bool ebCompileSuccess = false;
            bool first = true;

            foreach (string line in response)
            {
                if (first)
                {
                    // Skip first line as it is the request line
                    first = false;
                    continue;
                }

                if (line.StartsWith("link") && line.Contains("failed"))
                {
                    co.AddLinkError(line);
                }
                if (line.Contains("LINK : fatal error"))
                {
                    co.AddLinkError(line);
                }
                if (line.Contains("Unable to rebuild library"))
                {
                    co.AddLinkError(line);
                }

                //if (line.Contains("Source file " + sourceName + " compiled successfully"))
                try
                {
                    if (compileCommand.CheckResponseSuccess(line))
                    {
                        ebCompileSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    ebCompileSuccess = true; // Cannot report success / fail as we don't know how to interpret the response when there is no regex
                }
            }

            // We can have successful BASIC, but failed EB.COMPILE (e.g. due to missing PROGRAM/SUBROUTINE header)
            if (!ebCompileSuccess)
            {
                co.AddBasicError(QueueToString(response));
            }
        }

        private bool BasicCompile(string sourceName, string folder, CompileOutput co)
        {
            string errorText;
            JShellCommand command = GetJShellCommand(JShellCommandType.Compile);
            string cmd = string.Format(command.Command, folder, sourceName);
            string lastLine;
            Queue<string> cmdOutput;
            if (RunAndGetLastLine(ConsoleType.JShell, cmd, out cmdOutput, out errorText, out lastLine) == ExecuteResult.Error)
            {
                co.SetTelnetError();
                return false;
            }

            co.SetText(cmdOutput);
            co.Scan();
            bool isSuccess = co.IsCompilationSuccessful(command);

            if (!isSuccess && co.Outcome == CompileOutcome.Success)
            {
                // just make sure that we show an error, even if we didn't found it
                co.AddBasicError(QueueToString(cmdOutput));
            }

            return isSuccess;
        }

        private void DeleteDollarFile(string folder, string sourceName)
        {
            string delLastLine;
            Queue<string> delCmdOutput;
            string delErrorText;
            string deleteCommand = string.Format(GetJShellCommand(JShellCommandType.DeleteDollarFile).Command, folder, sourceName);
            RunAndGetLastLine(ConsoleType.JShell, deleteCommand, out delCmdOutput, out delErrorText, out delLastLine);
            // TODO we might want to check the response to confirm that it was delete
        }

        private string GetTargetFolderInNormalizedForm(string folderInRawFormat, bool isCatalog)
        {
            if (isCatalog)
            {
                return folderInRawFormat;
            }

            string folder = folderInRawFormat.Replace('\\', _PathDelimiter).Replace('/', _PathDelimiter);

            if (folder.Trim() == string.Empty)
            {
                folder = ".";
            }

            folder = folder.TrimStart(new char[] { '/', '\\' });
            return folder;
        }

        private DeploymentOutput DeploySingleRecord(DeploymentRecord dRecord, string Version = null)
        {
            DeploymentOutput result = GetDefaultDeploymentOutput(dRecord);

            if (AbortDeployment)
            {
                result.Outcome = DeploymentOutcome.Skipped;
                result.Details = "Aborted deployment due to previous command error";
                return result;
            }

            if (Settings.NotAuthorizableApplications != null &&
                Settings.NotAuthorizableApplications.Contains(dRecord.TypicalName))
            {
                if (dRecord.DeploymentMethod == DeploymentMethod.Record_A)
                {
                    result.Outcome = DeploymentOutcome.Skipped;
                    result.DoesNotAffectDeploymentStatus = true;
                    result.Details = "The application is marked as not authorizable";
                    return result;
                }
                else if (dRecord.DeploymentMethod == DeploymentMethod.Record_IA)
                {
                    result.Method = DeploymentMethod.Record_I;
                    dRecord.DeploymentMethod = DeploymentMethod.Record_I;
                }
            }

            try
            {
                bool skipAuthorization;
                switch (dRecord.DeploymentMethod)
                {
                    case DeploymentMethod.Record_I:
                        InputRecordInT24(dRecord, result, out skipAuthorization, Version);
                        break;
                    case DeploymentMethod.Record_A:
                        AuthorizeRecordInT24(dRecord, result);
                        break;
                    case DeploymentMethod.Record_IA:
                        InputRecordInT24(dRecord, result, out skipAuthorization, Version);
                        // TODO we might have used a self-authorizing version, so authorization might be unnecessary in all cases
                        if ((result.Outcome == DeploymentOutcome.Successful || dRecord.SkipChecksForAuthorization) && !skipAuthorization)
                        {
                            AuthorizeRecordInT24(dRecord, result);
                        }
                        break;
                    case DeploymentMethod.Record_V:
                        VerifyRecordInT24(dRecord, result, Version); // V/PROCESS,@ID
                        break;
                    default:
                        throw new ApplicationException("Invalid deployment method for records: " + dRecord.DeploymentMethod);
                }

                return result;
            }
            catch (Exception ex)
            {
                result.Outcome = DeploymentOutcome.Failed;
                result.Details = ex.Message;
                return result;
            }
        }

        private static DeploymentOutput GetDefaultDeploymentOutput(DeploymentRecord record)
        {
            return new DeploymentOutput
            {
                Outcome = DeploymentOutcome.Failed,
                UnitType = UnitTypes.Record,
                DeploymentType = DeploymentTypes.Direct,
                PackName = record.DeploymentPack,
                TypicalName = record.TypicalName,
                UnitName = record.RecordName,
                UnitVersion = record.RecordVersion,
                DeployableInstanceToT24 = record.RecordInstance,
                InitialInstanceOnT24 = record.ExistingRecordInstance,
                CompanyName = record.Company,
                Details = "",
                Method = record.DeploymentMethod,
                Record = record
            };
        }

        private void InputRecordInT24(DeploymentRecord dRecord, DeploymentOutput result, out bool skipAuth, string _Version = null)
        {
            skipAuth = false;
            FieldInfoList versionFields;
            if (dRecord.HasSpecifiedVersion)
            {
                versionFields = dRecord.DeploymentVersion.Fields;
                TypicalFieldsAnalyzer.MarkSystemFieldsAsNonSychronizable(versionFields, RequestManager.RecordsSystemFields);

            }
            else
                versionFields = TypicalFieldsAnalyzer.DeduceApplicationFieldsFromInstance(dRecord.RecordInstance);


            result.ApplicationFields = versionFields;

            string applicationName = dRecord.TypicalName;

            List<DeploymentFieldValues> fieldValues;

            // ABC-331: disable routines in case of post bcon IA with no values

            Routine routineToRun = dRecord.IgnoreSpecialRoutine ? null : _Settings.T24Routines.GetRoutineByApplication(applicationName);
            RecordDataSynchronizer recDataSync = new RecordDataSynchronizer();
            if (routineToRun == null || routineToRun.EnableSmartOFS)
            {
                fieldValues =
                    recDataSync.GetAttributeValuesSynchronized(
                        versionFields,
                        dRecord.RecordInstance,
                        dRecord.ExistingRecordInstance,
                        _Settings.SkipWhiteSpaceInFieldValue,
                        dRecord.SkipValueDeletions ?? _RecordDeploymentPreventDeletionDefault,
                        dRecord.InputSameValues ?? _RecordDeploymentInputUnchangedDefault);
            }
            else
            {
                fieldValues = recDataSync.GetAttributeValues(versionFields, dRecord.RecordInstance, dRecord.ExistingRecordInstance);
            }

            result.FieldsUpdatedInRecord = fieldValues;

            if (fieldValues.Count == 0 && dRecord.ExistingRecordInstance != null)
            {
                result.Outcome = DeploymentOutcome.Skipped;
                result.DoesNotAffectDeploymentStatus = true;
                result.Details = "No values to update in the existing T24 record";
                return;
            }

            string version = (_Version != null) ? _Version : dRecord.VersionName;
            if (version == NoVersionReservedName)
            {
                // deploy with the default application
                version = ",";
            }
            if (!version.StartsWith(","))
            {
                version = "," + version;
            }
            result.DeploymentVersionName = (version == ",") ? "" : version;

            string company = GetCompany(dRecord, _Settings);
            string userNamePasswordCompany = _Settings.InputUser + "/" + _Settings.InputPassword;
            if (!string.IsNullOrEmpty(company))
            {
                userNamePasswordCompany += "/" + company;
            }

            string t24Id = GlobusRequestBase.GetEscapedID(dRecord.RecordID);

            List<InstanceAttribute> targetAttributes = fieldValues.ConvertAll(fv => new InstanceAttribute(fv.FieldName, fv.TargetValue));

            string ofsMesageBody = FormatFieldValuesForOfsMessage(targetAttributes);

            bool skipVals;
            if (dRecord.SkipValueDeletions != null)
                skipVals = (bool)dRecord.SkipValueDeletions;
            else
                skipVals = _RecordDeploymentPreventDeletionDefault;

            bool deployInputUnchanged;
            if (dRecord.InputSameValues != null)
                deployInputUnchanged = (bool)dRecord.InputSameValues;
            else
                deployInputUnchanged = _RecordDeploymentInputUnchangedDefault;

            string ofsReplace = string.Empty;
            if (skipVals && deployInputUnchanged && (routineToRun == null) && dRecord.UseReplaceOption)
            {
                if (!string.IsNullOrEmpty(company) && _OFSReplaceText.Length>1)
                {
                    ofsReplace = _OFSReplaceText.Remove(0,1);
                }
                else
                {
                    ofsReplace = _OFSReplaceText;
                }
            }
                

            //todo: if Company is set, ofsReplace should be ///

            string ofsCommand = applicationName +
                             version +
                             "/I/PROCESS," +
                             userNamePasswordCompany + ofsReplace + "," +
                             t24Id +
                             ofsMesageBody;

            // in some cases we need to run a special routine, instead of calling directly a T24 application
            if (routineToRun != null)
            {
                string errorTextRoutine;
                bool ok = DeployRecordUsingCustomRoutine(routineToRun, ofsCommand, out errorTextRoutine);
                if (!ok)
                {
                    result.Details = errorTextRoutine;
                    return;
                }

                if (routineToRun.PostCallActions == Routine.SequenceToA.Nothing)
                    skipAuth = true;

                string ofsAction;
                if (routineToRun.PostCallActions == Routine.SequenceToA.IA)
                {
                    // send an OFS wth just INPUT in order to move the record to INAU status
                    ofsAction = "I";
                }
                else
                {
                    // the command will be changed to a SEE command to verify the record existance 
                    ofsAction = "S";
                }

                ofsCommand = applicationName +
                          version +
                          "/" + ofsAction + "/PROCESS," +
                          _Settings.InputUser + "/" + _Settings.InputPassword + ","
                          + // TODO should we put company here? Most likely it is not used for system applicaiton like SS, so no need
                          t24Id;
            }

            string errorText;
            string lastResponseLine;

            ExecuteResult success = RunOfsCommand(ofsCommand, out lastResponseLine, out errorText, -1);
            if (success == ExecuteResult.Error)
            {
                result.Details = errorText;
                return;
            }

            ParseOfsResponseForInputCommand(result, dRecord, lastResponseLine, out skipAuth);
        }

        private static void ParseOfsResponseForInputCommand(DeploymentOutput result, DeploymentRecord dRecord, string lastResponseLine, out bool skipAuth)
        {
            skipAuth = false;

            // TODO we should better use OFSResponseReader here instead of duplicating its logic
            bool inputSuccess = OFSResponseReader.IsOfsResponseSuccessful(lastResponseLine, dRecord.RecordID, dRecord.AllowDifferentResponseID);
            if (!inputSuccess)
            {
                if (lastResponseLine.TrimEnd().EndsWith("/-1/NO,LIVE RECORD NOT CHANGED"))
                {
                    result.Details = "LIVE RECORD NOT CHANGED";
                    result.Outcome = DeploymentOutcome.Skipped;
                    result.DoesNotAffectDeploymentStatus = true;
                    skipAuth = true;
                    return;
                }
                else
                {
                    result.Details = lastResponseLine;
                    return;
                }
            }

            // read the resulting instance of the deployment
            result.Details = GetRecordChangeDetailsAfterInput(
                                    dRecord.DeploymentMethod,
                                    result.InitialInstanceOnT24 == null,
                                    result.FieldsUpdatedInRecord
                                );

            result.UpdatedInstanceOnT24 = ReadUpdatedInstanceFromOfsResponse(dRecord, lastResponseLine);

            // set field values after deployment, for the user to inspect them easily
            FillFinalValuesForFieldsAttemptedToBeSet(result);

            result.Outcome = DeploymentOutcome.Successful;
        }

        private void AuthorizeRecordInT24(DeploymentRecord dRecord, DeploymentOutput result)
        {
            string id;
            Instance currentT24Instance;
            switch (dRecord.DeploymentMethod)
            {
                case DeploymentMethod.Record_IA:
                    currentT24Instance = result.UpdatedInstanceOnT24;
                    id = (currentT24Instance != null || !dRecord.SkipChecksForAuthorization) ?
                        currentT24Instance.GetAttributeValue("@ID")
                        :
                        dRecord.RecordInstance.GetAttributeValue("@ID")
                        ;
                    break;
                case DeploymentMethod.Record_A:
                    currentT24Instance = result.InitialInstanceOnT24;
                    id = dRecord.RecordInstance.GetAttributeValue("@ID");
                    break;
                default:
                    throw new ApplicationException("Only A and IA deployment methods are expected");
            }

            if (string.IsNullOrEmpty(id))
            {
                result.Details = "Cannot authorize record with empty @ID";
                result.Outcome = DeploymentOutcome.Failed;
                return;
            }

            if (!dRecord.SkipChecksForAuthorization && (currentT24Instance == null))
            {
                result.Details = "Can not authorize a missing record";
                result.Outcome = DeploymentOutcome.Failed;
                return;
            }

            if (!dRecord.SkipChecksForAuthorization && IsRecordAuthorized(currentT24Instance) && dRecord.DeploymentMethod == DeploymentMethod.Record_A)
            {
                result.Details = "The record in T24 has already been authorized";
                result.DoesNotAffectDeploymentStatus = true;
                result.Outcome = DeploymentOutcome.Skipped;
                return;
            }

            string company = GetCompany(dRecord, _Settings);
            string userNamePasswordCompany = _Settings.AuthorizationUser + "/" + _Settings.AuthorizationPassword;
            if (!string.IsNullOrEmpty(company))
            {
                userNamePasswordCompany += "/" + company;
            }

            string ofsCmd = "A";
            // Some records require VERIFY instead of AUTHORIZE
            // TODO make it configurable
            if (dRecord.TypicalName == "EB.DEV.HELPER")
            {
                ofsCmd = "V";
            }

            string command = string.Format("{0},/{1}/PROCESS,{2},{3}",
                dRecord.TypicalName, ofsCmd, userNamePasswordCompany, GlobusRequestBase.GetEscapedID(id));

            string errorText;
            string lastResponseLine;
            ExecuteResult success = RunOfsCommand(command, out lastResponseLine, out errorText, -1);
            if (success == ExecuteResult.Error)
            {
                result.Outcome = DeploymentOutcome.Failed;
                result.Details = errorText;
                return;
            }

            // process OFS response
            string details;
            DeploymentOutcome outcome = ParseOfsResponseForAuthorizeOrVerifyCommand(ofsCmd, lastResponseLine, out details);

            if (dRecord.DeploymentMethod == DeploymentMethod.Record_A)
            {
                result.Outcome = outcome;
                result.Details = details;
            }
            else if (dRecord.DeploymentMethod == DeploymentMethod.Record_IA)
            {
                if (outcome == DeploymentOutcome.Skipped)
                {
                    result.Details += " NB: The record was already authorized";
                }

                if (outcome == DeploymentOutcome.Failed)
                {
                    result.Outcome = outcome;
                    result.Details = details;
                }

                if (outcome == DeploymentOutcome.Successful)
                {
                    result.UpdatedInstanceOnT24 = ReadUpdatedInstanceFromOfsResponse(dRecord, lastResponseLine);
                    FillFinalValuesForFieldsAttemptedToBeSet(result);
                }
            }
            else
            {
                Debug.Fail("Unexpected deployment method when authorizing");
            }
        }

        private void VerifyRecordInT24(DeploymentRecord dRecord, DeploymentOutput result, string _Version = null)
        {
            Instance currentT24Instance = dRecord.RecordInstance;
            string id = dRecord.RecordInstance.GetAttributeValue("@ID");

            if (string.IsNullOrEmpty(id))
            {
                result.Details = "Cannot verify record with empty @ID";
                result.Outcome = DeploymentOutcome.Failed;
                return;
            }

            if (currentT24Instance == null)
            {
                result.Details = "Can not verify a missing record";
                result.Outcome = DeploymentOutcome.Failed;
                return;
            }

            string company = GetCompany(dRecord, _Settings);
            string userNamePasswordCompany = _Settings.InputUser + "/" + _Settings.InputPassword;
            if (!string.IsNullOrEmpty(company))
            {
                userNamePasswordCompany += "/" + company;
            }

            string version = (_Version != null) ? _Version : dRecord.VersionName;
            if (version == NoVersionReservedName)
            {
                // deploy with the default application
                version = ",";
            }
            if (!version.StartsWith(","))
            {
                version = "," + version;
            }
            result.DeploymentVersionName = (version == ",") ? "" : version;
            const string ofsCmd = "V";
            string command = string.Format(
                    "{0}{4}/{1}/PROCESS,{2},{3}",
                    dRecord.TypicalName,
                    ofsCmd,
                    userNamePasswordCompany,
                    GlobusRequestBase.GetEscapedID(id),
                    version
                );

            string errorText;
            string lastResponseLine;
            ExecuteResult success;

            EndOfResponseCondition endOfResponseCondition =
                _Settings.OFSResultHandling.GetEndOfResponseCondition("V", dRecord.FullVersionName);

            CustomCommandResult customCommandResult =
                _Settings.OFSResultHandling.GetCustomCommandResult("V", dRecord.FullVersionName);

            string details = string.Empty;
            if (endOfResponseCondition != null && customCommandResult != null)
            {
                // USAGE IN TELNETCOMMMDEF.XML
                /*
                <OFSResultHandling>
				<OFSCommand action="V" version="BUILD.CONTROL">
					<CommandResult isSuccess="true" isRegEx="false" isPartial="true">Transfer of records to data accounts complete.</CommandResult>
					<EndOfResponseCondition timeOut="100000" isRegEx="true">NORMAL TERMINATION</EndOfResponseCondition>
				</OFSCommand>
			    </OFSResultHandling>
                */

                // hack of the inner logic
                string originalResponseStart = _Settings.OFSResponseStart;
                string originalResponseEnd = _Settings.OFSResponseEnd;
                CustomCommandResult originalCustomCommandResult = CustomCommandDefinition;
                _Settings.OFSResponseStart = string.Empty;
                _Settings.OFSResponseEnd = string.Empty;
                try
                {
                    CustomCommandDefinition = customCommandResult;
                    Queue<string> resultLines;
                    success = RunOfsCommand(command, out lastResponseLine, out errorText, true, endOfResponseCondition, out resultLines, -1);
                    foreach (string s in resultLines)
                    {
                        details += s + "\r\n";
                    }
                }
                finally
                {
                    _Settings.OFSResponseEnd = originalResponseEnd;
                    _Settings.OFSResponseStart = originalResponseStart;
                    CustomCommandDefinition = originalCustomCommandResult;
                }
            }
            else
            {
                Queue<string> resultLines;
                success = RunOfsCommand(command, out lastResponseLine, out errorText, false, null, out resultLines, -1);
                foreach (string s in resultLines)
                {
                    details += s + "\r\n";
                }
            }

            if (success == ExecuteResult.Error)
            {
                result.Outcome = DeploymentOutcome.Failed;
                result.Details = errorText;
                return;
            }

            if (endOfResponseCondition != null && customCommandResult != null)
            {
                result.Outcome = DeploymentOutcome.Successful;
            }
            else
            {
                result.Outcome = ParseOfsResponseForAuthorizeOrVerifyCommand(ofsCmd, lastResponseLine, out details);
            }

            result.Details = details;
        }

        private static void FillFinalValuesForFieldsAttemptedToBeSet(DeploymentOutput result)
        {
            if (result.UpdatedInstanceOnT24 != null)
            {
                foreach (DeploymentFieldValues field in result.FieldsUpdatedInRecord)
                {
                    field.FinalValue = result.UpdatedInstanceOnT24.GetAttributeValue(field.FieldName, true);
                }
            }
        }

        private static DeploymentOutcome ParseOfsResponseForAuthorizeOrVerifyCommand(string command, string lastResponseLine, out string details)
        {
            // TODO we should better use OFSResponseReader here instead of duplicating its logic
            string[] ofsStatements = lastResponseLine.Split(',');

            bool ofsSuccess = false;
            string[] twoSlashes = { "/" };
            string[] ofsSplit = lastResponseLine.Split(twoSlashes, StringSplitOptions.None);
            if (ofsSplit.Length >= 2)
            {
                if (ofsSplit[2].StartsWith("1"))
                {
                    ofsSuccess = true;
                }
            }

            if (ofsStatements.Length <= 1 || !ofsSuccess)
            {
                if (command == "A")
                {
                    if (lastResponseLine.Contains("UNAUTH. RECORD MISSING"))
                    {
                        details = "Missing record to authorize";
                        return DeploymentOutcome.Skipped;
                    }
                    else
                    {
                        details = lastResponseLine;
                        return DeploymentOutcome.Failed;
                    }
                }
                else
                {
                    Debug.Assert(command == "V");
                    details = lastResponseLine;
                    return DeploymentOutcome.Failed;
                }
            }

            details = (command == "A") ? "Record authorized" : "Record verified";
            return DeploymentOutcome.Successful;
        }

        private bool DeployRecordUsingCustomRoutine(Routine routineToRun, string ofsMessage, out string errorText)
        {
            // obsolete: bool deployDirect = ConfigurationManager.AppSettings["UseTempFileToInputRecordWithCustomRoutine"] == "false";

            if (routineToRun.CallType == Routine.CallArgumentType.UsingOFS)
            {
                return DeployRecordUsingCustomRoutineDirect(routineToRun.Name + (this._OS == OperatingSystem.Win ? "\r\n" : "\n") + ofsMessage, out errorText);
            }
            else
            {
                Debug.Assert(routineToRun.CallType == Routine.CallArgumentType.UsingFile);
                return DeployRecordUsingCustomRoutineUsingTempFile(routineToRun.Name, ofsMessage, out errorText);
            }
        }

        private bool DeployRecordUsingCustomRoutineUsingTempFile(string routineToRun, string ofsMessage, out string errorText)
        {
            string tempFileName = "ofs" + Guid.NewGuid().ToString("N"); // this should be unique an valid for a file name
            if (!AppendOfsMessageToFile(tempFileName, ofsMessage, out errorText))
            {
                errorText = string.Format("Could not write OFS message to temp file '{0}' . Details: {1}", tempFileName, errorText);
                return false;
            }

            bool res;
            try
            {
                res = DeployRecordUsingCustomRoutineDirect(routineToRun + (this._OS == OperatingSystem.Win ? "\r\n" : "\n") + tempFileName, out errorText);
            }
            finally
            {
                //  we might want to delete the temp file, but it's good if it stays for troubleshooting purposes
                if (ConfigurationManager.AppSettings["DeleteTempFileForInputtingRecordWithCustomRoutine"] == "true")
                {
                    DeleteFile(tempFileName, out errorText);
                }
            }

            return res;
        }

        private bool AppendOfsMessageToFile(string fileName, string ofsMessage, out string errorText)
        {
            bool isInMsDos = _OS == OperatingSystem.Win;
            int portionLength = isInMsDos ? 200 : 900; // the total length should be max 1023, but in DOS we have problems with > 256 characters, so just less.

            IEnumerable<string> portionsForEchoing;

            if (isInMsDos)
            {
                if (ofsMessage.Contains("=") && ofsMessage.Last() != ',')
                {
                    // in MS-DOS there is a limitation with echo-ing commands that end with '=D' where D is any digit. Just put a harmless commaat the end of OFS command to avoid issues.
                    ofsMessage += ",";
                }

                portionsForEchoing = SplitByCommaAndEscapeOfsCommandForEchoingInMsDos(ofsMessage, portionLength);
            }
            else
            {
                portionsForEchoing = SplitAndEscapeTextInPortionsForEchoing(ofsMessage, portionLength, false);
            }

            foreach (string portion in portionsForEchoing)
            {
                string lastLine;
                Queue<string> res;
                string spaceOrEmpty = isInMsDos ? "" : " ";
                string echoCommand = string.Format("echo {0}{1}>> {2}", portion, spaceOrEmpty, fileName);
                if (RunAndGetLastLine(ConsoleType.Shell, echoCommand, out res, out errorText, out lastLine) == ExecuteResult.Error)
                {
                    return false;
                }
            }

            errorText = null;
            return true;
        }

        internal static IEnumerable<string> SplitTextToPortionsByComma(string text)
        {
            string[] items = text.Split(new[] { "," }, StringSplitOptions.None);
            for (int i = 0; i < items.Length; i++)
            {
                string item = items[i];
                if (i != items.Length - 1)
                {
                    // only the last item would not have a comma appended
                    item += ",";
                }

                if (item.Length > 0)
                    yield return item;
            }
        }

        internal static IEnumerable<string> SplitByCommaAndEscapeOfsCommandForEchoingInMsDos(string text, int recommendedPortionLength)
        {
            foreach (string portion in SplitTextToPortionsByComma(text))
            {
                string escapedPortion = EscapeCommandForMsDosEcho(portion);
                if (escapedPortion.Length > recommendedPortionLength)
                {
                    // TODO we could try to split a long field value, too, but not in this version
                    throw new ApplicationException(string.Format(
                        "The OFS message [{0}] contains a field value that is unexpectedly long, and thus can't be processed! The problematic portion is [{1}]", text, portion));
                }

                yield return escapedPortion;
            }
        }

        internal static IEnumerable<string> SplitAndEscapeTextInPortionsForEchoing(string text, int recommendedPortionLength, bool isForMsDos)
        {
            string escapedText = isForMsDos ? EscapeCommandForMsDosEcho(text) : EscapeCommandForUnixShellEcho(text);
            char escapeChar = isForMsDos ? '^' : '\\';

            int processedChars = 0;
            int totalChars = escapedText.Length;
            while (processedChars < totalChars)
            {
                int remainingChars = totalChars - processedChars;
                if (remainingChars <= recommendedPortionLength)
                {
                    string lastPortion = escapedText.Substring(processedChars, remainingChars);
                    if (!isForMsDos)
                    {
                        lastPortion = EncloseWithQuotes(lastPortion);
                    }
                    yield return lastPortion;
                    break;
                }
                else
                {
                    int portionLength = recommendedPortionLength;

                    // make sure we don't end with the escape character since this will be an invalid command
                    if (escapedText[processedChars + portionLength - 1] == escapeChar)
                    {
                        int numberOfConsequtiveBackSlashesAfterRecommendedPortion = 0;
                        for (int i = processedChars + recommendedPortionLength; i < totalChars; i++)
                        {
                            if (escapedText[i] != escapeChar)
                            {
                                break;
                            }

                            numberOfConsequtiveBackSlashesAfterRecommendedPortion++;
                        }

                        // include the character after the last backslash
                        portionLength += (numberOfConsequtiveBackSlashesAfterRecommendedPortion + 1);

                        if (portionLength > remainingChars)
                        {
                            portionLength = remainingChars;
                        }
                    }

                    string portion = escapedText.Substring(processedChars, portionLength);
                    processedChars += portionLength;

                    if (!isForMsDos)
                    {
                        portion = EncloseWithQuotes(portion);
                    }

                    yield return portion;
                }
            }
        }

        private static string EscapeCommandForUnixShellEcho(string command)
        {
            // see http://docsrv.sco.com:507/en/man/html.C/echo.C.html for details
            string result = command.Replace(@"\", @"\\\"); // escape backslash, since it is a reserved escape character
            result = result.Replace("\"", "\\\""); // escape quote with a backslash
            return result;
        }

        private static string EscapeCommandForUnixShell(string command)
        {
            //see http://tldp.org/LDP/abs/html/special-chars.html for details
            var charsToEscape = new[]
                                    {
                                        "$","#"
                                    };

            return charsToEscape.Aggregate(command, (current, ch) => current.Replace(ch, string.Format("\\{0}", ch)));
        }

        private static string EscapeCommandForMsDosEcho(string command)
        {
            // see http://www.experts-exchange.com/OS/Microsoft_Operating_Systems/MS_DOS/Q_20460692.html
            command = command.Replace("^", "^^");

            const char escapeChar = '^';
            char[] specialChars = new char[] { '<', '>', '|', ')', '(', '&' };
            foreach (char c in specialChars)
            {
                string replacement = new string(new char[] { escapeChar, c });
                command = command.Replace(c.ToString(), replacement);
            }

            return command;
        }

        private static string EncloseWithQuotes(string str)
        {
            return "\"" + str + "\"";
        }

        private bool DeployRecordUsingCustomRoutineDirect(string command, out string errorText)
        {
            string lastLine;
            Queue<string> res;

            var connector = GetActivedConnectorByType(ConsoleType.JShell);
            var endOfResponseCondition = new EndOfResponseCondition(connector.ShellPrompt);
            endOfResponseCondition.DontExpectCommandEcho = true;
            if (RunAndGetLastLine(ConsoleType.JShell, command, out res, out errorText, out lastLine, false, endOfResponseCondition) == ExecuteResult.Error)
            {
                return false;
            }

            if (lastLine != "SUCCESS")
            {
                return false;
            }

            return true;
        }

        private ExecuteResult RunOfsCommand(string command, out string lastResponseLine, out string errorText, int timeout)
        {
            return RunOfsCommand(command, out lastResponseLine, out errorText, false, null, timeout);
        }

        private ExecuteResult RunOfsCommand(string command, out string lastResponseLine, out string errorText, bool useCustomResultVerification, EndOfResponseCondition endOfResponseCondition, int timeout)
        {
            Queue<string> res;
            return RunOfsCommand(command, out lastResponseLine, out errorText, useCustomResultVerification, endOfResponseCondition, out res, timeout);
        }

        private ExecuteResult RunOfsCommand(string command, out string lastResponseLine, out string errorText, bool useCustomResultVerification, EndOfResponseCondition endOfResponseCondition, out Queue<string> res, int timeout)
        {
            ExecuteResult execResult;

            ConsoleType consoleType = ConsoleType.OFS;
            switch (_Settings.Type)
            {
                case Settings.IOType.SharpSsH:
                case Settings.IOType.SSH:
                case Settings.IOType.Telnet:
                    {
                        command = _Settings.OFSRequestStart + command + _Settings.OFSRequestEnd;
                    }
                    break;
                case Settings.IOType.HttpWebService:
                    {
                        consoleType = ConsoleType.HttpWebService;
                    }
                    break;
                case Settings.IOType.HttpRestService:
                    {
                        consoleType = ConsoleType.HttpRestService;
                    }
                    break;
                default:
                    throw new InvalidOperationException("Unkown OFS communication type chanel");
            }

            if (string.IsNullOrEmpty(_Settings.OFSResponseEnd))
            {
                // TODO should wait for at least one line response
                execResult = RunAndGetLastLine(consoleType, command, out res, out errorText, out lastResponseLine, useCustomResultVerification, endOfResponseCondition, timeout);

                if (execResult == ExecuteResult.Error)
                {
                    return execResult;
                }
            }
            else
            {
                // wait for _Settings.OFSRequestEnd
                execResult = RunAndGetLastLine(consoleType, command, out res, out errorText, out lastResponseLine, useCustomResultVerification, new EndOfResponseCondition(_Settings.OFSResponseEnd), timeout);

                if (execResult != ExecuteResult.Success)
                {
                    return execResult;
                }

                lastResponseLine = res.Last();
            }

            // clear the Start/End responses (but are we really getting those)
            if (!string.IsNullOrEmpty(_Settings.OFSResponseStart))
            {
                int pos = lastResponseLine.IndexOf(_Settings.OFSResponseStart);
                if (pos >= 0)
                {
                    lastResponseLine = lastResponseLine.Substring(pos + _Settings.OFSResponseStart.Length);
                }
                else if (res.Count > 2)
                {
                    string[] arrRes = res.ToArray();
                    for (int i = 1; i < arrRes.Length - 1; i++)
                    {
                        pos = arrRes[i].IndexOf(_Settings.OFSResponseStart);
                        if (pos >= 0)
                        {
                            StringBuilder sbResult = new StringBuilder();
                            sbResult.Append(arrRes[i].Substring(pos + _Settings.OFSResponseStart.Length)
                            );
                            for (int j = i + 1; j < arrRes.Length; j++)
                            {
                                sbResult.Append(arrRes[j]);
                            }

                            lastResponseLine = sbResult.ToString();
                            break;
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(_Settings.OFSResponseEnd))
            {
                int pos = lastResponseLine.IndexOf(_Settings.OFSResponseEnd);
                if (pos >= 0)
                {
                    lastResponseLine = lastResponseLine.Substring(0, pos);
                }
            }

            return ExecuteResult.Success;
        }

        #endregion

        #region Private Static Methods

        private static string FormatFieldValuesForOfsMessage(IEnumerable<InstanceAttribute> attributes)
        {
            StringBuilder ofsMessageBuilder = new StringBuilder();
            foreach (InstanceAttribute attr in attributes)
            {
                int mv, sv;
                string t24FieldName = GetFieldNameComponents(attr.Name, out mv, out sv);
                string fieldMsgPart = "," + t24FieldName + ":" + mv + ":" + sv + "=" + OFSMessageConverter.ImportMsgSubstChars(attr.Value);
                ofsMessageBuilder.Append(fieldMsgPart);
            }
            return ofsMessageBuilder.ToString();
        }

        private static string GetFieldNameComponents(string validataFieldName, out int mv, out int sv)
        {
            string t24FieldName;
            if (!AttributeNameParser.ParseComplexFieldName(validataFieldName, out t24FieldName, out mv, out sv))
            {
                mv = 1;
                sv = 1;
            }

            return t24FieldName;
        }

        private static string GenerateBackupErrorMessage(string[] response)
        {
            StringBuilder error = new StringBuilder();

            // TODO why do we start with 1? Probably it is the command itself
            for (int i = 1; i < response.Length; i++)
            {
                error.AppendLine(response[i]);
                if (i > 5)
                {
                    error.Append("...");
                    break;
                }
            }

            return error.ToString();
        }

        private static bool IsMatched(string sasFieldName, string sasFieldValue, string[] ofsStatements, out string t24value)
        {
            int mv, sv;
            string t24FieldName = GetFieldNameComponents(sasFieldName, out mv, out sv);
            string fullT24FieldName = t24FieldName + ":" + mv + ":" + sv;

            t24value = "";
            bool isMatch = false;
            foreach (string statement in ofsStatements)
            {
                if (statement.StartsWith(fullT24FieldName + "="))
                {
                    if (statement.Split('=')[1] == sasFieldValue)
                    {
                        isMatch = true;
                        break;
                    }
                    else
                    {
                        t24value = statement.Split('=')[1];
                        break;
                    }
                }
            }

            return isMatch;
        }

        private static Dictionary<TKey, TValue> Merge<TKey, TValue>(IEnumerable<Dictionary<TKey, TValue>> dictionaries)
        {
            var result = new Dictionary<TKey, TValue>();
            foreach (var dict in dictionaries)
            {
                foreach (var key in dict.Keys)
                {
                    if (!result.ContainsKey(key))
                    {
                        result.Add(key, dict[key]);
                    }
                }

            }

            return result;
            // was: return dictionaries.SelectMany(x => x).ToDictionary(x => x.Key, y => y.Value);
        }

        private static bool IsValidHash(string hash)
        {
            if (hash == null)
            {
                return false;
            }

            if (hash.Length != 32)
            {
                return false;
            }

            foreach (char c in hash)
            {
                // chech for valid hexadecimal character
                if ((c >= 'a' && c <= 'f') || (c >= '0' && c <= '9'))
                {
                    continue;
                }
                return false;
            }

            return true;
        }

        /// <summary>
        /// Escape the comma in the response line values
        /// </summary>
        /// <param name="ofsOriginalStatements"></param>
        /// <returns></returns>
        private static List<string> GetParsedStatements(IEnumerable<string> ofsOriginalStatements)
        {
            List<String> result = new List<string>();
            StringBuilder currentOfsBuilder = new StringBuilder();

            foreach (string ofs in ofsOriginalStatements)
            {
                if (IsNewOfsStatement(ofs))
                {
                    result.Add(currentOfsBuilder.ToString());

                    // start building a new OFS
                    currentOfsBuilder = new StringBuilder(ofs);
                }
                else
                {
                    if (currentOfsBuilder.Length > 0)
                    {
                        currentOfsBuilder.Append(',');
                    }
                    currentOfsBuilder.Append(ofs);
                }
            }

            if (currentOfsBuilder.Length > 0)
            {
                result.Add(currentOfsBuilder.ToString());
            }

            return result;
        }

        /// <summary>
        /// Example for success: "FIELD1:1:1=VA,LUE"
        /// Examples for failure: "34FEILD:1:1=VALUE", "FIELD:VALUE", "FIELD1:1=VALUE"
        /// </summary>
        /// <param name="ofsStatement"></param>
        /// <returns></returns>
        private static bool IsNewOfsStatement(string ofsStatement)
        {
            return OfsNewStatementRegex.IsMatch(ofsStatement);
        }

        private static string GetRecordChangeDetailsAfterInput(DeploymentMethod method, bool isNew, List<DeploymentFieldValues> attributesUpdated)
        {
            if (attributesUpdated.Count == 0)
            {
                Debug.Fail("Should not be here");

                switch (method)
                {
                    case DeploymentMethod.Record_I:
                        return isNew ? "Created a new record" : "Updated an existing record";
                    case DeploymentMethod.Record_IA:
                        return isNew ? "Created and authorized a new record" : "Updated and authorized an existing record";
                    default:
                        return "Unexpected deployment method " + method;
                }
            }

            string startingText;
            switch (method)
            {
                case DeploymentMethod.Record_I:
                    startingText = isNew
                                       ? string.Format("Created record with {0} fields: ", attributesUpdated.Count)
                                       : string.Format("Updated {0} fields of an existing record: ", attributesUpdated.Count);
                    break;
                case DeploymentMethod.Record_IA:
                    startingText = isNew
                                       ? string.Format("Created and authorized a record with {0} fields: ", attributesUpdated.Count)
                                       : string.Format("Updated and authorized {0} fields of an existing record: ", attributesUpdated.Count);
                    break;
                default:
                    return "Unexpected deployment method " + method;
            }

            List<string> namesOfChangedFields = attributesUpdated.ConvertAll(attribute => attribute.FieldName);
            return startingText + string.Join(", ", namesOfChangedFields.ToArray());
        }

        private static Instance ReadUpdatedInstanceFromOfsResponse(DeploymentRecord deplRecord, string ofsResponse)
        {
            Instance updatedInstance = null;
            FieldInfoList allFields = TypicalFieldsAnalyzer.DeduceApplicationFieldsFromInstance(deplRecord.RecordInstance);

            // we use only simple attributes, because the multivalues cannot be procesed easily there
            MetadataTypical typical = GetMetadataTypical(deplRecord, allFields.FindAll(f => !f.HasMultiValues));

            OFSResponseReader reader = new OFSResponseReader();

            string vxml = reader.ReadOFS(new GlobusMessage.OFSMessage(ofsResponse), typical, false, deplRecord.RecordID, deplRecord.AllowDifferentResponseID);
            if (!string.IsNullOrEmpty(vxml) && !vxml.EndsWith("@ID:1:1=RECORD MISSING"))
            {
                try
                {
                    updatedInstance = Instance.FromXmlString(vxml);
                }
                catch (XmlException xe) { }
            }
            return updatedInstance;
        }

        private static MetadataTypical GetMetadataTypical(DeploymentRecord deplRecord, List<FieldInfo> fields)
        {
            MetadataTypical typical = new MetadataTypical();
            typical.Name = deplRecord.TypicalName;
            typical.CatalogName = deplRecord.CatalogName;

            List<TypicalAttribute> nonMultiValuesAttributes = CreateTypicalAttributesFromFields(fields);
            typical.Attributes.AddRange(nonMultiValuesAttributes);

            return typical;
        }

        private static List<TypicalAttribute> CreateTypicalAttributesFromFields(List<FieldInfo> fields)
        {
            return fields.ConvertAll(fld =>
            {
                TypicalAttribute attribute = new TypicalAttribute();
                attribute.Name = fld.FieldName;
                attribute.Type = AttributeDataType.String;
                return attribute;
            });
        }

        private static string GetCompany(DeploymentRecord dRecord, Settings settings)
        {
            if (!string.IsNullOrEmpty(dRecord.Company))
            {
                return dRecord.Company;
            }

            return settings.Companies.GetCompany(dRecord.TypicalName);
        }

        private static string QueueToString(Queue<string> stringQueue)
        {
            StringBuilder sbError = new StringBuilder();
            foreach (string outputLine in stringQueue)
            {
                sbError.Append(outputLine);
                sbError.Append("\r\n");
            }

            string strError = sbError.ToString();
            if (strError.Length > 2)
            {
                // remove the last newline
                strError = strError.Substring(0, strError.Length - 2);
            }

            return strError;
        }

        private static string QueueToString(Queue<string> stringQueue, bool skipFirstAndLast)
        {
            if (!skipFirstAndLast)
            {
                return QueueToString(stringQueue);
            }

            StringBuilder sbError = new StringBuilder();
            for (int scc = 1; scc < stringQueue.Count - 1; scc++)
            {
                string outputLine = stringQueue.ElementAt(scc);
                sbError.Append(outputLine);
                sbError.Append("\r\n");
            }

            string strError = sbError.ToString();
            if (strError.Length > 2)
            {
                // remove the last newline
                strError = strError.Substring(0, strError.Length - 2);
            }

            return strError;
        }
        public static bool IsRecordAuthorized(Instance instance)
        {
            bool iaAuthorized = instance.GetAttributeValue("RECORD.STATUS", true) == "";
            bool lacksAuthorizer = instance.GetAttributeValue("AUTHORISER", true) == "";
            Debug.Assert(iaAuthorized == !lacksAuthorizer, "Why a non-authorized record has an authorizer ?!?");
            return iaAuthorized;
        }

        private string GetFullPath(string jPath)
        {
            Debug.Assert(!string.IsNullOrEmpty(jPath), "'jPath' should be initialized - call LocateLibBinDir before!!!");

            string result = jPath;
            if (result.StartsWith("."))
            {
                // in case of a relative path 
                // (actually, this can only happen if the user has specified it in the config)
                // reconstruct the full path
                result = FtpPath.BuildX(HomeDir, jPath, FtpLogsSubdir);
            }

            return result;
        }

        #endregion

        public DeploymentOutput ExecuteClassicRecordAction(DeploymentRecord record, string Version = null)
        {
            DeploymentOutput result = GetDefaultDeploymentOutput(record);

            if (AbortDeployment)
            {
                result.Outcome = DeploymentOutcome.Skipped;
                result.Details = "Aborted deployment due to previous command error";
                return result;
            }

            try
            {
                switch (record.DeploymentMethod)
                {
                    case DeploymentMethod.Record_V:
                        ExecuteClassicRecordVerify(record, Version);
                        break;

                    default:
                        throw new NotSupportedException(string.Format("'ExecuteClassicRecordAction()' does not support method: '{0}'", record.DeploymentMethod));
                }

                result.Outcome = DeploymentOutcome.Successful;
            }
            catch (Exception ex)
            {
                if (ex is System.Threading.ThreadAbortException)
                {
                    AbortDeployment = true;
                }
                result.Outcome = DeploymentOutcome.Failed;
                result.Details = ex.Message;
            }
            finally
            {
                if (!AbortDeployment)
                {
                    // restore the initial state (CTRL+U)
                    Queue<string> temp;
                    string errorText;
                    ExecuteGlobusClassicCommand(CodeHelper.CTRL_U, out temp, out errorText, -1);
                    ExecuteGlobusClassicCommand(CodeHelper.CTRL_U, out temp, out errorText, -1);
                }
            }

            return result;
        }

        private void ExecuteClassicRecordVerify(DeploymentRecord record, string _Version = null)
        {
            Instance currentT24Instance = record.RecordInstance;
            if (currentT24Instance == null)
                throw new InvalidOperationException("Can not verify a missing record");

            string id = currentT24Instance.GetAttributeValue("@ID");
            if (string.IsNullOrEmpty(id))
                throw new InvalidOperationException("Cannot verify record with empty @ID");

            // TODO - Company is not supported (please use an Classig user with default company that is needed (in TelnetCommmDef.xml)
            // string company = GetCompany(record, _Settings);

            Queue<string> temp;
            string errorText;
            string version = _Version == null ? "" : _Version;
            // send VERIFY command
            string message = string.Format("{0}{3} {1} {2}", record.FullVersionName, "V", GlobusRequestBase.GetEscapedID(id), version);
            if (ExecuteGlobusClassicCommand(message, out temp, out errorText, false, new EndOfResponseCondition("AWAITING PAGE INSTRUCTIONS"), -1) == ExecuteResult.Error)
                throw new ApplicationException(errorText);

            // commit
            if (ExecuteGlobusClassicCommand(CodeHelper.CTRL_V, out temp, out errorText, false, new EndOfResponseCondition("AWAITING PAGE INSTRUCTIONS"), -1) == ExecuteResult.Error)
                throw new ApplicationException(errorText);

            // at this point a question for accept overrides might have resulted in closing the conection or exiting form Classic to JShell
            if (!_GlobusClassicConnector.IsLogged)
                return;

            // Go back
            if (ExecuteGlobusClassicCommand(CodeHelper.CTRL_U, out temp, out errorText, false, new EndOfResponseCondition("AWAITING ID"), -1) == ExecuteResult.Error)
                throw new ApplicationException(errorText);

            if (ExecuteGlobusClassicCommand(CodeHelper.CTRL_U, out temp, out errorText, -1) == ExecuteResult.Error)
                throw new ApplicationException(errorText);
        }

    }
}
