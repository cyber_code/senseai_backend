using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using OFSCommonMethods.IO;
using OFSCommonMethods.IO.Telnet;
using ValidataCommon;
namespace T24TelnetAdapter
{
    public enum ResponseStatus
    {
        /// <summary>
        /// The complete response was processed as expected
        /// </summary>
        Total,

        /// <summary>
        /// Custom status for dealing with error messages related to CheckSumTool 
        /// </summary>
        ToolNotExist,

        /// <summary>
        /// The expected response was not received in the expected time
        /// </summary>
        TimeOut,

        /// <summary>
        /// Unexpected communication error
        /// </summary>
        UnexpectedError
    }

    public delegate void CommEvent(string text, byte[] bytes);

    /// <summary>
    /// Connector to T24 (usually socket-based)
    /// </summary>
    public abstract class T24TelnetConnector : IDisposable
    {
        private const string DefaultShellPrompt = "$ ";
        const string CHECKSUM_TOOL_NAME = "ChecksumToolSMS";

        #region Private Members

        private TerminalConnection _TerminalConnection;

        protected bool _IsLogged;

        protected string _Error;

        protected int _TimeOut = 15000; // default time-out is 15 seconds

        private string _ShellPrompt = ""; // for JShell and System Shell
        
        internal Common.Logger Log
        {
            get { return _TerminalConnection.Log; }
            set { _TerminalConnection.Log = value; }
        }


        protected ConnectionTypeSetting _ConnectionSettings;

        public abstract ConsoleType ConnectionType { get; }

        protected string _LastResponse;

        private string _AccountName;

        private string _AccountPassword;

        private string _LoginStepName;

        public OperatingSystem OS;

        #endregion

        #region Events

        public event CommEvent OnSend;

        public event CommEvent OnReceive;

        #endregion

        #region Public Properties

        public bool JoinTerminalLines { get; set; }

        public string AccountName
        {
            get { return _AccountName; }
            set { _AccountName = value; }
        }

        public string AccountPassword
        {
            get { return _AccountPassword; }
            set { _AccountPassword = value; }
        }

        public ResponseStatus LastStatus;

        public bool IsConnected
        {
            get { return _TerminalConnection.IsConnected; }
        }

        public bool IsLogged
        {
            get { return IsConnected && _IsLogged; }
        }

        /// <summary>
        /// Gets the last error and clears it
        /// </summary>
        /// <value>The last error.</value>
        public string LastError
        {
            get
            {
                string temp = _Error;
                _Error = null;
                return temp;
            }
        }

        public string[] ResponseBuffer;

        public bool IsSocketConnectionError
        {
            get { return _TerminalConnection != null && _TerminalConnection.SocketConnectionException != null; }
        }

        public Exception SocketConnectionException
        {
            get { return _TerminalConnection == null ? null : _TerminalConnection.SocketConnectionException; }
        }

        #endregion

        private static readonly char[] NewLineChars = {(char) AsciiCodes.LF, (char) AsciiCodes.CR};

        #region Class Lifecycle

        private void StartListeningForNetworkTraffic()
        {
            _TerminalConnection.OnSend += FireSendEvent;
            _TerminalConnection.OnReceive += FireReceiveEvent;
        }

        private void FireSendEvent(string text, byte[] bytes)
        {
            if (OnSend != null)
            {
                OnSend(text, bytes);
            }
        }

        private void FireReceiveEvent(string text, byte[] bytes)
        {
            if (OnReceive != null)
            {
                OnReceive(text, bytes);
            }
        }

        public void InitializeConnectonParameters(TerminalConnectionType terminalType,
                                        ConnectionTypeSetting settings,
                                        string host,
                                        int port,
                                        string identityFile)
        {
            _TimeOut = settings.TelnetResponseTimeOut;
            _ConnectionSettings = settings;
            
            switch (terminalType)
            {
                case TerminalConnectionType.Telnet:
                    _TerminalConnection = new TelnetConnection(host, port, settings.PageCode);
                    break;
                case TerminalConnectionType.TCServer:
                    _TerminalConnection = new TCServerConnection(host, port, settings.PageCode, _TimeOut);
                    break;
                case TerminalConnectionType.SharpSsh:
                case TerminalConnectionType.Ssh:
                    _TerminalConnection = new SshConnection(host, port, identityFile, _AccountName, _AccountPassword, _TimeOut, settings.PageCode, JoinTerminalLines, terminalType);
                    break;
                case TerminalConnectionType.HttpWebService:
                    _TerminalConnection = new HttpWebServiceConnection(settings.OFSWebServiceUrl, port, settings.PageCode, settings.WebServiceResponseTimeOut);
                    break;
                case TerminalConnectionType.HttpRestService:
                    _TerminalConnection = new HttpRestServiceConnection(settings.OFSWebServiceUrl, port, settings.PageCode, settings.WebServiceResponseTimeOut);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("terminalType");
            }


            _TerminalConnection.AttempCorrectionForSentCmd =
            (
                   this.ConnectionType == ConsoleType.JShell
                || this.ConnectionType == ConsoleType.OFS // for OFS we will stop corrections after login
            );
            StartListeningForNetworkTraffic();
        }

        ~T24TelnetConnector()
        {
            // TODO proper IDisposable should be used, the finalizer should be optional, see http://msdn.microsoft.com/en-us/library/ms244737%28VS.80%29.aspx
            //Debug.Assert(!IsLogged);
            //Debug.Assert(!IsConnected);

            Disconnect();
        }

        public void Dispose()
        {
            Disconnect();
        }

        #endregion

        #region Public Methods

        public void Disconnect()
        {
            try
            {
                if (IsLogged)
                {
                    Logout();
                }

                if (_TerminalConnection.ConnectionType == TerminalConnectionType.SharpSsh || _TerminalConnection.ConnectionType == TerminalConnectionType.Ssh || IsConnected)
                {
                    // in SSH we need to disconnect explicitely even if disconnected by a exit command, otherwise a thread lives forever
                    _TerminalConnection.Disconnect();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to disconnect from terminal session", ex);
            }
            finally
            {
                _IsLogged = false;
            }
        }

        public virtual bool Login()
        {
            if (IsLogged)
            {
                Log.Warn("Trying to login while already logged-in");
                return true;
            }

            if (ConnectionType == ConsoleType.OFS)
                _TerminalConnection.AttempCorrectionForSentCmd = true; // To avoid dancing tSS

            if (!IsConnected)
            {
                _TerminalConnection.Connect();
            }

            Log.Info(string.Format("Trying to login to {0} as {1}", ConnectionType, _AccountName));

            string response = string.Empty;
            List<ExecutionStep> loginSteps = _ConnectionSettings.GetAllLoginSteps();
            foreach (ExecutionStep es in loginSteps)
            {
                // some login steps need account or password (so clone them to keep them clean)
                ExecutionStep loginStep = (ExecutionStep)es.Clone();
                loginStep.SetAccountName(_AccountName);
                loginStep.SetPassword(_AccountPassword);
                _LoginStepName = loginStep.StepName;

                bool stepPassed = ExecuteStepAndGetResponse(loginStep, out response);
                if (es.StepName.ToLower() == "go to dos")
                {
                    // Hack for Set Prompt to work
                    Thread.Sleep(1000);
                }
                if (response == null)
                {
                    response = "";
                }

                if (!stepPassed)
                {
                    Log.Error(string.Format("Failed to execute login step '{0}'! Response: {1}", loginStep.StepName, response));
                    return false;
                }
                else
                {
                    Debug.WriteLine(string.Format("Result of '{0}' = {1}", loginStep.StepName, response));
                }
            }

            // initilalize the prompt for the shells
            if (ConnectionType == ConsoleType.JShell || ConnectionType == ConsoleType.Shell || ConnectionType == ConsoleType.DBShell)
            {
                response = response.Replace("\r", "\n");
                string[] lastResponseLines = response.Split(new string[]{"\n"}, StringSplitOptions.RemoveEmptyEntries);

                if (lastResponseLines.Length == 0)
                {
                    if (ConnectionType == ConsoleType.JShell)
                    {
                        Log.Error(string.Format("Could not determine the jShell prompt due to empty response."));
                        return false;
                    }
                    else
                    {
                        Log.Warn(string.Format("Could not determine the shell prompt due to empty response. Would attempt to use the default prompt instead."));
                        _ShellPrompt = DefaultShellPrompt;
                    }
                }
                else
                {
                    _ShellPrompt = lastResponseLines.Last();

                    if (ConnectionType == ConsoleType.Shell || ConnectionType == ConsoleType.DBShell)
                    {
                        if (_ShellPrompt != DefaultShellPrompt && loginSteps.Count > 0)
                        {
                            string lastCommand = loginSteps.Last().Command;
                            if (lastCommand.Contains(DefaultShellPrompt))
                            {
                                Log.Debug("The shell prompt was assumed to be the default one, ignoring the unexpected response from the last command that should have set it.");
                                _ShellPrompt = DefaultShellPrompt;
                            }
                        }

                        if (_ShellPrompt != DefaultShellPrompt)
                            Log.Warn("The last step should have set the expected shell prompt '$ ' but is '" + (_ShellPrompt ?? "") + "' instead");
                    }   
                }
            }
            if (ConnectionType == ConsoleType.OFS)
                _TerminalConnection.AttempCorrectionForSentCmd = false; // Because it not supported well and not needed anyway 

            Log.Info("Login successful!");

            _IsLogged = true;

            return true;
        }

        public virtual bool RunTelnetCommand(string command, EndOfResponseCondition endOfResponseCondition, UnexpectedCommunicationErrorList unexpectedCommunicationErrors, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            try
            {
                VerifyIsLoggedIn();

                SetError(null);
                DeflateReceiveBuffer();

                Send(command, false, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
                System.Threading.Thread.Sleep(2000);
                if(_TerminalConnection.ConnectionType == TerminalConnectionType.TCServer)
                {
                    // TCServer commands always end with a newline, so 
                    Debug.Assert(!EndOfResponseCondition.IsValid(endOfResponseCondition));

                    endOfResponseCondition = new EndOfResponseCondition("\r\n")
                                                 {
                                                     DontExpectCommandEcho = true
                                                 };
                }

                if (!EndOfResponseCondition.IsValid(endOfResponseCondition))
                {
                    if (endOfResponseCondition == null)
                        endOfResponseCondition = new EndOfResponseCondition(_ShellPrompt);
                    else
                        endOfResponseCondition.WaitForText = _ShellPrompt;

                    return FillResponse(command, endOfResponseCondition, unexpectedCommunicationErrors);
                }

                ResponseBuffer = ReceiveUntilExpectedTextIsRead(command, endOfResponseCondition, unexpectedCommunicationErrors, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
                return LastStatus == ResponseStatus.Total;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Failed to process command '{0}'", command), ex);
                SetError(ex.Message);
                ResponseBuffer = new string[] { "Error: " + ex.Message };
                return false;
            }
        }

        internal int Timeout
        {
            get { return _TimeOut; }
            set { _TimeOut = value; }
        }

        internal string ShellPrompt
        {
            get { return _ShellPrompt; }
        }

        #endregion

        #region Private Methods

        protected void VerifyIsLoggedIn()
        {
            if (!IsConnected)
            {
                throw new Exception("There is no active terminal connection.");
            }

            if (!IsLogged)
            {
                throw new Exception("Not Logged In");
            }
        }

        protected bool ExecuteStep(ExecutionStep es, bool isLogoutStep = false)
        {
            string response;
            return ExecuteStepAndGetResponse(es, out response,isLogoutStep);
        }
        
        private bool ExecuteStepAndGetResponse(ExecutionStep es, out string response, bool isLogoutStep = false)
        {
            Log.Info(string.Format("Executing step '{0}'...", es.StepName));


            UnexpectedCommunicationErrorList unexpectedCommunicationErrors = _ConnectionSettings.CommonSettings.UnexpectedCommunicationErrors.GetFilteredByConsoleType(ConnectionType)
                .GetEnabledDuringLoginAndLogoff();

            if (es.Command != null)
            {
                Send(es.Command, isLogoutStep);
            }

            if (string.IsNullOrEmpty(es.WaitFor))
            {
                Thread.Sleep(500); // TODO we should be able to specify a timeout for each step, but we usually know what to expect
                bool hasBytes;
                response = GetAvailableText(out hasBytes);
            }
            else
            {
                int numberOfExtraWaitFors = StringHelper.GetNumberOfOccurrences(es.Command, es.WaitFor);
                if (numberOfExtraWaitFors > 0)
                {
                    Log.Debug(string.Format("WaitFor '{0}' is contained in command '{1}' {2} times", es.WaitFor, es.Command, numberOfExtraWaitFors));
                }

                string errMsg;
                response = Receive(new EndOfResponseCondition(es.WaitFor, numberOfExtraWaitFors + 1), unexpectedCommunicationErrors, out errMsg);
                if (response == null)
                {
                    Log.Error(string.Format("Failed to process step '{0}'", es.StepName));

                    errMsg = string.Format(
                                "Login fail: Unable to establish '{0}' terminal connection. " +
                                "Execution step: '{1}' failed! Details: {2}.",
                                ConnectionType,
                                es.StepName,
                                errMsg
                            );

                    SetError(errMsg);

                    return false;
                }
            }

            Log.Info(string.Format("Succeeded to process step '{0}'", es.StepName));
            return true;
        }

        private void ExecuteLastExitStep(ExecutionStep es)
        {
            Log.Info(string.Format("Executing last exit step '{0}'...", es.StepName));

            UnexpectedCommunicationErrorList unexpectedCommunicationErrors = _ConnectionSettings.CommonSettings.UnexpectedCommunicationErrors.GetFilteredByConsoleType(ConnectionType)
                .GetEnabledDuringLoginAndLogoff();

            if (es.Command != null)
            {
                Send(es.Command, true);
                string dummyErrMsg;
                if (!string.IsNullOrEmpty(es.WaitFor))
                    Receive(new EndOfResponseCondition(es.WaitFor), unexpectedCommunicationErrors, out dummyErrMsg);
            }
            else
            {
                Debug.Fail("The last exit step should have some command!");
                Log.Error("The last exit step should have some command!");
            }
        }

        protected string GetAvailableText(out bool hasBytes, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            return _TerminalConnection.GetAvailableText(out hasBytes);
        }

        protected string Receive(EndOfResponseCondition endOfResponseCondition, UnexpectedCommunicationErrorList unexpectedCommunicationErrors, out string errMsg)
        {
            errMsg = null;
            _LastResponse = null;
            string response = string.Empty;
            DateTime dtStart = DateTime.Now;
            int lastErr = 0;
            do
            {
                bool hasBytes;
                string partialResponse = GetAvailableText(out hasBytes);
                if (hasBytes)
                {
                    dtStart = DateTime.Now;
                }

                response += partialResponse;
                _LastResponse = response;

                UnexpectedCommunicationError communicationInterruption = CheckAndProcessUnexpectedCommunicationErrors(unexpectedCommunicationErrors, ref response, ref lastErr);
                if (communicationInterruption != null)
                {
                    if (communicationInterruption.IsError)
                    {
                        Log.Warn(_Error);
                        errMsg = string.Format("Unexpected communication error: ", _Error);
                        continue;
                    }
                    // else continue to the end

                    //else if (endOfResponseCondition.IsSatisfied(response))
                    //{
                    //    return response;    // should be OK
                    //}

                    // 
                    //_IsLogged = false;

                    //return response;
                }

                if (endOfResponseCondition.IsSatisfied(response))
                {
                    return response;
                }

                Thread.Sleep(20);
            }
            while ((DateTime.Now - dtStart).TotalMilliseconds < _TimeOut);

            errMsg = string.Format("Did not receive expected text '{0}' in {1} miliseconds", endOfResponseCondition, _TimeOut);
            Log.Warn(_Error);

            return null;
        }

        protected string EmptyReceiveBuffer(string residual)
        {
            // TODO reuse base class method
            try
            {
                for (int i = 0; i < 100 /*MAX ITERATIONS*/; i++)
                {
                    Thread.Sleep(50);

                    bool hasBytes;
                    string temp = GetAvailableText(out hasBytes);
                    if (!hasBytes)
                    {
                        break;
                    }

                    residual += temp;
                }

                return residual;
            }
            catch // just in case
            {
                return "";
            }
        }

        private void DeflateReceiveBuffer()
        {
            try
            {
                bool hasBytes;
                string partialResponse = GetAvailableText(out hasBytes);
                if (!hasBytes)
                {
                    // OK
                    return;
                }

                LogAsWarning(string.Format("WARNING: Unprocessed response(s) detected:\r\n{0}", partialResponse));
            }
            catch (Exception ex)
            {
                LogAsWarningAndAssert(string.Format("INTERNAL EXCEPTION (0):\r\n{0}", ex.Message));
            }
        }

        protected void SetError(string message)
        {
            _Error = message;
        }

        protected bool FillResponse(string command, EndOfResponseCondition eorc, UnexpectedCommunicationErrorList unexpectedCommunicationErrors)
        {
            string[] res;
            if (ConnectionType == ConsoleType.OFS)
            {
                res = ReceiveUntilSpecifiedNumberOfLinesAreRead(command, 2, unexpectedCommunicationErrors); // 2 = the first command that we have sent + the single line response
            }
            else
            {
                Debug.Assert(ConnectionType != ConsoleType.Classic); // we don't have a generic way of handling such commands

                // we are in JSH or SH, so we should wait for the prompt to see when the command has finished.
                res = ReceiveUntilExpectedTextIsRead(command, eorc, unexpectedCommunicationErrors);
            }

            ResponseBuffer = res;

            if (LastStatus == ResponseStatus.TimeOut)
            {
                SetError(String.Format("Time-out during execution of current request."));
                return false;
            }

            return true;
        }

        private string[] ReceiveUntilExpectedTextIsRead(string sentCommand, EndOfResponseCondition endOfResponseCondition, UnexpectedCommunicationErrorList unexpectedCommunicationErrors, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            Log.Debug(string.Format("Expect to read '{0}'", endOfResponseCondition));

            return ReceiveExpectedResponse(sentCommand, endOfResponseCondition, 0, false, unexpectedCommunicationErrors, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
        }

        private string[] ReceiveUntilSpecifiedNumberOfLinesAreRead(string sentCommand, int linesCount, UnexpectedCommunicationErrorList unexpectedCommunicationErrors)
        {
            Log.Debug(string.Format("Expect to read {0} lines", linesCount));

            return ReceiveExpectedResponse(sentCommand, null, linesCount, true, unexpectedCommunicationErrors);
        }

        private string[] ReceiveExpectedResponse(string sentCommand, EndOfResponseCondition endOfResponseCondition, int linesCount, bool limitResponseNumOfLines, UnexpectedCommunicationErrorList unexpectedCommunicationErrors, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            string response = string.Empty;
            DateTime dtStart = DateTime.Now;
            int lastErr = 0;
            LastStatus = ResponseStatus.TimeOut;
            bool warningLogged = false;

            // figure out which timeout to use
            int timeOut = _TimeOut;
            if (endOfResponseCondition != null && endOfResponseCondition.TimeOut > 0)
            {
                timeOut = endOfResponseCondition.TimeOut;
            }

            do
            {
                bool hasBytes;
                do
                {
                    response += GetAvailableText(out hasBytes, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
                    bool patientTimeout = (endOfResponseCondition == null ? false : endOfResponseCondition.PatientTimeout);

                    if (hasBytes && patientTimeout)
                    {
                        dtStart = DateTime.Now;
                    }

                    Thread.Sleep(20);
                } while (hasBytes);

                string[] responseLines = response.Split(NewLineChars, StringSplitOptions.RemoveEmptyEntries);

                // check if the response contains errors (actually those are expected, not unexpected :) )
                UnexpectedCommunicationError communicationError = CheckAndProcessUnexpectedCommunicationErrors(unexpectedCommunicationErrors, ref response, ref lastErr);
                if (communicationError != null)
                {
                    LastStatus = communicationError.IsError
                        ? ResponseStatus.UnexpectedError
                        : ResponseStatus.Total;

                    continue;
                }

                int indexOfSentCommand;
                if (EndOfResponseCondition.IsValid(endOfResponseCondition) && endOfResponseCondition.DontExpectCommandEcho)
                {
                    indexOfSentCommand = 0;
                }
                else
                {
                    indexOfSentCommand = GetIndexOfCompletionOfCommand(sentCommand, responseLines, ref warningLogged);
                }

                // break if the expected line ending has been read
                if (EndOfResponseCondition.IsValid(endOfResponseCondition))
                {
                    // special handling for TCServer, since the code below trims the new lines
                    if (_TerminalConnection.ConnectionType == TerminalConnectionType.TCServer 
                        && endOfResponseCondition.WaitForText == "\r\n" 
                        &&  response.EndsWith("\r\n"))
                    {
                        LastStatus = ResponseStatus.Total;
                        break;
                    }

                    var linesToAnalyze = new List<string>(responseLines);

                    if (indexOfSentCommand >= 0)
                        if (sentCommand != string.Empty)
                            if (!endOfResponseCondition.DontExpectCommandEcho)
                                linesToAnalyze.RemoveAt(indexOfSentCommand);

                    if (IsResponseProperlyTerminated(string.Join("\n"
                        ,linesToAnalyze), endOfResponseCondition,linesToAnalyze.Count))
                    {
                        LastStatus = ResponseStatus.Total;
                        break;
                    }
                }

                // break if the line limit has been reached
                if (limitResponseNumOfLines)
                {
                    if (indexOfSentCommand < 0)
                    {
                        if (sentCommand.Length < string.Join("", responseLines).Length)
                        {
                            LogAsWarningAndAssert("The sent command should have already been read, since the buffer is already bigger than the sent command");
                        }
                    }
                    else
                    {
                        if ((responseLines.Length - indexOfSentCommand) >= linesCount)
                        {
                            LastStatus = ResponseStatus.Total;
                            break;
                        }
                    }
                }

                // hack - break if the check sum tool does not exist
                if (IsResponseIndicatingThatCheckSumToolIsMissing(response))
                {
                    LastStatus = ResponseStatus.ToolNotExist;
                    break;
                }

                Thread.Sleep(20);
            }
            while ((DateTime.Now - dtStart).TotalMilliseconds < timeOut);

            if (LastStatus == ResponseStatus.ToolNotExist)
            {
                SetError("ChecksumToolSMS was not found on target environment.");
            }
            else if (LastStatus == ResponseStatus.TimeOut)
            {
                Log.Debug(string.Format("Timeout: the current request was not processed in {0} miliseconds", timeOut));

                SetError("Time-out during execution of current request.");
            }

            // TODO what about the timed-out request, should we return the result, at all?
            string[] resultLines = response.Split(NewLineChars, StringSplitOptions.RemoveEmptyEntries);

            if (!EndOfResponseCondition.IsValid(endOfResponseCondition) || !endOfResponseCondition.DontExpectCommandEcho)
            {
                resultLines = GetResultStartingWithSentCommand(sentCommand, resultLines, INPUT_USER, INPUT_PASSWORD, AUTHORIZATION_USER, AUTHORIZATION_PASSWORD);
            }

            if (!limitResponseNumOfLines || resultLines.Length <= linesCount)
            {
                return resultLines;
            }
            else
            {
                string[] limitedResult = new string[linesCount];
                Array.Copy(resultLines, limitedResult, linesCount);
                return limitedResult;
            }
        }

        protected UnexpectedCommunicationError CheckAndProcessUnexpectedCommunicationErrors(UnexpectedCommunicationErrorList unexpectedCommunicationErrors, ref string response,ref int lastErr)
        {
            if (unexpectedCommunicationErrors == null)
                return null;
            
            UnexpectedCommunicationError communicationError = unexpectedCommunicationErrors.FindForMessage( response,ref lastErr);

            if (communicationError == null || communicationError.LinesToSend.Length == 0)
                return null;

            // set more user friendly error
            if (communicationError.IsError)
                SetError(communicationError.GetDisplayErrorMessage(response));

            var sbFullResponse = new System.Text.StringBuilder(response);
            // make sure everything is read
            string tempResponse = EmptyReceiveBuffer("");
            sbFullResponse.Append(tempResponse);

            // try to recover from the error
            foreach (string command in communicationError.LinesToSend)
            {
                Send(command);

                // todo - for real recovery implement receive instead of Sleep
                //Thread.Sleep(5000);

                // empty the buffer
                bool hasBytes;
                tempResponse = GetAvailableText(out hasBytes);
                sbFullResponse.Append(tempResponse);
            }

            tempResponse = EmptyReceiveBuffer("");
            sbFullResponse.Append(tempResponse);

            response = sbFullResponse.ToString();

            return communicationError;
        }

        protected void Send(string command, bool isLogoutStep = false, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            string enryptedCommand = _LoginStepName.ToLower().Contains("password") ? "******" : command;
            if (command != null)
            {
                if (INPUT_USER.Trim() != "" && INPUT_PASSWORD.Trim() != "")
                {
                    enryptedCommand = command.Replace(INPUT_USER + "/" + INPUT_PASSWORD, INPUT_USER + "/******");
                }
                if (AUTHORIZATION_USER.Trim() != "" && AUTHORIZATION_PASSWORD.Trim() != "")
                {
                    enryptedCommand = enryptedCommand.Replace(AUTHORIZATION_USER + "/" + AUTHORIZATION_PASSWORD, AUTHORIZATION_USER + "/******");
                }
            }

            string ofsRequestStart = "<MSG>";
            string ofsRequestEnd = "</MSG>";
            string[] arr = new string[] { };

            if (command.StartsWith(ofsRequestStart) && command.EndsWith(ofsRequestEnd))
            {
                enryptedCommand = command.Substring(ofsRequestStart.Length);
                enryptedCommand = enryptedCommand.Substring(0, enryptedCommand.Length - ofsRequestEnd.Length);


                // ENQUIRY.SELECT,/I/PROCESS,GEORGE001/1234567,%STANDARD.SELECTION'
                // ENQUIRY.SELECT,          ,GEORGE001/1234567,%STANDARD.SELECTION'
                // app        ,      func   , user/pass       , @ID , fields...

                arr = enryptedCommand.Split(',');

                if (arr.Length >= 3)
                {
                    if (arr[2].Contains('/'))
                    {
                        string[] tmp = arr[2].Split('/');
                        if (tmp != null && tmp.Length > 1)
                        tmp[1] = "******";
                        arr[2] = string.Join("/", tmp);
                    }

                    enryptedCommand = string.Concat(ofsRequestStart, string.Join(",", arr), ofsRequestEnd);
                }
            }
            Log.Debug(string.Format("Writing to {0}: '{1}'", ConnectionType, enryptedCommand));

            _TerminalConnection.Log = this.Log; 
			_TerminalConnection.Send(command, isLogoutStep);
            _TerminalConnection.LastCommand = command;
        }       

        protected virtual bool Reconnect()
        {
            Disconnect();
            return Login();
        }

        protected virtual bool Logout()
        {
            Log.Info("Trying to logout...");
            try
            {
                if(ExecuteLogoutSteps())
                {
                    Log.Info("Succeeded to logout!");
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Log.Warn("Failed to logout. Detailed error: " + ex.Message);
                return false;
            }
            finally
            {
                _IsLogged = false;
            }
        }

        public List<ExecutionStep> LoginSteps
        {
            get { return _ConnectionSettings.GetAllLoginSteps(); }
        }

        public List<ExecutionStep> ExitSteps
        {
            get { return _ConnectionSettings.GetAllExitSteps(); }
        }

        private bool ExecuteLogoutSteps()
        {
            List<ExecutionStep> exitSteps = _ConnectionSettings.GetAllExitSteps();
            foreach (ExecutionStep es in exitSteps)
            {
                if (! _TerminalConnection.IsConnected)
                {
                    Log.Warn("Strange, there is no active connection, the logout might have completed without executing all exit steps");
                    return true;
                }

                // we should to update command for abort step but since we are not sure which one is (although it should be the first) we will check every step
                UpdateAbortStep(es);

                if (es == exitSteps.Last())
                {
                    // The last exit step should lead the end of the terminal connection
                    ExecuteLastExitStep(es);
                }
                else
                {
                    bool res = ExecuteStep(es, true);
                    if (!res)
                    {
                        Log.Warn(string.Format("Failed to execute exit step: '{0}'", es.StepName));
                        return false;
                    }
                }
            }

            return true;
        }

        private void UpdateAbortStep(ExecutionStep es)
        {
            // value ctrl+c to abort long time execution is correct when used from console ie. directly from keyboard
            // but when that value should be sent over telnet in fact should be send it hex value which is "\x3"
            // so this method will replace ctrl+c with "\x3"

            if (es.Command.ToLower().Equals("ctrl+c")) es.Command = "\x3";
        }

        public static bool IsResponseIndicatingThatCheckSumToolIsMissing(string response)
        {
            if (String.IsNullOrEmpty(response) || !response.Contains(CHECKSUM_TOOL_NAME))
            {
                return false;
            }

            /*Windows response:
             
              ChecksumToolSMS F:\temenos\PSGMB-R08-000\bnk.run\lib
              'ChecksumToolSMS' is not recognized as an internal or external command, operable program or batch file.
            */
            if (response.Contains("is not recognized as an internal or external command"))
            {
                return true;
            }

            /*AIX response in SH shell:
             
                ChecksumToolSMSsd /homenew/MIGVB03/bnk.run/lib
                sh: ChecksumToolSMS:  not found.
             */
            if (response.Contains(CHECKSUM_TOOL_NAME + ":  not found"))
            {
                return true;
            }

            /*
             JShell response:
             
             ChecksumToolSMS somefile.txt
             ChecksumToolSMS: No such file or directory
             */
            if (response.Contains(CHECKSUM_TOOL_NAME + ": No such file or directory"))
            {
                return true;
            }

            return false;
        }

        

        #endregion

        #region Private Static Methods

        private static bool IsResponseProperlyTerminated(string responseAsSingleLine, EndOfResponseCondition endOfResponseCondition, int numLines)
        {
            if (endOfResponseCondition.IsSatisfied(responseAsSingleLine, true, numLines))
            {
                return true;
            }

            // TODO - Should we trim in order to be able to handle the case where newline is splitting the terminatingLineEnd

            return false;
        }

        //this functions were static changed to non static
        protected void LogAsWarning(string message)
        {
            Log.Warn(message);
        }

        protected void LogAsWarningAndAssert(string message)
        {
            LogAsWarning(message);
            Debug.Fail(message);
        }

        private string[] GetResultStartingWithSentCommand(string sentCommand, string[] result, string INPUT_USER = "", string INPUT_PASSWORD = "", string AUTHORIZATION_USER = "", string AUTHORIZATION_PASSWORD = "")
        {
            bool warningLogged = false;
            int pos = GetIndexOfCompletionOfCommand(sentCommand, result, ref warningLogged);

            if (sentCommand != null)
            {
                if (INPUT_USER.Trim() != "" && INPUT_PASSWORD.Trim() != "")
                {
                    sentCommand = sentCommand.Replace(INPUT_USER + "/" + INPUT_PASSWORD, INPUT_USER + "/******");
                }
                if (AUTHORIZATION_USER.Trim() != "" && AUTHORIZATION_PASSWORD.Trim() != "")
                {
                    sentCommand = sentCommand.Replace(AUTHORIZATION_USER + "/" + AUTHORIZATION_PASSWORD, AUTHORIZATION_USER + "/******");
                }
            }

            if (pos < 0)
            {
                LogAsWarningAndAssert("The response does not contain the sent command: " + sentCommand);
                // we don't do anything here, but maybe we should return empty array?
            }

            // if pos = 0, we don't need to do anything -> the first item is the command
            if (pos > 0)
            {
                // the sent command is split with new line(s)
                List<string> tempResult = new List<string>();
                tempResult.Add(sentCommand);
                for (int i = pos + 1; i < result.Length; i++)
                {
                    tempResult.Add(result[i]);
                }
                return tempResult.ToArray();
            }

            return result;
        }

        private int GetIndexOfFirstDifference(string text, string expectedStartOfText)
        {
            if (string.IsNullOrEmpty(expectedStartOfText))
            {
                return -1;
            }

            int minLength = Math.Min(text.Length, expectedStartOfText.Length);
            for (int i = 0; i < minLength; i++)
            {
                if (expectedStartOfText[i] != text[i])
                {
                    return i;
                }
            }

            return -1;
        }

        private int GetIndexOfCompletionOfCommand(string command, string[] lines, ref bool warningLogged)
        {
            if (command == string.Empty)
            {
                return 0;
            }

            string[] tmp = command.Split(NewLineChars, StringSplitOptions.RemoveEmptyEntries);
            if (tmp.Length > 1)
            {
                //If the sent command is delimited by NewLine, join the command without NewLine
                command = string.Join("", tmp);
            }

            string response = "";
            for (int i = 0; i < lines.Length; i++)
            {
                response += lines[i];

                if (!command.StartsWith(response) && !warningLogged)
                {
                    string msg = "The read data does not start with the command that has been sent. ";
                    int indexOfFirstDiff = GetIndexOfFirstDifference(command, response);
                    if (indexOfFirstDiff >= 0)
                    {
                        msg += "The first difference is at position " + indexOfFirstDiff;
                    }
                    else
                    {
                        msg += "The current data is longer than the response";
                    }

                    LogAsWarningAndAssert(msg);
                    warningLogged = true;
                }

                if (response.Length > command.Length)
                {
                    break;
                }

                if (command == response)
                {
                    return i;
                }
            }

            return -1;
        }

        public bool RunCommandWithoutTerminatingString(string command, out Queue<string> res, out string errorText)
        {
            errorText = "";
            bool success = true;

            if (!RunTelnetCommand(command, null, null))
            {
                errorText = LastError;
                success = false;
            }

            res = ReadResponseFromTelnetBuffer();

            // TODO refactor this, the base class should not be aware of its subclasses
            if (this is GlobusClassicConnector)
            {
                GlobusClassicConnector classic = (GlobusClassicConnector) this;

                if (!success)
                {
                    return false;
                }

                if (!classic.IsExecutedSuccessfully)
                {
                    foreach (string s in res)
                    {
                        errorText += s + "\r\n";
                    }
                }

                return classic.IsExecutedSuccessfully;
            }

            if (!success && LastStatus != ResponseStatus.Total)
            {
                if (ConnectionType == ConsoleType.OFS ||
                    ConnectionType == ConsoleType.JShell)
                {
                    // we might end up in the DEBUG mode of T24 terminal communication, so we should try to recover, by quitting it
                    try
                    {
                        // TODO the following code does not run very 
                        RunTelnetCommand("q", null, null); // quit command
                        RunTelnetCommand("y", null, null); // confirm the quitting
                    }
                    catch (Exception ex)
                    {
                        errorText = ex.Message;
                    }
                }
            }

            return success;
        }

        public bool RunTelnetCommandWithoutTerminatingString(string command, out Queue<string> res, out string errorText, out string lastline)
        {
            bool success = RunCommandWithoutTerminatingString(command, out res, out errorText);
            lastline = GetLastLineOfResponse(res);
            return success;
        }

        public Queue<string> ReadResponseFromTelnetBuffer()
        {
            Queue<string> res = new Queue<string>();
            if (ResponseBuffer != null)
            {
                foreach (string line in ResponseBuffer)
                {
                    if (line != null && !line.Equals("") && !line.Equals("\n"))
                    {
                        res.Enqueue(line);
                    }
                }
            }
            else
            {
                Debug.Fail("Please check why response buffer is null!");
            }

            return res;
        }

        public string GetLastLineOfResponse(Queue<string> res)
        {
            if (res == null)
            {
                return "";
            }

            List<string> responsesList = new List<string>(res);
            responsesList.RemoveAll(s => string.IsNullOrEmpty(s));
            if (responsesList.Count == 0)
            {
                return "";
            }

            string lastline = responsesList.Last();

            // the OFS does not have a command prompt
            if (ConnectionType == ConsoleType.OFS)
            {
                return lastline;
            }

            if (ConnectionType == ConsoleType.Classic)
            {
                // TODO is this the proper handling?
                return lastline;
            }

            if (lastline != ShellPrompt)
            {
                return lastline;
            }

            // the last line is the current prompt, so get the semi last (the first one should be the command itself)
            if (responsesList.Count > 2)
            {
                return responsesList[responsesList.Count - 2];
            }

            // Debug.Fail("There is no last line!");

            return "";
        }

        #endregion
        
    }
}