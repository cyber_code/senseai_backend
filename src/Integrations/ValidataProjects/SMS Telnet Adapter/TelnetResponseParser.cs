﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T24TelnetAdapter
{
    public class TelnetResponseParser
    {
        #region Public Methods

        public List<string> GetGoodStringsTrim(string str)
        {
            List<string> result = new List<string>();
            foreach (string s in GetGoodStrings(str))
            {
                string[] arr = s.Split(new string[] {"\r\n"},StringSplitOptions.None);

                foreach(string ai in arr)
                {
                    result.Add(ai.Trim());                  
                }
            }

            return result;
        }

        public string[] GetGoodStrings(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return new string[]{};
            }
            
            List<List<char>> res = new List<List<char>>();
            bool startNew = true;
            int i = 0;
            while (i < str.Length)
            {
                char toAdd = '\0';

                if (char.IsControl(str[i]))
                {
                    switch (str[i++])
                    {
                        case (char)10:
                            toAdd = (char)10;
                            break;

                        case (char)13:
                            toAdd = (char)13;
                            break;

                        case (char)27:
                            switch (str[i++])
                            {
                                case 'Y':
                                    i += 2;
                                    break;

                                case '#':
                                case '(':
                                case ')':
                                case '%':
                                    i++;
                                    break;
                                case '[':
                                    //int start = i;
                                    while ((i < str.Length) && (str[i] < '?'))
                                    {
                                        i++;
                                    }
                                    i++;

                                    // TODO
                                    //try
                                    //{
                                    //    int pos = ParseEscapeANSI(str.Substring(start, i - start), 0);
                                    //}
                                    //catch { }
                                    
                                    break;

                                default:
                                    break;
                            }
                            break;

                        default:
                            break;
                    }

                    if (toAdd == '\0')
                    {
                        startNew = true;
                        continue;
                    }
                }

                if (startNew)
                {
                    res.Add(new List<char>());
                    startNew = false;
                }

                if (toAdd != '\0')
                {
                    res[res.Count - 1].Add(toAdd);
                }
                else
                {
                    res[res.Count - 1].Add(str[i++]);
                }
            }

            List<string> result = new List<string>();
            string separator = new string(new char[] { (char)0x01 });
            foreach (List<char> curr in res)
            {
                string tempRes = new string(curr.ToArray());
                tempRes = tempRes.Replace("\r\n", separator).Replace("\r", separator).Replace(separator, "\r\n");
                result.Add(tempRes);
            }
            
            return result.ToArray();
        }

        #endregion
    }
}
