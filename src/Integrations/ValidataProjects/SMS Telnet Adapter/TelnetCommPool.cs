﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OFSCommonMethods.IO;
using System.Linq;

namespace T24TelnetAdapter
{
    public static class TelnetCommPool
    {
        #region Helper Classes

        private struct LoginKey
        {
            public bool JoinTerminalLines;
            public ConsoleType ConnectorType;
            public TerminalConnectionType TerminalType;
            public string Host;
            public int Port;
            public string AccountName;
            public string AccountPassword;
            public string SshIdentityFile;

            /// <summary>
            /// To be used, if we want to have several concurrent connection to the same environment with the same user/password
            /// </summary>
            public string UniqueID; 
        }

        #endregion

        private static readonly Object _Sync = new Object();

        private static readonly Dictionary<LoginKey, T24TelnetConnector> _Connectors = new Dictionary<LoginKey, T24TelnetConnector>();

        public static OfsConnector GetOfsConnector(Settings settings)
        {
            return (OfsConnector) GetOrCreateConnector(settings, ConsoleType.OFS);
        }

        public static GlobusClassicConnector GetGlobusClassicConnector(Settings settings)
        {
            return (GlobusClassicConnector) GetOrCreateConnector(settings, ConsoleType.Classic);
        }

        public static JShellConnector GetJShellConnector(Settings settings)
        {
            return (JShellConnector) GetOrCreateConnector(settings, ConsoleType.JShell);
        }

        public static DBShellConnector GetDBShellConnector(Settings settings)
        {
            return (DBShellConnector)GetOrCreateConnector(settings, ConsoleType.DBShell);
        }

        public static SystemShellConnector GetSystemShellConnector(Settings settings)
        {
            return (SystemShellConnector) GetOrCreateConnector(settings, ConsoleType.Shell);
        }

        public static HttpWebServiceConnector GetHttpWebServiceConnector(Settings settings)
        {
            return (HttpWebServiceConnector)GetOrCreateConnector(settings, ConsoleType.HttpWebService);
        }

        public static HttpRestServiceConnector GetHttpRestServiceConnector(Settings settings)
        {
            return (HttpRestServiceConnector)GetOrCreateConnector(settings, ConsoleType.HttpRestService);
        }

        private static TerminalConnectionType GetTerminalType(Settings.IOType ioType)
        {
            switch(ioType)
            {
                case Settings.IOType.Telnet:
                    return TerminalConnectionType.Telnet;
                case Settings.IOType.SharpSsH:
                    return  TerminalConnectionType.SharpSsh;
                case Settings.IOType.SSH:
                    return TerminalConnectionType.Ssh;
                case Settings.IOType.TCServer:
                    return TerminalConnectionType.TCServer;
                case Settings.IOType.HttpWebService:
                    return TerminalConnectionType.HttpWebService;
                case Settings.IOType.HttpRestService:
                    return TerminalConnectionType.HttpRestService;
                default:
                    return TerminalConnectionType.Telnet; // this is the most common
            }
        }

        private static T24TelnetConnector GetOrCreateConnector(Settings settings, ConsoleType consoleType)
        {
            lock (_Sync)
            {
                LoginKey loginKey;
                
                switch(consoleType)
                {
                    case ConsoleType.DBShell:
                        {
                            if (settings.DBConnection == null) return null;

                            string host;
                            int port;
                            DetermineDBHostAndPort(settings.DBConnection, out host, out port);
                            loginKey = new LoginKey
                            {
                                ConnectorType = consoleType,
                                Host = host,
                                Port = port,
                                AccountName = settings.DBConnection.Username,
                                AccountPassword = settings.DBConnection.Password,
                                SshIdentityFile = settings.DBConnection.IdentityFile,
                                TerminalType = GetTerminalType(settings.DBConnection.ConnType),
                                
                                // TODO: maybe add separate for the db conn for the below? 
                                UniqueID = settings.UniqueID,
                                JoinTerminalLines = settings.JoinTerminalLines
                            };
                        }
                        break;

                    case ConsoleType.Classic:
                    case ConsoleType.JShell:
                    case ConsoleType.OFS:
                    case ConsoleType.Shell:
                    
                        {
                            string host;
                            int port;
                            DetermineHostAndPort(settings, out host, out port);
                            loginKey = new LoginKey
                            {
                                ConnectorType = consoleType,
                                Host = host,
                                Port = port,
                                AccountName = settings.GlobusUserName,
                                AccountPassword = settings.GlobusPassword,
                                SshIdentityFile = settings.IdentityFile,
                                TerminalType = GetTerminalType(settings.Type),
                                UniqueID = settings.UniqueID,
                                JoinTerminalLines = settings.JoinTerminalLines
                            };
                        }
                        break;
                    case ConsoleType.HttpWebService:
                        {
                            var webServiceConnection = settings.ConnectionTypes.FirstOrDefault(ct => ct.ConnectionType == ConsoleType.HttpWebService);
                            if (webServiceConnection == null)
                                return null;

                            loginKey = new LoginKey
                            {
                                ConnectorType = consoleType,
                                Host = webServiceConnection.OFSWebServiceUrl,
                                Port = 8080,
                                AccountName = settings.GlobusUserName,
                                AccountPassword = settings.GlobusPassword,
                                SshIdentityFile = settings.IdentityFile,
                                TerminalType = GetTerminalType(Settings.IOType.HttpWebService),
                                UniqueID = settings.UniqueID,
                                JoinTerminalLines = settings.JoinTerminalLines
                            };
                        }
                        break;
                    case ConsoleType.HttpRestService:
                        {
                            var webServiceConnection = settings.ConnectionTypes.FirstOrDefault(ct => ct.ConnectionType == ConsoleType.HttpRestService);
                            if (webServiceConnection == null)
                                return null;

                            loginKey = new LoginKey
                            {
                                ConnectorType = consoleType,
                                Host = webServiceConnection.OFSWebServiceUrl,
                                Port = 8080,
                                AccountName = settings.GlobusUserName,
                                AccountPassword = settings.GlobusPassword,
                                SshIdentityFile = settings.IdentityFile,
                                TerminalType = GetTerminalType(Settings.IOType.HttpRestService),
                                UniqueID = settings.UniqueID,
                                JoinTerminalLines = settings.JoinTerminalLines
                            };
                        }
                        break;
                    default:
                        throw new InvalidOperationException("Unknown value for consoleType");
                }
                

                if (!_Connectors.ContainsKey(loginKey))
                {
                    T24TelnetConnector connector = CreateConnector(loginKey, settings.GetConnectionSettingsByType(consoleType));
                    _Connectors.Add(loginKey, connector);
                }

                T24TelnetConnector res = _Connectors[loginKey];
                return res;
            }
        }

        private static void DetermineHostAndPort(Settings settings, out string host, out int port)
        {
            string[] ipAddrItems = settings.GlobusServer.Split(new[] { ':' });
            if (ipAddrItems.Length > 1)
            {
                int.TryParse(ipAddrItems[1], out port); //  the port can be overrided using the URL
                host = ipAddrItems[0];
            }
            else
            {
                host = settings.GlobusServer;
                port = settings.Type == Settings.IOType.SharpSsH || settings.Type == Settings.IOType.SSH ? 22 : 23;
            }
        }
        private static void DetermineDBHostAndPort(DBConnSettings settings, out string host, out int port)
        {
            string[] ipAddrItems = settings.ServerIP.Split(new[] { ':' });
            if (ipAddrItems.Length > 1)
            {
                int.TryParse(ipAddrItems[1], out port); //  the port can be overrided using the URL
                host = ipAddrItems[0];
            }
            else
            {
                host = settings.ServerIP;
                port = settings.ConnType == Settings.IOType.SSH ? 22 : 23;
            }
        }

        public static void ReleaseAll()
        {
            lock (_Sync)
            {
                foreach (T24TelnetConnector tssComm in _Connectors.Values)
                {
                    if (tssComm is HttpWebServiceConnector) continue;
                    if (tssComm != null)
                    {
                        tssComm.Disconnect();
                    }
                }

                _Connectors.Clear();
            }
        }

        public static void Release(T24TelnetConnector comm, bool withRemove = true)
        {
            lock (_Sync)
            {
                if (comm == null)
                {
                    return;
                }

                if (!(comm is HttpWebServiceConnector)) comm.Disconnect();

                foreach (LoginKey key in _Connectors.Keys)
                {
                    T24TelnetConnector connector = _Connectors[key];
                    if (connector == comm)
                    {
                        if(withRemove)
                            _Connectors.Remove(key);
                        break;
                    }
                }
            }
        }

        private static T24TelnetConnector CreateConnector(LoginKey lk, ConnectionTypeSetting connectorSettings)
        {
            T24TelnetConnector connector;

            Debug.Assert(lk.ConnectorType == connectorSettings.ConnectionType);

            switch (lk.ConnectorType)
            {
                case ConsoleType.JShell:
                    connector = new JShellConnector();
                    break;
                case ConsoleType.Shell:
                    connector = new SystemShellConnector();
                    break;
                case ConsoleType.DBShell:
                    connector = new DBShellConnector();
                    break;
                case ConsoleType.OFS:
                    connector = new OfsConnector();
                    break;
                case ConsoleType.Classic:
                    connector = new GlobusClassicConnector();
                    break;
                case ConsoleType.HttpWebService:
                    connector = new HttpWebServiceConnector();
                    break;
                case ConsoleType.HttpRestService:
                    connector = new HttpRestServiceConnector();
                    break;
                default:
                    throw new InvalidOperationException("Unkown Connection Type");
            }

            connector.AccountName = lk.AccountName;
            connector.AccountPassword = lk.AccountPassword;
            connector.JoinTerminalLines = lk.JoinTerminalLines;
            connector.InitializeConnectonParameters(lk.TerminalType, connectorSettings, lk.Host, lk.Port, lk.SshIdentityFile);

            return connector;
        }
    }
}