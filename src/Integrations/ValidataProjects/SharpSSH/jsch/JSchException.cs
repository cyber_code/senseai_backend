using System;

namespace Tamir.SharpSsh.jsch
{
	/// <summary>
	/// Summary description for JSchException.
	/// </summary>
	public class JSchException : java.Exception
	{
		public JSchException() : base()
		{
			//
			// TODO: Add constructor logic here
			//
		}
        public JSchException(string msg, System.Collections.Generic.List<string> steps) : base (msg)
		{
            if (steps != null && steps.Count > 0)
            {
                this.Data["algneg"] = "";
                this.Data["algneg"] += "\r\n Algoritham negotiation details: \r\n =====================================\r\n";
                foreach (var step in steps)
                {
                    this.Data["algneg"] += step;
                    this.Data["algneg"] += "\r\n";

                }
                this.Data["algneg"] += "===================================== \r\n";
            }
		}

        public JSchException(string msg) : base(msg)
        {
        }
    }
}
