using Tamir.SharpSsh.java;
using String = System.String;

namespace Tamir.SharpSsh.jsch
{
    public class SftpException : Exception
    {
        public int id;
        public String message;

        public SftpException(int id, String message)
            : base()
        {
            this.id = id;
            this.message = string.IsNullOrEmpty(message)
                               ? "Error code = " + id
                               : message;
        }

        public override string ToString()
        {
            return message;
        }

        public override String toString()
        {
            return message;
        }
    }
}