﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataFtp
{
    public interface IShellCommunicator
    {
         bool SendShellCommand(string command, out string errorMessage);

    }
}
