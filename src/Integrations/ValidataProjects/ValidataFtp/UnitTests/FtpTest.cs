﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using NUnit.Framework;

namespace ValidataFtp.UnitTests
{
    [TestFixture]
    public class FtpTest
    {
        private FtpConnector _FtpConn;
        private string _Hostname;
        private string _User;
        private string _Password;

        [TestFixtureSetUp]
        public void Init()
        {
            // specify settings for a live FTP here
            _Hostname = "ftp://10.10.10.37";
            _User = "GlobusT24";
            _Password = "globus";
            _FtpConn = new FtpConnector(false, false, _Hostname, _User, _Password, true, false, false);
        }

        [Test]
        [Explicit]
        public void TestUnixFileFormatUpload()
        {
            const string uploadedData = "str\r\n\r\nstr";
            FtpConnector ftpConn = new FtpConnector(false, false, "ftp://66.40.52.5", "stavas7", "2232189", true, false, false); // ftp on unix server 
            ftpConn.UploadFile("/sometestfile.txt", StrToByteArray(uploadedData), true);

            ftpConn.UseBinary = false;
            string downloadedData = ByteArrayToStr(ftpConn.DownloadFile("/sometestfile.txt"));

            Assert.AreNotEqual(uploadedData, downloadedData);

            ftpConn.DeleteFile("/sometestfile.txt");
        }

        [Test]
        public void TestFtpPath()
        {
            string res1 = FtpPath.Build("/shb/t24dev/bnk/bnk.run", "t24dev/bnk/bnk.run", "/shb/t24dev/bnk/bnk.run/dwlib/lib.el", "");
            Assert.AreEqual("t24dev/bnk/bnk.run/dwlib/lib.el", res1);

            string res2 = FtpPath.Build("/shb/t24dev/bnk/bnk.run", "/shb/t24dev/bnk/bnk.run", "/shb/t24dev/bnk/bnk.run/dwlib/lib.el", "");
            Assert.AreEqual( "/shb/t24dev/bnk/bnk.run/dwlib/lib.el", res2);

            string res3 = FtpPath.Build("/shb/t24dev/bnk/bnk.run", "", "/shb/t24dev/bnk/bnk.run/dwlib/lib.el", "");
            Assert.AreEqual("dwlib/lib.el", res3);
        }

        [Test]
        [Explicit]
        public void TestShouldConnectUsingAnonymous()
        {
            FtpConnector ftpConn = new FtpConnector(false, false, _Hostname, "anonymous", "", true, false, false);
            ftpConn.UploadFile("test.txt", new byte[] {0, 0}, true);
            Assert.AreEqual(2, GetFileSizetOnFtp("/test.txt"));
            ftpConn.DeleteFile("/test.txt");
        }

        [Test]
        [Explicit]
        [ExpectedException(typeof(WebException))]
        public void TestShouldRaiseProtocolErrorException()
        {
            FtpConnector ftpConn = new FtpConnector(false, false, "ftp://127.0.0.1", "test", "beta", true, false, false); // This should be wrong parameter
            ftpConn.DownloadFile("/someNotExistingPath");
        }

        [Test]
        [Explicit]
        [ExpectedException(typeof(WebException))]
        public void TestShouldRaiseFileNotFoundException()
        {
            _FtpConn.DownloadFile("/someNotExistingPath"); 
        }

        [Test]
        [Explicit]
        public void TestShouldUploadBinaryArray()
        {
            FileInfo fileInfo = new FileInfo(CreateTestFile());
            string ftpFile = "/" + fileInfo.Name;
            FileStream fs = fileInfo.OpenRead();

            byte[] fileData = new byte[fileInfo.Length];
            fs.Read(fileData, 0, (int) fileInfo.Length);

            _FtpConn.UploadFile(ftpFile, fileData, true);

            Assert.AreEqual(GetFileSizetOnFtp(ftpFile), fileInfo.Length);

            fs.Close();

            DeleteFileOnFtp(ftpFile);
        }

        #region Helpers

        private static string CreateTestFile()
        {
            string fileName = Path.GetTempFileName();
            FileStream stream = new FileStream(fileName, FileMode.Append, FileAccess.Write);
            StreamWriter writer = new StreamWriter(stream);
            try
            {
                writer.BaseStream.Seek(0, SeekOrigin.End);
                writer.WriteLine("asd" + Guid.NewGuid());
            }
            catch (IOException ex)
            {
                Debug.Fail(ex.ToString());
            }
            finally
            {
                writer.Close();
            }
            return fileName;
        }

        private long GetFileSizetOnFtp(string ftpFile)
        {
            try
            {
                FtpWebRequest reqFTP = (FtpWebRequest) WebRequest.Create(_Hostname + ftpFile);
                reqFTP.Method = WebRequestMethods.Ftp.GetFileSize;
                reqFTP.KeepAlive = false;
                reqFTP.UseBinary = true;
                reqFTP.UsePassive = false;
                reqFTP.Credentials = new NetworkCredential(_User, _Password);
                FtpWebResponse response = (FtpWebResponse) reqFTP.GetResponse();

                return response.ContentLength;
            }
            catch (WebException)
            {
                return 0;
            }
        }

        private void DeleteFileOnFtp(string ftpFile)
        {
            try
            {
                _FtpConn.DeleteFile(ftpFile);
            }
            catch (WebException) {}
        }

        public static string ComputeMD5Hash(string fileName)
        {
            return ComputeHash(fileName, new MD5CryptoServiceProvider());
        }

        public static string ComputeHash(string fileName, HashAlgorithm hashAlgorithm)
        {
            var stmcheck = File.OpenRead(fileName);
            try
            {
                byte[] hash = hashAlgorithm.ComputeHash(stmcheck);
                string computed = BitConverter.ToString(hash).Replace("-", "");
                return computed;
            }
            finally
            {
                stmcheck.Close();
            }
        }

        public static byte[] StrToByteArray(string str)
        {
            return new ASCIIEncoding().GetBytes(str);
        }

        public static string ByteArrayToStr(byte[] array)
        {
            return new ASCIIEncoding().GetString(array);
        }

        #endregion
    }
}