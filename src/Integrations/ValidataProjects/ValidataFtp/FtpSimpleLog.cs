﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using ValidataCommon;

namespace ValidataFtp
{
    public class FtpSimpleLog
    {
        // TODO: Load from configuration
        public static bool Enabled = true;

        public static void LogLines(params string[] lst)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string l in lst)
            {
                sb.AppendLine(l);
            }

            Log(sb.ToString());
        }

        public static void LogLine(string str)
        {
            Log(str);
        }

        private static Logger _Logger;

        public static string LogsSubfolder { get; set; }

        private static string CreateNewFilePath()
        {
            string ouptutPath = Path.GetDirectoryName(Application.ExecutablePath);
            if (ouptutPath.Contains("\\Web Applications\\bin") || ouptutPath.Contains("\\WebServer\\bin"))
            {
                // this hack work in case the host process is AdapterGateway or other executable located in web application\bin directory
                ouptutPath = Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + "AdapterFiles\\";
            }

            string path = Path.Combine(ouptutPath, "Logs\\Ftp");
            if (!string.IsNullOrEmpty(LogsSubfolder))
            {
                path = Path.Combine(path, LogsSubfolder);
            }
            Directory.CreateDirectory(path);

            path = Path.Combine(path, string.Format("ftp_{0}.txt", DateTime.Now.ToString("yyyyMMdd_HHmmss")));
            return path;
        }

        private static void Log(string str)
        {
            if (!Enabled)
            {
                return;
            }

            if (_Logger == null)
            {
                _Logger = new Logger(CreateNewFilePath(), true, false);
            }

            try
            {
                _Logger.WriteMessage(str);
            }
            catch (Exception ex)
            {
                Debug.Write(ex);
            }
        }
    }
}