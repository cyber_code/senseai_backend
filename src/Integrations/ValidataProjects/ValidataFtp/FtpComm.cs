﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;

namespace ValidataFtp
{
    internal class FtpComm : FtpCommBase
    {
        private FtpSocketConnector _AccessRightsFtpConn;
        private int _RequestTimeOut = 10*1000;

        #region IFtpComm Members

        internal static int DefaultPort
        {
            get { return 21; }
        }

        internal override void Upload(string destFilePath, byte[] binaryArray)
        {
            // Get and Configurate ftp request object
            FtpWebRequest request = GetRequest(destFilePath);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.ContentLength = binaryArray.Length;

            // Write binary data to stream
            Stream outputStream = request.GetRequestStream();
            outputStream.Write(binaryArray, 0, binaryArray.Length);
            outputStream.Close();
        }

        /// <summary>
        /// Download file from ftp server
        /// </summary>
        /// <param name="destFilePath">path to file on ftp</param>
        /// <param name="tempFile">local path to file for save</param>
        internal override void Download(string destFilePath, string tempFile)
        {
            Download(destFilePath, tempFile, FileMode.OpenOrCreate);
        }

        /// <summary>
        /// Delete file on ftp server
        /// </summary>
        /// <param name="destFilePath"></param>
        internal override void Delete(string destFilePath)
        {
            var request = GetRequest(destFilePath);
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            request.GetResponse();
        }

        internal override void CreateDirectory(string destDirectoryPath)
        {
            var request = GetRequest(destDirectoryPath);
            request.Method = WebRequestMethods.Ftp.MakeDirectory;
            request.GetResponse();
        }

        private FtpDirectoryListingParams GetFtpDirectorySettings(OFSCommonMethods.IO.Settings settings)
        {
            // TODO add caching, since creation of regexes is expensive
            var ftpParams = new FtpDirectoryListingParams();
            ftpParams.ListLineRegex = FtpDirectoryListParser.GetListLineRegex(settings.FtpListLineRegex);
            ftpParams.SkippedLinesRegex = FtpDirectoryListParser.GetSkippedLinesRegexes(settings.FtpListSkippedLinesRegex);
            ftpParams.DirectoryGroupText = settings.FtpDirectoryGroupText;

            if(string.IsNullOrEmpty(ftpParams.DirectoryGroupText))
            {
                throw new ApplicationException("The DirectoryGroupText used for FTP directory listing parsing is empty");
            }

            return ftpParams;
        }

        internal override List<FtpListItem> ListDirectory(string directoryPath, OFSCommonMethods.IO.Settings settings, bool isWinOS)
        {
            string listResponse = ListDirectoryA(directoryPath, true);
            FtpSimpleLog.LogLine(listResponse);

            List<FtpListItem> contents = FtpDirectoryListParser.Parse(listResponse, GetFtpDirectorySettings(settings));

            foreach (FtpListItem item in contents)
            {
                if (directoryPath.Length <= 1)
                {
                    item.FullPath = directoryPath + item.Name;
                }
                else
                {
                    item.FullPath = directoryPath +
                                    (!isWinOS ? Path.AltDirectorySeparatorChar : Path.DirectorySeparatorChar) +
                                    item.Name;
                }
            }

            return contents;
        }

        internal override bool TestConnection(string hostnameWithoutPrefix, out string errorMessage)
        {
            // TODO - May be use not default but given port!
            return FtpSocketConnector.TryConnectToFtpServer(hostnameWithoutPrefix, DefaultPort, out errorMessage);
        }

        internal override bool ChangeAccessRights(string destPath, string rights, out string communicationLog, out string errMessage)
        {
            return FtpSocketConnector.ChangeAccessRights(
                    _Settings,
                    destPath,
                    rights,
                    out communicationLog,
                    out errMessage,
                    ref _AccessRightsFtpConn
                );
        }

        internal override bool DirectoryExists(string destDirecotryPath)
        {
            // TODO - This logic is not good - please find good method to detect existance
            //      Or just try to create and catch exceptions of existance!
            try
            {
                try
                {
                    ListDirectoryA(destDirecotryPath, false);
                }
                catch (WebException)
                {
                    string strResponse = ListDirectoryA(destDirecotryPath, true);
                    // This occurs also in case directory is empty?!?!?
                    bool exists = !string.IsNullOrEmpty(strResponse);
                    return exists;
                }

                return true;
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    FtpWebResponse response = (FtpWebResponse)ex.Response;
                    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    {
                        // Directory not found.  
                        return false;
                    }
                }

                throw;
            }
        }

        public override void Release()
        {
            if (_AccessRightsFtpConn != null)
                _AccessRightsFtpConn.Disconnect();
        }

        #endregion

        #region Class Lifecycle

        internal FtpComm(FtpConnectionSettings settings) 
            : base(settings) 
        {
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Return configurated ftp request object
        /// </summary>
        private FtpWebRequest GetRequest(string destPath)
        {
            destPath = GetFtpPath(destPath, _Settings.WorkingDirectory);

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(_Settings.Host + destPath));
            request.KeepAlive = _Settings.KeepAliveDefault;
            request.UseBinary = _Settings.UseBinary;
            request.UsePassive = _Settings.UsePassive;
            request.Credentials = new NetworkCredential(_Settings.UserName, _Settings.Password);
            request.EnableSsl = _Settings.EnableSsl;
            request.Timeout = _Settings.Timeout;
            request.Proxy = null;
            // TODO: request.Proxy = new WebProxy(
            //  + ProxyCredentials...
            // m_proxy = New WebProxy(ProxyName, ProxyPort)
            // m_proxy.Credentials = New NetworkCredential(ProxyUserName, ProxyPwd, ProxyDomain)

            return request;
        }

        /// <summary>
        /// Download file from ftp server
        /// </summary>
        /// <param name="destFilePath">path to file on ftp</param>
        /// <param name="filePath">local path to file for save</param>
        /// <param name="mode">file mode for file</param>
        private void Download(string destFilePath, string filePath, FileMode mode)
        {
            using (var outputStream = new FileStream(filePath, mode))
            {
                Download(destFilePath, outputStream);
            }
        }

        /// <summary>
        /// Download file from ftp server
        /// </summary>
        /// <param name="destFilePath">path to file on ftp</param>
        /// <param name="outputStream">output stream object</param>
        private void Download(string destFilePath, Stream outputStream)
        {
            // Get and Configurate ftp request object
            var request = GetRequest(destFilePath);

            request.Method = WebRequestMethods.Ftp.DownloadFile;

            using (var response = (FtpWebResponse)request.GetResponse())
            {
                using (var ftpStream = response.GetResponseStream())
                {
                    const int bufferSize = 2048;
                    var buffer = new byte[bufferSize];
                    int contentLength;

                    do
                    {
                        contentLength = ftpStream.Read(buffer, 0, bufferSize);
                        outputStream.Write(buffer, 0, contentLength);
                    }
                    while (contentLength != 0);
                }
            }
        }

        private string ListDirectoryA(string directoryPath, bool detailed)
        {
            using (Stream stream = ListDirectoryContents(directoryPath, detailed))
            {
                string listDir = (new StreamReader(stream)).ReadToEnd();
                return listDir;
            }
        }

        /// <summary>
        /// List directories under give path
        /// </summary>
        /// <param name="destDirecotryPath">directory for list</param>
        /// <param name="detailed">list details for directories</param>
        /// <returns>stream to read</returns>
        private Stream ListDirectoryContents(string destDirecotryPath, bool detailed)
        {
            var request = GetRequest(destDirecotryPath);
            request.Method = detailed ? WebRequestMethods.Ftp.ListDirectoryDetails : WebRequestMethods.Ftp.ListDirectory;
            request.KeepAlive = _Settings.KeepAliveFirst;
            request.Timeout = _RequestTimeOut;
            Stream result = request.GetResponse().GetResponseStream();
            return result;
        }

        #endregion

    }
}
