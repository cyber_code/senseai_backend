﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataFtp
{
    internal abstract class FtpCommBase
    {
        protected FtpConnectionSettings _Settings;
        internal FtpConnectionSettings FtpSettings
        {
            get { return _Settings; }
        }

        internal FtpCommBase(FtpConnectionSettings connSettings)
        {
            System.Diagnostics.Debug.Assert(connSettings != null);
            _Settings = connSettings;
        }

        internal abstract void Upload(string destFilePath, byte[] binaryArray);

        internal abstract void Download(string destFilePath, string tempFile);

        internal abstract void Delete(string destFilePath);

        internal abstract void CreateDirectory(string destDirectoryPath);

        internal abstract List<FtpListItem> ListDirectory(string directoryPath, OFSCommonMethods.IO.Settings settings, bool isWinOS);

        internal abstract bool DirectoryExists(string destDirecotryPath);

        internal abstract bool TestConnection(string hostnameWithoutPrefix, out string errorMessage);

        internal abstract bool ChangeAccessRights(string destPath, string rights, out string communicationLog, out string errMessage);

        #region Protected and private methods

        protected virtual string GetFtpPath(string destPath, string workingDir)
        {
            return FtpPath.EscapePathCharacters(GetFtpPathA(destPath, workingDir));
        }

        private string GetFtpPathA(string destPath, string workingDir)
        {
            if (string.IsNullOrEmpty(destPath))
            {
                return workingDir;
            }

            while (!string.IsNullOrEmpty(destPath) && destPath.StartsWith("/"))
            {
                destPath = destPath.Substring(1);
            }

            if (!workingDir.EndsWith("/") && !workingDir.EndsWith("\\"))
            {
                workingDir = workingDir + "/" ;
            }

            string result = workingDir + destPath;
            if (!string.IsNullOrEmpty(result))
            {
                if (!result.StartsWith("/"))
                {
                    result = "/" + result;
                }
            }

            return result.Replace("\\", "/").TrimEnd(new char[] { '/' });
        }

        #endregion

        public virtual void Release() { }
    }
}
