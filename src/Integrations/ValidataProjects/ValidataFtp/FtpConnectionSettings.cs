﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataFtp
{
    public class FtpConnectionSettings
    {
        public string Host;

        public int Port;

        public string UserName;

        public string Password;

        public string WorkingDirectory;

        public bool KeepAliveFirst;

        public bool KeepAliveDefault;

        public bool UseBinary;

        public bool UsePassive;

        public bool EnableSsl;

        public int Timeout;

        public string IdentityFilePath;

        public string Passphrase;
    }
}
