﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using VSSH;

namespace ValidataFtp
{
    internal class SFtpComm : FtpCommBase
    {
        #region IFtpComm Members

        internal static int DefaultPort
        {
            get { return 22; }
        }

        internal override void Upload(string destFilePath, byte[] binaryArray)
        {
            string errMsg;
            if (!VSshFtp.Upload(CreateTempFile(binaryArray), GetFtpPath(destFilePath, getWorkingDir()), out errMsg))
            {
                throw new Exception(errMsg);
            }
        }

        internal override void Download(string destFilePath, string tempFile)
        {
            string errMsg;
            if (!VSshFtp.Download(GetFtpPath(destFilePath, getWorkingDir()), tempFile, out errMsg))
            {
                throw new Exception(errMsg);
            }
        }

        internal override void Delete(string destFilePath)
        {
            string errMsg;
            if (!VSshFtp.Delete(GetFtpPath(destFilePath, getWorkingDir()), out errMsg))
            {
                throw new Exception(errMsg);
            }
        }

        internal override void CreateDirectory(string destDirectoryPath)
        {
            string errMsg;
            if (!VSshFtp.CreateDirectory(GetFtpPath(destDirectoryPath, getWorkingDir()), out errMsg))
            {
                throw new Exception(errMsg);
            }
        }

        internal override List<FtpListItem> ListDirectory(string directoryPath, OFSCommonMethods.IO.Settings settings, bool isWinOS)
        {
            string errMsg;
            var ls = VSshFtp.List(GetFtpPath(directoryPath, getWorkingDir()), out errMsg);
            if (ls == null)
            {
                throw new Exception(errMsg);
            }

            List<FtpListItem> result = new List<FtpListItem>();

            foreach (string name in ls.Keys)
            {
                if (name == "." || name == "..")
                {
                    continue;
                }

                FtpListItem item = new FtpListItem();
                if (directoryPath.Length <= 1)
                {
                    item.FullPath = directoryPath + name;
                }
                else
                {
                    item.FullPath = directoryPath +
                                    (!isWinOS ? Path.AltDirectorySeparatorChar : Path.DirectorySeparatorChar) +
                                    name;
                }

                item.Name = name;
                item.IsDirectory = IsDirectory(ls[name]);

                result.Add(item);
            }

            return result;
        }

        private string getWorkingDir()
        {
            var sshNetComm = (VSshFtp as VSshNETFtp);
            return 
                sshNetComm != null 
                && string.IsNullOrEmpty (sshNetComm.WorkingDir ) 
                ? _Settings.WorkingDirectory 
                : sshNetComm.WorkingDir;
        }

        internal override bool DirectoryExists(string destDirecotryPath)
        {
            try
            {
                var ls = ListDirectory(destDirecotryPath, null, false);
                return ls != null;
            }
            catch
            {
                return false;
            }
        }

        internal override bool TestConnection(string hostnameWithoutPrefix, out string errorMessage)
        {
            VSshFtpBase conn;

            if (_UseSharpSSH)
                conn = new VSshFtp(_Settings.Host, _Settings.Port, _Settings.UserName, _Settings.Password, VSshFtpBase.Protocol.SFTP);
            else
                conn = new VSshNETFtp(_Settings.Host, _Settings.Port, _Settings.UserName, _Settings.Password, VSshFtpBase.Protocol.SFTP);


            if (!string.IsNullOrEmpty(_Settings.IdentityFilePath))
            {
                conn.AddIdentityFile(_Settings.IdentityFilePath, _Settings.Passphrase);
            }
            
            if (conn.Connect(out errorMessage))
            {
                conn.Disconnect();
                return true;
            }

            return false;
        }

        internal override bool ChangeAccessRights(string destPath, string rights, out string communicationLog, out string errMessage)
        {
            communicationLog = string.Empty;
            return VSshFtp.ChangeAccessRights(destPath, rights, out errMessage);
        }

        public override void Release()
        {
            if(_VSshFtp != null)
                _VSshFtp.Disconnect();
        }

        #endregion

        #region Properties

        private VSshFtpBase _VSshFtp = null;
        private bool _UseSharpSSH;

        private VSshFtpBase VSshFtp
        {
            get {
                if (_VSshFtp == null)
                {
                    if (_UseSharpSSH)
                        _VSshFtp = new VSshFtp(_Settings.Host, _Settings.Port, _Settings.UserName, _Settings.Password, VSshFtpBase.Protocol.SFTP);
                    else
                        _VSshFtp = new VSshNETFtp(_Settings.Host, _Settings.Port, _Settings.UserName, _Settings.Password, VSshFtpBase.Protocol.SFTP);

                    if (!string.IsNullOrEmpty(_Settings.IdentityFilePath))
                    {
                        _VSshFtp.AddIdentityFile(_Settings.IdentityFilePath, _Settings.Passphrase);
                    }

                    string errMsg;
                    if (!_VSshFtp.Connect(out errMsg))
                    {
                        _VSshFtp = null;
                        throw new Exception(errMsg);
                    }
                }

                return _VSshFtp;
            }
        }

        #endregion

        #region Class Lifecycle

        internal SFtpComm(FtpConnectionSettings settings, bool useSharpSSh) 
            : base(settings) 
        {
            _UseSharpSSH = useSharpSSh;
        }
        
        ~SFtpComm()
        {
            Release();
        }

        #endregion

        #region Private Methods

        private string CreateTempFile(byte[] binaryArray)
        {
            var tempFile = System.IO.Path.GetTempFileName();
            if (binaryArray == null)
            {
                binaryArray = new byte[0] { };
            }

            System.IO.File.WriteAllBytes(tempFile, binaryArray);

            return tempFile;
        }

        private bool IsDirectory(string p)
        {
            return p.StartsWith("d") ||
                     p.StartsWith("l") ||
                     p.StartsWith("D") ||
                     p.StartsWith("L");
        }


        #endregion
    }
}
