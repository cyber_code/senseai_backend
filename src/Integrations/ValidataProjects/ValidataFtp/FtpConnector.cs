﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Linq;
using System.Windows.Forms;
using SetPermissionsApproach = OFSCommonMethods.IO.Settings.SetPermissionsApproach;
using OFSCommonMethods.IO;
using OFSCommonMethods.IO.Telnet;

namespace ValidataFtp
{
    
    public class FtpConnector : ValidataCommon.IABCFileConnector
    {
        #region Constants and Statics

        /// <summary>
        /// Carriage Return (ASCII 13)
        /// </summary>
        private const byte CR = 13;

        /// <summary>
        /// Line Feed (ASCII 10)
        /// </summary>
        private const byte LF = 10;

        /// <summary>
        /// Ftp Path Delimiter
        /// </summary>
        private const string PathDelimiter = "/";

        #endregion

        #region Private Memebers

        enum CRTreatment { RemoveCR, AddCR, None };
        
        private string _ErrorMessage = String.Empty;

        private FtpCommBase _Comm;

        #endregion

        #region Public Properties
        public Settings.SetPermissionsApproach SetAccessApproach { get; set; }

        public IShellCommunicator ShellComm { get; set;  }

        public char DirectorySeparator
        {
            get { return IsWinOS ? '\\' : '/'; }
        }

        public string Hostname
        {
            get { return _Comm.FtpSettings.Host; }
        }

        public string HostnameWithoutPrefix
        {
            get {
                string prefix = "sftp://";
                if (!Hostname.Contains(prefix))
                    prefix = "ftp://";
                return Hostname.Replace(prefix, ""); 
            }
        }

        public Settings Settings
        {
            get;
            private set;
        }

        public string User
        {
            get { return _Comm.FtpSettings.UserName; }
        }

        public bool EnableSsl
        {
            get { return _Comm.FtpSettings.EnableSsl; }
            set { _Comm.FtpSettings.EnableSsl = value; }
        }

        public bool IsWinOS
        {
            get;
            set;
        }

        /// <summary>
        /// Whether port for incoming data form server is decide from server of client
        /// True - port is decided by server
        /// False - port is decided by clinet
        /// </summary>
        public bool UsePassive
        {
            get { return _Comm.FtpSettings.UsePassive; }
            set { _Comm.FtpSettings.UsePassive = value; }
        }

        /// <summary>
        /// Transfer data as binary sequential
        /// </summary>
        public bool UseBinary
        {
            get { return _Comm.FtpSettings.UseBinary; }
            set { _Comm.FtpSettings.UseBinary = value; }
        }

        public string ErrorMessage
        {
            get { return _ErrorMessage; }
        }

        public string IdentityFilePath
        {
            get { return _Comm.FtpSettings.IdentityFilePath; }
            set { _Comm.FtpSettings.IdentityFilePath = value; }
        }

        public string Passphrase
        {
            get { return _Comm.FtpSettings.Passphrase; }
            set { _Comm.FtpSettings.Passphrase = value; }
        }

        public bool SkipDirChecks { get; set; }

        #endregion

        #region Events

        public event CommandExecutionError OnCommandExecutionError;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Just for unit tests
        /// </summary>
        /// <param name="hostname"></param>
        internal FtpConnector(bool isSFtp, bool useSharpSSH, string hostname, string user, string password, bool keepAliveDefault, bool keepAliveFirst, bool usePassive)
           : this(isSFtp, useSharpSSH, String.Empty, hostname, user, password, keepAliveDefault, keepAliveFirst, usePassive)
        {
        }


        public FtpConnector(bool isSFtp, bool useSharpSSH, string configurationSet, string hostname, string user, string password, bool keepAliveDefault, bool keepAliveFirst, bool usePassive = false, string logsSubfolder = "")
        {
            FtpSimpleLog.LogsSubfolder = logsSubfolder;

            if (!String.IsNullOrEmpty(configurationSet))
            {
                Settings = new Settings();
                Settings.PhantomSet = configurationSet;
                ExecutionStepContainer.LoadConnectionSettings(Settings);
                usePassive = Settings.FtpUsePassive;
            }

            FtpConnectionSettings ftpConnectionSettings = new FtpConnectionSettings(){
                        Host = hostname,
                        UserName = user,
                        Password = password,
                        Timeout = -1,
                        EnableSsl = false,
                        UseBinary = false,
                        WorkingDirectory = PathDelimiter,//OBSOLETE: GetWorkingDirectory(null),
                        KeepAliveDefault = keepAliveDefault,
                        KeepAliveFirst = keepAliveFirst,
                        UsePassive = usePassive
                    };

            ftpConnectionSettings.Port = ExtractPort(hostname);
            SetAccessApproach = SetPermissionsApproach.Communicator;

            if (!isSFtp)
            {
                if (ftpConnectionSettings.Port < 0)
                {
                    ftpConnectionSettings.Port = FtpComm.DefaultPort;
                }
                
                _Comm = new FtpComm(ftpConnectionSettings);
            }
            else
            {
                if (ftpConnectionSettings.Port < 0)
                {
                    ftpConnectionSettings.Port = SFtpComm.DefaultPort;
                }

                ftpConnectionSettings.Host = ExtractIP(hostname);
                _Comm = new SFtpComm(ftpConnectionSettings, useSharpSSH);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Try connect to ftp server
        /// </summary>
        /// <returns></returns>
        public bool TryConnectToFtpServer()
        {
            return _Comm.TestConnection(HostnameWithoutPrefix, out _ErrorMessage);
        }

        /// <summary>
        /// Check if the ftp server is available and if not ask the user if he wants to retry connection
        /// </summary>
        /// <returns></returns>
        public bool MakeSureFtpServerIsAvailable()
        {
            while (true)
            {
                if (TryConnectToFtpServer())
                {
                    return true;
                }

                // if there is a problem say so, and ask the user if he wants to retry
                DialogResult result = MessageBox.Show(
                        _ErrorMessage,
                        HostnameWithoutPrefix + " FTP connection failure",
                        MessageBoxButtons.RetryCancel,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1
                    );

                if (result == DialogResult.Cancel)
                {
                    // give up trying
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets a list of directory items (files and subdirectories)
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <returns></returns>
        public List<ValidataCommon.FileNameListItem> ListDirectoryItems(string directoryPath)
        {

            List<FtpListItem> ifli = _Comm.ListDirectory(directoryPath, Settings, IsWinOS);
            List<ValidataCommon.FileNameListItem> IlistItems = new List<ValidataCommon.FileNameListItem>();
            foreach (var ifl in ifli)
        {
                IlistItems.Add(ifl);
        }
            return IlistItems;
        } 

        /// <summary>
        /// Create directory recursively on ftp server
        /// </summary>
        /// <param name="destDirectoryPath">directory to create on ftp server</param>
        public void CreateDirectoryRecursively(string destDirectoryPath)
        {
          
            bool shouldRetry = false;

            do
            {
                bool result = true;
                try
                {
                    FtpSimpleLog.LogLine(string.Format("Check for path: {0} and create subdirectories if neccessary", destDirectoryPath));
                    CreateDirectoriesIfNeeded(destDirectoryPath);
                }
                catch (WebException ex)
                {
                    FtpSimpleLog.LogLine(string.Format("\tException during check: {0}\r\n{1}\r\n", ex.Message, ex.StackTrace));
                    result = AskUserIfToRetry("Error checking directory: " + destDirectoryPath, ex, out shouldRetry);
                    if (!result && !shouldRetry)
                    {
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    FtpSimpleLog.LogLine(string.Format("\tEXCEPTION: {0}\r\n{1}\r\n", ex.Message, ex.StackTrace));
                    result = AskUserIfToRetry("Error checking directory: " + destDirectoryPath, ex, out shouldRetry);
                    if (!result && !shouldRetry)
                    {
                        throw;
                    }
                }

                if (result)
                {
                    return;
                }
            }
            while (shouldRetry);
        }

        /// <summary>
        /// Upload file to ftp server
        /// </summary>
        /// <param name="destFilePath">save path on ftp server</param>
        /// <param name="fileContents">file content as binary array</param>
        /// <param name="isSourceFile">Is Source File</param>
        public void UploadFile(string destFilePath, byte[] fileContents, bool isSourceFile)
        {
            bool shouldRetry = false;

            do
            {
                bool result = true;
                try
                {
                    FtpSimpleLog.LogLine(string.Format("UPLOADING FILE: {0} (Is Source: {1})", destFilePath, isSourceFile));
                    Upload(destFilePath, fileContents, isSourceFile);
                }
                catch (WebException ex)
                {
                    FtpSimpleLog.LogLine(string.Format("\tEXCEPTION: {0}\r\n{1}\r\n", ex.Message, ex.StackTrace));

                    result = AskUserIfToRetry("Error uploading file to: " + destFilePath, ex, out shouldRetry);
                    if (!result && !shouldRetry)
                    {
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    FtpSimpleLog.LogLine(string.Format("\tEXCEPTION: {0}\r\n{1}\r\n", ex.Message, ex.StackTrace));

                    result = AskUserIfToRetry("Error uploading file to: " + destFilePath, ex, out shouldRetry);
                    if (!result && !shouldRetry)
                    {
                        throw;
                    }
                }

                if (result)
                {
                    FtpSimpleLog.LogLine("\tSuccess uploading file");
                    return;
                }
            }
            while (shouldRetry);
        }

        /// <summary>
        /// Download file and return its content as binary array
        /// </summary>
        /// <param name="fileName">path to file on ftp</param>       
        public byte[] DownloadFile(string fileName)
        {
            bool shouldRetry;

            do
            {
                try
                {
                    FtpSimpleLog.LogLine(string.Format("DOWNLOADING FILE: {0}", fileName));
                    byte[] res = Download(fileName);
                    FtpSimpleLog.LogLine("\tSUCCESS");
                    return res;
                }
                //catch (WebException ex)
                //{
                //    FtpSimpleLog.LogLine(string.Format("\tEXCEPTION: {0}\r\n{1}\r\n", ex.Message, ex.StackTrace));
                //    bool result = AskUserIfToRetry("Error downloading file: " + fileName, ex, out shouldRetry);
                //    if (!result && !shouldRetry)
                //    {
                //        throw;
                //    }
                //}
                catch (Exception ex)
                {
                    FtpSimpleLog.LogLine(string.Format("\tEXCEPTION: {0}\r\n{1}\r\n", ex.Message, ex.StackTrace));
                    bool result = AskUserIfToRetry("Error downloading file: " + fileName, ex, out shouldRetry);
                    if (!result && !shouldRetry)
                    {
                        throw;
                    }
                }
            }
            while (shouldRetry);

            return null;
        }

        /// <summary>
        /// Delete file on ftp server
        /// </summary>
        /// <param name="destFilePath"></param>
        public void DeleteFile(string destFilePath)
        {
            bool shouldRetry = false;

            do
            {
                bool result = true;
                try
                {
                    FtpSimpleLog.LogLine(string.Format("DELETING FILE: {0}", destFilePath));
                    _Comm.Delete(destFilePath);
                }
                catch (WebException ex)
                {
                    FtpSimpleLog.LogLine(string.Format("\tEXCEPTION: {0}\r\n{1}\r\n", ex.Message, ex.StackTrace));
                    result = AskUserIfToRetry("Error deleting file: " + destFilePath, ex, out shouldRetry);
                    if (!result && !shouldRetry)
                    {
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    FtpSimpleLog.LogLine(string.Format("\tEXCEPTION: {0}\r\n{1}\r\n", ex.Message, ex.StackTrace));
                    result = AskUserIfToRetry("Error deleting file: " + destFilePath, ex, out shouldRetry);
                    if (!result && !shouldRetry)
                    {
                        throw;
                    }
                }

                if (result)
                {
                    FtpSimpleLog.LogLine("\tSUCCESS");
                    return;
                }
            }
            while (shouldRetry);
        }

        #endregion

        #region Private Methods

        #region Upload

        /// <summary>
        /// Upload file to ftp server
        /// </summary>
        /// <param name="destFilePath">save path on ftp server</param>
        /// <param name="fileContents">file content as binary array</param>
        /// <param name="isSourceFile">Is Source File</param>
        public void Upload(string destFilePath, byte[] fileContents, bool isSourceFile)
        {
            // this is a windows host but the file may or may not contain CR so process it anyway
            // if the env is windows => add CR if the file doesnt have CRs
            // if the env is unix => remove CR if the file has CRs

            CRTreatment crMode;

            if (IsWinOS)
            {
                crMode = fileContents.Contains<byte>(CR) ? CRTreatment.None : CRTreatment.AddCR;
            }
            else
                crMode = fileContents.Contains<byte>(CR) ? CRTreatment.RemoveCR : CRTreatment.None;

            byte[] binaryArray = handleCRLF(fileContents, isSourceFile, crMode, _Comm is SFtpComm, IsWinOS);

            _Comm.Upload(destFilePath, binaryArray);
            SetFullAccess(destFilePath);
        }

        private static byte[] handleCRLF(byte[] fileContents, bool isSourceFile, CRTreatment crMode, bool isSFTP, bool isWin)
        {
            // isWin means whether the reciving OS is windows, so for download always true, for upload depends on env

            byte[] result;

            if (isSourceFile)
            {
                // TODO : this is straight copy-paste from the shared wrapper so put in one place

                List<byte> bytelist;

                if (isSFTP)
                {
                    bytelist = SFTPhandleCRLF(fileContents, crMode);
                }
                else
                    bytelist = new List<byte>(fileContents);

                // The bellow enusres a newline at the end - as in the FTP connector
                if (bytelist.Count > 0 && bytelist.Last<byte>() != LF)
                {

                    if (isWin)
                        bytelist.Add(CR);
                    bytelist.Add(LF);

                }

                result = bytelist.ToArray();
            }
            else
                result = fileContents;

                

            return result;








            //    old logic:
            // ---------------
            //    int newLen = fileContents.Length + 2;
            //    byte[] binaryArray;

            //    binaryArray = new byte[newLen];

            //    if (
            //        fileContents.Length >= 2
            //        &&
            //        (fileContents[fileContents.Length - 1] != LF ||
            //         fileContents[fileContents.Length - 2] != CR)
            //        )
            //    {
            //        int cc = 0;
            //        foreach (byte b in fileContents)
            //        {
            //            binaryArray[cc] = b;
            //            cc++;
            //        }
            //        binaryArray[binaryArray.Length - 2] = CR;
            //        binaryArray[binaryArray.Length - 1] = LF;
            //    }
            //    else
            //    {
            //        binaryArray = fileContents;
            //    }
            //}
            //else
            //{
            //    binaryArray = fileContents;
            //}
            //return binaryArray;
        }

        private static List<byte> SFTPhandleCRLF(byte[] fileContents, CRTreatment crMode)
        {
            List<byte> bytelist;
            bytelist = new List<byte>();

            for (int bcc = 0; bcc < fileContents.Count<byte>(); bcc++)
            {
                byte bt = fileContents[bcc];

                switch (crMode)
                {
                    case CRTreatment.None:
                        bytelist.Add(bt);
                        break;

                    case CRTreatment.AddCR:
                        if (bt == LF)
                            bytelist.Add(CR);
                        bytelist.Add(bt);
                        break;

                    case CRTreatment.RemoveCR:
                        if (bt != CR)
                            bytelist.Add(bt);
                        break;
                }
            }
            return bytelist;
        }

        #endregion

        #region Download

        /// <summary>
        /// Download file and return its content as binary array
        /// </summary>
        /// <param name="destFilePath">path to file on ftp</param>        
        private byte[] Download(string destFilePath)
        {
            var tempFile = Path.GetTempFileName();
            _Comm.Download(destFilePath, tempFile);
            byte[] fileContents = File.ReadAllBytes(tempFile);
            
            // We dont seem to suport ASCII mode for donwloads, but if needed 
            // isSource must be provided in an argument
                       
            // CRTreatment crMode = fileContents.Contains<byte>(CR) ? CRTreatment.None : CRTreatment.AddCR;
            // fileContents = handleCRLF(fileContents, isSource, crMode, _Comm is SFtpComm, true);
            

            return fileContents;
        }

        #endregion

        #region Directory


        /// <summary>
        /// Create directory on ftp server
        /// </summary>
        /// <param name="destDirectoryPath">directory to create on ftp server</param>
        /// <param name="recursively">recursively create direcotry</param>
        private void CreateDirectoriesIfNeeded(string destDirectoryPath)
        {
            var createdPath = destDirectoryPath[0] == '/' ? "/" : "";
            foreach (var dir in destDirectoryPath.Split('/'))
            {
                if (dir == "")
                {
                    continue;
                }
                createdPath += dir;
                CreateOneDirectory(createdPath);
                createdPath += "/";
            }
        }

        private void CreateOneDirectory(string destDirectoryPath)
        {
            if (!SkipDirChecks)
            {
                if (DirectoryExist(destDirectoryPath))
                {
                    // SetFullAccess(destDirectoryPath);
                    return;
                }

            }


            try
            {
                FtpSimpleLog.LogLine(string.Format("Creating directory: {0}", destDirectoryPath));
                _Comm.CreateDirectory(destDirectoryPath);
                SetFullAccess(destDirectoryPath);
            }
            catch (Exception eee)
            {
                FtpSimpleLog.LogLine(string.Format("\tError creating directory: {0}", destDirectoryPath));
                
                // Since we are skipping dir checks a failed mkdir is not a fatal error
                if (SkipDirChecks) return;
                
                throw eee;
            }
            
        }


        /// <summary>
        /// Check for directory on ftp server
        /// </summary>
        /// <param name="destDirecotryPath">directory path on ftp</param>        
        public bool DirectoryExist(string destDirecotryPath)
        {
            FtpSimpleLog.LogLine(string.Format("Check for directory: {0}", destDirecotryPath));

            return _Comm.DirectoryExists(destDirecotryPath);
        }

        #endregion

        #region Helper

        private void SplitHostname(string hostname, out string ip, out int port)
        {
            ip = hostname;
            port = -1;

            int pos = hostname.LastIndexOf(":");
            if (pos < 0)
            {
                return;
            }

            for (int i = pos+1; i < hostname.Length; i++)
            {
                if (!char.IsDigit(hostname[i]))
                {
                    return;
                }
            }

            ip = hostname.Substring(0, pos);
            port = int.Parse(hostname.Substring(pos + 1));
        }

        private int ExtractPort(string hostname)
        {
            string ip;
            int port;
            SplitHostname(hostname, out ip, out port);

            return port;
        }

        private string ExtractIP(string hostname)
        {
            string s = ExtractIPWithoutPort(hostname);
            int pos = s.IndexOf("://");
            if (pos >= 0)
            {
                return s.Substring(pos + "://".Length);
            }

            return s;
        }

        private string ExtractIPWithoutPort(string hostname)
        {
            string ip;
            int port;
            SplitHostname(hostname, out ip, out port);

            return ip;
        }

        /// <summary>
        /// Set full access rights
        /// </summary>
        /// <param name="destPath"></param>
        /// <returns></returns>
        private bool SetFullAccess(string destPath)
        {
            string rights = "777";

            string logShell = string.Format("Changing access rights of: '{0}' to {1} through system shell chmod", destPath, rights);
            string logSkip = string.Format("Skipped changing access rights of: '{0}' ", destPath);
            string logComm = string.Format("Changing access rights of: '{0}' to {1} with FTP/SFTP", destPath, rights);

            switch (SetAccessApproach)
            { 
                case SetPermissionsApproach.Communicator:

                    FtpSimpleLog.LogLine(logComm);
                    return ChangeAccessRights(destPath, "777");

                case SetPermissionsApproach.Shell:
                    
                    // So far the implementation is such that 
                    // whichever class uses the FTP communicator is responsible 
                    // to set the approach to Shell only in case the OS is UNIX
                    // so chmod is valid
                    
                    string etext;
                    
                    if (ShellComm == null)
                    {
                        FtpSimpleLog.LogLine(logSkip);
                        return true;
                    }
                    else
                    {
                        FtpSimpleLog.LogLine(logShell);
                        return ShellComm.SendShellCommand(string.Format("chmod {0} {1}", rights, destPath), out etext);
                    }

                default:
                        FtpSimpleLog.LogLine(logSkip);
                        return true;
            
            }
            
        }

        public void Release()
        {
            if (_Comm != null)
                _Comm.Release();
        }

        /// <summary>
        /// Changing access rights
        /// </summary>
        /// <param name="destPath"></param>
        /// <param name="rights"></param>
        /// <returns></returns>
        private bool ChangeAccessRights(string destPath, string rights)
        {
            string communicationLog;
            string errMessage;
            bool res = _Comm.ChangeAccessRights(
                    destPath,
                    rights,
                    out communicationLog,
                    out errMessage
                );

            if (!string.IsNullOrEmpty(communicationLog))
            {
                FtpSimpleLog.LogLine("COMMUNICATION LOG:");
                FtpSimpleLog.LogLine(communicationLog);
            }

            if (res)
            {
                FtpSimpleLog.LogLine("\tSUCCESS");
            }
            else
            {
                FtpSimpleLog.LogLine(string.Format("\tERROR: {0}\r\n", errMessage));
            }

            return res;
        }

        #endregion

        #region Call Back

        private bool AskUserIfToRetry(string errorText, Exception exception, out bool shouldRetry)
        {
            shouldRetry = false;

            FTPExecutionEventArgs.Action doAction;
            string userMessage = errorText + "\r\n\r\n" + exception.Message;
            FireError("FTP Error", userMessage, out doAction);
            switch (doAction)
            {
                case FTPExecutionEventArgs.Action.Abort:
                    return false;

                case FTPExecutionEventArgs.Action.Retry:
                    shouldRetry = true;
                    return false;

                default:
                    throw new InvalidOperationException("Unkown Action");
            }
        }

        private void FireError(string section, string errorMessage, out FTPExecutionEventArgs.Action doAction)
        {
            if (OnCommandExecutionError == null)
            {
                doAction = FTPExecutionEventArgs.Action.Abort;
                return;
            }

            FTPExecutionEventArgs evArgs = new FTPExecutionEventArgs(section, errorMessage);
            OnCommandExecutionError(this, evArgs);

            doAction = evArgs.ErrorTreatAction;
        }

        #endregion

        #endregion

        
    }
}