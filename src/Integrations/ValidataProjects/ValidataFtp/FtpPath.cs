﻿using System;
using System.Diagnostics;
using System.Web;
using Common;
using System.Collections.Generic;
using ValidataCommon;

namespace ValidataFtp
{
    public class FtpPath
    {
        // TODO Get rid of VirtualPathUtility - it is for web pages, not for file systems.

        public static string GetDeploymentPath(FolderMapList folderMapList, string relationalPath, out bool isCatalog)
        {
            relationalPath = RemoveLeadingSlashes(relationalPath);

            foreach(FolderMap fm in folderMapList)
            {
                if (StringMethods.Path.CompareWildcards(relationalPath, fm.Source, true))
                {
                    isCatalog = (fm.Action == FolderMap.Actions.Catalog);
                    return fm.Target;
                }
            }

            isCatalog = false;
            return relationalPath;

            //TODO????_FilesUploadedToRelativePath ? file.RelationalPath : ".";
        }

        public static string BuildHomeRelative(string ftpHome, FolderMapList folderMapList, string relationalPath, out bool isCatalog, string logsSubfolder)
        {
            ValidataFtp.FtpSimpleLog.LogsSubfolder = logsSubfolder;
            try
            {
                relationalPath = RemoveLeadingSlashes(relationalPath);

                FtpSimpleLog.LogLines(
                        "BuildHomeRelative() ftp path:",
                        "\tFtp Home: " + ftpHome,
                        "\tFile Relative Path: " + relationalPath,
                        "\r\nUsing folder map: " + folderMapList.ToString()
                    );

                string result = ftpHome;
                if (result == null)
                {
                    result = string.Empty;
                }

                relationalPath = GetDeploymentPath(folderMapList, relationalPath, out isCatalog);
                if (isCatalog)
                {
                    FtpSimpleLog.LogLine("\tBuilt Ftp Path (Catalog): " + relationalPath);
                    return relationalPath;
                }

                if (ftpHome == string.Empty && relationalPath == string.Empty)
                {
                    FtpSimpleLog.LogLine("\tBuilt Ftp Path: " + string.Empty);
                    return string.Empty;
                }

                bool appendedLeadingSlash = false;
                if (!result.StartsWith("/") && !result.StartsWith("\\"))
                {
                    appendedLeadingSlash = true;
                    result = "/" + result;
                }

                result = VirtualPathUtility.AppendTrailingSlash(result);

                if (!string.IsNullOrEmpty(relationalPath))
                {
                    result = VirtualPathUtility.Combine(result, relationalPath);
                }

                //result = Concat(result, relationalPath);

                if (appendedLeadingSlash)
                {
                    result = result.Substring(1);
                }

                result = result.Replace("\\", "/");

                FtpSimpleLog.LogLine("\tBuilt Ftp Path: " + result);

                return result;
            }
            catch (HttpException ex)
            {
                throw new ApplicationException(
                    string.Format("Unable to build deployment path! {0} Ftp Home='{1}'; Deploy directory='{2}'",
                        ex.Message, ftpHome,relationalPath
                        ),
                        ex
                    );
            }
        }

        private static string RemoveLeadingSlashes(string path)
        {
            try
            {
                int i = 0;
                for (; i < path.Length; i++)
                {
                    if (path[i] != '\\' && path[i] != '/')
                    {
                        break;
                    }
                }

                return path.Substring(i);
            }
            catch
            {
                return path;
            }
        }

        public static string MakeRelative(string baseDirPath, string path)
        {
            if (IsFullyQualifiedWindowsPath(baseDirPath))
            {
                // handle Windows path
                Debug.Assert(IsFullyQualifiedWindowsPath(path));

                // make sure it is treated as a folder
                if (!baseDirPath.EndsWith("\\"))
                {
                    baseDirPath = baseDirPath + "\\";
                }

                string res = VirtualPathUtility.MakeRelative(baseDirPath.Substring(2), path.Substring(2));
                return res.Replace("/", "\\");
            }

            // handle a UNIX path - make sure it is treated as a folder
            if (!baseDirPath.EndsWith("/"))
            {
                baseDirPath = baseDirPath + "/";
            }
            return VirtualPathUtility.MakeRelative(baseDirPath, path);
        }

        public static bool IsFullyQualifiedWindowsPath(string baseDirPath)
        {
            return (baseDirPath.Length >= 3)
                   && (char.IsLetter(baseDirPath[0]))
                   && (baseDirPath[1] == ':')
                   && (baseDirPath[2] == '\\' || baseDirPath[2] == '/');
        }

        public static string Build(string t24HomeDir, string ftpWorkingDirectory, string filePath, string logsSubfolder)
        {
            ValidataFtp.FtpSimpleLog.LogsSubfolder = logsSubfolder;
            // TODO - REVISE
            FtpSimpleLog.LogLines(
                    "Build() ftp path:",
                    "\tT24HomeDir: " + t24HomeDir,
                    "\tFtp Home: " + ftpWorkingDirectory,
                    "\tFile Path: " + filePath
                );

            string ftpRootDir = GetFtpRootDirectory(t24HomeDir, ftpWorkingDirectory);
            if (string.IsNullOrEmpty(ftpRootDir))
            {
                FtpSimpleLog.LogLine("\tBuilt Ftp Path: " + filePath);
                return filePath;
            }

            ftpRootDir = ftpRootDir.Replace("\\", "/");
            string[] ftps = ftpRootDir.Split(new char[] {'/'}, StringSplitOptions.RemoveEmptyEntries);

            filePath = filePath.Replace("\\", "/");
            string[] files = filePath.Split(new char[] {'/'}, StringSplitOptions.RemoveEmptyEntries);

            // TODO: System.Web.VirtualPathUtility.MakeRelative(ftpRootDir, filePath);

#if DEBUG
            for (int i = 0; i < ftps.Length; i++)
            {
                Debug.Assert(ftps[i] == files[i]);
            }
#endif
            string temp = string.Empty;
            for (int i = ftps.Length; i < files.Length; i++)
            {
                temp += files[i];
                if (i != (files.Length - 1))
                {
                    temp += '/';
                }
            }

            FtpSimpleLog.LogLine("\tBuilt Ftp Path: " + temp);

            return temp;
        }

        public static string BuildX(string baseDirPath, string restOfPath, string logsSubfolder)
        {
            ValidataFtp.FtpSimpleLog.LogsSubfolder = logsSubfolder;
            try
            {
                FtpSimpleLog.LogLines(
                    "BuildX() ftp path:",
                    "\tBASE_PATH " + baseDirPath,
                    "\tREST: " + restOfPath
                    );

                string res;

                if (IsFullyQualifiedWindowsPath(baseDirPath))
                {
                    // Handle Windows paths here

                    // make sure it is treated as a folder
                    if (!baseDirPath.EndsWith("\\"))
                    {
                        baseDirPath = baseDirPath + "\\";
                    }

                    if (string.IsNullOrEmpty(restOfPath))
                    {
                        res = baseDirPath.Substring(2);
                    }
                    else
                    {
                        res = VirtualPathUtility.Combine(baseDirPath.Substring(2), restOfPath);
                    }
                    
                    res = baseDirPath.Substring(0, 2) + res;
                    res = res.Replace("/", "\\");
                    FtpSimpleLog.LogLine("\tBuilt T24 Path: " + res);
                    return res;
                }

                // handle a UNIX path 
                
                // make sure it is treated as a folder
                if (!baseDirPath.EndsWith("/"))
                {
                    baseDirPath = baseDirPath + "/";
                }
                res = VirtualPathUtility.Combine(baseDirPath, restOfPath);
                FtpSimpleLog.LogLine("\tBuilt T24 Path: " + res);
                return res;
            }
            catch
            {
                // TODO log error
                return restOfPath;
            }
        }

        public static string Concat(string relationPath, string fileName)
        {
            // TODO - REVISE
            if (string.IsNullOrEmpty(relationPath))
            {
                return fileName;
            }

            if (string.IsNullOrEmpty(fileName))
            {
                return relationPath;
            }

            relationPath = relationPath.Replace('\\', '/').Trim();
            while (!string.IsNullOrEmpty(relationPath) && relationPath.EndsWith("/"))
            {
                relationPath = relationPath.Substring(0, relationPath.Length - 1);
            }

            fileName = fileName.Replace('\\', '/').Trim();
            while (!string.IsNullOrEmpty(fileName) && fileName.StartsWith("/"))
            {
                fileName = fileName.Substring(1);
            }

            return relationPath + "/" + fileName;
        }

        private static string GetFtpRootDirectory(string t24HomeDir, string ftpWorkingDirectory)
        {
            // TODO - REVISE
            if (string.IsNullOrEmpty(ftpWorkingDirectory))
            {
                return t24HomeDir;
            }

            string ftpHomeDir = t24HomeDir;

            string ftpDir = ftpWorkingDirectory.Replace('/', '\\');
            string[] ftps = ftpDir.Split(new char[] {'\\'}, StringSplitOptions.RemoveEmptyEntries);
            ftpHomeDir = ftpHomeDir.Replace('/', '\\');
            string[] homes = ftpHomeDir.Split(new char[] {'\\'}, StringSplitOptions.RemoveEmptyEntries);

#if DEBUG
            int homesIdx = homes.Length - 1;
            int ftpsIdx = ftps.Length - 1;
            for (; homesIdx >= 0 && ftpsIdx >= 0; homesIdx--, ftpsIdx--)
            {
                Debug.Assert(ftps[ftpsIdx] == homes[homesIdx]);
            }
#endif
            string temp = string.Empty;
            for (int i = 0; i < homes.Length - ftps.Length; i++)
            {
                temp += homes[i] + "\\";
            }
            if (string.IsNullOrEmpty(temp))
            {
                return temp;
            }

            ftpHomeDir = temp.Remove(temp.Length - 1);

            return ftpHomeDir;
        }


        /// <summary>
        /// Escape characters
        /// </summary>
        private static readonly Dictionary<char, string> _EscapeCharacters =
            new Dictionary<char, string> { { '%', "%25" } };

        internal static string EscapePathCharacters(string destDirectoryPath)
        {
            string result = destDirectoryPath;
            foreach (KeyValuePair<char, string> pair in _EscapeCharacters)
            {
                result = destDirectoryPath.Replace(pair.Key.ToString(), pair.Value);
            }

            return result;
        }

        /// <summary>
        /// Builds a fully qualified full path between a directory and a folder/file..
        /// </summary>
        /// <param name="directory">The target directory. For Windows environments this should be a fully qualified path, e.g. "D:\\..."</param>
        /// <param name="targetFile">The target file/folder - for example files/MyFile.txt</param>
        /// <returns>A full path built from the directory and the target folder</returns>
        public static string BuildFullPath(string directory, string targetFile)
        {
            targetFile = targetFile.Replace("..\\", "")
                                   .Replace(".\\", "")
                                   .Replace("../", "");
            return DetermineOSAndConcat(directory, targetFile, true);
        }

        public static string DetermineOSAndConcat(string directory, string targetFile)
        {
            return DetermineOSAndConcat(directory, targetFile, false);
        }

        // inspired by BuildFullPath, but this one doesnt ignore relative path markers
        private static string DetermineOSAndConcat(string directory, string targetFile, bool requireFullPath)
        {
            const char windowsDelimiter = '\\';
            const char unixDelimiter = '/';

            targetFile = targetFile ?? "";
            directory = directory ?? "";

            bool isWindowsPath = isWindowsDir(directory, requireFullPath);
            char pathDelimiter = isWindowsPath ? windowsDelimiter : unixDelimiter;

            string file = isWindowsPath
                                        ? targetFile.Replace(unixDelimiter, windowsDelimiter)
                                               .TrimStart(windowsDelimiter)
                                        : targetFile.Replace(windowsDelimiter, unixDelimiter)
                                               .TrimStart(unixDelimiter);

            string targetDirectory = isWindowsPath
                                         ? directory.Replace(unixDelimiter, windowsDelimiter).
                                               TrimEnd(windowsDelimiter)
                                         : directory.Replace(windowsDelimiter, unixDelimiter).
                                               TrimEnd(unixDelimiter);

            return String.Concat(targetDirectory, pathDelimiter, file);
        }

        private static bool isWindowsDir(string directory, bool requireFullPath)
        {
            if (requireFullPath)
                return IsFullyQualifiedWindowsPath(directory);
            else
                return directory.Contains("\\");
        }
    }
}