﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace ValidataFtp
{
    public class FtpSocketConnector
    {
        const string QUIT_COMMAND = "QUIT";
        #region Classes

        internal enum Response
        {
            Error_Login = -3,
            Error_TimeOut = -2,
            Error_Unknown = -1,

            Success = 200,
            Success_SetPassword = 230,
            Success_SetUser = 331,
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Try to connect to FTP Server
        /// </summary>
        /// <returns></returns>
        public static bool TryConnectToFtpServer(string hostnameWithoutPrefix, int defaultFtpPort, out string errorMessage)
        {
            IPEndPoint ipe;
            Socket socket = Connect(hostnameWithoutPrefix, defaultFtpPort, out ipe, out errorMessage);
            if (socket != null)
            {
                Disconnect(socket);
                return true;
            }

            return false;
        }

        public static bool ChangeAccessRights(FtpConnectionSettings settings, string destPath, string rights, out string communicationLog, out string errMessage, ref FtpSocketConnector fsConnector)
        {
            errMessage = null;
            communicationLog = string.Empty;

            try
            {
                if (fsConnector == null || !fsConnector.IsConnected)
                {
                    fsConnector = CreateAccessRightsFtpSocketConnector(settings, out errMessage);
                    
                    if (fsConnector == null || !String.IsNullOrEmpty(errMessage))
                        return false;
                }

                Response response = fsConnector.SendCommand(string.Format("SITE CHMOD {0} {1}", rights, destPath));
                if (response != Response.Success)
                {
                    errMessage = "Unable to change access rights!";
                }
            }
            catch (Exception ex)
            {
                errMessage = ex.Message + Environment.NewLine + ex.StackTrace;
                return false;
            }
            finally
            {
                if (fsConnector != null)
                {
                    communicationLog = fsConnector._CommLogDelta;
                }
            }

            return errMessage == null;
        }

        private static FtpSocketConnector CreateAccessRightsFtpSocketConnector(FtpConnectionSettings settings, out string errMessage)
        {
            IPEndPoint ipe;
            Socket socket = Connect(settings.Host, settings.Port, out ipe, out errMessage);
            if (socket == null)
            {
                return null;
            }

            FtpSocketConnector fsConnector = new FtpSocketConnector(socket, ipe);
            fsConnector.SetMode(settings.UseBinary);
            //TODO: Not needed to send CHMOD request! settings.UsePassive
            Response response = fsConnector.Login(settings.UserName, settings.Password);

            if (response != Response.Success)
            {
                errMessage = "Unable to authenticate!";
                fsConnector.Disconnect();
            }

            return fsConnector;
        }

        private Response Login(string userName, string password)
        {
            Response res = SendCommand("USER " + userName);
            switch (res)
            {
                case Response.Success_SetUser:
                    res = SendCommand("PASS " + password);
                    if (res != Response.Success_SetPassword)
                    {
                        return Response.Error_Login;
                    }
                    return Response.Success;
                case Response.Success_SetPassword:
                    return Response.Success;
            }

            return Response.Success;
        }

        #endregion

        #region Private Static Methods

        private static IEnumerable<IPAddress> GetHostIPAddresses(string host, out string errorMessage)
        {
            errorMessage = null;

            // check if it is IP
            IPAddress ipa;
            if (IPAddress.TryParse(host, out ipa))
            {
                return new List<IPAddress> { ipa };
            }

            try
            {
                // NOTE: First try Dns.Resolve since Dns.GetHostEntry can be slow
                IPHostEntry hostEntry = Dns.Resolve(host);
                return hostEntry.AddressList;
            }
            catch (Exception)
            {
            }

            try
            {
                IPHostEntry iphost = Dns.GetHostEntry(host);
                return iphost.AddressList;
            }
            catch (Exception ex)
            {
                errorMessage = string.Format("The host '{0}' does not seem to be a valid IP address or domain. Details: {1}", host, ex.Message);
            }

            return null;
        }

        private static Socket Connect(string hostnameWithoutPrefix, int port, out IPEndPoint ipEndPoint, out string errorMessage)
        {
            ipEndPoint = null;
            Socket socket;

            if (hostnameWithoutPrefix.Contains("://"))
            {
                hostnameWithoutPrefix = hostnameWithoutPrefix.Substring(hostnameWithoutPrefix.IndexOf("://") + "://".Length);
            }

            try
            {
                IEnumerable<IPAddress> addresses = GetHostIPAddresses(hostnameWithoutPrefix, out errorMessage);
                if (addresses == null)
                {
                    return null;
                }

                foreach (IPAddress address in addresses)
                {
                    IPEndPoint ipe = new IPEndPoint(address, port);
                    socket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    socket.Connect(ipe);
                    if (socket.Connected)
                    {
                        ipEndPoint = ipe;
                        return socket;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            return null;
        }

        private static void Disconnect(Socket socket)
        {
            try
            {
                if (socket != null)
                {
                    if (socket.Connected)
                    {
                        socket.Close();
                    }
                }
            }
            catch {}
        }

        private static Response Int2Response(int val)
        {
            try
            {
                return (Response) val;
            }
            catch
            {
                return Response.Error_Unknown;
            }
        }

        #endregion

        public bool IsConnected
        {
            get
            {
                if (_Socket != null)
                {
                    return _Socket.Connected;
                }

                return false;
            }
        }

        #region Private Members

        private Socket _Socket;

        private IPEndPoint _IPEndPoint;

        private string _Responses;

        private string _CommLog;

        private string _CommLogDelta;

        #endregion

        #region Class Lifecycle

        private FtpSocketConnector(Socket socket, IPEndPoint ipEndPoint)
        {
            Debug.Assert(socket != null);
            _Socket = socket;

            Debug.Assert(ipEndPoint != null);
            _IPEndPoint = ipEndPoint;

            _CommLog = string.Empty;
        }

        #endregion

        #region Methods

        private bool SetMode(bool isBinnary)
        {
            Response r = SendCommand(string.Format("TYPE {0}", isBinnary ? "I" : "A"));
            return (r == Response.Success);
        }

        public void Disconnect()
        {
            if (_Socket != null)
            {
                if (_Socket.Connected)
                {
                    SendCommand(QUIT_COMMAND);
                }
            }

            Disconnect(_Socket);

            _Socket = null;
            _IPEndPoint = null;
        }

        private Response SendCommand(string command)
        {
            // deflate receive buffer
            while (GetResponseLine(false) != null) {}
            string enryptedCommand = command.StartsWith("PASS ") ? "******" : command;

            FireEvent(string.Format("{0}: SENDING: {1}", DateTime.Now.ToLongTimeString(), enryptedCommand));
            Byte[] buff = Encoding.ASCII.GetBytes((command + "\r\n").ToCharArray());
            _Socket.Send(buff, buff.Length, 0);

            if (command == QUIT_COMMAND) return new Response();
            return GetResponse();
        }

        private Response GetResponse()
        {
            while (true)
            {
                string line = GetResponseLine();
                if (line == null)
                {
                    return Response.Error_TimeOut;
                }

                if (Regex.Match(line, "^[0-9]+ ").Success)
                {
                    return Int2Response(int.Parse(line.Substring(0, 3)));
                }
            }
        }

        private string GetResponseLine()
        {
            return GetResponseLine(true);
        }

        private string GetResponseLine(bool waitFor)
        {
            TimeSpan tsTimeOut = new TimeSpan(0, 0, 30); // 30 sec
            DateTime dtStart = DateTime.Now;
            do
            {
                string n = ReceiveNext();
                if (!string.IsNullOrEmpty(n))
                {
                    dtStart = DateTime.Now;
                }

                _Responses += n;

                int idx = _Responses.IndexOf('\n');
                if (idx >= 0)
                {
                    string result = _Responses.Substring(0, idx);
                    _Responses = _Responses.Substring(idx + 1);

                    FireEvent(string.Format("{0}: RECEIVED: {1}", DateTime.Now.ToLongTimeString(), result));
                    return result;
                }

                if (!waitFor)
                {
                    return null;
                }

                Thread.Sleep(50);
            }
            while ((DateTime.Now - dtStart) < tsTimeOut);

            return null;
        }

        private void FireEvent(string msg)
        {
            _CommLog += "\t" + msg + "\r\n";
            _CommLogDelta = msg;

            Debug.WriteLine(msg);
        }

        private string ReceiveNext()
        {
            string result = string.Empty;

            while (_Socket.Available > 0)
            {
                Byte[] buff = new Byte[512];
                long received = _Socket.Receive(buff, 512, 0);
                result += Encoding.ASCII.GetString(buff, 0, (int) received);
                Thread.Sleep(50);
            }

            return result;
        }

        #endregion
    }
}