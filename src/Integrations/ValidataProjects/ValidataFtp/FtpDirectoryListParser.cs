﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using OFSCommonMethods.IO;

namespace ValidataFtp
{

    #region Additional classes

    ///<summary>
    /// Represents a single line from a LIST command response. It is either a file or a directory
    ///</summary>
    public class FtpListItem : ValidataCommon.FileNameListItem
    {
        public string Name { get; set; }
        public bool IsDirectory { get; set; }
        public string FullPath { get; set; }
    }

    #endregion

    public class FtpDirectoryListingParams
    {
        public Regex ListLineRegex;
        public List<Regex> SkippedLinesRegex;
        public string DirectoryGroupText;
    }

    ///<summary>
    /// Parses the FTP "LIST" command to its actual items - directories and files
    ///</summary>
    public static class FtpDirectoryListParser
    {
        #region Public Methods

        ///<summary>
        /// Parse a LIST response using a response reader.
        ///</summary>
        ///<param name="ftpResponseText"></param>
        ///<param name="ftpParams"></param>
        ///<returns></returns>
        public static List<FtpListItem> Parse(string ftpResponseText, FtpDirectoryListingParams ftpParams)
        {

            List<FtpListItem> parsedLines = new List<FtpListItem>();

            using (TextReader responseReader = new StringReader(ftpResponseText))
            {
                string line;
                while ((line = responseReader.ReadLine()) != null)
                {
                    FtpListItem itemResult = ParseLine(line, ftpParams);
                    if (itemResult != null)
                    {
                        parsedLines.Add(itemResult);
                    }
                }
            }

            return parsedLines;
        }

        /// <summary>
        /// Parse a single response line 
        /// </summary>
        /// <param name="line"></param>
        /// <param name="ftpParams"></param>
        /// <returns></returns>
        public static FtpListItem ParseLine(string line, FtpDirectoryListingParams ftpParams)
        {
            Match match = ftpParams.ListLineRegex.Match(line);

            if (match.Success)
            {
                return ParseMatch(match.Groups, ftpParams.DirectoryGroupText);
            }

            // Try the skipped lines
            foreach (Regex skippedLine in ftpParams.SkippedLinesRegex)
            {
                match = skippedLine.Match(line);

                if (match.Success)
                {
                    FtpSimpleLog.LogLine("Skipped line by configuration: '" + line + "'");
                    return null;
                }
            }

            FtpSimpleLog.LogLine("Failed parsing line: '" + line + "'");
            return null;
        }

        #endregion

        #region Private Properties

        public static Regex GetListLineRegex(string strListRegex)
        {
            if (!String.IsNullOrEmpty(strListRegex))
            {
                return new Regex(strListRegex, RegexOptions.IgnoreCase);
            }
            else
            {
                return _ListLineDefault;
            }
        }

        public static List<Regex> GetSkippedLinesRegexes(IEnumerable<string> skippedLinesRegex)
        {
            if (skippedLinesRegex == null)
            {
                return _SkippedLinesDefault;
            }

            List<Regex> skippedLinesRegexes = new List<Regex>();

            foreach (String regexString in skippedLinesRegex)
            {
                Regex regex = new Regex(regexString);
                skippedLinesRegexes.Add(regex);
            }

            return skippedLinesRegexes;
        }

        #endregion

        #region Private Methods

        private static FtpListItem ParseMatch(GroupCollection matchGroups, string dirMatch)
        {
            FtpListItem result = new FtpListItem();
            result.IsDirectory = matchGroups["dir"].Value.Equals(dirMatch, StringComparison.InvariantCultureIgnoreCase);
            result.Name = matchGroups["name"].Value;
            // TODO: Parse the item Date

            //if (!result.IsDirectory)
            //    result.Size = long.Parse(matchGroups["size"].Value);

            return result;
        }

        #endregion

        #region PrivateFields

        private static readonly List<Regex> _SkippedLinesDefault = new List<Regex> {new Regex(@"^(total)+\s+\d+$")};

        private static readonly Regex _ListLineDefault =
            new Regex(
                @"^(?<dir>[-dl])(?<ownerSec>[-r][-w][-x])(?<groupSec>[-r][-w][-x])(?<everyoneSec>[-r][-w][-x])\s+(?:\d+)\s+(?<owner>\w+)\s+(?<group>\w+)\s+(?<size>\d+)\s+(?<month>\w+)\s+(?<day>\d{1,2})\s+(((?<hour>\d{1,2}):(?<minutes>\d{1,2}))|(?<year>\d{2,4}))+\s+(?<name>.*)$",
                RegexOptions.IgnoreCase);

        //private static readonly Regex _WindowsStyleDefault = new Regex(@"^(?<month>\d{1,2})-(?<day>\d{1,2})-(?<year>\d{1,2})\s+(?<hour>\d{1,2}):(?<minutes>\d{1,2})(?<ampm>am|pm)\s+(?<dir>[<]dir[>])?\s+(?<size>\d+)?\s+(?<name>.*)$", RegexOptions.IgnoreCase);
        //private static readonly Regex _FileZillaStyle = new Regex(@"(?<dir>[-dl])(?<ownerSec>)[-r][-w][-x](?<groupSec>)[-r][-w][-x](?<everyoneSec>)[-r][-w][-x]\s+(?:\d)\s+(?<owner>\w+)\s+(?<group>\w+)\s+(?<size>\d+)\s+(?<month>\w+)\s+(?<day>\d{1,2})\s+(?<year>\w+)\s+(?<name>.*)$", RegexOptions.IgnoreCase);


        #endregion
    }
}