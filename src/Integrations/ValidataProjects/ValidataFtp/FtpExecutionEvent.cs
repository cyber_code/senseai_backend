﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidataFtp
{
    public class FTPExecutionEventArgs : EventArgs
    {
        #region Enums

        public enum Action
        {
            Abort = 0,
            Retry
        }

        #endregion

        public readonly string Message;

        public readonly string Section;

        public Action ErrorTreatAction;

        #region Class Lifecycle

        internal FTPExecutionEventArgs(string section, string message)
        {
            Section = section;
            Message = message;
            ErrorTreatAction = Action.Abort;
        }

        #endregion
    }

    public delegate void CommandExecutionError(object sender, FTPExecutionEventArgs args);
}
