using System;
using System.Web;
using System.Xml;
using ValidataCommon;

namespace AXMLEngine
{
    /// <summary>
    /// GatewayConfiguration writes and reads the gateway configuration section.
    /// </summary>
    [Serializable]
    public class GatewayConfiguration
    {
        #region Private members

        /// <summary>
        /// The input file
        /// </summary>
        private string _InputFile;

        /// <summary>
        /// The output file
        /// </summary>
        private string _OutputFile;

        /// <summary>
        /// The name of the errors file
        /// </summary>
        private string _ErrorsFile;

        /// <summary>
        /// The name of the adapter
        /// </summary>
        private string _AdapterName;

        /// <summary>
        /// The name of the mapping schema
        /// </summary>
        private string _MappingSchema;

        /// <summary>
        /// The name of the catalog
        /// </summary>
        private string _CatalogName;

        #endregion

        #region Public methods

        /// <summary>
        /// Writes the gateway configuration to a XmlWriter.
        /// </summary>
        /// <param name="xmlWriter">An XmlWriter instance to write to.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.GATEWAY_CONFIGURATION_TAG);

            //gateway configuration consists of input file, output file, error file, adapter, 
            //mapping schema and catalog name
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_INPUTFILE_TAG, _InputFile);
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_OUTPUTFILE_TAG, _OutputFile);
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_ERRORSFILE_TAG, _ErrorsFile);
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_ADAPTER_NAME_TAG,
                                         Utils.GetHtmlEncodedValue(_AdapterName));
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_MAPPING_SCHEMA_TAG,
                                         Utils.GetHtmlEncodedValue(_MappingSchema));
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_CATALOG_NAME_TAG,
                                         Utils.GetHtmlEncodedValue(_CatalogName));


            // Close gatewayConfiguration definition
            xmlWriter.WriteEndElement();

            xmlWriter.Flush();
        }

        /// <summary>
        /// Writes the gateway configuration to a XmlWriter.
        /// </summary>
        /// <param name="xmlWriter">An XmlWriter instance to write to.</param>
        public void ToXMLFile(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.GATEWAY_CONFIGURATION_TAG);

            //gateway configuration consists of input file, output file, error file, adapter, 
            //mapping schema and catalog name
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_INPUTFILE_TAG, _InputFile);
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_OUTPUTFILE_TAG, _OutputFile);
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_ERRORSFILE_TAG, _ErrorsFile);
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_ADAPTER_NAME_TAG,
                                         Utils.GetHtmlEncodedValue(_AdapterName));
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_MAPPING_SCHEMA_TAG,
                                         Utils.GetHtmlEncodedValue(_MappingSchema));
            xmlWriter.WriteElementString(AXMLConstants.CONFIGURATION_CATALOG_NAME_TAG,
                                         Utils.GetHtmlEncodedValue(_CatalogName));


            // Close gatewayConfiguration definition
            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Constructs the GatewayConfiguration section from XML.
        /// </summary>
        /// <param name="configReader"></param>
        public void FromXML(XmlReader configReader)
        {
            while (configReader.Read())
            {
                if (configReader.NodeType == XmlNodeType.Element)
                {
                    switch (configReader.Name)
                    {
                        case AXMLConstants.GATEWAY_CONFIGURATION_TAG:
                            break;
                        case AXMLConstants.CONFIGURATION_INPUTFILE_TAG:
                            _InputFile = configReader.ReadString();
                            break;
                        case AXMLConstants.CONFIGURATION_OUTPUTFILE_TAG:
                            _OutputFile = configReader.ReadString();
                            break;
                        case AXMLConstants.CONFIGURATION_ERRORSFILE_TAG:
                            _ErrorsFile = configReader.ReadString();
                            break;
                        case AXMLConstants.CONFIGURATION_ADAPTER_NAME_TAG:
                            _AdapterName = configReader.ReadString();
                            break;
                        case AXMLConstants.CONFIGURATION_MAPPING_SCHEMA_TAG:
                            _MappingSchema = configReader.ReadString();
                            break;
                        case AXMLConstants.CONFIGURATION_CATALOG_NAME_TAG:
                            _CatalogName = configReader.ReadString();
                            break;
                    }
                }
                else
                {
                    switch (configReader.NodeType)
                    {
                        case XmlNodeType.EndElement:
                            if (configReader.Name == AXMLConstants.GATEWAY_CONFIGURATION_TAG)
                            {
                                return;
                            }
                            break;
                    }
                }
            }
        }

        #endregion

        #region Public properties

        /// <summary>
        /// The input file
        /// </summary>
        public string InputFile
        {
            get { return _InputFile; }
            set { _InputFile = value; }
        }

        /// <summary>
        /// The output file
        /// </summary>
        public string OutputFile
        {
            get { return _OutputFile; }
            set { _OutputFile = value; }
        }

        /// <summary>
        /// The name of the errors file
        /// </summary>
        public string ErrorsFile
        {
            get { return _ErrorsFile; }
            set { _ErrorsFile = value; }
        }

        /// <summary>
        /// The name of the adapter
        /// </summary>
        public string AdapterName
        {
            get { return _AdapterName; }
            set { _AdapterName = value; }
        }

        /// <summary>
        /// The name of the mapping schema
        /// </summary>
        public string MappingSchema
        {
            get { return _MappingSchema; }
            set { _MappingSchema = value; }
        }

        /// <summary>
        /// The name of the catalog
        /// </summary>
        public string CatalogName
        {
            get { return _CatalogName; }
            set { _CatalogName = value; }
        }

        #endregion
    }
}