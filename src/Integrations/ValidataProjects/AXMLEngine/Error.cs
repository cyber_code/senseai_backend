using System;
using System.Xml;

namespace AXMLEngine
{
    /// <summary>
    /// An Error object describes an AXML error.
    /// </summary>
    [Serializable]
    public class Error
    {
        #region Enums

        public enum ErrorTypes
        {
            Warning = 0,
            Normal,
            Critical
        }

        #endregion

        #region Private members

        /// <summary>
        /// The Error message
        /// </summary>
        private readonly string _Message;

        /// <summary>
        /// Type of the error message
        /// </summary>
        private readonly ErrorTypes _Type;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Constructs an Error instance
        /// </summary>
        /// <param name="message">The error message.</param>
        public Error(string message)
        {
            _Message = message;
            _Type = ErrorTypes.Normal;
        }

        /// <summary>
        /// Constructs an Error instance
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="type">Type of error</param>
        public Error(string message, ErrorTypes type)
        {
            _Message = message;
            _Type = type;
        }

        #endregion

        #region Public properties

        /// <summary>
        /// The Error message
        /// </summary>
        public string Message
        {
            get { return _Message; }
        }

        /// <summary>
        /// Type of error message
        /// </summary>
        public ErrorTypes Type
        {
            get { return _Type; }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Writes the Error object to XML
        /// </summary>
        /// <param name="xmlWriter">The XmlWriter to write to.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteElementString(AXMLConstants.ERROR_TAG, _Message);
        }

        /// <summary>
        /// Creates an Error object instance from XML
        /// </summary>
        /// <param name="errorReader">The string containing the XML</param>
        /// <returns>The constructed error object or null.</returns>
        public static Error FromXML(XmlReader errorReader)
        {
            while (errorReader.Read())
            {
                if (errorReader.NodeType == XmlNodeType.Element && errorReader.Name == AXMLConstants.ERROR_TAG)
                {
                    string strType = errorReader.GetAttribute(AXMLConstants.ERRORSET_ATTRIBUTE_TYPE);
                    string message = errorReader.ReadString();
                    return new Error(message, GetAttrType(strType));
                }
            }

            return null;
        }

        internal Instance GetInstance()
        {
            Instance result = new Instance("AppError", "NavigatorCatalog");
            result.Catalog = "NavigatorCatalog";
            result.AddAttribute("Error Message", this.Message);

            return result;
        }

        #endregion

        #region Private Static Methods

        private static ErrorTypes GetAttrType(string strType)
        {
            if (string.Compare(strType, ErrorTypes.Warning.ToString(), true) == 0)
            {
                return ErrorTypes.Warning;
            }

            if (string.Compare(strType, ErrorTypes.Critical.ToString(), true) == 0)
            {
                return ErrorTypes.Critical;
            }

            return ErrorTypes.Normal;
        }

        #endregion
    }
}