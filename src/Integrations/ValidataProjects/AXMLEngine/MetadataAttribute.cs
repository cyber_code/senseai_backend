//===============================================================================
// MetadataAttribute.cs
//===============================================================================
//
// Metadata attribute.
//
// History:
//	Version:                Date:                   Author:
//	1.00                    15/12/2004              Ilian
//	Description: Created initial version
//
//===============================================================================
// Copyright (C) 2004-2006 Theseus Technology Partners
// All rights reserved.
//===============================================================================

using System;
using Validata.Common;

namespace AXMLEngine
{
    /// <summary>
    /// MetadataAttribute.
    /// </summary>
    [Serializable]
    public class MetadataAttribute
    {
        #region Private And Protected Members

        /// <summary>
        /// Attribute ID
        /// </summary>
        private string _ID;

        /// <summary>
        /// Attribute Name
        /// </summary>
        private string _Name;

        /// <summary>
        /// Attribute Type
        /// </summary>
        private AttributeDataType _Type = AttributeDataType.Unknown;

        /// <summary>
        /// Attribute Description
        /// </summary>
        private string _Description;

        /// <summary>
        /// Attribute Remarks
        /// </summary>
        private string _Remarks;

        #endregion

        #region Public Properties

        /// <summary>
        /// Attribute ID
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        /// <summary>
        /// Attribute Name
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// Attribute Type
        /// </summary>
        public AttributeDataType Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        /// <summary>
        /// Attribute Description
        /// </summary>
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        /// <summary>
        /// Attribute remarks
        /// </summary>
        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }

        #endregion

    }
}