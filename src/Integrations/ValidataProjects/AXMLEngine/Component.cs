using System;
using System.Xml;

namespace AXMLEngine
{
    /// <summary>
    /// A component/machine participating in a system infrastructure
    /// </summary>
    [Serializable]
    public class Component
    {
        /// <summary>
        /// The default port used for communication with the performance client
        /// </summary>
        public const int DefaultPerformanceClientPort = 9877;

        #region Private members

        private PerformanceIndicatorsCollection _Indicators = new PerformanceIndicatorsCollection();
        private string _Name;
        private string _URL;
        private int _Port = DefaultPerformanceClientPort;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Constructs an instance of the Component class.
        /// </summary>
        public Component()
        {
        }

        /// <summary>
        /// Constructs an instance of the Component class.
        /// </summary>
        /// <param name="name">The name of the performance adapter</param>
        /// <param name="url">The URL of the performance adapter.</param>
        public Component(string name, string url, int port)
        {
            _Name = name;
            _URL = url;
            _Port = port;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Serializes the infrasture component to XML
        /// </summary>
        /// <param name="xmlWriter">The writer where to serialize the object.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.PERFORMANCE_ADAPTER_TAG);

            xmlWriter.WriteAttributeString(AXMLConstants.PERFORMANCE_ADAPTER_NAME_ATTRIBUTE, _Name);
            xmlWriter.WriteAttributeString(AXMLConstants.PERFORMANCE_ADAPTER_URL_ATTRIBUTE, _URL);
            xmlWriter.WriteAttributeString(AXMLConstants.PERFORMANCE_ADAPTER_PORT_ATTRIBUTE, _Port.ToString());

            foreach (PerformanceIndicator indicator in _Indicators)
            {
                indicator.ToXML(xmlWriter);
            }

            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Reads the metadata section of the Performance Adapter 
        /// </summary>
        /// <param name="adapterReader">The contents of the metadata section</param>
        /// <returns>False if an exception occurs</returns>
        public bool FromXML(XmlReader adapterReader)
        {
            // Read input data and locate root typical
            while (adapterReader.Read())
            {
                if (adapterReader.NodeType == XmlNodeType.Element)
                {
                    if (adapterReader.Name == AXMLConstants.PERFORMANCE_INDICATORS_SECTION_TAG)
                    {
                        //this is the root tag - skip it
                        continue;
                    }
                    else if (adapterReader.Name == AXMLConstants.PERFORMANCE_ADAPTER_TAG)
                    {
                        _Name = adapterReader.GetAttribute(AXMLConstants.PERFORMANCE_ADAPTER_NAME_ATTRIBUTE);
                        _URL = adapterReader.GetAttribute(AXMLConstants.PERFORMANCE_ADAPTER_URL_ATTRIBUTE);
                        string portNumber = adapterReader.GetAttribute(AXMLConstants.PERFORMANCE_ADAPTER_PORT_ATTRIBUTE);
                        if (!String.IsNullOrEmpty(portNumber))
                        {
                            int port = 0;
                            if (int.TryParse(portNumber, out port) && port > 0)
                                _Port = port;
                        }
                    }
                    else if (adapterReader.Name == AXMLConstants.INDICATOR_TAG)
                    {
                        using (XmlReader reader = adapterReader.ReadSubtree())
                        {
                            PerformanceIndicator indicator = PerformanceIndicator.FromXML(reader);
                            _Indicators.Add(indicator);
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Adds a performance indicator to the list of indicators
        /// </summary>
        /// <param name="indicator"></param>
        public void AddIndicator(PerformanceIndicator indicator)
        {
            _Indicators.Add(indicator);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the name of the infrastructure component
        /// </summary>
        public string Name
        {
            get { return _Name; }
        }

        /// <summary>
        /// Gets the IP of the infrastructure component
        /// </summary>
        public string URL
        {
            get { return _URL; }
        }

        /// <summary>
        /// Gets the Port of the infrastructure component, used for communicaton with the performance adapter
        /// </summary>
        public int Port
        {
            get { return _Port; }
            set { _Port = value; }
        }

        /// <summary>
        /// The indicators to measure for this infrastructure component
        /// </summary>
        public PerformanceIndicatorsCollection Indicators
        {
            get { return _Indicators; }
        }

        #endregion
    }
}