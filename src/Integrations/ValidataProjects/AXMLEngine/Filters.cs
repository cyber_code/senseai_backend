//===============================================================================
// Filters.cs
//===============================================================================
//
// Filters.
//
// History:
//	Version:                Date:                   Author:
//	1.00                    15/12/2004              Ilian
//	Description: Created initial version
//
//===============================================================================
// Copyright (C) 2004-2006 Theseus Technology Partners
// All rights reserved.
//===============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Validata.Common;
using ValidataCommon;
using System.Linq;

namespace AXMLEngine
{
  

    /// <summary>
    /// Filters.
    /// </summary>
    [Serializable]
    public class Filters
    {
        #region Private And Protected Members

        /// <summary>
        /// Filters collection
        /// </summary>
        
        /*Do not set private. It is used for serialization*/
        public List<Filter> FilterList = new List<Filter>();

        #endregion

        #region Public Properties


        /// <summary>
        /// Filters collection
        /// </summary>
        [XmlIgnore]
        public List<Filter> FiltersCollection
        {
            get { return FilterList.Where(f => !f.IsPostFilter).ToList(); }
            //set { FilterList = value; }
        }

        /// <summary>
        /// Filters collection
        /// </summary>
        [XmlIgnore]
        public List<Filter> PostFiltersCollection
        {
            get { return FilterList.Where(f => f.IsPostFilter).ToList(); }
        }

        /// <summary>
        /// Current count of filters
        /// </summary>
        public int Count
        {
            get { return FiltersCollection.Count; }
        }

        /// <summary>
        /// Current count of post filters
        /// </summary>
        public int PostFiltersCount
        {
            get { return PostFiltersCollection.Count; }
        }

        /// <summary>
        /// Indexer implementation.
        /// </summary>
        public Filter this[int index]
        {
            get { return FilterList[index]; }
            set { FilterList[index] = value; }
        }

        #endregion

        #region Class Lifecycle
        public Filters()
        {
        }

        public Filters(List<FilterElement> filters)
        {
            foreach (var filterElement in filters)
            {
                AddFilter(filterElement);
            }
        }

        public Filters(IEnumerable<ValidataCommon.Interfaces.IFilteringConstraint> filters)
        {
            if (filters != null)
                filters.ToList().ForEach(n => AddFilter(n));
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Add new filter to the collection
        /// </summary>
        /// <param name="filters">Filtering constraint</param>
        public void AddFilter(ValidataCommon.Interfaces.IFilteringConstraint filteringConstraint)
        {
            AddFilter(filteringConstraint.AttributeName, filteringConstraint.TestValue, filteringConstraint.DataType,filteringConstraint.Condition,filteringConstraint.Operator, filteringConstraint.AttributeLimit);
        }

        /// <summary>
        /// Adds filter to filters collection
        /// </summary>
        /// <param name="attrName">Filter attribute name</param>
        /// <param name="attrFilter">Filter attribute value</param>
        /// <param name="attrDataType">Filter attribute data type</param>
        /// <param name="ft">Filter type</param>
        /// <returns>If succeeded returns true, otherwise - false</returns>
        public bool AddFilter(string attrName, string attrFilter, AttributeDataType attrDataType  ,FilterType ft)
        {
            if (attrName != null && attrFilter != null)
            {
                Filter f = new Filter(attrName, attrFilter, attrDataType, ft);
                FilterList.Add(f);

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddFilter(string attrName, string attrFilter, AttributeDataType attrDataType, FilterCondition condition, FilterType ft, string Limit)
        {
            if (attrName != null && attrFilter != null)
            {
                Filter f = new Filter(attrName, attrFilter, attrDataType, condition, ft, Limit);
                FilterList.Add(f);

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Adds filter to filters collection
        /// </summary>
        /// <param name="filter">Adapter Filter</param>
        /// <returns>If succeeded returns true, otherwise - false</returns>
        public bool AddFilter(Filter filter)
        {
            if (filter != null)
            {
                FilterList.Add(filter);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Add number of filters
        /// </summary>
        /// <param name="filters"></param>
        public void AddFilterRange(IEnumerable<Filter> filters)
        {
            FilterList.AddRange(filters);
        }

        /// <summary>
        /// Serializes the filters section to the XmlWriter
        /// </summary>
        /// <param name="xmlWriter">XmlWriter where to serialize the Filters section</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.FILTERS_TAG);

            foreach (Filter ft in FilterList)
            {
                ft.ToXML(xmlWriter);
            }

            xmlWriter.WriteEndElement();

            xmlWriter.Flush();
        }

        /// <summary>
        /// Reads the Filter section from XML
        /// </summary>
        /// <param name="filterReader">The XML string defining the Filters section.</param>
        public void FromXML(XmlReader filterReader)
        {
            // Init reader
            bool nodeIsRead = false;
            // Read input data and locate root typical
            while (nodeIsRead || filterReader.Read())
            {
                //ReadOuterXML operations cause the next node to be loaded so we need to skip reading 
                if (nodeIsRead)
                {
                    nodeIsRead = false;
                }
                if (filterReader.NodeType == XmlNodeType.Element)
                {
                    switch (filterReader.Name)
                    {
                        case AXMLConstants.FILTER_TAG:
                            string logOp = filterReader.GetAttribute(AXMLConstants.FILTER_LOGICAL_OPERATION_ATTRIBUTE);
                            using (XmlReader reader = filterReader.ReadSubtree())
                            {
                                Filter filter = Filter.FromXML(reader, logOp);
                                AddFilter(filter);
                            }
                            nodeIsRead = true;
                            break;
                    }
                }
            }
        }


        /// <summary>
        /// Gets the filter manager.
        /// </summary>
        /// <returns></returns>
        public FilterManager GetFilterManager()
        {
            return new FilterManager(this.FiltersCollection.Select(n => n as ValidataCommon.Interfaces.IFilteringConstraint));
        }

        #endregion

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach(Filter filter in FiltersCollection)
            {
                sb.AppendLine(filter.ToString());
            }
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Filters)) return false;
            return Equals((Filters)obj);
        }

        public bool Equals(Filters other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            foreach (Filter filter in FiltersCollection)
            {
                if (!other.FiltersCollection.Contains(filter))
                    return false;
            }

            foreach (Filter filter in other.FiltersCollection)
            {
                if (!FiltersCollection.Contains(filter))
                    return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            return (FilterList != null ? FilterList.GetHashCode() : 0);
        }

        
    }
}