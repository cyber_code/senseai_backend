using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Linq;
using ValidataCommon;

namespace AXMLEngine
{
    /// <summary>
    /// MetadataTypical.
    /// </summary>
    [Serializable]
    public class MetadataTypical : ValidataCommon.IMetadataEx
    {
        #region Private And Protected Members

        /// <summary>
        /// Typical ID
        /// </summary>
        private string _ID;

        /// <summary>
        /// Typical Base Class
        /// </summary>
        private string _BaseClass;

        /// <summary>
        /// Typical Catalog Name
        /// </summary>
        private string _CatalogName;

        /// <summary>
        /// Typical Name
        /// </summary>
        private string _Name;

        /// <summary>
        /// Typical Attributes
        /// </summary>
        private List<TypicalAttribute> _Attributes = new List<TypicalAttribute>();

        /// <summary>
        /// A dictionary allowing fast access to the typical attributes by name
        /// </summary>
        private Dictionary<string, TypicalAttribute> _AttributesByName;

        /// <summary>
        /// Version Name
        /// </summary>
        private string _VersionName;

        /// <summary>
        /// Ordered version attributes
        /// </summary>
        private List<string> _VersionAttributes = new List<string>();

        /// <summary>
        /// Synch Object
        /// </summary>
        private readonly object _syncObj = new object();

     
        #endregion

        #region Public Properties

        /// <summary>
        /// Typical ID
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        /// <summary>
        /// Typical Base Class ID
        /// </summary>
        public string BaseClass
        {
            get { return _BaseClass; }
            set { _BaseClass = value; }
        }

        /// <summary>
        /// Typical Catalog Name
        /// </summary>
        public string CatalogName
        {
            get { return _CatalogName; }
            set { _CatalogName = value; }
        }

        /// <summary>
        /// Typical Name
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// Typical Name
        /// </summary>
        public string VersionName
        {
            get { return _VersionName; }
            set { _VersionName = value; }
        }
        
      

        /// <summary>
        /// Typical Attributes 
        /// </summary>
        public List<TypicalAttribute> Attributes
        {
            get { return _Attributes; }
            set { _Attributes = value; }
        }

        [System.Xml.Serialization.XmlIgnore()]
        /// <summary>
        /// Gets the attributes list.
        /// </summary>
        /// <value>The attributes list.</value>
        public List<ValidataCommon.IMetadataAttribute> AttributesList
        {
            get { return Attributes.ToMetadataAttributesList(); }
        }

        [System.Xml.Serialization.XmlIgnore()]
        /// <summary>
        /// Gets the attributes for version.
        /// </summary>
        /// <value>The attributes for version.</value>
        public List<TypicalAttribute> AttributesForVersion
        {
            get { return GetAttributesForVersion(); }
        }

        public bool HasVersion
        {
            get
            {
                return (
                           !string.IsNullOrEmpty(_VersionName) 
                           && _VersionName != "No Value" 
                           &&  _VersionAttributes != null
                       );
            }
        }

        private List<TypicalAttribute> GetAttributesForVersion()
        {
            if (string.IsNullOrEmpty(_VersionName) || _VersionName == "No Value")
            {
                Debug.Fail("No version: returning complete list...");
                return Attributes;
            }

            if (_VersionAttributes == null || _VersionAttributes.Count == 0 /*TODO: the second check migh not be neccessary*/)
            {
                Debug.Fail( "No version attributes: returning complete list...");
                return Attributes;
            }

            List<TypicalAttribute> result = new List<TypicalAttribute>();
            Dictionary<string, TypicalAttribute> attributes = AttributesByName;
            foreach (var versionAttr in _VersionAttributes)
            {
                string attrName = versionAttr.Trim();
                if (!string.IsNullOrEmpty(attrName))
                {
                    if (!attributes.ContainsKey(attrName))
                    {
                        Debug.Fail(string.Format("There is an attribute '{0}' in the version that is missing in the typical", attrName));
                        continue;
                    }

                    result.Add(attributes[attrName]);
                }
            }

            return result;
        }

        /// <summary>
        /// Ordered version attributes
        /// </summary>
        public List<string> VersionAttributes
        {
            get { return _VersionAttributes; }
            set { _VersionAttributes = value; }
        }

        [System.Xml.Serialization.XmlIgnore()]
        /// <summary>
        /// A dictionary allowing fast search for the attributes by name. By default, this 
        /// dictionary is empty. It is populated on the first request to it.
        /// </summary>
        public Dictionary<string, TypicalAttribute> AttributesByName
        {
            get
            {
                if (_AttributesByName == null)
                {
                    lock (_syncObj)
                    {
                        _AttributesByName = new Dictionary<string, TypicalAttribute>();
                        foreach (TypicalAttribute typAttr in _Attributes)
                        {
                            if (!_AttributesByName.ContainsKey(typAttr.Name))
                            {
                                _AttributesByName.Add(typAttr.Name, typAttr);
                            }
                        }
                    }
                }
                return _AttributesByName;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [System.Xml.Serialization.XmlIgnore()]
        public IEnumerable<IMetadataAttribute> NormalizedAttributesNamesList
        {
            get
            {
                var normalizedAttributes = new Dictionary<string, IMetadataAttribute>();
                foreach (var attribute in AttributesList)
                {
                    string shortName = AttributeNameParser.GetShortFieldName(attribute.Name);
                    if(!normalizedAttributes.ContainsKey(shortName))
                    {
                        normalizedAttributes[shortName] = new SimpleMetadataAttribute
                                                          {
                                                              Name = shortName
                                                          };
                    }
                }

                return normalizedAttributes.Values;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds typical attribute to attributes collection
        /// </summary>
        /// <param name="attr">Typical attribute</param>
        /// <returns>If succeeded returnst true, otherwise - false</returns>
        public bool AddAttribute(TypicalAttribute attr)
        {
            if (attr != null)
            {
                _Attributes.Add(attr);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void AddVersionAttribute(string versionAttribute)
        {
            if (string.IsNullOrEmpty(versionAttribute))
            {
                Debug.Fail("Try to add a null or empty version");
                return;
            }

            if (_VersionAttributes.Contains(versionAttribute))
            {
                Debug.Fail(string.Format("There is a duplicate attribute '{0}' in the version", versionAttribute));
                return;
            }

            _VersionAttributes.Add(versionAttribute);
        }

        /// <summary>
        /// Constructs a MetadataTypical object from XML
        /// </summary>
        /// <param name="typicalReader"></param>
        /// <param name="catalogName"></param>
        /// <returns></returns>
        public static MetadataTypical FromXML(XmlReader typicalReader, string catalogName)
        {
            MetadataTypical currentTypical = new MetadataTypical();
            currentTypical.CatalogName = catalogName;

            while (typicalReader.Read())
            {
                if (typicalReader.NodeType == XmlNodeType.Element)
                {
                    if (typicalReader.Name == AXMLConstants.TYPICAL_TAG)
                    {
                        ReadTypicalAttributes(currentTypical, typicalReader);
                    }
                    else if (typicalReader.Name == AXMLConstants.ATTRIBUTE_TAG)
                    {
                        currentTypical.AddAttribute(TypicalAttribute.FromXML(typicalReader));
                    }
                    else if (typicalReader.Name == AXMLConstants.VERSION_TAG)
                    {
                        ReadVersionAttributes(currentTypical, typicalReader);
                    }
                    else if (typicalReader.Name == AXMLConstants.VERSIONATTRIBUTE_TAG)
                    {
                        currentTypical.AddVersionAttribute(
                            XmlConvert.DecodeName(typicalReader.GetAttribute(AXMLConstants.ATTRIBUTE_ATTRIBUTE_NAME))
                            );
                    }
                }
                else
                {
                    switch (typicalReader.NodeType)
                    {
                        case XmlNodeType.EndElement:
                            if (typicalReader.Name == AXMLConstants.METADATA_TAG)
                            {
                                //should not happen - reached the closing tag - end reading
                                return currentTypical;
                            }
                            break;
                    }
                }
            }

            return currentTypical;
        }

        /// <summary>
        /// Reads the typical attributes.
        /// </summary>
        /// <param name="currentTypical">The current typical.</param>
        /// <param name="typicalReader">The typical reader.</param>
        private static void ReadTypicalAttributes(MetadataTypical currentTypical, XmlReader typicalReader)
        {
            currentTypical.ID = XmlConvert.DecodeName(typicalReader.GetAttribute(AXMLConstants.TYPICAL_ATTRIBUTE_ID));
            currentTypical.Name = XmlConvert.DecodeName(typicalReader.GetAttribute(AXMLConstants.TYPICAL_ATTRIBUTE_NAME));
            currentTypical.BaseClass = XmlConvert.DecodeName(typicalReader.GetAttribute(AXMLConstants.TYPICAL_ATTRIBUTE_BASECLASS));
        }

        private static void ReadVersionAttributes(MetadataTypical currentTypical, XmlReader typicalReader)
        {
            currentTypical.VersionName = XmlConvert.DecodeName(typicalReader.GetAttribute(AXMLConstants.TYPICAL_ATTRIBUTE_NAME));
        }

        /// <summary>
        /// Writes data to XML.
        /// </summary>
        /// <param name="xmlWriter">The XML writer.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.TYPICAL_TAG);
            xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_ATTRIBUTE_ID, ID);
            xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_NAME_ATTRIBUTE, Name);
            xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_BASECLASS_ATTRIBUTE, BaseClass);

            foreach (TypicalAttribute ta in Attributes)
            {
                ta.ToXML(xmlWriter);
            }

            if (!string.IsNullOrEmpty(_VersionName))
            {
                xmlWriter.WriteStartElement(AXMLConstants.VERSION_TAG);
                xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_NAME_ATTRIBUTE, _VersionName);

                foreach (string versionName in _VersionAttributes)
                {
                    xmlWriter.WriteStartElement(AXMLConstants.VERSIONATTRIBUTE_TAG);
                    xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_NAME_ATTRIBUTE, versionName);
                    xmlWriter.WriteEndElement();
                }

                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();

            xmlWriter.Flush();
        }

        public override string ToString()
        {
            return "[" + (CatalogName ?? "?")  + "]" + Name + " - " + _Attributes.Count;
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default constructor
        /// </summary>
        public MetadataTypical()
        {
        }
      
        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="src"></param>
        public MetadataTypical(ValidataCommon.IMetadataEx src)
        {
            this.BaseClass = src.BaseClass;
            this.CatalogName = src.CatalogName;
            this.Name = src.Name;
            this.
            _Attributes.AddRange(src.AttributesList.Select(n => new TypicalAttribute(n,n.DisciplineNoderef,n.UserNoderef)));
            //_VersionAttributes.AddRange(src.);

            _AttributesByName = _Attributes.ToDictionary(n => n.Name, n => n);
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="src"></param>
        public MetadataTypical(ValidataCommon.IMetadata src)
        {
            this.BaseClass = src.BaseClass;
            this.CatalogName = src.CatalogName;
            this.Name = src.Name;

            _Attributes.AddRange(src.AttributesList.Select(n => new TypicalAttribute(n)));
            //_VersionAttributes.AddRange(src.);

            _AttributesByName = _Attributes.ToDictionary(n => n.Name, n => n);
        }

        #endregion

        public MetadataTypical Clone()
        {
            return Clone(true);
        }

        public MetadataTypical Clone(bool cloneAttributeCollections)
        {
            MetadataTypical result = new MetadataTypical()
            {
                ID = this.ID,
                BaseClass = this.BaseClass,
                CatalogName = this.CatalogName,
                Name = this.Name,
                VersionName = this.VersionName
            };

            if (cloneAttributeCollections)
            {
                result._Attributes.AddRange(this.Attributes);
                result._VersionAttributes.AddRange(this._VersionAttributes);

                result._AttributesByName = new Dictionary<string, TypicalAttribute>();
                foreach (string key in this._AttributesByName.Keys)
                {
                    result._AttributesByName[key] = _AttributesByName[key];
                }
            }
            else
            {
                result._Attributes = this.Attributes;
                result._VersionAttributes = this._VersionAttributes;
                result._AttributesByName = this._AttributesByName;
            }

            return result;
        }
    }
}