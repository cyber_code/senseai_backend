//===============================================================================
// AdapterParameter.cs
//===============================================================================
//
// Adapter parameter.
//
// History:
//	Version:                Date:                   Author:
//	1.00                    15/12/2004              Ilian
//	Description: Created initial version
//
//===============================================================================
// Copyright (C) 2004-2006 Theseus Technology Partners
// All rights reserved.
//===============================================================================

using System;

namespace AXMLEngine
{
    /// <summary>
    /// AdapterParameter.
    /// </summary>
    [Serializable]
    public class AdapterParameter
    {
        #region Private And Protected Members

        /// <summary>
        /// Parameter Name
        /// </summary>
        private string _Name;

        /// <summary>
        /// Parameter Value
        /// </summary>
        private string _Value;

        #endregion

        #region Public properties

        /// <summary>
        /// Parameter name
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// Parameter value
        /// </summary>
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default constructor
        /// </summary>
        public AdapterParameter()
        {
            _Name = null;
            _Value = null;
        }

        /// <summary>
        /// Additional constructor
        /// </summary>
        /// <param name="name">Parameter name</param>
        /// <param name="val">Parameter value </param>
        public AdapterParameter(string name, string val)
        {
            _Name = name;
            _Value = val;
        }

        #endregion

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return string.Format("[{0}]={1}", _Name, _Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(AdapterParameter)) return false;
            return Equals((AdapterParameter)obj);
        }

        public bool Equals(AdapterParameter other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other._Name, _Name) && Equals(other._Value, _Value);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((_Name != null ? _Name.GetHashCode() : 0)*397) ^ (_Value != null ? _Value.GetHashCode() : 0);
            }
        }
    }
}