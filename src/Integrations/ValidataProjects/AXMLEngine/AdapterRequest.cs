using System;
using System.Diagnostics;
using System.Xml;

namespace AXMLEngine
{
    #region Public Enumerations

    public enum RequestType
    {
        /// <summary>
        /// Import of Instances to Validata from an external source
        /// </summary>
        Import = 1,
        /// <summary>
        /// Export of Instances from Validata to an external destination 
        /// </summary>
        Export = 3,
        /// <summary>
        /// Response to a request 
        /// </summary>
        RequestResponse = 5
    }

    /// <summary>
    /// Determines the loading options of the AXML engine. Allows skipping one or more sections of the file.
    /// </summary>
    [Flags]
    public enum RequestLoadFlags 
    {
        /// <summary>
        /// Loads all sections in memory
        /// </summary>
        LoadNormal = 0,

        /// <summary>
        /// Skip the section with Instances and Errors
        /// </summary>
        SkipInstancesAndErrors = 1,

        /// <summary>
        /// Do not add errors to the ErrorSet of the Dataset- just notify when each one is read 
        /// </summary>
        OnlyNotifyForReadErrors = 2,

        /// <summary>
        /// Do not add insances to the XmlInstances of the Dataset - just notify when each one is read 
        /// </summary>
        OnlyNotifyForReadInstances = 4,
    }

    public enum RequestAction
    {
        None = 0,

        Release = 1
    }

    #endregion

    /// <summary>
    /// Adapter request.
    /// </summary>
    [Serializable]
    public class AdapterRequest
    {
        #region Private And Protected Members

        /// <summary>
        /// Request ID
        /// </summary>
        private string _ID;

        /// <summary>
        /// Request type
        /// </summary>
        private RequestType _Type = RequestType.Import;

        /// <summary>
        /// Request action
        /// </summary>
        private RequestAction _RequestAction = RequestAction.None;

        /// <summary>
        /// Request typical
        /// </summary>
        private string _RequestTypical;

        /// <summary>
        /// Request typical
        /// </summary>
        private string _RequestTypicalID;

        /// <summary>
        /// Response typical
        /// </summary>
        private string _ResponseTypical;

        /// <summary>
        /// Response typical
        /// </summary>
        private string _ResponseTypicalID;

        /// <summary>
        /// Request assembly
        /// </summary>
        private string _Assembly;

        /// <summary>
        /// Target System Date
        /// </summary>
        private string _TargetSystemDate;

        /// <summary>
        /// Response assembly
        /// </summary>
        private string _ResponseAssembly;

        /// <summary>
        /// Adapter Request Parameters
        /// </summary>
        private AdapterParameters _Parameters = new AdapterParameters();

        /// <summary>
        /// Adapter request filters
        /// </summary>
        private Filters _Filters = new Filters();

        /// <summary>
        /// Enquiry mask
        /// </summary>
        private LevelStructureHierarchy _EnquiryMask;

        /// <summary>
        /// Adapter request metadata
        /// </summary>
        private Metadata _Metadata = new Metadata();

        /// <summary>
        /// Adapter request instances
        /// </summary>
        private Datasets _Datasets = new Datasets();

        /// <summary>
        /// The gateway configuration section
        /// </summary>
        private readonly GatewayConfiguration _GatewayConfig = new GatewayConfiguration();

        #endregion

        #region Public Properties

        /// <summary>
        /// Request ID
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        /// <summary>
        /// Request type
        /// </summary>
        public RequestType Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        /// <summary>
        /// Request action
        /// </summary>
        public RequestAction Action
        {
            get { return _RequestAction; }
            set { _RequestAction = value; }
        }

        /// <summary>
        /// Request typical
        /// </summary>
        public string RequestTypical
        {
            get { return _RequestTypical; }
            set { _RequestTypical = value; }
        }

        /// <summary>
        /// Request typical ID
        /// </summary>
        public string RequestTypicalID
        {
            get { return _RequestTypicalID; }
            set { _RequestTypicalID = value; }
        }

        /// <summary>
        /// Response typical
        /// </summary>
        public string ResponseTypical
        {
            get { return _ResponseTypical; }
            set { _ResponseTypical = value; }
        }

        /// <summary>
        /// Response typical ID
        /// </summary>
        public string ResponseTypicalID
        {
            get { return _ResponseTypicalID; }
            set { _ResponseTypicalID = value; }
        }

        /// <summary>
        /// Request assembly
        /// </summary>
        public string Assembly
        {
            get { return _Assembly; }
            set { _Assembly = value; }
        }

        /// <summary>
        /// Target System Date
        /// </summary>
        public string TargetSystemDate
        {
            get { return _TargetSystemDate; }
            set { _TargetSystemDate = value; }
        }

        /// <summary>
        /// Response Assembly
        /// </summary>
        public string ResponseAssembly
        {
            get { return _ResponseAssembly; }
            set { _ResponseAssembly = value; }
        }

        /// <summary>
        /// Adapter Request Parameters
        /// </summary>
        public AdapterParameters Parameters
        {
            get { return _Parameters; }
            set { _Parameters = value; }
        }

        /// <summary>
        /// Adapter request filters
        /// </summary>
        public Filters Filters
        {
            get { return _Filters; }
            set { _Filters = value; }
        }

        public LevelStructureHierarchy EnquiryMask
        {
            get { return _EnquiryMask; }
            set { _EnquiryMask = value; }
        }

        /// <summary>
        /// Adapter request metadata
        /// </summary>
        public Metadata RequestMetadata
        {
            get { return _Metadata; }
            set { _Metadata = value; }
        }

        /// <summary>
        /// Adapter request instances
        /// </summary>
        public Datasets Datasets
        {
            get { return _Datasets; }
            set { _Datasets = value; }
        }

        /// <summary>
        /// The gateway configuration section
        /// </summary>
        public GatewayConfiguration GatewayConfiguration
        {
            get { return _GatewayConfig; }
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default constructor
        /// </summary>
        public AdapterRequest()
        {
        }

        /// <summary>
        /// Additional constructor
        /// </summary>
        /// <param name="id">Request ID</param>
        /// <param name="type">Request Type</param>
        public AdapterRequest(string id, RequestType type)
        {
            _ID = id;
            _Type = type;
        }

        /// <summary>
        /// Additional constructor
        /// </summary>
        /// <param name="id">Request ID</param>
        /// <param name="action">Request Action</param>
        public AdapterRequest(string id, RequestAction action)
        {
            _ID = id;
             Debug.Assert(action == RequestAction.Release,"Use this overload only for releasing connections!");

            _RequestAction = action;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates Adapter Request From external XML file
        /// </summary>
        /// <param name="xmlReader"> Source XML reader</param>
        /// <param name="loadFlags">Loading flags for the AdapterRequest</param>
        /// <returns>If succeeded returns true, otherwise - false</returns>
        public bool FromXML(XmlReader xmlReader, RequestLoadFlags loadFlags)
        {
            bool foundRootNode = false;

            // Read input data and locate root typical
            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == XmlNodeType.Element)
                {
                    if (xmlReader.IsEmptyElement)
                    {
                        continue;
                    }

                    if ((xmlReader.Name == AXMLConstants.ADAPTER_REQUEST_TAG) ||
                        (xmlReader.Name == AXMLConstants.ADAPTER_RESPONSE_TAG))
                    {
                        foundRootNode = true;
                        _ID = xmlReader.GetAttribute(AXMLConstants.ADAPTER_REQUEST_ID);
                        _Type = StringToRequestType(xmlReader.GetAttribute(AXMLConstants.ADAPTER_REQUEST_TYPE));
                        _RequestAction = StringToRequestAction(xmlReader.GetAttribute(AXMLConstants.ADAPTER_REQUEST_ACTION));
                    }
                    if (foundRootNode)
                    {
                        switch (xmlReader.Name)
                        {
                            case AXMLConstants.REQUEST_TYPICAL_TAG:
                                {
                                    //get the request typical ID
                                    _RequestTypicalID = xmlReader.GetAttribute(AXMLConstants.TYPICAL_ID_ATTRIBUTE);
                                    RequestTypical = xmlReader.ReadString().Trim('\r', '\n', ' ');
                                    break;
                                }
                            case AXMLConstants.RESPONSE_TYPICAL_TAG:
                                {
                                    //get the request typical ID
                                    _ResponseTypicalID = xmlReader.GetAttribute(AXMLConstants.TYPICAL_ID_ATTRIBUTE);
                                    ResponseTypical = xmlReader.ReadString().Trim('\r', '\n', ' ');
                                    break;
                                }
                            case AXMLConstants.REQUEST_TARGET_DATE:
                                {
                                    _TargetSystemDate = xmlReader.ReadString().Trim('\r', '\n', ' ');
                                    break;
                                }
                            case AXMLConstants.REQUEST_ASSEMBLY_TAG:
                                {
                                    _Assembly = xmlReader.ReadString().Trim('\r', '\n', ' ');
                                    break;
                                }
                            case AXMLConstants.RESPONSE_ASSEMBLY_TAG:
                                {
                                    ResponseAssembly = xmlReader.ReadString().Trim('\r', '\n', ' ');
                                    break;
                                }
                            case AXMLConstants.ADAPTER_PARAMETERS_TAG:
                                {
                                    _Parameters = new AdapterParameters();
                                    using (XmlReader parameterReader = xmlReader.ReadSubtree())
                                    {
                                        _Parameters.FromXML(parameterReader);
                                    }
                                    break;
                                }
                            case AXMLConstants.FILTERS_TAG:
                                {
                                    using (XmlReader filterReader = xmlReader.ReadSubtree())
                                    {
                                        Filters.FromXML(filterReader);
                                    }
                                    break;
                                }
                            case AXMLConstants.ENQUIRY_MASK_TAG:
                                {
                                    using (XmlReader filterReader = xmlReader.ReadSubtree())
                                    {
                                        EnquiryMask = LevelStructureHierarchy.FromXML(filterReader);
                                    }
                                    break;
                                }
                            case AXMLConstants.METADATA_TAG:
                                {
                                    _Metadata = new Metadata();
                                    using (XmlReader metadataReader = xmlReader.ReadSubtree())
                                    {
                                        _Metadata.FromXML(metadataReader);
                                    }
                                    break;
                                }
                            case AXMLConstants.INSTANCES_TAG:
                                {
                                    if ((loadFlags & RequestLoadFlags.SkipInstancesAndErrors) == 0)
                                    {
                                        using (XmlReader datasetsReader = xmlReader.ReadSubtree())
                                        {
                                            _Datasets.FromXML(datasetsReader, loadFlags);
                                        }
                                    }
                                    else
                                    {
                                        xmlReader.Skip();
                                    }

                                    break;
                                }
                            case AXMLConstants.GATEWAY_CONFIGURATION_TAG:
                                {
                                    using (XmlReader configReader = xmlReader.ReadSubtree())
                                    {
                                        _GatewayConfig.FromXML(configReader);
                                    }
                                    break;
                                }
                        }
                    }
                }
            }


            return foundRootNode;
        }

        /// <summary>
        /// Serializes the AdapterRequesto to XML
        /// </summary>
        /// <param name="xmlWriter">The xmlWriter to write output to.</param>
        /// <summary>
        /// Serializes the AdapterRequesto to XML
        /// </summary>
        /// <param name="xmlWriter">The xmlWriter to write output to.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            if (Type != RequestType.RequestResponse)
            {
                xmlWriter.WriteStartElement(AXMLConstants.ADAPTER_REQUEST_TAG);
            }
            else
            {
                xmlWriter.WriteStartElement(AXMLConstants.ADAPTER_RESPONSE_TAG);
            }

            xmlWriter.WriteAttributeString(AXMLConstants.ADAPTER_REQUEST_ID, _ID);
            xmlWriter.WriteAttributeString(AXMLConstants.ADAPTER_REQUEST_TYPE, _Type.ToString());

            if (_RequestAction != RequestAction.None)
            {
                xmlWriter.WriteAttributeString(AXMLConstants.ADAPTER_REQUEST_ACTION, _RequestAction.ToString());
            }

            //output request typical
            xmlWriter.WriteStartElement(AXMLConstants.REQUEST_TYPICAL_TAG);
            xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_ID_ATTRIBUTE, RequestTypicalID);
            xmlWriter.WriteString(RequestTypical);
            xmlWriter.WriteEndElement();

            //output response typical
            xmlWriter.WriteStartElement(AXMLConstants.RESPONSE_TYPICAL_TAG);
            xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_ID_ATTRIBUTE, ResponseTypicalID);
            xmlWriter.WriteString(ResponseTypical);
            xmlWriter.WriteEndElement();

            //output gateway configuration
            GatewayConfiguration.ToXML(xmlWriter);

            //output adapter parameters
            Parameters.ToXML(xmlWriter);

            //output filters
            Filters.ToXML(xmlWriter);

            if (EnquiryMask != null)
            {
                EnquiryMask.ToXML(xmlWriter);
            }

            //output metadata
            RequestMetadata.ToXML(xmlWriter);

            //output instances
            Datasets.ToXML(xmlWriter);

            //End of adapter request or response tag
            xmlWriter.WriteEndElement();
            xmlWriter.Flush();
        }

        /// <summary>
        /// Gets the request metadata typical.
        /// </summary>
        /// <returns></returns>
        public MetadataTypical GetRequestMetadataTypical()
        {
            return RequestMetadata.Typicals.FindByID(RequestTypicalID);
        }

        /// <summary>
        /// Gets the response metadata typical.
        /// </summary>
        /// <returns></returns>
        public MetadataTypical GetResponseMetadataTypical()
        {
            return RequestMetadata.Typicals.FindByID(ResponseTypicalID);
        }


        #endregion

        #region Private And Protected Methods

        /// <summary>
        /// Converts string to Request type
        /// </summary>
        /// <param name="requestType">String representation of request type</param>
        /// <returns>Returns converted Request Type</returns>
        private static RequestType StringToRequestType(string requestType)
        {
            RequestType result = 0;

            requestType = requestType.ToUpper();

            switch (requestType)
            {
                case "IMPORT":
                    result = RequestType.Import;
                    break;
                case "EXPORT":
                    result = RequestType.Export;
                    break;
                case "REQUESTRESPONSE":
                    result = RequestType.RequestResponse;
                    break;
                default:
                    Debug.Fail("Unexpected request type: " + requestType);
                    break;
            }

            return result;
        }

        /// <summary>
        /// Converts string to Request action
        /// </summary>
        /// <param name="requestAction">String representation of request action</param>
        /// <returns>Returns converted Request Action</returns>
        private static RequestAction StringToRequestAction(string requestAction)
        {
            if (requestAction == null)
            {
                return RequestAction.None;
            }

            requestAction = requestAction.ToUpper();
            if(requestAction ==  RequestAction.Release.ToString().ToUpper())
            {
                return RequestAction.Release;
            }

            return RequestAction.None;
        }

        #endregion

        /// <summary>
        /// Gets whether current request is for releasing the adapter
        /// </summary>
        public bool IsReleaseRequest { get { return Action == RequestAction.Release; } }
    }
}