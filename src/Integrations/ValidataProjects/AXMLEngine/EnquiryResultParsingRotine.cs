﻿using System.Collections.Generic;

namespace AXMLEngine
{
    /// <summary>
    /// The class can contain 
    ///     either LevelStructureHierarchy instance
    ///     either a string with template name
    /// None of them or only one should be defined per instance
    /// </summary>
    public class EnquiryResultParsingRotine
    {
        public readonly LevelStructureHierarchy LevelStructure;

        public readonly string TemplateName = string.Empty;

        public List<Filter> PostFilters
        {
            get;
            set;
        }

        /// <summary>
        /// Defautl constructor
        /// </summary>
        private EnquiryResultParsingRotine()
        {
        }

        /// <summary>
        /// Constructs with already initialized level structure
        /// </summary>
        /// <param name="levelStructure"></param>
        public EnquiryResultParsingRotine(LevelStructureHierarchy levelStructure)
        {
            LevelStructure = levelStructure;
        }

        /// <summary>
        /// Constructs with template name
        /// </summary>
        /// <param name="templateName"></param>
        public EnquiryResultParsingRotine(string templateName)
        {
            if (templateName == null)
            {
                templateName = string.Empty;
            }

            TemplateName = templateName;
        }

        /// <summary>
        /// Constructs EnquiryResultParsingRotine instance from string which can contain a template name 
        /// or LevelStructureHierarchy xml serialized object
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static EnquiryResultParsingRotine FromString(string value)
        {
            // try to deserialize the string if execption is thrown it is 
            //  assumed that the string contains template name, if the 
            //  deserialization is successfull the result will contain LevelStructure

            if (string.IsNullOrEmpty(value))
            {
                return EnquiryResultParsingRotine.None;
            }

            // try to deserialize as short string 
            LevelStructureHierarchy lsh = LevelStructureHierarchy.FromShortString(value);
            if (lsh != null)
            {
                return new EnquiryResultParsingRotine(lsh);
            }

            // hack - prevent going to try/catch section if first character is not '<'
            if (!value.Trim().StartsWith("<"))
            {
                return new EnquiryResultParsingRotine(value);
            }

            try
            {
                // try to deserialize from full xml string
                var result = LevelStructureHierarchy.FromXmlString(value);
                return new EnquiryResultParsingRotine(result);
            }
            catch (System.Exception)
            {
                // return as name
                return new EnquiryResultParsingRotine(value);
            }
        }

        public static EnquiryResultParsingRotine None
        {
            get { return new EnquiryResultParsingRotine(); }
        }
    }
}
