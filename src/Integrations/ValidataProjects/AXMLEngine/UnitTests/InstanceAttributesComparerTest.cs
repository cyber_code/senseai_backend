using System;
using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;
using ValidataCommon;

namespace AXMLEngine.UnitTests
{
    [TestFixture]
    public class InstanceAttributesComparerTest
    {

        [TestFixtureSetUp]
        public void TestSave()
        {
            DifferenceErrorConfiguration save;

            // configuration
            save = new DifferenceErrorConfiguration();
            save.Version = 1;
            save.DifferenceErrorIgnoreRuleList = new List<DifferenceErrorIgnoreRule>();
            save.DifferenceErrorConversionRuleList = new List<DifferenceErrorConversionRule>();

            // Difference Ignore
            DifferenceErrorIgnoreRule ignoreRule = null;

            // 1st rule
            ignoreRule = new DifferenceErrorIgnoreRule();
            ignoreRule.Name = "Ignore NEW in ARRANGEMENT";
            ignoreRule.Enabled = true;
            ignoreRule.Source = new IgnoreRuleElement("*", "*", "ARRANGEMENT", "NEW");
            ignoreRule.Target = new IgnoreRuleElement("*", "*", "ARRANGEMENT", "*");
            save.DifferenceErrorIgnoreRuleList.Add(ignoreRule);

            // 2nd rule
            ignoreRule = new DifferenceErrorIgnoreRule();
            ignoreRule.Name = "Ignore NULL in AA.*";
            ignoreRule.Enabled = true;
            ignoreRule.Source = new IgnoreRuleElement("*", "AA.*", "*", "NULL");
            ignoreRule.Target = new IgnoreRuleElement("*", "AA.*", "*", "*");
            save.DifferenceErrorIgnoreRuleList.Add(ignoreRule);

            // Difference Conversion
            DifferenceErrorConversionRule conversionRule = new DifferenceErrorConversionRule();

            // Decimal
            conversionRule.Name = "Convert Decimals";
            conversionRule.Enabled = true;
            conversionRule.Source = new RuleElement("*", "*", "*");
            conversionRule.Target = new RuleElement("*", "*", "*");
            conversionRule.ConversionType = "DECIMAL";
            save.DifferenceErrorConversionRuleList.Add(conversionRule);

            // store
            DifferenceErrorConfiguration.SerializeToXml(save, Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");
        }

        [Test]
        public void TestIdentical()
        {
            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");
            Instance instance = new Instance();
            instance.AddAttribute("test1", "test");
            instance.AddAttribute("test2", "test");

            InstanceAttributesComparisonResult result = InstanceAttributesComparer.Compare(instance, instance);
            Assert.IsTrue(result.IsCompletetelyIdentical);
            Assert.AreEqual(2, result.Identical.Count);
            Assert.AreEqual(0, result.DifferentValues.Count);
            Assert.AreEqual(0, result.OnlyInFirst.Count);
            Assert.AreEqual(0, result.OnlyInSecond.Count);
            Assert.IsEmpty(result.GetAllDissimilaritiesText());
            InstanceAttributesComparer.CompareFunction = null;
        }

        [Test]
        public void TestDifferentValues()
        {
            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");
            Instance first = new Instance();
            first.AddAttribute("test1", "something");

            Instance second = new Instance();
            second.AddAttribute("test1", "another thing");

            InstanceAttributesComparisonResult result = InstanceAttributesComparer.Compare(first, second);

            Assert.AreEqual(0, result.Identical.Count); 
            Assert.AreEqual(1, result.DifferentValues.Count);
            Assert.AreEqual(0, result.OnlyInFirst.Count);
            Assert.AreEqual(0, result.OnlyInSecond.Count);
          //  Assert.AreEqual("1 different attribute values : ", result.GetAllDissimilaritiesText());
            InstanceAttributesComparer.CompareFunction = null;
        }

        [Test]
        public void TestOnlyInFirstValues()
        {
            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");
            Instance first = new Instance();
            first.AddAttribute("test1", "something");

            Instance second = new Instance();

            InstanceAttributesComparisonResult result = InstanceAttributesComparer.Compare(first, second);

            Assert.AreEqual(0, result.Identical.Count);
            Assert.AreEqual(0, result.DifferentValues.Count);
            Assert.AreEqual(1, result.OnlyInFirst.Count);
            Assert.AreEqual(0, result.OnlyInSecond.Count);
            InstanceAttributesComparer.CompareFunction = null;
        }

        [Test]
        public void TestOnlyInSecondValues()
        {
            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");
            Instance first = new Instance();

            Instance second = new Instance();
            second.AddAttribute("test1", "something");

            InstanceAttributesComparisonResult result = InstanceAttributesComparer.Compare(first, second);

            Assert.AreEqual(0, result.Identical.Count);
            Assert.AreEqual(0, result.DifferentValues.Count);
            Assert.AreEqual(0, result.OnlyInFirst.Count);
            Assert.AreEqual(1, result.OnlyInSecond.Count);
            InstanceAttributesComparer.CompareFunction = null;
        }

        [Test]
        public void TestComplex()
        {
            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") + @"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");
            Instance first = new Instance();
            first.AddAttribute("test5", "something");
            first.AddAttribute("test6", "something");
            first.AddAttribute("test0", "something");
            first.AddAttribute("test1", "something");
            first.AddAttribute("test2", "something");

            Instance second = new Instance();
            second.AddAttribute("test4", "something");
            second.AddAttribute("test3", "something");
            second.AddAttribute("test1", "another thing");
            second.AddAttribute("test2", "another thing");
            second.AddAttribute("test0", "something");

            InstanceAttributesComparisonResult result = InstanceAttributesComparer.Compare(first, second);

            Assert.AreEqual(1, result.Identical.Count);
            Assert.AreEqual(2, result.DifferentValues.Count);
            Assert.AreEqual(2, result.OnlyInFirst.Count);
            Assert.AreEqual(2, result.OnlyInSecond.Count);

            Debug.Write(result.GetAllDissimilaritiesText());
            InstanceAttributesComparer.CompareFunction = null;
        }


    }
}