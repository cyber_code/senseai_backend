﻿using NUnit.Framework;

namespace AXMLEngine.UnitTests
{
    [TestFixture]
    public class EnquiryLevelStructure_Test
    {
        [Test]
        public void TestXmlSerialization()
        {
            // deserialize
            var result = LevelStructureHierarchy.FromXmlString(GetDefaultLevelStructureHierarchyXMLString());

            ValidateDefaultLevelStructureHierarchy(result);

            // serialize
            var xmlString = LevelStructureHierarchy.ToXmlString(result);

            // deserialize again
            var result1 = LevelStructureHierarchy.FromXmlString(xmlString);

            Compare(result, result1);
        }

        [Test]
        public void TestStringSerialization()
        {
            // deserialize
            var defaultLsh = LevelStructureHierarchy.FromXmlString(GetDefaultLevelStructureHierarchyXMLString());

            // serialize
            var strLsh = LevelStructureHierarchy.ToShortString(defaultLsh);

            // deserialize again
            var strTransformedLsh = LevelStructureHierarchy.FromShortString(strLsh);

            Compare(defaultLsh, strTransformedLsh);
        }

        [Test]
        public void TestStringSerializationAdvancedMode()
        {
            // deserialize
            var defaultLsh = LevelStructureHierarchy.FromXmlString(GetDefaultLevelStructureHierarchyXMLStringAdvancedMode());

            // serialize
            var strLsh = LevelStructureHierarchy.ToShortString(defaultLsh);

            // deserialize again
            var strTransformedLsh = LevelStructureHierarchy.FromShortString(strLsh);

            Compare(defaultLsh, strTransformedLsh);
            Assert.IsTrue(strTransformedLsh.AdvancedMode,"The de-serialized string was not in advanced mode.");
        }
        [Test]
        public void TestAXMLEngine()
        {
            // test that if no enquiry mask is defined (e.g. null) it wont be serialized
            AXMLEngine engine = new AXMLEngine();
            engine.AdaptersRequests.Add(new AdapterRequest("1", RequestType.RequestResponse));

            string xmlWithoutEnquiryMask = engine.ToXML();

            engine = new AXMLEngine();
            engine.FromXML(xmlWithoutEnquiryMask);
            Assert.IsNull(engine.DefaultRequest.EnquiryMask);

            // test serialization of the enquiry mask
            var enquiryMask = LevelStructureHierarchy.FromXmlString(GetDefaultLevelStructureHierarchyXMLString());
            engine.DefaultRequest.EnquiryMask = enquiryMask;
            // add some metadata in order to see that it is read correctly
            engine.DefaultRequest.RequestMetadata.Typicals.Add(new MetadataTypical());
            {
                engine.DefaultRequest.RequestMetadata.Typicals[0].Name = "1234";
                engine.DefaultRequest.RequestMetadata.Typicals[0].CatalogName = "XXX";
                engine.DefaultRequest.RequestMetadata.Typicals[0].AddAttribute(new TypicalAttribute { Name = "@ID" });
            }
            string xmlWithEnquiryMask = engine.ToXML();

            engine = new AXMLEngine();
            engine.FromXML(xmlWithEnquiryMask);
            Assert.IsNotNull(engine.DefaultRequest.EnquiryMask);
            Compare(engine.DefaultRequest.EnquiryMask, enquiryMask);

            Assert.AreEqual(engine.DefaultRequest.RequestMetadata.Typicals.Count, 1);
            Assert.AreEqual(engine.DefaultRequest.RequestMetadata.Typicals[0].Attributes.Count, 1);
            Assert.AreEqual(engine.DefaultRequest.RequestMetadata.Typicals[0].Attributes[0].Name, "@ID");
            
        }

        private void Compare(LevelStructureHierarchy a, LevelStructureHierarchy b)
        {
            Assert.AreEqual(a.Occurrence, b.Occurrence);
            Assert.AreEqual(a.RowTemplates.Count, b.RowTemplates.Count);
            for (int i = 0; i < a.RowTemplates.Count; i++)
            {
                Assert.AreEqual(a.RowTemplates[i].Name, b.RowTemplates[i].Name);
                Assert.AreEqual(a.RowTemplates[i].Fields.Count, b.RowTemplates[i].Fields.Count);

                for (int j = 0; j < a.RowTemplates[i].Fields.Count; j++)
                {
                    Assert.AreEqual(a.RowTemplates[i].Fields[j].Value, b.RowTemplates[i].Fields[j].Value);
                    Assert.AreEqual(a.RowTemplates[i].Fields[j].Operand, b.RowTemplates[i].Fields[j].Operand);
                    Assert.AreEqual(a.RowTemplates[i].Fields[j].Type, b.RowTemplates[i].Fields[j].Type);
                }
            }

            if (a.SubLevels == null)
            {
                Assert.IsNull(b.SubLevels);
                return;
            }

            Assert.AreEqual(a.SubLevels.Count, b.SubLevels.Count);
            for (int i = 0; i < a.SubLevels.Count; i++)
            {
                Compare(a.SubLevels[i], b.SubLevels[i]);
            }
        }

        private string GetDefaultLevelStructureHierarchyXMLString()
        {
            return
            "<Enquiry name=\"YOUR.ENQ.NAME\">\r\n" +
            "		<Adapters>\r\n" +
            "			<Adapter name=\"ADAPTER-1-NAME\"/>\r\n" +
            "			<Adapter name=\"ADAPTER-2-NAME\"/>\r\n" +
            "		</Adapters>\r\n" +
            "		<Level1_1RowTemplate>\r\n" +
            "			<Field type=\"integer\" value=\"Drun Drun\" operand=\"LIKE\"/>\r\n" +
            "		</Level1_1RowTemplate>\r\n" +
            "		<Level2_1RowTemplate>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "		</Level2_1RowTemplate>\r\n" +
            "		<Level3_1RowTemplate>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "		</Level3_1RowTemplate>\r\n" +
            "		<Response>\r\n" +
            "			<Level>\r\n" +
            "				<RowTemplate ref=\"Level1_1RowTemplate\"/>\r\n" +
            "				<Level>\r\n" +
            "					<RowTemplate ref=\"Level2_1RowTemplate\"/>\r\n" +
            "					<Level>\r\n" +
            "						<RowTemplate ref=\"Level3_1RowTemplate\"/>\r\n" +
            "					</Level>\r\n" +
            "				</Level>\r\n" +
            "			</Level>\r\n" +
            "		</Response>\r\n" +
            "	</Enquiry>\r\n";
        }

        private string GetDefaultLevelStructureHierarchyXMLStringAdvancedMode ()
        {
            return
            "<Enquiry name=\"YOUR.ENQ.NAME\" mode=\"1\">\r\n" +
            "		<Adapters>\r\n" +
            "			<Adapter name=\"ADAPTER-1-NAME\"/>\r\n" +
            "			<Adapter name=\"ADAPTER-2-NAME\"/>\r\n" +
            "		</Adapters>\r\n" +
            "		<Level1_1RowTemplate>\r\n" +
            "			<Field type=\"integer\" value=\"Drun Drun\" operand=\"LIKE\"/>\r\n" +
            "		</Level1_1RowTemplate>\r\n" +
            "		<Level2_1RowTemplate>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "		</Level2_1RowTemplate>\r\n" +
            "		<Level3_1RowTemplate>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "			<Field type=\"string\"/>\r\n" +
            "		</Level3_1RowTemplate>\r\n" +
            "		<Response>\r\n" +
            "			<Level>\r\n" +
            "				<RowTemplate ref=\"Level1_1RowTemplate\"/>\r\n" +
            "				<Level>\r\n" +
            "					<RowTemplate ref=\"Level2_1RowTemplate\"/>\r\n" +
            "					<Level>\r\n" +
            "						<RowTemplate ref=\"Level3_1RowTemplate\"/>\r\n" +
            "					</Level>\r\n" +
            "				</Level>\r\n" +
            "			</Level>\r\n" +
            "		</Response>\r\n" +
            "	</Enquiry>\r\n";
        }
        private void ValidateDefaultLevelStructureHierarchy(LevelStructureHierarchy lsh)
        {
            // first level
            Assert.IsTrue(lsh.RowTemplates.Count == 1);
            Assert.IsTrue(lsh.RowTemplates[0].Fields.Count == 1);
            Assert.IsTrue(lsh.RowTemplates[0].Fields[0].Operand == ValueOperand.LIKE);
            Assert.IsTrue(lsh.RowTemplates[0].Fields[0].Value == "Drun Drun");
            Assert.IsTrue(lsh.RowTemplates[0].Fields[0].Type == ValueType.Integer);

            // second level
            Assert.IsTrue(lsh.SubLevels.Count == 1);
            Assert.IsTrue(lsh.SubLevels[0].RowTemplates.Count == 1);
            Assert.IsTrue(lsh.SubLevels[0].RowTemplates[0].Fields.Count == 1);

            // third level
            Assert.IsTrue(lsh.SubLevels[0].SubLevels.Count == 1);
            Assert.IsTrue(lsh.SubLevels[0].SubLevels[0].RowTemplates.Count == 1);
            Assert.IsTrue(lsh.SubLevels[0].SubLevels[0].RowTemplates[0].Fields.Count == 6);
        }
    }
}

