using System;
using System.Data;
using System.Diagnostics;
using NUnit.Framework;

namespace AXMLEngine.UnitTests
{
    [TestFixture]
    [Explicit]
    public class TestVxmlToTableLoader
    {
        static readonly string[] AttributeNames = new string[]
                                          {
                                              "A1",
                                              "A2",
                                          };


        const int itemsCount = 2;
        const string catalogName = "Workflow";
        const string typicalName = "TEST_ReportLine";

        [Test]
        public void TestFile()
        {
            string filePath = GenerateSampleFile();
            
            DataTable tbl = new DataTable();
            tbl.Columns.Add("A1");
            tbl.Columns.Add("A3"); // this is intentionally different

            VxmlToTableLoader loader = new VxmlToTableLoader();
            loader.ReadVxmlFile(tbl, filePath);

            Assert.AreEqual(itemsCount, tbl.Rows.Count);
            Assert.IsTrue(tbl.Rows[0]["A1"].ToString().StartsWith("_A1"));
        }

        private static string GenerateSampleFile()
        {
            string filePath = string.Format(@"{0}temp\{1}_{2}_{3}.xml", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") ,catalogName, typicalName, itemsCount);

            InstanceVxmlGenerator.Generate(filePath,
                                           itemsCount,
                                           catalogName,
                                           typicalName,
                                           AttributeNames,
                                           new string[] {"A2"});

            Trace.Write(filePath + " generated successfully");

            return filePath;
        }
    }
}