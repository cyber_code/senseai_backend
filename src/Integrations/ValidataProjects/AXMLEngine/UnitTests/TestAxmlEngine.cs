using System;
using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;
using Validata.Common;
using ValidataCommon;

namespace AXMLEngine.UnitTests
{
    [TestFixture]
    public class TestAxmlEngine
    {


        [TestFixtureSetUp]
        public void TestSave()
        {
            DifferenceErrorConfiguration save;

            // configuration
            save = new DifferenceErrorConfiguration();
            save.Version = 1;
            save.DifferenceErrorIgnoreRuleList = new List<DifferenceErrorIgnoreRule>();
            save.DifferenceErrorConversionRuleList = new List<DifferenceErrorConversionRule>();

            // Difference Ignore
            DifferenceErrorIgnoreRule ignoreRule = null;

            // 1st rule
            ignoreRule = new DifferenceErrorIgnoreRule();
            ignoreRule.Name = "Ignore NEW in ARRANGEMENT";
            ignoreRule.Enabled = true;
            ignoreRule.Source = new IgnoreRuleElement("*", "*", "ARRANGEMENT", "NEW");
            ignoreRule.Target = new IgnoreRuleElement("*", "*", "ARRANGEMENT", "*");
            save.DifferenceErrorIgnoreRuleList.Add(ignoreRule);

            // 2nd rule
            ignoreRule = new DifferenceErrorIgnoreRule();
            ignoreRule.Name = "Ignore NULL in AA.*";
            ignoreRule.Enabled = true;
            ignoreRule.Source = new IgnoreRuleElement("*", "AA.*", "*", "NULL");
            ignoreRule.Target = new IgnoreRuleElement("*", "AA.*", "*", "*");
            save.DifferenceErrorIgnoreRuleList.Add(ignoreRule);

            // Difference Conversion
            DifferenceErrorConversionRule conversionRule = new DifferenceErrorConversionRule();

            // Decimal
            conversionRule.Name = "Convert Decimals";
            conversionRule.Enabled = true;
            conversionRule.Source = new RuleElement("*", "*", "*");
            conversionRule.Target = new RuleElement("*", "*", "*");
            conversionRule.ConversionType = "DECIMAL";
            save.DifferenceErrorConversionRuleList.Add(conversionRule);

            // store
            DifferenceErrorConfiguration.SerializeToXml(save, Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");
        }

        [Test]
        public void TestWriteRead()
        {
            string xml = PrepareAndSerializeConfig();
            Debug.WriteLine(xml);
            VerifyConfigurationIsReadCorrectly(xml);
        }

        private const string WickedString = "String1<>&'";
        private const string WickedString2 = "String2<>&'";
        private const int ComponentPort = 1111;

        private static void VerifyConfigurationIsReadCorrectly(string xml)
        {
            AXMLEngine deserialized = new AXMLEngine();
            deserialized.FromXML(xml);

            VerifyInstancesAreOK(deserialized);
            VerifyErrorsAreOK(deserialized);
            VerifyFiltersAreOK(deserialized);
            VerifyParametersAreOK(deserialized);
            VerifyComponentsAndPerfIndicatorsAreOK(deserialized);
            VerifyMetadataIsOK(deserialized);
            VerifyExecutionTimesAreOK(deserialized);
        }

        private static void VerifyMetadataIsOK(AXMLEngine deserialized)
        {
            //check if Metadata is OK
            Assert.AreEqual(deserialized.DefaultRequest.RequestMetadata.Typicals.Count, 1);
            MetadataTypical typical = deserialized.DefaultRequest.RequestMetadata.Typicals[0];
            Assert.AreEqual(WickedString, typical.Name);
            Assert.AreEqual("CatalogName", typical.CatalogName);
            Assert.AreEqual(WickedString, typical.BaseClass);
            Assert.AreEqual("1", typical.ID);

            Assert.AreEqual(2, typical.Attributes.Count);

            TypicalAttribute ta = typical.Attributes[0];
            Assert.AreEqual(WickedString, ta.Name);
            Assert.AreEqual(AttributeDataType.DateTime, ta.Type);
            Assert.AreEqual(WickedString, ta.Remarks);
            Assert.AreEqual(8, ta.DataLength);

            TypicalAttribute ta2 = typical.Attributes[1];
            Assert.AreEqual(WickedString2, ta2.Name);
            Assert.AreEqual(AttributeDataType.String, ta2.Type);
            Assert.AreEqual("", ta2.Remarks);
            Assert.AreEqual(0, ta2.DataLength);
        }

        private static void VerifyParametersAreOK(AXMLEngine deserialized)
        {
            Assert.AreEqual(1, deserialized.DefaultRequest.Parameters.Parameters.Count);
            Assert.AreEqual("adapter", deserialized.DefaultRequest.Parameters.AdapterName);
            Assert.AreEqual(WickedString, deserialized.DefaultRequest.Parameters.GetParameterValue("param"));
        }

        private static readonly DateTime ExecutionEndTime = new DateTime(2000, 1, 1, 14, 15, 16, 30);

        private static void VerifyExecutionTimesAreOK(AXMLEngine deserialized)
        {
            Assert.AreEqual(deserialized.DefaultRequest.Datasets.ExecutionTimes.Count, 1);
            ExecutionTiming timing = deserialized.DefaultRequest.Datasets.ExecutionTimes[0];

            Assert.AreEqual(2, timing.InstanceID);
            Assert.AreEqual(DateTime.MinValue, timing.Start);
            Assert.AreEqual(ExecutionEndTime, timing.End);
        }

        private static void VerifyComponentsAndPerfIndicatorsAreOK(AXMLEngine deserialized)
        {
            // check Components with performance indicators & measurements
            Assert.AreEqual(deserialized.DefaultRequest.Datasets.Components.Count, 1);

            Component componentRead = deserialized.DefaultRequest.Datasets.Components[0];
            Assert.AreEqual(WickedString, componentRead.Name);
            Assert.AreEqual(WickedString, componentRead.URL);
            Assert.AreEqual(ComponentPort, componentRead.Port);
            Assert.AreEqual(1, componentRead.Indicators.Count);

            PerformanceIndicator performanceIndicatorRead = componentRead.Indicators[0];
            Assert.AreEqual("CPU", performanceIndicatorRead.Name);
            Assert.AreEqual(1, performanceIndicatorRead.MemberValues.Count);
            Assert.AreEqual("MIN", performanceIndicatorRead.MemberValues[0].Function);
            Assert.AreEqual("1", performanceIndicatorRead.MemberValues[0].Value);
        }

        private static void VerifyFiltersAreOK(AXMLEngine deserialized)
        {
            Assert.AreEqual(Enum.GetValues(typeof (FilterType)).Length, deserialized.DefaultRequest.Filters.Count);
            foreach (Filter f in deserialized.DefaultRequest.Filters.FiltersCollection)
            {
                Assert.AreEqual(WickedString, f.AttributeName);
                Assert.AreEqual(WickedString, f.AttributeFilter);
                Assert.AreEqual(AttributeDataType.String, f.AttributeDataType);
            }
        }

        private static void VerifyErrorsAreOK(AXMLEngine deserialized)
        {
            Assert.AreEqual(1, deserialized.DefaultRequest.Datasets.DefaultErrorSet.Errors.Count);
            Assert.AreEqual(WickedString, deserialized.DefaultRequest.Datasets.DefaultErrorSet.Errors[0].Message);
            Assert.AreEqual(Error.ErrorTypes.Normal, deserialized.DefaultRequest.Datasets.DefaultErrorSet.Errors[0].Type);
        }

        private static void VerifyInstancesAreOK(AXMLEngine deserialized)
        {
            Assert.AreEqual(2, deserialized.DefaultRequest.Datasets.DefaultDataset.XmlInstances.Count);

            new InstanceAttributesComparer(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"AdapterFiles\\Assemblies\\DifferenceErrorUT.Config");
            Assert.IsTrue(InstanceAttributesComparer.Compare(
                              GetSampleInstance(),
                              deserialized.DefaultRequest.Datasets.DefaultDataset.XmlInstances[0]).IsCompletetelyIdentical);
            Assert.IsTrue(InstanceAttributesComparer.Compare(
                              GetSampleInstance(),
                              deserialized.DefaultRequest.Datasets.DefaultDataset.XmlInstances[1]).IsCompletetelyIdentical);
        }

        private static string PrepareAndSerializeConfig()
        {
            AXMLEngine engine = new AXMLEngine();

            engine.AdaptersRequests.Add(new AdapterRequest());

            // dataset with instances
            Instance instance = GetSampleInstance();
            engine.DefaultRequest.Datasets.AddDataset(new AXMLDataset(1));
            engine.DefaultRequest.Datasets.DefaultDataset.AddInstance(instance);
            engine.DefaultRequest.Datasets.DefaultDataset.AddInstance(instance);

            // errors
            engine.DefaultRequest.Datasets.DefaultErrorSet.AddError(new Error(WickedString, Error.ErrorTypes.Normal));

            // execution times
            engine.DefaultRequest.Datasets.ExecutionTimes.Add(new ExecutionTiming
                                                                  {
                                                                      InstanceID = 2,
                                                                      Start = DateTime.MinValue,
                                                                      End = ExecutionEndTime,
                                                                  });

            // Components with performance indicators & measurements
            Component component = new Component(WickedString, WickedString, ComponentPort);
            PerformanceIndicator performanceIndicator = new PerformanceIndicator("CPU");
            performanceIndicator.AddMemberValue("1", "MIN");
            component.AddIndicator(performanceIndicator);
            engine.DefaultRequest.Datasets.Components.Add(component);

            // metadata
            MetadataTypical typical = new MetadataTypical();
            typical.CatalogName = "CatalogName";
            typical.Name = WickedString;
            typical.BaseClass = WickedString;
            typical.ID = "1";
            typical.AddAttribute(new TypicalAttribute(WickedString, AttributeDataType.DateTime, WickedString, 8));
            typical.AddAttribute(new TypicalAttribute(WickedString2, AttributeDataType.String, null, 0));
            engine.DefaultRequest.RequestMetadata.Typicals.Add(typical);

            // filters
            foreach (FilterType filterType in Enum.GetValues(typeof (FilterType)))
            {
                engine.DefaultRequest.Filters.AddFilter(WickedString, WickedString, AttributeDataType.String, filterType);
            }

            engine.DefaultRequest.GatewayConfiguration.ErrorsFile = "error.xml";

            // adapter name & parameters
            engine.DefaultRequest.Parameters.AdapterName = "adapter";
            engine.DefaultRequest.Parameters.AddParameter("param", WickedString);

            return engine.ToXML();
        }

        private static Instance GetSampleInstance()
        {
            Instance instance = new Instance();
            instance.Catalog = "SampleCatalog";
            instance.TypicalName = WickedString;
            instance.AddAttribute(WickedString, WickedString);
            instance.AddAttribute(WickedString2, WickedString2);
            return instance;
        }
    }
}