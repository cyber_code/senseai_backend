﻿using System.Collections.Generic;
using NUnit.Framework;

namespace AXMLEngine.UnitTests
{
    [TestFixture]
    public class TestInstanceMultiAnsSubValue
    {
        private static Instance GetSampleInstance()
        {
            Instance instance = new Instance();

            // notice the mixed order of the attributes
            instance.AddAttribute("a-1~1", "1");
            instance.AddAttribute("a-1~3", "2");
            instance.AddAttribute("a-1~2", "3");
            instance.AddAttribute("a-2~2", "4");
            instance.AddAttribute("a-2~1", "5");

            instance.AddAttribute("b-1~1", "6");// only 1 multivalue
            instance.AddAttribute("c", "7");  // no multivalue 

            return instance;
        }

        [Test]
        public void TestGetMultivalueAttributesByShortFieldName()
        {
            Instance instance = GetSampleInstance();

            Assert.AreEqual(5, instance.GetMultivalueAttributesByShortFieldName("a").Count);
            Assert.AreEqual(1, instance.GetMultivalueAttributesByShortFieldName("b").Count);
            Assert.AreEqual(0, instance.GetMultivalueAttributesByShortFieldName("c").Count);
        }

        [Test]
        public void TestGetAttributesGroupedByFIeldNameAndMultivalueIndices()
        {
            Instance instance = GetSampleInstance();

            Dictionary<int, List<InstanceAttribute>> groupA = instance.GetSubvalueAttributesGroupedByFieldNameAndMultivalueIndex("a");
            Assert.AreEqual(2, groupA.Count);
            Assert.AreEqual("a-1~1", groupA[1][0].Name);
            Assert.AreEqual("a-1~2", groupA[1][1].Name);
            Assert.AreEqual("a-1~3", groupA[1][2].Name);

            Assert.AreEqual(1, instance.GetSubvalueAttributesGroupedByFieldNameAndMultivalueIndex("b").Count);
            Assert.AreEqual(0, instance.GetSubvalueAttributesGroupedByFieldNameAndMultivalueIndex("c").Count);
        }
    }
}