using System;
using NUnit.Framework;

namespace AXMLEngine.UnitTests
{
    [TestFixture]
    public class TestInstanceXmlSerialization
    {
        [Test]
        public void TestEmptyInstanceSerializeAndDeserialize()
        {
            AssertInstanceSerializationWorks(GetEmptyInstance());
        }

        [Test]
        public void TestSimpleInstanceSerializeAndDeserialize()
        {
            AssertInstanceSerializationWorks(GetSimpleInstance());
        }

        [Test]
        public void TestComplexInstanceSerializeAndDeserialize()
        {
            AssertInstanceSerializationWorks(GetWeirdInstance());
        }

        private static void AssertInstanceSerializationWorks(Instance original)
        {
            string vxml = original.ToXmlString();
            Instance deserialized = Instance.FromXmlString(vxml);
            AssertIstancesAreEquivalent(original, deserialized);
        }

        private static void AssertIstancesAreEquivalent(Instance inst1, Instance inst2)
        {
            string[] attributes1 = inst1.GetAttributeNames();
            Array.Sort(attributes1);
            string[] attributes2 = inst2.GetAttributeNames();
            Array.Sort(attributes2);

            Assert.AreEqual(attributes1.Length, attributes2.Length);
            for (int i = 0; i < attributes1.Length; i++)
            {
                Assert.AreEqual(attributes1[i], attributes2[i]);
                string attrName = attributes1[i];
                Assert.AreEqual(inst1.GetAttributeValue(attrName), inst2.GetAttributeValue(attrName));
            }
        }

        #region Test Data

        private static Instance GetEmptyInstance()
        {
            return new Instance("EmptyTypical", "Empty Catalog");
        }

        private static Instance GetSimpleInstance()
        {
            Instance instance = new Instance("Typical", "Catalog");
            instance.AddAttribute("simpleAttribute", "simpleValue");
            return instance;
        }

        private static Instance GetWeirdInstance()
        {
            const string weirdChars = "!@#$%^&*()~<>";
            Instance instance = new Instance("Typical" + weirdChars, "Catalog" + weirdChars);
            instance.AddAttribute("complexAttribute" + weirdChars, "complexValue" + weirdChars);
            return instance;
        }

        private static InstanceCollection GetTestInstances()
        {
            InstanceCollection collection = new InstanceCollection();
            collection.Add(GetEmptyInstance());
            collection.Add(GetSimpleInstance());
            collection.Add(GetWeirdInstance());
            return collection;
        }

        [Test]
        public void TestMultipleInstanceSerializeAndDeserialize()
        {
            InstanceCollection originalInstances = GetTestInstances();

            string result = originalInstances.GetXml(InstanceCollection.GetXmlFragmentWritingSettings());
            InstanceCollection deserializedInstances = InstanceCollection.FromXmlString(result, InstanceCollection.GetXmlFragmentReadingSettings());

            AssertInstanceCollectionSerializationWorks(originalInstances, deserializedInstances);
        }

        private static InstanceCollection GetNonEmptyInstances(InstanceCollection col)
        {
            InstanceCollection nonEmptyInstances = new InstanceCollection();
            foreach(Instance instance in col)
            {
                if(instance.Attributes.Count > 0)
                {
                    nonEmptyInstances.Add(instance);
                }
            }

            return nonEmptyInstances;
        }

        private static void AssertInstanceCollectionSerializationWorks(InstanceCollection original, InstanceCollection deserialized)
        {
            //original = GetNonEmptyInstances(original); // the empty instances are skipped, which might not be a good idea
            Assert.AreEqual(original.Count, deserialized.Count); 

            for (int i = 0; i < original.Count; i++)
            {
                AssertIstancesAreEquivalent(original[i], deserialized[i]);
            }
        }

        #endregion
    }
}