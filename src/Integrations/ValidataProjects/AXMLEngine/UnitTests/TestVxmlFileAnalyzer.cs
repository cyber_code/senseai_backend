﻿using NUnit.Framework;
using System;

namespace AXMLEngine.UnitTests
{
    [TestFixture]
    [Explicit]
    public class TestVxmlFileAnalyzer
    {
        [Test]
        public void Count_2_Instances_In_Vxml()
        {
            VxmlFileAnalyzer.VxmlFileStats stats = VxmlFileAnalyzer.AnalyzeVxmlFile(Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") +@"temp\Workflow_TEST_ReportLine_2.xml");
            Assert.AreEqual(2, stats.TotalInstancesCount);
        }
    }
}