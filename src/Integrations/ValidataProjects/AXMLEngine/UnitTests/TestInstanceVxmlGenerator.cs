﻿using System;
using System.Diagnostics;
using NUnit.Framework;

namespace AXMLEngine.UnitTests
{
    [TestFixture]
    [Explicit]
    public class TestInstanceVxmlGenerator
    {
        [Test]
        public void Generate_TEST_ReportLine()
        {
            string[] attributeNames = new string[]
                                          {
                                              "LineID",
                                              "ConsolidationKey",
                                              "Currency",
                                              "Type",
                                              "Description1",
                                              "Description2",
                                              "DTBAL",
                                              "CRBAL",
                                              "CALC_NetBal"
                                          };

            const int itemsCount = 2;
            const string catalogName = "Workflow";
            const string typicalName = "TEST_ReportLine";

            string filePath = string.Format(@"{0}temp\{1}_{2}_{3}.xml", Environment.ExpandEnvironmentVariables("%VALIDATA_ROOT%") ,catalogName, typicalName, itemsCount);

            InstanceVxmlGenerator.Generate(filePath,
                                           itemsCount,
                                           catalogName,
                                           typicalName,
                                           attributeNames,
                                           new string[] { "LineID" }); // a siingle key change is enough

            Trace.Write(filePath + " generated successfully");
        }
    }
}