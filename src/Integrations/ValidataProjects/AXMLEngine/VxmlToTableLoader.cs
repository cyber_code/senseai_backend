﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace AXMLEngine
{
    /// <summary>
    /// Load VXML data from a file to a DataTable
    /// </summary>
    public class VxmlToTableLoader
    {
        private DataTable _Table;
        private List<string> _ColumnNames;
        private List<DataRow> _InvalidRows;

        /// <summary>
        /// Gets the invalid rows.
        /// </summary>
        /// <value>The invalid rows.</value>
        public List<DataRow> InvalidRows
        {
            get { return _InvalidRows; }
        }

        /// <summary>
        /// Reads the VXML file (we expect it to have a start element)
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="filePath">The file path.</param>
        public void ReadVxmlFile(DataTable table, string filePath)
        {
            _Table = table;
            _ColumnNames = GetTableColumnNames(table);
            _InvalidRows = new List<DataRow>();

            foreach (Instance inst in VxmlFileInstancesEnumerator.EnumerateInstances(filePath))
            {
                AddRowFromInstance(inst);
            }
        }

        private static List<string> GetTableColumnNames(DataTable table)
        {
            List<string> columnNames = new List<string>();
            foreach (DataColumn column in table.Columns)
            {
                columnNames.Add(column.ColumnName);
            }

            columnNames.Sort();
            return columnNames;
        }

        private void AddRowFromInstance(Instance instance)
        {
            DataRow row = _Table.NewRow();
            foreach (InstanceAttribute attr in instance.Attributes)
            {
                if (_ColumnNames.BinarySearch(attr.Name) >= 0)
                {
                    try
                    {
                        // TODO we might have to do a conversion if the column is not string column
                        row[attr.Name] = attr.Value;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                        Debug.Fail("Could not set value in column " + attr.Name);
                    }
                }
            }

            try
            {
                _Table.Rows.Add(row);
            }
            catch (ConstraintException)
            {
                _InvalidRows.Add(row);
            }
        }
    }
}