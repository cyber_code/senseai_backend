﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace AXMLEngine
{
    [Serializable]
    public class ExecutionTimingsList : List<ExecutionTiming>
    {
        public void ToXML(XmlWriter xmlWriter)
        {
            // Write performance Components to XML
            foreach (ExecutionTiming timing in this)
            {
                timing.ToXML(xmlWriter);
                xmlWriter.WriteString(Environment.NewLine);
            }
        }

        /// <summary>
        /// Used in case the execution times are a part of another XML document
        /// </summary>
        /// <param name="xmlWriter"></param>
        public static void OpenParentNode(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.EXECUTION_TIMES_SECTION_TAG);
            xmlWriter.WriteString(Environment.NewLine);
        }

        /// <summary>
        /// Used in case the execution times are a part of another XML document
        /// </summary>
        /// <param name="xmlWriter"></param>
        public static void CloseParentNode(XmlWriter xmlWriter)
        {
            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Used in case the execution times are written in their own XML document
        /// </summary>
        /// <param name="xmlWriter"></param>
        public static void OpenRootNode(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.EXECUTION_TIME_CATALOG);
            xmlWriter.WriteAttributeString(AXMLConstants.EXECUTION_TIMES_ROOT_ATR_NAME,
                                           AXMLConstants.EXECUTION_TIMES_ROOT_ATR_VALUE);
            xmlWriter.WriteString(Environment.NewLine);
        }

        /// <summary>
        /// Used in case the execution times are written in their own XML document
        /// </summary>
        /// <param name="xmlWriter"></param>
        public static void CloseRootNode(XmlWriter xmlWriter)
        {
            xmlWriter.WriteEndElement();
        }

        public bool FromXML(XmlReader xmlReader)
        {
            bool isStartElementRead = false;

            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == XmlNodeType.Element)
                {
                    if (xmlReader.Name == AXMLConstants.EXECUTION_TIMES_SECTION_TAG)
                    {
                        isStartElementRead = true;

                        ExecutionTiming timing = new ExecutionTiming();
                        if (timing.FromXML(xmlReader))
                        {
                            Add(timing);
                        }
                    }
                }
                else if (xmlReader.NodeType == XmlNodeType.EndElement)
                {
                    if (xmlReader.Name == AXMLConstants.EXECUTION_TIMES_SECTION_TAG)
                    {
                        return true;
                    }
                }
            }

            return ! isStartElementRead; // it might be empty, but it should not be incomplete
        }
    }
}