﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Xml;
using ValidataCommon;

namespace AXMLEngine
{
    /// <summary>
    /// Collection of InstanceAttribute
    /// </summary>
    [Serializable]
    public class InstanceAttributesCollection : List<InstanceAttribute>, IEnumerable<IAttribute>
    {
        #region Constructors

        public InstanceAttributesCollection()
        {
        }

        public InstanceAttributesCollection(IEnumerable<InstanceAttribute> instances)
            : base(instances)
        {
        }

        public InstanceAttributesCollection(int capacity)
            : base(capacity)
        {
        }

        #endregion

        public NameValueCollection AsNameValueCollection
        {
            get
            {
                NameValueCollection result = new NameValueCollection();
                foreach (InstanceAttribute ia in this)
                {
                    result[ia.Name] = ia.Value;
                }

                return result;
            }
        }

        public Dictionary<string, string> AsDictionary
        {
            get
            {
                Dictionary<string, string> result = new Dictionary<string, string>();
                foreach (InstanceAttribute ia in this)
                {
                    result[ia.Name] = ia.Value;
                }

                return result;
            }
        }

        public void SortByAttributeName()
        {
            Sort(InstanceAttribute.CompareAttributeName);
        }

        public static InstanceAttributesCollection GetSortedCopy(IEnumerable<InstanceAttribute> instances)
        {
            InstanceAttributesCollection copy = new InstanceAttributesCollection(instances);
            copy.SortByAttributeName();
            return copy;
        }

        public string GetXml()
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter stringWriter = new StringWriter(sb))
            {
                using (XmlWriter xmlWriter = new SafeXmlTextWriter(stringWriter))
                {
                    ToXml(xmlWriter);
                    xmlWriter.Flush();
                }
            }

            return sb.ToString();
        }

        public void ToXml(XmlWriter writer)
        {
            foreach (InstanceAttribute attribute in this)
            {
                attribute.ToXML(writer);
            }
        }

        public InstanceAttribute Find(string attrName)
        {
            foreach (InstanceAttribute ia in this)
            {
                if (ia.Name == attrName)
                {
                    return ia;
                }
            }

            return null;
        }

        #region IEnumerable<IAttribute> Members

        IEnumerator<IAttribute> IEnumerable<IAttribute>.GetEnumerator()
        {
            int idx = 0;

            while (idx < base.Count)
            {
                yield return base[idx++];
            }
        }

        #endregion
    }
}