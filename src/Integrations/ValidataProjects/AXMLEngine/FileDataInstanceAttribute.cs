﻿using System;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using ValidataCommon;

namespace AXMLEngine
{
    [Serializable]
    public sealed class FileDataInstanceAttribute : InstanceAttribute
    {
        private const string AttributeName = "File Data";

        public FileDataInstanceAttribute(byte[] blobValue) : base(AttributeName, String.Empty)
        {
            BlobValue = blobValue;
            BlobSize = blobValue.LongLength;
        }

        [XmlAttribute]
        public long BlobSize { get; private set; }

        [XmlIgnore]
        public byte[] BlobValue { get; private set; }

        /// <summary>
        /// Writes the attribute to the XML output.
        /// </summary>
        /// <param name="xmlWriter"></param>
        public override void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(XmlConvert.EncodeName(Name));
            xmlWriter.WriteAttributeString(XmlConvert.EncodeName("BlobSize"), Utils.GetHtmlEncodedValue(BlobSize.ToString()));
            xmlWriter.WriteBinHex(BlobValue, 0, BlobValue.Length - 1);
            xmlWriter.WriteEndElement();
        }
    }
}