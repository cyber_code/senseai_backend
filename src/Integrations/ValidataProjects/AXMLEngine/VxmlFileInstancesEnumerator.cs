using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace AXMLEngine
{
    /// <summary>
    /// Analyzer of a VXML file (currently only count the number of instances)
    /// </summary>
    public static class VxmlFileInstancesEnumerator
    {
        /// <summary>
        /// Reads the VXML file (we expect it to have a start element)
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public static IEnumerable<Instance> EnumerateInstances(string filePath)
        {
            if (!File.Exists(filePath))
            {
               yield break;
            }

            // Load XML Document
            using (XmlReader xmlReader = new XmlTextReader(filePath))
            {
                //move to root element
                while (true)
                {
                    if (!xmlReader.Read())
                    {
                        yield break;
                    }
                    else if (xmlReader.NodeType == XmlNodeType.Element)
                    {
                        //we have reached the root element
                        break;
                    }
                }

                xmlReader.MoveToContent();

                foreach (Instance instance in InstanceCollection.EnumerateInstancesFromXml(xmlReader))
                {
                    yield return instance;
                }
            }
        }
    }
}