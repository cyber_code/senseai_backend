using System;
using System.Xml;

namespace AXMLEngine
{
    [Serializable]
    public class MemberValue
    {
        #region Private members

        private string _MeasuredValue;
        private string _AggregateFunctionType;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Constructs an instance of MemberValue class
        /// </summary>
        /// <param name="memVal"></param>
        /// <param name="ft"></param>
        public MemberValue(string memVal, string ft)
        {
            _MeasuredValue = memVal;
            _AggregateFunctionType = ft;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Serializes a member value object to XML
        /// </summary>
        /// <param name="xmlWriter">XmlWritern</param>
        /// <param name="index">Index of the member value</param>
        public void ToXML(XmlWriter xmlWriter, int index)
        {
            xmlWriter.WriteStartElement(AXMLConstants.MEMBERVALUE_TAG);

            xmlWriter.WriteAttributeString(AXMLConstants.MEMBER_VALUE_ATTRIBUTE, _MeasuredValue);
            xmlWriter.WriteAttributeString(AXMLConstants.MEMBER_FUNCTION_ATTRIBUTE, _AggregateFunctionType);

            xmlWriter.WriteEndElement();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The measured value
        /// </summary>
        public string Value
        {
            get { return _MeasuredValue; }
        }

        /// <summary>
        /// The aggregating function name
        /// </summary>
        public string Function
        {
            get { return _AggregateFunctionType; }
        }

        #endregion

        public override string ToString()
        {
            return string.Format("{0} {1}", (Function ?? ""), (Value ?? ""));
        }
    }
}