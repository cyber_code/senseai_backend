using System;
using System.Xml;

namespace AXMLEngine
{
    /// <summary>
    /// Performance indicator name + optional measurements
    /// </summary>
    [Serializable]
    public class PerformanceIndicator
    {
        #region Private members

        /// <summary>
        /// The name of the indicator
        /// </summary>
        private string _Name;

        /// <summary>
        /// The member values
        /// </summary>
        private MemberValueCollection _MemberValues;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Constructs an instance of the PerformanceIndicator class
        /// </summary>
        public PerformanceIndicator(string name)
        {
            _Name = name;
            _MemberValues = new MemberValueCollection();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Serializes a Performance Indicator object to AXML
        /// </summary>
        /// <param name="xmlWriter">XmlWriter where to serialize the Filters section</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.INDICATOR_TAG);

            //write name 
            xmlWriter.WriteAttributeString(AXMLConstants.INDICATOR_NAME_ATTRIBUTE, _Name);

            //write member values
            for (int i = 0; i < _MemberValues.Count; i++)
            {
                MemberValue memVal = _MemberValues[i];
                memVal.ToXML(xmlWriter, i);
            }

            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Constructs a PerformanceIndicator object from XML.
        /// </summary>
        /// <param name="indicatorReader">A reader for reading the performance indcator.</param>
        public static PerformanceIndicator FromXML(XmlReader indicatorReader)
        {
            PerformanceIndicator indicator = null;
            while (indicatorReader.Read())
            {
                if (indicatorReader.NodeType == XmlNodeType.Element)
                {
                    if (indicatorReader.Name == AXMLConstants.INDICATOR_TAG)
                    {
                        string name = indicatorReader.GetAttribute(AXMLConstants.INDICATOR_NAME_ATTRIBUTE);

                        indicator = new PerformanceIndicator(name);
                    }
                    else if (indicatorReader.Name == AXMLConstants.MEMBERVALUE_TAG)
                    {
                        string val = indicatorReader.GetAttribute(AXMLConstants.MEMBER_VALUE_ATTRIBUTE);
                        string function = indicatorReader.GetAttribute(AXMLConstants.MEMBER_FUNCTION_ATTRIBUTE);

                        if (indicator == null)
                            throw new ApplicationException("The XML is not valid for the PerformanceIndicator section!");
                        
                        indicator.AddMemberValue(val, function);
                    }
                }
            }

            return indicator;
        }

        /// <summary>
        /// Add a member value to the member value collection
        /// </summary>
        /// <param name="memVal">The value of the member values</param>
        /// <param name="fType">The function type</param>
        public void AddMemberValue(string memVal, string fType)
        {
            MemberValue memValue = new MemberValue(memVal, fType);
            _MemberValues.Add(memValue);
        }

        #endregion

        #region Public properties

        /// <summary>
        /// The name of the performance indicator
        /// </summary>
        public string Name
        {
            get { return _Name; }
        }

        /// <summary>
        /// The values stored in the result collection
        /// </summary>
        public MemberValueCollection MemberValues
        {
            get { return _MemberValues; }
        }

        #endregion

        public override string ToString()
        {
            return string.Format("{0} - {1}", (_Name ?? ""), _MemberValues.Count);
        }
    }
}