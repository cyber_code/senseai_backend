﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using ValidataCommon;

namespace AXMLEngine
{
    public static class InstanceVxmlGenerator
    {
        public static void Generate(string filePath,
                                    int count,
                                    string catalogName,
                                    string typicalName,
                                    IEnumerable<string> allAttributeNames,
                                    IEnumerable<string> incrementalAttributeNames)
        {
            using (XmlWriter xmlWriter = new SafeXmlTextWriter(filePath, Encoding.UTF8))
            {
                xmlWriter.WriteStartElement(catalogName);

                foreach (Instance instance in InstanceGenerator.Generate(count, catalogName, typicalName, allAttributeNames, incrementalAttributeNames))
                {
                    instance.ToXML(xmlWriter);
                }

                xmlWriter.WriteEndElement();
            }
        }
    }
}