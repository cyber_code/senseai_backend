﻿using System;
using System.Xml;

namespace AXMLEngine
{
    [Serializable]
    public class ExecutionTiming
    {
        public ulong InstanceID;
        public DateTime Start;
        public DateTime End;

        public TimeSpan Duration
        {
            get { return End - Start; }
        }

        private const string DateFormat = "dd/MM/yyyy HH:mm:ss.fff";

        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.EXECUTION_TIME_TAG);

            xmlWriter.WriteAttributeString(AXMLConstants.EXECUTION_TIME_CATALOG_ATR_NAME,
                                           AXMLConstants.EXECUTION_TIME_CATALOG);

            xmlWriter.WriteElementString(AXMLConstants.INSTANCE_ID_ATTR, InstanceID.ToString());
            xmlWriter.WriteElementString(AXMLConstants.OPERATION_START_TIME_ATTR, Start.ToString(DateFormat));
            xmlWriter.WriteElementString(AXMLConstants.OPERATION_END_TIME_ATTR, End.ToString(DateFormat));
            xmlWriter.WriteElementString(AXMLConstants.OPERATION_DURATION, End.Subtract(Start).TotalMilliseconds.ToString());

            xmlWriter.WriteEndElement();
        }

        public bool FromXML(XmlReader xmlReader)
        {
            int success = 0;
            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == XmlNodeType.Element)
                {
                    if (xmlReader.Name == AXMLConstants.EXECUTION_TIME_TAG)
                    {
                        success++;
                        /*InstanceID = ulong.Parse(xmlReader.GetAttribute(AXMLConstants.INSTANCE_ID_ATTR));
                        Start = DateTime.ParseExact(xmlReader.GetAttribute(AXMLConstants.OPERATION_START_TIME_ATTR),DateFormat, null);
                        End = DateTime.ParseExact(xmlReader.GetAttribute(AXMLConstants.OPERATION_END_TIME_ATTR), DateFormat, null);

                        return true;*/
                    }

                    if (xmlReader.Name == AXMLConstants.INSTANCE_ID_ATTR)
                    {
                        InstanceID = ulong.Parse(xmlReader.ReadString());
                        success++;
                    }

                    if (xmlReader.Name == AXMLConstants.OPERATION_START_TIME_ATTR)
                    {
                        Start = DateTime.ParseExact(xmlReader.ReadString(), DateFormat, null);
                        success++;
                    }

                    if (xmlReader.Name == AXMLConstants.OPERATION_END_TIME_ATTR)
                    {
                        End = DateTime.ParseExact(xmlReader.ReadString(), DateFormat, null);
                        return success == 3;
                    }
                }
            }

            return false;
        }
    }
}