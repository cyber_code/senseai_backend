using System;
using System.Web;
using System.Xml;
using Validata.Common;
using ValidataCommon;

namespace AXMLEngine
{
    /// <summary>
    /// 	Filter
    /// </summary>
    [Serializable]
    public class Filter : FilterElement
    {
        #region Public Enumerations

        /// <summary>
        /// 	Names of filters to display in the GUI
        /// </summary>
        public new static string[] FilterDisplayNames = { ">", ">=", "=", "<=", "<", "like", "<>", "not like" };

        #endregion

        #region Public Static Methods

        /// <summary>
        /// 	Gets the T24 filter criteria.
        /// </summary>
        /// <param name="fltType"> Type of the filter. </param>
        /// <returns> </returns>
        public new static string GetT24FilterCriteria(FilterType fltType)
        {
            switch (fltType)
            {
                case FilterType.Equal:
                    return "EQ";
                case FilterType.NotEqual:
                    return "NE";
                case FilterType.Like:
                    return "LK";
                case FilterType.NotLike:
                    return "UL";
                case FilterType.GreaterThan:
                    return "GT";
                case FilterType.Less:
                    return "LT";
                case FilterType.GreaterOrEqual:
                    return "GE";
                case FilterType.LessOrEqual:
                    return "LE";
                case FilterType.Between:
                    return "RG";
                default:
                    throw new NotSupportedException("The filter " + fltType + " is not supported.");
            }
        }

        /// <summary>
        /// 	Gets the display name of the filter type.
        /// </summary>
        /// <param name="filterSymbol"> The filter symbol. </param>
        /// <returns> </returns>
        public new static FilterType GetFilterTypeFromDisplayName(string filterSymbol)
        {
            int index = FindDisplayNameIndex(filterSymbol);
            if (index < 0)
            {
                throw new InvalidOperationException(filterSymbol + " is not expected");
            }
            else
            {
                return (FilterType)index;
            }
        }

        public new static string GetFilterTypeStringFromDisplayName(string filterSymbol)
        {
            return GetFilterTypeFromDisplayName(filterSymbol).ToString();
        }

        public static string FilterType2TGNConstraintName(string filter)
        {
            switch (GetFilterTypeFromDisplayName(filter))
            {
                case FilterType.GreaterThan:
                    return "greater than";
                case FilterType.GreaterOrEqual:
                    return "greater than or equal to";
                case FilterType.Equal:
                    return "equal to";
                case FilterType.LessOrEqual:
                    return "less than or equal to";
                case FilterType.Less:
                    return "less than";
                case FilterType.Like:
                    return "like";
                case FilterType.NotEqual:
                    return "not equal";
                case FilterType.NotLike:
                    return "not like";
                default:
                    throw new Exception("Unknown Filter Type!");
            }
        }

        /// <summary>
        /// 	Converts Logical operation type to Filter Type
        /// </summary>
        /// <param name="logicalOperation"> Logical operation type </param>
        /// <returns> Returns converted Logical operation type </returns>
        public new static FilterType LogicalOperationToFilterType(string logicalOperation)
        {
            return (FilterType)Enum.Parse(typeof(FilterType), logicalOperation, true);
        }

        public new static FilterCondition ToFilterCondition(string condition)
        {
            return condition == "OR" ? FilterCondition.OR : condition == "AND" ? FilterCondition.AND : FilterCondition.Unknown;
            // return (FilterCondition)Enum.Parse(typeof(FilterCondition), condition, true);
        }


        /// <summary>
        /// 	Constructs a Filter object from XML.
        /// </summary>
        /// <param name="filterReader"> The XML defining the filter. </param>
        /// <param name="logicalOperation"> The logical operation attribute. </param>
        /// <returns> The constructed filter instance. </returns>
        public new static Filter FromXML(XmlReader filterReader, string logicalOperation)
        {
            string attrName = "";
            string attrFilter = "";
            string attrLimit = "";
            bool isPostFilter = false;
            AttributeDataType attrDataType = AttributeDataType.Unknown;
            FilterCondition condition = FilterCondition.Unknown;
            while (filterReader.Read())
            {
                if (filterReader.NodeType == XmlNodeType.Element)
                {
                    switch (filterReader.Name)
                    {
                        case AXMLConstants.FILTER_TAG:
                            {
                                //do nothing
                                break;
                            }

                        case AXMLConstants.FILTER_ATTRIBUTE_NAME_TAG:
                            {
                                attrName = filterReader.ReadString();
                                break;
                            }
                        case AXMLConstants.FILTER_ATTRIBUTE_FILTER_TAG:
                            {
                                attrFilter = filterReader.ReadString();
                                break;
                            }
                        case AXMLConstants.FILTER_ATTRIBUTE_DATA_TYPE_TAG:
                            {
                                attrDataType = AttributeDataTypeConvertor.ToEnum(filterReader.ReadString());
                                break;
                            }
                        case AXMLConstants.FILTER_CONDITION_TAG:
                            {
                                condition = filterReader.ReadString() == "OR" ? FilterCondition.OR : filterReader.ReadString() == "AND" ? FilterCondition.AND : FilterCondition.Unknown;
                                break;
                            }
                        case AXMLConstants.FILTER_IS_POST_FILTER_TAG:
                            {
                                bool.TryParse(filterReader.ReadString(), out isPostFilter);
                                break;
                            }
                    }
                }
            }

            FilterType fType = LogicalOperationToFilterType(logicalOperation);
            Filter ft = new Filter(attrName, attrFilter, attrDataType, condition, fType, attrLimit);
            ft.IsPostFilter = isPostFilter;

            return ft;
        }

        #endregion

        #region Public Properties

        public bool IsPostFilter { get; set; }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// 	Default constructor
        /// </summary>
        public Filter()
        {
        }

        /// <summary>
        /// 	Construtctor
        /// </summary>
        /// <param name="attrName"> Attribute name </param>
        /// <param name="attrFilter"> Attribute filter </param>
        /// <param name="attrDataType"> Attribute data type </param>
        /// <param name="ft"> Filter type </param>
        public Filter(string attrName, string attrFilter, AttributeDataType attrDataType, FilterType ft)
            : base(attrName, attrFilter, attrDataType, ft)
        {
        }

        public Filter(string attrName, string attrFilter, AttributeDataType attrDataType, FilterCondition condition, FilterType ft, string Limit)
          : base(attrName, attrFilter, attrDataType, condition, ft, Limit)
        {
        }

        #endregion

        #region Private Static methods

        private static int FindDisplayNameIndex(string filterDisplayName)
        {
            if (string.IsNullOrEmpty(filterDisplayName))
            {
                return -1;
            }

            for (int i = 0; i < FilterDisplayNames.Length; i++)
            {
                if (string.Compare(FilterDisplayNames[i], filterDisplayName.Trim(), true) == 0)
                {
                    return i;
                }
            }

            return -1;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// 	Serializes the filters section to the XmlWriter
        /// </summary>
        /// <param name="xmlWriter"> XmlWriter where to serialize the Filters section </param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.FILTER_TAG);

            xmlWriter.WriteAttributeString(AXMLConstants.FILTER_LOGICAL_OPERATION_TAG, Type.ToString().ToUpper());

            xmlWriter.WriteElementString(AXMLConstants.FILTER_ATTRIBUTE_NAME_TAG, AttributeName);

            xmlWriter.WriteElementString(AXMLConstants.FILTER_ATTRIBUTE_FILTER_TAG, AttributeFilter);

            xmlWriter.WriteElementString(AXMLConstants.FILTER_ATTRIBUTE_DATA_TYPE_TAG,
                                         Utils.GetHtmlEncodedValue(AttributeDataTypeConvertor.ToString(AttributeDataType).ToUpper()));

            xmlWriter.WriteElementString(AXMLConstants.FILTER_IS_POST_FILTER_TAG, IsPostFilter.ToString());

            xmlWriter.WriteEndElement();
        }

        #endregion

        #region Overrides

        /// <summary>
        /// 	Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see> .
        /// </summary>
        /// <returns> A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see> . </returns>
        public override string ToString()
        {
            return string.Format("[{0}] {1} {2}", AttributeName, Type, AttributeFilter);
        }

        /// <summary>
        /// </summary>
        /// <param name="obj"> </param>
        /// <returns> </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Filter)) return false;
            return Equals((Filter)obj);
        }

        #endregion

        public bool Equals(Filter other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other._AttributeName, _AttributeName) && Equals(other._AttributeFilter, _AttributeFilter) && Equals(other._AttributeDataType, _AttributeDataType);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (_AttributeName != null ? _AttributeName.GetHashCode() : 0);
                result = (result * 397) ^ (_AttributeFilter != null ? _AttributeFilter.GetHashCode() : 0);
                result = (result * 397) ^ _AttributeDataType.GetHashCode();
                return result;
            }
        }
    }
}