//===============================================================================
// InstanceAttribute.cs
//===============================================================================
//
// Instance attributes.
//
// History:
//	Version:                Date:                   Author:
//	1.00                    15/12/2004              Ilian
//	Description: Created initial version
//
//===============================================================================
// Copyright (C) 2004-2006 Theseus Technology Partners
// All rights reserved.
//===============================================================================

using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using ValidataCommon;

namespace AXMLEngine
{
    /// <summary>
    /// Name-Value pair for attribute name & value
    /// </summary>
    [Serializable]
    public class InstanceAttribute : IAttribute
    {
        #region Nested Classes

        public class AttributeNameComparer :  IComparer<InstanceAttribute>
        {
            public int Compare(InstanceAttribute x, InstanceAttribute y)
            {
                return CompareAttributeName(x, y);
            }
        }

        #endregion

        #region Private And Protected Members

        /// <summary>
        /// Instance Attribute Name
        /// </summary>
        private string _Name;

        /// <summary>
        /// Instance Attribute Value
        /// </summary>
        private string _Value;

        #endregion

        #region Public Properties

        /// <summary>
        /// Instace Attribute Name
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// Instance Attribute Value
        /// </summary>
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InstanceAttribute()
        {
            _Name = null;
            _Value = null;
        }

        /// <summary>
        /// Additional Constructor
        /// </summary>
        /// <param name="name">Instance Attribute Name</param>
        /// <param name="val">Instance Attribute Value</param>
        public InstanceAttribute(string name, string val)
        {
            _Name = name;
            _Value = val;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Returns the attribute value as DateTime object
        /// </summary>
        /// <returns>DateTime object</returns>
        public DateTime GetDateTimeValue()
        {
            return GetDateTimeFromValue(_Value);
        }

        /// <summary>	
        /// Gets DateTime value from string value
        /// </summary>
        /// <param name="val">Value</param>
        /// <returns>DateTime object</returns>
        public static DateTime GetDateTimeFromValue(string val)
        {
            return ValidataConverter.ParseDateTime(val, DateParsingOptions.All);
        }

        /// <summary>
        /// Writes the attribute to the XML output.
        /// </summary>
        /// <param name="xmlWriter"></param>
        public virtual void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteElementString(XmlConvert.EncodeLocalName(Name), Utils.GetHtmlEncodedValue(Value));
        }

        public override string ToString()
        {
            return (_Name ?? "") + "=" + (_Value ?? "");
        }

        #endregion

        public static int CompareAttributeName(InstanceAttribute first, InstanceAttribute second)
        {
            return string.Compare(first.Name, second.Name);
        }
    }
}