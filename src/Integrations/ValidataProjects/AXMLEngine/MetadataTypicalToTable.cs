﻿using System.Collections.Generic;
using System.Data;

namespace AXMLEngine
{
    public static class MetadataTypicalToTable
    {
        /// <summary>
        /// Creates the table (all columns are string)
        /// </summary>
        /// <param name="typical">The typical.</param>
        /// <returns></returns>
        public static DataTable CreateTable(MetadataTypical typical)
        {
            DataTable table = new DataTable();
            CreateColumns(table, typical.Attributes);
            return table;
        }

        /// <summary>
        /// Creates the columns (all columns are string)
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="typicalAttributes">The typical attributes.</param>
        public static void CreateColumns(DataTable table, IEnumerable<TypicalAttribute> typicalAttributes)
        {
            List<DataColumn> primaryKeys = new List<DataColumn>();
            foreach (TypicalAttribute attr in typicalAttributes)
            {
                DataColumn column = table.Columns.Add(attr.Name);
                column.DefaultValue = "";
                if (attr.Remarks == "KEY")
                {
                    primaryKeys.Add(column);
                }
            }
            table.PrimaryKey = primaryKeys.ToArray();
        }
    }
}