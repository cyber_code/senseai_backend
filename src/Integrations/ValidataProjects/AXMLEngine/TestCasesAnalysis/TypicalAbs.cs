﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using ValidataCommon;

namespace TestCasesAnalysis
{
    public class TypicalAbs
    {
        public static List<Association> Associations = new List<Association>();
        public static List<ActionParams> ActionParamses = new List<ActionParams>();
        public static List<TestStep> TestSteps = new List<TestStep>();
        public static List<DataRule> DataRules = new List<DataRule>();

        public List<string> IDs = new List<string>();

        public static bool IgnoreMultiAttributes;

        public string CatalogName
        {
            get;
            set;
        }

        public string TypicalName
        {
            get;
            set;
        }

        public string GroupName
        {
            get;
            set;
        }

        public string Errors
        {
            get;
            set;
        }

        public List<string> Attributes = new List<string>();
        public List<string> MultiAttributes = new List<string>();

        public static string GetChildInnerTextSafe(XmlNode parent, string name)
        {
            var childNode = GetChild(parent, name);
            if (childNode == null)
                return "";

            return childNode.InnerText;
        }

        public static XmlNode GetChild(XmlNode parent, string name)
        {
            foreach(XmlNode node in parent.ChildNodes)
            {
                if (node.Name == name)
                    return node;
            }
            return null;
        }

        public static List<string> GetAssociatedID(string id1)
        {
            var result = new List<string>();

            foreach (Association assoc in Associations)
            {
                if (assoc.TargetID == id1)
                {
                    result.Add(assoc.SourceID);
                }

                if (assoc.SourceID == id1)
                {
                    result.Add(assoc.TargetID);
                }
            }

            return result;
        }

        /*public Typical GetTypicalByID(string id)
        {
            foreach (Typical typ in )
        }*/

        public static TestStep GetTestStepByID(string id)
        {
            foreach (TestStep step in TestSteps)
            {
                if (step.ID == id)
                    return step;
            }

            return null;
        }

        public TypicalAbs(XmlNode node)
        {
            TypicalName = string.Empty;
            GroupName = string.Empty;
            TypicalName = XmlConvert.DecodeName(node.Name);

            if (TypicalName == "TestStep")
            {
                AnalyzeTestStep(node);
            }

            if (TypicalName == "Data Rule")

            {
                AnalyzeDataRule(node);
            }

            if (TypicalName == "ActionParams")
            {
                AnalyzeActionParameters(node);
            }

            if (TypicalName == "Association")
            {
                AnalyzeAssociation(node);
            }

            if ((TypicalName == "FilteringConstraint") || (TypicalName == "Automatic Calculation Item"))
            {
                AnalyzeAttribute(XmlConvert.DecodeName(GetChildInnerTextSafe(node, "AttributeName")), "Value");

                XmlNode typNode = GetChild(node, "TypicalName");
                XmlNode catNode = GetChild(node, "CatalogueName");
               
                if (typNode != null)
                    TypicalName = XmlConvert.DecodeName(typNode.InnerText);
                if (catNode != null)
                    CatalogName =  XmlConvert.DecodeName(catNode.InnerText);
                else
                    CatalogName = "[UNKNOWN]";

                return;
            }

            /*if (TypicalName == "Automatic Calculation Item")
            {
                XmlNode typNode = GetChild(node, "TypicalName");
                XmlNode catNode = GetChild(node, "CatalogueName");

                XmlNode attribute = GetChild(node, "AttributeName");

                AnalyzeAttribute(attribute.InnerText, "Value");

                if (typNode != null)
                    TypicalName = XmlConvert.DecodeName(typNode.InnerText);
                if (catNode != null)
                    CatalogName = catNode.InnerText;
                else
                    CatalogName = "[UNKNOWN]";

                return;
            }*/

            XmlAttribute catalogAttribute = node.Attributes["Catalog"];
            if (catalogAttribute != null)
            {
                CatalogName = catalogAttribute.Value;

                // analyze children - typical attributes
                foreach(XmlNode childNode in node.ChildNodes)
                {
                    AnalyzeAttribute(childNode);
                }
            }
            else
            {
                CatalogName = "[UNKNOWN]";
            }

            XmlAttribute groupAttribute = node.Attributes["groupName"];
            if (groupAttribute != null)
            {
                GroupName = groupAttribute.Value;
            }
            else
                GroupName = string.Empty;
        }

        private void AnalyzeActionParameters(XmlNode node)
        {
            var actionParam = new ActionParams();
            actionParam.ID = GetChildInnerTextSafe(node, "ID");
            actionParam.AdapterName = GetChildInnerTextSafe(node, "AdapterName");
            actionParam.FinancialObject = GetChildInnerTextSafe(node, "FinancialObject");

            ActionParamses.Add(actionParam);
        }

        private void AnalyzeAssociation(XmlNode node)
        {
            var assoc = new Association();
            assoc.ID = GetChildInnerTextSafe(node, "ID");
            assoc.Name = GetChildInnerTextSafe(node, "Name");
            assoc.Alias = GetChildInnerTextSafe(node, "Alias");
            assoc.SourceID = GetChildInnerTextSafe(node, "SourceID");
            assoc.TargetID = GetChildInnerTextSafe(node, "TargetID");

            Associations.Add(assoc);
        }

        private void AnalyzeDataRule(XmlNode node)
        {
            /*
              <Data_x0020_Rule>
    <RuleType>2</RuleType>
    <Operator>-1</Operator>
    <Attribute>[Not Selected]</Attribute>
    <ID>461968</ID>
  </Data_x0020_Rule>
             */

            var datarule = new DataRule();
            datarule.ID = GetChildInnerTextSafe(node, "ID");
            datarule.RuleType = GetChildInnerTextSafe(node, "RuleType");
            datarule.Operator = GetChildInnerTextSafe(node, "Operator");
            datarule.Attribute = GetChildInnerTextSafe(node, "Attribute");

            DataRules.Add(datarule);
        }

        private void AnalyzeTestStep(XmlNode node)
        {
    /*
  <TestStep CheckStatus="0">
    <ID>470925</ID>
    <Name>A. Close Page</Name>
    <Description>No Value</Description>
    <Type>1</Type>
    <LogicalDay>1</LogicalDay>
    <ExpectedResult>No Value</ExpectedResult>
    <ResultType>Generic FO</ResultType>
    <StepIndex>1</StepIndex>
    <Priority>2</Priority>
    <ExternalRef>No Value</ExternalRef>
    <TestStatus>0</TestStatus>
    <ManualOrAuto>2</ManualOrAuto>
    <EndCaseOnError>0</EndCaseOnError>
    <ExpectedFail>0</ExpectedFail>
    <Configuration>No Value</Configuration>
    <TestDataType>0</TestDataType>
    <Label>No Value</Label>
    <TestCaseID>470924</TestCaseID>
  </TestStep>
            */

            var testStep = new TestStep();

            testStep.ID = GetChildInnerTextSafe(node, "ID");
            testStep.Name = GetChildInnerTextSafe(node, "Name");
            testStep.Type = GetChildInnerTextSafe(node, "Type");
            testStep.LogicalDay = GetChildInnerTextSafe(node, "LogicalDay");
            testStep.ExpectedResult = GetChildInnerTextSafe(node, "ExpectedResult");
            testStep.ResultType = GetChildInnerTextSafe(node, "ResultType");

            TestSteps.Add(testStep);
            
        }

        private const string NO_VALUE = "No Value";

        private void AnalyzeAttribute(XmlNode childNode)
        {
            AnalyzeAttribute(XmlConvert.DecodeName(childNode.Name), childNode.InnerText);
        }

        private void AnalyzeAttribute(string attributeName, string attributeValue)
        {
            if (attributeValue != NO_VALUE && attributeValue != "")
            {
                if (attributeName == "ID")
                {
                    ID = attributeValue;
                    IDs.Add(ID);
                }

                string mValue = CheckMultiValue(attributeName);
                if (mValue == null)
                {
                    Attributes.Add(attributeName);
                }
                else
                {
                    if (!MultiAttributes.Contains(mValue))
                    {
                        MultiAttributes.Add(mValue);
                        Attributes.Add(mValue);
                    }
                }
            }
        }

        protected static string CheckMultiValue(string attributeName)
        {
            if (IgnoreMultiAttributes)
                return null;

            int index = attributeName.IndexOf("-");
            int index2 = attributeName.IndexOf("~");
            if ((index > 0) && (index2 > 0) && (index2 > index))
                return attributeName.Substring(0, index) + "-1~1";
            return null;
        }

        public void AddAttributes(TypicalAbs typicalAbs)
        {
            foreach(string attribute in typicalAbs.Attributes)
            {
                if (!Attributes.Contains(attribute))
                {
                    Attributes.Add(attribute);   
                }

                if (!IDs.Contains(ID))
                {
                    IDs.Add(ID);
                }
            }
        }

        public string ID
        {
            get;
            set;

        }

        private TypicalMap GetTypicalMap(string catalogName, string typicalName)
        {
            foreach (TypicalMap map in TypicalMap.TypicalMaps)
            {
                if (map.MapsToTypical(catalogName, typicalName))
                    return map;
            }

            return null;
        }

        public static void AnalyzeDataRules(List<TypicalAbs> typicals)
        {
            foreach (DataRule rule in DataRules)
            {
                AnaylzeDataRule(rule, typicals);
            }
        }

        internal static ActionParams GetActionParam(string id)
        {
            foreach(ActionParams param in ActionParamses)
            {
                if (param.ID == id)
                {
                    return param;
                }
            }

            return null;
        }

        private static void AnaylzeDataRule(DataRule rule, List<TypicalAbs> typicals)
        {
            string attribute = rule.Attribute;
            if (attribute == "[Not Selected]")
                return;

            List<string> assoc = GetAssociatedID(rule.ID);
            foreach(string id in assoc)
            {
                var testStep = GetTestStepByID(id);
                if (testStep != null)
                {
                    string typicalName = GetTypical(testStep.ResultType);
                    string catalogName = GetCatalog(testStep.ResultType);

                    if (testStep.ResultType == string.Empty)
                    {
                        List<string> testStepAssocs = GetAssociatedID(testStep.ID);
                        foreach (string assocId in testStepAssocs)
                        {
                            var ap = GetActionParam(assocId);
                            if (ap != null)
                            {
                                typicalName = GetTypical(ap.FinancialObject);
                                catalogName = GetCatalog(ap.FinancialObject);
                                break;
                            }
                        }
                    }
                    if ((typicalName == string.Empty) && (catalogName == string.Empty))
                        return;

                    foreach(TypicalAbs typical in typicals)
                    {
                        if ((typical.TypicalName == typicalName) && (typical.CatalogName == catalogName))
                        {
                            attribute = ConvertMultiValue(attribute);
                            if (attribute != null)
                                if (!typical.Attributes.Contains(attribute))
                                {
                                    typical.Attributes.Add(attribute);
                                }
                            return;
                        }
                    }
                }
            }

        }

        private static string ConvertMultiValue(string attribute)
        {
            string mValue = CheckMultiValue(attribute);
            return mValue;
        }

        private static string GetCatalog(string resultType)
        {
            string catalog;
            string typical;

            ActionParamHelper.SplitCatalogAndEntitylNames(resultType, out catalog, out typical);
            return catalog;
        }

        private static string GetTypical(string resultType)
        {
            string catalog;
            string typical;

            ActionParamHelper.SplitCatalogAndEntitylNames(resultType, out catalog, out typical);
            return typical;
        }
    }

    public class TypicalAbsComparer : IComparer<TypicalAbs>
    {
        public int Compare(TypicalAbs x, TypicalAbs y)
        {
            bool xUnk = false;
            if (x.CatalogName.StartsWith("[U"))
            {
                xUnk = true;
            }

            bool yUnk = false;
            if (y.CatalogName.StartsWith("[U"))
                yUnk = true;

            if (!((xUnk) && (yUnk)))
            {
                if (xUnk)
                    return 1;
                if (yUnk)
                    return -1;
            }

            int result = x.CatalogName.CompareTo(y.CatalogName);
            if (result != 0)
                return result;

            result = x.TypicalName.CompareTo(y.TypicalName);
            if (result != 0)
                return result;
            
            result = x.GroupName.CompareTo(y.GroupName);
            return result;
        }
    }

    public class Association
    {
        public string ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Alias
        {
            get;
            set;
        }

        public string SourceID
        {
            get;
            set;
        }

        public string TargetID
        {
            get;
            set;
        }

    }

    public class TestCase
    {
        public string ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string CreateDate
        {
            get;
            set;
        }

        public string BusinessFunction
        {
            get;
            set;
        }

        public string CaseIndex
        {
            get;
            set;
        }

        public string TestFolderID
        {
            get;
            set;
        }
    }

    public class TestStep
    {
        public string ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        public string LogicalDay
        {
            get;
            set;
        }

        public string ExpectedResult
        {
            get;
            set;
        }

        public string ResultType
        {
            get;
            set;
        }

        public string StepIndex
        {
            get;
            set;
        }

        public string Priority
        {
            get;
            set;
        }

        // ...

    }

    public class ActionParams
    {
        /*
  <ActionParams>
    <AdapterName>T24WebAdapter</AdapterName>
    <FinancialObject>[CUSTOMER Profile]:CUSTOMER</FinancialObject>
    <ForwardMappingSchema>No Value</ForwardMappingSchema>
    <GUIMappingSchema>No Value</GUIMappingSchema>
    <HttpCatalogue>No Value</HttpCatalogue>
    <HttpMappingSchema>No Value</HttpMappingSchema>
    <HttpRequestTypical>No Value</HttpRequestTypical>
    <ID>441640</ID>
    <IsHistoryBound>False</IsHistoryBound>
    <ReverseMappingSchema>No Value</ReverseMappingSchema>
    <TargetObject>T24WebAdapter</TargetObject>
    <FWMapSchemaCatalog/>
    <FWMapSchemaName/>
    <REVMapSchemaCatalog/>
    <REVMapSchemaName/>
  </ActionParams>
         */

        public string AdapterName
        {
            get;
            set;
        }

        public string FinancialObject
        {
            get;
            set;
        }

        public string ID
        {
            get;
            set;
        }

        // We don't need the other fields for now

    }

    public class DataRule
    {
        public string RuleType
        {
            get;
            set;
        }

        public string Operator
        {
            get;
            set;
        }

        // <Attribute>[Not Selected]</Attribute>
        public string Attribute
        {
            get;
            set;
        }

        public string ID
        {
            get;
            set;
        }
    }
}
