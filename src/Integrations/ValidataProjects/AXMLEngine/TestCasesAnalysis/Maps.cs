﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace TestCasesAnalysis
{
    public enum AttributeChange
    {
        Deleted,
        Added,
        Modified,
        Unchanged
    }

    public class MapBase
    {
        public static XmlNode GetChild(XmlNode parent, string name)
        {
            foreach (XmlNode node in parent.ChildNodes)
            {
                if (node.Name == name)
                    return node;
            }
            return null;
        }
    }

    public class AttributeMap : MapBase
    {
        public AttributeMap(XmlNode node)
        {
            Name = GetChild(node, "Attribute_x0020_Name").InnerText;
            TypicalID = GetChild(node, "TypicalID").InnerText;

            string status = GetChild(node, "Status").InnerText;
            switch (status)
            {
                case "Added":
                    ChangeType = AttributeChange.Added;
                    break;
                case "Unchanged":
                    ChangeType = AttributeChange.Unchanged;
                    break;
                case "Modified":
                    ChangeType = AttributeChange.Modified;
                    break;
                case "Deleted":
                    ChangeType = AttributeChange.Deleted;
                    break;
            }

            ID = GetChild(node, "ID").InnerText;
        }

        public static void AssignAttributeDetails(XmlNode node)
        {
            try
            {
                string id = GetChild(node, "AttributeID").InnerText;
                AttributeMap map = GetAttributeMap(id);
                if (map == null)
                    return;

                map.DataType = GetChild(node, "Data_x0020_Type").InnerText;
                map.DefaultStatus = GetChild(node, "Default_x0020_Status").InnerText;
                map.DataLength = GetChild(node, "Data_x0020_Length").InnerText;
            }
            catch (Exception)
            {
                // TODO why swallow the exception?
            }
        }

        public string DataType { get; set; }

        public string DefaultStatus { get; set; }

        public string DataLength { get; set; }

        public string TypicalID { get; set; }

        public static List<AttributeMap> AttributeMaps = new List<AttributeMap>();

        public string Name { get; set; }

        public AttributeChange ChangeType { get; set; }

        public string NewType { get; set; }

        public string ID { get; set; }

        public static AttributeMap GetAttributeMap(string id)
        {
            return AttributeMaps.FirstOrDefault(map => map.ID == id);
        }
    }

    public class TypicalMap : MapBase
    {
        public TypicalMap(XmlNode node)
        {
            Name = GetChild(node, "Typical_x0020_Name").InnerText;

            XmlNode catalogNode = GetChild(node, "Catalog_x0020_Name");
            if (catalogNode != null)
                CatalogName = catalogNode.InnerText;

            ID = GetChild(node, "ID").InnerText;
        }

        public static List<TypicalMap> TypicalMaps = new List<TypicalMap>();

        public string Name { get; set; }

        public string CatalogName { get; set; }

        public string ID { get; set; }

        public bool MapsToTypical(string catalogName, string typicalName)
        {
            return (Name == typicalName) && ((catalogName == null) || (CatalogName == null) || (CatalogName == catalogName));
        }

        public static TypicalMap GetTypicalMap(string id)
        {
            return TypicalMaps.FirstOrDefault(map => map.ID == id);
        }

        public AttributeChange GetChange(string attributeName)
        {
            foreach (AttributeMap attr in Attributes.Where(attr => attr.Name == attributeName))
            {
                return attr.ChangeType;
            }

            return AttributeChange.Unchanged;
        }

        public List<AttributeMap> Attributes = new List<AttributeMap>();

        public AttributeMap GetAttribute(string attribute)
        {
            return Attributes.FirstOrDefault(map => map.Name == attribute);
        }
    }
}