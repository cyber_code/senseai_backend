﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestCasesAnalysis
{
    public class TypicalRemap
    {

        public TypicalRemap(VxmlTypical source)
        {
            SourceTypical = source;
            FromCatalog = source.CatalogName;
            FromTypical = source.TypicalName;

            ToCatalog = source.CatalogName;
            ToTypical = source.TypicalName;

            TypicalMissing = source.TypicalMissing;
            CatalogMissing = source.CatalogMissing;

            _AttributeRemaps = new List<AttributeRemap>();
            foreach (var attrib in source.Attributes)
            {
                var attrRemap = new AttributeRemap
                                    {
                                        From = attrib,
                                        To = attrib,
                                        ParentFullTypicalName = FullName,
                                        Missing = false
                                    };

                if (source.MissingAttributes != null)
                    if (source.MissingAttributes.Contains(attrib))
                    {
                        attrRemap.Missing = true;
                    }

                if (source.TypicalMissing)
                {
                    attrRemap.Missing = true;
                }

                _AttributeRemaps.Add(attrRemap);
            }
        }

        public bool CatalogMissing
        {
            get; 
            set;
        }

        public bool TypicalMissing
        {
            get; 
            set;
        }

        private readonly List<AttributeRemap> _AttributeRemaps = new List<AttributeRemap>();

        public List<AttributeRemap> AttributeRemaps
        {
            get { return _AttributeRemaps; }
        }

        public VxmlTypical SourceTypical
        {
            get; set;
        }

        public string FromCatalog { get; set; }
        public string FromTypical { get; set; }

        public string ToCatalog { get; set; }
        public string ToTypical { get; set; }

        public string FullName
        {
            get { return string.Format("{0}:{1}", FromCatalog, FromTypical); }
        }

        public string FullNameTo
        {
            get { return string.Format("{0}:{1}", ToCatalog, ToTypical); }
        }
        

        public bool CatalogRemapped
        { 
            get
            {
                return FromCatalog != ToCatalog;
            }
        }

        public bool TypicalRemapped
        {
            get
            {
                return FromTypical != ToTypical;
            }
        }


        public bool CatalogOrTypicalRemapped
        {
            get { return CatalogRemapped || TypicalRemapped; }
        }

        public int AttributesRemapped
        {
            get 
            {
                return _AttributeRemaps.Count(attributeRemap => attributeRemap.IsRemapped);
            }
        }

        public string RemappedAttributes
        {
            get
            {
                var res = new StringBuilder();
                foreach(var attrib in AttributeRemaps)
                {
                    if (attrib.IsRemapped)
                    {
                        if (res.Length > 0)
                        {
                            res.Append(",");
                        }

                        res.Append(attrib.From);
                    }
                    
                }
                return res.ToString();
            }
        }

        public string RemapSummary
        {
            get
            {
                var result = new StringBuilder();
                if (CatalogRemapped)
                    result.Append("Catalog,");
                if (TypicalRemapped)
                    result.Append("Typical,");
                if (AttributesRemapped > 5)
                {
                    result.AppendFormat("{0} attributes,", AttributesRemapped);
                }
                else
                {
                    if (AttributesRemapped > 0)
                    {
                        result.Append("Attributes:");
                        result.Append(RemappedAttributes);
                        result.Append(",");
                    }
                }

                if (result.Length > 0)
                    result.Remove(result.Length - 1, 1);
                return result.ToString();
            }
        }

        public string Issues
        {
            get; set;
        }

        public AttributeRemap GetAttributeByFromName(string fromAttribute)
        {
            return _AttributeRemaps.FirstOrDefault(attrib => attrib.From == fromAttribute);
        }

        public IEnumerable<string> GetAttributeNames()
        {
            return AttributeRemaps.Select(attr => attr.To);
        }

        public int DuplicatedAttributes
        {
            get
            {
                return AttributeRemaps.Count(IsDuplicated);
            }
        }

        private bool IsDuplicated(AttributeRemap attr)
        {
            foreach (AttributeRemap curAttr in AttributeRemaps)
            {
                if (curAttr == attr)
                    continue;
                if (curAttr.To == attr.To)
                    return true;
            }
            return false;
        }
    }
}
