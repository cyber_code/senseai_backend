﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using ValidataCommon;

namespace TestCasesAnalysis
{
    public class VxmlTypical
    {
        public static List<ActionParams> ActionParamses = new List<ActionParams>();
        public static List<TestStep> TestSteps = new List<TestStep>();
        public static List<DataRule> DataRules = new List<DataRule>();

        public static Dictionary<string, List<Association>> AssocBySourceId = new Dictionary<string, List<Association>>();
        public static Dictionary<string, List<Association>> AssocByTargetId = new Dictionary<string, List<Association>>();

        public List<string> IDs = new List<string>();

        public static bool IgnoreMultiAttributes;

        public string CatalogName { get; set; }
        public string TypicalName { get; set; }
        public string GroupName { get; set; }
        public string Errors { get; set; }

        public bool CatalogMissing { get; set; }
        public bool TypicalMissing { get; set; }

        public TypicalRemap Remapping { get; set; }

        public List<string> Attributes = new List<string>();
        public List<string> MultiAttributes = new List<string>();

        public static string GetChildInnerTextSafe(XmlNode parent, string name)
        {
            var childNode = GetChild(parent, name);
            if (childNode == null)
                return "";

            return childNode.InnerText;
        }

        public static XmlNode GetChild(XmlNode parent, string name)
        {
            foreach (XmlNode node in parent.ChildNodes)
            {
                if (node.Name == name)
                    return node;
            }
            return null;
        }

        public static List<string> GetAssociatedID(string id1)
        {
            var result = new List<string>();

            List<Association> associations;
            if (AssocBySourceId.TryGetValue(id1, out associations))
            {
                associations.ForEach(n => result.Add(n.TargetID));
            }

            if (AssocByTargetId.TryGetValue(id1, out associations))
            {
                associations.ForEach(n => result.Add(n.SourceID));
            }

            return result;
        }

        public static TestStep GetTestStepByID(string id)
        {
            return TestSteps.FirstOrDefault(step => step.ID == id);
        }

        public VxmlTypical()
        {
        }

        public VxmlTypical(XmlNode node)
        {
            TypicalName = string.Empty;
            GroupName = string.Empty;
            TypicalName = XmlConvert.DecodeName(node.Name);

            if (TypicalName == "TestStep")
            {
                AnalyzeTestStep(node);
            }

            if (TypicalName == "Data Rule")

            {
                AnalyzeDataRule(node);
            }

            if (TypicalName == "ActionParams")
            {
                AnalyzeActionParameters(node);
            }

            if (TypicalName == "Association")
            {
                AnalyzeAssociation(node);
            }

            if ((TypicalName == "FilteringConstraint") || (TypicalName == "Automatic Calculation Item") || (TypicalName == "Automatic Calculation"))
            {
                AnalyzeAttribute(XmlConvert.DecodeName(GetChildInnerTextSafe(node, "AttributeName")), "Value");

                XmlNode typNode = GetChild(node, "TypicalName");
                XmlNode catNode = GetChild(node, "CatalogueName");

                if (typNode != null)
                    TypicalName = XmlConvert.DecodeName(typNode.InnerText);

                CatalogName = catNode != null
                    ? XmlConvert.DecodeName(catNode.InnerText)
                    : "[UNKNOWN]";

                return;
            }

            XmlAttribute catalogAttribute = node.Attributes["Catalog"];
            if (catalogAttribute != null)
            {
                CatalogName = catalogAttribute.Value;

                // analyze children - typical attributes
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    AnalyzeAttribute(childNode);
                }
            }
            else
            {
                CatalogName = "[UNKNOWN]";
            }

            XmlAttribute groupAttribute = node.Attributes["groupName"];
            GroupName = groupAttribute != null
                ? groupAttribute.Value
                : string.Empty;
        }

        private void AnalyzeActionParameters(XmlNode node)
        {
            var actionParam = new ActionParams
            {
                ID = GetChildInnerTextSafe(node, "ID"),
                AdapterName = GetChildInnerTextSafe(node, "AdapterName"),
                FinancialObject = GetChildInnerTextSafe(node, "FinancialObject")
            };

            ActionParamses.Add(actionParam);
        }

        private void AnalyzeAssociation(XmlNode node)
        {
            var assoc = new Association
            {
                ID = GetChildInnerTextSafe(node, "ID"),
                Name = GetChildInnerTextSafe(node, "Name"),
                Alias = GetChildInnerTextSafe(node, "Alias"),
                SourceID = GetChildInnerTextSafe(node, "SourceID"),
                TargetID = GetChildInnerTextSafe(node, "TargetID")
            };

            List<Association> associations;
            if (AssocBySourceId.TryGetValue(assoc.SourceID, out associations))
            {
                associations.Add(assoc);
            }
            else
            {
                AssocBySourceId[assoc.SourceID] = new List<Association> { assoc };
            }


            if (AssocByTargetId.TryGetValue(assoc.TargetID, out associations))
            {
                associations.Add(assoc);
            }
            else
            {
                AssocByTargetId[assoc.TargetID] = new List<Association> { assoc };
            }
        }

        private void AnalyzeDataRule(XmlNode node)
        {
            /*
              <Data_x0020_Rule>
                <RuleType>2</RuleType>
                <Operator>-1</Operator>
                <Attribute>[Not Selected]</Attribute>
                <ID>461968</ID>
              </Data_x0020_Rule>
             */

            var datarule = new DataRule
            {
                ID = GetChildInnerTextSafe(node, "ID"),
                RuleType = GetChildInnerTextSafe(node, "RuleType"),
                Operator = GetChildInnerTextSafe(node, "Operator"),
                Attribute = GetChildInnerTextSafe(node, "Attribute")
            };

            DataRules.Add(datarule);
        }

        private void AnalyzeTestStep(XmlNode node)
        {
            /*
              <TestStep CheckStatus="0">
                <ID>470925</ID>
                <Name>A. Close Page</Name>
                <Description>No Value</Description>
                <Type>1</Type>
                <LogicalDay>1</LogicalDay>
                <ExpectedResult>No Value</ExpectedResult>
                <ResultType>Generic FO</ResultType>
                <StepIndex>1</StepIndex>
                <Priority>2</Priority>
                <ExternalRef>No Value</ExternalRef>
                <TestStatus>0</TestStatus>
                <ManualOrAuto>2</ManualOrAuto>
                <EndCaseOnError>0</EndCaseOnError>
                <ExpectedFail>0</ExpectedFail>
                <Configuration>No Value</Configuration>
                <TestDataType>0</TestDataType>
                <Label>No Value</Label>
                <TestCaseID>470924</TestCaseID>
              </TestStep>
            */

            var testStep = new TestStep
            {
                ID = GetChildInnerTextSafe(node, "ID"),
                Name = GetChildInnerTextSafe(node, "Name"),
                Type = GetChildInnerTextSafe(node, "Type"),
                LogicalDay = GetChildInnerTextSafe(node, "LogicalDay"),
                ExpectedResult = GetChildInnerTextSafe(node, "ExpectedResult"),
                ResultType = GetChildInnerTextSafe(node, "ResultType")
            };


            TestSteps.Add(testStep);
        }

        private const string NO_VALUE = "No Value";

        private void AnalyzeAttribute(XmlNode childNode)
        {
            AnalyzeAttribute(XmlConvert.DecodeName(childNode.Name), childNode.InnerText);
        }

        private void AnalyzeAttribute(string attributeName, string attributeValue)
        {
            if (attributeName == "" || attributeValue == NO_VALUE || attributeValue == "") return;

            if (attributeName == "ID")
            {
                ID = attributeValue;
                IDs.Add(ID);
            }

            string mValue = CheckMultiValue(attributeName);
            if (mValue == null)
            {
                Attributes.Add(attributeName);
            }
            else
            {
                if (!MultiAttributes.Contains(mValue))
                {
                    MultiAttributes.Add(mValue);
                    Attributes.Add(mValue);
                }
            }
        }

        protected static string CheckMultiValue(string attributeName)
        {
            if (IgnoreMultiAttributes)
                return null;

            int index = attributeName.IndexOf("-");
            int index2 = attributeName.IndexOf("~");
            if ((index > 0) && (index2 > 0) && (index2 > index))
                return attributeName.Substring(0, index) + "-1~1";

            return null;
        }

        public void AddAttributes(VxmlTypical typicalAbs)
        {
            foreach (string attribute in typicalAbs.Attributes)
            {
                if (!Attributes.Contains(attribute))
                {
                    Attributes.Add(attribute);
                }

                if (!IDs.Contains(ID))
                {
                    IDs.Add(ID);
                }
            }
        }

        public string ID { get; set; }

        public List<string> MissingAttributes
        {
            get; set;
        }

        public static void AnalyzeDataRules(List<VxmlTypical> typicals)
        {
            foreach (DataRule rule in DataRules)
            {
                AnaylzeDataRule(rule, typicals);
            }
        }

        internal static ActionParams GetActionParam(string id)
        {
            return ActionParamses.FirstOrDefault(param => param.ID == id);
        }

        private static void AnaylzeDataRule(DataRule rule, List<VxmlTypical> typicals)
        {
            string attribute = rule.Attribute;
            if (attribute == "[Not Selected]")
                return;

            List<string> assoc = GetAssociatedID(rule.ID);
            foreach (string id in assoc)
            {
                var testStep = GetTestStepByID(id);
                if (testStep != null)
                {
                    string typicalName = GetTypical(testStep.ResultType);
                    string catalogName = GetCatalog(testStep.ResultType);

                    if (testStep.ResultType == string.Empty)
                    {
                        List<string> testStepAssocs = GetAssociatedID(testStep.ID);
                        foreach (string assocId in testStepAssocs)
                        {
                            var ap = GetActionParam(assocId);
                            if (ap != null)
                            {
                                typicalName = GetTypical(ap.FinancialObject);
                                catalogName = GetCatalog(ap.FinancialObject);
                                break;
                            }
                        }
                    }

                    if ((typicalName == string.Empty) && (catalogName == string.Empty))
                        return;

                    foreach (VxmlTypical typical in typicals)
                    {
                        if ((typical.TypicalName == typicalName) && (typical.CatalogName == catalogName))
                        {
                            attribute = ConvertMultiValue(attribute);
                            if (attribute != null)
                                if (!typical.Attributes.Contains(attribute))
                                {
                                    typical.Attributes.Add(attribute);
                                }
                            return;
                        }
                    }
                }
            }
        }

        private static string ConvertMultiValue(string attribute)
        {
            string mValue = CheckMultiValue(attribute);
            return mValue;
        }

        private static string GetCatalog(string resultType)
        {
            string catalog;
            string typical;

            ActionParamHelper.SplitCatalogAndEntitylNames(resultType, out catalog, out typical);
            return catalog;
        }

        private static string GetTypical(string resultType)
        {
            string catalog;
            string typical;

            ActionParamHelper.SplitCatalogAndEntitylNames(resultType, out catalog, out typical);
            return typical;
        }
    }

    public class VxmlTypicalComparer : IComparer<VxmlTypical>
    {
        public int Compare(VxmlTypical x, VxmlTypical y)
        {
            bool xUnk = x.CatalogName.StartsWith("[U");
            bool yUnk = y.CatalogName.StartsWith("[U");

            if (!((xUnk) && (yUnk)))
            {
                if (xUnk)
                    return 1;
                if (yUnk)
                    return -1;
            }

            int result = x.CatalogName.CompareTo(y.CatalogName);
            if (result != 0)
                return result;

            result = x.TypicalName.CompareTo(y.TypicalName);
            if (result != 0)
                return result;

            result = x.GroupName.CompareTo(y.GroupName);
            return result;
        }
    }

    public class Association
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string SourceID { get; set; }
        public string TargetID { get; set; }
    }

    public class TestCase
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string CreateDate { get; set; }
        public string BusinessFunction { get; set; }
        public string CaseIndex { get; set; }
        public string TestFolderID { get; set; }
    }

    public class TestStep
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string LogicalDay { get; set; }
        public string ExpectedResult { get; set; }
        public string ResultType { get; set; }
        public string StepIndex { get; set; }
        public string Priority { get; set; }
        // ...
    }

    public class ActionParams
    {
        /*
  <ActionParams>
    <AdapterName>T24WebAdapter</AdapterName>
    <FinancialObject>[CUSTOMER Profile]:CUSTOMER</FinancialObject>
    <ForwardMappingSchema>No Value</ForwardMappingSchema>
    <GUIMappingSchema>No Value</GUIMappingSchema>
    <HttpCatalogue>No Value</HttpCatalogue>
    <HttpMappingSchema>No Value</HttpMappingSchema>
    <HttpRequestTypical>No Value</HttpRequestTypical>
    <ID>441640</ID>
    <IsHistoryBound>False</IsHistoryBound>
    <ReverseMappingSchema>No Value</ReverseMappingSchema>
    <TargetObject>T24WebAdapter</TargetObject>
    <FWMapSchemaCatalog/>
    <FWMapSchemaName/>
    <REVMapSchemaCatalog/>
    <REVMapSchemaName/>
  </ActionParams>
         */

        public string AdapterName { get; set; }
        public string FinancialObject { get; set; }
        public string ID { get; set; }

        // We don't need the other fields for now
    }

    public class DataRule
    {
        public string RuleType { get; set; }
        public string Operator { get; set; }

        // <Attribute>[Not Selected]</Attribute>
        public string Attribute { get; set; }

        public string ID { get; set; }
    }
}
