﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestCasesAnalysis
{
    public class AttributeRemap
    {
        public AttributeRemap()
        {
            Missing = false;
        }

        public string ParentFullTypicalName
        {
            get; 
            set;
        }

        public string From { get; set; }
        public string To { get; set; }

        public bool IsRemapped
        {
            get { return From != To; }
        }

        public bool Missing
        {
            get; set;
        }
    }
}
