﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using ValidataCommon;

namespace TestCasesAnalysis
{
    public static class RemapHelper
    {
        private static Dictionary<XmlNode, XmlNode> _NodesToReplace = new Dictionary<XmlNode, XmlNode>();

        public static Dictionary<XmlNode, XmlNode> NodesToReplace
        {
            get { return _NodesToReplace; }
        }

        public static void RenameTypicalInNode(XmlNode node, string fromCatalog, string fromTypical, string toCatalog, string toTypical)
        {
            RenameTypicalInNode(node, fromCatalog, fromTypical, toCatalog, toTypical, null);
        }

        internal static void RenameTypicalInNode(XmlNode node, string fromCatalog, string fromTypical, string toCatalog, string toTypical, TypicalRemap remap)
        {
            switch (node.Name)
            {
                case "ActionParams":
                    {
                        if ((fromCatalog != toCatalog) || (fromTypical != toTypical))
                            RenameFullTypicalNameInText(node, "FinancialObject", fromCatalog, fromTypical, toCatalog, toTypical);
                        break;
                    }
                case "TestStep":
                    {
                        if ((fromCatalog != toCatalog) || (fromTypical != toTypical))
                            RenameFullTypicalNameInText(node, "ResultType", fromCatalog, fromTypical, toCatalog, toTypical);
                        break;
                    }
                case "FilteringConstraint":
                    {
                        HandleAttributeRemaps(node, remap);

                        if ((fromCatalog != toCatalog) || (fromTypical != toTypical))
                            RenameCatalogAndTypicalInNodes(VxmlTypical.GetChild(node, "CatalogueName"),
                                                    VxmlTypical.GetChild(node, "TypicalName"), fromCatalog, fromTypical,
                                                    toCatalog, toTypical);
                        break;
                    }
                case "Automatic_x0020_Calculation_x0020_Item":
                    {
                        HandleAttributeRemaps(node, remap);

                        if ((fromCatalog != toCatalog) || (fromTypical != toTypical))
                            RenameCatalogAndTypicalInNodes(VxmlTypical.GetChild(node, "CatalogueName"),
                                                    VxmlTypical.GetChild(node, "TypicalName"), fromCatalog, fromTypical,
                                                    toCatalog, toTypical);
                        break;
                    }
                case "Data_x0020_Pool_x0020_Item":
                    {
                        HandleAttributeRemaps(node, remap);

                        if ((fromCatalog != toCatalog) || (fromTypical != toTypical))
                            RenameFullTypicalNameInText(node, "FinancialObject", fromCatalog, fromTypical, toCatalog, toTypical);
                        break;
                    }
                case "Automatic_x0020_Calculation":
                    {
                        AutomaticCalculationAttributeRemaps(node, remap);

                        if ((fromCatalog != toCatalog) || (fromTypical != toTypical))
                            RenameCatalogAndTypicalInNodes(VxmlTypical.GetChild(node, "CatalogueName"),
                                                   VxmlTypical.GetChild(node, "TypicalName"), fromCatalog, fromTypical,
                                                   toCatalog, toTypical);
                        break;
                    }
                default:
                    {
                        RenameTypicalInNodeNameAndAttribute(node, fromCatalog, fromTypical, toCatalog, toTypical, remap);

                        break;
                    }
            }
        }

        private static void HandleAttributeRemaps(XmlNode node, TypicalRemap remap)
        {
            var catalogName = VxmlTypical.GetChild(node, "CatalogueName");
            var typicalName = VxmlTypical.GetChild(node, "TypicalName");
            var attributeName = VxmlTypical.GetChild(node, "AttributeName");

            if ((catalogName == null) || (typicalName == null) || (attributeName == null))
            {
                return;
            }

            string catName = XmlConvert.DecodeName(catalogName.InnerText);
            string typName = XmlConvert.DecodeName(typicalName.InnerText);
            string atrName = XmlConvert.DecodeName(attributeName.InnerText);

            if ((remap.FromCatalog == catName) && (remap.FromTypical == typName))
            {
                foreach(var attrRemap in remap.AttributeRemaps)
                {
                    if (attrRemap.IsRemapped)
                    {
                        if (atrName == attrRemap.From)
                        {
                            attributeName.InnerText = XmlConvert.EncodeName(attrRemap.To);
                        }
                    }
                }
            }
        }

        private static void AutomaticCalculationAttributeRemaps(XmlNode node, TypicalRemap remap)
        {
            var attributeName = VxmlTypical.GetChild(node, "AttributeName");
            string atrName = XmlConvert.DecodeName(attributeName.InnerText);

            foreach (var attrRemap in remap.AttributeRemaps)
            {
                if (attrRemap.IsRemapped)
                {
                    //node.InnerText = XmlConvert.EncodeName(attrRemap.To);
                    if (atrName == attrRemap.From)
                    {
                        attributeName.InnerText = XmlConvert.EncodeName(attrRemap.To);
                    }
                }
            }
        }

        private static void RemapAttributes(XmlNode typicalNode, List<AttributeRemap> attributeRemaps)
        {
            var _NodesToRenameWithValues = new Dictionary<XmlNode, KeyValuePair<string, string>>();
            XmlDocument doc = typicalNode.OwnerDocument;

            foreach (XmlNode child in typicalNode.ChildNodes)
            {
                foreach (AttributeRemap remap in attributeRemaps)
                {
                    if (remap.From == XmlConvert.DecodeName(child.Name))
                    {
                        var nv = new KeyValuePair<string, string>(remap.To, child.InnerText);
                        _NodesToRenameWithValues.Add(child, nv);
                    }
                }
            }

            foreach (XmlNode attrToRename in _NodesToRenameWithValues.Keys)
            {
                KeyValuePair<string, string> nv = _NodesToRenameWithValues[attrToRename];

                XmlNode newNode = doc.CreateNode(XmlNodeType.Element, XmlConvert.EncodeName(nv.Key), GetDefaultURI(doc));

                newNode.InnerText = nv.Value;
                typicalNode.InsertAfter(newNode, attrToRename);
                typicalNode.RemoveChild(attrToRename);
            }
        }

        private static void RenameCatalogAndTypicalInNodes(XmlNode catalogNode, XmlNode typicalNode, string fromCatalog, string fromTypical, string toCatalog, string toTypical)
        {
            if ((catalogNode != null && typicalNode != null) && (XmlConvert.DecodeName(catalogNode.InnerText) == fromCatalog) && (XmlConvert.DecodeName(typicalNode.InnerText) == fromTypical))
            {
                catalogNode.InnerText = XmlConvert.EncodeName(toCatalog);
                typicalNode.InnerText = XmlConvert.EncodeName(toTypical);
            }
        }

        private static void RenameFullTypicalNameInText(XmlNode node, string childName, string fromCatalog, string fromTypical, string toCatalog, string toTypical)
        {
            XmlNode childNode = VxmlTypical.GetChild(node, childName);
            if (childNode == null)
            {
                return;
            }

            childNode.InnerText = GetTextToRenameFullTypical(childNode.InnerText, fromCatalog, fromTypical, toCatalog, toTypical);
        }

        private static string GetTextToRenameFullTypical(string catEntityText, string fromCatalog, string fromTypical, string toCatalog, string toTypical)
        {
            if (string.IsNullOrEmpty(catEntityText) || catEntityText == "No Value")
            {
                return catEntityText;
            }

            string catalogName, entityName;
            if (!ActionParamHelper.SplitCatalogAndEntitylNames(catEntityText, out catalogName, out entityName))
            {
                return catEntityText;
            }

            if ((fromCatalog != catalogName) || (fromTypical != entityName))
            {
                return catEntityText;
            }

            return ActionParamHelper.CombineCatalogAndEntityNames(toCatalog, toTypical);
        }

        private static void RenameTypicalInNodeNameAndAttribute(XmlNode node, string fromCatalog, string fromTypical, string toCatalog, string toTypical)
        {
            RenameTypicalInNodeNameAndAttribute(node, fromCatalog, fromTypical, toCatalog, toTypical, null);
        }

        private static void RenameTypicalInNodeNameAndAttribute(XmlNode node, string fromCatalog, string fromTypical, string toCatalog, string toTypical, TypicalRemap typicalRemap)
        {
            if (node.Attributes == null)
                return;

            XmlAttribute catAttribute = node.Attributes["Catalog"];
            if (catAttribute == null)
            {
                return;
            }

            string catalogName = catAttribute.Value;
            string typicalName = XmlConvert.DecodeName(node.Name);

            XmlDocument doc = node.OwnerDocument;
            if (doc == null)
                return;

            if (typicalRemap != null)
                if ((catalogName == typicalRemap.FromCatalog) && (typicalName == typicalRemap.FromTypical))
                {
                    if (typicalRemap.AttributesRemapped > 0)
                    {
                        RemapAttributes(node, typicalRemap.AttributeRemaps);
                    }
                }

            if ((catalogName == fromCatalog) && (typicalName == fromTypical))
            {
                if ((catalogName != toCatalog) && (typicalName == toTypical))
                {
                    catAttribute.Value = toCatalog;
                }
                else
                {
                    XmlNode parent = node.ParentNode;
                    if (parent == null)
                        return;

                    if (toTypical == null)
                        return;

                    var newNode = doc.CreateNode(XmlNodeType.Element, XmlConvert.EncodeName(toTypical), GetDefaultURI(doc));
                    if (newNode.Attributes == null)
                        return;

                    foreach (XmlAttribute attr in node.Attributes)
                    {
                        var attrib2 = attr.Clone() as XmlAttribute;
                        XmlAttribute newAttribute = newNode.Attributes.Append(attrib2);
                        if (newAttribute.Name == "Catalog")
                            newAttribute.Value = toCatalog;
                    }

                    foreach (XmlNode child in node.ChildNodes)
                    {
                        newNode.AppendChild(child.Clone());
                    }

                    _NodesToReplace.Add(node, newNode);
                }
            }
        }

        private static string GetDefaultURI(XmlDocument doc)
        {
            return doc.ChildNodes[doc.ChildNodes.Count - 1].NamespaceURI;
        }

        public static void RenameTypicalsInNode(XmlNode node, List<TypicalRemap> remappings)
        {
            foreach(TypicalRemap remap in remappings)
            {
                RenameTypicalInNode(node, remap.FromCatalog, remap.FromTypical, remap.ToCatalog, remap.ToTypical, remap);
            }
        }
    }
}
