using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Xml;
using ValidataCommon;

namespace AXMLEngine
{
    /// <summary>
    /// An instance is a lightweigh collection of attribute names and values. 
    /// It provides fast search by attibute name, but also preserves the order of the attributes
    /// </summary>
    [Serializable]
    public class Instance : IValidataRecord
    {
        #region Private And Protected Members

        /// <summary>
        /// Attribute ID name
        /// </summary>
        private const string ATTR_ID_NAME = "ID";

        /// <summary>
        /// Instance ID
        /// </summary>
        private string _ID;

        /// <summary>
        /// Instance Typical Name
        /// </summary>
        private string _TypicalName;

        /// <summary>
        /// Instance Typical Attributes
        /// </summary>
        private InstanceAttributesCollection _Attributes = new InstanceAttributesCollection();

        /// <summary>
        /// Dictionairy of Instance Typical Attribute accessible by Name
        /// </summary>
        private readonly Dictionary<string, InstanceAttribute> _AttributesByName = new Dictionary<string, InstanceAttribute>();

        /// <summary>
        /// Additional properties, stored as XML attributes in the instances tag.
        /// </summary>
        private readonly Dictionary<string, string> _AdditionalProperties = new Dictionary<string, string>();

        #endregion

        #region Public Properties

        /// <summary>
        /// Instance ID
        /// </summary>
        public string ID
        {
            get { return _ID; }
        }

        /// <summary>
        /// Instance Typical Name
        /// </summary>
        public string TypicalName
        {
            get { return _TypicalName; }
            set { _TypicalName = value; }
        }

        /// <summary>
        /// Instance Catalog Name
        /// </summary>
        public string Catalog
        {
            get { return GetAdditionalPropertyValue(AXMLConstants.INSTANCE_ATTRIBUTE_CATALOG); }
            set { SetAdditionalPropertyValue(AXMLConstants.INSTANCE_ATTRIBUTE_CATALOG, value); }
        }

        /// <summary>
        /// Used for T24 instances
        /// </summary>
        public string CurrNo
        {
            get { return GetAdditionalPropertyValue("VersionNo"); }
            set { SetAdditionalPropertyValue("VersionNo", value); }
        }

        /// <summary>
        /// Used for Seapine Defects import.
        /// </summary>
        public string Item
        {
            set { SetAdditionalPropertyValue("Item", value); }
        }

        /// <summary>
        /// Instance Typical Attributes
        /// </summary>
        public InstanceAttributesCollection Attributes
        {
            get { return _Attributes; }
            set { _Attributes = value; }
        }

        #endregion

        #region IRecord Members

        /// <summary>
        /// Gets name of the catalog
        /// </summary>
        public string CatalogName
        {
            get { return this.Catalog; }
            set { this.Catalog = value; }
        }

        IEnumerable<IAttribute> IValidataRecord.Attributes
        {
            get { return this.Attributes; }
        }

        public IAttribute FindAttribute(string name)
        {
            // return this.Attributes.Find(name);
            return AttributeByName(name);
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Instance()
        {
        }

        /// <summary>
        /// Additional Constructor
        /// </summary>
        /// <param name="typicalName">Instance Typical name</param>
        /// <param name="catalogName">Typical Catalog name</param>
        public Instance(string typicalName, string catalogName)
        {
            _TypicalName = typicalName;
            Catalog = catalogName;
        }

        /// <summary>
        /// Instanciate copy from source
        /// </summary>
        /// <param name="src">Source record</param>
        public Instance(IValidataRecord src)
        {
            this._TypicalName = src.TypicalName;
            this.Catalog = src.CatalogName;

            foreach (var srcAttr in src.Attributes)
            {
                this.AddAttribute(srcAttr.Name, srcAttr.Value);
            }
        }

        #endregion

        #region Public Methods

        public Instance CloneInstance()
        {
            Instance newInstance = new Instance(this.TypicalName, this.Catalog);
            foreach (InstanceAttribute ia in this.Attributes)
            {
                newInstance.AddAttribute(ia.Name, ia.Value);
            }
            newInstance._ID = this.ID;

            return newInstance;
        
        }

        private const string T24ID_ATTRIBUTE_NAME = "@ID";

        public string T24Id
        {
            get
            {
                return GetAttributeValue(T24ID_ATTRIBUTE_NAME);
            }
        }

        /// <summary>
        /// Adds Instance Attribute to attributes collection
        /// </summary>
        /// <param name="name">Instance Attribute Name</param>
        /// <param name="val">Instance Attribute Value</param>
        /// <remarks>Warning: the passed attribute name is Xml-decoded, and the passed value is Html-decoded</remarks>
        ///  <returns>If succeeded returns true, otherwise - false</returns>
        public bool AddAttribute(string name, string val)
        {
            if (name == null || val == null)
            {
                return false;
            }

            // TODO IIvan - this is quite an unexpected side effect (see my e-mail from Thu 16/08/2012 16:17 "warning - potentially dangerous method in AXMLEngine.Instance class")
            name = XmlConvert.DecodeName(name);
            val = Utils.GetHtmlDecodedValue(val);

            InstanceAttribute iAttr = new InstanceAttribute(name, val);
            _Attributes.Add(iAttr);
            _AttributesByName[name] = iAttr;

            if (name == ATTR_ID_NAME)
            {
                _ID = val;
            }

            return true;
        }

        /// <summary>
        /// Adds instance attribute to attributes collection
        /// </summary>
        /// <param name="instanceAttr">Instance attribute</param>
        /// <returns>If succeeded returns true, otherwise - false</returns>
        public bool AddAttribute(InstanceAttribute instanceAttr)
        {
            if (instanceAttr == null)
            {
                return false;
            }

            _Attributes.Add(instanceAttr);

            string name = XmlConvert.DecodeName(instanceAttr.Name);

            _AttributesByName[name] = instanceAttr;

            if (instanceAttr.Name == ATTR_ID_NAME)
            {
                _ID = instanceAttr.Value;
            }

            return true;
        }

        /// <summary>
        /// Retrieves attribute value by supplied attribute name
        /// </summary>
        /// <param name="attributeName">Attribute Name</param>
        /// <returns>The value of the XML attribute</returns>
        public string GetAttributeValue(string attributeName)
        {
            InstanceAttribute attr = AttributeByName(attributeName);
            return attr != null ? attr.Value : String.Empty;
        }


        /// <summary>
        /// Gets the attribute value.
        /// </summary>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <param name="tryExpandedAttributeNameIfExactMatchIsMissing">if set to <c>true</c> [try expanded attribute name if exact match is missing].</param>
        /// <returns></returns>
        public string GetAttributeValue(string attributeName, bool tryExpandedAttributeNameIfExactMatchIsMissing)
        {
            InstanceAttribute attr = tryExpandedAttributeNameIfExactMatchIsMissing 
                ? GetAttributeByShortName(attributeName) 
                : AttributeByName(attributeName);

            return attr != null ? attr.Value : String.Empty;
        }

        /// <summary>
        /// Gets an attribute of this instance by its name.
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public InstanceAttribute AttributeByName(string attributeName)
        {
            if (_AttributesByName.ContainsKey(attributeName))
            {
                return _AttributesByName[attributeName];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the attribute by its short name (if it does not exist try the multivalue name, i.e. -1~1
        /// </summary>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <returns></returns>
         public InstanceAttribute GetAttributeByShortName(string attributeName)
         {
             InstanceAttribute result = AttributeByName(attributeName);
             if(result == null)
             {
                 result = AttributeByName(attributeName + "-1~1");
             }

            return result;
         }

        /// <summary>
        /// Sets attribute value
        /// </summary>
        /// <param name="attributeName">Attribute Name</param>
        /// <param name="val">Value</param>
        public bool SetAttributeValue(string attributeName, string val)
        {
            InstanceAttribute attr = AttributeByName(attributeName);

            if (attr != null)
            {
                attr.Value = val;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Writes the instance and its attributes to the output XML.
        /// </summary>
        /// <param name="xmlWriter"></param>
        public void ToXML(XmlWriter xmlWriter)
        {
            ToXML(xmlWriter, false);
        }

        /// <summary>
        /// Writes the instance and its attributes to the output XML.
        /// </summary>
        /// <param name="xmlWriter"></param>
        /// <param name="skipEmpty">skip empty attributes</param>
        public void ToXML(XmlWriter xmlWriter, bool skipEmpty)
        {
            xmlWriter.WriteStartElement(XmlConvert.EncodeName(TypicalName));

            // write other XML attributes that may have been added to this instance
            foreach (string xmlAttr in _AdditionalProperties.Keys)
            {
                string xmlAttrVal = _AdditionalProperties[xmlAttr];
                if (xmlAttrVal.Trim().Length > 0)
                {
                    xmlWriter.WriteAttributeString(xmlAttr, xmlAttrVal);
                }
            }

            // write instance attributes
            foreach (InstanceAttribute attr in Attributes)
            {
                if (skipEmpty)
                if (string.IsNullOrEmpty(attr.Value) || attr.Value == "No Value")
                {
                    continue;
                }

                attr.ToXML(xmlWriter);
            }

            xmlWriter.WriteFullEndElement();
        }

        /// <summary>
        /// Retrieve Xml string representation of the instance
        /// </summary>
        /// <returns></returns>
        public string ToXmlString()
        {
            return ToXmlString(false);
        }

        /// <summary>
        /// Retrieve Xml string representation of the instance
        /// </summary>
        /// <param name="skipEmptyValues">Skip empty values</param>
        /// <returns></returns>
        public string ToXmlString(bool skipEmptyValues)
        {
            using (StringWriter sw = new StringWriter())
            {
                using (XmlWriter xmlWriter = new SafeXmlTextWriter(sw))
                {
                    ToXML(xmlWriter, skipEmptyValues);
                }

                return sw.ToString();
            }
        }

        /// <summary>
        /// Initializes an instance from an Xml string
        /// </summary>
        /// <param name="vxml">The vxml.</param>
        /// <returns></returns>
        public static Instance FromXmlString(string vxml)
        {
            using (TextReader textReader = new StringReader(vxml))
            {
                using (XmlReader xmlReader = XmlReader.Create(textReader))
                {
                    return FromXML(xmlReader);
                }
            }
        }

        /// <summary>
        /// Initializes an instance from an Xml Reader, positioned at the start of the typical
        /// </summary>
        /// <param name="xmlReader">The XML reader.</param>
        /// <returns></returns>
        public static Instance FromXML(XmlReader xmlReader)
        {
            if (xmlReader.NodeType != XmlNodeType.Element)
            {
                xmlReader.Read();
            }

            string catalogName = XmlConvert.DecodeName(xmlReader.GetAttribute(AXMLConstants.INSTANCE_ATTRIBUTE_CATALOG));
            string typicalName = XmlConvert.DecodeName(xmlReader.Name);

            Instance currentInstance = new Instance(typicalName, catalogName);
            currentInstance.ReadXmlAttributes(xmlReader);
            currentInstance.ReadAttributes(xmlReader);

            return currentInstance;
        }

        /// <summary>
        /// Reads the XML attributes attached to the Instance node.
        /// </summary>
        /// <param name="xmlReader">The XmlReader positioned on the instance XML node.</param>
        private void ReadXmlAttributes(XmlReader xmlReader)
        {
            if (xmlReader.HasAttributes)
            {
                xmlReader.MoveToFirstAttribute();
                for (int i = 0; i < xmlReader.AttributeCount; i++)
                {
                    SetAdditionalPropertyValue(xmlReader.Name, xmlReader.Value);
                    xmlReader.MoveToNextAttribute();
                }
            }
        }

        /// <summary>
        /// Reads the attributes.
        /// </summary>
        /// <param name="xmlReader">The XML reader.</param>
        private void ReadAttributes(XmlReader xmlReader)
        {
            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == XmlNodeType.Element)
                {
                    string attrName = XmlConvert.DecodeName(xmlReader.Name);
                    string attrValue = HttpUtility.HtmlDecode(xmlReader.ReadString());
                    AddAttribute(attrName, attrValue);
                }
                else if (xmlReader.NodeType == XmlNodeType.EndElement)
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Checks if the values equal.
        /// </summary>
        /// <param name="currValue">The curr value.</param>
        /// <param name="otherValue">The other value.</param>
        /// <returns></returns>
        private static bool AreValuesEqual(string currValue, string otherValue)
        {
            if (currValue == string.Empty || currValue == "No Value")
            {
                currValue = null;
            }

            if (otherValue == string.Empty || otherValue == "No Value")
            {
                otherValue = null;
            }

            return (currValue==otherValue);
        }

        /// <summary>
        /// Adds an additional property (such as Catalog)
        /// </summary>
        /// <param name="attributeName">The name of the XML attribute</param>
        /// <param name="attributeValue">The value of the XML attribute</param>
        public void SetAdditionalPropertyValue(string attributeName, string attributeValue)
        {
            if (attributeValue == null)
            {
                _AdditionalProperties.Remove(attributeName);
            }
            else
            {
                _AdditionalProperties[attributeName] = attributeValue;
            }
        }

        /// <summary>
        /// Gets the value of the additional property
        /// </summary>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <returns></returns>
        public string GetAdditionalPropertyValue(string attributeName)
        {
            if (_AdditionalProperties.ContainsKey(attributeName))
            {
                return _AdditionalProperties[attributeName];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Reorder Instance Attributes according Typical
        /// </summary>
        /// <param name="typical"></param>
        /// <param name="useTypicalAttributeOrder"></param>
        public void ReorderAttributes(MetadataTypical typical, bool useTypicalAttributeOrder)
        {
            InstanceAttributesCollection sortedAttributes = new InstanceAttributesCollection();

            if (useTypicalAttributeOrder && (typical != null))
            {
                List<string> attributesFromVersion = new List<string>();

                if (typical.HasVersion)
                {
                    for (int i = 0; i < typical.VersionAttributes.Count; i++)
                    {
                        string typAttributeName = typical.VersionAttributes[i].Trim();
                        if (!string.IsNullOrEmpty(typAttributeName))
                        {
                            InstanceAttribute newAttribute = AttributeByName(typAttributeName);
                            if (newAttribute != null)
                            {
                                sortedAttributes.Add(newAttribute);
                                attributesFromVersion.Add(typAttributeName);
                            }
                        }
                    }
                }

                int typAttributesCount = typical.Attributes.Count;

                for (int i = 0; i < typAttributesCount; i++)
                {
                    string typAttributeName = typical.Attributes[i].Name.Trim();
                    if (typAttributeName != String.Empty && !attributesFromVersion.Contains(typAttributeName))
                    {
                        InstanceAttribute newAttribute = AttributeByName(typAttributeName);
                        if (newAttribute != null)
                        {
                            sortedAttributes.Add(newAttribute);
                        }
                    }
                }
            }
            else
            {
                SortedDictionary<string, InstanceAttribute> dictAttributtes = new SortedDictionary<string, InstanceAttribute>();

                for (int i = 0; i < _Attributes.Count; i++)
                {
                    string attributeName = _Attributes[i].Name.Trim();
                    if (attributeName != String.Empty)
                    {
                        InstanceAttribute newAttribute = AttributeByName(attributeName);
                        if (newAttribute != null)
                        {
                            dictAttributtes.Add(attributeName, newAttribute);
                        }
                    }
                }

                foreach (KeyValuePair<string, InstanceAttribute> kvp in dictAttributtes)
                {
                    sortedAttributes.Add(kvp.Value);
                }
            }

            _Attributes = sortedAttributes;
        }

        /// <summary>
        /// Serializes the instance to CSV.
        /// </summary>
        /// <returns></returns>
        public string ToCSV()
        {
            return CsvInstanceConvertor.FromInstance(this, true);
        }

        /// <summary>
        /// Serializes the instance to CSV.
        /// </summary>
        /// <param name="includeHeaders">Include or Not Headers (Attribute Names)</param>
        /// <returns></returns>
        public string ToCSV(bool includeHeaders)
        {
            return CsvInstanceConvertor.FromInstance(this, includeHeaders);
        }

        /// <summary>
        /// Serializes the instance to CSV.
        /// </summary>
        /// <param name="separator">The separator.</param>
        /// <returns></returns>
        public string ToCSV(char separator)
        {
            return CsvInstanceConvertor.FromInstance(this, true, separator);
        }

        /// <summary>
        /// Serializes the instance to CSV.
        /// </summary>
        /// <param name="includeHeaders">Include or Not Headers (Attribute Names)</param>
        /// <param name="separator">Custom Separator</param>
        /// <returns></returns>
        public string ToCSV(bool includeHeaders, char separator)
        {
            return CsvInstanceConvertor.FromInstance(this, includeHeaders, separator);
        }

        public bool IsEqual(Instance otherInstance)
        {
            if (Catalog != otherInstance.Catalog
                && (!string.IsNullOrEmpty(Catalog) || !string.IsNullOrEmpty(otherInstance.Catalog)))
            {
                return false;
            }

            if (TypicalName != otherInstance.TypicalName)
            {
                return false;
            }

            List<string> attributeNames = new List<string>();
            foreach (string attrName in _AttributesByName.Keys)
            {
                attributeNames.Add(attrName);
            }
            foreach (string attrName in otherInstance._AttributesByName.Keys)
            {
                if (!attributeNames.Contains(attrName))
                {
                    attributeNames.Add(attrName);
                }
            }

            foreach (string attrName in attributeNames)
            {
                if (attrName == "ID")
                {
                    continue;
                }

                string currValue = ((_AttributesByName.ContainsKey(attrName))
                                        ?
                                            _AttributesByName[attrName].Value
                                        :
                                            null
                                   );

                string otherValue = ((otherInstance._AttributesByName.ContainsKey(attrName))
                                         ?
                                             otherInstance._AttributesByName[attrName].Value
                                         :
                                             null
                                    );

                if (!AreValuesEqual(currValue, otherValue))
                {
                    return false;
                }
            }

            return true;
        }

        public void RemoveAttribute(string attributeName)
        {
            if (_AttributesByName.ContainsKey(attributeName))
            {
                // remove both from list and from dictionary
                foreach (InstanceAttribute ia in _Attributes)
                {
                    if (ia.Name == attributeName)
                    {
                        _Attributes.Remove(ia);
                        break;
                    }
                }

                _AttributesByName.Remove(attributeName);
            }
        }

        public string[] GetAttributeNames()
        {
            List<string> attrNames = new List<string>(_Attributes.Count);
            foreach(InstanceAttribute attr in _Attributes)
            {
                attrNames.Add(attr.Name);
            }

            return attrNames.ToArray();
        }



        public List<InstanceAttribute> GetSimpleAttributes()
        {
            List<InstanceAttribute> simpleAttributes = new List<InstanceAttribute>();
            foreach (InstanceAttribute attr in Attributes)
            {
                if (! AttributeNameParser.IsMultiValue(attr.Name))
                {
                    simpleAttributes.Add(attr);
                }
            }

            return simpleAttributes;
        }

        public string GetMultivalueByIndex(List<InstanceAttribute> multivalues, int multivalueIndex)
        {
            foreach (InstanceAttribute instanceAttribute in multivalues)
            {
                if (AttributeNameParser.GetMultiValueIndex(instanceAttribute.Name) == multivalueIndex)
                {
                    return instanceAttribute.Value;
                }
            }

            return String.Empty;
        }

        public string GetSubvalueByIndex(List<InstanceAttribute> subvalues, int subvalueIndex)
        {
            foreach (InstanceAttribute instanceAttribute in subvalues)
            {
                if (AttributeNameParser.GetSubValueIndex(instanceAttribute.Name) == subvalueIndex)
                {
                    return instanceAttribute.Value;
                }
            }

            return String.Empty;
        }

        public Dictionary<int, List<InstanceAttribute>> GetSubvalueAttributesGroupedByFieldNameAndMultivalueIndex(string fieldName)
        {
            Dictionary<int, List<InstanceAttribute>> result = new Dictionary<int, List<InstanceAttribute>>();

            List<InstanceAttribute> attributes = GetMultivalueAttributesByShortFieldName(fieldName);

            foreach (InstanceAttribute attr in attributes)
            {
                int mv = AttributeNameParser.GetMultiValueIndex(attr.Name);
                List<InstanceAttribute> subvalues;
                if (result.ContainsKey(mv))
                {
                    subvalues = result[mv];
                }
                else
                {
                    subvalues = new List<InstanceAttribute>();
                    result[mv] = subvalues;
                }
                subvalues.Add(attr);
            }

            //  sort the results
            foreach (List<InstanceAttribute>  subValueAttributes in result.Values)
            {
                subValueAttributes.Sort(CompareAttributesBySubValueIndex);
            }

            return result;
        }

        public static int CompareAttributesBySubValueIndex(InstanceAttribute i1, InstanceAttribute i2)
        {
            int sv1 = AttributeNameParser.GetSubValueIndex(i1.Name);
            int sv2 = AttributeNameParser.GetSubValueIndex(i2.Name);
            if (sv1 < sv2)
            {
                return -1;
            }
            if (sv1 > sv2)
            {
                return 1;
            }
            return 0;
        }

        public List<InstanceAttribute> GetMultivalueAttributesByShortFieldName(string shortMultivalueFieldName)
        {
            Dictionary<string, List<InstanceAttribute>> multiAttributes = GetMultivalueAttributes();
            if(multiAttributes.ContainsKey(shortMultivalueFieldName))
            {
                return multiAttributes[shortMultivalueFieldName];
            }
            else
            {
                return new List<InstanceAttribute>();
            }
        }

        public Dictionary<string, List<InstanceAttribute>> GetMultivalueAttributes()
        {
            Dictionary<string, List<InstanceAttribute>> multiAttributes = new Dictionary<string, List<InstanceAttribute>>();
            foreach (InstanceAttribute attr in Attributes)
            {
                string shortName;
                int multiValueIndex, subValueIndex;
                if (AttributeNameParser.ParseComplexFieldName(attr.Name, out shortName, out multiValueIndex, out subValueIndex))
                {
                    List<InstanceAttribute> attributes;
                    if (multiAttributes.ContainsKey(shortName))
                    {
                        attributes = multiAttributes[shortName];
                    }
                    else
                    {
                        attributes = new List<InstanceAttribute>();
                        multiAttributes.Add(shortName, attributes);
                    }

                    attributes.Add(attr);
                }
            }

            return multiAttributes;
        }

        public void RebuildHashOfAttributeNames()
        {
            _AttributesByName.Clear();
            foreach(InstanceAttribute ia in _Attributes )
            {
                _AttributesByName.Add(ia.Name, ia);
            }
        }

        #endregion

        #region Other Convert Methods

        /// <summary>
        /// Converts instance to ValidataCommon.T24Record object
        /// </summary>
        /// <returns>ValidataCommon.T24Record object</returns>
        public T24Record ToT24Record()
        {
            T24Record result = new T24Record(this.TypicalName);

            foreach (var ia in this.Attributes)
            {
                result.Fields.Add(ia.Name, ia.Value);
            }

            return result;
        }

        /// <summary>
        /// Converts ValidataCommon.T24Record object to Instance
        /// </summary>
        /// <param name="record">T24 Record object</param>
        /// <returns>Instance</returns>
        public static Instance FromT24Record(T24Record record)
        {
            Instance result = new Instance(record.ApplicationName, "");

            foreach (var field in record.Fields)
            {
                result.AddAttribute(field.Name, field.Value);
            }

            return result;
        }

        #endregion

        public override string ToString()
        {
            return string.Format("{0}-{1} - {2}",
                                 (Catalog ?? "?"),
                                 (_TypicalName ?? "?"),
                                 _Attributes.Count);
        }

        
    }
}