﻿using System.Collections.Generic;

namespace AXMLEngine
{
    public class InstanceGenerator
    {
        public static IEnumerable<Instance> Generate(int count,
                                                     string catalogName,
                                                     string typicalName,
                                                     IEnumerable<string> allAttributeNames,
                                                     IEnumerable<string> incrementalAttributeNames)
        {
            Instance template = new Instance(typicalName, catalogName);
            foreach (string attrName in allAttributeNames)
            {
                template.AddAttribute(attrName, "_" + attrName);
            }

            return GenerateFromTemplate(count, template, incrementalAttributeNames);
        }

        public static IEnumerable<Instance> GenerateFromTemplate(int count, Instance template, IEnumerable<string> incrementalAttributeNames)
        {
            for (int i = 0; i < count; i ++)
            {
                Dictionary<string, string> changes = new Dictionary<string, string>();
                foreach (string attrName in incrementalAttributeNames)
                {
                    changes[attrName] = "_" + attrName + i; // use this notation to create unique strings (so that string interning does not interfere)
                }

                yield return CreateFromTemplate(template, changes);
            }
        }

        private static Instance CreateFromTemplate(Instance template, Dictionary<string, string> changes)
        {
            Instance instance = template.CloneInstance();
            foreach (KeyValuePair<string, string>  attr in changes)
            {
                instance.SetAttributeValue(attr.Key, attr.Value);
            }
            return instance;
        }
    }
}