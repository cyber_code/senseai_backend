using System;
using System.Xml;

namespace AXMLEngine
{
    /// <summary>
    /// Storage of the Instances section
    /// </summary>
    [Serializable]
    public class Datasets
    {
        #region Private members

        /// <summary>
        /// Containers of Instances
        /// </summary>
        private readonly AXMLDatasetCollection _Datasets;

        /// <summary>
        /// Containers of errors (applicable only for response, not for request)
        /// </summary>
        private readonly ErrorSetCollection _ErrorSets;

        /// <summary>
        /// Performance Indicators  and where to measure them
        /// </summary>
        private readonly Performance _Performance;

        /// <summary>
        /// Containers of measured times (applicable only for response, not for request)
        /// </summary>
        private readonly ExecutionTimingsList _ExecutionTimes;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Constructs a Datasets object instance
        /// </summary>
        public Datasets()
        {
            _Datasets = new AXMLDatasetCollection();
            AXMLDataset defaultDataset = new AXMLDataset(0);
            _Datasets.Add(defaultDataset);

            _ErrorSets = new ErrorSetCollection();
            ErrorSet defaultErrorSet = new ErrorSet(0);
            _ErrorSets.Add(defaultErrorSet);

            _Performance = new Performance();

            _ExecutionTimes = new ExecutionTimingsList();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Serializes the dataset collection to XML
        /// </summary>
        /// <param name="xmlWriter">An XmlWriter where to write the datasets.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.INSTANCES_TAG);

            _Performance.ToXML(xmlWriter);

            // datasets
            xmlWriter.WriteStartElement(AXMLConstants.DATASET_TAG);
            foreach (AXMLDataset dataset in _Datasets)
            {
                dataset.ToXML(xmlWriter);
            }
            xmlWriter.WriteEndElement(); //End of datasets section

            // errorsets
            xmlWriter.WriteStartElement(AXMLConstants.ERRORSET_TAG);
            foreach (ErrorSet errorSet in _ErrorSets)
            {
                errorSet.ToXML(xmlWriter);
            }
            xmlWriter.WriteEndElement(); //End of errorset section

            // execution times
            ExecutionTimingsList.OpenParentNode(xmlWriter);
            _ExecutionTimes.ToXML(xmlWriter);
            ExecutionTimingsList.CloseParentNode(xmlWriter);

            xmlWriter.WriteEndElement(); //end instances tag
        }

        /// <summary>
        /// Loads instances and associations from the Instances section in AXML.
        /// </summary>
        /// <param name="datasetReader">The contents of the instances section</param>
        /// <returns>True if successfull.</returns>
        /// <param name="loadFlags">Options for loading</param>
        public bool FromXML(XmlReader datasetReader, RequestLoadFlags loadFlags)
        {
            if (!((loadFlags & RequestLoadFlags.OnlyNotifyForReadInstances) != 0))
            {
                //clear the old datasets or the default dataset
                _Datasets.Clear();

                //clear the old errorsets or the default errorset
                _ErrorSets.Clear();
            }

            _Performance.Components.Clear();

            // Init reader
            bool readingDataset = false;
            bool readingErrorset = false;
            try
            {
                // Read input data and locate root typical
                while (datasetReader.Read())
                {
                    if (datasetReader.NodeType == XmlNodeType.Element)
                    {
                        if (datasetReader.Name == AXMLConstants.DATASET_TAG)
                        {
                            //dataset section 
                            readingDataset = true;
                            readingErrorset = false;
                        }
                        else if (datasetReader.Name == AXMLConstants.ERRORSET_TAG)
                        {
                            //errorset section
                            readingDataset = false;
                            readingErrorset = true;
                        }
                        else if (readingDataset && (datasetReader.Name == AXMLConstants.DATASET_ROW_TAG))
                        {
                            //dataset row tag
                            string datasetID = datasetReader.GetAttribute(AXMLConstants.DATASET_ATTRIBUTE_ID);

                            if (!((loadFlags & RequestLoadFlags.OnlyNotifyForReadInstances) != 0))
                            {
                                int id;
                                int.TryParse(datasetID, out id);
                                AXMLDataset dataset = new AXMLDataset(id);

                                using (XmlReader dsReader = datasetReader.ReadSubtree())
                                {
                                    dataset.FromXML(dsReader, loadFlags);
                                }

                                _Datasets.Add(dataset);
                            }
                            else
                            {
                                //only one (default) dataset exists
                                if (_Datasets.Count == 1)
                                {
                                    using (XmlReader instancesReader = datasetReader.ReadSubtree())
                                    {
                                        _Datasets[0].FromXML(instancesReader, loadFlags);
                                    }
                                }
                            }
                        }
                        else if (readingErrorset && (datasetReader.Name == AXMLConstants.ERRORSET_ROW_TAG))
                        {
                            //errorset row tag
                            string errorsetID = datasetReader.GetAttribute(AXMLConstants.ERRORSET_ATTRIBUTE_ID);

                            if (!((loadFlags & RequestLoadFlags.OnlyNotifyForReadErrors) != 0))
                            {
                                int id;
                                int.TryParse(errorsetID, out id);

                                ErrorSet errorSet = new ErrorSet(id);
                                using (XmlReader errorsReader = datasetReader.ReadSubtree())
                                {
                                    errorSet.FromXML(errorsReader, ((loadFlags & RequestLoadFlags.OnlyNotifyForReadErrors) != 0));
                                }

                                _ErrorSets.Add(errorSet);
                            }
                            else
                            {
                                //only one (default) error exists
                                if (_Datasets.Count == 1)
                                {
                                    using (XmlReader errorsReader = datasetReader.ReadSubtree())
                                    {
                                        _ErrorSets[0].FromXML(errorsReader, ((loadFlags & RequestLoadFlags.OnlyNotifyForReadErrors) != 0));
                                    }
                                }
                            }
                        }
                        else if (datasetReader.Name == AXMLConstants.PERFORMANCE_INDICATORS_SECTION_TAG)
                        {
                            using (XmlReader reader = datasetReader.ReadSubtree())
                            {
                                _Performance.FromXML(reader);
                            }
                        }
                        else if (datasetReader.Name == AXMLConstants.EXECUTION_TIMES_SECTION_TAG)
                        {
                            using (XmlReader reader = datasetReader.ReadSubtree())
                            {
                                _ExecutionTimes.FromXML(reader);
                            }
                        }
                    }
                    else
                    {
                        switch (datasetReader.NodeType)
                        {
                            case XmlNodeType.EndElement:
                                if (datasetReader.Name == AXMLConstants.INSTANCES_TAG)
                                {
                                    //reached the closing tag - end reading
                                    return true;
                                }
                                break;
                        }
                    }
                }
            }
            finally
            {
                //even if there are no instances there must be a default dataset
                if (_Datasets.Count == 0)
                {
                    AXMLDataset dataset = new AXMLDataset(0);
                    _Datasets.Add(dataset);
                }

                //evein if there are no errors there must be a default errorset
                if (_ErrorSets.Count == 0)
                {
                    ErrorSet errorSet = new ErrorSet(0);
                    _ErrorSets.Add(errorSet);
                }
            }

            return true;
        }

        /// <summary>
        /// Adds the given dataset to the Dataset collection
        /// </summary>
        /// <param name="ds">The AXMLDataset to be added.</param>
        public void AddDataset(AXMLDataset ds)
        {
            _Datasets.Add(ds);
        }

        /// <summary>
        /// Adds the given error set to the Errorset collection
        /// </summary>
        /// <param name="es">The ErrorSet object to be added.</param>
        public void AddErrorSet(ErrorSet es)
        {
            _ErrorSets.Add(es);
        }

        /// <summary>
        /// Adds the Component to the 
        /// </summary>
        /// <param name="pa">The Component object to be added.</param>
        public void AddComponent(Component pa)
        {
            _Performance.Components.Add(pa);
        }

        /// <summary>
        /// Clears the currently created datasets and errorsets
        /// </summary>
        public void ClearContents()
        {
            _Datasets.Clear();
            _ErrorSets.Clear();
        }

        /// <summary>
        /// Get the dataset of the given index
        /// </summary>
        /// <param name="index">The index of the dataset.</param>
        /// <returns>An AXMLDataset object.</returns>
        public AXMLDataset GetDataset(int index)
        {
            if (index >= 0 && index < _Datasets.Count)
            {
                return _Datasets[index];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the ErrorSet at the given index
        /// </summary>
        /// <param name="index">The index of the dataset.</param>
        /// <returns>An ErrorSet object.</returns>
        public ErrorSet GetErrorSet(int index)
        {
            if (index > 0 && index < _ErrorSets.Count)
            {
                return _ErrorSets[index];
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region Public properties		

        /// <summary>
        /// Returns the default dataset
        /// </summary>
        public AXMLDataset DefaultDataset
        {
            get { return _Datasets[0]; }
        }

        /// <summary>
        /// Returns the default ErrorSet
        /// </summary>
        public ErrorSet DefaultErrorSet
        {
            get { return _ErrorSets[0]; }
        }

        /// <summary>
        /// Retrive All Error Sets
        /// </summary>
        public ErrorSetCollection ErrorSets
        {
            get { return _ErrorSets;  }
        }

        public bool HasErrors
        {
            get
            {
                foreach (ErrorSet errSet in ErrorSets)
                {
                    if (errSet.Errors.Count > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// The number of dataset rows
        /// </summary>
        public int DatasetCount
        {
            get { return _Datasets.Count; }
        }

        public ComponentsCollection Components
        {
            get { return _Performance.Components; }
        }

        public ExecutionTimingsList ExecutionTimes
        {
            get { return _ExecutionTimes; }
        }

        #endregion
    }
}