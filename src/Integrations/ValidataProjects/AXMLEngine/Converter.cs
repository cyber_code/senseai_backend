using System.Web;
using ValidataCommon;

namespace AXMLEngine
{
    /// <summary>
    /// Converter class, containing shared implementations of string conversions
    /// </summary>
    public class Converter
    {
        #region Public Static Methods for Encoding Invalid Characters

        /// <summary>
        /// Encodes attribute value to create valid XML elements
        /// </summary>
        /// <param name="attributeValue">Intended Attribute Value</param>
        /// <returns>Encoded Value, safe to store in XML</returns>
        public static string EncodeAttributeValue(string attributeValue)
        {
            return Utils.GetHtmlEncodedValue(attributeValue);
        }

        /// <summary>
        /// Decodes attribute value from valid XML elements
        /// </summary>
        /// <param name="encodedValue">Encoded Attribute Value</param>
        /// <returns>Decoded Value</returns>
        public static string DecodeAttributeValue(string encodedValue)
        {
            return HttpUtility.HtmlDecode(encodedValue);
        }

        #endregion
    }
}