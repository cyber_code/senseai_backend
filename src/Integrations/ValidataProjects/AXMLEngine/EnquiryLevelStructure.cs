﻿using System;
using System.Text;
using System.Xml;
using System.Collections.Generic;


namespace AXMLEngine
{
    #region Enums

    ///<summary>
    ///Value type
    ///</summary>
    public enum ValueType
    {
        // todo - merge this with Validata.Common.AttributeDataType (located in ValidataCommon)
        String,
        Integer,
        Currency,
        Date
    }

    ///<summary>
    ///Operand type
    ///</summary>
    public enum ValueOperand
    {
        // todo - merge this with AXMLEngine.FilterType
        EQ,
        NE,
        GT,
        GE,
        LT,
        LE,
        LIKE,
        STARTSWITH,
        ENDSWITH,
        REGEX
    }

    #endregion

    ///<summary>
    ///Field
    ///</summary>
    public class Field
    {
        #region Public Properties

        ///<summary>
        ///Get, set value type of the field
        ///</summary>
        public ValueType Type
        {
            get;
            set;
        }

        ///<summary>
        ///Get, set value of the field used for search criteria
        ///</summary>
        public string Value
        {
            get;
            set;
        }

        /// <summary>
        /// Get, set operand for comparing of search criteria
        /// </summary>
        public ValueOperand Operand
        {
            get;
            set;
        }

        ///<summary>
        ///Get, set fieldName to map this value to corresponding result typical attribute.
        ///</summary>
        public string FieldName
        {
            get;
            set;
        }

        #endregion

        #region Lifecycle

        ///<summary>
        ///Shadow copy of the object
        ///</summary>
        ///<returns></returns>
        public Field ShadowCopy()
        {
            return new Field { Value = this.Value, Type = this.Type, Operand = this.Operand, FieldName = this.FieldName };
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Field() { }

        ///<summary>
        ///Construct Field structure from valid XML Node
        ///</summary>
        ///<param name="fieldNode">XML Node</param>
        public Field(XmlNode fieldNode)
        {
            try
            {
                XmlAttribute attrType = fieldNode.Attributes["type"];
                if (attrType != null)
                {
                    try
                    {
                        Type = (ValueType)Enum.Parse(typeof(ValueType), attrType.Value, true);
                    }
                    catch (ArgumentException)
                    {
                        Type = ValueType.String;
                    }
                }
                else
                {
                    Type = ValueType.String;
                }

                XmlAttribute attrValue = fieldNode.Attributes["value"];
                if (attrValue != null)
                {
                    Value = attrValue.Value;
                }

                XmlAttribute attrOperand = fieldNode.Attributes["operand"];
                if (attrOperand != null)
                {
                    try
                    {
                        Operand = (ValueOperand)Enum.Parse(typeof(ValueOperand), attrOperand.Value, true);
                    }
                    catch (ArgumentException)
                    {
                        Operand = ValueOperand.EQ;
                    }
                }
                else
                {
                    Operand = ValueOperand.EQ;
                }

                XmlAttribute attrFieldName = fieldNode.Attributes["fieldName"];
                if (attrFieldName != null)
                {
                    FieldName = attrFieldName.Value;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Internal Methods (Serialization)
        private const string Nullable = "@NULL@";
        private const string ShortStringSerializationDelimiter = "^";

        internal static string ToShortString(Field field)
        {
            StringBuilder sbResult = new StringBuilder();

            sbResult.Append(field.Type.ToString().ToLower());
            sbResult.Append(ShortStringSerializationDelimiter);
           
            if (field.Value == null)
            { 
                sbResult.Append(Nullable); 
            }
            else
            {
                sbResult.Append(field.Value);
            }
            sbResult.Append(ShortStringSerializationDelimiter);
            
            sbResult.Append(field.Operand);
            sbResult.Append(ShortStringSerializationDelimiter);

            sbResult.Append(field.FieldName);

            return sbResult.ToString();
        }

        internal static Field FromShortString(string value)
        {
            Field result = new Field();
            //int pos = value.IndexOf(ShortStringSerializationDelimiter);
            //result.Type = (ValueType)Enum.Parse(typeof(ValueType), value.Substring(0, pos), true);

            //value = value.Substring(pos+1);

            //pos = value.LastIndexOf(ShortStringSerializationDelimiter);
            //result.Operand = (ValueOperand)Enum.Parse(typeof(ValueOperand), value.Substring(pos + 1));

            //result.Value = value.Substring(0, pos);
            
            //if (result.Value == Nullable)
            //    result.Value = null;            

            string[] items = value.Split(new string[] { ShortStringSerializationDelimiter }, StringSplitOptions.None);

            for (int i = 0; i < items.Length; i++)
            {
                switch(i)
                {
                    case 0:
                        result.Type = (ValueType)Enum.Parse(typeof(ValueType), items[0], true);
                    break;

                    case 1:
                    {
                        result.Value = items[1];
                        if (result.Value == Nullable)
                            result.Value = null;
                    }
                    break;

                    case 2:
                        result.Operand = (ValueOperand)Enum.Parse(typeof(ValueOperand), items[2]);
                    break;

                    case 3:
                    {
                        result.FieldName = items[3];
                        if (String.IsNullOrEmpty(result.FieldName))
                            result.FieldName = null;
                    }
                    break;
                }
            }

            return result;
        }

        #endregion
    }

    ///<summary>
    ///Row template structure
    ///</summary>
    public class LevelRowTemplate
    {
        private readonly List<Field> _Fields = new List<Field>();

        #region Public Methods

        /// <summary>
        /// Name of the template
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        ///<summary>
        ///List of fields
        ///</summary>
        public List<Field> Fields
        {
            get { return _Fields; }
        }

        #endregion

        #region Lifecycle

        /// <summary>
        /// Default constructor
        /// </summary>
        public LevelRowTemplate() { }

        ///<summary>
        ///Shadow copy of the object
        ///</summary>
        ///<returns></returns>
        public LevelRowTemplate ShadowCopy()
        {
            var rowTemplate = new LevelRowTemplate { Name = this.Name };

            var fields = new List<Field>();
            foreach (Field field in _Fields)
                rowTemplate.Fields.Add(field.ShadowCopy());

            return rowTemplate;
        }

        ///<summary>
        ///Construct RowTemplate structure from valid XML Node
        ///</summary>
        ///<param name="template">XML Node</param>
        public LevelRowTemplate(XmlNode template)
        {
            Name = template.Attributes["ref"].Value;
            foreach (XmlNode fieldNode in template.ChildNodes)
                _Fields.Add(new Field(fieldNode));
        }

        #endregion

        #region Internal Methods (Serialization)

        private const string ShortStringSerializationFiledDelimiter = "|";

        internal static string ToShortString(LevelRowTemplate levelRowTemplate)
        {
            StringBuilder sbResult = new StringBuilder();
            sbResult.Append(levelRowTemplate.Name ?? string.Empty);
            sbResult.Append(ShortStringSerializationFiledDelimiter);

            System.Diagnostics.Debug.Assert(levelRowTemplate._Fields != null);
            if (levelRowTemplate._Fields != null)
            {
                foreach (var filed in levelRowTemplate._Fields)
                {
                    sbResult.Append(Field.ToShortString(filed));
                    if (filed != levelRowTemplate._Fields[levelRowTemplate._Fields.Count - 1])
                    {
                        // append delimiter if field is not the last one
                        sbResult.Append(ShortStringSerializationFiledDelimiter);
                    }
                }
            }

            return sbResult.ToString();
        }

        internal static LevelRowTemplate FromShortString(string value)
        {
            LevelRowTemplate result = new LevelRowTemplate();
            string[] parts = value.Split(new string[] { ShortStringSerializationFiledDelimiter }, StringSplitOptions.None);

            result.Name = parts[0];

            for (int i = 1; i < parts.Length; i++)
            {
                result._Fields.Add(Field.FromShortString(parts[i]));
            }

            return result;
        }

        #endregion
    }

    ///<summary>
    ///Hierarchy of structures
    ///</summary>
    public class LevelStructureHierarchy
    {
        private List<LevelStructureHierarchy> _levels;

        private List<LevelRowTemplate> _rowTemplates;

        #region Public Properties

        ///<summary>
        ///Get, set position of matching (First, Last, <Number>)
        ///</summary>
        public string Occurrence
        {
            get;
            set;
        }

        public bool AdvancedMode { get; set; }

        ///<summary>
        ///List of row templates
        ///</summary>
        public List<LevelRowTemplate> RowTemplates
        {
            get { return _rowTemplates; }
            set { _rowTemplates = value; }
        }

        /// <summary>
        /// Parent level for the level
        /// </summary>
        public LevelStructureHierarchy ParentLevel { get; set; }

        ///<summary>
        ///List of nested levels
        ///</summary>
        public List<LevelStructureHierarchy> SubLevels
        {
            get { return _levels; }
        }

        #endregion

        #region Lifecycle

        /// <summary>
        /// Default constructor
        /// </summary>
        public LevelStructureHierarchy() { }

        ///<summary>
        ///Construct Level structure from valid XML Node
        ///</summary>
        ///<param name="levelNode">XML node</param>
        public LevelStructureHierarchy(XmlNode levelNode)
        {
            try
            {
                XmlAttribute attrOccurrence = levelNode.Attributes["occurrence"];
                if (attrOccurrence != null)
                    Occurrence = attrOccurrence.Value;

                XmlNodeList rowTemplates = levelNode.SelectNodes("RowTemplate");
                if (rowTemplates != null && rowTemplates.Count > 0)
                {
                    foreach (XmlNode rowTemplateNode in rowTemplates)
                    {
                        AddRowTemplate(new LevelRowTemplate(rowTemplateNode));
                    }
                }
                else
                    throw new InvalidOperationException("RowTemplate element does not exist for the level");


                XmlNodeList subLevels = levelNode.SelectNodes("Level");
                if (subLevels != null && subLevels.Count > 0)
                {
                    _levels = new List<LevelStructureHierarchy>();
                    foreach (XmlNode subLevel in subLevels)
                        AddSubLevel(new LevelStructureHierarchy(subLevel));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Public Methods

        ///<summary>
        ///Shadow copy of the object
        ///</summary>
        ///<returns></returns>
        public LevelStructureHierarchy ShadowCopy()
        {
            var level = new LevelStructureHierarchy { Occurrence = Occurrence, ParentLevel = ParentLevel };
            foreach (LevelRowTemplate rowTemplate in _rowTemplates)
            {
                level.AddRowTemplate(rowTemplate.ShadowCopy());
            }

            return level;
        }

        ///<summary>
        ///Add sub level
        ///</summary>
        ///<param name="levelStruture">Level structure</param>
        public void AddSubLevel(LevelStructureHierarchy levelStruture)
        {
            if (_levels == null) _levels = new List<LevelStructureHierarchy>();
            levelStruture.ParentLevel = this;
            _levels.Add(levelStruture);
        }

        public bool HaveSublevels()
        {
            if (SubLevels == null || SubLevels.Count == 0)
            {
                return false;
            }

            return true;
        }

        ///<summary>
        ///Add row template
        ///</summary>
        public void AddRowTemplate(LevelRowTemplate rowTemplate)
        {
            if (_rowTemplates == null)
                _rowTemplates = new List<LevelRowTemplate>();
            _rowTemplates.Add(rowTemplate);
        }

        /// <summary>
        /// Extract row templates and put them into Level
        /// </summary>
        /// <param name="doc"><c>Xml</c> Document</param>
        public static void PreformatEnquiryNode(XmlNode doc)
        {
            try
            {
                var adaptersNode = doc.SelectSingleNode("Adapters");
                if (adaptersNode != null)
                {
                    doc.RemoveChild(adaptersNode);
                }

                XmlNode levelNode = doc.SelectSingleNode("Response");
                if (levelNode == null)
                    throw new Exception("Response element does not exist.");

                XmlNodeList rowTemplates = levelNode.SelectNodes("Level//RowTemplate");

                if (rowTemplates == null)
                    throw new Exception("RowTemplate element does not exist.");

                var tempNodes = new Dictionary<string, XmlNode>();

                foreach (XmlNode template in rowTemplates)
                {
                    XmlAttribute refAttr = template.Attributes["ref"];
                    if (refAttr == null)
                        throw new Exception("'ref' attribute for RowTemplate element does not exist.");

                    XmlNode templateRef = doc.SelectSingleNode(refAttr.Value);
                    if (templateRef == null)
                        throw new Exception(String.Format("RowTemplate element {0} is not defined", refAttr.Value));


                    var nodes = new List<XmlNode>();
                    foreach (XmlNode fieldNode in templateRef.ChildNodes)
                        nodes.Add(fieldNode.Clone());

                    foreach (XmlNode fieldNode in nodes)
                        template.AppendChild(fieldNode);

                    if (!tempNodes.ContainsKey(refAttr.Value))
                        tempNodes.Add(refAttr.Value, templateRef);
                }

                foreach (XmlNode tempNode in tempNodes.Values)
                {
                    doc.RemoveChild(tempNode);
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Invalid content of EnquiryConfiguration.xml file. " + "Details: " + ex.Message);
            }
        }

        #endregion

        #region Public Methods (Serialization)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public static LevelStructureHierarchy FromXmlString(string xmlString)
        {
            bool isAdvancedMode = false;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlString);

            var enquiryNode = doc.FirstChild;
            if(enquiryNode.Attributes["mode"]!=null)
                if (enquiryNode.Attributes["mode"].Value=="1")
                    isAdvancedMode = true;

            if (enquiryNode is XmlDeclaration)
            {
                enquiryNode = doc.ChildNodes[1];
            }
            PreformatEnquiryNode(enquiryNode);

            XmlNode responseLevelNode = enquiryNode.SelectSingleNode("Response/Level");
            if (responseLevelNode == null)
            {
                throw new InvalidOperationException("Response/Level element does not exist in the passed string");
            }

            return new LevelStructureHierarchy(responseLevelNode)
                {AdvancedMode = isAdvancedMode};

        }

        public static string ToXmlString(LevelStructureHierarchy levelStructureHierarchy)
        {
            return ToXmlString(levelStructureHierarchy, "UNKNOWN", "UNKNOWN");
        }

        public static string ToXmlString(LevelStructureHierarchy levelStructureHierarchy, string enquiryName, string adapterName)
        {
            XmlDocument doc = new XmlDocument();
            var enquiryNode = doc.AppendChild(doc.CreateElement("Enquiry"));
            {
                // todo - do we need it?
                AppendAttribute(enquiryNode, "name", enquiryName);
                if (levelStructureHierarchy.AdvancedMode)
                    AppendAttribute(enquiryNode, "mode", "1");
            }
            var adaptersNode = enquiryNode.AppendChild(doc.CreateElement("Adapters"));
            {
                // todo - do we need it?
                var adapterNode = adaptersNode.AppendChild(doc.CreateElement("Adapter"));
                AppendAttribute(adapterNode, "name", adapterName);
            }

            // add level x row templates
            AddRowTemplateRecursive(doc, enquiryNode, levelStructureHierarchy);

            // add hierarchy
            var responseNode = enquiryNode.AppendChild(doc.CreateElement("Response"));
            AddLevelRecursive(doc, responseNode, levelStructureHierarchy);

            return doc.OuterXml;
        }
        
        private const string ShortStringLevelStructurePrefix = "<<<";

        private const string ShortStringLevelStructureSuffix = ">>>";

        private const string ShortStringSerializationItemDelimiter = "||";

        public static string ToShortString(LevelStructureHierarchy levelStructureHierarchy)
        {
            if (levelStructureHierarchy == null)
            {
                return string.Empty;
            }

            StringBuilder sbResult = new StringBuilder();
            if (levelStructureHierarchy.AdvancedMode)
            {
                sbResult.Append(ShortStringLevelStructurePrefix);
                sbResult.Append("M1");
                sbResult.Append(ShortStringLevelStructureSuffix);
            }
            WriteShortStringRecursively(levelStructureHierarchy, sbResult, 0, 0, 0);

            return sbResult.ToString();
        }

       
        private static void WriteShortStringRecursively(LevelStructureHierarchy levelStructureHierarchy,StringBuilder sbResult, int level, int index, int parentIndex)
        {
            
            sbResult.Append(ShortStringLevelStructurePrefix);

            sbResult.Append(levelStructureHierarchy.Occurrence ?? string.Empty);
            sbResult.Append(ShortStringSerializationItemDelimiter);

            sbResult.Append(level);
            sbResult.Append(ShortStringSerializationItemDelimiter);

            sbResult.Append(parentIndex);
            sbResult.Append(ShortStringSerializationItemDelimiter);

            // row templates
            System.Diagnostics.Debug.Assert(levelStructureHierarchy._rowTemplates != null);
            if (levelStructureHierarchy._rowTemplates != null)
            {
                foreach (var rowTemplate in levelStructureHierarchy._rowTemplates)
                {
                    sbResult.Append(LevelRowTemplate.ToShortString(rowTemplate));
                    if (rowTemplate != levelStructureHierarchy._rowTemplates[levelStructureHierarchy._rowTemplates.Count - 1])
                    {
                        // append delimiter only in case the rowTemplate is not the last one
                        sbResult.Append(ShortStringSerializationItemDelimiter);
                    }
                }
            }

            sbResult.Append(ShortStringLevelStructureSuffix);

            if (levelStructureHierarchy._levels != null)
            {
                for (int i = 0; i < levelStructureHierarchy._levels.Count; i++)
                {
                    var subLevel = levelStructureHierarchy._levels[i];
                    WriteShortStringRecursively(subLevel, sbResult, level + 1, i, index);
                }
            }
        }

        public static LevelStructureHierarchy FromShortString(string value)
        {
            
            try
            {
                bool isAdvanced = false;
                string isAdvancedString = String.Concat(ShortStringLevelStructurePrefix, "M1", ShortStringLevelStructureSuffix);
               
                if (!value.StartsWith(ShortStringLevelStructurePrefix) ||
                    !value.EndsWith(ShortStringLevelStructureSuffix))
                {
                    // not in the required format
                    return null;
                }
                else
                {
                    if (value.Contains(isAdvancedString))
                    {
                        isAdvanced = true;
                        value=value.Replace(isAdvancedString, "");

                    }
                    value = value.Substring(
                        ShortStringLevelStructurePrefix.Length,
                        value.Length - ShortStringLevelStructurePrefix.Length - ShortStringLevelStructureSuffix.Length
                        );
                }

                string[] levelStrings = value.Split(new string[] { ShortStringLevelStructureSuffix + ShortStringLevelStructurePrefix }, StringSplitOptions.None);

                int level;
                int parentIdx;
                LevelStructureHierarchy result = FromShortStringSingleLevel(levelStrings[0], out level, out parentIdx);
                result.AdvancedMode = isAdvanced;

                for (int i = 1; i < levelStrings.Length; i++)
                {
                    LevelStructureHierarchy childLevelStructure =
                        FromShortStringSingleLevel(levelStrings[i], out level, out parentIdx);

                    bool isAdded = AppendToParent(result, 0, 0, childLevelStructure, level, parentIdx);
                    System.Diagnostics.Debug.Assert(isAdded);
                }

                return result;
            }
            catch
            {
                return null;
            }
        }

        private static bool AppendToParent(LevelStructureHierarchy master, int masterLevel, int masterIdx, LevelStructureHierarchy childLevelStructure, int level, int parentIdx)
        {
            if ((masterLevel + 1) == level && masterIdx == parentIdx)
            {
                if (master._levels == null)
                {
                    master._levels = new List<LevelStructureHierarchy>();
                }

                master._levels.Add(childLevelStructure);
                childLevelStructure.ParentLevel = master;
                return true;
            }

            for (int i = 0; i < master._levels.Count; i++)
            {
                if (AppendToParent(master._levels[i], masterLevel + 1, i, childLevelStructure, level, parentIdx))
                {
                    return true;
                }
            }

            return false;
        }

        private static LevelStructureHierarchy FromShortStringSingleLevel(string levelStr, out int level, out int parentIdx)
        {
            string[] parts = levelStr.Split(new string[] { ShortStringSerializationItemDelimiter }, StringSplitOptions.None);

            LevelStructureHierarchy result = new LevelStructureHierarchy();
            result._rowTemplates = new List<LevelRowTemplate>();
            result.Occurrence = parts[0] == string.Empty ? null : parts[0];
            level = int.Parse(parts[1]);
            parentIdx = int.Parse(parts[2]);

            for (int i = 3; i < parts.Length; i++)
            {
                result._rowTemplates.Add(LevelRowTemplate.FromShortString(parts[i]));
            }

            return result;
        }

        #endregion

        #region Internal Methods

        internal void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.ENQUIRY_MASK_TAG);

            var raw = LevelStructureHierarchy.ToXmlString(this);

            xmlWriter.WriteRaw(raw);

            xmlWriter.WriteEndElement();

            xmlWriter.Flush();
        }

        internal static LevelStructureHierarchy FromXML(XmlReader reader)
        {
            reader.Read();

            string outerXml = reader.ReadOuterXml();

            // this is implemented because of the added xmlns (by the XmlDocumen/XmlNode)
            //  remove the outer tag with xmlns attribute
            int from = outerXml.IndexOf('>');
            int to = outerXml.LastIndexOf('<');
            if (from >= 0 && to >= 0 && from < to)
            {
                outerXml = outerXml.Substring(from + 1, to - (from + 1));
            }

            return LevelStructureHierarchy.FromXmlString(outerXml);
        }

        #endregion

        #region Private Static Methods

        private static XmlAttribute AppendAttribute(XmlNode node, string name, string value)
        {
            var attribute = node.Attributes.Append(node.OwnerDocument.CreateAttribute(name));
            attribute.Value = value;
            return attribute;
        }

        private static void AddLevelRecursive(XmlDocument doc, XmlNode parentNode, LevelStructureHierarchy levelStructureHierarchy)
        {
            var levelNode = parentNode.AppendChild(doc.CreateElement("Level"));

            if (!string.IsNullOrEmpty(levelStructureHierarchy.Occurrence))
            {
                AppendAttribute(levelNode, "occurrence", levelStructureHierarchy.Occurrence);
            }
            
            foreach (var rowTemplate in levelStructureHierarchy.RowTemplates)
            {
                var rowTemplateNode = levelNode.AppendChild(doc.CreateElement("RowTemplate"));
                AppendAttribute(rowTemplateNode, "ref", rowTemplate.Name);
            }

            if (levelStructureHierarchy.SubLevels != null)
            {
                foreach (var subLevel in levelStructureHierarchy.SubLevels)
                {
                    AddLevelRecursive(doc, levelNode, subLevel);
                }
            }
        }

        private static void AddRowTemplateRecursive(XmlDocument doc, XmlNode enquiryNode, LevelStructureHierarchy levelStructureHierarchy)
        {
            foreach (var rowTemplate in levelStructureHierarchy.RowTemplates)
            {
                var rowTemplateNode = enquiryNode.AppendChild(doc.CreateElement(rowTemplate.Name));

                foreach(var field in rowTemplate.Fields)
                {
                    var fieldNode = rowTemplateNode.AppendChild(doc.CreateElement("Field"));

                    AppendAttribute(fieldNode, "type", field.Type.ToString().ToLower());

                    if (field.Value != null)
                    {
                        AppendAttribute(fieldNode, "operand", field.Operand.ToString());
                        AppendAttribute(fieldNode, "value", field.Value.ToString());
                    }

                    if (!string.IsNullOrEmpty(field.FieldName))
                    {
                        AppendAttribute(fieldNode, "fieldName", field.FieldName.ToString());
                    }
                }
            }

            if (levelStructureHierarchy.SubLevels != null)
            {
                foreach (var subLevel in levelStructureHierarchy.SubLevels)
                {
                    AddRowTemplateRecursive(doc, enquiryNode, subLevel);
                }
            }

        }

        #endregion
    }
}
