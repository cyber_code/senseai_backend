﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AXMLEngine
{
    internal static class TypicalAttributeExtensions
    {
        internal static List<ValidataCommon.IMetadataAttribute> ToMetadataAttributesList(this List<TypicalAttribute> attributes)
        {
            return attributes.Select(n => n.ToMetadataAttribute()).ToList();
        }

        internal static ValidataCommon.IMetadataAttribute ToMetadataAttribute(this TypicalAttribute attribute)
        {
            return new SimpleMetadataAttribute 
            {
                Name = attribute.Name,
                DataType = attribute.Type, 
                DataLength = attribute.DataLength,
                Remarks = attribute.Remarks
            };
        }
    }

    internal class SimpleMetadataAttribute : ValidataCommon.IMetadataAttribute
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public string MinValue { get; set; }
        public string MaxValue { get; set; }
        public string DefaultValue { get; set; }
        public ulong DataLength { get; set; }
        public string PaddingChar { get; set; }
        public Validata.Common.AttributeDataType DataType { get; set; }
        public Validata.Common.AttributeStatus Status { get; set; }
        public double Tolerance { get; set; }
        public string Prefix { get; set; }
        public bool IsMultiValue { get; set; }
        public ushort MultipleValuesCount { get; set; }
        public ushort SuperMultipleValuesCount { get; set; }
        public bool Ignore { get; set; }
        public ulong GroupId { get; set; }
        public string[] LookupValues { get; set; }
        public ulong DisciplineNoderef { get; set; }

        public ulong UserNoderef { get; set; }
    }
}
