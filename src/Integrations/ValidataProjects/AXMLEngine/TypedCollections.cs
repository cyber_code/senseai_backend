//===============================================================================
// TypedCollections.cs
//===============================================================================
//
// Implementations of strong-typed collections for the AXML engine.
//
// History:
//	Version:                Date:                   Author:
//	1.00                    13/01/2004              Dimitar
//	Description: Created initial version
//
//===============================================================================
// Copyright (C) 2004-2006 Theseus Technology Partners
// All rights reserved.
//===============================================================================

using System;
using System.Collections.Generic;

namespace AXMLEngine
{
    /// <summary>
    /// Strongly-typed collection for the MetadataTypicals
    /// </summary>
    [Serializable]
    public class TypicalCollection : List<MetadataTypical>
    {
        /// <summary>
        /// Finds the by ID.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public MetadataTypical FindByID(string id)
        {
            foreach (MetadataTypical metadataTypical in this)
            {
                if (metadataTypical.ID == id)
                {
                    return metadataTypical;
                }
            }
            return null;
        }
    }

    /// <summary>
    /// Collection of AdapterRequests
    /// </summary>
    [Serializable]
    public class AdapterRequestsCollection : List<AdapterRequest>
    {
    }

    /// <summary>
    /// Collection of associations in Instance AXML.
    /// </summary>
    [Serializable]
    public class AssociationCollection : List<XMLAssociation>
    {
    }

    /// <summary>
    /// Collection of AXML datasest.
    /// </summary>
    [Serializable]
    public class AXMLDatasetCollection : List<AXMLDataset>
    {
    }

    /// <summary>
    /// Collection of Components.
    /// </summary>
    [Serializable]
    public class ComponentsCollection : List<Component>
    {
    }

    /// <summary>
    /// Collection of Errors
    /// </summary>
    [Serializable]
    public class ErrorCollection : List<Error>
    {
    }

    /// <summary>
    /// Collection of ErrorSets
    /// </summary>
    [Serializable]
    public class ErrorSetCollection : List<ErrorSet>
    {
    }

    /// <summary>
    /// Strong-typed collection for the PerformanceIndicator class
    /// </summary>
    [Serializable]
    public class PerformanceIndicatorsCollection : List<PerformanceIndicator>
    {
        /// <summary>
        /// Find an indicator by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public PerformanceIndicator Find(string name)
        {
            foreach (PerformanceIndicator pi in this)
            {
                if (pi.Name == name)
                {
                    return pi;
                }
            }

            return null;
        }
    }

    /// <summary>
    /// Collection of MemberValues
    /// </summary>
    [Serializable]
    public class MemberValueCollection : List<MemberValue>
    {
        /// <summary>
        /// Get value of MemberValue
        /// </summary>
        /// <param name="functionName"></param>
        /// <returns></returns>
        public string GetValue(string functionName)
        {
            foreach (MemberValue value in this)
            {
                if (value.Function == functionName)
                {
                    return value.Value;
                }
            }

            return null;
        }
    }
}