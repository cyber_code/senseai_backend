﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using ValidataCommon;

namespace AXMLEngine
{
    internal class InstanceCollectionCache
    {
        public int HashCode;
        public InstanceCollection Result;
    }

    /// <summary>
    /// Collection of Instances
    /// </summary>
    [Serializable]
    public class InstanceCollection : List<Instance>
    {
        private static readonly Dictionary<string, InstanceCollectionCache> _Cache = new Dictionary<string, InstanceCollectionCache>();

        #region Constructors

        public InstanceCollection()
        {
        }

        public InstanceCollection(IEnumerable<Instance> instances)
            : base(instances)
        {
        }

        public InstanceCollection(IEnumerable<IValidataRecord> instances)
        {
            foreach (var validataRecord in instances)
            {
                Add(validataRecord as Instance);
            }
        }


        public InstanceCollection(int capacity)
            : base(capacity)
        {
        }

        #endregion

        #region XML Writing

        /// <summary>
        /// Writes the instances to XML
        /// </summary>
        /// <param name="writer">The writer.</param>
        public void ToXml(XmlWriter writer)
        {
            foreach (Instance instance in this)
            {
                instance.ToXML(writer);
            }
        }

        /// <summary>
        /// Gets the XML fragment reading settings.
        /// </summary>
        /// <returns></returns>
        public static XmlWriterSettings GetXmlFragmentWritingSettings()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            return settings;
        }

        /// <summary>
        /// Gets the XML string.
        /// </summary>
        /// <returns></returns>
        public string GetXml()
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter stringWriter = new StringWriter(sb))
            {
                using (XmlWriter xmlWriter = new SafeXmlTextWriter(stringWriter))
                {
                    ToXml(xmlWriter);
                    xmlWriter.Flush();
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Writes the instances to XML
        /// </summary>
        public string GetXml(XmlWriterSettings settings)
        {
            StringBuilder sb = new StringBuilder();

            // TODO we should use SafeXmlTextWriter, but so far I have not found how to both inherit XmlTextWriter and use XmlWriterSettings (Ivan Mitev)
            using (XmlWriter xmlWriter = XmlWriter.Create(sb, settings))
            {
                ToXml(xmlWriter);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Writes the instances to XML
        /// </summary>
        public string GetXmlFragment()
        {
            return GetXml(GetXmlFragmentWritingSettings());
        }
        
        #endregion

        #region XML Reading

        /// <summary>
        /// Gets the XML fragment reading settings.
        /// </summary>
        /// <returns></returns>
        public static XmlReaderSettings GetXmlFragmentReadingSettings()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            return settings;
        }

        /// <summary>
        /// Reads multiple instances from VXML, by default it relies the VXML to be a well-formed XML document
        /// </summary>
        /// <param name="instancesVxml">The instances VXML.</param>
        /// <returns></returns>
        public static InstanceCollection FromXmlString(string instancesVxml)
        {
            return FromXmlString(instancesVxml, new XmlReaderSettings());
        }

        /// <summary>
        /// Reads multiple instances from VXML
        /// </summary>
        /// <param name="instancesVxml">The attributes VXML.</param>
        /// <param name="settings">The settings for reading the XML.</param>
        /// <returns></returns>
        public static InstanceCollection FromXmlString(string instancesVxml, XmlReaderSettings settings)
        {
            using (TextReader textReader = new StringReader(instancesVxml))
            {
                using (XmlReader xmlReader = XmlReader.Create(textReader, settings))
                {
                    return FromXml(xmlReader);
                }
            }
        }

        /// <summary>
        /// Initialize the collection from an XML reader
        /// </summary>
        /// <param name="xmlReader">The XML reader.</param>
        /// <returns></returns>
        public static InstanceCollection FromXml(XmlReader xmlReader)
        {
            InstanceCollection instances = new InstanceCollection(EnumerateInstancesFromXml(xmlReader));
            return instances;
        }

        /// <summary>
        /// Reads instances from a XML file 
        /// </summary>
        /// <param name="xmlFilePath">The path to the XML file.</param>
        /// <returns></returns>
        public static InstanceCollection FromXmlFile(string xmlFilePath)
        {
            return FromXmlFile(xmlFilePath, new XmlReaderSettings());
        }

        /// <summary>
        /// Initialize the collection from an XML file
        /// </summary>
        /// <param name="xmlFilePath">The path to the XML file.</param>
        /// <param name="settings">The settings for XML parsing.</param>
        /// <returns></returns>
        public static InstanceCollection FromXmlFile(string xmlFilePath, XmlReaderSettings settings)
        {
            InstanceCollection instances = new InstanceCollection(EnumerateInstancesFromXmlFile(xmlFilePath, settings));
            return instances;
        }

        /// <summary>
        /// Enumerates the instances from XML.
        /// </summary>
        /// <param name="xmlReader">The XML reader.</param>
        /// <returns></returns>
        public static IEnumerable<Instance> EnumerateInstancesFromXml(XmlReader xmlReader)
        {
            while (xmlReader.Read())
            {
                if (xmlReader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                if (xmlReader.IsEmptyElement)
                {
                    // TODO should we really skip it -> it is still a valid instance, no matter that it has no attributes
                    continue;
                }

                Instance instance = Instance.FromXML(xmlReader);
                yield return instance;
            }
        }

        /// <summary>
        /// Enumerates the instances from XML file.
        /// </summary>
        /// <param name="xmlFilePath">The XML file.</param>
        /// <returns></returns>
        public static IEnumerable<Instance> EnumerateInstancesFromXmlFile(string xmlFilePath)
        {
            return EnumerateInstancesFromXmlFile(xmlFilePath, new XmlReaderSettings());
        }

        /// <summary>
        /// Enumerates the instances from XML file.
        /// </summary>
        /// <param name="xmlFilePath">The path to the XML file.</param>
        /// <param name="settings">The setting for parsing the XML.</param>
        /// <returns></returns>
        public static IEnumerable<Instance> EnumerateInstancesFromXmlFile(string xmlFilePath, XmlReaderSettings settings)
        {
            using (XmlReader xmlReader = XmlReader.Create(xmlFilePath, settings))
            {
                return EnumerateInstancesFromXml(xmlReader);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Find instances by which has attribute 'attributeName' with value equal to 'attributeValue'
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="attributeValue"></param>
        /// <returns></returns>
        public InstanceCollection Find(string attributeName, string attributeValue)
        {
            InstanceCollection result = new InstanceCollection();

            foreach (Instance instance in this)
            {
                var attribute = instance.AttributeByName(attributeName);
                if (attribute != null)
                {
                    if (attribute.Value == attributeValue)
                    {
                        result.Add(instance);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Clones the collection with its objects
        /// </summary>
        /// <returns></returns>
        public InstanceCollection Clone()
        {
            InstanceCollection result = new InstanceCollection();
            foreach (var instance in this)
            {
                result.Add(instance.CloneInstance());
            }

            return result;
        }

        #endregion

        #region Other Convert Methods

        /// <summary>
        /// Convert collection to ValidataCommon.T24RecordsCollection
        /// </summary>
        /// <returns></returns>
        public T24RecordsCollection ToT24Records()
        {
            T24RecordsCollection result = new T24RecordsCollection();

            foreach (var instance in this)
            {
                result.Add(instance.ToT24Record());
            }

            return result;
        }

        /// <summary>
        /// Creates InstanceCollection from ValidataCommon.T24RecordsCollection
        /// </summary>
        /// <param name="recordsCollection">Records collection</param>
        /// <returns>Instances Collection</returns>
        public static InstanceCollection FromT24Records(T24RecordsCollection recordsCollection)
        {
            int currentHash = 0;
            string cacheKey = String.Empty;
            if (!String.IsNullOrEmpty(recordsCollection.CatalogName) && !String.IsNullOrEmpty(recordsCollection.ApplicationName))
            {
                currentHash = recordsCollection.GetHashCode();
                cacheKey = String.Format("[{0}]:{1}", recordsCollection.CatalogName, recordsCollection.ApplicationName);
                if (_Cache.ContainsKey(cacheKey) && _Cache[cacheKey].HashCode == currentHash)
                {
                    return _Cache[cacheKey].Result;
                }
            }

            InstanceCollection result = new InstanceCollection();
            if (recordsCollection == null)
            {
                System.Diagnostics.Debug.Fail("Is it OK?");
                return result;
            }

            foreach (var record in recordsCollection.Records)
            {
                result.Add(Instance.FromT24Record(record));
            }

            if (!String.IsNullOrEmpty(cacheKey) && currentHash > 0)
            {
                _Cache[cacheKey] = new InstanceCollectionCache() { HashCode = currentHash, Result = result };
            }

            return result;
        }

        #endregion
    }
}