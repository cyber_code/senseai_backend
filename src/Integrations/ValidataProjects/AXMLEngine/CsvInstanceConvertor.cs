﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace AXMLEngine
{
    public class CsvInstanceConvertor
    {
        /// <summary>
        /// The Default Separator
        /// </summary>
        public static char DefaultSeparator = ',';

        #region Conversion from Instance to CSV

        public static string FromInstance(Instance instance, bool includeHeaders)
        {
            return FromInstance(instance, includeHeaders, DefaultSeparator);
        }

        private static void RemoveLastChar(StringBuilder sb)
        {
            if ((sb.Length - 1) > 0)
            {
                sb.Remove(sb.Length - 1, 1);
            }
        }

        public static string FromInstance(Instance instance, bool includeHeaders, char separator)
        {
            StringBuilder instanceCsv = new StringBuilder();

            if (includeHeaders)
            {
                // process Catalog & Typical Name
                AddCsvEscapedItem(instanceCsv, instance.Catalog, separator);
                AddCsvEscapedItem(instanceCsv, instance.TypicalName, separator);
                RemoveLastChar(instanceCsv);

                // process attributes
                StringBuilder attributes = new StringBuilder();
                foreach (InstanceAttribute attribute in instance.Attributes)
                {
                    AddCsvEscapedItem(attributes, attribute.Name, separator);
                }
                RemoveLastChar(attributes);

                instanceCsv.AppendLine();
                instanceCsv.Append(attributes);
                instanceCsv.AppendLine();
            }

            {   // process values
                StringBuilder values = new StringBuilder();
                foreach (InstanceAttribute attribute in instance.Attributes)
                {
                    AddCsvEscapedItem(values, attribute.Value, separator);
                }
                RemoveLastChar(values);

                instanceCsv.Append(values);
            }

            return instanceCsv.ToString();
        }

        /// <summary>
        /// Convert2s the CSV field.
        /// </summary>
        /// <param name="sb">The string builder.</param>
        /// <param name="itemText">The STR.</param>
        /// <param name="separator">The separator.</param>
        private static void AddCsvEscapedItem(StringBuilder sb, string itemText, char separator)
        {
            string s = itemText ?? "";

            if (s.IndexOfAny(new char[] {separator, '"'}) >= 0)
            {
                // We have to quote the string
                sb.Append("\"");
                sb.Append(s.Replace("\"", "\"\""));
                sb.Append("\"");
            }
            else
            {
                sb.Append(s);
            }

            sb.Append(separator);
        }

        #endregion

        #region Conversion from CSV to Instance

        public static Instance ParseInstanceFromCsv(string csv)
        {
            return ParseInstanceFromCsv(csv, DefaultSeparator);
        }

        public static Instance ParseInstanceFromCsv(string csv, char separator)
        {
            StringReader sr = new StringReader(csv);
            string typicalInfoCsv = sr.ReadLine();
            string attributeNamesCsv = sr.ReadLine();
            string attributeValuesCsv = sr.ReadLine();
            sr.Close();

            List<string> typ = ParseCsvFields(typicalInfoCsv, separator);
            List<string> attr = ParseCsvFields(attributeNamesCsv, separator);
            List<string> val = ParseCsvFields(attributeValuesCsv, separator);

            Debug.Assert(typ.Count == 2);
            Debug.Assert(attr.Count == val.Count);

            Instance instance = new Instance(typ[1], typ[0]);

            for (int i = 0; i < attr.Count; i ++)
            {
                instance.AddAttribute(attr[i], val[i]);
            }

            return instance;
        }

        /// <summary>
        /// Parses the fields and pushes the fields into the result arraylist
        /// </summary>
        /// <param name="data"></param>
        /// <param name="separator"></param>
        private static List<string> ParseCsvFields(string data, char separator)
        {
            List<string> result = new List<string>();
            int pos = -1;
            while (pos < data.Length)
            {
                result.Add(ParseCsvField(data, separator, ref pos));
            }

            return result;
        }

        /// <summary>
        /// Parses the field at the given position of the data, modified pos to match
        /// the first unparsed position and returns the parsed field
        /// </summary>
        /// <param name="data"></param>
        /// <param name="separator"></param>
        /// <param name="startSeparatorPosition"></param>
        /// <returns></returns>
        private static string ParseCsvField(string data, char separator, ref int startSeparatorPosition)
        {
            if (startSeparatorPosition == data.Length - 1)
            {
                startSeparatorPosition++;
                // The last field is empty
                return "";
            }

            int fromPos = startSeparatorPosition + 1;

            // Determine if this is a quoted field
            if (data[fromPos] == '"')
            {
                // If we're at the end of the string, let's consider this a field that
                // only contains the quote
                if (fromPos == data.Length - 1)
                {
                    return "\"";
                }

                // Otherwise, return a string of appropriate length with double quotes collapsed
                // Note that FSQ returns data.Length if no single quote was found
                int nextSingleQuote = FindSingleQuote(data, fromPos + 1);
                startSeparatorPosition = nextSingleQuote + 1;
                return data.Substring(fromPos + 1, nextSingleQuote - fromPos - 1).Replace("\"\"", "\"");
            }

            // The field ends in the next separator or EOL
            int nextSeparator = data.IndexOf(separator, fromPos);
            if (nextSeparator == -1)
            {
                startSeparatorPosition = data.Length;
                return data.Substring(fromPos);
            }
            else
            {
                startSeparatorPosition = nextSeparator;
                return data.Substring(fromPos, nextSeparator - fromPos);
            }
        }

        /// <summary>
        /// Returns the index of the next single quote mark in the string 
        /// (starting from startFrom)
        /// </summary>
        /// <param name="data"></param>
        /// <param name="startFrom"></param>
        /// <returns></returns>
        private static int FindSingleQuote(string data, int startFrom)
        {
            int i = startFrom - 1;
            while (++i < data.Length)
            {
                if (data[i] == '"')
                {
                    // If this is a double quote, bypass the chars
                    if (i < data.Length - 1 && data[i + 1] == '"')
                    {
                        i++;
                        continue;
                    }
                    else
                    {
                        return i;
                    }
                }
            }
            // If no quote found, return the end value of i (data.Length)
            return i;
        }

        #endregion
    }
}