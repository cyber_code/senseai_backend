using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using ValidataCommon;

namespace AXMLEngine
{
    public class WriteInstancesEventArgs : EventArgs
    {
        public WriteInstancesEventArgs(XmlWriter xmlWriter, AXMLDataset dataset)
        {
            this.xmlWriter = xmlWriter;
            this.dataset = dataset;
        }

        public XmlWriter xmlWriter;
        public AXMLDataset dataset;
    }

    public class ReadInstancesEventArgs : EventArgs
    {
        public ReadInstancesEventArgs(Instance instance, AXMLDataset dataset)
        {
            this.instance = instance;
            this.dataset = dataset;
        }

        public Instance instance;
        public AXMLDataset dataset;
    }

    /// <summary>
    /// Contains and manages instances and associations part of one dataset.
    /// </summary>
    [Serializable]
    public class AXMLDataset
    {
        #region Private members

        /// <summary>
        /// Adapter request instances
        /// </summary>
        private InstanceCollection _Instances;

        /// <summary>
        /// Adapter request instances
        /// </summary>
        private AssociationCollection _Associations;

        /// <summary>
        /// The id of the dataset
        /// </summary>
        private int _ID;

        /// <summary>
        /// Source Associations  - contains ArrayLists for each source
        /// </summary>
        private readonly Hashtable _SourceAssociations;

        /// <summary>
        /// Target Associations  - contains ArrayLists for each target
        /// </summary>
        private readonly Hashtable _TargetAssociations;

        private const string DATASET_TAG = "<dataset>";
        private const string CLOSE_DATASET_TAG = "</dataset>";

        private const string DATAROW_TAG = "<row id=\"0\">";
        private const string CLOSE_DATAROW_TAG = "</row>";

        private bool _PostponedWritingInstances;

        private string _ErrorsFilePath = String.Empty;

        private string _ExecutionTimesFilePath = String.Empty;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Constructs an instace of an AXML dataset
        /// </summary>
        public AXMLDataset(int datasetID)
        {
            _Instances = new InstanceCollection();
            _Associations = new AssociationCollection();
            _ID = datasetID;

            _SourceAssociations = new Hashtable();
            _TargetAssociations = new Hashtable();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Serializes the dataset to XML
        /// </summary>
        /// <param name="xmlWriter">An xmlWriter where to write the dataset.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.DATASET_ROW_TAG);
            xmlWriter.WriteAttributeString(AXMLConstants.DATASET_ATTRIBUTE_ID, _ID.ToString());
            xmlWriter.WriteAttributeString(AXMLConstants.POSTPONED_WRITING_INSTANCES,
                                           _PostponedWritingInstances.ToString());

            if (_PostponedWritingInstances)
            {
                WriteInstancesEventArgs args = new WriteInstancesEventArgs(xmlWriter, this);

                if (WriteExternalInstances != null)
                {
                    WriteExternalInstances(this, args);
                }
            }
            else
            {
                if(EnumerableInstances != null)
                {
                    foreach(Instance instance in EnumerableInstances)
                        instance.ToXML(xmlWriter);
                }

                else
                {
                    //write all instances
                    foreach (Instance inst in _Instances)
                    {
                        inst.ToXML(xmlWriter);
                    }
                }
                

                //write all associations
                foreach (XMLAssociation assoc in _Associations)
                {
                    assoc.ToXML(xmlWriter);
                }
            }

            //End of dataset
            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Fills in an AXML dataset from instance data
        /// </summary>
        /// <param name="xmlReader">The input xml reader</param>
        /// <param name="loadFlags">Options for loading</param>
        public void FromXML(XmlReader xmlReader, RequestLoadFlags loadFlags)
        {
            // Read input data and locate root typical
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:

                        if (xmlReader.IsEmptyElement)
                        {
                            continue;
                        }

                        if ((xmlReader.Name == AXMLConstants.DATASET_ROW_TAG) ||
                            (xmlReader.Name == AXMLConstants.DATASET_TAG))
                        {
                            //found the root node - just skip it
                            continue;
                        }

                        if (xmlReader.Name == AXMLConstants.ASSOCIATION_TAG)
                        {
                            using (XmlReader associationReader = xmlReader.ReadSubtree())
                            {
                                XMLAssociation assoc = XMLAssociation.FromXML(associationReader);
                                _Associations.Add(assoc);

                                // Keep the SourceAssociations and TargetAssociations hashtable up-to-date
                                RegisterSourceAndTarget(assoc);
                            }
                        }
                        else
                        {
                            Instance currentInstance = Instance.FromXML(xmlReader);

                            if (!((loadFlags & RequestLoadFlags.OnlyNotifyForReadInstances) == 0))
                            {
                                ReadInstancesEventArgs args = new ReadInstancesEventArgs(currentInstance, this);
                                if (ReadExternalInstances != null)
                                {
                                    ReadExternalInstances(this, args);
                                }
                            }
                            else
                            {
                                _Instances.Add(currentInstance);
                            }
                        }

                        break;
                    case XmlNodeType.Text:
                        break;
                    case XmlNodeType.EndElement:
                        if (xmlReader.Name == AXMLConstants.DATASET_ROW_TAG)
                        {
                            /*we have reached the end of the instances section*/
                            return;
                        }
                        else
                        {
                            Debug.Fail("Unexpected end element " + xmlReader.Name);
                        }
                        break;
                    case XmlNodeType.CDATA:
                        break;
                    case XmlNodeType.ProcessingInstruction:
                        break;
                    case XmlNodeType.Comment:
                        break;
                    case XmlNodeType.XmlDeclaration:
                        break;
                    case XmlNodeType.Document:
                        break;
                    case XmlNodeType.DocumentType:
                        break;
                    case XmlNodeType.EntityReference:
                        break;
                }
            }
        }

        /// <summary>
        /// Fills in a dataset from a multiple root string containg instances
        /// </summary>
        /// <param name="instances"></param>
        public void FromRawInstances(string instances)
        {
            StringBuilder sb = new StringBuilder(instances.Length + 200);

            sb.Append(DATASET_TAG);
            sb.Append(DATAROW_TAG);

            sb.Append(instances);

            sb.Append(CLOSE_DATAROW_TAG);
            sb.Append(CLOSE_DATASET_TAG);

            using (StringReader sr = new StringReader(sb.ToString()))
            {
                using (XmlTextReader xmlReader = new XmlTextReader(sr){Namespaces = false})
                {
                    FromXML(xmlReader, RequestLoadFlags.LoadNormal);
                }
            }
        }
 
        /// <summary>
        /// Generates VXML on the basis of the instances in this dataset.
        /// </summary>
        /// <returns>The VXML as a string.</returns>
        public string GetVXML()
        {
            using (StringWriter sw = new StringWriter())
            {
                using (XmlWriter xmlWriter = new SafeXmlTextWriter(sw))
                {
                    WriteVXML(xmlWriter);

                    string result = sw.ToString();
                    return result.Replace("utf-16", "utf-8");
                }
            }
        }

        /// <summary>
        /// Generates VXML file with the instances in this dataset.
        /// </summary>
        /// <param name="outputFile">The output file where to write the VXML.</param>
        public void WriteVXMLToFile(string outputFile)
        {
            using (StreamWriter sw = File.CreateText(outputFile))
            {
                using (XmlWriter xmlWriter = new SafeXmlTextWriter(sw))
                {
                    WriteVXML(xmlWriter);
                    sw.Flush();
                }
            }
        }

        /// <summary>
        /// Gets the instances section of the AXML as string.
        /// </summary>
        /// <returns>A string containing all instances without the INSTANCES or any other root tags.</returns>
        public string GetInstancesVXMLWithoutRootTag()
        {
            using (StringWriter sw = new StringWriter())
            {
                using (XmlWriter xmlWriter = new SafeXmlTextWriter(sw))
                {
                    foreach (Instance ins in _Instances)
                    {
                        ins.ToXML(xmlWriter);
                    }

                    sw.Flush();
                    return sw.ToString();
                }
            }
        }

        /// <summary>
        /// Adds an association to the dataset.
        /// </summary>
        /// <param name="assoc">The association to add.</param>
        public void AddAssociation(XMLAssociation assoc)
        {
            if (assoc != null)
            {
                RegisterSourceAndTarget(assoc);
                XmlAssociations.Add(assoc);
            }
        }

        /// <summary>
        /// Adds an instance to the collection.
        /// </summary>
        /// <param name="inst">The instance to add.</param>
        public void AddInstance(Instance inst)
        {
            if (inst != null)
            {
                XmlInstances.Add(inst);
            }
        }

        public void AddInstances(IEnumerable<Instance> instances)
        {
            XmlInstances.AddRange(instances);
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            _Instances.Clear();
            _Associations.Clear();
            _SourceAssociations.Clear();
            _TargetAssociations.Clear();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Adds an association to the SourceAssociations and TargetAssociations hashtables.
        /// </summary>
        /// <param name="assoc">An XMLAssociation instance.</param>
        private void RegisterSourceAndTarget(XMLAssociation assoc)
        {
            //fill the source associations
            ArrayList sourceAssoc;

            if (_SourceAssociations.ContainsKey(assoc.Source))
            {
                sourceAssoc = (ArrayList) _SourceAssociations[assoc.Source];
            }
            else
            {
                sourceAssoc = new ArrayList();
                _SourceAssociations[assoc.Source] = sourceAssoc;
            }

            sourceAssoc.Add(assoc);

            //fill the target associations
            ArrayList targetAssoc;

            if (_TargetAssociations.ContainsKey(assoc.Target))
            {
                targetAssoc = (ArrayList) _TargetAssociations[assoc.Target];
            }
            else
            {
                targetAssoc = new ArrayList();
                _TargetAssociations[assoc.Target] = targetAssoc;
            }

            targetAssoc.Add(assoc);
        }

        /// <summary>
        /// Writers the contents of this dataset to the designated XmlWriter
        /// </summary>
        /// <param name="xmlWriter"></param>
        private void WriteVXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartDocument();

            //Write validata start tag
            xmlWriter.WriteStartElement(AXMLConstants.MAIN_TAG);
            //xmlWriter.WriteAttributeString( VALIDATA_XMLNS_NAME, VALIDATA_XMLNS_VALUE );

            //write down all instances
            foreach (Instance inst in _Instances)
            {
                inst.ToXML(xmlWriter);
            }

            //write all associations
            foreach (XMLAssociation assoc in _Associations)
            {
                assoc.ToXML(xmlWriter);
            }

            //End writting Validata Tag
            xmlWriter.WriteEndElement();

            //End writting XML Document
            xmlWriter.WriteEndDocument();
        }

        #endregion

        #region Public properties		

        /// <summary>
        /// Adapter request instances
        /// </summary>
        public InstanceCollection XmlInstances
        {
            get { return _Instances; }
            set { _Instances = value; }
        }

        /// <summary>
        /// Adapter request associations
        /// </summary>
        public AssociationCollection XmlAssociations
        {
            get { return _Associations; }
            set { _Associations = value; }
        }

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        /// <summary>
        /// Source Associations - contains ArrayList for each source
        /// </summary>
        public Hashtable SourceAssociations
        {
            get { return _SourceAssociations; }
        }

        /// <summary>
        /// Target Associations - contains ArrayList for each target
        /// </summary>
        public Hashtable TargetAssociations
        {
            get { return _TargetAssociations; }
        }

        /// <summary>
        /// The instances might not be written immediately for performance reasons
        /// </summary>
        public bool PostponedWritingInstances
        {
            get { return _PostponedWritingInstances; }
            set { _PostponedWritingInstances = value; }
        }

        /// <summary>
        /// The errors file path
        /// </summary>
        public string ErrorsFilePath
        {
            get { return _ErrorsFilePath; }
            set { _ErrorsFilePath = value; }
        }        
        
        /// <summary>
        /// The execution times file path
        /// </summary>
        public string ExecutionTimesFilePath
        {
            get { return _ExecutionTimesFilePath; }
            set { _ExecutionTimesFilePath = value; }
        }

        public IEnumerable EnumerableInstances
        {
            get; set; 
        }

        #endregion

        #region Public Events

        public delegate void InstanceExternalCreationEventHandler(object dataset, WriteInstancesEventArgs fe);

        public event InstanceExternalCreationEventHandler WriteExternalInstances;

        public delegate void InstanceExternalReadEventHandler(object dataset, ReadInstancesEventArgs fe);

        public event InstanceExternalReadEventHandler ReadExternalInstances;

        #endregion
    }
}