namespace AXMLEngine
{
    /// <summary>
    /// Contains definitions of constants used throughout the AXMLEngine.
    /// </summary>
    public class AXMLConstants
    {
        #region XML tags

        /// <summary>
        /// Adapter request tag
        /// </summary>
        public const string MAIN_TAG = "Validata";

        /// <summary>
        /// Adapter request tag
        /// </summary>
        public const string ADAPTER_REQUEST_TAG = "adapterRequest";

        /// <summary>
        /// Adapter response tag
        /// </summary>
        public const string ADAPTER_RESPONSE_TAG = "adapterResponse";

        /// <summary>
        /// Tag for typical attribute
        /// </summary>
        public const string TYPICAL_ATTRIBUTE_TAG = "attribute";

        /// <summary>
        /// Request typical tag
        /// </summary>
        public const string REQUEST_TYPICAL_TAG = "requestTypical";

        /// <summary>
        /// Response typical tag
        /// </summary>
        public const string RESPONSE_TYPICAL_TAG = "responseTypical";

        /// <summary>
        /// Request assembly tag
        /// </summary>
        public const string REQUEST_ASSEMBLY_TAG = "requestAssembly";

        /// <summary>
        /// Instances tag
        /// </summary>
        public const string INSTANCES_TAG = "instances";

        /// <summary>
        /// Response assembly tag
        /// </summary>
        public const string RESPONSE_ASSEMBLY_TAG = "responseAssembly";

        /// <summary>
        /// Target System Date tag
        /// </summary>
        public const string REQUEST_TARGET_DATE = "targetSystemDate";

        #region Metadata Section

        /// <summary>
        /// Metadata tag. Encloses the section for metadata definition.
        /// </summary>
        public const string METADATA_TAG = "metadata";

        /// <summary>
        /// Catalog Tag
        /// </summary>
        public const string CATALOG_TAG = "catalogue";

        /// <summary>
        /// Typical tag
        /// </summary>
        public const string TYPICAL_TAG = "typical";

        /// <summary>
        /// Attribute tag
        /// </summary>
        public const string ATTRIBUTE_TAG = "attribute";

        /// <summary>
        /// Version tag
        /// </summary>
        public const string VERSION_TAG = "version";

        /// <summary>
        /// Version tag
        /// </summary>
        public const string VERSIONATTRIBUTE_TAG = "versionAttribute";

        #region Metadata Attribute XML attributes

        /// <summary>
        /// Attribute Type tag
        /// </summary>
        public const string ATTRIBUTE_ATTRIBUTE_TYPE = "type";

        /// <summary>
        /// Attribute Attribute Name 
        /// </summary>
        public const string ATTRIBUTE_ATTRIBUTE_NAME = "name";

        /// <summary>
        /// Attribute Attribute Remarks
        /// </summary>
        public const string ATTRIBUTE_ATTRIBUTE_REMARKS = "remarks";

        /// <summary>
        /// Attribute Attribute Data Length
        /// </summary>
        public const string ATTRIBUTE_ATTRIBUTE_DATALENGTH = "dataLength";

        #endregion

        #endregion

        #region Filters

        /// <summary>
        /// Filters tag
        /// </summary>
        public const string FILTERS_TAG = "filters";

        /// <summary>
        /// Enquiry Mask
        /// </summary>
        public const string ENQUIRY_MASK_TAG = "enquiryMask";

        /// <summary>
        /// Filter Tag
        /// </summary>
        public const string FILTER_TAG = "filter";

        /// <summary>
        /// Filter Attribute Name Tag
        /// </summary>
        public const string FILTER_ATTRIBUTE_NAME_TAG = "AttributeName";

        /// <summary>
        /// Filter Attribute Filter Tag
        /// </summary>
        public const string FILTER_ATTRIBUTE_FILTER_TAG = "AttributeFilter";

        /// <summary>
        /// Filter Attribute Data Type Tag
        /// </summary>
        public const string FILTER_ATTRIBUTE_DATA_TYPE_TAG = "AttributeDataType";

        public const string FILTER_CONDITION_TAG = "FilterCondition";

        /// <summary>
        /// Filter Logical Operation Tag
        /// </summary>
        public const string FILTER_LOGICAL_OPERATION_TAG = "logicalOperation";

        /// <summary>
        /// Is post filter tag
        /// </summary>
        public const string FILTER_IS_POST_FILTER_TAG = "IsPostFilter";

        #endregion

        #region Gateway configuration

        /// <summary>
        /// Gateway configuration Tag
        /// </summary>
        public const string GATEWAY_CONFIGURATION_TAG = "gatewayConfiguration";

        /// <summary>
        /// Gateway configuration input file Tag
        /// </summary>
        public const string CONFIGURATION_INPUTFILE_TAG = "InputFile";

        /// <summary>
        /// Gateway configuration output file Tag
        /// </summary>
        public const string CONFIGURATION_OUTPUTFILE_TAG = "OutputFile";

        /// <summary>
        /// Gateway configuration error file Tag
        /// </summary>
        public const string CONFIGURATION_ERRORSFILE_TAG = "ErrorsFile";

        /// <summary>
        /// Gateway configuration adapter name tag
        /// </summary>
        public const string CONFIGURATION_ADAPTER_NAME_TAG = "Adapter";

        /// <summary>
        /// Gateway configuration mapping schema tag
        /// </summary>
        public const string CONFIGURATION_MAPPING_SCHEMA_TAG = "MappingSchema";

        /// <summary>
        /// Gateway configuration catalog name tag
        /// </summary>
        public const string CONFIGURATION_CATALOG_NAME_TAG = "CatalogName";

        #endregion

        #region Adapter Parameters

        /// <summary>
        /// Adapter parameters tag
        /// </summary>
        public const string ADAPTER_PARAMETERS_TAG = "parameters";

        #endregion

        #region Datasets, Instances, Associations

        /// <summary>
        /// Encloses the datasets section in AXML
        /// </summary>
        public const string DATASET_TAG = "dataset";

        /// <summary>
        /// Contains a single AXML dataset
        /// </summary>
        public const string DATASET_ROW_TAG = "row";

        /// <summary>
        /// Association instance tag
        /// </summary>
        public const string ASSOCIATION_TAG = "Association";

        /// <summary>
        /// Association ID
        /// </summary>
        public const string ASSOCIATION_ID_TAG = "id";

        /// <summary>
        /// Association Name
        /// </summary>
        public const string ASSOCIATION_NAME_TAG = "Name";

        /// <summary>
        /// Association Alias
        /// </summary>
        public const string ASSOCIATION_ALIAS_TAG = "Alias";

        /// <summary>
        /// Association Source ID
        /// </summary>
        public const string ASSOCIATION_SOURCE_TAG = "SourceID";

        /// <summary>
        /// Association Target ID
        /// </summary>
        public const string ASSOCIATION_TARGET_TAG = "TargetID";

        /// <summary>
        /// ErrorSet tag
        /// </summary>
        public const string ERRORSET_TAG = "errorset";

        /// <summary>
        /// ErrorSet Row tag
        /// </summary>
        public const string ERRORSET_ROW_TAG = "row";

        /// <summary>
        /// Error object tag
        /// </summary>
        public const string ERROR_TAG = "error";

        public const string OFS_VALIDATA_TAG = "OFSMessage";

        public const string OFS_GLOBUS_ID = "GlobusID";

        public const string GENERATOR_ID_TAG = "GeneratorID";

        #endregion

        #region Performance Indicators

        /// <summary>
        /// Performance Indicators tag
        /// </summary>
        public const string PERFORMANCE_INDICATORS_SECTION_TAG = "performanceIndicators";

        /// <summary>
        /// Performance Adapter tag
        /// </summary>
        public const string PERFORMANCE_ADAPTER_TAG = "Component";

        /// <summary>
        /// Indicator tag
        /// </summary>
        public const string INDICATOR_TAG = "indicator";

        /// <summary>
        /// Member Value tag
        /// </summary>
        public const string MEMBERVALUE_TAG = "membervalue";

        #endregion


        #region Performance Indicators

        /// <summary>
        /// Name of the parent node, used in 
        /// </summary>
        public const string EXECUTION_TIMES_SECTION_TAG = "ExecutionTimes";

        public const string EXECUTION_TIMES_ROOT_ATR_NAME = "xmlns";

        public const string EXECUTION_TIMES_ROOT_ATR_VALUE = "http://tempuri.org/FinObjects.xsd";

        /// <summary>
        /// Name of the typical, used for measuring the execution timing and duration
        /// </summary>
        public const string EXECUTION_TIME_TAG = "ExecutionTime";

        public const string EXECUTION_TIME_CATALOG_ATR_NAME = "Catalog";

        public const string EXECUTION_TIME_CATALOG = "NavigatorCatalog";

        public const string INSTANCE_ID_ATTR = "ID";

        public const string OPERATION_START_TIME_ATTR = "StartTime";

        public const string OPERATION_END_TIME_ATTR = "EndTime";

        public const string OPERATION_DURATION = "Duration";

        #endregion

        #endregion

        #region XML attributes

        #region Adapter Request

        /// <summary>
        /// Adapter request ID
        /// </summary>
        public const string ADAPTER_REQUEST_ID = "ID";

        /// <summary>
        /// Adapter request type
        /// </summary>
        public const string ADAPTER_REQUEST_TYPE = "type";

        /// <summary>
        /// Adapter request action
        /// </summary>
        public const string ADAPTER_REQUEST_ACTION = "action";

        #endregion

        #region Catalog

        /// <summary>
        /// Catalog Name
        /// </summary>
        public const string CATALOG_NAME_ATTRIBUTE = "name";

        #endregion

        #region Typical

        /// <summary>
        /// Typical Name
        /// </summary>
        public const string TYPICAL_NAME_ATTRIBUTE = "name";

        /// <summary>
        /// Typical ID
        /// </summary>
        public const string TYPICAL_ID_ATTRIBUTE = "typicalID";

        /// <summary>
        /// Typical Base class
        /// </summary>
        public const string TYPICAL_BASECLASS_ATTRIBUTE = "baseclass";

        #endregion

        #region Typical Attribute

        /// <summary>
        /// Typical attribute ID 
        /// </summary>
        public const string TYPICAL_ATTRIBUTE_ID = "id";

        /// <summary>
        /// Typical attribute base class
        /// </summary>
        public const string TYPICAL_ATTRIBUTE_BASECLASS = "baseclass";

        /// <summary>
        /// Typical Attribute Name
        /// </summary>
        public const string TYPICAL_ATTRIBUTE_NAME = "name";

        /// <summary>
        /// Typical Attribute Type
        /// </summary>
        public const string TYPICAL_ATTRIBUTE_TYPE = "type";

        /// <summary>
        /// Typical Attribute Type
        /// </summary>
        public const string TYPICAL_ATTRIBUTE_REMARKS = "remarks";

        /// <summary>
        /// Typical Attribute Type
        /// </summary>
        public const string TYPICAL_ATTRIBUTE_DATALENGTH = "dataLength";

        #endregion

        #region Datasets, Instances and Associations

        /// <summary>
        /// Catalog attribute for Instance
        /// </summary>
        public const string INSTANCE_ATTRIBUTE_CATALOG = "Catalog";

        /// <summary>
        /// ID attribute for AXML dataset
        /// </summary>
        public const string DATASET_ATTRIBUTE_ID = "id";

        /// <summary>
        /// ID attribute for AXML dataset
        /// </summary>
        public const string ERRORSET_ATTRIBUTE_ID = "id";

        /// <summary>
        /// TYPE attribute for AXML dataset
        /// </summary>
        public const string ERRORSET_ATTRIBUTE_TYPE = "type";

        /// <summary>
        /// Postepond writing instances attribute for AXML dataset
        /// </summary>
        public const string POSTPONED_WRITING_INSTANCES = "PostponedWritingInstances";

        /// <summary>
        /// ID attribute for AXML dataset
        /// </summary>
        public const string REQUEST_ATTRIBUTE_ID = "RequestID";

        public const string REQUEST_EXECUTION_STATE = "RequestExeState";

        public const string REQUEST_AFTER_EXECUTION_STATE = "RequestAfterExeState";

        public const string REQUEST_ERROR_STATE = "RequestErrorState";


        #endregion

        #region General

        /// <summary>
        /// Attribute for Instance
        /// </summary>
        public const string VALIDATA_NAMESPACE_ATTRIBUTE = "xmlns";

        /// <summary>
        /// Attribute for Instance
        /// </summary>
        public const string VALIDATA_VERSION_ATTRIBUTE = "version";

        /// <summary>
        /// Validata namespace
        /// </summary>
        public const string VALIDATA_NAMESPACE = "www.validata.gr";

        /// <summary>
        /// Validata version
        /// </summary>
        public const string VALIDATA_VERSION = "1.0.0";

        #endregion

        #region Filters

        /// <summary>
        /// Logical Operation Attribute Name
        /// </summary>
        public const string FILTER_LOGICAL_OPERATION_ATTRIBUTE = "logicalOperation";

        #endregion

        #region Performance Indicators

        /// <summary>
        /// Performance Adapter Name
        /// </summary>
        public const string PERFORMANCE_ADAPTER_NAME_ATTRIBUTE = "Name";

        /// <summary>
        /// Performance Adapter Name
        /// </summary>
        public const string PERFORMANCE_ADAPTER_URL_ATTRIBUTE = "url";

        /// <summary>
        /// Performance Adapter Name
        /// </summary>
        public const string PERFORMANCE_ADAPTER_PORT_ATTRIBUTE = "port";

        /// <summary>
        /// Indicator Name
        /// </summary>
        public const string INDICATOR_NAME_ATTRIBUTE = "Name";

        /// <summary>
        /// Value of the member
        /// </summary>
        public const string MEMBER_VALUE_ATTRIBUTE = "value";

        /// <summary>
        /// Member Function
        /// </summary>
        public const string MEMBER_FUNCTION_ATTRIBUTE = "function";

        #endregion

        #endregion
    }
}