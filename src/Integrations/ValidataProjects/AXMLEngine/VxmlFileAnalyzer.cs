using System.Collections.Generic;
using System.IO;

namespace AXMLEngine
{
    /// <summary>
    /// Analyzer of a VXML file (currently only count the number of instances)
    /// </summary>
    public static class VxmlFileAnalyzer
    {
        #region Nested Classes

        /// <summary>
        /// Typical stats
        /// </summary>
        public class TypicalStats
        {
            public ulong CountOfInstances;
        }

        /// <summary>
        /// Stats from a VXML file
        /// </summary>
        public class VxmlFileStats
        {
            private readonly Dictionary<string, TypicalStats> _TypicalStats = new Dictionary<string, TypicalStats>();

            /// <summary>
            /// Gets the typical stats.
            /// </summary>
            /// <param name="catalogName">Name of the catalog.</param>
            /// <param name="typicalName">Name of the typical.</param>
            /// <returns></returns>
            public TypicalStats GetTypicalStats(string catalogName, string typicalName)
            {
                string key = GetKey(catalogName, typicalName);
                return _TypicalStats.ContainsKey(key) ? _TypicalStats[key] : null;
            }

            /// <summary>
            /// Gets the total instances count.
            /// </summary>
            /// <value>The total instances count.</value>
            public ulong TotalInstancesCount
            {
                get
                {
                    ulong count = 0;
                    foreach (TypicalStats tStat in _TypicalStats.Values)
                    {
                        count += tStat.CountOfInstances;
                    }
                    return count;
                }
            }

            private void IncrementCount(string catalogName, string typicalName)
            {
                string key = GetKey(catalogName, typicalName);

                TypicalStats stats;
                if (_TypicalStats.ContainsKey(key))
                {
                    stats = _TypicalStats[key];
                }
                else
                {
                    stats = new TypicalStats();
                    _TypicalStats.Add(key, stats);
                }

                stats.CountOfInstances++;
            }

            private static string GetKey(string catalogName, string typicalName)
            {
                return "[" + catalogName + "]:" + typicalName;
            }

            /// <summary>
            /// Processes the instance.
            /// </summary>
            /// <param name="instance">The instance.</param>
            public void ProcessInstance(Instance instance)
            {
                IncrementCount(instance.Catalog, instance.TypicalName);
            }
        }

        #endregion

        /// <summary>
        /// Reads the VXML file (we expect it to have a start element)
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public static VxmlFileStats AnalyzeVxmlFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return null;
            }

            VxmlFileStats stats = new VxmlFileStats();
            foreach (Instance inst in VxmlFileInstancesEnumerator.EnumerateInstances(filePath))
            {
                stats.ProcessInstance(inst);
            }
            return stats;
        }
    }
}