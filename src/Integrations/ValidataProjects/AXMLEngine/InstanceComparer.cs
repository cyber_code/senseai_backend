﻿//using System.Collections.Generic;
//using System.Text;

//namespace AXMLEngine
//{
//    /// <summary>
//    /// Compares attributes of two instances
//    /// </summary>
//    public class InstanceAttributesComparer
//    {
//        public static InstanceAttributesComparisonResult Compare(Instance first, Instance second)
//        {
//            return Compare(first.Attributes, second.Attributes);
//        }

//        public static InstanceAttributesComparisonResult Compare(
//            InstanceAttributesCollection first, InstanceAttributesCollection second)
//        {
//            InstanceAttributesComparisonResult result = new InstanceAttributesComparisonResult();

//            InstanceAttributesCollection firstSorted = InstanceAttributesCollection.GetSortedCopy(first);
//            InstanceAttributesCollection secondSorted = InstanceAttributesCollection.GetSortedCopy(second);

//            int posInFirst = 0;
//            int posInSecond = 0;

//            while (posInFirst < firstSorted.Count && posInSecond < secondSorted.Count)
//            {
//                InstanceAttribute firstAttribute = firstSorted[posInFirst];
//                InstanceAttribute secondAttribute = secondSorted[posInSecond];

//                switch (string.Compare(firstAttribute.Name, secondAttribute.Name))
//                {
//                    case 0:
//                        if (firstAttribute.Value == secondAttribute.Value)
//                        {
//                            result.AddIdentical(firstAttribute.Name, firstAttribute.Value);
//                        }
//                        else
//                        {
//                            result.AddDifferent(firstAttribute.Name, firstAttribute.Value, secondAttribute.Value);
//                        }
//                        posInFirst++;
//                        posInSecond++;
//                        break;
//                    case -1:
//                        result.AddOnlyInFirst(firstAttribute.Name, firstAttribute.Value);
//                        posInFirst++;
//                        break;
//                    case 1:
//                        result.AddOnlyInSecond(secondAttribute.Name, secondAttribute.Value);
//                        posInSecond++;
//                        break;
//                }
//            }

//            // AddRemainingUnmatchedAttributes for first instance
//            for (int i = posInFirst; i < firstSorted.Count; i++)
//            {
//                result.AddOnlyInFirst(firstSorted[i].Name, firstSorted[i].Value);
//            }

//            // AddRemainingUnmatchedAttributes for second instance
//            for (int i = posInSecond; i < secondSorted.Count; i++)
//            {
//                result.AddOnlyInSecond(secondSorted[i].Name, secondSorted[i].Value);
//            }

//            return result;
//        }
//    }

//    /// <summary>
//    /// Results of comparison of attributes of two instances
//    /// </summary>
//    public class InstanceAttributesComparisonResult
//    {
//        #region Nested classes

//        public struct DistinctValues
//        {
//            public string FirstValue;
//            public string SecondValue;

//            public DistinctValues(string firstValue, string secondValue)
//            {
//                FirstValue = firstValue;
//                SecondValue = secondValue;
//            }
//        }

//        #endregion

//        #region Members

//        public Dictionary<string, string> Identical = new Dictionary<string, string>();
//        public Dictionary<string, DistinctValues> DifferentValues = new Dictionary<string, DistinctValues>();
//        public Dictionary<string, string> OnlyInFirst = new Dictionary<string, string>();
//        public Dictionary<string, string> OnlyInSecond = new Dictionary<string, string>();

//        #endregion

//        #region Adding items

//        public void AddDifferent(string attributeName, string firstValue, string secondValue)
//        {
//            DifferentValues.Add(attributeName, new DistinctValues(firstValue, secondValue));
//        }

//        public void AddIdentical(string attributeName, string attributeValue)
//        {
//            Identical.Add(attributeName, attributeValue);
//        }

//        public void AddOnlyInFirst(string attributeName, string attributeValue)
//        {
//            if (attributeValue != "")
//            {
//                OnlyInFirst.Add(attributeName, attributeValue);
//            }
//        }

//        public void AddOnlyInSecond(string attributeName, string attributeValue)
//        {
//            if (attributeValue != "")
//            {
//                OnlyInSecond.Add(attributeName, attributeValue);
//            }
//        }

//        #endregion

//        #region Getting result as detailed description

//        public string GetDifferencesText()
//        {
//            if (DifferentValues.Count == 0)
//            {
//                return "";
//            }

//            StringBuilder builder = new StringBuilder();

//            builder.AppendFormat("{0} different attribute values:", DifferentValues.Count);
//            builder.AppendLine();

//            foreach (KeyValuePair<string, DistinctValues> diff in DifferentValues)
//            {
//                builder.AppendFormat("[{0}] : [First Instance] {1} [Second Instance] {2}",
//                                     diff.Key, diff.Value.FirstValue, diff.Value.SecondValue);
//                builder.AppendLine();
//            }

//            return builder.ToString();
//        }

//        public string GetAllDissimilaritiesText()
//        {
//            if (IsCompletetelyIdentical)
//            {
//                return "";
//            }
//            else
//            {
//                return GetDifferencesText() + GetUniqueForBothText();
//            }
//        }

//        public string GetUniqueForBothText()
//        {
//            return GetOnlyInFirstText() + GetOnlyInSecondText();
//        }

//        public string GetOnlyInFirstText()
//        {
//            return GetOnlyInOneOfTheTwoText(OnlyInFirst, true);
//        }

//        public string GetOnlyInSecondText()
//        {
//            return GetOnlyInOneOfTheTwoText(OnlyInSecond, false);
//        }

//        private static string GetOnlyInOneOfTheTwoText(Dictionary<string, string> items, bool isFirst)
//        {
//            if (items.Count == 0)
//            {
//                return "";
//            }

//            StringBuilder builder = new StringBuilder();

//            builder.AppendFormat("Attributes present only in {0} instance:", isFirst ? "first" : "second");
//            builder.AppendLine();

//            foreach (KeyValuePair<string, string> item in items)
//            {
//                builder.AppendFormat("[{0}] : {1}", item.Key, item.Value);
//                builder.AppendLine();
//            }

//            return builder.ToString();
//        }

//        #endregion

//        public bool IsCompletetelyIdentical
//        {
//            get { return DifferentValues.Count == 0 && OnlyInFirst.Count == 0 && OnlyInSecond.Count == 0; }
//        }
//    }
//}