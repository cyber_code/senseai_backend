using System.Xml;

namespace AXMLEngine
{
    /// <summary>
    /// Provides implementations of common XML tasks
    /// </summary>
    internal class XmlUtils
    {
        /// <summary>
        /// Reads to the end of a specified section.
        /// </summary>
        /// <param name="xmlReader">The XMLTextReader used for the current read operation</param>
        /// <param name="sectionName">The name of the section which is to be skipped.</param>
        public static void SkipSection(XmlTextReader xmlReader, string sectionName)
        {
            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == XmlNodeType.EndElement)
                {
                    if (xmlReader.Name == sectionName)
                    {
                        return;
                    }
                }
            }

            return;
        }
    }
}