using System;
using System.Collections;
using System.IO;
using System.Xml;
using Validata.Common;

namespace AXMLEngine
{
    /// <summary>
    /// Metadata.
    /// </summary>
    [Serializable]
    public class Metadata
    {
        #region Private And Protected Members

        /// <summary>
        /// Metadata Attributes
        /// </summary>
        private Hashtable _Attributes;

        /// <summary>
        /// Metadata Typicals
        /// </summary>
        private TypicalCollection _Typicals;

        #endregion;

        #region Public Properties

        /// <summary>
        /// Metadata Attributes
        /// </summary>
        public Hashtable Attributes
        {
            get { return _Attributes; }
            set { _Attributes = value; }
        }

        /// <summary>
        /// Metradata Typicals
        /// </summary>
        public TypicalCollection Typicals
        {
            get { return _Typicals; }
            set { _Typicals = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Finds the typical by name and ID.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public MetadataTypical GetTypicalByNameAndID(string name, string id)
        {
            foreach(MetadataTypical typical in Typicals)
            {
                if(typical.Name == name && typical.ID == id)
                {
                    return typical;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds the typical by catalog name and typical name
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public MetadataTypical GetTypicalByName(string catalogName, string typicalName)
        {
            foreach (MetadataTypical typical in Typicals)
            {
                if (typical.CatalogName == catalogName)
                if (typical.Name == typicalName)
                {
                    return typical;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the best typical match
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public MetadataTypical GetBestTypicalMatch(string name, string id)
        {
            MetadataTypical match = GetTypicalByNameAndID(name, id);
            if (match != null)
                return match;

            return GetTypicalByName(name);
        }

        private MetadataTypical GetTypicalByName(string name)
        {
            foreach (MetadataTypical typical in Typicals)
            {
                if (typical.Name == name)
                {
                    return typical;
                }
            }

            return null;
        }

        /// <summary>
        /// Adds metadata attribute to attributes collection
        /// </summary>
        /// <param name="id">Attribute ID</param>
        /// <param name="name">Attribute Name</param>
        /// <param name="type">Attribute Type</param>
        /// <returns>If succeeded returns true, otherwise - false</returns>
        public bool AddAttribute(string id, string name, AttributeDataType type)
        {
            if (id != null && name != null)
            {
                MetadataAttribute mAttr = new MetadataAttribute();
                mAttr.ID = id;
                mAttr.Name = name;
                mAttr.Type = type;

                _Attributes.Add(id, mAttr);

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Adds metadata attribute to attributes collection
        /// </summary>
        /// <param name="attr">Metadata attribute</param>
        /// <returns>If succeeded returns true, otherwise - false</returns>
        public bool AddAttribute(MetadataAttribute attr)
        {
            if (attr != null)
            {
                _Attributes.Add(attr.ID, attr);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Reads the metadata section of the Adapter request
        /// </summary>
        /// <param name="metaData">The contents of the metadata section</param>
        /// <returns>False if an exce</returns>
        public void FromXML(string metaData)
        {
            using(StringReader sr = new StringReader(metaData))
            using (XmlTextReader tr = new XmlTextReader(sr) { Namespaces = false })
            {
                FromXML(tr);
            }
        }

        /// <summary>
        /// Reads the metadata section of the Adapter request
        /// </summary>
        /// <param name="metaReader">The contents of the metadata section</param>
        /// <returns>False if an exce</returns>
        public void FromXML(XmlReader metaReader)
        {
            // Init reader
            string currentCatalog = "";
            while (metaReader.Read())
            {
                if (metaReader.NodeType == XmlNodeType.Element)
                {
                    if (metaReader.Name == AXMLConstants.CATALOG_TAG)
                    {
                        //catalog node
                        currentCatalog = metaReader.GetAttribute(AXMLConstants.CATALOG_NAME_ATTRIBUTE);
                    }
                    else if (metaReader.Name == AXMLConstants.TYPICAL_TAG)
                    {
                        //typical node
                        using (XmlReader metaTypicalReader = metaReader.ReadSubtree())
                        {
                            MetadataTypical typDef = MetadataTypical.FromXML(metaTypicalReader, currentCatalog);
                            _Typicals.Add(typDef);
                        }
                    }
                }
                else
                {
                    switch (metaReader.NodeType)
                    {
                        case XmlNodeType.EndElement:
                            if (metaReader.Name == AXMLConstants.METADATA_TAG)
                            {
                                return;
                            }
                            break;
                    }
                }
            }

        }

        /// <summary>
        /// Exports Metadata Objects to XML
        /// </summary>
        /// <param name="xmlWriter">An XmlWriter where the output is written to.</param>
        /// <returns>Returns Generated XML</returns>
        public void ToXML(XmlWriter xmlWriter)
        {
            //Write validata start tag
            xmlWriter.WriteStartElement(AXMLConstants.METADATA_TAG);

            //Generate Catalogues XML
            //Genarate Typicals XML
            GenerateTypicalsXML(xmlWriter);

            //End writting Metadata Tag
            xmlWriter.WriteEndElement();
            xmlWriter.Flush();
        }

        #endregion

        #region Class Lifecycle

        public Metadata()
        {
            _Attributes = new Hashtable();
            _Typicals = new TypicalCollection();
        }

        #endregion

        #region Private And Protected Methods

        /// <summary>
        /// Generates Metadata Typicals XML
        /// </summary>
        /// <param name="xmlWriter">The XmlWriter to which the output is written.</param>
        private void GenerateTypicalsXML(XmlWriter xmlWriter)
        {
            Hashtable catalogs = new Hashtable();

            foreach (MetadataTypical metadataTypical in _Typicals)
            {
                if (catalogs.ContainsKey(metadataTypical.CatalogName))
                {
                    ArrayList typicals = (ArrayList) catalogs[metadataTypical.CatalogName];
                    typicals.Add(metadataTypical);
                    catalogs[metadataTypical.CatalogName] = typicals;
                }
                else
                {
                    ArrayList typicals = new ArrayList();
                    typicals.Add(metadataTypical);
                    catalogs[metadataTypical.CatalogName] = typicals;
                }
            }

            foreach (string catalogName in catalogs.Keys)
            {
                xmlWriter.WriteStartElement(AXMLConstants.CATALOG_TAG);
                xmlWriter.WriteAttributeString(AXMLConstants.CATALOG_NAME_ATTRIBUTE, catalogName);

                ArrayList typicals = (ArrayList) catalogs[catalogName];

                foreach (MetadataTypical mdt in typicals)
                {
                    mdt.ToXML(xmlWriter);
                }

                xmlWriter.WriteEndElement();
                xmlWriter.Flush();
            }
        }

        #endregion
    }
}