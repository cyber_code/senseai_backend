using System;
using System.IO;
using System.Text;
using System.Xml;
using ValidataCommon;

namespace AXMLEngine
{
    /// <summary>
    /// An ErrorSet object contains errors connected to the Dataset with the same ID.
    /// </summary>
    [Serializable]
    public class ErrorSet
    {
        #region Private members

        private int _ID;
        private ErrorCollection _Errors;
        private string _ErrorsFilePath = String.Empty;

        #endregion

        public delegate void ErrorReadDelegate(Error error);
        public event ErrorReadDelegate OnErrorRead;

        #region Private constants

        private const string ERRORSET_ROW_TAG = "<row id=\"0\">";
        private const string CLOSE_ERRORSET_ROW_TAG = "</row>";

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Constructs an instance of an AXML ErrorSet
        /// </summary>
        public ErrorSet(int errorSetID)
        {
            _ID = errorSetID;
            _Errors = new ErrorCollection();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Serializes the errorSet to XML.
        /// </summary>
        /// <param name="xmlWriter">The XmlWriter object to write to.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.ERRORSET_ROW_TAG);
            xmlWriter.WriteAttributeString(AXMLConstants.ERRORSET_ATTRIBUTE_ID, _ID.ToString());

            if (_ErrorsFilePath != String.Empty)
            {
                if (File.Exists(_ErrorsFilePath))
                {
                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.ConformanceLevel = ConformanceLevel.Fragment;
                    settings.IgnoreWhitespace = true;
                    settings.IgnoreComments = true;
                    using (XmlReader reader = XmlReader.Create(_ErrorsFilePath, settings))
                    {
                        if (reader.Read())
                        {
                            while (true)
                            {
                                reader.MoveToContent();
                                xmlWriter.WriteRaw("\n\t");
                                xmlWriter.WriteRaw(reader.ReadOuterXml());
                                if (reader.EOF)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (Error error in _Errors)
                {
                    error.ToXML(xmlWriter);
                }
            }

            //end of error set
            xmlWriter.WriteEndElement();
            xmlWriter.Flush();
        }

        /// <summary>
        /// Fills in the error set from the given XML.
        /// </summary>
        /// <param name="errorsReader">An XML reader.</param>
        /// <param name="dontLoadInMemory"></param>
        public void FromXML(XmlReader errorsReader, bool dontLoadInMemory)
        {
            // Read input data and locate root typical
            while (errorsReader.Read())
            {
                if (errorsReader.NodeType == XmlNodeType.Element)
                {
                    if ((errorsReader.Name == AXMLConstants.ERRORSET_TAG) ||
                        (errorsReader.Name == AXMLConstants.ERRORSET_ROW_TAG))
                    {
                        //this is the root tag - skip it
                        continue;
                    }
                    else if (errorsReader.Name == AXMLConstants.ERROR_TAG)
                    {
                        using (XmlReader singleErrorReader = errorsReader.ReadSubtree())
                        {
                            Error error = Error.FromXML(singleErrorReader);
                            
                            // notify
                            if (OnErrorRead != null)
                            {
                                OnErrorRead(error);
                            }

                            // for performance reasons we might not want to 
                            if(! dontLoadInMemory)
                            {
                                _Errors.Add(error);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Fills in the error set from the given XML.
        /// </summary>
        /// <param name="errors">An XML string.</param>
        public void FromRawErrors(string errors)
        {
            StringBuilder sb = new StringBuilder(errors.Length + 200);

            sb.Append(ERRORSET_ROW_TAG);
            sb.Append(errors);
            sb.Append(CLOSE_ERRORSET_ROW_TAG);

            using (StringReader sr = new StringReader(sb.ToString()))
            {
                using (XmlTextReader xmlReader = new XmlTextReader(sr) { Namespaces = false })
                {
                    FromXML(xmlReader, false);
                }
            }
        }

        /// <summary>
        /// Adds an error with this message to the ErrorSet
        /// </summary>
        /// <param name="error">The error object</param>
        public void AddError(Error error)
        {
            _Errors.Add(error);
        }

        /// <summary>
        /// Gets the instances section of the AXML as string.
        /// </summary>
        /// <returns>A string containing all instances without the INSTANCES or any other root tags.</returns>
        public string GetInstancesVXMLWithoutRootTag()
        {
            using (StringWriter sw = new StringWriter())
            {
                using (XmlWriter xmlWriter = new SafeXmlTextWriter(sw))
                {
                    if (_Errors != null)
                    {
                        foreach (Error err in _Errors)
                        {
                            err.GetInstance().ToXML(xmlWriter);
                        }
                    }

                    sw.Flush();
                    return sw.ToString();
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Erros
        /// </summary>
        public ErrorCollection Errors
        {
            get { return _Errors; }
        }

        /// <summary>
        /// Errorset errors file path
        /// </summary>
        public string ErrorsFilePath
        {
            get { return _ErrorsFilePath; }
            set { _ErrorsFilePath = value; }
        }

        #endregion
    }
}