//===============================================================================
// AdapterParameters.cs
//===============================================================================
//
// Adapter parameters.
//
// History:
//	Version:                Date:                   Author:
//	1.00                    15/12/2004              Ilian
//	Description: Created initial version
//
//===============================================================================
// Copyright (C) 2004-2006 Theseus Technology Partners
// All rights reserved.
//===============================================================================

using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AXMLEngine
{
    /// <summary>
    /// Collection of AdapterParameters.
    /// </summary>
    [Serializable]
    public class AdapterParameters : IXmlSerializable
    {
        #region Constants and Enumerations

        /// <summary>
        /// Parameters tag
        /// </summary>
        private const string PARAMETERS_TAG = "parameters";

        #endregion

        #region Private And Protected Members

        /// <summary>
        /// Adapter name
        /// </summary>
        private string _AdapterName;

        /// <summary>
        /// Adapter parameters
        /// </summary>
        private Hashtable _Parameters;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default constructor
        /// </summary>
        public AdapterParameters()
        {
            _Parameters = new Hashtable();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Adapter Name
        /// </summary>
        public string AdapterName
        {
            get { return _AdapterName; }
            set { _AdapterName = value; }
        }

        /// <summary>
        /// Adapter parameters
        /// </summary>
        public Hashtable Parameters
        {
            get { return _Parameters; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds parameter to parameters collection
        /// </summary>
        /// <param name="name">Parameter name</param>
        /// <param name="val">Parameter value</param>
        /// <returns>If succeeded returns true, otherwise - false.</returns>
        public bool AddParameter(string name, string val)
        {
            if (name != null && val != null)
            {
                AdapterParameter ap = new AdapterParameter(name, val);
                _Parameters[name] = ap;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Adds parameter to parameters collection
        /// </summary>
        /// <param name="ap">Adapter parameter</param>
        /// <returns>If succeeded returns true, otherwise - false</returns>
        public bool AddParameter(AdapterParameter ap)
        {
            if (ap != null)
            {
                _Parameters[ap.Name] = ap;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the parameter value.
        /// </summary>
        /// <param name="paramName">Name of the param.</param>
        /// <returns></returns>
        public string GetParameterValue(string paramName)
        {
            AdapterParameter parameter = (AdapterParameter)_Parameters[paramName];
            return parameter != null ? parameter.Value : null;
        }

        /// <summary>
        /// Create Adapter Parameters object from XML string
        /// </summary>
        /// <param name="xmlReader">Source XML string</param>
        /// <returns>If succeeded return true, otherwise - false</returns>
        public bool FromXML(XmlReader xmlReader)
        {
            bool foundRootNode = false;

            // Read input data and locate root typical
            bool nodeIsRead = false;
            while (nodeIsRead || xmlReader.Read())
            {
                // ReadOuterXML operations cause the next node to be loaded so we need to skip reading 
                if (nodeIsRead)
                {
                    nodeIsRead = false;
                }

                // Cycle trough XML Elements
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                        // Analyze incoming elements
                        if (xmlReader.Name == PARAMETERS_TAG)
                        {
                            foundRootNode = true;
                            continue;
                        }

                        if (foundRootNode)
                        {
                            //Set node name
                            _AdapterName = xmlReader.Name;
                            ReadParameters(xmlReader.ReadOuterXml());
                            nodeIsRead = true;
                        }
                        break;
                }
            }

            return foundRootNode;
        }

        /// <summary>
        /// Generate Adapter Parameters XML string
        /// </summary>
        /// <param name="xmlWriter">Open xmlWriter where to output.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.ADAPTER_PARAMETERS_TAG);

            if (!string.IsNullOrEmpty(_AdapterName))
            {
                xmlWriter.WriteStartElement(_AdapterName);

                foreach (AdapterParameter ap in _Parameters.Values)
                {
                    if (ap.Name == "LOAD_ID")
                        continue;
                    xmlWriter.WriteElementString(ap.Name, ap.Value);
                }

                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();

            xmlWriter.Flush();
        }

        #endregion

        #region Private And Protected Members

        /// <summary>
        /// Read Parameters from xml string
        /// </summary>
        /// <param name="xml">Source XML string</param>
        /// <returns>If succeeded returns true, otherwise - false</returns>
        private void ReadParameters(string xml)
        {
            using (StringReader sReader = new StringReader(xml))
            {
                using (XmlTextReader xmlReader = new XmlTextReader(sReader) { Namespaces = false })
                {
                    bool foundRootElement = false;
                    string name = null;

                    // Read input data and locate root typical
                    while (xmlReader.Read())
                    {
                        if (xmlReader.Name == _AdapterName)
                        {
                            foundRootElement = true;
                            continue;
                        }

                        if (foundRootElement)
                        {
                            // Cycle trough XML Elements
                            switch (xmlReader.NodeType)
                            {
                                case XmlNodeType.Element:
                                    {
                                        // Read Adapter Parameter Name
                                        name = xmlReader.Name;
                                        break;
                                    }
                                case XmlNodeType.Text:
                                    {
                                        // Add readed parameter to parameters collection
                                        _Parameters.Add(name, new AdapterParameter(name, xmlReader.Value));
                                        break;
                                    }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (AdapterParameter parameter in _Parameters.Values)
            {
                sb.AppendLine(parameter.ToString());
            }
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(AdapterParameters)) return false;
            return Equals((AdapterParameters)obj);
        }

        public bool Equals(AdapterParameters other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            if (AdapterName != other.AdapterName)
                return false;

            foreach (var key in other.Parameters.Keys)
            {
                if (key.ToString() == "ID")
                    continue;
                if (key.ToString() == "LOAD_ID")
                    continue;
                if (Parameters.ContainsKey(key))
                {
                    if (!other.Parameters[key].Equals(Parameters[key]))
                        return false;
                }
                else
                {
                    if (!other.Parameters[key].Equals(new AdapterParameter(key.ToString(), string.Empty)))
                        return false;
                }
            }

            foreach (var key in Parameters.Keys)
            {
                if (key.ToString() == "LOAD_ID")
                    continue;
                if (key.ToString() == "ID")
                    continue;
                if (other.Parameters.ContainsKey(key))
                {
                    if (!Parameters[key].Equals(other.Parameters[key]))
                        return false;
                }
                else
                {
                    if (!Parameters[key].Equals(new AdapterParameter(key.ToString(), string.Empty)))
                        return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((_AdapterName != null ? _AdapterName.GetHashCode() : 0)*397) ^ (_Parameters != null ? _Parameters.GetHashCode() : 0);
            }
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            using (TextReader textReader = new StringReader(reader.ReadOuterXml()))
            {
                using (XmlReader xmlReader = XmlReader.Create(textReader))
                {
                    FromXML(xmlReader);
                }
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            ToXML(writer);
        }
    }
}