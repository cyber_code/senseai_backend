using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace AXMLEngine
{
    public class AdaptersXMLParser
    {
        #region Private And Protected Members

        /// <summary>
        /// Adapter XSD file Path
        /// </summary>
        private string _XSDFilePath;

        /// <summary>
        /// Dataset storage for Adapter XSD file
        /// </summary>
        private DataSet _DataSet;

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default Class constructor
        /// </summary>
        /// <param name="xsdFilePath">Full path to last version of Adapter XSD schema</param>
        public AdaptersXMLParser(string xsdFilePath)
        {
            _XSDFilePath = xsdFilePath;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Generate Validata XML from Adapter XML
        /// </summary>
        /// <param name="AdapterXML">Adapter XML</param>
        /// <returns>Returns generated Validata XML</returns>
        public string GetVXML(string AdapterXML)
        {
            if (_XSDFilePath != String.Empty)
            {
                //Load XML Schema
                LoadXSDFile();

                //Create empty lists for expected typicals
                ArrayList commands = new ArrayList();
                ArrayList responses = new ArrayList();
                ArrayList errors = new ArrayList();

                //Full created lists
                BuildLists(_DataSet, commands, responses, errors);

                return GetMatchingEntries(AdapterXML, commands, responses, errors);
            }
            else
            {
                string instances = GetInstancesOnly(AdapterXML);

                if (instances == string.Empty)
                {
                    instances = GetInstancesOnlyNoInstancesTag(AdapterXML);
                }

                return instances;
            }
        }

        #endregion

        #region Private And Protected Methods

        /// <summary>
        /// Loads Adapter XSD file and creates Dataset from it.
        /// </summary>
        /// <returns>Returns true if succeed</returns>
        private void LoadXSDFile()
        {
            _DataSet = new DataSet();
            _DataSet.ReadXmlSchema(_XSDFilePath);
        }

        /// <summary>
        /// Process XSD DataSet and Build lists with valid commands, responses and errors
        /// </summary>
        /// <param name="ds">Target XSD DataSet</param>
        /// <param name="commands">Returned Valid Adapter Commands</param>
        /// <param name="responses">Returned Valid Adapter Responses</param>
        /// <param name="errors">Returned Valid Adapter Errors</param>
        private static void BuildLists(DataSet ds,
                                       ArrayList commands, ArrayList responses, ArrayList errors)
        {
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    switch (dc.ColumnName)
                    {
                        case "COMMAND":
                            commands.Add(dt.TableName);
                            break;
                        case "RESPONSE":
                            responses.Add(dt.TableName);
                            break;
                        case "ERROR":
                            errors.Add(dt.TableName);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Process Source Adapter XML, extract valid Adapter Entries and build Validata XML
        /// </summary>
        /// <param name="xmlData">Source XML Data</param>
        /// <param name="commands">List with valid Adapter commands</param>
        /// <param name="responses">List with valid Adapter responses</param>
        /// <param name="errors">List with valid Adapter errors</param>
        /// <returns>Returns generated Validata XML</returns>
        private static string GetMatchingEntries(string xmlData,
                                                 ArrayList commands,
                                                 ArrayList responses,
                                                 ArrayList errors)
        {
            StringBuilder sb = new StringBuilder();

            using(StringReader sReader = new StringReader(xmlData))
            using (XmlTextReader xmlReader = new XmlTextReader(sReader) { Namespaces = false })
            {
                XPathDocument xpathDoc = new XPathDocument(xmlReader);
                XPathNavigator nav = xpathDoc.CreateNavigator();
                XmlNamespaceManager nsmanager = new XmlNamespaceManager(nav.NameTable);

                nav.MoveToRoot();
                nav.MoveToFirstNamespace();
                nav.MoveToFirstChild();

                //Check if we have namespace
                if (!nav.Name.StartsWith("xml"))
                {
                    string prefix = "", name;
                    int nsPos = nav.Name.LastIndexOf(":");
                    if (nsPos >= 0)
                    {
                        prefix = nav.Name.Substring(0, nsPos);
                        name = nav.Name.Substring(nsPos + 1);
                    }
                    else
                    {
                        name = nav.Name;
                    }

                    //Add namespace
                    nsmanager.AddNamespace(prefix, name);
                }

                #region Look for Valid Adapter Commands

                //Extract all known commands
                for (int c = 0; c < commands.Count; c++)
                {
                    //Get command name
                    string commandName = commands[c].ToString();

                    //search for current command in source XML
                    string xpath = "//" + commandName;

                    XPathExpression xpe = nav.Compile(xpath);
                    xpe.SetContext(nsmanager);

                    XPathNodeIterator Iterator = nav.Select(xpe);

                    //Cycle through current command instances
                    while (Iterator.MoveNext())
                    {
                        string currentCommand = Iterator.Current.Name;

                        sb.Append("<");
                        sb.Append(XmlConvert.EncodeName(currentCommand));
                        sb.Append(">");
                        sb.Append(Environment.NewLine);

                        if (Iterator.Current.MoveToFirstChild())
                        {
                            sb.Append("<");
                            sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                            sb.Append(">");
                            sb.Append(Iterator.Current.Value);
                            sb.Append("</");
                            sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                            sb.Append(">");
                            sb.Append(Environment.NewLine);


                            while (Iterator.Current.MoveToNext())
                            {
                                sb.Append("<");
                                sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                                sb.Append(">");
                                sb.Append(Iterator.Current.Value);
                                sb.Append("</");
                                sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                                sb.Append(">");
                                sb.Append(Environment.NewLine);
                            }
                        }

                        sb.Append("</");
                        sb.Append(XmlConvert.EncodeName(currentCommand));
                        sb.Append(">");
                        sb.Append(Environment.NewLine);
                    }
                }

                #endregion

                #region Look for valid Adapter responses

                //Extract all known responses
                for (int r = 0; r < responses.Count; r++)
                {
                    //Get response name
                    string responseName = responses[r].ToString();

                    //search for current response in source XML
                    string xpath = "//" + responseName;

                    XPathExpression xpe = nav.Compile(xpath);
                    xpe.SetContext(nsmanager);

                    XPathNodeIterator Iterator = nav.Select(xpe);

                    //Cycle through current command instances
                    while (Iterator.MoveNext())
                    {
                        string currentResponse = Iterator.Current.Name;

                        sb.Append("<");
                        sb.Append(XmlConvert.EncodeName(currentResponse));
                        sb.Append(">");
                        sb.Append(Environment.NewLine);

                        if (Iterator.Current.MoveToFirstChild())
                        {
                            sb.Append("<");
                            sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                            sb.Append(">");
                            sb.Append(Iterator.Current.Value);
                            sb.Append("</");
                            sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                            sb.Append(">");
                            sb.Append(Environment.NewLine);


                            while (Iterator.Current.MoveToNext())
                            {
                                sb.Append("<");
                                sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                                sb.Append(">");
                                sb.Append(Iterator.Current.Value);
                                sb.Append("</");
                                sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                                sb.Append(">");
                                sb.Append(Environment.NewLine);
                            }
                        }

                        sb.Append("</");
                        sb.Append(XmlConvert.EncodeName(currentResponse));
                        sb.Append(">");
                        sb.Append(Environment.NewLine);
                    }
                }

                #endregion

                #region Look for valid Adapter errors

                //Extract all known errors
                for (int e = 0; e < errors.Count; e++)
                {
                    //Get response name
                    string errorName = errors[e].ToString();

                    //search for current response in source XML
                    string xpath = "//" + errorName;

                    XPathExpression xpe = nav.Compile(xpath);
                    xpe.SetContext(nsmanager);

                    XPathNodeIterator Iterator = nav.Select(xpe);

                    //Cycle through current command instances
                    while (Iterator.MoveNext())
                    {
                        string currentError = Iterator.Current.Name;

                        sb.Append("<");
                        sb.Append(XmlConvert.EncodeName(currentError));
                        sb.Append(">");
                        sb.Append(Environment.NewLine);

                        if (Iterator.Current.MoveToFirstChild())
                        {
                            sb.Append("<");
                            sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                            sb.Append(">");
                            sb.Append(Iterator.Current.Value);
                            sb.Append("</");
                            sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                            sb.Append(">");
                            sb.Append(Environment.NewLine);

                            while (Iterator.Current.MoveToNext())
                            {
                                sb.Append("<");
                                sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                                sb.Append(">");
                                sb.Append(Iterator.Current.Value);
                                sb.Append("</");
                                sb.Append(XmlConvert.EncodeName(Iterator.Current.Name));
                                sb.Append(">");
                                sb.Append(Environment.NewLine);
                            }
                        }

                        sb.Append("</");
                        sb.Append(XmlConvert.EncodeName(currentError));
                        sb.Append(">");
                        sb.Append(Environment.NewLine);
                    }

                    #endregion
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Retrieves the Instances XML data from the incoming XML
        /// </summary>
        /// <param name="xmlData">XML Data</param>
        /// <returns>Instances XML</returns>
        private static string GetInstancesOnly(string xmlData)
        {
            bool needToRead = true;
            string instancesXML = String.Empty;

            using (StringReader sReader = new StringReader(xmlData))
            {
                using (XmlTextReader xmlReader = new XmlTextReader(sReader) { Namespaces = false })
                {
                    while (true)
                    {
                        if (needToRead)
                        {
                            if (!xmlReader.Read())
                            {
                                break;
                            }
                        }
                        else
                        {
                            needToRead = true;
                        }

                        if (xmlReader.NodeType == XmlNodeType.Element)
                        {
                            switch (xmlReader.Name)
                            {
                                case "instances":
                                    instancesXML = xmlReader.ReadInnerXml();
                                    needToRead = false;
                                    break;
                            }
                        }
                    }
                }
            }

            return instancesXML;
        }

        /// <summary>
        /// Retrieves the Instances XML data from the incoming XML
        /// </summary>
        /// <param name="xmlData">XML Data</param>
        /// <returns>Instances XML</returns>
        private static string GetInstancesOnlyNoInstancesTag(string xmlData)
        {
            bool needToRead = true;
            string instancesXML = String.Empty;

            using (StringReader sReader = new StringReader(xmlData))
            {
                using (XmlTextReader xmlReader = new XmlTextReader(sReader) { Namespaces = false })
                {
                    while (true)
                    {
                        if (needToRead)
                        {
                            if (!xmlReader.Read())
                            {
                                break;
                            }
                        }
                        else
                        {
                            needToRead = true;
                        }

                        if (xmlReader.NodeType == XmlNodeType.Element)
                        {
                            switch (xmlReader.Name)
                            {
                                case "Validata":
                                    instancesXML = xmlReader.ReadInnerXml();
                                    needToRead = false;
                                    break;
                            }
                        }
                    }
                }
            }

            return instancesXML;
        }

        #endregion
    }
}