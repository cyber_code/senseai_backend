using System;
using System.Web;
using System.Xml;
using ValidataCommon;

namespace AXMLEngine
{
    /// <summary>
    /// Association class.
    /// </summary>
    [Serializable]
    public class XMLAssociation
    {
        #region Private And Protected Members

        /// <summary>
        /// Association ID
        /// </summary>
        private string _ID; //TODO not used

        /// <summary>
        /// Association Name
        /// </summary>
        private string _Name;

        /// <summary>
        /// Association Alias
        /// </summary>
        private string _Alias;

        /// <summary>
        /// Association source node
        /// </summary>
        private ulong _Source;

        /// <summary>
        /// Association Target node
        /// </summary>
        private ulong _Target;

        #endregion

        #region Public Properties

        /// <summary>
        /// Association Name
        /// </summary>
        public string Name
        {
            get { return _Name; }
        }

        /// <summary>
        /// Association Alias
        /// </summary>
        public string Alias
        {
            get { return _Alias; }
        }

        /// <summary>
        /// Association source node
        /// </summary>
        public ulong Source
        {
            get { return _Source; }
        }

        /// <summary>
        /// Association Target node
        /// </summary>
        public ulong Target
        {
            get { return _Target; }
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <param name="name">Association name</param>
        /// <param name="alias">Association alias</param>
        /// <param name="src">Source node ID</param>
        /// <param name="trg">Target node ID</param>
        public XMLAssociation(string name, string alias, ulong src, ulong trg)
        {
            _ID = "";
            _Name = name;
            _Alias = alias;
            _Source = src;
            _Target = trg;
        }

        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <param name="id">The id of the association</param>
        /// <param name="name">Association name</param>
        /// <param name="alias">Association alias</param>
        /// <param name="src">Source node ID</param>
        /// <param name="trg">Target node ID</param>
        public XMLAssociation(string id, string name, string alias, ulong src, ulong trg)
        {
            _ID = id;
            _Name = name;
            _Alias = alias;
            _Source = src;
            _Target = trg;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Serializes an Association instance to XML.
        /// </summary>
        /// <param name="xmlWriter">The XML writer where the object is serialized.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.ASSOCIATION_TAG);

            xmlWriter.WriteElementString(AXMLConstants.ASSOCIATION_NAME_TAG, Utils.GetHtmlEncodedValue(Name));
            xmlWriter.WriteElementString(AXMLConstants.ASSOCIATION_ALIAS_TAG, Utils.GetHtmlEncodedValue(Alias));
            xmlWriter.WriteElementString(AXMLConstants.ASSOCIATION_SOURCE_TAG, Source.ToString());
            xmlWriter.WriteElementString(AXMLConstants.ASSOCIATION_TARGET_TAG, Target.ToString());

            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Creates an Association instance from XML.
        /// </summary>
        /// <param name="assocReader">The XML describing the association.</param>
        /// <returns>An association instance.</returns>
        public static XMLAssociation FromXML(XmlReader assocReader)
        {
            string name = "", alias = "", id = "";
            ulong source = 0, target = 0;

            while (assocReader.Read())
            {
                if (assocReader.NodeType == XmlNodeType.Element)
                {
                    switch (assocReader.Name)
                    {
                        case AXMLConstants.ASSOCIATION_TAG:
                            //the root node - skip it
                            break;
                        case AXMLConstants.ASSOCIATION_ID_TAG:
                            id = assocReader.ReadString();
                            break;

                        case AXMLConstants.ASSOCIATION_NAME_TAG:
                            name = assocReader.ReadString();
                            break;

                        case AXMLConstants.ASSOCIATION_ALIAS_TAG:
                            alias = assocReader.ReadString();
                            break;

                        case AXMLConstants.ASSOCIATION_SOURCE_TAG:
                            source = ulong.Parse(assocReader.ReadString());
                            break;

                        case AXMLConstants.ASSOCIATION_TARGET_TAG:
                            target = ulong.Parse(assocReader.ReadString());
                            break;
                    }
                }
            }

            XMLAssociation assoc = new XMLAssociation(id, name, alias, source, target);

            return assoc;
        }

        #endregion
    }
}