﻿using System;
using System.Collections.Generic;

namespace AXMLEngine.T24Objects
{
    public class T24Enquiry
    {
        public struct ResultField
        {
            public string FieldName;
            public string FieldLbl;
            public int Column;
            public string Operation;
            public int RowVal;
            public bool IsBody;

            public static bool operator ==(ResultField x, ResultField y)
            {
                return x.FieldName == y.FieldName && x.FieldLbl == y.FieldLbl && x.Column == y.Column && x.Operation == y.Operation;
            }
            public static bool operator !=(ResultField x, ResultField y)
            {
                return x.FieldName != y.FieldName || x.FieldLbl != y.FieldLbl || x.Column != y.Column || x.Operation != y.Operation;
            }

            public string SSFieldName
            {
                get
                {
                    // todo - this is strange logic, but for most of enquiryes XXX-LIST, field name doesn't match the SS field name
                    //  for example 'Description', but in SS is 'DESCRIPTION'. My observation is that value in OPERATION matches the field name in SS
                    if (FieldName.ToUpper() != FieldName
                        && !string.IsNullOrEmpty(Operation))
                    {
                        return Operation;
                    }

                    return FieldName;
                }
            }
        }
        public struct SelectionField
        {
            public string TableField;
            public string FieldLbl;
        }

        public List<ResultField> Columns = new List<ResultField>();
        public List<SelectionField> Selections = new List<SelectionField>();
        public List<string> HeaderLines;

        public string Name { get; private set; }
        public string FileName { get; private set; }

        private readonly Comparison<ResultField> _colComp = SortByRowCol;
        private static int SortByRowCol(ResultField r1, ResultField r2)
        {
            // First - header comes before body
            if (!r1.IsBody && r2.IsBody) return -1;
            if (r1.IsBody && !r2.IsBody) return 1;

            // Second - sort by row value
            if (r1.RowVal < r2.RowVal) return -1;
            if (r1.RowVal > r2.RowVal) return 1;

            // Third - same section, same row: sort by col
            if (r1.Column < r2.Column) return -1;

            return 1;
        }

        public T24Enquiry(string name, string fileName)
        {
            Name = name;
            FileName = fileName;
        }

        public T24Enquiry(Instance enqInstance, out string errorText)
        {
            errorText = "";
            Name = enqInstance.T24Id;

            foreach (InstanceAttribute ia in enqInstance.GetMultivalueAttributesByShortFieldName("FILE.NAME"))
            {
                FileName = ia.Value;
            }

            var headers = new Dictionary<int, string>();
            foreach (InstanceAttribute ia in enqInstance.GetMultivalueAttributesByShortFieldName("HEADER"))
            {
                string header = ia.Value;

                string[] splitted = header.Split(new[] { "@(", ")" }, StringSplitOptions.RemoveEmptyEntries);
                if (splitted.Length < 2)
                {
                    //some unexpected header value
                    continue;
                }

                string[] columnArgs = splitted[0].Split(',');
                int firstArg;
                if (columnArgs.Length == 0 || !int.TryParse(columnArgs[0], out firstArg))
                {
                    //some unexpected header value
                    continue;
                }

                headers[firstArg] = splitted[1];
            }

            foreach (InstanceAttribute ia in enqInstance.GetMultivalueAttributesByShortFieldName("FIELD.NAME"))
            {
                int mvIndex, column;

                try
                {
                    mvIndex = int.Parse(ia.Name.Split('-')[1].Split('~')[0]);
                }
                catch (Exception eee)
                {
                    errorText = "Error parsing field \"" + ia.Value + "\"'s mv index: " + eee.Message;
                    return;
                }

                var colMV = enqInstance.AttributeByName("COLUMN-" + mvIndex + "~1");
                if (colMV == null) continue;
                string[] fullColumn = colMV.Value.Split(',');

                if (int.TryParse(fullColumn[0], out column))
                {
                    int rowVal = 0;
                    bool isBody = true;

                    if (fullColumn.Length == 2)
                    {
                        //TODO: Handle 153,+1 type columns

                        // Gordan 11 aug 2015 - Handling like this:
                        // before comma is column value, after comma is row value

                        // + and - prefixed rows are from body section; and
                        // non-prefixed are from header section

                        // without row is again body field with row assumed 0, however
                        // row = 0 is a header field

                        // first come all header fields, next body ones; and then
                        // within each section they get sorted by row (minus are before plus), then column.

                        // Go ->

                        string rowString = fullColumn[1];
                        if (int.TryParse(rowString, out rowVal))
                        {
                            if (!rowString.StartsWith("+") && !rowString.StartsWith("-"))
                                isBody = false;
                        }

                        // rest of the implementation is in SortByRowCol
                    }

                    string dispBreak = "";

                    var db = enqInstance.AttributeByName("DISPLAY.BREAK-" + mvIndex + "~1");
                    if (db != null)
                        dispBreak = db.Value;
                    if (dispBreak == "NONE")
                        continue;

                    string fldLbl = "";
                    var fl = enqInstance.AttributeByName("FIELD.LBL-" + mvIndex + "~1");

                    if (fl != null)
                        fldLbl = fl.Value;
                    //else
                    //{
                    //    string columnValue;
                    //    if (headers.TryGetValue(column, out columnValue))
                    //        fldLbl = columnValue;
                    //}

                    var fldOperation = enqInstance.AttributeByName("OPERATION-" + mvIndex + "~1");
                    if (fldOperation != null && fldOperation.Value.StartsWith("\"") && fldOperation.Value.EndsWith("\""))
                    {
                        //Label column
                        continue;
                    }

                    string operation = fldOperation == null ? string.Empty : fldOperation.Value;
                    Columns.Add(
                        new ResultField
                        {
                            FieldName = ia.Value,
                            Column = column,
                            FieldLbl = fldLbl,
                            Operation = operation,
                            IsBody = isBody,
                            RowVal = rowVal

                        }

                    );
                }
                else
                {
                    //Column is not numerical
                }

                Columns.Sort(_colComp);

            }

            foreach (InstanceAttribute ia in enqInstance.GetMultivalueAttributesByShortFieldName("SELECTION.FLDS"))
            {
                int mvIndex;
                try
                {
                    mvIndex = int.Parse(ia.Name.Split('-')[1].Split('~')[0]);
                }
                catch (Exception eee)
                {
                    errorText = "Error parsing field \"" + ia.Value + "\"'s mv index: " + eee.Message;
                    return;
                }

                string selLbl = ia.Value;
                var lblMV = enqInstance.AttributeByName("SEL.LBL-" + mvIndex + "~1");
                if (lblMV != null)
                    selLbl = lblMV.Value;
                Selections.Add(new SelectionField { FieldLbl = selLbl, TableField = ia.Value });
            }
        }
    }
}