using System;
using System.Xml;

namespace AXMLEngine
{
    /// <summary>
    /// Handler of a section for Performance data
    /// </summary>
    [Serializable]
    public class Performance
    {
        #region Private members

        /// <summary>
        /// Collection of all Performance Components defined in the section.
        /// </summary>
        private ComponentsCollection _Components;

        #endregion

        #region Class Lifecycle

        public Performance()
        {
            _Components = new ComponentsCollection();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Serializes the Performance section to XML
        /// </summary>
        /// <param name="xmlWriter">XmlWriter where to serialize the Performance section</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            // Start performance indicators section
            xmlWriter.WriteStartElement(AXMLConstants.PERFORMANCE_INDICATORS_SECTION_TAG);

            // Write performance Components to XML
            foreach (Component adapter in _Components)
            {
                adapter.ToXML(xmlWriter);
            }

            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Reads the metadata section of the Performance Adapter 
        /// </summary>
        /// <param name="indicatorsSectionReader">The contents of the metadata section</param>
        /// <returns>False if an exception occurs</returns>
        public void FromXML(XmlReader indicatorsSectionReader)
        {
            while (indicatorsSectionReader.Read())
            {
                if (indicatorsSectionReader.NodeType == XmlNodeType.Element)
                {
                    if (indicatorsSectionReader.Name == AXMLConstants.PERFORMANCE_INDICATORS_SECTION_TAG)
                    {
                        //this is the root tag - skip it
                        continue;
                    }
                    else if (indicatorsSectionReader.Name == AXMLConstants.PERFORMANCE_ADAPTER_TAG)
                    {
                        Component adapter = new Component();
                        using (XmlReader componentReader = indicatorsSectionReader.ReadSubtree())
                        {
                            adapter.FromXML(componentReader);
                        }
                        Components.Add(adapter);
                    }
                }
            }
        }

        #endregion

        #region Public properties

        public ComponentsCollection Components
        {
            get { return _Components; }
        }

        #endregion
    }
}