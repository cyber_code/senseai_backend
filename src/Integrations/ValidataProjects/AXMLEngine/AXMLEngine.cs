//===============================================================================
// AXMLEngine.cs
//===============================================================================
//
// This class is used to generate and parse Adapter communication XML Format.
//
// History:
//	Version:                Date:                   Author:
//	1.00                    10/03/2006              Dimitar
//	Description: Created initial version
//
//===============================================================================
// Copyright (C) 2004-2006 Theseus Technology Partners
// All rights reserved.
//===============================================================================
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using ValidataCommon;

namespace AXMLEngine
{
    /// <summary>
    /// AXMLEngine generates and parses XML files for communication with any adapter
    /// </summary>
    [Serializable]
    public class AXMLEngine
    {
        #region Private And Protected Members

        /// <summary>
        /// Adapter requests
        /// </summary>
        private readonly AdapterRequestsCollection _AdapterRequests = new AdapterRequestsCollection();

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets default request typical of the default request
        /// </summary>
        public MetadataTypical DefaultRequestTypical
        {
            get
            {
                var request = DefaultRequest;
                return request.RequestMetadata.Typicals.FirstOrDefault(typ => typ.ID == request.RequestTypicalID);
            }
        }

        /// <summary>
        /// Gets default response typical of the default request
        /// </summary>
        public MetadataTypical DefaultResponseTypical
        {
            get
            {
                var response = DefaultRequest;
                return response.RequestMetadata.Typicals.FirstOrDefault(typ => typ.ID == response.ResponseTypicalID);
            }
        }

        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default constructor
        /// </summary>
        public AXMLEngine()
        {
        }

        /// <summary>
        /// Constructs from AXML
        /// </summary>
        public AXMLEngine(string xml)
        {
            FromXML(xml);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Fill all AXMLEngine properties from input xml string
        /// </summary>
        /// <param name="xml">Input XML String</param>
        /// <returns>Returns true if succeeded, otherwise returns false</returns>
        public bool FromXML(string xml)
        {
            return FromXML(xml, RequestLoadFlags.LoadNormal);
        }

        /// <summary>
        /// Fill all AXMLEngine properties from input xml string
        /// </summary>
        /// <param name="xml">Input XML String</param>
        /// <param name="loadFlags">Loading flags</param>
        /// <returns>Returns true if succeeded, otherwise returns false</returns>
        public bool FromXML(string xml, RequestLoadFlags loadFlags)
        {
            using (StringReader stringReader = new StringReader(xml))
            {
                using (XmlTextReader engineReader = new XmlTextReader(stringReader) { Namespaces = false })
                {
                    engineReader.WhitespaceHandling = WhitespaceHandling.None;
                    engineReader.XmlResolver = null;
                    engineReader.Normalization = false;
                    engineReader.Namespaces = false;

                    return FromXMLFileCoreProcessing(loadFlags, engineReader);
                }
            }
        }

        /// <summary>
        /// Fill all AXMLEngine properties from input xml string
        /// </summary>
        /// <param name="xmlFilePath">Input XML File</param>
        /// <param name="loadFlags">Loading flags</param>
        /// <returns>Returns true if succeeded, otherwise returns false</returns>
        public bool FromXMLFile(string xmlFilePath, RequestLoadFlags loadFlags)
        {
            using (StreamReader streamReader = new StreamReader(xmlFilePath))
            {
                using (XmlTextReader engineReader = new XmlTextReader(streamReader) { Namespaces = false })
                {
                    engineReader.WhitespaceHandling = WhitespaceHandling.None;
                    engineReader.XmlResolver = null;
                    engineReader.Normalization = false;

                    return FromXMLFileCoreProcessing(loadFlags, engineReader);
                }
            }
        }

        private bool FromXMLFileCoreProcessing(RequestLoadFlags loadFlags, XmlReader engineReader)
        {
            try
            {
                // Read input data and locate root typical
                while (engineReader.Read())
                {
                    switch (engineReader.NodeType)
                    {
                            //Cycle trough XML Elements
                        case XmlNodeType.Element:
                            // Analyze incoming elements
                            if (engineReader.Name == AXMLConstants.ADAPTER_REQUEST_TAG
                                || engineReader.Name == AXMLConstants.ADAPTER_RESPONSE_TAG)
                            {
                                if (_AdapterRequests.Count == 0)
                                    _AdapterRequests.Add(new AdapterRequest()); // default request

                                using (XmlReader requestReader = engineReader.ReadSubtree())
                                {
                                    DefaultRequest.FromXML(requestReader, loadFlags);
                                }
                            }
                            break;
                    }
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Fill all AXMLEngine properties from input xml document
        /// </summary>
        /// <param name="xmlDoc">Source XML Document</param>
        /// <returns></returns>
        public bool FromXML(XmlDocument xmlDoc)
        {
            return FromXML(xmlDoc.OuterXml, RequestLoadFlags.LoadNormal);
        }

        /// <summary>
        /// Fill all AXMLEngine properties from input xml document
        /// </summary>
        /// <param name="xmlDoc">Source XML Document</param>
        /// <param name="loadFlags">Flags for loading</param>
        /// <returns></returns>
        public bool FromXML(XmlDocument xmlDoc, RequestLoadFlags loadFlags)
        {
            return FromXML(xmlDoc.OuterXml, loadFlags);
        }

        /// <summary>
        /// Generates the XML for the requests
        /// </summary>
        /// <param name="xmlWriter">A constructed XmlWriter object.</param>
        public void GenerateAXMLRequest(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement(AXMLConstants.MAIN_TAG);
            xmlWriter.WriteAttributeString(AXMLConstants.VALIDATA_NAMESPACE_ATTRIBUTE, AXMLConstants.VALIDATA_NAMESPACE);
            xmlWriter.WriteAttributeString(AXMLConstants.VALIDATA_VERSION_ATTRIBUTE, AXMLConstants.VALIDATA_VERSION);

            foreach (AdapterRequest request in _AdapterRequests)
            {
                request.ToXML(xmlWriter);
            }

            //end main tag
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
        }

        /// <summary>
        /// Generates AXML Response XML
        /// </summary>
        /// <param name="instances">Result instances</param>
        /// <param name="errors">adapter errors</param>
        /// <returns>Returns generated Response XML</returns>
        public static string GenerateAXMLResponse(IEnumerable<Instance> instances, string errors)
        {
            AXMLEngine engine = new AXMLEngine();
            engine.AddAdapterResponse("1");
            engine.DefaultRequest.Type = RequestType.RequestResponse;
            engine.DefaultRequest.Datasets.DefaultDataset.XmlInstances.AddRange(instances);
            engine.DefaultRequest.Datasets.DefaultErrorSet.FromRawErrors(errors);
            engine.DefaultRequest.Type = RequestType.RequestResponse;

            return engine.ToXML();
        }

        /// <summary>
        /// Generates AXML Response XML
        /// </summary>
        /// <param name="adapterInstancesXML">Instances VXML</param>
        /// <param name="errors">adapter errors</param>
        /// <returns>Returns generated Response XML</returns>
        public static string GenerateAXMLResponse(string adapterInstancesXML, string errors)
        {
            AXMLEngine engine = new AXMLEngine();
            engine.AddAdapterResponse("1");
            engine.DefaultRequest.Type = RequestType.RequestResponse;
            engine.DefaultRequest.Datasets.DefaultDataset.FromRawInstances(adapterInstancesXML);
            //Source changed to C:\Temenos\R14\bnk\bnk.run\UNIT.BP\UNIT.TEST.MANAGER
            //7m0870 	CALL @STUB.RTN.NAME(OUT.VARS, OUT.DATA, OUT.ERR, OUT.RES)m
            //jBASE debugger->
            //Some OFS reponses like this one contains invalid xml chars, and have to be removed
            engine.DefaultRequest.Datasets.DefaultErrorSet.FromRawErrors(errors.Replace((char)15, ' '));
            engine.DefaultRequest.Type = RequestType.RequestResponse;

            return engine.ToXML();
        }

        public static string GenerateAXMLResponse(string adapterInstancesXML, string errors, string pdfResponse)
        {
            AXMLEngine engine = new AXMLEngine();
            engine.AddAdapterResponse("1");
            engine.DefaultRequest.Type = RequestType.RequestResponse;
            engine.DefaultRequest.Datasets.DefaultDataset.FromRawInstances(adapterInstancesXML);
            //Source changed to C:\Temenos\R14\bnk\bnk.run\UNIT.BP\UNIT.TEST.MANAGER
            //7m0870 	CALL @STUB.RTN.NAME(OUT.VARS, OUT.DATA, OUT.ERR, OUT.RES)m
            //jBASE debugger->
            //Some OFS reponses like this one contains invalid xml chars, and have to be removed
            engine.DefaultRequest.Datasets.DefaultErrorSet.FromRawErrors(errors.Replace((char)15, ' '));
            engine.DefaultRequest.Type = RequestType.RequestResponse;

            engine.AddAdapterResponse("3");
            engine.DefaultRequest.Type = RequestType.RequestResponse;
            engine.DefaultRequest.Datasets.DefaultDataset.FromRawInstances(pdfResponse);

            return engine.ToXML();
        }

        /// <summary>
        /// Generates AXML Success Response XML
        /// </summary>
        /// <param name="adapterInstancesXML">Instances VXML</param>
        /// <param name="errors">adapter errors</param>
        /// <returns>Returns generated Response XML</returns>
        public static string GenerateAXMLResponse(string adapterInstancesXML)
        {
            var engine = new AXMLEngine();
            engine.AddAdapterResponse("1");
            engine.DefaultRequest.Type = RequestType.RequestResponse;
            engine.DefaultRequest.Datasets.DefaultDataset.FromRawInstances(adapterInstancesXML);
            engine.DefaultRequest.Type = RequestType.RequestResponse;

            return engine.ToXML();
        }

        /// <summary>
        /// Generates AXML Errors Response XML
        /// </summary>
        /// <param name="adapterInstancesXML">Instances VXML</param>
        /// <param name="errors">adapter errors</param>
        /// <returns>Returns generated Response XML</returns>
        public static string GenerateAXMLErrorResponse(string errors)
        {
            var engine = new AXMLEngine();
            engine.AddAdapterResponse("1");
            engine.DefaultRequest.Type = RequestType.RequestResponse;
            engine.DefaultRequest.Datasets.DefaultErrorSet.AddError(new Error(errors));

            return engine.ToXML();
        }

        /// <summary>
        /// Writes the contents of the AXML engine to a XML file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        public void ToXMLFile(string fileName)
        {
            string dirName = Path.GetDirectoryName(fileName);
            if(!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
            }

            using (XmlWriter xmlWriter = new SafeXmlTextWriter(fileName, Encoding.UTF8))
            {
                ToXMLFile(xmlWriter);
            }
        }

        /// <summary>
        /// Writes the contents of the AXML engine to a XML writer.
        /// </summary>
        /// <returns></returns>
        public void ToXMLFile(XmlWriter xmlWriter)
        {
            ToXMLProcess(xmlWriter);
        }

        /// <summary>
        /// Writes the contents of the AXML engine as an XML string.
        /// </summary>
        /// <returns></returns>
        public string ToXML()
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (XmlWriter xmlWriter = new SafeXmlTextWriter(sw))
                {
                    ToXMLProcess(xmlWriter);
                }
            }

            string xmlResult = sb.ToString();
            return xmlResult.Replace("utf-16", "utf-8");
        }

        private void ToXMLProcess(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement(AXMLConstants.MAIN_TAG);
            xmlWriter.WriteAttributeString(AXMLConstants.VALIDATA_NAMESPACE_ATTRIBUTE, AXMLConstants.VALIDATA_NAMESPACE);
            xmlWriter.WriteAttributeString(AXMLConstants.VALIDATA_VERSION_ATTRIBUTE, AXMLConstants.VALIDATA_VERSION);

            foreach (AdapterRequest request in _AdapterRequests)
            {
                request.ToXML(xmlWriter);
            }

            //end main tag
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Flush();
        }

        /// <summary>
        /// Gets the VXML containing the instances returned in the adapter response.
        /// </summary>
        /// <param name="inputFilename"></param>
        /// <returns>String containing VXML from the instances in the response.</returns>
        public static string GetVXMLFromResponse(string inputFilename)
        {
            string axml = Utils.ReadFileContents(inputFilename, Encoding.UTF8);

            AXMLEngine engine = new AXMLEngine();
            engine.FromXML(axml, RequestLoadFlags.LoadNormal);

            string instanceVXML = engine.DefaultRequest.Datasets.DefaultDataset.GetVXML();
            return instanceVXML;
        }

        /// <summary>
        /// Gets, split and write into files the VXML containing the instances returned in the adapter response.  
        /// </summary>
        /// <param name="inputFilename">The input filename.</param>
        /// <param name="outputFolder">The output folder.</param>
        /// <param name="CHUNK_SIZE">The CHUN k_ SIZE.</param>
        /// <param name="typicalName">Name of the typical.</param>
        /// <returns></returns>
        public static List<string> ExtractAndSplitVXMLFromResponseIntoFiles(string inputFilename, string outputFolder,
                                                                            uint CHUNK_SIZE, string typicalName)
        {
            string _IxportUniqueID = Guid.NewGuid().ToString();
            List<string> files = new List<string>();

            StreamReader sReader = new StreamReader(inputFilename, Encoding.UTF8);

            XmlTextReader xmlReader = new XmlTextReader(sReader) { Namespaces = false };

            /*
            XmlReader xmlReader = XmlReader.Create(inputFilename);
            */
            xmlReader.ReadToFollowing(AXMLConstants.INSTANCES_TAG);
            xmlReader.ReadToFollowing(AXMLConstants.DATASET_TAG);
            xmlReader.ReadToFollowing(AXMLConstants.DATASET_ROW_TAG);

            //string tempStr = xmlReader.ReadOuterXml();
            string encodedTypicalName = XmlConvert.EncodeName(typicalName);
            if (xmlReader.Read())
            {
                //string instanceTagName = xmlReader.Name;
                StringBuilder sb = new StringBuilder();

                string nameSpaceString = xmlReader.NamespaceURI;
                if (nameSpaceString != String.Empty)
                {
                    nameSpaceString = "xmlns=\"" + nameSpaceString + "\"";
                }

                uint currentInstance = 0;
                uint totalChunks = 0;
                bool hasInstances = false;


                while (true)
                {
                    if (xmlReader.Name == encodedTypicalName)
                    {
                        currentInstance++;
                        hasInstances = true;
                        if (nameSpaceString != String.Empty)
                        {
                            sb.Append(xmlReader.ReadOuterXml().Replace(nameSpaceString, String.Empty));
                        }
                        else
                        {
                            sb.Append(xmlReader.ReadOuterXml());
                        }

                        if (currentInstance >= CHUNK_SIZE)
                        {
                            totalChunks++;
                            string fileName = WriteChunkFile(sb.ToString(), totalChunks, outputFolder, _IxportUniqueID);
                            if (fileName != String.Empty)
                            {
                                files.Add(fileName);
                            }

                            sb = new StringBuilder();
                            currentInstance = 0;
                        }
                    }
                    else if ((xmlReader.NodeType == XmlNodeType.EndElement) &&
                             (xmlReader.Name == AXMLConstants.DATASET_TAG))
                    {
                        //we have reached the end of the instance section
                        break;
                    }
                    else if ((xmlReader.NodeType == XmlNodeType.Element) &&
                             (xmlReader.Name == AXMLConstants.DATASET_ROW_TAG))
                    {
                        //when we reach another dataset row just get in it
                        xmlReader.Read();
                    }
                    else if ((xmlReader.NodeType == XmlNodeType.EndElement) &&
                             (xmlReader.Name == AXMLConstants.DATASET_ROW_TAG))
                    {
                        //we have reached the end of the dataset row section
                        xmlReader.Read();
                    }
                    else
                    {
                        //instances of other typicals are skipped
                        xmlReader.Skip();
                    }
                }


                /*
               do 
                {
                    currentInstance++;
                                      
                    if(nameSpaceString != String.Empty)
                         sb.Append(xmlReader.ReadOuterXml().Replace(nameSpaceString,String.Empty));
                    else
                         sb.Append(xmlReader.ReadOuterXml());
                   
                    if (currentInstance >= CHUNK_SIZE)
                    {
                        totalChunks++;
                        string fileName = WriteChunkFile(sb.ToString(), totalChunks, outputFolder, _IxportUniqueID);
                        if (fileName != String.Empty)
                            files.Add(fileName);

                        sb = null;
                        sb = new StringBuilder();
                        currentInstance = 0;
                    }
                }
                while (xmlReader.ReadToNextSibling(instanceTagName));
                */
                if (hasInstances && (currentInstance > 0))
                {
                    totalChunks++;
                    string fileName = WriteChunkFile(sb.ToString(), totalChunks, outputFolder, _IxportUniqueID);
                    if (fileName != String.Empty)
                    {
                        files.Add(fileName);
                    }

                    sb = null;
                    currentInstance = 0;
                }
                else if (hasInstances && (currentInstance == 0))
                {
                    //we don't have to do anything
                    sb = null;
                }
                else
                {
                    //if there are no instances to be transformed - generate an empty chunk file
                    totalChunks = 1;
                    string fileName = WriteChunkFile("", totalChunks, outputFolder, _IxportUniqueID);
                    if (fileName != String.Empty)
                    {
                        files.Add(fileName);
                    }

                    sb = null;
                    currentInstance = 0;
                }
            }

            return files;
        }

        public static void ExtractVXMLFromResponseIntoFile(string inputFilename, string outputFilename)
        {
            using (XmlWriter xmlWriter = new SafeXmlTextWriter(outputFilename, Encoding.UTF8))
            {
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement(AXMLConstants.MAIN_TAG);
                xmlWriter.WriteAttributeString(AXMLConstants.VALIDATA_NAMESPACE_ATTRIBUTE,
                                               AXMLConstants.VALIDATA_NAMESPACE);
                xmlWriter.WriteAttributeString(AXMLConstants.VALIDATA_VERSION_ATTRIBUTE, AXMLConstants.VALIDATA_VERSION);

                StreamReader sReader = new StreamReader(inputFilename, Encoding.UTF8);

                XmlTextReader xmlReader = new XmlTextReader(sReader) { Namespaces = false };

                xmlReader.MoveToContent();
                xmlReader.ReadToFollowing(AXMLConstants.INSTANCES_TAG);
                xmlReader.ReadToFollowing(AXMLConstants.DATASET_TAG);
                xmlReader.ReadToFollowing(AXMLConstants.DATASET_ROW_TAG);

                if (xmlReader.Read())
                {
                    while (true)
                    {
                        xmlReader.MoveToContent();

                        if ((xmlReader.NodeType == XmlNodeType.EndElement) &&
                            (xmlReader.Name == AXMLConstants.DATASET_TAG))
                        {
                            //we have reached the end of the instance section
                            break;
                        }
                        else if ((xmlReader.NodeType == XmlNodeType.Element) &&
                                 (xmlReader.Name == AXMLConstants.DATASET_ROW_TAG))
                        {
                            //when we reach another dataset row just get in it
                            xmlReader.Read();
                        }
                        else if ((xmlReader.NodeType == XmlNodeType.EndElement) &&
                                 (xmlReader.Name == AXMLConstants.DATASET_ROW_TAG))
                        {
                            //we have reached the end of the dataset row section
                            xmlReader.Read();
                        }
                        else
                        {
                            xmlWriter.WriteRaw(xmlReader.ReadOuterXml());
                        }
                    }
                }
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
            }
        }

        public static void WriteToFile(string filePath, string data)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                using (XmlWriter xmlWriter = new SafeXmlTextWriter(fs, Encoding.UTF8))
                {
                    xmlWriter.WriteStartDocument();
                    xmlWriter.WriteStartElement(AXMLConstants.MAIN_TAG);

                    xmlWriter.WriteRaw(data);

                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndDocument();

                    xmlWriter.Flush();
                }
            }
        }

        /// <summary>
        /// Write chunk file
        /// </summary>
        /// <param name="chunk">instances (xml content of chunk)</param>
        /// <param name="totalChunks">Number of chunk</param>
        /// <param name="outputFolder">Output Folder</param>
        /// <param name="importUniqueID">Output Folder</param>      
        /// <returns>FileName if file creation is succesfull, empty if not</returns>
        private static string WriteChunkFile(string chunk, uint totalChunks, string outputFolder, string importUniqueID)
        {
            string fileName = GenerateImportFileName(totalChunks, outputFolder, importUniqueID);

            WriteToFile(fileName, chunk);

            return fileName;
        }

        /// <summary>
        /// Generates the name of the import file.
        /// </summary>
        /// <param name="currentChunk">The current chunk.</param>
        /// <param name="outputFolder">The output folder.</param>
        /// <param name="importUniqueID">The import unique ID.</param>
        /// <returns></returns>
        private static string GenerateImportFileName(uint currentChunk, string outputFolder, string importUniqueID)
        {
            return string.Format("{0}{1}_imp_{2}.xml",
                                 outputFolder,
                                 importUniqueID,
                                 currentChunk);
        }

        /// <summary>
        /// Gets the VXML containing the instances returned in the adapter response.
        /// </summary>
        /// <param name="inputFilename">The input AXML file</param>
        /// <param name="outputFilename">The target path where to write the result</param>
        /// <returns>String containing VXML from the instances in the response.</returns>
        public static void ExtractAndWriteInstancesFromAxml(string inputFilename, string outputFilename)
        {
            //TODO use streaming mode instead of loading the whole file
            string axml = Utils.ReadFileContents(inputFilename);

            AXMLEngine engine = new AXMLEngine();
            engine.FromXML(axml, RequestLoadFlags.LoadNormal);

            engine.DefaultRequest.Datasets.DefaultDataset.WriteVXMLToFile(outputFilename);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gives access to the adapter requests collection
        /// </summary>
        public AdapterRequestsCollection AdaptersRequests
        {
            get { return _AdapterRequests; }
        }

        /// <summary>
        /// Returns the default adapter request.
        /// </summary>
        public AdapterRequest DefaultRequest
        {
            get
            {
                if(_AdapterRequests.Count == 0)
                    throw new ApplicationException("There is no default request");

                return _AdapterRequests[0];
            }
        }

        /// <summary>
        /// Returns the default adapter response.
        /// </summary>
        public AdapterRequest DefaultResponse
        {
            get
            {
                if (_AdapterRequests.Count == 0)
                {
                    throw new ApplicationException("There is no default response");
                }
                return _AdapterRequests[0];
            }
        }

        #endregion

        #region Object Interface

        /// <summary>
        /// Adds a new adapter request to the AXMLEngine object
        /// </summary>
        /// <param name="requestID">The ID of the new request</param>
        /// <param name="isImport">True if the request </param>
        /// <returns></returns>
        public AdapterRequest AddAdapterRequest(string requestID, bool isImport)
        {
            AdapterRequest request = new AdapterRequest(
                requestID,
                isImport ? RequestType.Import : RequestType.Export);

            _AdapterRequests.Add(request);
            return request;
        }

        /// <summary>
        /// Adds the adapter response.
        /// </summary>
        /// <param name="requestID">The request id</param>
        /// <param name="action">Action</param>
        /// <returns>Adapter Request Object</returns>
        public AdapterRequest AddAdapterRequest(string requestID, RequestAction action)
        {
            AdapterRequest result = new AdapterRequest(requestID, action);
            _AdapterRequests.Add(result);

            return result;
        }

        /// <summary>
        /// Adds the adapter response.
        /// </summary>
        /// <param name="responseID">The response ID.</param>
        /// <returns></returns>
        public AdapterRequest AddAdapterResponse(string responseID)
        {
            AdapterRequest response = new AdapterRequest(responseID, RequestType.RequestResponse);
            _AdapterRequests.Add(response);
            return response;
        }

        #endregion
    }
}