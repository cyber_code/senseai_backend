//===============================================================================
// TypicalAttribute.cs
//===============================================================================
//
// Typical attribute.
//
// History:
//	Version:                Date:                   Author:
//	1.00                    15/12/2004              Ilian
//	Description: Created initial version
//
//===============================================================================
// Copyright (C) 2004-2006 Theseus Technology Partners
// All rights reserved.
//===============================================================================

using System;
using System.Xml;
using Validata.Common;

namespace AXMLEngine
{
    /// <summary>
    /// TypicalAttribute.
    /// </summary>
    [Serializable]
    public class TypicalAttribute
    {
        #region Private And Protected Members

        /// <summary>
        /// Attribute name
        /// </summary>
        private string _Name;

        /// <summary>
        /// Attribute Type
        /// </summary>
        private AttributeDataType _Type;

        /// <summary>
        /// Attribute Remarks
        /// </summary>
        private string _Remarks;

        /// <summary>
        /// Attribute DataLength
        /// </summary>
        private ulong _DataLength;

        /// <summary>
        /// Attribute DisciplineNoderef
        /// </summary>
        private ulong _DisciplineNoderef;

        /// <summary>
        /// Attribute UserNoderef
        /// </summary>
        private ulong _UserNoderef;


        #endregion

        #region Public Properties

        /// <summary>
        /// Attribute name
        /// </summary>
        public string Name
        {
            get { return _Name ?? string.Empty; }
            set { _Name = value; }
        }

        /// <summary>
        /// Attribute Type
        /// </summary>
        public AttributeDataType Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        /// <summary>
        /// Attribute Remarks
        /// </summary>
        public string Remarks
        {
            get { return _Remarks ?? string.Empty; }
            set { _Remarks = value; }
        }

        /// <summary>
        /// Attribute DataLength
        /// </summary>
        public ulong DataLength
        {
            get { return _DataLength; }
            set { _DataLength = value; }
        }

        /// <summary>
        /// Attribute DisciplineNoderef
        /// </summary>
        public ulong DisciplineNoderef
        {
            get { return _DisciplineNoderef; }
            set { _DisciplineNoderef = value; }
        }

        /// <summary>
        /// Attribute UserNoderef
        /// </summary>
        public ulong UserNoderef
        {
            get { return _UserNoderef; }
            set { _UserNoderef = value; }
        }


        #endregion

        #region Class Lifecycle

        /// <summary>
        /// Default constructor
        /// </summary>
        public TypicalAttribute()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="src">Source</param>
        public TypicalAttribute(ValidataCommon.IMetadataAttribute src)
            : this(src.Name, src.DataType, src.Remarks, src.DataLength)
        {
        }

        public TypicalAttribute(ValidataCommon.IMetadataAttribute src,ulong disciplineNoderef, ulong userNoderef)
        {
            _Name = src.Name;
            _Type = src.DataType;
            _Remarks = src.Remarks;
            _DataLength = src.DataLength;
            _DisciplineNoderef = disciplineNoderef;
            _UserNoderef = userNoderef;
        }

        /// <summary>
        /// Additional constructor
        /// </summary>
        /// <param name="name">Attribute name</param>
        /// <param name="type">Attribute Type</param>
        /// <param name="remarks">Attribute Remarks</param>
        /// <param name="dataLength">Attribute DataLength</param>
        public TypicalAttribute(string name, AttributeDataType type, string remarks, ulong dataLength)
        {
            _Name = name;
            _Type = type;
            _Remarks = remarks;
            _DataLength = dataLength;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Write attributes of TypicalAttribute to the XML document
        /// </summary>
        /// <param name="xmlWriter">The XML writer.</param>
        public void ToXML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(AXMLConstants.TYPICAL_ATTRIBUTE_TAG);
            xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_NAME_ATTRIBUTE, Name);
            xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_ATTRIBUTE_TYPE, Type.ToString());
            xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_ATTRIBUTE_REMARKS, Remarks);
            xmlWriter.WriteAttributeString(AXMLConstants.TYPICAL_ATTRIBUTE_DATALENGTH, DataLength.ToString());
            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Reads the typical attribute.
        /// </summary>
        /// <param name="typicalReader">The typical reader.</param>
        /// <returns></returns>
        public static TypicalAttribute FromXML(XmlReader typicalReader)
        {
            TypicalAttribute ta = new TypicalAttribute();

            ta.Name = XmlConvert.DecodeName(typicalReader.GetAttribute(AXMLConstants.ATTRIBUTE_ATTRIBUTE_NAME));

            ta.Type = (AttributeDataType)
                      Enum.Parse(typeof (AttributeDataType),
                                 XmlConvert.DecodeName(
                                     typicalReader.GetAttribute(
                                         AXMLConstants.ATTRIBUTE_ATTRIBUTE_TYPE)), true);

            ta.Remarks =
                XmlConvert.DecodeName(typicalReader.GetAttribute(AXMLConstants.ATTRIBUTE_ATTRIBUTE_REMARKS));

            ta.DataLength = ulong.Parse(
                XmlConvert.DecodeName(
                    typicalReader.GetAttribute(AXMLConstants.ATTRIBUTE_ATTRIBUTE_DATALENGTH)));

            return ta;
        }

        #endregion

        public override string ToString()
        {
            return Name + " " + _Type;
        }
    }
}