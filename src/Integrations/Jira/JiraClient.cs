﻿using Core.Abstractions.Bindings;
using Core.Configuration;
using System.Collections.Generic;
using Atlassian.Jira;
using Jira.Issue;
using System;
using RestSharp;
using Jira.Project;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Reflection;
using Core.Abstractions;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using SenseAI.Domain.AdminModel;
using SenseAI.Domain.JiraModel;
using Jira.Webhook;

namespace Jira
{
    public class JiraClient : RestClient, IJiraClient
    {
        private readonly IJiraBindingConfiguration _bindingConfiguration;
        private readonly IWorkContext _workContext;
        private readonly ISubProjectRepository _subProjectRepository;
        private readonly IJiraUserSubprojectRepository _jiraUserSubprojectRepository;
        private readonly SenseAIConfig _config;
        private Atlassian.Jira.Jira _jira;

        public JiraClient(IJiraBindingConfiguration bindingConfiguration, IWorkContext workContext, ISubProjectRepository subProjectRepository, IJiraUserSubprojectRepository jiraUserSubprojectRepository, SenseAIConfig config)
            : base(bindingConfiguration)
        {
            _bindingConfiguration = bindingConfiguration;
            _workContext = workContext;
            _subProjectRepository = subProjectRepository;
            _jiraUserSubprojectRepository = jiraUserSubprojectRepository;
            _config = config;
        }

        private IBindingConfiguration BindingConfiguration => (IBindingConfiguration)_bindingConfiguration;

        public IEnumerable<Atlassian.Jira.Project> GetAllProjects(Guid subProjectId)
        {
            _jira = Atlassian.Jira.Jira.CreateRestClient(_subProjectRepository.GetSubProjectById(subProjectId).JiraLink,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraUsername,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraToken);
            return _jira.Projects.GetProjectsAsync().Result;
        }

        public Atlassian.Jira.Issue GetIssue(Guid subProjectId, string key)
        {
            _jira = Atlassian.Jira.Jira.CreateRestClient(_subProjectRepository.GetSubProjectById(subProjectId).JiraLink,
                                            _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraUsername,
                                            _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraToken);
            return _jira.Issues.GetIssueAsync(key).Result;
        }

        public IEnumerable<Atlassian.Jira.IssueType> GetIssueTypesByProjectKey(Guid subProjectId, string projectKey)
        {
            _jira = Atlassian.Jira.Jira.CreateRestClient(_subProjectRepository.GetSubProjectById(subProjectId).JiraLink,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraUsername,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraToken);
            return _jira.IssueTypes.GetIssueTypesForProjectAsync(projectKey).Result;
        }

        public IEnumerable<IssueTypeFieldsResponse> GetIssueTypeFieldsByIssueType(Guid subProjectId, string IssueTypeKey, string projectKey)
        {
            _jira = Atlassian.Jira.Jira.CreateRestClient(_subProjectRepository.GetSubProjectById(subProjectId).JiraLink,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraUsername,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraToken);
            var list = new List<IssueTypeFieldsResponse>();

            var rawBody = $"/rest/api/2/issue/createmeta?projectKeys={projectKey}&expand=projects.issuetypes.fields";
            var json = _jira.RestClient.ExecuteRequestAsync(Method.GET, rawBody).Result;
            var projects = json.SelectToken("projects[0]");
            var index = 0;
            foreach (var proj_obj in (JArray)projects["issuetypes"])
            {
                if (proj_obj.SelectToken("name").ToString() == IssueTypeKey)
                {
                    var token = json.SelectToken($"projects[0].issuetypes[{index}]");
                    //JToken outer = JToken.Parse(token);
                    //JObject inner = token["fields"].Value<JObject>();
                    foreach (KeyValuePair<string, JToken> sub_obj in (JObject)token["fields"])
                    {
                        var name = sub_obj.Value.SelectToken("name").ToString();
                        var key = sub_obj.Value.SelectToken("name").ToString();
                        var required = bool.Parse(sub_obj.Value.SelectToken("required").ToString());
                        var issuetypeattribute = new IssueTypeFieldsResponse(name.Replace(" ", ""), key, required);
                        list.Add(issuetypeattribute);
                    }
                }
                index++;
            }

            Atlassian.Jira.Issue issue = new Atlassian.Jira.Issue(_jira, projectKey);
            var itemProperties = issue.GetType().GetProperties().ToList<PropertyInfo>();
            foreach (var item in itemProperties)
            {
                var issueTypeFieldsResponse = list.Find(a => a.Name == item.Name);
                if (issueTypeFieldsResponse == null)
                {
                    list.Add(new IssueTypeFieldsResponse(item.Name, item.Name, false));
                }
            }

            return list;
        }

        public IEnumerable<ProjectStatus> GetIssueStatuses(Guid subProjectId, string projectKey)
        {
            _jira = Atlassian.Jira.Jira.CreateRestClient(_subProjectRepository.GetSubProjectById(subProjectId).JiraLink,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraUsername,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraToken);
            List<ProjectStatus> projectStatuses = new List<ProjectStatus>();

            var workflowSchema = $"/rest/projectconfig/1/workflowscheme/{projectKey}";
            var workflowSchemaResult = _jira.RestClient.ExecuteRequestAsync(Method.GET, workflowSchema).Result;

            var mappings = workflowSchemaResult.SelectToken("mappings[0].name").ToString();

            var projectStatus = $"/rest/projectconfig/1/workflow?workflowName={mappings}&projectKey={projectKey}";
            var projectStatusResult = _jira.RestClient.ExecuteRequestAsync(Method.GET, projectStatus).Result;
            foreach (var stat in projectStatusResult.SelectToken("sources"))
            {
                var status = stat.SelectToken("fromStatus.name").ToString();
                var id = stat.SelectToken("fromStatus.id").ToString();
                projectStatuses.Add(new ProjectStatus(id, status));
            }
            return projectStatuses;
        }

        public IEnumerable<IssuePriority> GetIssuePriorities(Guid subProjectId)
        {
            _jira = Atlassian.Jira.Jira.CreateRestClient(_subProjectRepository.GetSubProjectById(subProjectId).JiraLink,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraUsername,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraToken);
            return _jira.Priorities.GetPrioritiesAsync().Result;
        }

        public IEnumerable<IssueResolution> GetIssueResolutions(Guid subProjectId)
        {
            _jira = Atlassian.Jira.Jira.CreateRestClient(_subProjectRepository.GetSubProjectById(subProjectId).JiraLink,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraUsername,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraToken);
            return _jira.Resolutions.GetResolutionsAsync().Result;
        }

        public IEnumerable<JiraUser> GetJiraUsers(Guid subProjectId, JiraUserStatus userStatus = JiraUserStatus.Active, System.Threading.CancellationToken token = default(System.Threading.CancellationToken))
        {
            _jira = Atlassian.Jira.Jira.CreateRestClient(_subProjectRepository.GetSubProjectById(subProjectId).JiraLink,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraUsername,
                                              _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraToken);
            List<JiraUser> jiraUserList = new List<JiraUser>();
            foreach (var item in "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z".Split(','))
            {
                jiraUserList.AddRange(_jira.Users.SearchUsersAsync(item).Result);
            }
            return jiraUserList.Distinct();
        }

        public bool PingJira(string username, string password, string jiraLink)
        {
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{username}:{password}");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            HttpResponseMessage result = new HttpResponseMessage();
            try
            {
                result = client.GetAsync($"{jiraLink}/rest/api/2/status").Result;
            }
            catch
            {
                return false;
            }
            var responseBody = result.Content.ReadAsStringAsync().Result;
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                try
                {
                    var obj = JsonConvert.DeserializeObject<JiraPing[]>(responseBody);
                    if (obj.Count() == 0)
                    {
                        return false;
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool AddWebhook(AddJiraWebhook addJiraWebhook)
        {
            string jiraLink = _subProjectRepository.GetSubProjectById(addJiraWebhook.SubprojectId).JiraLink;
            string jiraUsername = _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(addJiraWebhook.SubprojectId, _workContext.FullName).JiraUsername;
            string jiraToken = _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(addJiraWebhook.SubprojectId, _workContext.FullName).JiraToken;
            _jira = Atlassian.Jira.Jira.CreateRestClient(jiraLink, jiraUsername, jiraToken);

            addJiraWebhook.Name = $"{addJiraWebhook.SubprojectId}-SAI-{addJiraWebhook.JiraProjectKey}";
            //https://saijira.validata-software.com/dataextraction/api/Jira/EventReceived?subprojectId=2ABAF18B-F759-46FE-881C-BD16FB28F30A&url=http://192.168.64.113:8080&username=haxhi&token=123456
            string baseApiLink = $"{_config.SaiJiraEndpoint}/api/Jira/EventReceived?";
            addJiraWebhook.Url = $"{baseApiLink}subprojectId={addJiraWebhook.SubprojectId}&url={jiraLink}&username={jiraUsername}&token={jiraToken}";
            var rawBody = "/rest/webhooks/1.0/webhook";
            var result = _jira.RestClient.ExecuteRequestAsync(Method.POST, rawBody, addJiraWebhook).Result;
            return true;
        }

        public bool UpdateWebhook(UpdateJiraWebhook addJiraWebhook)
        {
            string jiraLink = _subProjectRepository.GetSubProjectById(addJiraWebhook.SubprojectId).JiraLink;
            string jiraUsername = _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(addJiraWebhook.SubprojectId, _workContext.FullName).JiraUsername;
            string jiraToken = _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(addJiraWebhook.SubprojectId, _workContext.FullName).JiraToken;
            _jira = Atlassian.Jira.Jira.CreateRestClient(jiraLink, jiraUsername, jiraToken);

            addJiraWebhook.Name = $"{addJiraWebhook.SubprojectId}-SAI-{addJiraWebhook.JiraProjectKey}";
            //https://saijira.validata-software.com/dataextraction/api/Jira/EventReceived?subprojectId=2ABAF18B-F759-46FE-881C-BD16FB28F30A&url=http://192.168.64.113:8080&username=haxhi&token=123456
            string baseApiLink = $"{_config.SaiJiraEndpoint}/api/Jira/EventReceived?";
            addJiraWebhook.Url = $"{baseApiLink}subprojectId={addJiraWebhook.SubprojectId}&url={jiraLink}&username={jiraUsername}&token={jiraToken}";
            var rawBody = addJiraWebhook.Self.Substring(addJiraWebhook.Self.IndexOf("/rest/")); //"/rest/webhooks/1.0/webhook";
            var result = _jira.RestClient.ExecuteRequestAsync(Method.PUT, rawBody, addJiraWebhook).Result;
            return true;
        }

        public IEnumerable<GetJiraWebhooks> GetWebhooks(Guid subProjectId)
        {
            string jiraLink = _subProjectRepository.GetSubProjectById(subProjectId).JiraLink;
            string jiraUsername = _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraUsername;
            string jiraToken = _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(subProjectId, _workContext.FullName).JiraToken;
            _jira = Atlassian.Jira.Jira.CreateRestClient(jiraLink, jiraUsername, jiraToken);

            var rawBody = "/rest/webhooks/1.0/webhook";
            var result = _jira.RestClient.ExecuteRequestAsync(Method.GET, rawBody).Result;
            return result.ToObject<IEnumerable<GetJiraWebhooks>>();
        }
    }
}