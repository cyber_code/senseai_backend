﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jira.Issue
{
    public sealed class IssueTypeFieldsResponse
    {
        public IssueTypeFieldsResponse(string name, string id, bool isRequired)
        {
            Name = name;
            Id = id;
            IsRequired = isRequired;
        }

        public string Name { get; set; }

        public string Id { get; set; }

        public bool IsRequired { get; set; }
    }
}