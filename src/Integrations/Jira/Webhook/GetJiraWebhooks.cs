﻿using Newtonsoft.Json;
using System;

namespace Jira.Webhook
{
    public sealed class GetJiraWebhooks
    {
        [JsonProperty(PropertyName = "self")]
        public string Self { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "events")]
        public string[] Events { get; set; }

        [JsonProperty(PropertyName = "filters")]
        public Filters Filters { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        public bool Enabled { get; set; }
    }
}