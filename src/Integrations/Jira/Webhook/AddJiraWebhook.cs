﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Jira.Webhook
{
    public sealed class AddJiraWebhook
    {
        public Guid SubprojectId { get; set; }

        public string JiraProjectKey { get; set; }

        public string JiraProjectName { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "events")]
        public string[] Events { get; set; }

        [JsonProperty(PropertyName = "filters")]
        public Filters Filters { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "enabled")]
        public bool Enabled { get; set; }
    }

    public class Filters
    {
        [JsonProperty(PropertyName = "issue-related-events-section")]
        public string IssueRelatedEventsSection { get; set; }
    }
}