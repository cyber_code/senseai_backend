﻿using Newtonsoft.Json;
using System;

namespace Jira.Webhook
{
    public sealed class UpdateJiraWebhook
    {
        [JsonProperty(PropertyName = "self")]
        public string Self { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraProjectKey { get; set; }

        public string JiraProjectName { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "events")]
        public string[] Events { get; set; }

        [JsonProperty(PropertyName = "filters")]
        public Filters Filters { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "enabled")]
        public bool Enabled { get; set; }
    }
}