﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jira.Project
{
    public class ProjectStatus
    {
        public ProjectStatus(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public string Id { get; set; }

        public string Name { get; set; }
    }
}