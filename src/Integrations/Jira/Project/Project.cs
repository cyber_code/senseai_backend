﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jira.Project
{
    public class JiraProject
    {
        public Projects[] Projects { get; set; }
    }

    public class Projects
    {
        public string Id { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public IssueTypes[] IssueTypes { get; set; }
    }

    public class IssueTypes
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public Fields Fields { get; set; }
    }

    public class Fields
    {
        public Field Summary { get; set; }

        public Field IssueType { get; set; }

        public Field description { get; set; }
    }

    public class Field
    {
        public bool IsRequired { get; set; }

        public string Name { get; set; }
    }
}