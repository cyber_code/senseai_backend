﻿using Atlassian.Jira;
using Jira.Issue;
using Jira.Project;
using Jira.Webhook;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jira
{
    public interface IJiraClient
    {
        Atlassian.Jira.Issue GetIssue(Guid subProjectId, string key);

        IEnumerable<Atlassian.Jira.Project> GetAllProjects(Guid subProjectId);

        IEnumerable<IssueType> GetIssueTypesByProjectKey(Guid subProjectId, string projectKey);

        IEnumerable<IssueTypeFieldsResponse> GetIssueTypeFieldsByIssueType(Guid subProjectId, string issueTypeId, string projectKey);

        IEnumerable<ProjectStatus> GetIssueStatuses(Guid subProjectId, string projectKey);

        IEnumerable<IssuePriority> GetIssuePriorities(Guid subProjectId);

        IEnumerable<IssueResolution> GetIssueResolutions(Guid subProjectId);

        IEnumerable<JiraUser> GetJiraUsers(Guid subProjectId, JiraUserStatus userStatus = JiraUserStatus.Active, System.Threading.CancellationToken token = default(System.Threading.CancellationToken));

        bool PingJira(string username, string password, string jiraLink);

        bool AddWebhook(AddJiraWebhook addJiraWebhook);

        bool UpdateWebhook(UpdateJiraWebhook updateJiraWebhook);

        IEnumerable<GetJiraWebhooks> GetWebhooks(Guid subProjectId);
    }
}