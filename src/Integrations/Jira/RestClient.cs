﻿using Newtonsoft.Json;
using Core.Abstractions.Bindings;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;

namespace Jira
{
    public class RestClient : IRestClient
    {
        #region [ Properties ]

        protected readonly IBindingConfiguration _bindingConfiguration;

        private HttpClient _client;

        private HttpClient Client => _client ?? (_client = new HttpClient(handler));

        #endregion [ Properties ]

        #region [ Initialization ]

        private HttpClientHandler handler = new HttpClientHandler();

        private int size = 1000;

        /// <inheritdoc />
        /// <summary>
        ///     Sets the api binging propertys for requests made by this client instance
        /// </summary>
        /// <param name="bindingConfiguration"></param>
        protected RestClient(IBindingConfiguration bindingConfiguration)
        {
            _bindingConfiguration = bindingConfiguration;
            ConfigurateHttpClient();
        }

        private void ConfigurateHttpClient()
        {
            if (string.IsNullOrEmpty(_bindingConfiguration.Client.Endpoint))
            {
                throw new Exception("BaseAddress/Endpoint is required!");
            }

            Client.BaseAddress = new Uri(_bindingConfiguration.Client.Endpoint);
            Client.Timeout = _bindingConfiguration.Client.Timeout == 0 ? new TimeSpan(0, 0, 0, 10) : new TimeSpan(0, 0, 0, _bindingConfiguration.Client.Timeout);

            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Range = new RangeHeaderValue(0, size);

            if (_bindingConfiguration.Headers != null)
            {
                //Add here other header config's
                Client.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse(_bindingConfiguration.Headers.ContentType));
            }
        }

        #endregion [ Initialization ]

        #region [ IRestClient: Implementation]

        /// <inheritdoc />
        public Task<T> GetAsync<T>(string uri)
        {
            return InvokeAsync<T>(
                client => client.GetAsync(uri),
                response => response.Content.ReadAsAsync<T>());
        }

        /// <inheritdoc />
        public Task<T> PostAsJsonAsync<T>(object data, string uri)
        {
            StringContent content = new StringContent(JsonConvert.SerializeObject(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() }));
            return InvokeAsync<T>(
                        client => client.PostAsync(uri, content),
                        response => response.Content.ReadAsAsync<T>());
        }

        /// <inheritdoc />
        public Task PostAsJsonAsync(object data, string uri)
        {
            return InvokeAsync<object>(client => client.PostAsJsonAsync(uri, data));
        }

        /// <inheritdoc />
        public Task PutAsJsonAsync(object data, string uri)
        {
            return InvokeAsync<object>(client => client.PutAsJsonAsync(uri, data));
        }

        /// <inheritdoc />
        public Task<T> PutAsJsonAsync<T>(object data, string uri)
        {
            return InvokeAsync<T>(
                client => client.PutAsJsonAsync(uri, data),
                response => response.Content.ReadAsAsync<T>());
        }

        #endregion [ IRestClient: Implementation]

        private async Task<T> InvokeAsync<T>(
            Func<HttpClient, Task<HttpResponseMessage>> operation,
            Func<HttpResponseMessage, Task<T>> actionOnResponse = null)
        {
            //TODO: Add ignore ssl TLS 2 certificate
            if (operation == null)
                throw new ArgumentNullException(nameof(operation));
            HttpResponseMessage response;

            try
            {
                response = await operation(Client).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                throw new JiraException("Error communicating with Jira service.", e);
                //throw new Exception($"Error connectiong to resource server!{Environment.NewLine}Error message: '{e.Message}'");
            }

            if (!response.IsSuccessStatusCode)
            {
                var exception = new Exception($"Jira returned an error. StatusCode : {response.StatusCode}");
                exception.Data.Add("StatusCode", response.StatusCode);
                throw new JiraException($"Jira returned an error. StatusCode : {response.StatusCode}", exception);
            }

            return actionOnResponse != null ? await actionOnResponse(response).ConfigureAwait(false) : default(T);
        }
    }
}