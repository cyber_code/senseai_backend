﻿using System;
using System.Runtime.Serialization;

namespace Jira
{
    [Serializable]
    public class JiraException : Exception
    {
        public JiraException()
        {
        }

        public JiraException(string message) : base(message)
        {
        }

        public JiraException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public JiraException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}