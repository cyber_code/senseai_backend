﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ValidataT24Deployment
{
    public class AAProductDesigner
    {
        public AAProductDesigner()
        {
            CURRENCY = new List<MVField>();
            PROPERTY_CONDITIONS = new List<PropertyConditions>();
            CALCULATION_SOURCE = new List<CalculationsSource>();
        }
        public string ID { get; set; }

        public string DESCRIPTION { get; set; }
        public string PARENT_PRODUCT { get; set; }

        public string PRODUCT_GROUP { get; set; }

        public List<MVField> CURRENCY { get; set; }

        public List<PropertyConditions> PROPERTY_CONDITIONS { get; set; }

        public List<CalculationsSource> CALCULATION_SOURCE { get; set; }

    }

    public class MVField
    {
        public int MVIndex { get; set; }

        public int SVIndex { get; set; }

        public string Value { get; set; }
    }


    public class PropertyConditions
    {
        public PropertyConditions(int MvIndex,string propperty,string prdProperty,string arrLink)
        {
            PROPERTY = new MVField { MVIndex = MvIndex, SVIndex = 1, Value = propperty };
            PRD_PROPERTY = new MVField { MVIndex = MvIndex, SVIndex = 1, Value = prdProperty };
            ARR_LINK = new MVField { MVIndex = MvIndex, SVIndex = 1, Value = arrLink };

        }
        public MVField PROPERTY { get; set; }

        public MVField PRD_PROPERTY { get; set; }

        public MVField ARR_LINK { get; set; }
    }

    public class CalculationsSource
    {
        public CalculationsSource(int MvIndex, string calcProperty, string sourceType, string sourceBalance)
        {
            CALC_PROPERTY = new MVField { MVIndex = MvIndex, SVIndex = 1, Value = calcProperty };
            SOURCE_TYPE = new MVField { MVIndex = MvIndex, SVIndex = 1, Value = sourceType };
            SOURCE_BALANCE = new MVField { MVIndex = MvIndex, SVIndex = 1, Value = sourceBalance };

        }
        public MVField CALC_PROPERTY { get; set; }

        public MVField SOURCE_TYPE { get; set; }

        public MVField SOURCE_BALANCE { get; set; }
    }
}
