﻿using System.Collections.Generic;

namespace ValidataT24Deployment
{
    public class AAPrdDesAccount
    {
        public string DESCRIPTION { get; set; }
        public string FULL_DESCRIPTION { get; set; }
        public string CATEGORY { get; set; }
        public string DATE_CONVENTION { get; set; }
        public string DATE_ADJUSTMENT { get; set; }
        public List<MVField> BUS_DAY_CENTRES { get; set; }
        public string INACTIVE_MONTHS { get; set; }
        public string PASSBOOK { get; set; }
        public string SHADOW_ACCOUNT { get; set; }
        public string GENERATE_IBAN { get; set; }
        public string ACCOUNTING_COMPANY { get; set; }
        public string BASE_DATE_KEY { get; set; }
        public string BALANCE_TREATMENT { get; set; }
        public string EXTERNAL_POSTING { get; set; }
        public string HVT_FLAG { get; set; }
        public string MULTI_CURRENCY { get; set; }
        public string BALANCE_CONVERSION_MKT { get; set; }
        public string EARLY_PROCESSING { get; set; }
        public string ON_RESTRUCTURE { get; set; }
        public string DELINK_WITH_BALANCE { get; set; }
        public string DEFAULT_NEGOTIABLE { get; set; }
        public string ID { get; set; }
    }
}
