﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidataT24Deployment
{
    public class AAPrdDesInterest
    {
        public string DESCRIPTION { get; set; }
        public string FULL_DESCRIPTION { get; set; }
        public string CALC_THRESHOLD { get; set; }
        public string DAY_BASIS { get; set; }
        public string ACCRUAL_RULE { get; set; }
        public string RATE_TIER_TYPE { get; set; }
        public string REFER_LIMIT { get; set; }
        public string COMPOUND_TYPE { get; set; }
        public List<MVField> FIXED_RATE { get; set; }
        public List<MVField> FLOATING_INDEX { get; set; }
        public List<MVField> PERIODIC_INDEX { get; set; }
        public List<MVField> PERIODIC_PERIOD { get; set; }
        public List<MVField> PERIODIC_METHOD { get; set; }
        public List<MVField> PERIODIC_RESET { get; set; }
        public List<MVField> CUSTOM_RATE { get; set; }
        public List<MVField> LINKED_RATE { get; set; }
        public List<MVField> MARGIN_TYPE { get; set; }
        public List<MVField> MARGIN_OPER { get; set; }
        public List<MVField> MARGIN_RATE { get; set; }
        public List<MVField> TIER_MIN_RATE { get; set; }
        public List<MVField> TIER_MAX_RATE { get; set; }
        public List<MVField> TIER_NEGATIVE_RATE { get; set; }
        public List<MVField> TIER_AMOUNT { get; set; }
        public List<MVField> TIER_PERCENT { get; set; }
        public List<MVField> ON_ACTIVITY { get; set; }
        public List<MVField> RECALCULATE { get; set; }
        public string COMPOUND_YIELD_METHOD { get; set; }
        public string MIN_INT_AMOUNT { get; set; }
        public string MIN_INT_WAIVE { get; set; }
        public string SUPPRESS_ACCRUAL { get; set; }
        public string ACCOUNTING_MODE { get; set; }
        public string INTEREST_METHOD { get; set; }
        public string RATE_TYPE { get; set; }
        public string INTERNAL_BOOKING { get; set; }
        public string CUSTOM_RATE_CALC { get; set; }
        public string RUNTIME_RATE_CALC { get; set; }
        public List<MVField> CUSTOM_TYPE { get; set; }
        public List<MVField> CUSTOM_NAME { get; set; }
        public List<MVField> CUSTOM_VALUE { get; set; }
        public string FT_TXN_TYPE { get; set; }
        public string ADJUST_OPTION { get; set; }

        public string ID { get; internal set; }
    }
}
