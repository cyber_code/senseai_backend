﻿using AXMLEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidataT24Deployment
{
    internal class Helper
    {
        internal static string GetProofFailMsg(Instance manager)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var pError in manager.GetMultivalueAttributesByShortFieldName("PRODUCT.ERROR"))
            {
                sb.AppendLine($"Error: {pError.Value}");
            }

            foreach (var sugg in manager.GetMultivalueAttributesByShortFieldName("SUGGESTION"))
            {
                sb.AppendLine($"Suggestion: {sugg.Value}");
            }

            return sb.ToString();
        }

        internal static bool HasProofErrors(Instance manager)
        {
            return !string.IsNullOrEmpty(manager.GetAttributeByShortName("PRODUCT.ERROR")?.Value);
        }

        internal static string GetProdDate(string t24Id)
        {
            return t24Id.Substring(t24Id.IndexOf("-") + 1);
        }

        internal static string GetProdName(string t24Id)
        {
            return t24Id.Contains("-") ? t24Id.Substring(0, t24Id.IndexOf("-")) : t24Id;
        }

    }
}
