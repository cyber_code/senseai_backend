﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AXMLEngine;
using T24TelnetAdapter;

namespace ValidataT24Deployment
{
    public class RequestManagerWrapper
    {
        private string _TelnetSetName;

        public RequestManagerWrapper(string telnetSetName)
        {
            this._TelnetSetName = telnetSetName;
        }

        public bool DeployAAProdFull(AAProductDesigner aadesigner, AAPrdDesAccount aAPrdDesAccount, AAPrdDesInterest aAPrdDesInterest, out string errText)
        {

            AXMLEngine.Instance designerInstance = AXMLConvert.AAProductDesigner(aadesigner);

            string availableDate;
            DeploymentOutput result = null;

            RequestManager reqman = new RequestManager(
                T24TelnetAdapter.OperatingSystem.Win,
                _TelnetSetName,
                "",
                "",
                "",
                "",
                true
                );

            try
            {
                // If there is a AAPrdDesAccount deploy it
                if (null != aAPrdDesAccount)
                {
                    Instance apdAccount = AXMLConvert.AAPrdDesAccount(aAPrdDesAccount);
                    result = inputInstance(apdAccount, reqman, out errText);
                    if (result.Outcome != DeploymentOutcome.Failed)
                    {
                        result = authoriseInstance(result.UpdatedInstanceOnT24?.T24Id?? apdAccount.T24Id, apdAccount.TypicalName, reqman, out errText);
                    }                    
                }

                // If everything is OK and there is a AAPrdDesInterest deploy it
                if (null != aAPrdDesInterest && (result == null || result.Outcome != DeploymentOutcome.Failed))
                {
                    Instance apdInterest = AXMLConvert.AAPrdDesInterest(aAPrdDesInterest);
                    result = inputInstance(apdInterest, reqman, out errText);
                    if (result.Outcome != DeploymentOutcome.Failed)
                    {
                        result = authoriseInstance(result.UpdatedInstanceOnT24?.T24Id?? apdInterest.T24Id, apdInterest.TypicalName, reqman, out errText);
                    }
                }

                // If everything is OK deploy the AA product designer
                if (result == null || result.Outcome != DeploymentOutcome.Failed)
                {
                    result = inputInstance(designerInstance, reqman, out errText);
                    if (result.Outcome != DeploymentOutcome.Failed)
                    {
                        var apdID = result.UpdatedInstanceOnT24?.T24Id ?? designerInstance.T24Id;
                        availableDate = Helper.GetProdDate(apdID);
                        result = authoriseInstance(apdID , designerInstance.TypicalName, reqman, out errText);

                        if (result.Outcome != DeploymentOutcome.Failed)
                        {

                            AXMLEngine.Instance managerInst = new AXMLEngine.Instance("AA.PRODUCT.MANAGER", "dummyCatalog");
                            managerInst.AddAttribute("@ID", Helper.GetProdName(designerInstance.T24Id));
                            managerInst.AddAttribute("DESCRIPTION", "AA new prod.");
                            managerInst.AddAttribute("ACTION-1~1", "PROOF");

                            managerInst.AddAttribute("AVAILABLE.DATE-1~1", availableDate);
                            managerInst.AddAttribute("PROCESS.METHOD-1~1", "ONLINE");

                            result = iaInstance(managerInst, reqman, out errText);

                            if (result.Outcome != DeploymentOutcome.Failed)
                            {

                                if (Helper.HasProofErrors(result.UpdatedInstanceOnT24))
                                {
                                    errText = $"Failed proofing of product:\r\n{Helper.GetProofFailMsg(result.UpdatedInstanceOnT24)}";
                                    result.Outcome = DeploymentOutcome.Failed;
                                }
                                else
                                {

                                    managerInst.AttributeByName("ACTION-1~1").Value = "PUBLISH";
                                    result = iaInstance(managerInst, reqman, out errText);
                                }
                            }
                        }
                    }
                }
                else errText = string.Empty;

            }
            catch (Exception eee)
            {
                errText = eee.Message;
                result = new DeploymentOutput() { Outcome = DeploymentOutcome.Failed };
            }

            if (string.IsNullOrEmpty(errText) && result?.Outcome != DeploymentOutcome.Successful)
                errText = result?.Details;


            bool prodPublished = result.Outcome != DeploymentOutcome.Failed && CheckProductPublished(aadesigner.ID, reqman);
            reqman.Release();

            return prodPublished;

        }

        #region For Unit Testing

        public bool CheckProductPublished(string atID, RequestManager reqman = null)
        {
            bool newReqMan = null == reqman;

            if (newReqMan)
                reqman = new RequestManager(
                  T24TelnetAdapter.OperatingSystem.Win,
                  _TelnetSetName,
                  "",
                  "",
                  "",
                  "",
                  true
                  );

            RequestManager.OFSResponseErrorType errType;
            string errText;
            var prodInst = reqman.GetRecordDataByCompanyAndID(new AXMLEngine.MetadataTypical() { Name = "AA.PRODUCT" }, "", atID, true, out errType, out errText);

            if (newReqMan)
                reqman.Release();

            return !string.IsNullOrEmpty(prodInst?.GetAttributeByShortName("LAST.PUBLISHED")?.Value);
        }

        public DeploymentOutput DeployPrdDesAccount(AAPrdDesAccount aAPrdDesAccount, out string errText)
        {
            Instance inst = AXMLConvert.AAPrdDesAccount(aAPrdDesAccount);

            RequestManager reqman = new RequestManager(
                T24TelnetAdapter.OperatingSystem.Win,
                _TelnetSetName,
                "",
                "",
                "",
                "",
                true
                );

            DeploymentOutput result = inputInstance(inst, reqman, out errText);
            if (result.Outcome == DeploymentOutcome.Successful)
            {
                result = authoriseInstance(result.UpdatedInstanceOnT24.T24Id, inst.TypicalName, reqman, out errText);
            }
            reqman.Release();

            return result;
        }

        public DeploymentOutput DeployPrdDesInterest(AAPrdDesInterest aAPrdDesInterest, out string errText)
        {
            Instance inst = AXMLConvert.AAPrdDesInterest(aAPrdDesInterest);

            RequestManager reqman = new RequestManager(
                T24TelnetAdapter.OperatingSystem.Win,
                _TelnetSetName,
                "",
                "",
                "",
                "",
                true
                );

            DeploymentOutput result = inputInstance(inst, reqman, out errText);
            if (result.Outcome == DeploymentOutcome.Successful)
            {
                result = authoriseInstance(result.UpdatedInstanceOnT24.T24Id, inst.TypicalName, reqman, out errText);
            }
            reqman.Release();

            return result;
        }

        public DeploymentOutput DeployProofPublishAAProd(AAProductDesigner aadesigner, out string errText)
        {


            AXMLEngine.Instance designerInstance = AXMLConvert.AAProductDesigner(aadesigner);

            string availableDate;

            RequestManager reqman = new RequestManager(
                T24TelnetAdapter.OperatingSystem.Win,
                _TelnetSetName,
                "",
                "",
                "",
                "",
                true
                );

            DeploymentOutput result = inputInstance(designerInstance, reqman, out errText);
            if (result.Outcome == DeploymentOutcome.Successful)
            {
                availableDate = Helper.GetProdDate(result.UpdatedInstanceOnT24.T24Id);
                result = authoriseInstance(result.UpdatedInstanceOnT24.T24Id, designerInstance.TypicalName, reqman, out errText);

                if (result.Outcome == DeploymentOutcome.Successful)
                {

                    AXMLEngine.Instance managerInst = new AXMLEngine.Instance("AA.PRODUCT.MANAGER", "dummyCatalog");
                    managerInst.AddAttribute("@ID", Helper.GetProdName(designerInstance.T24Id));
                    managerInst.AddAttribute("DESCRIPTION", "AA new prod.");
                    managerInst.AddAttribute("ACTION-1~1", "PROOF");

                    managerInst.AddAttribute("AVAILABLE.DATE-1~1", availableDate);
                    managerInst.AddAttribute("PROCESS.METHOD-1~1", "ONLINE");

                    result = iaInstance(managerInst, reqman, out errText);

                    if (result.Outcome == DeploymentOutcome.Successful)
                    {

                        if (Helper.HasProofErrors(result.UpdatedInstanceOnT24))
                        {
                            errText = Helper.GetProofFailMsg(result.UpdatedInstanceOnT24);
                            result.Outcome = DeploymentOutcome.Failed;
                        }
                        else
                        {

                            managerInst.AttributeByName("ACTION-1~1").Value = "PUBLISH";
                            result = iaInstance(managerInst, reqman, out errText);
                        }
                    }
                }


            }

            if (string.IsNullOrEmpty(errText))
                errText
                    = result.Outcome != DeploymentOutcome.Successful
                    ? result.Details
                    : string.Empty;

            reqman.Release();

            return result;



        }

        public DeploymentOutput DeployProofPublishAAProd(AXMLEngine.Instance designerInstance, out string errText)
        {
            string availableDate;

            RequestManager reqman = new RequestManager(
                T24TelnetAdapter.OperatingSystem.Win,
                _TelnetSetName,
                "",
                "",
                "",
                "",
                true
                );

            DeploymentOutput result = inputInstance(designerInstance, reqman, out errText);
            if (result.Outcome == DeploymentOutcome.Successful)
            {
                availableDate = result.UpdatedInstanceOnT24.T24Id.Substring(designerInstance.T24Id.Length + 1);
                result = authoriseInstance(result.UpdatedInstanceOnT24.T24Id, designerInstance.TypicalName, reqman, out errText);

                if (result.Outcome == DeploymentOutcome.Successful)
                {

                    AXMLEngine.Instance managerInst = new AXMLEngine.Instance("AA.PRODUCT.MANAGER", "dummyCatalog");
                    managerInst.AddAttribute("@ID", designerInstance.T24Id);
                    managerInst.AddAttribute("DESCRIPTION", "AA new prod.");
                    managerInst.AddAttribute("ACTION-1~1", "PROOF");

                    managerInst.AddAttribute("AVAILABLE.DATE-1~1", availableDate);
                    managerInst.AddAttribute("PROCESS.METHOD-1~1", "ONLINE");

                    result = iaInstance(managerInst, reqman, out errText);

                    if (result.Outcome == DeploymentOutcome.Successful)
                    {
                        managerInst.AttributeByName("ACTION-1~1").Value = "PUBLISH";
                        result = iaInstance(managerInst, reqman, out errText);
                    }
                }
            }


            reqman.Release();

            return result;
        }

        public DeploymentOutput DeployAnyInstance(AXMLEngine.Instance inst, out string errText)
        {
            RequestManager reqman = new RequestManager(
                T24TelnetAdapter.OperatingSystem.Win,
                _TelnetSetName,
                "",
                "",
                "",
                "",
                true
                );

            DeploymentOutput result = inputInstance(inst, reqman, out errText);
            if (result.Outcome == DeploymentOutcome.Successful)
            {
                result = authoriseInstance(result.UpdatedInstanceOnT24.T24Id, inst.TypicalName, reqman, out errText);
            }
            reqman.Release();

            return result;
        }
        
        #endregion

        #region I/A OFS methods
        private DeploymentOutput authoriseInstance(string t24Id, string typical, RequestManager reqman, out string errText)
        {
            DeploymentOutput result;

            AXMLEngine.Instance inst = new AXMLEngine.Instance(typical, "dummyCatalog");
            inst.AddAttribute("@ID", t24Id);

            try
            {
                result = reqman.DeployRecord(new DeploymentRecord()
                {
                    RecordInstance = inst,
                    RecordName = inst.GetAttributeValue("@ID"),
                    DeploymentPack = "Dummy Packet",
                    Company = "",
                    SkipChecksForAuthorization = true,
                    DeploymentMethod = DeploymentMethod.Record_A,
                    SkipValueDeletions = true,
                    InputSameValues = true,
                    UseReplaceOption = false

                }, true, false);



                errText
                    = result.Outcome != DeploymentOutcome.Successful
                    ? $"Error during authorise of {typical}>{t24Id}:\r\n{result.Details}"
                    : string.Empty;
            }
            catch (Exception eee)
            {
                result = new DeploymentOutput() { Outcome = DeploymentOutcome.Failed };
                errText = $"Exception during authorise of {inst.TypicalName}>{t24Id}:\r\n {eee.Message}";
            }

            return result;
        }

        private DeploymentOutput inputInstance(AXMLEngine.Instance inst, RequestManager reqman, out string errText)
        {
            DeploymentOutput result;
            string t24Id = inst.GetAttributeValue("@ID");

            try
            {
                result = reqman.DeployRecord(new DeploymentRecord()
                {
                    RecordInstance = inst,
                    RecordName = t24Id,
                    DeploymentPack = "Dummy Packet",
                    Company = "",
                    DeploymentMethod = DeploymentMethod.Record_I,
                    SkipValueDeletions = true,
                    InputSameValues = true,
                    UseReplaceOption = false,
                    AllowDifferentResponseID = true

                }, true, false);



                errText
                    = result.Outcome != DeploymentOutcome.Successful
                    ? $"Error during input of {inst.TypicalName}>{t24Id}:\r\n{result.Details}"
                    : string.Empty;
            }
            catch (Exception eee)
            {
                result = new DeploymentOutput() { Outcome = DeploymentOutcome.Failed };
                errText = $"Exception during input of {inst.TypicalName}>{t24Id}:\r\n {eee.Message}";
            }

            return result;
        }

        private DeploymentOutput iaInstance(AXMLEngine.Instance inst, RequestManager reqman, out string errText)
        {
            DeploymentOutput result;
            string t24Id = inst.GetAttributeValue("@ID");

            try
            {
                result = reqman.DeployRecord(new DeploymentRecord()
                {
                    RecordInstance = inst,
                    RecordName = t24Id,
                    DeploymentPack = "Dummy Packet",
                    Company = "",
                    DeploymentMethod = DeploymentMethod.Record_IA,
                    SkipValueDeletions = true,
                    InputSameValues = true,
                    UseReplaceOption = false,
                    AllowDifferentResponseID = true

                }, true, false);



                errText
                    = result.Outcome != DeploymentOutcome.Successful
                    ? $"Error during IA of {inst.TypicalName}>{t24Id}:\r\n{result.Details}"
                    : string.Empty;
            }
            catch (Exception eee)
            {
                result = new DeploymentOutput() { Outcome = DeploymentOutcome.Failed };
                errText = $"Exception during IA of {inst.TypicalName}>{t24Id}:\r\n {eee.Message}";
            }

            return result;
        }
        
        #endregion
    }
}
