﻿using AXMLEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidataT24Deployment
{
    public class AXMLConvert
    {

        public static Instance AAProductDesigner(AAProductDesigner aadesigner)
        {
            var atID = aadesigner.ID;
            var typical = "AA.PRODUCT.DESIGNER";

            AXMLEngine.Instance inst = new AXMLEngine.Instance(typical, "dummyCatalog");
            inst.AddAttribute("@ID", atID);
            inst.AddAttribute("DESCRIPTION", aadesigner.DESCRIPTION);
            inst.AddAttribute("PRODUCT.GROUP-1~1", aadesigner.PRODUCT_GROUP);

            if (!string.IsNullOrEmpty(aadesigner.PARENT_PRODUCT))
                inst.AddAttribute("PARENT.PRODUCT", aadesigner.PARENT_PRODUCT);

            if (null != aadesigner.CURRENCY)
                foreach (var currMv in aadesigner.CURRENCY)
                    inst.AddAttribute($"CURRENCY-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            if (null != aadesigner.PROPERTY_CONDITIONS)
                foreach (var currMv in aadesigner.PROPERTY_CONDITIONS)
                {
                    inst.AddAttribute($"PROPERTY-{currMv.PROPERTY.MVIndex}~{currMv.PROPERTY.SVIndex}", currMv.PROPERTY.Value);
                    inst.AddAttribute($"PRD.PROPERTY-{currMv.PRD_PROPERTY.MVIndex}~{currMv.PRD_PROPERTY.SVIndex}", currMv.PRD_PROPERTY.Value);
                    inst.AddAttribute($"ARR.LINK-{currMv.ARR_LINK.MVIndex}~{currMv.ARR_LINK.SVIndex}", currMv.ARR_LINK.Value);
                }

            if (null != aadesigner.CALCULATION_SOURCE)
                foreach (var currMv in aadesigner.CALCULATION_SOURCE)
                {
                    inst.AddAttribute($"CALC.PROPERTY-{currMv.CALC_PROPERTY.MVIndex}~{currMv.CALC_PROPERTY.SVIndex}", currMv.CALC_PROPERTY.Value);
                    inst.AddAttribute($"SOURCE.TYPE-{currMv.SOURCE_TYPE.MVIndex}~{currMv.SOURCE_TYPE.SVIndex}", currMv.SOURCE_TYPE.Value);
                    inst.AddAttribute($"SOURCE.BALANCE-{currMv.SOURCE_BALANCE.MVIndex}~{currMv.SOURCE_BALANCE.SVIndex}", currMv.SOURCE_BALANCE.Value);
                }

            return inst;
        }


        public static Instance AAPrdDesAccount(AAPrdDesAccount aAPrdDesAccount)
        {
            string val = "";
            string atrName;

            var atID = aAPrdDesAccount.ID;
            var typical = "AA.PRD.DES.ACCOUNT";

            AXMLEngine.Instance inst = new AXMLEngine.Instance(typical, "dummyCatalog");
            inst.AddAttribute("@ID", atID);

            atrName = "DESCRIPTION".Replace("_", ".");
            val = aAPrdDesAccount.DESCRIPTION;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "FULL_DESCRIPTION".Replace("_", ".");
            val = aAPrdDesAccount.FULL_DESCRIPTION;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "CATEGORY".Replace("_", ".");
            val = aAPrdDesAccount.CATEGORY;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "DATE_CONVENTION".Replace("_", ".");
            val = aAPrdDesAccount.DATE_CONVENTION;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "DATE_ADJUSTMENT".Replace("_", ".");
            val = aAPrdDesAccount.DATE_ADJUSTMENT;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "INACTIVE_MONTHS".Replace("_", ".");
            val = aAPrdDesAccount.INACTIVE_MONTHS;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "PASSBOOK".Replace("_", ".");
            val = aAPrdDesAccount.PASSBOOK;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "SHADOW_ACCOUNT".Replace("_", ".");
            val = aAPrdDesAccount.SHADOW_ACCOUNT;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "GENERATE_IBAN".Replace("_", ".");
            val = aAPrdDesAccount.GENERATE_IBAN;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "ACCOUNTING_COMPANY".Replace("_", ".");
            val = aAPrdDesAccount.ACCOUNTING_COMPANY;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "BASE_DATE_KEY".Replace("_", ".");
            val = aAPrdDesAccount.BASE_DATE_KEY;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "BALANCE_TREATMENT".Replace("_", ".");
            val = aAPrdDesAccount.BALANCE_TREATMENT;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "EXTERNAL_POSTING".Replace("_", ".");
            val = aAPrdDesAccount.EXTERNAL_POSTING;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "HVT_FLAG".Replace("_", ".");
            val = aAPrdDesAccount.HVT_FLAG;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "MULTI_CURRENCY".Replace("_", ".");
            val = aAPrdDesAccount.MULTI_CURRENCY;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "BALANCE_CONVERSION_MKT".Replace("_", ".");
            val = aAPrdDesAccount.BALANCE_CONVERSION_MKT;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "EARLY_PROCESSING".Replace("_", ".");
            val = aAPrdDesAccount.EARLY_PROCESSING;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "ON_RESTRUCTURE".Replace("_", ".");
            val = aAPrdDesAccount.ON_RESTRUCTURE;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "DELINK_WITH_BALANCE".Replace("_", ".");
            val = aAPrdDesAccount.DELINK_WITH_BALANCE;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "DEFAULT_NEGOTIABLE".Replace("_", ".");
            val = aAPrdDesAccount.DEFAULT_NEGOTIABLE;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);
            

            atrName = "BUS_DAY_CENTRES".Replace("_", ".");
            if (aAPrdDesAccount.BUS_DAY_CENTRES != null)
                foreach (var currMv in aAPrdDesAccount.BUS_DAY_CENTRES)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            return inst;

        }

        public static Instance AAPrdDesInterest(AAPrdDesInterest aAPrdDesInterest)
        {
            string val = "";
            string atrName;

            var atID = aAPrdDesInterest.ID;
            var typical = "AA.PRD.DES.INTEREST";

            AXMLEngine.Instance inst = new AXMLEngine.Instance(typical, "dummyCatalog");
            inst.AddAttribute("@ID", atID);

            atrName = "DESCRIPTION".Replace("_", ".");
            val = aAPrdDesInterest.DESCRIPTION;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "FULL_DESCRIPTION".Replace("_", ".");
            val = aAPrdDesInterest.FULL_DESCRIPTION;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "CALC_THRESHOLD".Replace("_", ".");
            val = aAPrdDesInterest.CALC_THRESHOLD;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "DAY_BASIS".Replace("_", ".");
            val = aAPrdDesInterest.DAY_BASIS;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "ACCRUAL_RULE".Replace("_", ".");
            val = aAPrdDesInterest.ACCRUAL_RULE;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "RATE_TIER_TYPE".Replace("_", ".");
            val = aAPrdDesInterest.RATE_TIER_TYPE;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "REFER_LIMIT".Replace("_", ".");
            val = aAPrdDesInterest.REFER_LIMIT;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "COMPOUND_TYPE".Replace("_", ".");
            val = aAPrdDesInterest.COMPOUND_TYPE;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "COMPOUND_YIELD_METHOD".Replace("_", ".");
            val = aAPrdDesInterest.COMPOUND_YIELD_METHOD;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "MIN_INT_AMOUNT".Replace("_", ".");
            val = aAPrdDesInterest.MIN_INT_AMOUNT;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "MIN_INT_WAIVE".Replace("_", ".");
            val = aAPrdDesInterest.MIN_INT_WAIVE;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "SUPPRESS_ACCRUAL".Replace("_", ".");
            val = aAPrdDesInterest.SUPPRESS_ACCRUAL;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "ACCOUNTING_MODE".Replace("_", ".");
            val = aAPrdDesInterest.ACCOUNTING_MODE;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "INTEREST_METHOD".Replace("_", ".");
            val = aAPrdDesInterest.INTEREST_METHOD;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "RATE_TYPE".Replace("_", ".");
            val = aAPrdDesInterest.RATE_TYPE;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "INTERNAL_BOOKING".Replace("_", ".");
            val = aAPrdDesInterest.INTERNAL_BOOKING;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "CUSTOM_RATE_CALC".Replace("_", ".");
            val = aAPrdDesInterest.CUSTOM_RATE_CALC;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "RUNTIME_RATE_CALC".Replace("_", ".");
            val = aAPrdDesInterest.RUNTIME_RATE_CALC;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "FT_TXN_TYPE".Replace("_", ".");
            val = aAPrdDesInterest.FT_TXN_TYPE;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);

            atrName = "ADJUST_OPTION".Replace("_", ".");
            val = aAPrdDesInterest.ADJUST_OPTION;
            if (!string.IsNullOrEmpty(val))
                inst.AddAttribute(atrName, val);


            atrName = "FIXED_RATE".Replace("_", ".");
            if (null != aAPrdDesInterest.FIXED_RATE)
                foreach (var currMv in aAPrdDesInterest.FIXED_RATE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "FLOATING_INDEX".Replace("_", ".");
            if (null != aAPrdDesInterest.FLOATING_INDEX)
                foreach (var currMv in aAPrdDesInterest.FLOATING_INDEX)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "PERIODIC_INDEX".Replace("_", ".");
            if (null != aAPrdDesInterest.PERIODIC_INDEX)
                foreach (var currMv in aAPrdDesInterest.PERIODIC_INDEX)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "PERIODIC_PERIOD".Replace("_", ".");
            if (null != aAPrdDesInterest.PERIODIC_PERIOD)
                foreach (var currMv in aAPrdDesInterest.PERIODIC_PERIOD)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "PERIODIC_METHOD".Replace("_", ".");
            if (null != aAPrdDesInterest.PERIODIC_METHOD)
                foreach (var currMv in aAPrdDesInterest.PERIODIC_METHOD)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "PERIODIC_RESET".Replace("_", ".");
            if (null != aAPrdDesInterest.PERIODIC_RESET)
                foreach (var currMv in aAPrdDesInterest.PERIODIC_RESET)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "CUSTOM_RATE".Replace("_", ".");
            if (null != aAPrdDesInterest.CUSTOM_RATE)
                foreach (var currMv in aAPrdDesInterest.CUSTOM_RATE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "LINKED_RATE".Replace("_", ".");
            if (null != aAPrdDesInterest.LINKED_RATE)
                foreach (var currMv in aAPrdDesInterest.LINKED_RATE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "MARGIN_TYPE".Replace("_", ".");
            if (null != aAPrdDesInterest.MARGIN_TYPE)
                foreach (var currMv in aAPrdDesInterest.MARGIN_TYPE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "MARGIN_OPER".Replace("_", ".");
            if (null != aAPrdDesInterest.MARGIN_OPER)
                foreach (var currMv in aAPrdDesInterest.MARGIN_OPER)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "MARGIN_RATE".Replace("_", ".");
            if (null != aAPrdDesInterest.MARGIN_RATE)
                foreach (var currMv in aAPrdDesInterest.MARGIN_RATE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "TIER_MIN_RATE".Replace("_", ".");
            if (null != aAPrdDesInterest.TIER_MIN_RATE)
                foreach (var currMv in aAPrdDesInterest.TIER_MIN_RATE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "TIER_MAX_RATE".Replace("_", ".");
            if (null != aAPrdDesInterest.TIER_MAX_RATE)
                foreach (var currMv in aAPrdDesInterest.TIER_MAX_RATE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "TIER_NEGATIVE_RATE".Replace("_", ".");
            if (null != aAPrdDesInterest.TIER_NEGATIVE_RATE)
                foreach (var currMv in aAPrdDesInterest.TIER_NEGATIVE_RATE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "TIER_AMOUNT".Replace("_", ".");
            if (null != aAPrdDesInterest.TIER_AMOUNT)
                foreach (var currMv in aAPrdDesInterest.TIER_AMOUNT)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "TIER_PERCENT".Replace("_", ".");
            if (null != aAPrdDesInterest.TIER_PERCENT)
                foreach (var currMv in aAPrdDesInterest.TIER_PERCENT)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "ON_ACTIVITY".Replace("_", ".");
            if (null != aAPrdDesInterest.ON_ACTIVITY)
                foreach (var currMv in aAPrdDesInterest.ON_ACTIVITY)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "RECALCULATE".Replace("_", ".");
            if (null != aAPrdDesInterest.RECALCULATE)
                foreach (var currMv in aAPrdDesInterest.RECALCULATE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "CUSTOM_TYPE".Replace("_", ".");
            if (null != aAPrdDesInterest.CUSTOM_TYPE)
                foreach (var currMv in aAPrdDesInterest.CUSTOM_TYPE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "CUSTOM_NAME".Replace("_", ".");
            if (null != aAPrdDesInterest.CUSTOM_NAME)
                foreach (var currMv in aAPrdDesInterest.CUSTOM_NAME)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);

            atrName = "CUSTOM_VALUE".Replace("_", ".");
            if (null != aAPrdDesInterest.CUSTOM_VALUE)
                foreach (var currMv in aAPrdDesInterest.CUSTOM_VALUE)
                    inst.AddAttribute($"{atrName}-{currMv.MVIndex}~{currMv.SVIndex}", currMv.Value);


            return inst;
        }

    }
}
