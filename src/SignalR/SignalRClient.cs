﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SignalR
{
    public class SignalRClient : ISignalRClient
    {
        private readonly IHubContext<NotificationsHub> _hubContext;

        public SignalRClient(IHubContext<NotificationsHub> hubContext)
        {
            _hubContext = hubContext;
        }



        public async Task SendAsync(SignalRCommand command)
        {
            await _hubContext.Clients
                             .Group(command.Group)
                             .SendAsync(command.Method, command.Value);
        }
    }
}