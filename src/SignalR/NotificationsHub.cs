﻿using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace SignalR
{
    //[Authorize]
    [HubName("NotificationsHub")]
    public class NotificationsHub : Hub
    {
        public override async Task OnConnectedAsync()
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Identity.Name ?? string.Empty);
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception ex)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, Context.User.Identity.Name ?? string.Empty);
            await base.OnDisconnectedAsync(ex);
        }

        /// <summary>
        /// Generic method for SignalR
        /// </summary>
        /// <param name="message">GroupName, MethodName and Message</param>
        /// <returns></returns>
        public Task DispatchAsync(string[] message)
        {
            return Clients.Group(message[0]).SendAsync(message[1], message[2]);
        }

        public void JoinGroup(string groupName)
        {
            Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }
    }
}