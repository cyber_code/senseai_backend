﻿namespace SignalR
{
    public sealed class SignalRCommand
    {
        public string Group { get; set; }

        public string Method { get; set; }

        public object Value { get; set; }
    }
}