﻿using System.Threading.Tasks;

namespace SignalR
{
    public interface ISignalRClient
    {
        Task SendAsync(SignalRCommand command);

        
    }
}