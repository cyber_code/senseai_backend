﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public interface IJiraUserSubprojectRepository : IRepository<JiraUserSubprojectConfigs>
    {
        void Add(JiraUserSubprojectConfigs jiraUserMapping);

        void Update(JiraUserSubprojectConfigs jiraUserMapping);

        void Delete(Guid id);

        JiraUserSubprojectConfigs GetJiraUserSubprojectConfigs(Guid subProjectId, string UserId);

        JiraUserSubprojectConfigs GetJiraUserMappingById(Guid SubprojectId);
    }
}