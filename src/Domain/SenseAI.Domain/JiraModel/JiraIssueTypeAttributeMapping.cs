﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public sealed class JiraIssueTypeAttributeMapping : BaseEntity
    {
        public JiraIssueTypeAttributeMapping(Guid jiraIssueTypeMappingId, string senseaiField, string jiraField, bool required, int index) : base(Guid.NewGuid())
        {
            JiraIssueTypeMappingId = jiraIssueTypeMappingId;
            SenseaiField = senseaiField;
            JiraField = jiraField;
            Required = required;
            Index = index;
        }

        public JiraIssueTypeAttributeMapping(Guid Id, Guid jiraIssueTypeMappingId, string senseaiField, string jiraField, bool required, int index) : base(Id)
        {
            JiraIssueTypeMappingId = jiraIssueTypeMappingId;
            SenseaiField = senseaiField;
            JiraField = jiraField;
            Required = required;
            Index = index;
        }

        public Guid JiraIssueTypeMappingId { get; private set; }

        public string SenseaiField { get; private set; }

        public string JiraField { get; private set; }

        public bool Required { get; private set; }

        public int Index { get; private set; }
    }
}