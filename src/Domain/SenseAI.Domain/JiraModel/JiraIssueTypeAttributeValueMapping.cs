﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public sealed class JiraIssueTypeAttributeValueMapping : BaseEntity
    {

        public JiraIssueTypeAttributeValueMapping(Guid jiraIssueTypeAttributeMappingId, string senseaiValue, string jiraValue)
        {
            JiraIssueTypeAttributeMappingId = jiraIssueTypeAttributeMappingId;
            SenseaiValue = senseaiValue;
            JiraValue = jiraValue;
        }
        public JiraIssueTypeAttributeValueMapping(Guid id, Guid jiraIssueTypeAttributeMappingId, string senseaiValue, string jiraValue) : base(id)
        {
            JiraIssueTypeAttributeMappingId = jiraIssueTypeAttributeMappingId;
            SenseaiValue = senseaiValue;
            JiraValue = jiraValue;
        }
        public Guid JiraIssueTypeAttributeMappingId { get; set; }

        public string SenseaiValue { get; set; }

        public string JiraValue { get; set; }
    }
}
