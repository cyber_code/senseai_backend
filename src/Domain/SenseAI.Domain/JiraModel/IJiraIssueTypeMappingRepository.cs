﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public interface IJiraIssueTypeMappingRepository : IRepository<JiraIssueTypeMapping>
    {
        void Add(JiraIssueTypeMapping jiraIssueTypeMapping);

        void Update(JiraIssueTypeMapping jiraIssueTypeMapping);

        void Delete(Guid id);
    }
}
