﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public sealed class JiraProjectMapping : BaseEntity
    {
        public JiraProjectMapping(Guid id, Guid projectId, Guid subprojectId, string jiraProjectKey, string jiraProjectName,
            bool mapHierarchy, Guid herarchyParentId, Guid hierarchyTypeId)
            : base(id)
        {
            ProjectId = projectId;
            SubprojectId = subprojectId;
            JiraProjectKey = jiraProjectKey;
            JiraProjectName = jiraProjectName;
            MapHierarchy = mapHierarchy;
            HierarchyParentId = herarchyParentId;
            HierarchyTypeId = hierarchyTypeId;
        }

        public JiraProjectMapping(Guid projectId, Guid subprojectId, string jiraProjectKey, string jiraProjectName,
             bool mapHierarchy, Guid herarchyParentId, Guid hierarchyTypeId)
            : base(Guid.NewGuid())
        {
            ProjectId = projectId;
            SubprojectId = subprojectId;
            JiraProjectKey = jiraProjectKey;
            JiraProjectName = jiraProjectName;
            MapHierarchy = mapHierarchy;
            HierarchyParentId = herarchyParentId;
            HierarchyTypeId = hierarchyTypeId;
        }

        public Guid ProjectId { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraProjectKey { get; set; }

        public string JiraProjectName { get; set; }

        public bool MapHierarchy { get; set; }

        public Guid HierarchyParentId { get; set; }

        public Guid HierarchyTypeId { get; set; }
    }
}