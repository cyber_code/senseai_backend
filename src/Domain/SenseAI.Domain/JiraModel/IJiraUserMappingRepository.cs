﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public interface IJiraUserMappingRepository : IRepository<JiraUserMapping>
    {
        void Add(JiraUserMapping jiraUserMapping);

        void Update(JiraUserMapping jiraUserMapping);

        void Delete(Guid id);

        //List<JiraProjectMapping> GetJiraUserMappingBySubprojectId(Guid SubprojectId);
    }
}