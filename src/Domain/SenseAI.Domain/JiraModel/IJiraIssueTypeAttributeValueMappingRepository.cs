﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
   public interface IJiraIssueTypeAttributeValueMappingRepository : IRepository<JiraIssueTypeAttributeValueMapping>
    {
        void Add(JiraIssueTypeAttributeValueMapping jiraIssueTypeAttributeValueMapping);
        void Update(JiraIssueTypeAttributeValueMapping jiraIssueTypeAttributeValueMapping);
        void Delete(Guid id);


    }
}
