﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public interface IJiraIssueTypeAttributeMappingRepository : IRepository<JiraIssueTypeAttributeMapping>
    {
        void Add(JiraIssueTypeAttributeMapping jiraIssueTypeMapping);

        void Update(JiraIssueTypeAttributeMapping jiraIssueTypeMapping);

        void Delete(Guid id);

    }
}
