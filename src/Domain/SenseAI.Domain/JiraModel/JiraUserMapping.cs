﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public sealed class JiraUserMapping : BaseEntity
    {
        public JiraUserMapping(Guid id, Guid subprojectId, string jiraProjectKey, string jiraProjectName)
            : base(id)
        {
            SubprojectId = subprojectId;
            JiraUserId = jiraProjectKey;
            SenseaiUserId = jiraProjectName;
        }

        public JiraUserMapping(Guid subprojectId, string jiraProjectKey, string jiraProjectName)
            : base(Guid.NewGuid())
        {
            SubprojectId = subprojectId;
            JiraUserId = jiraProjectKey;
            SenseaiUserId = jiraProjectName;
        }

        public Guid SubprojectId { get; set; }

        public string JiraUserId { get; set; }

        public string SenseaiUserId { get; set; }
    }
}