﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public sealed class JiraIssueTypeMapping : BaseEntity
    {
        public JiraIssueTypeMapping(Guid jiraProjectMappingId, string senseaiIssueTypeField, string jiraIssueTypeFieldId, string jiraIssueTypeFieldName, bool allowAttachments) : base(Guid.NewGuid())
        {
            JiraProjectMappingId = jiraProjectMappingId;
            SenseaiIssueTypeField = senseaiIssueTypeField;
            JiraIssueTypeFieldId = jiraIssueTypeFieldId;
            JiraIssueTypeFieldName = jiraIssueTypeFieldName;
            AllowAttachments = allowAttachments;
        }

        public JiraIssueTypeMapping(Guid id, Guid jiraProjectMappingId, string senseaiIssueTypeField, string jiraIssueTypeFieldId, string jiraIssueTypeFieldName, bool allowAttachments) : base(id)
        {
            JiraProjectMappingId = jiraProjectMappingId;
            SenseaiIssueTypeField = senseaiIssueTypeField;
            JiraIssueTypeFieldId = jiraIssueTypeFieldId;
            JiraIssueTypeFieldName = jiraIssueTypeFieldName;
            AllowAttachments = allowAttachments;
        }

        public Guid JiraProjectMappingId { get; private set; }

        public string SenseaiIssueTypeField { get; private set; }

        public string JiraIssueTypeFieldId { get; private set; }

        public string JiraIssueTypeFieldName { get; private set; }

        public bool AllowAttachments { get; private set; }
    }
}