﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public sealed class JiraUserSubprojectConfigs : BaseEntity
    {
        public JiraUserSubprojectConfigs(Guid id, string userId, Guid subprojectId, string jiraUsername, string jiraToken)
            : base(id)
        {
            UserId = userId;
            SubprojectId = subprojectId;
            JiraUsername = jiraUsername;
            JiraToken = jiraToken;
        }

        public JiraUserSubprojectConfigs(string userId, Guid subprojectId, string jiraUsername, string jiraToken)
            : base(Guid.NewGuid())
        {
            UserId = userId;
            SubprojectId = subprojectId;
            JiraUsername = jiraUsername;
            JiraToken = jiraToken;
        }

        public string UserId { get; set; }

        public Guid SubprojectId { get; set; }

        public string JiraUsername { get; set; }

        public string JiraToken { get; set; }
    }
}