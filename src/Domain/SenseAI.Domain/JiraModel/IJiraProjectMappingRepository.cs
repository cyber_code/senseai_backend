﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.JiraModel
{
    public interface IJiraProjectMappingRepository : IRepository<JiraProjectMapping>
    {
        void Add(JiraProjectMapping jiraProjectMapping);

        void Update(JiraProjectMapping jiraProjectMapping);

        void Delete(Guid id);

        JiraProjectMapping GetJiraProjectMappingBySubprojectId(Guid SubprojectId);

        JiraProjectMapping GetJiraProjectMappingById(Guid Id);

        List<JiraProjectMapping> GetJiraProjectMappings();
    }
}