﻿namespace SenseAI.Domain
{
    public enum PasteType
    {
        Copy = 0,
        Cut = 1
    }

    public enum AdapterType
    {
        T24WebAdapter,
    }

    public enum ActionType
    {
        Menu,
        See,
        Delete,
        Authorize,
        Reverse,
        SingOff,
        Tab,
        Input,
        EnquiryAction,
        EnquiryResult,
        Verify,
        Validate
    }

    public enum TypicalType
    {
        Application = 1,
        Version = 2,
        AAAplication = 3,
        Enquiry = 4,
        ProjectResourcing = 5
    }

    public enum MandatoryField
    {
        Not_Mandatory = 0,
        Mandatory = 1,
        All = 2
    }

    public enum MessageType
    {
        Failed = 0,
        Successful = 1,
        Warning = 2,
        Information = 3
    }

    public enum WorkflowStatus
    {
        ReadyToIssue = 1,
        ReadyToGenerate = 2,
        GeneratingInProcess = 3,
        ReadyToExport = 4,
        ExportingInProcess = 5
    }

    public enum ArisWorkflowItemLinkState
    {
        None,
        Yes,
        No
    }

    public enum GenericWorkflowItemLinkState
    {
        None,
        Yes,
        No
    }

    public enum StatusType
    {
        ToDo = 0,
        InProgress = 1,
        Done = 2
    }

    public enum ProcessType
    {
        Generic = 1,
        Aris = 2
    }

   

    public enum InvestigationStatus
    {
        Identified=0,
        Accepted=1,
        Deferred=2,
        Evaluated= 3
    }

    public enum ChangeStatus
    {
        Submitted=0,
        Studied=1,
        Reviewed=2,
        PMOApproved=3,
        SecurityReviewed=4,
        QAReviewed=5,
        InDevelopment=6
    }

    public enum IssuePriority
    {
        Low,
        Medium,
        High,
        Critical
    }

    public enum IssueSeverity
    {
        Low,
        Medium,
        High,
        Critical
    }
    public enum TypeDefect
    {
        Software,
        Cosmetic,
        Performance,
        Database,
        Configuration,
        Environmental,
        Documentation
    }

    public enum IssueLinkType
    {
        DefaultLinkType
    }

    public enum DurationUnit
    {
        Weeks = 0,
        Days = 1
    }
}