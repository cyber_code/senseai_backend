﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.DataModel
{
    public sealed class TypicalChanges : BaseEntity
    {
        public TypicalChanges(Guid id) : base(id)
        { }

        public TypicalChanges(Guid catalogId, string title, string json, string xml, string xmlHash, TypicalType type, int status) : this(Guid.NewGuid(), catalogId, title, json, xml, xmlHash, type,status)
        { }

        public TypicalChanges(Guid id, Guid catalogId, string title, string json, string xml, string xmlHash, TypicalType type, int status) : base(id)
        {
            CatalogId = catalogId;
            Title = title;
            Json = json;
            Xml = xml;
            XmlHash = xmlHash;
            Type = type;
            Status = status;
        }

        public string Title { get; private set; }

        public string Json { get; private set; }

        public string Xml { get; private set; }

        public string XmlHash { get; private set; }

        public Guid CatalogId { get; private set; }

        public TypicalType Type { get; private set; }
        public int Status { get; private set; }
    }
}