﻿using System;
using System.Collections.Generic;

namespace SenseAI.Domain.DataModel
{
    public sealed class TypicalChangesMetadata
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }
        public Guid SubProjectId { get; set; }
        public Guid SystemId { get; set; }
        public Guid CatalogId { get; set; }
        public string Hash { get; set; }
        public Guid Id { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public bool HasAutomaticId { get; set; }
        public bool IsConfiguration { get; set; }

        public TypicalChangesAttributes[] Attributes;
    }

    public sealed class TypicalChangesAttributes
    {
        public TypicalChangesAttributes(string name, bool isMultiValue, string[] possibleValues, bool isRequired, bool isNoChange, bool isExternal, 
            int minimumLength, int maximumLength, string relatedApplicationName, string extraData, bool relatedApplicationIsConfiguration)
        {
            Name = name;
            IsMultiValue = isMultiValue;
            PossibleValues = possibleValues;
            IsRequired = isRequired;
            IsNoChange = isNoChange;
            IsExternal = isExternal;
            MinimumLength = minimumLength;
            MaximumLength = maximumLength;
            RelatedApplicationName = relatedApplicationName;
            ExtraData = extraData;
            RelatedApplicationIsConfiguration = relatedApplicationIsConfiguration;
        }

        public string Name { get; set; }
        public bool IsMultiValue { get; set; }
        public string[] PossibleValues { get; set; }
        public bool IsRequired { get; set; }
        public bool IsNoInput { get; set; }
        public bool IsNoChange { get; set; }
        public bool IsExternal { get; set; }
        public int MinimumLength { get; set; }
        public int MaximumLength { get; set; }
        public string RelatedApplicationName { get; set; }
        public string ExtraData { get; set; }
        public bool RelatedApplicationIsConfiguration { get; set; }

    }
   public class AttributeNameComparer:IEqualityComparer<TypicalChangesAttributes>
    {
        public bool Equals(TypicalChangesAttributes x, TypicalChangesAttributes y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.Name == y.Name;
        }
        public int GetHashCode(TypicalChangesAttributes product)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(product, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashProductName = product.Name == null ? 0 : product.Name.GetHashCode();

            //Get hash code for the Code field.
            //   int hashProductCode = product.Code.GetHashCode();

            //Calculate the hash code for the product.
            return hashProductName;// ^ hashProductCode;
        }
    }
  public  class AttributeComparer : IEqualityComparer<TypicalChangesAttributes>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(TypicalChangesAttributes x, TypicalChangesAttributes y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;
            //Check whether the products' properties are equal.
            return x.Name == y.Name && x.ExtraData == y.ExtraData && x.IsExternal == y.IsExternal && x.IsMultiValue == y.IsMultiValue && x.IsNoChange == y.IsNoChange && x.IsNoInput == y.IsNoInput && x.IsRequired == y.IsRequired && x.MaximumLength == y.MaximumLength && x.MinimumLength == y.MinimumLength && x.RelatedApplicationName == y.RelatedApplicationName && x.RelatedApplicationIsConfiguration==y.RelatedApplicationIsConfiguration;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(TypicalChangesAttributes product)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(product, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashProductName = product.Name == null ? 0 : product.Name.GetHashCode();

            //Get hash code for the Code field.
            //   int hashProductCode = product.Code.GetHashCode();

            //Calculate the hash code for the product.
            return hashProductName;// ^ hashProductCode;
        }

    }
}