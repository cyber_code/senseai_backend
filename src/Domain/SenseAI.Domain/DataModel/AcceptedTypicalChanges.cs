﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.DataModel
{
    public sealed class AcceptedTypicalChanges : BaseEntity
    {
        public AcceptedTypicalChanges(Guid id) : base(id)
        { }

        public AcceptedTypicalChanges(Guid typicalId, string typicalName, string oldxml, string newxml, string oldxmlHash, string newxmlHash, int versionId, string oldJson, string newJson) : this(Guid.NewGuid(),  typicalId,  typicalName,  oldxml,  newxml,  oldxmlHash,  newxmlHash,  versionId,oldJson,newJson)
        { }

        public AcceptedTypicalChanges(Guid id, Guid typicalId, string typicalName, string oldxml, string newxml, string oldxmlHash, string newxmlHash, int versionId, string oldJson, string newJson) : base(id)
        {
            TypicalId = typicalId;
            TypicalName = typicalName;
            OldXml = oldxml;
            NewXml = newxml;
            OldXmlHash = oldxmlHash;
            NewXmlHash = newxmlHash;
            VersionId = versionId;
            OldJson = oldJson;
            NewJson = newJson;

        }



        public string OldXml { get; private set; }
        public string NewXml { get; private set; }
        public string OldJson { get; private set; }
        public string NewJson { get; private set; }

        public string OldXmlHash { get; private set; }
        public string NewXmlHash { get; private set; }

        public Guid TypicalId { get; private set; }
        public string TypicalName { get; private set; }

        
        public int VersionId { get; private set; }
    }
}