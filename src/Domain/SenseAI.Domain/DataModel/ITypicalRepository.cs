﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.DataModel
{
    public interface ITypicalRepository : IRepository<Typical>
    {
        void Add(Typical typical);

        void Update(Typical typical);

        void Delete(Guid id);

        Typical GetTypical(Guid id);

        Typical GetTypical(Guid catalogId, string name, TypicalType typicalType);

        bool CheckIfTypicalExist(Guid catalogId, string typicalName);

        void AddEnquiryActions(Guid catalogId, string typicalName, string action );
    }
}