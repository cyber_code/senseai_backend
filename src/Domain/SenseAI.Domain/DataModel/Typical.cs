﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.DataModel
{
    public sealed class Typical : BaseEntity
    {
        public Typical(Guid id) : base(id)
        { }

        public Typical(Guid catalogId, string title, string json, string xml, string xmlHash, TypicalType type) : this(Guid.NewGuid(), catalogId, title, json, xml, xmlHash, type)
        { }

        public Typical(Guid id, Guid catalogId, string title, string json, string xml, string xmlHash, TypicalType type) : base(id)
        {
            CatalogId = catalogId;
            Title = title;
            Json = json;
            Xml = xml;
            XmlHash = xmlHash;
            Type = type;
        }

        public Typical(Guid id,  string title) : base(id)
        {
            Title = title;
        }

        public string Title { get; private set; }

        public string Json { get; private set; }

        public string Xml { get; private set; }

        public string XmlHash { get; private set; }

        public Guid CatalogId { get; private set; }

        public TypicalType Type { get; private set; }

        public void SetTitle(string title)
        {
            Title = title;
        }
    }
}