﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.DataModel
{
    public interface IAcceptedTypicalChangesRepository : IRepository<AcceptedTypicalChanges>
    {
        void Add(AcceptedTypicalChanges acceptedTypicalChanges);

        void Update(AcceptedTypicalChanges acceptedTypicalChanges);

        void Delete(Guid id);

        AcceptedTypicalChanges GetAcceptedTypicalChanges(Guid id);
    }
}