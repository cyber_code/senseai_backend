﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.DataModel
{
    public class ProductionData : BaseEntity
    {
        public ProductionData(Guid id) : base(id)
        { }

        public ProductionData(Guid typicalId, string json) : this(Guid.NewGuid(), typicalId, json)
        { }

        public ProductionData(Guid id, Guid typicalId, string json) : base(id)
        {
            TypicalId = typicalId;
            Json = json;
        }

        public Guid TypicalId { get; private set; }

        public string Json { get; private set; }
    }
}