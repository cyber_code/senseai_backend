﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.DataModel
{
    public interface ITypicalChangesRepository : IRepository<TypicalChanges>
    {
        void Add(TypicalChanges typicalChanges);

        void Update(TypicalChanges typicalChanges);

        void Delete(Guid id);

        TypicalChanges GetTypicalChanges(Guid id);
    }
}