﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.PeopleModel
{
    public interface IRoleRepository : IRepository<Role>
    {
        void Add(Role role);

        void Update(Role role);

        void Delete(Guid id);

    }
}
