﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.PeopleModel
{
    public interface IPeopleRepository : IRepository<People>
    {
        void Add(People people);

        void Update(People people);

        void Delete(Guid id);

    }
}
