﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.PeopleModel
{
    public sealed class People : BaseEntity
    {
        public People(Guid id) :
            base(id)
        { }

        public People(Guid roleId, string name, string surname, string email, string address, int userIdFromAdminPanel, Guid subProjectId, string color)
            : this(Guid.NewGuid(), roleId, name, surname, email, address, userIdFromAdminPanel, subProjectId, color)
        { }

        public People(Guid id, Guid roleId, string name, string surname, string email, string address, int userIdFromAdminPanel, Guid subProjectId, string color)
          : base(id)
        {
            RoleId = roleId;
            Name = name;
            Surname = surname;
            Email = email;
            Address = address;
            UserIdFromAdminPanel = userIdFromAdminPanel;
            SubProjectId = subProjectId;
            Color = color;
        }

        public People(Guid id, string name, string surname)
           : base(id)
        {
            Name = name;
            Surname = surname;
        }

        public Guid RoleId { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public string Email { get; private set; }
        public string Address { get; private set; }
        public int UserIdFromAdminPanel { get; private set; }
        public Guid SubProjectId { get; private set; }
        public string Color { get; private set; }

    }
}