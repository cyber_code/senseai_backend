﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.PeopleModel
{
    public sealed class Role : BaseEntity
    {
        public Role(Guid id) :
            base(id)
        { }

        public Role(string title, string description, Guid subProjectId)
            : this(Guid.NewGuid(), title, description, subProjectId)
        { }

        public Role(Guid id, string title, string description, Guid subProjectId)
          : base(id)
        {
            Title = title;
            Description = description;
            SubProjectId = subProjectId;
        }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid SubProjectId { get; private set; }
    }
}