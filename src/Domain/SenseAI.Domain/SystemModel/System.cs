﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.SystemModel
{
    public sealed class System : BaseEntity
    {
        public System(string title, string adapterName)
        {
            Title = title;
            AdapterName = adapterName;
        }

        public System(Guid id, string title, string adapterName) : base(id)
        {
            Title = title;
            AdapterName = adapterName;
        }

        public string Title { get; private set; }

        public string AdapterName { get; private set; }
    }
}