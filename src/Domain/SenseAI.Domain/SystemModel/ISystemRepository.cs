﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.SystemModel
{
    public interface ISystemRepository : IRepository<System>
    {
        void Add(System system);

        void Update(System system);

        void Delete(Guid id);

        List<System> GetSystems();
    }
}