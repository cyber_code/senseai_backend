﻿using System;
using SenseAI.Domain.BaseModel;

namespace SenseAI.Domain.SystemTags
{
    public interface ISystemTagRepository : IRepository<SystemTag>
    {
        void Add(SystemTag systemTag);

        void Update(SystemTag systemTag);

        void Delete(Guid id);
    }
}