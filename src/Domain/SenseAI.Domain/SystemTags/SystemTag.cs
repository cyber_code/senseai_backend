﻿using System;
using SenseAI.Domain.BaseModel;

namespace SenseAI.Domain.SystemTags
{
    public sealed class SystemTag : BaseEntity
    {
        public Guid SystemId { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }

        public SystemTag(Guid id, Guid systemId, string title, string description) : base(id)
        {
            SystemId = systemId;
            Title = title;
            Description = description;
        }

        public SystemTag(Guid systemId, string title, string description)
        {
            SystemId = systemId;
            Title = title;
            Description = description;
        }
    }
}