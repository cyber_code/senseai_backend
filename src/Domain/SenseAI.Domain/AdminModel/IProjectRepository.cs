﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.AdminModel
{
    public interface IProjectRepository : IRepository<Project>
    {
        void Add(Project project);

        void Update(Project project);

        void Delete(Guid id);
    }
}