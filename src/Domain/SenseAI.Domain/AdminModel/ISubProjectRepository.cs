﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.AdminModel
{
    public interface ISubProjectRepository : IRepository<SubProject>
    {
        void Add(SubProject subProject);

        void Update(SubProject subProject);

        void Delete(Guid id);

        SubProject GetSubProjectById(Guid subProjectId);
    }
}