﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.AdminModel
{
    public interface IFolderRepository : IRepository<Folder>
    {
        void Add(Folder folder);

        void Update(Folder folder);

        void Delete(Guid id);

        void Paste(Guid folderId);

        Guid ArisFolder(Folder folder);
    }
}