﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.AdminModel
{
    public sealed class Settings : BaseEntity
    {
        public Settings(Guid projectId, Guid subProjectId, Guid systemId, Guid catalogId)
        {
            ProjectId = projectId;
            SubProjectId = subProjectId;
            SystemId = systemId;
            CatalogId = catalogId;
        }

        public Settings(Guid id, Guid projectId, Guid subProjectId, Guid systemId, Guid catalogId) : base(id)
        {
            ProjectId = projectId;
            SubProjectId = subProjectId;
            SystemId = systemId;
            CatalogId = catalogId;
        }

        public Guid ProjectId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public Guid SystemId { get; private set; }

        public Guid CatalogId { get; private set; }
    }
}