﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.AdminModel
{
    public sealed class Project : BaseEntity
    {
        public Project(Guid id) :
            base(id)
        { }

        public Project(string title, string description)
            : this(Guid.NewGuid(), title, description)
        { }

        public Project(Guid id, string title, string description)
          : base(id)
        {
            Title = title;
            Description = description;
        }

        public string Title { get; private set; }

        public string Description { get; private set; }
    }
}