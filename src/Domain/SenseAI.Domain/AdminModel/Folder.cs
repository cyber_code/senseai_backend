﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.AdminModel
{
    public sealed class Folder : BaseEntity
    {
        public Folder(Guid id)
            : base(id)
        {
        }

        public Folder(string title, string description, Guid subProjectId) : base()
        {
            Title = title;
            Description = description;
            SubProjectId = subProjectId;
        }

        public Folder(Guid id, string title, string description) : base(id)
        {
            Title = title;
            Description = description;
        }

        public Folder(Guid id, Guid subProjectId, string title, string description)
            : base(id)
        {
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
        }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }
    }
}