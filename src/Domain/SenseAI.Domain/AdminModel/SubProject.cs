﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.AdminModel
{
    public sealed class SubProject : BaseEntity
    {
        public SubProject(Guid id) :
            base(id)
        { }

        public SubProject(Guid projectId, string title, string description, bool isJiraIntegrationEnabled, string jiraLink)
            : this(Guid.NewGuid(), projectId, title, description, isJiraIntegrationEnabled, jiraLink)
        { }

        //public SubProject(Guid id, Guid projectId, string title, string description)
        //  : base(id)
        //{
        //    ProjectId = projectId;
        //    Title = title;
        //    Description = description;
        //}

        public SubProject(Guid id, Guid projectId, string title, string description, bool isJiraIntegrationEnabled, string jiraLink)
          : base(id)
        {
            ProjectId = projectId;
            Title = title;
            Description = description;
            IsJiraIntegrationEnabled = isJiraIntegrationEnabled;
            JiraLink = jiraLink;
        }

        public Guid ProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public bool IsJiraIntegrationEnabled { get; set; }

        public string JiraLink { get; set; }
    }
}