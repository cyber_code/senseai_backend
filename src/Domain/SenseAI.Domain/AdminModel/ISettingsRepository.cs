﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.AdminModel
{
    public interface ISettingsRepository : IRepository<Settings>
    {
        void Add(Settings settings);

        void Update(Settings settings);

        void Delete(Guid id);
    }
}