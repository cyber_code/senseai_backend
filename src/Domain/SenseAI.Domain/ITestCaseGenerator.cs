﻿using SenseAI.Domain.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using System.Collections.Generic;

namespace SenseAI.Domain
{
    public interface ITestCaseGenerator
    {
        List<TestCase> Generate(WorkflowVersionPaths workflowPath);
    }
}