﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.AAProductionBuilderModel
{
    public class ProductGroupPropertyFieldState : BaseEntity
    {
        public int Id { get; set; }

        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }

        public string PropertyId { get; set; }

        public string FieldId { get; set; }

        public int MultiValueIndex { get; set; }

        public int SubValueIndex { get; set; }

        public string Value { get; set; }

        public string CurrencyId { get; set; }
    }
}