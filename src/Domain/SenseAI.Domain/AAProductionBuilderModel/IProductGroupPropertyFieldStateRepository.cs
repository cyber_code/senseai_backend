﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.AAProductionBuilderModel
{
    public interface IProductGroupPropertyFieldStateRepository : IRepository<ProductGroupPropertyFieldState>
    {
        void Add(ProductGroupPropertyFieldState productGroupPropertyFieldState);

        void AddBulk(IList<ProductGroupPropertyFieldState> productGroupPropertyFieldStates);

        void Update(ProductGroupPropertyFieldState productGroupPropertyFieldState);

        void Delete(Guid sessionId);

        void Delete(Guid sessionId, Guid productGroupId, string propertyId, string currencyId);

        List<ProductGroupPropertyFieldState> Get(Guid sessionId, Guid productGroupId);
    }
}