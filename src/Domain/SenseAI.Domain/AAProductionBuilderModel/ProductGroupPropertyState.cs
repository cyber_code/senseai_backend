﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.AAProductionBuilderModel
{
    public class ProductGroupPropertyState : BaseEntity
    {
        public Guid SessionId { get; set; }

        public Guid ProductGroupId { get; set; }

        public string PropertyId { get; set; }

        public string CurrencyId { get; set; }

        public bool IsSelected { get; set; }
    }
}
