﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.AAProductionBuilderModel
{
    public interface IProductGroupPropertyStateRepository : IRepository<ProductGroupPropertyState>
    {
        void AddBulk(IList<ProductGroupPropertyState> productGroupPropertyStates);

        void Update(Guid sessionId, Guid productId, string propertyId, string currencyId);

        void UpdateAllProductGroupStatus(Guid sessionId, Guid productId);

        void Delete(Guid sessionId);
    }
}