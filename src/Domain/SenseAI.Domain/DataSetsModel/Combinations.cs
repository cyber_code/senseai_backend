﻿using System.Collections.Generic;

namespace SenseAI.Domain.DataSetsModel
{
    public sealed class Combinations
    {
        public string Attribute { get; set; }

        public List<string> Values { get; set; }
    }
}