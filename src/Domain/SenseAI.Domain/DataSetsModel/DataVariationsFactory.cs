﻿using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SenseAI.Domain.DataSetsModel
{
    /// <summary>
    ///     Generator for data variations
    /// </summary>
    public static class DataVariationsFactory
    {
        /// <summary>
        ///      Multiples the instance template by combining in cartesian product the equivalence sets
        /// </summary>
        /// <param name="instanceTemplate">The instance template</param>
        /// <param name="equivalenceSets">The equivalence sets</param>
        /// <returns></returns>
        public static List<TypicalInstance> VariationsFactory(TypicalInstance instanceTemplate, Dictionary<string, List<string>> equivalenceSets)
        {
            var result = new List<TypicalInstance>();
            var equivalencesList = new List<List<KeyValuePair<string, string>>>();

            foreach (var equivalenceKey in equivalenceSets.Keys)
            {
                var subSet = new List<KeyValuePair<string, string>>();

                equivalenceSets[equivalenceKey].ForEach(item => subSet.Add(new KeyValuePair<string, string>(equivalenceKey, item)));
                equivalencesList.Add(subSet);
            }
            if (equivalencesList.Any())
            {
                var cartesianProduct = Cartesiant(equivalencesList);

                foreach (var subSet in cartesianProduct)
                {
                    var dataRowInstance = (TypicalInstance)instanceTemplate.Clone();

                    foreach (KeyValuePair<string, string> item in (IEnumerable)subSet)
                    {
                        var dataRowInstanceAttribute = dataRowInstance.Attributes.First(attribute => attribute.Name.Equals(item.Key));

                        if (dataRowInstanceAttribute != null)
                        {
                            dataRowInstanceAttribute.Value = item.Value;
                        }
                        else
                        {
                            throw new AggregateException($"Can't find the equivalence attribute {item.Key} in the typical attributes list!");
                        }
                    }

                    result.Add(dataRowInstance);
                }
            }

            return result;
        }

        public static string[] GenerateCombinations(string[] Array1, int[] Array2)
        {
            if (Array1 == null) throw new ArgumentNullException("Array1");
            if (Array2 == null) throw new ArgumentNullException("Array2");
            if (Array1.Length != Array2.Length)
                throw new ArgumentException("Must be the same size as Array1.", "Array2");

            if (Array1.Length == 0)
                return new string[0];

            int outputSize = 1;
            var current = new int[Array1.Length];
            for (int i = 0; i < current.Length; ++i)
            {
                if (Array2[i] < 1)
                    throw new ArgumentException("Contains invalid values.", "Array2");
                if (Array1[i] == null)
                    throw new ArgumentException("Contains null values.", "Array1");
                outputSize *= Array2[i];
                current[i] = 1;
            }

            var result = new string[outputSize];
            for (int i = 0; i < outputSize; ++i)
            {
                var sb = new StringBuilder();
                for (int j = 0; j < current.Length; ++j)
                {
                    sb.Append(Array1[j]);
                    sb.Append(current[j].ToString());
                    if (j != current.Length - 1)
                        sb.Append(' ');
                }
                result[i] = sb.ToString();
                int incrementIndex = current.Length - 1;
                while (incrementIndex >= 0 && current[incrementIndex] == Array2[incrementIndex])
                {
                    current[incrementIndex] = 1;
                    --incrementIndex;
                }
                if (incrementIndex >= 0)
                    ++current[incrementIndex];
            }
            return result;
        }

        /// <summary>
        ///     Produces the cartesian product for the given sets
        /// </summary>
        /// <param name="sets">The list of eqiovalences</param>
        /// <returns>The cartesian product for the sets</returns>
        private static IEnumerable Cartesiant(this IEnumerable<IEnumerable> sets)
        {
            var slots = sets
                .Select(x => x.GetEnumerator())
                .Where(x => x.MoveNext())
                .ToArray();

            while (true)
            {
                yield return slots.Select(x => x.Current);

                foreach (var slot in slots)
                {
                    // reset the slot if it couldn't move next
                    if (!slot.MoveNext())
                    {
                        // stop when the last enumerator resets
                        if (Equals(slot, slots.Last())) { yield break; }
                        slot.Reset();
                        slot.MoveNext();

                        continue;
                    }
                    // we could increase the current enumerator without reset so stop here
                    break;
                }
            }
        }
    }
}