﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.DataSetsModel
{
    public interface IDataSetRepository : IRepository<DataSet>
    {
        void Add(DataSet dataSet);

        void Update(DataSet dataSet);

        void UpdateCombinations(Guid id,string combinations);

        void Delete(Guid id);

        void UpdateItem(DataSetItem dataSetItem);

        void DeleteItem(Guid id);

        DataSet Get(Guid dataSetId);

        DataSet GetDataSet(Guid dataSetId);

        int GetNumberOfSameDatasets(Guid parentId);
    }
}