﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.DataSetsModel
{
    public class DataSetItem : BaseEntity
    {
        public DataSetItem(Guid dataSetId, string row)
          : base(Guid.NewGuid())
        {
            DataSetId = dataSetId;
            Row = row;
        }

        public DataSetItem(Guid id, Guid dataSetId, string row)
         : base(id)
        {
            DataSetId = dataSetId;
            Row = row;
        }

        public Guid DataSetId { get; private set; }

        public string Row { get; private set; }
    }
}