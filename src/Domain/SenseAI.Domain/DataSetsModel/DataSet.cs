﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace SenseAI.Domain.DataSetsModel
{
    public class DataSet : BaseEntity
    {
        private IList<DataSetItem> _dataSetItems;

        public DataSet(Guid subProjectId, Guid catalogId, Guid systemId, Guid typicalId, string title, decimal coverage, string combinations, DataSetItem[] dataSetItems)
          : base(Guid.NewGuid())
        {
            SubProjectId = subProjectId;
            CatalogId = catalogId;
            SystemId = systemId;
            TypicalId = typicalId;
            Title = title;
            Coverage = coverage;
            Combinations = combinations;
            _dataSetItems = dataSetItems;
        }

        public DataSet(Guid subProjectId, Guid catalogId, Guid systemId, Guid typicalId, string title, decimal coverage, string combinations, DataSetItem[] dataSetItems, Guid parentId)
            : base(Guid.NewGuid())
        {
            SubProjectId = subProjectId;
            CatalogId = catalogId;
            SystemId = systemId;
            TypicalId = typicalId;
            Title = title;
            Coverage = coverage;
            Combinations = combinations;
            _dataSetItems = dataSetItems;
            ParentId = parentId;
        }

        public DataSet(Guid id, Guid subProjectId, Guid catalogId, Guid systemId, Guid typicalId, string title, decimal coverage, string combinations, DataSetItem[] dataSetItems)
          : base(id)
        {
            SubProjectId = subProjectId;
            CatalogId = catalogId;
            SystemId = systemId;
            TypicalId = typicalId;
            Title = title;
            Coverage = coverage;
            Combinations = combinations;
            _dataSetItems = dataSetItems;
        }

        public DataSet(Guid id, Guid subProjectId, Guid catalogId, Guid systemId, Guid typicalId, string title, decimal coverage, string combinations, DataSetItem[] dataSetItems, Guid parentId)
        : base(id)
        {
            SubProjectId = subProjectId;
            CatalogId = catalogId;
            SystemId = systemId;
            TypicalId = typicalId;
            Title = title;
            Coverage = coverage;
            Combinations = combinations;
            _dataSetItems = dataSetItems;
            ParentId = parentId;
        }


        public DataSet(Guid id, string title, string combinations, decimal coverage, DataSetItem[] dataSetItems)
        : base(id)
        {
            Title = title;
            Coverage = coverage;
            Combinations = combinations;
            _dataSetItems = dataSetItems;
        }

        public Guid SubProjectId { get; private set; }

        public Guid CatalogId { get; private set; }

        public Guid SystemId { get; private set; }

        public Guid TypicalId { get; private set; }

        public string Title { get; private set; }

        public decimal Coverage { get; private set; }

        public string Combinations { get; private set; }

        public Guid ParentId { get; private set; }

        public IReadOnlyCollection<DataSetItem> DataSetItems
        {
            get
            {
                return _dataSetItems.ToImmutableArray();
            }
        }
    }
}