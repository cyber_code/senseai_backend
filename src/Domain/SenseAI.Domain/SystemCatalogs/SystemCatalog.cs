﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.SystemCatalogs
{
    public sealed class SystemCatalog : BaseEntity
    {
        public SystemCatalog(Guid subprojectId, Guid systemId, Guid catalogId)
        {
            SubprojectId = subprojectId;
            SystemId = systemId;
            CatalogId = catalogId;
        }
        public SystemCatalog(Guid id, Guid subprojectId, Guid systemId, Guid catalogId) :base(id)
        {
            SubprojectId = subprojectId;
            SystemId = systemId;
            CatalogId = catalogId;
        }
        //Id,SubprojectId, SystemId, CatalogId.
        public Guid SubprojectId { get; private set; }
        public Guid SystemId { get; private set; }
        public Guid CatalogId { get; private set; }


    }
}
