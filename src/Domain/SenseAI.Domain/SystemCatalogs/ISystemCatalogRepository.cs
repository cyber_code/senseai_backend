﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.SystemCatalogs
{
   public interface ISystemCatalogRepository : IRepository<SystemCatalog>
    {
        void Add(SystemCatalog systemCatalog);

        void Update(SystemCatalog systemCatalog);

        void Delete(Guid id);

        List<SystemCatalog> GetSystemCatalogs();
    }
}
