﻿namespace SenseAI.Domain.WorkflowModel
{
    public sealed class Attribute
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}