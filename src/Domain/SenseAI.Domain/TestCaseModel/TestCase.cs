﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class TestCase : BaseEntity
    {
        private readonly IList<TestStep> _testSteps;

        public TestCase(string title, string description) : base(Guid.NewGuid())
        {
            Title = title;
            Description = description;
            _testSteps = new List<TestStep>();
        }

        public TestCase(Guid id, string title, string description) : base(id)
        {
            Title = title;
            Description = description;
            _testSteps = new List<TestStep>();
        }
        
        public string Description { get; private set; }

        public string Title { get; private set; }

        public IReadOnlyCollection<TestStep> TestSteps
        {
            get
            {
                return _testSteps.ToImmutableArray();
            }
        }

   
        public void AddTestSteps(List<TestStep> testSteps)
        {
            foreach (TestStep ts in testSteps)
            {
                /*This extra mapping of test step is Id of test step*/
                TestStep testStep = new TestStep(ts.Id, ts.Title, ts.Description, ts.AdapterName, ts.SystemName, ts.CatalogName, ts.TypicalId, ts.TypicalName, ts.Parameters, ts.IsNegativeStep, ts.ExpectedError, ts.Type, ts.Action, ts.WorkflowItemId, ts.LinkWorkflowId, ts.CatalogId);

                foreach (var pc in ts.Filters)
                    testStep.AddFilter(pc.AttributeName, pc.OperatorName, pc.AttributeValue, pc.DynamicData);

                foreach (var c in ts.PostFilters)
                    testStep.AddPostFilter(c.AttributeName, c.OperatorName, c.AttributeValue, c.DynamicData);

                foreach (var ac in ts.DynamicDatas)
                    testStep.AddDynamicData(ac.TestStepId, ac.SourceAttributeName, ac.TargetAttributeName, ac.PlainTextFormula, ac.UiMode);

                testStep.TypicalInstance = ts.TypicalInstance;
                testStep.DataSetId = ts.DataSetId;
                _testSteps.Add(testStep);
            }
        }
    }
}