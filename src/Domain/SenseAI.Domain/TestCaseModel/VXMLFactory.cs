﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace SenseAI.Domain.WorkflowModel
{
    public class VXMLFactory
    {
        private XmlDocument _vXmlDoc;
        private XmlElement _navigatorCatalogNode;
        public const string attrRECORD_STATUS = "RECORD.STATUS";
        public const string attrERROR_MESSAGE = "Error Message";

        private Func<string, int> FnGetTypicalType { get; set; }
        private VXMLFactory()
        {
            _vXmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = _vXmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);

            XmlElement root = _vXmlDoc.DocumentElement;
            _vXmlDoc.InsertBefore(xmlDeclaration, root);

            _navigatorCatalogNode = _vXmlDoc.CreateElement(string.Empty, "NavigatorCatalog", string.Empty);
            _vXmlDoc.AppendChild(_navigatorCatalogNode);
        }

        public static byte[] TransformToVXml(Guid subProjectId,
                                             string subProjectTile,
                                             Guid filderId,
                                             string folderTitle,
                                             string folderDescription,
                                             Guid externalSystemId,
                                             string requirement,
                                             WorkflowTestCases[] generatedTestCases,
                                             Func<Guid, List<TypicalInstance>> fnGetDataPool,
                                             Func<string, int> fnGetTypicalType,
                                             out long[] testCaseIds)
        {
            var factory = new VXMLFactory();
            factory.FnGetTypicalType = fnGetTypicalType;

            var numericSubProjectId = factory.AppendSubProject(subProjectId, subProjectTile);
            var numericTestFolderId = factory.AppendTestFolder(numericSubProjectId, filderId, folderTitle, folderDescription);


            List<long> _testCaseIds = new List<long>();
            generatedTestCases.GroupBy(i => i.TestCaseId)
                .OrderBy(testCase => testCase.First().TCIndex)
                .ToList()
                .ForEach(testCase =>
                 {

                     var numericTestCaseId = factory.AppendTestCase(externalSystemId, testCase.FirstOrDefault().TestCaseId, testCase.FirstOrDefault()?.TestCaseTitle, numericTestFolderId);

                     long testCaseId;
                     if (long.TryParse(numericTestCaseId, out testCaseId))
                         _testCaseIds.Add(testCaseId);

                     var numericDataPoolId = factory.AppendDataPool(Guid.NewGuid(), "Data pool " + testCase.FirstOrDefault().TestCaseTitle.Split("TC").Last());
                     factory.AppendTestCaseDataPoolCollectionAssocation(Guid.NewGuid(), numericDataPoolId, numericTestCaseId);
                     Dictionary<int, TestStepJson> _generatedTestSteps = new Dictionary<int, TestStepJson>();
                     var count = 1;
                     testCase.OrderBy(step => step.TSIndex).ToList()
                    .ForEach(testStep =>
                    {
                        var json = JObject.Parse(testStep.TestStepJson);
                        var generatedTestStepJson = JsonConvert.DeserializeObject<TestStepJson>(json.ToString());

                        var instances = fnGetDataPool(generatedTestStepJson.DataSetId);
                        count *= instances.Count;


                    });

                     testCase.OrderBy(step => step.TSIndex).ToList()
                     .ForEach(testStep =>
                     {
                         var json = JObject.Parse(testStep.TestStepJson);
                         var generatedTestStepJson = JsonConvert.DeserializeObject<TestStepJson>(json.ToString());
                         _generatedTestSteps.Add(testStep.TSIndex, generatedTestStepJson);
                         var numericTestStepId = factory.AppendTestStep(numericTestCaseId, testStep.TSIndex, testStep.TestStepId, generatedTestStepJson);
                         var numericSystemId = factory.AppendSystem(Guid.NewGuid(), string.IsNullOrEmpty(generatedTestStepJson.SystemName) ? "SenseAI Input System" : generatedTestStepJson.SystemName);
                         var numericActionParamId = factory.AppendActionParams(Guid.NewGuid(), generatedTestStepJson);
                         if (generatedTestStepJson.IsNegativeStep)
                         {
                             var appErrorId = factory.AppendAppError(Guid.NewGuid(), generatedTestStepJson);
                             factory.AppendTestStepToAppErrorAssocation(Guid.NewGuid(), numericTestStepId, appErrorId);
                         }
                         factory.AppendDefaultAssocation(Guid.NewGuid(), numericTestStepId, numericSystemId);
                         factory.AppendDefaultAssocation(Guid.NewGuid(), numericTestStepId, numericActionParamId);
                         factory.AppendAutomaticCalculation(Guid.NewGuid(), generatedTestStepJson, generatedTestStepJson.DynamicDatas, _generatedTestSteps, numericTestStepId, numericTestCaseId, false);
                         factory.AppendConstains(Guid.NewGuid(), generatedTestStepJson, _generatedTestSteps, numericTestStepId, numericTestCaseId);

                         if (generatedTestStepJson.DataSetId == null || generatedTestStepJson.DataSetId == Guid.Empty)
                         {
                             var idtestdata = factory.AppendSimpleTestData(generatedTestStepJson);
                             factory.AppendDefaultAssocation(Guid.NewGuid(), numericTestStepId, idtestdata);
                         }


                         var instances = fnGetDataPool(generatedTestStepJson.DataSetId);
                         var noOfCycles = 1;
                         var numericInstancesIdList = new List<string>();
                         if (instances.Count > 0)
                         {
                             noOfCycles = count / instances.Count;
                         }

                         instances.ForEach(instance =>
                         {
                             for (int i = 0; i < noOfCycles; i++)
                                 numericInstancesIdList.Add(factory.AppendTestData(generatedTestStepJson, instance));
                         });

                         var numericDataPoolItemId = factory.AppendDataPoolItem(Guid.NewGuid(), testStep.TSIndex, generatedTestStepJson, numericInstancesIdList);

                         factory.AppendDataPoolItemsCollectionAssocation(Guid.NewGuid(), numericDataPoolItemId, numericDataPoolId);
                         foreach (var instanceId in numericInstancesIdList)
                         { factory.AppendFOToDataPoolItemAssocation(Guid.NewGuid(), numericDataPoolItemId, instanceId); }

                     });
                     for (var j = 1; j <= count; j++)
                     {
                         var numericDataRowId = factory.AppendDataPoolRow(Guid.NewGuid(), $"Data Row - {j}");
                         factory.AppendDataPoolRowToDataPoolAssociation(Guid.NewGuid(), numericDataRowId, numericDataPoolId);
                     }
                 });

            testCaseIds = _testCaseIds.ToArray();
            return factory.ToByteArray();
        }

        #region [ Append Entities ]

        private string AppendSubProject(Guid id, string title)
        {
            var subProjectId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Subproject", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", subProjectId},
                {"Name", title}
            });
            _navigatorCatalogNode.AppendChild(node);

            return subProjectId;
        }

        private string AppendTestFolder(string subProjectId, Guid id, string title, string description)
        {
            string testFilderId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "TestFolder", string.Empty);
            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", testFilderId},
                {"Name", $"{title} {DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt")}"},
                {"Description", description},
                {"ProjectID", subProjectId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return testFilderId;
        }

        private string AppendSystem(Guid id, string title)
        {
            var systemId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Systems", string.Empty);
            node.SetAttribute("Catalog", "NavigatorCatalog");
            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", systemId},
                {"Name",title}
            });
            _navigatorCatalogNode.AppendChild(node);

            return systemId;
        }

        private string AppendTestCase(Guid externalSystemId, Guid id, string title, string testFolderId)
        {
            var testCaseId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "TestCase", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", testCaseId},
                {"Name", title},
                {"Description",title},
                {"Type", "2" },
                {"Priority", "2"},
                {"Status", "0"},
                {"CreateDate", DateTime.Now.ToString()},
                {"BusinessFunction", "No Value"},
                {"CaseIndex", "0"},
                {"TestFolderID", testFolderId},
                {"ExternalSystemId", externalSystemId.ToString()},
                {"Origination", "SenseAI"},
            });

            _navigatorCatalogNode.AppendChild(node);

            return testCaseId;
        }

        private string AppendTestStep(string testCaseId, int stepIndex, Guid id, TestStepJson generatedTestStep)
        {
            var testStepId = ExtractNumericId(id);
            testStepId = (decimal.Parse(testStepId) * decimal.Parse(testCaseId)).ToString().Substring(0, 6);
            int typicalType = FnGetTypicalType(generatedTestStep.CatalogName);

            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "TestStep", string.Empty);
            node.SetAttribute("CheckStatus", "0");

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>
            {
                {"ID", testStepId},
                {"Name", generatedTestStep.Title},
                {"Label", GetTestStepLabel(stepIndex, generatedTestStep) },
                {"Description", generatedTestStep.Description?? "No Value"},
                {"Configuration",GetTestStepParametter(generatedTestStep, typicalType)},
                {"SystemId", string.IsNullOrEmpty(generatedTestStep.SystemName)
                        ? "SenseAI Input System"
                        : generatedTestStep.SystemName },
                {"Adapter",generatedTestStep.AdapterName },
                {"ResultType",// typicalType == 4
                            //?
                            $"[{generatedTestStep.CatalogName}]:{generatedTestStep.TypicalName.Split(',')[0]}"
                //: "No Value"
                },
                {"StepIndex", stepIndex.ToString()},
                {"ManualOrAuto", "2"},
                {"Priority", "2"},
                {"LogicalDay", "1"},
                {"ExpectedResult", "No Value"},
                {"ExternalRef", "No Value"},
                {"UserInterface", typicalType == 3 ?  "TestTreeManager" : "T24TestBuilder"},
                { "TestCaseID", testCaseId},
                {"Type", GetTestStepType(generatedTestStep)},
                {"ExpectedFail", "0"},
                { "EndCaseOnError", "1"},
                {"TestStatus", "0"},
                {"TestDataType","1"},
                {"ScriptID","0"},

            });

            _navigatorCatalogNode.AppendChild(node);

            return testStepId;
        }
        private void AppendAutomaticCalculation(Guid id, TestStepJson generatedTestStep, List<DynamicData> dynamicDatas, Dictionary<int, TestStepJson> generatedTestSteps, string numericTestStepId, string numericTestCaseId, bool filter)
        {


            dynamicDatas.ToList().ForEach(automaticCalculation =>
            {
                if (automaticCalculation != null)
                {
                    var automaticCalculationId = ExtractNumericId(Guid.NewGuid());
                    XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Automatic_x0020_Calculation", string.Empty);
                    var itemid = AppendAutomaticCalculationItem(automaticCalculation, generatedTestSteps, numericTestCaseId);
                    AppendDynamicDataItemDynamicDataAssocation(Guid.NewGuid(), itemid, automaticCalculationId);
                    CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
                    {
                        {"ID", automaticCalculationId},
                        {"AttributeName",automaticCalculation.TargetAttributeName},
                        {"CatalogueName",generatedTestStep.CatalogName},
                        {"TypicalName", generatedTestStep.TypicalName.Split(',')[0]},
                        {"Plain Text Formula", automaticCalculation.TestStepId==Guid.Empty
                                        ? Encoding.UTF8.GetString(automaticCalculation.PlainTextFormula, 0, automaticCalculation.PlainTextFormula.Length)
                                        : $"r_{automaticCalculation.SourceAttributeName.Replace("@", "_")}"},
                        {"TargetDataType", "0"},
                        {"UI Mode", "2" },
                        {"PreserveResultMethod" ,"No Value"}
                    });

                    _navigatorCatalogNode.AppendChild(node);
                    if (filter) AppendAutomaticCalculationToFilterAssocation(Guid.NewGuid(), automaticCalculationId, numericTestStepId);
                    else
                        AppendDefaultAssocation(Guid.NewGuid(), automaticCalculationId, numericTestStepId);
                }
            });
            return;
        }
        private string AppendAutomaticCalculationItem(DynamicData generatedAutomaticCalculationItem, Dictionary<int, TestStepJson> generatedTestSteps, string numericTestCaseId)
        {
            var automaticCalculationItemId = ExtractNumericId(Guid.NewGuid());
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Automatic_x0020_Calculation_x0020_Item", string.Empty);
            var testStep = generatedTestSteps.First(items => items.Value.Id == generatedAutomaticCalculationItem.TestStepId.ToString());
            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", automaticCalculationItemId},
                {"AttributeName", generatedAutomaticCalculationItem.SourceAttributeName},
                {"CatalogueName",generatedTestSteps[testStep.Key].CatalogName},
                {"TypicalName",GetTestStepTypical(generatedTestSteps[testStep.Key])},
                {"Collect From", "2"},
                {"Type", "0" },
                {"Variable Name" ,$"r_{generatedAutomaticCalculationItem.SourceAttributeName.Replace("@", "_")}"}
            });
            _navigatorCatalogNode.AppendChild(node);
            var associateTestStepId = (decimal.Parse(ExtractNumericId(new Guid(testStep.Value.Id))) * decimal.Parse(numericTestCaseId)).ToString().Substring(0, 6);
            AppendDynamicDataItemTestStepAssocation(Guid.NewGuid(), automaticCalculationItemId, associateTestStepId);


            return automaticCalculationItemId;
        }
        private void AppendConstains(Guid id, TestStepJson generatedTestStep, Dictionary<int, TestStepJson> generatedTestSteps, string numericTestStepId, string numericTestCaseId)
        {

            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "FilteringConstraint", string.Empty);

            generatedTestStep.Filters?.Where(f => !string.IsNullOrEmpty(f.OperatorName))
                .ToList()
                .ForEach(generatedFilter =>
                {
                    var filterId = ExtractNumericId(Guid.NewGuid());
                    CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
                    {
                        {"ID", filterId},
                        {"AttributeName",  generatedFilter.AttributeName},
                        {"TestValue",generatedFilter.AttributeValue},
                        {"OpCode",((FilterType)Enum.Parse(typeof(FilterType), generatedFilter.OperatorName)).ToString()},
                        {"IsMask", "false"},
                        {"IsPostFilter", "false" },
                        {"TypicalName",generatedTestStep.TypicalName },
                        {"CatalogueName", generatedTestStep.CatalogName }

                    });
                    _navigatorCatalogNode.AppendChild(node);
                    AppendFilterConstrainTestStepAssocation(Guid.NewGuid(), numericTestStepId, filterId);
                    if (generatedFilter.DynamicData != null)
                    {
                        List<DynamicData> data = new List<DynamicData>();
                        data.Add(generatedFilter.DynamicData);
                        AppendAutomaticCalculation(Guid.NewGuid(), generatedTestStep, data, generatedTestSteps, filterId, numericTestCaseId, true);
                    }
                }
            );
            generatedTestStep.PostFilters?.Where(f => !string.IsNullOrEmpty(f.OperatorName))
                .ToList()
                .ForEach(generatedFilter =>
                {
                    var filterId = ExtractNumericId(Guid.NewGuid());
                    CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
                    {
                        {"ID", filterId},
                        {"AttributeName",  generatedFilter.AttributeName},
                        {"Value",generatedFilter.AttributeValue},
                        {"Type",((FilterType)Enum.Parse(typeof(FilterType), generatedFilter.OperatorName)).ToString()},
                        {"IsMask", "false"},
                        {"IsPostFilter", "true" },
                         {"TypicalName",generatedTestStep.TypicalName },
                        {"CatalogueName", generatedTestStep.CatalogName }
                    });
                    _navigatorCatalogNode.AppendChild(node);
                    AppendFilterConstrainTestStepAssocation(Guid.NewGuid(), numericTestStepId, filterId);
                    if (generatedFilter.DynamicData != null)
                    {
                        List<DynamicData> data = new List<DynamicData>();
                        data.Add(generatedFilter.DynamicData);
                        AppendAutomaticCalculation(Guid.NewGuid(), generatedTestStep, data, generatedTestSteps, filterId, numericTestCaseId, true);
                    }
                }
            );

            return;
        }
        private string GetTestStepTypical(TestStepJson generatedTestStep)
        {
            if (!string.IsNullOrEmpty(generatedTestStep.TypicalName) && generatedTestStep.TypicalName.Contains(","))
            {
                return generatedTestStep.TypicalName.Split(',').First();
            }

            return generatedTestStep.TypicalName;
        }



        private string AppendActionParams(Guid id, TestStepJson generatedTestStep)
        {
            var actionParamId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "ActionParams", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", actionParamId},
                {"AdapterName",generatedTestStep.AdapterName},
                {"FinancialObject",$"[{ generatedTestStep.CatalogName }]:{ generatedTestStep.TypicalName.Split(',')[0] }"},
                {"TargetObject",generatedTestStep.AdapterName},
                {"IsHistoryBound", "False"}
            });
            _navigatorCatalogNode.AppendChild(node);

            return actionParamId;
        }

        private string AppendAppError(Guid id, TestStepJson generatedTestStep)
        {
            var actionParamId = ExtractNumericId(id);

            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "AppError", string.Empty);
            node.SetAttribute("Catalog", "NavigatorCatalog");
            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", actionParamId},
                {"Error Message",generatedTestStep.ExpectedError},

            });
            _navigatorCatalogNode.AppendChild(node);

            return actionParamId;
        }
        private string AppendDataPool(Guid id, string title)
        {
            var dataPoolId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Data_x0020_Pool", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>
            {
                {"ID", ExtractNumericId(id)},
                {"Name", title}
            });
            _navigatorCatalogNode.AppendChild(node);

            return dataPoolId;
        }

        private string AppendDataPoolItem(Guid id, int stepIndex, TestStepJson generatedTestStep, List<string> instancesOrder)
        {
            var dataPoolId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Data_x0020_Pool_x0020_Item", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>
            {
                {"ID", ExtractNumericId(id)},
                {"Label", $"Step {stepIndex} label"},
                {"FinancialObject",$"[{ generatedTestStep.CatalogName }]:{ generatedTestStep.TypicalName.Split(',')[0] }"},
                {"InstancesOrder", string.Join(";", instancesOrder.ToArray())},
            });
            _navigatorCatalogNode.AppendChild(node);

            return dataPoolId;
        }

        private string AppendDataPoolRow(Guid id, string title)
        {
            var dataPoolId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Data_x0020_Pool_x0020_Row", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>
            {
                {"ID", ExtractNumericId(id)},
                {"Name", title}
            });
            _navigatorCatalogNode.AppendChild(node);

            return dataPoolId;
        }

        private string AppendTestData(TestStepJson generatedTestStep, TypicalInstance instance)
        {
            var testDataId = ExtractNumericId(Guid.NewGuid());
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, generatedTestStep.TypicalName.Split(',')[0], string.Empty);
            node.SetAttribute("Catalog", generatedTestStep.CatalogName);

            var attributes = new Dictionary<string, string>();
            instance.Attributes.ForEach(attribute =>
            {
                if (!attributes.ContainsKey(attribute.Name))
                    attributes.Add(attribute.Name, attribute.Value);
                else
                    attributes[attribute.Name] = attribute.Value;
            });
            if (!attributes.ContainsKey("ID"))
                attributes.Add("ID", testDataId);
            else
                attributes["ID"] = testDataId;

            CreateNodeXml(_vXmlDoc, node, attributes);
            _navigatorCatalogNode.AppendChild(node);

            return testDataId;
        }
        private string AppendSimpleTestData(TestStepJson generatedTestStep)
        {
            var testDataId = ExtractNumericId(Guid.NewGuid());
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, generatedTestStep.TypicalName.Split(',')[0], string.Empty);
            node.SetAttribute("Catalog", generatedTestStep.CatalogName);

            var attributes = new Dictionary<string, string>();

            if (!attributes.ContainsKey("ID"))
                attributes.Add("ID", testDataId);
            else
                attributes["ID"] = testDataId;

            CreateNodeXml(_vXmlDoc, node, attributes);
            _navigatorCatalogNode.AppendChild(node);

            return testDataId;
        }
        private string AppendDefaultAssocation(Guid id, string sourceId, string targetId)
        {
            var assocationId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Association", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", assocationId},
                {"Name", "Default Association"},
                {"Alias","this is it"},
                {"SourceID",sourceId},
                {"TargetID",targetId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return assocationId;
        }
        private string AppendAutomaticCalculationToFilterAssocation(Guid id, string sourceId, string targetId)
        {
            var assocationId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Association", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", assocationId},
                {"Name", "Automatic Calculation to Filter"},
                {"Alias","has automatic calculation"},
                {"SourceID",sourceId},
                {"TargetID",targetId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return assocationId;
        }
        private string AppendDynamicDataItemTestStepAssocation(Guid id, string sourceId, string targetId)
        {
            var assocationId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Association", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", assocationId},
                {"Name", "Automatic Calculation Item to TestStep"},
                {"Alias","item to step"},
                {"SourceID",sourceId},
                {"TargetID",targetId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return assocationId;
        }
        private string AppendFilterConstrainTestStepAssocation(Guid id, string sourceId, string targetId)
        {
            var assocationId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Association", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", assocationId},
                {"Name", "TestStepFilterConstraint"},
                {"Alias","is filtered by"},
                {"SourceID",sourceId},
                {"TargetID",targetId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return assocationId;
        }
        private string AppendDynamicDataItemDynamicDataAssocation(Guid id, string sourceId, string targetId)
        {
            var assocationId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Association", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", assocationId},
                {"Name", "Automatic Calculation Item Collection"},
                {"Alias","contains in calculation"},
                {"SourceID",sourceId},
                {"TargetID",targetId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return assocationId;
        }
        private string AppendTestStepToAppErrorAssocation(Guid id, string sourceId, string targetId)
        {
            var assocationId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Association", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", assocationId},
                {"Name", "Test Step to Expected Result"},
                {"Alias","Test Step to Expected Result"},
                {"SourceID",sourceId},
                {"TargetID",targetId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return assocationId;
        }

        private string AppendTestCaseDataPoolCollectionAssocation(Guid id, string sourceId, string targetId)
        {
            var assocationId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Association", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", assocationId},
                {"Name", "Test Case Data Pool Collection"},
                {"Alias","this is it"},
                {"SourceID",sourceId},
                {"TargetID",targetId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return assocationId;
        }

        private string AppendDataPoolItemsCollectionAssocation(Guid id, string sourceId, string targetId)
        {
            var assocationId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Association", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", assocationId},
                {"Name", "Data Pool Items Collection"},
                {"Alias","this is it"},
                {"SourceID",sourceId},
                {"TargetID",targetId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return assocationId;
        }

        private string AppendFOToDataPoolItemAssocation(Guid id, string sourceId, string targetId)
        {
            var assocationId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Association", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", assocationId},
                {"Name", "FO to Data Pool Item"},
                {"Alias","this is it"},
                {"SourceID",sourceId},
                {"TargetID",targetId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return assocationId;
        }

        private string AppendDataPoolRowToDataPoolAssociation(Guid id, string sourceId, string targetId)
        {
            var assocationId = ExtractNumericId(id);
            XmlElement node = _vXmlDoc.CreateElement(string.Empty, "Association", string.Empty);

            CreateNodeXml(_vXmlDoc, node, new Dictionary<string, string>()
            {
                {"ID", assocationId},
                {"Name", "DataPoolRow to DataPool Association"},
                {"Alias","this is it"},
                {"SourceID",sourceId},
                {"TargetID",targetId}
            });
            _navigatorCatalogNode.AppendChild(node);

            return assocationId;
        }

        #endregion [ Append Entities ]

        private static string ExtractNumericId(Guid id)
        {
            if (id == Guid.Empty)
            {
                id = Guid.NewGuid();
            }

            var returnVal = string.Join(null, System.Text.RegularExpressions.Regex.Split(id.ToString(), "[^\\d]"));
            returnVal = decimal.Parse(returnVal).ToString();
            if (returnVal.Length > 6)
            {
                return returnVal.Substring(0, 6);
            }

            return returnVal;
        }

        private void CreateNodeXml(XmlDocument doc, XmlElement subProject, Dictionary<string, string> attributes)
        {
            foreach (var item in attributes)
            {
                XmlElement xmlElement = doc.CreateElement(string.Empty, XmlConvert.EncodeName(item.Key), string.Empty);
                XmlText text = doc.CreateTextNode(item.Value);
                xmlElement.AppendChild(text);
                subProject.AppendChild(xmlElement);
            }
        }

        private byte[] ToByteArray()
        {
            MemoryStream ms = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(ms))
            {
                _vXmlDoc.WriteTo(writer);
            }

            return ms.ToArray();
        }

        #region [ Converter ]

        private string GetTestStepLabel(int stepIndex, TestStepJson generatedTestStep)
        {
            return generatedTestStep.DataSetId != null && generatedTestStep.DataSetId != Guid.Empty
                                ? $"Step {stepIndex} label"
                                : "No Value";
        }

        private string GetTestStepParametter(TestStepJson generatedTestStep, int typicalType)
        {
            var parameters = new List<string>();

            if (typicalType == 4 && !(generatedTestStep.Action == SenseAI.Domain.ActionType.See || generatedTestStep.Action == ActionType.EnquiryResult))
                return $"CLICK(\"{generatedTestStep.Parameters.ToString()}\")";

            if (typicalType == 3)
            {
                if (generatedTestStep.Action == ActionType.Input)
                    parameters.Add("AA.NEW");
                else
                    parameters.Add("AA");
            }

            if (!string.IsNullOrEmpty(generatedTestStep.TypicalName) && generatedTestStep.TypicalName.Contains(","))
                parameters.Add(generatedTestStep.TypicalName.Split(',').Last());

            switch (generatedTestStep.Action)
            {
                case ActionType.Menu:
                    parameters.Add(",MNU,,,,,,");
                    break;

                case ActionType.See:
                    parameters.Add("S");
                    break;

                case ActionType.Delete:
                    parameters.Add("D");
                    break;

                case ActionType.Authorize:
                    parameters.Add("A");
                    break;

                case ActionType.Reverse:
                    parameters.Add("R");
                    break;

                case ActionType.Tab:
                    parameters.Add(",TAB,,,,,,");
                    break;

                case ActionType.Input:
                    parameters.Add("I");
                    break;

                case ActionType.Verify:
                    parameters.Add("V");
                    break;

                case ActionType.SingOff:
                case ActionType.EnquiryAction:
                case ActionType.EnquiryResult:

                    break;
            }

            if (parameters.Count > 1)
                return string.Join(",", parameters.ToArray());

            parameters.Add(generatedTestStep.Parameters);

            return string.Join("", parameters.ToArray());
        }

        private string GetTestStepType(TestStepJson generatedTestStep)
        {
            if (generatedTestStep.IsNegativeStep)
                return "2";

            switch (generatedTestStep.Action)
            {
                case SenseAI.Domain.ActionType.EnquiryResult:
                case SenseAI.Domain.ActionType.See:
                    return "2";

                default:
                    return "1";
            }

        }

        #endregion [ Converter ]
    }
    [DataContract(Name = "FilterType", Namespace = "http://schemas.datacontract.org/2004/07/ValidataCommon")]
    public enum FilterType : int
    {
        [EnumMember()]
        GreaterThan = 0,

        [EnumMember()]
        GreaterOrEqual = 1,

        [EnumMember()]
        Equal = 2,

        [EnumMember()]
        LessOrEqual = 3,

        [EnumMember()]
        Less = 4,

        [EnumMember()]
        Like = 5,

        [EnumMember()]
        NotEqual = 6,

        [EnumMember()]
        NotLike = 7,

        [EnumMember()]
        TemplateName = 8,

        [EnumMember()]
        Between = 9,
    }
}