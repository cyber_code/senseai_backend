﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace SenseAI.Domain.TestCaseModel
{
    public sealed class ATSTestCases : BaseEntity
    {
        private readonly IList<ATSTestSteps> _steps = new List<ATSTestSteps>();

        public ATSTestCases(ulong noderef, string uId, Guid workflowId, Guid workflowPathId, int versionId, Guid testCaseId, Guid dataSetId, ulong dataSetNoderef, ulong projectNodref, ulong subProjectNodref)
        {
            Noderef = noderef;
            UId = uId;
            WorkflowId = workflowId;
            WorkflowPathId = workflowPathId;
            VersionId = versionId;
            TestCaseId = testCaseId;
            DataSetId = dataSetId;
            DataSetNoderef = dataSetNoderef;
            ProjectNodref = projectNodref;
            SubProjectNodref = subProjectNodref;
        }

        public ulong Noderef { get; private set; }

        public string UId { get; private set; }

        public Guid WorkflowId { get; private set; }

        public Guid WorkflowPathId { get; private set; }

        public int VersionId { get; private set; }

        public Guid TestCaseId { get; private set; }

        public Guid DataSetId { get; private set; }

        public ulong DataSetNoderef { get; private set; }

        public ulong ProjectNodref { get; private set; }

        public ulong SubProjectNodref { get; private set; }

        public IEnumerable<ATSTestSteps> Steps
        {
            get { return _steps.ToImmutableArray(); }
        }


        public void AddATSTestSteps(ATSTestSteps[] steps)
        {
            foreach (var step in steps)
            {
                _steps.Add(step);
            }
        }

        public void AddATSTestStep(ATSTestSteps step)
        {
            _steps.Add(step); 
        }
    }
}
