﻿using Newtonsoft.Json;
using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class DynamicDataSuggestion : BaseEntity
    {
        public DynamicDataSuggestion()
        {

        }
        public DynamicDataSuggestion(Guid id,string sourceTypicalName, string sourceTypicalAttributeName, string targetTypicalName, string targetTypicalAttributeName, int frequency) : base(id)
        {
            this.SourceTypicalName = sourceTypicalName;
            this.SourceTypicalAttributeName = sourceTypicalAttributeName;
            this.TargetTypicalName = targetTypicalName;
            this.TargetTypicalAttributeName = targetTypicalAttributeName;
            this.Frequency = frequency;
        }
        public DynamicDataSuggestion(string sourceTypicalName, string sourceTypicalAttributeName,string targetTypicalName, string targetTypicalAttributeName, int frequency) : this(Guid.NewGuid(),sourceTypicalName,sourceTypicalAttributeName,targetTypicalName,targetTypicalAttributeName,frequency)
        {
           
        }


        public string SourceTypicalName { get; set; }

        public string SourceTypicalAttributeName { get; set; }

        public string TargetTypicalName { get; set; }

        public string TargetTypicalAttributeName { get; set; }

        public int Frequency { get; set; }
    }
}