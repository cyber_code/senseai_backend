﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class ExportedTestCase : BaseEntity
    {
        public Guid WorkflowId { get; set; }
        public long TestCaseId { get; set; }
        public DateTime ExportTime { get; set; }
        public Guid SprintId { get; set; }
    }
}
