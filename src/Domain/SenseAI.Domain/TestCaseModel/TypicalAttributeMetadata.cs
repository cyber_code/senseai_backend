﻿namespace SenseAI.Domain.WorkflowModel
{
    public sealed class TypicalAttributeMetadata
    {
        public TypicalAttributes[] Attributes { get; set; }
    }

    public sealed class TypicalAttributes
    {
        public string Name { get; set; }

        public string DataType { get; set; }

        public string Status { get; set; }

        public string DefValue { get; set; }

        public string MinValue { get; set; }

        public string MaxValue { get; set; }

        public string Length { get; set; }
    }
}