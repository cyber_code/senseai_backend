﻿using Newtonsoft.Json;

namespace SenseAI.Domain.TestCaseModel
{
    public sealed class ATSFilter
    {
        [JsonConstructor]
        public ATSFilter(string id, ATSDynamicData dynamicData)
        {
            Id = id;
            DynamicData = dynamicData; 
        }

        public string Id { get; private set; }
         
        public ATSDynamicData DynamicData { get; private set; } 
    }
}
