﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public interface IDynamicDataSuggestionRepository : IRepository<DynamicDataSuggestion>
    {
        void Add(DynamicDataSuggestion dynamicDataSuggestion);

        void Update(DynamicDataSuggestion dynamicDataSuggestion);

        DynamicDataSuggestion Get(string sourceTypicalName, string sourceTypicalAttributeName, string targetTypicalName, string targetTypicalAttributeName);
    }
}