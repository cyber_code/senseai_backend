﻿using SenseAI.Domain.BaseModel;
using System.Collections.Generic;

namespace SenseAI.Domain.WorkflowModel
{
    public class TypicalInstance : BaseEntity
    {
        public List<Attribute> Attributes { get; set; } = new List<Attribute>();

        public object Clone()
        {
            var result = new TypicalInstance { Attributes = new List<Attribute>() };
            Attributes.ForEach(attribute => result.Attributes.Add(new Attribute { Name = attribute.Name, Value = attribute.Value }));

            return result;
        }
    }
}