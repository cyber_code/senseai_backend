﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class TestStep : BaseEntity
    {
        private IList<Filter> _filters;
        private IList<Filter> _postFilters;
        private IList<DynamicData> _dynamicDatas;

        internal TestStep(string title, string description, string adapterName, string systemName, string catalogName, Guid typicalId, string typicalName,
            string parameters, bool isNegativeStep, string expectedError, string type, int action, Guid workflowItemId, Guid linkWorkflowId, Guid catalogId)
        {
            Title = title;
            Description = description;
            AdapterName = adapterName;
            SystemName = systemName;
            CatalogName = catalogName;
            TypicalName = typicalName;
            TypicalId = typicalId;
            Parameters = parameters;
            IsNegativeStep = isNegativeStep;
            ExpectedError = expectedError;
            Type = type;
            Action = action;
            _filters = new List<Filter>();
            _postFilters = new List<Filter>();
            _dynamicDatas = new List<DynamicData>();
            WorkflowItemId = workflowItemId;
            LinkWorkflowId = linkWorkflowId;
            CatalogId = catalogId;
        }

        internal TestStep(Guid id, string title, string description, string adapterName, string systemName, string catalogName, Guid typicalId, string typicalName,
           string parameters, bool isNegativeStep, string expectedError, string type, int action, Guid workflowItemId, Guid linkWorkflowId, Guid catalogId) : base(id)
        {
            Title = title;
            Description = description;
            AdapterName = adapterName;
            SystemName = systemName;
            CatalogName = catalogName;
            TypicalName = typicalName;
            TypicalId = typicalId;
            Parameters = parameters;
            IsNegativeStep = isNegativeStep;
            ExpectedError = expectedError;
            Type = type;
            Action = action;
            _filters = new List<Filter>();
            _postFilters = new List<Filter>();
            _dynamicDatas = new List<DynamicData>();
            WorkflowItemId = workflowItemId;
            LinkWorkflowId = linkWorkflowId;
            CatalogId = catalogId;
        }

        internal TestStep(Guid id, string title, string description, string adapterName, string systemName, string catalogName, Guid typicalId, string typicalName,
            string parameters, bool isNegativeStep, string expectedError, string type, int action) : base(id)
        {
            Title = title;
            Description = description;
            AdapterName = adapterName;
            SystemName = systemName;
            CatalogName = catalogName;
            TypicalName = typicalName;
            TypicalId = typicalId;
            Parameters = parameters;
            IsNegativeStep = isNegativeStep;
            ExpectedError = expectedError;
            Type = type;
            Action = action;
            _filters = new List<Filter>();
            _postFilters = new List<Filter>();
            _dynamicDatas = new List<DynamicData>();
        }

        internal TestStep(Guid id, string title, string description, string adapterName, string systemName, string catalogName,
            string typicalName, Guid typicalId, string parameters, bool isNegativeStep, string expectedError, Filter[] filters, Filter[] postFilters,
            DynamicData[] dynamicDatas, TypicalInstance typicalInstance, Guid dataSetId) : base(id)
        {
            Title = title;
            Description = description;
            AdapterName = adapterName;
            SystemName = systemName;
            CatalogName = catalogName;
            TypicalName = typicalName;
            TypicalId = typicalId;
            Parameters = parameters;
            IsNegativeStep = isNegativeStep;
            ExpectedError = expectedError;
            _filters = filters;
            _postFilters = postFilters;
            _dynamicDatas = dynamicDatas;
            TypicalInstance = typicalInstance;
            DataSetId = dataSetId;
        }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string AdapterName { get; private set; }

        public string SystemName { get; private set; }

        public string CatalogName { get; private set; }

        public Guid CatalogId { get; private set; }

        public string TypicalName { get; private set; }

        public Guid TypicalId { get; private set; }

        public string Parameters { get; private set; }

        public string Type { get; private set; }

        public int Action { get; private set; }

        public bool IsNegativeStep { get; private set; }

        public string ExpectedError { get; private set; }

        public Guid DataSetId { get; set; }

        public TypicalInstance TypicalInstance { get; set; }

        public Guid WorkflowItemId { get; set; }

        public Guid LinkWorkflowId { get; set; }

        public IReadOnlyCollection<Filter> Filters
        {
            get
            {
                return _filters.ToImmutableArray();
            }
        }

        public IReadOnlyCollection<Filter> PostFilters
        {
            get
            {
                return _postFilters.ToImmutableArray();
            }
        }

        public IReadOnlyCollection<DynamicData> DynamicDatas
        {
            get
            {
                return _dynamicDatas.ToImmutableArray();
            }
        }

        public void AddFilter(string attributeName, string operatorName, string attributeValue, DynamicData dynamicData)
        {
            _filters.Add(new Filter(attributeName, operatorName, attributeValue, dynamicData));
        }

        public void AddPostFilter(string attributeName, string operatorName, string attributeValue, DynamicData dynamicData)
        {
            _postFilters.Add(new Filter(attributeName, operatorName, attributeValue, dynamicData));
        }

        public void AddDynamicData(Guid testStepId, string sourceAttributeName, string targetAttributeName, byte[] plainTextFormula, short? uiMode)
        {
            _dynamicDatas.Add(new DynamicData(testStepId, sourceAttributeName, targetAttributeName, plainTextFormula, uiMode));
        }
    }
}