﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class TestGenerationFactory
    {
        public static readonly Dictionary<string, string> SubProjectXml = new Dictionary<string, string>();

        public static readonly Dictionary<string, string> TestFolderXml = new Dictionary<string, string>();

        public static readonly Dictionary<string, string> TestCaseXml = new Dictionary<string, string>();

        public static readonly Dictionary<string, string> TestStepXml = new Dictionary<string, string>();

        public static readonly Dictionary<string, string> ActionParamsXml = new Dictionary<string, string>();

        public static readonly Dictionary<string, string> TestStepAssociationsXml = new Dictionary<string, string>();

        public static readonly Dictionary<string, string> SystemsXml = new Dictionary<string, string>();

        public static readonly Dictionary<string, string> TestDataXml = new Dictionary<string, string>();

        static TestGenerationFactory()
        {
            SubProjectAttributes();
            TestFolderAttributes();
            TestCaseAttributes();
            TestStepAttributes();
            ActionParamsAttributes();
        }

        public static byte[] Create(WorkflowTestCases[] generatedTestCases)
        {
            XmlDocument doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);

            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement navigatorCatalog = doc.CreateElement(string.Empty, "NavigatorCatalog", string.Empty);
            doc.AppendChild(navigatorCatalog);

            //TODO: Get real sub project
            XmlElement subProject = doc.CreateElement(string.Empty, "Subproject", string.Empty);
            CreateNodeXml(doc, subProject, SubProjectXml);
            navigatorCatalog.AppendChild(subProject);

            //TODO: Get real test folder
            XmlElement testFolder = doc.CreateElement(string.Empty, "TestFolder", string.Empty);
            CreateNodeXml(doc, testFolder, TestFolderXml);
            navigatorCatalog.AppendChild(testFolder);

            int systemId = GenerateId(0);

            SystemsXml.Clear();
            SystemsXml.Add("ID", systemId.ToString());
            SystemsXml.Add("Name", "Erind Input System");

            XmlElement systems = doc.CreateElement(string.Empty, "Systems", string.Empty);
            systems.SetAttribute("Catalog", "NavigatorCatalog");
            CreateNodeXml(doc, systems, SystemsXml);
            navigatorCatalog.AppendChild(systems);

            var groupedPlanDetails = generatedTestCases.GroupBy(i => i.TestCaseId);

            foreach (var group in groupedPlanDetails)
            {
                int testCaseId = GenerateId(1);

                TestCaseXml.Add("ID", testCaseId.ToString());
                TestCaseXml.Add("Name", group.FirstOrDefault().TestCaseTitle);

                XmlElement testCase = doc.CreateElement(string.Empty, "TestCase", string.Empty);
                CreateNodeXml(doc, testCase, TestCaseXml);
                navigatorCatalog.AppendChild(testCase);

                WorkflowTestCases[] planDetailsList = group.ToArray();

                int stepIndex = 1;
                foreach (var item in planDetailsList)
                {
                    int testStepId = GenerateId(2);
                    int actionParamsId = GenerateId(3);
                    int testDataId = GenerateId(4);

                    var json = JObject.Parse(item.TestStepJson);
                    var testStepObj = JsonConvert.DeserializeObject<TestStepJson>(json.ToString());

                    //test step
                    TestStepAttributes();
                    TestStepXml.Add("ID", testStepId.ToString());
                    TestStepXml.Add("Name", testStepObj.Title);
                    TestStepXml.Add("ResultType", "No Value");
                    TestStepXml.Add("StepIndex", stepIndex++.ToString());
                    TestStepXml.Add("TestCaseID", testCaseId.ToString());
                    TestStepXml.Add("Description", testStepObj.Description);

                    XmlElement testStep = doc.CreateElement(string.Empty, "TestStep", string.Empty);
                    CreateNodeXml(doc, testStep, TestStepXml);

                    navigatorCatalog.AppendChild(testStep);

                    //action params
                    ActionParamsAttributes();
                    ActionParamsXml.Add("ID", actionParamsId.ToString());
                    ActionParamsXml.Add("AdapterName", testStepObj.AdapterName);
                    ActionParamsXml.Add("FinancialObject", $"[{ testStepObj.CatalogName }]:{ testStepObj.TypicalName }");
                    ActionParamsXml.Add("TargetObject", testStepObj.AdapterName);

                    XmlElement actionParams = doc.CreateElement(string.Empty, "ActionParams", string.Empty);
                    CreateNodeXml(doc, actionParams, ActionParamsXml);
                    navigatorCatalog.AppendChild(actionParams);

                    //association test step with system
                    TestStepAssociationsAttributes();
                    TestStepAssociationsXml.Add("SourceID", testStepId.ToString());
                    TestStepAssociationsXml.Add("TargetID", systemId.ToString());

                    XmlElement associationSystemWithTestStep = doc.CreateElement(string.Empty, "Association", string.Empty);
                    CreateNodeXml(doc, associationSystemWithTestStep, TestStepAssociationsXml);
                    navigatorCatalog.AppendChild(associationSystemWithTestStep);

                    //association test step with action params
                    TestStepAssociationsAttributes();
                    TestStepAssociationsXml.Add("SourceID", testStepId.ToString());
                    TestStepAssociationsXml.Add("TargetID", actionParamsId.ToString());

                    XmlElement associationActionParamsWithTestStep = doc.CreateElement(string.Empty, "Association", string.Empty);
                    CreateNodeXml(doc, associationActionParamsWithTestStep, TestStepAssociationsXml);
                    navigatorCatalog.AppendChild(associationActionParamsWithTestStep);

                    //association test step with test data
                    TestStepAssociationsAttributes();
                    TestStepAssociationsXml.Add("SourceID", testStepId.ToString());
                    TestStepAssociationsXml.Add("TargetID", testDataId.ToString());

                    XmlElement associationTestDataWithTestStep = doc.CreateElement(string.Empty, "Association", string.Empty);
                    CreateNodeXml(doc, associationTestDataWithTestStep, TestStepAssociationsXml);
                    navigatorCatalog.AppendChild(associationTestDataWithTestStep);

                    //test data
                    TestDataXml.Clear();
                    if (testStepObj.TypicalInstance != null && testStepObj.TypicalInstance.Attributes != null)
                    {
                        foreach (var testdata in testStepObj.TypicalInstance.Attributes)
                        {
                            TestDataXml.Add(testdata.Name, testdata.Value);
                        }
                    }
                    XmlElement testData = doc.CreateElement(string.Empty, testStepObj.TypicalName, string.Empty);
                    testData.SetAttribute("Catalog", testStepObj.CatalogName);
                    CreateNodeXml(doc, testData, TestDataXml);
                    navigatorCatalog.AppendChild(testData);
                }
            }

            //doc.Save("F:\\testcases.xml");

            MemoryStream ms = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(ms))
            {
                doc.WriteTo(writer); // Write to memorystream
            }

            return ms.ToArray();
        }

        private static int GenerateId(int index)
        {
            return int.Parse(DateTime.Now.ToString("HHmmssfff") + index);
        }

        private static void CreateNodeXml(XmlDocument doc, XmlElement subProject, Dictionary<string, string> attributes)
        {
            foreach (var item in attributes)
            {
                XmlElement xmlElement = doc.CreateElement(string.Empty, item.Key, string.Empty);
                XmlText text = doc.CreateTextNode(item.Value);
                xmlElement.AppendChild(text);
                subProject.AppendChild(xmlElement);
            }
        }

        #region [ Attributes definition ]

        private static void SubProjectAttributes()
        {
            SubProjectXml.Clear();
            SubProjectXml.Add("ID", "1");
            SubProjectXml.Add("Name", "Main Subproject");
        }

        private static void TestFolderAttributes()
        {
            TestFolderXml.Clear();
            TestFolderXml.Add("ID", "1");
            TestFolderXml.Add("Name", "SenseAI Generated");
            TestFolderXml.Add("Description", "No Value");
            TestFolderXml.Add("ProjectID", "1");
        }

        private static void TestCaseAttributes()
        {
            TestCaseXml.Clear();

            TestCaseXml.Add("Description", "No Value");
            TestCaseXml.Add("Type", "2");
            TestCaseXml.Add("Priority", "2");
            TestCaseXml.Add("Status", "2");
            TestCaseXml.Add("CreateDate", "2");
            TestCaseXml.Add("BusinessFunction", "No Value");
            TestCaseXml.Add("CaseIndex", "0");

            TestCaseXml.Add("TestFolderID", "1");
        }

        private static void TestStepAttributes()
        {
            TestStepXml.Clear();

            TestStepXml.Add("Type", "1");
            TestStepXml.Add("LogicalDay", "1");
            TestStepXml.Add("ExpectedResult", "No Value");
            TestStepXml.Add("Priority", "2");
            TestStepXml.Add("ExternalRef", "No Value");
            TestStepXml.Add("TestStatus", "0");
            TestStepXml.Add("ManualOrAuto", "2");
            TestStepXml.Add("EndCaseOnError", "0");
            TestStepXml.Add("ExpectedFail", "0");
            TestStepXml.Add("Configuration", "No Value");
            TestStepXml.Add("TestDataType", "0");
            TestStepXml.Add("Label", "No Value");
            TestStepXml.Add("UserInterface", "TestTreeManager");
            TestStepXml.Add("ScriptID", "0");
        }

        private static void ActionParamsAttributes()
        {
            ActionParamsXml.Clear();

            //those attributes should be taken from test step
            ActionParamsXml.Add("IsHistoryBound", "False");

            //those should be checked
            //ActionParamsXml.Add("FWMapSchemaCatalog", "No Value");
            //ActionParamsXml.Add("FWMapSchemaName", "No Value");
            //ActionParamsXml.Add("REVMapSchemaCatalog", "No Value");
            //ActionParamsXml.Add("REVMapSchemaName", "No Value");
        }

        private static void TestStepAssociationsAttributes()
        {
            TestStepAssociationsXml.Clear();

            TestStepAssociationsXml.Add("ID", "1");
            TestStepAssociationsXml.Add("Name", "Default Association");
            TestStepAssociationsXml.Add("Alias", "this is it");
        }

        #endregion [ Attributes definition ]
    }
}