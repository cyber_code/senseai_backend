﻿using System;
using System.Collections.Generic;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class TestStepJson
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string AdapterName { get; set; }

        public string SystemName { get; set; }

        public string CatalogName { get; set; }

        public Guid CatalogId { get; set; }

        public string TypicalName { get; set; }

        public string Parameters { get; set; }

        public WorkflowType Type { get; set; }

        public ActionType Action { get; set; }

        public bool IsNegativeStep { get; set; }

        public string ExpectedError { get; set; }

        public TypicalInstance TypicalInstance { get; set; }

        public List<Filter> Filters { get; set; }

        public List<Filter> PostFilters { get; set; }

        public List<DynamicData> DynamicDatas { get; set; } = new List<DynamicData>();
        public Guid DataSetId { get; set; }
    }
}