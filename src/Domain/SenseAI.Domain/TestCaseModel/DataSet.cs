﻿using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class DataSetsIds
    {
        public DataSetsIds(Guid pathItemId, Guid[] ids)
        {
            PathItemId = pathItemId;
            Ids = ids;
        }

        public DataSetsIds(Guid pathItemId, Guid[] ids, int tsIndex)
        {
            PathItemId = pathItemId;
            Ids = ids;
            TSIndex = tsIndex;
        }

        public Guid PathItemId { get; private set; }

        public Guid[] Ids { get; private set; }

        public int TSIndex { get; private set; }
    }
}