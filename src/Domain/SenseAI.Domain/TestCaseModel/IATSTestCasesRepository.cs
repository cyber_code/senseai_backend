﻿using SenseAI.Domain.BaseModel;
using SenseAI.Domain.TestCaseModel; 

namespace SenseAI.Domain.WorkflowModel
{
    public interface IATSTestCasesRepository : IRepository<ATSTestCases>
    {
        void Add(ATSTestCases aTSTestCase);
    }
}