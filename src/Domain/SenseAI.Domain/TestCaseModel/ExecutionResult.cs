﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class ExecutionResult
    {
        public ExecutionResult(Guid workflowId, long testCaseId, string testCaseTitle, long testCycleId, DateTime startDate, DateTime endDate, Guid sprintId)
        {
            WorkFlowId = workflowId;
            TestCaseId = testCaseId;
            TestCaseTitle = testCaseTitle;
            TestCycleId = testCycleId;
            Status = Status;
            StartDate = startDate;
            EndDate = endDate;
            SprintId = sprintId;
        }

        public Guid WorkFlowId { get; set; }
        public long TestCaseId { get; set; }
        public string TestCaseTitle { get; set; }
        public long TestCycleId { get; set; }
        public int Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid SprintId { get; set; }
    }
}
