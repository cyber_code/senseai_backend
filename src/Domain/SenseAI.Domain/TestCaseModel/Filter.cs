﻿using Newtonsoft.Json;
using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class Filter 
    {
        [JsonConstructor]
        internal Filter(string attributeName, string operatorName, string attributeValue, DynamicData dynamicData)
        {
            AttributeName = attributeName;
            OperatorName = operatorName;
            AttributeValue = attributeValue;
            DynamicData = dynamicData;
        }

        public string AttributeName { get; private set; }

        public string OperatorName { get; private set; }

        public string AttributeValue { get; private set; }

        public DynamicData DynamicData { get; private set; }
    }
}