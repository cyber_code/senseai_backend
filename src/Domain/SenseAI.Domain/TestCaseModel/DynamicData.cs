﻿using Newtonsoft.Json;
using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class DynamicData : BaseEntity
    {
        [JsonConstructor]
        public DynamicData(Guid testStepId, string sourceAttributeName, string targetAttributeName, byte[] plainTextFormula, short? uiMode)
        {
            TestStepId = testStepId;
            SourceAttributeName = sourceAttributeName;
            TargetAttributeName = targetAttributeName;
            PlainTextFormula = plainTextFormula;
            UiMode = uiMode;
        }

        public Guid TestStepId { get; private set; }

        public string SourceAttributeName { get; private set; }

        public string TargetAttributeName { get; private set; }

        public byte[] PlainTextFormula { get; private set; }

        public short? UiMode { get; private set; }
    }
}