﻿
using Newtonsoft.Json;

namespace SenseAI.Domain.TestCaseModel
{
    public sealed class ATSDynamicData
    {
        [JsonConstructor]
        public ATSDynamicData(string id, ulong noderef)
        {
            Id = id;
            Noderef = noderef;
        }

        public string Id { get; private set; }

        public ulong Noderef { get; private set; }
    }
}
