﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public interface ITypicalInstanceRepository : IRepository<TypicalInstance>
    {
        string Get(Guid typicalId);
    }
}