﻿using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class TestCaseDesingerData
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public TestStepDesingerData[] TestSteps { get; set; }
    }

    public sealed class TestStepDesingerData
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string AdapterName { get; set; }

        public string CatalogName { get; set; }

        public string TypicalName { get; set; }

        public Guid TypicalId { get; set; }

        public string Parameters { get; set; }

        public bool IsNegativeStep { get; set; }

        public string ExpectedError { get; set; }

        public string Type { get; set; }

        public int Action { get; set; }

        public TypicalInstance TypicalInstance { get; set; }

        public Filter[] Filters { get; set; }

        public Filter[] PostFilters { get; set; }

        public DynamicData[] DynamicDatas { get; set; }

        public Guid[] DataSetIds { get; set; }
    }
}