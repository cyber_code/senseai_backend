﻿namespace SenseAI.Domain.TestCaseModel
{
    public sealed class EnquiryActions
    {
        public string Action { get; set; }
        public string Type { get; set; }
    }
}
