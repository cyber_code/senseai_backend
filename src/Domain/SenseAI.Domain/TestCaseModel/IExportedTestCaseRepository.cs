﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public interface IExportedTestCaseRepository : IRepository<ExportedTestCase>
    {
        void StoreExportedTestCaseReference(Guid WorkflowId, long testCaseId);
    }
}