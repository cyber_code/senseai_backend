﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace SenseAI.Domain.TestCaseModel
{
    public sealed class ATSTestSteps
    {
        private IList<ATSFilter> _filters;
        private IList<ATSFilter> _postFilters;
        private IList<ATSDynamicData> _dynamicDatas;

        public ATSTestSteps(ulong noderef, string uId, ulong testCaseNoderef, Guid testStepId)
        {
            Noderef = noderef;
            UId = uId;
            TestCaseNoderef = testCaseNoderef;
            TestStepId = testStepId;
            _filters = new List<ATSFilter>();
            _postFilters = new List<ATSFilter>();
            _dynamicDatas = new List<ATSDynamicData>();
        }

        public ulong Noderef { get; private set; }

        public string UId { get; private set; }

        public ulong TestCaseNoderef { get; private set; }

        public Guid TestStepId { get; private set; }


        public IReadOnlyCollection<ATSFilter> Filters
        {
            get
            {
                return _filters.ToImmutableArray();
            }
            private set
            {
                value.ToImmutableList().ForEach(f =>
                {
                    _filters.Add(f);
                });
            }
        }

        public IReadOnlyCollection<ATSFilter> PostFilters
        {
            get
            {
                return _postFilters.ToImmutableArray();
            }
            private set
            {
                value.ToImmutableList().ForEach(f =>
                {
                    _postFilters.Add(f);
                });
            }
        }


        [JsonProperty(PropertyName = "DynamicDatas")]
        public IReadOnlyCollection<ATSDynamicData> DynamicDatas
        {
            get
            {
                return _dynamicDatas.ToImmutableArray();
            }
            private set
            {
                value.ToImmutableList().ForEach(f =>
                {
                    _dynamicDatas.Add(f);
                });
            }
        }

        public void AddATSDynamicData(string id, ulong noderef)
        {
            _dynamicDatas.Add(new ATSDynamicData(id, noderef));
        }

        public void AddATSPostFilters(string id, ATSDynamicData dynamicData)
        {
            _postFilters.Add(new ATSFilter(id, dynamicData));
        }

        public void AddATSFilters(string id, ATSDynamicData dynamicData)
        {
            _filters.Add(new ATSFilter(id, dynamicData));
        }

    }
}
