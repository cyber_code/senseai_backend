﻿using System.Security.Cryptography;
using System.Text;

namespace SenseAI.Domain
{
    public sealed class MD5HashManager
    {
        /// <summary>
        /// Compute MD5 hash for the specified string
        /// </summary>
        /// <param name="content">String content to compute hash</param>
        /// <returns>MD5 hash code</returns>
        public string ComputeStringHash(string content)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5.ComputeHash(Encoding.Default.GetBytes(content)))
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }
    }
}
