﻿namespace SenseAI.Domain.Adapters
{
    public sealed class AdapterSteps
    {
        public string Name { get; set; }

        public Step[] Steps { get; set; }
    }

    public class Step
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
