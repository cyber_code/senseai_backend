﻿namespace SenseAI.Domain.Adapters
{
    public sealed class T24AdapterSettings : IAdapterSettings
    {
        public new enum Operators
        {
            /// <summary>
            ///     Greater Than
            /// </summary>
            GreaterThan = 0,

            /// <summary>
            ///     Greater Or Equal
            /// </summary>
            GreaterOrEqual = 1,

            /// <summary>
            ///     Equal
            /// </summary>
            Equal = 2,

            /// <summary>
            ///     Less Or Equal
            /// </summary>
            LessOrEqual = 3,

            /// <summary>
            ///     Less
            /// </summary>
            Less = 4,

            /// <summary>
            ///     Like
            /// </summary>
            Like = 5,

            /// <summary>
            ///     Not Equal
            /// </summary>
            NotEqual = 6,

            /// <summary>
            ///     Not Like
            /// </summary>
            NotLike = 7,

            /// <summary>
            ///     Enquiry Result Template Name
            /// </summary>
            TemplateName = 8,

            /// <summary>
            /// Between
            /// </summary>
            Between = 9
        }

        public new enum ActionTypes
        {
            Menu,
            See,
            Delete,
            Authorize,
            Reverse,
            SingOff,
            Tab,
            Input,
            EnquiryAction,
            EnquiryResult,
            Verify
        }
    }
}