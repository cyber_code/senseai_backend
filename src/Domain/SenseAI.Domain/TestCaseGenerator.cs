﻿
using SenseAI.Domain.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SenseAI.Domain
{
    public sealed class TestCaseGenerator : ITestCaseGenerator
    {
        private List<WorkflowItem> _items;
        private List<WorkflowItemLink> _links;
        List<SystemModel.System> _systems;
        int index = 0;
        private Workflow _workflow;

        private WorkflowVersionPaths _workflowPath;
        private WorkflowVersionPathDetails[] _workflowPathDetails;

        public TestCaseGenerator(Workflow workflow, List<SystemModel.System> systems)
        {
            _workflow = workflow;
            _systems = systems;
        }

        /// <summary>
        /// Generate test cases for workflow based on a workflow path
        /// </summary>
        /// <param name="workflowPaths"></param>
        /// <param name="dataSetsIds"></param>
        /// <returns></returns>
        public List<TestCase> Generate(WorkflowVersionPaths workflowPath)
        {
            if (_workflow == null)
                throw new ArgumentException("The workflow object is null.", "workflow");

            if (workflowPath == null || workflowPath.Items == null || !workflowPath.Items.Any())
                throw new ArgumentException("The workflow doesn't have any path.", "workflowPaths");

            if (workflowPath.Items.Any(w => w.DataSetId == Guid.Empty && w.ActionType == (int)ActionType.Input))
                throw new ArgumentException("Data sets ids cannot be empty.", "dataSetsIds");

            List<TestCase> testCases = new List<TestCase>();
            _workflowPath = workflowPath;//

            _workflowPathDetails = workflowPath.Items.Where(wfp => wfp.Type == 1).OrderBy(tsi => tsi.Index).ToArray();
            /*Get items and links of workflow related to this path*/
            _items = _workflow.Items.Where(ct => workflowPath.Items.FirstOrDefault(wfp => wfp.Id == ct.Id) != null).ToList();
            _links = _workflow.ItemLinks.Where(ct => (workflowPath.Items.FirstOrDefault(wfp => wfp.Id == ct.FromId) != null) ||
                                        (workflowPath.Items.FirstOrDefault(wfp => wfp.Id == ct.ToId) != null)).ToList();

            if (_items.Count == 0 || _links.Count == 0)
                throw new Exception("The workflow does not have any items or there is no links between items.");

            var startItem = _items.Where(ct => ct is StartWorkflowItem &&
            (_links.Where(il => il.ToId == ct.Id).FirstOrDefault() == null)).FirstOrDefault();

            if (startItem == null)
                throw new Exception("The workflow does not have a start item.");

            List<TestStep> list = new List<TestStep>();

            foreach (var workflowItemLink in _links.Where(ct => ct.FromId == startItem.Id).ToList())
            {
                var workflowItem = _items.FirstOrDefault(ct => ct.Id == workflowItemLink.ToId);
                if (workflowItem == null)
                    continue;
                ProcessGeneration(workflowItem, list, testCases, ((StartWorkflowItem)startItem).SystemId);
            }
            if (testCases.Count == 0)
                throw new Exception("No test cases are generated for path in the WF.");
            return testCases;
        }

        #region "Private methods for test case generation"

        /// <summary>
        /// Generate the test cases
        /// </summary>
        /// <param name="workflowItem"></param>
        /// <param name="list"></param>
        /// <param name="testCases"></param>
        ///<param name="systemId"></param>
        private void ProcessGeneration(WorkflowItem workflowItem, List<TestStep> list, List<TestCase> testCases, Guid systemId)
        {
            index = index + 1;
            if (workflowItem is StartWorkflowItem)
            {
                StartWorkflow(workflowItem, list, testCases);
            }
            else if (workflowItem is ConditionWorkflowItem)
            {
                ConditionWorkflow(workflowItem, list, testCases, systemId);
            }
            else if (workflowItem is LinkedWorkflowItem)
            {
                index = index + 1;
                LinkWorkflow(workflowItem, list, testCases, systemId);
            }
            else if (workflowItem is ManualWorkflowItem)
            {
                ManualWorkflow(list, workflowItem, testCases, systemId);
            }
            else if (workflowItem is ActionWorkflowItem)
            {
                switch ((ActionType)workflowItem.ActionType)
                {
                    case ActionType.Menu:
                    case ActionType.Authorize:
                    case ActionType.Delete:
                    case ActionType.Reverse:
                    case ActionType.See:
                    case ActionType.SingOff:
                    case ActionType.EnquiryResult:
                    case ActionType.Verify:
                    case ActionType.Tab:
                        ActionWorkflowWithNoData(list, workflowItem, testCases, systemId);
                        break;

                    case ActionType.Input:
                    case ActionType.EnquiryAction:
                        ActionWorkflowWithData(workflowItem, list, testCases, systemId);
                        break;

                    default:
                        throw new NotImplementedException("This action type is not implemented");
                }
            }
            else
                throw new NotImplementedException("This type is not implemented");
        }

        /// <summary>
        /// Action workflow with no data
        /// </summary>
        /// <param name="list"></param>
        /// <param name="workflowItem"></param>
        /// <param name="testCases"></param>
        /// <param name="systemId"></param>
        private void ActionWorkflowWithNoData(List<TestStep> list, WorkflowItem workflowItem, List<TestCase> testCases, Guid systemId)
        {
            list.Add(FillTestStep((ActionWorkflowItem)workflowItem, systemId, new TypicalInstance(), list, testCases));
            list = CreateTestCase(list, workflowItem, testCases);
            GenerateRecursively(workflowItem, list, testCases, systemId);

        }

        private List<TestStep> CreateTestCase(List<TestStep> list, WorkflowItem workflowItem, List<TestCase> testCases)
        {
            if (workflowItem.IsLastItem && list.Count > 0)
            {
                TestCase testCase = new TestCase(_workflow.Title + " TC " + (testCases.Count + 1).ToString(), _workflow.Description);
                testCase.AddTestSteps(list);
                testCases.Add(testCase);
                list = new List<TestStep>();
            }
            return list;
        }

        private void ManualWorkflow(List<TestStep> list, WorkflowItem workflowItem, List<TestCase> testCases, Guid systemId)
        {
            list.Add(FillManualTestStep((ManualWorkflowItem)workflowItem, systemId, new TypicalInstance(), list, testCases));
            list = CreateTestCase(list, workflowItem, testCases);
            GenerateRecursively(workflowItem, list, testCases, systemId);
        }

        /// <summary>
        /// Action workflow with input data
        /// </summary>
        /// <param name="workflowItem"></param>
        /// <param name="list"></param>
        /// <param name="testCases"></param>
        /// <param name="systemId"></param>
        private void ActionWorkflowWithData(WorkflowItem workflowItem, List<TestStep> list, List<TestCase> testCases, Guid systemId)
        {
            list.Add(FillTestStep((ActionWorkflowItem)workflowItem, systemId, null, list, testCases));
            list = CreateTestCase(list, workflowItem, testCases);
            GenerateRecursively(workflowItem, list, testCases, systemId);
        }

        /// <summary>
        /// Conditition workflow item
        /// </summary>
        /// <param name="newWorkflowItem"></param>
        /// <param name="list"></param>
        /// <param name="testCases"></param>
        ///<param name="systemId"></param>
        private void ConditionWorkflow(WorkflowItem newWorkflowItem, List<TestStep> list, List<TestCase> testCases, Guid systemId)
        {
            var items = _links.Where(ct => ct.FromId == newWorkflowItem.Id).ToList();
            foreach (var item in items)
            {
                index = 1;
                List<TestStep> currentList = new List<TestStep>();
                currentList.AddRange(list);
                var workflowItem = _items.FirstOrDefault(ct => ct.Id == item.ToId);
                if (workflowItem == null)
                    continue;
                ProcessGeneration(workflowItem, currentList, testCases, systemId);
            }
        }

        /// <summary>
        /// Start workflow item
        /// </summary>
        /// <param name="newWorkflowItem"></param>
        /// <param name="list"></param>
        /// <param name="testCases"></param>
        private void StartWorkflow(WorkflowItem newWorkflowItem, List<TestStep> list, List<TestCase> testCases)
        {
            TestCase testCase = new TestCase(_workflow.Title + " TC " + (testCases.Count + 1).ToString(), _workflow.Description);

            testCase.AddTestSteps(list);
            testCases.Add(testCase);
            list = new List<TestStep>();

            var startWorkflowItem = (StartWorkflowItem)newWorkflowItem;
            GenerateRecursively(newWorkflowItem, list, testCases, startWorkflowItem.SystemId);

        }

        /// <summary>
        /// Link workflow item
        /// </summary>
        /// <param name="newWorkflowItem"></param>
        /// <param name="list"></param>
        /// <param name="testCases"></param>
        /// <param name="systemId"></param>
        private void LinkWorkflow(WorkflowItem _newWorkflowItem, List<TestStep> list, List<TestCase> testCases, Guid systemId)
        {
            TestCase testCase = new TestCase(_workflow.Title + " TC " + (testCases.Count + 1).ToString(), _workflow.Description);

            if (list.Count > 0)
            {
                testCase.AddTestSteps(list);
                testCases.Add(testCase);
                list = new List<TestStep>();
            }
            var items = _workflow.ItemLinks.Where(ct => ct.FromId == _newWorkflowItem.Id);
            if (items.Count() > 1)
            {
                items = items.Where(w => w.UniqueId.Contains(_newWorkflowItem.UniqueId)).ToList();
            }
            var item = items.FirstOrDefault();

            var workflowItems = _workflow.Items.Where(ct => ct.Id == item.ToId);//
            var workflowItem = _workflow.Items.FirstOrDefault(ct => ct.Id == item.ToId);

            if (workflowItems.Count() > 1)
            {
                if (item.UniqueId != null && workflowItems.FirstOrDefault(w => w.UniqueId.Contains(item.UniqueId)) != null)
                    workflowItem = workflowItems.FirstOrDefault(w => w.UniqueId.Contains(item.UniqueId));
                else
                    workflowItem = workflowItems.FirstOrDefault(w => item.UniqueId.Contains(w.UniqueId));
            }

            GenerateRecursively(workflowItem, list, testCases, systemId);
        }

        /// <summary>
        /// Fill the test step with data from the work flow
        /// </summary>
        /// <param name="workflowItem"></param>
        /// <param name="systemId"></param>
        /// <param name="typicalInstance"></param>
        /// <param name="listTestSteps"></param>
        /// <param name="testCases"></param>
        /// <returns></returns>
        private TestStep FillTestStep(ActionWorkflowItem workflowItem, Guid systemId, TypicalInstance typicalInstance, List<TestStep> listTestSteps, List<TestCase> testCases)
        {
            string typicalName = workflowItem.TypicalName;
            string catalogName = workflowItem.CatalogName;
            Guid catalogId = workflowItem.CatalogId;
            if (workflowItem.ActionType == (int)ActionType.Menu || workflowItem.ActionType == (int)ActionType.Tab)
            {
                typicalName = "NavigationInfo";
                catalogName = "ProjectResourcing";
            }
            var system = _systems.FirstOrDefault(s => s.Id == systemId);
            TestStep testStep = new TestStep(workflowItem.Id, workflowItem.Title, workflowItem.Description, system == null ? "" : system.AdapterName, system == null ? "" : system.Title, catalogName, workflowItem.TypicalId, typicalName,
                workflowItem.Parameters, workflowItem.IsNegativeStep, workflowItem.ExpectedError, ((int)WorkflowType.Action).ToString(), workflowItem.ActionType, workflowItem.Id, workflowItem.ParenID, catalogId);

            foreach (var c in workflowItem.Constraints)
            {
                DynamicData dynamicData = null;
                bool add = true;
                if (CheckDynamicData(c.DynamicData))
                {
                    add = _workflowPathDetails.ToList().FirstOrDefault(ct => ct.Id == workflowItem.Id) != null;//check if the DD is releated with any test steps in the path
                    if (add)
                    {
                        var testStepId = GetTestStepId(testCases, listTestSteps, c.DynamicData.SourceWorkflowItemId, workflowItem.ParenID);
                        dynamicData = new DynamicData(testStepId, c.DynamicData.SourceAttributeName, c.DynamicData.TargetAttributeName, Encoding.UTF8.GetBytes(c.DynamicData.Path), c.DynamicData.UiMode);
                    }
                }
                if (add)
                    testStep.AddFilter(c.AttributeName, c.Operator, c.AttributeValue, dynamicData);
            }

            foreach (var pc in workflowItem.PostConstraints)
            {
                DynamicData dynamicData = null;
                bool add = true;
                if (CheckDynamicData(pc.DynamicData))
                {
                    add = _workflowPathDetails.ToList().FirstOrDefault(ct => ct.Id == workflowItem.Id) != null;//check if the DD is releated with any test steps in the path
                    if (add)
                    {
                        var testStepId = GetTestStepId(testCases, listTestSteps, pc.DynamicData.SourceWorkflowItemId, workflowItem.ParenID);
                        dynamicData = new DynamicData(testStepId, pc.DynamicData.SourceAttributeName, pc.DynamicData.TargetAttributeName, Encoding.UTF8.GetBytes(pc.DynamicData.Path), pc.DynamicData.UiMode);
                    }
                }
                if (add)
                    testStep.AddPostFilter(pc.AttributeName, pc.Operator, pc.AttributeValue, dynamicData);
            }

            foreach (var ac in workflowItem.DynamicDatas)
            {
                if (string.IsNullOrEmpty(ac.SourceAttributeName) && ac.SourceWorkflowItemId == Guid.Empty)
                    testStep.AddDynamicData(Guid.Empty, null, ac.TargetAttributeName, ac.PlainTextFormula, ac.UiMode);
                else
                {
                    int index = _workflowPath.Items.ToList().FindIndex(ct => ct.Id == workflowItem.Id);

                    if (workflowItem.DynamicDatas.Where(w => w.TargetAttributeName == ac.TargetAttributeName).Count() > 1)
                        index = _workflowPath.Items.ToList().FindIndex(ct => ct.Id == workflowItem.Id &&
                     ct.Title.ToLower() == ac.PathName.ToLower());

                    if (index != -1)
                    {
                        var testStepId = GetTestStepId(testCases, listTestSteps, ac.SourceWorkflowItemId, workflowItem.ParenID);
                        testStep.AddDynamicData(testStepId, ac.SourceAttributeName, ac.TargetAttributeName, Encoding.UTF8.GetBytes(ac.Path), ac.UiMode);
                    }
                }
            }

            testStep.TypicalInstance = typicalInstance;
            testStep.DataSetId = _workflowPathDetails.FirstOrDefault(ct => ct.Id == workflowItem.Id).DataSetId;

            return testStep;
        }

        private TestStep FillManualTestStep(ManualWorkflowItem workflowItem, Guid systemId, TypicalInstance typicalInstance, List<TestStep> listTestSteps, List<TestCase> testCases)
        {
            string typicalName = "Generic FO";
            string catalogName = "FinancialObjects";
            Guid catalogId = workflowItem.CatalogId;
            var system = _systems.FirstOrDefault(s => s.Id == systemId);

            TestStep testStep = new TestStep(workflowItem.Id, workflowItem.Title, workflowItem.Description, system == null ? "" : system.AdapterName, system == null ? "" : system.Title, catalogName, workflowItem.TypicalId, typicalName,
                workflowItem.Parameters, false, "", ((int)WorkflowType.Manual).ToString(), workflowItem.ActionType, workflowItem.Id, workflowItem.ParenID, catalogId);

            testStep.TypicalInstance = typicalInstance;
            return testStep;
        }

        /// <summary>
        /// Check the status of dd object
        /// </summary>
        /// <param name="dynamicData"></param>
        /// <returns></returns>
        private bool CheckDynamicData(WorkflowItemDynamicData dynamicData)
        {
            return dynamicData != null && dynamicData.SourceWorkflowItemId != Guid.Empty &&
                !string.IsNullOrEmpty(dynamicData.SourceAttributeName);
        }

        /// <summary>
        /// Find the test step id 
        /// </summary>
        /// <param name="testCases"></param>
        /// <param name="testSteps"></param>
        /// <param name="workflowItemId"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        private Guid GetTestStepId(List<TestCase> testCases, List<TestStep> testSteps, Guid workflowItemId, Guid parentID)
        {
            Guid tsId = Guid.Empty;
            foreach (var tc in testCases)
            {
                TestStep testStep = tc.TestSteps.FirstOrDefault(ct => ct.WorkflowItemId == workflowItemId);
                var items = tc.TestSteps.Where(ct => ct.WorkflowItemId == workflowItemId);
                if (items.Count() > 1 && parentID != Guid.Empty)
                    testStep = tc.TestSteps.FirstOrDefault(ct => ct.WorkflowItemId == workflowItemId && ct.LinkWorkflowId == parentID);
                if (testStep != null)
                {
                    tsId = testStep.Id;
                    break;
                }
            }
            if (tsId == Guid.Empty)
            {
                foreach (var ts in testSteps)
                {
                    if (parentID != Guid.Empty)
                    {
                        if (ts.LinkWorkflowId == parentID && ts.WorkflowItemId == workflowItemId)
                        {
                            tsId = ts.Id;
                            break;
                        }
                    }
                    else if (ts.WorkflowItemId == workflowItemId)
                    {
                        tsId = ts.Id;
                        break;
                    }
                }
            }
            if (tsId == Guid.Empty)
            {
                foreach (var ts in testSteps)
                {
                    if (ts.WorkflowItemId == workflowItemId)
                    {
                        tsId = ts.Id;
                        break;
                    }
                }
            }
            return tsId;
        }

        /// <summary>
        /// Generate recursively the test cases
        /// </summary>
        /// <param name="newWorkflowItem"></param>
        /// <param name="list"></param>
        /// <param name="testCases"></param>
        /// <param name="systemId">systemId</param>
        private void GenerateRecursively(WorkflowItem newWorkflowItem, List<TestStep> list, List<TestCase> testCases, Guid systemId)
        {
            var items = _workflow.ItemLinks.Where(ct => ct.FromId == newWorkflowItem.Id);

            if (items.Count() == 0 || (newWorkflowItem.ParenID != Guid.Empty && items.Where(ct => ct.ParenID == newWorkflowItem.ParenID).Count() == 0)
               )
            {
                TestCase testCase = new TestCase(_workflow.Title + " TC " + (testCases.Count + 1).ToString(), _workflow.Description);
                testCase.AddTestSteps(list);
                testCases.Add(testCase);
                return;
            }

            if (items.Count() > 1)
            {
                items = items.Where(w => w.UniqueId.Contains(newWorkflowItem.UniqueId)).ToList();
            }

            foreach (var newWorkflowItemLink in items)
            {
                var workflowItems = _workflow.Items.Where(ct => ct.Id == newWorkflowItemLink.ToId);//
                var workflowItem = _workflow.Items.FirstOrDefault(ct => ct.Id == newWorkflowItemLink.ToId);

                if (workflowItems.Count() > 1)
                {
                    if (newWorkflowItemLink.UniqueId != null && workflowItems.FirstOrDefault(w => w.UniqueId.Contains(newWorkflowItemLink.UniqueId)) != null)
                        workflowItem = workflowItems.FirstOrDefault(w => w.UniqueId.Contains(newWorkflowItemLink.UniqueId));
                    else
                        workflowItem = workflowItems.FirstOrDefault(w => newWorkflowItemLink.UniqueId.Contains(w.UniqueId));

                }
                ProcessGeneration(workflowItem, list, testCases, systemId);
            }
        }

        #endregion "Private methods for test case generation"
    }
}