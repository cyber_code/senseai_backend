﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.CatalogModel
{
    public interface ICatalogRepository : IRepository<Catalog>
    {
        void Add(Catalog catalog);

        void Update(Catalog catalog);

        void Delete(Guid id);

        List<Catalog> GetCatalogs(Guid subProjectId, Guid systemId);
    }
}