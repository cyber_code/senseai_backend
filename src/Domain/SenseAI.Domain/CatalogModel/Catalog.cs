﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.CatalogModel
{
    public sealed class Catalog : BaseEntity
    {
        public Catalog(Guid id, Guid projectId, Guid subProjectId, string title, string description, int type) : base(id)
        {
            ProjectId = projectId;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
            Type = type;
        }

        public Catalog(Guid projectId, Guid subProjectId, string title, string description, int type)
            : this(Guid.NewGuid(), projectId, subProjectId, title, description, type)
        {
        }

        public Guid ProjectId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public int Type { get; private set; }
    }
}