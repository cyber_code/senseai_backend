﻿namespace SenseAI.Domain.ValidationModel
{
    public sealed class RequestValidation
    {
        public RequestValidation(MessageType messageType, string message)
        {
            MessageType = messageType;
            Message = message;
        }

        public MessageType MessageType { get; private set; }

        public string Message { get; private set; }
    }
}