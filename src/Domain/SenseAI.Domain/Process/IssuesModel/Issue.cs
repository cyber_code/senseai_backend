﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.IssuesModel
{
    public sealed class Issue : BaseEntity
    {
        public Issue(Guid id) : base(id)
        {
        }

        public Issue(Guid id, Guid processId, Guid subProjectId, Guid issueTypeId, Guid assignedId, string title, string description, StatusType status, int estimate, string reason, string implication, string comment, Guid typicalId, string attribute, bool approvedForInvestigation, InvestigationStatus investigationStatus, ChangeStatus changeStatus, Guid reporterId, Guid approvedId, DateTime dateSubmitted, DateTime approvalDate, DateTime startDate, IssuePriority priority, IssueSeverity severity, DateTime dueDate, TypeDefect type, Guid componentId, string testCaseId) : base(id)
        {
            ProcessId = processId;
            SubProjectId = subProjectId;
            IssueTypeId = issueTypeId;
            AssignedId = assignedId;
            Title = title;
            Description = description;
            Status = status;
            Estimate = estimate;
            Reason = reason;
            Implication = implication;
            Comment = comment;
            TypicalId = typicalId;
            Attribute = attribute;
            ApprovedForInvestigation = approvedForInvestigation;
            InvestigationStatus = investigationStatus;
            ChangeStatus = changeStatus;
            ReporterId = reporterId;
            ApprovedId = approvedId;
            DateSubmitted = dateSubmitted;
            ApprovalDate = approvalDate;
            StartDate = startDate;
            Priority = priority;
            Severity = severity;
            DueDate = dueDate;
            Type = type;
            ComponentId = componentId;
            TestCaseId = testCaseId;
    }

        public Issue(Guid processId, Guid subProjectId, Guid issueTypeid, Guid assignedId, string title, string description, StatusType status, int estimate, string reason, string implication, string comment, Guid typicalId, string attribute, bool approvedForInvestigation, InvestigationStatus investigationStatus, ChangeStatus changeStatus, Guid reporterId, Guid approvedId, DateTime dateSubmitted, DateTime approvalDate, DateTime startDate, IssuePriority priority, IssueSeverity severity, DateTime dueDate, TypeDefect type, Guid componentId, string testCaseId) : base(Guid.NewGuid())
        {
            ProcessId = processId;
            SubProjectId = subProjectId;
            IssueTypeId = issueTypeid;
            AssignedId = assignedId;
            Title = title;
            Description = description;
            Status = status;
            Estimate = estimate;
            Reason = reason;
            Implication = implication;
            Comment = comment;
            TypicalId = typicalId;
            Attribute = attribute;
            ApprovedForInvestigation = approvedForInvestigation;
            InvestigationStatus = investigationStatus;
            ChangeStatus = changeStatus;
            ReporterId = reporterId;
            ApprovedId = approvedId;
            DateSubmitted = dateSubmitted;
            ApprovalDate = approvalDate;
            StartDate = startDate;
            Priority = priority;
            Severity = severity;
            DueDate = dueDate;
            Type = type;
            ComponentId = componentId;
            TestCaseId = testCaseId;
        }

        public Guid ProcessId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public Guid IssueTypeId { get; private set; }

        public Guid AssignedId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public StatusType Status { get; private set; }

        public int Estimate { get; private set; }

        public string Reason { get; private set; }

        public string Implication { get; private set; }
        
        public string Comment { get; private set; }

        public Guid TypicalId { get; private set; }

        public string Attribute { get; private set; }

        public bool ApprovedForInvestigation { get; private set; }
        public InvestigationStatus InvestigationStatus { get; private set; }

        public ChangeStatus ChangeStatus { get; private set; }

        public Guid ReporterId { get; private set; }

        public Guid ApprovedId { get; private set; }

        public DateTime DateSubmitted { get; private set; }

        public DateTime ApprovalDate { get; private set; }

        public DateTime StartDate { get; private set; }

        public IssuePriority Priority { get; private set; }
        public IssueSeverity Severity { get; private set; }
        public DateTime DueDate { get; private set; }
        public TypeDefect Type { get; private set; }
        public Guid ComponentId { get; private set; }

        public string TestCaseId { get; private set; }
    }
}