﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.Process.IssuesModel
{
    public interface IIssueRepository : IRepository<Issue>
    {
        void Add(Issue issue);

        void Update(Issue issue);

        void Delete(Guid id);
    }
}