﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.Process.IssuesModel
{
    public interface IIssueTypeFieldsRepository : IRepository<IssueTypeFields>
    {
        void Add(IssueTypeFields issueTypeFields);

        void Update(IssueTypeFields issueTypeFields);

        bool Delete(Guid id);
    }
}