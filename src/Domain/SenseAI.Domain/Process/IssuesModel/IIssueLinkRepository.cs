﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.Process.IssuesModel
{
    public interface IIssueLinkRepository : IRepository<IssueLink>
    {
        void Add(IssueLink issueLink);

        void Update(IssueLink issueLink);

        void Delete(Guid id);
    }
}