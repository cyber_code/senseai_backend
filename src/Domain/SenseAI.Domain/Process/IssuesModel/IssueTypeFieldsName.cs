﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.IssuesModel
{
    public sealed class IssueTypeFieldsName : BaseEntity
    {
        public IssueTypeFieldsName(Guid id) : base(id)
        {

        }

        public IssueTypeFieldsName(Guid id, string name) : base(id)
        {
            Name = name;

        }

        public IssueTypeFieldsName(string name) : base(Guid.NewGuid())
        {
            Name = name;
        }

      

        public string Name { get; private set; }

       
    }
}