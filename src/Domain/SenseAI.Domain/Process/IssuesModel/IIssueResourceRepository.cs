﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.Process.IssuesModel
{
    public interface IIssueResourceRepository : IRepository<IssueResource>
    {
        void Add(IssueResource issue);

        void Update(IssueResource issue);

        void Delete(Guid id);
    }
}
