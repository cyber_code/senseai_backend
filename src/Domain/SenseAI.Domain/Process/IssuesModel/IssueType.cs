﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.IssuesModel
{
    public sealed class IssueType : BaseEntity
    {
        public IssueType(Guid id) : base(id)
        {

        }

        public IssueType(Guid id, Guid subProjectId, string title, string description, string unit, bool def) : base(id)
        {
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
            Unit = unit;
            Default = def;

        }

        public IssueType(Guid subProjectId, string title, string description, string unit, bool def) : base(Guid.NewGuid())
        {
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
            Unit = unit;
            Default = def;
        }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }
        public string Unit { get; private set; }
        public bool Default { get; private set; }
    }
}