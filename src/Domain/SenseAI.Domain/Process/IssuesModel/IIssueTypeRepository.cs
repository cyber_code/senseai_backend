﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.Process.IssuesModel
{
    public interface IIssueTypeRepository : IRepository<IssueType>
    {
        void Add(IssueType issueType);

        void Update(IssueType issueType);

        bool Delete(Guid id, bool forceDelete, bool removeDefault);
    }
}