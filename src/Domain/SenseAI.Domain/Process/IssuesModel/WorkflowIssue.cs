﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.IssuesModel
{
    public sealed class WorkflowIssue : BaseEntity
    {
        public WorkflowIssue(Guid workflowId, Guid issueId) : base(Guid.NewGuid())
        {
            WorkflowId = workflowId;
            IssueId = issueId;
        }

        public WorkflowIssue(Guid id, Guid workflowId, Guid issueId) : base(id)
        {
            WorkflowId = workflowId;
            IssueId = issueId;
        }

        public Guid IssueId { get; private set; }

        public Guid WorkflowId { get; private set; }
    }
}