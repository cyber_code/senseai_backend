﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.Process.IssuesModel
{
    public sealed class IssueComment : BaseEntity
    {
        public IssueComment(Guid id, Guid issueId, string comment, Guid commenterId, Guid parentId):base(id)
        {
            IssueId = issueId;
            Comment = comment;
            CommenterId = commenterId;
            ParentId = parentId;
        }
        public IssueComment(Guid issueId, string comment, Guid commenterId, Guid parentId)
        {
            IssueId = issueId;
            Comment = comment;
            CommenterId = commenterId;
            ParentId = parentId;
        }

        public Guid IssueId { get; private set; }
        public string Comment { get; private set; }
        public Guid CommenterId { get; private set; }
        public Guid ParentId { get; private set; }

    }
}
