﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.IssuesModel
{
    public interface IWorkflowIssueRepository : IRepository<Issue>
    {
        void Add(WorkflowIssue workflowIssue);

        void Update(WorkflowIssue workflowIssue);

        void Delete(Guid id);
    }
}