﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.Process.IssuesModel
{
    public sealed class IssueResource : BaseEntity
    {
        public IssueResource(Guid id, Guid issueId, string title, string path): base(id)
        {
            IssueId = issueId;
            Title = title;
            Path = path;
        }
        public IssueResource(Guid issueId, string title, string path)
        {
            IssueId = issueId;
            Title = title;
            Path = path;
        }

        public Guid IssueId { get; private set; }
        public string Title { get; private set; }
        public string Path { get; private set; }

    }
}
