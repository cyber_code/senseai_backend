﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.IssuesModel
{
    public sealed class IssueTypeFields : BaseEntity
    {
        public IssueTypeFields(Guid id) : base(id)
        {

        }

        public IssueTypeFields(Guid id, Guid issueTypeId, Guid issueTypeFieldsNameId, bool check) : base(id)
        {
            IssueTypeId = issueTypeId;
            IssueTypeFieldsNameId = issueTypeFieldsNameId;
            Checked = check;

        }

        public IssueTypeFields(Guid issueTypeId, Guid issueTypeFieldsNameId, bool check) : base(Guid.NewGuid())
        {
            IssueTypeId = issueTypeId;
            IssueTypeFieldsNameId = issueTypeFieldsNameId;
            Checked = check;
           
        }

        public Guid IssueTypeId { get; private set; }
        public Guid IssueTypeFieldsNameId { get; private set; }

        public bool Checked { get; private set; }
    }
}