﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.IssuesModel
{
    public sealed class IssueLink : BaseEntity
    {
        public IssueLink(Guid srcIssueId, IssueLinkType linkType, Guid dstIssueId) : base(Guid.NewGuid())
        {
            SrcIssueId = srcIssueId;
            LinkType = linkType;
            DstIssueId = dstIssueId;
        }

        public Guid SrcIssueId { get; private set; }
        public IssueLinkType LinkType { get; private set; }
        public Guid DstIssueId { get; private set; }
    }
}