﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.Process.IssuesModel
{
    public interface IIssueCommentRepository : IRepository<IssueComment>
    {
        void Add(IssueComment issue);

        void Update(IssueComment issue);

        void Delete(Guid id);
    }
}
