﻿using System;
using SenseAI.Domain.BaseModel;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.PeopleModel;
using SenseAI.Domain.Process.RequirementPrioritiesModel;
using SenseAI.Domain.Process.RequirementTypesModel;

namespace SenseAI.Domain.Process.ProcessModel
{
    public sealed class Process : BaseEntity
    {
        public Guid SubProjectId { get; private set; }
        public Guid HierarchyId { get; private set; }
        public Guid CreatedId { get; private set; }
        public Guid OwnerId { get; private set; }
        public ProcessType ProcessType { get; private set; }
        public Guid RequirementPriorityId { get; private set; }
        public Guid RequirementTypeId { get; private set; }

        public RequirementType Requirement { get; private set; }//For document generation
        public RequirementPriority Priority { get; private set; }//For document generation
        public People Assignee { get; private set; }//For document generation
        public Typical CoreApplication { get; private set; }//For document generation

        public string Title { get; private set; }
        public string Description { get; private set; }
        public int ExpectedNumberOfTestCases { get; private set; }
        public Guid TypicalId { get; private set; }

        public Process(Guid subProjectId,
                       Guid hierarchyId,
                       Guid createdId,
                       Guid ownerId,
                       ProcessType processType,
                       string title,
                       string description,
                       Guid requirementPriorityId,
                       Guid requirementTypeId,
                       int expectedNumberOfTestCases,
                       Guid typicalId)
        {
            SubProjectId = subProjectId;
            HierarchyId = hierarchyId;
            CreatedId = createdId;
            OwnerId = ownerId;
            ProcessType = processType;
            Title = title;
            Description = description;
            RequirementTypeId = requirementTypeId;
            RequirementPriorityId = requirementPriorityId;
            ExpectedNumberOfTestCases = expectedNumberOfTestCases;
            TypicalId = typicalId;
        }

        public Process(Guid id,
                       Guid subProjectId,
                       Guid hierarchyId,
                       Guid createdId,
                       Guid ownerId,
                       ProcessType processType,
                       string title,
                       string description,
                       Guid requirementPriorityId,
                       Guid requirementTypeId,
                       int expectedNumberOfTestCases,
                       Guid typicalId)
            : base(id)
        {
            SubProjectId = subProjectId;
            HierarchyId = hierarchyId;
            CreatedId = createdId;
            OwnerId = ownerId;
            ProcessType = processType;
            Title = title;
            Description = description;
            RequirementTypeId = requirementTypeId;
            RequirementPriorityId = requirementPriorityId;
            ExpectedNumberOfTestCases = expectedNumberOfTestCases;
            TypicalId = typicalId;
        }

        public Process(Guid id,
                      Guid hierarchyId,
                      People owner,
                      ProcessType processType,
                      string title,
                      string description,
                       RequirementType requirementType,
                         RequirementPriority requirementPriority,
                      int expectedNumberOfTestCases,
                      Typical typical)
           : base(id)
        {
            HierarchyId = hierarchyId;
            Assignee = owner;
            ProcessType = processType;
            Title = title;
            Description = description;
            Requirement = requirementType;
            Priority = requirementPriority;
            ExpectedNumberOfTestCases = expectedNumberOfTestCases;
            CoreApplication = typical;
        }
    }
}