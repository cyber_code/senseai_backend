﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.ProcessModel
{
    public sealed class ProcessWorkflow : BaseEntity
    {

        public ProcessWorkflow(Guid id) : base(id)
        {

        }

        public ProcessWorkflow(Guid id, Guid processId, Guid workflowId) : base(id)
        {
            ProcessId = processId;
            WorkflowId = workflowId;
        }

        public ProcessWorkflow(Guid processId, Guid workflowId) : base(Guid.NewGuid())
        {
            ProcessId = processId;
            WorkflowId = workflowId;
        }

        public Guid ProcessId { get; private set; }

        public Guid WorkflowId { get; private set; }
    }
}
