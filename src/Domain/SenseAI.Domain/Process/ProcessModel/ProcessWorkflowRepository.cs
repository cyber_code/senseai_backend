﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.ProcessModel
{
    public interface IProcessWorkflowRepository : IRepository<ProcessWorkflow>
    {
        void Add(ProcessWorkflow process);

        void Update(ProcessWorkflow process);

        void Delete(Guid id); 
    }
}
