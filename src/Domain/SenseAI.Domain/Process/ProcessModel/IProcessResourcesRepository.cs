﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.Process.ProcessModel
{
    public interface IProcessResourcesRepository : IRepository<ProcessResources>
    {
        void Add(ProcessResources process);

        void Update(ProcessResources process);

        void Delete(Guid id); 
    }
}
