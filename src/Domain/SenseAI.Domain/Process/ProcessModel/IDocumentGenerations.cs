﻿using SenseAI.Domain.Adapters;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain.WorkflowModel; 
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.Process.ProcessModel
{
    public interface IDocumentGenerations
    {
        bool Generate(string projectTitle, string subProjectTitle, string path, string apiURL, string uiURL, Dictionary<Hierarchy, List<Hierarchy>> hierarchies, Dictionary<Process, List<Workflow>> processAndWorkflows,
            Dictionary<Guid, List<WorkflowVersionPaths>> workflowPaths, Dictionary<Guid, List<WorkflowTestCases>> workflowTestCases, Dictionary<DataSet, int> dataSets,
            Dictionary<Guid, string> processImages,  List<AdapterSteps> adapterSteps, Dictionary<Guid, string> workflowVideos, Dictionary<Guid, int> processResources);
    }
}
