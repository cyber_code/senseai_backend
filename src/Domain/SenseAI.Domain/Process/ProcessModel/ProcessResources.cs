﻿using Microsoft.AspNetCore.Http;
using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;
namespace SenseAI.Domain.Process.ProcessModel
{
    public sealed class ProcessResources : BaseEntity
    {
        public ProcessResources(string fileName, Guid processId, String filePath)
        {
            FileName = fileName;
            ProcessId = processId;
            FilePath = filePath;
        }
        public ProcessResources(Guid id, string fileName, Guid processId, string filePath) : base (id)
        {
            FileName = fileName;
            ProcessId = processId;
            FilePath = filePath;
        }
        public string FileName { get; private set; }
        public Guid ProcessId { get; private set; }
        public string FilePath { get; private set; }
    }

}
