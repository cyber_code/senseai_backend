﻿using System;
using SenseAI.Domain.BaseModel;

namespace SenseAI.Domain.Process.ProcessModel
{
    public interface IProcessRepository : IRepository<Process>
    {
        void Add(Process process);

        void Update(Process process);

        bool Delete(Guid id, bool forceDelete);

        bool Delete(Guid id);

        void Paste(Process process, PasteType type);
    }
}