﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.RequirementPrioritiesModel
{
    public sealed class RequirementPriority : BaseEntity
    {
        public RequirementPriority(Guid id) : base(id)
        {
        }

        public RequirementPriority(Guid id, Guid subProjectId, string title, string code, int index) : base(id)
        {
            SubProjectId = subProjectId;
            Title = title;
            Code = code;
            Index = index;
        }
        public RequirementPriority(Guid subProjectId, string title, string code, int index) : base(Guid.NewGuid())
        {
            SubProjectId = subProjectId;
            Title = title;
            Code = code;
            Index = index;
        }

        public Guid SubProjectId { get; private set; }
        public string Title { get; private set; }
        public string Code { get; private set; }
        public int Index { get; private set; }
    }
}