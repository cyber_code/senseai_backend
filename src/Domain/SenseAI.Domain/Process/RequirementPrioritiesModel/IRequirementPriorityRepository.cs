﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.RequirementPrioritiesModel
{
    public interface IRequirementPriorityRepository : IRepository<RequirementPriority>
    {
        void Add(RequirementPriority requirementPriority);

        void Update(RequirementPriority requirementPriority);

        void UpdateIndex(RequirementPriority requirementPriority);

        void Delete(Guid id, bool forceDelete);
    }
}