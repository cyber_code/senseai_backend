﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.SprintsModel
{
    public sealed class SprintIssues : BaseEntity
    {
        public SprintIssues(Guid id) :
            base(id)
        { }

        public SprintIssues(Guid sprintId, Guid issueId, int status)
            : this(Guid.NewGuid(), sprintId,issueId,status)
        { }

        public SprintIssues(Guid id, Guid sprintId, Guid issueId, int status)
          : base(id)
        {
            SprintId = sprintId;
            IssueId = issueId;
            Status = status;
        }

        public Guid SprintId  { get; private set; }

        public Guid IssueId { get; private set; }
        public int Status { get; private set; }

    }
}