﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.Process.SprintsModel
{
    public interface ISprintIssuesRepository : IRepository<SprintIssues>
    {
        void Add(SprintIssues sprintIssues);

        void Update(SprintIssues sprintIssues);

        void Delete(Guid id);

    }
}
