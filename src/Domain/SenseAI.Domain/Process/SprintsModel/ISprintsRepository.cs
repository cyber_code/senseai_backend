﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.Process.SprintsModel
{
    public interface ISprintsRepository : IRepository<Sprints>
    {
        void Add(Sprints sprints);

        void Update(Sprints sprints);

        void Delete(Guid id);
        void Start(Guid id);
        void End(Guid id);
    }
}
