﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.SprintsModel
{
    public sealed class Sprints : BaseEntity
    {
        public Sprints(Guid id) :
            base(id)
        { }

        public Sprints(string title, string description, DateTime start, DateTime end, Guid subProjectId, int duration, DurationUnit durationUnit)
            : this(Guid.NewGuid(), title, description, start, end,subProjectId, duration, durationUnit)
        { }

        public Sprints(Guid id, string title, string description, DateTime start, DateTime end, Guid subProjectId, int duration, DurationUnit durationUnit)
          : base(id)
        {
            Title = title;
            Description = description;
            Start = start;
            End = end;
            SubProjectId = subProjectId;
            Duration = duration;
            DurationUnit = durationUnit;
        }

        public string Title { get; private set; }

        public string Description { get; private set; }
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }
        public Guid SubProjectId { get; private set; }
        public int Status { get; private set; }
        public int Duration { get; private set; }
        public DurationUnit DurationUnit { get; private set; }
    }
}