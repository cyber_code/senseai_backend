﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.RequirementTypesModel
{
    public sealed class RequirementType : BaseEntity
    {
        public RequirementType(Guid id) : base(id)
        {
        }

        public RequirementType(Guid id, Guid subProjectId, string title, string code) : base(id)
        {
            SubProjectId = subProjectId;
            Title = title;
            Code = code;
        }
        public RequirementType(Guid subProjectId, string title, string code) : base(Guid.NewGuid())
        {
            SubProjectId = subProjectId;
            Title = title;
            Code = code;
        }

        public Guid SubProjectId { get; private set; }
        public string Title { get; private set; }
        public string Code { get; private set; }
    }
}