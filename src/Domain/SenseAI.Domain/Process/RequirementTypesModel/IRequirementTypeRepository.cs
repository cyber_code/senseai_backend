﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.Process.RequirementTypesModel
{
    public interface IRequirementTypeRepository : IRepository<RequirementType>
    {
        void Add(RequirementType requirementType);

        void Update(RequirementType requirementType);

        void Delete(Guid id, bool forceDelete);
    }
}