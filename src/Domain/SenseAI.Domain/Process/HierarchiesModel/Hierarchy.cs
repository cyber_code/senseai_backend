﻿using System;
using SenseAI.Domain.BaseModel;

namespace SenseAI.Domain.Process.HierarchiesModel
{
    public sealed class Hierarchy : BaseEntity
    {
        public Guid ParentId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public Guid HierarchyTypeId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string HierarchyTypeTitle { get; private set; }

        public Hierarchy(Guid parentId, Guid subProjectId, Guid hierarchyTypeId, string title, string description)
        {
            ParentId = parentId;
            SubProjectId = subProjectId;
            HierarchyTypeId = hierarchyTypeId;
            Title = title;
            Description = description;
        }

        public Hierarchy(Guid id, Guid parentId, Guid subProjectId, Guid hierarchyTypeId, string title, string description) : base(id)
        {
            ParentId = parentId;
            SubProjectId = subProjectId;
            HierarchyTypeId = hierarchyTypeId;
            Title = title;
            Description = description;
        }

        public Hierarchy(Guid id, string title, string description) : base(id)
        {
            Title = title;
            Description = description;
        }

        public Hierarchy(Guid id, string HTTitle, string title, string description) : base(id)
        {
            HierarchyTypeTitle = HTTitle;
            Title = title;
            Description = description;
        }

        public void SetHierarchyTypeId(Guid hierarchyTypeId)
        {
            this.HierarchyTypeId = hierarchyTypeId;
        }

        public void SetParentId(Guid parentId)
        {
            this.ParentId = parentId;
        }

        public void SetHierarchyTypeTitle(string hierarchyTypeTitle)
        {
            this.HierarchyTypeTitle = hierarchyTypeTitle;
        }
    }
}