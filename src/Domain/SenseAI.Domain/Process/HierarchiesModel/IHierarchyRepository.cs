﻿using System;
using SenseAI.Domain.BaseModel;

namespace SenseAI.Domain.Process.HierarchiesModel
{
    public interface IHierarchyRepository : IRepository<Hierarchy>
    {
        void Add(Hierarchy hierarchy);

        void AddFromImport(Hierarchy hierarchy);

        void Update(Hierarchy hierarchy);

        bool Delete(Guid id, bool forceDelete);
        void Move(Guid fromHierarchyId, Guid toHierarchyId);
    }
}