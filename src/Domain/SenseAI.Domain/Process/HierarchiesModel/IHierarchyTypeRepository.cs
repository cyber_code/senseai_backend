﻿using System;
using System.Collections.Generic;
using SenseAI.Domain.BaseModel;

namespace SenseAI.Domain.Process.HierarchiesModel
{
    public interface IHierarchyTypeRepository : IRepository<HierarchyType>
    {
        bool Add(HierarchyType hierarchyType, bool forceDelete, out string parentName);

        void Update(HierarchyType hierarchyType);

        bool Delete(Guid id, bool forceDelete);

        void Move(Guid id);

        List<HierarchyType> GetHierarchyTypeLevels(Guid SubProjectId);

        HierarchyType GetHierarchyTypeById(Guid id);

        bool CheckForHierarchyTypes(Guid SubProjectId, bool forceDelete);
    }
}