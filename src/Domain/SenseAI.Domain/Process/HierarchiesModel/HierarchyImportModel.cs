﻿using System.Collections.Generic;

namespace SenseAI.Domain.Process.HierarchiesModel
{
    public sealed class HierarchyImportModel
    {
        public List<HierarchyTypeImportModelTable> HierarchyTypeImportModelTable { get; set; }

        public List<HierarchyImportModelTable> HierarchyImportModelTabel { get; set; }
    }

    public sealed class HierarchyImportModelTable
    {
        public HierarchyImportModelTable(string field1, string field2, string field3,
            string field4, string field5, string field6, string field7, int level)
        {
            Field1 = field1;
            Field2 = field2;
            Field3 = field3;
            Field4 = field4;
            Field5 = field5;
            Field6 = field6;
            Field7 = field7;
            Level = level;
        }

        public string Field1 { get; set; }

        public string Field2 { get; set; }

        public string Field3 { get; set; }

        public string Field4 { get; set; }

        public string Field5 { get; set; }

        public string Field6 { get; set; }

        public string Field7 { get; set; }

        public int Level { get; set; }
    }

    public sealed class HierarchyTypeImportModelTable
    {
        public HierarchyTypeImportModelTable(string headerField1, string headerField2,
            string headerField3, string headerField4, string headerField5, int headerLevel)
        {
            HeaderField1 = headerField1;
            HeaderField2 = headerField2;
            HeaderField3 = headerField3;
            HeaderField4 = headerField4;
            HeaderField5 = headerField5;
            HeaderLevel = headerLevel;
        }

        public string HeaderField1 { get; set; }

        public string HeaderField2 { get; set; }

        public string HeaderField3 { get; set; }

        public string HeaderField4 { get; set; }

        public string HeaderField5 { get; set; }

        public int HeaderLevel { get; set; }
    }
}