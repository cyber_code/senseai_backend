﻿using System;
using SenseAI.Domain.BaseModel;

namespace SenseAI.Domain.Process.HierarchiesModel
{
    public sealed class HierarchyType : BaseEntity
    {
        public HierarchyType(Guid parentId, Guid subProjectId, string title, string description)
        {
            ParentId = parentId;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
        }

        public HierarchyType(Guid parentId, Guid subProjectId, string title, string description, int level)
        {
            ParentId = parentId;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
            Level = level;
        }

        public HierarchyType(Guid id, Guid parentId, Guid subProjectId, string title, string description) : base(id)
        {
            ParentId = parentId;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
        }

        public HierarchyType(Guid id, Guid parentId, Guid subProjectId, string title, string description, int level) : base(id)
        {
            ParentId = parentId;
            SubProjectId = subProjectId;
            Title = title;
            Description = description;
            Level = level;
        }

        public Guid ParentId { get; private set; }

        public Guid SubProjectId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public int Level { get; private set; }
    }
}