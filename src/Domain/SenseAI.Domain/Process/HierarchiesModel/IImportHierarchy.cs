﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.Process.HierarchiesModel
{
    public interface IImportHierarchy
    {
        HierarchyImportModel ImportHierarchyFromExcelFile(Guid subProjectId, IFormFile File);
    }
}