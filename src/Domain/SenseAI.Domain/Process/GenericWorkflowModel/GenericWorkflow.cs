﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace SenseAI.Domain.Process.GenericWorkflowModel
{
    public sealed class GenericWorkflow : BaseEntity
    {
        private readonly IList<GenericWorkflowItem> _items = new List<GenericWorkflowItem>();        
        private readonly IList<GenericWorkflowItemLink> _itemLinks = new List<GenericWorkflowItemLink>();

        public GenericWorkflow(Guid id) : base(id)
        {
        }

        public GenericWorkflow(Guid id, Guid processId, string title, string description, string designerJson) : base(id)
        {
            ProcessId = processId;
            Title = title;
            Description = description;
            DesignerJson = designerJson;
        }

        public GenericWorkflow(Guid id, Guid processId, string title, string workflowImage) : base(id)
        {
            ProcessId = processId;
            Title = title;
            WorkflowImage = workflowImage;
        }

        public Guid ProcessId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string DesignerJson { get; private set; }

        public string WorkflowImage { get; private set; }


        public IEnumerable<GenericWorkflowItem> Items
        {
            get { return _items.ToImmutableArray(); }
        }

        public IReadOnlyCollection<GenericWorkflowItemLink> ItemLinks
        {
            get { return _itemLinks.ToImmutableArray(); }
        }

        public GenericWorkflowItem AddItem(Guid id, GenericWorkflowType type, string title, string description)
        {
            var item = new GenericWorkflowItem(id, type, title, description);

            _items.Add(item);

            return item;
        }

        public GenericWorkflowItem AddItem(GenericWorkflowItem item)
        {
            _items.Add(item);

            return item;
        }

        public void AddItemLink(Guid fromId, Guid toId, GenericWorkflowItemLinkState state)
        {
            _itemLinks.Add(new GenericWorkflowItemLink(fromId, toId, state));
        }

    }
}
