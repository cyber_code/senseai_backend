﻿using System;

namespace SenseAI.Domain.Process.GenericWorkflowModel
{
    public sealed class GenericWorkflowItemLink
    {
        public GenericWorkflowItemLink(Guid fromId, Guid toId, GenericWorkflowItemLinkState state)
        {
            FromId = fromId;
            ToId = toId;
            State = state;
        }

        public Guid FromId { get; private set; }

        public Guid ToId { get; private set; }

        public GenericWorkflowItemLinkState State { get; private set; }
    }
}
