﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.GenericWorkflowModel
{
    public interface IGenericWorkflowRepository : IRepository<GenericWorkflow>
    {
        void Add(GenericWorkflow GenericWorkflow);

        void Save(GenericWorkflow GenericWorkflow, string designerJson);

        void Delete(Guid id);

        void Issue(Guid id, string genericWorkflowImage);

        void Update(Guid id);

        void Rename(Guid id, string title);

        string RollbackGenericWorkflow(Guid genericWorkflowId, int versionId);

        GenericWorkflow GetGenericWorkflowByVersion(Guid genericWork, int versionId);

        int GetNrVersions(Guid genericWorkflowId);

        GenericWorkflow GetGenericWorkflow(Guid id);

        string GetGenericWorkflowImage(Guid genericWorkflowId, int versionId);
    }
}
