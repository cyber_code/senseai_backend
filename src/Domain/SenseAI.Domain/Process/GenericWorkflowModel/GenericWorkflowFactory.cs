﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace SenseAI.Domain.Process.GenericWorkflowModel
{
    public sealed class GenericWorkflowFactory
    {
        public static GenericWorkflow Create(Guid id, string designerJson, Guid processId, string title = "", string description = "")
        {
            var json = GetParsedJson(designerJson);
            var genericWorkflowDesignerData = JsonConvert.DeserializeObject<GenericWorkflowDesignerData>(json.ToString());

            var arisWorkflow = Create(id, genericWorkflowDesignerData, processId, title, description);

            return arisWorkflow;
        }

        private static GenericWorkflow Create(Guid id, GenericWorkflowDesignerData arisWorkflowDesignerData, Guid processId, string title, string description)
        {
            GenericWorkflow genericWorkflow = new GenericWorkflow(id, processId, title, description, "");
            if (arisWorkflowDesignerData.Cells == null)
                return genericWorkflow;

            foreach (var cell in arisWorkflowDesignerData.Cells)
            {
                if (cell.Type.ToLowerInvariant() == "link" || cell.Type.ToLowerInvariant() == "app.link" || cell.Type.ToLowerInvariant() == "standard.link")
                {
                    genericWorkflow.AddItemLink(cell.source.id, cell.target.id, cell.State);
                }
                else
                    genericWorkflow.AddItem(cell.CustomData.Id, cell.CustomData.Type, cell.CustomData.Title, cell.CustomData.Description);
            }
            return genericWorkflow;
        }

        private static JObject GetParsedJson(string json)
        {
            try
            {
                return JObject.Parse(json);
            }
            catch (Exception)
            {
                throw new Exception("Json could not parsed.");
            } 
        }

    }
}
