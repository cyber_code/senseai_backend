﻿using System;

namespace SenseAI.Domain.Process.GenericWorkflowModel
{
    public sealed class GenericWorkflowDesignerData
    {
        public Cell[] Cells { get; set; }
    }

    public sealed class Cell
    {
        public GenericWorkflowDesignerCellItem CustomData { get; set; }

        public Source source { get; set; }

        public Target target { get; set; }

        public string Type { get; set; }

        public GenericWorkflowItemLinkState State { get; set; }
    }

    public class Source
    {
        public Guid id { get; set; }
    }

    public class Target
    {
        public Guid id { get; set; }
    }

    public class GenericWorkflowDesignerCellItem
    {
        public GenericWorkflowType Type { get; set; }

        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}
