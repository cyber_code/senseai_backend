﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.GenericWorkflowModel
{
    public sealed class GenericWorkflowItem : BaseEntity
    {
        public GenericWorkflowItem(Guid id, GenericWorkflowType type, string title, string description) : base(id)
        {
            Type = type;
            Title = title;
            Description = description;
        }

        public GenericWorkflowType Type { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }


    }
}
