﻿namespace SenseAI.Domain.Process.GenericWorkflowModel
{
    public enum GenericWorkflowType
    {
        Start,
        Activity, 
        Conditional, 
    }
}
