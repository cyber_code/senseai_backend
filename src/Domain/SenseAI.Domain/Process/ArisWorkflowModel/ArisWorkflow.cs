﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace SenseAI.Domain.Process.ArisWorkflowModel
{
    public sealed class ArisWorkflow : BaseEntity
    {
        private readonly IList<ArisWorkflowItem> _items = new List<ArisWorkflowItem>();
        private readonly IList<ArisWorkflowItemLink> _itemLinks = new List<ArisWorkflowItemLink>();

        public ArisWorkflow(Guid id) : base(id)
        {
        }

        public ArisWorkflow(Guid id, Guid processId, string title, string description, string designerJson, string moduleId = "", bool isParsed = true) : base(id)
        {
            ProcessId = processId;
            Title = title;
            Description = description;
            DesignerJson = designerJson;
            ModuleId = moduleId;
            IsParsed = isParsed;
        }

        public Guid ProcessId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string DesignerJson { get; private set; }

        public string ModuleId { get; private set; }

        public bool IsParsed { get; private set; }

        public IEnumerable<ArisWorkflowItem> Items
        {
            get { return _items.ToImmutableArray(); }
        }

        public IReadOnlyCollection<ArisWorkflowItemLink> ItemLinks
        {
            get { return _itemLinks.ToImmutableArray(); }
        }

        public ArisWorkflowItem AddItem(Guid id, ArisWorkflowType type, string title, string description, string typicalName, Guid arisWorkflowLinkId, Guid arisSourceOccurenceId)
        {
            var item = new ArisWorkflowItem(id, type, title, description, typicalName, arisWorkflowLinkId);

            _items.Add(item);

            return item;
        }

        public ArisWorkflowItem AddItem(ArisWorkflowItem item)
        {
            _items.Add(item);

            return item;
        }

        public void RemoveItems(ArisWorkflowItem[] items)
        {
            foreach (var item in items)
            {
                _items.Remove(item);
            }

        }

        public void AddItemLink(Guid fromId, Guid toId, ArisWorkflowItemLinkState state)
        {
            _itemLinks.Add(new ArisWorkflowItemLink(fromId, toId, state));
        }

        public void RemoveItemLink(Guid fromId, Guid toId)
        {
            var item = _itemLinks.FirstOrDefault(w => w.FromId == fromId && w.ToId == toId);
            _itemLinks.Remove(item);
        }

        public void RemoveItemLinks(ArisWorkflowItemLink[] items)
        {
            foreach (var item in items)
            {
                _itemLinks.Remove(item);
            }

        }

        public void SetIsParsed(bool isParsed)
        {
            IsParsed = isParsed;
        }

        public void SetProcessId(Guid processId)
        {
            ProcessId = processId;
        }

        public void NormalizeCoordinatesOfWorkItems()
        {
            float minX = float.MaxValue;
            float minY = float.MaxValue;

            foreach (ArisWorkflowItem item in this.Items)
            {
                minX = Math.Min(minX, item.X);
                minY = Math.Min(minY, item.Y);
            }

            if (minX == float.MaxValue)
                minX = 0;

            if (minY == float.MaxValue)
                minY = 0;

            minX -= 10;
            minY -= 10;

            foreach (ArisWorkflowItem item in this.Items)
            {
                item.AdjustPosition(minX, minY);
            }

        }

        public void OrderByCordinateY()
        {
            var items2Order = this.Items.ToList();
            this._items.Clear();
            items2Order = items2Order.OrderBy(o => o.Y).ThenBy(o => o.X).ToList();

            items2Order.ForEach(e =>
            {
                this._items.Add(e);
            });
        }

        public void GroupRoles()
        {
            var activities = this.Items.Where(w => w.Type == ArisWorkflowType.Activity);
            foreach (ArisWorkflowItem activity in activities)
            {
                var linksTo = this.ItemLinks.Where(w => w.ToId == activity.Id);
                var roles = this.Items.Where(w => linksTo.Select(s => s.FromId).Contains(w.Id)).ToList();
                roles = roles.Where(w => w.Type == ArisWorkflowType.Role).ToList();

                if (roles.Count() == 0 || roles.Count() == 1)
                    continue;

                string title = string.Join("; ", roles.Select(s => s.Title).ToArray());
                string description = string.Join("; ", roles.Select(s => s.Description).ToArray());
                var role = roles.FirstOrDefault();

                ArisWorkflowItem groupedRole = new ArisWorkflowItem(Guid.NewGuid(), ArisWorkflowType.Role, title, description, "", Guid.Empty);
                var roleLinks = linksTo.Where(w => roles.Select(s => s.Id).Contains(w.FromId));

                this.RemoveItems(roles.ToArray());
                this.RemoveItemLinks(roleLinks.ToArray());

                groupedRole.SetPosition(role.X, role.Y);
                this.AddItem(groupedRole);
                this.AddItemLink(groupedRole.Id, activity.Id, SenseAI.Domain.ArisWorkflowItemLinkState.No);
            }

        }
    }
}
