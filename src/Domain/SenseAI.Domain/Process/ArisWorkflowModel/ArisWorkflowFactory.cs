﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace SenseAI.Domain.Process.ArisWorkflowModel
{
    public sealed class ArisWorkflowFactory
    {
        public static ArisWorkflow Create(Guid id, string designerJson, Guid processId, string title = "", string description = "")
        {
            var json = GetParsedJson(designerJson);
            var workflowDesignerData = JsonConvert.DeserializeObject<ArisWorkflowDesignerData>(json.ToString());

            var arisWorkflow = Create(id, workflowDesignerData, processId, title, description);

            return arisWorkflow;
        }

        private static ArisWorkflow Create(Guid id, ArisWorkflowDesignerData arisWorkflowDesignerData, Guid processId, string title, string description)
        {
            var arisWorkflow = new ArisWorkflow(id, processId, title, description, "");
            if (arisWorkflowDesignerData.Cells == null)
                return arisWorkflow;
            foreach (var cell in arisWorkflowDesignerData.Cells)
            {
                if (cell.Type.ToLowerInvariant() == "link" || cell.Type.ToLowerInvariant() == "app.link" || cell.Type.ToLowerInvariant() == "standard.link")
                {
                    arisWorkflow.AddItemLink(cell.source.id, cell.target.id, cell.State);
                }
                else
                    arisWorkflow.AddItem(cell.CustomData.Id, cell.CustomData.Type, cell.CustomData.Title, cell.CustomData.Description, cell.CustomData.TypicalName, cell.CustomData.ArisWorkflowLinkId, Guid.Empty);
            }
            return arisWorkflow;
        }

        private static JObject GetParsedJson(string json)
        {
            try
            {
                return JObject.Parse(json);
            }
            catch (Exception)
            {
                throw new Exception("Json could not parsed.");
            }
        }

    }
}
