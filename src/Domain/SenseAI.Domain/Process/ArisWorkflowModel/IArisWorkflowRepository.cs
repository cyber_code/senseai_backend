﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.Process.ArisWorkflowModel
{
    public interface IArisWorkflowRepository : IRepository<ArisWorkflow>
    {
        void Add(ArisWorkflow arisWorkflow);

        void Save(ArisWorkflow arisWorkflow, string designerJson);

        void Delete(Guid id);

        void Issue(Guid id, string arisWorkflowImage);

        void Update(Guid id);

        void Rename(Guid id, string title);

        string RollbackWorkflow(Guid arisWorkflowId, int versionId);

        ArisWorkflow GetArisWorkflowByVersion(Guid arisWorkflowId, int versionId);

        //ArisWorkflow GetLastVersion(Guid workflowId);

        int GetNrVersions(Guid arisWorkflowId);

        ArisWorkflow GetArisWorkflow(Guid id);

        string GetArisWorkflowImage(Guid arisWorkflowId, int versionId);
    }
}
