﻿using SenseAI.Domain.DataModel;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.IO;

namespace SenseAI.Domain.Process.ArisWorkflowModel
{
    public interface IImportArisWorkflow
    {
        List<ArisWorkflow> XmlToArisWorkflow(string fileContent);

        List<Workflow> XmlToWorkflow(string fileContent, Guid folderId, Guid systemId, Guid catalogId, string catalogTitle, List<Typical> typicls);

        Workflow ArisWorkflowToWorkflow(ArisWorkflow arisWorkflow, Guid folderId, Guid systemId, Guid catalogId, string catalogTitle, List<Typical> typicls);
    }
}
