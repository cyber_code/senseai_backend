﻿namespace SenseAI.Domain.Process.ArisWorkflowModel
{
    public enum ArisWorkflowType
    {
        Activity,
        Role,
        T24Screen,
        SystemType,
        Resource,
        Conditional,
        Event,
        Process
    }
}
