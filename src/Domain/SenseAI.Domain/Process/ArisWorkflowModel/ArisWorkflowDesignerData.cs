﻿using System;

namespace SenseAI.Domain.Process.ArisWorkflowModel
{
    public sealed class ArisWorkflowDesignerData
    {
        public Cell[] Cells { get; set; }
    }

    public sealed class Cell
    {
        public ArisWorkflowDesignerCellItem CustomData { get; set; }

        public Source source { get; set; }

        public Target target { get; set; }

        public string Type { get; set; }

        public ArisWorkflowItemLinkState State { get; set; }
    }

    public class Source
    {
        public Guid id { get; set; }
    }

    public class Target
    {
        public Guid id { get; set; }
    }

    public class ArisWorkflowDesignerCellItem
    {
        public ArisWorkflowType Type { get; set; }

        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string TypicalName { get; set; }

        public Guid ArisWorkflowLinkId { get; set; }
    }
}
