﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Drawing;

namespace SenseAI.Domain.Process.ArisWorkflowModel
{
    public sealed class ArisWorkflowItem : BaseEntity
    {
        public ArisWorkflowItem(Guid id, ArisWorkflowType type, string title, string description, string typicalName, Guid arisWorkflowLinkId, string arisSourceOccurenceId = "") : base(id)
        {
            Type = type;
            Title = title;
            Description = description;
            TypicalName = typicalName;
            ArisWorkflowLinkId = arisWorkflowLinkId;
            ArisSourceOccurenceId = arisSourceOccurenceId;
        }

        public ArisWorkflowType Type { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string TypicalName { get; private set; }

        public float X { get; private set; }

        public float Y { get; private set; }

        public Guid ArisWorkflowLinkId{ get; private set; }

        public void SetPosition(float x, float y)
        {
            X = x;
            Y = y;
        }

        public void AdjustPosition(float x, float y)
        {
            X -= x;
            Y -= y;
        }

        public string ArisSourceOccurenceId { get; private set; }

        public string ModuleId { get; private set; }

        public void SetModuleId(string moduleId)
        {
            ModuleId = moduleId;
        }

        public void SetArisWorkflowLinkId(Guid arisWorkflowLinkId)
        {
            ArisWorkflowLinkId = arisWorkflowLinkId;
        }
    }
}
