﻿using System;

namespace SenseAI.Domain.Process.ArisWorkflowModel
{
    public sealed class ArisWorkflowItemLink
    {
        public ArisWorkflowItemLink(Guid fromId, Guid toId, ArisWorkflowItemLinkState state)
        {
            FromId = fromId;
            ToId = toId;
            State = state;
        }

        public Guid FromId { get; private set; }

        public Guid ToId { get; private set; }

        public ArisWorkflowItemLinkState State { get; private set; }
    }
}
