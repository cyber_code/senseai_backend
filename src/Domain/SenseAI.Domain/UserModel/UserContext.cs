﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.UserModel
{
    public sealed class UserContext : BaseEntity
    {
        public UserContext(Guid tenantId, int userId, string fullName, int subProjectId, string subProjectName, int projectId, string projectName, string email, string phone)
        {
            TenantId = tenantId;
            UserId = userId;
            FullName = fullName;
            SubProjectId = subProjectId;
            SubProjectName = subProjectName;
            ProjectId = projectId;
            ProjectName = projectName;
            Email = email;
            Phone = phone;
        }

        public Guid TenantId { get; private set; }

        public int UserId { get; private set; }

        public string FullName { get; private set; }

        public int SubProjectId { get; private set; }

        public string SubProjectName { get; private set; }

        public int ProjectId { get; private set; }

        public string ProjectName { get; private set; }

        public string Email { get; private set; }

        public string Phone { get; private set; }
    }
}