﻿using System;

namespace SenseAI.Domain.UserModel
{
    public interface IUserContextRepository
    {
        void Add(UserContext userContext);

        void Update(UserContext userContext);

        void Delete(Guid id);
    }
}