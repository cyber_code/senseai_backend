﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.WorkflowModel
{
    public interface IWorkflowRepository : IRepository<Workflow>
    {
        void Add(Workflow workflow);

        void Save(Workflow workflow, string designerJson, string workflowPathsJson);

        void Rename(Guid workflowId, string title);

        void Update(Guid workflowId, string json, bool markAsInvalid);

        void Update(Guid id);

        void UpdateStatus(Guid id, WorkflowStatus status);

        void SetIsIssued(Guid id);

        void SetIsTestCasegenerated(Guid id);

        void Delete(Guid id);

        Workflow Paste(Guid workflowId, Guid folderId);

        void Cut(Guid workflowId, Guid folderId);

        void Issue(Guid id, string workflowImage);

        string RollbackWorkflow(Guid workflowId, int versionId);

        /*This Get and Update I have added just for simulation of test cases generation*/

        Workflow GetWorkflowByVersion(Guid workflowId, int versionId);

        int GetNrVersions(Guid workflowId);

        Workflow GetLastVersion(Guid id);

        void Update(Guid WorkflowId, int VersionId);

        string GetWorkflowImage(Guid workflowId, int versionId);

        bool IsWorkflowLinked(Guid workflowId);

        int GetWorkflowStatus(Guid id);

        Workflow GetWorkflowDetails(Guid workflowId);
         
        void UpgradeLinkedWorkflowVersion(Guid workflowId, int currentVersionId, int newVersionId);
    }
}