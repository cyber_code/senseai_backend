﻿using System.Collections.Generic;

namespace SenseAI.Domain.WorkflowModel
{
    public interface IGeneratePaths
    {
        List<Path> Generate(Workflow workflow);
    }
}