﻿namespace SenseAI.Domain.WorkflowModel
{
    public sealed class WorkflowItemConstraint
    {
        public WorkflowItemConstraint(string attributeName, string @operator, string attributeValue, WorkflowItemDynamicData dynamicData, bool isPostFilter)
        {
            AttributeName = attributeName;
            Operator = @operator;
            AttributeValue = attributeValue;
            DynamicData = dynamicData;
            IsPostFilter = isPostFilter;
        }

        public string AttributeName { get; private set; }

        public string Operator { get; private set; }

        public string AttributeValue { get; private set; }

        public WorkflowItemDynamicData DynamicData { get; private set; }

        public bool IsPostFilter { get; private set; }
    }
}