﻿using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class WorkflowItemDynamicData
    {
        public WorkflowItemDynamicData(Guid sourceWorkflowItemId, string sourceAttributeName, string targetAttributeName, byte[] plainTextFormula, string path, short? uiMode, string pathName)
        {
            SourceWorkflowItemId = sourceWorkflowItemId;
            SourceAttributeName = sourceAttributeName;
            TargetAttributeName = targetAttributeName;
            PlainTextFormula = plainTextFormula;
            Path = path;
            UiMode = uiMode;
            PathName = pathName;
        }

        public Guid SourceWorkflowItemId { get; private set; }

        public string SourceAttributeName { get; private set; }

        public string TargetAttributeName { get; private set; }

        public byte[] PlainTextFormula { get; private set; }

        public string Path { get;  set; }

        public short? UiMode { get; private set; }

        public string PathName { get; private set; }
    }
}