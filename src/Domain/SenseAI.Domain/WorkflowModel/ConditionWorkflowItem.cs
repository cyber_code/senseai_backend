﻿using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class ConditionWorkflowItem : WorkflowItem
    {
        internal ConditionWorkflowItem(
            Guid id, string title, string description,
            Guid catalogId, string catalogName, Guid typicalId, string typicalName, string typicalType, int actionType, string parameters,
             WorkflowItemConstraint[] constraints
        )
            : base(id, title, description, catalogId, catalogName, typicalId, typicalName, typicalType, actionType, parameters)
        {
            Constraints = constraints;
        }

        public WorkflowItemConstraint[] Constraints { get; private set; }

        public string Type { get { return ((int)WorkflowType.Condition).ToString(); } }
    }
}