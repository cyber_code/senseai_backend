﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public abstract class WorkflowItem : BaseEntity
    {
        protected WorkflowItem(
            Guid id, string title, string description,
            Guid catalogId, string catalogName, Guid typicalId, string typicalName,
            string typicalType, int actionType, string parameters

        )
            : base(id)
        {
            Title = title;
            Description = description;
            CatalogId = catalogId;
            CatalogName = catalogName;
            TypicalId = typicalId;
            TypicalName = typicalName;
            TypicalType = typicalType;
            ActionType = actionType;
            Parameters = parameters;
        }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid CatalogId { get; private set; }

        public string CatalogName { get; private set; }

        public Guid TypicalId { get; private set; }

        public string TypicalName { get; private set; }

        public string TypicalType { get; private set; }

        public int ActionType { get; private set; }

        public string Parameters { get; private set; }

        public Guid ParenID { get; private set; }

        public string UniqueId { get; private set; }

        public bool IsLastItem { get; private set; }

        public void SetTitle(string title)
        {
            this.Title = title;
        }

        public void SetIsLastItem(bool isLastItem)
        {
            this.IsLastItem = isLastItem;
        }

        public void SetParameters(string parameters)
        {
            this.Parameters = parameters;
        }

        public void GetCatalogName(string catalogName)
        {
            this.CatalogName = catalogName;
        }

        public void SetParenID(Guid parenID)
        {
            this.ParenID = parenID;
        }

        public void SetUniqueId(string uniqueId)
        {
            this.UniqueId = uniqueId;
        }
    }
}