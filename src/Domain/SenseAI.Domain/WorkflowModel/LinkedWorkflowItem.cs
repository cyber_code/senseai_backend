﻿using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class LinkedWorkflowItem : WorkflowItem
    {
        internal LinkedWorkflowItem(
            Guid id, string title, string description,
            Guid catalogId, string catalogName, Guid typicalId, string typicalName, string typicalType, int actionType, string parameters,
             Guid workflowId, int workflowVersionId
        )
            : base(id, title, description, catalogId, catalogName, typicalId, typicalName, typicalType, actionType, parameters)
        {
            WorkflowId = workflowId;
            WorkflowVersionId = workflowVersionId;
        }

        public Guid WorkflowId { get; private set; }

        public int WorkflowVersionId { get; private set; }

        public string Type { get { return ((int)WorkflowType.Linked).ToString(); } }
    }
}