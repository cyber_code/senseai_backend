﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class WorkflowFactory
    {
        public static Workflow Create(Guid id, string title, string description, string designerJson)
        {
            var json = JObject.Parse(designerJson);
            var workflowDesignerData = JsonConvert.DeserializeObject<WorkflowDesignerData>(json.ToString());

            var workflow = Create(id, workflowDesignerData, title, description);

            return workflow;
        }

        public static Workflow Create(Guid id, string designerJson)
        {
            var json = JObject.Parse(designerJson);
            var workflowDesignerData = JsonConvert.DeserializeObject<WorkflowDesignerData>(json.ToString());

            var workflow = Create(id, workflowDesignerData);

            return workflow;
        }

        private static Workflow Create(Guid id, WorkflowDesignerData workflowDesignerData, string title = "", string description = "")
        {
            var workflow = new Workflow(id, title, description, Guid.Empty, true, true,false , false);

            foreach (var cell in workflowDesignerData.Cells)
            {
                if (cell.Type.ToLowerInvariant() == "link" || cell.Type.ToLowerInvariant() == "app.link" || cell.Type.ToLowerInvariant() == "standard.link")
                {
                    workflow.AddItemLink(cell.Source.Id, cell.Target.Id, cell.State);
                }
                else
                {
                    var item = cell.CustomData;

                    switch (item?.Type)
                    {
                        case WorkflowType.Start:
                            workflow.AddStartItem(
                                item.Id,
                                item.SystemId,
                                item.Title ?? string.Empty,
                                item.Description ?? string.Empty,
                                item.CatalogId,
                                item.AdapterName,
                                item.CatalogName,
                                item.TypicalId,
                                item.TypicalName,
                                item.TypicalType,
                                item.ActionType,
                                item.Parameters
                            );
                            break;

                        case WorkflowType.Action:
                            workflow.AddActionItem(
                                item.Id,
                                item.Title,
                                item.Description,
                                item.CatalogId,
                                item.CatalogName,
                                item.TypicalId,
                                item.TypicalName,
                                item.TypicalType,
                                item.ActionType,
                                item.Parameters,
                               item.Constraints == null ? new WorkflowItemConstraint[0] : item.Constraints.Select(
                                    constraint => new WorkflowItemConstraint(constraint.AttributeName, constraint.Operator,
                                        constraint.AttributeValue, constraint.DynamicData != null ?
                                        new WorkflowItemDynamicData(constraint.DynamicData.SourceWorkflowItemId, constraint.DynamicData.SourceAttributeName,
                                        constraint.DynamicData.TargetAttributeName, constraint.DynamicData.PlainTextFormula, constraint.DynamicData.Path, constraint.DynamicData.UiMode, constraint.DynamicData.PathName) : null, constraint.IsPostFilter)
                                ).ToArray(),
                                 item.PostConstraints == null ? new WorkflowItemConstraint[0] : item.PostConstraints.Select(
                                    constraint => new WorkflowItemConstraint(constraint.AttributeName, constraint.Operator, constraint.AttributeValue,
                                     constraint.DynamicData != null ?
                                        new WorkflowItemDynamicData(constraint.DynamicData.SourceWorkflowItemId, constraint.DynamicData.SourceAttributeName,
                                        constraint.DynamicData.TargetAttributeName, constraint.DynamicData.PlainTextFormula, constraint.DynamicData.Path, constraint.DynamicData.UiMode, constraint.DynamicData.PathName) : null, constraint.IsPostFilter)
                                ).ToArray(),
                               item.Constraints == null ? new WorkflowItemDynamicData[0] : item.DynamicDatas.Select(
                                    dynamicDatas => new WorkflowItemDynamicData(dynamicDatas.SourceWorkflowItemId,
                                    dynamicDatas.SourceAttributeName, dynamicDatas.TargetAttributeName, dynamicDatas.PlainTextFormula, dynamicDatas.Path, dynamicDatas.UiMode, dynamicDatas.PathName)
                                ).ToArray(),
                                item.IsNegativeStep,
                                item.ExpectedError,
                                item.IsTitleChanged
                            );
                            break;

                        case WorkflowType.Condition:
                            workflow.AddConditionItem(
                                item.Id,
                                item.Title,
                                item.Description,
                                item.CatalogId,
                                item.CatalogName,
                                item.TypicalId,
                                item.TypicalName,
                                item.TypicalType,
                                item.ActionType,
                                item.Parameters,
                                item.Constraints == null ? new WorkflowItemConstraint[0] : item.Constraints.Select(
                                    constraint => new WorkflowItemConstraint(constraint.AttributeName, constraint.Operator,
                                        constraint.AttributeName, null, constraint.IsPostFilter)
                                ).ToArray()
                            );
                            break;

                        case WorkflowType.Linked:
                            workflow.AddLinkedItem(
                                item.Id,
                                item.Title,
                                item.Description,
                                item.CatalogId,
                                item.CatalogName,
                                item.TypicalId,
                                item.TypicalName,
                                item.TypicalType,
                                item.ActionType,
                                item.Parameters,
                                item.WorkflowId,
                                item.VersionId
                            );
                            break;
                        case WorkflowType.Manual:
                            workflow.AddManualItem(
                                item.Id,
                                item.Title,
                                item.Description,
                                item.CatalogId,
                                item.CatalogName,
                                item.TypicalId,
                                item.TypicalName,
                                item.TypicalType,
                                item.ActionType,
                                item.Action,
                                ""
                            );
                            break;
                    }
                }
            }

            return workflow;
        }

        public static Workflow TECreate(Guid id, string designerJson)
        {
            var json = JObject.Parse(designerJson);
            var workflowDesignerData = JsonConvert.DeserializeObject<TEWorkflowDesignerData>(json.ToString());

            var workflow = Create(id, workflowDesignerData);

            return workflow;
        }

        private static Workflow Create(Guid id, TEWorkflowDesignerData workflowDesignerData)
        {
            var workflow = new Workflow(id, workflowDesignerData.Title, workflowDesignerData.Description, workflowDesignerData.FolderId, true, true);

            foreach (var item in workflowDesignerData.Items)
            {
                switch (item?.Type)
                {
                    case WorkflowType.Start:
                        workflow.AddStartItem(
                            item.Id,
                            item.SystemId,
                            item.Title ?? string.Empty,
                            item.Description ?? string.Empty,
                            item.CatalogId,
                            item.AdapterName,
                            item.CatalogName,
                            item.TypicalId,
                            item.TypicalName,
                            item.TypicalType,
                            item.ActionType,
                            item.Parameters
                        );
                        break;

                    case WorkflowType.Action:
                        workflow.AddActionItem(
                            item.Id,
                            item.Title,
                            item.Description,
                            item.CatalogId,
                            item.CatalogName,
                            item.TypicalId,
                            item.TypicalName,
                            item.TypicalType,
                            item.ActionType,
                            item.Parameters,
                           item.Constraints == null ? new WorkflowItemConstraint[0] : item.Constraints.Select(
                                constraint => new WorkflowItemConstraint(constraint.AttributeName, constraint.Operator,
                                    constraint.AttributeValue, constraint.DynamicData != null ?
                                    new WorkflowItemDynamicData(constraint.DynamicData.SourceWorkflowItemId, constraint.DynamicData.SourceAttributeName,
                                    constraint.DynamicData.TargetAttributeName, constraint.DynamicData.PlainTextFormula, constraint.DynamicData.Path, constraint.DynamicData.UiMode, constraint.DynamicData.PathName) : null, constraint.IsPostFilter)
                            ).ToArray(),
                             item.PostConstraints == null ? new WorkflowItemConstraint[0] : item.PostConstraints.Select(
                                constraint => new WorkflowItemConstraint(constraint.AttributeName, constraint.Operator, constraint.AttributeValue,
                                 constraint.DynamicData != null ?
                                    new WorkflowItemDynamicData(constraint.DynamicData.SourceWorkflowItemId, constraint.DynamicData.SourceAttributeName,
                                    constraint.DynamicData.TargetAttributeName, constraint.DynamicData.PlainTextFormula, constraint.DynamicData.Path, constraint.DynamicData.UiMode, constraint.DynamicData.PathName) : null, constraint.IsPostFilter)
                            ).ToArray(),
                           item.Constraints == null ? new WorkflowItemDynamicData[0] : item.DynamicDatas.Select(
                                dynamicDatas => new WorkflowItemDynamicData(dynamicDatas.SourceWorkflowItemId,
                                dynamicDatas.SourceAttributeName, dynamicDatas.TargetAttributeName, dynamicDatas.PlainTextFormula, dynamicDatas.Path, dynamicDatas.UiMode, dynamicDatas.PathName)
                            ).ToArray(),
                            item.IsNegativeStep,
                            item.ExpectedError
                        );
                        break;

                    case WorkflowType.Condition:
                        workflow.AddConditionItem(
                            item.Id,
                            item.Title,
                            item.Description,
                            item.CatalogId,
                            item.CatalogName,
                            item.TypicalId,
                            item.TypicalName,
                            item.TypicalType,
                            item.ActionType,
                            item.Parameters,
                            item.Constraints == null ? new WorkflowItemConstraint[0] : item.Constraints.Select(
                                constraint => new WorkflowItemConstraint(constraint.AttributeName, constraint.Operator,
                                    constraint.AttributeName, null, constraint.IsPostFilter)
                            ).ToArray()
                        );
                        break;

                    case WorkflowType.Linked:
                        workflow.AddLinkedItem(
                            item.Id,
                            item.Title,
                            item.Description,
                            item.CatalogId,
                            item.CatalogName,
                            item.TypicalId,
                            item.TypicalName,
                            item.TypicalType,
                            item.ActionType,
                            item.Parameters,
                            item.WorkflowId,
                            item.VersionId
                        );
                        break;
                    case WorkflowType.Manual:
                        workflow.AddManualItem(
                            item.Id,
                            item.Title,
                            item.Description,
                            item.CatalogId,
                            item.CatalogName,
                            item.TypicalId,
                            item.TypicalName,
                            item.TypicalType,
                            item.ActionType,
                            item.Action,
                            ""
                        );
                        break;
                }
            }

            foreach (var itemLink in workflowDesignerData.ItemLinks)
            {
                workflow.AddItemLink(itemLink.FromId, itemLink.ToId, WorkflowItemLinkState.Yes);
            }
            return workflow;
        }

    }
}