﻿using System;

namespace SenseAI.Domain.WorkflowModel
{
    public enum WorkflowItemLinkState
    {
        None,
        Yes,
        No
    }

    public sealed class WorkflowItemLink
    {
        public WorkflowItemLink(Guid fromId, Guid toId, WorkflowItemLinkState state)
        {
            FromId = fromId;
            ToId = toId;
            State = state;
        }

        public Guid FromId { get; private set; }

        public Guid ToId { get; private set; }

        public WorkflowItemLinkState State { get; private set; }

        public Guid ParenID { get; private set; }

        public string UniqueId { get; private set; }

        public void SetParenID(Guid parenID)
        {
            this.ParenID = parenID;
        }

        public void SetFromId(Guid fromId)
        {
            this.FromId = fromId;
        }

        public void SetToId(Guid toId)
        {
            this.ToId = toId;
        }

        public void SetUniqueId(string uniqueId)
        {
            this.UniqueId = uniqueId;
        }
    }
}