﻿using System.Collections.Generic;

namespace SenseAI.Domain.WorkflowModel
{
    public interface IWorkflowPathsGenerator
    {
        List<WorkflowPathDesinger> Generate();
    }
}