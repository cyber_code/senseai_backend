﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace SenseAI.Domain.WorkflowModel
{
    public class WorkflowVersionPaths : BaseEntity
    {
        private readonly IList<WorkflowVersionPathDetails> _items = new List<WorkflowVersionPathDetails>();

        public WorkflowVersionPaths(Guid id, Guid workflowId, int versionId, string title, bool selected, decimal coverage, int index, bool isLast) : base(id)
        {
            WorkflowId = workflowId;
            VersionId = versionId;
            Title = title;
            Selected = selected;
            Coverage = coverage;
            Index = index;
            IsLast = isLast;
        }

        public Guid WorkflowId { get; private set; }

        public int VersionId { get; private set; }

        public string Title { get; private set; }

        public bool Selected { get; private set; }

        public decimal Coverage { get; private set; }

        public int Index { get; private set; }

        public bool IsLast { get; private set; }

        public IEnumerable<WorkflowVersionPathDetails> Items
        {
            get { return _items.ToImmutableArray(); }
        }

        public void AddItem(Guid pathItemId, Guid workflowId, int versionId, Guid pathId, string title, int type, int actionType,
            Guid catalogId, Guid typicalId, string typicalName, string typicalType, Guid dataSetId, string pathItemJson, int index)
        {
            var item = new WorkflowVersionPathDetails(pathItemId, workflowId, versionId, pathId, title, type, actionType, catalogId, typicalId,
                typicalName, typicalType, dataSetId, pathItemJson, index);

            _items.Add(item);
        }

        public void AddItems(WorkflowVersionPathDetails [] items)
        {
            foreach (var item in items)
            {
                _items.Add(item);
            }
        }
    }
}