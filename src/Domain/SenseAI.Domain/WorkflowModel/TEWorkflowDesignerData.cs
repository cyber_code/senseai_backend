﻿using System; 

namespace SenseAI.Domain.WorkflowModel
{
    public class TEWorkflowDesignerData
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public Guid FolderId { get; set; }

        public WorkflowDesignerCellItem [] Items { get; set; }

        public WorkflowDesignertemLinks[] ItemLinks { get; set; }
    }

    public class WorkflowDesignertemLinks
    {
        public Guid FromId { get; set; }

        public Guid ToId { get; set; }

        public int State { get; set; }
    }
}
