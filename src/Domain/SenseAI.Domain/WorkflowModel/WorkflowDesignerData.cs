﻿using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class WorkflowDesignerData
    {
        public Cell[] Cells { get; set; }
    }

    public sealed class Cell
    {
        public WorkflowDesignerCellItem CustomData { get; set; }

        public WorkflowDesignerCellLink Source { get; set; }

        public WorkflowDesignerCellLink Target { get; set; }

        public string Type { get; set; }

        public WorkflowItemLinkState State { get; set; }
    }

    public sealed class WorkflowDesignerCellItem
    {
        public WorkflowType Type { get; set; }

        public Guid Id { get; set; }

        public Guid SystemId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public Guid CatalogId { get; set; }

        public string TypicalName { get; set; }

        public string TypicalType { get; set; }

        public string CatalogName { get; set; }

        public Guid TypicalId { get; set; }

        public string AdapterName { get; set; }

        public int ActionType { get; set; }

        public string Parameters { get; set; }

        public string Action { get; set; }

        public WorkflowDesignerCellItemConstraint[] Constraints { get; set; }

        public WorkflowDesignerCellItemConstraint[] PostConstraints { get; set; }

        public WorkflowDesignerCellItemDynamicDatas[] DynamicDatas { get; set; }

        public decimal Coverage { get; set; }

        public bool Selected { get; set; }

        public bool IsNegativeStep { get; set; }

        public bool IsTitleChanged { get; set; }

        public string ExpectedError { get; set; }

        public Guid WorkflowId { get; set; }

        public int VersionId { get; set; }
    }

    public sealed class WorkflowDesignerCellItemConstraint
    {
        public string AttributeName { get; set; }

        public string Operator { get; set; }

        public string AttributeValue { get; set; }

        public WorkflowDesignerCellItemDynamicDatas DynamicData { get; set; }

        public bool IsPostFilter { get; set; }
    }

    public sealed class WorkflowDesignerCellItemDynamicDatas
    {
        public Guid SourceWorkflowItemId { get; set; }

        public string SourceAttributeName { get; set; }

        public string TargetAttributeName { get; set; }
        public string FormulaText { get; set; }

        public string PathName { get; set; }

        public byte[] PlainTextFormula { get; set; }

        public string Path { get; set; }

        public short? UiMode { get; set; }
    }

    public sealed class WorkflowDesignerCellLink
    {
        public Guid Id { get; set; }
    }
}