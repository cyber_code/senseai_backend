﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public class WorkflowVersionPathDetails : BaseEntity
    {
        public WorkflowVersionPathDetails(Guid id, Guid workflowId, int versionId, Guid pathId, string title, int type, int actionType,
            Guid catalogId, Guid typicalId, string typicalName, string typicalType, Guid dataSetId, string pathItemJson, int index) : base(id)
        {
            WorkflowId = workflowId;
            VersionId = versionId;
            PathId = pathId;
            Title = title;
            Type = type;
            ActionType = actionType;
            CatalogId = catalogId;
            TypicalId = typicalId;
            TypicalName = typicalName;
            TypicalType = typicalType;            
            DataSetId = dataSetId;
            PathItemJson = pathItemJson;
            Index = index;
        }

        public Guid WorkflowId { get; private set; }

        public int VersionId { get; private set; }

        public Guid PathId { get; private set; }
         
        public string Title { get; private set; }

        public int Type { get; private set; }

        public int ActionType { get; private set; }

        public Guid CatalogId { get; private set; }

        public Guid TypicalId { get; private set; }

        public string TypicalName { get; private set; }

        public string TypicalType { get; private set; }        

        public Guid DataSetId { get; private set; }

        public string PathItemJson { get; private set; }

        public int Index { get; private set; }
        
        public void SetDataSetId(Guid dataSetId)
        {
            DataSetId = dataSetId;
        }
    }
}