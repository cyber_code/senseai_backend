﻿using Newtonsoft.Json;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class WorkflowPathDesinger
    {
        [JsonProperty("paths")]
        public WorkflowPath[] WorkflowPaths { get; set; }
    }

    public sealed class WorkflowPath
    {
        public Guid PathId { get; set; }

        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("coverage")]
        public string Coverage { get; set; }

        [JsonProperty("selected")]
        public bool Selected { get; set; }

        [JsonProperty("isBestPath")]
        public bool IsBestPath { get; set; }

        [JsonProperty("itemIds")]
        public Guid[] ItemIds { get; set; }
    }
}