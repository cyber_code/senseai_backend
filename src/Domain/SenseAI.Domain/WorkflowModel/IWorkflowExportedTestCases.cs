﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.WorkflowModel
{
    public interface IWorkflowExportedTestCases : IRepository<WorkflowExportedTestCases>
    {
        void Add(WorkflowExportedTestCases workflowExportedTestCases);
    }
}
