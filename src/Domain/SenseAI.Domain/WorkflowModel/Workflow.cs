﻿using SenseAI.Domain.BaseModel;
using SenseAI.Domain.ValidationModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class Workflow : BaseEntity
    {
        private readonly IList<WorkflowItem> _items = new List<WorkflowItem>();
        private readonly IList<WorkflowItemLink> _itemLinks = new List<WorkflowItemLink>();

        public Workflow(Guid id)
           : base(id)
        {
        }

        public Workflow(string title, string description, Guid folderId, bool isValid = true, bool isParsed = true)
            : this(Guid.NewGuid(), title, description, folderId, isValid, isParsed)
        {
        }

        public Workflow(Guid id, string title, string description, Guid folderId, bool isValid, bool isParsed, bool isissued = false, bool isGeneratedTestCases = false)
            : base(id)
        {
            Title = title;
            Description = description;
            FolderId = folderId;
            IsValid = isValid;
            IsParsed = isParsed;
            IsIssued = isissued;
            IsGeneratedTestCases = isGeneratedTestCases;
        }

        public Workflow(Guid id, string title, string workflowImage) : base(id)
        {
            Title = title;
            WorkflowImage = workflowImage;
        }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Guid FolderId { get; private set; }

        public bool IsValid { get; private set; }

        public string WorkflowPathsJson { get; private set; }

        public bool IsParsed { get; private set; }

        public string WorkflowImage { get; private set; }

        public bool IsIssued { get; private set; }

        public bool IsGeneratedTestCases { get; private set; }

        public IEnumerable<WorkflowItem> Items
        {
            get { return _items.ToImmutableArray(); }
        }

        public IReadOnlyCollection<WorkflowItemLink> ItemLinks
        {
            get { return _itemLinks.ToImmutableArray(); }
        }

        public StartWorkflowItem AddStartItem(
            Guid id, Guid systemId, string title, string description,
            Guid catalogId, string adapterName, string catalogName, Guid typicalId, string typicalName, string typicalType, int actionType, string parameters
        )
        {
            var item = new StartWorkflowItem(
                id, systemId, title, description, catalogId, adapterName, catalogName, typicalId, typicalName, typicalType, actionType, parameters
            );

            _items.Add(item);

            return item;
        }

        public ActionWorkflowItem AddActionItem(
            Guid id, string title, string description,
            Guid catalogId, string catalogName, Guid typicalId, string typicalName, string typicalType, int actionType, string parameters,
            WorkflowItemConstraint[] constraints, WorkflowItemConstraint[] postConstraints,
            WorkflowItemDynamicData[] dynamicDatas, bool isNegativeStep, string expectedError, bool isTitleChanged = false
        )
        {
            var item = new ActionWorkflowItem(
                id, title, description, catalogId, catalogName, typicalId, typicalName, typicalType, actionType, parameters,
                 constraints, postConstraints, dynamicDatas, isNegativeStep, expectedError, isTitleChanged
            );

            _items.Add(item);

            return item;
        }

        public ConditionWorkflowItem AddConditionItem(
            Guid id, string title, string description,
            Guid catalogId, string catalogName, Guid typicalId, string typicalName, string typicalType, int actionType, string parameters,
             WorkflowItemConstraint[] constraints
        )
        {
            var item = new ConditionWorkflowItem(
                id, title, description, catalogId, catalogName, typicalId, typicalName, typicalType, actionType, parameters,
                constraints
            );

            _items.Add(item);

            return item;
        }

        public LinkedWorkflowItem AddLinkedItem(
            Guid id, string title, string description,
            Guid catalogId, string catalogName, Guid typicalId, string typicalName, string typicalType, int actionType, string parameters, Guid workflowId,
            int workflowVersionId
        )
        {
            var item = new LinkedWorkflowItem(
                id, title, description, catalogId, catalogName, typicalId, typicalName, typicalType, actionType, parameters, workflowId, workflowVersionId
            );

            _items.Add(item);

            return item;
        }

        public ManualWorkflowItem AddManualItem(
    Guid id, string title, string description,
    Guid catalogId, string catalogName, Guid typicalId, string typicalName, string typicalType, int actionType, string parameters, string role
)
        {
            var item = new ManualWorkflowItem(
                id, title, description, catalogId, catalogName, typicalId, typicalName, typicalType, actionType, parameters, role
            );

            _items.Add(item);

            return item;
        }

        public void AddItemLink(Guid fromId, Guid toId, WorkflowItemLinkState state)
        {
            var item = new WorkflowItemLink(fromId, toId, state);

            _itemLinks.Add(item);
        }
        public void RemoveItemLink(Guid fromId, Guid toId, WorkflowItemLinkState state)
        {
            var item = _itemLinks.FirstOrDefault(w => w.FromId == fromId && w.ToId == toId);

            _itemLinks.Remove(item);
        }

        public void AddItems(List<WorkflowItem> workflowItems)
        {
            foreach (var item in workflowItems)
            {
                this._items.Add(item);
            }
        }

        public void AddLinks(List<WorkflowItemLink> workflowItemLinks)
        {
            foreach (var item in workflowItemLinks)
            {
                this._itemLinks.Add(item);
            }
        }

        public void SetWorkflowPathsJson(string workflowPathsJson)
        {
            WorkflowPathsJson = workflowPathsJson;
        }

        public void SetIsParsed(bool isParsed)
        {
            IsParsed = isParsed;
        }
        public void SetParentIds(Guid parentId)
        {
            foreach (var item in this._items)
            {
                item.SetParenID(parentId);
            }

            foreach (var itemLink in this._itemLinks)
            {
                itemLink.SetParenID(parentId);
            }
        }

        public void SetUniqueIds(string uniqueId)
        {
            foreach (var item in this._items)
            {
                item.SetUniqueId(uniqueId);
            }

            foreach (var itemLink in this._itemLinks)
            {
                itemLink.SetUniqueId(uniqueId);
            }
        }

        public void AppendLinkWorkflow(LinkedWorkflowItem linkedWorkflowItem, Workflow linkedWorkflow, string uniqueId)
        {
            var lkFirstWF = linkedWorkflow.Items.FirstOrDefault(ct => ct is StartWorkflowItem && linkedWorkflow._itemLinks.FirstOrDefault(wf => wf.ToId == ct.Id) == null);

            lkFirstWF.SetTitle("Switch System");
            var lkLastWf = linkedWorkflow.Items.Where(ct => linkedWorkflow._itemLinks.FirstOrDefault(wf => wf.FromId == ct.Id) == null);

            var nextItem = this.ItemLinks.FirstOrDefault(ct => ct.FromId == linkedWorkflowItem.Id);

            var itemF = new WorkflowItemLink(linkedWorkflowItem.Id, lkFirstWF.Id, WorkflowItemLinkState.Yes);
            itemF.SetParenID(linkedWorkflowItem.ParenID);
            itemF.SetUniqueId(uniqueId);
            this._itemLinks.Add(itemF);

            if (nextItem != null)
            {
                foreach (var wfi in lkLastWf)
                {
                    var itemL = new WorkflowItemLink(wfi.Id, nextItem.ToId, WorkflowItemLinkState.Yes);
                    itemL.SetParenID(wfi.ParenID);
                    itemL.SetUniqueId(wfi.UniqueId);
                    wfi.SetIsLastItem(true);
                    this._itemLinks.Add(itemL);
                }
                var item = _items.FirstOrDefault(w => w.Id == nextItem.ToId);
                this._itemLinks.Remove(nextItem);
            }
        }

        public bool Validate(out RequestValidation requestValidation)
        {
            requestValidation = new RequestValidation(MessageType.Successful, "");
            if (!this.Items.Any(ct => ct is StartWorkflowItem))
            {
                requestValidation = new RequestValidation(MessageType.Warning, "Workflow does not have any start workflow item.");
                return false;
            }

            var items = this.Items.Where(ct => ct is ActionWorkflowItem);
            if (items.Any(ct => ct.TypicalId == Guid.Empty && (ct.ActionType != (int)ActionType.Tab && ct.ActionType != (int)ActionType.Menu)))
            {
                requestValidation = new RequestValidation(MessageType.Failed, "For One or more workflow items the typicalId is not selected.");
                return false;
            }

            if (items.Any(ct => string.IsNullOrEmpty(ct.TypicalName) && (ct.ActionType != (int)ActionType.Tab && ct.ActionType != (int)ActionType.Menu)))
            {
                requestValidation = new RequestValidation(MessageType.Failed, "For One or more workflow items the typical name is not set.");
                return false;
            }

            if (items.Any(ct => ct.CatalogId == Guid.Empty && (ct.ActionType != (int)ActionType.Tab && ct.ActionType != (int)ActionType.Menu)))
            {
                requestValidation = new RequestValidation(MessageType.Failed, "For One or more workflow items the catalogId is not selected.");
                return false;
            }

            if (items.Any(ct => string.IsNullOrEmpty(ct.CatalogName) && (ct.ActionType != (int)ActionType.Tab && ct.ActionType != (int)ActionType.Menu)))
            {
                requestValidation = new RequestValidation(MessageType.Failed, "For One or more workflow items the catalog name is not set.");
                return false;
            }

            return true;
        }
    }
}