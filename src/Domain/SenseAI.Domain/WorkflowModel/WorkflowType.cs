﻿namespace SenseAI.Domain.WorkflowModel
{
    public enum WorkflowType
    {
        Start,
        Action,
        Condition,
        Linked,
        Manual
    }
}