﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;

namespace SenseAI.Domain.WorkflowModel
{
    public interface IWorkflowVersionPathsRepository : IRepository<WorkflowVersionPaths>
    {
        void Add(WorkflowVersionPaths[] workflowPaths);

        void Update(WorkflowVersionPaths[] workflowPaths);

        void Update(Guid workflowId, Guid workflowPathId, Guid pathItemId, int index, Guid dataSetId); 

        void UpdateOnLastVersion(Guid oldDataSetId, Guid newDataSetId);

        WorkflowVersionPaths Get(Guid workflowId, Guid pathId);

        List<WorkflowVersionPaths> Get(Guid workflowId, int prevVersionId);

        List<WorkflowVersionPaths> GetVersionPaths(Guid workflowId, int versionId);

        bool CheckDataSetOnLastVersions(Guid workflowId, Guid dataSetId);

        bool CheckDataSetOnNotLastVersions(Guid workflowId, Guid dataSetId);
    }
}