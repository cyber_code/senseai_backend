﻿using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class StartWorkflowItem : WorkflowItem
    {
        internal StartWorkflowItem(
            Guid id, Guid systemId, string title, string description,
            Guid catalogId, string adapterName, string catalogName, Guid typicalId, string typicalName, string typicalType, int actionType, string parameters
        )
            : base(id, title, description, catalogId, catalogName, typicalId, typicalName, typicalType, actionType, parameters)
        {
            SystemId = systemId;
            AdapterName = adapterName;
        }

        public Guid SystemId { get; private set; }

        public string AdapterName { get; private set; }

        public void SetAdapterName(string adapterName)
        {
            this.AdapterName = adapterName;
        }

        public string Type { get { return ((int)WorkflowType.Start).ToString(); } }
    }
}