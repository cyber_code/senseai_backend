﻿using SenseAI.Domain.BaseModel; 

namespace SenseAI.Domain.WorkflowModel
{
    public interface IWorkflowTestCasesRepository : IRepository<WorkflowTestCases>
    {
        void Add(WorkflowTestCases[] workflowTestCases);

        void Update(WorkflowTestCases [] workflowTestCases);
    }
}