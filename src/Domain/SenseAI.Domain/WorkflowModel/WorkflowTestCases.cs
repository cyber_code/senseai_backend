﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class WorkflowTestCases : BaseEntity
    {
        public WorkflowTestCases(Guid id,
                                Guid workflowId,
                                int versionId,
                                Guid workflowPathId,
                                Guid testCaseId,
                                string testCaseTitle,
                                int tCIndex,
                                Guid testStepId,
                                string testStepTitle,
                                string testStepJson,
                                Guid typicalId,
                                string typicalName,
                                int tSIndex,
                                int tSType) : base(id)
        {
            WorkflowId = workflowId;
            VersionId = versionId;
            WorkflowPathId = workflowPathId;
            TestCaseId = testCaseId; 
            TestCaseTitle = testCaseTitle;
            TCIndex = tCIndex;
            TestStepId = testStepId; 
            TestStepTitle = testStepTitle;
            TestStepJson = testStepJson;
            TypicalId = typicalId;
            TypicalName = typicalName;
            TSIndex = tSIndex;
            TestStepType = tSType;
        }
        public Guid WorkflowId { get; private set; }

        public int VersionId { get; private set; }

        public Guid WorkflowPathId { get; private set; } 

        public Guid TestCaseId { get; private set; }
         
        public string TestCaseTitle { get; private set; }

        public int TCIndex { get; private set; }

        public Guid TestStepId { get; private set; } 

        public string TestStepTitle { get; private set; }

        public string TestStepJson { get; private set; }
         
        public string TypicalName { get; private set; }

        public Guid TypicalId { get; private set; }

        public int TSIndex { get; private set; }

        public int TestStepType { get; private set; } 
    }
}