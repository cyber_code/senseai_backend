﻿using SenseAI.Domain.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class WorkflowExportedTestCases : BaseEntity
    {
        public Guid WorkflowId { get; set; }

        public int VersionId { get; set; }

        public bool IsLast { get; set; }

        public string SenseAITestCases { get; set; }

        public string AtsTestCases { get; set; }
    }
}
