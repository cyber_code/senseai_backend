﻿using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class ActionWorkflowItem : WorkflowItem
    {
        internal ActionWorkflowItem(
            Guid id, string title, string description,
            Guid catalogId, string catalogName, Guid typicalId, string typicalName, string typicalType, int actionType, string parameters,
           WorkflowItemConstraint[] constraints, WorkflowItemConstraint[] postConstraints, WorkflowItemDynamicData[] dynamicDatas, bool isNegativeStep, string expectedError, bool isTitleChanged = false
        )
            : base(id, title, description, catalogId, catalogName, typicalId, typicalName, typicalType, actionType, parameters)
        {
            Constraints = constraints;
            PostConstraints = postConstraints;
            DynamicDatas = dynamicDatas;
            IsNegativeStep = isNegativeStep;
            ExpectedError = expectedError;
            IsTitleChanged = isTitleChanged;
        }

        public WorkflowItemConstraint[] Constraints { get; private set; }

        public WorkflowItemConstraint[] PostConstraints { get; private set; }

        public WorkflowItemDynamicData[] DynamicDatas { get;  set; }

        public bool IsNegativeStep { get; set; }

        public string ExpectedError { get; set; }

        public string Type { get { return ((int)WorkflowType.Action).ToString(); } }

        public bool IsTitleChanged { get; set; }
         
    }
}