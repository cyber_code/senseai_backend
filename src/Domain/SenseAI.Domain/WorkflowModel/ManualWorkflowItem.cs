﻿using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class ManualWorkflowItem : WorkflowItem
    {
        internal ManualWorkflowItem(
            Guid id, string title, string description,
            Guid catalogId, string catalogName, Guid typicalId, string typicalName, string typicalType, int actionType, string parameters, string role
        )
            : base(id, title, description, catalogId, catalogName, typicalId, typicalName, typicalType, actionType, parameters)
        {
            Role = role;
        }

        public string Type { get { return ((int)WorkflowType.Manual).ToString(); } }

        public string Role { get; private set; }
    }
}