﻿using SenseAI.Domain.BaseModel;
using System;

namespace SenseAI.Domain.WorkflowModel
{
    public sealed class Path : BaseEntity
    {
        public Path(string title, int number, decimal coverage, bool isBestPath, Guid[] itemIds) : base(Guid.NewGuid())
        {
            Title = title;
            Number = number;
            Coverage = coverage;
            IsBestPath = isBestPath;
            ItemIds = itemIds;
        }

        public string Title { get; private set; }

        public int Number { get; private set; }

        public decimal Coverage { get; private set; }

        public bool IsBestPath { get; private set; }

        public Guid[] ItemIds { get; private set; }
    }
}