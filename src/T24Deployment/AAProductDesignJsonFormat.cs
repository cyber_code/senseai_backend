﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T24Deployment
{
    public class AAProductDesignJsonFormat
    {
        public string ApplicationName { get; set; }

        public string Key { get; set; }

        public Content Content { get; set; }

        public string Date { get; set; }
    }

    public class Content
    {
        public string id { get; set; }
        public string DESCRIPTION11 { get; set; }
        public string PRODUCTGROUP { get; set; }
        public string CURRENCY11 { get; set; }
        public string PRODUCT { get; set; }
        public string EFFECTIVEDATE { get; set; }
        public string CURRNO { get; set; }
        public string INPUTTER11 { get; set; }
        public string DATETIME11 { get; set; }
        public string AUTHORISER { get; set; }
        public string COCODE { get; set; }
        public string DEPTCODE { get; set; }
        public string PROPERTY11 { get; set; }
        public string PRDPROPERTY11 { get; set; }
        public string ARRLINK11 { get; set; }
        public string CALCPROPERTY11 { get; set; }
        public string SOURCETYPE11 { get; set; }
        public string SOURCEBALANCE11 { get; set; }
    }
}
