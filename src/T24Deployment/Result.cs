﻿namespace T24Deployment
{
    public class Result
    {
        public bool IsDeployed { get; set; }

        public string Error { get; set; }
    }
}
