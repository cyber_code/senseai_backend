﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using T24TelnetAdapter;
using ValidataT24Deployment;

namespace T24Deployment
{
    class Program
    {
        private const string ACCOUNT = "ACCOUNT";
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                AAPrdDesAccount prdDesAccount = GetPrdDesAccount(args[0]);

                AAPrdDesInterest prdDesInterst = null;// GetPrdDesInterest();

                AAProductDesigner aaDesigner = GetProductdesigner();

                RequestManagerWrapper t24Deployer = new RequestManagerWrapper("R18-REST-226");

                string errText;
                bool prodPublished = t24Deployer.DeployAAProdFull(aaDesigner, prdDesAccount, prdDesInterst, out errText);

                var response = new Result
                {
                    IsDeployed = prodPublished,
                    Error = errText
                };

                Console.WriteLine(JsonConvert.SerializeObject(response));
            }
        }

        private static AAProductDesigner GetProductdesigner()
        {
            var jsonPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"aaproductdesigner.json");
            var json = File.ReadAllText(jsonPath);
            var jsonData = JsonConvert.DeserializeObject<AAProductDesignJsonFormat>(json);

            var properties = jsonData.Content.PROPERTY11.Split('|');
            var prdProperties = jsonData.Content.PRDPROPERTY11.Split('|');
            var arrLink = jsonData.Content.ARRLINK11.Split('|');

            var calProperty = jsonData.Content.CALCPROPERTY11.Split('|');
            var sourceType = jsonData.Content.SOURCETYPE11.Split('|');
            var sourceBalance = jsonData.Content.SOURCEBALANCE11.Split('|');


            var aadesigner = new AAProductDesigner
            {
                ID = jsonData.Content.id,
                DESCRIPTION = jsonData.Content.DESCRIPTION11,
                PRODUCT_GROUP = jsonData.Content.PRODUCTGROUP
            };

            aadesigner.CURRENCY.Add(new MVField { MVIndex = 1, SVIndex = 1, Value = jsonData.Content.CURRENCY11 });
            for (int i = 0; i < properties.Length; i++)
            {

                aadesigner.PROPERTY_CONDITIONS.Add(new PropertyConditions(i + 1, properties[i], prdProperties[i], arrLink[i]));
            }
            for (int i = 0; i < calProperty.Length; i++)
            {

                aadesigner.CALCULATION_SOURCE.Add(new CalculationsSource(i + 1, calProperty[i], sourceType[i], sourceBalance[i]));
            }

            return aadesigner;
        }

        private static AAPrdDesInterest GetPrdDesInterest()
        {
            return new AAPrdDesInterest();
        }

        private static AAPrdDesAccount GetPrdDesAccount(string jsonPath)
        {
            var requestFilePath = jsonPath;

            var json = File.ReadAllText(jsonPath);

            var jsonData = JsonConvert.DeserializeObject<List<InputClass>>(json);

            var AaPrdDesAccount = new AAPrdDesAccount
            {
                BUS_DAY_CENTRES = new List<MVField>(),
                DEFAULT_NEGOTIABLE = "YES"
            };

            var account = jsonData.Where(w => w.PropertyClass == ACCOUNT);


            foreach (var item in account)
            {
                switch (item.Field)
                {
                    case "@ID"://
                        AaPrdDesAccount.ID = item.Value;
                        break;
                    case "DESCRIPTION"://
                        AaPrdDesAccount.DESCRIPTION = item.Value;
                        break;
                    case "CATEGORY"://
                        AaPrdDesAccount.CATEGORY = item.Value;
                        break;
                    case "DATE.CONVENTION"://
                        AaPrdDesAccount.DATE_CONVENTION = item.Value;
                        break;
                    case "BUS.DAY.CENTRES"://
                        AaPrdDesAccount.BUS_DAY_CENTRES.Add(new MVField
                        {
                            MVIndex = item.MultiValueIndex,
                            SVIndex = item.SubValueIndex,
                            Value = item.Value
                        });
                        break;
                    case "PASSBOOK"://
                        AaPrdDesAccount.PASSBOOK = item.Value;
                        break;
                    case "GENERATE.IBAN"://
                        AaPrdDesAccount.GENERATE_IBAN = item.Value;
                        break;
                    case "ACCOUNTING.COMPANY"://
                        AaPrdDesAccount.ACCOUNTING_COMPANY = item.Value;
                        break;
                    case "FULL.DESCRIPTION"://
                        AaPrdDesAccount.FULL_DESCRIPTION = item.Value;
                        break;
                    default:
                        break;
                }
            }

            return AaPrdDesAccount;
        }
    }
}
