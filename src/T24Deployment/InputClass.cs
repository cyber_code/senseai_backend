﻿using System;
using System.Collections.Generic;
using System.Text;

namespace T24Deployment
{
    public class InputClass
    {
        public string ProductGroup { get; set; }
        public string PropertyClass { get; set; }
        public string Property { get; set; }
        public string Field { get; set; }
        public int MultiValueIndex { get; set; }
        public int SubValueIndex { get; set; }
        public string Value { get; set; }
        public string CurrencyId { get; set; }
    }
}
