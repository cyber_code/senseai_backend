﻿using Events.AAProductBuilder;
using Messaging.Commands;
using Messaging.Events;
using Microsoft.AspNetCore.SignalR;
using Persistence.Internal;
using System.Threading.Tasks;

namespace Application.Events.AAProductBuilder
{
    public sealed class QuestionAnsweredEventHandler : ICommandHandler<QuestionAnsweredEvent, bool>
    {
        private readonly IHubContext<NotificationsHub> _hubContext;

        public QuestionAnsweredEventHandler(IHubContext<NotificationsHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public CommandResponse<bool> Handle(QuestionAnsweredEvent @event)
        {
            _hubContext.Clients
                             .Group(@event.ProductGroupPropertyFieldState.SessionId.ToString())
                             .SendAsync("UpdatedAttribute", @event.ProductGroupPropertyFieldState).Wait();            

            return new CommandResponse<bool>(true);
        }

        public bool Validate(QuestionAnsweredEvent command)
        {
            throw new System.NotImplementedException();
        }
    }
}
