﻿using Autofac;
using Autofac.Core;
using Microsoft.EntityFrameworkCore;
using Application.Commands.AdministrationModule.Projects;
using Application.Decorators;
using Application.Queries.DesignModule.TreeView;
using Application.Services;
using Core;
using Core.Abstractions;
using Core.Configuration;
using DataExtraction;
using SenseAI.Domain.BaseModel;
using SenseAI.Domain.WorkflowModel;
using External.QualitySuite;
using Logging.Messaging;
using Logging.Messaging.Persistence;
using Messaging;
using Messaging.Commands;
using Messaging.Commands.Decorators;
using Messaging.Queries;
using Messaging.Queries.Decorators;
using NeuralEngine;
using PathGenerations;
using Persistence;
using Persistence.Internal;
using System.Linq;
using Common;
using Core.Abstractions.Caching;
using SignalR;
using SenseAI.Domain.Process.ArisWorkflowModel;
using ImportAris;
using SenseAI.Domain.Process.HierarchiesModel;
using ImportExcel;
using Jira;
using SenseAI.Domain.Process.ProcessModel;
using DocumentGeneration;

namespace Application
{
    public sealed class DependencyRegistrar : IDependencyRegistrar
    {
        public int Order => 0;

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, SenseAIConfig config)
        {
            RegisterCaching(builder);

            RegisterMessagingBus(builder);

            RegisterAutoMapper(builder);

            RegisterDataLayer(builder, typeFinder);

            RegisterConfig(builder);

            RegisterCommandAndQueryLogger(builder, config, typeFinder);

            RegisterCommandHandlers(builder, config, typeFinder);

            RegisterQueryHandlers(builder, config, typeFinder);

            RegisterServices(builder);
        }

        private static void RegisterMessagingBus(ContainerBuilder builder)
        {
            builder
                .RegisterType<Bus>()
                .As<IBus>()
                .SingleInstance();
        }

        private static void RegisterAutoMapper(ContainerBuilder builder)
        {
            builder
                .RegisterType<AutoMapperTypeAdapterFactory>()
                .As<ITypeAdapterFactory>()
                .SingleInstance();
        }

        private static void RegisterDataLayer(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            builder
                .RegisterType<EfDataProviderManager>()
                .As<IDataProviderManager>()
                .InstancePerDependency();

            builder
                .Register(context => context.Resolve<IDataProviderManager>().GetDataProvider())
                .As<IDataProvider>()
                .InstancePerDependency();

            builder
                .Register(context =>
                new SenseAIObjectContext(context.Resolve<DbContextOptions<SenseAIObjectContext>>(),
                                     context.Resolve<IWorkContext>()))
                .As<IDbContext>()
                .InstancePerLifetimeScope();

            builder
                .RegisterGeneric(typeof(EfRepository<,>))
                .As(typeof(IBaseRepository<,>));

            builder
                .RegisterAssemblyTypes(typeof(EfRepository<,>).Assembly)
                .AsClosedTypesOf(typeof(IRepository<>));
        }

        private static void RegisterConfig(ContainerBuilder builder)
        {
            builder
                .RegisterType<IdentityServerConfig>()
                .As<IIdentityServerConfig>()
                .SingleInstance();
        }

        private static void RegisterCommandAndQueryLogger(ContainerBuilder builder, SenseAIConfig config, ITypeFinder typeFinder)
        {
            builder
                .Register(context => new MessagingLogContext(context.Resolve<DbContextOptions<MessagingLogContext>>()))
                .InstancePerLifetimeScope();

            builder
                .RegisterType<MessagingLogger>()
                .As<IMessagingLogger>()
                .InstancePerLifetimeScope();
        }

        private static void RegisterCommandHandlers(ContainerBuilder builder, SenseAIConfig config, ITypeFinder typeFinder)
        {
            builder
                .RegisterAssemblyTypes(typeof(AddSubProjectHandler).Assembly)
                .As(
                    type => type.GetInterfaces()
                        .Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(ICommandHandler<,>)))
                        .Select(interfaceType => new KeyedService("commandHandler", interfaceType))
                )
                .InstancePerLifetimeScope();

            builder
                .RegisterGenericDecorator(typeof(ExceptionCommandHandlerDecorator<,>), typeof(ICommandHandler<,>), "commandHandler", "exceptionCommandHandler")
                .InstancePerLifetimeScope();

            builder.RegisterGenericDecorator(typeof(LoggingCommandHandler<,>), typeof(ICommandHandler<,>), "exceptionCommandHandler")
                .InstancePerLifetimeScope();
        }

        private static void RegisterQueryHandlers(ContainerBuilder builder, SenseAIConfig config, ITypeFinder typeFinder)
        {
            builder
                .RegisterAssemblyTypes(typeof(GetFoldersHandler).Assembly)
                .As(
                    type => type.GetInterfaces()
                        .Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(IQueryHandler<,>)))
                        .Select(interfaceType => new KeyedService("queryHandler", interfaceType))
                )
                .InstancePerLifetimeScope();

            builder
                .RegisterGenericDecorator(typeof(ExceptionQueryHandlerDecorator<,>), typeof(IQueryHandler<,>), "queryHandler", "exceptionQueryHandler")
                .InstancePerLifetimeScope();

            builder.RegisterGenericDecorator(typeof(LoggingQueryHandler<,>), typeof(IQueryHandler<,>), "exceptionQueryHandler")
                .InstancePerLifetimeScope();
        }

        private static void RegisterServices(ContainerBuilder builder)
        {
            builder
                .RegisterType<InstallationService>()
                .As<IInstallationService>()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<NeuralEngineClient>()
                .As<INeuralEngineClient>()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<JiraClient>()
                .As<IJiraClient>()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<DataExtractionClient>()
                .As<IDataExtractionClient>()
                .InstancePerLifetimeScope();

            builder
               .RegisterType<QualitySuiteClient>()
               .As<IQualitySuiteClient>()
               .InstancePerLifetimeScope();

            builder
               .RegisterType<GeneratePaths>()
               .As<IGeneratePaths>()
               .InstancePerLifetimeScope();

            builder
                .RegisterType<EncryptionService>()
                .As<IEncryptionService>()
                .InstancePerLifetimeScope();

            builder
               .RegisterType<SignalRClient>()
               .As<ISignalRClient>()
               .InstancePerLifetimeScope();

            builder
               .RegisterType<ImportArisWorkflow>()
               .As<IImportArisWorkflow>()
               .InstancePerLifetimeScope();

            builder
               .RegisterType<ImportHierarchiesFromExcel>()
               .As<IImportHierarchy>()
               .InstancePerLifetimeScope();

            builder
               .RegisterType<DocumentGenerations>()
               .As<IDocumentGenerations>()
               .InstancePerLifetimeScope();
        }

        private void RegisterCaching(ContainerBuilder builder)
        {
            builder
                .RegisterType<MemoryCacheManager>()
                .As<IStaticCacheManager>()
                .SingleInstance();
        }
    }
}