﻿using Messaging;

namespace Application
{
    public sealed class Bus : BaseBus
    {
        public Bus() : base(new CommandHandlerResolver(), new QueryHandlerResolver())
        {
        }
    }
}