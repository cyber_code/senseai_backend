﻿using SenseAI.Domain.AdminModel;
using SenseAI.Domain.CatalogModel;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using SenseAI.Domain.SystemModel;
using SenseAI.Domain.Process.IssuesModel;
using Persistence.AAProductBuilderModel;
using Persistence.DataModel;
using Persistence.Internal;
using Persistence.SystemModel;
using Persistence.WorkflowModel;
using Persistence.WorkflowModel;
using System;
using SenseAI.Domain;
using SenseAI.Domain.Process.ArisWorkflowModel;
using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain.Process.ProcessModel;
using SenseAI.Domain.Process.GenericWorkflowModel;
using SenseAI.Domain.Process.SprintsModel;
using Core.Configuration;
using Microsoft.EntityFrameworkCore;
using SenseAI.Domain.SystemTags;
using SenseAI.Domain.SystemCatalogs;
using SenseAI.Domain.PeopleModel;
using SenseAI.Domain.Process.RequirementTypesModel;
using SenseAI.Domain.Process.RequirementPrioritiesModel;
using Persistence.Process.IssuesModel;
using System.Collections.Generic;

namespace Application.Services
{
    public class InstallationService : IInstallationService
    {
        #region Fields

        private readonly SenseAIConfig _saiConfig;
        private readonly IProjectRepository _projectRepository;
        private readonly ISubProjectRepository _subProjectRepository;
        private readonly SenseAIObjectContext _saiObjectContext;
        private readonly IFolderRepository _folderRepository;
        private readonly ISettingsRepository _settingsRepository;
        private readonly ITypicalRepository _typicalRepository;
        private readonly ITypicalChangesRepository _typicalChangesRepository;
        private readonly ICatalogRepository _catalogRepository;
        private readonly IDataSetRepository _dataSetRepository;
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IDynamicDataSuggestionRepository _dynamicDataSuggestionRepository;
        private readonly IWorkflowVersionPathsRepository _workflowPathsRepository;
        private readonly IAcceptedTypicalChangesRepository _acceptedTypicalChangesRepository;
        private readonly IIssueTypeRepository _issueTypesRepository;
        private readonly IArisWorkflowRepository _arisWorkflowRepository;
        private readonly IIssueRepository _issueRepository;
        private readonly IIssueResourceRepository _issueResourceRepository;
        private readonly IIssueCommentRepository _issueCommentRepository;
        private readonly IProcessWorkflowRepository _processWorkflowRepository;

        private readonly IProcessResourcesRepository _processResourceRepository;
        private readonly IWorkflowIssueRepository _workflowIssueRepository;

        private readonly IRequirementTypeRepository _requirementTypeRepository;
        private readonly IRequirementPriorityRepository _requirementPriorityRepository;
        private readonly IProcessRepository _processRepository;

        private readonly IHierarchyRepository _hierarchyRepository;
        private readonly IHierarchyTypeRepository _hierarchyTypeRepository;

        private readonly IGenericWorkflowRepository _genericWorkflowRepository;

        private readonly ISprintsRepository _sprintsRepository;
        private readonly ISprintIssuesRepository _sprintIssuesRepository;
        private readonly ISystemRepository _systemRepository;
        private readonly ISystemTagRepository _systemTagRepository;
        private readonly ISystemCatalogRepository _systemCatalogRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IPeopleRepository _peopleRepository;

        #endregion Fields

        public InstallationService(
                        SenseAIConfig saiConfig,
                        IProjectRepository projectRepository,
                        ISubProjectRepository subProjectRepository,
                        SenseAIObjectContext saiObjectContext,
                        IFolderRepository folderRepository,
                        ISettingsRepository settingsRepository,
                        ITypicalRepository typicalRepository,
                        ICatalogRepository catalogRepository,
                        IDataSetRepository dataSetRepository,
                        IWorkflowRepository workflowRepository,
                        IDynamicDataSuggestionRepository dynamicDataSuggestionRepository,
                        IWorkflowVersionPathsRepository workflowPathsRepository,
                        ITypicalChangesRepository typicalChangesRepository,
                        IAcceptedTypicalChangesRepository acceptedTypicalChangesRepository,
                        IIssueTypeRepository issueTypeRepository,
                        IArisWorkflowRepository arisWorkflowRepository,
                        IIssueRepository issueRepository,
                        IIssueResourceRepository issueResourceRepository,
                        IIssueCommentRepository issueCommentRepository,
                        IProcessWorkflowRepository processWorkflowRepository,
                        IProcessResourcesRepository processResourceRepository,
                        IWorkflowIssueRepository workflowIssueRepository,
                        IRequirementTypeRepository requirementTypeRepository,
                        IRequirementPriorityRepository requirementPriorityRepository,
                        IProcessRepository processRepository,
                        IHierarchyRepository hierarchyRepository,
                        IHierarchyTypeRepository hierarchyTypeRepository,
                        IGenericWorkflowRepository genericWorkflowRepository,
                        ISprintsRepository sprintsRepository,
                        ISprintIssuesRepository sprintIssuesRepository,
                        ISystemRepository systemRepository,
                        ISystemTagRepository systemTagRepository,
                        ISystemCatalogRepository systemCatalogRepository,
                        IRoleRepository roleRepository,
                        IPeopleRepository peopleRepository
           )
        {
            _saiConfig = saiConfig;
            _projectRepository = projectRepository;
            _subProjectRepository = subProjectRepository;
            _saiObjectContext = saiObjectContext;
            _folderRepository = folderRepository;
            _settingsRepository = settingsRepository;
            _typicalRepository = typicalRepository;
            _catalogRepository = catalogRepository;
            _dataSetRepository = dataSetRepository;
            _workflowRepository = workflowRepository;
            _dynamicDataSuggestionRepository = dynamicDataSuggestionRepository;
            _workflowPathsRepository = workflowPathsRepository;
            _typicalChangesRepository = typicalChangesRepository;
            _acceptedTypicalChangesRepository = acceptedTypicalChangesRepository;
            _issueTypesRepository = issueTypeRepository;
            _arisWorkflowRepository = arisWorkflowRepository;
            _issueRepository = issueRepository;
            _issueResourceRepository = issueResourceRepository;
            _issueCommentRepository = issueCommentRepository;
            _processWorkflowRepository = processWorkflowRepository;
            _processResourceRepository = processResourceRepository;
            _workflowIssueRepository = workflowIssueRepository;
            _requirementTypeRepository = requirementTypeRepository;
            _requirementPriorityRepository = requirementPriorityRepository;
            _processRepository = processRepository;
            _hierarchyRepository = hierarchyRepository;
            _hierarchyTypeRepository = hierarchyTypeRepository;
            _genericWorkflowRepository = genericWorkflowRepository;
            _sprintsRepository = sprintsRepository;
            _sprintIssuesRepository = sprintIssuesRepository;
            _systemRepository = systemRepository;
            _systemTagRepository = systemTagRepository;
            _systemCatalogRepository = systemCatalogRepository;
            _roleRepository = roleRepository;
            _peopleRepository = peopleRepository;
        }

        /// <summary>
        /// Install data
        /// </summary>
        /// <param name="installSampleData">A value indicating whether to install sample data</param>
        public void InstallData(bool installSampleData = true)
        {
            if (_saiConfig.IsTesting && _saiConfig.TestWithRealDatabase)
            {
                _saiObjectContext.Database.Migrate();
            }
            InsertRequiredData();

            if (installSampleData)
            {
                InsertSampleData();
            }
        }

        private void InsertRequiredData()
        {
            //TODO put required data
        }

        private void InsertSampleData()
        {
            //put smaple data if you need
            InsertProjects();
            InsertSubProjects();
            InsertFolders();
            InsertNavigations();
            InsertSystems();
            InsertSystemTags();
            InsertSettings();
            InsertCatalogs();
            InsertSystemCatalog();
            InsertTypicals();
            InsertWorkflows();
            InsertWorkflowVersions();
            InsertWorkflowPaths();
            InsertDynamicDataSuggestions();
            InsertProductionDatas();
            InsertQueries();
            InsertParameters();
            InsertQueryParameters();
            InsertProducts();
            //InsertSections();
            //InsertProductSections();
            InsertProductStates();
            //InsertProductPropertyStates();
            InsertDataSets();
            InsertGeneratedTestCases();
            InsertTypicalChanges();
            InsertAcceptedTypicalChanges();
            PreviousWorkflowItems();
            InsertArisWorkflow();
            InsertIssueTypeFieldsName();
            InsertIssueTypes();
            InsertIssues();
            InsertIssueResources();
            InsertIssueComments();
            InsertProcessWorkflows();
            InsertProcessResources();
            InsertWorkflowIssues();
            InsertRequirementTypes();
            InsertRequirementPriorities();
            InsertProcesses();
            InsertGenericWorkflow();
            InsertHierarchyTypes();
            InsertHierarchies();
            InsertSprints();
            InsertSprintIssues();
            InsertRoles();
            InsertPeoples();
        }

        private void InsertProjects()
        {
            var project1 = new Project(Variables.ProjectId, "Testing 2019", "Test Project 1");
            var project2 = new Project(Variables.DelProjectId, "Testing 2019 Del", "Test Project 1 Del");

            _projectRepository.Add(project1);
            _projectRepository.Add(project2);
        }

        private void InsertSubProjects()
        {
            var subProject1 = new SubProject(Variables.SubProjectId, Variables.ProjectId, "T24 R16", "Test SubProject 1", false, "");
            var subProject2 = new SubProject(Variables.DelSubProjectId, Variables.DelProjectId, "Test SubProject 1 Del", "Test SubProject 1 Del", false, "");

            _subProjectRepository.Add(subProject1);
            _subProjectRepository.Add(subProject2);
        }

        private void InsertFolders()
        {
            var folder1 = new Folder(Variables.FolderId, Variables.SubProjectId, "Folder 1", "Folder 1");
            var folder2 = new Folder(Guid.NewGuid(), Variables.SubProjectId, "Aris Workflow", "Aris Workflow");
            var folder3 = new Folder(Variables.DelFolderId, Variables.SubProjectId, "Folder 2", "Folder 2");
            var folder4 = new Folder(Guid.NewGuid(), Variables.DelSubProjectId, "Folder 2", "Folder 2");
            var folder5 = new Folder(Variables.ImportFolderId, Guid.NewGuid(), "Folder 2", "Folder 2");

            _folderRepository.Add(folder1);
            _folderRepository.Add(folder2);
            _folderRepository.Add(folder3);
            _folderRepository.Add(folder4);
            _folderRepository.Add(folder5);
        }

        private void InsertCatalogs()
        {
            /*For project and subproject*/
            var catalog1 = new Catalog(Variables.CatalogId, Variables.ProjectId, Variables.SubProjectId, "R16", "R16", 1);
            var catalog2 = new Catalog(Variables.DelCatalogId, Variables.DelProjectId, Variables.DelSubProjectId, "R16 Del", "R16 Del", 1);

            _catalogRepository.Add(catalog1);
            _catalogRepository.Add(catalog2);

            /*For project*/
            catalog1 = new Catalog(Guid.NewGuid(), Variables.ProjectId, Guid.Empty, "R16", "R16", 1);
            _catalogRepository.Add(catalog1);

            /*For teant*/
            catalog1 = new Catalog(Guid.NewGuid(), Guid.Empty, Guid.Empty, "R16", "R16", 1);
            _catalogRepository.Add(catalog1);
        }

        private void InsertSystemCatalog()
        {
            SystemCatalog systemCatalog1 = new SystemCatalog(Guid.NewGuid(), Variables.SubProjectId, Variables.SystemId, Variables.CatalogId);
            _systemCatalogRepository.Add(systemCatalog1);
        }

        private void InsertNavigations()
        {
            _saiObjectContext.Add(new NavigationData
            {
                Id = new Guid("e2ab61fc-0f26-4fff-a976-b0e4a881e36d"),
                Name = "Home",
                CatalogId = Variables.CatalogId,
                ParentId = new Guid("A7B68BEC-684A-4649-9C56-1A008FB6E449"),
                Url = "Home"
            });
            _saiObjectContext.SaveChanges();
        }

        private void InsertSystems()
        {
            SenseAI.Domain.SystemModel.System system1 = new SenseAI.Domain.SystemModel.System(Variables.SystemId, "T24", "T24Webadapter");
            _systemRepository.Add(system1);
        }

        private void InsertSystemTags()
        {
            SystemTag systemTag1 = new SystemTag(Guid.NewGuid(), Variables.SystemId, "Inpput", "Input");
            _systemTagRepository.Add(systemTag1);
        }

        private void InsertProductionDatas()
        {
            _saiObjectContext.Add(new ProductionDataData
            {
                Id = new Random().Next(int.MaxValue),
                CatalogId = new Guid("85434986-25DE-4F9B-B2EF-000171B2BA58"),
                Json = "{'@id':'SYSTEM','DEFAULT.LCY':'BSNSS','DEFAULT.FCY':'BSNSS','CURR.NO':'1','DATE.TIME - 1~1':'1603281658','AUTHORISER':'97161_INPUTTER_OFS_MB.OFS.AUTH','CO.CODE':'GB0010001','DEPT.CODE':'1','INPUTTER - 1~1':'1_201012m | 46_CONV.AA.ACCRUAL.FREQUENCY.R16'}"
            });
            _saiObjectContext.SaveChanges();
        }

        private void InsertIssueTypeFieldsName()
        {
            _saiObjectContext.Add(new IssueTypeFieldsNameData
            {
                Id = Variables.IssueTypeFieldsNameId,
                Name = "Description"
            });
            _saiObjectContext.Add(new IssueTypeFieldsNameData
            {
                Id = Guid.NewGuid(),
                Name = "Reason"
            });
            _saiObjectContext.SaveChanges();
        }

        private void InsertGeneratedTestCases()
        {
            _saiObjectContext.Add(new WorkflowTestCasesData
            {
                Id = new Guid("306d4610-7c46-49d8-b54d-0b1f591d3109"),
                WorkflowId = new Guid("9e90b265-e7b3-4dbb-8e81-089d71cf7442"),
                WorkflowPathId = new Guid("98bb5c0d-5670-4151-b76c-065c782ce1f3"),
                TestCaseId = new Guid("ff21a6de-5e43-430a-a7c2-58285f607275"),
                TestCaseTitle = "Tc1",
                TCIndex = 0,
                TestStepId = new Guid("4e82073b-b54d-4333-b8c0-bc16c0d047db"),
                TestStepTitle = "Menu: Authorise/Delete Customer",
                TestStepJson = "{'Title':'Menu: Authorise / Delete Customer','Description':'','AdapterName':'T24WebAdapter','SystemName':'T24 Author System','CatalogName':'ProjectResourcing','TypicalName':'NavigationInfo','TypicalId':'00000000-0000-0000-0000-000000000000','Parameters':'User Menu > Customer > Authorise / Delete Customer ','Type':'1','Action':0,'IsNegativeStep':false,'ExpectedError':null,'DataSetIds':null,'TypicalInstance':{'Attributes':[],'Id':'70e2c0c4 - 4d95 - 41dc - 843f - d6a06e9cef81'},'Filters':[],'PostFilters':[],'DynamicDatas':[],'Id':'970942c2 - b9cd - 44ba - 943e-b2bd65156af0'}",
                TypicalId = new Guid("85434986-25DE-4F9B-B2EF-000171B2BA58"),
                TypicalName = "Customer",
                TSIndex = 0
            });
            _saiObjectContext.SaveChanges();
        }

        private void InsertTypicals()
        {
            var typical1 = new Typical(Variables.TypicalId, Variables.CatalogId, "Customer", "{'TenantId':'30788c4a-ea74-4aa8-b505-891303dbaeaa','ProjectId':'0b450e5c-9b76-4851-b341-a42fdc129d68','SubProjectId':'3adf6db6-b640-439d-b6d3-1aaa9d9fd626','SystemId':'b5bcc43f-afc8-4f67-8eb1-b633994ce363','CatalogId':'88a86645-6867-4e6a-97be-68e2236a3807','HashId':'A820CBB11F99D6985AF83949E2F6B2D9','Id':'480af1d1-53a5-43c3-60f5-1387c6df6b31','Type':4,'Title':'PPL.CLIENTCONDADVICECONF','HasAutomaticId':false,'IsConfiguration':false,'Attributes':[{'Name':'ClientConditionsID','IsMultiValue':false,'PossibleValues':null,'IsRequired':false,'IsNoInput':false,'IsNoChange':false,'IsExternal':false,'MinimumLength':0,'MaximumLength':35,'RelatedApplicationName':'PPL.CLIENTCONDADVICECONF','RelatedApplicationIsConfiguration':false,'ExtraData':null}]}", "<row>< TenantId >30788c4a-ea74-4aa8-b505-891303dbaeaa </ TenantId >  < ProjectId > 0b450e5c-9b76-4851-b341-a42fdc129d68 </ ProjectId >  < SubProjectId > 3adf6db6-b640-439d-b6d3-1aaa9d9fd626 </ SubProjectId >  < SystemId > b5bcc43f-afc8-4f67-8eb1-b633994ce363 </ SystemId >  < CatalogId > 88a86645-cd68-4e6a-97be-e68e2236a3807 </ CatalogId >  < HashId > A820CBB11F99D6985AF83949E2F6B2D9 </ HashId >  < Id > 480af1d1-b53a-43c3-a60f-1387c6df6b31 </ Id >  < Type > 4 </ Type >  < Title > PPL.CLIENTCONDADVICECONF </ Title >  < HasAutomaticId > false </ HasAutomaticId >  < IsConfiguration > false </ IsConfiguration >  < Attributes >    < Name > ClientConditionsID </ Name >    < IsMultiValue > false </ IsMultiValue >    < PossibleValues />    < IsRequired > false </ IsRequired >    < IsNoInput > false </ IsNoInput >    < IsNoChange > false </ IsNoChange >    < IsExternal > false </ IsExternal >    < MinimumLength > 0 </ MinimumLength >    < MaximumLength > 35 </ MaximumLength >    < RelatedApplicationName > PPL.CLIENTCONDADVICECONF </ RelatedApplicationName >    < RelatedApplicationIsConfiguration > false </ RelatedApplicationIsConfiguration >    < ExtraData />  </ Attributes ></ row >", "847BF157A8370AD25181ECD6679852EA", SenseAI.Domain.TypicalType.Application);
            var typical2 = new Typical(Variables.DataSetTypicalId, Variables.CatalogId, "Account", "{'TenantId':'30788c4a-ea74-4aa8-b505-891303dbaeaa','ProjectId':'0b450e5c-9b76-4851-b341-a42fdc129d68','SubProjectId':'3adf6db6-b640-439d-b6d3-1aaa9d9fd626','SystemId':'b5bcc43f-afc8-4f67-8eb1-b633994ce363','CatalogId':'88a86645-6867-4e6a-97be-68e2236a3807','HashId':'A820CBB11F99D6985AF83949E2F6B2D9','Id':'480af1d1-53a5-43c3-60f4-1387c6df6b31','Type':4,'Title':'PPL.CLIENTCONDADVICECONF','HasAutomaticId':false,'IsConfiguration':false,'Attributes':[{'Name':'ClientConditionsID','IsMultiValue':false,'PossibleValues':null,'IsRequired':false,'IsNoInput':false,'IsNoChange':false,'IsExternal':false,'MinimumLength':0,'MaximumLength':35,'RelatedApplicationName':'PPL.CLIENTCONDADVICECONF','RelatedApplicationIsConfiguration':false,'ExtraData':null}]}", "<row>< TenantId >30788c4a-ea74-4aa8-b505-891303dbaeaa </ TenantId >  < ProjectId > 0b450e5c-9b76-4851-b341-a42fdc129d68 </ ProjectId >  < SubProjectId > 3adf6db6-b640-439d-b6d3-1aaa9d9fd626 </ SubProjectId >  < SystemId > b5bcc43f-afc8-4f67-8eb1-b633994ce363 </ SystemId >  < CatalogId > 88a86645-cd68-4e6a-97be-e68e2236a3807 </ CatalogId >  < HashId > A820CBB11F99D6985AF83949E2F6B2D9 </ HashId >  < Id > 480af1d1-b53a-43c3-a60f-1387c6df6b31 </ Id >  < Type > 4 </ Type >  < Title > PPL.CLIENTCONDADVICECONF </ Title >  < HasAutomaticId > false </ HasAutomaticId >  < IsConfiguration > false </ IsConfiguration >  < Attributes >    < Name > ClientConditionsID </ Name >    < IsMultiValue > false </ IsMultiValue >    < PossibleValues />    < IsRequired > false </ IsRequired >    < IsNoInput > false </ IsNoInput >    < IsNoChange > false </ IsNoChange >    < IsExternal > false </ IsExternal >    < MinimumLength > 0 </ MinimumLength >    < MaximumLength > 35 </ MaximumLength >    < RelatedApplicationName > PPL.CLIENTCONDADVICECONF </ RelatedApplicationName >    < RelatedApplicationIsConfiguration > false </ RelatedApplicationIsConfiguration >    < ExtraData />  </ Attributes ></ row >", "847BF157A8370AD25181ECD6679852EA", SenseAI.Domain.TypicalType.Application);
            var typical3 = new Typical(Variables.DelTypicalId, Variables.DelCatalogId, "Saving Account", "{'TenantId':'30788c4a-ea74-4aa8-b505-891303dbaeaa','ProjectId':'0b450e5c-9b76-4851-b341-a42fdc129d68','SubProjectId':'3adf6db6-b640-439d-b6d3-1aaa9d9fd626','SystemId':'b5bcc43f-afc8-4f67-8eb1-b633994ce363','CatalogId':'88a86645-6867-4e6a-97be-68e2236a3807','HashId':'A820CBB11F99D6985AF83949E2F6B2D9','Id':'480af1d1-53a5-43c3-60f4-1387c6df6b31','Type':4,'Title':'PPL.CLIENTCONDADVICECONF','HasAutomaticId':false,'IsConfiguration':false,'Attributes':[{'Name':'ClientConditionsID','IsMultiValue':false,'PossibleValues':null,'IsRequired':false,'IsNoInput':false,'IsNoChange':false,'IsExternal':false,'MinimumLength':0,'MaximumLength':35,'RelatedApplicationName':'PPL.CLIENTCONDADVICECONF','RelatedApplicationIsConfiguration':false,'ExtraData':null}]}", "<row>< TenantId >30788c4a-ea74-4aa8-b505-891303dbaeaa </ TenantId >  < ProjectId > 0b450e5c-9b76-4851-b341-a42fdc129d68 </ ProjectId >  < SubProjectId > 3adf6db6-b640-439d-b6d3-1aaa9d9fd626 </ SubProjectId >  < SystemId > b5bcc43f-afc8-4f67-8eb1-b633994ce363 </ SystemId >  < CatalogId > 88a86645-cd68-4e6a-97be-e68e2236a3807 </ CatalogId >  < HashId > A820CBB11F99D6985AF83949E2F6B2D9 </ HashId >  < Id > 480af1d1-b53a-43c3-a60f-1387c6df6b31 </ Id >  < Type > 4 </ Type >  < Title > PPL.CLIENTCONDADVICECONF </ Title >  < HasAutomaticId > false </ HasAutomaticId >  < IsConfiguration > false </ IsConfiguration >  < Attributes >    < Name > ClientConditionsID </ Name >    < IsMultiValue > false </ IsMultiValue >    < PossibleValues />    < IsRequired > false </ IsRequired >    < IsNoInput > false </ IsNoInput >    < IsNoChange > false </ IsNoChange >    < IsExternal > false </ IsExternal >    < MinimumLength > 0 </ MinimumLength >    < MaximumLength > 35 </ MaximumLength >    < RelatedApplicationName > PPL.CLIENTCONDADVICECONF </ RelatedApplicationName >    < RelatedApplicationIsConfiguration > false </ RelatedApplicationIsConfiguration >    < ExtraData />  </ Attributes ></ row >", "847BF157A8370AD25181ECD6679852EA", SenseAI.Domain.TypicalType.Application);
            //
            _typicalRepository.Add(typical1);
            _typicalRepository.Add(typical2);
            _typicalRepository.Add(typical3);
        }

        private void InsertDynamicDataSuggestions()
        {
            var id = new Guid("1C607F93-E206-4D6B-BBC2-07AF0FEC8DB2");
            var id2 = new Guid("F3E66326-C687-4218-8D85-08D6C8939CCF");
            var dynamicdata1 = new DynamicDataSuggestion(id, "CUSTOMER,INPUT", "@ID", "ACCOUNT,AUTH", "CUSTOMER", 50);
            var dynamicdata2 = new DynamicDataSuggestion(id2, "CURRENT.ACCOUNT", "@ID", "AA.ARRANGEMENT.ACTIVITY,AA", "@ID", 20);

            _dynamicDataSuggestionRepository.Add(dynamicdata1);
            _dynamicDataSuggestionRepository.Add(dynamicdata2);
        }

        private void InsertDataSets()
        {
            var datasetId = new Guid("2318551E-3151-42F6-97EC-14F01D32A18E");
            var datasetId2 = new Guid("d3a1ae22-840a-46c8-a515-04be11e9bd47");
            var subProjectId = new Guid("90c39e80-9ccb-4dbf-bfd7-396b2201e01d");
            var catalogId1 = new Guid("3cb308f8-026e-47b8-bd22-1c3bac2c06d2");
            var systemId = new Guid("8b1005c0-cdc2-440a-94fd-4a6a793654bf");
            var typicalId = new Guid("85434986-25DE-4F9B-B2EF-000171B2BA58");
            DataSetItem[] datasetitems = InsertDataSetItems();
            var dataset = new DataSet(datasetId, subProjectId, catalogId1, systemId, typicalId, "dataset", 40, "[{'Attribute':'@ID','Values':['191']}]", datasetitems);
            var dataset2 = new DataSet(datasetId2, subProjectId, catalogId1, systemId, typicalId, "dataset2", 60, "[{'Attribute':'@ID','Values':['191']}]", InsertDataSetItems2());

            _dataSetRepository.Add(dataset);
            _dataSetRepository.Add(dataset2);
        }

        private DataSetItem[] InsertDataSetItems()
        {
            var datasetItemId = new Guid("0bc78141-200c-4457-9811-00640e1fd9d2");
            var datasetItemId2 = new Guid("dbf08e7b-846c-4214-b056-006e80f139d1");
            var datasetId = new Guid("2318551E-3151-42F6-97EC-14F01D32A18E");

            var datasetItem = new DataSetItem(datasetItemId, datasetId, "[{'Name':'LANGUAGE','Value':'1'}]");
            var datasetItem2 = new DataSetItem(datasetItemId2, datasetId, "[{'Name':'LANGUAGE','Value':'1'},{'Name':'NATIONALITY','Value':'GB'},{'Name':'RESIDENCE','Value':'GB'},{'Name':'SECTOR','Value':'1001'},{'Name':'TITLE','Value':'MR'}]");

            DataSetItem[] datasetitems = { datasetItem, datasetItem2 };
            return datasetitems;
        }

        private DataSetItem[] InsertDataSetItems2()
        {
            var datasetItemId = new Guid("a973dd97-13c8-4d9c-a399-034bc69f205c");
            var datasetItemId2 = new Guid("515ae898-ca94-4fdd-90d4-05defbc23259");
            var datasetId = new Guid("2318551E-3151-42F6-97EC-14F01D32A18E");

            var datasetItem = new DataSetItem(datasetItemId, datasetId, "[{'Name':'LANGUAGE','Value':'1'}]");
            var datasetItem2 = new DataSetItem(datasetItemId2, datasetId, "[{'Name':'DEBIT.CURRENCY','Value':'USD'}]");

            DataSetItem[] datasetitems = { datasetItem, datasetItem2 };
            return datasetitems;
        }

        private void InsertSettings()
        {
            var setting1 = new Settings(Guid.NewGuid(), Variables.ProjectId, Variables.SubProjectId, Variables.SystemId, Variables.CatalogId);
            _settingsRepository.Add(setting1);
        }

        private void InsertWorkflows()
        {
            var workflow1 = new Workflow(Variables.WorkflowId, "Workflow1", "workflow 1", Variables.FolderId, true, true);

            var designjson = "{'cells':[{'type':'fsa.State','icon':'message','eventType':'end','size':{'width':60,'height':60},'position':{'x':310,'y':130},'angle':0,'preserveAspectRatio':true,'customData':{'Type':0,'Id':'5b051302-c6f8-4086-8a9a-d6df5755bd9a','SystemId':'0addab9b-eb40-48b2-ac73-e3b39ee13b4d','AdapterName':'T24WebAdapter','Title':'Start','Description':'','CatalogId':'00000000-0000-0000-0000-000000000000','CatalogName':'','TypicalId':'00000000-0000-0000-0000-000000000000','TypicalName':'','Parameters':'','DataSetsItemIds':[],'DynamicDatas':[],'Constraints':[],'PostConstraints':[]},'id':'5b051302-c6f8-4086-8a9a-d6df5755bd9a','z':1,'attrs':{'circle':{'stroke-width':2,'stroke':'#26ff51','stroke-dasharray':'0'},'text':{'font-weight':'Bold','text':'Start','fill':'black','font-family':'Roboto Condensed','stroke-width':0},'root':{'dataTooltipPosition':'left','dataTooltipPositionSelector':'.joint-stencil'}}},{'type':'standard.Rectangle','size':{'width':290,'height':40},'position':{'x':185,'y':230},'angle':0,'customData':{'Type':1,'Id':'7bf57c5b-a7ac-41cd-8132-776e664b51b7','SystemId':'00000000-0000-0000-0000-000000000000','Title':'Navigate to Customer Creation','Description':'','CatalogId':'00000000-0000-0000-0000-000000000000','CatalogName':'','TypicalId':'00000000-0000-0000-0000-000000000000','TypicalName':'','ActionType':0,'Parameters':'','DataSetsItemIds':[],'Constraints':[],'DynamicDatas':[],'PostConstraints':[]},'id':'7bf57c5b-a7ac-41cd-8132-776e664b51b7','z':2,'attrs':{'body':{'stroke':'#26ff51','fill':'white','height':30,'strokeDasharray':'0','rx':20,'ry':20},'image':{'xlinkHref':'assets/actionicons/navigationstep.svg'},'label':{'fill':'black','text':'Navigate to Customer Creation','fontFamily':'Roboto Condensed','fontWeight':'Bold','strokeWidth':0},'root':{'dataTooltipPosition':'left','dataTooltipPositionSelector':'.joint-stencil','type':'T24 Navigation Step'}}},{'type':'standard.Link','router':{'name':'normal'},'connector':{'name':'rounded'},'labels':[],'source':{'id':'5b051302-c6f8-4086-8a9a-d6df5755bd9a'},'target':{'id':'7bf57c5b-a7ac-41cd-8132-776e664b51b7'},'id':'7feeae18-0c90-43e4-ae74-c13affc8d07a','z':3,'attrs':{'line':{'stroke':'#26ff51','strokeDasharray':'0','strokeWidth':2,'fill':'none','sourceMarker':{'type':'path','d':'M 0 0 0 0','stroke':'none'},'targetMarker':{'type':'path','d':'M 0 -5 -10 0 0 5 z','stroke':'none'},'connection':true,'strokeLinejoin':'round'},'wrapper':{'connection':true,'strokeWidth':10,'strokeLinejoin':'round'}}},]}";
            var WorkflowPathsJson = "{'paths':[{'title':' Path 1','number':0,'coverage':'N / A','isBestPath':true,'itemIds':['5b051302-c6f8-4086-8a9a-d6df5755bd9a','7bf57c5b-a7ac-41cd-8132-776e664b51b7','18dfd71a-b268-4cbb-ade8-021d6dfa4ea5','3d5f497f-c00d-4a2b-bd96-6a8e69c9730e','149a5cb7-7c6b-4e99-8dbc-c87852c48a49','62d7cced-cc79-47cd-b636-08b4a3df267c'],'selected':true}]}";

            _workflowRepository.Add(workflow1);
            _workflowRepository.Save(workflow1, designjson, WorkflowPathsJson);
            _workflowRepository.Issue(workflow1.Id, "");
        }

        private void InsertWorkflowPaths()
        {
            var workflowid = new Guid("9e90b265-e7b3-4dbb-8e81-089d71cf7442");
            var workflowid2 = new Guid("9e90b265-e7b3-4dbb-8e81-089d71cf7443");
            var id = new Guid("98bb5c0d-5670-4151-b76c-065c782ce1f3");
            var id2 = new Guid("4dcf5021-1bc9-4f81-9c43-06ce9e998e24");
            var id3 = new Guid("4dcf5021-1bc9-4f81-9c43-06ce9e998e23");
            var pathid = new Guid("6519fd8d-6016-44d2-9a15-0660a75b9fba");
            var pathid3 = new Guid("6519fd8d-6016-44d2-9a15-0660a75b9fbc");
            var pathItemId = new Guid("5b051302-c6f8-4086-8a9a-d6df5755bd9a");
            var pathItemId2 = new Guid("7bf57c5b-a7ac-41cd-8132-776e664b51b7");
            var pathItemId3 = new Guid("7bf57c5b-a7ac-41cd-8132-776e664b51b6");
            var catalogId1 = new Guid("3cb308f8-026e-47b8-bd22-1c3bac2c06d2");
            var typicalId1 = new Guid("85434986-25DE-4F9B-B2EF-000171B2BA58");
            var datasetId = new Guid("2318551E-3151-42F6-97EC-14F01D32A18E");

            List<WorkflowVersionPaths> workflowPaths = new List<WorkflowVersionPaths>();

            WorkflowVersionPaths workflowVersionPath = new WorkflowVersionPaths(pathid, workflowid, 0, "Path1", true, 90, 0, true);
            workflowVersionPath.AddItem(id, workflowid, 0, pathid, "T24 Application Step", 1, (int)ActionType.Input, catalogId1, typicalId1, "CUSTOMER", "1", datasetId, "{'paths':[{'title':' Path 1','number':0,'coverage':'N / A','isBestPath':true,'itemIds':['5e89a286-fcd3-453a-92cc-253d17668818','b22791c0-f252-4611-a8c1-8dee03626aab','39b23783-7ab0-4b70-8b65-70857a0126ce'],'selected':true}]}", 0);
            workflowVersionPath.AddItem(id2, workflowid, 0, pathid, "T24 Application Step", 1, (int)ActionType.Input, catalogId1, typicalId1, "CUSTOMER", "1", datasetId, "{'paths':[{'title':' Path 1','number':0,'coverage':'N / A','isBestPath':true,'itemIds':['5e89a286-fcd3-453a-92cc-253d17668818','b22791c0-f252-4611-a8c1-8dee03626aab','39b23783-7ab0-4b70-8b65-70857a0126ce'],'selected':true}]}", 1);
            workflowPaths.Add(workflowVersionPath);

            workflowVersionPath = new WorkflowVersionPaths(pathid3, workflowid2, 0, "Path2", true, 90, 0, true);
            workflowVersionPath.AddItem(id3, workflowid2, 0, pathid3, "T24 Application Step", 1, (int)ActionType.Input, catalogId1, typicalId1, "CUSTOMER", "1", datasetId, "{'paths':[{'title':' Path 1','number':0,'coverage':'N / A','isBestPath':true,'itemIds':['5e89a286-fcd3-453a-92cc-253d17668818','b22791c0-f252-4611-a8c1-8dee03626aab','39b23783-7ab0-4b70-8b65-70857a0126ce'],'selected':true}]}", 0);
            workflowPaths.Add(workflowVersionPath);

            _workflowPathsRepository.Add(workflowPaths.ToArray());
        }

        private void InsertWorkflowVersions()
        {
            //_saiObjectContext.Add(new WorkflowVersionData
            //{
            //    WorkflowId = new Guid("9e90b265-e7b3-4dbb-8e81-089d71cf7442"),
            //    Title = "version1",
            //    VersionId = 0,
            //    Description = "version 1",
            //    IsLast = true,
            //    DesignerJson = "{'cells':[{'type':'fsa.State','icon':'message','eventType':'end','size':{'width':60,'height':60},'position':{'x':310,'y':130},'angle':0,'preserveAspectRatio':true,'customData':{'Type':0,'Id':'5b051302-c6f8-4086-8a9a-d6df5755bd9a','SystemId':'0addab9b-eb40-48b2-ac73-e3b39ee13b4d','AdapterName':'T24WebAdapter','Title':'Start','Description':'','CatalogId':'00000000-0000-0000-0000-000000000000','CatalogName':'','TypicalId':'00000000-0000-0000-0000-000000000000','TypicalName':'','Parameters':'','DataSetsItemIds':[],'DynamicDatas':[],'Constraints':[],'PostConstraints':[]},'id':'5b051302-c6f8-4086-8a9a-d6df5755bd9a','z':1,'attrs':{'circle':{'stroke-width':2,'stroke':'#26ff51','stroke-dasharray':'0'},'text':{'font-weight':'Bold','text':'Start','fill':'black','font-family':'Roboto Condensed','stroke-width':0},'root':{'dataTooltipPosition':'left','dataTooltipPositionSelector':'.joint-stencil'}}},{'type':'standard.Rectangle','size':{'width':290,'height':40},'position':{'x':185,'y':230},'angle':0,'customData':{'Type':1,'Id':'7bf57c5b-a7ac-41cd-8132-776e664b51b7','SystemId':'00000000-0000-0000-0000-000000000000','Title':'Navigate to Customer Creation','Description':'','CatalogId':'00000000-0000-0000-0000-000000000000','CatalogName':'','TypicalId':'00000000-0000-0000-0000-000000000000','TypicalName':'','ActionType':0,'Parameters':'','DataSetsItemIds':[],'Constraints':[],'DynamicDatas':[],'PostConstraints':[]},'id':'7bf57c5b-a7ac-41cd-8132-776e664b51b7','z':2,'attrs':{'body':{'stroke':'#26ff51','fill':'white','height':30,'strokeDasharray':'0','rx':20,'ry':20},'image':{'xlinkHref':'assets/actionicons/navigationstep.svg'},'label':{'fill':'black','text':'Navigate to Customer Creation','fontFamily':'Roboto Condensed','fontWeight':'Bold','strokeWidth':0},'root':{'dataTooltipPosition':'left','dataTooltipPositionSelector':'.joint-stencil','type':'T24 Navigation Step'}}},{'type':'standard.Link','router':{'name':'normal'},'connector':{'name':'rounded'},'labels':[],'source':{'id':'5b051302-c6f8-4086-8a9a-d6df5755bd9a'},'target':{'id':'7bf57c5b-a7ac-41cd-8132-776e664b51b7'},'id':'7feeae18-0c90-43e4-ae74-c13affc8d07a','z':3,'attrs':{'line':{'stroke':'#26ff51','strokeDasharray':'0','strokeWidth':2,'fill':'none','sourceMarker':{'type':'path','d':'M 0 0 0 0','stroke':'none'},'targetMarker':{'type':'path','d':'M 0 -5 -10 0 0 5 z','stroke':'none'},'connection':true,'strokeLinejoin':'round'},'wrapper':{'connection':true,'strokeWidth':10,'strokeLinejoin':'round'}}},]}",
            //    WorkflowPathsJson = "{'paths':[{'title':' Path 1','number':0,'coverage':'N / A','isBestPath':true,'itemIds':['5b051302-c6f8-4086-8a9a-d6df5755bd9a','7bf57c5b-a7ac-41cd-8132-776e664b51b7','18dfd71a-b268-4cbb-ade8-021d6dfa4ea5','3d5f497f-c00d-4a2b-bd96-6a8e69c9730e','149a5cb7-7c6b-4e99-8dbc-c87852c48a49','62d7cced-cc79-47cd-b636-08b4a3df267c'],'selected':true}]}",
            //    IsSyncWithML = false
            //});

            //_saiObjectContext.SaveChanges();
        }

        private void InsertTypicalChanges()
        {
            var catalogId = new Guid("3cb308f8-026e-47b8-bd22-1c3bac2c06d2");
            var typicalId1 = new Guid("85434986-25DE-4F9B-B2EF-000171B2BA59");
            var typicalId2 = new Guid("4545DEEB-2E9C-43DB-BE0C-000522A6ED3D");
            var typical1 = new TypicalChanges(typicalId1, catalogId, "Customer1", "{'TenantId':'30788c4a-ea74-4aa8-b505-891303dbaeaa ','ProjectId':'0b450e5c-9b76-4851-b341-a42fdc129d68','SubProjectId':'3adf6db6-b640-439d-b6d3-1aaa9d9fd626','SystemId':'b5bcc43f-afc8-4f67-8eb1-b633994ce363','CatalogId':'88a86645-6867-4e6a-97be-68e2236a3807','HashId':'A820CBB11F99D6985AF83949E2F6B2D9','Id':'480af1d1-53a5-43c3-60f5-1387c6df6b31','Type':4,'Title':'PPL.CLIENTCONDADVICECONF','HasAutomaticId':false,'IsConfiguration':false,'Attributes':[{'Name':'ClientConditions','IsMultiValue':false,'PossibleValues':null,'IsRequired':false,'IsNoInput':false,'IsNoChange':false,'IsExternal':false,'MinimumLength':0,'MaximumLength':35,'RelatedApplicationName':'PPL.CLIENTCONDADVICECONF','RelatedApplicationIsConfiguration':false,'ExtraData':null}]}", "<row>< TenantId >30788c4a-ea74-4aa8-b505-891303dbaeaa </ TenantId >  < ProjectId > 0b450e5c-9b76-4851-b341-a42fdc129d68 </ ProjectId >  < SubProjectId > 3adf6db6-b640-439d-b6d3-1aaa9d9fd626 </ SubProjectId >  < SystemId > b5bcc43f-afc8-4f67-8eb1-b633994ce363 </ SystemId >  < CatalogId > 88a86645-cd68-4e6a-97be-e68e2236a3807 </ CatalogId >  < HashId > A820CBB11F99D6985AF83949E2F6B2D9 </ HashId >  < Id > 480af1d1-b53a-43c3-a60f-1387c6df6b31 </ Id >  < Type > 4 </ Type >  < Title > PPL.CLIENTCONDADVICECONF </ Title >  < HasAutomaticId > false </ HasAutomaticId >  < IsConfiguration > false </ IsConfiguration >  < Attributes >    < Name > ClientConditionsID </ Name >    < IsMultiValue > false </ IsMultiValue >    < PossibleValues />    < IsRequired > false </ IsRequired >    < IsNoInput > false </ IsNoInput >    < IsNoChange > false </ IsNoChange >    < IsExternal > false </ IsExternal >    < MinimumLength > 0 </ MinimumLength >    < MaximumLength > 35 </ MaximumLength >    < RelatedApplicationName > PPL.CLIENTCONDADVICECONF </ RelatedApplicationName >    < RelatedApplicationIsConfiguration > false </ RelatedApplicationIsConfiguration >    < ExtraData />  </ Attributes ></ row >", "847BF157A8360AD25181ECD6679852EA", SenseAI.Domain.TypicalType.Application, 1);
            var typical2 = new TypicalChanges(typicalId2, catalogId, "Account", "{'TenantId':'30788c4a-ea74-4aa8-b505-891303dbaeaa','ProjectId':'0b450e5c-9b76-4851-b341-a42fdc129d68','SubProjectId':'3adf6db6-b640-439d-b6d3-1aaa9d9fd626','SystemId':'b5bcc43f-afc8-4f67-8eb1-b633994ce363','CatalogId':'88a86645-6867-4e6a-97be-68e2236a3807','HashId':'A820CBB11F99D6985AF83949E2F6B2D9','Id':'480af1d1-53a5-43c3-60f4-1387c6df6b31','Type':4,'Title':'PPL.CLIENTCONDADVICECONF','HasAutomaticId':false,'IsConfiguration':false,'Attributes':[{'Name':'ClientConditionsID','IsMultiValue':false,'PossibleValues':null,'IsRequired':false,'IsNoInput':false,'IsNoChange':false,'IsExternal':false,'MinimumLength':0,'MaximumLength':25,'RelatedApplicationName':'PPL.CLIENTCONDADVICECONF','RelatedApplicationIsConfiguration':false,'ExtraData':null}]}", "<row>< TenantId >30788c4a-ea74-4aa8-b505-891303dbaeaa </ TenantId >  < ProjectId > 0b450e5c-9b76-4851-b341-a42fdc129d68 </ ProjectId >  < SubProjectId > 3adf6db6-b640-439d-b6d3-1aaa9d9fd626 </ SubProjectId >  < SystemId > b5bcc43f-afc8-4f67-8eb1-b633994ce363 </ SystemId >  < CatalogId > 88a86645-cd68-4e6a-97be-e68e2236a3807 </ CatalogId >  < HashId > A820CBB11F99D6985AF83949E2F6B2D9 </ HashId >  < Id > 480af1d1-b53a-43c3-a60f-1387c6df6b31 </ Id >  < Type > 4 </ Type >  < Title > PPL.CLIENTCONDADVICECONF </ Title >  < HasAutomaticId > false </ HasAutomaticId >  < IsConfiguration > false </ IsConfiguration >  < Attributes >    < Name > ClientConditionsID </ Name >    < IsMultiValue > false </ IsMultiValue >    < PossibleValues />    < IsRequired > false </ IsRequired >    < IsNoInput > false </ IsNoInput >    < IsNoChange > false </ IsNoChange >    < IsExternal > false </ IsExternal >    < MinimumLength > 0 </ MinimumLength >    < MaximumLength > 35 </ MaximumLength >    < RelatedApplicationName > PPL.CLIENTCONDADVICECONF </ RelatedApplicationName >    < RelatedApplicationIsConfiguration > false </ RelatedApplicationIsConfiguration >    < ExtraData />  </ Attributes ></ row >", "847BF157A8371AD25181ECD6679852EA", SenseAI.Domain.TypicalType.Application, 1);

            _typicalChangesRepository.Add(typical1);
            _typicalChangesRepository.Add(typical2);
        }

        private void InsertAcceptedTypicalChanges()
        {
            var id = new Guid("C50C7A2C-6B77-4600-9650-F78EAF29CACE");
            var typicalId1 = new Guid("85434986-25DE-4F9B-B2EF-000171B2BA59");
            var typical1 = new AcceptedTypicalChanges(id, typicalId1, "Customer1", "<row>< TenantId >30788c4a-ea74-4aa8-b505-891303dbaeaa </ TenantId >  < ProjectId > 0b450e5c-9b76-4851-b341-a42fdc129d68 </ ProjectId >  < SubProjectId > 3adf6db6-b640-439d-b6d3-1aaa9d9fd626 </ SubProjectId >  < SystemId > b5bcc43f-afc8-4f67-8eb1-b633994ce363 </ SystemId >  < CatalogId > 88a86645-cd68-4e6a-97be-e68e2236a3807 </ CatalogId >  < HashId > A820CBB11F99D6985AF83949E2F6B2D9 </ HashId >  < Id > 480af1d1-b53a-43c3-a60f-1387c6df6b31 </ Id >  < Type > 4 </ Type >  < Title > PPL.CLIENTCONDADVICECONF </ Title >  < HasAutomaticId > false </ HasAutomaticId >  < IsConfiguration > false </ IsConfiguration >  < Attributes >    < Name > ClientConditionsID </ Name >    < IsMultiValue > false </ IsMultiValue >    < PossibleValues />    < IsRequired > false </ IsRequired >    < IsNoInput > false </ IsNoInput >    < IsNoChange > false </ IsNoChange >    < IsExternal > false </ IsExternal >    < MinimumLength > 0 </ MinimumLength >    < MaximumLength > 35 </ MaximumLength >    < RelatedApplicationName > PPL.CLIENTCONDADVICECONF </ RelatedApplicationName >    < RelatedApplicationIsConfiguration > false </ RelatedApplicationIsConfiguration >    < ExtraData />  </ Attributes ></ row >", "<row>< TenantId >30788c4a-ea74-4aa8-b505-891303dbaeaa </ TenantId >  < ProjectId > 0b450e5c-9b76-4851-b341-a42fdc129d68 </ ProjectId >  < SubProjectId > 3adf6db6-b640-439d-b6d3-1aaa9d9fd626 </ SubProjectId >  < SystemId > b5bcc43f-afc8-4f67-8eb1-b633994ce363 </ SystemId >  < CatalogId > 88a86645-cd68-4e6a-97be-e68e2236a3807 </ CatalogId >  < HashId > A820CBB11F99D6985AF83949E2F6B2D9 </ HashId >  < Id > 480af1d1-b53a-43c3-a60f-1387c6df6b31 </ Id >  < Type > 4 </ Type >  < Title > PPL.CLIENTCONDADVICECONF </ Title >  < HasAutomaticId > false </ HasAutomaticId >  < IsConfiguration > false </ IsConfiguration >  < Attributes >    < Name > ClientConditionsID </ Name >    < IsMultiValue > false </ IsMultiValue >    < PossibleValues />    < IsRequired > false </ IsRequired >    < IsNoInput > false </ IsNoInput >    < IsNoChange > false </ IsNoChange >    < IsExternal > false </ IsExternal >    < MinimumLength > 0 </ MinimumLength >    < MaximumLength > 35 </ MaximumLength >    < RelatedApplicationName > PPL.CLIENTCONDADVICECONF </ RelatedApplicationName >    < RelatedApplicationIsConfiguration > false </ RelatedApplicationIsConfiguration >    < ExtraData />  </ Attributes ></ row >", "847BF157A8360AD25181ECD6679852EA", "847BF157A8360AD25181ECD6679844EA", 1, "{'TenantId':'30788c4a-ea74-4aa8-b505-891303dbaeaa','ProjectId':'0b450e5c-9b76-4851-b341-a42fdc129d68','SubProjectId':'3adf6db6-b640-439d-b6d3-1aaa9d9fd626','SystemId':'b5bcc43f-afc8-4f67-8eb1-b633994ce363','CatalogId':'88a86645-6867-4e6a-97be-68e2236a3807','HashId':'A820CBB11F99D6985AF83949E2F6B2D9','Id':'480af1d1-53a5-43c3-60f5-1387c6df6b31','Type':4,'Title':'PPL.CLIENTCONDADVICECONF','HasAutomaticId':false,'IsConfiguration':false,'Attributes':[{'Name':'ClientConditions','IsMultiValue':false,'PossibleValues':null,'IsRequired':false,'IsNoInput':false,'IsNoChange':false,'IsExternal':false,'MinimumLength':0,'MaximumLength':35,'RelatedApplicationName':'PPL.CLIENTCONDADVICECONF','RelatedApplicationIsConfiguration':false,'ExtraData':null}]}", "{'TenantId':'30788c4a-ea74-4aa8-b505-891303dbaeaa','ProjectId':'0b450e5c-9b76-4851-b341-a42fdc129d68','SubProjectId':'3adf6db6-b640-439d-b6d3-1aaa9d9fd626','SystemId':'b5bcc43f-afc8-4f67-8eb1-b633994ce363','CatalogId':'88a86645-6867-4e6a-97be-68e2236a3807','HashId':'A820CBB11F99D6985AF83949E2F6B2D9','Id':'480af1d1-53a5-43c3-60f5-1387c6df6b31','Type':4,'Title':'PPL.CLIENTCONDADVICECONF','HasAutomaticId':false,'IsConfiguration':false,'Attributes':[{'Name':'ClientConditionsID','IsMultiValue':false,'PossibleValues':null,'IsRequired':false,'IsNoInput':false,'IsNoChange':false,'IsExternal':false,'MinimumLength':0,'MaximumLength':35,'RelatedApplicationName':'PPL.CLIENTCONDADVICECONF','RelatedApplicationIsConfiguration':false,'ExtraData':null}]}");

            _acceptedTypicalChangesRepository.Add(typical1);
        }

        private void InsertArisWorkflow()
        {
            var arisWorkflow1 = new ArisWorkflow(Guid.NewGuid(), Variables.ProcessId, "First aris", "First aris", "");
            var arisWorkflow2 = new ArisWorkflow(Guid.NewGuid(), Variables.DelProcessId, "Second aris", "Second aris", "");

            _arisWorkflowRepository.Add(arisWorkflow1);
            _arisWorkflowRepository.Add(arisWorkflow2);
        }

        private void InsertGenericWorkflow()
        {
            var id = new Guid("C50C7A2C-6B77-4600-9650-F78EAF29CACE");
            var genericWorkflow1 = new GenericWorkflow(id, Variables.ProcessId, "First generic", "First generic", "");
            var genericWorkflow2 = new GenericWorkflow(Guid.NewGuid(), Variables.DelProcessId, "Second generic", "Second generic", "");

            _genericWorkflowRepository.Add(genericWorkflow1);
            _genericWorkflowRepository.Add(genericWorkflow2);
        }

        private void PreviousWorkflowItems()
        {
            //var workflowid = new Guid("89A6B6B8-E45E-4E73-8D2A-F5598027FE10");
            //var folderid = new Guid("3adf6db6-b640-439d-b6d3-1aaa9d9fd626");
            //var wf3 = new Workflow(workflowid, "Workflow3", "workflow 3", folderid, 0, true, true);

            //_workflowRepository.Add(wf3);
            //var designjson3 = "{'cells':[{'type':'fsa.State','icon':'message','eventType':'end','size':{'width':60,'height':60},'position':{'x':170,'y':100},'angle':0,'preserveAspectRatio':true,'customData':{'Type':0,'Id':'304447c5-0daa-4336-8b80-9964c24c71fa','SystemId':'5789a839-68ef-4f62-bc8a-ea2cee9befe1','AdapterName':'T24WebAdapter','Title':'Start','Description':'','CatalogId':'00000000-0000-0000-0000-000000000000','CatalogName':'','TypicalId':'00000000-0000-0000-0000-000000000000','TypicalName':'','Parameters':'','DataSetsItemIds':[],'DynamicDatas':[],'Constraints':[],'PostConstraints':[]},'id':'304447c5-0daa-4336-8b80-9964c24c71fa','z':1,'attrs':{'circle':{'stroke-width':2,'stroke':'#26ff51','stroke-dasharray':'0'},'text':{'font-weight':'Bold','text':'Start','fill':'black','font-family':'Roboto Condensed','stroke-width':0},'root':{'dataTooltipPosition':'left','dataTooltipPositionSelector':'.joint-stencil'}}},{'type':'standard.Rectangle','size':{'width':200,'height':40},'position':{'x':100,'y':210},'angle':0,'customData':{'Type':1,'Id':'6b9d4fc1-ac06-4160-b50c-411d42477ccd','SystemId':'00000000-0000-0000-0000-000000000000','Title':'CUSTOMER','Description':'','CatalogId':'764cffef-ec71-4d16-acf0-ef9f80211c5c','CatalogName':'R17 Model Bank Applications','TypicalId':'3ac2f06e-57a2-41b9-ad9c-99e4bcd410ab','TypicalName':'CUSTOMER','ActionType':7,'Parameters':'','DataSetsItemIds':[],'DynamicDatas':[],'Constraints':[],'PostConstraints':[]},'id':'6b9d4fc1-ac06-4160-b50c-411d42477ccd','z':2,'embeds':['f11af0ab-358e-4f30-a0f6-17ee4f21ab63','869a5ab8-a473-4d48-a634-f4b4f1364dbd'],'attrs':{'body':{'stroke':'#26ff51','fill':'white','height':30,'strokeDasharray':'0','rx':20,'ry':20},'image':{'xlinkHref':'assets/actionicons/actionstep.svg'},'label':{'fill':'black','text':'CUSTOMER','fontFamily':'Roboto Condensed','fontWeight':'Bold','strokeWidth':0},'root':{'dataTooltipPosition':'left','dataTooltipPositionSelector':'.joint-stencil','type':'T24 Application Step'}}},{'type':'standard.Link','source':{'id':'304447c5-0daa-4336-8b80-9964c24c71fa'},'target':{'id':'6b9d4fc1-ac06-4160-b50c-411d42477ccd'},'router':{'name':'normal'},'connector':{'name':'rounded'},'labels':[],'id':'ee1c1f48-a64c-43cb-b854-5ef720916188','z':4,'vertices':[{'x':200,'y':185}],'attrs':{'line':{'stroke':'#26ff51','targetMarker':{'d':'M 0 -5 -10 0 0 5 z','stroke':'none'},'strokeDasharray':'0','fill':'none','sourceMarker':{'type':'path','d':'M 0 0 0 0','stroke':'none'}}}},{'type':'standard.Rectangle','size':{'width':200,'height':40},'position':{'x':100,'y':301},'angle':0,'customData':{'Type':1,'Id':'028dd1c1-c9af-49df-a8a6-0cd3cb13cf01','SystemId':'00000000-0000-0000-0000-000000000000','Title':'ACCOUNT','Description':'','CatalogId':'764cffef-ec71-4d16-acf0-ef9f80211c5c','CatalogName':'R17 Model Bank Applications','TypicalId':'a01c3fd2-d26f-4c70-9ea9-e78f1b15eaeb','TypicalName':'ACCOUNT','ActionType':7,'Parameters':'','DataSetsItemIds':[],'DynamicDatas':[],'Constraints':[],'PostConstraints':[]},'id':'028dd1c1-c9af-49df-a8a6-0cd3cb13cf01','z':6,'embeds':['dbc6c3d0-4fc0-498c-ba70-693e38f49a38'],'attrs':{'body':{'stroke':'#26ff51','fill':'white','height':30,'strokeDasharray':'0','rx':20,'ry':20},'image':{'xlinkHref':'assets/actionicons/actionstep.svg'},'label':{'fill':'black','text':'ACCOUNT','fontFamily':'Roboto Condensed','fontWeight':'Bold','strokeWidth':0},'root':{'dataTooltipPosition':'left','dataTooltipPositionSelector':'.joint-stencil','type':'T24 Application Step'}}},{'type':'standard.Link','router':{'name':'normal'},'connector':{'name':'rounded'},'labels':[],'source':{'id':'6b9d4fc1-ac06-4160-b50c-411d42477ccd'},'target':{'id':'028dd1c1-c9af-49df-a8a6-0cd3cb13cf01'},'id':'01d9c7fc-5e6e-4821-b5e1-b0f67a8638c4','z':8,'attrs':{'line':{'stroke':'#26ff51','strokeDasharray':'0','strokeWidth':2,'fill':'none','sourceMarker':{'type':'path','d':'M 0 0 0 0','stroke':'none'},'targetMarker':{'type':'path','d':'M 0 -5 -10 0 0 5 z','stroke':'none'},'connection':true,'strokeLinejoin':'round'},'wrapper':{'connection':true,'strokeWidth':10,'strokeLinejoin':'round'}}},{'type':'standard.Rectangle','size':{'width':200,'height':40},'position':{'x':100,'y':434},'angle':0,'customData':{'Type':1,'Id':'7f3d80e9-3e8f-4c62-8b74-34b01f0e436e','SystemId':'00000000-0000-0000-0000-000000000000','Title':'SAVINGS.ACCOUNT','Description':'','CatalogId':'764cffef-ec71-4d16-acf0-ef9f80211c5c','CatalogName':'R17 Model Bank Applications','TypicalId':'f3a1ccc5-f1d7-4f1e-aa5b-b42466be2556','TypicalName':'SAVINGS.ACCOUNT','ActionType':7,'Parameters':'','DataSetsItemIds':[],'DynamicDatas':[],'Constraints':[],'PostConstraints':[]},'id':'7f3d80e9-3e8f-4c62-8b74-34b01f0e436e','z':9,'embeds':['e1b3c93d-e84f-4a11-8f09-280f15a55302'],'attrs':{'body':{'stroke':'#26ff51','fill':'white','height':30,'strokeDasharray':'0','rx':20,'ry':20},'image':{'xlinkHref':'assets/actionicons/actionstep.svg'},'label':{'fill':'black','text':'SAVINGS.ACCOUNT','fontFamily':'Roboto Condensed','fontWeight':'Bold','strokeWidth':0},'root':{'dataTooltipPosition':'left','dataTooltipPositionSelector':'.joint-stencil','type':'T24 Application Step'}}},{'type':'standard.Link','router':{'name':'normal'},'connector':{'name':'rounded'},'labels':[],'source':{'id':'028dd1c1-c9af-49df-a8a6-0cd3cb13cf01'},'target':{'id':'7f3d80e9-3e8f-4c62-8b74-34b01f0e436e'},'id':'98a398d9-41ef-46ee-a90b-623974c5458b','z':11,'attrs':{'line':{'stroke':'#26ff51','strokeDasharray':'0','strokeWidth':2,'fill':'none','sourceMarker':{'type':'path','d':'M 0 0 0 0','stroke':'none'},'targetMarker':{'type':'path','d':'M 0 -5 -10 0 0 5 z','stroke':'none'},'connection':true,'strokeLinejoin':'round'},'wrapper':{'connection':true,'strokeWidth':10,'strokeLinejoin':'round'}}}]}";
            //var WorkflowPathsJson3 = "{'paths':[{'title':' Path 1','number':0,'coverage':'N/A','isBestPath':true,'itemIds':['304447c5-0daa-4336-8b80-9964c24c71fa','6b9d4fc1-ac06-4160-b50c-411d42477ccd','028dd1c1-c9af-49df-a8a6-0cd3cb13cf01','7f3d80e9-3e8f-4c62-8b74-34b01f0e436e'],'selected':true}]}";
            ////add workflow
            //_workflowRepository.Save(wf3, designjson3, WorkflowPathsJson3);

            ////add workflowpaths
            //var pathid = new Guid("BFF18387-4195-4B8A-BCC9-22136368E5CB");
            //var catalogId = new Guid("764CFFEF-EC71-4D16-ACF0-EF9F80211C5C");
            //var wfp1 = new WorkflowPaths(Guid.NewGuid(), workflowid, pathid, "Path 1", 0, new Guid("028DD1C1-C9AF-49DF-A8A6-0CD3CB13CF01"), "ACCOUNT", 1, 7, WorkflowPathsJson3, catalogId, "ACCOUNT", new Guid("4545DEEB-2E9C-43DB-BE0C-000522A6ED3D"), Guid.NewGuid(), 0, true, 2, true);
            //var wfp2 = new WorkflowPaths(Guid.NewGuid(), workflowid, pathid, "Path 1", 0, new Guid("85434986-25DE-4F9B-B2EF-000171B2BA58"), "CUSTOMER", 1, 7, WorkflowPathsJson3, catalogId, "CUSTOMER", new Guid("4545DEEB-2E9C-43DB-BE0C-000522A6ED3D"), Guid.NewGuid(), 0, true, 1, true);
            //var wfp3 = new WorkflowPaths(Guid.NewGuid(), workflowid, pathid, "Path 1", 0, new Guid("00000000-0000-0000-0000-000000000000"), "Start", 1, 7, WorkflowPathsJson3, catalogId, "", new Guid("4545DEEB-2E9C-43DB-BE0C-000522A6ED3D"), Guid.NewGuid(), 0, true, 0, true);
            //var wfp4 = new WorkflowPaths(Guid.NewGuid(), workflowid, pathid, "Path 1", 0, new Guid("F3A1CCC5-F1D7-4F1E-AA5B-B42466BE2556"), "SAVING ACCOUNT", 1, 7, WorkflowPathsJson3, catalogId, "SAVINGS.ACCOUNT", new Guid("4545DEEB-2E9C-43DB-BE0C-000522A6ED3D"), Guid.NewGuid(), 0, true, 3, true);
            //WorkflowPaths[] wfp = { wfp1, wfp2, wfp3, wfp4 };
            //_workflowPathsRepository.Add(wfp);
        }

        #region AAProductBuilder

        private void InsertQueries()
        {
            var query1 = new QueryData
            {
                Id = "List_Of_Payment_Types",
                Title = "List Of Payment Types",
                Text = @"SELECT	[Key]
                                ,[xml].value('(//row/DESCRIPTION-1_x007E_1)[1]', 'nvarchar(max)') [Value]
                        FROM    ProductionData
                        WHERE   ApplicationName = 'AA.PAYMENT.TYPE'
                                AND[Key] NOT LIKE '%;%'
                                AND[xml].value('(//row/RECORD.STATUS)[1]', 'nvarchar(max)') IS NULL
                                AND[xml].value('(//row/CALC.TYPE)[1]', 'nvarchar(max)') IN ('CONSTANT', 'LINEAR')
                                AND([Key] LIKE '%CONSTANT%' OR [Key] LIKE '%LINEAR%')"
            };

            var query2 = new QueryData
            {
                Id = "List_Of_Interest_Basis_By_Currency",
                Title = "List Of Interest Basis By Currency",
                Text = @"SELECT	[INTEREST.BASIS].[Key]
                                ,[INTEREST.BASIS].[DESCRIPTION][Value]
                        FROM    (
                                    SELECT  [Key]
                                            ,[xml].value('(//row/DESCRIPTION-1_x007E_1)[1]', 'nvarchar(max)') [DESCRIPTION]
                                    FROM    ProductionData
                                    WHERE   ApplicationName = 'INTEREST.BASIS'
                                            AND[Key] NOT LIKE '%;%'
                                            AND[xml].value('(//row/RECORD.STATUS)[1]', 'nvarchar(max)') IS NULL
                                )   [INTEREST.BASIS]
                                INNER JOIN  (
                                                SELECT  LEFT(ID, CHARINDEX(' ', ID) - 1) ID
                                                FROM    (
                                                            SELECT  [xml].value('(//row/INTEREST.DAY.BASIS)[1]', 'nvarchar(max)') ID
                                                            FROM    ProductionData
                                                            WHERE   ApplicationName = 'CURRENCY'
                                                                    AND[Key] NOT LIKE '%;%'
                                                                    AND[xml].value('(//row/RECORD.STATUS)[1]', 'nvarchar(max)') IS NULL
                                                                    AND[Key] = @CURRENCY
                                                        )   S
                                            )   CURRENCY
                                    ON CURRENCY.ID = [INTEREST.BASIS].[Key]"
            };

            _saiObjectContext.Add(query1);
            _saiObjectContext.Add(query2);

            _saiObjectContext.SaveChanges();
        }

        private void InsertParameters()
        {
            var parameter1 = new ParameterData
            {
                Id = "@CURRENCY"
            };

            var parameter2 = new ParameterData
            {
                Id = "@CHANGEPRODUCT"
            };

            _saiObjectContext.Add(parameter1);
            _saiObjectContext.Add(parameter2);

            _saiObjectContext.SaveChanges();
        }

        private void InsertQueryParameters()
        {
            var queryParameter = new QueryParameterData
            {
                QueryId = "List_Of_Interest_Basis_By_Currency",
                ParameterId = "@CURRENCY"
            };

            _saiObjectContext.Add(queryParameter);

            _saiObjectContext.SaveChanges();
        }

        private void InsertProducts()
        {
            var product = new ProductGroupData
            {
                Id = Guid.Parse("2CF99C3C-212D-449C-902B-589E79A7690E"),
                Title = "CURRENT.ACCOUNTS"
            };

            _saiObjectContext.Add(product);

            _saiObjectContext.SaveChanges();
        }

        private void InsertSections()
        {
            var section1 = new PropertyData
            {
                Id = "ACCHARGE",
                Description = "ACCHARGE"
            };

            var section2 = new PropertyData
            {
                Id = "BALANCE",
                Description = "BALANCE"
            };

            var section3 = new PropertyData
            {
                Id = "DRINTEREST",
                Description = "DRINTEREST"
            };

            _saiObjectContext.Add(section1);
            _saiObjectContext.Add(section2);
            _saiObjectContext.Add(section3);

            _saiObjectContext.SaveChanges();
        }

        //private void InsertProductSections()
        //{
        //    var productSection1 = new ProductGroupPropertyClassData
        //    {
        //        ProductGroupId = Guid.Parse("2CF99C3C-212D-449C-902B-589E79A7690E"),
        //        PropertyId = "ACCHARGE"
        //    };

        //    var productSection2 = new ProductGroupPropertyClassData
        //    {
        //        ProductGroupId = Guid.Parse("2CF99C3C-212D-449C-902B-589E79A7690E"),
        //        PropertyId = "BALANCE"
        //    };

        //    var productSection3 = new ProductGroupPropertyClassData
        //    {
        //        ProductGroupId = Guid.Parse("2CF99C3C-212D-449C-902B-589E79A7690E"),
        //        PropertyId = "DRINTEREST"
        //    };

        //    _saiObjectContext.Add(productSection1);
        //    _saiObjectContext.Add(productSection2);
        //    _saiObjectContext.Add(productSection3);

        //    _saiObjectContext.SaveChanges();
        //}

        private void InsertProductStates()
        {
            var productState1 = new ProductGroupPropertyStateData
            {
                SessionId = Guid.Parse("6F49C73E-EC09-41E9-833A-9998BD56C7D0"),
                ProductGroupId = Guid.Parse("2CF99C3C-212D-449C-902B-589E79A7690E"),
                PropertyId = "ACCHARGE",
                CurrencyId = Guid.NewGuid().ToString(),
                IsSelected = false
            };

            var productState2 = new ProductGroupPropertyStateData
            {
                SessionId = Guid.Parse("6F49C73E-EC09-41E9-833A-9998BD56C7D0"),
                ProductGroupId = Guid.Parse("2CF99C3C-212D-449C-902B-589E79A7690E"),
                PropertyId = "BALANCE",
                CurrencyId = Guid.NewGuid().ToString(),
                IsSelected = false
            };

            var productState3 = new ProductGroupPropertyStateData
            {
                SessionId = Guid.Parse("6F49C73E-EC09-41E9-833A-9998BD56C7D0"),
                ProductGroupId = Guid.Parse("2CF99C3C-212D-449C-902B-589E79A7690E"),
                PropertyId = "DRINTEREST",
                CurrencyId = Guid.NewGuid().ToString(),
                IsSelected = false
            };

            _saiObjectContext.Add(productState1);
            _saiObjectContext.Add(productState2);
            _saiObjectContext.Add(productState3);

            _saiObjectContext.SaveChanges();
        }

        private void InsertProductPropertyStates()
        {
            var productPropertyState = new ProductGroupPropertyFieldStateData
            {
                SessionId = Guid.Parse("6F49C73E-EC09-41E9-833A-9998BD56C7D0"),
                ProductGroupId = Guid.Parse("2CF99C3C-212D-449C-902B-589E79A7690E"),
                PropertyId = "ACCHARGE",
                FieldId = "Currency",
                Value = "EUR"
            };

            _saiObjectContext.Add(productPropertyState);

            _saiObjectContext.SaveChanges();
        }

        #endregion AAProductBuilder

        private void InsertIssueTypes()
        {
            var issueType1 = new IssueType(Variables.IssueTypeId, Variables.SubProjectId, "Issue Type Test1", "Issue type description 1", "Story point", true);
            var issueType2 = new IssueType(Variables.DelIssueTypeId, Variables.DelSubProjectId, "Issue Type Test2", "Issue type description 2", "Man days", false);
            var businessChangeRequestIssueType = new IssueType(Variables.BusinessChangeRequestIssueTypeId, Variables.SubProjectId, "Business CR", "This is the Issue Type for Issues representing Business Change Requests", "Hours", true);
            var technicalChangeRequestIssueType = new IssueType(Variables.TechnicalChangeRequestIssueTypeId, Variables.SubProjectId, "Technical CR", "This is the Issue Type for Issues representing Technical Change Requests", "Story points", true);

            _issueTypesRepository.Add(issueType1);
            _issueTypesRepository.Add(issueType2);
            _issueTypesRepository.Add(businessChangeRequestIssueType);
            _issueTypesRepository.Add(technicalChangeRequestIssueType);
        }

        private void InsertIssues()
        {
            var issue1 = new Issue(Variables.IssueId, Variables.ProcessId, Variables.SubProjectId, Variables.IssueTypeId, Guid.NewGuid(), "Issue title 1", "Issue description 1", StatusType.ToDo, 1, "", "", "", Variables.TypicalId, "", true, InvestigationStatus.Accepted, ChangeStatus.Submitted, Guid.Empty, Guid.Empty, DateTime.Now, DateTime.Now, DateTime.Now, IssuePriority.Low, IssueSeverity.Low, DateTime.Now, TypeDefect.Cosmetic, Guid.Empty, "");
            var issue2 = new Issue(Variables.DelIssueId, Variables.DelProcessId, Variables.DelSubProjectId, Variables.DelIssueTypeId, Guid.NewGuid(), "Issue title 2", "Issue description 2", StatusType.ToDo, 1, "", "", "", Variables.TypicalId, "", true, InvestigationStatus.Accepted, ChangeStatus.Submitted, Guid.Empty, Guid.Empty, DateTime.Now, DateTime.Now, DateTime.Now, IssuePriority.Low, IssueSeverity.Low, DateTime.Now, TypeDefect.Cosmetic, Guid.Empty, "");

            _issueRepository.Add(issue1);
            _issueRepository.Add(issue2);
        }

        private void InsertIssueResources()
        {
            var issueResource1 = new IssueResource(Guid.NewGuid(), Variables.IssueId, "title1", "path1");
            var IssueResource2 = new IssueResource(Guid.NewGuid(), Variables.IssueId, "title2", "path2");

            _issueResourceRepository.Add(issueResource1);
            _issueResourceRepository.Add(IssueResource2);

            issueResource1 = new IssueResource(Guid.NewGuid(), Variables.DelIssueId, "title1", "path1");
            IssueResource2 = new IssueResource(Guid.NewGuid(), Variables.DelIssueId, "title2", "path2");

            _issueResourceRepository.Add(issueResource1);
            _issueResourceRepository.Add(IssueResource2);
        }

        private void InsertIssueComments()
        {
            var issueComment1 = new IssueComment(Guid.NewGuid(), Variables.IssueId, "comment1", Guid.NewGuid(), Guid.Empty);
            var IssueComment2 = new IssueComment(Guid.NewGuid(), Variables.DelIssueId, "comment1", Guid.NewGuid(), Guid.Empty);
            _issueCommentRepository.Add(issueComment1);
            _issueCommentRepository.Add(IssueComment2);
        }

        private void InsertRequirementTypes()
        {
            RequirementType requirementType1 = new RequirementType(Variables.RequirementTypeId, Variables.SubProjectId, "Requirement Type 1", "REQUIREMENT_TYPE_1_CODE");
            RequirementType requirementType2 = new RequirementType(Guid.NewGuid(), Variables.SubProjectId, "Requirement Type 2", "REQUIREMENT_TYPE_2_CODE");
            RequirementType requirementType3 = new RequirementType(Guid.NewGuid(), Variables.SubProjectId, "Requirement Type 3", "REQUIREMENT_TYPE_3_CODE");

            _requirementTypeRepository.Add(requirementType1);
            _requirementTypeRepository.Add(requirementType2);
            _requirementTypeRepository.Add(requirementType3);
        }

        private void InsertRequirementPriorities()
        {
            RequirementPriority requirementPriority1 = new RequirementPriority(Variables.RequirementPriorityId, Variables.SubProjectId, "Requirement Priority 1", "REQUIREMENT_PRIORITY_1_CODE", 1);
            RequirementPriority requirementPriority2 = new RequirementPriority(Guid.NewGuid(), Variables.SubProjectId, "Requirement Priority 2", "REQUIREMENT_PRIORITY_2_CODE", 2);
            RequirementPriority requirementPriority3 = new RequirementPriority(Guid.NewGuid(), Variables.SubProjectId, "Requirement Priority 3", "REQUIREMENT_PRIORITY_3_CODE", 3);

            _requirementPriorityRepository.Add(requirementPriority1);
            _requirementPriorityRepository.Add(requirementPriority2);
            _requirementPriorityRepository.Add(requirementPriority3);
        }

        private void InsertProcesses()
        {
            Process process1 = new Process(Variables.ProcessId, Variables.SubProjectId, Variables.HierarchyId, Guid.NewGuid(), Guid.NewGuid(), ProcessType.Generic, "Process Title 1", "Process Description 1", Guid.Empty, Guid.Empty, 0, Guid.Empty);
            Process process2 = new Process(Variables.DelProcessId, Variables.DelSubProjectId, Variables.DelHierarchyId, Guid.NewGuid(), Guid.NewGuid(), ProcessType.Generic, "Process Title 2", "Process Description 2", Guid.Empty, Guid.Empty, 0, Guid.Empty);

            _processRepository.Add(process1);
            _processRepository.Add(process2);
        }

        private void InsertProcessWorkflows()
        {
            var processWorkflow1 = new ProcessWorkflow(Guid.NewGuid(), Variables.ProcessId, Variables.WorkflowId);
            var processWorkflow2 = new ProcessWorkflow(Guid.NewGuid(), Variables.ProcessId, Guid.NewGuid());

            _processWorkflowRepository.Add(processWorkflow1);
            _processWorkflowRepository.Add(processWorkflow2);

            processWorkflow1 = new ProcessWorkflow(Guid.NewGuid(), Variables.DelProcessId, Variables.DelWorkflowId);
            processWorkflow2 = new ProcessWorkflow(Guid.NewGuid(), Variables.DelProcessId, Guid.NewGuid());

            _processWorkflowRepository.Add(processWorkflow1);
            _processWorkflowRepository.Add(processWorkflow2);
        }

        private void InsertProcessResources()
        {
            ProcessResources processResources1 = new ProcessResources(Guid.NewGuid(), "fn", Variables.ProcessId, "");
            ProcessResources processResources2 = new ProcessResources(Guid.NewGuid(), "fn 2", Variables.ProcessId, "");

            _processResourceRepository.Add(processResources1);
            _processResourceRepository.Add(processResources2);

            processResources1 = new ProcessResources(Guid.NewGuid(), "fn", Variables.DelProcessId, "");
            processResources2 = new ProcessResources(Guid.NewGuid(), "fn 2", Variables.DelProcessId, "");

            _processResourceRepository.Add(processResources1);
            _processResourceRepository.Add(processResources2);
        }

        private void InsertWorkflowIssues()
        {
            var workflowIssue1 = new WorkflowIssue(Guid.NewGuid(), Variables.WorkflowId, Variables.IssueId);
            var workflowIssue2 = new WorkflowIssue(Guid.NewGuid(), Guid.NewGuid(), Variables.IssueId);

            _workflowIssueRepository.Add(workflowIssue1);
            _workflowIssueRepository.Add(workflowIssue2);

            workflowIssue1 = new WorkflowIssue(Guid.NewGuid(), Variables.DelWorkflowId, Variables.DelIssueId);
            workflowIssue2 = new WorkflowIssue(Guid.NewGuid(), Guid.NewGuid(), Variables.DelIssueId);

            _workflowIssueRepository.Add(workflowIssue1);
            _workflowIssueRepository.Add(workflowIssue2);
        }

        #region Hierarchies

        private void InsertHierarchyTypes()
        {
            HierarchyType hierarchyType1 = new HierarchyType(Variables.HierarchyTypeId, Guid.Empty, Variables.SubProjectId, "HierarchyType Title 1", "HierarchyType Description 1");
            HierarchyType hierarchyType2 = new HierarchyType(Variables.DelHierarchyTypeId, Guid.Empty, Variables.DelSubProjectId, "HierarchyType Title 2", "HierarchyType Description 2");
            HierarchyType hierarchyType3 = new HierarchyType(Variables.HierarchyTypeId2, Guid.Empty, Variables.SubProjectId, "HierarchyType Title 3", "HierarchyType Description 3");
            string parentName = "";
            _hierarchyTypeRepository.Add(hierarchyType1, false, out parentName);
            _hierarchyTypeRepository.Add(hierarchyType2, false, out parentName);
            _hierarchyTypeRepository.Add(hierarchyType3, false, out parentName);
        }

        private void InsertHierarchies()
        {
            Hierarchy hierarchy1 = new Hierarchy(Variables.HierarchyId, Guid.Empty, Variables.SubProjectId, Variables.HierarchyTypeId, "Hierarchy Title 1", "Hierarchy Description 1");
            Hierarchy hierarchy2 = new Hierarchy(Variables.DelHierarchyId, Guid.Empty, Variables.DelSubProjectId, Variables.DelHierarchyTypeId, "Hierarchy Title 2", "Hierarchy Description 2");
            Hierarchy fromHierarchy = new Hierarchy(Variables.FromHierarchyId, Guid.Empty, Variables.SubProjectId, Variables.HierarchyTypeId, "From Hierarchy Title 1", "From Hierarchy Description 1");
            Hierarchy toHierarchy = new Hierarchy(Variables.ToHierarchyId, Guid.Empty, Variables.SubProjectId, Variables.HierarchyTypeId, "To Hierarchy Title 1", "To Hierarchy Description 1");

            _hierarchyRepository.Add(hierarchy1);
            _hierarchyRepository.Add(hierarchy2);
            _hierarchyRepository.Add(fromHierarchy);
            _hierarchyRepository.Add(toHierarchy);
        }

        #endregion Hierarchies

        #region Sprints

        private void InsertSprints()
        {
            var sprint1 = new Sprints(Variables.SprintId, "title1", "description1", DateTime.Parse("2019-08-01"), DateTime.Parse("2019-09-01"), Variables.SubProjectId, 0, DurationUnit.Weeks);
            var sprint2 = new Sprints(Variables.DelSprintId, "title2", "description2", DateTime.Parse("2019-08-01"), DateTime.Parse("2019-09-01"), Variables.DelSubProjectId, 0, DurationUnit.Weeks);

            _sprintsRepository.Add(sprint1);
        }

        private void InsertSprintIssues()
        {
            var sprintIssue1 = new SprintIssues(Guid.NewGuid(), Variables.SprintId, Variables.IssueId, (int)StatusType.Done);

            _sprintIssuesRepository.Add(sprintIssue1);
        }

        #endregion Sprints

        #region Peoples

        private void InsertRoles()
        {
            Role role1 = new Role(Variables.RoleId, "Role 1", "Role 1", Variables.SubProjectId);
            Role role2 = new Role(Variables.DelRoleId, "Role 2", "Role 2", Variables.SubProjectId);

            _roleRepository.Add(role1);
            _roleRepository.Add(role2);
        }

        private void InsertPeoples()
        {
            People people1 = new People(Guid.NewGuid(), Variables.RoleId, "name1", "surname1", "email1", "address1", 0, Variables.SubProjectId, "#5cb85c");
            People people2 = new People(Guid.NewGuid(), Variables.DelRoleId, "name2", "surname2", "email2", "address2", 0, Variables.SubProjectId, "#5cb85c");

            _peopleRepository.Add(people1);
            _peopleRepository.Add(people2);
        }

        #endregion Peoples
    }
}