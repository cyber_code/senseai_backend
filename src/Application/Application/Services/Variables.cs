﻿using System;

namespace Application.Services
{
    public class Variables
    {
        public static Guid TenantId = new Guid("30788c4a-ea74-4aa8-b505-891303dbaeaa");
        public static Guid ProjectId = new Guid("687d62f3-1b39-415d-8190-248686c6ddc4");
        public static Guid SubProjectId = new Guid("90c39e80-9ccb-4dbf-bfd7-396b2201e01d");
        public static Guid SystemId = new Guid("d202e3fa-cce5-420b-bd5a-066bb4db972b");
        public static Guid CatalogId = new Guid("3cb308f8-026e-47b8-bd22-1c3bac2c06d2");
        public static Guid FolderId = new Guid("3adf6db6-b640-439d-b6d3-1aaa9d9fd626");
        public static Guid ImportFolderId = new Guid("3adf6db6-b640-439d-b6d3-1aaa9d9fd888");
        public static Guid HierarchyTypeId = new Guid("0aac6890-f378-45c2-9543-17ec6f141cbb");
        public static Guid HierarchyTypeId2 = new Guid("6a9149db-a95c-46ad-b017-845d38e3e50a");
        public static Guid HierarchyId = new Guid("687d62f3-1b39-415d-8190-248686c6ddd5");
        public static Guid RequirementTypeId = new Guid("d0e59579-efdc-4975-a6d3-a7d78abe4718");
        public static Guid RequirementPriorityId = new Guid("325d546a-3e98-452c-b017-61bfc2e5a4a1");
        public static Guid ProcessId = new Guid("2d31acef-5293-4515-bd68-d039d8c0a753");
        public static Guid WorkflowId = new Guid("9e90b265-e7b3-4dbb-8e81-089d71cf7442");
        public static Guid GenericWorkflowId = new Guid("C50C7A2C-6B77-4600-9650-F78EAF29CACE");
        public static Guid IssueTypeFieldsNameId = new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d");
        public static Guid IssueTypeId = new Guid("90c39e80-9ccb-4dba-bfd7-396b2201e01d");
        public static Guid BusinessChangeRequestIssueTypeId = new Guid("9f2b7e57-4858-48ea-9c39-ccac6df1421c");
        public static Guid TechnicalChangeRequestIssueTypeId = new Guid("8f7c9cd1-447c-42b8-b0c8-648b1169a493");
        public static Guid IssueId = new Guid("C50C7A2C-6B77-1600-9650-F78EAF29CACE");
        public static Guid RoleId = new Guid("d50C7A2C-6B77-1600-9650-F78EAF29CAdf");
        public static Guid SprintId = new Guid("DCEBA3A2-387E-4CA6-B53A-BB9FF1236EBA");
        public static Guid FromHierarchyId = new Guid("6f31386f-70c1-498f-9dde-25eb0972daee");
        public static Guid ToHierarchyId = new Guid("6ce4ca06-834f-48af-ae5f-1137d56b91d4");
        public static Guid NavigationId = new Guid("e2ab61fc-0f26-4fff-a976-b0e4a881e36d");
        public static Guid NavigationParentId = new Guid("A7B68BEC-684A-4649-9C56-1A008FB6E449");
        public static Guid TypicalId = new Guid("85434986-25DE-4F9B-B2EF-000171B2BA58");
        public static string TypicalName = "Customer";
        public static Guid DatasetId = new Guid("2318551E-3151-42F6-97EC-14F01D32A18E");
        public static Guid DataSetTypicalId = new Guid("85434986-25DE-4F9B-B2EF-000171B2BA60");
        public static Guid DataSetPathItemId = new Guid("5b051302-c6f8-4086-8a9a-d6df5755bd9a");
        public static Guid DataSetItemId = new Guid("dbf08e7b-846c-4214-b056-006e80f139d1");
        public static Guid SuggestionCurrentItemId = Guid.Parse("06FBDC0E-C5EA-4DFB-8E82-08D6A7BB825D");

        public static Guid DelProjectId = new Guid("687d62f3-1b39-415d-8190-248686c6ddd5");
        public static Guid DelSubProjectId = new Guid("90c39e80-9ccb-4dbf-bfd7-396b2201e02e");
        public static Guid DelFolderId = new Guid("3adf6db6-b640-439d-b6d3-1aaa9d9fd637");
        public static Guid DelCatalogId = new Guid("3cb308f8-026e-47b8-bd22-1c3bac2c06E3");
        public static Guid DelHierarchyTypeId = new Guid("0aac6890-f378-45c2-9543-17ec6f141cdd");
        public static Guid DelHierarchyId = new Guid("687d62f3-1b39-415d-8190-248686c6ddd6");
        public static Guid DelProcessId = new Guid("2d31acef-5293-4515-bd68-d039d8c0a764");
        public static Guid DelWorkflowId = new Guid("9e90b265-e7b3-4dbb-8e81-089d71cf7553");
        public static Guid DelIssueTypeId = new Guid("90c39e80-9ccb-4dba-bfd7-396b2201e0dc");
        public static Guid DelIssueId = new Guid("C50C7A2C-6B77-1600-9650-F78EAF29CADF");
        public static Guid DelRoleId = new Guid("d50C7A2C-6B77-1600-9650-F78EAF29Cddd");
        public static Guid DelSprintId = new Guid("DCEBA3A2-387E-4CA6-B53A-BB9FF1236EDD");
        public static Guid DelTypicalId = new Guid("85434986-25DE-4F9B-B2EF-000171B2BA69");
    }
}
