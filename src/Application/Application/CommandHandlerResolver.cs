﻿using Core;
using Core.Abstractions;
using Messaging;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application
{
    internal sealed class CommandHandlerResolver : IHandlerResolver
    {
        private readonly Dictionary<Type, Type> _handlers;

        internal CommandHandlerResolver()
        {
            var typeFinder = EngineContext.Current.Resolve<ITypeFinder>();
            var handlers = typeFinder.FindClassesOfType(typeof(ICommandHandler<,>));

            _handlers = handlers.ToDictionary(
                        type => type.GetInterfaces()
                            .First(i => i.IsGenericType)
                            .GetGenericArguments()[0]
                    );
        }

        public Type Get(Type type)
        {
            return _handlers[type];
        }
    }
}