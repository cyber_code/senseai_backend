﻿using Newtonsoft.Json;
using Core.Abstractions;
using Core.Configuration;
using Logging.Messaging;
using Logging.Messaging.Persistence.Model;
using Messaging.Queries;
using Messaging.Queries.Decorators;
using System;
using System.Collections.Generic;

namespace Application.Decorators
{
    public sealed class LoggingQueryHandler<TQuery, TQueryResult> : BaseQueryHandlerDecorator<TQuery, TQueryResult>
        where TQuery : IQuery<TQueryResult>
    {
        private const string NAMESPACE_PREFIX = "Queries.";
        private readonly IMessagingLogger _messageLogger;
        private readonly IWorkContext _workContext;
        private readonly SenseAIConfig _config;

        public LoggingQueryHandler(
            IQueryHandler<TQuery, TQueryResult> handler, IMessagingLogger messageLogger, IWorkContext workContext,
            SenseAIConfig config
        )
            : base(handler)
        {
            _messageLogger = messageLogger;
            _workContext = workContext;
            _config = config;
        }

        public override List<string> Errors
        {
            get { return Handler.Errors; }
            set { Handler.Errors = value; }
        }

        public override QueryResponse<TQueryResult> Handle(TQuery queryObject)
        {
            var started = DateTime.Now;

            var response = Handler.Handle(queryObject);

            var ended = DateTime.Now;

            if (_config.LogAllQueries || !response.Successful)
            {
                var log = new QueryLog();

                log.QueriedBy = _workContext.UserId;
                log.AppName = "SenseAI"; // DAMTODO Make it configurable
                log.CalledFrom = _workContext.CalledFrom; // DAMTODO Make it configurable
                // ReSharper disable once PossibleNullReferenceException
                log.Name = typeof(TQuery).FullName.Replace(NAMESPACE_PREFIX, string.Empty);
                log.Json = JsonConvert.SerializeObject(queryObject);

                if (!response.Successful)
                {
                    var exception = response.Error.ExceptionObject;

                    log.ErrorExceptionType = response.Error.ExceptionType;
                    log.ErrorExceptionMessage = response.Error.ExceptionMessage;
                    log.ErrorExceptionStackTrace = exception.StackTrace;
                    log.ErrorExceptionJson = JsonConvert.SerializeObject(exception);
                }

                log.Started = started;
                log.Ended = ended;
                log.ElapsedMilliseconds = (long)(ended - started).TotalMilliseconds;
                log.IpAddress = _workContext.IpAddress; // DAMTODO Make it configurable

                _messageLogger.Log(log);
            }

            return response;
        }

        public override bool Validate(TQuery query)
        {
            return Handler.Validate(query);
        }
    }
}