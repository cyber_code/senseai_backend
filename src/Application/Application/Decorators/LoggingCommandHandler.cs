﻿using Newtonsoft.Json;
using Core.Abstractions;
using Logging.Messaging;
using Logging.Messaging.Persistence.Model;
using Messaging.Commands;
using Messaging.Commands.Decorators;
using System;
using System.Collections.Generic;

namespace Application.Decorators
{
    public sealed class LoggingCommandHandler<TCommand, TCommandResult> : BaseCommandHandlerDecorator<TCommand, TCommandResult>
        where TCommand : ICommand<TCommandResult>
    {
        private const string NAMESPACE_PREFIX = "Commands.";
        private readonly IMessagingLogger _messageLogger;
        private readonly IWorkContext _workContext;

        public LoggingCommandHandler(
            ICommandHandler<TCommand, TCommandResult> handler, IMessagingLogger messageLogger, IWorkContext workContext
        )
            : base(handler)
        {
            _messageLogger = messageLogger;
            _workContext = workContext;
        }

        public override List<string> Errors
        {
            get { return Handler.Errors; }
            set { Handler.Errors = value; }
        }

        public override CommandResponse<TCommandResult> Handle(TCommand command)
        {
            var started = DateTime.Now;

            var response = Handler.Handle(command);

            var ended = DateTime.Now;

            var log = new CommandLog();

            log.ExecutedBy = _workContext.UserId;
            log.AppName = "SenseAI"; // DAMTODO Make it configurable
            log.CalledFrom = _workContext.CalledFrom; // DAMTODO Make it configurable
            log.Name = typeof(TCommand).FullName.Replace(NAMESPACE_PREFIX, string.Empty);
            log.Json = JsonConvert.SerializeObject(command);

            if (!response.Successful)
            {
                var exception = response.Error.ExceptionObject;

                log.ErrorExceptionType = response.Error.ExceptionType;
                log.ErrorExceptionMessage = response.Error.ExceptionMessage;
                log.ErrorExceptionStackTrace = exception.StackTrace;
                log.ErrorExceptionJson = JsonConvert.SerializeObject(exception, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                } 
                );
            }

            log.Started = started;
            log.Ended = ended;
            log.ElapsedMilliseconds = (long)(ended - started).TotalMilliseconds;
            log.IpAddress = _workContext.IpAddress; // DAMTODO Make it configurable

            _messageLogger.Log(log);

            return response;
        }

        public override bool Validate(TCommand command)
        {
            return Handler.Validate(command);
        }
    }
}