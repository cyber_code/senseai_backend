﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.TestGenerationModule;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.TestGenerationModule
{
    public sealed class GetTestSuiteCatalogsHandler : IQueryHandler<GetTestSuiteCatalogs, GetTestSuiteCatalogsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetTestSuiteCatalogsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTestSuiteCatalogsResult[]> Handle(GetTestSuiteCatalogs queryObject)
        {
            var testcases = from generatedTests in _dbContext.Set<WorkflowTestCasesData>().AsNoTracking()
                                      where generatedTests.WorkflowId == queryObject.WorkflowId
                                      select generatedTests.TestStepJson;

            var teststeps = JsonConvert.DeserializeObject<TestStepJson[]>("[" + string.Join(",", testcases) + "]");


            var catalogs = teststeps.Select(ct => (ct.CatalogName, ct.CatalogId)).Distinct();
            var result = catalogs.Select(ct => new GetTestSuiteCatalogsResult(ct.CatalogName, ct.CatalogId));
            return new QueryResponse<GetTestSuiteCatalogsResult[]>(result.ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No catalogs found with WorkflowId: '{queryObject.WorkflowId}'."));
        }

        public bool Validate(GetTestSuiteCatalogs query)
        {
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}