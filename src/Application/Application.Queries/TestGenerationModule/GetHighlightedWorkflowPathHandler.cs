﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.TestGenerationModule;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.TestGenerationModule
{
    public class GetHighlightedWorkflowPathHandler : IQueryHandler<GetHighlightedWorkflowPath, GetHighlightedWorkflowPathResult>
    {
        private readonly IDbContext _dbContext;

        public GetHighlightedWorkflowPathHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetHighlightedWorkflowPathResult> Handle(GetHighlightedWorkflowPath queryObject)
        {
            var workflowPathItems = from workflowpath in _dbContext.Set<WorkflowVersionPathDetailsData>()
                                    where workflowpath.PathId == queryObject.WorkflowPathId
                                    orderby workflowpath.Index
                                    select new
                                    {
                                        workflowpath.WorkflowId,
                                        workflowpath.PathId,
                                        workflowpath.Id,
                                        workflowpath.Index
                                    };
            if (workflowPathItems.Count() == 0)
                throw new Exception("No path items found with this workflowpath id");
            Guid workflowId = workflowPathItems.FirstOrDefault().WorkflowId;

            var workflowVersion = (from workflowversion in _dbContext.Set<WorkflowVersionData>()
                                   where workflowversion.WorkflowId == workflowId && workflowversion.IsLast == true
                                   select new
                                   {
                                       workflowversion.DesignerJson
                                   }).FirstOrDefault();
            if (workflowVersion == null)
                throw new Exception(string.Format("No workflow version found with this WorkflowId: '{0}'", workflowId));

            Guid[] workflowPathItemIds = workflowPathItems.Select(r => r.Id).ToArray();
            var result = new GetHighlightedWorkflowPathResult(workflowVersion.DesignerJson, workflowPathItemIds);

            return new QueryResponse<GetHighlightedWorkflowPathResult>(result, result is GetHighlightedWorkflowPathResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Workflow Paths found with WorkflowPathId: '{queryObject.WorkflowPathId}'."));
        }

        public bool Validate(GetHighlightedWorkflowPath query)
        {
            if (query.WorkflowPathId == Guid.Empty)
            {
                Errors.Add("WorkflowPathId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}