﻿using Newtonsoft.Json;
using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Persistence.WorkflowModel;
using Queries.TestGenerationModule;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.TestGenerationModule
{
    public sealed class ExportTestCasesHandler : IQueryHandler<ExportTestCases, ExportTestCasesResult>
    {
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IDbContext _dbContext;
        private readonly IExportedTestCaseRepository _exportedTestCaseRepository;

        public ExportTestCasesHandler(IDbContext dbContext, IWorkflowRepository workflowRepository, IExportedTestCaseRepository exportedTestCaseRepository)
        {
            _workflowRepository = workflowRepository;
            _dbContext = dbContext;
            _exportedTestCaseRepository = exportedTestCaseRepository;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<ExportTestCasesResult> Handle(ExportTestCases queryObject)
        {
            #region [ old ]

            //var query = from plan in _dbContext.Set<GeneratedTestCasesData>()

            //    where plan.WorkflowId == queryObject.WorkflowId
            //    select new
            //    {
            //        plan.Id,
            //        plan.WorkflowId,
            //        plan.WorkflowPathId,
            //        plan.TestCaseId,
            //        plan.TestCaseTitle,
            //        plan.TestStepId,
            //        plan.TestStepTitle,
            //        plan.TestStepJson,
            //        plan.TCIndex,
            //        plan.TypicalName,
            //        plan.TSIndex
            //    };

            //var fileArray = TestGenerationFactory.Create(query
            //    .Select(plan => new GeneratedTestCases(plan.WorkflowId, plan.WorkflowPathId, plan.TestCaseId, plan.TestCaseTitle, plan.TCIndex, plan.TestStepId, plan.TestStepTitle, plan.TestStepJson, plan.TypicalName, plan.TSIndex)).ToArray());

            #endregion [ old ]

            MessageType messageType = MessageType.Successful;
            string message = string.Empty;
            //Get workflow status
            int workflowStatus = _workflowRepository.GetWorkflowStatus(queryObject.WorkflowId);
            if (workflowStatus != (int)WorkflowStatus.ReadyToExport)
            {
                //can't export test case if the status is not "ReadyToExport"
                //this action will stop exporting test cases
                messageType = MessageType.Information;
                var workflowDetails = _workflowRepository.GetWorkflowDetails(queryObject.WorkflowId);
                message = $"This workflow is currently in progress by another user.If you continue they will lose any unsaved progress.";
                return new QueryResponse<ExportTestCasesResult>(new ExportTestCasesResult(null), new QueryResponseMessage((int)messageType, message));
            }

            try
            {
                _workflowRepository.UpdateStatus(queryObject.WorkflowId, WorkflowStatus.ExportingInProcess);
                var scriptedGropus = from testCasesData in _dbContext.Set<WorkflowTestCasesData>()
                                     join workflowData in _dbContext.Set<WorkflowData>() on testCasesData.WorkflowId equals workflowData.Id
                                     join folderData in _dbContext.Set<FolderData>() on workflowData.FolderId equals folderData.Id
                                     join subProjectData in _dbContext.Set<SubProjectData>() on folderData.SubProjectId equals subProjectData.Id
                                     join projectData in _dbContext.Set<ProjectData>() on subProjectData.ProjectId equals projectData.Id
                                     where testCasesData.WorkflowId.Equals(queryObject.WorkflowId)
                                     group testCasesData by new
                                     {
                                         Requirement = workflowData.Title,
                                         RequirementDescription = workflowData.Description,
                                         SubProjectId = subProjectData.Id,
                                         SubProjectTitle = subProjectData.Title,
                                         FolderId = folderData.Id,
                                         FolderTitle = folderData.Title,
                                         FolderDescription = folderData.Description,
                                         ExternalSystemId = testCasesData.WorkflowId
                                     } into g
                                     select new
                                     {
                                         GroupData = g.Key,
                                         Steps = g.Select(step => new WorkflowTestCases(step.Id,
                                                                                        step.WorkflowId,
                                                                                         step.VersionId,
                                                                                         step.WorkflowPathId,
                                                                                         step.TestCaseId,
                                                                                         step.TestCaseTitle,
                                                                                         step.TCIndex,
                                                                                         step.TestStepId,
                                                                                         step.TestStepTitle,
                                                                                         step.TestStepJson, 
                                                                                         step.TypicalId,
                                                                                         step.TypicalName,
                                                                                         step.TSIndex,
                                                                                         step.TestStepType)).ToList()
                                     };

                long[] testCaseIds;
                var result = VXMLFactory.TransformToVXml(
                    scriptedGropus.First().GroupData.SubProjectId,
                    scriptedGropus.First().GroupData.SubProjectTitle,
                    scriptedGropus.First().GroupData.FolderId,
                    scriptedGropus.First().GroupData.FolderTitle,
                    scriptedGropus.First().GroupData.FolderDescription,
                    scriptedGropus.First().GroupData.ExternalSystemId,
                    scriptedGropus.First().GroupData.Requirement,
                    scriptedGropus.First().Steps.ToArray(),
                    GetDataPool,
                    GetTypicalTypeByCatalogName,
                    out testCaseIds);

                foreach (var testCaseId in testCaseIds)
                {
                    _exportedTestCaseRepository.StoreExportedTestCaseReference(queryObject.WorkflowId, testCaseId);
                }
                _workflowRepository.UpdateStatus(queryObject.WorkflowId, WorkflowStatus.ReadyToIssue);

                return new QueryResponse<ExportTestCasesResult>(new ExportTestCasesResult(result));
            }
            catch (Exception ex)
            {
                messageType = MessageType.Failed;
                _workflowRepository.UpdateStatus(queryObject.WorkflowId, WorkflowStatus.ReadyToExport);
                _workflowRepository.SetIsTestCasegenerated(queryObject.WorkflowId);
                return new QueryResponse<ExportTestCasesResult>(new ExportTestCasesResult(null), new QueryResponseMessage((int)messageType, "An error has occured during exporting process"));
            }
        }

        public bool Validate(ExportTestCases query)
        {
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            return true;
        }

        private List<TypicalInstance> GetDataPool(Guid datasetId)
        {
            var scriptedGropus = from dataSetItem in _dbContext.Set<DataSetItemData>()
                                 where dataSetItem.DataSetId.Equals(datasetId)
                                 select dataSetItem.Row;

            var returnList = new List<TypicalInstance>();

            foreach (var instanceData in scriptedGropus)
            {
                Debug.WriteLine(instanceData);
                var instance = new TypicalInstance();
                // var json = JObject.Parse(instanceData);
                var attributesList = JsonConvert.DeserializeObject<List<SenseAI.Domain.WorkflowModel.Attribute>>(instanceData);
                attributesList.ForEach(attribute => instance.Attributes.Add(attribute));

                returnList.Add(instance);
            }

            return returnList;
        }

        private int GetTypicalTypeByCatalogName(string catalogName)
        {
            var scriptedGropus = from catalog in _dbContext.Set<Persistence.CatalogModel.CatalogData>()
                                 where catalog.Title.Equals(catalogName)
                                 select catalog.Type;

            return scriptedGropus.FirstOrDefault();
        }
    }
}