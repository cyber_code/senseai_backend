﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.TestGenerationModule;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.TestGenerationModule
{
    public sealed class GetTestSuiteHandler : IQueryHandler<GetTestSuite, GetTestSuiteResult>
    {
        private readonly IDbContext _dbContext;

        public GetTestSuiteHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTestSuiteResult> Handle(GetTestSuite queryObject)
        {
            var qGeneratedTestCases = from generatedTests in _dbContext.Set<WorkflowTestCasesData>().AsNoTracking()
                                      where generatedTests.WorkflowId == queryObject.WorkflowId && generatedTests.WorkflowPathId == queryObject.WorkflowPathId
                                      select generatedTests;

            var qDataSets = _dbContext.Set<DataSetData>().AsNoTracking().Where(ds => qGeneratedTestCases.Select(gt => gt.TypicalId).Contains(ds.TypicalId)).ToList();

            var testSuite = (from ts in qGeneratedTestCases
                             select new
                             {
                                 ts.TestCaseId,
                                 ts.TestCaseTitle,
                                 ts.TCIndex
                             }).Distinct().OrderBy(ct => ct.TCIndex);

            var testCases = testSuite.Select(tc => new TestCasesResult(tc.TestCaseId, tc.TestCaseTitle, GetTestStepsResult(qGeneratedTestCases.Where(q => q.TestCaseId == tc.TestCaseId).OrderBy(ts => ts.TSIndex).ToList(), qDataSets).ToArray()));

            return new QueryResponse<GetTestSuiteResult>(new GetTestSuiteResult("Number of test cases: " + testSuite.Count(), testCases.ToArray()), testSuite.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Test Suite found with WorkflowId: '{queryObject.WorkflowId}' and WorkflowPathId: '{queryObject.WorkflowPathId}'."));
        }

        public bool Validate(GetTestSuite query)
        {
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            if (query.WorkflowPathId == Guid.Empty)
            {
                Errors.Add("WorkflowPathId cannot be empty.");
                return false;
            }
            return true;
        }

        private List<TestStepsResult> GetTestStepsResult(List<WorkflowTestCasesData> generatedTestSteps, List<DataSetData> dataSets)
        {
            List<TestStepsResult> result = new List<TestStepsResult>();
            decimal coverage = 0;
            foreach (var ts in generatedTestSteps)
            {
                coverage = 0;

                var testStepDesingerData = JsonConvert.DeserializeObject<TestStepDesingerData>(ts.TestStepJson);
                if (testStepDesingerData.DataSetIds != null && testStepDesingerData.DataSetIds.Length > 0)
                    coverage = dataSets.Where(ct => testStepDesingerData.DataSetIds.Contains(ct.Id)).Select(ct => ct.Coverage).FirstOrDefault();

                result.Add(new TestStepsResult(ts.Id, ts.TestStepTitle, coverage == 0 ? "N/A" : coverage.ToString() + "%", testStepDesingerData.DataSetIds,
                    ts.TypicalId, ts.TypicalName, testStepDesingerData.Action, ts.TSIndex));
            }
            return result;
        }
    }
}