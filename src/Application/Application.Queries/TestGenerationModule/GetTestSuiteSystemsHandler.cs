﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.TestGenerationModule;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.TestGenerationModule
{
    public sealed class GetTestSuiteSystemsHandler : IQueryHandler<GetTestSuiteSystems, GetTestSuiteSystemsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetTestSuiteSystemsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTestSuiteSystemsResult[]> Handle(GetTestSuiteSystems queryObject)
        {
            var testcases = from generatedTests in _dbContext.Set<WorkflowTestCasesData>().AsNoTracking()
                                      where generatedTests.WorkflowId == queryObject.WorkflowId
                                      select generatedTests.TestStepJson;

            var teststeps = JsonConvert.DeserializeObject<TestStepJson[]>("[" + string.Join(",", testcases) + "]");


            var systems = teststeps.Select(ct => ct.SystemName).Distinct();
            var result = systems.Select(ct => new GetTestSuiteSystemsResult(ct));
            return new QueryResponse<GetTestSuiteSystemsResult[]>(result.ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No systens found with WorkflowId: '{queryObject.WorkflowId}'."));
        }

        public bool Validate(GetTestSuiteSystems query)
        {
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}