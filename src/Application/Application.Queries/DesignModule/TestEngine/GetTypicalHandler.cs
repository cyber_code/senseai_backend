﻿using Microsoft.EntityFrameworkCore;
using SenseAI.Domain;
using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DesignModule;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Queries.DesignModule.TestEngine;
using Persistence.CatalogModel;
using Persistence.SystemCatalogModel;

namespace Application.Queries.DesignModule.TestEngine
{
    public sealed class GetTypicalHandler : IQueryHandler<GetTypical, GetTypicalResult>
    {
        private readonly IDbContext _dbContext;

        public GetTypicalHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTypicalResult> Handle(GetTypical queryObject)
        {
            GetTypicalResult result = null;

            var typicalType = (TypicalType)queryObject.Type;
            TypicalData typical = null;
            var catalogs = _dbContext.Set<SystemCatalogData>().AsNoTracking().
                Where(w => w.SubprojectId == queryObject.SubProjectId && w.SystemId == queryObject.SystemId).Select(s => s.CatalogId).ToList();

            var catalog = _dbContext.Set<CatalogData>().AsNoTracking().
                           FirstOrDefault(w => catalogs.Contains(w.Id) && w.Type == (int)TypicalType.Application);

            if (typicalType == TypicalType.Application || typicalType == TypicalType.Version)
            {
                if (catalog != null)
                    typical = _dbContext.Set<TypicalData>().AsNoTracking().FirstOrDefault(w => w.CatalogId == catalog.Id &&
                            w.Title.ToLower() == queryObject.Name.ToLower() && w.Type == (TypicalType)queryObject.Type);
            }
            else if (typicalType == TypicalType.Enquiry || typicalType == TypicalType.AAAplication)
            {
                catalog = _dbContext.Set<CatalogData>().AsNoTracking().FirstOrDefault(w => catalogs.Contains(w.Id) && w.Type == (int)typicalType);
                if (catalog != null)
                    typical = _dbContext.Set<TypicalData>().AsNoTracking().FirstOrDefault(w => w.CatalogId == catalog.Id &&
                              w.Title.ToLower() == queryObject.Name.ToLower() && w.Type == (TypicalType)queryObject.Type);
            }
            if (typical != null)
            {
                var jsonObject = JsonConvert.DeserializeObject<TypicalItem>(typical.Json.ToString());
                result = new GetTypicalResult(typical.Id, (int)typical.Type, typical.Title, typical.CatalogId, catalog.Title, jsonObject.Attributes == null ? null : jsonObject.Attributes.Select(s => s.Name).ToList());
            }

            return new QueryResponse<GetTypicalResult>(result, typical == null ? null : new QueryResponseMessage((int)MessageType.Information, $"No typicals found in catalog."));
        }

        public bool Validate(GetTypical query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }

            if (query.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }

            if (string.IsNullOrEmpty(query.Name))
            {
                Errors.Add("Name cannot be empty.");
                return false;
            }
            return true;
        }
    }
}