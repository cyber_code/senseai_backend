﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DesignModule;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule
{
    public sealed class SearchAttributesHandler : IQueryHandler<SearchAttributes, AttributesResult[]>
    {
        private readonly IDbContext _dbContext;

        public SearchAttributesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<AttributesResult[]> Handle(SearchAttributes queryObject)
        {
            var query = (from typical in _dbContext.Set<TypicalData>().AsNoTracking()
                         where typical.Id == queryObject.TypicalId
                         select new
                         {
                             typical.Id,
                             typical.Title,
                             typical.Json
                         }).FirstOrDefault();

            if (query == null)
                return new QueryResponse<AttributesResult[]>(null, new QueryResponseMessage((int)MessageType.Information, $"No attributes found in typical."));

            var jsonObject = JsonConvert.DeserializeObject<TypicalItem>(query.Json.ToString());
            var result = jsonObject.Attributes.OrderBy(o => o.Name).Where(ct => string.IsNullOrEmpty(queryObject.SearchString) || ct.Name.ToLower().StartsWith(queryObject.SearchString.ToLower()));

            return new QueryResponse<AttributesResult[]>(result.Distinct().ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No attributes found in typical."));
        }

        public bool Validate(SearchAttributes query)
        {
            if (query.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}