﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule
{
    public class GetWorkflowByVersionHandler : IQueryHandler<GetWorkflowByVersion, GetWorkflowResult>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowByVersionHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowResult> Handle(GetWorkflowByVersion queryObject)
        {
            var linkedWorkflows = from workflowItem in _dbContext.Set<WorkflowItemData>().AsNoTracking()
                                  join workflow in _dbContext.Set<WorkflowData>().AsNoTracking()
                                  on workflowItem.WorkflowId equals workflow.Id
                                  where workflowItem.WorkflowLinkId == queryObject.WorkflowId &&
                                        workflowItem.WorkflowLinkVersionId == queryObject.VersionId
                                  select new LinkedWorkflow(workflowItem.WorkflowId, workflow.Title);

            var query = from workflow in _dbContext.Set<WorkflowVersionData>().AsNoTracking()
                        where workflow.WorkflowId == queryObject.WorkflowId && workflow.VersionId == queryObject.VersionId
                        select new GetWorkflowResult(workflow.WorkflowId, workflow.Title, workflow.Description, Guid.Empty, workflow.DesignerJson,
                        workflow.WorkflowPathsJson, linkedWorkflows.Distinct().ToArray(), workflow.IsParsed, queryObject.VersionId);

            return new QueryResponse<GetWorkflowResult>(query.FirstOrDefault(), query.Any() ? null :
                new QueryResponseMessage((int)MessageType.Information, $"No workflow found."));
        }

        public bool Validate(GetWorkflowByVersion query)
        {
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}