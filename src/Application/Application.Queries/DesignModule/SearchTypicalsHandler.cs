﻿using Microsoft.EntityFrameworkCore;
using SenseAI.Domain;
using Messaging.Queries;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DesignModule;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule
{
    public sealed class SearchTypicalsHandler : IQueryHandler<SearchTypicals, SearchTypicalsResult[]>
    {
        private readonly IDbContext _dbContext;

        public SearchTypicalsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<SearchTypicalsResult[]> Handle(SearchTypicals queryObject)
        {
            var type = _dbContext.Set<CatalogData>().AsNoTracking().FirstOrDefault(ct => ct.Id == queryObject.CatalogId)?.Type;

            List<int> list = new List<int>();

            if (type.HasValue)
                list.Add(type.Value);

            if (type == (int)TypicalType.Application)
                list.Add((int)TypicalType.Version);

            var query = from typical in _dbContext.Set<TypicalData>().AsNoTracking()
                        where typical.CatalogId == queryObject.CatalogId
                                && list.Contains((int)typical.Type)
                                && ((string.IsNullOrEmpty(queryObject.SearchString)) || typical.Title.ToLower().StartsWith(queryObject.SearchString.ToLower()))
                        select new
                        {
                            typical.Id,
                            typical.Title
                        };

            var result = query.Select(typical => new SearchTypicalsResult(typical.Id, typical.Title)).OrderBy(ct => ct.Title).ToArray();
            if (string.IsNullOrEmpty(queryObject.SearchString))
                result = result.Take(100).ToArray();

            return new QueryResponse<SearchTypicalsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No typicals found in catalog."));
        }

        public bool Validate(SearchTypicals query)
        {
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}