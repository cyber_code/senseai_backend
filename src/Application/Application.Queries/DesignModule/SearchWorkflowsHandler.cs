﻿using Iveonik.Stemmers;
using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule;
using StopWord;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.DesignModule
{
    public sealed class SearchWorkflowsHandler : IQueryHandler<SearchWorkflows, SearchWorkflowsResult[]>
    {
        private readonly IDbContext _dbContext;
        private List<SearchWorkflowsResult> container = new List<SearchWorkflowsResult>();

        public SearchWorkflowsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public static string WordStemming(string SearchPhrase)
        {
            string documentWithoutPunctuation = "";
            string final = null;
            foreach (var c in SearchPhrase)
            {
                if (!char.IsPunctuation(c))
                {
                    documentWithoutPunctuation += c;
                }
            }
            SearchPhrase = documentWithoutPunctuation;

            List<string> termStrings = SearchPhrase.ToLower().Split(' ').ToList();

            List<string> stemmedTerms = new List<string>();
            EnglishStemmer stemmer = new EnglishStemmer();
            foreach (var ts in termStrings)
            {
                stemmedTerms.Add(stemmer.Stem(ts));
            }
            List<string> terms = new List<string>();
            foreach (var stemmedterm in stemmedTerms.Distinct())
            {
                if (!terms.Any(i => i == stemmedterm))
                {
                    terms.Add(stemmedterm);
                }
            }
            final = String.Join(" ", terms);

            return WordStops(final);
        }

        public static string WordStops(string SearchPhrase)
        {
            var _stopWordslist = StopWords.GetStopWords();

            string[] wordList = SearchPhrase.Split(new char[] { '.', '?', '!', ' ', ';', ':', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(item => item.ToLowerInvariant()).ToArray();

            string final = null;

            var result = wordList.Except(_stopWordslist).ToArray();

            final = String.Join(" ", result);

            return final;
        }

        public QueryResponse<SearchWorkflowsResult[]> Handle(SearchWorkflows queryObject)
        {
            var sentence = WordStemming(queryObject.SearchString);

            string[] wordList = sentence.Split(new char[] { '.', '?', '!', ' ', ';', ':', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(item => item.ToLowerInvariant()).ToArray();

            var foldersWithPartialMatching = GetFoldersWithPartialMatching(queryObject, wordList);

            var workflowsWithPartialMatching = GetWorkflowsWithPartialMatching(queryObject, wordList);

            var workflowItemsWithPartialMatching = GetWorkflowItemsWithPartialMatching(queryObject, wordList);

            var foldersWithFullMatching = GetFoldersWithFullMatching(queryObject, foldersWithPartialMatching);

            var workflowsWithFullMatching = GetWorkflowsWithFullMatching(queryObject, workflowsWithPartialMatching);

            var workflowItemsWithFullMatching = GetWorkflowItemsWithFullMatching(queryObject, workflowItemsWithPartialMatching);

            var workflowItemTypicalsWithFullMatching = GetWorkflowItemTypicalsWithFullMatching(queryObject, workflowItemsWithPartialMatching);

            /******  SEARCH logic  *****************************
             * 1. Full Typical       /  5. Partial Typical
             * 2. Full WorkFlowItem  /  6. Partial WorkFlowItem
             * 3. Full WorkFlow      /  7. Partial WorkFlow
             * 4. Full Folder        /  8. Partial Folder
             */

            AddDistinct(workflowItemTypicalsWithFullMatching);

            AddDistinct(workflowItemsWithFullMatching);

            AddDistinct(workflowsWithFullMatching);

            AddDistinct(foldersWithFullMatching);

            AddDistinct(workflowItemsWithPartialMatching);

            AddDistinct(workflowsWithPartialMatching);

            AddDistinct(foldersWithPartialMatching);

            return new QueryResponse<SearchWorkflowsResult[]>(container.ToArray(), container.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No workflows found in SubProject."));
        }

        public bool Validate(SearchWorkflows query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }

        private Item[] GetFoldersWithPartialMatching(SearchWorkflows queryObject, string[] wordList)
        {
            var foldersPartialQuery =
                from folder in _dbContext.Set<FolderData>().AsNoTracking()
                join workflow in _dbContext.Set<WorkflowData>().AsNoTracking()
                    on folder.Id equals workflow.FolderId
                where
                    folder.SubProjectId == queryObject.SubProjectId &&
                    wordList.Any(
                        word =>
                        (folder.Title ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant()) ||
                        (folder.Description ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant())
                    )
                select new Item
                {
                    Id = workflow.Id,
                    Title = workflow.Title,
                    Description = workflow.Description,
                    FolderId = workflow.FolderId,
                    FolderTitle = folder.Title,
                    FolderDescription = folder.Description
                };

            return foldersPartialQuery.ToArray();
        }

        private Item[] GetWorkflowsWithPartialMatching(SearchWorkflows queryObject, string[] wordList)
        {
            var workflowsPartialQuery =
                from workflow in _dbContext.Set<WorkflowData>().AsNoTracking()
                join folder in _dbContext.Set<FolderData>().AsNoTracking()
                    on workflow.FolderId equals folder.Id
                where
                    folder.SubProjectId == queryObject.SubProjectId &&
                    wordList.Any(
                        word =>
                            (workflow.Title ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant())
                    )
                select new Item
                {
                    Id = workflow.Id,
                    Title = workflow.Title,
                    Description = workflow.Description,
                    FolderId = workflow.FolderId,
                    FolderTitle = folder.Title,
                    FolderDescription = folder.Description
                };

            return workflowsPartialQuery.ToArray();
        }

        private Item[] GetWorkflowItemsWithPartialMatching(SearchWorkflows queryObject, string[] wordList)
        {
            var workflowsPartialQuery =
                from folder in _dbContext.Set<FolderData>().AsNoTracking()
                join workflow in _dbContext.Set<WorkflowData>().AsNoTracking()
                    on folder.Id equals workflow.FolderId
                join workflowitem in _dbContext.Set<WorkflowItemData>().AsNoTracking()
                    on workflow.Id equals workflowitem.WorkflowId
                where
                    (folder.SubProjectId == queryObject.SubProjectId) &&
                    wordList.Any(
                          word =>
                              (workflowitem.Title ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant()) ||
                              (workflowitem.Description ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant()) ||
                              (workflowitem.TypicalTitle ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant())

                    )
                select new Item
                {
                    Id = workflow.Id,
                    Title = workflow.Title,
                    Description = workflow.Description,
                    FolderId = workflow.FolderId,
                    FolderTitle = folder.Title,
                    FolderDescription = folder.Description,
                    WorkflowItemTitle = workflowitem.Title,
                    WorkflowItemDescription = workflowitem.Description,
                    TypicalTitle = workflowitem.TypicalTitle
                };

            return workflowsPartialQuery.ToArray();
        }

        private Item[] GetFoldersWithFullMatching(SearchWorkflows queryObject, Item[] foldersWithPartialMatching)
        {
            return foldersWithPartialMatching
                .Where(
                    item =>
                        (item.FolderTitle ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant() ||
                        (item.FolderDescription ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant()
                )
                .ToArray();
        }

        private Item[] GetWorkflowsWithFullMatching(SearchWorkflows queryObject, Item[] workflowsWithPartialMatching)
        {
            return workflowsWithPartialMatching
                .Where(
                    item =>
                        (item.Title ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant() ||
                        (item.Description ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant()
                )
                .ToArray();
        }

        private Item[] GetWorkflowItemsWithFullMatching(SearchWorkflows queryObject, Item[] workflowsWithPartialMatching)
        {
            return workflowsWithPartialMatching
                .Where(
                    item =>
                        (item.WorkflowItemTitle ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant() ||
                        (item.WorkflowItemDescription ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant()
                )
                .ToArray();
        }

        private Item[] GetWorkflowItemTypicalsWithFullMatching(SearchWorkflows queryObject, Item[] workflowsWithPartialMatching)
        {
            return workflowsWithPartialMatching
                .Where(
                    item =>
                        (item.TypicalTitle ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant()
                )
                .ToArray();
        }

        private List<SearchWorkflowsResult> AddDistinct(Item[] items)
        {
            foreach (var item in items)
            {
                if (!container.Any(w => w.Id == item.Id))
                {
                    container.Add(
                        new SearchWorkflowsResult(item.Id, item.Title, item.Description, item.FolderId)
                    );
                }
            }
            return container;
        }
    }

    internal sealed class Item
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public Guid FolderId { get; set; }

        public string FolderTitle { get; set; }

        public string FolderDescription { get; set; }

        public string TypicalTitle { get; internal set; }

        public string WorkflowItemTitle { get; set; }

        public string WorkflowItemDescription { get; set; }
    }
}