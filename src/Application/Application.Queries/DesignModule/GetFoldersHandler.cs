﻿using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Queries.DesignModule;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule.TreeView
{
    public sealed class GetFoldersHandler : IQueryHandler<GetFolders, GetFoldersResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetFoldersHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetFoldersResult[]> Handle(GetFolders queryObject)
        {
            var query = from folder in _dbContext.Set<FolderData>()
                        where folder.SubProjectId == queryObject.SubProjectId
                        select new
                        {
                            folder.Id,
                            folder.SubProjectId,
                            folder.Title,
                            folder.Description
                        };

            var result = query.OrderBy(ab => ab.Title).Select(folder => new GetFoldersResult(folder.Id, folder.SubProjectId, folder.Title, folder.Description)).ToArray();

            return new QueryResponse<GetFoldersResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No Folders found."));
        }

        public bool Validate(GetFolders query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}