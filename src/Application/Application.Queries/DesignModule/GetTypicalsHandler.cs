﻿using Microsoft.EntityFrameworkCore;
using SenseAI.Domain;
using Messaging.Queries;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DesignModule;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule
{
    public sealed class GetTypicalsHandler : IQueryHandler<GetTypicals, GetTypicalsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetTypicalsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTypicalsResult[]> Handle(GetTypicals queryObject)
        {
            List<GetTypicalsResult> result = new List<GetTypicalsResult>();
            var catalog = _dbContext.Set<CatalogData>().AsNoTracking().FirstOrDefault(ct => ct.Id == queryObject.CatalogId);
            if (catalog != null)
            {
                List<int> list = new List<int>
                        {
                            catalog.Type
                        };
                if (catalog.Type == (int)TypicalType.Application)
                    list.Add((int)TypicalType.Version);

                result = (from typical in _dbContext.Set<TypicalData>().AsNoTracking()
                         where typical.CatalogId == queryObject.CatalogId && list.Contains((int)typical.Type)
                         orderby typical.Title
                         select new GetTypicalsResult
                         (
                             typical.Id,
                             typical.Title
                         )).ToList();
            }

            return new QueryResponse<GetTypicalsResult[]>(result.ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No typicals found in catalog."));
        }

        public bool Validate(GetTypicals query)
        {
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}