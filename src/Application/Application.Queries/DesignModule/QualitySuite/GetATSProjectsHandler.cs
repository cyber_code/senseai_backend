﻿using External.QualitySuite;
using Messaging.Queries;
using Queries.DesignModule.QualitySuite;
using SenseAI.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule.QualitySuite
{
    internal class GetATSProjectsHandler : IQueryHandler<GetATSProject, GetATSProjectResult[]>
    {
        private readonly IQualitySuiteClient _service;
        private readonly External.QualitySuite.ApiModel.Credentials _credentials;

        public GetATSProjectsHandler(IQualitySuiteClient service)
        {
            _service = service;
            _credentials = new External.QualitySuite.ApiModel.Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetATSProjectResult[]> Handle(GetATSProject queryObject)
        {
            var clientService = _service.CreateClient(_credentials);
            var projects = clientService.GetProjectList().Select(ct => new GetATSProjectResult(ct.Name, ct.Noderef));

            return new QueryResponse<GetATSProjectResult[]>(projects.ToArray(), projects.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
             string.Format("No project found in ATS.")));
        }

        public bool Validate(GetATSProject query)
        {
            return true;
        }
    }
}