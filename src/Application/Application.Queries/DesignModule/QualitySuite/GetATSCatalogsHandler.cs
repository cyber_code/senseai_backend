﻿using External.QualitySuite;
using Messaging.Queries;
using Queries.DesignModule.QualitySuite;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule.QualitySuite
{
    internal class GetATSCatalogsHandler : IQueryHandler<GetATSCatalogs, GetATSCatalogsResult[]>
    {
        private readonly IQualitySuiteClient _service;
        private readonly External.QualitySuite.ApiModel.Credentials _credentials;

        public GetATSCatalogsHandler(IQualitySuiteClient service)
        {
            _service = service;
            _credentials = new External.QualitySuite.ApiModel.Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetATSCatalogsResult[]> Handle(GetATSCatalogs queryObject)
        {
            var clientService = _service.CreateClient(_credentials,queryObject.ProjectId, queryObject.SubProjectId);
            var catalogs = clientService.GetCatalogs().Select(ct => new GetATSCatalogsResult(ct.Name, ct.Noderef));

            return new QueryResponse<GetATSCatalogsResult[]>(catalogs.ToArray(), catalogs.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
              string.Format("No catalogs found in ATS.")));
        }

        public bool Validate(GetATSCatalogs query)
        {
            if (query.ProjectId == 0)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == 0)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}