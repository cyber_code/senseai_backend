﻿using External.QualitySuite;
using Messaging.Queries;
using Queries.DesignModule.QualitySuite;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SenseAI.Domain;
using External.QualitySuite.ApiModel;

namespace Application.Queries.DesignModule.QualitySuite
{
    internal class GetATSAdaptersPerSystemsHandler : IQueryHandler<GetATSAdaptersPerSystems, GetATSAdaptersPerSystemsResult[]>
    {
        private readonly IQualitySuiteClient _service;
        private readonly External.QualitySuite.ApiModel.Credentials _credentials;

        public GetATSAdaptersPerSystemsHandler(IQualitySuiteClient service)
        {
            _service = service;
            _credentials = new External.QualitySuite.ApiModel.Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetATSAdaptersPerSystemsResult[]> Handle(GetATSAdaptersPerSystems queryObject)
        {
            var clientService = _service.CreateClient(_credentials, queryObject.ProjectId, queryObject.SubProjectId);
            var products = clientService.GetAdaptersPerSystems(queryObject.SubProjectId).Select(ct => new GetATSAdaptersPerSystemsResult(ct.Key,( ((List<ValidataItem>)ct.Value).Select(cti=> new GetATSAdaptersPerSystemsResultItem(cti.Name,cti.Noderef))).ToArray()));

            return new QueryResponse<GetATSAdaptersPerSystemsResult[]>(products.ToArray(), products.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
                string.Format("No Adapters found for projectId: {0} and subProjectId: {1}", queryObject.ProjectId, queryObject.SubProjectId)));
        }

        public bool Validate(GetATSAdaptersPerSystems query)
        {
            if (query.ProjectId == 0)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == 0)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
        
            return true;
        }
    }
}