﻿using External.QualitySuite;
using Messaging.Queries;
using Queries.DesignModule.QualitySuite;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SenseAI.Domain;

namespace Application.Queries.DesignModule.QualitySuite
{
    internal class GetATSReleaseSummaryHandler : IQueryHandler<GetATSReleaseSummary, GetATSReleaseSummaryResult>
    {
        private readonly IQualitySuiteClient _service;
        private readonly External.QualitySuite.ApiModel.Credentials _credentials;

        public GetATSReleaseSummaryHandler(IQualitySuiteClient service)
        {
            _service = service;
            _credentials = new External.QualitySuite.ApiModel.Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetATSReleaseSummaryResult> Handle(GetATSReleaseSummary queryObject)
        {
            var clientService = _service.CreateClient(_credentials, queryObject.ProjectId, queryObject.SubProjectId);
            var releaseSummary = clientService.GetReleaseSummary(queryObject.ReleaseIterationId);
            GetATSReleaseSummaryResult result = null;
            if (releaseSummary != null)
                result = new GetATSReleaseSummaryResult(releaseSummary.TotalTestCaseExecutions, releaseSummary.PassedTestCaseExecutions, releaseSummary.OutstandingDefects);

            return new QueryResponse<GetATSReleaseSummaryResult>(result, result == null ? null : new QueryResponseMessage((int)MessageType.Information,
                                        string.Format("No items found in release iteration.")));
        }

        public bool Validate(GetATSReleaseSummary query)
        {
            if (query.ReleaseIterationId == 0)
            {
                Errors.Add("ReleaseIterationId cannot be empty.");
                return false;
            }
            if (query.ProjectId == 0)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == 0)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}