﻿using External.QualitySuite;
using Messaging.Queries;
using Queries.DesignModule.QualitySuite;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SenseAI.Domain;

namespace Application.Queries.DesignModule.QualitySuite
{
    internal class GetATSFeaturesHandler : IQueryHandler<GetATSFeatures, GetATSFeaturesResult[]>
    {
        private readonly IQualitySuiteClient _service;
        private readonly External.QualitySuite.ApiModel.Credentials _credentials;

        public GetATSFeaturesHandler(IQualitySuiteClient service)
        {
            _service = service;
            _credentials = new External.QualitySuite.ApiModel.Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetATSFeaturesResult[]> Handle(GetATSFeatures queryObject)
        {
            var clientService = _service.CreateClient(_credentials, queryObject.ProjectId, queryObject.SubProjectId);
            List<GetATSFeaturesResult> features = new List<GetATSFeaturesResult>();
            if (queryObject.FolderId == 0)
                features = clientService.GetFeatures(queryObject.ProductId).Select(ct => new GetATSFeaturesResult(ct.Name, ct.Noderef, ct.Type)).ToList();
            else
                features = clientService.GetFeatures(queryObject.ProductId, queryObject.FolderId).Select(ct => new GetATSFeaturesResult(ct.Name, ct.Noderef, "Feature")).ToList();

            return new QueryResponse<GetATSFeaturesResult[]>(features.ToArray(), features.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
                string.Format("The given product does not have any Features.")));
        }

        public bool Validate(GetATSFeatures query)
        {
            if (query.ProjectId == 0)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == 0)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (query.ProductId == 0)
            {
                Errors.Add("ProductId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}