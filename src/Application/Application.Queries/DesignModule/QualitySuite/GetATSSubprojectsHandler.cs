﻿using External.QualitySuite;
using Messaging.Queries;
using Queries.DesignModule.QualitySuite;
using SenseAI.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule.QualitySuite
{
    internal class GetATSSubprojectsHandler : IQueryHandler<GetATSSubproject, GetATSSubprojectResult[]>
    {
        private readonly IQualitySuiteClient _service;
        private readonly External.QualitySuite.ApiModel.Credentials _credentials;

        public GetATSSubprojectsHandler(IQualitySuiteClient service)
        {
            _service = service;
            _credentials = new External.QualitySuite.ApiModel.Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetATSSubprojectResult[]> Handle(GetATSSubproject queryObject)
        {
            var clientService = _service.CreateClient(_credentials);
            var subProjects = clientService.GetSubProjectList(queryObject.NoderefField.ToString()).Select(ct => new GetATSSubprojectResult(ct.Name, ct.Noderef));

            return new QueryResponse<GetATSSubprojectResult[]>(subProjects.ToArray(), subProjects.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
             string.Format("No sub project found in ATS, in project.")));
        }

        public bool Validate(GetATSSubproject query)
        {
            if (query.NoderefField == 0)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}