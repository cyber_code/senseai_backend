﻿using External.QualitySuite;
using Messaging.Queries;
using Queries.DesignModule.QualitySuite;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SenseAI.Domain;

namespace Application.Queries.DesignModule.QualitySuite
{
    internal class GetATSUsercasesHandler : IQueryHandler<GetATSUsecases, GetATSUsecasesResult[]>
    {
        private readonly IQualitySuiteClient _service;
        private readonly External.QualitySuite.ApiModel.Credentials _credentials;

        public GetATSUsercasesHandler(IQualitySuiteClient service)
        {
            _service = service;
            _credentials = new External.QualitySuite.ApiModel.Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetATSUsecasesResult[]> Handle(GetATSUsecases queryObject)
        {
            var clientService = _service.CreateClient(_credentials, queryObject.ProjectId, queryObject.SubProjectId);
            List<GetATSUsecasesResult> userCases = new List<GetATSUsecasesResult>();
            if (queryObject.FolderId == 0)
                userCases = clientService.GetUseCases(queryObject.FeatureId).Select(ct => new GetATSUsecasesResult(ct.Name, ct.Noderef, ct.Type)).ToList();
            else
                userCases = clientService.GetUseCases(queryObject.FeatureId, queryObject.FolderId).Select(ct => new GetATSUsecasesResult(ct.Name, ct.Noderef, "UseCase")).ToList();

            return new QueryResponse<GetATSUsecasesResult[]>(userCases.ToArray(), userCases.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
                string.Format("The given feature does not have any use case.")));
        }

        public bool Validate(GetATSUsecases query)
        {
            if (query.ProjectId == 0)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == 0)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.FeatureId == 0)
            {
                Errors.Add("FeatureId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}