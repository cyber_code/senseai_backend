﻿using External.QualitySuite;
using Messaging.Queries;
using Queries.DesignModule.QualitySuite;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SenseAI.Domain;

namespace Application.Queries.DesignModule.QualitySuite
{
    internal class GetATSRequirementsHandler : IQueryHandler<GetATSRequirements, GetATSRequirementsResult[]>
    {
        private readonly IQualitySuiteClient _service;
        private readonly External.QualitySuite.ApiModel.Credentials _credentials;

        public GetATSRequirementsHandler(IQualitySuiteClient service)
        {
            _service = service;
            _credentials = new External.QualitySuite.ApiModel.Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetATSRequirementsResult[]> Handle(GetATSRequirements queryObject)
        {
            var clientService = _service.CreateClient(_credentials, queryObject.ProjectId, queryObject.SubProjectId);
            var features = clientService.GetRequirements(queryObject.UseCaseId).Select(ct => new GetATSRequirementsResult(ct.Name, ct.Noderef));

            return new QueryResponse<GetATSRequirementsResult[]>(features.ToArray(), features.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
                string.Format("The given usecase does not have any requirements.", queryObject.UseCaseId)));
        }

        public bool Validate(GetATSRequirements query)
        {
            if (query.ProjectId == 0)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == 0)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.UseCaseId == 0)
            {
                Errors.Add("UseCaseId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}