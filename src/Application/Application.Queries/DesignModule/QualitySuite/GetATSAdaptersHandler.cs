﻿using External.QualitySuite;
using Messaging.Queries;
using Queries.DesignModule.QualitySuite;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SenseAI.Domain;

namespace Application.Queries.DesignModule.QualitySuite
{
    internal class GetATSAdaptersHandler : IQueryHandler<GetATSAdapters, GetATSAdaptersResult[]>
    {
        private readonly IQualitySuiteClient _service;
        private readonly External.QualitySuite.ApiModel.Credentials _credentials;

        public GetATSAdaptersHandler(IQualitySuiteClient service)
        {
            _service = service;
            _credentials = new External.QualitySuite.ApiModel.Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetATSAdaptersResult[]> Handle(GetATSAdapters queryObject)
        {
            var clientService = _service.CreateClient(_credentials, queryObject.ProjectId, queryObject.SubProjectId);
            var products = clientService.GetAdapters(queryObject.SubProjectId).Select(ct => new GetATSAdaptersResult(ct.Name, ct.Noderef));

            return new QueryResponse<GetATSAdaptersResult[]>(products.ToArray(), products.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
                string.Format("No Adapters found for projectId: {0} and subProjectId: {1}.", queryObject.ProjectId, queryObject.SubProjectId)));
        }

        public bool Validate(GetATSAdapters query)
        {
            if (query.ProjectId == 0)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == 0)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}