﻿using External.QualitySuite;
using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.SystemModel;
using Persistence.TestCaseModel;
using Persistence.WorkflowModel;
using Queries.DesignModule.QualitySuite;
using Queries.DesignModule.System;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule.Systems
{
    public sealed class CheckATSTestCasesHandler : IQueryHandler<CheckATSTestCases, bool>
    {
        private readonly IDbContext _dbContext;
        private readonly IQualitySuiteClient _service;
        private readonly External.QualitySuite.ApiModel.Credentials _credentials;
        int versionId = 0;

        public CheckATSTestCasesHandler(IDbContext dbContext, IQualitySuiteClient service)
        {
            _dbContext = dbContext;
            _service = service;
            _credentials = new External.QualitySuite.ApiModel.Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };
        }

        public List<string> Errors { get; set; }

        public QueryResponse<bool> Handle(CheckATSTestCases queryObject)
        {
            bool result = true;
            QueryResponseMessage queryResponseMessage;

            if (queryObject.WorkflowPathId != Guid.Empty)
                versionId = _dbContext.Set<WorkflowVersionPathsData>().AsNoTracking().FirstOrDefault(w => w.Id == queryObject.WorkflowPathId).VersionId;
            else
                versionId = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == queryObject.WorkflowId && w.IsLast).VersionId;

            var atsTC = _dbContext.Set<ATSTestCasesData>().AsNoTracking()
                .Where(u => u.WorkflowId == queryObject.WorkflowId && u.VersionId == versionId).ToList();

            if (atsTC.Count() == 0)
            {
                result = true;
                queryResponseMessage = new QueryResponseMessage((int)MessageType.Successful, "Test case can be exported to ATS");
            }
            else if (atsTC.FirstOrDefault().ProjectNodref == queryObject.ProjectId && atsTC.FirstOrDefault().SubProjectNodref == queryObject.SubProjectId)
            {
                result = true;
                queryResponseMessage = new QueryResponseMessage((int)MessageType.Successful, "Test case can be exported to ATS");
            }
            else
            {
                result = false;

                var clientService = _service.CreateClient(_credentials);
                var subProjects = clientService.GetSubProjectList(atsTC.FirstOrDefault().ProjectNodref.ToString()).Select(ct => new GetATSSubprojectResult(ct.Name, ct.Noderef));
                var subProjectName = subProjects.Where(u => u.NoderefField == atsTC.FirstOrDefault().SubProjectNodref.ToString()).FirstOrDefault().NameField;

                if (atsTC.Count == 1)
                    queryResponseMessage = new QueryResponseMessage((int)MessageType.Failed, "Please export this Versioned Test Case to [" + subProjectName + "] Subproject");
                else
                    queryResponseMessage = new QueryResponseMessage((int)MessageType.Failed, "Please export these Versioned Test Cases to [" + subProjectName + "] Subproject");
            }

            return new QueryResponse<bool>(result, queryResponseMessage);
        }

        public bool Validate(CheckATSTestCases query)
        {
            return true;
        }
    }
}