﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using NeuralEngine;
using NeuralSuggestion = NeuralEngine.Suggestion;
using Newtonsoft.Json;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule.Suggestions;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;
using QueriesDynamicData = Queries.DesignModule.Suggestions.DynamicData;
using Persistence.CatalogModel;

namespace Application.Queries.DesignModule.Suggestions
{
    public sealed class GetComputerGeneratedWorkflowsHandler : IQueryHandler<GetComputerGeneratedWorkflows, GetComputerGeneratedWorkflowsResult>
    {
        private readonly INeuralEngineClient _service;
        private readonly IDbContext _dbContext;

        public GetComputerGeneratedWorkflowsHandler(INeuralEngineClient service, IDbContext dbContext)
        {
            _service = service;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetComputerGeneratedWorkflowsResult> Handle(GetComputerGeneratedWorkflows queryObject)
        {
            string message = string.Empty;
            SenseAI.Domain.MessageType messageType;
            GetComputerGeneratedWorkflowsResult workflowresult = null;
            List<Workflows> resultList = new List<Workflows>();
            Workflows result = null;
            NeuralSuggestion.GetComputerGeneratedWorkflowsResponse respond = null;

            NeuralSuggestion.GetComputerGeneratedWorkflowsRequest request = new NeuralSuggestion.GetComputerGeneratedWorkflowsRequest
            {
                CatalogId = queryObject.CatalogId,
                ProjectId = queryObject.ProjectId,
                SubProjectId = queryObject.SubProjectId,
                TenantId = queryObject.TenantId,
                TypicalId = queryObject.TypicalId,
                TypicalTitle = queryObject.TypicalTitle,
                WorkflowTitle = queryObject.WorkflowTitle
            };

            try
            {
                //TO-DO - Call ML service
                respond = _service.GetWorkflows(request);

                //List<NeuralSuggestion.Workflows> workflowItemList = new List<NeuralSuggestion.Workflows>();
                //var lstworkflows = _dbContext.Set<WorkflowData>().AsNoTracking().ToList().GetRange(0, 10);
                //foreach (var workflow in lstworkflows)
                //{
                //    if (workflow.DesignerJson == null || workflow.WorkflowPathsJson == null)
                //        continue;
                //    NeuralSuggestion.Workflows item = new NeuralSuggestion.Workflows();
                //    var exported = WorkflowFactory.Create(workflow.DesignerJson);
                //    item.WorkflowItem = exported.WorkflowItem;
                //    item.WorkflowItemLink = exported.WorkflowItemLink;
                //    WorkflowPathDesinger workflowPathDesinger = JsonConvert.DeserializeObject<WorkflowPathDesinger>(workflow.WorkflowPathsJson);
                //    int indexNumber = 1;
                //    item.WorkflowPath = workflowPathDesinger.Paths.Select(ct => new NeuralSuggestion.WorkflowPath
                //    {
                //        Number = indexNumber++,
                //        Coverage = ct.Coverage == "N/A" ? 0 : decimal.Parse(ct.Coverage),
                //        IsBestPath = ct.IsBestPath,
                //        ItemIds = ct.ItemIds
                //    }).ToList();
                //    workflowItemList.Add(item);
                //}
                //respond = new NeuralSuggestion.GetComputerGeneratedWorkflowsResponse
                //{
                //    Workflows = workflowItemList
                //};

                int indexTitleNumber = 1;
                foreach (var res in respond.Workflows)
                {
                    string GetCatalogName(Guid catalogId)
                    {
                        var catalogTitle = _dbContext.Set<CatalogData>().AsNoTracking().Where(a => a.Id == catalogId).FirstOrDefault();
                        return !string.IsNullOrEmpty(catalogTitle.Title) ? catalogTitle.Title : "";
                    }
                    var item = res.WorkflowItem.Select(it => new GetSuggestionResultWorkflowItem(it.Id, it.Title,
                            it.Description, it.CatalogId,
                            GetCatalogName(it.CatalogId),
                            it.TypicalId, it.SystemId, it.TypicalTitle, (string.IsNullOrEmpty(it.TypicalType) || it.TypicalType == "0" ? "1" : ((int)Enum.Parse(typeof(TypicalType), it.TypicalType)).ToString()),//DOTO: after the activation for typical type
                            it.ActionType != "None" ? (int)Enum.Parse(typeof(ActionType), it.ActionType) : -1,
                            it.DynamicData == null ? null : it.DynamicData.Select(dd => new QueriesDynamicData(dd.SourceWorkflowItemId, dd.SourceAttributeTitle, dd.TargetAttributeTitle, dd.FormulaText)).ToArray(),
                            (int)Enum.Parse(typeof(WorkflowType), it.Type),
                            it.IsNegativeStep,
                            it.ExpectedError ?? ""
                            )).ToList();

                    var itemLink = res.WorkflowItemLink.Select(itLink => new GetSuggestionResultWorkflowItemLink(itLink.SourceId
                            , itLink.TargetId, (SenseAI.Domain.WorkflowModel.WorkflowItemLinkState)((int)itLink.State))).ToList();

                    var path = res.WorkflowPath.Select(p => new GetPathsResultWorkflowPath("", p.Number, p.Coverage == 0 ? "N/A" : p.Coverage.ToString() + "%", p.IsBestPath, p.ItemIds)).ToList();

                    result = new Workflows($"{queryObject.WorkflowTitle} {indexTitleNumber++}", item,
                        itemLink, path
                        );

                    resultList.Add(result);
                }
                workflowresult = new GetComputerGeneratedWorkflowsResult(resultList);
                messageType = SenseAI.Domain.MessageType.Successful;
            }
            catch (Exception exMessage)
            {
                messageType = SenseAI.Domain.MessageType.Failed;
                message = exMessage.Message;
            }

            return new QueryResponse<GetComputerGeneratedWorkflowsResult>(workflowresult, new QueryResponseMessage((int)messageType, message));
        }

        public bool Validate(GetComputerGeneratedWorkflows query)
        {
            if (query.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (query.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (query.TypicalTitle == string.Empty)
            {
                Errors.Add("Typical title can't be empty.");
                return false;
            }
            if (query.TypicalId == Guid.Empty)
            {
                Errors.Add("Typical id can't be empty.");
                return false;
            }
            if (query.WorkflowTitle == string.Empty)
            {
                Errors.Add("Workflow title can't be empty.");
                return false;
            }
            return true;
        }
    }
}