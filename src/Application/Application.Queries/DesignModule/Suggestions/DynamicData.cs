﻿using System;

namespace Application.Queries.DesignModule.Suggestions
{
    public sealed class DynamicData
    {
        public DynamicData(Guid sourceWorkflowItemId, string sourceAttributeName, string targetAttributeName, string formulaText)
        {
            SourceWorkflowItemId = sourceWorkflowItemId;
            SourceAttributeName = sourceAttributeName;
            TargetAttributeName = targetAttributeName;
            FormulaText = formulaText;
        }

        public Guid SourceWorkflowItemId { get; set; }

        public string SourceAttributeName { get; set; }

        public string TargetAttributeName { get; set; }

        public string FormulaText { get; set; }
    }
}