﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NeuralEngine;
using NeuralEngine.DataSet;
using NeuralEngine.Suggestion;
using Queries.DataModule.Data.DataSets;
using QueriesSuggestions = Queries.DesignModule.Suggestions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule.Suggestions
{
    public sealed class WorkflowFactory
    {
        public static Workflows Create(string DesingerJson)
        {
            var json = JObject.Parse(DesingerJson);
            var workflowDesignerData = JsonConvert.DeserializeObject<WorkflowDesignerData>(json.ToString());
            var getSuggestionsRequest = Create(workflowDesignerData);
            return getSuggestionsRequest;
        }

        public static Workflows Create(WorkflowDesignerData workflowDesignerData)
        {
            var getSuggestionsRequest = new Workflows();
            var links = new List<WorkflowItemLink>();
            var items = new List<WorkflowItem>();
            foreach (var cell in workflowDesignerData.Cells)
            {
                if (cell.Type.ToLowerInvariant() == "link" || cell.Type.ToLowerInvariant() == "app.link" || cell.Type.ToLowerInvariant() == "standard.link")
                {
                    var workflowItemLink = new WorkflowItemLink
                    {
                        SourceId = cell.Source.Id,
                        TargetId = cell.Target.Id,
                        State = (NeuralEngine.Suggestion.WorkflowItemLinkState)(cell.State)
                    };
                    links.Add(workflowItemLink);
                }
                else
                {
                    var item = cell.CustomData;
                    switch (item?.Type)
                    {
                        case WorkflowType.Start:
                            var start = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                /*No need for DynamicData*/
                                Id = item.Id,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Start",
                                TypicalId = item.TypicalId,
                                TypicalTitle = item.TypicalName,
                                TypicalType = "0"
                            };
                            items.Add(start);
                            break;

                        case WorkflowType.Action:
                        case WorkflowType.Condition:
                            var workflowItem = new WorkflowItem
                            {
                                ActionType = item.Type == WorkflowType.Condition ? "None" : ((ActionType)item.ActionType).ToString(),
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                DynamicData = item.DynamicDatas == null ? new NeuralEngine.Suggestion.DynamicData[0] : item.DynamicDatas.Select(
                                    dynamicDatas => new NeuralEngine.Suggestion.DynamicData
                                    {
                                        FormulaText = "",
                                        SourceWorkflowItemId = dynamicDatas.SourceWorkflowItemId,
                                        SourceAttributeTitle = dynamicDatas.SourceAttributeName,
                                        TargetAttributeTitle = dynamicDatas.TargetAttributeName
                                    }).ToArray(),
                                Id = item.Id,
                                /*No need for SystemId*/
                                Title = item.Title,
                                Type = item.Type.ToString(),
                                TypicalId = item.TypicalId,
                                TypicalTitle = item.TypicalName,
                                TypicalType = string.IsNullOrEmpty(item.TypicalType) ? "0" : ((TypicalType)int.Parse(item.TypicalType)).ToString()
                            };
                            items.Add(workflowItem);
                            break;

                        case WorkflowType.Linked:
                            var linked = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                Id = item.Id,
                                WorkflowId = item.WorkflowId,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Linked",
                                TypicalId = item.TypicalId,
                                TypicalType = "0"
                            };
                            items.Add(linked);
                            break;

                        case WorkflowType.Manual:
                            var manual = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                Id = item.Id,
                                WorkflowId = item.WorkflowId,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Manual",
                                TypicalId = item.TypicalId,
                                TypicalType = "0"
                            };
                            items.Add(manual);
                            break;

                        default:
                            break;
                    }
                }
            }
            getSuggestionsRequest.WorkflowItem = items;
            getSuggestionsRequest.WorkflowItemLink = links;
            return getSuggestionsRequest;
        }

        public static GetSuggestionsRequest Create(QueriesSuggestions.GetSuggestions queryObject)
        {
            var json = JObject.Parse(queryObject.Json);
            var workflowDesignerData = JsonConvert.DeserializeObject<WorkflowDesignerData>(json.ToString());
            var getSuggestionsRequest = Create(queryObject, workflowDesignerData);
            return getSuggestionsRequest;
        }

        public static GetSuggestionsRequest Create(QueriesSuggestions.GetSuggestions queryObject, WorkflowDesignerData workflowDesignerData)
        {
            var getSuggestionsRequest = new GetSuggestionsRequest();
            var links = new List<WorkflowItemLink>();
            var items = new List<WorkflowItem>();
            foreach (var cell in workflowDesignerData.Cells)
            {
                if (cell.Type.ToLowerInvariant() == "link" || cell.Type.ToLowerInvariant() == "app.link" || cell.Type.ToLowerInvariant() == "standard.link")
                {
                    var workflowItemLink = new WorkflowItemLink
                    {
                        SourceId = cell.Source.Id,
                        TargetId = cell.Target.Id,
                        State = (NeuralEngine.Suggestion.WorkflowItemLinkState)(cell.State)
                    };
                    links.Add(workflowItemLink);
                }
                else
                {
                    var item = cell.CustomData;
                    switch (item?.Type)
                    {
                        case WorkflowType.Start:
                            var start = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                /*No need for DynamicData*/
                                Id = item.Id,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Start",
                                TypicalId = item.TypicalId,
                                TypicalTitle = item.TypicalName,
                                TypicalType = "0"
                            };
                            items.Add(start);
                            break;

                        case WorkflowType.Action:
                        case WorkflowType.Condition:
                            var workflowItem = new WorkflowItem
                            {
                                ActionType = item.Type == WorkflowType.Condition ? "None" : ((ActionType)item.ActionType).ToString(),
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                DynamicData = item.DynamicDatas == null ? new NeuralEngine.Suggestion.DynamicData[0] : item.DynamicDatas.Select(
                                    dynamicDatas => new NeuralEngine.Suggestion.DynamicData
                                    {
                                        FormulaText = "",
                                        SourceWorkflowItemId = dynamicDatas.SourceWorkflowItemId,
                                        SourceAttributeTitle = dynamicDatas.SourceAttributeName,
                                        TargetAttributeTitle = dynamicDatas.TargetAttributeName
                                    }).ToArray(),
                                Id = item.Id,
                                /*No need for SystemId*/
                                Title = item.Title,
                                Type = item.Type.ToString(),
                                TypicalId = item.TypicalId,
                                TypicalTitle = item.TypicalName,
                                TypicalType = string.IsNullOrEmpty(item.TypicalType) ? "0" : ((TypicalType)int.Parse(item.TypicalType)).ToString()
                            };
                            items.Add(workflowItem);
                            break;

                        case WorkflowType.Linked:
                            var linked = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                Id = item.Id,
                                WorkflowId = item.WorkflowId,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Linked",
                                TypicalId = item.TypicalId,
                                TypicalType = "0"
                            };
                            items.Add(linked);
                            break;

                        case WorkflowType.Manual:
                            var manual = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                Id = item.Id,
                                WorkflowId = item.WorkflowId,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Manual",
                                TypicalId = item.TypicalId,
                                TypicalType = "0"
                            };
                            items.Add(manual);
                            break;

                        default:
                            break;
                    }
                }
            }
            getSuggestionsRequest.CurrentItemId = queryObject.CurrentItemId;
            getSuggestionsRequest.TenantId = queryObject.TenantId;
            getSuggestionsRequest.ProjectId = queryObject.ProjectId;
            getSuggestionsRequest.SubProjectId = queryObject.SubProjectId;
            getSuggestionsRequest.CatalogId = queryObject.CatalogId;
            // DAMTODO getSuggestionsRequest.SystemId = queryObject.SystemId;
            getSuggestionsRequest.WorkflowItemLinks = links.ToArray();
            getSuggestionsRequest.WorkflowItems = items.ToArray();
            return getSuggestionsRequest;
        }

        public static GetPathsRequest Create(QueriesSuggestions.GetPaths queryObject)
        {
            var json = JObject.Parse(queryObject.Json);
            var workflowDesignerData = JsonConvert.DeserializeObject<WorkflowDesignerData>(json.ToString());
            var getSuggestionsRequest = Create(queryObject, workflowDesignerData);
            return getSuggestionsRequest;
        }

        public static GetPathsRequest Create(QueriesSuggestions.GetPaths queryObject, WorkflowDesignerData workflowDesignerData)
        {
            var getPaths = new GetPathsRequest();
            var links = new List<WorkflowItemLink>();
            var items = new List<WorkflowItem>();
            foreach (var cell in workflowDesignerData.Cells)
            {
                if (cell.Type.ToLowerInvariant() == "link" || cell.Type.ToLowerInvariant() == "app.link" || cell.Type.ToLowerInvariant() == "standard.Link")
                {
                    var workflowItemLink = new WorkflowItemLink
                    {
                        SourceId = cell.Source.Id,
                        TargetId = cell.Target.Id,
                        State = (NeuralEngine.Suggestion.WorkflowItemLinkState)(cell.State)
                    };
                    links.Add(workflowItemLink);
                }
                else
                {
                    var item = cell.CustomData;
                    switch (item?.Type)
                    {
                        case WorkflowType.Start:
                            var start = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                /*No need for DynamicData*/
                                Id = item.Id,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Start",
                                TypicalId = item.TypicalId,
                                TypicalTitle = item.TypicalName,
                                TypicalType = "0"
                            };
                            items.Add(start);
                            break;

                        case WorkflowType.Action:
                        case WorkflowType.Condition:
                            var workflowItem = new WorkflowItem
                            {
                                ActionType = item.Type == WorkflowType.Condition ? "None" : ((ActionType)item.ActionType).ToString(),
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                DynamicData = item.DynamicDatas == null ? new NeuralEngine.Suggestion.DynamicData[0] : item.DynamicDatas.Select(
                                    dynamicDatas => new NeuralEngine.Suggestion.DynamicData
                                    {
                                        FormulaText = "",
                                        SourceWorkflowItemId = dynamicDatas.SourceWorkflowItemId,
                                        SourceAttributeTitle = dynamicDatas.SourceAttributeName,
                                        TargetAttributeTitle = dynamicDatas.TargetAttributeName
                                    }).ToArray(),
                                Id = item.Id,
                                /*No need for SystemId*/
                                Title = item.Title,
                                Type = item.Type.ToString(),
                                TypicalId = item.TypicalId,
                                TypicalTitle = item.TypicalName,
                                TypicalType = string.IsNullOrEmpty(item.TypicalType) ? "0" : ((TypicalType)int.Parse(item.TypicalType)).ToString()
                            };
                            items.Add(workflowItem);
                            break;

                        default:
                            break;
                    }
                }
            }

            getPaths.TenantId = queryObject.TenantId;
            getPaths.ProjectId = queryObject.ProjectId;
            getPaths.SubProjectId = queryObject.SubProjectId;
            getPaths.CatalogId = queryObject.CatalogId;
            // DAMTODO getPaths.SystemId = queryObject.SystemId;
            getPaths.WorkflowItemLinks = links.ToArray();
            getPaths.WorkflowItems = items.ToArray();
            return getPaths;
        }

        public static WorkflowCoverageRequest Create(QueriesSuggestions.GetWorkflowCoverage queryObject)
        {
            var getCoverage = new WorkflowCoverageRequest();
            getCoverage.TenantId = queryObject.TenantId;
            getCoverage.ProjectId = queryObject.ProjectId;
            getCoverage.SubProjectId = queryObject.SubProjectId;
            getCoverage.CatalogId = queryObject.CatalogId;
            //getCoverage.SystemId = queryObject.SystemId;
            getCoverage.WorkflowId = queryObject.WorkflowId;
            return getCoverage;
        }

        public static IssueWorkflowRequest Create(QueriesSuggestions.IssueWorkflowToML queryObject, string Json, int versionId)
        {
            var json = JObject.Parse(Json);
            var workflowDesignerData = JsonConvert.DeserializeObject<WorkflowDesignerData>(json.ToString());

            return Create(queryObject, workflowDesignerData, versionId);
        }

        public static IssueWorkflowRequest Create(QueriesSuggestions.IssueWorkflowToML queryObject, WorkflowDesignerData workflowDesignerData, int versionId)
        {
            var issueWorkflow = new IssueWorkflowRequest();
            var links = new List<WorkflowItemLink>();
            var items = new List<WorkflowItem>();
            foreach (var cell in workflowDesignerData.Cells)
            {
                if (cell.Type.ToLowerInvariant() == "link" || cell.Type.ToLowerInvariant() == "app.link" || cell.Type.ToLowerInvariant() == "standard.link")
                {
                    var workflowItemLink = new WorkflowItemLink
                    {
                        SourceId = cell.Source.Id,
                        TargetId = cell.Target.Id,
                        State = (NeuralEngine.Suggestion.WorkflowItemLinkState)(cell.State)
                    };
                    links.Add(workflowItemLink);
                }
                else
                {
                    var item = cell.CustomData;
                    switch (item?.Type)
                    {
                        case WorkflowType.Start:
                            var start = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                /*No need for DynamicData*/
                                WorkflowId = queryObject.WorkflowId,
                                Id = item.Id,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Start",
                                TypicalId = item.TypicalId,
                                TypicalTitle = item.TypicalName,
                                TypicalType = "0"
                            };
                            items.Add(start);
                            break;

                        case WorkflowType.Action:
                        case WorkflowType.Condition:
                            var workflowItem = new WorkflowItem
                            {
                                ActionType = item.Type == WorkflowType.Condition ? "None" : ((ActionType)item.ActionType).ToString(),
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                DynamicData = item.DynamicDatas == null ? new NeuralEngine.Suggestion.DynamicData[0] : item.DynamicDatas.Select(
                                    dynamicDatas => new NeuralEngine.Suggestion.DynamicData
                                    {
                                        FormulaText = "",
                                        SourceWorkflowItemId = dynamicDatas.SourceWorkflowItemId,
                                        SourceAttributeTitle = dynamicDatas.SourceAttributeName,
                                        TargetAttributeTitle = dynamicDatas.TargetAttributeName
                                    }).ToArray(),
                                Id = item.Id,
                                WorkflowId = queryObject.WorkflowId,
                                /*No need for SystemId*/
                                Title = item.Title,
                                Type = item.Type.ToString(),
                                TypicalId = item.TypicalId,
                                TypicalTitle = item.TypicalName,
                                TypicalType = string.IsNullOrEmpty(item.TypicalType) ? "0" : ((TypicalType)int.Parse(item.TypicalType)).ToString()
                            };
                            items.Add(workflowItem);
                            break;

                        case WorkflowType.Linked:
                            var linked = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                Id = item.Id,
                                WorkflowId = queryObject.WorkflowId,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Linked",
                                TypicalId = item.TypicalId,
                                TypicalType = "0"
                            };
                            items.Add(linked);
                            break;

                        case WorkflowType.Manual:
                            var manual = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                Id = item.Id,
                                WorkflowId = queryObject.WorkflowId,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Manual",
                                TypicalId = item.TypicalId,
                                TypicalType = "0"
                            };
                            items.Add(manual);
                            break;

                        default:
                            break;
                    }
                }
            }
            issueWorkflow.VersionId = versionId;
            issueWorkflow.TenantId = queryObject.TenantId;
            issueWorkflow.ProjectId = queryObject.ProjectId;
            issueWorkflow.SubProjectId = queryObject.SubProjectId;
            issueWorkflow.CatalogId = queryObject.CatalogId;
            issueWorkflow.WorkflowId = queryObject.WorkflowId;
            // DAMTODO issueWorkflow.SystemId = queryObject.SystemId;
            issueWorkflow.WorkflowItemLinks = links.ToArray();
            issueWorkflow.WorkflowItems = items.ToArray();
            return issueWorkflow;
        }

        public static GetDataSetsItemsRequest Create(GetSuggestedDataSets queryObject, string desingerJson)
        {
            var json = JObject.Parse(desingerJson);
            var workflowDesignerData = JsonConvert.DeserializeObject<WorkflowDesignerData>(json.ToString());
            var getDataSetSuggestionsRequest = Create(queryObject, workflowDesignerData);
            return getDataSetSuggestionsRequest;
        }

        public static GetDataSetsItemsRequest Create(GetSuggestedDataSets queryObject, WorkflowDesignerData workflowDesignerData)
        {
            var getDataSetSuggestionsRequest = new GetDataSetsItemsRequest();
            var links = new List<WorkflowItemLink>();
            var items = new List<WorkflowItem>();
            foreach (var cell in workflowDesignerData.Cells)
            {
                if (cell.Type.ToLowerInvariant() == "link" || cell.Type.ToLowerInvariant() == "app.link" || cell.Type.ToLowerInvariant() == "standard.link")
                {
                    var workflowItemLink = new WorkflowItemLink
                    {
                        SourceId = cell.Source.Id,
                        TargetId = cell.Target.Id,
                        State = (NeuralEngine.Suggestion.WorkflowItemLinkState)(cell.State)
                    };
                    links.Add(workflowItemLink);
                }
                else
                {
                    var item = cell.CustomData;
                    switch (item?.Type)
                    {
                        case WorkflowType.Start:
                            var start = new WorkflowItem
                            {
                                ActionType = "None",
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                /*No need for DynamicData*/
                                Id = item.Id,
                                WorkflowId = item.WorkflowId,
                                SystemId = item.SystemId,
                                Title = item.Title,
                                Type = "Start",
                                TypicalId = item.TypicalId,
                                TypicalTitle = item.TypicalName,
                                TypicalType = "0"
                            };
                            items.Add(start);
                            break;

                        case WorkflowType.Action:
                        case WorkflowType.Condition:
                            var workflowItem = new WorkflowItem
                            {
                                ActionType = item.Type == WorkflowType.Condition ? "None" : ((ActionType)item.ActionType).ToString(),
                                CatalogId = item.CatalogId,
                                CatalogTitle = item.CatalogName,
                                Description = item.Description,
                                DynamicData = item.DynamicDatas == null ? new NeuralEngine.Suggestion.DynamicData[0] : item.DynamicDatas.Select(
                                    dynamicDatas => new NeuralEngine.Suggestion.DynamicData
                                    {
                                        FormulaText = "",
                                        SourceWorkflowItemId = dynamicDatas.SourceWorkflowItemId,
                                        SourceAttributeTitle = dynamicDatas.SourceAttributeName,
                                        TargetAttributeTitle = dynamicDatas.TargetAttributeName
                                    }).ToArray(),
                                Id = item.Id,
                                /*No need for SystemId*/
                                WorkflowId = item.WorkflowId,
                                Title = item.Title,
                                Type = item.Type.ToString(),
                                TypicalId = item.TypicalId,
                                TypicalTitle = item.TypicalName,
                                TypicalType = string.IsNullOrEmpty(item.TypicalType) ? "0" : ((TypicalType)int.Parse(item.TypicalType)).ToString()
                            };
                            items.Add(workflowItem);
                            break;

                        default:
                            break;
                    }
                }
            }
            getDataSetSuggestionsRequest.WorkflowItemLinks = links;
            getDataSetSuggestionsRequest.WorkflowItems = items;
            return getDataSetSuggestionsRequest;
        }
    }
}