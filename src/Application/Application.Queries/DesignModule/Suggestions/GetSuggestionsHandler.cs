﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using NeuralEngine;
using NeuralEngine.Suggestion;
using Persistence.CatalogModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule.Suggestions;
using System;
using System.Linq;
using QueriesDynamicData = Queries.DesignModule.Suggestions.DynamicData;
using System.Collections.Generic;

namespace Application.Queries.DesignModule.Suggestions
{
    public sealed class GetSuggestionsHandler : IQueryHandler<GetSuggestions, GetSuggestionsResult>
    {
        private readonly INeuralEngineClient _service;
        private readonly IDbContext _dbContext;

        public GetSuggestionsHandler(INeuralEngineClient service, IDbContext dbContext)
        {
            _service = service;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSuggestionsResult> Handle(GetSuggestions queryObject)
        {
            //queryObject.CatalogId = _dbContext.Set<CatalogData>().FirstOrDefault(ct => ct.SubProjectId == queryObject.SubProjectId && ct.Type == 1).Id;
            var request = WorkflowFactory.Create(queryObject);
            foreach (var lkWorkflow in request.WorkflowItems.Where(ct => ct.Type.ToLower() == "linked"))
            {
                string innerJson = _dbContext.Set<WorkflowData>().AsNoTracking().FirstOrDefault(wf => wf.Id == lkWorkflow.WorkflowId).DesignerJson;
                queryObject.Json = innerJson;
                var innerWorkflow = WorkflowFactory.Create(queryObject);
                request.AppendLinkWorkflow(lkWorkflow, innerWorkflow);
            }
            var catalogs = (from catalog in _dbContext.Set<CatalogData>()
                            where ((catalog.ProjectId == Guid.Empty && catalog.SubProjectId == Guid.Empty) ||
                        (catalog.ProjectId == queryObject.ProjectId && catalog.SubProjectId == Guid.Empty) ||
                        (catalog.ProjectId == queryObject.ProjectId && catalog.SubProjectId == queryObject.SubProjectId))
                            select new
                            {
                                catalog.Id,
                                catalog.Title
                            }).ToArray();
            GetSuggestionsResult result = new GetSuggestionsResult(new GetSuggestionsResultItem[0], new GetSuggestionsResultItem[0]);
            string message = "";
            SenseAI.Domain.MessageType messageType = SenseAI.Domain.MessageType.Failed;
            try
            {
                var respond = _service.GetSuggestions(request);

                if (ValidateRespond(respond, out message))
                {
                    result = new GetSuggestionsResult
                    (
                        respond.PreItems
                            .Select(item => new GetSuggestionsResultItem
                          (
                              new GetSuggestionResultWorkflowItem(
                                  item.WorkflowItem.TypicalTitle,
                                  item.WorkflowItem.Description,
                                  item.WorkflowItem.CatalogId,
                                  catalogs.FirstOrDefault(catalog => catalog.Id == item.WorkflowItem.CatalogId)?.Title,
                                  item.WorkflowItem.TypicalId,
                                  item.WorkflowItem.SystemId,
                                  item.WorkflowItem.TypicalTitle,
                                  (string.IsNullOrEmpty(item.WorkflowItem.TypicalType) || item.WorkflowItem.TypicalType == "0") ? "1" : ((int)Enum.Parse(typeof(TypicalType), item.WorkflowItem.TypicalType)).ToString(),//DOTO: after the activation for typical type
                                  item.WorkflowItem.ActionType != "None" ? (int)Enum.Parse(typeof(ActionType),
                                  item.WorkflowItem.ActionType) : -1,
                                  item.WorkflowItem.DynamicData.Select(it => new QueriesDynamicData(it.SourceWorkflowItemId, it.SourceAttributeTitle, it.TargetAttributeTitle, it.FormulaText)).ToArray(),
                                  (int)Enum.Parse(typeof(WorkflowType), item.WorkflowItem.Type)
                                  , item.WorkflowItem.IsNegativeStep,item.WorkflowItem.ExpectedError??""),
                                  new GetSuggestionResultWorkflowItemLink(item.WorkflowItemLink.SourceId,
                                  item.WorkflowItemLink.TargetId,
                                  (SenseAI.Domain.WorkflowModel.WorkflowItemLinkState)item.WorkflowItemLink.State)
                          )).ToArray(),
                        respond.PostItems.Select(item => new GetSuggestionsResultItem
                        (
                             new GetSuggestionResultWorkflowItem(
                                 item.WorkflowItem.TypicalTitle,
                                 item.WorkflowItem.Description,
                                 item.WorkflowItem.CatalogId,
                                 catalogs.FirstOrDefault(catalog => catalog.Id == item.WorkflowItem.CatalogId)?.Title,
                                 item.WorkflowItem.TypicalId,
                                 item.WorkflowItem.SystemId,
                                 item.WorkflowItem.TypicalTitle,
                                 (string.IsNullOrEmpty(item.WorkflowItem.TypicalType) || item.WorkflowItem.TypicalType == "0") ? "1" : ((int)Enum.Parse(typeof(TypicalType), item.WorkflowItem.TypicalType)).ToString(),//DOTO: after the activation for typical type
                                 item.WorkflowItem.ActionType != "None" ? (int)Enum.Parse(typeof(ActionType),
                                 item.WorkflowItem.ActionType) : -1,
                                 item.WorkflowItem.DynamicData.Select(it => new QueriesDynamicData
                                                                  (it.SourceWorkflowItemId, it.SourceAttributeTitle, it.TargetAttributeTitle, it.FormulaText)).ToArray(),
                                                                  (int)Enum.Parse(typeof(WorkflowType), item.WorkflowItem.Type), item.WorkflowItem.IsNegativeStep, item.WorkflowItem.ExpectedError??""),
                            new GetSuggestionResultWorkflowItemLink(item.WorkflowItemLink.SourceId
                            , item.WorkflowItemLink.TargetId, (SenseAI.Domain.WorkflowModel.WorkflowItemLinkState)((int)item.WorkflowItemLink.State))
                        )).ToArray()
                    );
                    messageType = SenseAI.Domain.MessageType.Successful;
                }
            }
            catch (Exception ex)
            {
                message = ex.InnerException?.Message;
            }
            return new QueryResponse<GetSuggestionsResult>(result, new QueryResponseMessage((int)messageType, message));
        }

        public bool Validate(GetSuggestions query)
        {
            if (query.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (query.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (query.CurrentItemId == Guid.Empty)
            {
                Errors.Add("CurrentItemId cannot be empty.");
                return false;
            }
            if (string.IsNullOrEmpty(query.Json))
            {
                Errors.Add("Json cannot be empty.");
                return false;
            }
            return true;
        }

        private bool ValidateRespond(GetSuggestionsResponse respond, out string message)
        {
            message = "";
            if (respond.PreItems == null && respond.PostItems == null)
            {
                message = "Error from ML, no pre or post items are suggested.";
                return false;
            }
            if (respond.PostItems != null && respond.PostItems.Any(ct => ct.WorkflowItem == null))
                respond.PostItems = new NeuralEngine.Suggestion.GetSuggestionsResponseItem[0];

            if (respond.PreItems != null && respond.PreItems.Any(ct => ct.WorkflowItem == null))
                respond.PreItems = new NeuralEngine.Suggestion.GetSuggestionsResponseItem[0];

            if (respond.PreItems.Count() == 0 && respond.PostItems.Count() == 0)
            {
                message = "Pre and post suggestions are empty from ML.";
                return false;
            }

            if (respond.PreItems.Any(ct => ct.WorkflowItem.TypicalId == Guid.Empty))
            {
                message = "One ore many pre suggestions have typical Id empty.";
                return false;
            }

            if (respond.PreItems.Any(ct => string.IsNullOrEmpty(ct.WorkflowItem.TypicalTitle)))
            {
                message = "One ore many pre suggestions have typical name empty.";
                return false;
            }

            if (respond.PreItems.Any(ct => ct.WorkflowItem.CatalogId == Guid.Empty))
            {
                message = "One ore many pre suggestions have catalog Id empty.";
                return false;
            }

            if (respond.PostItems.Any(ct => ct.WorkflowItem.TypicalId == Guid.Empty))
            {
                var postitem = respond.PostItems.Where(ct => ct.WorkflowItem.TypicalId == Guid.Empty).ToList();
                message = "One ore many post suggestions have typical Id empty.";
                return false;
            }

            if (respond.PostItems.Any(ct => string.IsNullOrEmpty(ct.WorkflowItem.TypicalTitle)))
            {
                message = "One ore many post suggestions have typical name empty.";
                return false;
            }

            if (respond.PostItems.Any(ct => ct.WorkflowItem.CatalogId == Guid.Empty))
            {
                message = "One ore many post suggestions have catalog Id empty.";
                return false;
            }

            return true;
        }
    }
}