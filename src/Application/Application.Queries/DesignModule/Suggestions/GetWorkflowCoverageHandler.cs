﻿using Messaging.Queries;
using NeuralEngine;
using Persistence.Internal;
using Queries.DesignModule.Suggestions;
using SenseAI.Domain;
using System;
using System.Collections.Generic;

namespace Application.Queries.DesignModule.Suggestions
{
    public sealed class GetWorkflowCoverageHandler : IQueryHandler<GetWorkflowCoverage, GetWorkflowCoverageResult>
    {
        private readonly INeuralEngineClient _service;
        private readonly IDbContext _dbContext;

        public GetWorkflowCoverageHandler(INeuralEngineClient service, IDbContext dbContext)
        {
            _service = service;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowCoverageResult> Handle(GetWorkflowCoverage queryObject)
        {
            var request = WorkflowFactory.Create(queryObject);
            var respond = _service.GetWorkflowCoverage(request);

            var result = new GetWorkflowCoverageResult
            (

                respond.WorkflowId,
                respond.WorkflowCoverage,
                respond.DataCoverage,
                respond.Status
            );

            return new QueryResponse<GetWorkflowCoverageResult>(result, result is GetWorkflowCoverageResult ? null : new QueryResponseMessage((int)MessageType.Information, "No WorkflowCoverage."));
        }

        public bool Validate(GetWorkflowCoverage query)
        {
            if (query.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (query.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}