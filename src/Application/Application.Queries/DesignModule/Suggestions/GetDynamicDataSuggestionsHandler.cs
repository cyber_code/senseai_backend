﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule.Suggestions;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule.Suggestions
{
    public class GetDynamicDataSuggestionsHandler : IQueryHandler<GetDynamicDataSuggestions, GetDynamicDataSuggestionsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetDynamicDataSuggestionsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetDynamicDataSuggestionsResult[]> Handle(GetDynamicDataSuggestions queryObject)
        {
            var query = from dynamicData in _dbContext.Set<DynamicDataSuggestionData>().AsNoTracking()
                        where
                            dynamicData.SourceTypicalName == queryObject.SourceTypicalName
                            && dynamicData.TargetTypicalName == queryObject.TargetTypicalName
                        select new
                        {
                            dynamicData.SourceTypicalName,
                            dynamicData.SourceTypicalAttributeName,
                            dynamicData.TargetTypicalName,
                            dynamicData.TargetTypicalAttributeName
                        };

            var result = query.Select(
                dynamicData => new GetDynamicDataSuggestionsResult(
                    dynamicData.SourceTypicalName,
                            dynamicData.SourceTypicalAttributeName,
                            dynamicData.TargetTypicalName,
                            dynamicData.TargetTypicalAttributeName
                )
            ).ToArray();

            return new QueryResponse<GetDynamicDataSuggestionsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Dynamic Data Suggestions found with SourceTypicalName: '{queryObject.SourceTypicalName}' and TargetTypicalName: '{queryObject.TargetTypicalName}'."));
        }

        public bool Validate(GetDynamicDataSuggestions query)
        {
            if (string.IsNullOrWhiteSpace(query.SourceTypicalName))
            {
                Errors.Add("Source typical name cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(query.TargetTypicalName))
            {
                Errors.Add("Target typical name cannot be empty.");
                return false;
            }

            return true;
        }
    }
}