﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Core.Configuration;
using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using NeuralEngine;
using NeuralEngine.Suggestion;
using Persistence.CatalogModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule.Suggestions;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule.Suggestions
{
    public sealed class IssueWorkflowToMLHandler : IQueryHandler<IssueWorkflowToML, bool>
    {
        private readonly IDbContext _dbContext;
        private readonly INeuralEngineClient _service;
        private readonly IWorkflowRepository _workflowRepository;
        private readonly SenseAIConfig _config;

        public IssueWorkflowToMLHandler(IDbContext dbContext, INeuralEngineClient service, IWorkflowRepository workflowRepository, SenseAIConfig config)
        {
            _service = service;
            _dbContext = dbContext;
            _workflowRepository = workflowRepository;
            _config = config;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<bool> Handle(IssueWorkflowToML queryObject)
        {
            var workflow = (from wversion in _dbContext.Set<WorkflowVersionData>()
                            where wversion.WorkflowId == queryObject.WorkflowId && wversion.IsLast == true
                            select new
                            {
                                wversion.Title,
                                wversion.DesignerJson,
                                wversion.WorkflowPathsJson,
                                wversion.VersionId,
                                wversion.IsLast
                            }).FirstOrDefault();

            if (workflow != null)
            {
                var json = JObject.Parse(workflow.WorkflowPathsJson);
                // queryObject.CatalogId = _dbContext.Set<CatalogData>().FirstOrDefault(ct => ct.SubProjectId == queryObject.SubProjectId && ct.Type == 1).Id;  //DAMTODO to get this from workcontext
                WorkflowPathDesinger workflowPaths = JsonConvert.DeserializeObject<WorkflowPathDesinger>(json.ToString());
                var respond = false;
                var isErrorHappened = workflowPaths.WorkflowPaths.Count() == 0;

                var request = WorkflowFactory.Create(queryObject, workflow.DesignerJson, workflow.VersionId);
                foreach (var lkWorkflow in request.WorkflowItems.Where(ct => ct.Type.ToLower() == "linked"))
                {
                    string innerJson = _dbContext.Set<WorkflowData>().AsNoTracking().FirstOrDefault(wf => wf.Id == lkWorkflow.WorkflowId).DesignerJson;

                    var innerWorkflow = WorkflowFactory.Create(queryObject, innerJson, workflow.VersionId);
                    request.AppendLinkWorkflow(lkWorkflow, innerWorkflow);
                }
                foreach (var path in workflowPaths.WorkflowPaths)
                {
                    IssueWorkflowRequest issueWorkflowRequest = new IssueWorkflowRequest
                    {
                        TenantId = queryObject.TenantId,
                        ProjectId = queryObject.ProjectId,
                        SubProjectId = queryObject.SubProjectId,
                        // DAMTODO SystemId = queryObject.SystemId,
                        CatalogId = queryObject.CatalogId,
                        VersionId = workflow.VersionId,
                        Title = workflow.Title,
                        Coverage = path.Coverage.ToString(),
                        WorkflowId = queryObject.WorkflowId
                    };
                    issueWorkflowRequest.WorkflowItems = request.WorkflowItems.Where(ct => path.ItemIds.Contains(ct.Id)).ToArray();
                    issueWorkflowRequest.WorkflowItemLinks = request.WorkflowItemLinks.Where(ct => path.ItemIds.Contains(ct.SourceId)).ToArray();

                    respond = _service.PostIssueWorkflow(issueWorkflowRequest);
                    if (!respond)
                        isErrorHappened = true;
                }

                if (!isErrorHappened)
                {
                    _workflowRepository.Update(queryObject.WorkflowId, workflow.VersionId);
                }

                return new QueryResponse<bool>(respond, respond ? null : new QueryResponseMessage((int)MessageType.Information, "Couldn't issue the Workflow."));
            }

            return new QueryResponse<bool>(false, new QueryResponseMessage((int)MessageType.Information, "Couldn't issue the Workflow."));
        }

        public bool Validate(IssueWorkflowToML query)
        {
            if (query.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (query.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.SystemId == Guid.Empty)//TODO: is not used in query
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}