﻿using System;

namespace Application.Queries.DesignModule.Suggestions
{
    public enum WorkflowType
    {
        Start,
        Action,
        Condition,
        Linked,
        Manual
    }

    public enum WorkflowItemLinkState
    {
        None,
        Yes,
        No
    }

    public sealed class WorkflowDesignerData
    {
        public Cell[] Cells { get; set; }
    }

    public sealed class Cell
    {
        public WorkflowDesignerCellItem CustomData { get; set; }

        public WorkflowDesignerCellLink Source { get; set; }

        public WorkflowDesignerCellLink Target { get; set; }

        public string Type { get; set; }

        public WorkflowItemLinkState State { get; set; }
    }

    public sealed class WorkflowDesignerCellItem
    {
        public WorkflowType Type { get; set; }

        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public string CatalogName { get; set; }

        public string TypicalName { get; set; }

        public string TypicalType { get; set; }

        public Guid TypicalId { get; set; }

        public int ActionType { get; set; }

        public string Parameters { get; set; }

        public DynamicData[] DynamicDatas { get; set; }

        public Guid WorkflowId { get; set; }
    }

    public sealed class WorkflowDesignerCellItemDynamicDatas
    {
        public Guid SourceWorkflowItemId { get; set; }

        public string SourceAttributeName { get; set; }

        public string TargetAttributeName { get; set; }

        public string PlainTextFormula { get; set; }

        public string Path { get; set; }

        public short? UiMode { get; set; }
    }

    public sealed class WorkflowDesignerCellLink
    {
        public Guid Id { get; set; }
    }

    public sealed class WorkflowDesignerCellItemConstraint
    {
        public string AttributeName { get; set; }

        public string Operator { get; set; }

        public string AttributValue { get; set; }
    }

    public sealed class WorkflowDesignerCellItemAutomaticCalculations
    {
        public Guid TestStepId { get; set; }

        public string AttributeName { get; set; }

        public byte[] PlainTextFormula { get; set; }

        public short? UiMode { get; set; }
    }
}