﻿using Microsoft.EntityFrameworkCore;
using Core.Configuration;
using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using NeuralEngine;
using Persistence.CatalogModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule.Suggestions;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule.Suggestions
{
    public sealed class GetPathsHandler : IQueryHandler<GetPaths, GetPathsResult>
    {
        private readonly INeuralEngineClient _service;
        private readonly IGeneratePaths _generatePath;
        private readonly SenseAIConfig _configuration;
        private readonly IDbContext _dbContext;
        private readonly IWorkflowRepository _workflowRepository;

        public GetPathsHandler(INeuralEngineClient service, IGeneratePaths generatePath, IWorkflowRepository workflowRepository, SenseAIConfig configuration, IDbContext dbContext)
        {
            _service = service;
            _generatePath = generatePath;
            _configuration = configuration;
            _dbContext = dbContext;
            _workflowRepository = workflowRepository;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetPathsResult> Handle(GetPaths queryObject)
        {
            Workflow workflow = SenseAI.Domain.WorkflowModel.WorkflowFactory.Create(Guid.NewGuid(), queryObject.Json); 
            foreach (LinkedWorkflowItem lkWorkflow in workflow.Items.Where(ct => ct is LinkedWorkflowItem))
            {
                try
                {
                    var innerWorkflow = _workflowRepository.GetWorkflowByVersion(lkWorkflow.WorkflowId, lkWorkflow.WorkflowVersionId); 
                    lkWorkflow.SetTitle(lkWorkflow.Title + ": " + innerWorkflow.Title); 

                    innerWorkflow.SetParentIds(lkWorkflow.Id);
                    innerWorkflow.SetUniqueIds(lkWorkflow.Id.ToString());
                    lkWorkflow.SetUniqueId(Guid.Empty.ToString());
                    AppendRecursivelyLinkWorkflow(innerWorkflow, lkWorkflow.Id.ToString());

                    workflow.AppendLinkWorkflow(lkWorkflow, innerWorkflow, lkWorkflow.Id.ToString());
                    workflow.AddItems(innerWorkflow.Items.ToList());
                    workflow.AddLinks(innerWorkflow.ItemLinks.ToList());
                }
                catch (Exception ex)
                {
                    var workflowTitle = _dbContext.Set<WorkflowData>().Where(u => u.Id == lkWorkflow.WorkflowId).Select(u => u.Title).FirstOrDefault();
                    int messageType = (int)MessageType.Failed;
                    string message = $"There is linked workflow with title \" {workflowTitle} \" that is not issued!";
                    return new QueryResponse<GetPathsResult>(null, new QueryResponseMessage(messageType, message));

                }
            }

            var saiPaths = _generatePath.Generate(workflow);
            var result = new GetPathsResult(
                saiPaths.Select(
                    item => new GetPathsResultWorkflowPath(item.Title, item.Number, item.Coverage == 0 ? "N/A" : item.Coverage.ToString() + "%",
                    item.IsBestPath, item.ItemIds)).ToArray()
            );

            if (_configuration.EnableNeuralEngine && result.WorkflowPath.Count() > 1)
            {
                //queryObject.CatalogId = _dbContext.Set<CatalogData>().FirstOrDefault(ct => ct.SubProjectId == queryObject.SubProjectId && ct.Type == 1).Id;
                var request = WorkflowFactory.Create(queryObject);
                var mlPaths = _service.GetPaths(request);
                int index = 0;
                if (mlPaths.WorkflowPaths != null && mlPaths.WorkflowPaths.Count() > 0)
                {
                    result = new GetPathsResult(
                   mlPaths.WorkflowPaths.Select(
                       item => new GetPathsResultWorkflowPath("Path " + (++index).ToString(), index, item.Coverage == 0 ? "N/A" : item.Coverage.ToString("F2") + "%", item.IsBestPath, item.ItemIds)).ToArray());
                }
            }
            return new QueryResponse<GetPathsResult>(result, result is GetPathsResult ? null : new QueryResponseMessage((int)MessageType.Information, "No Paths found."));
        }

        public bool Validate(GetPaths query)
        {
            if (query.TenantId == Guid.Empty)//TODO: is not used in query
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (query.ProjectId == Guid.Empty)//TODO: is not used in query
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.SystemId == Guid.Empty)//TODO: is not used in query
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (string.IsNullOrEmpty(query.Json))
            {
                Errors.Add("Json cannot be empty.");
                return false;
            }
            return true;
        }

        private void AppendRecursivelyLinkWorkflow(Workflow workflow, string uniqueId)
        {
            if (workflow.Items.Where(ct => ct is LinkedWorkflowItem).Count() == 0)
                return;
            foreach (LinkedWorkflowItem lkWorkflow in workflow.Items.Where(ct => ct is LinkedWorkflowItem))
            {
                var innerWorkflow = _workflowRepository.GetWorkflowByVersion(lkWorkflow.WorkflowId, lkWorkflow.WorkflowVersionId);
                lkWorkflow.SetTitle(lkWorkflow.Title + ": " + innerWorkflow.Title);

                string innweUniqueId = uniqueId + "" + lkWorkflow.Id.ToString();
                innerWorkflow.SetParentIds(lkWorkflow.Id);
                innerWorkflow.SetUniqueIds(innweUniqueId);

                workflow.AppendLinkWorkflow(lkWorkflow, innerWorkflow, innweUniqueId);
                AppendRecursivelyLinkWorkflow(innerWorkflow, innweUniqueId);

                workflow.AddItems(innerWorkflow.Items.ToList());
                workflow.AddLinks(innerWorkflow.ItemLinks.ToList());
            }
        }
    }
}