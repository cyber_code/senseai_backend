﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule
{
    public class GetWorkflowVersionHandler : IQueryHandler<GetWorkflowVersion, GetWorkflowVersionResult>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowVersionHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowVersionResult> Handle(GetWorkflowVersion queryObject)
        {
            var query = from workflowVersion in _dbContext.Set<WorkflowVersionData>().AsNoTracking()
                        where workflowVersion.WorkflowId == queryObject.WorkflowId
                        && workflowVersion.VersionId == queryObject.Version
                        select new GetWorkflowVersionResult(workflowVersion.WorkflowId,
                            workflowVersion.VersionId,
                            workflowVersion.Title,
                            workflowVersion.Description,
                            workflowVersion.IsLast,
                            workflowVersion.DesignerJson,
                            workflowVersion.WorkflowPathsJson);

            return new QueryResponse<GetWorkflowVersionResult>(query.FirstOrDefault(), query.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No workflow version found."));
        }

        public bool Validate(GetWorkflowVersion query)
        {
            //TODO: validate queryObject.Version
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}