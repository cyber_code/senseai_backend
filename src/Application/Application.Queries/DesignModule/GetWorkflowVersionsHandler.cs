﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule
{
    public class GetWorkflowVersionsHandler : IQueryHandler<GetWorkflowVersions, GetWorkflowVersionsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowVersionsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowVersionsResult[]> Handle(GetWorkflowVersions queryObject)
        {
            var result = _dbContext.Set<WorkflowVersionData>().AsNoTracking().
                    Where(ct => ct.WorkflowId == queryObject.WorkflowId).Select(workflowVersion => new GetWorkflowVersionsResult(
                       workflowVersion.WorkflowId,
                               workflowVersion.VersionId,
                               workflowVersion.Title)).ToArray();

            return new QueryResponse<GetWorkflowVersionsResult[]>(result, result.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No workflow versions found."));
        }

        public bool Validate(GetWorkflowVersions query)
        {
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}