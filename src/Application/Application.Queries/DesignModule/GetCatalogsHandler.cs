﻿using Messaging.Queries;
using Persistence.CatalogModel;
using Persistence.Internal;
using Persistence.SystemCatalogModel;
using Queries.DesignModule;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule
{
    public sealed class GetCatalogsHandler : IQueryHandler<GetCatalogs, GetCatalogsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetCatalogsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetCatalogsResult[]> Handle(GetCatalogs queryObject)
        {
            var result = from catalog in _dbContext.Set<CatalogData>()
                         where catalog.ProjectId == queryObject.ProjectId
                            && catalog.SubProjectId == queryObject.SubProjectId
                         select new GetCatalogsResult(
                             catalog.Id,
                             catalog.ProjectId,
                             catalog.SubProjectId,
                             catalog.Title,
                             catalog.Description,
                             catalog.Type
                             );

            return new QueryResponse<GetCatalogsResult[]>(result.ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No catalogs found."));
        }

        public bool Validate(GetCatalogs query)
        {
            return true;
        }
    }
}