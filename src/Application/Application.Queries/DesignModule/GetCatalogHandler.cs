﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.CatalogModel;
using Persistence.Internal;
using Queries.DesignModule;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule
{
    public class GetCatalogHandler : IQueryHandler<GetCatalog, GetCatalogResult>
    {
        private readonly IDbContext _dbContext;

        public GetCatalogHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetCatalogResult> Handle(GetCatalog queryObject)
        {
            var query = from catalog in _dbContext.Set<CatalogData>().AsNoTracking()
                        where catalog.Id == queryObject.Id
                        select new GetCatalogResult(catalog.Id, catalog.ProjectId, catalog.SubProjectId, catalog.Title, catalog.Description, catalog.Type);

            return new QueryResponse<GetCatalogResult>(query.FirstOrDefault(), query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No catalog found."));
        }

        public bool Validate(GetCatalog query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}