﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule
{
    public class GetWorkflowHandler : IQueryHandler<GetWorkflow, GetWorkflowResult>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowResult> Handle(GetWorkflow queryObject)
        {
            var linkedWorkflows = from workflowItem in _dbContext.Set<WorkflowItemData>().AsNoTracking()
                                  join workflow in _dbContext.Set<WorkflowData>().AsNoTracking()
                                  on workflowItem.WorkflowId equals workflow.Id
                                  where workflowItem.WorkflowLinkId == queryObject.Id
                                  select new LinkedWorkflow(workflowItem.WorkflowId, workflow.Title);
            int versionId = 0;

            var lastVersion = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == queryObject.Id && w.IsLast);
            if (lastVersion != null)
                versionId = lastVersion.VersionId;

            var query = from workflow in _dbContext.Set<WorkflowData>().AsNoTracking()
                        where workflow.Id == queryObject.Id
                        select new GetWorkflowResult(workflow.Id, workflow.Title, workflow.Description, workflow.FolderId, workflow.DesignerJson,
                        workflow.WorkflowPathsJson, linkedWorkflows.Distinct().ToArray(), workflow.IsParsed, versionId);

            return new QueryResponse<GetWorkflowResult>(query.FirstOrDefault(), query.Any() ? null :
                new QueryResponseMessage((int)MessageType.Information, $"No workflow found."));
        }

        public bool Validate(GetWorkflow query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}