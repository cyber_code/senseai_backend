﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule
{
    public sealed class GetWorkflowsHandler : IQueryHandler<GetWorkflows, GetWorkflowsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowsResult[]> Handle(GetWorkflows queryObject)
        {
            var result = from workflow in _dbContext.Set<WorkflowData>().AsNoTracking()
                        join folder in _dbContext.Set<FolderData>().AsNoTracking() on workflow.FolderId equals folder.Id
                        orderby workflow.Title
                        where
                            workflow.FolderId == queryObject.FolderId
                        select new GetWorkflowsResult(
                                                    workflow.Id,
                            workflow.Title,
                            workflow.Description,
                            workflow.FolderId,
                            workflow.Status,
                            workflow.isIssued,
                            workflow.generatedTestCases
                        );

            return new QueryResponse<GetWorkflowsResult[]>(result.ToArray(), result.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No workflows found in folder."));
        }

        public bool Validate(GetWorkflows query)
        {
            if (query.FolderId == Guid.Empty)
            {
                Errors.Add("FolderId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}