﻿using System.Collections.Generic;
using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DesignModule;
using System.Linq;
using SenseAI.Domain;
using System;

namespace Application.Queries.DesignModule
{
    public class GetEnquiryActionsHandler : IQueryHandler<GetEnquiryActions, GetEnquiryActionsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetEnquiryActionsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetEnquiryActionsResult[]> Handle(GetEnquiryActions queryObject)
        {
            List<GetEnquiryActionsResult> result = new List<GetEnquiryActionsResult>();

            var query = (from enquiryaction in _dbContext.Set<EnquiryActionData>()
                         where (enquiryaction.TypicalName ?? "").ToLowerInvariant() == queryObject.TypicalName.ToLowerInvariant()
                               && enquiryaction.CatalogId == queryObject.CatalogId
                         select new
                         {
                             enquiryaction.Actions
                         }).FirstOrDefault();


            if (query == null)
                return new QueryResponse<GetEnquiryActionsResult[]>(null, new QueryResponseMessage((int)MessageType.Information, $"No Enquiry Actions found with TypicalName: '{queryObject.TypicalName}' and CatalogId: '{queryObject.CatalogId}'."));

            string actions = query.Actions;

            if (queryObject.TypicalName.ToLower().StartsWith("v"))
            {
                var lstResult = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(actions);
                lstResult.ForEach(f => {
                    result.Add(new GetEnquiryActionsResult { Action = f, Type = "label" });
                });
            }
            else
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GetEnquiryActionsResult>>(actions);

            return new QueryResponse<GetEnquiryActionsResult[]>(result.ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Enquiry Actions found with TypicalName: '{queryObject.TypicalName}' and CatalogId: '{queryObject.CatalogId}'."));
        }

        public bool Validate(GetEnquiryActions query)
        {
            if (string.IsNullOrWhiteSpace(query.TypicalName))
            {
                Errors.Add("TypicalName cannot be empty.");
                return false;
            }
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}