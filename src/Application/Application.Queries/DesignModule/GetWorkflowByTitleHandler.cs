﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule
{
    public class GetWorkflowByTitleHandler : IQueryHandler<GetWorkflowByTitle, GetWorkflowResult>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowByTitleHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowResult> Handle(GetWorkflowByTitle queryObject)
        {
            var query = from workflow in _dbContext.Set<WorkflowData>()
                        where workflow.Title == queryObject.Title
                        select new GetWorkflowResult(workflow.Id, workflow.Title, workflow.Description, workflow.FolderId, workflow.DesignerJson, workflow.WorkflowPathsJson, null, workflow.IsParsed, 0);

            return new QueryResponse<GetWorkflowResult>(query.FirstOrDefault(), query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Workflow found with Title: '{queryObject.Title}'."));
        }

        public bool Validate(GetWorkflowByTitle query)
        {
            //TODO: Validata only query.Title not others???

            if (query.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (query.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (string.IsNullOrEmpty(query.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}