﻿using Microsoft.EntityFrameworkCore;
using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.DesignModule
{
    public sealed class GetPreviousWorkflowItemsHandler : IQueryHandler<GetPreviousWorkflowItems, GetPreviousWorkflowItemsResult[]>
    {
        private readonly IDbContext _dbContext;
        private readonly IWorkflowRepository _workflowRepository;

        public GetPreviousWorkflowItemsHandler(IDbContext dbContext, IWorkflowRepository workflowRepository)
        {
            _dbContext = dbContext;
            _workflowRepository = workflowRepository;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetPreviousWorkflowItemsResult[]> Handle(GetPreviousWorkflowItems queryObject)
        {
            var workflow = WorkflowFactory.Create(Guid.NewGuid(), queryObject.DesignerJson);
            List<GetPreviousWorkflowItemsResult> result = new List<GetPreviousWorkflowItemsResult>();
            foreach (LinkedWorkflowItem lkWorkflow in workflow.Items.Where(ct => ct is LinkedWorkflowItem))
            {
                var innerWorkflow = _workflowRepository.GetWorkflowByVersion(lkWorkflow.WorkflowId, lkWorkflow.WorkflowVersionId);
                lkWorkflow.SetTitle(lkWorkflow.Title + ": " + innerWorkflow.Title);
                 

                innerWorkflow.SetParentIds(lkWorkflow.Id);
                innerWorkflow.SetUniqueIds(lkWorkflow.Id.ToString());
                lkWorkflow.SetUniqueId(Guid.Empty.ToString());
                AppendRecursivelyLinkWorkflow(innerWorkflow, lkWorkflow.Id.ToString());

                workflow.AppendLinkWorkflow(lkWorkflow, innerWorkflow, lkWorkflow.Id.ToString());
                workflow.AddItems(innerWorkflow.Items.ToList());
                workflow.AddLinks(innerWorkflow.ItemLinks.ToList());
            }

            if (queryObject.ItemIds == null || queryObject.ItemIds.Count() == 0)
            {
                var current = workflow.ItemLinks.FirstOrDefault(ct => ct.ToId == queryObject.CurrentId);
                WorkflowItemLink previous = null;

                while (current != null && (previous = workflow.ItemLinks.FirstOrDefault(ct => ct.FromId == current.FromId)) != null)
                {
                    var item = workflow.Items.FirstOrDefault(ct => ct.Id == previous.FromId);
                    if (item is ActionWorkflowItem)
                        result.Add(new GetPreviousWorkflowItemsResult(item.Id, item.Title, item.TypicalName, item.TypicalId));
                    current = workflow.ItemLinks.FirstOrDefault(ct => ct.ToId == previous.FromId);
                }
            }
            else
            {
                foreach (var itemId in queryObject.ItemIds)
                {
                    var item = workflow.Items.FirstOrDefault(ct => ct.Id == itemId);

                    if ((queryObject.ParenId != Guid.Empty) && (item.ParenID != Guid.Empty))
                    {
                        var items = workflow.Items.Where(ct => ct.Id == itemId);
                        if (items.Count() > 1)
                        {
                            int index = result.Count(ct => ct.Id == item.Id);
                            item = items.ElementAtOrDefault(index);
                        }

                        if ((itemId == queryObject.CurrentId) && item.ParenID == queryObject.ParenId)
                            break;
                    }
                    else if (itemId == queryObject.CurrentId)
                        break;
                    if (item is ActionWorkflowItem)
                        result.Add(new GetPreviousWorkflowItemsResult(item.Id, item.Title, item.TypicalName, item.TypicalId));
                }
            }
            return new QueryResponse<GetPreviousWorkflowItemsResult[]>(result.ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Previous Workflow Items found with CurrentId: '{queryObject.CurrentId}'."));
        }

        public bool Validate(GetPreviousWorkflowItems query)
        {
            if (query.CurrentId == Guid.Empty)
            {
                Errors.Add("CurrentId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(query.DesignerJson))
            {
                Errors.Add("DesignerJson cannot be empty.");
                return false;
            }
            return true;
        }

        private void AppendRecursivelyLinkWorkflow(Workflow workflow, string uniqueId)
        {
            if (workflow.Items.Where(ct => ct is LinkedWorkflowItem).Count() == 0)
                return;
            foreach (LinkedWorkflowItem lkWorkflow in workflow.Items.Where(ct => ct is LinkedWorkflowItem))
            {
                var innerWorkflow = _workflowRepository.GetWorkflowByVersion(lkWorkflow.WorkflowId, lkWorkflow.WorkflowVersionId);
                lkWorkflow.SetTitle(lkWorkflow.Title + ": " + innerWorkflow.Title);

                string innweUniqueId = uniqueId + "" + lkWorkflow.Id.ToString();
                innerWorkflow.SetParentIds(lkWorkflow.Id);
                innerWorkflow.SetUniqueIds(innweUniqueId);

                workflow.AppendLinkWorkflow(lkWorkflow, innerWorkflow, innweUniqueId);
                AppendRecursivelyLinkWorkflow(innerWorkflow, innweUniqueId);

                workflow.AddItems(innerWorkflow.Items.ToList());
                workflow.AddLinks(innerWorkflow.ItemLinks.ToList());
            }
        }
    }
}