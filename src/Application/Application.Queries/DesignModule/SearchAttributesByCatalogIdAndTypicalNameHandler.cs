﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SenseAI.Domain;
using Messaging.Queries;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DesignModule;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule
{
    public sealed class SearchAttributesByCatalogIdAndTypicalNameHandler : IQueryHandler<SearchAttributesByCatalogIdAndTypicalName, AttributesResult[]>
    {
        private readonly IDbContext _dbContext;

        public SearchAttributesByCatalogIdAndTypicalNameHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<AttributesResult[]> Handle(SearchAttributesByCatalogIdAndTypicalName queryObject)
        {
            var type = _dbContext.Set<CatalogData>().AsNoTracking().FirstOrDefault(ct => ct.Id == queryObject.CatalogId)?.Type;

            List<int> list = new List<int>();
            if (type.HasValue)
                list.Add(type.Value);

            if (type == (int)TypicalType.Application)
                list.Add((int)TypicalType.Version);

            var query = (from typical in _dbContext.Set<TypicalData>().AsNoTracking()
                         where list.Contains((int)typical.Type)
                            && typical.Title.Equals(queryObject.TypicalName)
                         select new
                         {
                             typical.Id,
                             typical.Title,
                             typical.Json
                         }).FirstOrDefault();

            if (query == null)
                return new QueryResponse<AttributesResult[]>(null,new QueryResponseMessage((int)MessageType.Information, $"No Attributes found with CatalogId: '{queryObject.CatalogId}' and TypicalName: '{queryObject.TypicalName}'."));
            
            var jsonObject = JsonConvert.DeserializeObject<TypicalItem>(query.Json.ToString());
            var result = jsonObject.Attributes.OrderBy(o => o.Name).Where(ct => string.IsNullOrEmpty(queryObject.SearchString) || ct.Name.ToLower().StartsWith(queryObject.SearchString.ToLower()));

            return new QueryResponse<AttributesResult[]>(result.Distinct().ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Attributes found with CatalogId: '{queryObject.CatalogId}' and TypicalName: '{queryObject.TypicalName}'."));
        }

        public bool Validate(SearchAttributesByCatalogIdAndTypicalName query)
        {
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(query.TypicalName))
            {
                Errors.Add("TypicalName cannot be empty.");
                return false;
            }
            return true;
        }
    }
}