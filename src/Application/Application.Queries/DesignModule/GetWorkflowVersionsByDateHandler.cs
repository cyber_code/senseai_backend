﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DesignModule;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DesignModule
{
    public class GetWorkflowVersionsByDateHandler : IQueryHandler<GetWorkflowVersionsByDate, GetWorkflowVersionsByDateResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowVersionsByDateHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowVersionsByDateResult[]> Handle(GetWorkflowVersionsByDate queryObject)
        {
            var query = from workflowVersion in _dbContext.Set<WorkflowVersionData>().AsNoTracking()
                        where
                            workflowVersion.WorkflowId == queryObject.WorkflowId
                            &&
                            (workflowVersion.DateCreated >= queryObject.DateFrom && workflowVersion.DateCreated <= queryObject.DateTo)
                        select new
                        {
                            workflowVersion.WorkflowId,
                            workflowVersion.VersionId,
                            workflowVersion.Title,
                            workflowVersion.Description,
                            workflowVersion.IsLast,
                            workflowVersion.WorkflowImage,
                            workflowVersion.DateCreated
                        };

            var result = query.Select(
                workflowVersion => new GetWorkflowVersionsByDateResult(
                    workflowVersion.WorkflowId,
                            workflowVersion.VersionId,
                            workflowVersion.Title,
                            workflowVersion.Description,
                            workflowVersion.IsLast,
                            workflowVersion.WorkflowImage,
                            workflowVersion.DateCreated
                )
            ).ToArray();

            return new QueryResponse<GetWorkflowVersionsByDateResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Workflow Versions found with WorkflowId: '{queryObject.WorkflowId}' and DateCreated between '{queryObject.DateFrom}' and '{queryObject.DateTo}'."));
        }

        public bool Validate(GetWorkflowVersionsByDate query)
        {//TODO: validate queryObject.DateFrom and queryObject.DateTo
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}