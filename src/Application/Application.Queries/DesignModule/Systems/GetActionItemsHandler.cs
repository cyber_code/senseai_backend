﻿using SenseAI.Domain;
using SenseAI.Domain.Adapters;
using Messaging.Queries;
using Persistence.Internal;
using Queries.DesignModule.Systems;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Application.Queries.DesignModule.Systems
{
    public sealed class GetActionItemsHandler : IQueryHandler<GetActionItems, GetActionItemsResult>
    {
        private readonly IDbContext _dbContext;

        public GetActionItemsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetActionItemsResult> Handle(GetActionItems queryObject)
        {
            GetActionItemsResult res = null;
            if (!Enum.IsDefined(typeof(AdapterType), queryObject.AdapterName))
            {
                throw new Exception("Adapter is not implemented.");
            }
            AdapterType adapterType = (AdapterType)Enum.Parse(typeof(AdapterType), queryObject.AdapterName);
            switch (adapterType)
            {
                case AdapterType.T24WebAdapter:
                    res = new GetActionItemsResult(queryObject.AdapterName, Enum.GetValues(typeof(T24AdapterSettings.ActionTypes))
                                       .Cast<T24AdapterSettings.ActionTypes>()
                                       .Select(v => v.ToString())
                                       .ToList());

                    break;
            }
            return new QueryResponse<GetActionItemsResult>(res, res == null ? null : new QueryResponseMessage((int)MessageType.Information, "No action items found."));

        }

        public bool Validate(GetActionItems query)
        {
            if (string.IsNullOrWhiteSpace(query.AdapterName))
            {
                Errors.Add("AdapterName cannot be empty.");
                return false;
            }
            return true;
        }
    }
}