﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.SystemModel;
using Queries.DesignModule.System;
using SenseAI.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DesignModule.Systems
{
    public sealed class GetSystemsHandler : IQueryHandler<GetSystems, GetSystemsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetSystemsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSystemsResult[]> Handle(GetSystems queryObject)
        {
            var query = from system in _dbContext.Set<SystemData>()
                        select new
                        {
                            system.Id,
                            system.Title,
                            system.AdapterName
                        };

            var result = query.Select(system => new GetSystemsResult(system.Id, system.Title, system.AdapterName)).ToArray();

            return new QueryResponse<GetSystemsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No Systems found."));
        }

        public bool Validate(GetSystems query)
        {
            return true;
        }
    }
}