﻿using SenseAI.Domain;
using SenseAI.Domain.Adapters;
using Messaging.Queries;
using Persistence.Internal;
using Queries.DesignModule.Systems;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Application.Queries.DesignModule.Systems
{
    public sealed class GetConditionItemsHandler : IQueryHandler<GetConditionItems, GetConditionItemsResult>
    {
        private readonly IDbContext _dbContext;

        public GetConditionItemsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetConditionItemsResult> Handle(GetConditionItems queryObject)
        {
            GetConditionItemsResult res = null;
            if(!Enum.IsDefined(typeof(AdapterType), queryObject.AdapterName))
            {
                throw new Exception("Adapter is not implemented.");
            }
            AdapterType adapterType = (AdapterType)Enum.Parse(typeof(AdapterType), queryObject.AdapterName);
            switch (adapterType)
            {
                case AdapterType.T24WebAdapter:
                    res = new GetConditionItemsResult(queryObject.AdapterName,
                  Enum.GetValues(typeof(T24AdapterSettings.Operators))
                                        .Cast<T24AdapterSettings.Operators>()
                                        .Select(v => v.ToString())
                                        .ToList());
                    break;
            }
            return new QueryResponse<GetConditionItemsResult>(res, res == null ? null : new QueryResponseMessage((int)MessageType.Information, "No condition items found."));

        }

        public bool Validate(GetConditionItems query)
        {
            if (string.IsNullOrWhiteSpace(query.AdapterName))
            {
                Errors.Add("AdapterName cannot be empty.");
                return false;
            }
            return true;
        }
    }
}