﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Persistence.AAProductBuilderModel;
using Persistence.Internal;
using Queries.AAProductBuilderModule;
using SenseAI.Domain.AAProductionBuilderModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.AAProductBuilderModule
{
    public sealed class GetPropertySelectedFieldValuesHandler : IQueryHandler<GetPropertySelectedFieldValues, GetPropertySelectedFieldValuesConfig>
    {
        private const string NEW_ARRANGEMENT = "NEWARRANGEMENT";

        private readonly IDbContext _dbContext;
        private readonly IProductGroupPropertyFieldStateRepository _productGroupPropertyFieldStateRepository;

        public GetPropertySelectedFieldValuesHandler(IDbContext dbContext,
                                                     IProductGroupPropertyFieldStateRepository productGroupPropertyFieldStateRepository)
        {
            _dbContext = dbContext;
            _productGroupPropertyFieldStateRepository = productGroupPropertyFieldStateRepository;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetPropertySelectedFieldValuesConfig> Handle(GetPropertySelectedFieldValues queryObject)
        {
            var getPropertySelectedFieldValuesConfig = new GetPropertySelectedFieldValuesConfig();

            //header configurations and values
            var headerFieldConfigs = (from fields in _dbContext.Set<FieldData>().AsNoTracking()
                                      where fields.PropertyId == NEW_ARRANGEMENT
                                      select new
                                      {
                                          fields.Id,
                                          fields.FieldId,
                                          fields.Json
                                      }).OrderBy(x => x.Id);

            var headerFieldValues = _dbContext.Set<ProductGroupPropertyFieldStateData>().AsNoTracking()
                                              .Where(w => w.SessionId == queryObject.SessionId
                                                          && w.ProductGroupId == queryObject.ProductGroupId
                                                          && w.PropertyId == NEW_ARRANGEMENT)
                                              .Select(x => new
                                              {
                                                  x.FieldId,
                                                  x.MultiValueIndex,
                                                  x.Value
                                              });

            var headerAttributes = new List<GetPropertySelectedFieldValuesHeaderFieldAttribute>();

            foreach (var headerFieldConfig in headerFieldConfigs)
            {
                var attribute = JsonConvert.DeserializeObject<GetPropertySelectedFieldValuesHeaderFieldAttribute>(headerFieldConfig.Json);
                var fieldValues = headerFieldValues.Where(x => x.FieldId == headerFieldConfig.FieldId).ToList();
                if (fieldValues.Any())
                {
                    foreach (var fieldValue in fieldValues)
                    {
                        var attr = (GetPropertySelectedFieldValuesHeaderFieldAttribute)attribute.Clone();
                        if (attr.IsMultiValue)
                        {
                            attr.Description = AddIndexes(attr.Description, fieldValue.MultiValueIndex, 1);
                        }
                        attr.Value = fieldValue.Value;

                        headerAttributes.Add(attr);
                    }
                }
                else
                {
                    if (attribute.IsMultiValue)
                    {
                        attribute.Description = AddIndexes(attribute.Description, 1, 1);
                    }

                    headerAttributes.Add(attribute);
                }
            }

            getPropertySelectedFieldValuesConfig.HeaderFieldValueConfigs = headerAttributes;

            //PropertyFieldValueConfigs
            var propertyFieldValueConfigs = new List<PropertyFieldValueConfig>();

            var propertyFieldConfigs = (from productGroupPropertyStateData in _dbContext.Set<ProductGroupPropertyStateData>().AsNoTracking()
                                        join productGroupPropertyClassData in _dbContext.Set<ProductGroupPropertyClassData>().AsNoTracking()
                                            on productGroupPropertyStateData.ProductGroupId equals productGroupPropertyClassData.ProductGroupId
                                        join propertyData in _dbContext.Set<PropertyData>().AsNoTracking()
                                            on productGroupPropertyClassData.PropertyClassId equals propertyData.PropertyClassId
                                        join fieldData in _dbContext.Set<FieldData>().AsNoTracking()
                                             on propertyData.Id equals fieldData.PropertyId
                                        where productGroupPropertyStateData.ProductGroupId == queryObject.ProductGroupId
                                              && productGroupPropertyStateData.SessionId == queryObject.SessionId
                                              && productGroupPropertyStateData.PropertyId == fieldData.PropertyId
                                              && productGroupPropertyStateData.IsSelected == true
                                              && productGroupPropertyStateData.PropertyId != NEW_ARRANGEMENT
                                        select new
                                        {
                                            productGroupPropertyStateData.PropertyId,
                                            PropertyDescription = propertyData.Description,
                                            productGroupPropertyStateData.CurrencyId,
                                            fieldData.Id,
                                            fieldData.FieldId,
                                            fieldData.Json
                                        })
                                        .GroupBy(g => new
                                        {
                                            g.PropertyId,
                                            g.PropertyDescription,
                                            g.CurrencyId
                                        })
                                        .Select(x => new
                                        {
                                            x.Key.PropertyId,
                                            x.Key.PropertyDescription,
                                            x.Key.CurrencyId,
                                            FieldConfigs = x.Select(y => new
                                            {
                                                y.Id,
                                                y.FieldId,
                                                y.Json
                                            }).OrderBy(o => o.Id).ToList()
                                        })
                                        .ToList();

            var propertyFieldValues = _productGroupPropertyFieldStateRepository.Get(queryObject.SessionId, queryObject.ProductGroupId);

            foreach (var propertyFieldConfig in propertyFieldConfigs)
            {
                var propertyFieldValueConfig = new PropertyFieldValueConfig
                {
                    Property = new GetPropertySelectedFieldValuesProperty
                    {
                        PropertyId = propertyFieldConfig.PropertyId,
                        PropertyName = propertyFieldConfig.PropertyDescription,
                        CurrencyId = propertyFieldConfig.CurrencyId
                    },
                    FieldConfigs = new List<GetPropertySelectedFieldValuesAttribute>(),
                    FieldValues = propertyFieldValues.Where(w => w.PropertyId == propertyFieldConfig.PropertyId).OrderBy(o => o.Id).ToList()
                };

                propertyFieldConfig.FieldConfigs.ForEach(x => propertyFieldValueConfig.FieldConfigs.Add(JsonConvert.DeserializeObject<GetPropertySelectedFieldValuesAttribute>(x.Json)));

                propertyFieldValueConfigs.Add(propertyFieldValueConfig);
            }

            getPropertySelectedFieldValuesConfig.PropertyFieldValueConfigs = propertyFieldValueConfigs;

            return new QueryResponse<GetPropertySelectedFieldValuesConfig>(getPropertySelectedFieldValuesConfig);
        }

        public bool Validate(GetPropertySelectedFieldValues query)
        {
            if (query.SessionId == Guid.Empty)
            {
                Errors.Add("SessionId cannot be empty.");
                return false;
            }
            if (query.ProductGroupId == Guid.Empty)
            {
                Errors.Add("ProductGroupId cannot be empty.");
                return false;
            }
            return true;
        }

        private string AddIndexes(string name, int multiValueIndex, int subValueIndex)
        {
            return name + " " + multiValueIndex + "." + subValueIndex;
        }
    }
}