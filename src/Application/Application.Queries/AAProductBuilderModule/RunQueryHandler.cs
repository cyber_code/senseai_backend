﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.AAProductBuilderModel;
using Persistence.Internal;
using Queries.AAProductBuilderModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SenseAI.Domain;

namespace Application.Queries.AAProductBuilderModule
{
    public class RunQueryHandler : IQueryHandler<RunQuery, RunQueryResult[]>
    {
        private readonly IDbContext _dbContext;

        public RunQueryHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public static bool CheckForSQLInjection(List<Parameter> parameters)
        {
            foreach (var parameter in parameters)
            {
                string[] sqlCheckList = {"--",
                                         ";--",
                                         ";",
                                         "/*",
                                         "*/",
                                         "@@",
                                         "@",
                                         "char ",
                                         "nchar ",
                                         "varchar ",
                                         "nvarchar ",
                                         "alter ",
                                         "begin ",
                                         "cast",
                                         "create ",
                                         "cursor ",
                                         "declare ",
                                         "delete ",
                                         "drop ",
                                         "end ",
                                         "exec ",
                                         "execute ",
                                         "fetch ",
                                         "insert ",
                                         "kill ",
                                         "select ",
                                         "sys",
                                         "sysobjects",
                                         "syscolumns",
                                         "table ",
                                         "update "};

                string CheckString = parameter.Value.Replace("'", "''");

                for (int i = 0; i <= sqlCheckList.Length - 1; i++)
                {
                    if (CheckString.StartsWith(sqlCheckList[i], StringComparison.OrdinalIgnoreCase))
                        return true;
                }
            }

            return false;
        }

        public QueryResponse<RunQueryResult[]> Handle(RunQuery queryObject)
        {
            var query = (from queries in _dbContext.Set<QueryData>().AsNoTracking()
                         where queries.Id == queryObject.QueryId
                         select queries.Text).AsEnumerable().FirstOrDefault();

            if (string.IsNullOrWhiteSpace(query))
                return new QueryResponse<RunQueryResult[]>(null, new QueryResponseMessage((int)MessageType.Information, $"No Query Dara found for Query with ID: {queryObject.QueryId}."));

            var result = new List<RunQueryResult>();

            var a = FormatQuery(query, queryObject.Parameters);

            _dbContext.QueryFromSql<QueryResultData>(FormatQuery(query, queryObject.Parameters))
                            .ToList()
                            .ForEach(x => result.Add(new RunQueryResult { Key = x.Key, Value = x.Value }));

            return new QueryResponse<RunQueryResult[]>(result.ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No data"));
        }

        public bool Validate(RunQuery query)
        {
            if (string.IsNullOrWhiteSpace(query.QueryId))
            {
                Errors.Add("QueryId cannot be empty.");
                return false;
            }
            if (CheckForSQLInjection(query.Parameters))
            {
                Errors.Add("Parameters have sql injection.");
                return false;
            }
            return true;
        }

        private string FormatQuery(string query, List<Parameter> parameters)
        {
            var parameterQuery = new StringBuilder();
            var declare = " DECLARE ";
            var type = " VARCHAR(100) = ";

            foreach (var parameter in parameters)
            {
                parameterQuery.Append(declare + parameter.ParameterId + type + $"'{parameter.Value}' ");
            }

            return parameterQuery + query;
        }
    }
}