﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Messaging.Queries;
using Persistence.AAProductBuilderModel;
using Persistence.Internal;
using Queries.AAProductBuilderModule;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.AAProductBuilderModule
{
    public class GetNewArrangementFieldConfigsHandler : IQueryHandler<GetNewArrangementFieldConfigs, List<GetNewArrangementFieldConfigsAttribute>>
    {
        private readonly IDbContext _dbContext;

        public GetNewArrangementFieldConfigsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<List<GetNewArrangementFieldConfigsAttribute>> Handle(GetNewArrangementFieldConfigs queryObject)
        {
            var fieldConfigs = (from fields in _dbContext.Set<FieldData>().AsNoTracking()
                                where fields.PropertyId == "NEWARRANGEMENT"
                                select new
                                {
                                    fields.Id,
                                    fields.FieldId,
                                    fields.Json
                                }).OrderBy(x => x.Id).ToList();

            var attributes = new List<GetNewArrangementFieldConfigsAttribute>();

            foreach (var fieldConfig in fieldConfigs)
            {
                var attribute = JsonConvert.DeserializeObject<GetNewArrangementFieldConfigsAttribute>(fieldConfig.Json);

                if (attribute.IsMultiValue)
                {
                    attribute.Description = AddIndexes(attribute.Description, 1, 1);
                }

                attributes.Add(attribute);
            }

            return new QueryResponse<List<GetNewArrangementFieldConfigsAttribute>>(attributes);
        }

        public bool Validate(GetNewArrangementFieldConfigs query)
        {
            return true;
        }

        private string AddIndexes(string name, int multiValueIndex, int subValueIndex)
        {
            return name + " " + multiValueIndex + "." + subValueIndex;
        }
    }
}