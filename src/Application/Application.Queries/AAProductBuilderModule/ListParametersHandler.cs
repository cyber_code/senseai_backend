﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.AAProductBuilderModel;
using Persistence.Internal;
using Queries.AAProductBuilderModule;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.AAProductBuilderModule
{
    public class ListParametersHandler : IQueryHandler<ListParameters, List<string>>
    {
        private readonly IDbContext _dbContext;

        public ListParametersHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<List<string>> Handle(ListParameters queryObject)
        {
            var query = from parameters in _dbContext.Set<ParameterData>().AsNoTracking()
                        select parameters.Id;

            return new QueryResponse<List<string>>(query.ToList(), query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No parameter found"));
        }

        public bool Validate(ListParameters query)
        {
            return true;
        }
    }
}