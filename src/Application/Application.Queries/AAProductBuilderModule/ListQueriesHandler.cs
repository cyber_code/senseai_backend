﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.AAProductBuilderModel;
using Persistence.Internal;
using Queries.AAProductBuilderModule;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.AAProductBuilderModule
{
    public class ListQueriesHandler : IQueryHandler<ListQueries, ListQueriesResult[]>
    {
        private readonly IDbContext _dbContext;

        public ListQueriesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<ListQueriesResult[]> Handle(ListQueries queryObject)
        {
            var query = from queries in _dbContext.Set<QueryData>().AsNoTracking()
                        join queryParameters in _dbContext.Set<QueryParameterData>().AsNoTracking()
                            on queries.Id equals queryParameters.QueryId into qp
                        from p in qp.DefaultIfEmpty()
                        select new
                        {
                            queries.Id,
                            queries.Title,
                            p.ParameterId
                        };

            var result = query
                            .GroupBy(g => new { g.Id, g.Title })
                            .Select(x => new ListQueriesResult(x.Key.Id, x.Key.Title, x.Where(z => z.ParameterId != null).Select(y => y.ParameterId).ToList()))
                            .ToArray();

            return new QueryResponse<ListQueriesResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No queries found"));
        }

        public bool Validate(ListQueries query)
        {
            return true;
        }
    }
}