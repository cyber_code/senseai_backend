﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.AAProductBuilderModel;
using Persistence.Internal;
using Queries.AAProductBuilderModule;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.AAProductBuilderModule
{
    public class GetQueryHandler : IQueryHandler<GetQuery, GetQueryResult>
    {
        private readonly IDbContext _dbContext;

        public GetQueryHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetQueryResult> Handle(GetQuery queryObject)
        {
            var query = from queries in _dbContext.Set<QueryData>().AsNoTracking()
                        join queryParameters in _dbContext.Set<QueryParameterData>().AsNoTracking()
                            on queries.Id equals queryParameters.QueryId into qp
                        from p in qp.DefaultIfEmpty()
                        where queries.Id == queryObject.QueryId
                        select new
                        {
                            queries.Id,
                            queries.Title,
                            p.ParameterId
                        };

            var result = query
                            .GroupBy(g => new { g.Id, g.Title })
                            .Select(x => new GetQueryResult(x.Key.Id, x.Key.Title, x.Where(z => z.ParameterId != null).Select(y => y.ParameterId).ToList()))
                            .First();

            return new QueryResponse<GetQueryResult>(result, result is GetQueryResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Query found with ID {queryObject.QueryId}."));
        }

        public bool Validate(GetQuery query)
        {
            if (string.IsNullOrWhiteSpace(query.QueryId))
            {
                Errors.Add("QueryId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}