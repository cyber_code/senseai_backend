﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.PeopleModel;
using Queries.PeopleModule;
using SenseAI.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.PeopleModule
{
    public sealed class GetPeoplesByIdsHandler : IQueryHandler<GetPeoplesByIds, GetPeoplesByIdsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetPeoplesByIdsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetPeoplesByIdsResult[]> Handle(GetPeoplesByIds queryObject)
        {
            var query = from people in _dbContext.Set<PeopleData>().AsNoTracking()
                        join role in _dbContext.Set<RoleData>().AsNoTracking()
                       on people.RoleId equals role.Id
                       where (queryObject.Ids.Any() &&
                          queryObject.Ids.Contains(people.Id))
                        select new
                        {
                            people.Id,
                            people.RoleId,
                            people.Name,
                            people.Surname,
                            people.Email,
                            people.Address,
                            role.Title,
                            people.UserIdFromAdminPanel,
                            people.SubProjectId,
                            people.Color
                        };

            var result = query.Select(people => new GetPeoplesByIdsResult(people.Id, people.RoleId, people.Name, people.Surname, people.Email, people.Address,people.Title,
                            people.UserIdFromAdminPanel, people.SubProjectId, people.Color)).ToArray();

            return new QueryResponse<GetPeoplesByIdsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No peoples found."));
        }

        public bool Validate(GetPeoplesByIds query)
        {
            return true;
        }
    }
}