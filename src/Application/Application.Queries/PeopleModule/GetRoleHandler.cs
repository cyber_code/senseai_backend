﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.PeopleModel;
using Queries.PeopleModule;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.PeopleModule
{
    public class GetRoleHandler : IQueryHandler<GetRole, GetRoleResult>
    {
        private readonly IDbContext _dbContext;

        public GetRoleHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetRoleResult> Handle(GetRole queryObject)
        {
            var query = from role in _dbContext.Set<RoleData>()
                        where role.Id == queryObject.Id
                        select new
                        {
                            role.Id,
                            role.Title,
                            role.Description,
                            role.SubProjectId
                        };

            var result = query.Select(role => new GetRoleResult(role.Id, role.Title, role.Description, role.SubProjectId)).FirstOrDefault();

            return new QueryResponse<GetRoleResult>(result, result is GetRoleResult ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No role found."));
        }

        public bool Validate(GetRole query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}