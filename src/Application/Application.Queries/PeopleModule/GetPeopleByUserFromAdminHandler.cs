﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.PeopleModel;
using Queries.PeopleModule;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.PeopleModule
{
    public class GetPeopleByUserFromAdminHandler : IQueryHandler<GetPeopleByUserFromAdmin, GetPeopleByUserFromAdminResult>
    {
        private readonly IDbContext _dbContext;

        public GetPeopleByUserFromAdminHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetPeopleByUserFromAdminResult> Handle(GetPeopleByUserFromAdmin queryObject)
        {
            var query = from people in _dbContext.Set<PeopleData>().AsNoTracking()
                        join role in _dbContext.Set<RoleData>().AsNoTracking()
                        on people.RoleId equals role.Id
                        where people.UserIdFromAdminPanel == queryObject.UserIdFromAdminPanel && 
                        people.SubProjectId == queryObject.SubProjectId

                        select new
                        {
                            people.Id,
                            people.RoleId,
                            people.Name,
                            people.Surname,
                            people.Email,
                            people.Address,
                            role.Title,
                            people.UserIdFromAdminPanel,
                            people.SubProjectId,
                            people.Color
                        };

            var result = query.Select(people => new GetPeopleByUserFromAdminResult(people.Id, people.RoleId, people.Name, people.Surname, people.Email, people.Address,people.Title,
                            people.UserIdFromAdminPanel, people.SubProjectId, people.Color)).FirstOrDefault();

            return new QueryResponse<GetPeopleByUserFromAdminResult>(result, result is GetPeopleByUserFromAdminResult ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No people found."));
        }

        public bool Validate(GetPeopleByUserFromAdmin query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (query.UserIdFromAdminPanel == 0)
            {
                Errors.Add("UserIdFromAdminPanel cannot be empty.");
                return false;
            }

            return true;
        }
    }
}