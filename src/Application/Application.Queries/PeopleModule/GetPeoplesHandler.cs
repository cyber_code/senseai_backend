﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.PeopleModel;
using Queries.PeopleModule;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.PeopleModule
{
    public sealed class GetPeoplesHandler : IQueryHandler<GetPeoples, GetPeoplesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetPeoplesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetPeoplesResult[]> Handle(GetPeoples queryObject)
        {
            var query = from people in _dbContext.Set<PeopleData>().AsNoTracking()
                        join role in _dbContext.Set<RoleData>().AsNoTracking()
                        on people.RoleId equals role.Id
                        where people.SubProjectId == queryObject.SubProjectId
                        select new
                        {
                            people.Id,
                            people.RoleId,
                            people.Name,
                            people.Surname,
                            people.Email,
                            people.Address,
                            role.Title,
                            people.UserIdFromAdminPanel,
                            people.SubProjectId,
                            people.Color
                        };

            var result = query.Select(people => new GetPeoplesResult(people.Id, people.RoleId, people.Name, people.Surname, people.Email, people.Address,people.Title,
                            people.UserIdFromAdminPanel, people.SubProjectId, people.Color)).ToArray();

            return new QueryResponse<GetPeoplesResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No peoples found."));
        }

        public bool Validate(GetPeoples query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}