﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.PeopleModel;
using Queries.PeopleModule;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.PeopleModule
{
    public sealed class GetRolesHandler : IQueryHandler<GetRoles, GetRolesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetRolesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetRolesResult[]> Handle(GetRoles queryObject)
        {
            var query = from role in _dbContext.Set<RoleData>().AsNoTracking()
                        where role.SubProjectId == queryObject.SubProjectId
                        select new
                        {
                            role.Id,
                            role.Title,
                            role.Description,
                            role.SubProjectId
                        };

            var result = query.Select(role => new GetRolesResult(role.Id, role.Title, role.Description, role.SubProjectId)).ToArray();

            return new QueryResponse<GetRolesResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No Roles found."));
        }

        public bool Validate(GetRoles query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}