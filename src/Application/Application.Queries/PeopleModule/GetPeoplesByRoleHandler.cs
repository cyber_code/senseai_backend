﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.PeopleModel;
using Queries.PeopleModule;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.PeopleModule
{
    public class GetPeoplesByRoleHandler : IQueryHandler<GetPeoplesByRole, GetPeoplesByRoleResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetPeoplesByRoleHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetPeoplesByRoleResult[]> Handle(GetPeoplesByRole queryObject)
        {
            var query = from people in _dbContext.Set<PeopleData>()
                        where people.RoleId == queryObject.RoleId
                        select new
                        {
                            people.Id,
                            people.RoleId,
                            people.Name,
                            people.Surname,
                            people.Email,
                            people.Address,
                            people.UserIdFromAdminPanel,
                            people.SubProjectId,
                            people.Color
                        };

            var result = query.Select(people => new GetPeoplesByRoleResult(people.Id, people.RoleId, people.Name, people.Surname, people.Email, people.Address,
                            people.UserIdFromAdminPanel, people.SubProjectId, people.Color)).ToArray();

            return new QueryResponse<GetPeoplesByRoleResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No people found."));
        }

        public bool Validate(GetPeoplesByRole query)
        {
            if (query.RoleId == Guid.Empty)
            {
                Errors.Add("RoleId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}