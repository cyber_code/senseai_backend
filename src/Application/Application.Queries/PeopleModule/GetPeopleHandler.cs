﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.PeopleModel;
using Queries.PeopleModule;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.PeopleModule
{
    public class GetPeopleHandler : IQueryHandler<GetPeople, GetPeopleResult>
    {
        private readonly IDbContext _dbContext;

        public GetPeopleHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetPeopleResult> Handle(GetPeople queryObject)
        {
            var query = from people in _dbContext.Set<PeopleData>().AsNoTracking()
                        join role in _dbContext.Set<RoleData>().AsNoTracking()
                        on people.RoleId equals role.Id
                        where people.Id == queryObject.Id
                        select new
                        {
                            people.Id,
                            people.RoleId,
                            people.Name,
                            people.Surname,
                            people.Email,
                            people.Address,
                            role.Title,
                            people.UserIdFromAdminPanel,
                            people.SubProjectId,
                            people.Color
                        };

            var result = query.Select(people => new GetPeopleResult(people.Id, people.RoleId, people.Name, people.Surname, people.Email, people.Address,people.Title,people.UserIdFromAdminPanel, people.SubProjectId, people.Color)).FirstOrDefault();

            return new QueryResponse<GetPeopleResult>(result, result is GetPeopleResult ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No people found."));
        }

        public bool Validate(GetPeople query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}