﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.SystemModel;
using Queries.AdministrationModule.Systems;
using System;
using System.Linq;
using System.Collections.Generic;
using Persistence.CatalogModel;
using Microsoft.EntityFrameworkCore;
using SenseAI.Domain;

namespace Application.Queries.AdministrationModule.Systems
{
    public sealed class GetTenantCatalogsHandler : IQueryHandler<GetTenantCatalogs, GetTenantCatalogsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetTenantCatalogsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTenantCatalogsResult[]> Handle(GetTenantCatalogs queryObject)
        {
            var query = from catalog in _dbContext.Set<CatalogData>().AsNoTracking()
                        where ((catalog.ProjectId == Guid.Empty && catalog.SubProjectId == Guid.Empty) || 
                        (catalog.ProjectId == queryObject.ProjectId && catalog.SubProjectId == Guid.Empty) || 
                        (catalog.ProjectId == queryObject.ProjectId && catalog.SubProjectId == queryObject.SubprojectId))
                        select new
                        {
                            catalog.Id,
                            catalog.ProjectId,
                            catalog.SubProjectId,
                            catalog.Title,
                            catalog.Description,
                            catalog.Type
                        };

            var result = query.Select(catalog => new GetTenantCatalogsResult(catalog.Id, catalog.ProjectId, catalog.SubProjectId, catalog.Title, catalog.Description, catalog.Type.ToString())).ToArray();

            return new QueryResponse<GetTenantCatalogsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No catalogs found."));
        }

        public bool Validate(GetTenantCatalogs query)
        {
            return true;
        }
    }
}