﻿using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Queries.AdministrationModule.Folder;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.AdministrationModule.Folder
{
    public class GetFolderHandler : IQueryHandler<GetFolder, GetFolderResult>
    {
        private readonly IDbContext _dbContext;

        public GetFolderHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetFolderResult> Handle(GetFolder queryObject)
        {
            var query = from folder in _dbContext.Set<FolderData>()
                        where folder.Id == queryObject.Id
                        select new GetFolderResult(folder.Id, folder.SubProjectId, folder.Title, folder.Description);

            return new QueryResponse<GetFolderResult>(query.FirstOrDefault(), query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Folder found."));
        }

        public bool Validate(GetFolder query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("FolderId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}