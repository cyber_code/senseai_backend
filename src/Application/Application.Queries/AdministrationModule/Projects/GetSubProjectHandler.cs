﻿using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Queries.AdministrationModule.Projects;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.AdministrationModule.Projects
{
    public class GetSubProjectHandler : IQueryHandler<GetSubProject, GetSubProjectResult>
    {
        private readonly IDbContext _dbContext;

        public GetSubProjectHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSubProjectResult> Handle(GetSubProject queryObject)
        {
            var query = from project in _dbContext.Set<SubProjectData>()
                        where project.Id == queryObject.Id
                        select new
                        {
                            project.Id,
                            project.ProjectId,
                            project.Title,
                            project.Description,
                            project.JiraLink,
                            project.IsJiraIntegrationEnabled
                        };

            var result = query.Select(project => new GetSubProjectResult(project.Id, project.ProjectId, project.Title, project.Description, project.JiraLink, project.IsJiraIntegrationEnabled)).FirstOrDefault();

            return new QueryResponse<GetSubProjectResult>(result, result is GetSubProjectResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Sub-Project found."));
        }

        public bool Validate(GetSubProject query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}