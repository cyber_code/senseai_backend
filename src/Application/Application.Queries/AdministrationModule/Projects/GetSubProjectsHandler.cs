﻿using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Queries.AdministrationModule.Projects;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.AdministrationModule.Projects
{
    public sealed class GetSubProjectsHandler : IQueryHandler<GetSubProjects, GetSubProjectsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetSubProjectsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSubProjectsResult[]> Handle(GetSubProjects queryObject)
        {
            var query = from subProject in _dbContext.Set<SubProjectData>()
                        where subProject.ProjectId == queryObject.ProjectId
                        select new
                        {
                            subProject.Id,
                            subProject.ProjectId,
                            subProject.Title,
                            subProject.Description
                        };

            var result = query.Select(subProject => new GetSubProjectsResult(subProject.Id, subProject.ProjectId, subProject.Title, subProject.Description)).ToArray();

            return new QueryResponse<GetSubProjectsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Sub-Projects found."));
        }

        public bool Validate(GetSubProjects query)
        {
            if (query.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}