﻿using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Queries.AdministrationModule.Projects;
using SenseAI.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.AdministrationModule.Projects
{
    public sealed class GetProjectsHandler : IQueryHandler<GetProjects, GetProjectsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetProjectsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetProjectsResult[]> Handle(GetProjects queryObject)
        {
            var query = from project in _dbContext.Set<ProjectData>()
                        select new
                        {
                            project.Id,
                            project.Title,
                            project.Description
                        };

            var result = query.Select(project => new GetProjectsResult(project.Id, project.Title, project.Description)).ToArray();

            return new QueryResponse<GetProjectsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No projects found."));
        }

        public bool Validate(GetProjects query)
        {
            return true;
        }
    }
}