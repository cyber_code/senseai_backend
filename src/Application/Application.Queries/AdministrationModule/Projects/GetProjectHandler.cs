﻿using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Queries.AdministrationModule.Projects;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.AdministrationModule.Projects
{
    public class GetProjectHandler : IQueryHandler<GetProject, GetProjectResult>
    {
        private readonly IDbContext _dbContext;

        public GetProjectHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetProjectResult> Handle(GetProject queryObject)
        {
            var query = from project in _dbContext.Set<ProjectData>()
                        where project.Id == queryObject.Id
                        select new
                        {
                            project.Id,
                            project.Title,
                            project.Description
                        };

            var result = query.Select(project => new GetProjectResult(project.Id, project.Title, project.Description)).FirstOrDefault();

            return new QueryResponse<GetProjectResult>(result, result is GetProjectResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Project found."));
        }

        public bool Validate(GetProject query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}