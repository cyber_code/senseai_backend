﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.SystemCatalogModel;
using Queries.AdministrationModule.SystemCatalogs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.AdministrationModule.SystemCatalogs
{
   public sealed class GetSystemCatalogHendler : IQueryHandler<GetSystemCatalog, GetSystemCatalogResult>
    {
        private readonly IDbContext _dbContext;

        public GetSystemCatalogHendler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public List<string> Errors { get; set; }
        public QueryResponse<GetSystemCatalogResult> Handle(GetSystemCatalog queryObject)
        {
            var result = (from systemCatalogs in _dbContext.Set<SystemCatalogData>()
                          where systemCatalogs.SubprojectId == queryObject.SubProjectId && systemCatalogs.SystemId == queryObject.SystemId
                          && systemCatalogs.CatalogId == queryObject.CatalogId
                          select new GetSystemCatalogResult(
                              systemCatalogs.Id,
                              systemCatalogs.SubprojectId,
                              systemCatalogs.SystemId,
                              systemCatalogs.CatalogId
                          )).FirstOrDefault();



            return new QueryResponse<GetSystemCatalogResult>(result, result != null ? null : new QueryResponseMessage((int)MessageType.Information,
                $"No relation for subproject, system and catalog!"));
        }

        public bool Validate(GetSystemCatalog query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty!");
                return false;
            }
            if (query.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}