﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.SystemCatalogModel;
using Queries.AdministrationModule.SystemCatalogs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SenseAI.Domain;

namespace Application.Queries.AdministrationModule.SystemCatalogs
{
    public sealed class GetCatalogSystemHandler : IQueryHandler<GetCatalogSystem, GetCatalogSystemResult>
    {
        private readonly IDbContext _dbContext;

        public GetCatalogSystemHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetCatalogSystemResult> Handle(GetCatalogSystem queryObject)
        {
            var result = (from systemCatalogs in _dbContext.Set<SystemCatalogData>().AsNoTracking()
                          where systemCatalogs.SubprojectId == queryObject.SubProjectId
                          && systemCatalogs.CatalogId == queryObject.CatalogId
                          select new GetCatalogSystemResult(systemCatalogs.Id,
                              systemCatalogs.SubprojectId,
                              systemCatalogs.SystemId,
                              systemCatalogs.CatalogId
                          )).FirstOrDefault();

            return new QueryResponse<GetCatalogSystemResult>(result, result != null ? null : new QueryResponseMessage((int)MessageType.Information,
                $"No system specified for catalog!"));
        }

        public bool Validate(GetCatalogSystem query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}