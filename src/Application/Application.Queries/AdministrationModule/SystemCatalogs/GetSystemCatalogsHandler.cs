﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.SystemCatalogModel;
using Queries.AdministrationModule.SystemCatalogs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SenseAI.Domain;
using Microsoft.EntityFrameworkCore;
using Persistence.CatalogModel;

namespace Application.Queries.AdministrationModule.SystemCatalogs
{
   public sealed class GetSystemCatalogsHandler : IQueryHandler<GetSystemCatalogs, GetSystemCatalogsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetSystemCatalogsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public List<string> Errors { get; set; }
        public QueryResponse<GetSystemCatalogsResult []> Handle(GetSystemCatalogs queryObject)
        {
            var result = (from systemCatalogs in _dbContext.Set<SystemCatalogData>().AsNoTracking() join
                          catalog in _dbContext.Set<CatalogData>().AsNoTracking() on systemCatalogs.CatalogId equals catalog.Id
                          where systemCatalogs.SubprojectId == queryObject.SubProjectId && systemCatalogs.SystemId == queryObject.SystemId
                          select new GetSystemCatalogsResult(
                              systemCatalogs.Id,
                              catalog.Title,
                              catalog.Type
                          )).ToArray();



            return new QueryResponse<GetSystemCatalogsResult[]>(result, result != null ? null : new QueryResponseMessage((int)MessageType.Information,
                $"No catalogs found in system!"));
        }

        public bool Validate(GetSystemCatalogs query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            
            if (query.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}