﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.SystemModel;
using Queries.AdministrationModule.Systems;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.AdministrationModule.Systems
{
    public sealed class GetSystemByIdHandler : IQueryHandler<GetSystemById, GetSystemByIdResult>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }
        public GetSystemByIdHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetSystemByIdResult> Handle(GetSystemById queryObject)
        {
            var query = from system in _dbContext.Set<SystemData>()
                        where system.Id == queryObject.Id
                        select new GetSystemByIdResult(system.Id, system.Title, system.AdapterName);

            return new QueryResponse<GetSystemByIdResult>(query.FirstOrDefault(), query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No system found."));
        }

        public bool Validate(GetSystemById query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }
            return true;
        }
    }
}