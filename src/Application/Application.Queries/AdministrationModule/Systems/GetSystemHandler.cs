﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.SystemModel;
using Queries.AdministrationModule.Systems;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.AdministrationModule.Systems
{
    public sealed class GetSystemHandler : IQueryHandler<GetSystems, GetSystemsResult>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }
        public GetSystemHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetSystemsResult> Handle(GetSystems queryObject)
        {
            var query = from system in _dbContext.Set<SystemData>()
                        where system.Title == queryObject.Title && system.AdapterName == queryObject.AdapterName
                        select new GetSystemsResult(system.Id, system.Title, system.AdapterName);

            return new QueryResponse<GetSystemsResult>(query.FirstOrDefault(), query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No system found."));
        }

        public bool Validate(GetSystems query)
        {
            if (String.IsNullOrEmpty(query.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }
            if (String.IsNullOrEmpty(query.AdapterName))
            {
                Errors.Add("AdapterName cannot be empty!");
                return false;
            }
            return true;
        }
    }
}