﻿using Messaging.Queries;
using Queries.AdministrationModule.Folder;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.AdministrationModule.Folder
{
    public class GetAdaptersHandler : IQueryHandler<GetAdapters, GetAdaptersResult[]>
    {
        public GetAdaptersHandler()
        {

        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetAdaptersResult[]> Handle(GetAdapters queryObject)
        {
            var query = Enum.GetNames(typeof(AdapterType));
            var result = from a in query
                         select new GetAdaptersResult(a);

            return new QueryResponse<GetAdaptersResult[]>(result.ToArray(), query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Adapters found."));
        }

        public bool Validate(GetAdapters query)
        {
            return true;
        }
    }
}