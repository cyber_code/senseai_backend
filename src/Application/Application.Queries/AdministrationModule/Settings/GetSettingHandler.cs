﻿using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Queries.AdministrationModule.Settings;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.AdministrationModule.Settings
{
    public sealed class GetSettingHandler : IQueryHandler<GetSetting, GetSettingResult>
    {
        private readonly IDbContext _dbContext;

        public GetSettingHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSettingResult> Handle(GetSetting queryObject)
        {
            var result = (from settings in _dbContext.Set<SettingsData>()
                        where settings.ProjectId == queryObject.ProjectId && settings.SubProjectId == queryObject.SubProjectId
                        select new GetSettingResult(
                            settings.Id,
                            settings.ProjectId,
                            settings.SubProjectId,
                            settings.SystemId,
                            settings.CatalogId
                        )).FirstOrDefault();

            return new QueryResponse<GetSettingResult>(result, result is GetSettingResult ? null : new QueryResponseMessage((int)MessageType.Information,
                $"No setting found for project and subproject!"));
        }

        public bool Validate(GetSetting query)
        {
            if (query.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}