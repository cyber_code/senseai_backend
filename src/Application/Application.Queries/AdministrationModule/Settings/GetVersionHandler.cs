﻿using Messaging.Queries;
using Queries.AdministrationModule.Settings;
using System.Collections.Generic;
using System.Reflection;

namespace Application.Queries.AdministrationModule.Settings
{
    public sealed class GetVersionHandler : IQueryHandler<GetVersion, GetVersionResult>
    {
        public List<string> Errors { get; set; }

        public QueryResponse<GetVersionResult> Handle(GetVersion queryObject)
        {
            var getVersionInfo = Assembly.GetEntryAssembly().GetName().Version;

            return new QueryResponse<GetVersionResult>(new GetVersionResult(getVersionInfo.ToString()));
        }

        public bool Validate(GetVersion query)
        {
            return true;
        }
    }
}