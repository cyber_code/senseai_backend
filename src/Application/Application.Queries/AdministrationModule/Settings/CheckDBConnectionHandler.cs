﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Queries.AdministrationModule.Settings;
using Logging.Messaging;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.AdministrationModule.Settings
{
    public sealed class CheckDBConnectionHandler : IQueryHandler<CheckDBConnection, CheckDBConnectionResult>
    {
        private readonly IDbContext _dbContext;
        private readonly IMessagingLogger _messagingLogger;

        public CheckDBConnectionHandler(IDbContext dbContext, IMessagingLogger messagingLogger)
        {
            _dbContext = dbContext;
            _messagingLogger = messagingLogger;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<CheckDBConnectionResult> Handle(CheckDBConnection queryObject)
        {
            var context = (DbContext)_dbContext;

            try
            {
                context.Database.OpenConnection();
                context.Database.CloseConnection();
            }
            catch (System.Data.SqlClient.SqlException)
            {
                return new QueryResponse<CheckDBConnectionResult>(new CheckDBConnectionResult(false), new QueryResponseMessage((int)MessageType.Information, "Cannot connect to Database server."));
            }

            return new QueryResponse<CheckDBConnectionResult>(new CheckDBConnectionResult(true));
        }

        public bool Validate(CheckDBConnection query)
        {
            return true;
        }
    }
}