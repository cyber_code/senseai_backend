﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.SystemTagModel;
using Queries.AdministrationModule.SystemTags;
using SenseAI.Domain;

namespace Application.Queries.AdministrationModule.SystemTags
{
    public sealed class GetSystemTagHandler : IQueryHandler<GetSystemTag, GetSystemTagResult>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetSystemTagHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetSystemTagResult> Handle(GetSystemTag queryObject)
        {
            var query = from systemTag in _dbContext.Set<SystemTagData>()
                        where systemTag.Id == queryObject.Id
                        select new
                        {
                            systemTag.Id,
                            systemTag.SystemId,
                            systemTag.Title,
                            systemTag.Description
                        };

            var result = query.Select(systemTag => new GetSystemTagResult(systemTag.Id, systemTag.SystemId, systemTag.Title, systemTag.Description)).FirstOrDefault();

            return new QueryResponse<GetSystemTagResult>(result, result != null ? null : new QueryResponseMessage((int)MessageType.Information, "No SystemTag found."));
        }

        public bool Validate(GetSystemTag query)
        {
            if (query.Id == null || query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            return true;
        }
    }
}