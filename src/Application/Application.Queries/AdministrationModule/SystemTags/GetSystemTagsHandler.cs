﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.SystemTagModel;
using Queries.AdministrationModule.SystemTags;
using SenseAI.Domain;

namespace Application.Queries.AdministrationModule.SystemTags
{
    public sealed class GetSystemTagsHandler : IQueryHandler<GetSystemTags, GetSystemTagsResult[]>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetSystemTagsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetSystemTagsResult[]> Handle(GetSystemTags queryObject)
        {
            var query = from systemTag in _dbContext.Set<SystemTagData>()
                        where systemTag.SystemId == queryObject.SystemId
                        select new
                        {
                            systemTag.Id,
                            systemTag.SystemId,
                            systemTag.Title,
                            systemTag.Description
                        };

            var result = query.Select(systemTag => new GetSystemTagsResult(systemTag.Id, systemTag.SystemId, systemTag.Title, systemTag.Description)).ToArray();

            return new QueryResponse<GetSystemTagsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No SystemTags found."));
        }

        public bool Validate(GetSystemTags query)
        {
            if (query.SystemId == null || query.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}