﻿using Messaging.Queries;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Queries.DataModule.Data.DataSets;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.DataSets
{
    public sealed class GetDataSetItemHandler : IQueryHandler<GetDataSetItem, GetDataSetItemResult>
    {
        private readonly IDbContext _dbContext;

        public GetDataSetItemHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetDataSetItemResult> Handle(GetDataSetItem queryObject)
        {
            var qDataSetItems = from dataSetItem in _dbContext.Set<DataSetItemData>()
                                where dataSetItem.DataSetId == queryObject.DataSetId
                                select new
                                {
                                    dataSetItem.Id,
                                    dataSetItem.DataSetId,
                                    dataSetItem.Row
                                };
            var rows = qDataSetItems.Select(ct => new Rows(ct.Id, ct.Row)).ToArray();

            return new QueryResponse<GetDataSetItemResult>(new GetDataSetItemResult(queryObject.DataSetId, rows), rows.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Data-Set-Items found."));
        }

        public bool Validate(GetDataSetItem query)
        {
            if (query.DataSetId == Guid.Empty)
            {
                Errors.Add("DataSetId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}