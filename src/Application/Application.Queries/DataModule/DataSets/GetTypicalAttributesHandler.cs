﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SenseAI.Domain;
using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.DataSets;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Application.Queries.DataModule.DataSets
{
    public sealed class GetTypicalAttributesHandler : IQueryHandler<GetTypicalAttributes, GetTypicalAttributesResult>
    {
        private readonly IDbContext _dbContext;

        public GetTypicalAttributesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTypicalAttributesResult> Handle(GetTypicalAttributes queryObject)
        {
            var qTypical = (from typical in _dbContext.Set<TypicalData>().AsNoTracking()
                            where typical.Id == queryObject.TypicalId
                            select new
                            {
                                typical.Id,
                                typical.Title,
                                typical.Json
                            }).FirstOrDefault();

            if (qTypical == null)
                return new QueryResponse<GetTypicalAttributesResult>(null, new QueryResponseMessage((int)MessageType.Information, $"No Typical found."));


            var result = JsonConvert.DeserializeObject<GetTypicalAttributesResult>(qTypical.Json.ToString());

            MandatoryField mandatoryField = (MandatoryField)queryObject.Mandatory;

            var attributes = result.Attributes;
            if (mandatoryField != MandatoryField.All)
                attributes = attributes?.Where(a => a.IsRequired == (mandatoryField == MandatoryField.Mandatory)).ToArray();

            return new QueryResponse<GetTypicalAttributesResult>(new GetTypicalAttributesResult(qTypical.Id, qTypical.Title, attributes), qTypical != null ? null : new QueryResponseMessage((int)MessageType.Information, $"No Typical Attributes found with TypicalId: '{queryObject.TypicalId}'."));
        }

        public bool Validate(GetTypicalAttributes query)
        {
            if (query.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}