﻿using Microsoft.EntityFrameworkCore;
using Application.Queries.DesignModule.Suggestions;
using Messaging.Queries;
using NeuralEngine;
using NeuralEngine.DataSet;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DataModule.Data.DataSets;
using System;
using System.Linq;
using SenseAI.Domain;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Application.Queries.DataModule.DataSets
{
    public sealed class GetSuggestedDataSetsHandler : IQueryHandler<GetSuggestedDataSets, GetSuggestedDataSetsResult[]>
    {
        private readonly INeuralEngineClient _neuralClient;
        private readonly IDbContext _dbContext;

        public GetSuggestedDataSetsHandler(INeuralEngineClient neuralClient, IDbContext dbContext)
        {
            _neuralClient = neuralClient;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSuggestedDataSetsResult[]> Handle(GetSuggestedDataSets queryObject)
        {
            var qWorkflow = (from workflow in _dbContext.Set<WorkflowVersionData>().AsNoTracking()
                             where workflow.WorkflowId == queryObject.WorkflowId && workflow.IsLast == true
                             select new
                             {
                                 workflow.DesignerJson
                             }).FirstOrDefault();

            GetSuggestedDataSetsResult[] result = Array.Empty<GetSuggestedDataSetsResult>();
            if (qWorkflow == null)
                return new QueryResponse<GetSuggestedDataSetsResult[]>(result, new QueryResponseMessage((int)MessageType.Failed, string.Format("Workflow is not found.")));

            string message = "";
            MessageType messageType = MessageType.Warning;

            var json = JObject.Parse(qWorkflow.DesignerJson);
            var workflowDesignerData = JsonConvert.DeserializeObject<WorkflowDesignerData>(json.ToString());
            GetDataSetsItemsRequest request = WorkflowFactory.Create(queryObject, qWorkflow.DesignerJson);

            foreach (var item in workflowDesignerData.Cells.Where(u => u.CustomData?.Type == WorkflowType.Linked))
            {
                var qInnerWorkflow = (from workflow in _dbContext.Set<WorkflowVersionData>().AsNoTracking()
                                 where workflow.WorkflowId == item.CustomData.WorkflowId && workflow.IsLast == true
                                 select new
                                 {
                                     workflow.DesignerJson
                                 }).FirstOrDefault();

                var innserJson = JObject.Parse(qInnerWorkflow.DesignerJson);
                var innerWorkflowDesignerData = JsonConvert.DeserializeObject<WorkflowDesignerData>(json.ToString());

                var innserWorkflow = WorkflowFactory.Create(queryObject, qWorkflow.DesignerJson);
                AppendRecursivelyLinkWorkflow(innserWorkflow, innerWorkflowDesignerData, queryObject);
                request.WorkflowItems.AddRange(innserWorkflow.WorkflowItems);
                request.WorkflowItemLinks.AddRange(innserWorkflow.WorkflowItemLinks);
            }
            request.TenantId = queryObject.TenantId;
            request.ProjectId = queryObject.ProjectId;
            request.SubProjectId = queryObject.SubProjectId;
            request.CatalogId = queryObject.CatalogId;
            request.TypicalName = queryObject.TypicalName;
            request.CurrentItemId = queryObject.PathItemId;
            request.TypicalId = queryObject.TypicalId;

            var mlResult = _neuralClient.GetDataSetItems(request);

            if (mlResult != null && mlResult.DataSets != null && mlResult.DataSets.Any(ct => ct.Items.Count() > 0))
            {
                result = mlResult.DataSets.Select(datasetitem => new GetSuggestedDataSetsResult(datasetitem.Id, datasetitem.Title, datasetitem.Coverage, datasetitem.Items)).ToArray();
                messageType = MessageType.Successful;
            }
            else
                message = string.Format("No rows for typical:'{0}', returned from ML.", queryObject.TypicalName);
            return new QueryResponse<GetSuggestedDataSetsResult[]>(result, new QueryResponseMessage((int)messageType, message));
        }

        private void AppendRecursivelyLinkWorkflow(GetDataSetsItemsRequest innserWorkflow, WorkflowDesignerData workflowDesignerData, GetSuggestedDataSets queryObject)
        {
            if (workflowDesignerData.Cells.Where(u => u.CustomData?.Type == WorkflowType.Linked).Count() == 0)
                return;

            foreach (var item in workflowDesignerData.Cells.Where(u => u.CustomData?.Type == WorkflowType.Linked))
            {
                var qInnerWorkflow = (from workflow in _dbContext.Set<WorkflowVersionData>().AsNoTracking()
                                      where workflow.WorkflowId == item.CustomData.WorkflowId && workflow.IsLast == true
                                      select new
                                      {
                                          workflow.DesignerJson
                                      }).FirstOrDefault();

                var innserJson = JObject.Parse(qInnerWorkflow.DesignerJson);
                var innerWorkflowDesignerData = JsonConvert.DeserializeObject<WorkflowDesignerData>(innserJson.ToString());

                var innerWorkflow = WorkflowFactory.Create(queryObject, innerWorkflowDesignerData);
                AppendRecursivelyLinkWorkflow(innserWorkflow, innerWorkflowDesignerData, queryObject);
                innserWorkflow.WorkflowItems.AddRange(innerWorkflow.WorkflowItems);
                innserWorkflow.WorkflowItemLinks.AddRange(innerWorkflow.WorkflowItemLinks);
            }
        }

        public bool Validate(GetSuggestedDataSets query)
        {
            if (query.TenantId == Guid.Empty)//TODO: is not used in query
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (query.ProjectId == Guid.Empty)//TODO: is not used in query
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (query.SystemId == Guid.Empty)//TODO: is not used in query
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(query.TypicalName))
            {
                Errors.Add("Typical name cannot be empty.");
                return false;
            }
            if (query.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}