﻿using Messaging.Queries;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Queries.DataModule.Data.DataSets;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.DataSets
{
    public sealed class GetDataSetItemsHandler : IQueryHandler<GetDataSetItems, GetDataSetItemsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetDataSetItemsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetDataSetItemsResult[]> Handle(GetDataSetItems queryObject)
        {
            var qDataSetItems = from dataSetItem in _dbContext.Set<DataSetItemData>()
                                join dataSet in _dbContext.Set<DataSetData>()
                                on dataSetItem.DataSetId equals dataSet.Id
                                where queryObject.DataSetIds.Contains(dataSet.Id)
                                select new
                                {
                                    dataSetItem.Id,
                                    dataSetItem.DataSetId,
                                    dataSet.Title,
                                    dataSetItem.Row
                                };
            var rows = qDataSetItems.GroupBy(ct => ct.DataSetId).ToList();
            var result = rows.Select(dataSetItem => new GetDataSetItemsResult(dataSetItem.Key, dataSetItem.FirstOrDefault().Title, dataSetItem.Select(ct => ct.Row).ToArray())).ToArray();

            return new QueryResponse<GetDataSetItemsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No Data-Set-Items found."));
        }

        public bool Validate(GetDataSetItems query)
        {
            if (query.DataSetIds.Count() == 0)
            {
                Errors.Add("DataSetIds cannot be zero.");
                return false;
            }
            if (query.DataSetIds.Any(ct => ct == Guid.Empty))
            {
                Errors.Add("DataSetIds cannot be empty.");
                return false;
            }
            return true;
        }
    }
}