﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.DataModel;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Queries.DataModule.Data.DataSets;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.DataModule.DataSets
{
    public sealed class GetDataSetsHandler : IQueryHandler<GetDataSets, GetDataSetsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetDataSetsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetDataSetsResult[]> Handle(GetDataSets queryObject)
        {
            var qDataSets = from dataset in _dbContext.Set<DataSetData>().AsNoTracking()
                            where dataset.SubProjectId == queryObject.SubProjectId
                            && dataset.TypicalId == queryObject.TypicalId
                            select new
                            {
                                dataset.Id,
                                dataset.Title,
                                dataset.Coverage,
                                dataset.Combinations
                            };

            if (!qDataSets.Any())
                return new QueryResponse<GetDataSetsResult[]>(null, new QueryResponseMessage((int)MessageType.Information, $"No Data Sets found."));

            var dsIds = qDataSets.Select(ct => ct.Id).ToArray();

            var rows = from datasetItem in _dbContext.Set<DataSetItemData>().AsNoTracking()
                       where dsIds.Contains(datasetItem.DataSetId)
                       group datasetItem by datasetItem.DataSetId into datasetItems
                       select new
                       {
                           DataSetId = datasetItems.Key,
                           NrRows = datasetItems.Count()
                       };

            var result = qDataSets.Select(dataset => new GetDataSetsResult(dataset.Id, dataset.Title, dataset.Combinations == "",
                dataset.Coverage, rows.FirstOrDefault(ct => ct.DataSetId == dataset.Id) == null ? 0 : rows.FirstOrDefault(ct => ct.DataSetId == dataset.Id).NrRows)).ToArray();

            return new QueryResponse<GetDataSetsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Data Sets found."));
        }

        public bool Validate(GetDataSets query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (query.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}