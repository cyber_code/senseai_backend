﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SenseAI.Domain;
using Messaging.Queries;
using Persistence.DataModel;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Queries.DataModule.Data.DataSets;
using Queries.DataModule.DataSets;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Application.Queries.DataModule.DataSets
{
    public sealed class GetDataSetHandler : IQueryHandler<GetDataSet, GetDataSetResult>
    {
        private readonly IDbContext _dbContext;

        public GetDataSetHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetDataSetResult> Handle(GetDataSet queryObject)
        {
            var qDataset = (from ds in _dbContext.Set<DataSetData>().AsNoTracking()
                            where ds.Id == queryObject.Id
                            select new
                            {
                                ds.Id,
                                ds.Title,
                                ds.Coverage,
                                ds.Combinations,
                                ds.TypicalId,
                            }).FirstOrDefault();

            if (qDataset == null)
                return new QueryResponse<GetDataSetResult>(null, new QueryResponseMessage((int)MessageType.Information, $"No Data Set found."));

            var qAttributes = (from typical in _dbContext.Set<TypicalData>().AsNoTracking()
                               where typical.Id == qDataset.TypicalId
                               select new
                               {
                                   typical.Id,
                                   typical.Title,
                                   typical.Json
                               }).FirstOrDefault();

            if (qDataset == null)
                return new QueryResponse<GetDataSetResult>(null, new QueryResponseMessage((int)MessageType.Information, $"No Typical found."));

            MandatoryField mandatoryField = (MandatoryField)queryObject.Mandatory;
            var attributes = JsonConvert.DeserializeObject<GetTypicalAttributesResult>(qAttributes.Json.ToString()).Attributes;
            if (mandatoryField != MandatoryField.All)
                attributes = attributes.Where(a => a.IsRequired == (mandatoryField == MandatoryField.Mandatory)).ToArray();

            Combinations[] combinations = null;
            if (!string.IsNullOrEmpty(qDataset.Combinations))
                combinations = JsonConvert.DeserializeObject<Combinations[]>(qDataset.Combinations);
            var result = new GetDataSetResult(qDataset.Id, qDataset.Title, qDataset.TypicalId, qDataset.Coverage, combinations, attributes);

            return new QueryResponse<GetDataSetResult>(result, result is GetDataSetResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Data Set found."));
        }

        public bool Validate(GetDataSet query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("DataSetId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}