﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DataModule.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.DataModule.Data
{
    public sealed class GetWorkflowPathItemIdsHandler : IQueryHandler<GetWorkflowPathItemIds, Guid[]>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowPathItemIdsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<Guid[]> Handle(GetWorkflowPathItemIds queryObject)
        {
            var result = from workflowPathItem in _dbContext.Set<WorkflowVersionPathDetailsData>().AsNoTracking()
                         where workflowPathItem.PathId == queryObject.WorkflowPathId 
                         orderby workflowPathItem.Index
                         select workflowPathItem.Id;

            return new QueryResponse<Guid[]>(result.ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Workflow Path Item found with WorkflowPathId: '{queryObject.WorkflowPathId}'."));
        }

        public bool Validate(GetWorkflowPathItemIds query)
        {
            if (query.WorkflowPathId == Guid.Empty)
            {
                Errors.Add("WorkflowPathId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}