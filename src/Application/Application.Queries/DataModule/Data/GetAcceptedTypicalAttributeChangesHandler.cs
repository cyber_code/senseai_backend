﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SenseAI.Domain.DataModel;
using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.DataModule.Data
{
    public sealed class GetAcceptedTypicalAttributeChangesHandler : IQueryHandler<GetAcceptedTypicalAttributeChanges, GetAcceptedTypicalAttributeChangesResult>
    {
        private readonly IDbContext _dbContext;

        public GetAcceptedTypicalAttributeChangesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetAcceptedTypicalAttributeChangesResult> Handle(GetAcceptedTypicalAttributeChanges queryObject)
        {
            var acceptedTypicalChange = (from typical in _dbContext.Set<AcceptedTypicalChangesData>()
                                         where typical.Id == queryObject.Id
                                         select typical).AsNoTracking().FirstOrDefault();

            var newTypicalAttributes = JsonConvert.DeserializeObject<TypicalChangesMetadata>(acceptedTypicalChange.NewJson).Attributes;
            var oldTypicalAttributes = JsonConvert.DeserializeObject<TypicalChangesMetadata>(acceptedTypicalChange.OldJson).Attributes;

            var newAttributes = newTypicalAttributes.Except(oldTypicalAttributes, new AttributeNameComparer()).ToArray();
            var deleteAttributes = oldTypicalAttributes.Except(newTypicalAttributes, new AttributeNameComparer()).ToArray();
            var changedAttributes = newTypicalAttributes.Except(oldTypicalAttributes, new AttributeComparer()).Except(newAttributes, new AttributeNameComparer()).ToArray();
            var result = new GetAcceptedTypicalAttributeChangesResult(newAttributes, deleteAttributes, changedAttributes);

            return new QueryResponse<GetAcceptedTypicalAttributeChangesResult>(result, result is GetAcceptedTypicalAttributeChangesResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Accepted Typical Attribute Changes found with Id: '{queryObject.Id}'."));
        }

        public bool Validate(GetAcceptedTypicalAttributeChanges query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}