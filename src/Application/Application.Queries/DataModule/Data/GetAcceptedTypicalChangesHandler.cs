﻿using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using SenseAI.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.Data
{
    public sealed class GetAcceptedTypicalChangesHandler : IQueryHandler<GetAcceptedTypicalChanges, GetAcceptedTypicalChangesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetAcceptedTypicalChangesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetAcceptedTypicalChangesResult[]> Handle(GetAcceptedTypicalChanges queryObject)
        {
            var query = from acceptedtypical in _dbContext.Set<AcceptedTypicalChangesData>()
                        select new AcceptedTypicalChangesData
                        {
                            Id = acceptedtypical.Id,
                            TypicalId = acceptedtypical.Id,
                            TypicalName = acceptedtypical.TypicalName,
                        };

            var result = query.Select(acceptedtypical => new GetAcceptedTypicalChangesResult(acceptedtypical.Id, acceptedtypical.TypicalId, acceptedtypical.TypicalName)).ToArray();

            return new QueryResponse<GetAcceptedTypicalChangesResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No Accepted Typical Changes found."));
        }

        public bool Validate(GetAcceptedTypicalChanges query)
        {
            return true;
        }
    }
}