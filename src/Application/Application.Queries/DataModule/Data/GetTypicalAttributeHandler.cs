﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using SenseAI.Domain;
using SenseAI.Domain.DataModel;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.Data
{
    public class GetTypicalAttributeHandler : IQueryHandler<GetTypicalAttribute, GetTypicalAttributeResult>
    {
        private readonly IDbContext _dbContext;

        public GetTypicalAttributeHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTypicalAttributeResult> Handle(GetTypicalAttribute queryObject)
        {
            var actualTypical = (from typical in _dbContext.Set<TypicalData>()
                                 where typical.Id == queryObject.TypicalId
                                 select typical).AsNoTracking().FirstOrDefault();

            var typicalAttributes = JsonConvert.DeserializeObject<TypicalChangesMetadata>(actualTypical.Json).Attributes;
            var result = typicalAttributes
                .Select(attribute => new GetTypicalAttributeResult(actualTypical.Id, attribute.Name, attribute.IsMultiValue, attribute.PossibleValues, attribute.IsRequired, attribute.IsNoInput, attribute.IsNoChange, attribute.IsExternal, attribute.MinimumLength, attribute.MaximumLength, attribute.RelatedApplicationName, attribute.ExtraData, attribute.RelatedApplicationIsConfiguration))
                .FirstOrDefault(x => x.Name == queryObject.Name);

            return new QueryResponse<GetTypicalAttributeResult>(result, result is GetTypicalAttributeResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Attribute found."));
        }

        public bool Validate(GetTypicalAttribute query)
        {

            if (query.TypicalId == null)
            {
                Errors.Add("Typical Id cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(query.Name))
            {
                Errors.Add("Name cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
