﻿using SenseAI.Domain;
using Messaging.Queries;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using System.Linq;
using System.Collections.Generic;
using Persistence.SystemCatalogModel;
using System;
using Microsoft.EntityFrameworkCore;

namespace Application.Queries.DataModule.Data
{
    public class GetNavigationsHandler : IQueryHandler<GetNavigations, GetNavigationsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetNavigationsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetNavigationsResult[]> Handle(GetNavigations queryObject)
        {
            var qCatalog = (from sysCatalog in _dbContext.Set<SystemCatalogData>().AsNoTracking() join 
                            catalog in _dbContext.Set<CatalogData>().AsNoTracking() on sysCatalog.CatalogId equals catalog.Id
                            where sysCatalog.SubprojectId == queryObject.SubProjectId && sysCatalog.SystemId == queryObject.SystemId && 
                                    catalog.Type == (int)TypicalType.Application
                            select new
                            {
                                catalog.Id
                                
                            }).FirstOrDefault();

            if (qCatalog == null)
                return new QueryResponse<GetNavigationsResult[]>(null, new QueryResponseMessage((int)MessageType.Information, $"No Catalog Data found."));

            var query = from navigation in _dbContext.Set<NavigationData>()
                        where navigation.CatalogId == qCatalog.Id
                        select new
                        {
                            navigation.Id,
                            navigation.Name,
                            navigation.Url,
                            navigation.ParentId
                        };

            var result = query.Select(navigation => new GetNavigationsResult(navigation.Id, navigation.Name, navigation.Url.Trim(), navigation.ParentId)).ToArray();

            return new QueryResponse<GetNavigationsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Navigations found."));
        }

        public bool Validate(GetNavigations query)
        {
            if (query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (query.SystemId == null || query.SystemId.Equals(Guid.Empty))
            {
                Errors.Add("SystemId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}