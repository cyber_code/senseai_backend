﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;

namespace Application.Queries.DataModule.Data
{
    public sealed class GetTypicalChangesHandler : IQueryHandler<GetTypicalChanges, GetTypicalChangesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetTypicalChangesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTypicalChangesResult[]> Handle(GetTypicalChanges queryObject)
        {
            var query = from typical in _dbContext.Set<TypicalChangesData>().AsNoTracking()
                        select new TypicalChangesData
                        {
                            Id = typical.Id,
                            Title = typical.Title,
                            Type = typical.Type,
                            Status = typical.Status
                        };

            var result = query.Select(typical => new GetTypicalChangesResult(typical.Id, typical.Title, (SenseAI.Domain.TypicalType)typical.Type, typical.Status)).ToArray();

            return new QueryResponse<GetTypicalChangesResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No Typical Changes found."));
        }

        public bool Validate(GetTypicalChanges query)
        {
            return true;
        }
    }
}