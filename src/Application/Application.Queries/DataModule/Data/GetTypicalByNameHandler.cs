﻿using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using SenseAI.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.Data
{
    public class GetTypicalByNameHandler : IQueryHandler<GetTypicalByName, GetTypicalResult>
    {
        private readonly IDbContext _dbContext;

        public GetTypicalByNameHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTypicalResult> Handle(GetTypicalByName queryObject)
        {
            var query = from typical in _dbContext.Set<TypicalData>()
                        where (int)typical.Type == queryObject.Type && typical.Title == queryObject.Name
                        select new
                        {
                            typical.Id,
                            typical.Title,
                            typical.Json,
                            typical.Type
                        };

            var result = query.Select(typical => new GetTypicalResult(typical.Id, typical.Title, typical.Json, typical.Type)).FirstOrDefault();

            return new QueryResponse<GetTypicalResult>(result, result is GetTypicalResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Typical found."));
        }

        public bool Validate(GetTypicalByName query)
        {//TODO: VALIDATE query.Type
            if (string.IsNullOrWhiteSpace(query.Name))
            {
                Errors.Add("Name cannot be empty.");
                return false;
            }
            return true;
        }
    }
}