﻿using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using SenseAI.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.Data
{
    public sealed class GetTypicalsByTypeHandler : IQueryHandler<GetTypicalsByType, GetTypicalsByTypeResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetTypicalsByTypeHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTypicalsByTypeResult[]> Handle(GetTypicalsByType queryObject)
        {
            var query = from typical in _dbContext.Set<TypicalData>()
                        where typical.Type == queryObject.Type //TODO: validate queryObject.Type
                        select new
                        {
                            typical.Id,
                            typical.Title,
                            typical.Type
                        };

            var result = query.Select(typical => new GetTypicalsByTypeResult(typical.Id, typical.Title, typical.Type)).OrderBy(ct => ct.Title).ToArray();

            return new QueryResponse<GetTypicalsByTypeResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Typical found with Type you specified."));
        }

        public bool Validate(GetTypicalsByType query)
        {
            return true;
        }
    }
}