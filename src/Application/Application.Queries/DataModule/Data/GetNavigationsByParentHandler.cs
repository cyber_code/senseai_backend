﻿using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.Data
{
    public class GetNavigationsByParentHandler : IQueryHandler<GetNavigationsByParent, GetNavigationsByParentResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetNavigationsByParentHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetNavigationsByParentResult[]> Handle(GetNavigationsByParent queryObject)
        {
            var query = from navigation in _dbContext.Set<NavigationData>()
                        where navigation.ParentId == queryObject.ParentId
                        select new
                        {
                            navigation.Id,
                            navigation.Name,
                            navigation.Url,
                            navigation.ParentId
                        };

            var result = query.Select(navigation => new GetNavigationsByParentResult(navigation.Id, navigation.Name, navigation.Url, navigation.ParentId)).ToArray();

            return new QueryResponse<GetNavigationsByParentResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Navigations found."));
        }

        public bool Validate(GetNavigationsByParent query)
        {
            if (query.ParentId == Guid.Empty)
            {
                Errors.Add("ParentId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}