﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DataModule.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.DataModule.Data
{
    public sealed class GetWorkflowPathItemsHandler : IQueryHandler<GetWorkflowPathItems, GetWorkflowPathItemsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowPathItemsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowPathItemsResult[]> Handle(GetWorkflowPathItems queryObject)
        {
            var result = from workflowPathItem in _dbContext.Set<WorkflowVersionPathDetailsData>().AsNoTracking()
                         where workflowPathItem.PathId == queryObject.WorkflowPathId
                         && (workflowPathItem.Type == 1 || workflowPathItem.Type == 4)
                         orderby workflowPathItem.Index
                         select new GetWorkflowPathItemsResult(
                              workflowPathItem.WorkflowId,
                              workflowPathItem.PathId,
                            workflowPathItem.Id,
                            workflowPathItem.Title,
                            workflowPathItem.CatalogId,
                            workflowPathItem.TypicalId,
                            workflowPathItem.TypicalName,
                            workflowPathItem.TypicalType,
                            workflowPathItem.DataSetId,
                            workflowPathItem.ActionType,
                            workflowPathItem.Index
                         );

            return new QueryResponse<GetWorkflowPathItemsResult[]>(result.ToArray(), result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Workflow Path Data found with WorkflowPathId: '{queryObject.WorkflowPathId}'."));
        }

        public bool Validate(GetWorkflowPathItems query)
        {
            if (query.WorkflowPathId == Guid.Empty)
            {
                Errors.Add("WorkflowPathId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}