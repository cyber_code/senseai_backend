﻿using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using SenseAI.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.Data
{
    public class GetProductionDatasHandler : IQueryHandler<GetProductionDatas, GetProductionDatasResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetProductionDatasHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetProductionDatasResult[]> Handle(GetProductionDatas queryObject)
        {
            var query = from productiondata in _dbContext.Set<ProductionDataData>()
                        select new
                        {
                            productiondata.Id,
                            productiondata.CatalogId,
                            productiondata.Json
                        };

            var result = query.Select(productiondata => new GetProductionDatasResult(productiondata.Id, productiondata.CatalogId, productiondata.Json)).ToArray();

            return new QueryResponse<GetProductionDatasResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No Production Data found."));
        }

        public bool Validate(GetProductionDatas query)
        {
            return true;
        }
    }
}