﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DataModule.Data;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.Data
{
    public sealed class GetWorkflowPathsHandler : IQueryHandler<GetWorkflowPaths, GetWorkflowPathsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowPathsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowPathsResult[]> Handle(GetWorkflowPaths queryObject)
        {
            var qWorkflowPaths = from workflowPath in _dbContext.Set<WorkflowVersionPathsData>()
                                 where workflowPath.WorkflowId == queryObject.WorkflowId && workflowPath.Selected == true && workflowPath.IsLast
                                 orderby workflowPath.Coverage, workflowPath.Title descending
                                 select new
                                 {
                                     workflowPath.Id,
                                     workflowPath.Title,
                                     workflowPath.Coverage
                                 };

            var result = qWorkflowPaths.Select(workflowPath => new GetWorkflowPathsResult(workflowPath.Id, workflowPath.Title, workflowPath.Coverage == 0 ? "N/A" : workflowPath.Coverage.ToString() + "%")).ToArray();

            return new QueryResponse<GetWorkflowPathsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Workflow Path Data found with WorkflowId: '{queryObject.WorkflowId}'."));
        }

        public bool Validate(GetWorkflowPaths query)
        {
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}