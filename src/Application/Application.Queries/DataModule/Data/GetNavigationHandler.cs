﻿using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.Data
{
    public class GetNavigationHandler : IQueryHandler<GetNavigation, GetNavigationResult>
    {
        private readonly IDbContext _dbContext;

        public GetNavigationHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetNavigationResult> Handle(GetNavigation queryObject)
        {
            var query = from navigation in _dbContext.Set<NavigationData>()
                        where navigation.Id == queryObject.Id
                        select new
                        {
                            navigation.Id,
                            navigation.Name,
                            navigation.Url,
                            navigation.ParentId
                        };

            var result = query.Select(navigation => new GetNavigationResult(navigation.Id, navigation.Name, navigation.Url, navigation.ParentId)).FirstOrDefault();

            return new QueryResponse<GetNavigationResult>(result, result is GetNavigationResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Navigation found.") );
        }

        public bool Validate(GetNavigation query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}