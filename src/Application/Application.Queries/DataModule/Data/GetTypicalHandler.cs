﻿using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.Data
{
    public class GetTypicalHandler : IQueryHandler<GetTypical, GetTypicalResult>
    {
        private readonly IDbContext _dbContext;

        public GetTypicalHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTypicalResult> Handle(GetTypical queryObject)
        {
            var query = from typical in _dbContext.Set<TypicalData>()
                        where typical.Id == queryObject.Id
                        select new
                        {
                            typical.Id,
                            typical.Title,
                            typical.Json,
                            typical.Type
                        };

            var result = query.Select(typical => new GetTypicalResult(typical.Id, typical.Title, typical.Json, typical.Type)).FirstOrDefault();

            return new QueryResponse<GetTypicalResult>(result, result is GetTypicalResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Typical found."));
        }

        public bool Validate(GetTypical query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}