﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Queries;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Queries.DataModule.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.DataModule.Data
{
    public sealed class GetTypicalAttributeChangesHandler : IQueryHandler<GetTypicalAttributeChanges, GetTypicalAttributeChangesResult>
    {
        private readonly IDbContext _dbContext;

        public GetTypicalAttributeChangesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetTypicalAttributeChangesResult> Handle(GetTypicalAttributeChanges queryObject)
        {
            var typicalChange = (from typical in _dbContext.Set<TypicalChangesData>()
                                 where typical.Id == queryObject.Id
                                 select typical).AsNoTracking().FirstOrDefault();

            if (typicalChange == null)
                return new QueryResponse<GetTypicalAttributeChangesResult>(null, new QueryResponseMessage((int)MessageType.Information, $"No Typical-Change found."));

            var actualTypical = (from typical in _dbContext.Set<TypicalData>()
                                 where typical.Title == typicalChange.Title
                                 && typical.CatalogId == typicalChange.CatalogId
                                 select typical).AsNoTracking().FirstOrDefault();

            if (actualTypical == null)
            {
                return new QueryResponse<GetTypicalAttributeChangesResult>(null, new QueryResponseMessage((int)MessageType.Information, $"This change is not correct. No typical found."));
            }

            var actualCatalog = (from catalog in _dbContext.Set<CatalogData>()
                                 where catalog.Id == typicalChange.CatalogId
                                 select catalog).AsNoTracking().FirstOrDefault();

            if (actualCatalog == null)
                return new QueryResponse<GetTypicalAttributeChangesResult>(null, new QueryResponseMessage((int)MessageType.Information, $"No Catalog found."));

            var listOfAffectedWorkflowsQuery = (from workflow in _dbContext.Set<WorkflowData>()
                                                join workflowitem in _dbContext.Set<WorkflowItemData>() on workflow.Id equals workflowitem.WorkflowId
                                                where workflowitem.TypicalTitle == typicalChange.Title
                                             && workflowitem.CatalogTitle == actualCatalog.Title
                                                select workflow).AsNoTracking().Distinct().ToList();
            var listOfAffectedWorkflows = listOfAffectedWorkflowsQuery.OrderBy(ab => ab.Title).Select(
               workflow => new Workflow(
                   workflow.Id,
                   workflow.Title,
                   workflow.Description,
                   workflow.FolderId,
                   workflow.IsValid,
                   workflow.IsParsed,
                   workflow.isIssued,
                   workflow.generatedTestCases
               )
           ).ToArray();

            var typicalChangeAttributes = JsonConvert.DeserializeObject<TypicalChangesMetadata>(typicalChange.Json).Attributes;
            var typicalAttributes = JsonConvert.DeserializeObject<TypicalChangesMetadata>(actualTypical.Json).Attributes;

            if (typicalAttributes == null)
                return new QueryResponse<GetTypicalAttributeChangesResult>(null, new QueryResponseMessage((int)MessageType.Failed, $"No Attribute found."));

            var newAttributes = typicalChangeAttributes.Except(typicalAttributes, new AttributeNameComparer()).ToArray();
            var deleteAttributes = typicalAttributes.Except(typicalChangeAttributes, new AttributeNameComparer()).ToArray();
            var changedAttributes = typicalChangeAttributes.Except(typicalAttributes, new AttributeComparer()).Except(newAttributes, new AttributeNameComparer()).ToArray();
            var result = new GetTypicalAttributeChangesResult(newAttributes, deleteAttributes, changedAttributes, listOfAffectedWorkflows);

            return new QueryResponse<GetTypicalAttributeChangesResult>(result, result is GetTypicalAttributeChangesResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Typical Attribute Changes found."));
        }

        public bool Validate(GetTypicalAttributeChanges query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}