﻿using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Data;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.DataModule.Data
{
    public class GetProductionDataHandler : IQueryHandler<GetProductionData, GetProductionDataResult>
    {
        private readonly IDbContext _dbContext;

        public GetProductionDataHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetProductionDataResult> Handle(GetProductionData queryObject)
        {
            var query = from productiondata in _dbContext.Set<ProductionDataData>()
                        where productiondata.Id == queryObject.Id
                        select new
                        {
                            productiondata.Id,
                            productiondata.CatalogId,
                            productiondata.Json
                        };

            var result = query.Select(productiondata => new GetProductionDataResult(productiondata.Id, productiondata.CatalogId, productiondata.Json)).FirstOrDefault();

            return new QueryResponse<GetProductionDataResult>(result, result is GetProductionDataResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No Production Data found."));
        }

        public bool Validate(GetProductionData query)
        {
            if (query.Id ==0)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}