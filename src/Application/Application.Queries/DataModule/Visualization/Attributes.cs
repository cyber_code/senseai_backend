﻿namespace Application.Queries.DataModule.Visualization
{
    public sealed class Attributes
    {
        public string Name { get; set; }

        public string[] PossibleValues { get; set; }

        public bool IsRequired { get; set; }

        public bool IsNoInput { get; set; }

        public bool IsNoChange { get; set; }

        public bool IsExternal { get; set; }

        public int MinimumLength { get; set; }

        public int MaximumLength { get; set; }

        public string RelatedApplicationName { get; set; }

        public string ExtraData { get; set; }
    }
}