﻿using Messaging.Queries;
using Persistence.DataModel;
using Persistence.Internal;
using Queries.DataModule.Visualization;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using SenseAI.Domain;

namespace Application.Queries.DataModule.Visualization
{
    public sealed class ModelVisualizationHandler : IQueryHandler<GetModelVisualization, GetModelVisualizationResult>
    {
        private readonly IDbContext _dbContext;

        public ModelVisualizationHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetModelVisualizationResult> Handle(GetModelVisualization queryObject)
        {
            DFS dfs = new DFS(_dbContext, queryObject.TypicalName, queryObject.Level);

            return new QueryResponse<GetModelVisualizationResult>(dfs.result, dfs.result != null ? null : new QueryResponseMessage((int)MessageType.Information, $"No Model Visualization found with TypicalName: '{queryObject.TypicalName}'."));
        }

        public bool Validate(GetModelVisualization query)
        {
            if (string.IsNullOrWhiteSpace(query.TypicalName))
            {
                Errors.Add("Typical name cannot be empty.");
                return false;
            }
            return true;
        }
    }

    public class DFS
    {
        public GetModelVisualizationResult result;
        private readonly IDbContext _dbContext;
        private readonly Random random = new Random();
        private readonly List<TypicalData> _madeUpTypicals = new List<TypicalData>();
        private readonly int _globalLevel;

        public DFS(IDbContext dbContext, string typicalName, int level)
        {
            _dbContext = dbContext;
            result = new GetModelVisualizationResult(new List<AssossiationItems>(), new List<AssossiationLinks>());
            _globalLevel = level;
            //if (GetTypicalType(typicalName) == TypicalType.AAAplication)
            //{
            //    if (level < 2)
            //        _globalLevel = 2;
            //}
            RecursiveSearch(typicalName, _globalLevel);
            //Search(firstTypical, level);
        }

        public void RecursiveSearch(string typicalNameToSearch, int level)
        {
            try
            {
                var typicalUncoverted = GetTypical(typicalNameToSearch);

                if (typicalUncoverted != null)
                {
                    var attributes = new List<string>();

                    var typical = JsonConvert.DeserializeObject<TypicalJson>(typicalUncoverted.Json);

                    attributes.AddRange(
                        typical.Attributes
                            .Where(attribute => !attribute.Name.StartsWith("{"))
                            .Select(attribute => attribute.RelatedApplicationName)
                            .Where(attributeName => !string.IsNullOrWhiteSpace(attributeName))
                            .Distinct()
                    );

                    if (typical.Type == (int)TypicalType.AAAplication)
                    {
                        attributes.AddRange(
                            typical.Attributes
                                .Where(attribute => attribute.Name.StartsWith("{"))
                                .Select(attribute => attribute.Name.Substring(0, attribute.Name.LastIndexOf('}')).Substring(1))
                                .Distinct()
                        );

                        _madeUpTypicals.AddRange(
                            typical.Attributes
                                .Where(attribute => attribute.Name.StartsWith("{"))
                                .GroupBy(attribute => attribute.Name.Substring(0, attribute.Name.LastIndexOf('}')).Substring(1))
                                .Select(
                                    item => new TypicalData
                                    {
                                        Title = item.Key,
                                        Json = JsonConvert.SerializeObject(
                                            new TypicalJson
                                            {
                                                Title = item.Key,
                                                HasAutomaticId = false,
                                                IsConfiguration = false,
                                                Type = (int)TypicalType.Application,
                                                Attributes = item.Select(
                                                attribute =>
                                                {
                                                    attribute.Name = attribute.Name.Substring(attribute.Name.LastIndexOf('}'));

                                                    return attribute;
                                                }
                                            )
                                            .ToList()
                                            }
                                        )
                                    }
                                )
                        );
                    }

                    if (result.Items.Any())
                    {
                        if (!result.Items.Where(ct => ct.Text == typical.Title).Any())
                        {
                            var color = String.Format("#{0:X6}", random.Next(0x1000000)); // = "#A197B9"
                            result.Items.Add(new AssossiationItems(Guid.NewGuid(), typical.Title, color));
                        }
                    }
                    else
                    {
                        var color = String.Format("#{0:X6}", random.Next(0x1000000));
                        result.Items.Add(new AssossiationItems(Guid.NewGuid(), typical.Title, color));
                    }

                    Guid From = result.Items.Where(ct => ct.Text == typical.Title).Select(c => c.Key).FirstOrDefault();

                    foreach (var attribute in attributes)
                    {
                        if (!result.Items.Where(ct => ct.Text == attribute).Any())
                        {
                            var colori = String.Format("#{0:X6}", random.Next(0x1000000));
                            result.Items.Add(new AssossiationItems(Guid.NewGuid(), attribute, colori));
                        }

                        Guid To = result.Items.Where(ct => ct.Text == attribute).Select(c => c.Key).FirstOrDefault();
                        var color = String.Format("#{0:X6}", random.Next(0x1000000));
                        result.Links.Add(new AssossiationLinks(From, To, color));

                        if (level > 1)
                        {
                            RecursiveSearch(attribute, level - 1);
                        }
                    }
                }
            }
            catch (Exception)
            {
                //
            }
        }

        private TypicalData GetTypical(string typicalName)
        {
            return
                _dbContext.Set<TypicalData>().FirstOrDefault(typical => typical.Title == typicalName) ??
                _madeUpTypicals.FirstOrDefault(typical => typical.Title == typicalName);
        }

        //private TypicalType GetTypicalType(string typicalName)
        //{
        //    return
        //        _dbContext.Set<TypicalData>().FirstOrDefault(typical => typical.Title == typicalName).Type;
        //}
    }
}