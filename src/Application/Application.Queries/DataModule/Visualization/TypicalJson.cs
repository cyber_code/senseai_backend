﻿using System.Collections.Generic;

namespace Application.Queries.DataModule.Visualization
{
    public sealed class TypicalJson
    {
        public int Type { get; set; }

        public string Title { get; set; }

        public bool HasAutomaticId { get; set; }

        public bool IsConfiguration { get; set; }

        public List<Attributes> Attributes { get; set; }
    }
}