﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using Persistence.Process.IssuesModel;
using Persistence.ReportsModel;
using Queries.ReportModule.TraceabilityReport;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain.AdminModel;

namespace Application.Queries.ReportModule.TraceabilityReport
{
    public sealed class GetTraceabilityReportHandler : IQueryHandler<GetTraceabilityReport, GetTraceabilityReportResult>
    {
        private readonly IDbContext _dbContext;

        public List<string> Errors { get; set; }

        public GetTraceabilityReportHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetTraceabilityReportResult> Handle(GetTraceabilityReport queryObject)
        {
            var zeroGuid = Guid.Parse("00000000-0000-0000-0000-000000000000");

            var id = queryObject.Id ?? zeroGuid;

            var nextLevel = GetNextLevel(queryObject, zeroGuid, id);

            string sql = String.Empty;

            string issuesGraphsql = String.Empty;

            string executionGraphsql = String.Empty;

            int openIssuesStatus = 2;

            int successfulExecutionStatus = 2;

            var todayDate = DateTime.Now;

            var date = DateTime.Now.AddDays(-5);

            if (queryObject.IssueTypes == null || !queryObject.IssueTypes.Any())
            {
                //queryObject.IssueTypes = _dbContext.Set<IssueTypeData>().Where(item => item.SubProjectId == queryObject.SubProjectId).Select(item => item.Id.ToString()).ToArray();
            }
            else
            {
                queryObject.IssueTypes = queryObject.IssueTypes[0].Split(',');
            }

            if (queryObject.Level == TraceablityReportLevel.Hierarchy)
            {
                sql = GetHierarchiesSql(queryObject.SubProjectId, id, openIssuesStatus, successfulExecutionStatus, queryObject.IssueTypes, todayDate);

                issuesGraphsql = GetOpenIssuesGraphSql(queryObject.SubProjectId, id, queryObject.IssueTypes, date);

                executionGraphsql = GetExecutionsGraphSql(queryObject.SubProjectId, id, successfulExecutionStatus, date);
            }
            else if (queryObject.Level == TraceablityReportLevel.Process)
            {
                sql = GetProcessesSql(queryObject.SubProjectId, id, openIssuesStatus, successfulExecutionStatus, queryObject.IssueTypes, todayDate);

                issuesGraphsql = GetOpenIssuesGraphOfGetProcessesSql(id, queryObject.IssueTypes, date);

                executionGraphsql = GetExecutionsGraphOfGetProcessesSql(id, successfulExecutionStatus, date);
            }
            else
            {
                sql = GetWorkflowsSql(queryObject.SubProjectId, id, openIssuesStatus, successfulExecutionStatus, queryObject.IssueTypes, todayDate);

                issuesGraphsql = GetOpenIssuesGraphOfGetWorkflowsSql(id, queryObject.IssueTypes, date);

                executionGraphsql = GetExecutionsGraphOfGetWorkflowsSql(id, successfulExecutionStatus, date);
            }

            var lines = _dbContext.QueryFromSql<TraceabilityReportLine>(sql).ToArray().Select(
                line => new GetTraceabilityReportResultLine
                {
                    Id = line.Id,
                    Title = line.Title,
                    Processes = line.Processes,
                    Workflows = line.Workflows,
                    TestCases = line.TestCases,
                    PlannedTestCases = line.PlannedTestCases,
                    OpenIssues = line.OpenIssues,
                    FailedExecutions = line.FailedExecutions,
                    SuccessfulExecutions = line.SuccessfulExecutions
                }
            ).ToArray();

            GetTraceablityReportResultExecutionLine[] executionLines = new GetTraceablityReportResultExecutionLine[] { };

            if (executionGraphsql != String.Empty)
            {
                executionLines = _dbContext.QueryFromSql<TraceabilityReportExecutionLine>(executionGraphsql).ToArray().Select(
                    line => new GetTraceablityReportResultExecutionLine
                    {
                        Id = line.Id,
                        Day = line.Day,
                        SuccessfulExecutions = line.SuccessfulExecutions,
                        FailedExecutions = line.FailedExecutions
                    }
                ).ToArray();
            }

            GetTraceablityReportResultIssuesLine[] issueLines = new GetTraceablityReportResultIssuesLine[] { };

            if (issuesGraphsql != String.Empty)
            {
                issueLines = _dbContext.QueryFromSql<TraceabilityReportIssuesLine>(issuesGraphsql).ToArray().Select(
                    line => new GetTraceablityReportResultIssuesLine
                    {
                        Id = line.Id,
                        Date = line.Date,
                        Status = line.Status,
                        Count = line.Count
                    }
                ).ToArray();
            }

            var result = new GetTraceabilityReportResult
            {
                NextLevel = nextLevel,
                Lines = lines,
                ExecutionLines = executionLines,
                IssuesLines = issueLines,
            };

            return new QueryResponse<GetTraceabilityReportResult>(result);
        }

        private TraceablityReportLevel GetNextLevel(GetTraceabilityReport queryObject, Guid zeroGuid, Guid id)
        {
            var nextLevel = TraceablityReportLevel.Hierarchy;

            if (id == zeroGuid)
            {
                var hierarchyTypeId = _dbContext.Set<HierarchyData>().Where(item => item.ParentId == zeroGuid).Select(item => item.HierarchyTypeId).FirstOrDefault();
                var hierarchyTypeLevel = _dbContext.Set<HierarchyTypeData>().Where(item => item.Id == hierarchyTypeId).Select(item => item.Level).FirstOrDefault();
                var maxHierarchyTypeLevel = _dbContext.Set<HierarchyTypeData>().Where(item => item.SubProjectId == queryObject.SubProjectId).Select(item => item.Level).Max();

                if (hierarchyTypeLevel == maxHierarchyTypeLevel)
                {
                    nextLevel = TraceablityReportLevel.Process;
                }
            }
            else
            {
                if (queryObject.Level == TraceablityReportLevel.Process)
                {
                    nextLevel = TraceablityReportLevel.Workflow;
                }
                else if (queryObject.Level == TraceablityReportLevel.Workflow)
                {
                    nextLevel = TraceablityReportLevel.End;
                }
                else
                {
                    var hierarchyTypeId = _dbContext.Set<HierarchyData>().Where(item => item.Id == id).Select(item => item.HierarchyTypeId).FirstOrDefault();
                    var hierarchyTypeLevel = _dbContext.Set<HierarchyTypeData>().Where(item => item.Id == hierarchyTypeId).Select(item => item.Level).FirstOrDefault();
                    var maxHierarchyTypeLevel = _dbContext.Set<HierarchyTypeData>().Where(item => item.SubProjectId == queryObject.SubProjectId).Select(item => item.Level).Max();

                    if (hierarchyTypeLevel == maxHierarchyTypeLevel - 1)
                    {
                        nextLevel = TraceablityReportLevel.Process;
                    }
                }
            }

            return nextLevel;
        }

        // This lists the Hierarchies based on a parent Hierarchy.
        private static string GetHierarchiesSql(Guid subProjectId, Guid hierarchyId, int openIssuesStatus, int successfulExecutionStatus, string[] issueTypes, DateTime date)
        {
            string sqlInClause = String.Empty;

            if (issueTypes != null)
            {
                sqlInClause = string.Format(" AND i.IssueTypeId IN ('{0}')", string.Join("', '", issueTypes));
            }

            return $@" 
                    DECLARE @SubProjectId UNIQUEIDENTIFIER
                    SET @SubProjectId = '{subProjectId}'

                    ;WITH NewHierarchies AS (
	                    SELECT th.Id, th.ParentId, th.Id AS TopParentId, CAST(th.Title AS VARCHAR(MAX)) TitlePath
	                    FROM Hierarchies th
	                    WHERE 
		                    th.SubProjectId = @SubProjectId AND 
		                    th.ParentId = '{hierarchyId}'

	                    UNION ALL

	                    SELECT h.Id, h.ParentId, nh.TopParentId, nh.TitlePath + ' / ' + TitlePath
	                    FROM Hierarchies h
	                    INNER JOIN NewHierarchies nh ON nh.Id = h.ParentID
	                    WHERE h.SubProjectId = @SubProjectId
                    )
                    SELECT nh.TopParentId Id, h.Title, nh.Processes, nh.Workflows, nh.TestCases, entc.PlannedTestCases, nh.OpenIssues, nh.FailedExecutions, nh.SuccessfulExecutions
                    FROM (
	                    SELECT 
		                    nh.TopParentId, 
		                    COUNT(DISTINCT p.Id) Processes, 
		                    COUNT(DISTINCT pw.WorkflowId) Workflows,
		                    COUNT(DISTINCT er.TestCaseId) TestCases,
		                    COUNT(DISTINCT i.Id) OpenIssues,
		                    COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN null ELSE CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) END) FailedExecutions,
		                    COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) ELSE null END) SuccessfulExecutions
	                    FROM NewHierarchies nh
	                    LEFT JOIN Processes p ON nh.Id = p.[HierarchyId]
	                    LEFT JOIN  (select pw.workflowid,pw.processid, wv.VersionId from ProcessWorkflows pw 
						inner join Workflows w on w.id=pw.WorkflowId inner join workflowversions wv on w.id=wv.WorkflowId and wv.IsLast=1) pw ON p.Id = pw.ProcessId
	                    LEFT JOIN Issues i ON p.Id = i.ProcessId AND i.[Status] <> {openIssuesStatus} {sqlInClause}
	                    LEFT JOIN ExecutionResults er ON pw.WorkflowId = er.WorkflowId and er.status<>4 and er.VersionId=pw.VersionId
	                    GROUP BY nh.TopParentId
                    ) nh
                    INNER JOIN Hierarchies h ON nh.TopParentId = h.Id
                    INNER JOIN (
	                    SELECT 
		                    nh.TopParentId, 
		                    SUM(ISNULL(p.ExpectedNumberOfTestCases, 0)) PlannedTestCases
	                    FROM NewHierarchies nh
	                    LEFT JOIN Processes p ON nh.Id = p.[HierarchyId]
	                    GROUP BY nh.TopParentId
                    ) entc 
                    ON h.Id = entc.TopParentId
                    ORDER BY h.Title ASC";
        }

        // This lists the Processes under one Hierarchy.
        private static string GetProcessesSql(Guid subProjectId, Guid hierarchyId, int openIssuesStatus, int successfulExecutionStatus, string[] issueTypes, DateTime date)
        {
            string sqlInClause = String.Empty;

            if (issueTypes != null)
            {
                sqlInClause = string.Format(" AND i.IssueTypeId IN ('{0}')", string.Join("', '", issueTypes));
            }

            return $@" 
                    SELECT np.Id, p.Title, 0 Processes, np.Workflows, np.TestCases, p.ExpectedNumberOfTestCases PlannedTestCases, np.OpenIssues, np.FailedExecutions, np.SuccessfulExecutions
                    FROM (
	                    SELECT 
		                    p.Id, 
		                    COUNT(DISTINCT pw.WorkflowId) Workflows,
		                    COUNT(DISTINCT er.TestCaseId) TestCases,
		                    COUNT(DISTINCT i.Id) OpenIssues,
                            COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN null ELSE CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) END) FailedExecutions,
		                    COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) ELSE null END) SuccessfulExecutions
	                    FROM Processes p
	                    LEFT JOIN  (select pw.workflowid,pw.processid, wv.VersionId from ProcessWorkflows pw 
						inner join Workflows w on w.id=pw.WorkflowId inner join workflowversions wv on w.id=wv.WorkflowId and wv.IsLast=1) pw ON p.Id = pw.ProcessId
	                    LEFT JOIN Issues i ON p.Id = i.ProcessId AND i.[Status] <> {openIssuesStatus} {sqlInClause}
	                    LEFT JOIN ExecutionResults er ON pw.WorkflowId = er.WorkflowId and er.status<>4 and er.VersionId=pw.VersionId
	                    WHERE p.[HierarchyId] = '{hierarchyId}'
	                    GROUP BY p.Id
                    ) np
                    INNER JOIN Processes p ON p.Id = np.Id
                    ORDER BY p.Title ASC";
        }

        // This lists the Workflows linked with a Process.
        private static string GetWorkflowsSql(Guid subProjectId, Guid hierarchyId, int openIssuesStatus, int successfulExecutionStatus, string[] issueTypes, DateTime date)
        {
            string sqlInClause = String.Empty;

            if (issueTypes != null)
            {
                sqlInClause = string.Format(" AND i.IssueTypeId IN ('{0}')", string.Join("', '", issueTypes));
            }

            return $@" 
                    SELECT nw.Id, w.Title, 0 Processes, 0 Workflows, nw.TestCases, 0 PlannedTestCases, nw.OpenIssues, nw.FailedExecutions, nw.SuccessfulExecutions
                    FROM (
	                    SELECT 
	                        w.Id,
                            COUNT(DISTINCT er.TestCaseId) TestCases,
                            COUNT(DISTINCT i.Id) OpenIssues,
		                    COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN null ELSE CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) END) FailedExecutions,
		                    COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) ELSE null END) SuccessfulExecutions
	                    FROM Workflows w inner join workflowversions wv on w.id=wv.WorkflowId and wv.IsLast=1 
	                    LEFT JOIN ProcessWorkflows pw ON w.Id = pw.WorkflowId
	                    LEFT JOIN WorkflowIssues wi ON pw.WorkflowId = wi.WorkflowId
                        LEFT JOIN Issues i ON i.Id = wi.IssueId AND i.[Status] <> {openIssuesStatus} {sqlInClause}
        	            LEFT JOIN Processes p ON p.Id = pw.ProcessId
	                    LEFT JOIN ExecutionResults er ON pw.WorkflowId = er.WorkflowId and er.status<>4 and er.VersionId=wv.VersionId
	                    WHERE pw.ProcessId = '{hierarchyId}'
	                    GROUP BY w.Id
                    ) nw
                    INNER JOIN Workflows w ON w.Id = nw.Id
                    ORDER BY w.Title ASC";
        }

        // This lists the Issues based on a parent Hierarchy to the from the given Date.
        private static string GetOpenIssuesGraphSql(Guid subProjectId, Guid hierarchyId, string[] issueTypes, DateTime date)
        {
            string sqlInClause = String.Empty;

            if (issueTypes != null)
            {
                sqlInClause = string.Format(" AND i.IssueTypeId IN ('{0}')", string.Join("', '", issueTypes));
            }

            return $@" 
                        DECLARE @SubProjectId UNIQUEIDENTIFIER
                        SET @SubProjectId = '{subProjectId}'

                        DECLARE @FromDate DATETIME2
                        SET @FromDate = '{date.ToString("yyyy-MM-dd")}'

                        ;WITH NewHierarchies AS 
                        (
	                        SELECT th.Id, th.ParentId, th.Id AS TopParentId
	                        FROM Hierarchies th
	                        WHERE 
		                        th.SubProjectId = @SubProjectId AND 
		                        th.ParentId = '{hierarchyId}'

	                        UNION ALL

	                        SELECT h.Id, h.ParentId, nh.TopParentId
	                        FROM Hierarchies h
	                        INNER JOIN NewHierarchies nh ON nh.Id = h.ParentID
	                        WHERE h.SubProjectId = @SubProjectId
                        )
                        SELECT nh.TopParentId Id, nh.[Date], nh.[Status], nh.[Count]
                        FROM (
	                        SELECT 
	                            nh.TopParentId,
		                        convert(CHAR(10), i.DateCreated, 120) AS [Date], 
		                        CASE WHEN i.[Status] = 2 THEN 2 ELSE 0 END AS [Status],
		                        count(i.[Status]) AS [Count]
	                        FROM NewHierarchies nh
	                        INNER JOIN Processes p ON nh.Id = p.[HierarchyId]
	                        INNER JOIN Issues i ON p.Id = i.ProcessId {sqlInClause}
	                        WHERE i.DateCreated >= @FromDate 
	                        GROUP BY nh.TopParentId, CONVERT(CHAR(10), i.DateCreated, 120), CASE WHEN i.[Status] = 2 THEN 2 ELSE 0 END
                        ) nh
                        INNER JOIN Hierarchies h ON nh.TopParentId = h.Id
                        ORDER BY h.Title ASC";
        }

        //This lists the issues of the Processes under one Hierarchy to the from the given Date. 
        private static string GetOpenIssuesGraphOfGetProcessesSql(Guid hierarchyId, string[] issueTypes, DateTime date)
        {
            string sqlInClause = String.Empty;

            if (issueTypes != null)
            {
                sqlInClause = string.Format(" AND i.IssueTypeId IN ('{0}')", string.Join("', '", issueTypes));
            }

            return $@"
                        DECLARE @FromDate DATETIME2
                        SET @FromDate = '{date.ToString("yyyy-MM-dd")}'
						
						SELECT nh.Id, nh.[Date], nh.[Status], nh.[Count]
                        FROM (
	                        SELECT 
	                            p.Id,
		                        convert(CHAR(10), i.DateCreated, 120) AS [Date], 
		                        CASE WHEN i.[Status] = 2 THEN 2 ELSE 0 END AS [Status],
		                        count(i.[Status]) AS [Count]
	                        FROM Processes p  
	                        INNER JOIN Issues i ON p.Id = i.ProcessId {sqlInClause}
	                        WHERE p.[HierarchyId] = '{hierarchyId}' 
                            AND i.DateCreated >= @FromDate 
	                        GROUP BY p.Id, CONVERT(CHAR(10), i.DateCreated, 120), CASE WHEN i.[Status] = 2 THEN 2 ELSE 0 END
                        ) nh
                        INNER JOIN Processes p ON nh.Id = p.Id
                        ORDER BY p.Title ASC";
        }

        //This lists the issues based on the Workflow under one Hierarchy to the from the given Date.
        private static string GetOpenIssuesGraphOfGetWorkflowsSql(Guid hierarchyId, string[] issueTypes, DateTime date)
        {
            string sqlInClause = String.Empty;

            if (issueTypes != null)
            {
                sqlInClause = string.Format(" AND i.IssueTypeId IN ('{0}')", string.Join("', '", issueTypes));
            }

            return $@"
                        DECLARE @FromDate DATETIME2
                        SET @FromDate = '{date.ToString("yyyy-MM-dd")}'
						
						SELECT nh.Id, nh.[Date], nh.[Status], nh.[Count]
                        FROM (
	                        SELECT 
	                            w.Id,
		                        convert(CHAR(10), i.DateCreated, 120) AS [Date], 
		                        CASE WHEN i.[Status] = 2 THEN 2 ELSE 0 END AS [Status],
		                        count(i.[Status]) AS [Count]
	                        FROM Workflows w
							LEFT JOIN ProcessWorkflows pw ON w.Id = pw.WorkflowId
							LEFT JOIN WorkflowIssues wi ON pw.WorkflowId = wi.WorkflowId
							LEFT JOIN Issues i ON i.Id = wi.IssueId {sqlInClause}
        					LEFT JOIN Processes p ON p.Id = pw.ProcessId
	                        WHERE p.Id = '{hierarchyId}'  
                            AND i.DateCreated >= @FromDate 
	                        GROUP BY w.Id, CONVERT(CHAR(10), i.DateCreated, 120), CASE WHEN i.[Status] = 2 THEN 2 ELSE 0 END
                        ) nh
                        INNER JOIN Workflows w ON w.Id = nh.Id
						ORDER BY w.Title ASC";
        }

        // This lists the Executions based on a parent Hierarchy to the from the given Date.
        private static string GetExecutionsGraphSql(Guid subProjectId, Guid hierarchyId, int successfulExecutionStatus, DateTime date)
        {
            return $@" 
                        DECLARE @SubProjectId UNIQUEIDENTIFIER
                        SET @SubProjectId = '{subProjectId}'

                        DECLARE @FromDate DATETIME2
                        SET @FromDate = '{date.ToString("yyyy-MM-dd")}'

                        ;WITH NewHierarchies AS 
                        (
	                        SELECT th.Id, th.ParentId, th.Id AS TopParentId
	                        FROM Hierarchies th
	                        WHERE 
		                        th.SubProjectId = @SubProjectId AND 
		                        th.ParentId = '{hierarchyId}'

	                        UNION ALL

	                        SELECT h.Id, h.ParentId, nh.TopParentId
	                        FROM Hierarchies h
	                        INNER JOIN NewHierarchies nh ON nh.Id = h.ParentID
	                        WHERE h.SubProjectId = @SubProjectId
                        )
                        SELECT nh.TopParentId Id, nh.[Day], nh.SuccessfulExecutions, nh.FailedExecutions
                        FROM (
	                        SELECT 
	                                nh.TopParentId,
		                            convert(CHAR(10), er.DateCreated, 120) AS [Day], 
		                            COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN null ELSE CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) END) FailedExecutions,
								    COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) ELSE null END) SuccessfulExecutions
	                        FROM NewHierarchies nh
	                            LEFT JOIN Processes p ON nh.Id = p.[HierarchyId]
								LEFT JOIN (select pw.workflowid,pw.processid, wv.VersionId from ProcessWorkflows pw 
						inner join Workflows w on w.id=pw.WorkflowId inner join workflowversions wv on w.id=wv.WorkflowId and wv.IsLast=1) pw ON p.Id = pw.ProcessId
								LEFT JOIN ExecutionResults er ON pw.WorkflowId = er.WorkflowId and    er.status <> 4 and  pw.VersionId=er.VersionId 
	                        WHERE er.DateCreated >= @FromDate 
	                        GROUP BY nh.TopParentId, CONVERT(CHAR(10), er.DateCreated, 120)
                        ) nh
                        INNER JOIN Hierarchies h ON nh.TopParentId = h.Id
                        ORDER BY h.Title ASC";
        }

        // This lists the Executions of the Processes under one Hierarchy to the from the given Date.
        private static string GetExecutionsGraphOfGetProcessesSql(Guid hierarchyId, int successfulExecutionStatus, DateTime date)
        {
            return $@" 
                        DECLARE @FromDate DATETIME2
                        SET @FromDate = '{date.ToString("yyyy-MM-dd")}'

                        SELECT np.Id, p.[Day], p.SuccessfulExecutions, p.FailedExecutions
                        FROM (
	                            SELECT 
							            p.Id,
		                                convert(CHAR(10), er.DateCreated, 120) AS [Day], 
		                                COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN null ELSE CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) END) FailedExecutions,
								        COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) ELSE null END) SuccessfulExecutions
	                                FROM Processes p 
								    LEFT JOIN  (select pw.workflowid,pw.processid, wv.VersionId from ProcessWorkflows pw 
						inner join Workflows w on w.id=pw.WorkflowId inner join workflowversions wv on w.id=wv.WorkflowId and wv.IsLast=1) pw ON p.Id = pw.ProcessId
								    LEFT JOIN ExecutionResults er ON pw.WorkflowId = er.WorkflowId and   er.status <> 4 and  pw.VersionId=er.VersionId 
	                            WHERE p.[HierarchyId] = '{hierarchyId}' 
                                AND er.DateCreated >= @FromDate
	                            GROUP BY p.Id, CONVERT(CHAR(10), er.DateCreated, 120)
                        ) p
                        INNER JOIN Processes np ON np.Id = p.Id
                        ORDER BY np.Title ASC
                     ";
        }

        // This lists the Executions of the Workflows linked with a Process to the from the given Date.
        private static string GetExecutionsGraphOfGetWorkflowsSql(Guid hierarchyId, int successfulExecutionStatus, DateTime date)
        {
            return $@" 
                        DECLARE @FromDate DATETIME2
                        SET @FromDate = '{date.ToString("yyyy-MM-dd")}'

					    SELECT nw.Id, nw.[Day], nw.SuccessfulExecutions, nw.FailedExecutions
                        FROM (
	                            SELECT 
	                                    w.Id,
		                                convert(CHAR(10), er.DateCreated, 120) AS [Day], 
		                                COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN null ELSE CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) END) FailedExecutions,
								        COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) ELSE null END) SuccessfulExecutions
	                            FROM Workflows w inner join workflowversions wv on w.id=wv.WorkflowId and wv.IsLast=1 
								    LEFT JOIN ProcessWorkflows pw ON w.Id = pw.WorkflowId
								    LEFT JOIN ExecutionResults er ON pw.WorkflowId = er.WorkflowId and    er.status <> 4 and  wv.VersionId=er.VersionId
	                            WHERE pw.ProcessId = '{hierarchyId}' 
							    AND er.DateCreated >= @FromDate
	                            GROUP BY w.Id, CONVERT(CHAR(10), er.DateCreated, 120)
                        ) nw
                        INNER JOIN Workflows w ON w.Id = nw.Id
                        ORDER BY w.Title ASC";
        }

        public bool Validate(GetTraceabilityReport query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            if (query.Level < 0)
            {
                Errors.Add("Level cannot be less then zero!");
                return false;
            }
            return true;
        }
    }
}
