﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.ReportsModel;
using Queries.ReportModule.TraceabilityReport;
using System;
using System.Collections.Generic;
using System.Linq;
using Application.Commands.ProcessModule.ArisWorkflows;
using Persistence.Process.IssuesModel;

namespace Application.Queries.ReportModule.TraceabilityReport
{
    public sealed class GetTraceabilityReportDetailHandler : IQueryHandler<GetTraceabilityReportDetail, GetTraceabilityReportDetailResult[]>
    {
        private readonly IDbContext _dbContext;

        public List<string> Errors { get; set; }

        public GetTraceabilityReportDetailHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetTraceabilityReportDetailResult[]> Handle(GetTraceabilityReportDetail queryObject)
        {
            var sql = string.Empty;

            int openIssuesStatus = 2;

            int successfulExecutionStatus = 2;

            var todayDate = DateTime.Now;

            if (queryObject.IssueTypes == null || !queryObject.IssueTypes.Any())
            {
                //queryObject.IssueTypes = _dbContext.Set<IssueTypeData>().Where(item => item.SubProjectId == queryObject.SubProjectId).Select(item => item.Id.ToString()).ToArray();
            }
            else
            {
                queryObject.IssueTypes = queryObject.IssueTypes[0].Split(',');
            }

            if (queryObject.Level == TraceablityReportLevel.Hierarchy)
            {
                sql = GetHierarchiesSql(queryObject.Id, openIssuesStatus, successfulExecutionStatus, queryObject.IssueTypes, todayDate);
            }
            else if (queryObject.Level == TraceablityReportLevel.Process)
            {
                sql = GetProcessesSql(queryObject.Id, openIssuesStatus, successfulExecutionStatus, queryObject.IssueTypes, todayDate);
            }
            else
            {
                sql = GetWorkflowsSql(queryObject.Id, successfulExecutionStatus, todayDate);
            }

            var result = _dbContext.QueryFromSql<TraceabilityReportLineDetail>(sql).ToArray().Select(
                line => new GetTraceabilityReportDetailResult
                {
                    Title = line.Title,
                    Workflows = line.Workflows,
                    TestCases = line.TestCases,
                    PlannedTestCases = line.PlannedTestCases,
                    OpenIssues = line.OpenIssues,
                    FailedExecutions = line.FailedExecutions,
                    SuccessfulExecutions = line.SuccessfulExecutions
                }
            ).ToArray();

            return new QueryResponse<GetTraceabilityReportDetailResult[]>(result);
        }

        // This lists the Processes under a specific Hierarchy (under child Hierarchies too).
        private static string GetHierarchiesSql(Guid id, int openIssuesStatus, int successfulExecutionStatus, string[] issueTypes, DateTime date)
        {
            string sqlInClause = String.Empty;

            if (issueTypes != null)
            {
                sqlInClause = string.Format(" AND i.IssueTypeId IN ('{0}')", string.Join("', '", issueTypes));
            }

            return $@" 
                    ;WITH NewHierarchies AS (
	                    SELECT th.Id, th.ParentId, th.Id AS TopParentId, th.Title AS TopParentTitle, CAST('' AS VARCHAR(MAX)) TitlePath
	                    FROM Hierarchies th
	                    WHERE 
		                    th.Id = '{id}'

	                    UNION ALL

	                    SELECT h.Id, h.ParentId, nh.TopParentId, nh.TopParentTitle, nh.TitlePath + CAST(h.Title AS VARCHAR(MAX)) + ' / ' 
	                    FROM Hierarchies h
	                    INNER JOIN NewHierarchies nh ON nh.Id = h.ParentID
                    )
                    SELECT nh.TitlePath + p.Title Title, Workflows, np.TestCases, p.ExpectedNumberOfTestCases PlannedTestCases, np.OpenIssues, np.FailedExecutions, np.SuccessfulExecutions
                    FROM Processes p
                    INNER JOIN NewHierarchies nh ON nh.Id = p.[HierarchyId]
                    INNER JOIN (
	                    SELECT 
		                        p.Id, 
		                        COUNT(DISTINCT pw.WorkflowId) Workflows,
		                        COUNT(DISTINCT er.TestCaseId) TestCases,
		                        SUM(ISNULL(p.ExpectedNumberOfTestCases, 0)) PlannedTestCases,
		                        COUNT(DISTINCT i.Id) OpenIssues,
		                        COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN null ELSE CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) END) FailedExecutions,
		                        COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) ELSE null END) SuccessfulExecutions
	                    FROM Processes p
	                    LEFT JOIN (select pw.workflowid,pw.processid , wv.VersionId from ProcessWorkflows pw inner join Workflows w on w.id=pw.WorkflowId inner join workflowversions wv on w.id=wv.WorkflowId and wv.IsLast=1) pw ON p.Id = pw.ProcessId
	                    LEFT JOIN Issues i ON p.Id = i.ProcessId AND i.[Status] <> {openIssuesStatus} {sqlInClause}
	                    LEFT JOIN ExecutionResults er ON pw.WorkflowId = er.WorkflowId
   and er.status <> 4 and pw.versionid=er.VersionId
	                    GROUP BY p.Id
                    ) np ON p.Id = np.Id";
        }

        // This lists the Workflows linked with a Process.
        private static string GetProcessesSql(Guid id, int openIssuesStatus, int successfulExecutionStatus, string[] issueTypes, DateTime date)
        {
            string sqlInClause = String.Empty;

            if (issueTypes != null)
            {
                sqlInClause = string.Format(" AND i.IssueTypeId IN ('{0}')", string.Join("', '", issueTypes));
            }

            return $@" 
                    SELECT w.Title, 0 Workflows, nw.TestCases, 0 PlannedTestCases, 0 OpenIssues, nw.FailedExecutions, nw.SuccessfulExecutions
                    FROM (
	                    SELECT 
	                        w.Id,
		                    COUNT(DISTINCT er.TestCaseId) TestCases,
                            --COUNT(DISTINCT i.Id) OpenIssues,
		                    COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN null ELSE CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) END) FailedExecutions,
		                    COUNT(DISTINCT CASE WHEN er.[Status] = {successfulExecutionStatus} THEN CAST(er.ExecutionTestCaseId AS VARCHAR) + CAST(er.TestCaseId AS VARCHAR) ELSE null END) SuccessfulExecutions
	                    FROM Workflows w inner join workflowversions wv on w.id=wv.WorkflowId and wv.IsLast=1 
	                    LEFT JOIN ProcessWorkflows pw ON w.Id = pw.WorkflowId 
	                    LEFT JOIN WorkflowIssues wi ON pw.WorkflowId = wi.WorkflowId
                        --LEFT JOIN Issues i ON i.Id = wi.IssueId AND i.[Status] <> {openIssuesStatus} {sqlInClause}
                        LEFT JOIN Processes p ON p.Id = pw.ProcessId
	                    LEFT JOIN ExecutionResults er ON pw.WorkflowId = er.WorkflowId and    er.status <> 4 and wv.VersionId=er.VersionId
	                    WHERE pw.ProcessId = '{id}' 
	                    GROUP BY w.Id
                    ) nw
                    INNER JOIN Workflows w ON w.Id = nw.Id
                    ORDER BY w.Title ASC";
        }

        // This lists the Test Cases linked with a Workflow.
        private static string GetWorkflowsSql(Guid id, int successfulExecutionStatus, DateTime date)
        {
            return $@"
                    SELECT er.TestCaseId, er.TestCaseTitle Title, 0 Processes, 0 Workflows, 0 TestCases, 0 PlannedTestCases, 0 OpenIssues, erw.FailedExecutions, erw.SuccessfulExecutions
                        FROM (
	                        SELECT 
		                        er.ExecutionTestCaseId,
		                        COUNT(DISTINCT CASE WHEN ern.[Status] = {successfulExecutionStatus} THEN null ELSE ern.ExecutionTestCaseId END) FailedExecutions,
		                        COUNT(DISTINCT CASE WHEN ern.[Status] = {successfulExecutionStatus} THEN ern.ExecutionTestCaseId ELSE null END) SuccessfulExecutions
	                        FROM ExecutionResults er
                            inner join workflows w on w.id=er.WorkflowId
							inner join workflowversions wv on w.id=wv.WorkflowId and wv.IsLast=1 and wv.VersionId=er.VersionId
                            LEFT JOIN ExecutionResults ern ON ern.ExecutionTestCaseId = er.ExecutionTestCaseId
	                        WHERE er.WorkflowId = '{id}'
	                        GROUP BY er.ExecutionTestCaseId
                        ) erw
                        INNER JOIN ExecutionResults er ON er.ExecutionTestCaseId = erw.ExecutionTestCaseId
                        where er.status <> 4
                        ORDER BY er.TestCaseId ASC";
        }

        public bool Validate(GetTraceabilityReportDetail query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }
            if (query.Level < 0)
            {
                Errors.Add("Level cannot be less then zero!");
                return false;
            }
            return true;
        }
    }
}
