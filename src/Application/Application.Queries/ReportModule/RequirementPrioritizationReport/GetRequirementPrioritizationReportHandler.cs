﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Queries.ReportModule.RequirementPrioritizationReport;
using SenseAI.Domain.Process.HierarchiesModel;
using Persistence.Process.ProcessModel;
using Persistence.Process.RequirementPrioritiesModel;
using Persistence.Process.HierarchiesModel;
using SenseAI.Domain;

namespace Application.Queries.ReportModule.RequirementPrioritizationReport
{
    public sealed class GetRequirementPrioritizationReportHandler : IQueryHandler<GetRequirementPrioritizationReport, GetRequirementPrioritizationReportResult>
    {
        public List<string> Errors { get; set; }

        IDbContext _dbContext;
        IHierarchyTypeRepository _hierarchyTypeRepository;

        public GetRequirementPrioritizationReportHandler(IDbContext dbContext, IHierarchyTypeRepository hierarchyTypeRepository)
        {
            _dbContext = dbContext;
            _hierarchyTypeRepository = hierarchyTypeRepository;
        }

        public QueryResponse<GetRequirementPrioritizationReportResult> Handle(GetRequirementPrioritizationReport queryObject)
        {
            var hierarchyTypes = _hierarchyTypeRepository.GetHierarchyTypeLevels(queryObject.SubProjectId);
            var columnNames = hierarchyTypes.Select(h => h.Title).ToList();
            columnNames.AddRange(new string[] { "Requirement", "Priority" });
            var reportData = new List<List<string>>();

            var processes = (from process in _dbContext.Set<ProcessData>()
                             join requirementPriority in _dbContext.Set<RequirementPriorityData>() on process.RequirementPriorityId equals requirementPriority.Id into reqProcJoined
                             from requirementPriority in reqProcJoined.DefaultIfEmpty()
                             where process.SubProjectId == queryObject.SubProjectId
                             select new
                             {
                                 process.HierarchyId,
                                 process.Title,
                                 Priority = requirementPriority == null ? "Unspecified" : requirementPriority.Title
                             }).ToList();

            processes.ForEach(process =>
            {
                var row = GetHierarchyPath(process.HierarchyId);
                row.AddRange(new string[] { process.Title, process.Priority });
                reportData.Add(row);
            });

            var result = new GetRequirementPrioritizationReportResult()
            {
                ColumnNames = columnNames.ToArray(),
                ReportData = reportData.Select(row => row.ToArray()).ToArray()
            };

            return new QueryResponse<GetRequirementPrioritizationReportResult>(result,
                processes.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No processes found in SubProject!"));
        }

        public bool Validate(GetRequirementPrioritizationReport query)
        {
            if(query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            return true;
        }

        #region Hierarchy Path helpers 
        /// <summary>
        /// Returns the hierarchy path for a given process hierarchyId
        /// </summary>
        /// <param name="processId"></param>
        /// <returns>Hierarchy path as a list of strings</returns>
        private List<string> GetHierarchyPath(Guid processHierarchyId)
        {
            List<string> hierarchyPath = new List<string>();

            Guid hierarchyId = processHierarchyId;

            while(hierarchyId != null && !hierarchyId.Equals(Guid.Empty))
            {
                Hierarchy hierarchy = GetHierarchy(hierarchyId);
                hierarchyPath.Insert(0, hierarchy.Title);
                hierarchyId = hierarchy.ParentId;
            }

           return hierarchyPath;
        }

        private Hierarchy GetHierarchy(Guid id)
        {
            var result = (from hierarchy in _dbContext.Set<HierarchyData>()
                          where hierarchy.Id == id
                          select new Hierarchy(hierarchy.ParentId,
                                               hierarchy.SubProjectId,
                                               hierarchy.HierarchyTypeId,
                                               hierarchy.Title,
                                               hierarchy.Description)).FirstOrDefault();
            return result;
        }

        #endregion
    }
}
