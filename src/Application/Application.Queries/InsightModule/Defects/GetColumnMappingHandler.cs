﻿using DataExtraction;
using Messaging.Queries;
using Queries.InsightModule;
using SenseAI.Domain;
using System.Collections.Generic;

namespace Application.Queries.InsightModule.Defects
{
    public sealed class GetColumnMappingHandler : IQueryHandler<DefectColumnMappingModel, List<string>>
    {
        private readonly IDataExtractionClient _service;

        public GetColumnMappingHandler(IDataExtractionClient service)
        {
            _service = service;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<List<string>> Handle(DefectColumnMappingModel queryObject)
        {
            var respond = _service.GetColumnMapping();
            return new QueryResponse<List<string>>(respond, respond.Count > 0 ? null : new QueryResponseMessage((int)MessageType.Information, "No Column Mapping."));
        }

        public bool Validate(DefectColumnMappingModel query)
        {
            return true;
        }
    }
}