﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.GenericWorkflowModel;
using Queries.ProcessModule.GenericWorkflows;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.ProcessModule.GenericWorkflows
{
    public sealed class GetGenericWorkflowsHandler : IQueryHandler<GetGenericWorkflows, GetGenericWorkflowResult>
    {
        private readonly IDbContext _dbContext;

        public List<string> Errors { get; set; }

        public GetGenericWorkflowsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetGenericWorkflowResult> Handle(GetGenericWorkflows queryObject)
        {
            var genericWorkflow = _dbContext.Set<GenericWorkflowData>().AsNoTracking().
                FirstOrDefault(w => w.ProcessId == queryObject.ProcessId);

            GetGenericWorkflowResult result = null;
            if (genericWorkflow != null)
                result = new GetGenericWorkflowResult(genericWorkflow.Id, genericWorkflow.ProcessId, genericWorkflow.Title, genericWorkflow.Description, genericWorkflow.DesignerJson);

            return new QueryResponse<GetGenericWorkflowResult>(result, genericWorkflow != null ? null : new QueryResponseMessage((int)MessageType.Information, $"Generic workflow not found in the process."));

        }

        public bool Validate(GetGenericWorkflows query)
        {
            if (query.ProcessId == Guid.Empty)
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
