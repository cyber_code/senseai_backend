﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal; 
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;
using Queries.ProcessModule.GenericWorkflows;
using Persistence.Process.GenericWorkflowModel;

namespace Application.Queries.DesignModule
{
    public class GetGenericWorkflowVersionsByDateHandler : IQueryHandler<GetGenericWorkflowVersionsByDate, GetGenericWorkflowVersionsByDateResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetGenericWorkflowVersionsByDateHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetGenericWorkflowVersionsByDateResult[]> Handle(GetGenericWorkflowVersionsByDate queryObject)
        {

            var query = from workflowVersion in _dbContext.Set<GenericWorkflowVersionData>().AsNoTracking()
                        where
                            workflowVersion.GenericWorkflowId == queryObject.GenericWorkflowId
                            &&
                            (workflowVersion.DateCreated >= queryObject.DateFrom && workflowVersion.DateCreated <= queryObject.DateTo)
                        select new
                        {
                            workflowVersion.GenericWorkflowId,
                            workflowVersion.VersionId,
                            workflowVersion.Title,
                            workflowVersion.Description,
                            workflowVersion.IsLast,
                            workflowVersion.WorkflowImage,
                            workflowVersion.DateCreated
                        };
            var result = query.Select(
             workflowVersion => new GetGenericWorkflowVersionsByDateResult(
                 workflowVersion.GenericWorkflowId,
                         workflowVersion.VersionId,
                         workflowVersion.Title,
                         workflowVersion.Description,
                         workflowVersion.IsLast,
                         workflowVersion.WorkflowImage,
                         workflowVersion.DateCreated
             )
         ).ToArray();

            return new QueryResponse<GetGenericWorkflowVersionsByDateResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No GenericWorkflow Versions found."));
        }

        public bool Validate(GetGenericWorkflowVersionsByDate query)
        {
            if (query.GenericWorkflowId == Guid.Empty)
            {
                Errors.Add("GenericWorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}