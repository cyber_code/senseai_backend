﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal; 
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;
using Queries.ProcessModule.GenericWorkflows;
using Persistence.Process.GenericWorkflowModel;

namespace Application.Queries.DesignModule
{
    public class GetGenericWorkflowVersionsHandler : IQueryHandler<GetGenericWorkflowVersions, GetGenericWorkflowVersionsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetGenericWorkflowVersionsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetGenericWorkflowVersionsResult[]> Handle(GetGenericWorkflowVersions queryObject)
        {
            var result = _dbContext.Set<GenericWorkflowVersionData>().AsNoTracking().
                    Where(ct => ct.GenericWorkflowId == queryObject.GenericWorkflowId).Select(workflowVersion => new GetGenericWorkflowVersionsResult(
                       workflowVersion.GenericWorkflowId,
                               workflowVersion.VersionId,
                               workflowVersion.Title)).ToArray();

            return new QueryResponse<GetGenericWorkflowVersionsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No GenericWorkflow Versions found."));
        }

        public bool Validate(GetGenericWorkflowVersions query)
        {
            if (query.GenericWorkflowId == Guid.Empty)
            {
                Errors.Add("GenericWorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}