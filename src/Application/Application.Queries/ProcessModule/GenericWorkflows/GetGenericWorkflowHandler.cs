﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.GenericWorkflowModel;
using Queries.ProcessModule.GenericWorkflows;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.ProcessModule.GenericWorkflows
{
    public sealed class GetGenericWorkflowHandler : IQueryHandler<GetGenericWorkflow, GetGenericWorkflowResult>
    {
        private readonly IDbContext _dbContext;

        public List<string> Errors { get; set; }

        public GetGenericWorkflowHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetGenericWorkflowResult> Handle(GetGenericWorkflow queryObject)
        {
            var genericWorkflow = _dbContext.Set<GenericWorkflowData>().AsNoTracking().
                FirstOrDefault(w => w.Id == queryObject.Id);
            if (genericWorkflow is null) 
                return new QueryResponse<GetGenericWorkflowResult>(null, new QueryResponseMessage((int)MessageType.Information, $"Generic workflow not found in the process."));

            return new QueryResponse<GetGenericWorkflowResult>(new GetGenericWorkflowResult(genericWorkflow.Id, genericWorkflow.ProcessId, genericWorkflow.Title,
                    genericWorkflow.Description, genericWorkflow.DesignerJson), genericWorkflow != null ? null : new QueryResponseMessage((int)MessageType.Information, $"Generic workflow not found in the process."));
        }

        public bool Validate(GetGenericWorkflow query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
