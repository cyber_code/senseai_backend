﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using Queries.ProcessModule.Hierarchies;
using SenseAI.Domain;
using Microsoft.EntityFrameworkCore;

namespace Application.Queries.ProcessModule.Hierarchies
{
    public sealed class GetLastHierarchyTypeHandler : IQueryHandler<GetLastHierarchyType, GetLastHierarchyTypeResult>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetLastHierarchyTypeHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetLastHierarchyTypeResult> Handle(GetLastHierarchyType queryObject)
        {
            var result = (from hierarchyType in _dbContext.Set<HierarchyTypeData>()
                         where hierarchyType.SubProjectId == queryObject.SubProjectId
                         orderby hierarchyType.Level descending
                         select new GetLastHierarchyTypeResult(
                                     hierarchyType.Id, 
                                     hierarchyType.ParentId,
                                     hierarchyType.SubProjectId,hierarchyType.Title,
                                     hierarchyType.Description)).FirstOrDefault();

            return new QueryResponse<GetLastHierarchyTypeResult>(result,
                result != null ? null : new QueryResponseMessage((int)MessageType.Information, $"No HierarchyType found in subProject."));
        }

        public bool Validate(GetLastHierarchyType query)
        {
            if(query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}