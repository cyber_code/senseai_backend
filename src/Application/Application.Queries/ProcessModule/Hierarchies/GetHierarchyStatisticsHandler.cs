﻿using Messaging.Queries;
using Persistence.Internal;
using Queries.ProcessModule.Hierarchies;
using System;
using System.Collections.Generic;
using System.Linq;
using Persistence.Process.ProcessModel;
using Persistence.Process.IssuesModel;
using SenseAI.Domain;
using Persistence.WorkflowModel;
using Microsoft.EntityFrameworkCore;

namespace Application.Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchyStatisticsHandler : IQueryHandler<GetHierarchyStatistics, GetHierarchyStatisticsResult>
    {
        private readonly IDbContext _dbContext;

        public GetHierarchyStatisticsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetHierarchyStatisticsResult> Handle(GetHierarchyStatistics queryObject)
        {
            GetHierarchyStatisticsResult HierarchyStatistics = GetStatistics(queryObject, _dbContext);

            return new QueryResponse<GetHierarchyStatisticsResult>(HierarchyStatistics,
                 HierarchyStatistics != null ? null : new QueryResponseMessage((int)MessageType.Information, $"No HierarchyStatistics found."));
        }

        public static GetHierarchyStatisticsResult GetStatistics(GetHierarchyStatistics queryObject, IDbContext dbContext)
        {
            var queryProcess = from process in dbContext.Set<ProcessData>()
                               where process.HierarchyId == queryObject.HierarchyId &&
                                     process.SubProjectId == queryObject.SubProjectId
                               select new
                               {
                                   process.HierarchyId,
                                   process.SubProjectId,
                                   process.Id
                               };
            var list = queryProcess.ToList().Select(x => x.Id);
            var queryProcessWorkflows = from processWorkflows in dbContext.Set<ProcessWorkflowsData>()
                                        where list.Contains(processWorkflows.ProcessId) 
                                        select new
                                        {
                                            processWorkflows.WorkflowId,
                                            processWorkflows.ProcessId
                                        };
            var workflowList = queryProcessWorkflows.ToList().Select(x => x.WorkflowId);
            var queryProcesTestCase = from executionTestCase in dbContext.Set<ExecutionResultData>()
                                      where workflowList.Contains(executionTestCase.WorkflowId)
                                      group executionTestCase by executionTestCase.TestCaseId into testcase
                                      select new
                                      {
                                        
                                          testcase.Key
                                      };
          
            var queryIssues = (from issues in dbContext.Set<IssueData>()
                              where list.Contains(issues.ProcessId)
                              select new
                              {
                                  issues.Id,
                                  issues.Status
                              }).ToList();
            queryIssues.AddRange((from process in dbContext.Set<ProcessData>().AsNoTracking()
                            join processWorkflows in dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
                            on process.Id equals processWorkflows.ProcessId
                            join workflowIssues in dbContext.Set<WorkflowIssueData>().AsNoTracking()
                           on processWorkflows.WorkflowId equals workflowIssues.WorkflowId

                            join issues in dbContext.Set<IssueData>().AsNoTracking()
                            on workflowIssues.IssueId equals issues.Id

                            where list.Contains(process.Id)
                                  select   new
                            {
                                issues.Id,
                                issues.Status
                            }).ToList());
          var qissues=  queryIssues.Select(p => new { p.Id, p.Status }).Distinct();
            GetHierarchyStatisticsResult HierarchyStatistics = new GetHierarchyStatisticsResult(queryProcess.Count(), queryProcessWorkflows.Count(), queryProcesTestCase.Count(),
            qissues.Count(), qissues.Where(w => w.Status == (int)StatusType.ToDo || (int)w.Status == (int)StatusType.InProgress).Count(), 0);
            return HierarchyStatistics;
        }

        public bool Validate(GetHierarchyStatistics query)
        {
            if (query.HierarchyId.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty!");
                return false;
            }
            if (query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubprojectId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}