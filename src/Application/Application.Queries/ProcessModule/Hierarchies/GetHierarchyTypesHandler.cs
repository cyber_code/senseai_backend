﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using Queries.ProcessModule.Hierarchies;
using SenseAI.Domain;
using Microsoft.EntityFrameworkCore;

namespace Application.Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchyTypesHandler : IQueryHandler<GetHierarchyTypes, GetHierarchyTypesResult[]>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetHierarchyTypesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetHierarchyTypesResult[]> Handle(GetHierarchyTypes queryObject)
        {
            var query = from hierarchyType in _dbContext.Set<HierarchyTypeData>()
                        where hierarchyType.SubProjectId == queryObject.SubProjectId
                        orderby hierarchyType.Level ascending
                        select new
                        {
                            hierarchyType.Id,
                            hierarchyType.ParentId,
                            hierarchyType.SubProjectId,
                            hierarchyType.Title,
                            hierarchyType.Description
                        };

            var results = query.Select(hT => new GetHierarchyTypesResult(hT.Id, hT.ParentId, hT.SubProjectId, hT.Title, hT.Description)).ToArray();

            return new QueryResponse<GetHierarchyTypesResult[]>(results,
                results.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No HierarchyTypes found with in subProject."));
        }

        public bool Validate(GetHierarchyTypes query)
        {
            if(query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}