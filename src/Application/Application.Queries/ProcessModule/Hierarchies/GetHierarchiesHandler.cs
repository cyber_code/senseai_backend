﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using Queries.ProcessModule.Hierarchies;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchiesHandler : IQueryHandler<GetHierarchies, GetHierarchiesResult[]>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetHierarchiesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetHierarchiesResult[]> Handle(GetHierarchies queryObject)
        {
            var query = from hierarchy in _dbContext.Set<HierarchyData>()
                        where hierarchy.HierarchyTypeId == queryObject.HierarchyTypeId
                            && hierarchy.SubProjectId == queryObject.SubProjectId
                        select new
                        {
                            hierarchy.Id,
                            hierarchy.ParentId,
                            hierarchy.SubProjectId,
                            hierarchy.HierarchyTypeId,
                            hierarchy.Title,
                            hierarchy.Description
                        };

            var results = query.Select(hierarchy => new GetHierarchiesResult(hierarchy.Id,
                                                                             hierarchy.ParentId,
                                                                             hierarchy.SubProjectId,
                                                                             hierarchy.HierarchyTypeId,
                                                                             hierarchy.Title,
                                                                             hierarchy.Description)).ToArray();

            return new QueryResponse<GetHierarchiesResult[]>(results,
                results.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
                    $"No Hierarchies found in subProject."));
        }

        public bool Validate(GetHierarchies query)
        {
            if (query.HierarchyTypeId.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyTypeId cannot be empty!");
                return false;
            }

            if (query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}