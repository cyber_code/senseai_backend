﻿using Iveonik.Stemmers;
using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.Hierarchies;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;
using StopWord;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Application.Queries.ProcessModule.Hierarchies
{
    public sealed class SearchHierarchiesHandler : IQueryHandler<SearchHierarchies, SearchHierarchiesResult>
    {
        private readonly IDbContext _dbContext;

        public SearchHierarchiesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public static string WordStemming(string SearchPhrase)
        {
            string documentWithoutPunctuation = "";
            string final = null;
            foreach (var c in SearchPhrase)
            {
                if (!char.IsPunctuation(c))
                {
                    documentWithoutPunctuation += c;
                }
            }
            SearchPhrase = documentWithoutPunctuation;

            List<string> termStrings = SearchPhrase.ToLower().Split(' ').ToList();

            List<string> stemmedTerms = new List<string>();
            EnglishStemmer stemmer = new EnglishStemmer();
            foreach (var ts in termStrings)
            {
                stemmedTerms.Add(stemmer.Stem(ts));
            }
            List<string> terms = new List<string>();
            foreach (var stemmedterm in stemmedTerms.Distinct())
            {
                if (!terms.Any(i => i == stemmedterm))
                {
                    terms.Add(stemmedterm);
                }
            }
            final = String.Join(" ", terms);

            return WordStops(final);
        }

        public static string WordStops(string SearchPhrase)
        {
            var _stopWordslist = StopWords.GetStopWords(CultureInfo.GetCultureInfo("en-US"));

            string[] wordList = SearchPhrase.Split(new char[] { '.', '?', '!', ' ', ';', ':', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(item => item.ToLowerInvariant()).ToArray();

            string final = null;

            var result = wordList.Except(_stopWordslist).ToArray();

            final = String.Join(" ", result);

            return final;
        }

        public QueryResponse<SearchHierarchiesResult> Handle(SearchHierarchies queryObject)
        {
            var sentence = WordStemming(queryObject.SearchCriteria);
            string[] wordList = sentence.Split(new char[] { '.', '?', '!', ' ', ';', ':', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(item => item.ToLowerInvariant()).ToArray();

            var result = new SearchHierarchiesResult(GetHierarchies(queryObject, wordList).ToArray(),
                GetProcess(queryObject, wordList).ToArray());

            return new QueryResponse<SearchHierarchiesResult>(result, result.ListOfHierarchies.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No HierarchyTypes found in subProject."));
        }

        public bool Validate(SearchHierarchies query)
        {
            if (query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            if (string.IsNullOrWhiteSpace(query.SearchCriteria))
            {
                Errors.Add("Search criteria cannot be empty!");
                return false;
            }

            return true;
        }

        private List<GetHierarchyResult> GetHierarchies(SearchHierarchies queryObject, string[] wordList)
        {
            var listOfHierarchy = from hierarchy in _dbContext.Set<HierarchyData>().AsNoTracking()
                                  where hierarchy.SubProjectId == queryObject.SubProjectId &&
                                  hierarchy.ParentId == ((hierarchy.ParentId == Guid.Empty || hierarchy.ParentId == null) ? hierarchy.ParentId : queryObject.ParentId) &&
                    wordList.Any(word =>
                        (hierarchy.Title ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant()) ||
                        (hierarchy.Description ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant()))
                                  select hierarchy;
            var listOfHierarchyresults = listOfHierarchy.Select(h => new GetHierarchyResult(h.Id, h.ParentId, h.SubProjectId, h.HierarchyTypeId, h.Title, h.Description)).ToList();

            return listOfHierarchyresults;
        }

        private List<GetProcessResult> GetProcess(SearchHierarchies queryObject, string[] wordList)
        {
            var listOfProcesses = from process in _dbContext.Set<ProcessData>()
                                  where process.SubProjectId == queryObject.SubProjectId &&
                                        wordList.Any(word =>
                                            (process.Title ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant()))
                                  select process;
            var listOfProcessesResults = listOfProcesses.Select(p => new GetProcessResult(p.Id, p.SubProjectId,p.HierarchyId, p.CreatedId, p.OwnerId, p.ProcessType, p.Title, p.Description, p.RequirementPriorityId, p.RequirementTypeId, p.ExpectedNumberOfTestCases, p.TypicalId)).ToList();

            return listOfProcessesResults;
        }
    }
}