﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using Queries.ProcessModule.Hierarchies;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchyHandler : IQueryHandler<GetHierarchy, GetHierarchyResult>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetHierarchyHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetHierarchyResult> Handle(GetHierarchy queryObject)
        {
            var query = from hierarchy in _dbContext.Set<HierarchyData>()
                        where hierarchy.Id == queryObject.Id
                        select new
                        {
                            hierarchy.Id,
                            hierarchy.ParentId,
                            hierarchy.SubProjectId,
                            hierarchy.HierarchyTypeId,
                            hierarchy.Title,
                            hierarchy.Description
                        };

            var result = query.Select(h => new GetHierarchyResult(h.Id, h.ParentId, h.SubProjectId, h.HierarchyTypeId, h.Title, h.Description)).FirstOrDefault();

            return new QueryResponse<GetHierarchyResult>(result,
                result != null ? null : new QueryResponseMessage((int)MessageType.Information, $"No Hierarchy found."));
        }

        public bool Validate(GetHierarchy query)
        {
            if (query.Id.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}