﻿using Application.Queries.ProcessModule.Hierarchies;
using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using Persistence.Process.IssuesModel;
using Persistence.Process.ProcessModel;
using Persistence.WorkflowModel;
using Persistence.WorkflowModel;
using Queries.ProcessModule.Hierarchies;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Hierarchies
{
    public class GetHierarchiesStatisticsHandler : IQueryHandler<GetHierarchiesStatistics, GetHierarchiesStatisticsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetHierarchiesStatisticsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetHierarchiesStatisticsResult[]> Handle(GetHierarchiesStatistics queryObject)
        {
            List<GetHierarchiesStatisticsResult> statistics = new List<GetHierarchiesStatisticsResult>();
              var hierarchiesData =(from hierarchies in _dbContext.Set<HierarchyData>().AsNoTracking()
                             where  hierarchies.SubProjectId == queryObject.SubProjectId
                             select hierarchies).ToList();


            var query = hierarchiesData.Where(x => x.ParentId == queryObject.ParentId);
            foreach (var hier in query)
            {
                var result = getStatisticsRecursively(hier, queryObject.SubProjectId, _dbContext, hierarchiesData);
                statistics.Add(new GetHierarchiesStatisticsResult(result.NrProcesses, result.NrWorkflows, result.NrTestCases, result.NrTotalIssues, result.NrOpenIssues, hier.Id, hier.Title, result.NrTestSteps));
            }

            return new QueryResponse<GetHierarchiesStatisticsResult[]>(statistics.ToArray(), statistics.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Hierarchies found with SubProjectId: '{queryObject.SubProjectId}'."));
        }
       
        private GetHierarchyStatisticsResult getStatisticsRecursively(HierarchyData hierarchy, Guid subProjectId,IDbContext dbContext, List<HierarchyData> h)
        {
             List<HierarchyData> leafs = GetLeafsOfHierarchies(hierarchy.Id,h);
            if (leafs.Count == 0)
                leafs.Add(hierarchy);
             var result = GetStatistics(leafs,subProjectId, _dbContext);
             return result;
        }
        public static GetHierarchyStatisticsResult GetStatistics(List<HierarchyData> children, Guid subProjectId, IDbContext dbContext)
        {
            var childrenlist= children.ToList().Select(x => x.Id);
            var query = (from process in dbContext.Set<ProcessData>().AsNoTracking()
                         join processWorkflows in dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
                         on process.Id equals processWorkflows.ProcessId into processWithWorkflow
                         from processLeftOuterJoinProcessWorkflow in processWithWorkflow.DefaultIfEmpty()
                        
                         join executionTestCase in dbContext.Set<ExecutionResultData>().AsNoTracking()
                         on processLeftOuterJoinProcessWorkflow.WorkflowId equals executionTestCase.WorkflowId into processWithExecutionTestCase
                         from processLeftOuterJoinExecutionTestCase in processWithExecutionTestCase.DefaultIfEmpty()
                         join issues in dbContext.Set<IssueData>().AsNoTracking()
                         on process.Id equals issues.ProcessId  
                into processWithIssues
                         from processLeftOuterJoinIssues in processWithIssues.DefaultIfEmpty()
                         where childrenlist.Contains(process.HierarchyId) &&
                               process.SubProjectId == subProjectId
                         select new
                         {
                             processid = process.Id,
                             workflowid =(Guid?)processLeftOuterJoinProcessWorkflow.WorkflowId,
                             testcaseid = (long?)processLeftOuterJoinExecutionTestCase.TestCaseId,
                             issueid = (Guid?)processLeftOuterJoinIssues.Id,
                             issueStatus =(StatusType?)processLeftOuterJoinIssues.Status
                         }).ToList();

            query.AddRange((from process in dbContext.Set<ProcessData>().AsNoTracking()
                            join processWorkflows in dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
                            on process.Id equals processWorkflows.ProcessId into processWithWorkflow
                            from processLeftOuterJoinProcessWorkflow in processWithWorkflow.DefaultIfEmpty()
                            join workflowIssues in dbContext.Set<WorkflowIssueData>().AsNoTracking()
                           on processLeftOuterJoinProcessWorkflow.WorkflowId equals workflowIssues.WorkflowId into workflowWithIssue
                            from processWorkflowLeftJoinWithIssue in workflowWithIssue.DefaultIfEmpty()
                            join executionTestCase in dbContext.Set<ExecutionResultData>().AsNoTracking()
                            on processLeftOuterJoinProcessWorkflow.WorkflowId equals executionTestCase.WorkflowId into processWithExecutionTestCase
                            from processLeftOuterJoinExecutionTestCase in processWithExecutionTestCase.DefaultIfEmpty()
                            join issues in dbContext.Set<IssueData>().AsNoTracking()
                            on processWorkflowLeftJoinWithIssue.IssueId equals issues.Id
                   into processWithIssues
                            from processLeftOuterJoinIssues in processWithIssues.DefaultIfEmpty()
                            where childrenlist.Contains(process.HierarchyId) &&
                                  process.SubProjectId == subProjectId
                            select new
                            {
                                processid = process.Id,
                                workflowid = (Guid?)processLeftOuterJoinProcessWorkflow.WorkflowId,
                                testcaseid = (long?)processLeftOuterJoinExecutionTestCase.TestCaseId,
                                issueid = (Guid?)processLeftOuterJoinIssues.Id,
                                issueStatus = (StatusType?)processLeftOuterJoinIssues.Status
                            }).ToList());
            var queryProcess = query.Select(p => p.processid)
                                    .Distinct();

            var queryProcessWorkflows = query.Where(p=>p.workflowid != null)
                                             .Select(p => p.workflowid)
                                             .Distinct();

            var queryProcesTestCase = query.Where(p => p.testcaseid != null)
                                           .Select(p => p.testcaseid)
                                           .Distinct();

            var queryIssues = query.Where(p => p.issueid != null)
                                   .Select(p => p.issueid)
                                   .Distinct();

            var queryIssuesOpen = query.Where(p => p.issueid != null && (p.issueStatus == (int)StatusType.ToDo || (int)p.issueStatus == (int)StatusType.InProgress))
                                       .Select(p => p.issueid)
                                       .Distinct();
            
            GetHierarchyStatisticsResult HierarchyStatistics = new GetHierarchyStatisticsResult(queryProcess.Count(), queryProcessWorkflows.Count(), queryProcesTestCase.Count(),               queryIssues.Count(), queryIssuesOpen.Count(), 0);
            return HierarchyStatistics;
        }
        public List<HierarchyData> GetLeafsOfHierarchies(Guid hierarchyId, List<HierarchyData> hierarchiesData)
        {
            List<HierarchyData> result = new List<HierarchyData>();
           
            var query = hierarchiesData.Where(x => x.ParentId == hierarchyId).ToList();

            query.ForEach(h => result.AddRange(GetLeafsOfHierarchies(h.Id, hierarchiesData)));
           
            result.AddRange(query.ToArray());
            return result;
        }

        public bool Validate(GetHierarchiesStatistics query)
        {
            if (query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
