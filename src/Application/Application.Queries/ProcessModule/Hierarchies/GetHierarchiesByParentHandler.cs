﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using Queries.ProcessModule.Hierarchies;
using SenseAI.Domain;
using Microsoft.EntityFrameworkCore;

namespace Application.Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchiesByParentHandler : IQueryHandler<GetHierarchiesByParent, GetHierarchiesByParentResult[]>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetHierarchiesByParentHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetHierarchiesByParentResult[]> Handle(GetHierarchiesByParent queryObject)
        {
            Guid hierarchyTypeParentID = Guid.Empty;
            if (!string.IsNullOrEmpty(queryObject.HierarchyTypeId))
                hierarchyTypeParentID = Guid.Parse(queryObject.HierarchyTypeId);
            var qHierarchyTyp = (from hierarchyType in _dbContext.Set<HierarchyTypeData>().AsNoTracking()
                                 where hierarchyType.SubProjectId == queryObject.SubProjectId
                                 && hierarchyType.ParentId == hierarchyTypeParentID
                                select new
                                 {
                                     hierarchyType.Id 
                                 }).FirstOrDefault();

            if (qHierarchyTyp == null)
                return new QueryResponse<GetHierarchiesByParentResult[]>(null, new QueryResponseMessage((int)MessageType.Information, $"No hierarchy type found."));

            Guid hierarchyParentID = Guid.Empty;
            if (!string.IsNullOrEmpty(queryObject.ParentId))
                hierarchyParentID = Guid.Parse(queryObject.ParentId);

            var qHierarchies = from hierarchy in _dbContext.Set<HierarchyData>()
                        where hierarchy.SubProjectId == queryObject.SubProjectId
                        && hierarchy.HierarchyTypeId == qHierarchyTyp.Id
                        && hierarchy.ParentId == hierarchyParentID
                        select new
                        {
                            hierarchy.Id,
                            hierarchy.ParentId,
                            hierarchy.SubProjectId,
                            hierarchy.HierarchyTypeId,
                            hierarchy.Title,
                            hierarchy.Description
                        };

            var results = qHierarchies.Select(hierarchy => new GetHierarchiesByParentResult(hierarchy.Id,
                                                                             hierarchy.Title,
                                                                             hierarchy.Description,
                                                                             hierarchy.ParentId,
                                                                             hierarchy.HierarchyTypeId
                                                                            )).ToArray();

            return new QueryResponse<GetHierarchiesByParentResult[]>(results,
                results.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
                    $"No Hierarchies found in subProject."));
        }

        public bool Validate(GetHierarchiesByParent query)
        {
            if (query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId must be provided!");
                return false;
            }

            return true;
        }
    }
}