﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using Queries.ProcessModule.Hierarchies;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.Hierarchies
{
    public sealed class GetHierarchyTypeHandler : IQueryHandler<GetHierarchyType, GetHierarchyTypeResult>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetHierarchyTypeHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetHierarchyTypeResult> Handle(GetHierarchyType queryObject)
        {
            var query = from hierarchyType in _dbContext.Set<HierarchyTypeData>()
                        where hierarchyType.Id == queryObject.Id
                        select new
                        {
                            hierarchyType.Id,
                            hierarchyType.ParentId,
                            hierarchyType.SubProjectId,
                            hierarchyType.Title,
                            hierarchyType.Description
                        };

            var result = query.Select(hT => new GetHierarchyTypeResult(hT.Id, hT.ParentId, hT.SubProjectId, hT.Title, hT.Description)).FirstOrDefault();

            return new QueryResponse<GetHierarchyTypeResult>(result, 
                result != null ? null : new QueryResponseMessage((int)MessageType.Information, $"No HierarchyType found."));
        }

        public bool Validate(GetHierarchyType query)
        {
            if (query.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            return true;
        }
    }
}