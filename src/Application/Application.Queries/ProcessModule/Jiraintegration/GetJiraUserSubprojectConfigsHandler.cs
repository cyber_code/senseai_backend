﻿using Jira;
using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Jira;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Abstractions;
using Persistence.AdminModel;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraUserSubprojectConfigsHandler : IQueryHandler<GetJiraUserSubprojectConfigs, GetJiraUserSubprojectConfigsResult>
    {
        private readonly IDbContext _dbContext;

        private readonly IJiraUserSubprojectRepository _repository;

        public GetJiraUserSubprojectConfigsHandler(IDbContext dbContext, IJiraUserSubprojectRepository repository)
        {
            _dbContext = dbContext;
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraUserSubprojectConfigsResult> Handle(GetJiraUserSubprojectConfigs queryObject)
        {
            var result = _repository.GetJiraUserSubprojectConfigs(queryObject.SubprojectId, queryObject.UserId);
            if (result == null)
            {
                return new QueryResponse<GetJiraUserSubprojectConfigsResult>(null, new QueryResponseMessage((int)MessageType.Warning, "No configuration found for this user/subproject"));
            }

            var subProjectResult = (from subProject in _dbContext.Set<SubProjectData>()
                                    where subProject.Id == queryObject.SubprojectId
                                    select new
                                    {
                                        subProject.JiraLink
                                    }).FirstOrDefault();
            if (subProjectResult == null || string.IsNullOrEmpty(subProjectResult.JiraLink))
            {
                new QueryResponse<GetJiraUserSubprojectConfigsResult>(null, new QueryResponseMessage((int)MessageType.Warning, "No Jira configuration link found for this subproject"));
            }

            GetJiraUserSubprojectConfigsResult getJiraUserSubprojectConfigsResult = new GetJiraUserSubprojectConfigsResult(result.JiraUsername, result.JiraToken, result.SubprojectId, result.UserId, subProjectResult.JiraLink);

            return new QueryResponse<GetJiraUserSubprojectConfigsResult>(getJiraUserSubprojectConfigsResult);
        }

        public bool Validate(GetJiraUserSubprojectConfigs query)
        {
            if (query.SubprojectId == Guid.Empty)
            {
                Errors.Add("Subproject cannot be empty");
                return false;
            }
            if (query.UserId == string.Empty)
            {
                Errors.Add("Username cannot be empty");
                return false;
            }
            return true;
        }
    }
}