﻿using Jira;
using Messaging.Queries;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;
using SenseAI.Domain.AdminModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraProjectHandler : IQueryHandler<GetJiraProjects, GetJiraProjectsResult[]>
    {
        private readonly IJiraClient _service;

        public GetJiraProjectHandler(IJiraClient service, ISubProjectRepository subProjectRepository)
        {
            _service = service;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraProjectsResult[]> Handle(GetJiraProjects queryObject)
        {
            IEnumerable<Atlassian.Jira.Project> serviceResult = _service.GetAllProjects(queryObject.SubprojectId);
            List<GetJiraProjectsResult> jiraProjectList = new List<GetJiraProjectsResult>();
            foreach (var project in serviceResult)
            {
                jiraProjectList.Add(new GetJiraProjectsResult
                {
                    ProjectId = project.Key,
                    ProjectName = project.Name
                });
            }

            return new QueryResponse<GetJiraProjectsResult[]>(jiraProjectList.ToArray(), jiraProjectList.ToArray() is GetJiraProjectsResult[]? null : new QueryResponseMessage((int)MessageType.Information, $"No project found."));
        }

        public bool Validate(GetJiraProjects query)
        {
            return true;
        }
    }
}