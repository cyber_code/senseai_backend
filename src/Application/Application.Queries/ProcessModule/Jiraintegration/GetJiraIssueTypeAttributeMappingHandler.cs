﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Jira;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueTypeAttributeMappingHandler : IQueryHandler<GetJiraIssueTypeAttributeMappings, GetJiraIssueTypeAttributeMappingsResult>
    {
        private readonly IJiraIssueTypeAttributeMappingRepository _repository;
        private readonly IDbContext _dbContext;

        public GetJiraIssueTypeAttributeMappingHandler(IJiraIssueTypeAttributeMappingRepository repository, IDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIssueTypeAttributeMappingsResult> Handle(GetJiraIssueTypeAttributeMappings queryObject)
        {
            var query = from jiraIssueTypeAttributeMapping in _dbContext.Set<JiraIssueTypeAttributeMappingData>().AsNoTracking()
                        where jiraIssueTypeAttributeMapping.Id == queryObject.Id
                        select new GetJiraIssueTypeAttributeMappingsResult(jiraIssueTypeAttributeMapping.Id, jiraIssueTypeAttributeMapping.JiraIssueTypeMappingId, jiraIssueTypeAttributeMapping.SenseaiField, jiraIssueTypeAttributeMapping.JiraField, jiraIssueTypeAttributeMapping.Required, jiraIssueTypeAttributeMapping.Index);

            return new QueryResponse<GetJiraIssueTypeAttributeMappingsResult>(query.FirstOrDefault(), query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No JiraIssueTypeAttributeMapping found."));
        }

        public bool Validate(GetJiraIssueTypeAttributeMappings query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}