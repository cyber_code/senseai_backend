﻿using Jira;
using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Jira;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraUserMappingHandler : IQueryHandler<GetJiraUserMapping, GetJiraUserMappingResult[]>
    {
        private readonly IDbContext _dbContext;
        private readonly IJiraUserMappingRepository _repository;

        public GetJiraUserMappingHandler(IDbContext dbContext, IJiraUserMappingRepository repository)
        {
            _dbContext = dbContext;
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraUserMappingResult[]> Handle(GetJiraUserMapping queryObject)
        {
            var query = (from jira in _dbContext.Set<JiraUserMappingData>().AsNoTracking()
                         where jira.SubprojectId == queryObject.SubprojectId
                         select new GetJiraUserMappingResult(jira.Id, jira.SubprojectId, jira.JiraUserId, "", jira.SenseaiUserId, "")).ToList();

            return new QueryResponse<GetJiraUserMappingResult[]>(query.ToArray(), query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No user found."));
        }

        public bool Validate(GetJiraUserMapping query)
        {
            return true;
        }
    }
}