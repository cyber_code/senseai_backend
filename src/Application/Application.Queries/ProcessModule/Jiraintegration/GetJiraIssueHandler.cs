﻿using System;
using System.Collections.Generic;
using Jira;
using Messaging.Queries;
using Persistence.Internal;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueHandler : IQueryHandler<GetJiraIssue, GetJiraIssueResult>
    {
        private readonly IJiraClient _service;
        private readonly IDbContext _dbContext;

        public GetJiraIssueHandler(IJiraClient service, IDbContext dbContext)
        {
            _service = service;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIssueResult> Handle(GetJiraIssue queryObject)
        {
            var issueFromJira = _service.GetIssue(queryObject.SubprojectId, queryObject.Key);
            GetJiraIssueResult getJiraIssueResult = new GetJiraIssueResult
            {
                ////DefectId = GetPropValue(issueFromJira, config.Mappings.Find(a => a.OriginalField == "DefectId").CsvField).ToString(),
                //AssignedTo = GetPropValue(issueFromJira, config.Mappings.Find(a => a.OriginalField == "AssignedTo").CsvField) == null ? "" : GetPropValue(issueFromJira, config.Mappings.Find(a => a.OriginalField == "AssignedTo").CsvField).ToString(),
                //Name = GetPropValue(issueFromJira, config.Mappings.Find(a => a.OriginalField == "Name").CsvField).ToString(),
                //Description = GetPropValue(issueFromJira, config.Mappings.Find(a => a.OriginalField == "Description").CsvField) == null ? "" : GetPropValue(issueFromJira, config.Mappings.Find(a => a.OriginalField == "Description").CsvField).ToString(),
                //Component = Components.Any() == false ? "" : Components.FirstOrDefault().ToString(),
                //Priority = Priority.Name,
                //Severity = Priority.Name, //TO-DO later
                //StartDate = issueFromJira.Created == null ? "1569934111" : ((DateTimeOffset)issueFromJira.Created).ToUnixTimeMilliseconds().ToString(),
                //ModifiedDate = issueFromJira.Updated == null ? "1569934111" : ((DateTimeOffset)issueFromJira.Updated).ToUnixTimeMilliseconds().ToString(),
                //TargetDate = issueFromJira.DueDate == null ? ((DateTimeOffset)issueFromJira.Created).AddDays(14).ToUnixTimeMilliseconds().ToString() : ((DateTimeOffset)issueFromJira.DueDate).ToUnixTimeMilliseconds().ToString(),
                //Status = Status.Name,
                //Type = Type,
                //TestCaseId = ""
            };
            return new QueryResponse<GetJiraIssueResult>(getJiraIssueResult, getJiraIssueResult is GetJiraIssueResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No issue found."));
        }

        public object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public bool Validate(GetJiraIssue query)
        {
            return true;
        }
    }
}