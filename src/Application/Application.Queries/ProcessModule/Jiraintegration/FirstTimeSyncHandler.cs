﻿using DataExtraction;
using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Jira;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain.AdminModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Jiraintegration
{
    public class FirstTimeSyncHandler : IQueryHandler<FirstTimeSync, bool>
    {
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;
        private readonly ISubProjectRepository _repository;

        public FirstTimeSyncHandler(IDataExtractionClient dataExtractionClient, IDbContext dbContext, ISubProjectRepository repository)
        {
            _dataExtractionClient = dataExtractionClient;
            _dbContext = dbContext;
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<bool> Handle(FirstTimeSync queryObject)
        {
            bool succesful = false;
            if (queryObject.Sync)
            {
                var url = _dbContext.Set<SubProjectData>().AsNoTracking()
                    .Where(p => p.Id == queryObject.SubprojectId)
                    .FirstOrDefault().JiraLink;

                var jiraConfig = _dbContext.Set<JiraUserSubprojectConfigsData>().AsNoTracking()
                    .Where(p => p.SubprojectId == queryObject.SubprojectId && p.UserId == queryObject.SenseAIUser)
                    .FirstOrDefault();

                if (jiraConfig != null && url != null)
                {
                    _dataExtractionClient.FirstTimeSync(new DataExtraction.JiraModel.FirstTimeSyncModel(queryObject.SubprojectId,
                        jiraConfig.JiraUsername, jiraConfig.JiraToken, url));
                    succesful = true;
                }
            }

            //update Subproject Status
            var query = (from project in _dbContext.Set<SubProjectData>().AsNoTracking()
                         where project.Id == queryObject.SubprojectId
                         select new
                         {
                             project.Id,
                             project.ProjectId,
                             project.Title,
                             project.Description,
                             project.JiraLink,
                             project.IsJiraIntegrationEnabled
                         }).FirstOrDefault();
            SubProject subProject = new SubProject(queryObject.SubprojectId, query.ProjectId, query.Title,
                query.Description, true, query.JiraLink);
            _repository.Update(subProject);

            return new QueryResponse<bool>(succesful);
        }

        public bool Validate(FirstTimeSync query)
        {
            return true;
        }
    }
}