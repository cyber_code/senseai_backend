﻿using System.Collections.Generic;
using Jira;
using Messaging.Queries;
using Persistence.Internal;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraUserHandler : IQueryHandler<GetJiraUsers, GetJiraUsersResult[]>
    {
        private readonly IJiraClient _service;
        private readonly IDbContext _dbContext;

        public GetJiraUserHandler(IJiraClient service, IDbContext dbContext)
        {
            _service = service;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraUsersResult[]> Handle(GetJiraUsers queryObject)
        {
            var userList = _service.GetJiraUsers(queryObject.SubprojectId);
            List<GetJiraUsersResult> getJiraIssueResult = new List<GetJiraUsersResult>();
            foreach (var status in userList)
            {
                getJiraIssueResult.Add(new GetJiraUsersResult
                {
                    Id = status.Key,
                    Name = status.DisplayName
                });
            }

            return new QueryResponse<GetJiraUsersResult[]>(getJiraIssueResult.ToArray(), getJiraIssueResult.ToArray() is GetJiraUsersResult[]? null : new QueryResponseMessage((int)MessageType.Information, $"No status found."));
        }

        public bool Validate(GetJiraUsers query)
        {
            return true;
        }
    }
}