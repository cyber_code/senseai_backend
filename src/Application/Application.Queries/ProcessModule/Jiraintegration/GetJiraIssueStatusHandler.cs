﻿using System;
using System.Collections.Generic;
using Jira;
using Messaging.Queries;
using Persistence.Internal;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueStatusHandler : IQueryHandler<GetJiraIssueStatuses, GetJiraIssueStatusesResult[]>
    {
        private readonly IJiraClient _service;
        private readonly IDbContext _dbContext;

        public GetJiraIssueStatusHandler(IJiraClient service, IDbContext dbContext)
        {
            _service = service;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIssueStatusesResult[]> Handle(GetJiraIssueStatuses queryObject)
        {
            var issueStatus = _service.GetIssueStatuses(queryObject.SubprojectId, queryObject.projectKey);
            List<GetJiraIssueStatusesResult> getJiraIssueResult = new List<GetJiraIssueStatusesResult>();
            foreach (var status in issueStatus)
            {
                getJiraIssueResult.Add(new GetJiraIssueStatusesResult
                {
                    Id = status.Id,
                    Name = status.Name
                });
            }

            return new QueryResponse<GetJiraIssueStatusesResult[]>(getJiraIssueResult.ToArray(), getJiraIssueResult.ToArray() is GetJiraIssueStatusesResult[]? null : new QueryResponseMessage((int)MessageType.Information, $"No status found."));
        }

        public object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public bool Validate(GetJiraIssueStatuses query)
        {
            return true;
        }
    }
}