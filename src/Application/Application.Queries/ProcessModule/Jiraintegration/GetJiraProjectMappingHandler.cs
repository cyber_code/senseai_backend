﻿using Jira;
using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Jira;
using Persistence.Process.HierarchiesModel;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;
using SenseAI.Domain.JiraModel;
using SenseAI.Domain.Process.HierarchiesModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraProjectMappingHandler : IQueryHandler<GetJiraProjectMapping, GetJiraProjectMappingResult>
    {
        private readonly IDbContext _dbContext;
        private readonly IJiraProjectMappingRepository _repository;
        private readonly IHierarchyTypeRepository _hierarchyTypeRepository;

        public GetJiraProjectMappingHandler(IDbContext dbContext, IJiraProjectMappingRepository repository, IHierarchyTypeRepository hierarchyTypeRepository)
        {
            _dbContext = dbContext;
            _repository = repository;
            _hierarchyTypeRepository = hierarchyTypeRepository;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraProjectMappingResult> Handle(GetJiraProjectMapping queryObject)
        {
            var hiererchyTypeName = string.Empty;
            var hiererchyParentName = string.Empty;
            var query = _repository.GetJiraProjectMappingBySubprojectId(queryObject.SubprojectId);

            if (query != null)
            {
                if (query.MapHierarchy)
                {
                    try
                    {
                        hiererchyTypeName = _hierarchyTypeRepository.GetHierarchyTypeById(query.HierarchyTypeId).Title;
                        hiererchyParentName = _dbContext.Set<HierarchyData>().AsNoTracking().Where(item => item.Id == query.HierarchyParentId).FirstOrDefault().Title;
                    }
                    catch
                    {
                    }
                }
                var jiraProjectMapping = new GetJiraProjectMappingResult(query.Id,
                    query.ProjectId, query.SubprojectId,
                    query.JiraProjectKey, query.JiraProjectName,
                    query.MapHierarchy,
                    hiererchyParentName,
                    query.HierarchyParentId,
                    hiererchyTypeName,
                    query.HierarchyTypeId
                    );
                return new QueryResponse<GetJiraProjectMappingResult>(jiraProjectMapping, jiraProjectMapping is GetJiraProjectMappingResult ? null : new QueryResponseMessage((int)MessageType.Information, $"No project found."));
            }
            return new QueryResponse<GetJiraProjectMappingResult>(null, new QueryResponseMessage((int)MessageType.Successful, $"No project found."));
        }

        public bool Validate(GetJiraProjectMapping query)
        {
            return true;
        }
    }
}