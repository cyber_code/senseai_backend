﻿using Core.Configuration;
using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Persistence.Process.RequirementPrioritiesModel;
using Queries.ProcessModule.Issues;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetIssueTypeFieldsAndAttributesHandler : IQueryHandler<GetIssueTypeFieldsAndAttributes, GetIssueTypeFieldsAndAttributesResult[]>
    {
        private readonly IDbContext _dbContext;
        private SenseAIConfig _configuration;

        public GetIssueTypeFieldsAndAttributesHandler(IDbContext dbContext, SenseAIConfig configuration)
        {
            _dbContext = dbContext;
            _configuration = configuration;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueTypeFieldsAndAttributesResult[]> Handle(GetIssueTypeFieldsAndAttributes queryObject)
        {
            var type = typeof(IssueData);
            PropertyInfo[] queryResult = type.GetProperties();

            var issueTypeFieldsList = new List<GetIssueTypeFieldsAndAttributesResult>();
            var listStatuses = new List<GetIssueTypeFieldsInfoResultValue>();
            var listPriorities = new List<GetIssueTypeFieldsInfoResultValue>();
            var listRequirementPriorities = new List<GetIssueTypeFieldsInfoResultValue>();

            foreach (StatusType val in Enum.GetValues(typeof(StatusType)))
            {
                listStatuses.Add(new GetIssueTypeFieldsInfoResultValue { Id = ((int)val).ToString(), Text = val.ToString() });
            }

            foreach (IssuePriority val in Enum.GetValues(typeof(IssuePriority)))
            {
                listPriorities.Add(new GetIssueTypeFieldsInfoResultValue { Id = ((int)val).ToString(), Text = val.ToString() });
            }

            foreach (var issueTypesField in queryResult)
            {
                bool required = false;
                Attribute attributeRequired = Attribute.GetCustomAttribute(issueTypesField, typeof(RequiredAttribute));
                if (attributeRequired != null)
                {
                    required = true;
                }
                switch (issueTypesField.Name)
                {
                    case "Status":
                        issueTypeFieldsList.Add(new GetIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = required,
                            Values = listStatuses.ToArray()
                        });
                        break;

                    case "Priority":
                        issueTypeFieldsList.Add(new GetIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = required,
                            Values = listPriorities.ToArray()
                        });
                        break;

                    default:
                        issueTypeFieldsList.Add(new GetIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = required,
                            Values = null
                        });
                        break;
                }
            }
            var exclude = _configuration.IssueFieldsToExclude;
            foreach (var item in exclude)
            {
                issueTypeFieldsList.RemoveAll(x => x.FieldName == item);
            }
            return new QueryResponse<GetIssueTypeFieldsAndAttributesResult[]>(issueTypeFieldsList.ToArray(), issueTypeFieldsList.ToArray() is GetIssueTypeFieldsAndAttributesResult[]? null : new QueryResponseMessage((int)MessageType.Information, $"No project found."));
        }

        public bool Validate(GetIssueTypeFieldsAndAttributes query)
        {
            if (query.IssueTypeId == Guid.Empty)
            {
                Errors.Add("Project id should be provided!");
                return false;
            }
            return true;
        }
    }
}