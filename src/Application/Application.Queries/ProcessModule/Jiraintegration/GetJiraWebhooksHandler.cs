﻿using Jira;
using Messaging.Queries;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Queries.ProcessModule.Jiraintegration
{
    public class GetJiraWebhooksHandler : IQueryHandler<GetJiraWebhooks, GetJiraWebhooksResult>
    {
        private readonly IJiraClient _jiraClient;
        private readonly IJiraProjectMappingRepository _jiraProjectMappingRepository;

        public GetJiraWebhooksHandler(IJiraClient jiraClient, IJiraProjectMappingRepository jiraProjectMappingRepository)
        {
            _jiraClient = jiraClient;
            _jiraProjectMappingRepository = jiraProjectMappingRepository;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraWebhooksResult> Handle(GetJiraWebhooks queryObject)
        {
            var projectMapping = _jiraProjectMappingRepository.GetJiraProjectMappingBySubprojectId(queryObject.subProjectId);
            var webhookName = $"{queryObject.subProjectId}-SAI-{projectMapping.JiraProjectKey}";
            var getJiraWebhooksResult = new GetJiraWebhooksResult();
            var result = _jiraClient.GetWebhooks(queryObject.subProjectId);
            foreach (var item in result)
            {
                if (item.Name == webhookName)
                {
                    getJiraWebhooksResult = new GetJiraWebhooksResult
                    {
                        Events = item.Events,
                        Filters = new Filters { IssueRelatedEventsSection = item.Filters.IssueRelatedEventsSection },
                        Name = item.Name,
                        Self = item.Self,
                        Url = item.Url,
                        Enabled = item.Enabled
                    };
                    break;
                }
            }
            return new QueryResponse<GetJiraWebhooksResult>(getJiraWebhooksResult, new QueryResponseMessage((int)MessageType.Successful, ""));
        }

        public bool Validate(GetJiraWebhooks query)
        {
            return true;
        }
    }
}