﻿using Jira;
using Messaging.Queries;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueTypesHandler : IQueryHandler<GetJiraIssueTypes, GetJiraIssueTypesResult[]>
    {
        private readonly IJiraClient _service;

        public GetJiraIssueTypesHandler(IJiraClient service)
        {
            _service = service;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIssueTypesResult[]> Handle(GetJiraIssueTypes queryObject)
        {
            IEnumerable<Atlassian.Jira.IssueType> serviceResult = _service.GetIssueTypesByProjectKey(queryObject.SubprojectId, queryObject.ProjectKey);
            List<GetJiraIssueTypesResult> jiraIssueTypeList = new List<GetJiraIssueTypesResult>();
            foreach (var issueTypes in serviceResult)
            {
                jiraIssueTypeList.Add(new GetJiraIssueTypesResult
                {
                    IssueTypeId = issueTypes.Id,
                    IssueTypeName = issueTypes.Name
                });
            }
            return new QueryResponse<GetJiraIssueTypesResult[]>(jiraIssueTypeList.ToArray(), jiraIssueTypeList.ToArray() is GetJiraIssueTypesResult[]? null : new QueryResponseMessage((int)MessageType.Information, $"No project found."));
        }

        public bool Validate(GetJiraIssueTypes query)
        {
            if (query.ProjectKey == string.Empty)
            {
                Errors.Add("Project id should be provided!");
                return false;
            }
            return true;
        }
    }
}