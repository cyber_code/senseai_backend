﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Jira;
using Queries.ProcessModule.JiraIntegration;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using Persistence.Process.IssuesModel;
using Persistence.AdminModel;
using Core.Configuration;

namespace Application.Queries.ProcessModule.Jiraintegration
{
    public sealed class GetJiraIntegrationSummaryHandler : IQueryHandler<GetJiraIntegrationSummary, GetJiraIntegrationSummaryResult>
    {
        private readonly IDbContext _dbContext;
        private SenseAIConfig _config;

        public GetJiraIntegrationSummaryHandler(IDbContext dbContext, SenseAIConfig senseAIConfig)
        {
            _dbContext = dbContext;
            _config = senseAIConfig;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIntegrationSummaryResult> Handle(GetJiraIntegrationSummary queryObject)
        {
            var result = from projectMapping in _dbContext.Set<JiraProjectMappingData>().AsNoTracking()
                         where projectMapping.Id == queryObject.ProjectMappingId
                         select new GetJiraIntegrationSummaryResult(
                             projectMapping.JiraProjectName,
                            (from subProject in _dbContext.Set<SubProjectData>().AsNoTracking()
                             where subProject.Id == Guid.Parse(projectMapping.SubprojectId.ToString())
                             select subProject).FirstOrDefault().Title,
                            (from issueType in _dbContext.Set<JiraIssueTypeMappingData>().AsNoTracking()
                             where issueType.JiraProjectMappingId == projectMapping.Id
                             select new IssueTypesSummary(
                                 issueType.JiraIssueTypeFieldName,
                                 Guid.Parse(issueType.SenseaiIssueTypeField) == _config.IssueTypeForRequirementId ? _config.RequirementTitle :
                                 (from issueT in _dbContext.Set<IssueTypeData>().AsNoTracking()
                                  where issueT.Id == Guid.Parse(issueType.SenseaiIssueTypeField)
                                  select issueT).FirstOrDefault().Title)).ToList(),
                             (from users in _dbContext.Set<JiraUserMappingData>().AsNoTracking()
                              where users.SubprojectId == projectMapping.SubprojectId
                              select users).Count()
                          );

            return new QueryResponse<GetJiraIntegrationSummaryResult>(result.FirstOrDefault());
        }

        public bool Validate(GetJiraIntegrationSummary query)
        {
            return true;
        }
    }
}