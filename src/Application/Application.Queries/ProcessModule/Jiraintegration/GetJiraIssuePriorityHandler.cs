﻿using System;
using System.Collections.Generic;
using Jira;
using Messaging.Queries;
using Persistence.Internal;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssuePriorityHandler : IQueryHandler<GetJiraIssuePriorities, GetJiraIssuePrioritiesResult[]>
    {
        private readonly IJiraClient _service;
        private readonly IDbContext _dbContext;

        public GetJiraIssuePriorityHandler(IJiraClient service, IDbContext dbContext)
        {
            _service = service;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIssuePrioritiesResult[]> Handle(GetJiraIssuePriorities queryObject)
        {
            var issueStatus = _service.GetIssuePriorities(queryObject.SubprojectId);
            List<GetJiraIssuePrioritiesResult> getJiraIssueResult = new List<GetJiraIssuePrioritiesResult>();
            foreach (var status in issueStatus)
            {
                getJiraIssueResult.Add(new GetJiraIssuePrioritiesResult
                {
                    Id = status.Id,
                    Name = status.Name
                });
            }

            return new QueryResponse<GetJiraIssuePrioritiesResult[]>(getJiraIssueResult.ToArray(), getJiraIssueResult.ToArray() is GetJiraIssuePrioritiesResult[]? null : new QueryResponseMessage((int)MessageType.Information, $"No status found."));
        }

        public object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public bool Validate(GetJiraIssuePriorities query)
        {
            return true;
        }
    }
}