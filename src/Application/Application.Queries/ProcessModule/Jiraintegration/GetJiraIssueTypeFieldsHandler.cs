﻿using Core.Configuration;
using Jira;
using Messaging.Queries;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;
using System.Collections.Generic;
using Integration = Jira.Issue;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueTypeFieldsHandler : IQueryHandler<GetJiraIssueTypeFieldsAndAttributes, GetJiraIssueTypeFieldsAndAttributesResult[]>
    {
        private readonly IJiraClient _service;
        private SenseAIConfig _configuration;

        public GetJiraIssueTypeFieldsHandler(IJiraClient service, SenseAIConfig configuration)
        {
            _service = service;
            _configuration = configuration;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIssueTypeFieldsAndAttributesResult[]> Handle(GetJiraIssueTypeFieldsAndAttributes queryObject)
        {
            var serviceResult = _service.GetIssueTypeFieldsByIssueType(queryObject.SubprojectId, queryObject.IssueTypeId, queryObject.JiraProjectKey);
            var jiraIssueTypeFieldsList = new List<GetJiraIssueTypeFieldsAndAttributesResult>();
            var listStatuses = new List<GetJiraIssueTypeFieldsInfoResultValue>();
            var listPriorities = new List<GetJiraIssueTypeFieldsInfoResultValue>();
            var listResolutions = new List<GetJiraIssueTypeFieldsInfoResultValue>();
            var statuses = _service.GetIssueStatuses(queryObject.SubprojectId, queryObject.JiraProjectKey);
            foreach (var status in statuses)
            {
                listStatuses.Add(new GetJiraIssueTypeFieldsInfoResultValue { Id = status.Id, Text = status.Name });
            }
            var priorities = _service.GetIssuePriorities(queryObject.SubprojectId);
            foreach (var priority in priorities)
            {
                listPriorities.Add(new GetJiraIssueTypeFieldsInfoResultValue { Id = priority.Id, Text = priority.Name });
            }
            var resolutions = _service.GetIssueResolutions(queryObject.SubprojectId);
            foreach (var resolution in resolutions)
            {
                listResolutions.Add(new GetJiraIssueTypeFieldsInfoResultValue { Id = resolution.Id, Text = resolution.Name });
            }
            foreach (var issueTypesField in serviceResult)
            {
                switch (issueTypesField.Name)
                {
                    case "Status":
                        jiraIssueTypeFieldsList.Add(new GetJiraIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = true,
                            Values = listStatuses.ToArray()
                        });
                        break;

                    case "Priority":
                        jiraIssueTypeFieldsList.Add(new GetJiraIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = issueTypesField.IsRequired,
                            Values = listPriorities.ToArray()
                        });
                        break;

                    case "Resolution":
                        jiraIssueTypeFieldsList.Add(new GetJiraIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = issueTypesField.IsRequired,
                            Values = listResolutions.ToArray()
                        });
                        break;

                    default:
                        jiraIssueTypeFieldsList.Add(new GetJiraIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = issueTypesField.IsRequired,
                            Values = null
                        });
                        break;
                }
            }

            var exclude = _configuration.JiraIssueFieldsToExclude;
            foreach (var item in exclude)
            {
                jiraIssueTypeFieldsList.RemoveAll(x => x.FieldName == item.Replace(" ", ""));
            }

            return new QueryResponse<GetJiraIssueTypeFieldsAndAttributesResult[]>(jiraIssueTypeFieldsList.ToArray(), jiraIssueTypeFieldsList.ToArray() is GetJiraIssueTypeFieldsAndAttributesResult[]? null : new QueryResponseMessage((int)MessageType.Information, $"No project found."));
        }

        public bool Validate(GetJiraIssueTypeFieldsAndAttributes query)
        {
            if (string.IsNullOrEmpty(query.IssueTypeId))
            {
                Errors.Add("Project id should be provided!");
                return false;
            }
            return true;
        }
    }
}