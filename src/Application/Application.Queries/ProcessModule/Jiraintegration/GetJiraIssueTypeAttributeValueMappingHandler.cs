﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Jira;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueTypeAttributeValueMappingHandler : IQueryHandler<GetJiraIssueTypeAttributeValueMapping, GetJiraIssueTypeAttributeValueMappingResult>
    {
        private readonly IJiraIssueTypeAttributeValueMappingRepository _repository;
        private readonly IDbContext _dbContext;

        public GetJiraIssueTypeAttributeValueMappingHandler(IJiraIssueTypeAttributeValueMappingRepository repository, IDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIssueTypeAttributeValueMappingResult> Handle(GetJiraIssueTypeAttributeValueMapping queryObject)
        {
            var query = from jiraIssueTypeAttributeValueMapping in _dbContext.Set<JiraIssueTypeAttributeValueMappingData>().AsNoTracking()
                        where jiraIssueTypeAttributeValueMapping.Id == queryObject.Id
                        select new GetJiraIssueTypeAttributeValueMappingResult(jiraIssueTypeAttributeValueMapping.Id, jiraIssueTypeAttributeValueMapping.JiraIssueTypeAttributeMappingId,
                                                                               jiraIssueTypeAttributeValueMapping.SenseaiValue, jiraIssueTypeAttributeValueMapping.JiraValue);

            return new QueryResponse<GetJiraIssueTypeAttributeValueMappingResult>(query.FirstOrDefault(), query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No JiraIssueTypeAttributeValueMapping found."));
        }

        public bool Validate(GetJiraIssueTypeAttributeValueMapping query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}