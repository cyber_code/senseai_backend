﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Jira;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueTypeMappingHandler : IQueryHandler<GetJiraIssueTypeMapping, GetJiraIssueTypeMappingResult>
    {
        private readonly IJiraIssueTypeMappingRepository _repository;
        private readonly IDbContext _dbContext;

        public GetJiraIssueTypeMappingHandler(IJiraIssueTypeMappingRepository repository, IDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIssueTypeMappingResult> Handle(GetJiraIssueTypeMapping queryObject)
        {
            var result = (from jiraIssueTypeMapping in _dbContext.Set<JiraIssueTypeMappingData>().AsNoTracking()
                          where jiraIssueTypeMapping.Id == queryObject.Id
                          select new GetJiraIssueTypeMappingResult(jiraIssueTypeMapping.Id,
                                 jiraIssueTypeMapping.JiraProjectMappingId,
                                 jiraIssueTypeMapping.SenseaiIssueTypeField,
                                 jiraIssueTypeMapping.JiraIssueTypeFieldId,
                                 jiraIssueTypeMapping.AllowAttachments,
                                 (from objJira in _dbContext.Set<JiraIssueTypeAttributeMappingData>().AsNoTracking()
                                  where objJira.JiraIssueTypeMappingId == queryObject.Id
                                  select new GetAttributeMapping
                                  {
                                      IsMandatory = objJira.Required,
                                      Index = objJira.Index,
                                      JiraValue = objJira.JiraField,
                                      SenseaiValue = objJira.SenseaiField,
                                      JiraIssueTypeMappingId = objJira.JiraIssueTypeMappingId,
                                      GetAttributeValuesMapping = (from objJiraValue in _dbContext.Set<JiraIssueTypeAttributeValueMappingData>().AsNoTracking()
                                                                   where objJiraValue.JiraIssueTypeAttributeMappingId == objJira.Id
                                                                   select new GetAttributeValuesMapping
                                                                   {
                                                                       AttributeMappingId = objJiraValue.JiraIssueTypeAttributeMappingId,
                                                                       JiraValue = objJiraValue.JiraValue,
                                                                       SenseaiValue = objJiraValue.SenseaiValue
                                                                   }).ToList()
                                  }).ToList()

                          )).FirstOrDefault();

            return new QueryResponse<GetJiraIssueTypeMappingResult>(result);
        }

        public bool Validate(GetJiraIssueTypeMapping query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}