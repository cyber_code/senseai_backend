﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Jira;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SenseAI.Domain;
using Persistence.Process.IssuesModel;
using Core.Configuration;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueTypeListByProjectMappingHandler : IQueryHandler<GetJiraIssueTypeListByProjectMapping, GetJiraIssueTypeListByProjectMappingResult[]>
    {
        private readonly SenseAIConfig _config;
        private readonly IDbContext _dbContext;

        public GetJiraIssueTypeListByProjectMappingHandler(SenseAIConfig senseAIConfig, IDbContext dbContext)
        {
            _config = senseAIConfig;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIssueTypeListByProjectMappingResult[]> Handle(GetJiraIssueTypeListByProjectMapping queryObject)
        {
            List<GetJiraIssueTypeListByProjectMappingResult> getResults = new List<GetJiraIssueTypeListByProjectMappingResult>();
            var query = (from jiraIssueTypeMapping in _dbContext.Set<JiraIssueTypeMappingData>().AsNoTracking()
                         join issueType in _dbContext.Set<IssueTypeData>().AsNoTracking()
                         on Guid.Parse(jiraIssueTypeMapping.SenseaiIssueTypeField) equals issueType.Id
                         where jiraIssueTypeMapping.JiraProjectMappingId == queryObject.JiraProjectMappingId
                         select new GetJiraIssueTypeListByProjectMappingResult(jiraIssueTypeMapping.Id,
                                                                  jiraIssueTypeMapping.JiraProjectMappingId,
                                                                  jiraIssueTypeMapping.SenseaiIssueTypeField,
                                                                  issueType.Title,
                                                                  jiraIssueTypeMapping.JiraIssueTypeFieldId,
                                                                  jiraIssueTypeMapping.JiraIssueTypeFieldName));
            getResults.AddRange(query);
            var req = (from jiraReqMapping in _dbContext.Set<JiraIssueTypeMappingData>().AsNoTracking()
                       where Guid.Parse(jiraReqMapping.SenseaiIssueTypeField) == _config.IssueTypeForRequirementId
                       && jiraReqMapping.JiraProjectMappingId == queryObject.JiraProjectMappingId
                       select new GetJiraIssueTypeListByProjectMappingResult(jiraReqMapping.Id,
                                                                jiraReqMapping.JiraProjectMappingId,
                                                                jiraReqMapping.SenseaiIssueTypeField,
                                                                _config.RequirementTitle,
                                                                jiraReqMapping.JiraIssueTypeFieldId,
                                                                jiraReqMapping.JiraIssueTypeFieldName)).ToList();
            getResults.AddRange(req);

            return new QueryResponse<GetJiraIssueTypeListByProjectMappingResult[]>(getResults.ToArray(), getResults.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No JiraIssueTypeMapping found."));
        }

        public bool Validate(GetJiraIssueTypeListByProjectMapping query)
        {
            if (query.JiraProjectMappingId == Guid.Empty)
            {
                Errors.Add("Project cannot be empty.");
                return false;
            }
            return true;
        }
    }
}