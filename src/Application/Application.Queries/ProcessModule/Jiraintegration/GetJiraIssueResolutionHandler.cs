﻿using System;
using System.Collections.Generic;
using Jira;
using Messaging.Queries;
using Persistence.Internal;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetJiraIssueResolutionHandler : IQueryHandler<GetJiraIssueResolutions, GetJiraIssueResolutionsResult[]>
    {
        private readonly IJiraClient _service;

        public GetJiraIssueResolutionHandler(IJiraClient service, IDbContext dbContext)
        {
            _service = service;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetJiraIssueResolutionsResult[]> Handle(GetJiraIssueResolutions queryObject)
        {
            var issueResolution = _service.GetIssueResolutions(queryObject.SubprojectId);
            List<GetJiraIssueResolutionsResult> getJiraResolutionResult = new List<GetJiraIssueResolutionsResult>();
            foreach (var status in issueResolution)
            {
                getJiraResolutionResult.Add(new GetJiraIssueResolutionsResult
                {
                    Id = status.Id,
                    Name = status.Name
                });
            }

            return new QueryResponse<GetJiraIssueResolutionsResult[]>(getJiraResolutionResult.ToArray(), getJiraResolutionResult.ToArray() is GetJiraIssueResolutionsResult[]? null : new QueryResponseMessage((int)MessageType.Information, $"No status found."));
        }

        public object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public bool Validate(GetJiraIssueResolutions query)
        {
            return true;
        }
    }
}