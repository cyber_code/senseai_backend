﻿using Core.Configuration;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Persistence.Process.RequirementPrioritiesModel;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetIssueTypeRequirementFieldsHandler : IQueryHandler<GetIssueTypeRequirementFieldsAndAttributes, GetIssueTypeFieldsAndAttributesResult[]>
    {
        private readonly IDbContext _dbContext;
        private SenseAIConfig _configuration;

        public GetIssueTypeRequirementFieldsHandler(IDbContext dbContext, SenseAIConfig configuration)
        {
            _dbContext = dbContext;
            _configuration = configuration;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueTypeFieldsAndAttributesResult[]> Handle(GetIssueTypeRequirementFieldsAndAttributes queryObject)
        {
            var type = typeof(ProcessData);
            PropertyInfo[] propertyInfo = type.GetProperties();

            var issueTypeFieldsList = new List<GetIssueTypeFieldsAndAttributesResult>();
            var listStatuses = new List<GetIssueTypeFieldsInfoResultValue>();
            var listPriorities = new List<GetIssueTypeFieldsInfoResultValue>();
            var listRequirementPriorities = new List<GetIssueTypeFieldsInfoResultValue>();

            foreach (StatusType val in Enum.GetValues(typeof(StatusType)))
            {
                listStatuses.Add(new GetIssueTypeFieldsInfoResultValue { Id = ((int)val).ToString(), Text = val.ToString() });
            }

            foreach (IssuePriority val in Enum.GetValues(typeof(IssuePriority)))
            {
                listPriorities.Add(new GetIssueTypeFieldsInfoResultValue { Id = ((int)val).ToString(), Text = val.ToString() });
            }

            foreach (var val in _dbContext.Set<RequirementPriorityData>()
                            .Where(item => item.SubProjectId == queryObject.SubprojectId)
                            .ToArray())
            {
                listRequirementPriorities.Add(new GetIssueTypeFieldsInfoResultValue { Id = val.Id.ToString(), Text = val.Title.ToString() });
            }

            foreach (var issueTypesField in propertyInfo)
            {
                bool required = false;
                Attribute attributeRequired = Attribute.GetCustomAttribute(issueTypesField, typeof(RequiredAttribute));
                if (attributeRequired != null)
                {
                    required = true;
                }
                switch (issueTypesField.Name)
                {
                    case "Status":
                        issueTypeFieldsList.Add(new GetIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = required,
                            Values = listStatuses.ToArray()
                        });
                        break;

                    case "RequirementPriorityId":
                        issueTypeFieldsList.Add(new GetIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = required,
                            Values = listRequirementPriorities.ToArray()
                        });
                        break;

                    case "Priority":
                        issueTypeFieldsList.Add(new GetIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = required,
                            Values = listStatuses.ToArray()
                        });
                        break;

                    default:
                        issueTypeFieldsList.Add(new GetIssueTypeFieldsAndAttributesResult
                        {
                            FieldName = issueTypesField.Name,
                            IsMandatory = required,
                            Values = null
                        });
                        break;
                }
            }
            var exclude = _configuration.RequirementFieldsToExclude;
            foreach (var item in exclude)
            {
                issueTypeFieldsList.RemoveAll(x => x.FieldName == item);
            }
            return new QueryResponse<GetIssueTypeFieldsAndAttributesResult[]>(issueTypeFieldsList.ToArray(), issueTypeFieldsList.ToArray() is GetIssueTypeFieldsAndAttributesResult[]? null : new QueryResponseMessage((int)MessageType.Information, $"No project found."));
        }

        public bool Validate(GetIssueTypeRequirementFieldsAndAttributes query)
        {
            return true;
        }
    }
}