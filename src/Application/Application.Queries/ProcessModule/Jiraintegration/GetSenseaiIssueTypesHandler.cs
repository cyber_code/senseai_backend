﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Queries.ProcessModule.JiraIntegration;
using Core.Configuration;

namespace Application.Queries.ProcessModule.JiraIntegration
{
    public sealed class GetSenseaiIssueTypesHandler : IQueryHandler<GetSenseaiIssueTypes, GetIssueTypesResult[]>
    {
        private readonly IDbContext _dbContext;
        private readonly SenseAIConfig _config;

        public GetSenseaiIssueTypesHandler(IDbContext dbContext, SenseAIConfig config)
        {
            _dbContext = dbContext;
            _config = config;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueTypesResult[]> Handle(GetSenseaiIssueTypes queryObject)
        {
            var query = (from issueType in _dbContext.Set<IssueTypeData>().AsNoTracking()
                         where issueType.SubProjectId == queryObject.SubProjectId
                         select new GetIssueTypesResult(issueType.Id, issueType.SubProjectId, issueType.Title, issueType.Description, issueType.Unit, issueType.Default)).ToList();
            var requirementItem = new GetIssueTypesResult(Guid.Parse(_config.IssueTypeForRequirementId.ToString()), queryObject.SubProjectId, _config.RequirementTitle, _config.RequirementTitle, "Story point", true);
            query.Add(requirementItem);
            return new QueryResponse<GetIssueTypesResult[]>(query.ToArray());
        }

        public bool Validate(GetSenseaiIssueTypes query)
        {
            if (query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}