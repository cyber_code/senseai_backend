﻿using Core.Abstractions;
using Jira;
using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.AdminModel;
using Persistence.Internal;
using Queries.ProcessModule.JiraIntegration;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Jiraintegration
{
    public class HasValidJiraCredentialsHandler : IQueryHandler<HasValidJiraCredentials, bool>
    {
        private readonly IDbContext _dbContext;
        private readonly IWorkContext _workContext;
        private readonly IJiraUserSubprojectRepository _jiraUserSubprojectRepository;
        private readonly IJiraClient _jiraClient;

        public HasValidJiraCredentialsHandler(IDbContext dbContext, IWorkContext workContext, IJiraUserSubprojectRepository jiraUserSubprojectRepository, IJiraClient jiraClient)
        {
            _dbContext = dbContext;
            _workContext = workContext;
            _jiraUserSubprojectRepository = jiraUserSubprojectRepository;
            _jiraClient = jiraClient;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<bool> Handle(HasValidJiraCredentials queryObject)
        {
            bool result = true;
            var subProject = (from subProj in _dbContext.Set<SubProjectData>().AsNoTracking()
                              where subProj.Id == queryObject.SubprojectId
                              select new
                              {
                                  subProj.JiraLink,
                                  subProj.IsJiraIntegrationEnabled
                              }).FirstOrDefault();

            if (subProject.IsJiraIntegrationEnabled || queryObject.SkipIsEnabled)
            {
                var jira = _jiraUserSubprojectRepository.GetJiraUserSubprojectConfigs(queryObject.SubprojectId, _workContext.FullName);

                if (jira == null)
                {
                    return new QueryResponse<bool>(false);
                }
                if (!string.IsNullOrEmpty(jira.JiraUsername) && !string.IsNullOrEmpty(jira.JiraToken))
                {
                    //valid info
                    result = _jiraClient.PingJira(jira.JiraUsername, jira.JiraToken, subProject.JiraLink);

                    return new QueryResponse<bool>(result);
                }
                else
                {
                    return new QueryResponse<bool>(false);
                }
            }
            else
            {
                return new QueryResponse<bool>(true);
            }
        }

        public bool Validate(HasValidJiraCredentials query)
        {
            if (query.SubprojectId == Guid.Empty)
            {
                Errors.Add("Subproject cannot be empty");
                return false;
            }
            return true;
        }
    }
}