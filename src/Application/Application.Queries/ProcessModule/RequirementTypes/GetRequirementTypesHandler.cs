﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.RequirementTypesModel;
using Queries.ProcessModule.RequirementTypes;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.RequirementTypes
{
    public class GetRequirementTypesHandler : IQueryHandler<GetRequirementTypes, GetRequirementTypesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetRequirementTypesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetRequirementTypesResult[]> Handle(GetRequirementTypes queryObject)
        {
            var query = (from requirementType in _dbContext.Set<RequirementTypeData>().AsNoTracking()
                         where requirementType.SubProjectId == queryObject.SubProjectId
                         select new GetRequirementTypesResult(requirementType.Id, requirementType.SubProjectId, requirementType.Title, requirementType.Code)).ToArray();

            return new QueryResponse<GetRequirementTypesResult[]>(query, 
                query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Requirement Types found in project!"));
        }

        public bool Validate(GetRequirementTypes query)
        {
            if(query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}
