﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.RequirementTypesModel;
using Queries.ProcessModule.RequirementTypes;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.RequirementTypes
{
    public class GetRequirementTypeHandler : IQueryHandler<GetRequirementType, GetRequirementTypeResult>
    {
        private readonly IDbContext _dbContext;

        public GetRequirementTypeHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetRequirementTypeResult> Handle(GetRequirementType queryObject)
        {
            var query = from requirementType in _dbContext.Set<RequirementTypeData>()
                        where requirementType.Id == queryObject.Id
                        select new GetRequirementTypeResult(requirementType.Id, requirementType.SubProjectId, requirementType.Title, requirementType.Code);

            return new QueryResponse<GetRequirementTypeResult>(query.FirstOrDefault(), query.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No Requirement Type found!"));
        }

        public bool Validate(GetRequirementType query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("RequirementTypeId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}
