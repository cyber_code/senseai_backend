﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.RequirementPrioritiesModel;
using Queries.ProcessModule.RequirementPriorities;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.RequirementPrioritys
{
    public class GetRequirementPriorityHandler : IQueryHandler<GetRequirementPriority, GetRequirementPriorityResult>
    {
        private readonly IDbContext _dbContext;

        public GetRequirementPriorityHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetRequirementPriorityResult> Handle(GetRequirementPriority queryObject)
        {
            var query = from requirementPriority in _dbContext.Set<RequirementPriorityData>()
                        where requirementPriority.Id == queryObject.Id
                     
                        select new GetRequirementPriorityResult(requirementPriority.Id, requirementPriority.SubProjectId, requirementPriority.Title, requirementPriority.Code,requirementPriority.Index);

            return new QueryResponse<GetRequirementPriorityResult>(query.FirstOrDefault(), query.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No Requirement Priority found!"));
        }

        public bool Validate(GetRequirementPriority query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("RequirementPriorityId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}
