﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.RequirementPrioritiesModel;
using Queries.ProcessModule.RequirementPriorities;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.RequirementPrioritys
{
    public class GetRequirementPrioritysHandler : IQueryHandler<GetRequirementPriorities, GetRequirementPrioritiesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetRequirementPrioritysHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetRequirementPrioritiesResult[]> Handle(GetRequirementPriorities queryObject)
        {
            var query = (from requirementPriority in _dbContext.Set<RequirementPriorityData>().AsNoTracking()
                         where requirementPriority.SubProjectId == queryObject.SubProjectId
                         orderby requirementPriority.Index
                         select new GetRequirementPrioritiesResult(requirementPriority.Id, requirementPriority.SubProjectId, requirementPriority.Title, requirementPriority.Code,requirementPriority.Index)).ToArray();

            return new QueryResponse<GetRequirementPrioritiesResult[]>(query, 
                query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Requirement Priorities found in project!"));
        }

        public bool Validate(GetRequirementPriorities query)
        {
            if(query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}
