﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssuTypeHandler : IQueryHandler<GetIssue, GetIssueResult>
    {
        private readonly IDbContext _dbContext;

        public GetIssuTypeHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueResult> Handle(GetIssue queryObject)
        {
            var query = from issue in _dbContext.Set<IssueData>()
                        where issue.Id == queryObject.Id
                        select new GetIssueResult(issue.Id, issue.ProcessId, issue.SubProjectId, issue.IssueTypeId, issue.AssignedId, issue.Title, issue.Description, (StatusType)issue.Status, issue.Estimate,issue.Reason,issue.Implication,issue.Comment,issue.TypicalId,issue.Attribute,issue.ApprovedForInvestigation,issue.InvestigationStatus,issue.ChangeStatus,issue.ReporterId,issue.ApprovedId,issue.DateSubmitted,issue.ApprovalDate,issue.StartDate, issue.Priority, issue.Severity, issue.DueDate, issue.Type, issue.ComponentId, issue.TestCaseId
                        );

            return new QueryResponse<GetIssueResult>(query.FirstOrDefault(), query.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No issue found."));
        }

        public bool Validate(GetIssue query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
