﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssuesByIssueTypeHandler : IQueryHandler<GetIssuesByIssueType, GetIssuesByIssueTypeResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetIssuesByIssueTypeHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssuesByIssueTypeResult[]> Handle(GetIssuesByIssueType queryObject)
        {
            var query = from issue in _dbContext.Set<IssueData>().AsNoTracking()
                        where  issue.SubProjectId == queryObject.SubProjectId
                        && issue.IssueTypeId==queryObject.IssueTypeId
                        select new GetIssuesByIssueTypeResult(issue.Id, issue.ProcessId, issue.SubProjectId, issue.IssueTypeId, issue.AssignedId, issue.Title, issue.Description, (StatusType)issue.Status,issue.Estimate, issue.Reason, issue.Implication, issue.Comment, issue.TypicalId, issue.Attribute, issue.ApprovedForInvestigation, issue.InvestigationStatus, issue.ChangeStatus, issue.ReporterId, issue.ApprovedId, issue.DateSubmitted, issue.ApprovalDate, issue.StartDate, issue.Priority, issue.Severity, issue.DueDate, issue.Type, issue.ComponentId, issue.TestCaseId);

            return new QueryResponse<GetIssuesByIssueTypeResult[]>(query.ToArray(), 
                query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No issues were found in type."));
        }

        public bool Validate(GetIssuesByIssueType query)
        {
           

            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.IssueTypeId == Guid.Empty)
            {
                Errors.Add("IssueTypeId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
