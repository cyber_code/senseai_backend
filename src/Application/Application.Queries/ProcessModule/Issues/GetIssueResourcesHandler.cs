﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq; 
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.Issues
{
    public sealed class GetIssueResourcesHandler : IQueryHandler<GetIssueResources, GetIssueResourcesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetIssueResourcesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get ; set ; }

        public QueryResponse<GetIssueResourcesResult[]> Handle(GetIssueResources queryObject)
        {
            var query = from issueResource in _dbContext.Set<IssueResourceData>()
                        where issueResource.IssueId == queryObject.IssueId
                        select new GetIssueResourcesResult(issueResource.Id, issueResource.IssueId, issueResource.Title, issueResource.Path);

            return new QueryResponse<GetIssueResourcesResult[]>(query.ToArray(), query.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No issue resources found."));

        }

        public bool Validate(GetIssueResources query)
        {
            if (query.IssueId == Guid.Empty)
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
