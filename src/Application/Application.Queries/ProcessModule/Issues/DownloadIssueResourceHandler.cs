﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.Issues;
using Queries.ProcessModule.ProcessResources;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public sealed class DownloadIssueResourceHandler : IQueryHandler<DownloadIssueResource, DownloadIssueResourceResult>
    {
        private readonly IDbContext _dbContext;

        public DownloadIssueResourceHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<DownloadIssueResourceResult> Handle(DownloadIssueResource queryObject)
        {
            var query = (from issueResource in _dbContext.Set<IssueResourceData>()
                         where issueResource.Id == queryObject.Id
                         select new
                         {
                             issueResource.Id,
                             issueResource.Title,
                             issueResource.IssueId,
                             issueResource.Path
                         }).FirstOrDefault();
            var file = File.Open(query.Path, FileMode.Open);
            var result = new DownloadIssueResourceResult(file, query.Title);

            return new QueryResponse<DownloadIssueResourceResult>(result, result is DownloadIssueResourceResult ? null :
                new QueryResponseMessage((int)MessageType.Information, $"No issue resource found."));

        }

        public bool Validate(DownloadIssueResource query)
        {
            if (query.Id == null || query.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
