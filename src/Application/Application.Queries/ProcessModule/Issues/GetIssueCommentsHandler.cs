﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.Issues
{
    public sealed class GetIssueCommentsHandler : IQueryHandler<GetIssueComments, GetIssueCommentsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetIssueCommentsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueCommentsResult[]> Handle(GetIssueComments queryObject)
        {
            var query = from issueCommnet in _dbContext.Set<IssueCommentData>()
                        where issueCommnet.IssueId == queryObject.IssueId
                        select new GetIssueCommentsResult(issueCommnet.Id, issueCommnet.IssueId, issueCommnet.Comment, issueCommnet.CommenterId, issueCommnet.ParentId);

            return new QueryResponse<GetIssueCommentsResult[]>(query.ToArray(), query.Any() ? null :
                new QueryResponseMessage((int)MessageType.Information, $"No issue comments found."));
        }

        public bool Validate(GetIssueComments query)
        {
            if (query.IssueId == Guid.Empty)
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
