﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Persistence.WorkflowModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssueWorkflowsHandler : IQueryHandler<GetIssueWorkflows, GetIssueWorkflowsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetIssueWorkflowsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueWorkflowsResult[]> Handle(GetIssueWorkflows query)
        {
            var result = from workflowIssue in _dbContext.Set<WorkflowIssueData>()
                         join workflow in _dbContext.Set<WorkflowData>() on workflowIssue.WorkflowId equals workflow.Id
                         where workflowIssue.IssueId == query.IssueId
                         select new GetIssueWorkflowsResult(workflow.Id, workflow.Title);

            QueryResponseMessage queryResponseMessage = null;
            if (!result.Any())
                queryResponseMessage = new QueryResponseMessage((int)MessageType.Information, $"No Workflow found linked with issue.");
            return new QueryResponse<GetIssueWorkflowsResult[]>(result.ToArray(), queryResponseMessage);
        }

        public bool Validate(GetIssueWorkflows query)
        {
            if (query.IssueId == Guid.Empty)
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
