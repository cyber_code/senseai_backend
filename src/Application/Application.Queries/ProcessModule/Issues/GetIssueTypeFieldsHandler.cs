﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssueTypeFieldsHandler : IQueryHandler<GetIssueTypeFields, GetIssueTypeFieldsResult>
    {
        private readonly IDbContext _dbContext;

        public GetIssueTypeFieldsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueTypeFieldsResult> Handle(GetIssueTypeFields queryObject)
        {
            var query = from issueTypeFields in _dbContext.Set<IssueTypeFieldsData>().AsNoTracking()
                        join issueTypeFieldsName in _dbContext.Set<IssueTypeFieldsNameData>().AsNoTracking()
                        on issueTypeFields.IssueTypeFieldsNameId equals issueTypeFieldsName.Id
                        where issueTypeFields.Id == queryObject.Id
                        select new GetIssueTypeFieldsResult(issueTypeFields.Id, issueTypeFields.IssueTypeId, issueTypeFields.IssueTypeFieldsNameId, issueTypeFields.Checked, issueTypeFieldsName.Name);

            return new QueryResponse<GetIssueTypeFieldsResult>(query.FirstOrDefault(), query.Any() ? null :
                new QueryResponseMessage((int)MessageType.Information, $"No issue type fields found."));
        }

        public bool Validate(GetIssueTypeFields query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            return true;
        }
    }
}