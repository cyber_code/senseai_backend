﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetWorkflowIssueHandler : IQueryHandler<GetWorkflowIssue, GetWorkflowIssueResult>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowIssueHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowIssueResult> Handle(GetWorkflowIssue query)
        {
            var result = from workflowIssue in _dbContext.Set<WorkflowIssueData>()
                        where workflowIssue.Id == query.Id
                        select new GetWorkflowIssueResult(workflowIssue.Id, workflowIssue.WorkflowId, workflowIssue.IssueId);

            QueryResponseMessage queryResponseMessage = null;
            if (!result.Any())
                queryResponseMessage = new QueryResponseMessage((int)MessageType.Information, $"No workflow issue found.");
            return new QueryResponse<GetWorkflowIssueResult>(result.FirstOrDefault(), queryResponseMessage);
        }

        public bool Validate(GetWorkflowIssue query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
