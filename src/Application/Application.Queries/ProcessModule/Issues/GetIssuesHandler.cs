﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssuesHandler : IQueryHandler<GetIssues, GetIssuesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetIssuesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssuesResult[]> Handle(GetIssues queryObject)
        {
            var query = from issue in _dbContext.Set<IssueData>().AsNoTracking()
                        where  issue.SubProjectId == queryObject.SubProjectId
                        select new GetIssuesResult(issue.Id, issue.ProcessId, issue.SubProjectId, issue.IssueTypeId, issue.AssignedId, issue.Title, issue.Description, (StatusType)issue.Status,issue.Estimate, issue.Reason, issue.Implication, issue.Comment, issue.TypicalId, issue.Attribute, issue.ApprovedForInvestigation, issue.InvestigationStatus, issue.ChangeStatus, issue.ReporterId, issue.ApprovedId, issue.DateSubmitted, issue.ApprovalDate, issue.StartDate, issue.Priority, issue.Severity, issue.DueDate, issue.Type, issue.ComponentId, issue.TestCaseId);

            return new QueryResponse<GetIssuesResult[]>(query.ToArray(), 
                query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No issues were found in process."));
        }

        public bool Validate(GetIssues query)
        {
           

            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
