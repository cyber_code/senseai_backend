﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Persistence.Process.SprintsModel;
using Persistence.WorkflowModel;
using Persistence.WorkflowModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetSprintIssuesTestCaseHandler : IQueryHandler<GetSprintIssuesTestCase, GetSprintIssuesTestCaseResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetSprintIssuesTestCaseHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSprintIssuesTestCaseResult[]> Handle(GetSprintIssuesTestCase query)
        {

            var result = from sprint in _dbContext.Set<SprintIssuesData>().AsNoTracking()
                         join issue in _dbContext.Set<IssueData>().AsNoTracking()
                        on sprint.IssueId equals issue.Id
                         join workflowIssue in _dbContext.Set<WorkflowIssueData>().AsNoTracking()
                        on issue.Id equals workflowIssue.IssueId
                         join testcase in _dbContext.Set<ExecutionResultData>().AsNoTracking()
                         on workflowIssue.WorkflowId equals testcase.WorkflowId
                         where sprint.SprintId == query.SprintId
                         group testcase by testcase.TestCaseId into testcase
                         
            select new GetSprintIssuesTestCaseResult(testcase.Key);
 
            QueryResponseMessage queryResponseMessage = null;
            if (!result.Any())
                queryResponseMessage = new QueryResponseMessage((int)MessageType.Information, $"No testCase found linked with sprint.");
            return new QueryResponse<GetSprintIssuesTestCaseResult[]>(result.ToArray(), queryResponseMessage);
        }

        public bool Validate(GetSprintIssuesTestCase query)
        {
            if (query.SprintId == Guid.Empty)
            {
                Errors.Add("SprintId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
