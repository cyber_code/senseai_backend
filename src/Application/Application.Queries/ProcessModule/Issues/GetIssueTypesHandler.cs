﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssueTypesHandler : IQueryHandler<GetIssueTypes, GetIssueTypesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetIssueTypesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueTypesResult[]> Handle(GetIssueTypes queryObject)
        {
            var query = (from issueType in _dbContext.Set<IssueTypeData>().AsNoTracking()
                         where issueType.SubProjectId == queryObject.SubProjectId
                         select new GetIssueTypesResult(issueType.Id, issueType.SubProjectId, issueType.Title, issueType.Description,issueType.Unit,issueType.Default)).ToArray();

            return new QueryResponse<GetIssueTypesResult[]>(query, 
                query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No issue types found in sub project."));
        }

        public bool Validate(GetIssueTypes query)
        {
            if(query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}
