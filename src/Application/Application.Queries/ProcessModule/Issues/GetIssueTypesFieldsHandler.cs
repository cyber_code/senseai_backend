﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssueTypesFieldsHandler : IQueryHandler<GetIssueTypesFields, GetIssueTypesFieldsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetIssueTypesFieldsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueTypesFieldsResult[]> Handle(GetIssueTypesFields queryObject)
        {
            var query = (from issueTypeFields in _dbContext.Set<IssueTypeFieldsData>().AsNoTracking()
                         join issueTypeFieldsName in _dbContext.Set<IssueTypeFieldsNameData>().AsNoTracking()
           on issueTypeFields.IssueTypeFieldsNameId equals issueTypeFieldsName.Id
                         where issueTypeFields.IssueTypeId == queryObject.IssueTypeId
                         select new GetIssueTypesFieldsResult(issueTypeFields.Id, issueTypeFields.IssueTypeId, issueTypeFields.IssueTypeFieldsNameId, issueTypeFields.Checked, issueTypeFieldsName.Name)).ToArray();
           
            return new QueryResponse<GetIssueTypesFieldsResult[]>(query, 
                query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No issue types fields found in issue type."));
        }

        public bool Validate(GetIssueTypesFields query)
        {
            if(query.IssueTypeId == null || query.IssueTypeId.Equals(Guid.Empty))
            {
                Errors.Add("IssueTypeId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}
