﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Persistence.Process.SprintsModel;
using Persistence.WorkflowModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetSprintIssuesWorkflowHandler : IQueryHandler<GetSprintIssuesWorkflow, GetSprintIssuesWorkflowResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetSprintIssuesWorkflowHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSprintIssuesWorkflowResult[]> Handle(GetSprintIssuesWorkflow query)
        {

            var result = from sprint in _dbContext.Set<SprintIssuesData>().AsNoTracking()
                         join issue in _dbContext.Set<IssueData>().AsNoTracking()
                        on sprint.IssueId equals issue.Id
                         join workflowIssue in _dbContext.Set<WorkflowIssueData>().AsNoTracking()
                        on  issue.Id equals workflowIssue.IssueId
                        join workflow in _dbContext.Set<WorkflowData>().AsNoTracking()
                        on workflowIssue.WorkflowId equals workflow.Id
                         where sprint.SprintId == query.SprintId
                         group workflow by workflow.Id into workflow
                         select new GetSprintIssuesWorkflowResult(workflow.Key);

            QueryResponseMessage queryResponseMessage = null;
            if (!result.Any())
                queryResponseMessage = new QueryResponseMessage((int)MessageType.Information, $"No workflow found linked with sprint.");
            return new QueryResponse<GetSprintIssuesWorkflowResult[]>(result.ToArray(), queryResponseMessage);
        }

        public bool Validate(GetSprintIssuesWorkflow query)
        {
            if (query.SprintId == Guid.Empty)
            {
                Errors.Add("SprintId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
