﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Application.Queries.ProcessModule.Issues
{
    public sealed class GetLinkedIssuesHandler : IQueryHandler<GetLinkedIssues, GetLinkedIssuesResult[]>
    {
        public List<string> Errors { get; set; }

        private readonly IDbContext _dbContext;

        public GetLinkedIssuesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetLinkedIssuesResult[]> Handle(GetLinkedIssues queryObject)
        {
            var result = new List<GetLinkedIssuesResult>();

          
            var queryLinkedTo = from issueLink in _dbContext.Set<IssueLinkData>().AsNoTracking()
                                join issue in _dbContext.Set<IssueData>().AsNoTracking()
                                on issueLink.DstIssueId equals issue.Id
                                where issueLink.SrcIssueId == queryObject.IssueId
                        select new GetLinkedIssuesResult(issueLink.DstIssueId, issueLink.LinkType, false, issue.Title);

            result.AddRange(queryLinkedTo);

            var queryLinkedFrom = from issueLink in _dbContext.Set<IssueLinkData>().AsNoTracking()
                                  join issue in _dbContext.Set<IssueData>().AsNoTracking()
                                  on issueLink.SrcIssueId equals issue.Id
                                  where issueLink.DstIssueId == queryObject.IssueId
                        select new GetLinkedIssuesResult(issueLink.SrcIssueId, issueLink.LinkType, false, issue.Title);
            
            result.AddRange(queryLinkedFrom);

            return new QueryResponse<GetLinkedIssuesResult[]>(result.ToArray());
        }

        public bool Validate(GetLinkedIssues query)
        {
            if(query.IssueId == null || query.IssueId.Equals(Guid.Empty))
            {
                Errors.Add("IssueId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}
