﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueTypeHandler : IQueryHandler<GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType, GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueTypeResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueTypeHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueTypeResult[]> Handle(GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType queryObject)
        {
            var query = from issue in _dbContext.Set<IssueData>().AsNoTracking()
                        where  issue.SubProjectId == queryObject.SubProjectId
                        && issue.IssueTypeId==queryObject.IssueTypeId 
                        && (issue.ProcessId==Guid.Empty ||issue.ProcessId==queryObject.ProcessId)
                        select new GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueTypeResult(issue.Id, issue.ProcessId, issue.SubProjectId, issue.IssueTypeId, issue.AssignedId, issue.Title, issue.Description, (StatusType)issue.Status, issue.Estimate,issue.Reason, issue.Implication, issue.Comment, issue.TypicalId, issue.Attribute, issue.ApprovedForInvestigation, issue.InvestigationStatus, issue.ChangeStatus, issue.ReporterId, issue.ApprovedId, issue.DateSubmitted, issue.ApprovalDate, issue.StartDate, issue.Priority, issue.Severity, issue.DueDate, issue.Type, issue.ComponentId);

            return new QueryResponse<GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueTypeResult[]>(query.ToArray(), 
                query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No issues were found in type."));
        }

        public bool Validate(GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType query)
        {
           

            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (query.IssueTypeId == Guid.Empty)
            {
                Errors.Add("IssueTypeId cannot be empty.");
                return false;
            }
            if (query.ProcessId == Guid.Empty)
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
