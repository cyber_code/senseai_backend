﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Persistence.Process.SprintsModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssuesForSprintPlannerWithFiltersHandler : IQueryHandler<GetIssuesForSprintPlannerWithFilters, GetIssuesForSprintPlannerWithFiltersResult>
    {
        private readonly IDbContext _dbContext;

        public GetIssuesForSprintPlannerWithFiltersHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssuesForSprintPlannerWithFiltersResult> Handle(GetIssuesForSprintPlannerWithFilters queryObject)
        {
            GetIssuesForSprintPlannerWithFiltersResult result = new GetIssuesForSprintPlannerWithFiltersResult();
            var query = from issue in _dbContext.Set<IssueData>().AsNoTracking()
                        where issue.SubProjectId == queryObject.SubProjectId && (issue.Status == StatusType.ToDo || issue.Status == StatusType.InProgress) &&
                       issue.ProcessId == (queryObject.ProcessId != Guid.Empty ? queryObject.ProcessId : issue.ProcessId) &&
                    
                       //    //issue.Title.ToLowerInvariant().Contains (( !String.IsNullOrEmpty( queryObject.SearchText) ? queryObject.SearchText : issue.Title).ToLowerInvariant()) &&
                       //  ((queryObject.AssigneeIds.Any() &&
                       //   queryObject.AssigneeIds.Contains(issue.AssignedId)) || (!queryObject.AssigneeIds.Any() &&
                       //   !queryObject.AssigneeIds.Contains(issue.AssignedId)))

                       // &&
                        !(from sprintissue in _dbContext.Set<SprintIssuesData>().AsNoTracking()
                             select sprintissue.IssueId).Contains(issue.Id)

                        select new GetOpenIssuesInBacklogResult(issue.Id, issue.ProcessId, issue.SubProjectId, issue.IssueTypeId, issue.AssignedId == null ? Guid.Empty : issue.AssignedId, issue.Title, issue.Description, (StatusType)issue.Status, issue.Estimate, issue.Reason, issue.Implication, issue.Comment, issue.TypicalId, issue.Attribute, issue.ApprovedForInvestigation, issue.InvestigationStatus, issue.ChangeStatus, issue.ReporterId, issue.ApprovedId, issue.DateSubmitted, issue.ApprovalDate, issue.StartDate, issue.Priority, issue.Severity, issue.DueDate, issue.Type, issue.ComponentId, issue.TestCaseId);
            result.BacklogIssues = query.ToArray();
            result.BacklogIssues = result.BacklogIssues.Where(x => x.Title.ToLowerInvariant().Contains((!String.IsNullOrEmpty(queryObject.SearchText) ? queryObject.SearchText : x.Title).ToLowerInvariant())).ToArray();
            if (queryObject.AssigneeIds.Any())
                result.BacklogIssues = result.BacklogIssues.Where(x => queryObject.AssigneeIds.Contains(x.AssignedId)).ToArray();

            var querySprint = from sprint in _dbContext.Set<SprintsData>().AsNoTracking()
                              join sprintIssues in _dbContext.Set<SprintIssuesData>().AsNoTracking()
                              on sprint.Id equals sprintIssues.SprintId
                              join issue in _dbContext.Set<IssueData>().AsNoTracking()
                              on sprintIssues.IssueId equals issue.Id

                              where sprint.SubProjectId == queryObject.SubProjectId &&
                              issue.ProcessId == (queryObject.ProcessId != Guid.Empty ? queryObject.ProcessId : issue.ProcessId) &&

                        //issue.Title.ToLowerInvariant().Contains((!String.IsNullOrEmpty(queryObject.SearchText) ? queryObject.SearchText : issue.Title).ToLowerInvariant()) &&
                        //   ((queryObject.AssigneeIds.Any() &&
                        //queryObject.AssigneeIds.Contains(issue.AssignedId)) || (!queryObject.AssigneeIds.Any() &&
                        //!queryObject.AssigneeIds.Contains(issue.AssignedId)))
                        // && 
                        sprint.Status != 2
                              orderby sprint.Title
                              select new GetIssueForOpenSprintsResult(issue.Id, issue.ProcessId, issue.SubProjectId, issue.IssueTypeId, issue.AssignedId == null?Guid.Empty:issue.AssignedId, issue.Title, issue.Description, (StatusType)issue.Status, issue.Estimate, issue.Reason, issue.Implication, issue.Comment, issue.TypicalId, issue.Attribute, issue.ApprovedForInvestigation, issue.InvestigationStatus, issue.ChangeStatus, issue.ReporterId, issue.ApprovedId, issue.DateSubmitted, issue.ApprovalDate, issue.StartDate, issue.Priority, issue.Severity, issue.DueDate, issue.Type, issue.ComponentId, sprint.Id,sprint.Title, issue.TestCaseId);


            result.SprintsIssues = querySprint.ToArray();
            result.SprintsIssues = result.SprintsIssues.Where(x => x.Title.ToLowerInvariant().Contains((!String.IsNullOrEmpty(queryObject.SearchText) ? queryObject.SearchText : x.Title).ToLowerInvariant())).ToArray();
            if (queryObject.AssigneeIds.Any())
                result.SprintsIssues = result.SprintsIssues.Where(x => queryObject.AssigneeIds.Contains(x.AssignedId)).ToArray();

            return new QueryResponse<GetIssuesForSprintPlannerWithFiltersResult>(result,
                query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No  issues for backlog or sprint were found for  SubProjectId: {queryObject.SubProjectId}"));
        }

        public bool Validate(GetIssuesForSprintPlannerWithFilters query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
