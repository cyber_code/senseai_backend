﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.PeopleModel;
using Persistence.Process.HierarchiesModel;
using Persistence.Process.IssuesModel;
using Persistence.Process.SprintsModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssuesWithDetailsHandler : IQueryHandler<GetIssuesWithDetails, GetIssuesWithDetailsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetIssuesWithDetailsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssuesWithDetailsResult[]> Handle(GetIssuesWithDetails queryObject)
        {
            var query = from issue in _dbContext.Set<IssueData>().AsNoTracking()
                        join issueType in _dbContext.Set<IssueTypeData>().AsNoTracking() on issue.IssueTypeId equals issueType.Id
                        join assignee in _dbContext.Set<PeopleData>().AsNoTracking() on issue.AssignedId equals assignee.Id into _issueAssignee
                        from assignee in _issueAssignee.DefaultIfEmpty()
                        join reporter in _dbContext.Set<PeopleData>().AsNoTracking() on issue.ReporterId equals reporter.Id into _issueReporter
                        from reporter in _issueReporter.DefaultIfEmpty()
                        join approver in _dbContext.Set<PeopleData>().AsNoTracking() on issue.ApprovedId equals approver.Id into _issueApprover
                        from approver in _issueApprover.DefaultIfEmpty()
                        join component in _dbContext.Set<HierarchyData>().AsNoTracking() on issue.ComponentId equals component.Id into _issueComponent
                        from component in _issueComponent.DefaultIfEmpty()
                        join sprintIssue in _dbContext.Set<SprintIssuesData>().AsNoTracking() on issue.Id equals sprintIssue.IssueId into _issueSprintIssue
                        from sprintIssue in _issueSprintIssue.DefaultIfEmpty()
                        join sprint in _dbContext.Set<SprintsData>().AsNoTracking() on sprintIssue.SprintId equals sprint.Id into _issueSprint
                        from sprint in _issueSprint.DefaultIfEmpty()
                        where  issue.SubProjectId == queryObject.SubProjectId
                        select new GetIssuesWithDetailsResult(
                            issue.Id,
                            issue.ProcessId,
                            issue.SubProjectId,
                            issueType.Id,
                            issueType.Title,
                            issue.AssignedId,
                            assignee.Name,
                            issue.Title,
                            issue.Description,
                            issue.Status,
                            (int)issue.Status < 0 ? "None" : issue.Status.ToString(),
                            issue.Estimate,
                            issue.Reason,
                            issue.Implication,
                            issue.Comment,
                            issue.TypicalId,
                            issue.Attribute,
                            issue.ApprovedForInvestigation,
                            issue.InvestigationStatus,
                            (int)issue.InvestigationStatus < 0 ? "None" : issue.InvestigationStatus.ToString(),
                            issue.ChangeStatus,
                            (int)issue.ChangeStatus < 0 ? "None" : issue.ChangeStatus.ToString(),
                            issue.ReporterId,
                            reporter.Name,
                            issue.ApprovedId,
                            approver.Name,
                            issue.DateSubmitted,
                            issue.ApprovalDate,
                            issue.StartDate,
                            issue.Priority,
                            (int)issue.Priority < 0 ? "None" : issue.Priority.ToString(),
                            issue.Severity,
                            (int)issue.Severity < 0 ? "None" : issue.Severity.ToString(),
                            issue.DueDate,
                            issue.Type,
                            (int)issue.Type < 0 ? "None" : issue.Type.ToString(),
                            issue.ComponentId,
                            component.Title,
                            sprint.Id == null ? Guid.Empty : sprint.Id,
                            sprint.Title,
                            issue.TestCaseId);

            return new QueryResponse<GetIssuesWithDetailsResult[]>(query.ToArray(), 
                query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No issues were found in subproject."));
        }

        public bool Validate(GetIssuesWithDetails query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
