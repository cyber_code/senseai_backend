﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssueTypeFieldsNamesHandler : IQueryHandler<GetIssueTypeFieldsNames, GetIssueTypeFieldsNamesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetIssueTypeFieldsNamesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueTypeFieldsNamesResult[]> Handle(GetIssueTypeFieldsNames queryObject)
        {
            var query = (from issueType in _dbContext.Set<IssueTypeFieldsNameData>().AsNoTracking()
                         select new GetIssueTypeFieldsNamesResult(issueType.Id, issueType.Name)).ToArray();

            return new QueryResponse<GetIssueTypeFieldsNamesResult[]>(query, 
                query.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No issue types fields name found "));
        }

        public bool Validate(GetIssueTypeFieldsNames query)
        {
           
            return true;
        }
    }
}
