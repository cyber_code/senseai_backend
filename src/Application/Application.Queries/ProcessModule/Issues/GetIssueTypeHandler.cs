﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssueTypeHandler : IQueryHandler<GetIssueType, GetIssueTypeResult>
    {
        private readonly IDbContext _dbContext;

        public GetIssueTypeHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueTypeResult> Handle(GetIssueType queryObject)
        {
            var query = from issueType in _dbContext.Set<IssueTypeData>()
                        where issueType.Id == queryObject.Id
                        select new GetIssueTypeResult(issueType.Id, issueType.SubProjectId, issueType.Title, issueType.Description, issueType.Unit,issueType.Default);

            return new QueryResponse<GetIssueTypeResult>(query.FirstOrDefault(), query.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No issue type found."));
        }

        public bool Validate(GetIssueType query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            return true;
        }
    }
}
