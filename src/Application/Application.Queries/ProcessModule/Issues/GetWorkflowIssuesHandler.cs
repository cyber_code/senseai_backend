﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetWorkflowIssuesHandler : IQueryHandler<GetWorkflowIssues, GetWorkflowIssuesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetWorkflowIssuesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetWorkflowIssuesResult[]> Handle(GetWorkflowIssues query)
        {
            var result = from workflowIssue in _dbContext.Set<WorkflowIssueData>()
                         join issue in _dbContext.Set<IssueData>() on workflowIssue.IssueId equals issue.Id
                         where workflowIssue.WorkflowId == query.WorkflowId
                         select new GetWorkflowIssuesResult(issue.Id, issue.Title);

            QueryResponseMessage queryResponseMessage = null;
            if (!result.Any())
                queryResponseMessage = new QueryResponseMessage((int)MessageType.Information, $"No Issue found linked with workflow.");
            return new QueryResponse<GetWorkflowIssuesResult[]>(result.ToArray(), queryResponseMessage);
        }

        public bool Validate(GetWorkflowIssues query)
        {
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
