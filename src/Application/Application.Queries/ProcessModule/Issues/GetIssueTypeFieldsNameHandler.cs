﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Queries.ProcessModule.Issues;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetIssueTypeFieldsNameHandler : IQueryHandler<GetIssueTypeFieldsName, GetIssueTypeFieldsNameResult>
    {
        private readonly IDbContext _dbContext;

        public GetIssueTypeFieldsNameHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetIssueTypeFieldsNameResult> Handle(GetIssueTypeFieldsName queryObject)
        {
            var query = from issueType in _dbContext.Set<IssueTypeFieldsNameData>()
                        where issueType.Id == queryObject.Id
                        select new GetIssueTypeFieldsNameResult(issueType.Id, issueType.Name);

            return new QueryResponse<GetIssueTypeFieldsNameResult>(query.FirstOrDefault(), query.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No issue type fields name found."));
        }

        public bool Validate(GetIssueTypeFieldsName query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            return true;
        }
    }
}
