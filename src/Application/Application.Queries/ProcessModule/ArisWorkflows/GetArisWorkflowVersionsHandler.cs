﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal; 
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;
using Queries.ProcessModule.ArisWorkflows;
using Persistence.Process.ArisWorkflowModel;

namespace Application.Queries.DesignModule
{
    public class GetArisWorkflowVersionsHandler : IQueryHandler<GetArisWorkflowVersions, GetArisWorkflowVersionsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetArisWorkflowVersionsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetArisWorkflowVersionsResult[]> Handle(GetArisWorkflowVersions queryObject)
        {
            var result = _dbContext.Set<ArisWorkflowVersionData>().AsNoTracking().
                    Where(ct => ct.ArisWorkflowId == queryObject.ArisWorkflowId).Select(workflowVersion => new GetArisWorkflowVersionsResult(
                       workflowVersion.ArisWorkflowId,
                               workflowVersion.VersionId,
                               workflowVersion.Title)).ToArray();

            return new QueryResponse<GetArisWorkflowVersionsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No ArisWorkflow Versions found."));
        }

        public bool Validate(GetArisWorkflowVersions query)
        {
            if (query.ArisWorkflowId == Guid.Empty)
            {
                Errors.Add("ArisWorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}