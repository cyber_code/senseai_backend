﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ArisWorkflowModel;
using Queries.ProcessModule.ArisWorkflows;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public sealed class GetArisWorkflowHandler : IQueryHandler<GetArisWorkflow, GetArisWorkflowResult>
    {
        private readonly IDbContext _dbContext;

        public List<string> Errors { get; set; }

        public GetArisWorkflowHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetArisWorkflowResult> Handle(GetArisWorkflow queryObject)
        {
            var arisWorkflow = _dbContext.Set<ArisWorkflowData>().AsNoTracking().
                FirstOrDefault(w => w.Id == queryObject.Id);

            return new QueryResponse<GetArisWorkflowResult>(new GetArisWorkflowResult(arisWorkflow.Id, arisWorkflow.ProcessId, arisWorkflow.Title,
                    arisWorkflow.Description, arisWorkflow.DesignerJson, arisWorkflow.IsParsed), arisWorkflow != null ? null : new QueryResponseMessage((int)MessageType.Information, $"Aris workflow not found in the process."));
        }

        public bool Validate(GetArisWorkflow query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
