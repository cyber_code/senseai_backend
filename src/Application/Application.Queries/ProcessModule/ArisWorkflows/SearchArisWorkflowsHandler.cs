﻿using Iveonik.Stemmers;
using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using StopWord;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;
using Queries.ProcessModule.ArisWorkflows;
using Persistence.Process.ProcessModel;
using Persistence.Process.ArisWorkflowModel;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public sealed class SearchArisWorkflowsHandler : IQueryHandler<SearchArisWorkflows, SearchArisWorkflowsResult[]>
    {
        private readonly IDbContext _dbContext;
        private List<SearchArisWorkflowsResult> container = new List<SearchArisWorkflowsResult>();

        public SearchArisWorkflowsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public static string WordStemming(string SearchPhrase)
        {
            string documentWithoutPunctuation = "";
            string final = null;
            foreach (var c in SearchPhrase)
            {
                if (!char.IsPunctuation(c))
                {
                    documentWithoutPunctuation += c;
                }
            }
            SearchPhrase = documentWithoutPunctuation;

            List<string> termStrings = SearchPhrase.ToLower().Split(' ').ToList();

            List<string> stemmedTerms = new List<string>();
            EnglishStemmer stemmer = new EnglishStemmer();
            foreach (var ts in termStrings)
            {
                stemmedTerms.Add(stemmer.Stem(ts));
            }
            List<string> terms = new List<string>();
            foreach (var stemmedterm in stemmedTerms.Distinct())
            {
                if (!terms.Any(i => i == stemmedterm))
                {
                    terms.Add(stemmedterm);
                }
            }
            final = String.Join(" ", terms);

            return WordStops(final);
        }

        public static string WordStops(string SearchPhrase)
        {
            var _stopWordslist = StopWords.GetStopWords();

            string[] wordList = SearchPhrase.Split(new char[] { '.', '?', '!', ' ', ';', ':', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(item => item.ToLowerInvariant()).ToArray();

            string final = null;

            var result = wordList.Except(_stopWordslist).ToArray();

            final = String.Join(" ", result);

            return final;
        }

        public QueryResponse<SearchArisWorkflowsResult[]> Handle(SearchArisWorkflows queryObject)
        {
            var sentence = WordStemming(queryObject.SearchString);

            string[] wordList = sentence.Split(new char[] { '.', '?', '!', ' ', ';', ':', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(item => item.ToLowerInvariant()).ToArray();

            var foldersWithPartialMatching = GetFoldersWithPartialMatching(queryObject, wordList);

            var workflowsWithPartialMatching = GetWorkflowsWithPartialMatching(queryObject, wordList);

            var workflowItemsWithPartialMatching = GetWorkflowItemsWithPartialMatching(queryObject, wordList);

            var foldersWithFullMatching = GetFoldersWithFullMatching(queryObject, foldersWithPartialMatching);

            var workflowsWithFullMatching = GetWorkflowsWithFullMatching(queryObject, workflowsWithPartialMatching);

            var workflowItemsWithFullMatching = GetWorkflowItemsWithFullMatching(queryObject, workflowItemsWithPartialMatching);

            var workflowItemTypicalsWithFullMatching = GetWorkflowItemTypicalsWithFullMatching(queryObject, workflowItemsWithPartialMatching);

            /******  SEARCH logic  *****************************
             * 1. Full Typical       /  5. Partial Typical
             * 2. Full WorkFlowItem  /  6. Partial WorkFlowItem
             * 3. Full WorkFlow      /  7. Partial WorkFlow
             * 4. Full Folder        /  8. Partial Folder
             */

            AddDistinct(workflowItemTypicalsWithFullMatching);

            AddDistinct(workflowItemsWithFullMatching);

            AddDistinct(workflowsWithFullMatching);

            AddDistinct(foldersWithFullMatching);

            AddDistinct(workflowItemsWithPartialMatching);

            AddDistinct(workflowsWithPartialMatching);

            AddDistinct(foldersWithPartialMatching);

            return new QueryResponse<SearchArisWorkflowsResult[]>(container.ToArray(), container.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Workflows found with SubProjectId: '{queryObject.SubProjectId}'."));
        }

        public bool Validate(SearchArisWorkflows query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }

        private Item[] GetFoldersWithPartialMatching(SearchArisWorkflows queryObject, string[] wordList)
        {
            var foldersPartialQuery =
                from process in _dbContext.Set<ProcessData>().AsNoTracking()
                join arisWorkflow in _dbContext.Set<ArisWorkflowData>().AsNoTracking()
                    on process.Id equals arisWorkflow.ProcessId
                where
                    wordList.Any(
                        word =>
                        (process.Title ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant())
                    )
                select new Item
                {
                    Id = arisWorkflow.Id,
                    Title = arisWorkflow.Title,
                    Description = arisWorkflow.Description,
                    ProcessId = arisWorkflow.ProcessId,
                    FolderTitle = arisWorkflow.Title,
                    FolderDescription = arisWorkflow.Description
                };

            return foldersPartialQuery.ToArray();
        }

        private Item[] GetWorkflowsWithPartialMatching(SearchArisWorkflows queryObject, string[] wordList)
        {
            var workflowsPartialQuery =
                from arisWorkflow in _dbContext.Set<ArisWorkflowData>().AsNoTracking()
                join process in _dbContext.Set<ProcessData>().AsNoTracking()
                    on arisWorkflow.ProcessId equals process.Id
                where
                     
                    wordList.Any(
                        word =>
                            (arisWorkflow.Title ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant())
                    )
                select new Item
                {
                    Id = arisWorkflow.Id,
                    Title = arisWorkflow.Title,
                    Description = arisWorkflow.Description,
                    ProcessId = arisWorkflow.ProcessId,
                    FolderTitle = arisWorkflow.Title,
                    FolderDescription = arisWorkflow.Description
                };

            return workflowsPartialQuery.ToArray();
        }

        private Item[] GetWorkflowItemsWithPartialMatching(SearchArisWorkflows queryObject, string[] wordList)
        {
            var workflowsPartialQuery =
                from process in _dbContext.Set<FolderData>().AsNoTracking()
                join arisWorkflow in _dbContext.Set<ArisWorkflowData>().AsNoTracking()
                    on process.Id equals arisWorkflow.ProcessId
                join arisWorkflowitem in _dbContext.Set<ArisWorkflowItemData>().AsNoTracking()
                    on arisWorkflow.Id equals arisWorkflowitem.ArisWorkflowId
                where
                    (process.SubProjectId == queryObject.SubProjectId) &&
                    wordList.Any(
                          word =>
                              (arisWorkflowitem.Title ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant()) ||
                              (arisWorkflowitem.Description ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant()) ||
                              (arisWorkflowitem.TypicalName ?? "").ToLowerInvariant().Contains(word.ToLowerInvariant())

                    )
                select new Item
                {
                    Id = arisWorkflow.Id,
                    Title = arisWorkflow.Title,
                    Description = arisWorkflow.Description,
                    ProcessId = arisWorkflow.ProcessId,
                    FolderTitle = process.Title,
                    FolderDescription = process.Description,
                    WorkflowItemTitle = arisWorkflowitem.Title,
                    WorkflowItemDescription = arisWorkflowitem.Description,
                    TypicalName = arisWorkflowitem.TypicalName
                };

            return workflowsPartialQuery.ToArray();
        }

        private Item[] GetFoldersWithFullMatching(SearchArisWorkflows queryObject, Item[] foldersWithPartialMatching)
        {
            return foldersWithPartialMatching
                .Where(
                    item =>
                        (item.FolderTitle ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant() ||
                        (item.FolderDescription ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant()
                )
                .ToArray();
        }

        private Item[] GetWorkflowsWithFullMatching(SearchArisWorkflows queryObject, Item[] workflowsWithPartialMatching)
        {
            return workflowsWithPartialMatching
                .Where(
                    item =>
                        (item.Title ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant() ||
                        (item.Description ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant()
                )
                .ToArray();
        }

        private Item[] GetWorkflowItemsWithFullMatching(SearchArisWorkflows queryObject, Item[] workflowsWithPartialMatching)
        {
            return workflowsWithPartialMatching
                .Where(
                    item =>
                        (item.WorkflowItemTitle ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant() ||
                        (item.WorkflowItemDescription ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant()
                )
                .ToArray();
        }

        private Item[] GetWorkflowItemTypicalsWithFullMatching(SearchArisWorkflows queryObject, Item[] workflowsWithPartialMatching)
        {
            return workflowsWithPartialMatching
                .Where(
                    item =>
                        (item.TypicalName ?? "").ToLowerInvariant() == queryObject.SearchString.ToLowerInvariant()
                )
                .ToArray();
        }

        private List<SearchArisWorkflowsResult> AddDistinct(Item[] items)
        {
            foreach (var item in items)
            {
                if (!container.Any(w => w.Id == item.Id))
                {
                    container.Add(
                        new SearchArisWorkflowsResult(item.Id, item.Title, item.Description, item.ProcessId)
                    );
                }
            }
            return container;
        }
    }

    internal sealed class Item
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public Guid ProcessId { get; set; }

        public string FolderTitle { get; set; }

        public string FolderDescription { get; set; }

        public string TypicalName { get; internal set; }

        public string WorkflowItemTitle { get; set; }

        public string WorkflowItemDescription { get; set; }
    }
}