﻿using Microsoft.EntityFrameworkCore;
using Messaging.Queries;
using Persistence.Internal;
using System;
using System.Linq;
using System.Collections.Generic;
using SenseAI.Domain;
using Persistence.Process.ArisWorkflowModel;
using Queries.ProcessModule.ArisWorkflows;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public class GetArisWorkflowVersionsByDateHandler : IQueryHandler<GetArisWorkflowVersionsByDate, GetArisWorkflowVersionsByDateResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetArisWorkflowVersionsByDateHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetArisWorkflowVersionsByDateResult[]> Handle(GetArisWorkflowVersionsByDate queryObject)
        {
            var query = from workflowVersion in _dbContext.Set<ArisWorkflowVersionData>().AsNoTracking()
                        where
                            workflowVersion.ArisWorkflowId == queryObject.ArisWorkflowId
                            &&
                            (workflowVersion.DateCreated >= queryObject.DateFrom && workflowVersion.DateCreated <= queryObject.DateTo)
                        select new
                        {
                            workflowVersion.ArisWorkflowId,
                            workflowVersion.VersionId,
                            workflowVersion.Title,
                            workflowVersion.Description,
                            workflowVersion.IsLast,
                            workflowVersion.WorkflowImage,
                            workflowVersion.DateCreated
                        };

            var result = query.Select(
                workflowVersion => new GetArisWorkflowVersionsByDateResult(
                    workflowVersion.ArisWorkflowId,
                            workflowVersion.VersionId,
                            workflowVersion.Title,
                            workflowVersion.Description,
                            workflowVersion.IsLast,
                            workflowVersion.WorkflowImage,
                            workflowVersion.DateCreated
                )
            ).ToArray();

            return new QueryResponse<GetArisWorkflowVersionsByDateResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, $"No Aris Workflow Versions found with WorkflowId: '{queryObject.ArisWorkflowId}' and DateCreated between '{queryObject.DateFrom}' and '{queryObject.DateTo}'."));
        }

        public bool Validate(GetArisWorkflowVersionsByDate query)
        {//TODO: validate queryObject.DateFrom and queryObject.DateTo
            if (query.ArisWorkflowId == Guid.Empty)
            {
                Errors.Add("ArisWorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}