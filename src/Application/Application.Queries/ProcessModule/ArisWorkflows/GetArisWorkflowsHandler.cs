﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ArisWorkflowModel;
using Queries.ProcessModule.ArisWorkflows;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public sealed class GetArisWorkflowsHandler : IQueryHandler<GetArisWorkflows, GetArisWorkflowResult>
    {
        private readonly IDbContext _dbContext;

        public List<string> Errors { get; set; }

        public GetArisWorkflowsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetArisWorkflowResult> Handle(GetArisWorkflows queryObject)
        {
            var arisWorkflow = _dbContext.Set<ArisWorkflowData>().AsNoTracking().
                  FirstOrDefault(w => w.ProcessId == queryObject.ProcessId);

            GetArisWorkflowResult result = null;
            if (arisWorkflow != null)
                result = new GetArisWorkflowResult(arisWorkflow.Id, arisWorkflow.ProcessId, arisWorkflow.Title, arisWorkflow.Description,
                    arisWorkflow.DesignerJson, arisWorkflow.IsParsed);
            return new QueryResponse<GetArisWorkflowResult>(result, arisWorkflow != null ? null : new QueryResponseMessage((int)MessageType.Information, $"Aris workflow not found in the process."));

        }

        public bool Validate(GetArisWorkflows query)
        {
            if (query.ProcessId == Guid.Empty)
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
