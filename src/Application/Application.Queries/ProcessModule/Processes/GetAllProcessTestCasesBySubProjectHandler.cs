﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Persistence.WorkflowModel;
using Persistence.WorkflowModel;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetAllProcessTestCasesBySubProjectHandler : IQueryHandler<GetAllProcessTestCasesBySubProject, GetAllProcessTestCasesBySubProjectResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetAllProcessTestCasesBySubProjectHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetAllProcessTestCasesBySubProjectResult[]> Handle(GetAllProcessTestCasesBySubProject queryObject)
        {
            var query = (from process in _dbContext.Set<ProcessData>().AsNoTracking() join processWorkflows in _dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
                                on process.Id equals processWorkflows.ProcessId
                                     join workflows in _dbContext.Set<WorkflowData>().AsNoTracking() on processWorkflows.WorkflowId equals workflows.Id
                                     join  testcase in _dbContext.Set<ExecutionResultData>().AsNoTracking()

                                          on workflows.Id equals testcase.WorkflowId
                                          where process.SubProjectId == queryObject.SubProjectId
                                select new GetAllProcessTestCasesBySubProjectResult(testcase.WorkflowId, testcase.TestCaseTitle,testcase.TestCaseId)).ToArray();
            List<GetAllProcessTestCasesBySubProjectResult> result = new List<GetAllProcessTestCasesBySubProjectResult>();


            return new QueryResponse<GetAllProcessTestCasesBySubProjectResult[]>(query, query.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No Testcase found in sub project."));
        }

        public bool Validate(GetAllProcessTestCasesBySubProject query)
        {
            if (query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
