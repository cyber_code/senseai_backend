﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.Processes
{
    public sealed class GetProcessHandler : IQueryHandler<GetProcess, GetProcessResult>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetProcessHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetProcessResult> Handle(GetProcess queryObject)
        {
            var result = from process in _dbContext.Set<ProcessData>()
                        where process.Id == queryObject.Id
                        select new GetProcessResult(
                            process.Id,
                            process.SubProjectId,
                            process.HierarchyId,
                            process.CreatedId,
                            process.OwnerId,
                            process.ProcessType,
                            process.Title,
                            process.Description,
                            process.RequirementPriorityId,
                            process.RequirementTypeId,
                            process.ExpectedNumberOfTestCases,
                            process.TypicalId
                        );


            return new QueryResponse<GetProcessResult>(result.FirstOrDefault(),
                result.FirstOrDefault() != null ? null : new QueryResponseMessage((int)MessageType.Information, $"No Process found."));
        }

        public bool Validate(GetProcess query)
        {
            if (query.Id.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}