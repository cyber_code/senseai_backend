﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Processes
{
    public sealed class GetProcessStatisticsHandler : IQueryHandler<GetProcessStatistics, GetProcessStatisticsResult>
    {
        private readonly IDbContext _dbContext;

        public GetProcessStatisticsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetProcessStatisticsResult> Handle(GetProcessStatistics queryObject)
        {
            var queryProcessWflows = from processWorkflows in _dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
                                     where processWorkflows.ProcessId == queryObject.ProcessId
                                     select new
                                     {
                                         processWorkflows.ProcessId,
                                         processWorkflows.Id
                                     };

            var queryIssues = (from issues in _dbContext.Set<IssueData>()
                              where issues.ProcessId == queryObject.ProcessId
                              select new
                              {
                                  issues.ProcessId,
                                  issues.Id,
                                  issues.Status
                              }).ToList();
            queryIssues.AddRange((from process in _dbContext.Set<ProcessData>().AsNoTracking()
                                  join processWorkflows in _dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
                                  on process.Id equals processWorkflows.ProcessId
                                  join workflowIssues in _dbContext.Set<WorkflowIssueData>().AsNoTracking()
                                 on processWorkflows.WorkflowId equals workflowIssues.WorkflowId

                                  join issues in _dbContext.Set<IssueData>().AsNoTracking()
                                  on workflowIssues.IssueId equals issues.Id

                                  where process.Id == queryObject.ProcessId
                                  select new
                                  {
                                      issues.ProcessId,
                                      issues.Id,
                                      issues.Status
                                  }).ToList());
            var qissues = queryIssues.Select(p => new { p.Id, p.Status }).Distinct();
            GetProcessStatisticsResult result = new GetProcessStatisticsResult(queryProcessWflows.Count(),
               qissues.Count(), qissues.Where(w => w.Status == (int)StatusType.ToDo || w.Status == StatusType.InProgress).Count());
            return new QueryResponse<GetProcessStatisticsResult>(result, result is GetProcessStatisticsResult ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No Process Statistics found."));
        }

        public bool Validate(GetProcessStatistics query)
        {
            if (query.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}