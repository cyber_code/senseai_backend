﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Persistence.Process.ProcessModel;
using Persistence.WorkflowModel;
using Persistence.WorkflowModel;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetAllProcessOpenIssuesBySubProjectHandler : IQueryHandler<GetAllProcessOpenIssuesBySubProject, GetAllProcessOpenIssuesBySubProjectResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetAllProcessOpenIssuesBySubProjectHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetAllProcessOpenIssuesBySubProjectResult[]> Handle(GetAllProcessOpenIssuesBySubProject queryObject)
        {
            var query = (from process in _dbContext.Set<ProcessData>().AsNoTracking()
                         join issues in _dbContext.Set<IssueData>().AsNoTracking()
on process.Id equals issues.ProcessId
                         where process.SubProjectId == queryObject.SubProjectId
                         && (issues.Status == (int)StatusType.ToDo || (int)issues.Status == (int)StatusType.InProgress)
                         select new GetAllProcessOpenIssuesBySubProjectResult(issues.Id, issues.Title, issues.Description)).ToList();

         query.AddRange((   from process in _dbContext.Set<ProcessData>().AsNoTracking()
            join processWorkflows in _dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
            on process.Id equals processWorkflows.ProcessId 
            join workflowIssues in _dbContext.Set<WorkflowIssueData>().AsNoTracking()
           on processWorkflows.WorkflowId equals workflowIssues.WorkflowId
           
            join issues in _dbContext.Set<IssueData>().AsNoTracking()
            on workflowIssues.IssueId equals issues.Id
  
            where process.SubProjectId == queryObject.SubProjectId 
                        && (issues.Status == (int)StatusType.ToDo || (int)issues.Status == (int)StatusType.InProgress)
            select new GetAllProcessOpenIssuesBySubProjectResult(issues.Id, issues.Title, issues.Description)).ToList());
         var x=   query.Select(p =>new { p.Id, p.Title, p.Description })
                                     .Distinct();
            var nb = x.Select(p=> new GetAllProcessOpenIssuesBySubProjectResult (p.Id, p.Title, p.Description)) ;
            return new QueryResponse<GetAllProcessOpenIssuesBySubProjectResult[]>(nb.ToArray(), query.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No Issues found in sub project."));
        }

        public bool Validate(GetAllProcessOpenIssuesBySubProject query)
        {
            if (query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}