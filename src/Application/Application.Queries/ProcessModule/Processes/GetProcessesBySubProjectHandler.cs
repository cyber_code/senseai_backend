﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;
using Microsoft.EntityFrameworkCore;

namespace Application.Queries.ProcessModule.Processes
{
    public class GetProcessesBySubProjectHandler : IQueryHandler<GetProcessesBySubProject, GetProcessesBySubProjectResult[]>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetProcessesBySubProjectHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetProcessesBySubProjectResult[]> Handle(GetProcessesBySubProject queryObject)
        {
            
            var result = _dbContext.Set<ProcessData>()
                .Where(p => p.SubProjectId == queryObject.SubProjectId)
                .AsNoTracking()
                .Select(process => new GetProcessesBySubProjectResult(
                              process.Id,
                              process.SubProjectId,
                              process.HierarchyId,
                              process.CreatedId,
                              process.OwnerId,
                              process.ProcessType,
                              process.Title,
                              process.Description,
                              process.RequirementPriorityId,
                              process.RequirementTypeId,
                              process.ExpectedNumberOfTestCases,
                              process.TypicalId
                          )).ToArray();

            return new QueryResponse<GetProcessesBySubProjectResult[]>(result,
            result.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
                $"No Processes found in sub project."));
        }

        public bool Validate(GetProcessesBySubProject query)
        {
            if (query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}