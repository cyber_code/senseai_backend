﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Processes
{
    public sealed class GetProcessWorkflowHandler : IQueryHandler<GetProcessWorkflow, GetProcessWorkflowResult>
    {
        private readonly IDbContext _dbContext;

        public GetProcessWorkflowHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetProcessWorkflowResult> Handle(GetProcessWorkflow queryObject)
        {
            var query = from processWorkflow in _dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
                        where processWorkflow.Id == queryObject.Id
                        select new GetProcessWorkflowResult(processWorkflow.Id, processWorkflow.ProcessId, processWorkflow.WorkflowId);

            var result = query.Select(processWorkflow => new GetProcessWorkflowResult(processWorkflow.Id, processWorkflow.ProcessId, processWorkflow.WorkflowId)).FirstOrDefault();

            return new QueryResponse<GetProcessWorkflowResult>(result, result is GetProcessWorkflowResult ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No process workflow is found."));
        }

        public bool Validate(GetProcessWorkflow query)
        {
            if (query.Id == null || query.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
