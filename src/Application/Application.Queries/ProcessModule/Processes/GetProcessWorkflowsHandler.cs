﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Persistence.WorkflowModel;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetProcessWorkflowsHandler : IQueryHandler<GetProcessWorkflows, GetProcessWorkflowsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetProcessWorkflowsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetProcessWorkflowsResult[]> Handle(GetProcessWorkflows queryObject)
        {
            var query = (from processWorkflows in _dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
                         join workflows in _dbContext.Set<WorkflowData>().AsNoTracking() on processWorkflows.WorkflowId equals workflows.Id
                         where processWorkflows.ProcessId == queryObject.ProcessId
                         orderby workflows.Title
                         select new GetProcessWorkflowsResult(processWorkflows.Id, workflows.Id, workflows.Title, workflows.Description)).ToArray();

            return new QueryResponse<GetProcessWorkflowsResult[]>(query, query.Any() ? null :
                new QueryResponseMessage((int)MessageType.Information, $"No workflows found in process."));
        }

        public bool Validate(GetProcessWorkflows query)
        {
            if (query.ProcessId == null || query.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
