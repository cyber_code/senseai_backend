﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Persistence.WorkflowModel;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Issues
{
    public class GetAllProcessWorkflowsBySubProjectHandler : IQueryHandler<GetAllProcessWorkflowsBySubProject, GetAllProcessWorkflowsBySubProjectResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetAllProcessWorkflowsBySubProjectHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetAllProcessWorkflowsBySubProjectResult[]> Handle(GetAllProcessWorkflowsBySubProject queryObject)
        {
            var query = (from process in _dbContext.Set<ProcessData>().AsNoTracking()
                         join processWorkflows in _dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
                         on process.Id equals processWorkflows.ProcessId
                         join workflows in _dbContext.Set<WorkflowData>().AsNoTracking() on processWorkflows.WorkflowId equals workflows.Id
                         where process.SubProjectId == queryObject.SubProjectId
                         group workflows by workflows.Id into workflow
                         select new GetAllProcessWorkflowsBySubProjectResult( workflow.Key)).ToArray();
         
            
            return new QueryResponse<GetAllProcessWorkflowsBySubProjectResult[]>(query,query.Any() ? null :
                new QueryResponseMessage((int)MessageType.Information, $"No Workflows found in sub project."));
        }

        public bool Validate(GetAllProcessWorkflowsBySubProject query)
        {
            if (query.SubProjectId == null || query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
