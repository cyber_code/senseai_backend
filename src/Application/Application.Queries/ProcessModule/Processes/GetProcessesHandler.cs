﻿using System;
using System.Linq;
using System.Collections.Generic;
using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.Processes
{
    public class GetProcessesHandler : IQueryHandler<GetProcesses, GetProcessesResult[]>
    {
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }

        public GetProcessesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public QueryResponse<GetProcessesResult[]> Handle(GetProcesses queryObject)
        {
            var query = from process in _dbContext.Set<ProcessData>()
                        where process.HierarchyId == queryObject.HierarchyId
                              && process.SubProjectId == queryObject.SubProjectId
                        select new
                        {
                            process.Id,
                            process.SubProjectId,
                            process.HierarchyId,
                            process.CreatedId,
                            process.OwnerId,
                            process.ProcessType,
                            process.Title,
                            process.Description,
                            process.RequirementPriorityId,
                            process.RequirementTypeId,
                            process.ExpectedNumberOfTestCases,
                            process.TypicalId
                        };

            var result = query.Select(process => new GetProcessesResult(process.Id,
                                                                        process.SubProjectId,
                                                                        process.HierarchyId,
                                                                        process.CreatedId,
                                                                        process.OwnerId,
                                                                        process.ProcessType,
                                                                        process.Title,
                                                                        process.Description,
                                                                        process.RequirementPriorityId,
                                                                        process.RequirementTypeId,
                                                                        process.ExpectedNumberOfTestCases,
                                                                        process.TypicalId)).ToArray();

            return new QueryResponse<GetProcessesResult[]>(result,
                result.Any() ? null : new QueryResponseMessage((int)MessageType.Information,
                    $"No Processes found in sub projectId."));
        }

        public bool Validate(GetProcesses query)
        {
            if (query.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (query.HierarchyId.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}