﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.ProcessResources;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Linq;

namespace Application.Queries.ProcessModule.ProcessResources
{
    public sealed class DownloadProcessResourcesHandler : IQueryHandler<DownloadProcessResources, DownloadProcessResourcesResult[]>
    {
        private readonly IDbContext _dbContext;

        public DownloadProcessResourcesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public List<string> Errors { get; set; }
        public QueryResponse<DownloadProcessResourcesResult[]> Handle(DownloadProcessResources queryObject)
        {

            var query = from processResource in _dbContext.Set<ProcessResourcesData>()
                        where processResource.ProcessId == queryObject.ProcessId
                        select new
                        {
                            processResource.Id,
                            processResource.FileName,
                            processResource.FilePath,
                            processResource.ProcessId
                        };
            List<FileStream> files = new List<FileStream>();

            byte[] fileBytes = null;
            foreach (var item in query)
            {
                if (File.Exists(item.FilePath))
                {
                    var file = File.Open(item.FilePath, FileMode.Open);
                    files.Add(file);
                }
            }
            if (files.Count() == 0)
                return new QueryResponse<DownloadProcessResourcesResult[]>(null, new QueryResponseMessage((int)MessageType.Information, "FilePath doesn't have file"));

            var memoryStream = new MemoryStream();
            using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
            {
                foreach (var item in files)
                {
                    var entry = archive.CreateEntry(Path.GetFileName(item.Name));
                    using (BinaryWriter writer = new BinaryWriter(entry.Open()))
                    {
                        int length = Convert.ToInt32(item.Length);
                        byte[] data = new byte[length];
                        item.Read(data, 0, length);
                        writer.Write(data); //write the binary data
                    }

                }

            }
            fileBytes = memoryStream.ToArray();

            var result = query.Select(processResources => new DownloadProcessResourcesResult(processResources.Id, processResources.FileName, processResources.ProcessId, fileBytes)).ToArray();

            return new QueryResponse<DownloadProcessResourcesResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No process resources found."));

        }

        public bool Validate(DownloadProcessResources query)
        {
            if (query.ProcessId == null || query.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("Process Id cannot be empty!");
                return false;
            }

            return true;
        }
    }

}
