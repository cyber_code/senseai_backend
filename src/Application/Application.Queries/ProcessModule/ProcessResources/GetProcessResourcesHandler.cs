﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.ProcessResources;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.ProcessResources
{
    public sealed class GetProcessResourcesHandler : IQueryHandler<GetProcessResources, GetProcessResourcesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetProcessResourcesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        } 
        public List<string> Errors { get; set; }

        public QueryResponse<GetProcessResourcesResult[]> Handle(GetProcessResources queryObject)
        {
            var query = from processResource in _dbContext.Set<ProcessResourcesData>()
                        where processResource.ProcessId == queryObject.ProcessId
                        select new
                        {
                            processResource.Id,
                            processResource.FileName,
                            processResource.ProcessId
                        };

            var result = query.Select(processResources => new GetProcessResourcesResult(processResources.Id, processResources.FileName, processResources.ProcessId)).ToArray();

            return new QueryResponse<GetProcessResourcesResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No process resources found."));

        }

        public bool Validate(GetProcessResources query)
        {
            if (query.ProcessId == null || query.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("Process Id cannot be empty!");
                return false;
            }

            return true;
        }
    }
}
