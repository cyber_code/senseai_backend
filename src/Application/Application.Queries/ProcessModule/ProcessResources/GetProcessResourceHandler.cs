﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.ProcessResources;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.ProcessResources
{
    public sealed class GetProcessResourceHandler : IQueryHandler<GetProcessResource, GetProcessResourceResult>
    {
        private readonly IDbContext _dbContext;

        public GetProcessResourceHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetProcessResourceResult> Handle(GetProcessResource queryObject)
        {
            var query = from processResource in _dbContext.Set<ProcessResourcesData>()
                        where processResource.Id == queryObject.Id
                        select new
                        {
                            processResource.Id,
                            processResource.FileName,
                            processResource.ProcessId
                        };

            var result = query.Select(processResource => new GetProcessResourceResult(processResource.Id, processResource.FileName, processResource.ProcessId)).FirstOrDefault();

            return new QueryResponse<GetProcessResourceResult>(result, result is GetProcessResourceResult ? null : 
                new QueryResponseMessage((int)MessageType.Information, $"No process resource found."));

        }

        public bool Validate(GetProcessResource query)
        {
            if (query.Id == null || query.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
