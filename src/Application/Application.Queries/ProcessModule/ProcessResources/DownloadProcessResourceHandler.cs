﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using Queries.ProcessModule.ProcessResources;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Application.Queries.ProcessModule.ProcessResources
{
    public sealed class DownloadProcessResourceHandler : IQueryHandler<DownloadProcessResource, DownloadProcessResourceResult>
    {
        private readonly IDbContext _dbContext;

        public DownloadProcessResourceHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<DownloadProcessResourceResult> Handle(DownloadProcessResource queryObject)
        {
            var query = (from processResource in _dbContext.Set<ProcessResourcesData>()
                         where processResource.Id == queryObject.Id
                         select new
                         {
                             processResource.Id,
                             processResource.FileName,
                             processResource.ProcessId,
                             processResource.FilePath
                         }).FirstOrDefault();
            var file = File.Open(query.FilePath, FileMode.Open);
            var result = new DownloadProcessResourceResult(file, query.FileName);

            return new QueryResponse<DownloadProcessResourceResult>(result, result is DownloadProcessResourceResult ? null :
                new QueryResponseMessage((int)MessageType.Information, $"No process resource found."));

        }

        public bool Validate(DownloadProcessResource query)
        {
            if (query.Id == null || query.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
