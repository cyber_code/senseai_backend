﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.SprintsModel;
using Queries.ProcessModule.Sprints;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Sprints
{
    public class GetOpenSprintBySubProjectHandler : IQueryHandler<GetOpenSprintBySubProject, GetOpenSprintBySubProjectResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetOpenSprintBySubProjectHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetOpenSprintBySubProjectResult[]> Handle(GetOpenSprintBySubProject queryObject)
        {
            var query = from sprint in _dbContext.Set<SprintsData>().AsNoTracking()
                        where sprint.SubProjectId == queryObject.SubProjectId
                        && sprint.Start.CompareTo(DateTime.Now)<1 && (sprint.End.CompareTo(DateTime.Now)>=1||
                        sprint.End.CompareTo(new DateTime(1,1,1))==0)
                        orderby sprint.Title
                        select new
                        {
                            sprint.Id,
                            sprint.Title,
                            sprint.Description,
                            sprint.Start,
                            sprint.End,
                            sprint.SubProjectId,
                            sprint.Duration,
                            sprint.DurationUnit
                        };

            var result = query.Select(sprint => new GetOpenSprintBySubProjectResult(sprint.Id, sprint.Title, sprint.Description, sprint.Start, sprint.End, sprint.SubProjectId, sprint.Duration, sprint.DurationUnit)).ToArray();

            return new QueryResponse<GetOpenSprintBySubProjectResult[]>(result, result.Any() ? null : 
                new QueryResponseMessage((int)MessageType.Information, "No open sprints found."));
           
        }

        public bool Validate(GetOpenSprintBySubProject query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}