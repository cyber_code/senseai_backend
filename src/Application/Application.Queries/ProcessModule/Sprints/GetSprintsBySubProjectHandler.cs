﻿using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.SprintsModel;
using Queries.ProcessModule.Sprints;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Sprints
{
    public class GetSprintsBySubProjectHandler : IQueryHandler<GetSprintsBySubProject, GetSprintsBySubProjectResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetSprintsBySubProjectHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSprintsBySubProjectResult[]> Handle(GetSprintsBySubProject queryObject)
        {
            var query = from sprint in _dbContext.Set<SprintsData>()
                        where sprint.SubProjectId == queryObject.SubProjectId
                        select new
                        {
                            sprint.Id,
                            sprint.Title,
                            sprint.Description,
                            sprint.Start,
                            sprint.End,
                            sprint.SubProjectId,
                            sprint.Duration,
                            sprint.DurationUnit
                        };

            var result = query.Select(sprint => new GetSprintsBySubProjectResult(sprint.Id, sprint.Title, sprint.Description, sprint.Start, sprint.End, sprint.SubProjectId, sprint.Duration, sprint.DurationUnit)).ToArray();

            return new QueryResponse<GetSprintsBySubProjectResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No Sprints found."));
        }

        public bool Validate(GetSprintsBySubProject query)
        {
            if (query.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}