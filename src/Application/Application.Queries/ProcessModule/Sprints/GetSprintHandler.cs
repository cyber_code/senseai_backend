﻿using Messaging.Queries;
using Persistence.Internal;
using Persistence.Process.SprintsModel;
using Queries.ProcessModule.Sprints;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Sprints
{
    public class GetSprintHandler : IQueryHandler<GetSprint, GetSprintResult>
    {
        private readonly IDbContext _dbContext;

        public GetSprintHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSprintResult> Handle(GetSprint queryObject)
        {
            var query = from sprint in _dbContext.Set<SprintsData>()
                        where sprint.Id == queryObject.Id
                        select new
                        {
                            sprint.Id,
                            sprint.Title,
                            sprint.Description,
                            sprint.Start,
                            sprint.End,
                            sprint.SubProjectId,
                            sprint.Duration,
                            sprint.DurationUnit
                        };

            var result = query.Select(sprint => new GetSprintResult(sprint.Id, sprint.Title, sprint.Description, sprint.Start, sprint.End, sprint.SubProjectId, sprint.Duration, sprint.DurationUnit)).FirstOrDefault();

            return new QueryResponse<GetSprintResult>(result, result is GetSprintResult ? null :
                new QueryResponseMessage((int)MessageType.Information, $"No sprint found."));
        }

        public bool Validate(GetSprint query)
        {
            if (query.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}