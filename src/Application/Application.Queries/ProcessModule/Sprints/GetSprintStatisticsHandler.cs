﻿using Messaging.Queries;
using Persistence.Internal;
using Queries.ProcessModule.Hierarchies;
using System;
using System.Collections.Generic;
using System.Linq;
using Persistence.Process.SprintsModel;
using Queries.ProcessModule.Sprints;
using Persistence.Process.ProcessModel;
using Persistence.Process.IssuesModel;
using Persistence.WorkflowModel;
using Persistence.WorkflowModel;
using SenseAI.Domain;

namespace Application.Queries.ProcessModule.Sprints
{
    public sealed class GetSprintStatisticsHandler : IQueryHandler<GetSprintStatistics, GetSprintStatisticsResult[]>
    {

        private readonly IDbContext _dbContext;

        public GetSprintStatisticsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSprintStatisticsResult[]> Handle(GetSprintStatistics queryObject)
        {
            var queryIssues = from issues in _dbContext.Set<IssueData>()
                              join sprintIssues in _dbContext.Set<SprintIssuesData>() on issues.Id equals sprintIssues.IssueId
                              where sprintIssues.SprintId == queryObject.SprintId
                              select new
                              {
                                  issues.Id,
                                  sprintIssues.Status,
                                  issues.ProcessId
                              };
            var processIds = queryIssues.Select(i => i.ProcessId).Distinct().ToList();

            var result = new List<GetSprintStatisticsResult>();
            foreach(var processId in processIds)
            {
                Guid hierarchyId = (from processes in _dbContext.Set<ProcessData>()
                                   where processes.Id == processId
                                   select processes.HierarchyId).First();

                var queryProcessIssues = queryIssues.Where(i => i.ProcessId == processId);
                var issueIds = queryProcessIssues.Where(i => i.ProcessId == processId).Select(i => i.Id).ToList();

                var queryWorkflows = from workflows in _dbContext.Set<WorkflowData>()
                                     join workflowIssues in _dbContext.Set<WorkflowIssueData>() on workflows.Id equals workflowIssues.WorkflowId
                                     where issueIds.Contains(workflowIssues.IssueId)
                                     select new
                                     {
                                         workflows.Id
                                     };
                var workflowIds = queryWorkflows.Select(wf => wf.Id).ToList();

                var queryTestcases = from testcases in _dbContext.Set<WorkflowTestCasesData>()
                                     where workflowIds.Contains(testcases.WorkflowId)
                                     select new
                                     {
                                         testcases.Id
                                     };

                int totalIssues = queryProcessIssues.Count();
                int openIssues = queryProcessIssues.Where(i => i.Status == (int)StatusType.ToDo || i.Status == (int)StatusType.InProgress).Count();
                int numWorkflows = queryWorkflows.Count();
                int numTestcases = queryTestcases.Count();

                result.Add(new GetSprintStatisticsResult(
                        hierarchyId,
                        processId,
                        totalIssues,
                        openIssues,
                        numWorkflows,
                        numTestcases));
            }


            return new QueryResponse<GetSprintStatisticsResult[]>(result.ToArray(),
                result.Any() ? null : new QueryResponseMessage((int)MessageType.Information , $"No process items found for sprint."));
        }

        public bool Validate(GetSprintStatistics query)
        {
            if(query.SprintId == null || query.SprintId.Equals(Guid.Empty))
            {
                Errors.Add("SprintId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
