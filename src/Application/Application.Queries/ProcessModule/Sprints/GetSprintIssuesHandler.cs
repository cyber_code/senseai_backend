﻿using Messaging.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Persistence.Process.SprintsModel;
using Queries.ProcessModule.Sprints;
using SenseAI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Sprints
{
    public class GetSprintIssuesHandler : IQueryHandler<GetSprintIssues, GetSprintIssuesResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetSprintIssuesHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSprintIssuesResult[]> Handle(GetSprintIssues queryObject)
        {
            var query = from sprint in _dbContext.Set<SprintIssuesData>().AsNoTracking()
                        join issue in _dbContext.Set<IssueData>().AsNoTracking()
                        on sprint.IssueId  equals issue.Id
                        where sprint.SprintId == queryObject.SprintId
                        select new
                        {
                            sprint.Id,
                            sprint.SprintId,
                            sprint.IssueId,
                            sprint.Status,
                            issue.ProcessId
                        };

            var result = query.Select(sprint => new GetSprintIssuesResult(sprint.Id, sprint.SprintId, sprint.IssueId,sprint.Status,sprint.ProcessId)).ToArray();

            return new QueryResponse<GetSprintIssuesResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No Issues found."));
        }

        public bool Validate(GetSprintIssues query)
        {
            if (query.SprintId == Guid.Empty)
            {
                Errors.Add("SprintId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}