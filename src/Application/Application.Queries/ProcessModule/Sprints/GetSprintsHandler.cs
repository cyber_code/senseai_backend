﻿using Messaging.Queries;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.SprintsModel;
using Queries.ProcessModule.Sprints;
using SenseAI.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Application.Queries.ProcessModule.Sprints
{
    public sealed class GetSprintsHandler : IQueryHandler<GetSprints, GetSprintsResult[]>
    {
        private readonly IDbContext _dbContext;

        public GetSprintsHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public QueryResponse<GetSprintsResult[]> Handle(GetSprints queryObject)
        {
            var query = from sprint in _dbContext.Set<SprintsData>()
                        select new
                        {
                            sprint.Id,
                            sprint.Title,
                            sprint.Description,
                            sprint.Start,
                            sprint.End,
                            sprint.SubProjectId,
                            sprint.Duration,
                            sprint.DurationUnit
                        };

            var result = query.Select(sprint => new GetSprintsResult(sprint.Id, sprint.Title, sprint.Description, sprint.Start,sprint.End,sprint.SubProjectId, sprint.Duration, sprint.DurationUnit)).ToArray();

            return new QueryResponse<GetSprintsResult[]>(result, result.Any() ? null : new QueryResponseMessage((int)MessageType.Information, "No Sprints found."));
        }

        public bool Validate(GetSprints query)
        {
            return true;
        }
    }
}