﻿
using Messaging.Commands;
using System.Collections.Generic;
using Commands.PeopleModule;
using SenseAI.Domain.PeopleModel;
using System;

namespace Application.Commands.PeopleModule
{
    public sealed class AddPeopleHandler : ICommandHandler<AddPeople, AddPeopleResult>
    {
        private readonly IPeopleRepository _repository;

        public AddPeopleHandler(IPeopleRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddPeopleResult> Handle(AddPeople command)
        {
            People people = new People(command.RoleId, command.Name, command.Surname, command.Email, command.Address,command.UserIdFromAdminPanel, command.SubProjectId, command.Color);

            _repository.Add(people);

            return new CommandResponse<AddPeopleResult>(new AddPeopleResult(people.Id));
        }

        public bool Validate(AddPeople command)
        {
            if (command.RoleId == null || command.RoleId.Equals(Guid.Empty))
            {
                Errors.Add("RoleId cannot be empty!");
                return false;
            }

            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be eplty!");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Name))
            {
                Errors.Add("Name cannot be empty!");
                return false;
            }
            return true;
        }
    }
}