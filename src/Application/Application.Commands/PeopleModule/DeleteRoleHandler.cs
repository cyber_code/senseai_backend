﻿
using Messaging.Commands;
using System;
using System.Collections.Generic;
using Commands.PeopleModule;
using SenseAI.Domain.PeopleModel;
using Persistence.Internal;
using Persistence.PeopleModel;
using System.Linq;

namespace Application.Commands.PeopleModule
{
    public sealed class DeleteRoleHandler : ICommandHandler<DeleteRole, bool>
    {
        private readonly IRoleRepository _repository;
        private readonly IDbContext _dbContext;

        public DeleteRoleHandler(IRoleRepository repository, IDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteRole command)
        {
            bool tmpResult = false;
            _repository.Delete(command.Id);
            tmpResult = true;

            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(DeleteRole command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            var query = from people in _dbContext.Set<PeopleData>()
                        where people.RoleId == command.Id
                        select new
                        {
                            people.Id,
                            people.RoleId,
                            people.Name,
                            people.Surname,
                            people.Email,
                            people.Address
                        };
            if (query.Count() > 0)
            { Errors.Add("Can not delete Role because it is connected to Users");
                return false;
            }
            return true;
        }
    }
}