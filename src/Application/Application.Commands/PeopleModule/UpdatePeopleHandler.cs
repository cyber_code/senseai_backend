﻿
using Messaging.Commands;
using System;
using System.Collections.Generic;
using Commands.PeopleModule;
using SenseAI.Domain.PeopleModel;

namespace Application.Commands.PeopleModule
{
    public sealed class UpdatePeopleHandler : ICommandHandler<UpdatePeople, bool>
    {
        private readonly IPeopleRepository _repository;

        public UpdatePeopleHandler(IPeopleRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdatePeople command)
        {
            bool tmpResult = false;
            People people = new People(command.Id, command.RoleId, command.Name, command.Surname, command.Email, command.Address, command.UserIdFromAdminPanel, command.SubProjectId, command.Color);

            _repository.Update(people);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(UpdatePeople command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (command.RoleId == Guid.Empty)
            {
                Errors.Add("RoleId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Name))
            {
                Errors.Add("Name cannot be empty.");
                return false;
            }
            
            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}