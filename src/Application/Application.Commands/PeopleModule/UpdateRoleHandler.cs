﻿
using Messaging.Commands;
using System;
using System.Collections.Generic;
using Commands.PeopleModule;
using SenseAI.Domain.PeopleModel;

namespace Application.Commands.PeopleModule
{
    public sealed class UpdateRoleHandler : ICommandHandler<UpdateRole, bool>
    {
        private readonly IRoleRepository _repository;

        public UpdateRoleHandler(IRoleRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateRole command)
        {
            bool tmpResult = false;
            Role role = new Role(command.Id, command.Title, command.Description, command.SubProjectId);

            _repository.Update(role);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(UpdateRole command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}