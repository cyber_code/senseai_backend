﻿
using Messaging.Commands;
using System.Collections.Generic;
using Commands.PeopleModule;
using SenseAI.Domain.PeopleModel;
using System;

namespace Application.Commands.PeopleModule
{
    public sealed class AddRoleHandler : ICommandHandler<AddRole, AddRoleResult>
    {
        private readonly IRoleRepository _repository;

        public AddRoleHandler(IRoleRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddRoleResult> Handle(AddRole command)
        {
            Role role = new Role(command.Title, command.Description, command.SubProjectId);

            _repository.Add(role);

            return new CommandResponse<AddRoleResult>(new AddRoleResult(role.Id));
        }

        public bool Validate(AddRole command)
        {
            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}