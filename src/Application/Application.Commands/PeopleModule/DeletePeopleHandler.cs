﻿
using Messaging.Commands;
using System;
using System.Collections.Generic;
using Commands.PeopleModule;
using SenseAI.Domain.PeopleModel;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using System.Linq;
using Persistence.Process.ProcessModel;

namespace Application.Commands.PeopleModule
{
    public sealed class DeletePeopleHandler : ICommandHandler<DeletePeople, bool>
    {
        private readonly IPeopleRepository _repository;
        private readonly IDbContext _dbContext;
        public DeletePeopleHandler(IPeopleRepository repository, IDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeletePeople command)
        {
            bool tmpResult = false;

            _repository.Delete(command.Id);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(DeletePeople command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            var query = from issue in _dbContext.Set<IssueData>()
                        where issue.AssignedId == command.Id
                        select new
                        {
                            issue.Id,
                            
                        };
            if (query.Count() > 0)
            {
                Errors.Add("Can not delete Assignee because it is connected to Issue");
                return false;
            }
            var query2 = from issuecomment in _dbContext.Set<IssueCommentData>()
                        where issuecomment.CommenterId == command.Id
                        select new
                        {
                            issuecomment.Id,

                        };
            if (query2.Count() > 0)
            {
                Errors.Add("Can not delete Assignee because it is connected to IssueComment");
                return false;
            }
            var query3 = from process in _dbContext.Set<ProcessData>()
                         where process.CreatedId == command.Id ||process.OwnerId==command.Id
                         select new
                         {
                             process.Id,

                         };
            if (query3.Count() > 0)
            {
                Errors.Add("Can not delete Assignee because it is connected to Requirement");
                return false;
            }
            return true;
        }
    }
}