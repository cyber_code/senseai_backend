﻿using Commands.ProcessModule.Processes;
using Core.Configuration;
using DataExtraction;
using Messaging.Commands;
using Persistence.Internal;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Linq;
using System.Collections.Generic;
using Persistence.Process.ProcessModel;
using Microsoft.EntityFrameworkCore;
using Persistence.AdminModel;

namespace Application.Commands.ProcessModule.Processes
{
    public sealed class DeleteProcessWorkflowHandle : ICommandHandler<DeleteProcessWorkflow, bool>
    {
        private readonly IProcessWorkflowRepository _processWorkflowRepository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;
        private SenseAIConfig _configuration;

        public DeleteProcessWorkflowHandle(IProcessWorkflowRepository processWorkflowRepository, IDataExtractionClient dataExtractionClient, IDbContext dbContext, SenseAIConfig configuration)
        {
            _processWorkflowRepository = processWorkflowRepository;
            _dataExtractionClient = dataExtractionClient;
            _dbContext = dbContext;
            _configuration = configuration;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteProcessWorkflow command)
        {
            var processResult = (from process in _dbContext.Set<ProcessWorkflowsData>().AsNoTracking()
                                 where process.Id == command.Id
                                 select process).FirstOrDefault();

            _processWorkflowRepository.Delete(command.Id);

            var data = _dbContext.Set<ProcessData>()
                .Where(item => item.Id == processResult.ProcessId)
                .Select(item => new { item.ExternalSystemId, item.SubProjectId })
                .First();

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == data.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.UpdateRequirementStatus(new DataExtraction.JiraModel.UpdateRequirementStatusPostModel(processResult.ProcessId));
            }

            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteProcessWorkflow command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}