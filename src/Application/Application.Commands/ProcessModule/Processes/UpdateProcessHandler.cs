﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Processes;
using SenseAI.Domain;
using SenseAI.Domain.Process.ProcessModel;
using DataExtraction;
using Core.Configuration;
using Persistence.Internal;
using Persistence.AdminModel;
using System.Linq;

namespace Application.Commands.ProcessModule.Processes
{
    public sealed class UpdateProcessHandler : ICommandHandler<UpdateProcess, bool>
    {
        private readonly IProcessRepository _processRepository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;
        private SenseAIConfig _configuration;

        public UpdateProcessHandler(IProcessRepository processRepository, IDataExtractionClient dataExtractionClient, SenseAIConfig configuration, IDbContext dbContext)
        {
            _processRepository = processRepository;
            _dataExtractionClient = dataExtractionClient;
            _configuration = configuration;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateProcess command)
        {
            Process process = new Process(command.Id,
                                          command.SubProjectId,
                                          command.HierarchyId,
                                          command.CreatedId,
                                          command.OwnerId,
                                          command.ProcessType,
                                          command.Title,
                                          command.Description,
                                          command.RequirementPriorityId,
                                          command.RequirementTypeId,
                                          command.ExpectedNumberOfTestCases,
                                          command.TypicalId);

            _processRepository.Update(process);

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == command.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.UpdateRequirement(
                    new DataExtraction.JiraModel.UpdateRequirementPostModel(
                        process.Id,
                        _configuration.IssueTypeForRequirementId,
                        _configuration.PeopleFields
                    )
                );
            }

            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateProcess command)
        {
            if (command.Id.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty!");
                return false;
            }

            if (command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (command.HierarchyId.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty!");
                return false;
            }

            if (command.CreatedId.Equals(Guid.Empty))
            {
                Errors.Add("CreatedId cannot be empty!");
                return false;
            }

            //if (command.OwnerId.Equals(Guid.Empty))
            //{
            //    Errors.Add("OwnerId cannot be empty!");
            //    return false;
            //}

            if (!(command.ProcessType.Equals(ProcessType.Generic)
                  || command.ProcessType.Equals(ProcessType.Aris)))
            {
                Errors.Add("ProcessType must be either Generic or Aris Type!");
                return false;
            }

            if (string.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }
            return true;
        }
    }
}