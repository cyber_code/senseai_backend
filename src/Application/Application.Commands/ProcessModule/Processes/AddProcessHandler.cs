﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Processes;
using SenseAI.Domain;
using SenseAI.Domain.Process.ProcessModel;
using DataExtraction;
using Core.Configuration;
using Persistence.Internal;
using Persistence.AdminModel;
using System.Linq;

namespace Application.Commands.ProcessModule.Processes
{
    public sealed class AddProcessHandler : ICommandHandler<AddProcess, AddProcessResult>
    {
        private readonly IProcessRepository _processRepository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;
        private SenseAIConfig _configuration;

        public AddProcessHandler(IProcessRepository processRepository, IDataExtractionClient dataExtractionClient, IDbContext dbContext, SenseAIConfig configuration)
        {
            _processRepository = processRepository;
            _dataExtractionClient = dataExtractionClient;
            _dbContext = dbContext;
            _configuration = configuration;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddProcessResult> Handle(AddProcess command)
        {
            Process process = new Process(command.SubProjectId,
                                          command.HierarchyId,
                                          command.CreatedId,
                                          command.OwnerId,
                                          command.ProcessType,
                                          command.Title,
                                          command.Description,
                                          command.RequirementPriorityId,
                                          command.RequirementTypeId,
                                          command.ExpectedNumberOfTestCases,
                                          command.TypicalId);

            _processRepository.Add(process);

            var subproject = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == command.SubProjectId);

            var iSJiraIntegrationEnabled = false;
            if(subproject.Any())
                iSJiraIntegrationEnabled = subproject.Select(item => item.IsJiraIntegrationEnabled == null ? false : item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.AddRequirement(
                    new DataExtraction.JiraModel.AddRequirementPostModel(
                        process.Id,
                        _configuration.IssueTypeForRequirementId,
                        _configuration.PeopleFields
                    )
                );
            }

            return new CommandResponse<AddProcessResult>(new AddProcessResult(process.Id));
        }

        public bool Validate(AddProcess command)
        {
            if (command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (command.HierarchyId.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty!");
                return false;
            }

            if (command.CreatedId.Equals(Guid.Empty))
            {
                Errors.Add("CreatedId cannot be empty!");
                return false;
            }

            //if (command.OwnerId.Equals(Guid.Empty))
            //{
            //    Errors.Add("OwnerId cannot be empty!");
            //    return false;
            //}

            if (!(command.ProcessType.Equals(ProcessType.Generic)
                  || command.ProcessType.Equals(ProcessType.Aris)))
            {
                Errors.Add("ProcessType must be either Generic or Aris Type!");
                return false;
            }

            if (string.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }
            return true;
        }
    }
}