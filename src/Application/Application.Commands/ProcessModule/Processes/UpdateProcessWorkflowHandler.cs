﻿using Commands.ProcessModule.Processes;
using Core.Configuration;
using DataExtraction;
using Messaging.Commands;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.ProcessModule.Processes
{
    public class UpdateProcessWorkflowHandler : ICommandHandler<UpdateProcessWorkflow, bool>
    {
        private readonly IProcessWorkflowRepository _repository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;
        private SenseAIConfig _configuration;

        public UpdateProcessWorkflowHandler(IProcessWorkflowRepository repository, IDataExtractionClient dataExtractionClient, SenseAIConfig configuration, IDbContext dbContext)
        {
            _repository = repository;
            _dataExtractionClient = dataExtractionClient;
            _configuration = configuration;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateProcessWorkflow command)
        {
            bool result = false;
            ProcessWorkflow processWorkflow = new ProcessWorkflow(command.Id, command.ProcessId, command.WorkflowId);
            _repository.Update(processWorkflow);


            var data = _dbContext.Set<ProcessData>()
                .Where(item => item.Id == command.Id)?
                .Select(item => new { item.ExternalSystemId, item.SubProjectId })
                .FirstOrDefault();
            bool? iSJiraIntegrationEnabled = false;
            if(data != null)
            { 
             iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == data.SubProjectId)?
                .Select(item => item.IsJiraIntegrationEnabled)
                .FirstOrDefault();
            }

            if (iSJiraIntegrationEnabled == true)
            {
                _dataExtractionClient.UpdateRequirementStatus(new DataExtraction.JiraModel.UpdateRequirementStatusPostModel(command.ProcessId));
            }
            result = true;
            return new CommandResponse<bool>(result);
        }

        public bool Validate(UpdateProcessWorkflow command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (command.ProcessId == null || command.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }

            if (command.WorkflowId == null || command.WorkflowId.Equals(Guid.Empty))
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}