﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Processes;
using SenseAI.Domain.Process.ProcessModel;
using SenseAI.Domain;
using DataExtraction;
using Core.Configuration;
using DataExtraction.JiraModel;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using System.Linq;
using Persistence.AdminModel;

namespace Application.Commands.ProcessModule.Processes
{
    public sealed class DeleteProcessHandler : ICommandHandler<DeleteProcess, bool>
    {
        private readonly IProcessRepository _processRepository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;
        private SenseAIConfig _configuration;

        public DeleteProcessHandler(IProcessRepository processRepository, IDataExtractionClient dataExtractionClient, IDbContext dbContext, SenseAIConfig configuration)
        {
            _processRepository = processRepository;
            _dataExtractionClient = dataExtractionClient;
            _dbContext = dbContext;
            _configuration = configuration;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteProcess command)
        {
            DeleteRequirementPostModel jiraModel = null;

            var data = _dbContext.Set<ProcessData>()
                .Where(item => item.Id == command.Id)
                .Select(item => new { item.ExternalSystemId, item.SubProjectId })
                .First();

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == data.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                jiraModel = new DeleteRequirementPostModel(command.Id, data.SubProjectId, data.ExternalSystemId);
            }

            var result = _processRepository.Delete(command.Id, command.ForceDelete);

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.DeleteRequirement(jiraModel);
            }

            if (!command.ForceDelete)
            {
                string message = "";

                if (result == true)
                    message = "Process with processId: " + command.Id + " is removed.";
                else
                    message = "Process with processId: " + command.Id + " has child element. Process is not removed.";

                return new CommandResponse<bool>(result, new CommandResponseMessage((int)MessageType.Information, message));
            }

            return new CommandResponse<bool>(result);
        }

        public bool Validate(DeleteProcess command)
        {
            if (command.Id.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}