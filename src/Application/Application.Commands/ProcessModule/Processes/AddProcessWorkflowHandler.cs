﻿using Commands.ProcessModule.Processes;
using Core.Configuration;
using DataExtraction;
using Messaging.Commands;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.ProcessModule.Processes
{
    public sealed class AddProcessWorkflowHandler : ICommandHandler<AddProcessWorkflow, AddProcessWorkflowResult>
    {
        private readonly IProcessWorkflowRepository _processWorkflowRepository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;
        private SenseAIConfig _configuration;

        public AddProcessWorkflowHandler(IProcessWorkflowRepository processWorkflowRepository, IDataExtractionClient dataExtractionClient, SenseAIConfig configuration, IDbContext dbContext)
        {
            _processWorkflowRepository = processWorkflowRepository;
            _dataExtractionClient = dataExtractionClient;
            _configuration = configuration;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddProcessWorkflowResult> Handle(AddProcessWorkflow command)
        {
            ProcessWorkflow processWorkflow = new ProcessWorkflow(command.ProcessId, command.WorkflowId);
            _processWorkflowRepository.Add(processWorkflow);

            var data = _dbContext.Set<ProcessData>()
                .Where(item => item.Id == command.ProcessId)
                .Select(item => new { item.SubProjectId })
                .First();

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == data.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.UpdateRequirementStatus(new DataExtraction.JiraModel.UpdateRequirementStatusPostModel(processWorkflow.ProcessId));
            }

            return new CommandResponse<AddProcessWorkflowResult>(new AddProcessWorkflowResult(processWorkflow.Id));
        }

        public bool Validate(AddProcessWorkflow command)
        {
            if (command.ProcessId == null || command.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }

            if (command.WorkflowId == null || command.WorkflowId.Equals(Guid.Empty))
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}