﻿using System;
using System.Collections.Generic;
using System.Linq;
using Messaging.Commands;
using Commands.ProcessModule.Processes;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.ProcessModel;

namespace Application.Commands.ProcessModule.Processes
{
    public sealed class ChangeProcessHierarchyHandler : ICommandHandler<ChangeProcessHierarchy, bool>
    {
        private readonly IDbContext _dbContext;
        private readonly IProcessRepository _repository;
        public List<string> Errors { get; set; }

        public ChangeProcessHierarchyHandler(IDbContext dbContext, IProcessRepository repository)
        {
            _dbContext = dbContext;
            _repository = repository;
        }

        public CommandResponse<bool> Handle(ChangeProcessHierarchy command)
        {
            ProcessData processData = _dbContext.Set<ProcessData>()
                                                .AsNoTracking()
                                                .FirstOrDefault(hD => hD.Id == command.ProcessId);

            if (processData == null)
            {
                throw new Exception("This Process does not exists!");
            }

            Process process = new Process(processData.Id,
                                          processData.SubProjectId,
                                          command.HierarchyId,
                                          processData.CreatedId,
                                          processData.OwnerId,
                                          processData.ProcessType,
                                          processData.Title,
                                          processData.Description,
                                          processData.RequirementPriorityId,
                                          processData.RequirementTypeId,
                                          processData.ExpectedNumberOfTestCases,
                                          processData.TypicalId);

            _repository.Update(process);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(ChangeProcessHierarchy command)
        {
            if (command.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty!");
                return false;
            }

            if (command.HierarchyId.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}