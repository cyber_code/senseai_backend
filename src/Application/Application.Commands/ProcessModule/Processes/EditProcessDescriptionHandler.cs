﻿using System;
using System.Collections.Generic;
using System.Linq;
using Messaging.Commands;
using Commands.ProcessModule.Processes;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.ProcessModel;

namespace Application.Commands.ProcessModule.Processes
{
    public sealed class EditProcessDescriptionHandler : ICommandHandler<EditProcessDescription, bool>
    {
        private readonly IDbContext _dbContext;
        private readonly IProcessRepository _repository;
        public List<string> Errors { get; set; }

        public EditProcessDescriptionHandler(IDbContext dbContext, IProcessRepository repository)
        {
            _dbContext = dbContext;
            _repository = repository;
        }

        public CommandResponse<bool> Handle(EditProcessDescription command)
        {
            ProcessData processData = _dbContext.Set<ProcessData>()
                                                .AsNoTracking()
                                                .FirstOrDefault(hD => hD.Id == command.ProcessId);

            if (processData == null)
            {
                throw new Exception("This Process does not exists!");
            }

            Process process = new Process(processData.Id,
                                          processData.SubProjectId,
                                          processData.HierarchyId,
                                          processData.CreatedId,
                                          processData.OwnerId,
                                          processData.ProcessType,
                                          processData.Title,
                                          command.Description,
                                          processData.RequirementPriorityId,
                                          processData.RequirementTypeId,
                                          processData.ExpectedNumberOfTestCases,
                                          processData.TypicalId);

            _repository.Update(process);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(EditProcessDescription command)
        {
            if (command.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}