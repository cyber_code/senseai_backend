﻿using Commands.ProcessModule.Processes;
using Messaging.Commands;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.ProcessModule.Processes
{
    public sealed class PasteProcessHandler : ICommandHandler<PasteProcess, PasteProcessResult>
    {
        private readonly IProcessRepository _processRepository;
        private readonly SenseAIObjectContext _dbContext;

        public PasteProcessHandler(IProcessRepository processRepository, SenseAIObjectContext dbContext)
        {
            _processRepository = processRepository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<PasteProcessResult> Handle(PasteProcess command)
        {
            //get process by id
            var process = _dbContext.Set<ProcessData>().FirstOrDefault(t => t.Id == command.ProcessId);
            Process pasteProcess;

            if (process == null)
                throw new Exception("This process does not exist");

            if (command.Type == PasteType.Copy)
            {
                process.Title += " - Copy";
                pasteProcess = new Process(process.SubProjectId,
                                           command.HierarchyId,
                                           process.CreatedId,
                                           process.OwnerId,
                                           process.ProcessType,
                                           process.Title,
                                           process.Description,
                                           process.RequirementPriorityId,
                                           process.RequirementTypeId,
                                           process.ExpectedNumberOfTestCases,
                                           process.TypicalId);
            } 
            else
            {
                pasteProcess = new Process(process.Id,
                                           process.SubProjectId,
                                           command.HierarchyId,
                                           process.CreatedId,
                                           process.OwnerId,
                                           process.ProcessType,
                                           process.Title,
                                           process.Description,
                                           process.RequirementPriorityId,
                                           process.RequirementTypeId,
                                           process.ExpectedNumberOfTestCases,
                                           process.TypicalId);

                
            }

            _processRepository.Paste(pasteProcess, command.Type);

            return new CommandResponse<PasteProcessResult>(new PasteProcessResult(pasteProcess.Id, pasteProcess.Title));
        }

        public bool Validate(PasteProcess command)
        {
            if (command.ProcessId == Guid.Empty)
            {
                Errors.Add("ProcessId cannot be empty!");
                return false;
            }

            if (command.HierarchyId == Guid.Empty)
            {
                Errors.Add("HierarchyId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}