﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Issues;
using SenseAI.Domain.Process.IssuesModel;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class DeleteIssueTypeHandler : ICommandHandler<DeleteIssueType, bool>
    {
        private readonly IIssueTypeRepository _repository;
        public List<string> Errors { get; set; }

        public DeleteIssueTypeHandler(IIssueTypeRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<bool> Handle(DeleteIssueType command)
        {
            bool result = _repository.Delete(command.Id, command.ForceDelete, false);

            return new CommandResponse<bool>(result);
        }

        public bool Validate(DeleteIssueType command)
        {
            if (command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            return true;
        }
    }
}