﻿using Commands.ProcessModule.Issues;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Commands.ProcessModule.Issues
{
    public class UpdateIssueTypeHandler : ICommandHandler<UpdateIssueType, bool>
    {
        private readonly IIssueTypeRepository _repository;
        private readonly IDbContext _dbContext;
        private readonly IIssueTypeFieldsRepository _fieldsRepository;

        public List<string> Errors { get; set; }

        public UpdateIssueTypeHandler(IDbContext dbContext, IIssueTypeRepository issueTypeRepository, IIssueTypeFieldsRepository fieldsRepository)
        {
            _repository = issueTypeRepository;
            _dbContext = dbContext;
            _fieldsRepository = fieldsRepository;
        }

        public CommandResponse<bool> Handle(UpdateIssueType command)
        {
            bool result = false;
            IssueType issueType = new IssueType(command.Id, command.SubProjectId, command.Title, command.Description, command.Unit, command.Default);
            _repository.Update(issueType);
            var issueFields = (from issueTypeFields in _dbContext.Set<IssueTypeFieldsData>().AsNoTracking()

                               where issueTypeFields.IssueTypeId == command.Id
                               select issueTypeFields).ToList();
            foreach (var issueField in issueFields)
            {
                _dbContext.Set<IssueTypeFieldsData>().Remove(issueField);
                _dbContext.SaveChanges();
            }
            foreach (var fields in command.IssueTypeFields)
            {
                SenseAI.Domain.Process.IssuesModel.IssueTypeFields issueTypeFields = new SenseAI.Domain.Process.IssuesModel.IssueTypeFields(issueType.Id, fields.IssueTypeFieldsNameId, fields.Checked);
                _fieldsRepository.Add(issueTypeFields);
            }

            result = true;
            return new CommandResponse<bool>(result);
        }

        public bool Validate(UpdateIssueType command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }


            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
