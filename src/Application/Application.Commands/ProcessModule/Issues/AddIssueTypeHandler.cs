﻿using System;
using System.Collections.Generic;
using System.Text;
using Commands.ProcessModule.Issues;
using Messaging.Commands;
using SenseAI.Domain.Process.IssuesModel;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class AddIssueTypeHandler : ICommandHandler<AddIssueType, AddIssueTypeResult>
    {
        private readonly IIssueTypeRepository _repository;
        private readonly IIssueTypeFieldsRepository _fieldsRepository;

        public AddIssueTypeHandler(IIssueTypeRepository repository, IIssueTypeFieldsRepository fieldsRepository)
        {
            _repository = repository;
            _fieldsRepository = fieldsRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddIssueTypeResult> Handle(AddIssueType command)
        {
            IssueType issueType = new IssueType(command.SubProjectId, command.Title, command.Description, command.Unit, command.Default);
            _repository.Add(issueType);
            foreach (var fields in command.IssueTypeFields)
            {
                SenseAI.Domain.Process.IssuesModel.IssueTypeFields issueTypeFields = new SenseAI.Domain.Process.IssuesModel.IssueTypeFields(issueType.Id, fields.IssueTypeFieldsNameId, fields.Checked);
                _fieldsRepository.Add(issueTypeFields);
            }
           

            return new CommandResponse<AddIssueTypeResult>(new AddIssueTypeResult(issueType.Id));
        }

        public bool Validate(AddIssueType command)
        {
            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
