﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Abstractions;
using NeuralEngine;
using NeuralEngine.DefectsModel;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.PeopleModel;
using Persistence.Process.HierarchiesModel;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;

namespace Application.Commands.ProcessModule.Issues
{
    internal class NeuralEngineSyncFacade
    {
        private INeuralEngineClient _neuralEngineClient;
        private IWorkContext _workContext;
        private IDbContext _dbContext;

        public NeuralEngineSyncFacade(INeuralEngineClient neuralEngineClient, IWorkContext workContext, IDbContext dbContext)
        {
            _neuralEngineClient = neuralEngineClient;
            _workContext = workContext;
            _dbContext = dbContext;
        }

        public Task<DefectResponse> SaveIssueToNeuralEngine(Issue issue)
        {
            try
            {
                var dateCreated = getIssueDateCreated(issue.Id);

                var item = new DefectPostModelItem
                {
                    DefectId = GuidToInteger(issue.Id).ToString(), // this is a workaround because Neural Engine expects an integer DefectId
                    Name = issue.Title,
                    Description = issue.Description,
                    Component = getHierarchyTitle(issue.ComponentId),
                    Priority = issue.Priority < 0 ? "None" : issue.Priority.ToString(),
                    Severity = issue.Severity < 0 ? "None" : issue.Severity.ToString(),
                    Status = issue.Status < 0 ? " None" : issue.Status.ToString(),
                    Type = issue.Type < 0 ? "None" : issue.Type.ToString(),
                    AssignedTo = getPeopleName(issue.AssignedId),
                    StartDate = tryGetUnixTime(dateCreated),
                    ModifiedDate = tryGetUnixTime(getIssueDateModified(issue.Id) ?? dateCreated),
                    TargetDate = tryGetUnixTime(issue.DueDate),
                    TestCaseId = issue.TestCaseId == null ? "" : issue.TestCaseId,
                    IssueType = getIssueTypeTitle(issue.IssueTypeId)
                };

                DefectPostModel model = new DefectPostModel
                {
                    TenantId = Guid.Parse(_workContext.TenantId ?? "30788c4a-ea74-4aa8-b505-891303dbaeaa"),
                    ProjectId = getProjectId(issue.SubProjectId),
                    SubProjectId = issue.SubProjectId,
                    CatalogId = getDefaultCatalogId(issue.SubProjectId),
                    Lang = "en",
                    Items = new DefectPostModelItem[] { item }
                };

                return Task.Run(() => _neuralEngineClient.SaveDefects(model));
            }
            catch(Exception e)
            {
                return Task.Run(() => new DefectResponse { Successful = false, Description = e.ToString() });
            }
        }

        private string getIssueTypeTitle(Guid issueTypeId)
        {
            return (from issuetype in _dbContext.Set<IssueTypeData>()
                    where issuetype.Id == issueTypeId
                    select issuetype.Title).FirstOrDefault();
        }

        private DateTime? getIssueDateCreated(Guid issueId)
        {
            return (from issue in _dbContext.Set<IssueData>()
                    where issue.Id == issueId
                    select issue.DateCreated).FirstOrDefault();
        }

        private DateTime? getIssueDateModified(Guid issueId)
        {
            return (from issue in _dbContext.Set<IssueData>()
                    where issue.Id == issueId
                    select issue.DateModified).FirstOrDefault();
        }

        public Task<DefectResponse> DeleteFromNeuralEngine(Guid issueId)
        {
            try
            {
                var hashId = getIssueHashId(issueId);
                var defectId = GuidToInteger(issueId).ToString();

                return Task.Run(() => _neuralEngineClient.DeleteDefects(hashId, defectId));
            }
            catch (Exception e)
            {
                return Task.Run(() => new DefectResponse { Successful = false, Description = e.ToString() });
            }
        }

        private string getIssueHashId(Guid issueId)
        {
            var subProjectId = getIssueSubProjectId(issueId);

            DefectPostModel model = new DefectPostModel
            {
                //TenantId = Guid.Parse(_workContext.TenantId), //
                TenantId = Guid.Parse(_workContext.TenantId ?? "30788c4a-ea74-4aa8-b505-891303dbaeaa"),
                ProjectId = getProjectId(subProjectId),
                SubProjectId = subProjectId,
                CatalogId = getDefaultCatalogId(subProjectId)
            };

            return model.HashId;
        }

        private string tryGetUnixTime(DateTime? t)
        {
            try
            {
                return ((DateTimeOffset)t).ToUnixTimeMilliseconds().ToString();
            }
            catch
            {
                // Neural Engine will throw an exception if we do not provide a parseable float for the UNIX times
                return "0";
            }
        }

        private string getPeopleName(Guid peopleId)
        {
            return (from people in _dbContext.Set<PeopleData>()
                    where people.Id == peopleId
                    select people.Name).FirstOrDefault() ?? "";
        }

        private string getHierarchyTitle(Guid hierarchyId)
        {
            return (from hierarchy in _dbContext.Set<HierarchyData>()
                    where hierarchy.Id == hierarchyId
                    select hierarchy.Title).FirstOrDefault() ?? "";
        }

        private Guid getDefaultCatalogId(Guid subProjectId)
        {
            return (from settings in _dbContext.Set<SettingsData>()
                    where settings.SubProjectId == subProjectId
                    select settings.CatalogId).FirstOrDefault();
        }

        private Guid getProjectId(Guid subProjectId)
        {
            return (from subproject in _dbContext.Set<SubProjectData>()
                    where subproject.Id == subProjectId
                    select subproject.ProjectId).FirstOrDefault();
        }

        private Guid getIssueSubProjectId(Guid issueId)
        {
            return (from issue in _dbContext.Set<IssueData>()
                    where issue.Id == issueId
                    select issue.SubProjectId).FirstOrDefault();
        }

        /// <summary>
        /// Converts the last 8 hexadecimal characters of a GUID to a 31-bit unsigned integer
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        private uint GuidToInteger(Guid guid)
        {
            var guidStr = guid.ToString();
            guidStr = guidStr.Substring(0, 8);
            return uint.Parse(guidStr, System.Globalization.NumberStyles.HexNumber) & 0x7fffffff;
        }
    }
}