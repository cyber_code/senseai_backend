﻿using Commands.ProcessModule.Issues;
using Messaging.Commands;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class AddIssueCommentHandler : ICommandHandler<AddIssueComment, AddIssueCommentResult>
    {
        private readonly IIssueCommentRepository _repository;

        public AddIssueCommentHandler(IIssueCommentRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddIssueCommentResult> Handle(AddIssueComment command)
        {
            IssueComment issue = new IssueComment(command.IssueId, command.Comment, command.CommenterId, command.ParentId);
            _repository.Add(issue);

            return new CommandResponse<AddIssueCommentResult>(new AddIssueCommentResult(issue.Id));
        }

        public bool Validate(AddIssueComment command)
        {
            if (command.IssueId == null || command.IssueId.Equals(Guid.Empty))
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }
            if (String.IsNullOrWhiteSpace(command.Comment))
            {
                Errors.Add("Comment cannot be empty.");
                return false;
            }
            if (command.CommenterId == null || command.CommenterId.Equals(Guid.Empty))
            {
                Errors.Add("CommenterId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
