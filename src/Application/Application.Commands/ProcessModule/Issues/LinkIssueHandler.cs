﻿using Commands.ProcessModule.Issues;
using Messaging.Commands;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SenseAI.Domain;
using Core.Configuration;
using DataExtraction;
using Persistence.AdminModel;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class LinkIssueHandler : ICommandHandler<LinkIssue, bool>
    {
        private readonly IIssueLinkRepository _issueLinkRepository;
        private readonly IDbContext _dbContext;
        private readonly IDataExtractionClient _dataExtractionClient;
        private SenseAIConfig _configuration;

        public LinkIssueHandler(IIssueLinkRepository issueLinkRepository, IDbContext dbContext, IDataExtractionClient dataExtractionClient, SenseAIConfig configuration)
        {
            _issueLinkRepository = issueLinkRepository;
            _dbContext = dbContext;
            _dataExtractionClient = dataExtractionClient;
            _configuration = configuration;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(LinkIssue command)
        {
            //var errorMessage = "Only links from Business Change Requests to Technical Change Requests are allowed!";
            var errorMessage = "Issue not found!";
            var query = from issue in _dbContext.Set<IssueData>()
                        join issueType in _dbContext.Set<IssueTypeData>() on issue.IssueTypeId equals issueType.Id
                        select new
                        {
                            IssueId = issue.Id,
                            issueType.Title,
                            issueType.Default,
                            issue.SubProjectId
                        };

            var srcIssueType = query.Where(x => x.IssueId == command.SrcIssueId).FirstOrDefault();
            if (srcIssueType == null) //|| !isBusinessChangeRequest(srcIssueType.Title, srcIssueType.Default))
                return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Failed, errorMessage));

            var dstIssueType = query.Where(x => x.IssueId == command.DstIssueId).FirstOrDefault();
            if (dstIssueType == null)// || !isTechnicalChangeRequest(dstIssueType.Title, dstIssueType.Default))
                return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Failed, errorMessage));

            _issueLinkRepository.Add(new IssueLink(command.SrcIssueId, command.LinkType, command.DstIssueId));

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == srcIssueType.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                //post issuelink to Jira
                _dataExtractionClient.LinkIssues(
                    new DataExtraction.JiraModel.LinkPostModel
                    {
                        MainIssueId = command.SrcIssueId,
                        LinkedIssueId = command.DstIssueId,
                        LinkName = _configuration.IssueLinkName
                    }
                );
            }
            return new CommandResponse<bool>(true);
        }

        public bool Validate(LinkIssue command)
        {
            if (command.SrcIssueId == null || command.SrcIssueId.Equals(Guid.Empty))
            {
                Errors.Add("SrcIssueId cannot be empty!");
                return false;
            }

            if (command.DstIssueId == null || command.DstIssueId.Equals(Guid.Empty))
            {
                Errors.Add("DstIssueId cannot be empty!");
                return false;
            }

            return true;
        }

        //private bool isBusinessChangeRequest(string title, bool @default)
        //{
        //    if (title == null)
        //        return false;
        //    if (!title.ToLower().Equals("business cr"))
        //        return false;
        //    if (!@default)
        //        return false;
        //    return true;
        //}

        //private bool isTechnicalChangeRequest(string title, bool @default)
        //{
        //    if (title == null)
        //        return false;
        //    if (!title.ToLower().Equals("technical cr"))
        //        return false;
        //    if (!@default)
        //        return false;
        //    return true;
        //}
    }
}