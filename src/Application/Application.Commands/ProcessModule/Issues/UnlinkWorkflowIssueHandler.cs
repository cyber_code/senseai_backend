﻿using System;
using System.Collections.Generic;
using System.Linq;
using Commands.ProcessModule.Issues;
using Messaging.Commands;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class UnlinkWorkflowIssueHandler : ICommandHandler<UnlinkWorkflowIssue, bool>
    {
        private readonly IWorkflowIssueRepository _repository;
        private readonly IDbContext _dbContext;

        public UnlinkWorkflowIssueHandler(IWorkflowIssueRepository repository, IDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UnlinkWorkflowIssue command)
        {
            var query = from workflowIssue in _dbContext.Set<WorkflowIssueData>()
                        where workflowIssue.WorkflowId == command.WorkflowId && workflowIssue.IssueId == command.IssueId
                        select new
                        {
                            workflowIssue.Id
                        };
            query.ToList().ForEach(wfIssue =>
            {
                _repository.Delete(wfIssue.Id);
            });
            return new CommandResponse<bool>(true);
        }

        public bool Validate(UnlinkWorkflowIssue command)
        {
            if (command.WorkflowId == null || command.WorkflowId.Equals(Guid.Empty))
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }

            if (command.IssueId == null || command.IssueId.Equals(Guid.Empty))
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
