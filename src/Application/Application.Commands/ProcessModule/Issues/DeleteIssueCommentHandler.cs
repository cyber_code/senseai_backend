﻿using Commands.ProcessModule.Issues;
using Messaging.Commands;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class DeleteIssueCommentHandler : ICommandHandler<DeleteIssueComment, bool>
    {
        private readonly IIssueCommentRepository _repository;

        public DeleteIssueCommentHandler(IIssueCommentRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteIssueComment command)
        {
            _repository.Delete(command.Id);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteIssueComment command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            } 
            return true;
        }
    }
}
