﻿using Commands.ProcessModule.Issues;
using Core.Abstractions;
using Core.Configuration;
using DataExtraction;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.AdminModel;
using NeuralEngine;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Commands.ProcessModule.Issues
{
    public class UpdateIssueHandler : ICommandHandler<UpdateIssue, bool>
    {
        private readonly IIssueRepository _repository;
        private readonly IDbContext _dbContext;
        private readonly IDataExtractionClient _dataExtractionClient;
        private SenseAIConfig _configuration;
        private readonly INeuralEngineClient _neuralEngineClient;
        private readonly IWorkContext _workContext;

        public UpdateIssueHandler(IIssueRepository repository,
            IDbContext dbContext,
            IDataExtractionClient dataExtractionClient,
            SenseAIConfig configuration,
            INeuralEngineClient neuralEngineClient,
            IWorkContext workContext)
        {
            _repository = repository;
            _dbContext = dbContext;
            _dataExtractionClient = dataExtractionClient;
            _configuration = configuration;
            _neuralEngineClient = neuralEngineClient;
            _workContext = workContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateIssue command)
        {
            bool result = false;

            Issue issue = new Issue(command.Id, command.ProcessId, command.SubProjectId, command.IssueTypeId, command.AssignedId, command.Title, command.Description, command.Status, command.Estimate, command.Reason, command.Implication, command.Comment, command.TypicalId, command.Attribute, command.ApprovedForInvestigation, command.InvestigationStatus, command.ChangeStatus, command.ReporterId, command.ApprovedId, command.DateSubmitted, command.ApprovalDate, command.StartDate, command.Priority, command.Severity, command.DueDate, command.Type, command.ComponentId, command.TestCaseId);

            _repository.Update(issue);

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == command.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.UpdateIssue(
                    new DataExtraction.JiraModel.UpdateIssuePostModel(
                        issue.Id,
                        _configuration.IssueTypeForRequirementId,
                        _configuration.RequirementLinkName,
                        _configuration.PeopleFields
                    )
                );
            }

            var synchronizer = new NeuralEngineSyncFacade(_neuralEngineClient, _workContext, _dbContext);
            synchronizer.SaveIssueToNeuralEngine(issue);


            result = true;
            return new CommandResponse<bool>(result);
        }

        public bool Validate(UpdateIssue command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            //if (command.ProcessId == null || command.ProcessId.Equals(Guid.Empty))
            //{
            //    Errors.Add("ProcessId cannot be empty.");
            //    return false;
            //}

            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (command.IssueTypeId == null || command.IssueTypeId.Equals(Guid.Empty))
            {
                Errors.Add("IssueTypeId cannot be empty.");
                return false;
            }

            if (command.AssignedId == null || command.AssignedId.Equals(Guid.Empty))
            {
                Errors.Add("AssignedId cannot be empty.");
                return false;
            }

            if (String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            return true;
        }
    }
}