﻿using Commands.ProcessModule.Issues;
using Messaging.Commands;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class UpdateIssueCommentHandler : ICommandHandler<EditIssueComment, bool>
    {
        private readonly IIssueCommentRepository _repository;

        public UpdateIssueCommentHandler(IIssueCommentRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(EditIssueComment command)
        {
            IssueComment issueComment = new IssueComment(command.Id, command.IssueId, command.Comment, command.CommenterId, command.ParentId);
            _repository.Update(issueComment);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(EditIssueComment command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            if (command.IssueId == null || command.IssueId.Equals(Guid.Empty))
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }
            if (String.IsNullOrWhiteSpace(command.Comment))
            {
                Errors.Add("Comment cannot be empty.");
                return false;
            } 
            if (command.CommenterId == null || command.CommenterId.Equals(Guid.Empty))
            {
                Errors.Add("CommenterId cannot be empty.");
                return false;
            }
            if (command.ParentId == null || command.ParentId.Equals(Guid.Empty))
            {
                Errors.Add("ParentId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
