﻿using System;
using System.Collections.Generic;
using System.Text;
using Commands.ProcessModule.Issues;
using Messaging.Commands;
using SenseAI.Domain.Process.IssuesModel;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class AddWorkflowIssueHandler : ICommandHandler<AddWorkflowIssue, AddWorkflowIssueResult>
    {
        private readonly IWorkflowIssueRepository _repository;

        public AddWorkflowIssueHandler(IWorkflowIssueRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddWorkflowIssueResult> Handle(AddWorkflowIssue command)
        {
            WorkflowIssue workflowIssue = new WorkflowIssue(command.WorkflowId, command.IssueId);
            _repository.Add(workflowIssue);

            return new CommandResponse<AddWorkflowIssueResult>(new AddWorkflowIssueResult(workflowIssue.Id));
        }

        public bool Validate(AddWorkflowIssue command)
        {
            if (command.WorkflowId == null || command.WorkflowId.Equals(Guid.Empty))
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }

            if (command.IssueId == null || command.IssueId.Equals(Guid.Empty))
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
