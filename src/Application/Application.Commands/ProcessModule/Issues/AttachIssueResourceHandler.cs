﻿using Commands.ProcessModule.Issues;
using Core.Configuration;
using DataExtraction;
using Messaging.Commands;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class AttachIssueResourceHandler : ICommandHandler<AttachIssueResource, AttachIssueResourceResult[]>
    {
        private readonly IIssueResourceRepository _repository;
        private readonly SenseAIConfig _config;
        private readonly IDbContext _dbContext;
        private readonly IDataExtractionClient _dataExtractionClient;

        public AttachIssueResourceHandler(IIssueResourceRepository repository, SenseAIConfig config, IDbContext dbContext, IDataExtractionClient dataExtractionClient)
        {
            _repository = repository;
            _config = config;
            _dbContext = dbContext;
            _dataExtractionClient = dataExtractionClient;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AttachIssueResourceResult[]> Handle(AttachIssueResource command)
        {
            var result = new AttachIssueResourceResult[command.FileCollection.Count];
            int i = 0;
            System.IO.Directory.CreateDirectory(_config.ProcessResourcePath);

            var data = _dbContext.Set<IssueData>()
                .Where(item => item.Id == command.IssueId)
                .Select(item => new { item.ExternalSystemId, item.SubProjectId })
                .First();

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == data.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            foreach (var file in command.FileCollection)
            {
                var filePath = _config.ProcessResourcePath;

                using (var fileStream = new FileStream(filePath + file.FileName, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
                IssueResource issueResource = new IssueResource(command.IssueId, file.FileName, filePath + file.FileName);
                _repository.Add(issueResource);

                if (iSJiraIntegrationEnabled)
                {
                    _dataExtractionClient.AddAttachment(new DataExtraction.JiraModel.AddAttachmentPostModel(issueResource.Id,
                        file, true));
                }

                result[i++] = new AttachIssueResourceResult(issueResource.Id);
            }

            return new CommandResponse<AttachIssueResourceResult[]>(result);
        }

        public bool Validate(AttachIssueResource command)
        {
            if (command.IssueId == null || command.IssueId.Equals(Guid.Empty))
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }
            if (command.FileCollection == null)
            {
                Errors.Add("File Content cannot be empty.");
                return false;
            }
            return true;
        }
    }
}