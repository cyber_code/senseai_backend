﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Issues;
using SenseAI.Domain.Process.IssuesModel;
using DataExtraction;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Core.Configuration;
using DataExtraction.JiraModel;
using Persistence.AdminModel;
using NeuralEngine;
using Core.Abstractions;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class DeleteIssueHandler : ICommandHandler<DeleteIssue, bool>
    {
        private readonly IIssueRepository _repository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;
        private SenseAIConfig _configuration;
        private readonly INeuralEngineClient _neuralEngineClient;
        private readonly IWorkContext _workContext;

        public DeleteIssueHandler(IIssueRepository repository,
            IDataExtractionClient dataExtractionClient,
            IDbContext dbContext,
            SenseAIConfig configuration,
            INeuralEngineClient neuralEngineClient,
            IWorkContext workContext)
        {
            _repository = repository;
            _dataExtractionClient = dataExtractionClient;
            _dbContext = dbContext;
            _configuration = configuration;
            _neuralEngineClient = neuralEngineClient;
            _workContext = workContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteIssue command)
        {
            DeleteIssuePostModel jiraModel = null;

            var data = _dbContext.Set<IssueData>()
                .Where(item => item.Id == command.Id)
                .Select(item => new { item.ExternalSystemId, item.SubProjectId })
                .First();

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == data.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                jiraModel = new DeleteIssuePostModel(command.Id, data.SubProjectId, data.ExternalSystemId);
            } 

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.DeleteIssue(jiraModel);
            }

            var synchronizer = new NeuralEngineSyncFacade(_neuralEngineClient, _workContext, _dbContext);
            var result = synchronizer.DeleteFromNeuralEngine(command.Id).Result;

            _repository.Delete(command.Id);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteIssue command)
        {
            if (command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            return true;
        }
    }
}