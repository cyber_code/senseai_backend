﻿using System;
using System.Collections.Generic;
using System.Text;
using Commands.ProcessModule.Issues;
using Messaging.Commands;
using SenseAI.Domain.Process.IssuesModel;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class DeleteWorkflowIssueHandler : ICommandHandler<DeleteWorkflowIssue, bool>
    {
        private readonly IWorkflowIssueRepository _repository;

        public DeleteWorkflowIssueHandler(IWorkflowIssueRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteWorkflowIssue command)
        {
            _repository.Delete(command.Id);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteWorkflowIssue command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
