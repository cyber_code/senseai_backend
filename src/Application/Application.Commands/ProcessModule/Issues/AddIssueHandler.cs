﻿using System;
using System.Collections.Generic;
using System.Linq;
using Commands.ProcessModule.Issues;
using Core.Abstractions;
using Core.Configuration;
using DataExtraction;
using Messaging.Commands;
using NeuralEngine;
using Persistence.AdminModel;
using Persistence.Internal;
using SenseAI.Domain.Process.IssuesModel;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class AddIssueHandler : ICommandHandler<AddIssue, AddIssueResult>
    {
        private readonly IIssueRepository _repository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;
        private SenseAIConfig _configuration;
        private INeuralEngineClient _neuralEngineClient;
        private IWorkContext _workContext;

        public AddIssueHandler(IIssueRepository repository,
            IDataExtractionClient dataExtractionClient,
            SenseAIConfig configuration,
            INeuralEngineClient neuralEngineClient,
            IWorkContext workContext,
            IDbContext dbContext)
        {
            _repository = repository;
            _dataExtractionClient = dataExtractionClient;
            _configuration = configuration;
            _neuralEngineClient = neuralEngineClient;
            _workContext = workContext;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddIssueResult> Handle(AddIssue command)
        {
            Issue issue = new Issue(command.ProcessId, command.SubProjectId, command.IssueTypeId, command.AssignedId, command.Title, command.Description, command.Status, command.Estimate, command.Reason, command.Implication, command.Comment, command.TypicalId, command.Attribute, command.ApprovedForInvestigation, command.InvestigationStatus, command.ChangeStatus, command.ReporterId, command.ApprovedId, command.DateSubmitted, command.ApprovalDate, command.StartDate, command.Priority, command.Severity, command.DueDate, command.Type, command.ComponentId, command.TestCaseId);

            _repository.Add(issue);

            //var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
            //    .Where(item => item.Id == command.SubProjectId)
            //    .Select(item => item.IsJiraIntegrationEnabled)
            //    .First();
            var subproject = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == command.SubProjectId);

            var iSJiraIntegrationEnabled = false;
            if (subproject.Any())
                iSJiraIntegrationEnabled = subproject.Select(item => item.IsJiraIntegrationEnabled == null ? false : item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.AddIssue(
                    new DataExtraction.JiraModel.AddIssuePostModel(
                        issue.Id,
                        _configuration.IssueTypeForRequirementId,
                        _configuration.RequirementLinkName,
                        _configuration.PeopleFields
                    )
                );
            }

            var synchronizer = new NeuralEngineSyncFacade(_neuralEngineClient, _workContext, _dbContext);
            synchronizer.SaveIssueToNeuralEngine(issue);

            return new CommandResponse<AddIssueResult>(new AddIssueResult(issue.Id));
        }

        public bool Validate(AddIssue command)
        {
            //if (command.ProcessId == null || command.ProcessId.Equals(Guid.Empty))
            //{
            //    Errors.Add("ProcessId cannot be empty.");
            //    return false;
            //}

            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (command.IssueTypeId == null || command.IssueTypeId.Equals(Guid.Empty))
            {
                Errors.Add("IssueTypeId cannot be empty.");
                return false;
            }

            if (command.AssignedId == null || command.AssignedId.Equals(Guid.Empty))
            {
                Errors.Add("AssignedId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            return true;
        }
    }
}