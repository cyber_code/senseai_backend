﻿using Commands.ProcessModule.Issues;
using Messaging.Commands;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SenseAI.Domain;
using DataExtraction;
using Core.Configuration;
using Persistence.AdminModel;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class UnlinkIssueHandler : ICommandHandler<UnlinkIssue, bool>
    {
        private readonly IIssueLinkRepository _issueLinkRepository;
        private readonly IDbContext _dbContext;
        private readonly IDataExtractionClient _dataExtractionClient;
        private SenseAIConfig _configuration;

        public UnlinkIssueHandler(IIssueLinkRepository issueLinkRepository, IDbContext dbContext, IDataExtractionClient dataExtractionClient, SenseAIConfig configuration)
        {
            _issueLinkRepository = issueLinkRepository;
            _dbContext = dbContext;
            _dataExtractionClient = dataExtractionClient;
            _configuration = configuration;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UnlinkIssue command)
        {
            var query = from issueLink in _dbContext.Set<IssueLinkData>()
                        where issueLink.SrcIssueId == command.SrcIssueId &&
                              issueLink.DstIssueId == command.DstIssueId
                        select new { issueLink.Id };
            var issueLinkResult = query.FirstOrDefault();

            if (issueLinkResult == null)
                return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Failed, "No issue link found!"));

            _issueLinkRepository.Delete(issueLinkResult.Id);

            var subProjectId = _dbContext.Set<IssueData>()
                .Where(item => item.Id == command.SrcIssueId)
                .Select(item => item.SubProjectId)
                .First();

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == subProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.DeleteLink(
                    new DataExtraction.JiraModel.DeleteLinkPostModel(
                        command.SrcIssueId,
                        command.DstIssueId,
                        _configuration.IssueLinkName
                    )
                );
            }

            return new CommandResponse<bool>(true);
        }

        public bool Validate(UnlinkIssue command)
        {
            if (command.SrcIssueId == null || command.SrcIssueId.Equals(Guid.Empty))
            {
                Errors.Add("SrcIssueId cannot be empty!");
                return false;
            }

            if (command.DstIssueId == null || command.DstIssueId.Equals(Guid.Empty))
            {
                Errors.Add("DstIssueId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}