﻿using System;
using System.Collections.Generic;
using System.Linq;
using Commands.ProcessModule.Issues;
using Core.Configuration;
using DataExtraction;
using Messaging.Commands;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class LinkIssueToProcessHandler : ICommandHandler<LinkIssueToProcess, bool>
    {
        private readonly IIssueRepository _repository;
        private readonly IDbContext _dbContext;
        private readonly IDataExtractionClient _dataExtractionClient;
        private SenseAIConfig _configuration;

        public LinkIssueToProcessHandler(IIssueRepository repository, IDbContext dbContext, IDataExtractionClient dataExtractionClient, SenseAIConfig configuration)
        {
            _repository = repository;
            _dbContext = dbContext;
            _dataExtractionClient = dataExtractionClient;
            _configuration = configuration;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(LinkIssueToProcess command)
        {
            var query = (from issue in _dbContext.Set<IssueData>()
                         where issue.Id == command.IssueId
                         select new Issue(issue.Id, command.ProcessId, issue.SubProjectId, issue.IssueTypeId, issue.AssignedId, issue.Title, issue.Description, issue.Status, issue.Estimate, issue.Reason, issue.Implication, issue.Comment, issue.TypicalId, issue.Attribute, issue.ApprovedForInvestigation, issue.InvestigationStatus, issue.ChangeStatus, issue.ReporterId, issue.ApprovedId, issue.DateSubmitted, issue.ApprovalDate, issue.StartDate, issue.Priority, issue.Severity, issue.DueDate, issue.Type, issue.ComponentId, issue.TestCaseId
                         )).FirstOrDefault();
            _repository.Update(query);

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == query.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.UpdateIssue(
                    new DataExtraction.JiraModel.UpdateIssuePostModel(
                        query.Id,
                        _configuration.IssueTypeForRequirementId,
                        _configuration.RequirementLinkName,
                        _configuration.PeopleFields
                    )
                );
            }

            return new CommandResponse<bool>(true);
        }

        public bool Validate(LinkIssueToProcess command)
        {
            if (command.IssueId == null || command.IssueId.Equals(Guid.Empty))
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }
            if (command.ProcessId == null || command.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}