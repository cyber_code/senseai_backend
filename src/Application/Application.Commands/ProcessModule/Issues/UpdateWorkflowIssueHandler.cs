﻿using Commands.ProcessModule.Issues;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Commands.ProcessModule.Issues
{
    public class UpdateWorkflowIssueHandler : ICommandHandler<UpdateWorkflowIssue, bool>
    {
        private readonly IWorkflowIssueRepository _repository;
        private readonly IDbContext _dbContext;

        public List<string> Errors { get; set; }

        public UpdateWorkflowIssueHandler(IDbContext dbContext, IWorkflowIssueRepository issueRepository)
        {
            _repository = issueRepository;
            _dbContext = dbContext;
        }

        public CommandResponse<bool> Handle(UpdateWorkflowIssue command)
        {
            WorkflowIssue workflowIssue = new WorkflowIssue(command.Id, command.WorkflowId, command.IssueId);
            _repository.Update(workflowIssue);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateWorkflowIssue command)
        {
            if(command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (command.WorkflowId == null || command.WorkflowId.Equals(Guid.Empty))
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }

            if (command.IssueId == null || command.IssueId.Equals(Guid.Empty))
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
