﻿using Commands.ProcessModule.Issues;
using DataExtraction;
using DataExtraction.JiraModel;
using Messaging.Commands;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using SenseAI.Domain.Process.IssuesModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Commands.ProcessModule.Issues
{
    public sealed class DeleteIssueResourceHandler : ICommandHandler<DeleteIssueResource, bool>
    {
        private readonly IIssueResourceRepository _repository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;

        public DeleteIssueResourceHandler(IIssueResourceRepository repository, IDataExtractionClient dataExtractionClient, IDbContext dbContext)
        {
            _repository = repository;
            _dataExtractionClient = dataExtractionClient;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteIssueResource command)
        {
            DeleteAttachmentPostModel jiraModel = null;
            var issueResoruce = _dbContext.Set<IssueResourceData>()
                .Where(item => item.Id == command.Id)
                .Select(item => new { item.IssueId, item.Title, item.ExternalSystemId })
                .First();

            var data = _dbContext.Set<IssueData>()
                .Where(item => item.Id == issueResoruce.IssueId)
                .Select(item => new { item.ExternalSystemId, item.SubProjectId })
                .First();

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == data.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                jiraModel = new DeleteAttachmentPostModel(
                    command.Id, issueResoruce.IssueId,
                    issueResoruce.Title, issueResoruce.ExternalSystemId);
                _dataExtractionClient.DeleteAttachment(jiraModel);
            }

            _repository.Delete(command.Id);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteIssueResource command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}