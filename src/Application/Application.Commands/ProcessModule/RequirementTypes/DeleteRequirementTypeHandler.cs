﻿using System;
using System.Collections.Generic;
using Commands.ProcessModule.RequirementTypes;
using Messaging.Commands;
using SenseAI.Domain.Process.RequirementTypesModel;

namespace Application.Commands.ProcessModule.RequirementTypes
{
    public sealed class DeleteRequirementTypeHandler : ICommandHandler<DeleteRequirementType, bool>
    {
        private readonly IRequirementTypeRepository _repository;
        public List<string> Errors { get; set; }

        public DeleteRequirementTypeHandler(IRequirementTypeRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<bool> Handle(DeleteRequirementType command)
        {
            _repository.Delete(command.Id, command.ForceDelete);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteRequirementType command)
        {
            if (command.Id.Equals(Guid.Empty))
            {
                Errors.Add("RequirementTypeId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}