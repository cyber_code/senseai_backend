﻿using System;
using System.Collections.Generic;
using Commands.ProcessModule.RequirementTypes;
using Messaging.Commands;
using SenseAI.Domain.Process.RequirementTypesModel;

namespace Application.Commands.ProcessModule.RequirementTypes
{
    public sealed class AddRequirementTypeHandler : ICommandHandler<AddRequirementType, AddRequirementTypeResult>
    {
        private readonly IRequirementTypeRepository _repository;

        public AddRequirementTypeHandler(IRequirementTypeRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddRequirementTypeResult> Handle(AddRequirementType command)
        {
            RequirementType requirementType = new RequirementType(command.SubProjectId, command.Title, command.Code);
            _repository.Add(requirementType);

            return new CommandResponse<AddRequirementTypeResult>(new AddRequirementTypeResult(requirementType.Id));
        }

        public bool Validate(AddRequirementType command)
        {
            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }

            return true;
        }
    }
}
