﻿using Commands.ProcessModule.RequirementTypes;
using Messaging.Commands;
using Persistence.Internal;
using SenseAI.Domain.Process.RequirementTypesModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.ProcessModule.RequirementTypes
{
    public class UpdateRequirementTypeHandler : ICommandHandler<UpdateRequirementType, bool>
    {
        private readonly IRequirementTypeRepository _repository;

        public List<string> Errors { get; set; }

        public UpdateRequirementTypeHandler(IRequirementTypeRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<bool> Handle(UpdateRequirementType command)
        {
            bool result = false;
            RequirementType requirementType = new RequirementType(command.Id, command.SubProjectId, command.Title, command.Code);
            _repository.Update(requirementType);
            result = true;
            return new CommandResponse<bool>(result);
        }

        public bool Validate(UpdateRequirementType command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("RequirementTypeId cannot be empty!");
                return false;
            }

            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }

            return true;
        }
    }
}
