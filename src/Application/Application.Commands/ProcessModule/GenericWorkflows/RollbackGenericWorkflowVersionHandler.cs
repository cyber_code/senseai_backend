﻿using System;
using SenseAI.Domain;
using System.Collections.Generic;
using Commands.ProcessModule.GenericWorkflows;
using Messaging.Commands;
using SenseAI.Domain.Process.GenericWorkflowModel;

namespace Application.Commands.ProcessModule.GenericWorkflows
{
    public class RollbackGenericWorkflowVersionHandler2 : ICommandHandler<RollbackGenericWorkflowVersion, RollbackGenericWorkflowVersionResult>
    {
        private readonly IGenericWorkflowRepository _genericWorkflowRepository;
        private readonly ICommandHandler<IssueGenericWorkflow, IssueGenericWorkflowResult> _issueCommand;

        public RollbackGenericWorkflowVersionHandler2(IGenericWorkflowRepository genericWorkflowRepository,
            ICommandHandler<IssueGenericWorkflow, IssueGenericWorkflowResult> issueCommand)
        {
            _genericWorkflowRepository = genericWorkflowRepository;
            _issueCommand = issueCommand;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<RollbackGenericWorkflowVersionResult> Handle(RollbackGenericWorkflowVersion command)
        { 
            var designerJson = _genericWorkflowRepository.RollbackGenericWorkflow(command.GenericWorkflowId, command.Version);

            var genericWorkflowImage = _genericWorkflowRepository.GetGenericWorkflowImage(command.GenericWorkflowId, command.Version);
            IssueGenericWorkflow issueGenericWorkflow = new IssueGenericWorkflow
            {
                GenericWorkflowId = command.GenericWorkflowId,
                NewVersion = false,
                GenericWorkflowImage = genericWorkflowImage
            };

            _issueCommand.Handle(issueGenericWorkflow);

            return new CommandResponse<RollbackGenericWorkflowVersionResult>(
                new RollbackGenericWorkflowVersionResult(command.GenericWorkflowId, command.Version, designerJson), new CommandResponseMessage((int)MessageType.Successful, "")
            );
        }

        public bool Validate(RollbackGenericWorkflowVersion command)
        {
            if (command.GenericWorkflowId == Guid.Empty)
            {
                Errors.Add("GenericWorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}