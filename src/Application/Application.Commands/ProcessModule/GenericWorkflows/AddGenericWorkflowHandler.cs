﻿using Commands.ProcessModule.GenericWorkflows;
using Messaging.Commands;
using SenseAI.Domain.Process.GenericWorkflowModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.ProcessModule.GenericWorkflows
{
    public sealed class AddGenericWorkflowHandler : ICommandHandler<AddGenericWorkflow, AddGenericWorkflowResult>
    {
        private readonly IGenericWorkflowRepository _repository;

        public List<string> Errors { get; set; }

        public AddGenericWorkflowHandler(IGenericWorkflowRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<AddGenericWorkflowResult> Handle(AddGenericWorkflow command)
        {
            var genericWorkflow = new GenericWorkflow(Guid.NewGuid(), command.ProcessId, command.Title, command.Description, "");
            _repository.Add(genericWorkflow);
            return new CommandResponse<AddGenericWorkflowResult>(new AddGenericWorkflowResult(genericWorkflow.Id));
        }

        public bool Validate(AddGenericWorkflow command)
        {
            if (command.ProcessId == Guid.Empty)
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }

            if (String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
