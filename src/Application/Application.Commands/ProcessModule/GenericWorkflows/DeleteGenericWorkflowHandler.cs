﻿using Commands.ProcessModule.GenericWorkflows;
using Messaging.Commands;
using SenseAI.Domain.Process.GenericWorkflowModel;
using System;
using System.Collections.Generic;

namespace ApplicationApplication.Commands.ProcessModule.GenericWorkflows
{
    public sealed class DeleteGenericWorkflowHandler : ICommandHandler<DeleteGenericWorkflow, bool>
    {
        private readonly IGenericWorkflowRepository _repository;

        public List<string> Errors { get; set; }

        public DeleteGenericWorkflowHandler(IGenericWorkflowRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<bool> Handle(DeleteGenericWorkflow command)
        {
            _repository.Delete(command.Id);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteGenericWorkflow command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }             
            return true;
        }
    }
}
