﻿
using Commands.ProcessModule.GenericWorkflows;
using Messaging.Commands;
using SenseAI.Domain.Process.GenericWorkflowModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.DesignModule.GenericWorkflowModel
{
    public sealed class RenameGenericWorkflowHandler : ICommandHandler<RenameGenericWorkflow, RenameGenericWorkflowResult>
    {
        private readonly IGenericWorkflowRepository _genericWorkflowRepository;

        public RenameGenericWorkflowHandler(IGenericWorkflowRepository genericWorkflowRepository)
        {
            _genericWorkflowRepository = genericWorkflowRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<RenameGenericWorkflowResult> Handle(RenameGenericWorkflow command)
        {
            _genericWorkflowRepository.Rename(command.Id, command.Title);

            return new CommandResponse<RenameGenericWorkflowResult>(
                new RenameGenericWorkflowResult(command.Id)
            );
        }

        public bool Validate(RenameGenericWorkflow command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}