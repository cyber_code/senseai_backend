﻿using Commands.ProcessModule.GenericWorkflows;
using Messaging.Commands;
using SenseAI.Domain.Process.GenericWorkflowModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.ProcessModule.GenericWorkflows
{
    public sealed class SaveGenericWorkflowHandler : ICommandHandler<SaveGenericWorkflow, SaveGenericWorkflowResult>
    {
        private readonly IGenericWorkflowRepository _repository;

        public List<string> Errors { get; set; }

        public SaveGenericWorkflowHandler(IGenericWorkflowRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<SaveGenericWorkflowResult> Handle(SaveGenericWorkflow command)
        {
            var genericWorkflow = GenericWorkflowFactory.Create(command.Id, command.DesignerJson,Guid.NewGuid());
            _repository.Save(genericWorkflow, command.DesignerJson);
            return new CommandResponse<SaveGenericWorkflowResult>(new SaveGenericWorkflowResult(command.Id));
        }

        public bool Validate(SaveGenericWorkflow command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (String.IsNullOrEmpty(command.DesignerJson))
            {
                Errors.Add("DesignerJson cannot be empty.");
                return false;
            }        
            return true;
        }
    }
}
