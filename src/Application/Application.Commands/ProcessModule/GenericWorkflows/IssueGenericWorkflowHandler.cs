﻿using Messaging.Commands;
using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;
using Commands.ProcessModule.GenericWorkflows;
using SenseAI.Domain.Process.GenericWorkflowModel;

namespace Application.Commands.ProcessModule.GenericWorkflows
{
    public sealed class IssueGenericWorkflowHandler : ICommandHandler<IssueGenericWorkflow, IssueGenericWorkflowResult>
    {
        private readonly IDbContext _dbContext;
        private readonly IGenericWorkflowRepository _genericWorkflowRepository;

        public IssueGenericWorkflowHandler(IGenericWorkflowRepository genericWorkflowRepository, IDbContext dbContext)
        {
            _genericWorkflowRepository = genericWorkflowRepository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<IssueGenericWorkflowResult> Handle(IssueGenericWorkflow command)
        {
            int nrVersions = _genericWorkflowRepository.GetNrVersions(command.GenericWorkflowId);

            if (nrVersions == 0 || command.NewVersion)
                _genericWorkflowRepository.Issue(command.GenericWorkflowId, command.GenericWorkflowImage);
            else
                _genericWorkflowRepository.Update(command.GenericWorkflowId);

            return new CommandResponse<IssueGenericWorkflowResult>(new IssueGenericWorkflowResult(command.GenericWorkflowId), new CommandResponseMessage((int)MessageType.Successful, ""));
        }

        public bool Validate(IssueGenericWorkflow command)
        {
            if (command.GenericWorkflowId == Guid.Empty)
            {
                Errors.Add("GenericWorkflowId cannot be empty.");
                return false;
            }
            return true;
        } 
    }
}