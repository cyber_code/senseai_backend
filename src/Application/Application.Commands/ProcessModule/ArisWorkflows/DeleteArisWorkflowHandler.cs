﻿using Commands.ProcessModule.ArisWorkflows;
using Messaging.Commands;
using SenseAI.Domain.Process.ArisWorkflowModel;
using System;
using System.Collections.Generic;

namespace ApplicationApplication.Commands.ProcessModule.ArisWorkflows
{
    public sealed class DeleteArisWorkflowHandler : ICommandHandler<DeleteArisWorkflow, bool>
    {
        private readonly IArisWorkflowRepository _repository;

        public List<string> Errors { get; set; }

        public DeleteArisWorkflowHandler(IArisWorkflowRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<bool> Handle(DeleteArisWorkflow command)
        {
            _repository.Delete(command.Id);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteArisWorkflow command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }             
            return true;
        }
    }
}
