﻿using Commands.ProcessModule.ArisWorkflows;
using Messaging.Commands;
using SenseAI.Domain.Process.ArisWorkflowModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public sealed class SaveArisWorkflowHandler : ICommandHandler<SaveArisWorkflow, SaveArisWorkflowResult>
    {
        private readonly IArisWorkflowRepository _repository;

        public List<string> Errors { get; set; }

        public SaveArisWorkflowHandler(IArisWorkflowRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<SaveArisWorkflowResult> Handle(SaveArisWorkflow command)
        {
            var arisWorkflow = ArisWorkflowFactory.Create(command.Id, command.DesignerJson,Guid.NewGuid());
            _repository.Save(arisWorkflow, command.DesignerJson);
            return new CommandResponse<SaveArisWorkflowResult>(new SaveArisWorkflowResult(command.Id));
        }

        public bool Validate(SaveArisWorkflow command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (String.IsNullOrEmpty(command.DesignerJson))
            {
                Errors.Add("DesignerJson cannot be empty.");
                return false;
            }        
            return true;
        }
    }
}
