﻿using Messaging.Commands;
using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;
using Commands.ProcessModule.ArisWorkflows;
using SenseAI.Domain.Process.ArisWorkflowModel;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public sealed class IssueArisWorkflowHandler : ICommandHandler<IssueArisWorkflow, IssueArisWorkflowResult>
    {
        private readonly IDbContext _dbContext;
        private readonly IArisWorkflowRepository _arisWorkflowRepository;

        public IssueArisWorkflowHandler(IArisWorkflowRepository arisWorkflowRepository, IDbContext dbContext)
        {
            _arisWorkflowRepository = arisWorkflowRepository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<IssueArisWorkflowResult> Handle(IssueArisWorkflow command)
        {
            int nrVersions = _arisWorkflowRepository.GetNrVersions(command.ArisWorkflowId);

            if (nrVersions == 0 || command.NewVersion)
                _arisWorkflowRepository.Issue(command.ArisWorkflowId, command.ArisWorkflowImage);
            else
                _arisWorkflowRepository.Update(command.ArisWorkflowId);

            return new CommandResponse<IssueArisWorkflowResult>(new IssueArisWorkflowResult(command.ArisWorkflowId), new CommandResponseMessage((int)MessageType.Successful, ""));
        }

        public bool Validate(IssueArisWorkflow command)
        {
            if (command.ArisWorkflowId == Guid.Empty)
            {
                Errors.Add("ArisWorkflowId cannot be empty.");
                return false;
            }
            return true;
        } 
    }
}