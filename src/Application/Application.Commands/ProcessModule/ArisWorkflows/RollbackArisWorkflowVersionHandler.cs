﻿using System;
using SenseAI.Domain;
using System.Collections.Generic;
using Commands.ProcessModule.ArisWorkflows;
using Messaging.Commands;
using SenseAI.Domain.Process.ArisWorkflowModel;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public class RollbackArisWorkflowVersionHandler2 : ICommandHandler<RollbackArisWorkflowVersion, RollbackArisWorkflowVersionResult>
    {
        private readonly IArisWorkflowRepository _arisWorkflowRepository;
        private readonly ICommandHandler<IssueArisWorkflow, IssueArisWorkflowResult> _issueCommand;

        public RollbackArisWorkflowVersionHandler2(IArisWorkflowRepository arisWorkflowRepository,
            ICommandHandler<IssueArisWorkflow, IssueArisWorkflowResult> issueCommand)
        {
            _arisWorkflowRepository = arisWorkflowRepository;
            _issueCommand = issueCommand;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<RollbackArisWorkflowVersionResult> Handle(RollbackArisWorkflowVersion command)
        { 
            var designerJson = _arisWorkflowRepository.RollbackWorkflow(command.ArisWorkflowId, command.Version);

            var arisWorkflowImage = _arisWorkflowRepository.GetArisWorkflowImage(command.ArisWorkflowId, command.Version);
            IssueArisWorkflow issueArisWorkflow = new IssueArisWorkflow
            {
                ArisWorkflowId = command.ArisWorkflowId,
                NewVersion = false,
                ArisWorkflowImage = arisWorkflowImage
            };

            _issueCommand.Handle(issueArisWorkflow);

            return new CommandResponse<RollbackArisWorkflowVersionResult>(
                new RollbackArisWorkflowVersionResult(command.ArisWorkflowId, command.Version, designerJson), new CommandResponseMessage((int)MessageType.Successful, "")
            );
        }

        public bool Validate(RollbackArisWorkflowVersion command)
        {
            if (command.ArisWorkflowId == Guid.Empty)
            {
                Errors.Add("ArisWorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}