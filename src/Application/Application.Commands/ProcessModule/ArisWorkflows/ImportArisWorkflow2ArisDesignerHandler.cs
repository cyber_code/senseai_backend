﻿using Commands.ProcessModule.ArisWorkflows;
using Messaging.Commands;
using Newtonsoft.Json;
using SenseAI.Domain;
using SenseAI.Domain.Process.ArisWorkflowModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public sealed class ImportArisWorkflow2ArisDesignerHandler : ICommandHandler<ImportArisWorkflow2ArisDesigner, ImportArisWorkflow2ArisDesignerResult[]>
    { 
        private readonly IImportArisWorkflow _importArisWorkflow;
        private readonly IArisWorkflowRepository _repository;

        public List<string> Errors { get; set; }

        public ImportArisWorkflow2ArisDesignerHandler(IArisWorkflowRepository repository, IImportArisWorkflow importArisWorkflow)
        {
            _repository = repository;
            _importArisWorkflow = importArisWorkflow;
        }

        public CommandResponse<ImportArisWorkflow2ArisDesignerResult[]> Handle(ImportArisWorkflow2ArisDesigner command)
        {
            var result = new List<ImportArisWorkflow2ArisDesignerResult>();
            List<ArisWorkflow> lstArisWorkflows = _importArisWorkflow.XmlToArisWorkflow(command.FileContent);
            foreach (var arisWorkflow in lstArisWorkflows)
            {
                arisWorkflow.SetIsParsed(false);
                arisWorkflow.SetProcessId(command.ProcessId); 
                var linked = arisWorkflow.Items.Where(w => w.Type == ArisWorkflowType.Process);
                foreach (var link in linked)
                {
                    if (link.ModuleId != null)
                    {
                        var linkWorkflow = lstArisWorkflows.FirstOrDefault(w => w.ModuleId == link.ModuleId);
                        if (linkWorkflow != null)
                            link.SetArisWorkflowLinkId(linkWorkflow.Id);
                    }
                }
                result.Add(new ImportArisWorkflow2ArisDesignerResult(arisWorkflow.Id, arisWorkflow.Title, JsonConvert.SerializeObject(arisWorkflow)));
            }
            return new CommandResponse<ImportArisWorkflow2ArisDesignerResult[]>(result.ToArray(), result.Any() ? null :
                new CommandResponseMessage((int)MessageType.Information, $"No aris workflows is imported."));
        }

        public bool Validate(ImportArisWorkflow2ArisDesigner command)
        {
            if (command.ProcessId == Guid.Empty)
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }

            string ext = Path.GetExtension(command.FileName);
            if (ext != ".xml")
            {
                Errors.Add("File extension not supported!");
                return false;
            }

            if (String.IsNullOrEmpty(command.FileContent))
            {
                Errors.Add("FileContent cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
