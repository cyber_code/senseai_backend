﻿using Commands.ProcessModule.ArisWorkflows;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.Internal;
using SenseAI.Domain.AdminModel;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.Process.ArisWorkflowModel;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public sealed class ExportArisWorkflow2WorkflowDesingerHandler : ICommandHandler<ExportArisWorkflow2WorkflowDesinger, bool>
    {
        private readonly IArisWorkflowRepository _repository;
        private readonly IImportArisWorkflow _importArisWorkflow;
        private readonly IFolderRepository _folderRepository;
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IGeneratePaths _generatePaths;
        private readonly IDbContext _dbContext;

        public ExportArisWorkflow2WorkflowDesingerHandler(IArisWorkflowRepository repository,
            IImportArisWorkflow importArisWorkflow, IFolderRepository folderRepository, IWorkflowRepository workflowRepository, IGeneratePaths generatePaths, IDbContext dbContext)
        {
            _repository = repository;
            _importArisWorkflow = importArisWorkflow;
            _folderRepository = folderRepository;
            _workflowRepository = workflowRepository;
            _generatePaths = generatePaths;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(ExportArisWorkflow2WorkflowDesinger command)
        {
            var arisWorkflowDetail = _repository.GetArisWorkflow(command.ArisWorkflowId);

            string catalogTitle = _dbContext.Set<CatalogData>().AsNoTracking().FirstOrDefault(w => w.Id == command.CatalogId).Title;

            var typicals = (from typical in _dbContext.Set<TypicalData>().AsNoTracking()
                            where typical.CatalogId == command.CatalogId
                            select new Typical(typical.Id, typical.CatalogId, typical.Title, "", "", "", SenseAI.Domain.TypicalType.Application)).ToList();

            ArisWorkflow arisWorkflow = ArisWorkflowFactory.Create(command.ArisWorkflowId, arisWorkflowDetail.DesignerJson, arisWorkflowDetail.ProcessId, arisWorkflowDetail.Title, arisWorkflowDetail.Description);

            Guid folderId = _folderRepository.ArisFolder(new Folder(Guid.NewGuid(), command.SubprojectId, "Aris Workflow", "Aris Workflow"));

            Workflow workflow = _importArisWorkflow.ArisWorkflowToWorkflow(arisWorkflow, folderId, command.SystemId, command.CatalogId, catalogTitle, typicals);
            workflow.SetIsParsed(false);

            _workflowRepository.Add(workflow);
            if (workflow.Items.Count() > 0 && workflow.ItemLinks.Count() > 0)
            {
                var paths = _generatePaths.Generate(workflow);
                int index = 1;
                var workflowPathDesinger = new WorkflowPathDesinger
                {
                    WorkflowPaths = paths.Select(s =>
                    new WorkflowPath
                    {
                        Coverage = s.Coverage.ToString(),
                        IsBestPath = s.IsBestPath,
                        ItemIds = s.ItemIds,
                        Selected = true,
                        Title = "Path " + (index++)
                    }).ToArray()
                };
                var jsonPath = JsonConvert.SerializeObject(workflowPathDesinger);
                _workflowRepository.Save(workflow, JsonConvert.SerializeObject(workflow), jsonPath);
            }

            return new CommandResponse<bool>(true);
        }

        public bool Validate(ExportArisWorkflow2WorkflowDesinger command)
        {
            if (command.SubprojectId == Guid.Empty)
            {
                Errors.Add("SubprojectId cannot be empty.");
                return false;
            }

            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }

            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (command.ArisWorkflowId == Guid.Empty)
            {
                Errors.Add("ArisWorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}