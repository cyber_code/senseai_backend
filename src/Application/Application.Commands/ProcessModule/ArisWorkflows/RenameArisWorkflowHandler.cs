﻿
using Commands.ProcessModule.ArisWorkflows;
using Messaging.Commands;
using SenseAI.Domain.Process.ArisWorkflowModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.DesignModule.ArisWorkflowModel
{
    public sealed class RenameArisWorkflowHandler : ICommandHandler<RenameArisWorkflow, RenameArisWorkflowResult>
    {
        private readonly IArisWorkflowRepository _arisWorkflowRepository;

        public RenameArisWorkflowHandler(IArisWorkflowRepository arisWorkflowRepository)
        {
            _arisWorkflowRepository = arisWorkflowRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<RenameArisWorkflowResult> Handle(RenameArisWorkflow command)
        {
            _arisWorkflowRepository.Rename(command.Id, command.Title);

            return new CommandResponse<RenameArisWorkflowResult>(
                new RenameArisWorkflowResult(command.Id)
            );
        }

        public bool Validate(RenameArisWorkflow command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}