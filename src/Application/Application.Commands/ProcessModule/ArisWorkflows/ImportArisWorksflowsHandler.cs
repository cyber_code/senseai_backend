﻿using Commands.ProcessModule.ArisWorkflows;
using Messaging.Commands;
using Newtonsoft.Json;
using SenseAI.Domain.Process.ArisWorkflowModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SenseAI.Domain.Process.ProcessModel;
using SenseAI.Domain;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public sealed class ImportArisWorksflowHandler : ICommandHandler<ImportArisWorkflows, ImportArisWorkflowsResult[]>
    {
        private readonly IArisWorkflowRepository _repository;
        private readonly IImportArisWorkflow _importArisWorkflow;
        private readonly IProcessRepository _processRepository;

        public List<string> Errors { get; set; }

        public ImportArisWorksflowHandler(IArisWorkflowRepository repository, IImportArisWorkflow importArisWorkflow, 
            IProcessRepository processRepository)
        {
            _repository = repository;
            _importArisWorkflow = importArisWorkflow;
            _processRepository = processRepository;
        }

        public CommandResponse<ImportArisWorkflowsResult []> Handle(ImportArisWorkflows command)
        {
            var result = new List<ImportArisWorkflowsResult>();
            List<ArisWorkflow> lstArisWorkflows = _importArisWorkflow.XmlToArisWorkflow(command.FileContent);
            foreach (var arisWorkflow in lstArisWorkflows)
            {
                arisWorkflow.SetIsParsed(false);
                var linked = arisWorkflow.Items.Where(w => w.Type == ArisWorkflowType.Process);
                foreach (var link in linked)
                {
                    if (link.ModuleId != null)
                    {
                        var linkWorkflow = lstArisWorkflows.FirstOrDefault(w => w.ModuleId == link.ModuleId);
                        if (linkWorkflow != null)
                            link.SetArisWorkflowLinkId(linkWorkflow.Id);
                    }
                }
                Process process = new Process(arisWorkflow.ProcessId, command.SubProjectId, command.HierarchyId, Guid.NewGuid(), Guid.NewGuid(), SenseAI.Domain.ProcessType.Aris, arisWorkflow.Title, arisWorkflow.Description, Guid.Empty, Guid.Empty, 0, Guid.Empty);
                _processRepository.Add(process);
                _repository.Add(arisWorkflow);
                _repository.Save(arisWorkflow, JsonConvert.SerializeObject(arisWorkflow));
                result.Add(new ImportArisWorkflowsResult(process.Id, process.Title));
            }
            return new CommandResponse<ImportArisWorkflowsResult[]>(result.ToArray(), result.Any() ? null :
               new CommandResponseMessage((int)MessageType.Information, $"No aris workflows is imported."));
        }

        public bool Validate(ImportArisWorkflows command)
        {
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubPojectId cannot be empty.");
                return false;
            }

            if (command.HierarchyId == Guid.Empty)
            {
                Errors.Add("HierarchyId cannot be empty.");
                return false;
            }

            string ext = Path.GetExtension(command.FileName);
            if (ext != ".xml")
            {
                Errors.Add("File extension not supported!");
                return false;
            }

            if (String.IsNullOrEmpty(command.FileContent))
            {
                Errors.Add("FileContent cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
