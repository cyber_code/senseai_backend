﻿using Commands.ProcessModule.ArisWorkflows;
using Messaging.Commands;
using SenseAI.Domain.Process.ArisWorkflowModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.ProcessModule.ArisWorkflows
{
    public sealed class AddArisWorkflowHandler : ICommandHandler<AddArisWorkflow, AddArisWorkflowResult>
    {
        private readonly IArisWorkflowRepository _repository;

        public List<string> Errors { get; set; }

        public AddArisWorkflowHandler(IArisWorkflowRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<AddArisWorkflowResult> Handle(AddArisWorkflow command)
        {
            var arisWorkflow = new ArisWorkflow(Guid.NewGuid(), command.ProcessId, command.Title, command.Description, "");
            _repository.Add(arisWorkflow);
            return new CommandResponse<AddArisWorkflowResult>(new AddArisWorkflowResult(arisWorkflow.Id));
        }

        public bool Validate(AddArisWorkflow command)
        {
            if (command.ProcessId == Guid.Empty)
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }

            if (String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
