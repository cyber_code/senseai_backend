﻿using Commands.ProcessModule.RequirementDocumentGeneration;
using Core.Configuration;
using Messaging.Commands;
using Persistence.Internal;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace Application.Commands.ProcessModule.RequirementDocumentGeneration
{
    public class GenerateRequirementDocsByDateHandler : ICommandHandler<GenerateRequirementDocsByDate, GenerateRequirementDocsByDateResult>
    {
        private readonly IDbContext _dbContext;
        private readonly IDocumentGenerations _documentGenerations;
        private readonly SenseAIConfig _config;


        public GenerateRequirementDocsByDateHandler(IDbContext dbContext, IDocumentGenerations documentGenerations, SenseAIConfig config)
        {
            _dbContext = dbContext;
            _documentGenerations = documentGenerations;
            _config = config;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<GenerateRequirementDocsByDateResult> Handle(GenerateRequirementDocsByDate command)
        {
            string dir = _config.DocumentGenerationSettings.DocumentsPath + "RequirementDocumentationByDate_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            GenerateDocumentationData data = new GenerateDocumentationData(_dbContext, _config, Guid.Empty, command.ProcessId, command.Date);

            _documentGenerations.Generate(data.ProjectTitle,
                                          data.SubProjectTitle,
                                          dir,
                                          command.ApiURL,
                                          command.UiURL,
                                          data.Hierarchies,
                                          data.ProcessAndWorkflows,
                                          data.WorkflowPaths,
                                          data.WorkflowTestCases,
                                          data.DataSets,
                                          data.ProcessImages,
                                          data.AdapterSteps,
                                          data.WorkflowVideos,
                                          data.ProcessResources);

            if (File.Exists(dir + ".zip"))
                File.Delete(dir + ".zip");

            ZipFile.CreateFromDirectory(dir, dir + ".zip");

            var file = File.Open(dir + ".zip", FileMode.Open);
            var result = new GenerateRequirementDocsByDateResult(file, "RequirementDocumentationByDate.zip");
            return new CommandResponse<GenerateRequirementDocsByDateResult>(result);
        }

        public bool Validate(GenerateRequirementDocsByDate command)
        {
            if (command.ProcessId == null || command.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }
            if (command.Date == null)
            {
                Errors.Add("Date cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
