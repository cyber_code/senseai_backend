﻿using Commands.ProcessModule.RequirementDocumentGeneration;
using Core.Configuration;
using Messaging.Commands;
using Persistence.Internal;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace Application.Commands.ProcessModule.RequirementDocumentGeneration
{
    public class GenerateHierarchyDocsByDateHandler : ICommandHandler<GenerateHierarchyDocsByDate, GenerateHierarchyDocsByDateResult>
    {
        private readonly IDbContext _dbContext;
        private readonly IDocumentGenerations _documentGenerations;
        private readonly SenseAIConfig _config;


        public GenerateHierarchyDocsByDateHandler(IDbContext dbContext, IDocumentGenerations documentGenerations, SenseAIConfig config)
        {
            _dbContext = dbContext;
            _documentGenerations = documentGenerations;
            _config = config;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<GenerateHierarchyDocsByDateResult> Handle(GenerateHierarchyDocsByDate command)
        {
            string dir = _config.DocumentGenerationSettings.DocumentsPath + "HierarchyDocumentationByDate_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            GenerateDocumentationData data = new GenerateDocumentationData(_dbContext, _config, command.HierarchyId, Guid.Empty, command.Date);

            _documentGenerations.Generate(data.ProjectTitle,
                                          data.SubProjectTitle,
                                          dir,
                                          command.ApiURL,
                                          command.UiURL,
                                          data.Hierarchies,
                                          data.ProcessAndWorkflows,
                                          data.WorkflowPaths,
                                          data.WorkflowTestCases,
                                          data.DataSets,
                                          data.ProcessImages,
                                          data.AdapterSteps,
                                          data.WorkflowVideos,
                                          data.ProcessResources);

            if (File.Exists(dir + ".zip"))
                File.Delete(dir + ".zip");

            ZipFile.CreateFromDirectory(dir, dir + ".zip");

            var file = File.Open(dir + ".zip", FileMode.Open);
            var result = new GenerateHierarchyDocsByDateResult(file, "HierarchyDocumentationByDate.zip");
            return new CommandResponse<GenerateHierarchyDocsByDateResult>(result);
        }

        public bool Validate(GenerateHierarchyDocsByDate command)
        {
            if (command.HierarchyId == null || command.HierarchyId.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty.");
                return false;
            }
            if (command.Date == null)
            {
                Errors.Add("Date cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
