﻿using Commands.ProcessModule.RequirementDocumentGeneration;
using Core.Configuration;
using Messaging.Commands;
using Persistence.Internal;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace Application.Commands.ProcessModule.RequirementDocumentGeneration
{
    public class GenerateHierarchyDocsHandler : ICommandHandler<GenerateHierarchyDocs, GenerateHierarchyDocsResult>
    {
        private readonly IDbContext _dbContext;
        private readonly IDocumentGenerations _documentGenerations;
        private readonly SenseAIConfig _config;


        public GenerateHierarchyDocsHandler(IDbContext dbContext, IDocumentGenerations documentGenerations, SenseAIConfig config)
        {
            _dbContext = dbContext;
            _documentGenerations = documentGenerations;
            _config = config;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<GenerateHierarchyDocsResult> Handle(GenerateHierarchyDocs command)
        {
            string dir = _config.DocumentGenerationSettings.DocumentsPath + "HierarchyDocumentation_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            GenerateDocumentationData data = new GenerateDocumentationData(_dbContext, _config, command.HierarchyId, Guid.Empty, null);

            _documentGenerations.Generate(data.ProjectTitle, 
                                          data.SubProjectTitle,
                                          dir,
                                          command.ApiURL,
                                          command.UiURL,
                                          data.Hierarchies,
                                          data.ProcessAndWorkflows,
                                          data.WorkflowPaths,
                                          data.WorkflowTestCases,
                                          data.DataSets,
                                          data.ProcessImages,
                                          data.AdapterSteps,
                                          data.WorkflowVideos,
                                          data.ProcessResources);

            if (File.Exists(dir + ".zip"))
                File.Delete(dir + ".zip");

            ZipFile.CreateFromDirectory(dir, dir + ".zip");

            var file = File.Open(dir + ".zip", FileMode.Open);
            var result = new GenerateHierarchyDocsResult(file, "HierarchyDocumentation.zip");
            return new CommandResponse<GenerateHierarchyDocsResult>(result);
        }

        public bool Validate(GenerateHierarchyDocs command)
        {
            if (command.HierarchyId == null || command.HierarchyId.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
