﻿using Core.Configuration;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Persistence.AdminModel;
using Persistence.DataModel;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Persistence.PeopleModel;
using Persistence.Process.ArisWorkflowModel;
using Persistence.Process.GenericWorkflowModel;
using Persistence.Process.HierarchiesModel;
using Persistence.Process.ProcessModel;
using Persistence.Process.RequirementPrioritiesModel;
using Persistence.Process.RequirementTypesModel;
using Persistence.WorkflowModel;
using SenseAI.Domain.Adapters;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.PeopleModel;
using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain.Process.ProcessModel;
using SenseAI.Domain.Process.RequirementPrioritiesModel;
using SenseAI.Domain.Process.RequirementTypesModel;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Application.Commands.ProcessModule.RequirementDocumentGeneration
{
    public class GenerateDocumentationData
    {
        private readonly IDbContext dbContext;
        private readonly SenseAIConfig config;

        private readonly Dictionary<Hierarchy, List<Hierarchy>> hierarchies;
        private Dictionary<Process, List<Workflow>> processAndWorkflows;
        private Dictionary<Guid, string> processImages;
        private Dictionary<Guid, string> workflowVideos;
        Dictionary<Guid, int> processResources;
        private Dictionary<Guid, List<WorkflowVersionPaths>> workflowPaths;
        private Dictionary<Guid, List<WorkflowTestCases>> workflowTestCases;
        private Dictionary<DataSet, int> dataSets;
        private List<Guid> dsIds;
        private List<RequirementType> requirementTypes;
        private List<RequirementPriority> requirementPriorities;
        private List<People> peoples;
        private List<AdapterSteps> adapterSteps;

        private Guid hierarchyId;
        private Guid processId;
        private DateTime? date;

        private string projectTitle;
        private string subProjectTitle;

        public GenerateDocumentationData(IDbContext _dbContext, SenseAIConfig _config, Guid _hierarchyId, Guid _processId, DateTime? _date)
        {
            dbContext = _dbContext;
            config = _config;
            hierarchyId = _hierarchyId;
            processId = _processId;
            date = _date;

            hierarchies = new Dictionary<Hierarchy, List<Hierarchy>>();
            processAndWorkflows = new Dictionary<Process, List<Workflow>>();
            processImages = new Dictionary<Guid, string>();
            workflowPaths = new Dictionary<Guid, List<WorkflowVersionPaths>>();
            workflowTestCases = new Dictionary<Guid, List<WorkflowTestCases>>();
            dataSets = new Dictionary<DataSet, int>();
            workflowVideos = new Dictionary<Guid, string>();
            processResources = new Dictionary<Guid, int>();
            dsIds = new List<Guid>();

            GetData();
        }

        private void GetData()
        {
            var _hierarchyId = Guid.Empty;

            string json = "";
            using (StreamReader sr = new StreamReader(config.DocumentGenerationSettings.AdapterSteps + "AdapterSteps.json"))
            {
                json = sr.ReadToEnd();
            }
            adapterSteps = JsonConvert.DeserializeObject<List<AdapterSteps>>(json);

            if (processId != Guid.Empty)
            {
                _hierarchyId = (from p in dbContext.Set<ProcessData>().AsNoTracking()
                                where p.Id == processId
                                select p.HierarchyId).FirstOrDefault();
            }
            else
                _hierarchyId = hierarchyId;

            var hierarchy = (from h in dbContext.Set<HierarchyData>().AsNoTracking()
                             where h.Id == _hierarchyId
                             select new Hierarchy(h.Id, h.ParentId, h.SubProjectId, h.HierarchyTypeId, h.Title, h.Description)).FirstOrDefault();
            var subProjectid = hierarchy.SubProjectId;
            var _subProject = dbContext.Set<SubProjectData>().Where(s => s.Id == subProjectid).FirstOrDefault();
            var _project = dbContext.Set<ProjectData>().Where(s => s.Id == _subProject.ProjectId).FirstOrDefault();

            projectTitle = _project.Title;
            subProjectTitle = _subProject.Title;

            requirementTypes = dbContext.Set<RequirementTypeData>().AsNoTracking().Where(w => w.SubProjectId == subProjectid).Select(s => new RequirementType(s.Id, Guid.Empty, s.Title, "")).ToList();
            requirementPriorities = dbContext.Set<RequirementPriorityData>().AsNoTracking().Where(w => w.SubProjectId == subProjectid).Select(s => new RequirementPriority(s.Id, Guid.Empty, s.Title, "", 0)).ToList();
            peoples = dbContext.Set<PeopleData>().AsNoTracking().Where(w => w.SubProjectId == subProjectid).Select(s => new People(s.Id, s.Name, s.Surname)).ToList();

            if (processId == Guid.Empty)
                GetHierarchiesData(hierarchyId);
            else
                GetRequirementData(processId);

            dsIds = dsIds.Distinct().ToList();
            var _dataSets = (from ds in dbContext.Set<DataSetData>().AsNoTracking()
                             where dsIds.Contains(ds.Id)
                             select new DataSet(ds.Id, ds.Title, ds.Combinations, ds.Coverage, null)).ToList();

            _dataSets.ForEach(ds => GetDataSetItems(ds));
            if (hierarchy.ParentId != Guid.Empty)
            {
                AttachHierarchies(hierarchy);
            }
        }

        private void GetRequirementData(Guid id)
        {
            var _process = (from p in dbContext.Set<ProcessData>().AsNoTracking()
                            where p.Id == id
                            select new Process(
                                       p.Id,
                                       p.SubProjectId,
                                       p.HierarchyId,
                                       p.CreatedId,
                                       p.OwnerId,
                                       p.ProcessType,
                                       p.Title,
                                       p.Description,
                                       p.RequirementPriorityId,
                                       p.RequirementTypeId,
                                       p.ExpectedNumberOfTestCases,
                                       p.TypicalId)).FirstOrDefault();

            var _hierarchy = (from h in dbContext.Set<HierarchyData>().AsNoTracking()
                              where h.Id == _process.HierarchyId
                              select new Hierarchy(h.Id, h.ParentId, h.SubProjectId, h.HierarchyTypeId, h.Title, h.Description)).FirstOrDefault();


            hierarchies.Add(_hierarchy, null);

            GetWorkflowsForProcess(_process);
        }

        private void AttachHierarchies(Hierarchy hierarchy)
        {
            if (hierarchy.ParentId == Guid.Empty)
            {
                return;
            }
            var _hierarchy = (from h in dbContext.Set<HierarchyData>().AsNoTracking()
                              where h.Id == hierarchy.ParentId
                              select new Hierarchy(h.Id, h.ParentId, h.SubProjectId, h.HierarchyTypeId, h.Title, h.Description)).FirstOrDefault();

            List<Hierarchy> list = new List<Hierarchy>();
            list.Add(hierarchy);
            hierarchies.Add(_hierarchy, list);
            AttachHierarchies(_hierarchy);
        }

        private void GetHierarchiesData(Guid id)
        {
            var _hierarchy = (from h in dbContext.Set<HierarchyData>().AsNoTracking()
                              where h.Id == id
                              select new Hierarchy(h.Id, h.ParentId, h.SubProjectId, h.HierarchyTypeId, h.Title, h.Description)).FirstOrDefault();

            var _hierarchyTypeTitle = dbContext.Set<HierarchyTypeData>().AsNoTracking().FirstOrDefault(w => w.Id == _hierarchy.HierarchyTypeId).Title;

            _hierarchy.SetHierarchyTypeTitle(_hierarchyTypeTitle);

            var _childs = (from h in dbContext.Set<HierarchyData>().AsNoTracking()
                           join
                           ht in dbContext.Set<HierarchyTypeData>().AsNoTracking() on h.HierarchyTypeId equals ht.Id
                           where h.ParentId == id
                           select new Hierarchy(h.Id, ht.Title, h.Title, h.Description)).ToList();

            var _processes = (from p in dbContext.Set<ProcessData>().AsNoTracking()
                              where p.HierarchyId == id
                              select new Process(
                                         p.Id,
                                         p.HierarchyId,
                                         p.OwnerId == Guid.Empty ? null : peoples.FirstOrDefault(w => w.Id == p.OwnerId),
                                         p.ProcessType,
                                         p.Title,
                                         p.Description,
                                         p.RequirementTypeId == Guid.Empty ? null : requirementTypes.FirstOrDefault(w => w.Id == p.RequirementTypeId),
                                         p.RequirementPriorityId == Guid.Empty ? null : requirementPriorities.FirstOrDefault(w => w.Id == p.RequirementPriorityId),
                                         p.ExpectedNumberOfTestCases,
                                          p.TypicalId == Guid.Empty ? null : new Typical(p.TypicalId, dbContext.Set<TypicalData>().AsNoTracking().FirstOrDefault(w => w.Id == p.TypicalId).Title)
                                         )).ToList();

            _processes.ForEach(p => GetWorkflowsForProcess(p));

            if (_childs.Count > 0 || _processes.Count > 0)
            {
                hierarchies.Add(_hierarchy, _childs);
            }

            foreach (Hierarchy hd in _childs)
            {
                GetHierarchiesData(hd.Id);
            }
        }

        private void GetWorkflowsForProcess(Process process)
        {

            var _workflowsIds = new List<WorkflowVersionData>();


            if (date == null)
            {

                _workflowsIds = (from pw in dbContext.Set<ProcessWorkflowsData>()
                                 join wv in dbContext.Set<WorkflowVersionData>() on pw.WorkflowId equals wv.WorkflowId
                                 where pw.ProcessId == process.Id && wv.IsLast == true
                                 select new WorkflowVersionData
                                 {
                                     WorkflowId = wv.WorkflowId,
                                     Title = wv.Title,
                                     WorkflowImage = wv.WorkflowImage,
                                     VersionId = wv.VersionId
                                 }
                                  ).ToList();
            }
            else
            { 
                var workflows = (from pw in dbContext.Set<ProcessWorkflowsData>()
                                 join wv in dbContext.Set<WorkflowVersionData>() on pw.WorkflowId equals wv.WorkflowId
                                 where pw.ProcessId == process.Id && wv.DateModified.Value.Date == date.Value.Date
                                 orderby wv.DateModified descending
                                 select wv).ToList();
                workflows.GroupBy(g => g.WorkflowId).ToList().ForEach(f =>
                {
                    var item = f.FirstOrDefault();
                    _workflowsIds.Add(new WorkflowVersionData { WorkflowId = item.WorkflowId, VersionId = item.VersionId });
                });
            }

            string img = "";

            if (process.ProcessType == SenseAI.Domain.ProcessType.Generic)
            {
                if (date == null)
                {
                    img = (from gw in dbContext.Set<GenericWorkflowData>()
                           join gwv in dbContext.Set<GenericWorkflowVersionData>() on gw.Id equals gwv.GenericWorkflowId
                           where gw.ProcessId == process.Id && gwv.IsLast == true
                           select gwv.WorkflowImage).FirstOrDefault();
                }
                else
                {

                    img = (from gw in dbContext.Set<GenericWorkflowData>()
                           join gwv in dbContext.Set<GenericWorkflowVersionData>() on gw.Id equals gwv.GenericWorkflowId
                           where gw.ProcessId == process.Id && gwv.DateModified.Value.Date == date.Value.Date
                           select gwv.WorkflowImage).FirstOrDefault();
                }
            }
            else if (process.ProcessType == SenseAI.Domain.ProcessType.Aris)
            {
                if (date == null)
                {

                    img = (from gw in dbContext.Set<ArisWorkflowData>()
                           join gwv in dbContext.Set<ArisWorkflowVersionData>() on gw.Id equals gwv.ArisWorkflowId
                           where gw.ProcessId == process.Id && gwv.IsLast == true
                           select gwv.WorkflowImage).FirstOrDefault();
                }
                else
                {
                    img = (from gw in dbContext.Set<ArisWorkflowData>()
                           join gwv in dbContext.Set<ArisWorkflowVersionData>() on gw.Id equals gwv.ArisWorkflowId
                           where gw.ProcessId == process.Id && gwv.DateModified.Value.Date == date.Value.Date
                           select gwv.WorkflowImage).FirstOrDefault();
                }
            }
            var attachmentresources = dbContext.Set<ProcessResourcesData>().AsNoTracking().Where(w => w.ProcessId == process.Id).Count();
            processResources.Add(process.Id, attachmentresources);

            var _workflows = _workflowsIds.Select(s => new Workflow(s.WorkflowId, s.Title, s.WorkflowImage)).ToList();
            processImages.Add(process.Id, img);
            processAndWorkflows.Add(process, _workflows);

            _workflows.ForEach(w => GetWorkflowPaths(w.Id, _workflowsIds.FirstOrDefault(wf => wf.WorkflowId == w.Id).VersionId));
            _workflows.ForEach(w => GetWorkflowTestCases(w.Id, _workflowsIds.FirstOrDefault(wf => wf.WorkflowId == w.Id).VersionId));
            _workflows.ForEach(w => GetWorkflowVideos(w));
        }

        private void GetWorkflowPaths(Guid workflowId, int versionId)
        {
            var paths = (from workflowPath in dbContext.Set<WorkflowVersionPathsData>().AsNoTracking()
                         where workflowPath.WorkflowId == workflowId && workflowPath.VersionId == versionId
                         select new WorkflowVersionPaths(workflowPath.Id, workflowPath.WorkflowId, workflowPath.VersionId, workflowPath.Title,
                            workflowPath.Selected, workflowPath.Coverage, workflowPath.Index, workflowPath.IsLast)
                                 ).ToList();

            paths.ForEach(f =>
            {
                var pathItems = dbContext.Set<WorkflowVersionPathDetailsData>().AsNoTracking().Where(w => w.WorkflowId == workflowId && w.PathId == f.Id &&
                        w.VersionId == versionId).Select(s => new WorkflowVersionPathDetails
                                              (
                                                s.Id,
                                                s.WorkflowId,
                                                versionId,
                                                s.PathId,
                                                s.Title,
                                                s.Type,
                                                s.ActionType,
                                                s.CatalogId,
                                                s.TypicalId,
                                                s.TypicalName,
                                                s.TypicalType,
                                                s.DataSetId,
                                                s.PathItemJson,
                                                s.Index)).ToArray();
                dsIds.AddRange(pathItems.Where(w => w.DataSetId != Guid.Empty).Select(s => s.DataSetId));
                f.AddItems(pathItems);
            });

            if (!workflowPaths.ContainsKey(workflowId))
            {
                workflowPaths.Add(workflowId, paths);
            }
        }

        private void GetWorkflowTestCases(Guid workflowId, int versionId)
        {
            var _workflowTestCases = (from gtc in dbContext.Set<WorkflowTestCasesData>().AsNoTracking()
                                      where gtc.WorkflowId == workflowId && gtc.VersionId==versionId
                                      select new WorkflowTestCases(
                                          gtc.Id,
                                          gtc.WorkflowId,
                                          gtc.VersionId,
                                          gtc.WorkflowPathId,
                                          gtc.TestCaseId,
                                          gtc.TestCaseTitle,
                                          gtc.TCIndex,
                                          gtc.TestStepId,
                                          gtc.TestStepTitle,
                                          gtc.TestStepJson,
                                          gtc.TypicalId,
                                          gtc.TypicalName,
                                          gtc.TSIndex,
                                          gtc.TestStepType
                                          )).ToList();

            if (!workflowTestCases.ContainsKey(workflowId))
                workflowTestCases.Add(workflowId, _workflowTestCases);
        }

        private void GetWorkflowVideos(Workflow _workflow)
        {
            string videoPath = $@"{config.TestEngineSettings.VideoPath}{_workflow.Id}.mp4";
            if (File.Exists(videoPath))
            {
                if (!workflowVideos.ContainsKey(_workflow.Id))
                    workflowVideos.Add(_workflow.Id, videoPath);
            }
        }

        private void GetDataSetItems(DataSet dataSet)
        {
            var rows = dbContext.Set<DataSetItemData>().AsNoTracking().Count(c => c.DataSetId == dataSet.Id);

            dataSets.Add(dataSet, rows);
        }

        public string ProjectTitle
        {
            get { return projectTitle; }
        }
        public string SubProjectTitle
        {
            get { return subProjectTitle; }
        }
        public Dictionary<Hierarchy, List<Hierarchy>> Hierarchies
        {
            get { return hierarchies; }
        }
        public Dictionary<Process, List<Workflow>> ProcessAndWorkflows
        {
            get { return processAndWorkflows; }
        }
        public Dictionary<Guid, string> ProcessImages
        {
            get { return processImages; }
        }
        public Dictionary<Guid, string> WorkflowVideos
        {
            get { return workflowVideos; }
        }
        public Dictionary<Guid, List<WorkflowVersionPaths>> WorkflowPaths
        {
            get { return workflowPaths; }
        }

        public Dictionary<Guid, int> ProcessResources
        {
            get { return processResources; }
        }
        public Dictionary<Guid, List<WorkflowTestCases>> WorkflowTestCases
        {
            get { return workflowTestCases; }
        }
        public Dictionary<DataSet, int> DataSets
        {
            get { return dataSets; }
        }
        public List<Guid> DsIds
        {
            get { return dsIds; }
        }
        public List<RequirementType> RequirementTypes
        {
            get { return requirementTypes; }
        }
        public List<RequirementPriority> RequirementPriorities
        {
            get { return requirementPriorities; }
        }
        public List<People> Peoples
        {
            get { return peoples; }
        }
        public List<AdapterSteps> AdapterSteps
        {
            get { return adapterSteps; }
        }
    }
}
