﻿using Commands.ProcessModule.RequirementDocumentGeneration;
using Core.Configuration;
using Messaging.Commands;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Application.Commands.ProcessModule.RequirementDocumentGeneration
{
    public class GenerateRequirementDocsHandler : ICommandHandler<GenerateRequirementDocs, GenerateRequirementDocsResult>
    {
        private readonly IDbContext _dbContext;
        private readonly IDocumentGenerations _documentGenerations;
        private readonly SenseAIConfig _config;


        public GenerateRequirementDocsHandler(IDbContext dbContext, IDocumentGenerations documentGenerations, SenseAIConfig config)
        {
            _dbContext = dbContext;
            _documentGenerations = documentGenerations;
            _config = config;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<GenerateRequirementDocsResult> Handle(GenerateRequirementDocs command)
        {
            string dir = _config.DocumentGenerationSettings.DocumentsPath + "RequirementDocumentation_" + DateTime.Now.ToString("yyyyMMddHHmmss");


            var subProjectid = _dbContext.Set<ProcessData>().Where(h => h.Id == command.ProcessId).Select(i => i.SubProjectId).FirstOrDefault();
            var _subProject = _dbContext.Set<SubProjectData>().Where(s => s.Id == subProjectid).FirstOrDefault();
            var _project = _dbContext.Set<ProjectData>().Where(s => s.Id == _subProject.ProjectId).FirstOrDefault();

            GenerateDocumentationData data = new GenerateDocumentationData(_dbContext, _config, Guid.Empty, command.ProcessId, null);

            _documentGenerations.Generate(data.ProjectTitle,
                                          data.SubProjectTitle,
                                          dir,
                                          command.ApiURL,
                                          command.UiURL,
                                          data.Hierarchies,
                                          data.ProcessAndWorkflows,
                                          data.WorkflowPaths,
                                          data.WorkflowTestCases,
                                          data.DataSets,
                                          data.ProcessImages,
                                          data.AdapterSteps,
                                          data.WorkflowVideos,
                                          data.ProcessResources);

            if (File.Exists(dir + ".zip"))
                File.Delete(dir + ".zip");

            ZipFile.CreateFromDirectory(dir, dir + ".zip");

            var file = File.Open(dir + ".zip", FileMode.Open);
            var result = new GenerateRequirementDocsResult(file, "RequirementDocumentation.zip");
            return new CommandResponse<GenerateRequirementDocsResult>(result);
        }

        public bool Validate(GenerateRequirementDocs command)
        {
            if (command.ProcessId == null || command.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
