﻿using Commands.ProcessModule.ProcessResources;
using Core.Configuration;
using DataExtraction;
using Messaging.Commands;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.IssuesModel;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Application.Commands.ProcessModule.ProcessResources
{
    public sealed class AddProccessResourcesHandler : ICommandHandler<AddProcessResources, AddProcessResourcesResult[]>
    {
        private readonly IProcessResourcesRepository _processResourcesRepository;
        private readonly SenseAIConfig _config;
        private readonly IDbContext _dbContext;
        private readonly IDataExtractionClient _dataExtractionClient;

        public AddProccessResourcesHandler(IProcessResourcesRepository processResourcesRepository, SenseAIConfig config, IDbContext dbContext, IDataExtractionClient dataExtractionClient)
        {
            _processResourcesRepository = processResourcesRepository;
            _config = config;
            _dbContext = dbContext;
            _dataExtractionClient = dataExtractionClient;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddProcessResourcesResult[]> Handle(AddProcessResources command)
        {
            var result = new AddProcessResourcesResult[command.FileCollection.Count];
            int i = 0;
            System.IO.Directory.CreateDirectory(_config.ProcessResourcePath);

            var data = _dbContext.Set<ProcessData>()
                .Where(item => item.Id == command.ProcessId)
                .Select(item => new { item.ExternalSystemId, item.SubProjectId })
                .First();

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == data.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            foreach (var file in command.FileCollection)
            {
                var filePath = _config.ProcessResourcePath;

                using (var fileStream = new FileStream(filePath + file.FileName, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
                SenseAI.Domain.Process.ProcessModel.ProcessResources processResource = new SenseAI.Domain.Process.ProcessModel.ProcessResources(file.FileName, command.ProcessId, filePath + file.FileName);
                _processResourcesRepository.Add(processResource);

                if (iSJiraIntegrationEnabled)
                {
                    _dataExtractionClient.AddRequirementAttachment(new DataExtraction.JiraModel.AddAttachmentPostModel(processResource.Id,
                        file, false));
                }

                result[i++] = new AddProcessResourcesResult(processResource.Id);
            }

            return new CommandResponse<AddProcessResourcesResult[]>(result);
        }

        public bool Validate(AddProcessResources command)
        {
            if (command.ProcessId == null || command.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }
            if (command.FileCollection == null)
            {
                Errors.Add("File Content cannot be empty.");
                return false;
            }
            return true;
        }
    }
}