﻿using Commands.ProcessModule.ProcessResources;
using Core.Configuration;
using Messaging.Commands;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Application.Commands.ProcessModule.ProcessResource
{
    public sealed class UpdateProcessResourcesHandler : ICommandHandler<UpdateProcessResources, bool>
    {
        private readonly IProcessResourcesRepository _processResourcesRepository;
        private readonly SenseAIConfig _config;
        public UpdateProcessResourcesHandler(IProcessResourcesRepository processResourcesRepository, SenseAIConfig config)
        {
            _processResourcesRepository = processResourcesRepository;
            _config = config;
        }
        public List<string> Errors { get; set; }
        public CommandResponse<bool> Handle(UpdateProcessResources command)
        {
            string filePath = Path.Combine(_config.ProcessResourcePath, command.File.FileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                command.File.CopyTo(fileStream);
            }
            SenseAI.Domain.Process.ProcessModel.ProcessResources processResources = new SenseAI.Domain.Process.ProcessModel.ProcessResources(command.Id, command.File.FileName, command.ProcessId, filePath);
            _processResourcesRepository.Update(processResources);
            return new CommandResponse<bool>(true);
        }
        public bool Validate(UpdateProcessResources command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (command.ProcessId == null || command.ProcessId.Equals(Guid.Empty))
            {
                Errors.Add("ProcessId cannot be empty.");
                return false;
            }
            if (command.File is null)
            {
                Errors.Add("File cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
