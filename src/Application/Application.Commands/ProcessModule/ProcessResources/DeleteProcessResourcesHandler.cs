﻿using Commands.ProcessModule.ProcessResources;
using DataExtraction;
using DataExtraction.JiraModel;
using Messaging.Commands;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Process.ProcessModel;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Commands.ProcessModule.ProcessResource
{
    public sealed class DeleteProcessResourcesHandler : ICommandHandler<DeleteProcessResourcess, bool>
    {
        private readonly IProcessResourcesRepository _processResourcesRepository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;

        public DeleteProcessResourcesHandler(IProcessResourcesRepository processResourcesRepository, IDataExtractionClient dataExtractionClient, IDbContext dbContext)
        {
            _processResourcesRepository = processResourcesRepository;
            _dataExtractionClient = dataExtractionClient;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteProcessResourcess command)
        {
            DeleteAttachmentPostModel jiraModel = null;
            var issueResoruce = _dbContext.Set<ProcessResourcesData>()
                .Where(item => item.Id == command.Id)
                .Select(item => new { item.ProcessId, item.FileName, item.ExternalSystemId })
                .First();

            var data = _dbContext.Set<ProcessData>()
                .Where(item => item.Id == issueResoruce.ProcessId)
                .Select(item => new { item.ExternalSystemId, item.SubProjectId })
                .First();

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == data.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                jiraModel = new DeleteAttachmentPostModel(
                    command.Id, issueResoruce.ProcessId,
                    issueResoruce.FileName, issueResoruce.ExternalSystemId);
                _dataExtractionClient.DeleteRequirementAttachment(jiraModel);
            }

            _processResourcesRepository.Delete(command.Id);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteProcessResourcess command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}