﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Hierarchies;
using SenseAI.Domain.Process.HierarchiesModel;
using DataExtraction;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using Microsoft.EntityFrameworkCore;
using Persistence.AdminModel;
using System.Linq;

namespace Application.Commands.ProcessModule.Hierarchies
{
    public sealed class UpdateHierarchyHandler : ICommandHandler<UpdateHierarchy, bool>
    {
        private readonly IHierarchyRepository _repository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;

        public UpdateHierarchyHandler(IHierarchyRepository repository, IDataExtractionClient dataExtractionClient, IDbContext dbContext)
        {
            _repository = repository;
            _dataExtractionClient = dataExtractionClient;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateHierarchy command)
        {
            var parentId = Guid.NewGuid();
            if (!string.IsNullOrEmpty(command.ParentId))
                parentId = Guid.Parse(command.ParentId);

            Hierarchy hierarchy = new Hierarchy(command.Id, parentId, command.SubProjectId, command.HierarchyTypeId, command.Title, command.Description);
            _repository.Update(hierarchy);

            var lowesthierarchyTypeLevel = _dbContext.Set<HierarchyTypeData>().AsNoTracking()
                .Where(item => item.SubProjectId == command.SubProjectId)
                .OrderByDescending(item => item.Level)
                .FirstOrDefault().Level;

            var hierarchyTypeLevel = _dbContext.Set<HierarchyTypeData>().AsNoTracking()
                .Where(item => item.Id == command.HierarchyTypeId)
                .FirstOrDefault().Level;

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == command.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled && (lowesthierarchyTypeLevel == hierarchyTypeLevel))
            {
                _dataExtractionClient.AddHierarchy(
                    new DataExtraction.JiraModel.AddHierarchyPostModel(
                        hierarchy.Id
                    )
                );
            }

            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateHierarchy command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty!");
                return false;
            }

            if (command.ParentId == null || command.ParentId.Equals(Guid.Empty))
            {
                Errors.Add("ParentId cannot be empty!");
                return false;
            }

            if (command.HierarchyTypeId == null || command.HierarchyTypeId.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyTypeId cannot be empty!");
                return false;
            }

            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (string.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }

            return true;
        }
    }
}