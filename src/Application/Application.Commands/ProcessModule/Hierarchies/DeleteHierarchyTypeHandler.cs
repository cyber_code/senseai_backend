﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Hierarchies;
using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain;

namespace Application.Commands.ProcessModule.Hierarchies
{
    public sealed class DeleteHierarchyTypeHandler : ICommandHandler<DeleteHierarchyType, bool>
    {
        private readonly IHierarchyTypeRepository _repository;

        public List<string> Errors { get; set; }

        public DeleteHierarchyTypeHandler(IHierarchyTypeRepository hierarchyTypeRepository)
        {
            _repository = hierarchyTypeRepository;
        }

        public CommandResponse<bool> Handle(DeleteHierarchyType command)
        {
            var result = _repository.Delete(command.Id, command.ForceDelete);

            if (!command.ForceDelete)
            {
                string message = "";

                if (result == true)
                    message = "Hierarchy type with id: " + command.Id + " is removed!";
                else
                    message = "Hierarchy type with id: " + command.Id + " has child elements. Hierarchy type is not removed!";

                return new CommandResponse<bool>(result, new CommandResponseMessage((int)MessageType.Information, message));
            }

            return new CommandResponse<bool>(result);
        }

        public bool Validate(DeleteHierarchyType command)
        {
            if (command.Id.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyTypeId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}