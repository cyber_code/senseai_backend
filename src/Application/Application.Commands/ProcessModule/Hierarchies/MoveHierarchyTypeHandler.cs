﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Hierarchies;
using SenseAI.Domain.Process.HierarchiesModel;


namespace Application.Commands.ProcessModule.Hierarchies
{
    public sealed class MoveHierarchyTypeHandler : ICommandHandler<MoveHierarchyType, bool>
    {
        private readonly IHierarchyTypeRepository _repository;

        public List<string> Errors { get; set; }

        public MoveHierarchyTypeHandler(IHierarchyTypeRepository hierarchyTypeRepository)
        {
            _repository = hierarchyTypeRepository;
        }

        public CommandResponse<bool> Handle(MoveHierarchyType command)
        {
            _repository.Move(command.Id);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(MoveHierarchyType command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            return true;
        }
    }
}