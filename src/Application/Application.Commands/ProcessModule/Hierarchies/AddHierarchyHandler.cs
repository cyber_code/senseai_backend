﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Hierarchies;
using SenseAI.Domain.Process.HierarchiesModel;
using DataExtraction;
using Persistence.Internal;
using Persistence.AdminModel;
using System.Linq;
using Persistence.Process.HierarchiesModel;
using Microsoft.EntityFrameworkCore;

namespace Application.Commands.ProcessModule.Hierarchies
{
    public sealed class AddHierarchyHandler : ICommandHandler<AddHierarchy, AddHierarchyResult>
    {
        private readonly IHierarchyRepository _repository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;

        public AddHierarchyHandler(IHierarchyRepository repository, IDataExtractionClient dataExtractionClient, IDbContext dbContext)
        {
            _repository = repository;
            _dataExtractionClient = dataExtractionClient;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddHierarchyResult> Handle(AddHierarchy command)
        {
            Guid hierarchyTypeId = Guid.Empty;
            if (!string.IsNullOrEmpty(command.HierarchyTypeId))
                hierarchyTypeId = Guid.Parse(command.HierarchyTypeId);

            Guid parentId = Guid.Empty;
            if (!string.IsNullOrEmpty(command.ParentId))
                parentId = Guid.Parse(command.ParentId);
            Hierarchy hierarchy = new Hierarchy(parentId, command.SubProjectId, hierarchyTypeId, command.Title, command.Description);
            _repository.Add(hierarchy);

            var lowesthierarchyTypeLevel = _dbContext.Set<HierarchyTypeData>().AsNoTracking()
                .Where(item => item.SubProjectId == command.SubProjectId)
                .OrderByDescending(item => item.Level)
                .FirstOrDefault().Level;
            lowesthierarchyTypeLevel = lowesthierarchyTypeLevel - 1;
            var hierarchyTypeLevel = _dbContext.Set<HierarchyTypeData>().AsNoTracking()
                .Where(item => item.Id == hierarchyTypeId)
                .FirstOrDefault()?.Level;

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == command.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled && (lowesthierarchyTypeLevel == hierarchyTypeLevel))
            {
                _dataExtractionClient.AddHierarchy(
                    new DataExtraction.JiraModel.AddHierarchyPostModel(
                        hierarchy.Id
                    )
                );
            }

            return new CommandResponse<AddHierarchyResult>(new AddHierarchyResult(hierarchy.Id));
        }

        public bool Validate(AddHierarchy command)
        {
            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (string.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }
            return true;
        }
    }
}