﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Hierarchies;
using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain;

namespace Application.Commands.ProcessModule.Hierarchies
{
    public sealed class AddHierarchyTypeHandler : ICommandHandler<AddHierarchyType, AddHierarchyTypeResult>
    {
        private readonly IHierarchyTypeRepository _repository;
        public List<string> Errors { get; set; }

        public AddHierarchyTypeHandler(IHierarchyTypeRepository hierarchyTypeRepository)
        {
            _repository = hierarchyTypeRepository;
        }

        public CommandResponse<AddHierarchyTypeResult> Handle(AddHierarchyType command)
        {
            Guid parentId = Guid.Empty;
            if (!string.IsNullOrEmpty(command.ParentId))
                Guid.TryParse(command.ParentId, out parentId);
            HierarchyType hierarchyType = new HierarchyType(parentId, command.SubProjectId, command.Title, command.Description);
            string parentName = "";
          var result=  _repository.Add(hierarchyType,command.ForceDelete,out parentName);

         string message = "";
            if (!command.ForceDelete)
            {


                if (result == true)
                {
                    message = "Hierarchy type with title: " + hierarchyType.Title + " is added!";
                    return new CommandResponse<AddHierarchyTypeResult>(new AddHierarchyTypeResult(hierarchyType.Id), new CommandResponseMessage((int)MessageType.Information, message));
                }
                else
                {
                    message = "You have already created Requirements on " + parentName + " level. If you want to add a new type you need to delete all the  created Requirements!";
                    return new CommandResponse<AddHierarchyTypeResult>(new AddHierarchyTypeResult(Guid.Empty), new CommandResponseMessage((int)MessageType.Information, message));
                }

                
            }
            return new CommandResponse<AddHierarchyTypeResult>(new AddHierarchyTypeResult(hierarchyType.Id));
        }

        public bool Validate(AddHierarchyType command)
        {
            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (string.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }
            return true;
        }
    }
}