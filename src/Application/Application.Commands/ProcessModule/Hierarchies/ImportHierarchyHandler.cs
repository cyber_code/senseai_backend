﻿using Commands.ProcessModule.Hierarchies;
using Messaging.Commands;
using Persistence.Process.HierarchiesModel;
using SenseAI.Domain;
using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain.Process.ProcessModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Commands.ProcessModule.Hierarchies
{
    public sealed class ImportHierarchyHandler : ICommandHandler<ImportHierarchy, bool>
    {
        private readonly IHierarchyTypeRepository _hierarchyTypeRepository;
        private readonly IHierarchyRepository _hierarchyRepository;
        private readonly IImportHierarchy _importHierarchy;
        private readonly IProcessRepository _processRepository;

        public ImportHierarchyHandler(IHierarchyTypeRepository hierarchyTypeRepository, IHierarchyRepository hierarchyRepository, IImportHierarchy importHierarchy, IProcessRepository processRepository)
        {
            _hierarchyTypeRepository = hierarchyTypeRepository;
            _hierarchyRepository = hierarchyRepository;
            _importHierarchy = importHierarchy;
            _processRepository = processRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(ImportHierarchy command)
        {
            var result = _hierarchyTypeRepository.CheckForHierarchyTypes(command.SubprojectId, command.ForceDelete);

            if (!command.ForceDelete && result)
            {
                string message = "";
                if (result == true)
                    message = "HierarchyTypes already exist with SubProjectId: " + command.SubprojectId + ". You can cancel import or delete HierarchyTypes.";

                return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Information, message));
            }

            //read excel
            var hImportModel = _importHierarchy.ImportHierarchyFromExcelFile(command.SubprojectId, command.File);
            //add header and data from excel
            var listOfHierarchies = AddLevels(command.SubprojectId, hImportModel);

            //Guid htId = Guid.Empty;
            foreach (var item in listOfHierarchies.ListHierarchies)
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(item.Title))
                    {
                        var hierarchy = new Hierarchy(item.Id, item.ParentId, command.SubprojectId, item.HierarchyTypeId, item.Title, item.Description);
                        _hierarchyRepository.AddFromImport(hierarchy);
                    }
                }
                catch (Exception ex)
                {
                }
            }
            foreach (var item in listOfHierarchies.ListProcessess)
            {
                if (!string.IsNullOrWhiteSpace(item.Title))
                {
                    var process = new Process(item.Id, command.SubprojectId, item.HierarchyId, Guid.NewGuid(), Guid.NewGuid(), ProcessType.Aris, item.Title, item.Description, Guid.Empty, Guid.Empty, 0, Guid.Empty);
                    _processRepository.Add(process);
                }
            }

            return new CommandResponse<bool>(true, new CommandResponseMessage((int)MessageType.Successful, ""));
        }

        public bool Validate(ImportHierarchy command)
        {
            if (command.SubprojectId == null || command.SubprojectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            if (command.File == null)
            {
                Errors.Add("File cannot be empty");
                return false;
            }
            return true;
        }

        private ImportModelStructure AddLevels(Guid subProjectId, HierarchyImportModel hModel)
        {
            List<Hierarchy> lstHierarchy = new List<Hierarchy>();
            List<Process> lstProcess = new List<Process>();
            var level1Id = Guid.Empty;
            var level2Id = Guid.Empty;
            var level3Id = Guid.Empty;
            var level4Id = Guid.Empty;
            var level5Id = Guid.Empty;
            var headerLevel1Id = Guid.Empty;
            var headerLevel2Id = Guid.Empty;
            var headerLevel3Id = Guid.Empty;
            var headerLevel4Id = Guid.Empty;
            var headerLevel5Id = Guid.Empty;
            var processHierarchyId = Guid.Empty;
            string parentName = "";
            //get hierarchyTypes by subprojectId
            var listHierarchyTypes = _hierarchyTypeRepository.GetHierarchyTypeLevels(subProjectId);
            if (listHierarchyTypes.Count == 0)
            {
                foreach (var item in hModel.HierarchyTypeImportModelTable)
                {
                    switch (item.HeaderLevel)
                    {
                        case 1:
                            if (!string.IsNullOrWhiteSpace(item.HeaderField1))
                            {
                                headerLevel1Id = Guid.NewGuid();
                                var hierarchytype = new HierarchyType(headerLevel1Id, Guid.Empty, subProjectId, item.HeaderField1, item.HeaderField1);
                                _hierarchyTypeRepository.Add(hierarchytype,false,out parentName);
                            }
                            break;

                        case 2:
                            if (!string.IsNullOrWhiteSpace(item.HeaderField2))
                            {
                                headerLevel2Id = Guid.NewGuid();
                                var hierarchytype = new HierarchyType(headerLevel2Id, headerLevel1Id, subProjectId, item.HeaderField2, item.HeaderField2);
                                _hierarchyTypeRepository.Add(hierarchytype,false, out parentName);
                            }
                            break;

                        case 3:
                            if (!string.IsNullOrWhiteSpace(item.HeaderField3))
                            {
                                headerLevel3Id = Guid.NewGuid();
                                var hierarchytype = new HierarchyType(headerLevel3Id, headerLevel2Id, subProjectId, item.HeaderField3, item.HeaderField3);
                                _hierarchyTypeRepository.Add(hierarchytype,false, out parentName);
                            }
                            break;

                        case 4:
                            if (!string.IsNullOrWhiteSpace(item.HeaderField4))
                            {
                                headerLevel4Id = Guid.NewGuid();
                                var hierarchytype = new HierarchyType(headerLevel4Id, headerLevel3Id, subProjectId, item.HeaderField4, item.HeaderField4);
                                _hierarchyTypeRepository.Add(hierarchytype,false, out parentName);
                            }
                            break;

                        case 5:
                            if (!string.IsNullOrWhiteSpace(item.HeaderField5))
                            {
                                headerLevel5Id = Guid.NewGuid();
                                var hierarchytype = new HierarchyType(headerLevel5Id, headerLevel4Id, subProjectId, item.HeaderField5, item.HeaderField5);
                                _hierarchyTypeRepository.Add(hierarchytype,false, out parentName);
                            }
                            break;
                    }
                }
                listHierarchyTypes = _hierarchyTypeRepository.GetHierarchyTypeLevels(subProjectId);
            }

            foreach (var item in hModel.HierarchyImportModelTabel)
            {
                switch (item.Level)
                {
                    case 1:
                        if (!string.IsNullOrWhiteSpace(item.Field1))
                        {
                            level1Id = Guid.NewGuid();
                            processHierarchyId = level1Id;
                            var hierarchy = new Hierarchy(level1Id, Guid.Empty, Guid.Empty, listHierarchyTypes[item.Level - 1].Id, item.Field1, item.Field1);
                            lstHierarchy.Add(hierarchy);
                        }
                        break;

                    case 2:
                        if (!string.IsNullOrWhiteSpace(item.Field2))
                        {
                            level2Id = Guid.NewGuid();
                            processHierarchyId = level2Id;
                            var hierarchy = new Hierarchy(level2Id, level1Id, Guid.Empty, listHierarchyTypes[item.Level - 1].Id, item.Field2, item.Field2);
                            lstHierarchy.Add(hierarchy);
                        }
                        break;

                    case 3:
                        if (!string.IsNullOrWhiteSpace(item.Field3))
                        {
                            level3Id = Guid.NewGuid();
                            processHierarchyId = level3Id;
                            var hierarchy = new Hierarchy(level3Id, level2Id, Guid.Empty, listHierarchyTypes[item.Level - 1].Id, item.Field3, item.Field3);
                            lstHierarchy.Add(hierarchy);
                        }
                        break;

                    case 4:
                        if (!string.IsNullOrWhiteSpace(item.Field4))
                        {
                            level4Id = Guid.NewGuid();
                            processHierarchyId = level4Id;
                            var hierarchy = new Hierarchy(level4Id, level3Id, Guid.Empty, listHierarchyTypes[item.Level - 1].Id, item.Field4, item.Field4);
                            lstHierarchy.Add(hierarchy);
                        }
                        break;

                    case 5:
                        if (!string.IsNullOrWhiteSpace(item.Field5))
                        {
                            level5Id = Guid.NewGuid();
                            processHierarchyId = level5Id;
                            var hierarchy = new Hierarchy(level5Id, level4Id, Guid.Empty, listHierarchyTypes[item.Level - 1].Id, item.Field5, item.Field5);
                            lstHierarchy.Add(hierarchy);
                        }
                        break;

                    case 6:
                        var process = new Process(Guid.NewGuid(), subProjectId, processHierarchyId, Guid.NewGuid(), Guid.NewGuid(), ProcessType.Aris, item.Field6, item.Field7, Guid.Empty, Guid.Empty, 0, Guid.Empty);
                        lstProcess.Add(process);
                        break;
                }
            }
            return new ImportModelStructure(lstHierarchy, lstProcess);
        }
    }
}