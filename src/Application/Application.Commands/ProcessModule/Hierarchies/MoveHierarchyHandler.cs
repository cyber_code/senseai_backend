﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Hierarchies;
using SenseAI.Domain.Process.HierarchiesModel;


namespace Application.Commands.ProcessModule.Hierarchies
{
    public sealed class MoveHierarchyHandler : ICommandHandler<MoveHierarchy, bool>
    {
        private readonly IHierarchyRepository _repository;

        public List<string> Errors { get; set; }

        public MoveHierarchyHandler(IHierarchyRepository hierarchyRepository)
        {
            _repository = hierarchyRepository;
        }

        public CommandResponse<bool> Handle(MoveHierarchy command)
        {
            _repository.Move(command.FromHierarchyId, command.ToHierarchyId);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(MoveHierarchy command)
        {
            if (command.FromHierarchyId == null || command.FromHierarchyId.Equals(Guid.Empty))
            {
                Errors.Add("FromHierarchyId cannot be empty!");
                return false;
            }

            if (command.ToHierarchyId == null || command.ToHierarchyId.Equals(Guid.Empty))
            {
                Errors.Add("ToHierarchyId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}