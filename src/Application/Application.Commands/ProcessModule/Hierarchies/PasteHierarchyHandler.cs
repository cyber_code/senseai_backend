﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Messaging.Commands;
using Commands.ProcessModule.Hierarchies;
using Persistence.Internal;
using Persistence.Process.HierarchiesModel;
using SenseAI.Domain.Process.HierarchiesModel;
using SenseAI.Domain;

namespace Application.Commands.ProcessModule.Hierarchies
{
    public sealed class PasteHierarchyHandler : ICommandHandler<PasteHierarchy, PasteHierarchyResult>
    {
        private readonly IDbContext _dbContext;
        private readonly IHierarchyRepository _repository;

        public List<string> Errors { get; set; }

        public PasteHierarchyHandler(IDbContext dbContext, IHierarchyRepository repository)
        {
            _dbContext = dbContext;
            _repository = repository;
        }

        public CommandResponse<PasteHierarchyResult> Handle(PasteHierarchy command)
        {
            HierarchyData hierarchyData = _dbContext.Set<HierarchyData>()
                                                    .AsNoTracking()
                                                    .FirstOrDefault(hD => hD.Id == command.HierarchyId);

            if (hierarchyData == null)
            {
                throw new Exception("This Hierarchy does not exists!");
            }
            Hierarchy hierarchy = null;
            if (command.PasteType.Equals(PasteType.Copy))
            {
                /*
                 * During the Copy we manage to create a New Copy of a particular
                 * Hierarchy object. The New object would have new Id, modified Title
                 * and may have a different ParentId. The other attributes would be identical.
                 */

                hierarchy = new Hierarchy(command.ParentId,
                                                    hierarchyData.SubProjectId,
                                                    hierarchyData.HierarchyTypeId,
                                                    hierarchyData.Title + " - Copy",
                                                    hierarchyData.Description);

                _repository.Add(hierarchy);
            }
            else if (command.PasteType.Equals(PasteType.Cut))
            {
                /*
                 * During the Cut we manage to move a selected Hierarchy object by
                 * changing only its ParentId.
                 */

                hierarchy = new Hierarchy(hierarchyData.Id,
                                                    command.ParentId,
                                                    hierarchyData.SubProjectId,
                                                    hierarchyData.HierarchyTypeId,
                                                    hierarchyData.Title,
                                                    hierarchyData.Description);

                _repository.Update(hierarchy);
            }

            return new CommandResponse<PasteHierarchyResult>(new PasteHierarchyResult(hierarchy.Id, hierarchy.Title));
        }

        public bool Validate(PasteHierarchy command)
        {
            if (command.HierarchyId.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}