﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Hierarchies;
using SenseAI.Domain.Process.HierarchiesModel;
using Persistence.Internal;
using SenseAI.Domain;
using DataExtraction.JiraModel;
using DataExtraction;
using Persistence.AdminModel;
using System.Linq;
using Persistence.Process.HierarchiesModel;

namespace Application.Commands.ProcessModule.Hierarchies
{
    public sealed class DeleteHierarchyHandler : ICommandHandler<DeleteHierarchy, bool>
    {
        private readonly IHierarchyRepository _repository;
        private readonly IDataExtractionClient _dataExtractionClient;
        private readonly IDbContext _dbContext;

        public DeleteHierarchyHandler(IHierarchyRepository repository, IDataExtractionClient dataExtractionClient, IDbContext dbContext)
        {
            _repository = repository;
            _dataExtractionClient = dataExtractionClient;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteHierarchy command)
        {
            DeleteHierarchyPostModel jiraModel = null;

            var data = _dbContext.Set<HierarchyData>()
                .Where(item => item.Id == command.Id)
                .Select(item => new { item.ExternalSystemId, item.SubProjectId })
                .First();

            var iSJiraIntegrationEnabled = _dbContext.Set<SubProjectData>()
                .Where(item => item.Id == data.SubProjectId)
                .Select(item => item.IsJiraIntegrationEnabled)
                .First();

            if (iSJiraIntegrationEnabled)
            {
                jiraModel = new DeleteHierarchyPostModel(command.Id, data.SubProjectId, data.ExternalSystemId);
            }

            var result = _repository.Delete(command.Id, command.ForceDelete);

            if (iSJiraIntegrationEnabled)
            {
                _dataExtractionClient.DeleteHierarchy(jiraModel);
            }

            if (!command.ForceDelete)
            {
                string message = "";

                if (result == true)
                    message = "Hierarchy with id: " + command.Id + " is removed!";
                else
                    message = "Hierarchy with id: " + command.Id + " has child elements. Hierarchy is not removed!";

                return new CommandResponse<bool>(result, new CommandResponseMessage((int)MessageType.Information, message));
            }

            return new CommandResponse<bool>(result);
        }

        public bool Validate(DeleteHierarchy command)
        {
            if (command.Id.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}