﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.ProcessModule.Hierarchies;
using SenseAI.Domain.Process.HierarchiesModel;

namespace Application.Commands.ProcessModule.Hierarchies
{
    public sealed class UpdateHierarchyTypeHandler : ICommandHandler<UpdateHierarchyType, bool>
    {
        private readonly IHierarchyTypeRepository _repository;
        public List<string> Errors { get; set; }

        public UpdateHierarchyTypeHandler(IHierarchyTypeRepository hierarchyTypeRepository)
        {
            _repository = hierarchyTypeRepository;
        }

        public CommandResponse<bool> Handle(UpdateHierarchyType command)
        {
            HierarchyType hierarchyType = new HierarchyType(command.Id, command.ParentId, command.SubProjectId, command.Title, command.Description);
            _repository.Update(hierarchyType);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateHierarchyType command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("HierarchyTypeId cannot be empty!");
                return false;
            }


            if(command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (string.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }
             
            return true;
        }
    }
}