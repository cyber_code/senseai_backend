﻿
using Messaging.Commands;
using System.Collections.Generic;
using SenseAI.Domain.Process.SprintsModel;
using Commands.ProcessModule.Sprints;
using System;

namespace Application.Commands.ProcessModule.Sprints
{
    public sealed class AddSprintsHandler : ICommandHandler<AddSprints, AddSprintsResult>
    {
        private readonly ISprintsRepository _repository;

        public AddSprintsHandler(ISprintsRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddSprintsResult> Handle(AddSprints command)
        {
            SenseAI.Domain.Process.SprintsModel.Sprints Sprints = new SenseAI.Domain.Process.SprintsModel.Sprints(command.Title, command.Description,command.Start,command.End, command.SubProjectId, command.Duration, command.DurationUnit);

            _repository.Add(Sprints);

            return new CommandResponse<AddSprintsResult>(new AddSprintsResult(Sprints.Id));
        }

        public bool Validate(AddSprints command)
        {
            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}