﻿
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using Commands.ProcessModule.Sprints;
using SenseAI.Domain.Process.SprintsModel;

namespace Application.Commands.ProcessModule.Sprints
{
    public sealed class DeleteSprintIssuesHandler : ICommandHandler<DeleteSprintIssues, bool>
    {
        private readonly ISprintIssuesRepository _repository;

        public DeleteSprintIssuesHandler(ISprintIssuesRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteSprintIssues command)
        {
            bool tmpResult = false;

            _repository.Delete(command.Id);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(DeleteSprintIssues command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}