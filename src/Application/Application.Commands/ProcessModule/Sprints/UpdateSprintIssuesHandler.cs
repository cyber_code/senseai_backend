﻿
using Messaging.Commands;
using System;
using System.Collections.Generic;
using Commands.ProcessModule.Sprints;
using SenseAI.Domain.Process.SprintsModel;

namespace Application.Commands.ProcessModule.Sprints
{
    public sealed class UpdateSprintIssuesHandler : ICommandHandler<UpdateSprintIssues, bool>
    {
        private readonly ISprintIssuesRepository _repository;

        public UpdateSprintIssuesHandler(ISprintIssuesRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateSprintIssues command)
        {
            bool tmpResult = false;
            SprintIssues SprintIssues = new SprintIssues(command.Id, command.SprintId, command.IssueId, command.Status);

            _repository.Update(SprintIssues);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(UpdateSprintIssues command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            if (command.SprintId == Guid.Empty)
            {
                Errors.Add("SprintId cannot be empty.");
                return false;
            }
            if (command.IssueId == Guid.Empty)
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}