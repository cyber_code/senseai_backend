﻿
using Messaging.Commands;
using System.Collections.Generic;
using SenseAI.Domain.Process.SprintsModel;
using Commands.ProcessModule.Sprints;
using System;

namespace Application.Commands.ProcessModule.Sprints
{
    public sealed class AddSprintIssuesHandler : ICommandHandler<AddSprintIssues, AddSprintIssuesResult>
    {
        private readonly ISprintIssuesRepository _repository;

        public AddSprintIssuesHandler(ISprintIssuesRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddSprintIssuesResult> Handle(AddSprintIssues command)
        {
            SprintIssues SprintIssues = new SprintIssues(command.SprintId, command.IssueId,command.Status);

            _repository.Add(SprintIssues);

            return new CommandResponse<AddSprintIssuesResult>(new AddSprintIssuesResult(SprintIssues.Id));
        }

        public bool Validate(AddSprintIssues command)
        {
            if (command.SprintId== Guid.Empty)
            {
                Errors.Add("SprintId cannot be empty.");
                return false;
            }
            if (command.IssueId == Guid.Empty)
            {
                Errors.Add("IssueId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}