﻿
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using Commands.ProcessModule.Sprints;
using SenseAI.Domain.Process.SprintsModel;

namespace Application.Commands.ProcessModule.Sprints
{
    public sealed class StartSprintsHandler : ICommandHandler<StartSprints, bool>
    {
        private readonly ISprintsRepository _repository;

        public StartSprintsHandler(ISprintsRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(StartSprints command)
        {
            bool tmpResult = false;

            _repository.Start(command.Id);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(StartSprints command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}