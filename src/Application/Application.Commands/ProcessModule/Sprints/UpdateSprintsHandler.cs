﻿
using Messaging.Commands;
using System;
using System.Collections.Generic;
using Commands.ProcessModule.Sprints;
using SenseAI.Domain.Process.SprintsModel;

namespace Application.Commands.ProcessModule.Sprints
{
    public sealed class UpdateSprintsHandler : ICommandHandler<UpdateSprints, bool>
    {
        private readonly ISprintsRepository _repository;

        public UpdateSprintsHandler(ISprintsRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateSprints command)
        {
            bool tmpResult = false;
            SenseAI.Domain.Process.SprintsModel.Sprints Sprints = new SenseAI.Domain.Process.SprintsModel.Sprints(command.Id, command.Title, command.Description, command.Start,command.End, command.SubProjectId, command.Duration, command.DurationUnit);

            _repository.Update(Sprints);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(UpdateSprints command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}