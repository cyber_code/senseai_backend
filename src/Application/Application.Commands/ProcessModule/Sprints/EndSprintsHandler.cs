﻿
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using Commands.ProcessModule.Sprints;
using SenseAI.Domain.Process.SprintsModel;

namespace Application.Commands.ProcessModule.Sprints
{
    public sealed class EndSprintsHandler : ICommandHandler<EndSprints, bool>
    {
        private readonly ISprintsRepository _repository;

        public EndSprintsHandler(ISprintsRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(EndSprints command)
        {
            bool tmpResult = false;

            _repository.End(command.Id);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(EndSprints command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}