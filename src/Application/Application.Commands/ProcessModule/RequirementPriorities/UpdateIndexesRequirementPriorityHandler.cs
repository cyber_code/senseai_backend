﻿using Commands.ProcessModule.RequirementPriorities;
using Messaging.Commands;
using Persistence.Internal;
using SenseAI.Domain.Process.RequirementPrioritiesModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.ProcessModule.RequirementPrioritys
{
    public class UpdateIndexesRequirementPriorityHandler : ICommandHandler<UpdateIndexesRequirementPriority, bool>
    {
        private readonly IRequirementPriorityRepository _repository;

        public List<string> Errors { get; set; }

        public UpdateIndexesRequirementPriorityHandler(IRequirementPriorityRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<bool> Handle(UpdateIndexesRequirementPriority command)
        {
            bool result = false;
            int i = 1;
            foreach (var priority in command.RequirementPriorities)
            {
                RequirementPriority requirementPriority = new RequirementPriority(priority.Id, priority.SubProjectId, priority.Title, priority.Code, i++);

                _repository.UpdateIndex(requirementPriority);
            }
            result = true;
            return new CommandResponse<bool>(result);
        }

        public bool Validate(UpdateIndexesRequirementPriority command)
        {

            return true;
        }
    }
}
