﻿using System;
using System.Collections.Generic;
using Commands.ProcessModule.RequirementPriorities;
using Messaging.Commands;
using SenseAI.Domain.Process.RequirementPrioritiesModel;

namespace Application.Commands.ProcessModule.RequirementPrioritys
{
    public sealed class DeleteRequirementPriorityHandler : ICommandHandler<DeleteRequirementPriority, bool>
    {
        private readonly IRequirementPriorityRepository _repository;
        public List<string> Errors { get; set; }

        public DeleteRequirementPriorityHandler(IRequirementPriorityRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<bool> Handle(DeleteRequirementPriority command)
        {
            _repository.Delete(command.Id, command.ForceDelete);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteRequirementPriority command)
        {
            if (command.Id.Equals(Guid.Empty))
            {
                Errors.Add("RequirementPriorityId cannot be empty!");
                return false;
            }

            return true;
        }
    }
}