﻿using Commands.ProcessModule.RequirementPriorities;
using Messaging.Commands;
using Persistence.Internal;
using SenseAI.Domain.Process.RequirementPrioritiesModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.ProcessModule.RequirementPrioritys
{
    public class UpdateRequirementPriorityHandler : ICommandHandler<UpdateRequirementPriority, bool>
    {
        private readonly IRequirementPriorityRepository _repository;

        public List<string> Errors { get; set; }

        public UpdateRequirementPriorityHandler(IRequirementPriorityRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<bool> Handle(UpdateRequirementPriority command)
        {
            bool result = false;
            RequirementPriority requirementPriority = new RequirementPriority(command.Id, command.SubProjectId, command.Title, command.Code,0);
            _repository.Update(requirementPriority);
            result = true;
            return new CommandResponse<bool>(result);
        }

        public bool Validate(UpdateRequirementPriority command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("RequirementPriorityId cannot be empty!");
                return false;
            }

            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }

            return true;
        }
    }
}
