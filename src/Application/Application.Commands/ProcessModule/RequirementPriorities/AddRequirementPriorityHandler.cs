﻿using System;
using System.Collections.Generic;
using Commands.ProcessModule.RequirementPriorities;
using Messaging.Commands;
using Persistence.Internal;
using Persistence.Process.RequirementPrioritiesModel;
using SenseAI.Domain.Process.RequirementPrioritiesModel;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Application.Commands.ProcessModule.RequirementPriorities
{
    public sealed class AddRequirementPriorityHandler : ICommandHandler<AddRequirementPriority, AddRequirementPriorityResult>
    {
        private readonly IRequirementPriorityRepository _repository;
        private readonly IDbContext _dbContext;

        public AddRequirementPriorityHandler(IRequirementPriorityRepository repository, IDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddRequirementPriorityResult> Handle(AddRequirementPriority command)
        {
          var query= (from requirementPriority in _dbContext.Set<RequirementPriorityData>().AsNoTracking()
             where requirementPriority.SubProjectId == command.SubProjectId
             select requirementPriority.Index).DefaultIfEmpty(0).Max();
            RequirementPriority requirementType = new RequirementPriority(command.SubProjectId, command.Title, command.Code,query+1);
            _repository.Add(requirementType);

            return new CommandResponse<AddRequirementPriorityResult>(new AddRequirementPriorityResult(requirementType.Id));
        }

        public bool Validate(AddRequirementPriority command)
        {
            if (command.SubProjectId == null || command.SubProjectId.Equals(Guid.Empty))
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }

            return true;
        }
    }
}
