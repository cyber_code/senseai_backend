﻿using Commands.ProcessModule.JiraIntegration;
using Jira;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System.Collections.Generic;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraWebhooksHandler : ICommandHandler<UpdateJiraWebhooks, bool>
    {
        private readonly IJiraClient _jiraClient;
        private readonly IJiraProjectMappingRepository _jiraProjectMappingRepository;

        public UpdateJiraWebhooksHandler(IJiraClient jiraClient, IJiraProjectMappingRepository jiraProjectMappingRepository)
        {
            _jiraClient = jiraClient;
            _jiraProjectMappingRepository = jiraProjectMappingRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateJiraWebhooks command)
        {
            var projectMapping = _jiraProjectMappingRepository.GetJiraProjectMappingBySubprojectId(command.SubProjectId);
            _jiraClient.UpdateWebhook(new Jira.Webhook.UpdateJiraWebhook
            {
                Self = command.Self,
                Name = command.Name,
                SubprojectId = command.SubProjectId,
                Filters = new Jira.Webhook.Filters { IssueRelatedEventsSection = $"project = \"{projectMapping.JiraProjectName }\""},
                Events = command.Events,
                Url = command.Url,
                JiraProjectKey = projectMapping.JiraProjectKey,
                JiraProjectName = projectMapping.JiraProjectName,
                Enabled = command.Enabled
            }
            );
            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateJiraWebhooks command)
        {
            return true;
        }
    }
}