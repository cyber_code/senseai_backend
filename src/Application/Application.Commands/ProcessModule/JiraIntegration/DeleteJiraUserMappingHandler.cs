﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class DeleteJiraUserMappingHandler : ICommandHandler<DeleteJiraUserMapping, bool>
    {
        private readonly IJiraUserMappingRepository _repository;

        public DeleteJiraUserMappingHandler(IJiraUserMappingRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteJiraUserMapping command)
        {
            _repository.Delete(command.Id);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteJiraUserMapping command)
        {
            if (command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}