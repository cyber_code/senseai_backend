﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraIssueTypeAttributeValueMappingHandler : ICommandHandler<AddJiraIssueTypeAttributeValueMapping, AddJiraIssueTypeAttributeValueMappingResult>
    {
        private readonly IJiraIssueTypeAttributeValueMappingRepository _repository;

        public AddJiraIssueTypeAttributeValueMappingHandler(IJiraIssueTypeAttributeValueMappingRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddJiraIssueTypeAttributeValueMappingResult> Handle(AddJiraIssueTypeAttributeValueMapping command)
        {
            JiraIssueTypeAttributeValueMapping jiraIssueTypeAttributeValueMapping = new JiraIssueTypeAttributeValueMapping(command.JiraIssueTypeAttributeMappingId, command.SenseaiValue, command.JiraValue);
            _repository.Add(jiraIssueTypeAttributeValueMapping);
            return new CommandResponse<AddJiraIssueTypeAttributeValueMappingResult>(new AddJiraIssueTypeAttributeValueMappingResult(jiraIssueTypeAttributeValueMapping.Id));
        }

        public bool Validate(AddJiraIssueTypeAttributeValueMapping command)
        {
            if (command.JiraIssueTypeAttributeMappingId == Guid.Empty)
            {
                Errors.Add("JiraIssueTypeAttributeMappingId cannot be empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.SenseaiValue))
            {
                Errors.Add("JiraValue cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraValue))
            {
                Errors.Add("SenseaiValue cannot be empty.");
                return false;
            }
            return true;
        }
    }
}