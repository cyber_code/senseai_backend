﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraIssueTypeAttributeValueMappingHandler : ICommandHandler<UpdateJiraIssueTypeAttributeValueMapping, bool>
    {
        private readonly IJiraIssueTypeAttributeValueMappingRepository _repository;

        public UpdateJiraIssueTypeAttributeValueMappingHandler(IJiraIssueTypeAttributeValueMappingRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateJiraIssueTypeAttributeValueMapping command)
        {
            JiraIssueTypeAttributeValueMapping jiraIssueTypeAttributeValueMapping = new JiraIssueTypeAttributeValueMapping(command.Id, command.JiraIssueTypeAttributeMappingId, command.SenseaiValue, command.JiraValue);
            _repository.Update(jiraIssueTypeAttributeValueMapping);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateJiraIssueTypeAttributeValueMapping command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty");
                return false;
            }
            if (command.JiraIssueTypeAttributeMappingId == Guid.Empty)
            {
                Errors.Add("JiraIssueTypeAttributeMappingId cannot be empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.SenseaiValue))
            {
                Errors.Add("SenseaiValue cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraValue))
            {
                Errors.Add("JiraValue cannot be empty.");
                return false;
            }
            return true;
        }
    }
}