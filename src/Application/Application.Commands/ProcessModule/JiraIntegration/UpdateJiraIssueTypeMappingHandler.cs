﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraIssueTypeMappingHandler : ICommandHandler<UpdateJiraIssueTypeMapping, bool>
    {
        private readonly IJiraIssueTypeMappingRepository _repository;

        public UpdateJiraIssueTypeMappingHandler(IJiraIssueTypeMappingRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateJiraIssueTypeMapping command)
        {
            JiraIssueTypeMapping jiraIssueTypeMapping = new JiraIssueTypeMapping(command.Id, command.JiraProjectMappingId, command.SenseaiIssueTypeField, command.JiraIssueTypeFieldId, command.JiraIssueTypeFieldName, command.AllowAttachments);
            _repository.Update(jiraIssueTypeMapping);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateJiraIssueTypeMapping command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty");
                return false;
            }
            if (command.JiraProjectMappingId == Guid.Empty)
            {
                Errors.Add("JiraProjectMappingId cannot be empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.SenseaiIssueTypeField))
            {
                Errors.Add("SenseaiIssueTypeField cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraIssueTypeFieldId))
            {
                Errors.Add("JiraIssueTypeField cannot be empty.");
                return false;
            }

            return true;
        }
    }
}