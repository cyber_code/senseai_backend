﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Jira;
using SenseAI.Domain;
using SenseAI.Domain.AdminModel;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraProjectMappingHandler : ICommandHandler<UpdateJiraProjectMapping, bool>
    {
        private readonly IJiraProjectMappingRepository _repository;
        private readonly IDbContext _dbContext;
        private readonly IJiraIssueTypeAttributeMappingRepository _attributeRepository;
        private readonly IJiraIssueTypeAttributeValueMappingRepository _attributeValueRepository;
        private readonly IJiraIssueTypeMappingRepository _jiraIssueTypeMappingRepository;
        private readonly IJiraUserMappingRepository _jiraUserMappingRepository;
        private readonly ISubProjectRepository _subProjectRepository;

        public UpdateJiraProjectMappingHandler(IJiraProjectMappingRepository repository, IDbContext dbContext, IJiraIssueTypeAttributeMappingRepository attributeRepository, IJiraIssueTypeAttributeValueMappingRepository attributeValueRepository, IJiraIssueTypeMappingRepository jiraIssueTypeMappingRepository, IJiraUserMappingRepository jiraUserMappingRepository, ISubProjectRepository subProjectRepository)
        {
            _repository = repository;
            _dbContext = dbContext;
            _attributeRepository = attributeRepository;
            _attributeValueRepository = attributeValueRepository;
            _jiraIssueTypeMappingRepository = jiraIssueTypeMappingRepository;
            _jiraUserMappingRepository = jiraUserMappingRepository;
            _subProjectRepository = subProjectRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateJiraProjectMapping command)
        {
            bool tmpResult = false;
            var beforeUpdate = _repository.GetJiraProjectMappingById(command.Id);

            JiraProjectMapping jiraProjectMapping =
                new JiraProjectMapping(
                    command.Id,
                    command.ProjectId,
                    command.SubprojectId,
                    command.JiraProjectKey,
                    command.JiraProjectName,
                    command.MapHierarchy,
                    command.HierarchyParentId,
                    command.HierarchyTypeId
                    );

            _repository.Update(jiraProjectMapping);

            if (beforeUpdate.JiraProjectKey != command.JiraProjectKey)
            {
                var jiraUserMappingToBeDeleted = _dbContext.Set<JiraUserMappingData>().AsNoTracking()
                    .Where(j => j.SubprojectId == command.SubprojectId).ToList();
                foreach (var jirauser in jiraUserMappingToBeDeleted)
                {
                    _jiraUserMappingRepository.Delete(jirauser.Id);
                }
                var issueTypeMappingIdToDeleteList = _dbContext.Set<JiraIssueTypeMappingData>().AsNoTracking()
                            .Where(m => m.JiraProjectMappingId == command.Id)
                            .Select(m => m.Id).ToList();
                foreach (var issueTypeMappingIdToDelete in issueTypeMappingIdToDeleteList)
                {
                    var issueTypeMappingAttributeToDelete = _dbContext.Set<JiraIssueTypeAttributeMappingData>().AsNoTracking()
                                .Where(ma => ma.JiraIssueTypeMappingId == issueTypeMappingIdToDelete).ToList();
                    if (issueTypeMappingAttributeToDelete.Any())
                    {
                        foreach (var item in issueTypeMappingAttributeToDelete)
                        {
                            var issueTypeMappingAttributeValueToDelete = _dbContext.Set<JiraIssueTypeAttributeValueMappingData>().AsNoTracking()
                                .Where(d => d.JiraIssueTypeAttributeMappingId == item.Id).ToList();
                            if (issueTypeMappingAttributeValueToDelete.Any())
                            {
                                foreach (var itemValue in issueTypeMappingAttributeValueToDelete)
                                {
                                    _attributeValueRepository.Delete(itemValue.Id);
                                }
                            }
                            _attributeRepository.Delete(item.Id);
                        }
                    }
                    _jiraIssueTypeMappingRepository.Delete(issueTypeMappingIdToDelete);
                }

                //update subproject isJiraEnabled
                var query = (from project in _dbContext.Set<SubProjectData>().AsNoTracking()
                             where project.Id == command.SubprojectId
                             select project).FirstOrDefault();
                SubProject subProject = new SubProject(query.Id, query.ProjectId, query.Title, query.Description, false, query.JiraLink);
                _subProjectRepository.Update(subProject);
            }

            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(UpdateJiraProjectMapping command)
        {
            if (command.SubprojectId == Guid.Empty)
            {
                Errors.Add("Subproject cannot be empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraProjectKey))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraProjectName))
            {
                Errors.Add("Json cannot be empty.");
                return false;
            }

            return true;
        }
    }
}