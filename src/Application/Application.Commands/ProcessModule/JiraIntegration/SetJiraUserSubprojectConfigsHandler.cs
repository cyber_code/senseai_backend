﻿using Commands.ProcessModule.JiraIntegration;
using Jira;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.AdminModel;
using Persistence.Internal;
using Persistence.Jira;
using SenseAI.Domain;
using SenseAI.Domain.AdminModel;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class SetJiraUserSubprojectConfigsHandler : ICommandHandler<SetJiraUserSubprojectConfigs, bool>
    {
        private readonly IJiraUserSubprojectRepository _repository;
        private readonly ISubProjectRepository _subProjectRepository;
        private readonly IDbContext _dbContext;
        private readonly IJiraClient _jiraClient;

        public SetJiraUserSubprojectConfigsHandler(IJiraUserSubprojectRepository repository, ISubProjectRepository subProjectRepository, IDbContext dbContext, IJiraClient jiraClient)
        {
            _repository = repository;
            _subProjectRepository = subProjectRepository;
            _dbContext = dbContext;
            _jiraClient = jiraClient;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(SetJiraUserSubprojectConfigs command)
        {
            var jiraUrl = command.JiraUrl;

            var query = (from project in _dbContext.Set<SubProjectData>().AsNoTracking()
                         where project.Id == command.SubprojectId
                         select project).FirstOrDefault();

            if (string.IsNullOrWhiteSpace(jiraUrl))
            {
                jiraUrl = query.JiraLink;
            }

            var getUserJiraSubproject = (from userjiraSubproject in _dbContext.Set<JiraUserSubprojectConfigsData>().AsNoTracking()
                                         where userjiraSubproject.SubprojectId == command.SubprojectId
                                         && userjiraSubproject.UserId == command.UserId
                                         select userjiraSubproject).FirstOrDefault();

            if (getUserJiraSubproject == null)
            {
                if (_jiraClient.PingJira(command.JiraUsername, command.JiraToken, jiraUrl))
                {
                    JiraUserSubprojectConfigs jiraUserSubprojectConfigs = new JiraUserSubprojectConfigs(command.UserId, command.SubprojectId, command.JiraUsername, command.JiraToken);
                    _repository.Add(jiraUserSubprojectConfigs);
                    if (jiraUserSubprojectConfigs.Id == null)
                        return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Failed, "Jira user subproject configuration failed to save!"));
                }
                else
                {
                    return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Failed, "Jira credentials are not valid!"));
                }
            }
            else
            {
                if (_jiraClient.PingJira(command.JiraUsername, command.JiraToken, jiraUrl))
                {
                    JiraUserSubprojectConfigs jiraUserSubprojectConfigs = new JiraUserSubprojectConfigs(getUserJiraSubproject.Id, getUserJiraSubproject.UserId, getUserJiraSubproject.SubprojectId, command.JiraUsername, command.JiraToken);
                    _repository.Update(jiraUserSubprojectConfigs);
                    if (jiraUserSubprojectConfigs.Id == null)
                        return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Failed, "Jira user subproject configuration failed to update!"));
                }
                else
                {
                    return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Failed, "Jira credentials are not valid!"));
                }
            }

            if (!string.IsNullOrWhiteSpace(command.JiraUrl))
            {
                SubProject subProject = new SubProject(command.SubprojectId, query.ProjectId, query.Title, query.Description, query.IsJiraIntegrationEnabled, command.JiraUrl);
                _subProjectRepository.Update(subProject);
            }

            return new CommandResponse<bool>(true, new CommandResponseMessage((int)MessageType.Successful, ""));
        }

        public bool Validate(SetJiraUserSubprojectConfigs command)
        {
            if (command.UserId == string.Empty)
            {
                Errors.Add("User cannot be empty");
                return false;
            }
            if (command.SubprojectId == Guid.Empty)
            {
                Errors.Add("Subproject cannot be empty");
                return false;
            }

            return true;
        }
    }
}