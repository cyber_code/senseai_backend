﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraUserMappingHandler : ICommandHandler<AddJiraUserMapping, AddJiraUserMappingResult>
    {
        private readonly IJiraUserMappingRepository _repository;

        public AddJiraUserMappingHandler(IJiraUserMappingRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddJiraUserMappingResult> Handle(AddJiraUserMapping command)
        {
            JiraUserMapping jiraUserMapping = new JiraUserMapping(command.SubprojectId, command.JiraUserId, command.SenseaiUserId);
            _repository.Add(jiraUserMapping);

            return new CommandResponse<AddJiraUserMappingResult>(new AddJiraUserMappingResult(jiraUserMapping.Id));
        }

        public bool Validate(AddJiraUserMapping command)
        {
            if (command.SubprojectId == Guid.Empty)
            {
                Errors.Add("Subproject cannot be empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraUserId))
            {
                Errors.Add("Jira userid cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.SenseaiUserId))
            {
                Errors.Add("Senseai userid cannot be empty.");
                return false;
            }

            return true;
        }
    }
}