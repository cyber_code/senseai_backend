﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraIssueTypeAttributeMappingHandler : ICommandHandler<AddJiraIssueTypeAttributeMapping, AddJiraIssueTypeAttributeMappingResult>
    {
        private readonly IJiraIssueTypeAttributeMappingRepository _repository;

        public AddJiraIssueTypeAttributeMappingHandler(IJiraIssueTypeAttributeMappingRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddJiraIssueTypeAttributeMappingResult> Handle(AddJiraIssueTypeAttributeMapping command)
        {
            JiraIssueTypeAttributeMapping jiraIssueTypeAttributeMapping = new JiraIssueTypeAttributeMapping(command.JiraIssueTypeMappingId, command.SenseaiField, command.JiraField, command.IsMandatory, command.Index);
            _repository.Add(jiraIssueTypeAttributeMapping);
            return new CommandResponse<AddJiraIssueTypeAttributeMappingResult>(new AddJiraIssueTypeAttributeMappingResult(jiraIssueTypeAttributeMapping.Id));
        }

        public bool Validate(AddJiraIssueTypeAttributeMapping command)
        {
            if (command.JiraIssueTypeMappingId == Guid.Empty)
            {
                Errors.Add("JiraIssueTypeMappingId cannot be empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.SenseaiField))
            {
                Errors.Add("SenseaiField cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraField))
            {
                Errors.Add("JiraField cannot be empty.");
                return false;
            }
            return true;
        }
    }
}