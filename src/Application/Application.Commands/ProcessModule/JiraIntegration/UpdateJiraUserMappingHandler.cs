﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraUserMappingHandler : ICommandHandler<UpdateJiraUserMapping, bool>
    {
        private readonly IJiraUserMappingRepository _repository;

        public UpdateJiraUserMappingHandler(IJiraUserMappingRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateJiraUserMapping command)
        {
            bool tmpResult = false;
            JiraUserMapping jiraUserMapping =
                new JiraUserMapping(command.Id, command.SubprojectId, command.JiraUserId, command.SenseaiUserId);
            _repository.Update(jiraUserMapping);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(UpdateJiraUserMapping command)
        {
            if (command.SubprojectId == Guid.Empty)
            {
                Errors.Add("Subproject cannot be empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraUserId))
            {
                Errors.Add("Jira userId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.SenseaiUserId))
            {
                Errors.Add("Sense.ai userId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}