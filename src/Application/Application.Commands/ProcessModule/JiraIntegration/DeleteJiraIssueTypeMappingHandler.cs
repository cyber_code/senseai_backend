﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Jira;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SenseAI.Domain;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class DeleteJiraIssueTypeMappingHandler : ICommandHandler<DeleteJiraIssueTypeMapping, bool>
    {
        private readonly IDbContext _dbContext;
        private readonly IJiraIssueTypeMappingRepository _repository;
        private readonly IJiraIssueTypeAttributeMappingRepository _attributeRepository;
        private readonly IJiraIssueTypeAttributeValueMappingRepository _attributeValueRepository;

        public DeleteJiraIssueTypeMappingHandler(IDbContext dbContext, IJiraIssueTypeMappingRepository repository, IJiraIssueTypeAttributeMappingRepository attributeRepository, IJiraIssueTypeAttributeValueMappingRepository attributeValueRepository)
        {
            _dbContext = dbContext;
            _repository = repository;
            _attributeRepository = attributeRepository;
            _attributeValueRepository = attributeValueRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteJiraIssueTypeMapping command)
        {
            var issueTypeMappingIdToDelete = _dbContext.Set<JiraIssueTypeMappingData>().AsNoTracking()
                            .Where(m => m.Id == command.Id)
                            .Select(m => m.Id).First();
            if (issueTypeMappingIdToDelete != null)
            {
                var issueTypeMappingAttributeToDelete = _dbContext.Set<JiraIssueTypeAttributeMappingData>().AsNoTracking()
                            .Where(ma => ma.JiraIssueTypeMappingId == issueTypeMappingIdToDelete).ToList();
                if (issueTypeMappingAttributeToDelete.Any())
                {
                    foreach (var item in issueTypeMappingAttributeToDelete)
                    {
                        var issueTypeMappingAttributeValueToDelete = _dbContext.Set<JiraIssueTypeAttributeValueMappingData>().AsNoTracking()
                            .Where(d => d.JiraIssueTypeAttributeMappingId == item.Id).ToList();
                        if (issueTypeMappingAttributeValueToDelete.Any())
                        {
                            foreach (var itemValue in issueTypeMappingAttributeValueToDelete)
                            {
                                _attributeValueRepository.Delete(itemValue.Id);
                            }
                        }
                        _attributeRepository.Delete(item.Id);
                    }
                }

                _repository.Delete(issueTypeMappingIdToDelete);
                return new CommandResponse<bool>(true, new CommandResponseMessage((int)MessageType.Successful, "Issue type has been deleted successfully"));
            }
            else
            {
                return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Failed, "Issue type doesn't exist"));
            }
        }

        public bool Validate(DeleteJiraIssueTypeMapping command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty");
                return false;
            }
            return true;
        }
    }
}