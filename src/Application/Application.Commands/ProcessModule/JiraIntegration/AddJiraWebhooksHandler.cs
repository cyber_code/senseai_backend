﻿using Commands.ProcessModule.JiraIntegration;
using Jira;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraWebhooksHandler : ICommandHandler<AddJiraWebhooks, bool>
    {
        private readonly IJiraClient _jiraClient;
        private readonly IJiraProjectMappingRepository _jiraProjectMappingRepository;

        public AddJiraWebhooksHandler(IJiraClient jiraClient, IJiraProjectMappingRepository jiraProjectMappingRepository)
        {
            _jiraClient = jiraClient;
            _jiraProjectMappingRepository = jiraProjectMappingRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(AddJiraWebhooks command)
        {
            var projectMapping = _jiraProjectMappingRepository.GetJiraProjectMappingBySubprojectId(command.SubProjectId);
            _jiraClient.AddWebhook(new Jira.Webhook.AddJiraWebhook
            {
                Name = command.Name,
                SubprojectId = command.SubProjectId,
                Filters = new Jira.Webhook.Filters { IssueRelatedEventsSection = $"project = \"{projectMapping.JiraProjectName }\"" },
                Events = command.Events,
                Url = command.Url,
                JiraProjectKey = projectMapping.JiraProjectKey,
                JiraProjectName = projectMapping.JiraProjectName,
                Enabled = false
            });
            return new CommandResponse<bool>(true);
        }

        public bool Validate(AddJiraWebhooks command)
        {
            return true;
        }
    }
}