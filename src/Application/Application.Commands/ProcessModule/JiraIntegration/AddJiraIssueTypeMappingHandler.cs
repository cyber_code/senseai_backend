﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.Jira;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraIssueTypeMappingHandler : ICommandHandler<AddJiraIssueTypeMapping, AddJiraIssueTypeMappingResult>
    {
        private readonly IDbContext _dbContext;
        private readonly IJiraIssueTypeMappingRepository _repository;
        private readonly IJiraIssueTypeAttributeMappingRepository _attributeRepository;
        private readonly IJiraIssueTypeAttributeValueMappingRepository _attributeValueRepository;

        public AddJiraIssueTypeMappingHandler(IDbContext dbContext, IJiraIssueTypeMappingRepository repository, IJiraIssueTypeAttributeMappingRepository attributeRepository, IJiraIssueTypeAttributeValueMappingRepository attributeValueRepository)
        {
            _dbContext = dbContext;
            _repository = repository;
            _attributeRepository = attributeRepository;
            _attributeValueRepository = attributeValueRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddJiraIssueTypeMappingResult> Handle(AddJiraIssueTypeMapping command)
        {
            if (command.isUpdate)
            {
                var issueTypeMappingIdToDelete = _dbContext.Set<JiraIssueTypeMappingData>().AsNoTracking()
                            .Where(m => m.Id == command.Id)
                            .Select(m => m.Id).First();
                if (issueTypeMappingIdToDelete != null)
                {
                    var issueTypeMappingAttributeToDelete = _dbContext.Set<JiraIssueTypeAttributeMappingData>().AsNoTracking()
                                .Where(ma => ma.JiraIssueTypeMappingId == issueTypeMappingIdToDelete).ToList();
                    if (issueTypeMappingAttributeToDelete.Any())
                    {
                        foreach (var item in issueTypeMappingAttributeToDelete)
                        {
                            var issueTypeMappingAttributeValueToDelete = _dbContext.Set<JiraIssueTypeAttributeValueMappingData>().AsNoTracking()
                                .Where(d => d.JiraIssueTypeAttributeMappingId == item.Id).ToList();
                            if (issueTypeMappingAttributeValueToDelete.Any())
                            {
                                foreach (var itemValue in issueTypeMappingAttributeValueToDelete)
                                {
                                    _attributeValueRepository.Delete(itemValue.Id);
                                }
                            }
                            _attributeRepository.Delete(item.Id);
                        }
                    }

                    _repository.Delete(issueTypeMappingIdToDelete);
                }
            }

            JiraIssueTypeMapping jiraIssueTypeMapping = new JiraIssueTypeMapping(command.JiraProjectMappingId, command.SenseaiIssueTypeField, command.JiraIssueTypeFieldId, command.JiraIssueTypeFieldName, command.AllowAttachments);
            _repository.Add(jiraIssueTypeMapping);
            foreach (var issueTypeAttribute in command.AttributeMapping)
            {
                JiraIssueTypeAttributeMapping jiraIssueTypeAttributeMapping = new JiraIssueTypeAttributeMapping(
                    jiraIssueTypeMapping.Id,
                    issueTypeAttribute.SenseaiValue,
                    issueTypeAttribute.JiraValue,
                    issueTypeAttribute.IsMandatory,
                    issueTypeAttribute.Index
                );
                _attributeRepository.Add(jiraIssueTypeAttributeMapping);
                foreach (var issueTypeAttributeValue in issueTypeAttribute.AttributeValuesMapping)
                {
                    JiraIssueTypeAttributeValueMapping jiraIssueTypeAttributeValueMapping
                    = new JiraIssueTypeAttributeValueMapping(
                        jiraIssueTypeAttributeMapping.Id,
                        issueTypeAttributeValue.SenseaiValue,
                        issueTypeAttributeValue.JiraValue
                    );
                    _attributeValueRepository.Add(jiraIssueTypeAttributeValueMapping);
                }
            }

            return new CommandResponse<AddJiraIssueTypeMappingResult>(new AddJiraIssueTypeMappingResult(jiraIssueTypeMapping.Id));
        }

        public bool JiraProjectMappingExist(bool isUpdate, Guid id, Guid projectMappingId, string senseaiIssueTypeField, string jiraIssueTypeField)
        {
            List<JiraIssueTypeMappingData> query = null;
            if (isUpdate)
            {
                query = (from jiraIssueTypeMapping in _dbContext.Set<JiraIssueTypeMappingData>().AsNoTracking()
                         where jiraIssueTypeMapping.JiraProjectMappingId == projectMappingId
                         && jiraIssueTypeMapping.SenseaiIssueTypeField == senseaiIssueTypeField
                         && jiraIssueTypeMapping.JiraIssueTypeFieldId == jiraIssueTypeField
                         && jiraIssueTypeMapping.Id != id
                         select jiraIssueTypeMapping).ToList();
            }
            else
            {
                query = (from jiraIssueTypeMapping in _dbContext.Set<JiraIssueTypeMappingData>().AsNoTracking()
                         where jiraIssueTypeMapping.JiraProjectMappingId == projectMappingId
                         && jiraIssueTypeMapping.SenseaiIssueTypeField == senseaiIssueTypeField
                         && jiraIssueTypeMapping.JiraIssueTypeFieldId == jiraIssueTypeField
                         select jiraIssueTypeMapping).ToList();
            }
            if (query.Count > 0)
            {
                return false;
            }
            return true;
        }

        public bool Validate(AddJiraIssueTypeMapping command)
        {
            if (command.JiraProjectMappingId == Guid.Empty)
            {
                Errors.Add("JiraProjectMappingId cannot be empty");
                return false;
            }
            if (command.isUpdate)
            {
                //update
                if (command.Id == Guid.Empty)
                {
                    Errors.Add("This row has not been found");
                    return false;
                }
            }

            if (!JiraProjectMappingExist(command.isUpdate, command.Id, command.JiraProjectMappingId, command.SenseaiIssueTypeField, command.JiraIssueTypeFieldId))
            {
                Errors.Add("Mapping for this IssueType exists!");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.SenseaiIssueTypeField))
            {
                Errors.Add("SenseaiIssueTypeField cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraIssueTypeFieldId))
            {
                Errors.Add("JiraIssueTypeField cannot be empty.");
                return false;
            }

            return true;
        }
    }
}