﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraIssueTypeAttributeMappingHandler : ICommandHandler<UpdateJiraIssueTypeAttributeMapping, bool>
    {
        private readonly IJiraIssueTypeAttributeMappingRepository _repository;

        public UpdateJiraIssueTypeAttributeMappingHandler(IJiraIssueTypeAttributeMappingRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateJiraIssueTypeAttributeMapping command)
        {
            JiraIssueTypeAttributeMapping jiraIssueTypeAttributeMapping = new JiraIssueTypeAttributeMapping(command.Id, command.JiraIssueTypeMappingId, command.SenseaiField, command.JiraField, command.Required, command.Index);
            _repository.Update(jiraIssueTypeAttributeMapping);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateJiraIssueTypeAttributeMapping command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty");
                return false;
            }
            if (command.JiraIssueTypeMappingId == Guid.Empty)
            {
                Errors.Add("JiraIssueTypeMappingId cannot be empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.SenseaiField))
            {
                Errors.Add("SenseaiField cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraField))
            {
                Errors.Add("JiraField cannot be empty.");
                return false;
            }
            return true;
        }
    }
}