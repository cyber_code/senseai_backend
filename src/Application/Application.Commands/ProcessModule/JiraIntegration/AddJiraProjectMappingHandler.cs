﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class AddJiraProjectMappingHandler : ICommandHandler<AddJiraProjectMapping, AddJiraProjectMappingResult>
    {
        private readonly IJiraProjectMappingRepository _repository;

        public AddJiraProjectMappingHandler(IJiraProjectMappingRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddJiraProjectMappingResult> Handle(AddJiraProjectMapping command)
        {
            JiraProjectMapping jiraProjectMapping = new JiraProjectMapping(
                command.ProjectId,
                command.SubprojectId,
                command.JiraProjectKey,
                command.JiraProjectName,
                command.MapHierarchy,
                command.HierarchyParentId,
                command.HierarchyTypeId
            );
            _repository.Add(jiraProjectMapping);

            return new CommandResponse<AddJiraProjectMappingResult>(new AddJiraProjectMappingResult(jiraProjectMapping.Id));
        }

        public bool Validate(AddJiraProjectMapping command)
        {
            if (command.SubprojectId == Guid.Empty)
            {
                Errors.Add("Subproject cannot be empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraProjectKey))
            {
                Errors.Add("Project key cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.JiraProjectName))
            {
                Errors.Add("ProjectName cannot be empty.");
                return false;
            }

            return true;
        }
    }
}