﻿using Commands.ProcessModule.JiraIntegration;
using Messaging.Commands;
using SenseAI.Domain.JiraModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.ProcessModule.JiraIntegration
{
    public sealed class UpdateJiraUserSubprojectConfigsHandler : ICommandHandler<UpdateJiraUserSubprojectConfigs, bool>
    {
        private readonly IJiraUserSubprojectRepository _repository;

        public UpdateJiraUserSubprojectConfigsHandler(IJiraUserSubprojectRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateJiraUserSubprojectConfigs command)
        {
            JiraUserSubprojectConfigs jiraUserSubprojectConfigs = new JiraUserSubprojectConfigs(command.Id, command.UserId, command.SubprojectId, command.JiraUsername, command.JiraToken);
            _repository.Update(jiraUserSubprojectConfigs);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateJiraUserSubprojectConfigs command)
        {
            if (command.SubprojectId == Guid.Empty)
            {
                Errors.Add("Subproject cannot be empty");
                return false;
            }
            return true;
        }
    }
}