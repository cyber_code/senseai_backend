﻿using Commands.AAProductBuilderModule;
using SenseAI.Domain.AAProductionBuilderModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.AAProductBuilderModule
{
    public class ProductCompletedHandler : ICommandHandler<ProductCompleted, bool>
    {
        private readonly IProductGroupPropertyStateRepository _productGroupPropertyStateRepository;

        public ProductCompletedHandler(IProductGroupPropertyStateRepository productGroupPropertyStateRepository)
        {
            _productGroupPropertyStateRepository = productGroupPropertyStateRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(ProductCompleted command)
        {
            _productGroupPropertyStateRepository.UpdateAllProductGroupStatus(command.SessionId, command.ProductGroupId);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(ProductCompleted command)
        {
            if (command.SessionId == Guid.Empty)
            {
                Errors.Add("SessionId cannot be empty.");
                return false;
            }
            if (command.ProductGroupId == Guid.Empty)
            {
                Errors.Add("ProductGroupId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}