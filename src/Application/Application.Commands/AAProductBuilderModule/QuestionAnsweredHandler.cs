﻿using Commands.AAProductBuilderModule;
using Core.Abstractions;
using Messaging.Commands;
using SenseAI.Domain.AAProductionBuilderModel;
using SignalR;
using System;
using System.Collections.Generic;

namespace Application.Commands.AAProductBuilderModule
{
    public class QuestionAnsweredHandler : ICommandHandler<QuestionAnswered, bool>
    {
        private const string NA = "N/A";

        private readonly IProductGroupPropertyFieldStateRepository _productGroupPropertyFieldStateRepository;
        private readonly ISignalRClient _signalRClient;
        private readonly IWorkContext _workContext;

        public QuestionAnsweredHandler(IProductGroupPropertyFieldStateRepository productGroupPropertyFieldStateRepository,
                                       ISignalRClient signalRClient,
                                       IWorkContext workContext)
        {
            _productGroupPropertyFieldStateRepository = productGroupPropertyFieldStateRepository;
            _signalRClient = signalRClient;
            _workContext = workContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(QuestionAnswered command)
        {
            var productGroupPropertyFieldState = new ProductGroupPropertyFieldState
            {
                SessionId = command.SessionId,
                ProductGroupId = command.ProductGroupId,
                PropertyId = command.PropertyId.ToUpper(),
                FieldId = command.FieldId.ToUpper(),
                MultiValueIndex = command.MultiValueIndex,
                SubValueIndex = command.SubValueIndex,
                Value = command.Value,
                CurrencyId = string.IsNullOrWhiteSpace(command.CurrencyId) ? NA : command.CurrencyId.ToUpper(),
            };

            _productGroupPropertyFieldStateRepository.Add(productGroupPropertyFieldState);
            _signalRClient.SendAsync(new SignalRCommand
            {
                Method = "AddFieldValue",
                Group = _workContext.FullName,
                Value = productGroupPropertyFieldState
            }).Wait();

            return new CommandResponse<bool>(true);
        }

        public bool Validate(QuestionAnswered command)
        {
            if (command.SessionId == Guid.Empty)
            {
                Errors.Add("SessionId cannot be empty.");
                return false;
            }
            if (command.ProductGroupId == Guid.Empty)
            {
                Errors.Add("ProductGroupId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.PropertyId))
            {
                Errors.Add("PropertyId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.FieldId))
            {
                Errors.Add("FieldId cannot be empty.");
                return false;
            }

            if (command.MultiValueIndex == 0)
            {
                Errors.Add("MultiValueIndex cannot be zero.");
                return false;
            }

            if (command.SubValueIndex == 0)
            {
                Errors.Add("SubValueIndex cannot be zero.");
                return false;
            }

            return true;
        }
    }
}