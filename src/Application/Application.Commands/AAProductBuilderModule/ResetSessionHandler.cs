﻿using Commands.AAProductBuilderModule;
using SenseAI.Domain.AAProductionBuilderModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.AAProductBuilderModule
{
    public sealed class ResetSessionHandler : ICommandHandler<ResetSession, bool>
    {
        private readonly IProductGroupPropertyStateRepository _productGroupPropertyStateRepository;
        private readonly IProductGroupPropertyFieldStateRepository _productGroupPropertyFieldStateRepository;

        public ResetSessionHandler(IProductGroupPropertyStateRepository productGroupPropertyStateRepository, IProductGroupPropertyFieldStateRepository productGroupPropertyFieldStateRepository)
        {
            _productGroupPropertyStateRepository = productGroupPropertyStateRepository;
            _productGroupPropertyFieldStateRepository = productGroupPropertyFieldStateRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(ResetSession command)
        {
            _productGroupPropertyFieldStateRepository.Delete(command.SessionId);
            _productGroupPropertyStateRepository.Delete(command.SessionId);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(ResetSession command)
        {
            if (command.SessionId == Guid.Empty)
            {
                Errors.Add("SessionId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}