﻿using Commands.AAProductBuilderModule;
using Core.Abstractions;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Persistence.AAProductBuilderModel;
using Persistence.Internal;
using SenseAI.Domain.AAProductionBuilderModel;
using SignalR;
using System;
using System.Collections.Generic;

using System.Linq;

namespace Application.Commands.AAProductBuilderModule
{
    public class HeaderQuestionAnsweredHandler : ICommandHandler<HeaderQuestionAnswered, bool>
    {
        private const string NEW_ARRANGEMENT = "NEWARRANGEMENT";
        private const string NA = "N/A";

        private readonly IProductGroupPropertyFieldStateRepository _productGroupPropertyFieldStateRepository;
        private readonly ISignalRClient _signalRClient;
        private readonly IDbContext _dbContext;
        private readonly IWorkContext _workContext;

        public HeaderQuestionAnsweredHandler(IDbContext dbContext,
                                             IProductGroupPropertyFieldStateRepository productGroupPropertyFieldStateRepository,
                                             ISignalRClient signalRClient,
                                             IWorkContext workContext)
        {
            _dbContext = dbContext;
            _productGroupPropertyFieldStateRepository = productGroupPropertyFieldStateRepository;
            _signalRClient = signalRClient;
            _workContext = workContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(HeaderQuestionAnswered command)
        {
            var fieldConfigs = (from fields in _dbContext.Set<FieldData>().AsNoTracking()
                                where fields.PropertyId == NEW_ARRANGEMENT
                                select new
                                {
                                    fields.Id,
                                    fields.FieldId,
                                    fields.Json
                                }).OrderBy(x => x.Id);

            var productGroupPropertyFieldStates = new List<ProductGroupPropertyFieldState>();

            foreach (var answer in command.Answers)
            {
                var productGroupPropertyFieldState = new ProductGroupPropertyFieldState
                {
                    SessionId = answer.SessionId,
                    ProductGroupId = answer.ProductGroupId,
                    PropertyId = NEW_ARRANGEMENT,
                    FieldId = answer.FieldId,
                    MultiValueIndex = answer.MultiValueIndex,
                    SubValueIndex = 1,
                    Value = answer.Value,
                    CurrencyId = NA,
                };

                productGroupPropertyFieldStates.Add(productGroupPropertyFieldState);
            }

            _productGroupPropertyFieldStateRepository.AddBulk(productGroupPropertyFieldStates);

            var attributes = new List<HeaderQuestionAnsweredNewArrangementFieldAttribute>();

            foreach (var fieldConfig in fieldConfigs)
            {
                var attribute = JsonConvert.DeserializeObject<HeaderQuestionAnsweredNewArrangementFieldAttribute>(fieldConfig.Json);
                var fieldValues = command.Answers.Where(x => x.FieldId == fieldConfig.FieldId).ToList();
                if (fieldValues.Any())
                {
                    foreach (var fieldValue in fieldValues)
                    {
                        var attr = (HeaderQuestionAnsweredNewArrangementFieldAttribute)attribute.Clone();
                        if (attr.IsMultiValue)
                        {
                            attr.Description = AddIndexes(attr.Description, fieldValue.MultiValueIndex, 1);
                        }
                        attr.Value = fieldValue.Value;

                        attributes.Add(attr);
                    }
                }
                else
                {
                    if (attribute.IsMultiValue)
                    {
                        attribute.Description = AddIndexes(attribute.Description, 1, 1);
                    }

                    attributes.Add(attribute);
                }
            }

            _signalRClient.SendAsync(new SignalRCommand
            {
                Method = "HeaderQuestionsAnswered",
                Group = _workContext.FullName,
                Value = attributes
            });

            return new CommandResponse<bool>(true);
        }

        public bool Validate(HeaderQuestionAnswered command)
        {
            foreach (var answer in command.Answers)
            {
                if (answer.SessionId == Guid.Empty)
                {
                    Errors.Add("SessionId cannot be empty.");
                    return false;
                }

                if (answer.ProductGroupId == Guid.Empty)
                {
                    Errors.Add("ProductGroupId cannot be empty.");
                    return false;
                }

                if (string.IsNullOrWhiteSpace(answer.FieldId))
                {
                    Errors.Add("FieldId cannot be empty.");
                    return false;
                }
            }
            return true;
        }

        private string AddIndexes(string name, int multiValueIndex, int subValueIndex)
        {
            return name + " " + multiValueIndex + "." + subValueIndex;
        }
    }
}