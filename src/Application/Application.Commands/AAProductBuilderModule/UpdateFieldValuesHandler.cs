﻿using Commands.AAProductBuilderModule;
using Messaging.Commands;
using SenseAI.Domain.AAProductionBuilderModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.AAProductBuilderModule
{
    public class UpdateFieldValuesHandler : ICommandHandler<UpdateFieldValues, bool>
    {
        private const string NA = "N/A";

        private readonly IProductGroupPropertyFieldStateRepository _productGroupPropertyFieldStateRepository;

        public UpdateFieldValuesHandler(IProductGroupPropertyFieldStateRepository productGroupPropertyFieldStateRepository)
        {
            _productGroupPropertyFieldStateRepository = productGroupPropertyFieldStateRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateFieldValues command)
        {
            if (string.IsNullOrWhiteSpace(command.CurrencyId))
            {
                command.CurrencyId = NA;
            }

            _productGroupPropertyFieldStateRepository.Delete(command.SessionId, command.ProductGroupId, command.PropertyId, command.CurrencyId);

            var productGroupPropertyFieldStates = new List<ProductGroupPropertyFieldState>();

            foreach (var fieldValue in command.FieldValues)
            {
                var productGroupPropertyFieldState = new ProductGroupPropertyFieldState
                {
                    SessionId = command.SessionId,
                    ProductGroupId = command.ProductGroupId,
                    PropertyId = command.PropertyId,
                    FieldId = fieldValue.FieldId,
                    MultiValueIndex = fieldValue.MultiValueIndex,
                    SubValueIndex = fieldValue.SubValueIndex,
                    Value = fieldValue.Value,
                    CurrencyId = command.CurrencyId
                };

                productGroupPropertyFieldStates.Add(productGroupPropertyFieldState);
            }

            _productGroupPropertyFieldStateRepository.AddBulk(productGroupPropertyFieldStates);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateFieldValues command)
        {
            return true;
        }
    }
}