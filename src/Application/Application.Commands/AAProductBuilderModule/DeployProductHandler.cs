﻿using Commands.AAProductBuilderModule;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Persistence.AAProductBuilderModel;
using Persistence.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Application.Commands.AAProductBuilderModule
{
    public class DeployProductHandler : ICommandHandler<DeployProduct, bool>
    {
        private readonly IDbContext _dbContext;

        public DeployProductHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeployProduct command)
        {
            var deployedProductData = from productGroupPropertyFieldStateData in _dbContext.Set<ProductGroupPropertyFieldStateData>().AsNoTracking()
                                      join productGroup in _dbContext.Set<ProductGroupData>().AsNoTracking()
                                          on productGroupPropertyFieldStateData.ProductGroupId equals productGroup.Id
                                      join productGroupPropertyClassData in _dbContext.Set<ProductGroupPropertyClassData>().AsNoTracking()
                                          on productGroupPropertyFieldStateData.ProductGroupId equals productGroupPropertyClassData.ProductGroupId
                                      join propertyData in _dbContext.Set<PropertyData>().AsNoTracking()
                                          on productGroupPropertyClassData.PropertyClassId equals propertyData.PropertyClassId
                                      where productGroupPropertyFieldStateData.SessionId == command.SessionId
                                              && productGroupPropertyFieldStateData.ProductGroupId == command.ProductGroupId
                                              && productGroupPropertyFieldStateData.PropertyId == propertyData.Id
                                      select new
                                      {
                                          ProductGroup = productGroup.Title,
                                          PropertyClass = productGroupPropertyClassData.PropertyClassId,
                                          Property = productGroupPropertyFieldStateData.PropertyId,
                                          Field = productGroupPropertyFieldStateData.FieldId,
                                          productGroupPropertyFieldStateData.MultiValueIndex,
                                          productGroupPropertyFieldStateData.SubValueIndex,
                                          productGroupPropertyFieldStateData.Value,
                                          productGroupPropertyFieldStateData.CurrencyId
                                      };

            var json = JsonConvert.SerializeObject(deployedProductData.ToList());

            var consolePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"T24Deployment");
            //var consolePath = @"C:\repos\validata\validata-ai\src\T24Deployment\bin\Debug";
            var jsonPath = consolePath + @"\account.json";
            var t24path = consolePath + @"\T24Deployment.exe";

            File.WriteAllText(jsonPath, json);

            var proc = new System.Diagnostics.Process();

            proc.StartInfo.WorkingDirectory = consolePath;
            proc.StartInfo.Arguments = jsonPath;
            proc.StartInfo.FileName = t24path;
            proc.StartInfo.RedirectStandardOutput = true;

            proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
            proc.Start();
            proc.WaitForExit();

            var result = JsonConvert.DeserializeObject<Result>(proc.StandardOutput.ReadToEnd());

            if (!result.IsDeployed)
                throw new Exception(result.Error);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeployProduct command)
        {
            if (command.SessionId == Guid.Empty)
            {
                Errors.Add("SessionId cannot be empty.");
                return false;
            }
            if (command.ProductGroupId == Guid.Empty)
            {
                Errors.Add("ProductGroupId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}