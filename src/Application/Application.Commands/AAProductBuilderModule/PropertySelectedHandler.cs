﻿using Commands.AAProductBuilderModule;
using Core.Abstractions;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Persistence.AAProductBuilderModel;
using Persistence.Internal;
using SenseAI.Domain.AAProductionBuilderModel;
using SignalR;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.AAProductBuilderModule
{
    public class PropertySelectedHandler : ICommandHandler<PropertySelected, bool>
    {
        private const string NA = "N/A";

        private readonly IDbContext _dbContext;
        private readonly IProductGroupPropertyStateRepository _productGroupPropertyStateRepository;
        private readonly ISignalRClient _signalRClient;
        private readonly IWorkContext _workContext;

        public PropertySelectedHandler(IDbContext dbContext,
                                       IProductGroupPropertyStateRepository productGroupPropertyStateRepository,
                                       ISignalRClient signalRClient,
                                       IWorkContext workContext)
        {
            _dbContext = dbContext;
            _productGroupPropertyStateRepository = productGroupPropertyStateRepository;
            _signalRClient = signalRClient;
            _workContext = workContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(PropertySelected command)
        {
            var currencyId = string.IsNullOrWhiteSpace(command.CurrencyId) ? NA : command.CurrencyId;
            _productGroupPropertyStateRepository.Update(command.SessionId, command.ProductGroupId, command.PropertyId, currencyId);

            var fieldConfigs = (from productGroupPropertyClassData in _dbContext.Set<ProductGroupPropertyClassData>().AsNoTracking()
                                join fieldData in _dbContext.Set<FieldData>().AsNoTracking()
                                     on productGroupPropertyClassData.PropertyClassId equals fieldData.PropertyClassId
                                where productGroupPropertyClassData.ProductGroupId == command.ProductGroupId
                                      && fieldData.PropertyId == command.PropertyId
                                select fieldData)
                                .OrderBy(o => o.Id)
                                .ToList();

            var property = (from properties in _dbContext.Set<PropertyData>().AsNoTracking()
                            join productGroupPropertyClassData in _dbContext.Set<ProductGroupPropertyClassData>().AsNoTracking()
                                on properties.PropertyClassId equals productGroupPropertyClassData.PropertyClassId
                            where productGroupPropertyClassData.ProductGroupId == command.ProductGroupId
                                  && properties.Id == command.PropertyId
                            select properties).FirstOrDefault();

            var attributes = new List<PropertySelectedAttribute>();
            fieldConfigs.ForEach(x => attributes.Add(JsonConvert.DeserializeObject<PropertySelectedAttribute>(x.Json)));

            var propertyFieldConfigs = new PropertySelectedConfig
            {
                Property = new PropertySelectedProperty
                {
                    PropertyId = property.Id,
                    PropertyName = property.Description,
                    CurrencyId = command.CurrencyId
                },
                Attributes = attributes
            };

            _signalRClient.SendAsync(new SignalRCommand
            {
                Method = "ConfigFields",
                Group = _workContext.FullName,
                Value = propertyFieldConfigs
            });

            return new CommandResponse<bool>(true);
        }

        public bool Validate(PropertySelected command)
        {
            if (command.SessionId == Guid.Empty)
            {
                Errors.Add("SessionId cannot be empty.");
                return false;
            }
            if (command.ProductGroupId == Guid.Empty)
            {
                Errors.Add("ProductGroupId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.PropertyId))
            {
                Errors.Add("PropertyId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}