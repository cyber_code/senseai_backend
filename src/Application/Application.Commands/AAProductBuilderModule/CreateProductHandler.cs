﻿using Commands.AAProductBuilderModule;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.AAProductBuilderModel;
using Persistence.Internal;
using SenseAI.Domain.AAProductionBuilderModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.AAProductBuilderModule
{
    public class CreateProductHandler : ICommandHandler<CreateProduct, bool>
    {
        private const string NEW_ARRANGEMENT = "NEWARRANGEMENT";
        private const string NA = "N/A";

        private readonly IDbContext _dbContext;
        private readonly IProductGroupPropertyStateRepository _productStateRepository;

        public CreateProductHandler(IDbContext dbContext,
                                    IProductGroupPropertyStateRepository productStateRepository)
        {
            _dbContext = dbContext;
            _productStateRepository = productStateRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(CreateProduct command)
        {
            var productGroupPropertyStates = new List<ProductGroupPropertyState>();

            var productGroupProperties = from productGroupPropertyClasses in _dbContext.Set<ProductGroupPropertyClassData>().AsNoTracking()
                                         join properties in _dbContext.Set<PropertyData>().AsNoTracking()
                                             on productGroupPropertyClasses.PropertyClassId equals properties.PropertyClassId
                                         join propertyClassData in _dbContext.Set<PropertyClassData>().AsNoTracking()
                                             on productGroupPropertyClasses.PropertyClassId equals propertyClassData.Id
                                         where productGroupPropertyClasses.ProductGroupId == command.ProductGroupId
                                         select new
                                         {
                                             command.SessionId,
                                             command.ProductGroupId,
                                             PropertyId = properties.Id,
                                             propertyClassData.IsCurrencySpecific
                                         };

            foreach (var productGroupProperty in productGroupProperties)
            {
                if (productGroupProperty.IsCurrencySpecific)
                {
                    foreach (var currencyId in command.Currencies)
                    {
                        productGroupPropertyStates.Add(new ProductGroupPropertyState
                        {
                            SessionId = productGroupProperty.SessionId,
                            ProductGroupId = productGroupProperty.ProductGroupId,
                            PropertyId = productGroupProperty.PropertyId,
                            CurrencyId = currencyId,
                            IsSelected = false
                        });
                    }
                }
                else
                {
                    productGroupPropertyStates.Add(new ProductGroupPropertyState
                    {
                        SessionId = productGroupProperty.SessionId,
                        ProductGroupId = productGroupProperty.ProductGroupId,
                        PropertyId = productGroupProperty.PropertyId,
                        CurrencyId = NA,
                        IsSelected = false
                    });
                }
            }

            productGroupPropertyStates.Add(new ProductGroupPropertyState
            {
                SessionId = command.SessionId,
                ProductGroupId = command.ProductGroupId,
                PropertyId = NEW_ARRANGEMENT,
                CurrencyId = NA,
                IsSelected = true
            });

            _productStateRepository.AddBulk(productGroupPropertyStates);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(CreateProduct command)
        {
            if (command.SessionId == Guid.Empty)
            {
                Errors.Add("SessionId cannot be empty.");
                return false;
            }

            if (command.ProductGroupId == Guid.Empty)
            {
                Errors.Add("ProductGroupId cannot be empty.");
                return false;
            }

            if (!command.Currencies.Any())
            {
                Errors.Add("Currencies cannot be empty.");
                return false;
            }

            return true;
        }
    }
}