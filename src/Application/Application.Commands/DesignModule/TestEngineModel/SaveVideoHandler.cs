﻿using Commands.DesignModule.WorkflowModel;
using Core.Configuration;
using Messaging.Commands;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Application.Commands.DesignModule.TestEngineModel
{
    public sealed class SaveVideoHandler : ICommandHandler<SaveVideo, bool>
    {
        private readonly SenseAIConfig _config;

        public SaveVideoHandler(SenseAIConfig config)
        {
            _config = config;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(SaveVideo command)
        {
            Directory.CreateDirectory(_config.TestEngineSettings.VideoPath);
            File.WriteAllBytes($@"{_config.TestEngineSettings.VideoPath}{command.WorkflowId}.mp4", command.VideoFile);
            command.VideoFile = null;
            return new CommandResponse<bool>(true);
        }

        public bool Validate(SaveVideo command)
        {
            if (command.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            return true;
        }
    }


}
