﻿using Core.Configuration;
using Messaging.Commands;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Application.Commands.DesignModule.TestEngineModel
{
    public sealed class HasVideoHandler : ICommandHandler<HasVideoModel, HasVideoModelResult>
    {
        private readonly SenseAIConfig _config;

        public HasVideoHandler(SenseAIConfig config)
        {
            _config = config;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<HasVideoModelResult> Handle(HasVideoModel command)
        {
            bool hasVideo = File.Exists($@"{_config.TestEngineSettings.VideoPath}{command.WorkflowId}.mp4");
            HasVideoModelResult result = new HasVideoModelResult(hasVideo);

            return new CommandResponse<HasVideoModelResult>(result);
        }

        public bool Validate(HasVideoModel command)
        {
            if (command.WorkflowId.Equals(Guid.Empty))
            {
                Errors.Add("Workflow cannot be empty");
                return false;
            }

            return true;
        }
    }

    public sealed class HasVideoModel : ICommand<HasVideoModelResult>
    {
        public Guid WorkflowId { get; set; }
    }

    public sealed class HasVideoModelResult
    {
        public HasVideoModelResult(bool hasVideo)
        {
            HasVideo = hasVideo;
        }

        public bool HasVideo { get; private set; }
    }

}
