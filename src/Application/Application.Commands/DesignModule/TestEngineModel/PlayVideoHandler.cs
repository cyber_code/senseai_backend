﻿using Core.Configuration;
using Messaging.Commands;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Application.Commands.DesignModule.TestEngineModel
{
    public sealed class PlayVideoHandler : ICommandHandler<PlayVideoModel, PlayVideoModelResult>
    {
        private readonly SenseAIConfig _config;

        public PlayVideoHandler(SenseAIConfig config)
        {
            _config = config;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<PlayVideoModelResult> Handle(PlayVideoModel command)
        {
            PlayVideoModelResult result;
            MemoryStream videoStream = new MemoryStream();
            using (var stream = new FileStream($@"{_config.TestEngineSettings.VideoPath}{command.WorkflowId}.mp4", FileMode.Open, FileAccess.Read))
            {
                stream.CopyTo(videoStream);
            }
            
            videoStream.Seek(0, SeekOrigin.Begin);
           
            result = new PlayVideoModelResult(command.WorkflowId, videoStream);

            return new CommandResponse<PlayVideoModelResult>(result);
        }

        public bool Validate(PlayVideoModel command)
        {
            if (command.WorkflowId.Equals(Guid.Empty))
            {
                Errors.Add("Workflow cannot be empty");
                return false;
            }

            return true;
        }
    }

    public sealed class PlayVideoModel : ICommand<PlayVideoModelResult>
    {
        public Guid WorkflowId { get; set; }
    }

    public sealed class PlayVideoModelResult
    {
        public PlayVideoModelResult(Guid workflowId, Stream videoFile)
        {
            WorkflowId = workflowId;
            VideoFile = videoFile;
        }

        public Guid WorkflowId { get; private set; }
        public Stream VideoFile { get; private set; }
    }

}
