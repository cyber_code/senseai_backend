﻿using Commands.DesignModule.Suggestions;
using Messaging.Commands;
using SenseAI.Domain.ValidationModel;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.DesignModule.Suggestions
{
    public sealed class SaveComputerGeneratedWorkflowsHandler : ICommandHandler<SaveComputerGeneratedWorkflows, SaveComputerGeneratedWorkflowsResult>
    {
        private readonly IWorkflowRepository _workflowRepository;

        public SaveComputerGeneratedWorkflowsHandler(IWorkflowRepository workflowRepository)
        {
            _workflowRepository = workflowRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<SaveComputerGeneratedWorkflowsResult> Handle(SaveComputerGeneratedWorkflows command)
        {
            RequestValidation requestValidation = null;
            List<Guid> WorkflowIds = new List<Guid>();
            int number = 1;
            foreach (var workflows in command.Workflows)
            {
                var newWorkflow = new Workflow($"{workflows.Title} {number++}", workflows.Description, workflows.FolderId);
                _workflowRepository.Add(newWorkflow);
                var saveWorkflow = WorkflowFactory.Create(newWorkflow.Id, workflows.Json);
                if (saveWorkflow.Validate(out requestValidation))
                {
                    _workflowRepository.Save(saveWorkflow, workflows.Json, workflows.WorkflowPathsJson);
                    WorkflowIds.Add(newWorkflow.Id);
                }
            }
            return new CommandResponse<SaveComputerGeneratedWorkflowsResult>(new SaveComputerGeneratedWorkflowsResult(WorkflowIds), new CommandResponseMessage((int)requestValidation.MessageType, requestValidation.Message));
        }

        public bool Validate(SaveComputerGeneratedWorkflows command)
        {
            foreach (var workflow in command.Workflows)
            {
                if (workflow.FolderId == Guid.Empty)
                {
                    Errors.Add("Folder cannot be empty.");
                    return false;
                }
                if (string.IsNullOrEmpty(workflow.Title))
                {
                    Errors.Add("Title cannot be empty.");
                    return false;
                }
                if (string.IsNullOrEmpty(workflow.Json))
                {
                    Errors.Add("DesignerJson cannot be empty.");
                    return false;
                }
                if (string.IsNullOrEmpty(workflow.WorkflowPathsJson))
                {
                    Errors.Add("Path json cannot be empty.");
                    return false;
                }
            }
            return true;
        }
    }
}