﻿using Commands.DesignModule.Suggestions;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using System.Collections.Generic;

namespace Application.Commands.DesignModule.Suggestions
{
    public sealed class SaveDynamicDataSuggestionHandler : ICommandHandler<SaveDynamicDataSuggestions, SaveDynamicDataSuggestionsResult>
    {
        private readonly IDynamicDataSuggestionRepository _dynamicDataSuggestionRepository;

        public SaveDynamicDataSuggestionHandler(IDynamicDataSuggestionRepository dynamicDataSuggestionRepository)
        {
            _dynamicDataSuggestionRepository = dynamicDataSuggestionRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<SaveDynamicDataSuggestionsResult> Handle(SaveDynamicDataSuggestions command)
        {
            var dynamicData = _dynamicDataSuggestionRepository.Get(command.SourceTypicalName, command.SourceAttributeName, command.TargetTypicalName, command.TargetAttributeName);
            if (dynamicData == null)
            {
                dynamicData = new DynamicDataSuggestion(command.SourceTypicalName, command.SourceAttributeName, command.TargetTypicalName, command.TargetAttributeName, 1);
                _dynamicDataSuggestionRepository.Add(dynamicData);
            }
            else
            {
                dynamicData.Frequency = dynamicData.Frequency + 1;
                _dynamicDataSuggestionRepository.Update(dynamicData);
            }

            return new CommandResponse<SaveDynamicDataSuggestionsResult>(
                new SaveDynamicDataSuggestionsResult(dynamicData.Id)
            );
        }

        public bool Validate(SaveDynamicDataSuggestions command)
        {
            if (string.IsNullOrWhiteSpace(command.SourceAttributeName))
            {
                Errors.Add("Source attribute name cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.TargetAttributeName))
            {
                Errors.Add("Target attribute name cannot be empty.");
                return false;
            }
            return true;
        }
    }
}