﻿using Commands.DesignModule.WorkflowModel;
using Messaging.Commands;
using Newtonsoft.Json.Linq;
using SenseAI.Domain;
using SenseAI.Domain.ValidationModel;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public sealed class SaveWorkflowHandler : ICommandHandler<SaveWorkflow, SaveWorkflowResult>
    {
        private readonly IWorkflowRepository _workflowRepository;

        public SaveWorkflowHandler(IWorkflowRepository workflowRepository)
        {
            _workflowRepository = workflowRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<SaveWorkflowResult> Handle(SaveWorkflow command)
        {
            RequestValidation requestValidation = null;
           
            var workflow = WorkflowFactory.Create(command.Id, command.Json);

            (var designerjson, bool isChanged) = ChangeAutomaticCalculationNameWhenTestStepNameChanges(workflow, command.Json);

            if (isChanged)
            {
                workflow = WorkflowFactory.Create(command.Id, designerjson.ToString());
            }
            workflow.SetIsParsed(true);
             
            if (workflow.Validate(out requestValidation))
                _workflowRepository.Save(workflow, designerjson.ToString(), command.WorkflowPathsJson);

            return new CommandResponse<SaveWorkflowResult>(new SaveWorkflowResult(workflow.Id, designerjson.ToString()),
                new CommandResponseMessage((int)requestValidation.MessageType, requestValidation.Message)); 
        }

        public bool Validate(SaveWorkflow command)
        {
            if (command.Id== Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Json))
            {
                Errors.Add("Json cannot be empty.");
                return false;
            }
            return true;
        }
        
        private (JObject, bool) ChangeAutomaticCalculationNameWhenTestStepNameChanges(Workflow workflow, string json)
        {
            JObject jObject = JObject.Parse(json);
            var cells = jObject["cells"];

            var itemsTitleChanged = workflow.Items.Where(w => w is ActionWorkflowItem).Cast<ActionWorkflowItem>().Where(w => w.IsTitleChanged);

            if (!itemsTitleChanged.Any())
                return (jObject, false);

            var itemsChangedListId = new List<Guid>();

            foreach (var item in cells)
            { 
                if (item["customData"] == null)
                    continue;

                var dynamicDatas = item["customData"]["DynamicDatas"];
                var postConstraints = item["customData"]["PostConstraints"];
                var constraints = item["customData"]["Constraints"];

                if (dynamicDatas != null && dynamicDatas.Any())
                foreach (var dynamicDataItem in dynamicDatas)
                {
                    var itemFound = itemsTitleChanged.Where(w => w.Id == Guid.Parse(dynamicDataItem["SourceWorkflowItemId"].ToString())).FirstOrDefault();

                    if (itemFound != null)
                    {
                        string[] valueToPut = dynamicDataItem["Path"].ToString().Split('.');
                        dynamicDataItem["Path"] = "[" + itemFound.Title + "]." + valueToPut[1];
                        itemsChangedListId.Add(itemFound.Id);
                    } 
                }

                if(postConstraints != null && postConstraints.Any())
                foreach(var postConstraintItem in postConstraints) 
                {
                    var itemActual = postConstraintItem["DynamicData"];
                        if (itemActual.HasValues)
                        {
                            var itemFound = itemsTitleChanged.Where(w => w.Id == Guid.Parse(itemActual["SourceWorkflowItemId"].ToString())).FirstOrDefault();

                            if (itemFound != null)
                            {
                                string[] valueToPut = itemActual["Path"].ToString().Split('.');
                                itemActual["Path"] = "[" + itemFound.Title + "]." + valueToPut[1];
                                itemsChangedListId.Add(itemFound.Id);
                            }
                        }
                }

                if (constraints != null && constraints.Any()) 
                foreach (var constraintItem in constraints)
                {
                    var itemActual = constraintItem["DynamicData"];
                        if (itemActual.HasValues)
                        {
                            var itemFound = itemsTitleChanged.Where(w => w.Id == Guid.Parse(itemActual["SourceWorkflowItemId"].ToString())).FirstOrDefault();

                            if (itemFound != null)
                            {
                                string[] valueToPut = itemActual["Path"].ToString().Split('.');
                                itemActual["Path"] = "[" + itemFound.Title + "]." + valueToPut[1];
                                itemsChangedListId.Add(itemFound.Id);
                            }
                        }
                } 
            }

            foreach (var item in cells)
            {
                if (item["customData"] == null)
                    continue;

                if (itemsChangedListId.Contains(Guid.Parse(item["customData"]["Id"].ToString())))
                {
                    item["customData"]["IsTitleChanged"] = "false";
                }

            }
            return (jObject, true);
        }
     
    }
}