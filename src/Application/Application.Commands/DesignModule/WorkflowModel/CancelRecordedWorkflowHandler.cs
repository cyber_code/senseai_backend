﻿using Commands.DesignModule.WorkflowModel;
using Core.Abstractions;
using Messaging.Commands;
using Newtonsoft.Json;
using Persistence.Internal;
using SenseAI.Domain;
using SenseAI.Domain.WorkflowModel;
using SignalR;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public sealed class CancelRecordedWorkflowHandler : ICommandHandler<CancelRecordedWorkflow, bool>
    {
        private readonly IWorkContext _workContext;
        private readonly ISignalRClient _signalRClient;
        private readonly IWorkflowRepository _workflowRepository;

        public CancelRecordedWorkflowHandler(IWorkContext workContext, ISignalRClient signalRClient, IWorkflowRepository workflowRepository)
        {
            _workContext = workContext;
            _signalRClient = signalRClient;
            _workflowRepository = workflowRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(CancelRecordedWorkflow command)
        {
            _workflowRepository.Delete(command.WorkflowId);
            _signalRClient.SendAsync(new SignalRCommand
            {
                Method = "CancelRecordedWorkflow",
                Group = _workContext.FullName,
                Value = command.WorkflowId.ToString()
            });
            return new CommandResponse<bool>(true);
        }

        public bool Validate(CancelRecordedWorkflow command)
        {
            if (command.WorkflowId == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        } 
    }
}
