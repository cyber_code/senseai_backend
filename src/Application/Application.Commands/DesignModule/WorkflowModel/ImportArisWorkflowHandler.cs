﻿using Microsoft.EntityFrameworkCore;
using Commands.DesignModule.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using Persistence.CatalogModel;
using Persistence.Internal;
using Persistence.SystemModel;
using System;
using System.Collections.Generic;
using System.Linq;
using Path = System.IO.Path;
using SenseAI.Domain.Process.ArisWorkflowModel;
using Newtonsoft.Json;
using Persistence.DataModel;
using SenseAI.Domain.DataModel;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public sealed class ImportArisWorkflowHandler : ICommandHandler<ImportArisWorkflow, bool>
    {
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IGeneratePaths _generatePaths;
        private readonly IDbContext _dbContext;
        private readonly IImportArisWorkflow _importArisWorkflow;

        public ImportArisWorkflowHandler(IWorkflowRepository workflowRepository, IDbContext dbContext,
            IImportArisWorkflow importArisWorkflow, IGeneratePaths generatePaths)
        {
            _workflowRepository = workflowRepository;
            _dbContext = dbContext;
            _importArisWorkflow = importArisWorkflow;
            _generatePaths = generatePaths;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(ImportArisWorkflow command)
        {
            string catalogTitle = _dbContext.Set<CatalogData>().AsNoTracking().FirstOrDefault(w => w.Id == command.CatalogId).Title;
            var typicals = (from typical in _dbContext.Set<TypicalData>().AsNoTracking()
                            where typical.CatalogId == command.CatalogId
                            select new Typical(typical.Id, typical.CatalogId, typical.Title, "", "", "", SenseAI.Domain.TypicalType.Application)).ToList();

            List<Workflow> workflows = _importArisWorkflow.XmlToWorkflow(command.File, command.FolderId, command.SystemId, command.CatalogId, catalogTitle, typicals);
            foreach (Workflow workflow in workflows)
            {
                workflow.Items.Where(w => w is ActionWorkflowItem).ToList().ForEach(ct1 =>
                {
                    ct1.GetCatalogName(GetCatalogName(command.CatalogId));
                });

                ((StartWorkflowItem)workflow.Items.Where(w => w is StartWorkflowItem).FirstOrDefault()).SetAdapterName(GetAdapterName(command.SystemId));

                workflow.SetIsParsed(false);
                _workflowRepository.Add(workflow);
                var paths = _generatePaths.Generate(workflow);
                int index = 1;
                var workflowPathDesinger = new WorkflowPathDesinger
                {
                    WorkflowPaths = paths.Select(s =>
                    new WorkflowPath
                    {
                        Coverage = s.Coverage.ToString(),
                        IsBestPath = s.IsBestPath,
                        ItemIds = s.ItemIds,
                        Selected = true,
                        Title = "Path " + (index++)
                    }).ToArray()
                };
                var jsonPath = JsonConvert.SerializeObject(workflowPathDesinger); 
                _workflowRepository.Save(workflow, JsonConvert.SerializeObject(workflow), jsonPath);
            }
            return new CommandResponse<bool>(true);
        }

        public bool Validate(ImportArisWorkflow command)
        {
            if (command.FolderId == Guid.Empty)
            {
                Errors.Add("FolderId cannot be empty.");
                return false;
            }

            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }

            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }

            string ext = Path.GetExtension(command.FileName);
            if (ext != ".xml")
            {
                Errors.Add("File extension not supported!");
                return false;
            }

            if (String.IsNullOrEmpty(command.File))
            {
                Errors.Add("FileContent cannot be empty.");
                return false;
            }
            return true;
        }

        private string GetAdapterName(Guid systemId)
        {
            var adapterResults = from system in _dbContext.Set<SystemData>().AsNoTracking()
                                 where system.Id == systemId
                                 select new
                                 {
                                     system.AdapterName,
                                 };
            return adapterResults.FirstOrDefault().AdapterName;
        }

        private string GetCatalogName(Guid catalogId)
        {
            var catalogResults = from catalog in _dbContext.Set<CatalogData>().AsNoTracking()
                                 where catalog.Id == catalogId
                                 select new
                                 {
                                     catalog.Title,
                                 };
            return catalogResults.Select(catalog => catalog.Title).FirstOrDefault().ToString();
        }
    }

}