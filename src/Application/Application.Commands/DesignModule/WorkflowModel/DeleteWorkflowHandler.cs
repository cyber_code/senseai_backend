﻿using Commands.DesignModule.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using System.Collections.Generic;
using System;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public sealed class DeleteWorkflowHandler : ICommandHandler<DeleteWorkflow, bool>
    {
        private readonly IWorkflowRepository _workflowRepository;

        public DeleteWorkflowHandler(IWorkflowRepository workflowRepository)
        {
            _workflowRepository = workflowRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteWorkflow command)
        {
            _workflowRepository.Delete(command.Id);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteWorkflow command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}