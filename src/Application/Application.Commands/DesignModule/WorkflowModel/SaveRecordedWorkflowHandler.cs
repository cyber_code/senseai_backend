﻿using Commands.DataModule.TestCases;
using Commands.DesignModule.WorkflowModel;
using Core.Abstractions;
using Messaging.Commands;
using Newtonsoft.Json; 
using SenseAI.Domain;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.WorkflowModel;
using SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using CommandsCombinations = Commands.DataModule.DataSets.Combinations;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public sealed class SaveRecordedWorkflowHandler : ICommandHandler<SaveRecordedWorkflow, bool>
    {
        private readonly IWorkflowRepository _workflowRepository;
        private readonly ISignalRClient _signalRClient;
        private readonly IGeneratePaths _generatePaths;
        private readonly IWorkContext _workContext;
        private readonly IDataSetRepository _dataSetRepository;
        private readonly ITypicalRepository _typicalRepository;
        private readonly IWorkflowVersionPathsRepository _workflowPathsRepository;

        private readonly ICommandHandler<IssueWorkflow, IssueWorkflowResult> _issueCommand;
        private readonly ICommandHandler<GenerateTestCases, GenerateTestCasesResult> _generateTestCasesCommand;

        List<DataSet> _dataSets = new List<DataSet>(); 

        public SaveRecordedWorkflowHandler(IWorkflowRepository workflowRepository, ISignalRClient signalRClient,
            IGeneratePaths generatePaths, IWorkContext workContext, IDataSetRepository dataSetRepository, IWorkflowVersionPathsRepository workflowPathsRepository,
            ICommandHandler<IssueWorkflow, IssueWorkflowResult> issueCommand,
            ICommandHandler<GenerateTestCases, GenerateTestCasesResult> generateTestCasesCommand,
            ITypicalRepository typicalRepository)
        {
            _workflowRepository = workflowRepository;
            _signalRClient = signalRClient;
            _generatePaths = generatePaths;
            _workContext = workContext;
            _dataSetRepository = dataSetRepository;
            _workflowPathsRepository = workflowPathsRepository;
            _issueCommand = issueCommand;
            _generateTestCasesCommand = generateTestCasesCommand;
            _typicalRepository = typicalRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(SaveRecordedWorkflow command)
        {
            var workflow = new Workflow(command.WorkflowId, command.Title, command.Title, command.Header.FolderId, true, false);
            Command2Workflow(command, workflow);

            var paths = GetPaths(workflow);
            var jsonPath = JsonConvert.SerializeObject(paths);
            _workflowRepository.Save(workflow, JsonConvert.SerializeObject(workflow), jsonPath);

            IssueWorkflow issueWorkflow = new IssueWorkflow
            {
                WorkflowId = workflow.Id,
                NewVersion = true,
                SkipStatus = true,
                WorkflowImage = "",
                TenantId = command.Header.TenantId,
                ProjectId = command.Header.ProjectId,
                SubProjectId = command.Header.SubProjectId,
                CatalogId = command.Header.CatalogId,
                SystemId = command.Header.SystemId,
                CurrentVersionId = 0,
                VersionStatus = IssueVersioning.NoAction
            };
            _issueCommand.Handle(issueWorkflow);

            if (_dataSets.Count > 0)
            {
                _dataSets.ForEach(w => _dataSetRepository.Add(w));
                GenerateTestCases generateTestCases = new GenerateTestCases
                {
                    WorkflowId = workflow.Id,
                    SkipStatus = true,
                    WorkflowPathId = paths.WorkflowPaths.FirstOrDefault().PathId
                };

                _generateTestCasesCommand.Handle(generateTestCases);
            }
            _signalRClient.SendAsync(new SignalRCommand
            {
                Method = "SaveRecordedWorkflow",
                Group = _workContext.FullName,
                Value = command.WorkflowId.ToString()
            });
            return new CommandResponse<bool>(true);
        }

        private WorkflowPathDesinger GetPaths(Workflow workflow)
        {
            WorkflowPathDesinger workflowPathDesinger = null;
            var paths = _generatePaths.Generate(workflow);
            int index = 1;
            workflowPathDesinger = new WorkflowPathDesinger
            {
                WorkflowPaths = paths.Select(s =>

                new WorkflowPath
                {
                    PathId = Guid.NewGuid(),
                    Coverage = s.Coverage.ToString(),
                    IsBestPath = s.IsBestPath,
                    ItemIds = s.ItemIds,
                    Selected = true,
                    Title = "Path " + (index++)
                }).ToArray()
            };
            return workflowPathDesinger;
        }

        private void Command2Workflow(SaveRecordedWorkflow command, Workflow workflow)
        {
            command.Items.ToList().ForEach(w => workflow.AddActionItem(w.Id, w.Title, w.Description == null ? "" : w.Description, w.CatalogId, w.CatalogName, w.TypicalId,
                 w.TypicalName, w.TypicalType, w.ActionType, w.Parameters, GetConstraints(w.Constraints), GetConstraints(w.PostConstraints), GetItemDynamicDatas(w.DynamicDatas),
                 !string.IsNullOrEmpty(w.ExpectedError), w.ExpectedError));

            command.ItemLinks.ToList().ForEach(w => workflow.AddItemLink(w.FromId, w.ToId, (WorkflowItemLinkState)w.State));

            var workflowItem = workflow.Items.Where(ct => (workflow.ItemLinks.Where(il => il.ToId == ct.Id).FirstOrDefault() == null)).FirstOrDefault();

            foreach (var item in workflow.Items.Where(w => w.ActionType == (int)ActionType.EnquiryAction || w.ActionType == (int)ActionType.EnquiryResult))
            {
                _typicalRepository.AddEnquiryActions(item.CatalogId, item.TypicalName.Replace(".RESULT", ""), item.Parameters);
            }

            workflow.Items.Where(w => (w.ActionType == (int)ActionType.EnquiryAction || w.ActionType == (int)ActionType.EnquiryResult) &&
            !w.TypicalName.ToLower().StartsWith("v_")).ToList().ForEach(f =>
            {
                f.SetParameters(GetParameters(f.Parameters));
            });

            var tesWorkflowItem = command.Items.Where(ct => (command.ItemLinks.Where(il => il.ToId == ct.Id).FirstOrDefault() == null)).FirstOrDefault();

            Guid startId = Guid.NewGuid();
            workflow.AddStartItem(startId, tesWorkflowItem.SystemId, "Start", "Start", tesWorkflowItem.CatalogId, AdapterType.T24WebAdapter.ToString(), "", Guid.Empty, "", "", 0, "");

            workflow.AddItemLink(startId, workflowItem.Id, WorkflowItemLinkState.Yes);

            Guid fromId = tesWorkflowItem.Id;
            Guid systemId = tesWorkflowItem.SystemId;
            while (command.ItemLinks.FirstOrDefault(w => w.FromId == fromId) != null)
            {
                var toId = command.ItemLinks.FirstOrDefault(w => w.FromId == fromId).ToId;

                tesWorkflowItem = command.Items.FirstOrDefault(w => w.Id == toId);

                if (systemId != tesWorkflowItem.SystemId)
                {
                    var wokrlowLink = workflow.ItemLinks.FirstOrDefault(w => w.FromId == fromId && w.ToId == toId);
                    startId = Guid.NewGuid();
                    workflow.AddStartItem(startId, tesWorkflowItem.SystemId, "Switch\nSystem", "Switch\nSystem", tesWorkflowItem.CatalogId, AdapterType.T24WebAdapter.ToString(), "", Guid.Empty, "", "", 0, "");
                    wokrlowLink.SetToId(startId);

                    workflow.AddItemLink(startId, toId, WorkflowItemLinkState.Yes);

                    systemId = tesWorkflowItem.SystemId;
                }

                fromId = tesWorkflowItem.Id;

            }
            

            if (command.DataSet == null)
                return;
            foreach (var teDataSet in command.DataSet)
            {
                List<DataSetItem> dataSetItems = new List<DataSetItem>(); 
                dataSetItems.Add(new DataSetItem(Guid.NewGuid(), JsonConvert.SerializeObject(teDataSet.Attributes)));
                DataSet dataSet = new DataSet(Guid.NewGuid(), command.Header.SubProjectId, command.Header.CatalogId,
                                                command.Header.SystemId, teDataSet.Typical.Id, "Data set for: " + teDataSet.Typical.Name + " " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), 0,
                                                GenerateCombinations(teDataSet), dataSetItems.ToArray());
                
                _dataSets.Add(dataSet);
            }
        }

        private WorkflowItemConstraint[] GetConstraints(Constraints[] constraints)
        {
            if (constraints == null || !constraints.Any())
                return new WorkflowItemConstraint[0];
            return
                constraints.Select(c => new WorkflowItemConstraint(
                c.AttributeName,
                c.Operator,
                c.AttributeValue,
                GetItemDynamicData(c.DynamicData), c.IsPostFilter)).ToArray();
        }

        private WorkflowItemDynamicData GetItemDynamicData(DynamicDatas dynamicData)
        {
            if (dynamicData == null)
                return null;
            return new WorkflowItemDynamicData(
                                      dynamicData.SourceWorkflowItemId,
                                      dynamicData.SourceAttributeName,
                                      dynamicData.TargetAttributeName,
                                      dynamicData.PlainTextFormula,
                                      dynamicData.Path,
                                      dynamicData.UiMode,
                                      dynamicData.PathName);
        }

        private WorkflowItemDynamicData[] GetItemDynamicDatas(DynamicDatas[] dynamicDatas)
        {
            if (dynamicDatas == null || !dynamicDatas.Any())
                return new WorkflowItemDynamicData[0];
            return dynamicDatas.Select(s => GetItemDynamicData(s)).ToArray();
        }
        private string GetParameters(string parameter)
        {
            EnquiryAction enquiryAction = null;
            try
            {
                enquiryAction = JsonConvert.DeserializeObject<EnquiryAction>(parameter);
            }
            catch (Exception ex)
            {
            }
            if (enquiryAction == null)
                return parameter;
            return enquiryAction.Type + ":" + enquiryAction.Action;
        } 

        private string GenerateCombinations(TEDataSets row)
        {
            string combinations = string.Empty;

            List<TEAttributes> attributes = new List<TEAttributes>();
            attributes.AddRange(row.Attributes.Select(ct => new TEAttributes { Name = ct.Name, Value = ct.Value }).ToList());

            var grAttributes = attributes.GroupBy(ct => ct.Name);

            var listOfCombinations = grAttributes.Select(att => new CommandsCombinations { Attribute = att.Key, Values = att.Select(val => val.Value).Distinct().ToList() });
            if (listOfCombinations.Count() > 0)
                combinations = JsonConvert.SerializeObject(listOfCombinations);

            return combinations;
        }

        public bool Validate(SaveRecordedWorkflow command)
        {
            if (command.WorkflowId == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            if (command.Items == null || command.Items.Count() == 0)
            {
                Errors.Add("No workflow items attached to workflow.");
                return false;
            }
            return true;
        }
    }
}
