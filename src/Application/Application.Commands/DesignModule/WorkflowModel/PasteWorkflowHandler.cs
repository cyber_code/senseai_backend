﻿using Commands.DesignModule.WorkflowModel;
using SenseAI.Domain;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using System.Collections.Generic;
using System;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public sealed class PasteWorkflowHandler : ICommandHandler<PasteWorkflow, PasteWorkflowResult>
    {
        private readonly IWorkflowRepository _workflowRepository;

        public PasteWorkflowHandler(IWorkflowRepository workflowRepository)
        {
            _workflowRepository = workflowRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<PasteWorkflowResult> Handle(PasteWorkflow command)
        {
            var result = new PasteWorkflowResult(command.WorkflowId, string.Empty);

            if (command.Type == PasteType.Cut)
            {
                _workflowRepository.Cut(command.WorkflowId, command.FolderId);
            }
            else if (command.Type == PasteType.Copy)
            {
                var workflow = _workflowRepository.Paste(command.WorkflowId, command.FolderId);

                result = new PasteWorkflowResult(workflow.Id, workflow.Title);
            }

            return new CommandResponse<PasteWorkflowResult>(result);
        }

        public bool Validate(PasteWorkflow command)
        {
            if (command.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            if (command.FolderId == Guid.Empty)
            {
                Errors.Add("FolderId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}