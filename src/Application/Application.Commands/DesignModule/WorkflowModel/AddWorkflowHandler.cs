﻿using Commands.DesignModule.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using System.Collections.Generic;
using System;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public sealed class AddWorkflowHandler : ICommandHandler<AddWorkflow, AddWorkflowResult>
    {
        private readonly IWorkflowRepository _workflowRepository;

        public AddWorkflowHandler(IWorkflowRepository workflowRepository)
        {
            _workflowRepository = workflowRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddWorkflowResult> Handle(AddWorkflow command)
        {
            var workflow = new Workflow(command.Title, command.Description, command.FolderId);

            _workflowRepository.Add(workflow);

            return new CommandResponse<AddWorkflowResult>(
                new AddWorkflowResult(workflow.Id)
            );
        }

        public bool Validate(AddWorkflow command)
        {
            if (command.FolderId == Guid.Empty)
            {
                Errors.Add("FolderId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}