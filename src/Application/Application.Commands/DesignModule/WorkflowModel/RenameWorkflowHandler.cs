﻿using Commands.DesignModule.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using System.Collections.Generic;
using System;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public sealed class RenameWorkflowHandler : ICommandHandler<RenameWorkflow, bool>
    {
        private readonly IWorkflowRepository _workflowRepository;

        public RenameWorkflowHandler(IWorkflowRepository workflowRepository)
        {
            _workflowRepository = workflowRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(RenameWorkflow command)
        {
            _workflowRepository.Rename(command.WorkflowId, command.Title);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(RenameWorkflow command)
        {
            if (command.WorkflowId == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}