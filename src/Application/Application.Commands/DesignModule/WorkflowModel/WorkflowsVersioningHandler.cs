﻿using Commands.DesignModule.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using System.Collections.Generic;
using System;
using Persistence.Internal;
using Microsoft.EntityFrameworkCore;
using Persistence.WorkflowModel;
using System.Linq;
using Persistence.AdminModel;
using Newtonsoft.Json;
using Commands.DataModule.TestCases;
using Newtonsoft.Json.Linq;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public sealed class WorkflowsVersioningHandler : ICommandHandler<WorkflowsVersioning, bool>
    {
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IWorkflowVersionPathsRepository _workflowVersionPathsRepository;
        private readonly IGeneratePaths _generatePaths;
        private readonly IDbContext _dbContext;
        private readonly ICommandHandler<IssueWorkflow, IssueWorkflowResult> _issue;
        ICommandHandler<GenerateTestCases, GenerateTestCasesResult> _generateCommand;

        public WorkflowsVersioningHandler(IWorkflowRepository workflowRepository, IDbContext dbContext,
            ICommandHandler<IssueWorkflow, IssueWorkflowResult> issue, IGeneratePaths generatePaths,
            ICommandHandler<GenerateTestCases, GenerateTestCasesResult> generateCommand, IWorkflowVersionPathsRepository workflowVersionPathsRepository)
        {
            _workflowRepository = workflowRepository;
            _dbContext = dbContext;
            _issue = issue;
            _generatePaths = generatePaths;
            _generateCommand = generateCommand;
            _workflowVersionPathsRepository = workflowVersionPathsRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(WorkflowsVersioning command)
        {
            var foldersData = _dbContext.Set<FolderData>().AsNoTracking().ToList();
            List<string> list = new List<string>() {
                "778AE691-82AD-4BB5-8DE5-1EAA64F33B94",
                "5D693ECC-BE74-4C35-958A-267E96D5E843",
                "3F167F31-01B6-4E02-833B-28493078ECA9",
                "1ACCB9EB-D982-408B-92A0-365C6B3C78A8",
                "B24B0764-4E4E-487C-B0D2-529BB6B0E7B1",
                "1991FDFC-2D3A-44E9-B574-574B2EFD86AA",
                "38F88C1B-3305-477E-A5CB-5CC76C4DB359",
                "3F0A427D-B86B-4284-810E-71F36ED541DB",
                "D7A065E7-AEF8-4561-A008-76DB688243E4",
                "737F269E-55BA-4523-B747-7CC18984BFE1",
                "872AC80C-7B1F-437E-AFD3-85F5A2088716",
                "8B7EB83D-D17D-4206-8A91-A05224D459E3",
                "877C37D5-1CAF-4CAF-AC03-B557B3A00710",
                "37711F07-1227-42AC-BECA-C01E52C7F879",
                "53C503C4-0B28-42D0-8E2E-C72D758A1338"
            };

            var workflowsData = _dbContext.Set<WorkflowData>().AsNoTracking().Where(w => list.Contains(w.Id.ToString())).ToList();

            foreach (var workflowData in workflowsData)
            {
                try
                {
                    var folder = foldersData.FirstOrDefault(w => w.Id == workflowData.FolderId);
                    if (folder != null)
                    {
                        var subProject = _dbContext.Set<SubProjectData>().AsNoTracking().FirstOrDefault(w => w.Id == folder.SubProjectId);
                        if (subProject != null)
                        {
                            command.SubProjectId = subProject.Id;
                            var project = _dbContext.Set<ProjectData>().AsNoTracking().FirstOrDefault(w => w.Id == subProject.ProjectId);
                            if (project != null)
                            {

                                command.ProjectId = project.Id;
                                var workflowVersion = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == workflowData.Id && w.IsLast);
                                var json = workflowData.DesignerJson;
                                var workflow = WorkflowFactory.Create(workflowData.Id, workflowData.DesignerJson);
                                if (!workflow.Items.Any(a => a is LinkedWorkflowItem))
                                    continue;

                                if (!command.Generate)
                                {

                                    if (workflow.Items.FirstOrDefault(w => w is LinkedWorkflowItem) != null)
                                    {

                                        foreach (var wfItem in workflow.Items.Where(wl => wl is LinkedWorkflowItem))
                                        {
                                            var wfLinked = (LinkedWorkflowItem)wfItem;
                                            var innerWorklowVersionData = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == wfLinked.WorkflowId && w.IsLast);
                                            if (innerWorklowVersionData == null)
                                            {
                                                var innerWorklowData = _dbContext.Set<WorkflowData>().AsNoTracking().FirstOrDefault(w => w.Id == wfLinked.WorkflowId);
                                                if (innerWorklowData == null)
                                                    continue;
                                                var innerWorkflow = WorkflowFactory.Create(innerWorklowData.Id, innerWorklowData.DesignerJson);
                                                if (!innerWorkflow.Items.Any(a => a is LinkedWorkflowItem))
                                                {
                                                    /*_generatePaths.Generate(innerWorkflow);
                                                    _workflowRepository.Save(innerWorkflow, workflowData.DesignerJson, JsonConvert.SerializeObject(GetPaths(innerWorkflow)));
                                                    IssueWorkflow innerIssueWorkflow = new IssueWorkflow
                                                    {
                                                        TenantId = command.TenantId,
                                                        ProjectId = command.ProjectId,
                                                        SubProjectId = command.SubProjectId,
                                                        CatalogId = command.CatalogId,
                                                        SystemId = command.SystemId,
                                                        WorkflowId = innerWorkflow.Id,
                                                        WorkflowImage = "",
                                                        NewVersion = true,
                                                        CurrentVersionId = 0,
                                                        SkipStatus = true,
                                                        VersionStatus = IssueVersioning.NoAction
                                                    };
                                                    var innerIssued = _issue.Handle(innerIssueWorkflow);*/
                                                }
                                                else
                                                {
                                                    IssueRecursovely(wfLinked, command);

                                                    var lkjsonObject = JToken.Parse(innerWorklowData.DesignerJson);

                                                    foreach (var item in lkjsonObject.SelectToken("cells").Children())
                                                    {
                                                        var customData = item.SelectToken("customData.WorkflowId");
                                                        if (customData != null)
                                                        {
                                                            var jvalue = new JObject();
                                                            var lastVersionId = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == Guid.Parse(customData.ToString()) && w.IsLast);
                                                            jvalue.Add("versionId", new JValue(lastVersionId.VersionId.ToString()));

                                                            if (item["customData"]["versionId"] != null)
                                                                item["customData"]["versionId"] = lastVersionId.VersionId.ToString();
                                                            else
                                                            {
                                                                var newProperty = new JProperty("versionId", lastVersionId.VersionId);
                                                                item["customData"].Last.AddAfterSelf(newProperty);
                                                            }
                                                        }

                                                    }
                                                    string lkjson = lkjsonObject.ToString();

                                                    var lkworkflow = WorkflowFactory.Create(innerWorklowVersionData.WorkflowId, lkjson);

                                                    _generatePaths.Generate(lkworkflow);
                                                    _workflowRepository.Save(lkworkflow, lkjson, JsonConvert.SerializeObject(GetPaths(lkworkflow)));
                                                    IssueWorkflow lkissueWorkflow = new IssueWorkflow
                                                    {
                                                        TenantId = command.TenantId,
                                                        ProjectId = command.ProjectId,
                                                        SubProjectId = command.SubProjectId,
                                                        CatalogId = command.CatalogId,
                                                        SystemId = command.SystemId,
                                                        WorkflowId = innerWorklowVersionData.WorkflowId,
                                                        WorkflowImage = innerWorklowVersionData == null ? "" : innerWorklowVersionData.WorkflowImage,
                                                        NewVersion = true,
                                                        CurrentVersionId = innerWorklowVersionData == null ? 0 : innerWorklowVersionData.VersionId,
                                                        SkipStatus = true,
                                                        VersionStatus = IssueVersioning.NoAction
                                                    };
                                                    var lkissued = _issue.Handle(lkissueWorkflow);

                                                }
                                            }
                                            else
                                            {

                                                var innerWorkflow = WorkflowFactory.Create(innerWorklowVersionData.WorkflowId, innerWorklowVersionData.DesignerJson);
                                                if (innerWorkflow.Items.Any(a => a is LinkedWorkflowItem))
                                                {
                                                    IssueRecursovely(wfLinked, command);

                                                    var lkjsonObject = JToken.Parse(innerWorklowVersionData.DesignerJson);

                                                    foreach (var item in lkjsonObject.SelectToken("cells").Children())
                                                    {
                                                        var customData = item.SelectToken("customData.WorkflowId");
                                                        if (customData != null)
                                                        {
                                                            var jvalue = new JObject();
                                                            var lastVersionId = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == Guid.Parse(customData.ToString()) && w.IsLast);
                                                            if (lastVersionId == null)
                                                                continue;
                                                            jvalue.Add("versionId", new JValue(lastVersionId.VersionId.ToString()));

                                                            if (item["customData"]["versionId"] != null)
                                                                item["customData"]["versionId"] = lastVersionId.VersionId.ToString();
                                                            else
                                                            {
                                                                var newProperty = new JProperty("versionId", lastVersionId.VersionId);
                                                                item["customData"].Last.AddAfterSelf(newProperty);
                                                            }
                                                        }

                                                    }
                                                    string lkjson = lkjsonObject.ToString();

                                                    var lkworkflow = WorkflowFactory.Create(innerWorklowVersionData.WorkflowId, lkjson);

                                                    _generatePaths.Generate(lkworkflow);
                                                    _workflowRepository.Save(lkworkflow, lkjson, JsonConvert.SerializeObject(GetPaths(lkworkflow)));
                                                    IssueWorkflow lkissueWorkflow = new IssueWorkflow
                                                    {
                                                        TenantId = command.TenantId,
                                                        ProjectId = command.ProjectId,
                                                        SubProjectId = command.SubProjectId,
                                                        CatalogId = command.CatalogId,
                                                        SystemId = command.SystemId,
                                                        WorkflowId = innerWorklowVersionData.WorkflowId,
                                                        WorkflowImage = innerWorklowVersionData == null ? "" : innerWorklowVersionData.WorkflowImage,
                                                        NewVersion = true,
                                                        CurrentVersionId = innerWorklowVersionData == null ? 0 : innerWorklowVersionData.VersionId,
                                                        SkipStatus = true,
                                                        VersionStatus = IssueVersioning.NoAction
                                                    };
                                                    var lkissued = _issue.Handle(lkissueWorkflow);

                                                }
                                                else
                                                {
                                                    /*_generatePaths.Generate(innerWorkflow);
                                                    _workflowRepository.Save(innerWorkflow, innerWorklowVersionData.DesignerJson, JsonConvert.SerializeObject(GetPaths(innerWorkflow)));
                                                    IssueWorkflow innerIssueWorkflow = new IssueWorkflow
                                                    {
                                                        TenantId = command.TenantId,
                                                        ProjectId = command.ProjectId,
                                                        SubProjectId = command.SubProjectId,
                                                        CatalogId = command.CatalogId,
                                                        SystemId = command.SystemId,
                                                        WorkflowId = innerWorkflow.Id,
                                                        WorkflowImage = "",
                                                        NewVersion = true,
                                                        CurrentVersionId = 0,
                                                        SkipStatus = true,
                                                        VersionStatus = IssueVersioning.NoAction
                                                    };
                                                    var innerIssued = _issue.Handle(innerIssueWorkflow);*/
                                                }
                                            }
                                        }


                                        var jsonObject = JToken.Parse(json);

                                        foreach (var item in jsonObject.SelectToken("cells").Children())
                                        {
                                            var customData = item.SelectToken("customData.WorkflowId");
                                            if (customData != null && Guid.Parse(customData.ToString()) != Guid.Empty)
                                            {
                                                var jvalue = new JObject();
                                                var lastVersionId = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == Guid.Parse(customData.ToString()) && w.IsLast);
                                                if (lastVersionId == null)
                                                    continue;
                                                jvalue.Add("versionId", new JValue(lastVersionId.VersionId.ToString()));


                                                if (item["customData"]["versionId"] != null)
                                                    item["customData"]["versionId"] = lastVersionId.VersionId.ToString();
                                                else
                                                {
                                                    var newProperty = new JProperty("versionId", lastVersionId.VersionId);
                                                    item["customData"].Last.AddAfterSelf(newProperty);
                                                }
                                            }

                                        }
                                        json = jsonObject.ToString();
                                    }
                                    workflow = WorkflowFactory.Create(workflowData.Id, json);








                                    _generatePaths.Generate(workflow);
                                    _workflowRepository.Save(workflow, json, JsonConvert.SerializeObject(GetPaths(workflow)));
                                    IssueWorkflow issueWorkflow = new IssueWorkflow
                                    {
                                        TenantId = command.TenantId,
                                        ProjectId = command.ProjectId,
                                        SubProjectId = command.SubProjectId,
                                        CatalogId = command.CatalogId,
                                        SystemId = command.SystemId,
                                        WorkflowId = workflowData.Id,
                                        WorkflowImage = workflowVersion == null ? "" : workflowVersion.WorkflowImage,
                                        NewVersion = true,
                                        CurrentVersionId = workflowVersion == null ? 0 : workflowVersion.VersionId,
                                        SkipStatus = true,
                                        VersionStatus = IssueVersioning.NoAction
                                    };
                                    var issued = _issue.Handle(issueWorkflow);
                                }
                                else
                                {
                                    var workflowVersionPaths = _dbContext.Set<WorkflowVersionPathsData>().AsNoTracking().Where(w => w.WorkflowId == workflowData.Id && w.IsLast).ToList();
                                    workflowVersionPaths.ForEach(f =>
                                    {
                                        var generated = _generateCommand.Handle(new GenerateTestCases
                                        {
                                            TenantId = command.TenantId,
                                            ProjectId = command.ProjectId,
                                            SubProjectId = command.SubProjectId,
                                            CatalogId = command.CatalogId,
                                            SystemId = command.SystemId,
                                            WorkflowId = workflowData.Id,
                                            WorkflowPathId = f.Id,
                                            SkipStatus = true
                                        });
                                    });
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
            return new CommandResponse<bool>(true);
        }

        private void IssueRecursovely(LinkedWorkflowItem wfLinked, WorkflowsVersioning command)
        {
            var innerWorklowVersionData = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == wfLinked.WorkflowId && w.IsLast);
            if (innerWorklowVersionData == null)
            {
                var innerWorklowData = _dbContext.Set<WorkflowData>().AsNoTracking().FirstOrDefault(w => w.Id == wfLinked.WorkflowId);
                var innerWorkflow = WorkflowFactory.Create(innerWorklowData.Id, innerWorklowData.DesignerJson);
                if (!innerWorkflow.Items.Any(a => a is LinkedWorkflowItem))
                {
                    /*_generatePaths.Generate(innerWorkflow);
                    _workflowRepository.Save(innerWorkflow, innerWorklowData.DesignerJson, JsonConvert.SerializeObject(GetPaths(innerWorkflow)));
                    IssueWorkflow innerIssueWorkflow = new IssueWorkflow
                    {
                        TenantId = command.TenantId,
                        ProjectId = command.ProjectId,
                        SubProjectId = command.SubProjectId,
                        CatalogId = command.CatalogId,
                        SystemId = command.SystemId,
                        WorkflowId = innerWorkflow.Id,
                        WorkflowImage = "",
                        NewVersion = true,
                        CurrentVersionId = 0,
                        SkipStatus = true,
                        VersionStatus = IssueVersioning.NoAction
                    };
                    var innerIssued = _issue.Handle(innerIssueWorkflow);*/
                    return;
                }

                foreach (var wfItem in innerWorkflow.Items.Where(wl => wl is LinkedWorkflowItem))
                {
                    var innerwfLinked = (LinkedWorkflowItem)wfItem;
                    IssueRecursovely(innerwfLinked, command);
                    var jsonObject = JToken.Parse(innerWorklowData.DesignerJson);

                    foreach (var item in jsonObject.SelectToken("cells").Children())
                    {
                        var customData = item.SelectToken("customData.WorkflowId");
                        if (customData != null)
                        {
                            var jvalue = new JObject();
                            var lastVersionId = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == Guid.Parse(customData.ToString()) && w.IsLast);
                            jvalue.Add("versionId", new JValue(lastVersionId.VersionId.ToString()));

                            if (item["customData"]["versionId"] != null)
                                item["customData"]["versionId"] = lastVersionId.VersionId.ToString();
                            else
                            {
                                var newProperty = new JProperty("versionId", lastVersionId.VersionId);
                                item["customData"].Last.AddAfterSelf(newProperty);
                            }
                        }

                    }
                    string json = jsonObject.ToString();

                    var workflow = WorkflowFactory.Create(innerWorklowData.Id, json);

                    _generatePaths.Generate(workflow);
                    _workflowRepository.Save(workflow, json, JsonConvert.SerializeObject(GetPaths(workflow)));
                    IssueWorkflow issueWorkflow = new IssueWorkflow
                    {
                        TenantId = command.TenantId,
                        ProjectId = command.ProjectId,
                        SubProjectId = command.SubProjectId,
                        CatalogId = command.CatalogId,
                        SystemId = command.SystemId,
                        WorkflowId = innerWorklowVersionData.WorkflowId,
                        WorkflowImage = innerWorklowVersionData == null ? "" : innerWorklowVersionData.WorkflowImage,
                        NewVersion = true,
                        CurrentVersionId = innerWorklowVersionData == null ? 0 : innerWorklowVersionData.VersionId,
                        SkipStatus = true,
                        VersionStatus = IssueVersioning.NoAction
                    };
                    var issued = _issue.Handle(issueWorkflow);
                }
            }
            else
            {
                var innerWorkflow = WorkflowFactory.Create(innerWorklowVersionData.WorkflowId, innerWorklowVersionData.DesignerJson);
                if (!innerWorkflow.Items.Any(a => a is LinkedWorkflowItem))
                {
                    /*_generatePaths.Generate(innerWorkflow);
                    _workflowRepository.Save(innerWorkflow, innerWorklowVersionData.DesignerJson, JsonConvert.SerializeObject(GetPaths(innerWorkflow)));
                    IssueWorkflow innerIssueWorkflow = new IssueWorkflow
                    {
                        TenantId = command.TenantId,
                        ProjectId = command.ProjectId,
                        SubProjectId = command.SubProjectId,
                        CatalogId = command.CatalogId,
                        SystemId = command.SystemId,
                        WorkflowId = innerWorkflow.Id,
                        WorkflowImage = "",
                        NewVersion = true,
                        CurrentVersionId = 0,
                        SkipStatus = true,
                        VersionStatus = IssueVersioning.NoAction
                    };
                    var innerIssued = _issue.Handle(innerIssueWorkflow);*/
                    return;
                }

                foreach (var wfItem in innerWorkflow.Items.Where(wl => wl is LinkedWorkflowItem))
                {
                    var innerwfLinked = (LinkedWorkflowItem)wfItem;
                    IssueRecursovely(innerwfLinked, command);

                    var jsonObject = JToken.Parse(innerWorklowVersionData.DesignerJson);

                    foreach (var item in jsonObject.SelectToken("cells").Children())
                    {
                        var customData = item.SelectToken("customData.WorkflowId");
                        if (customData != null)
                        {
                            var jvalue = new JObject();
                            var lastVersionId = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == Guid.Parse(customData.ToString()) && w.IsLast);
                            if (lastVersionId == null)
                                continue;
                            jvalue.Add("versionId", new JValue(lastVersionId.VersionId.ToString()));

                            if (item["customData"]["versionId"] != null)
                                item["customData"]["versionId"] = lastVersionId.VersionId.ToString();
                            else
                            {
                                var newProperty = new JProperty("versionId", lastVersionId.VersionId);
                                item["customData"].Last.AddAfterSelf(newProperty);
                            }
                        }

                    }
                    string json = jsonObject.ToString();

                    var workflow = WorkflowFactory.Create(innerWorklowVersionData.WorkflowId, json);

                    _generatePaths.Generate(workflow);
                    _workflowRepository.Save(workflow, json, JsonConvert.SerializeObject(GetPaths(workflow)));
                    IssueWorkflow issueWorkflow = new IssueWorkflow
                    {
                        TenantId = command.TenantId,
                        ProjectId = command.ProjectId,
                        SubProjectId = command.SubProjectId,
                        CatalogId = command.CatalogId,
                        SystemId = command.SystemId,
                        WorkflowId = innerWorklowVersionData.WorkflowId,
                        WorkflowImage = innerWorklowVersionData == null ? "" : innerWorklowVersionData.WorkflowImage,
                        NewVersion = true,
                        CurrentVersionId = innerWorklowVersionData == null ? 0 : innerWorklowVersionData.VersionId,
                        SkipStatus = true,
                        VersionStatus = IssueVersioning.NoAction
                    };
                    var issued = _issue.Handle(issueWorkflow);
                }
            }
        }

        private WorkflowPathDesinger GetPaths(Workflow workflow)
        {
            WorkflowPathDesinger workflowPathDesinger = null;
            var paths = _generatePaths.Generate(workflow);
            int index = 1;
            workflowPathDesinger = new WorkflowPathDesinger
            {
                WorkflowPaths = paths.Select(s =>

                new WorkflowPath
                {
                    PathId = Guid.NewGuid(),
                    Coverage = s.Coverage.ToString(),
                    IsBestPath = s.IsBestPath,
                    ItemIds = s.ItemIds,
                    Selected = true,
                    Title = "Path " + (index++)
                }).ToArray()
            };
            return workflowPathDesinger;
        }

        public bool Validate(WorkflowsVersioning command)
        {

            return true;
        }
    }
}