﻿using Commands.DesignModule.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using System;
using SenseAI.Domain;
using System.Collections.Generic;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public class RollbackWorkflowVersionHandler : ICommandHandler<RollbackWorkflowVersion, RollbackWorkflowVersionResult>
    {
        private readonly IWorkflowRepository _workflowRepository;
        private readonly ICommandHandler<IssueWorkflow, IssueWorkflowResult> _issueCommand;

        public RollbackWorkflowVersionHandler(IWorkflowRepository workflowRepository, ICommandHandler<IssueWorkflow, IssueWorkflowResult> issueCommand)
        {
            _workflowRepository = workflowRepository;
            _issueCommand = issueCommand;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<RollbackWorkflowVersionResult> Handle(RollbackWorkflowVersion command)
        {
            MessageType messageType = MessageType.Successful;
            string message = "";
            if (command.SkipStatus == false)
            {
                //check workflow status
                int workflowStatus = _workflowRepository.GetWorkflowStatus(command.WorkflowId);
                if (workflowStatus != (int)WorkflowStatus.ReadyToIssue)
                {
                    messageType = MessageType.Information;
                    message = $"This workflow is currently in progress by another user.If you continue they will lose any unsaved progress.";
                    return new CommandResponse<RollbackWorkflowVersionResult>(new RollbackWorkflowVersionResult(command.WorkflowId, command.Version, ""), new CommandResponseMessage((int)messageType, message));
                }
            }
            else
            {
                _workflowRepository.UpdateStatus(command.WorkflowId, WorkflowStatus.ReadyToIssue);
            }

            var designerJson = _workflowRepository.RollbackWorkflow(command.WorkflowId, command.Version);

            var workflowImage = _workflowRepository.GetWorkflowImage(command.WorkflowId, command.Version);
            IssueWorkflow issueWorkflow = new IssueWorkflow
            {
                TenantId = command.TenantId,
                ProjectId = command.ProjectId,
                SubProjectId = command.SubProjectId,
                CatalogId = command.CatalogId,
                SystemId = command.SystemId,
                CurrentVersionId = command.Version,
                WorkflowId = command.WorkflowId,
                NewVersion = true,
                WorkflowImage = workflowImage,
                SkipStatus = true,
                VersionStatus = IssueVersioning.LinkedNewVersion,
                IsRollBack = true
            };

            _issueCommand.Handle(issueWorkflow);
            return new CommandResponse<RollbackWorkflowVersionResult>(
                new RollbackWorkflowVersionResult(command.WorkflowId, command.Version, designerJson), new CommandResponseMessage((int)messageType, message)
            );
        }

        public bool Validate(RollbackWorkflowVersion command)
        {
            if (command.WorkflowId == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }
    }
}