﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Commands.DesignModule.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using Persistence.Internal;
using Persistence.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;
using Core.Abstractions;
using Commands.DataModule.TestCases;

namespace Application.Commands.DesignModule.WorkflowModel
{
    public sealed class IssueWorkflowHandler : ICommandHandler<IssueWorkflow, IssueWorkflowResult>
    {
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IWorkflowVersionPathsRepository _pathsRepository;
        private readonly IDbContext _dbContext;
        private readonly IWorkContext _workContext;
        private readonly IGeneratePaths _generatePaths;
        private readonly ICommandHandler<GenerateTestCases, GenerateTestCasesResult> _generateCommand;

        public IssueWorkflowHandler(IWorkflowRepository workflowRepository, IWorkflowVersionPathsRepository pathsRepository,
            IDbContext dbContext, IWorkContext workContext, ICommandHandler<GenerateTestCases, GenerateTestCasesResult> generateCommand,
            IGeneratePaths generatePaths)
        {
            _workflowRepository = workflowRepository;
            _pathsRepository = pathsRepository;
            _dbContext = dbContext;
            _workContext = workContext;
            _generateCommand = generateCommand;
            _generatePaths = generatePaths;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<IssueWorkflowResult> Handle(IssueWorkflow command)
        {
            MessageType messageType = MessageType.Successful;
            string message = string.Empty;

            var userModifiedId = _dbContext.Set<WorkflowData>().Where(u => u.Id == command.WorkflowId).Select(u => u.UserModified).FirstOrDefault();

            //check workflow status
            if (command.SkipStatus == false && userModifiedId != _workContext.UserId)
            {
                int workflowStatus = _workflowRepository.GetWorkflowStatus(command.WorkflowId);
                if (workflowStatus != (int)WorkflowStatus.ReadyToIssue)
                {
                    message = "This workflow is currently in progress by another user. If you continue they will lose any unsaved progress.";
                    messageType = MessageType.Information;
                    return new CommandResponse<IssueWorkflowResult>(new IssueWorkflowResult(command.WorkflowId, command.WorkflowImage), new CommandResponseMessage((int)messageType, message));
                }
            }
            else
            {
                _workflowRepository.UpdateStatus(command.WorkflowId, WorkflowStatus.ReadyToIssue);
            }

            int nrVersions = _workflowRepository.GetNrVersions(command.WorkflowId);
            bool newVersion = nrVersions == 0 || command.NewVersion;
            if (newVersion)
                _workflowRepository.Issue(command.WorkflowId, command.WorkflowImage);
            else
                _workflowRepository.Update(command.WorkflowId);

            Workflow workflow = _workflowRepository.GetLastVersion(command.WorkflowId);

            var json = JObject.Parse(workflow.WorkflowPathsJson);
            WorkflowPathDesinger workflowPathDesinger = JsonConvert.DeserializeObject<WorkflowPathDesinger>(json.ToString());
            foreach (LinkedWorkflowItem lkWorkflow in workflow.Items.Where(ct => ct is LinkedWorkflowItem))
            {
                try
                {
                    var innerWorkflow = _workflowRepository.GetWorkflowByVersion(lkWorkflow.WorkflowId, lkWorkflow.WorkflowVersionId);
                    lkWorkflow.SetTitle(lkWorkflow.Title + ": " + innerWorkflow.Title);

                    innerWorkflow.SetParentIds(lkWorkflow.Id);
                    innerWorkflow.SetUniqueIds(lkWorkflow.Id.ToString());
                    lkWorkflow.SetUniqueId(Guid.Empty.ToString());
                    AppendRecursivelyLinkWorkflow(innerWorkflow, lkWorkflow.Id.ToString());

                    workflow.AppendLinkWorkflow(lkWorkflow, innerWorkflow, lkWorkflow.Id.ToString());
                    workflow.AddItems(innerWorkflow.Items.ToList());
                    workflow.AddLinks(innerWorkflow.ItemLinks.ToList());
                }
                catch (Exception ex)
                {
                    var workflowTitle = _dbContext.Set<WorkflowData>().Where(u => u.Id == lkWorkflow.WorkflowId).Select(u => u.Title).FirstOrDefault();
                    messageType = MessageType.Failed;
                    message = $"There is linked workflow with title \" {workflowTitle} \", that is not issued!";
                    return new CommandResponse<IssueWorkflowResult>(new IssueWorkflowResult(command.WorkflowId, command.WorkflowImage), new CommandResponseMessage((int)messageType, message));
                }
            }

            if (workflowPathDesinger != null && workflowPathDesinger.WorkflowPaths.Count() > 0)
            {
                List<WorkflowVersionPaths> versionPaths = new List<WorkflowVersionPaths>();
                if (newVersion)
                {
                    versionPaths = GetWorkflowPaths(workflow, nrVersions, workflowPathDesinger);
                    _pathsRepository.Add(versionPaths.ToArray());
                }
                else
                {
                    nrVersions = nrVersions - 1;
                    versionPaths = GetWorkflowPaths(workflow, nrVersions, workflowPathDesinger);
                    _pathsRepository.Update(versionPaths.ToArray());
                }

                _pathsRepository.GetVersionPaths(workflow.Id, nrVersions).ToList().ForEach(f =>
                {
                    _generateCommand.Handle(new GenerateTestCases
                    {
                        TenantId = command.TenantId,
                        ProjectId = command.ProjectId,
                        SubProjectId = command.SubProjectId,
                        CatalogId = command.CatalogId,
                        SystemId = command.SystemId,
                        WorkflowId = command.WorkflowId,
                        WorkflowPathId = f.Id,
                        SkipStatus = true
                    });
                });
            }

            //update workflow status
            _workflowRepository.UpdateStatus(command.WorkflowId, WorkflowStatus.ReadyToGenerate);
            _workflowRepository.SetIsIssued(command.WorkflowId);

            if (newVersion && command.VersionStatus == IssueVersioning.LinkedUpagrade)
            {
                _workflowRepository.UpgradeLinkedWorkflowVersion(command.WorkflowId, command.CurrentVersionId, nrVersions);
            }
            else if (newVersion && command.VersionStatus == IssueVersioning.LinkedNewVersion)
            {
                NewLinkedVersion(command, nrVersions);
            }

            return new CommandResponse<IssueWorkflowResult>(new IssueWorkflowResult(command.WorkflowId, command.WorkflowImage), new CommandResponseMessage((int)messageType, message));
        }

        public bool Validate(IssueWorkflow command)
        {
            if (command.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (command.WorkflowId == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            return true;
        }

        private void AppendRecursivelyLinkWorkflow(Workflow workflow, string uniqueId)
        {
            if (workflow.Items.Where(ct => ct is LinkedWorkflowItem).Count() == 0)
                return;
            foreach (LinkedWorkflowItem lkWorkflow in workflow.Items.Where(ct => ct is LinkedWorkflowItem))
            {
                var innerWorkflow = _workflowRepository.GetWorkflowByVersion(lkWorkflow.WorkflowId, lkWorkflow.WorkflowVersionId);
                lkWorkflow.SetTitle(lkWorkflow.Title + ": " + innerWorkflow.Title);

                string innweUniqueId = uniqueId + "" + lkWorkflow.Id.ToString();
                innerWorkflow.SetParentIds(lkWorkflow.Id);
                innerWorkflow.SetUniqueIds(innweUniqueId);

                workflow.AppendLinkWorkflow(lkWorkflow, innerWorkflow, innweUniqueId);
                AppendRecursivelyLinkWorkflow(innerWorkflow, innweUniqueId);

                workflow.AddItems(innerWorkflow.Items.ToList());
                workflow.AddLinks(innerWorkflow.ItemLinks.ToList());
            }
        }

        //get workflow paths
        private List<WorkflowVersionPaths> GetWorkflowPaths(Workflow workflow, int nrVersion, WorkflowPathDesinger workflowPathDesinger)
        {
            List<WorkflowVersionPaths> list = new List<WorkflowVersionPaths>();
            int tcIndex = 0;
            int tsIndex = 0;
            foreach (var workflowPath in workflowPathDesinger.WorkflowPaths)
            {
                Guid pathId = Guid.NewGuid();
                tsIndex = 0;
                WorkflowVersionPaths workflowVersionPath = new WorkflowVersionPaths(pathId, workflow.Id, nrVersion, workflowPath.Title,
                                workflowPath.Selected, workflowPath.Coverage.ToLower() == "n/a" ? 0 : decimal.Parse(workflowPath.Coverage), tcIndex, false);
                foreach (var itemId in workflowPath.ItemIds)
                {
                    var wfItem = workflow.Items.FirstOrDefault(ct => ct.Id == itemId);

                    try
                    {
                        int pathItemType = 0;
                        int actionType = -1;
                        if (wfItem is ActionWorkflowItem)
                        {
                            pathItemType = 1;
                            actionType = wfItem.ActionType;
                        }
                        if (wfItem is ConditionWorkflowItem)
                            pathItemType = 2;
                        if (wfItem is LinkedWorkflowItem)
                            pathItemType = 3;
                        if (wfItem is ManualWorkflowItem)
                            pathItemType = 4;

                        workflowVersionPath.AddItem(itemId, workflow.Id, nrVersion, pathId, wfItem.Title, pathItemType, actionType, wfItem.CatalogId,
                            wfItem.TypicalId, wfItem.TypicalName == null ? "" : wfItem.TypicalName, wfItem.TypicalType == null ? "0" : wfItem.TypicalType,
                            Guid.Empty, workflow.WorkflowPathsJson, tsIndex);

                    }
                    catch (Exception ex)
                    {

                    }
                    tsIndex++;
                }
                list.Add(workflowVersionPath);
                tcIndex++;
            }
            return list;
        }

        private void NewLinkedVersion(IssueWorkflow command, int newVersionId)
        {
            var workflowIds = _dbContext.Set<WorkflowItemData>().AsNoTracking().Where(w => w.WorkflowLinkId == command.WorkflowId &&
                       w.WorkflowLinkVersionId == command.CurrentVersionId).Select(s => s.WorkflowId).ToList();
            if (command.IsRollBack)
                workflowIds = _dbContext.Set<WorkflowItemData>().AsNoTracking().Where(w => w.WorkflowLinkId == command.WorkflowId).Select(s => s.WorkflowId).ToList();
            
            var workflowVersions = _dbContext.Set<WorkflowVersionData>().AsNoTracking().Where(w => workflowIds.Contains(w.WorkflowId) && w.IsLast);
            foreach (var workflowVersion in workflowVersions)
            {
                var jsonObject = JToken.Parse(workflowVersion.DesignerJson);

                foreach (var item in jsonObject.SelectToken("cells").Children())
                {
                    var customData = item.SelectToken("customData.WorkflowId");
                    if (customData != null && customData.ToString() == command.WorkflowId.ToString())
                    {
                        var newProperty = new JProperty("versionId", newVersionId);
                        item["customData"]["versionId"] = newVersionId;
                    }

                }
                string json =  jsonObject.ToString();

                var workflow = WorkflowFactory.Create(workflowVersion.WorkflowId, json);
                _workflowRepository.Save(workflow, json, workflowVersion.WorkflowPathsJson);
                _workflowRepository.Issue(workflow.Id, workflowVersion.WorkflowImage);

                foreach (LinkedWorkflowItem lkWorkflow in workflow.Items.Where(ct => ct is LinkedWorkflowItem))
                {
                    var innerWorkflow = _workflowRepository.GetWorkflowByVersion(lkWorkflow.WorkflowId, lkWorkflow.WorkflowVersionId);

                    innerWorkflow.SetParentIds(lkWorkflow.Id);
                    innerWorkflow.SetUniqueIds(lkWorkflow.Id.ToString());
                    lkWorkflow.SetUniqueId(Guid.Empty.ToString());
                    AppendRecursivelyLinkWorkflow(innerWorkflow, lkWorkflow.Id.ToString());

                    workflow.AppendLinkWorkflow(lkWorkflow, innerWorkflow, lkWorkflow.Id.ToString());
                    workflow.AddItems(innerWorkflow.Items.ToList());
                    workflow.AddLinks(innerWorkflow.ItemLinks.ToList());
                }

                var prevPaths = _pathsRepository.Get(workflowVersion.WorkflowId, workflowVersion.VersionId);//prev items
                var paths = _generatePaths.Generate(workflow);

                WorkflowPathDesinger workflowPathDesinger = new WorkflowPathDesinger
                {
                    WorkflowPaths = paths.Select(s => new WorkflowPath
                    {
                        PathId = s.Id,
                        ItemIds = s.ItemIds,
                        Coverage = s.Coverage.ToString() == "0" ? "N/A" : s.Coverage.ToString(),
                        IsBestPath = s.IsBestPath,
                        Selected = true,
                        Title = s.Title


                    }).ToArray()
                };
                workflow.SetWorkflowPathsJson(JsonConvert.SerializeObject(workflowPathDesinger));
                var currentPaths = GetWorkflowPaths(workflow, workflowVersion.VersionId + 1, workflowPathDesinger);//current items
                List<WorkflowVersionPathDetails> versionPathDetails = new List<WorkflowVersionPathDetails>();
                prevPaths.ForEach(f => { versionPathDetails.AddRange(f.Items.ToList()); });

                currentPaths.ForEach(f =>
                {
                    f.Items.ToList().ForEach(f1 =>
                    {
                        var item = versionPathDetails.FirstOrDefault(w => w.Id == f1.Id);
                        if (item != null)
                        {
                            f1.SetDataSetId(item.DataSetId);
                        }
                    });
                });

                _pathsRepository.Add(currentPaths.ToArray());
                currentPaths.ForEach(f =>
                {
                    _generateCommand.Handle(new GenerateTestCases
                    {
                        TenantId = command.TenantId,
                        ProjectId = command.ProjectId,
                        SubProjectId = command.SubProjectId,
                        CatalogId = command.CatalogId,
                        SystemId = command.SystemId,
                        WorkflowId = command.WorkflowId,
                        WorkflowPathId = f.Id,
                        SkipStatus = true
                    });
                });

            }
        }
    }
}