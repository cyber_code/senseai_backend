﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Core.Extensions;
using SenseAI.Domain.WorkflowModel;
using External.QualitySuite.ApiModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Commands.DesignModule.QualitySuite;
using SenseAI.Domain.TestCaseModel;

namespace Application.Commands.DesignModule.QualitySuite
{
    internal class QualitySuiteTestCaseFactory
    {
        #region [ External Fn ]

        private static Func<Guid, List<TypicalInstance>> FnGetDataPool { get; set; }

        private static Func<string, int> FnGetTypicalType { get; set; }

        #endregion [ External Fn ]

        #region [ Class Variables ]

        private readonly Dictionary<string, KeyValuePair<string, External.QualitySuite.ApiModel.TestStep>> _testStepsIdMapps;
        private readonly External.QualitySuite.ApiModel.TestCase _atsTestCase;
        private readonly List<WorkflowTestCases> _testSteps;
        private readonly List<ValidataItem> _systems;
        private readonly List<ValidataItem> _adapters;
        private readonly ATSTestCases _atsTC;
        private readonly List<ATSTestCases> _lstAtsTCs;
        private readonly Dictionary<string, List<ValidataItem>> _adaptersPerSystem;
        private readonly List<Catalogs> _mapedCatalogs;
        private readonly List<Systems> _mapedSystems;
        private readonly string _testCaseName;
        private Dictionary<int, TestStepJson> _generatedTestSteps;
        private ATSTestSteps _atsTestStep;

        #endregion [ Class Variables ]

        #region [ Consts ]

        public const string attrRECORD_STATUS = "RECORD.STATUS";
        public const string attrERROR_MESSAGE = "Error Message";

        #endregion [ Consts ]

        #region [ Ctor ]

        private QualitySuiteTestCaseFactory(List<ValidataItem> systems, List<ValidataItem> adapters,
             Dictionary<string, List<ValidataItem>> atsAdaptersPerSystem, List<WorkflowTestCases> testSteps, List<Catalogs> mapedCatalogs,
             List<Systems> mapedSystems, Dictionary<string, KeyValuePair<string, External.QualitySuite.ApiModel.TestStep>> testStepsIdMapps,
            ATSTestCases atsTC, List<ATSTestCases> atsTestCases)
        {
            _systems = systems;
            _adapters = adapters;
            _adaptersPerSystem = atsAdaptersPerSystem;
            _testCaseName = testSteps.FirstOrDefault().TestCaseTitle;
            _testSteps = testSteps;
            _mapedCatalogs = mapedCatalogs;
            _mapedSystems = mapedSystems;
            _testStepsIdMapps = testStepsIdMapps;
            _atsTC = atsTC;
            _lstAtsTCs = atsTestCases;
            _atsTestCase = new External.QualitySuite.ApiModel.TestCase()
            {
                Name = _testCaseName,
                Noderef = _atsTC == null ? null : _atsTC.Noderef.ToString(),
                ID = _atsTC == null ? null : _atsTC.UId,
                Description = _testCaseName,
                Steps = new List<object>(),
                Origination = "SenseAI",
                DataPools = null
            };
        }

        #endregion [ Ctor ]

        #region [ Public Interface ]

        public static void RegisterDataPoolFunction(Func<Guid, List<TypicalInstance>> fnGetDataPool)
        {
            FnGetDataPool = fnGetDataPool;
        }

        public static void RegisterGetTypicalType(Func<string, int> fnGetTypicalType)
        {
            FnGetTypicalType = fnGetTypicalType;
        }

        /// <summary>
        ///     Factory method for Quality Suite scripted test case
        /// </summary>
        /// <param name="systems">The s/project list of systems </param>
        /// <param name="testSteps">The list of genenrated test steps</param>
        /// <param name="mapedCatalogs">list of mapped catalogs</param>
        /// <param name="mapedSystems">list of mapped systems</param>
        /// <param name="exportTime">Export time</param>
        /// <param name="fnGetDataPool">Functions that returns the step datapool</param>
        /// <returns></returns>
        public static External.QualitySuite.ApiModel.TestCase TestCaseFactory(List<ValidataItem> systems, List<ValidataItem> adapters, Dictionary<string, List<ValidataItem>> atsAdaptersPerSystem, List<WorkflowTestCases> testSteps,
              List<Catalogs> mapedCatalogs, List<Systems> mapedSystems, Dictionary<string, KeyValuePair<string, External.QualitySuite.ApiModel.TestStep>> testStepsIdMapps, ATSTestCases atsTC, List<ATSTestCases> atsTestCases)
        {
            return new QualitySuiteTestCaseFactory(systems, adapters, atsAdaptersPerSystem, testSteps, mapedCatalogs, mapedSystems, testStepsIdMapps, atsTC, atsTestCases).GenerateScriptedTestCase();
        }

        public static Dictionary<string, KeyValuePair<string, External.QualitySuite.ApiModel.TestStep>> GetExportMapping(External.QualitySuite.ApiModel.TestCase exportedTestCases,
                                                                                                                         External.QualitySuite.ApiModel.TestCase scriptedTrestCases, Guid testCaseId)
        {
            var testStepsIdMapps = new Dictionary<string, KeyValuePair<string, External.QualitySuite.ApiModel.TestStep>>();

            exportedTestCases.Steps.ForEach(exportedTestStep =>
            {
                var exportedStep = ((External.QualitySuite.ApiModel.TestStep)exportedTestStep);
                var scriptedStep = (External.QualitySuite.ApiModel.TestStep)scriptedTrestCases.Steps.FirstOrDefault(scriptedTestStep => ((External.QualitySuite.ApiModel.TestStep)scriptedTestStep).StepIndex == exportedStep.StepIndex);

                testStepsIdMapps.Add(testCaseId.ToString() +  scriptedStep.UID, new KeyValuePair<string, External.QualitySuite.ApiModel.TestStep>(exportedStep.UID, exportedStep));
            });

            return testStepsIdMapps;
        }

        #endregion [ Public Interface ]

        #region [ Parser ]

        /// <summary>
        /// Genetarte the scripted tast case
        /// </summary>
        /// <returns>Quality Suite scripted test case object</returns>
        private External.QualitySuite.ApiModel.TestCase GenerateScriptedTestCase()
        {
            _generatedTestSteps = new Dictionary<int, TestStepJson>();

            foreach (var testStep in _testSteps)
            {
                Debug.WriteLine(testStep.TestStepJson);
                var json = JObject.Parse(testStep.TestStepJson);
                var testStepJson = JsonConvert.DeserializeObject<TestStepJson>(json.ToString());
                int typicalType = FnGetTypicalType(testStepJson.CatalogName);
                testStepJson.CatalogName = _mapedCatalogs.FirstOrDefault(ct => ct.From == testStepJson.CatalogName).To;
                testStepJson.SystemName = _mapedSystems.FirstOrDefault(ct => ct.From == testStepJson.SystemName).To;
                _generatedTestSteps.Add(testStep.TSIndex, testStepJson);

                if (_atsTC != null)
                    _atsTestStep = _atsTC.Steps.FirstOrDefault(w => w.TestStepId == testStep.TestStepId);

                var atsTestStep = TestStepFactory(testStep.TSIndex, _generatedTestSteps[testStep.TSIndex], typicalType);
                if (_atsTestStep != null)
                {
                    atsTestStep.Noderef = _atsTestStep.Noderef;
                    atsTestStep.UID = _atsTestStep.UId;
                }
                var system = _systems.FirstOrDefault(gts => gts.Name.Equals(atsTestStep.SystemId));
                if (system == null)
                    throw new Exception(string.Format("System with name: '{0}' does not exist on ATS.", atsTestStep.SystemId));
                var adapter = _adapters.FirstOrDefault(gts => gts.Name.Equals(atsTestStep.Adapter));
                if (adapter == null)
                    throw new Exception(string.Format("Adapter with name: '{0}' does not exist on ATS.", atsTestStep.Adapter));
                var adaptersForThisSystem = _adaptersPerSystem.FirstOrDefault(x => x.Key.Equals(system.Noderef)).Value;
                if (adaptersForThisSystem == null)
                    throw new Exception(string.Format("System with name: '{0}' does not have any adapter associated.", atsTestStep.SystemId));
                var existAdapterInThisSystem = adaptersForThisSystem.FirstOrDefault(gts => gts.Name.Equals(atsTestStep.Adapter));
                if (existAdapterInThisSystem == null)
                    throw new Exception(string.Format("Adapter with name: '{0}' does not exist on system {1}.", atsTestStep.Adapter, atsTestStep.SystemId));
                atsTestStep.SystemId = system?.Noderef;
                //int typicalType = FnGetTypicalType(testStepJson.CatalogName);
                if (typicalType == 3 && testStepJson.Action == SenseAI.Domain.ActionType.Input)
                {
                    atsTestStep.Configuration = GetConfiguration(testStep);
                }
                _atsTestCase.Steps.Add(atsTestStep);
            }
            DoCartesianProductForDataPools();
            return _atsTestCase;
        }

        private string GetConfiguration(WorkflowTestCases testStep)
        {
            WorkflowTestCases generatedTest = null;
            string configuration = "I";

            var previousItems = _testSteps.Where(w => w.TSIndex < testStep.TSIndex).ToList().OrderByDescending(o => o.TSIndex).ToList();
            previousItems.ForEach(w =>
            {
                if (generatedTest != null)
                    return;
                var item = JsonConvert.DeserializeObject<TestStepJson>(w.TestStepJson.ToString());
                if (item.Action == SenseAI.Domain.ActionType.Input || item.Action == SenseAI.Domain.ActionType.See
                 || item.Action == SenseAI.Domain.ActionType.Authorize || item.Action == SenseAI.Domain.ActionType.Reverse
                  || item.Action == SenseAI.Domain.ActionType.Delete || item.Action == SenseAI.Domain.ActionType.Verify)
                {
                    generatedTest = w;
                }

            });
            int from = generatedTest == null ? 0 : generatedTest.TSIndex;

            var tsMenuNavigation = _testSteps.FirstOrDefault(w => (w.TSIndex < testStep.TSIndex && w.TSIndex >= from) && w.TypicalName.ToLower() == "navigationinfo");

            TestStepJson testStepJson = null;
            if (tsMenuNavigation != null)
            {
                testStepJson = JsonConvert.DeserializeObject<TestStepJson>(tsMenuNavigation.TestStepJson.ToString());
                if (testStepJson != null && testStepJson.Action == SenseAI.Domain.ActionType.Menu)
                    configuration = "AA.NEW,I";
            }
            else
            {
                var tsEnq = _testSteps.FirstOrDefault(w => (w.TSIndex == testStep.TSIndex - 1));
                if (tsEnq != null)
                {
                    testStepJson = JsonConvert.DeserializeObject<TestStepJson>(tsEnq.TestStepJson.ToString());
                    if (testStepJson != null && (testStepJson.Action == SenseAI.Domain.ActionType.EnquiryAction ||
                        testStepJson.Action == SenseAI.Domain.ActionType.EnquiryResult))
                        configuration = "AA.NEW,I";
                }
            }


            return configuration;
        }


        #region [ TestStep ]
        internal class DynamicDataComparer : IEqualityComparer<DynamicData>
        {
            public bool Equals(DynamicData x, DynamicData y)
            {
                string xFormula = Encoding.UTF8.GetString(x.PlainTextFormula, 0, x.PlainTextFormula.Length);
                string yFormula = Encoding.UTF8.GetString(x.PlainTextFormula, 0, x.PlainTextFormula.Length);
                if (x.TestStepId == y.TestStepId &&
                    x.SourceAttributeName == y.SourceAttributeName &&
                    x.TargetAttributeName == y.TargetAttributeName &&
                    xFormula == yFormula)
                {
                    return true;
                }
                return false;
            }

            public int GetHashCode(DynamicData obj)
            {
                return (obj.TestStepId + obj.SourceAttributeName + obj.TargetAttributeName + Encoding.UTF8.GetString(obj.PlainTextFormula, 0, obj.PlainTextFormula.Length)).GetHashCode();
            }
        }

        private External.QualitySuite.ApiModel.TestStep TestStepFactory(int stepIndex, TestStepJson generatedTestStep, int typicalType)
        {
            var testStepLabel = GetTestStepLabel(stepIndex, generatedTestStep);

            var testStepParameter = GetTestStepParameter(generatedTestStep, typicalType);

            List<DynamicData> dynamicDatas = generatedTestStep.DynamicDatas.Distinct(new DynamicDataComparer()).ToList();

            return new External.QualitySuite.ApiModel.TestStep
            {
                Noderef = 0,
                UID = generatedTestStep.Id,
                Name = generatedTestStep.Title,
                Label = testStepLabel,
                Description = generatedTestStep.Description,
                Configuration = testStepParameter,
                SystemId = string.IsNullOrEmpty(generatedTestStep.SystemName)
                          ? "SenseAI Input System"
                          : generatedTestStep.SystemName,
                Adapter = generatedTestStep.AdapterName,
                ResultType = GetResultType(generatedTestStep, typicalType),
                StepIndex = stepIndex,
                ManualOrAuto = generatedTestStep.Type == WorkflowType.Manual ? EnumsExecutionType.Manual : EnumsExecutionType.Automatic,
                Priority = EnumsTestStepPriority.Medium,
                LogicalDay = 1,
                Filters = FiltersFactory(generatedTestStep),
                ExpectedResult = string.Empty,
                ExternalRef = string.Empty,
                UserInterface = typicalType == 3 || generatedTestStep.Type == WorkflowType.Manual || generatedTestStep.TypicalName.ToLower().StartsWith("v_")
                                ? TestStepUserInterfaceType.Classic
                                : TestStepUserInterfaceType.T24TestBuilder,
                Type = GetTestStepType(generatedTestStep),
                ExpectedFail = generatedTestStep.IsNegativeStep && string.IsNullOrEmpty(generatedTestStep.ExpectedError),
                ExpectedResultRecord = ExpectedResultRecord(generatedTestStep),
                AttributesWithCustomCurrencyComparison = new List<string>(),
                AutomaticCalculations = AutomaticCalculationFactory(dynamicDatas),
                EndCaseOnError = 0,

                TestData = TestDataFactory(generatedTestStep, testStepLabel),
                TransactionExists = (generatedTestStep.Action == SenseAI.Domain.ActionType.See)
            };
        }

        private string GetTestStepLabel(int stepIndex, TestStepJson generatedTestStep)
        {
            return generatedTestStep.DataSetId != null && generatedTestStep.DataSetId != Guid.Empty ? $"Step {stepIndex} label" : null;
        }

        private string GetTestStepParameter(TestStepJson generatedTestStep, int catalogType)
        {
            var parameters = new List<string>();

            if (catalogType == 4 && !(generatedTestStep.Action == SenseAI.Domain.ActionType.See || generatedTestStep.Action == SenseAI.Domain.ActionType.EnquiryResult) &&
                !generatedTestStep.TypicalName.StartsWith("V_"))
            {
                string[] splitedParams = generatedTestStep.Parameters.Split(":");
                if (splitedParams.Length == 1)
                {
                    if (splitedParams[0].Contains("CLICK"))
                    {
                        return $"{splitedParams[0].Trim()}";
                    }
                    return $"CLICK(\"{splitedParams[0].Trim()}\")";
                }
                switch (splitedParams[0].Trim())
                {
                    case "label":
                        return $"CLICK(\"{splitedParams[1].Trim()}\")";

                    case "dropDown":
                        return $"CLICK(\"Select Drilldown\") > {splitedParams[1].Trim()}";

                    case "button":
                        return $"CLICK(\"{splitedParams[1].Trim()}\")";
                }

            }

            if (catalogType == 3)
            {
                if (generatedTestStep.Action == SenseAI.Domain.ActionType.Input)
                    parameters.Add("AA.NEW");
                else
                    parameters.Add("AA");
            }

            if (!string.IsNullOrEmpty(generatedTestStep.TypicalName) && generatedTestStep.TypicalName.Contains(","))
                parameters.Add(generatedTestStep.TypicalName.Split(',').Last());



            switch (generatedTestStep.Action)
            {
                case SenseAI.Domain.ActionType.Menu:
                    parameters.Add(",MNU,,,,,,");
                    break;

                case SenseAI.Domain.ActionType.See:
                    parameters.Add("S");
                    break;

                case SenseAI.Domain.ActionType.Delete:
                    parameters.Add("D");
                    break;

                case SenseAI.Domain.ActionType.Authorize:
                    parameters.Add("A");
                    break;

                case SenseAI.Domain.ActionType.Reverse:
                    parameters.Add("R");
                    break;

                case SenseAI.Domain.ActionType.Tab:
                    parameters.Add(",TAB,,,,,,");
                    break;

                case SenseAI.Domain.ActionType.Input:
                    parameters.Add("I");
                    break;

                case SenseAI.Domain.ActionType.Verify:
                    parameters.Add("V");
                    break;

                case SenseAI.Domain.ActionType.SingOff:
                case SenseAI.Domain.ActionType.EnquiryAction:
                case SenseAI.Domain.ActionType.EnquiryResult:

                    break;
            }
            if (generatedTestStep.Type == WorkflowType.Manual)
            {
                parameters.Clear();
            }
            if (parameters.Count > 1)
                return string.Join(",", parameters.ToArray());

            if ((generatedTestStep.Action == SenseAI.Domain.ActionType.EnquiryAction || generatedTestStep.Action == SenseAI.Domain.ActionType.EnquiryResult)
               && generatedTestStep.TypicalName.ToLower().StartsWith("v_"))
            {
                string[] prm = generatedTestStep.Parameters.Split(':');
                if (prm.Length > 1)
                    return prm[1];
            }

            parameters.Add(generatedTestStep.Parameters);

            return string.Join("", parameters.ToArray());
        }

        private string GetResultType(TestStepJson generatedTestStep, int typicalType)
        {
            if (typicalType == 4)
            {
                string typicalName = generatedTestStep.TypicalName;
                if (!typicalName.ToLower().EndsWith(".result"))
                    typicalName = typicalName + ".RESULT";
                return $"[{generatedTestStep.CatalogName}]:{typicalName}";
            }

            return "";
        }

        private string GetTestStepTypical(TestStepJson generatedTestStep)
        {
            if (!string.IsNullOrEmpty(generatedTestStep.TypicalName) && generatedTestStep.TypicalName.Contains(","))
            {
                return generatedTestStep.TypicalName.Split(',').First();
            }
            if (generatedTestStep.Action == SenseAI.Domain.ActionType.EnquiryAction || generatedTestStep.Action == SenseAI.Domain.ActionType.EnquiryResult)
            {
                string typicalName = generatedTestStep.TypicalName;
                if (!typicalName.ToLower().EndsWith(".result"))
                    typicalName = typicalName + ".RESULT";
                return typicalName;
            }
            return generatedTestStep.TypicalName;
        }

        private EnumsTestStepType GetTestStepType(TestStepJson generatedTestStep)
        {
            if (generatedTestStep.IsNegativeStep)
                return EnumsTestStepType.Check;

            switch (generatedTestStep.Action)
            {
                case SenseAI.Domain.ActionType.EnquiryResult:
                case SenseAI.Domain.ActionType.See:
                    return EnumsTestStepType.Check;

                default:
                    return EnumsTestStepType.Action;
            }
        }

        /// <summary>
        ///     Genetarte TestStep
        /// </summary>
        /// <param name="generatedTestStep">The generated TestStep</param>
        /// <param name="poolLabel">The data pool record gernerated for the instances</param>
        /// <returns>Quality Suite list of scripted TestStep</returns>
        private ValidataRecord TestDataFactory(TestStepJson generatedTestStep, string poolLabel)
        {
            var testData = new ValidataRecord { CatalogName = generatedTestStep.CatalogName, TypicalName = GetTestStepTypical(generatedTestStep) };

            if (generatedTestStep.TypicalInstance == null)
            {
                if (generatedTestStep.DataSetId != null && generatedTestStep.DataSetId != Guid.Empty)
                {
                    var dataPoolItem = new DataPoolItem { FinancialObjects = new List<object>(), Label = poolLabel };


                    FnGetDataPool(generatedTestStep.DataSetId).ForEach(instance =>
                    {
                        var dataRecord = new ValidataRecord
                        {
                            CatalogName = testData.CatalogName,
                            TypicalName = testData.TypicalName
                        };
                        instance.Attributes.ForEach(attribute => dataRecord.AddAttribute(attribute.Name, attribute.Value));

                        dataPoolItem.FinancialObjects.Add(dataRecord);
                    });


                    if (_atsTestCase.DataPools == null)
                        _atsTestCase.DataPools = new List<object>() { new DataPool { Name = $"Data Pool{_testCaseName.Split("TC").Last()}", DataPoolItems = new List<object>() } };

                    ((DataPool)_atsTestCase.DataPools.First()).DataPoolItems.Add(dataPoolItem);
                }
            }
            else
            {
                if (generatedTestStep.Type != SenseAI.Domain.WorkflowModel.WorkflowType.Manual && generatedTestStep.Action == SenseAI.Domain.ActionType.Menu && !generatedTestStep.TypicalInstance.Attributes.Any())
                {
                    testData.AddAttribute("CosScreen", "T24.MAIN.MENU");
                }
                if (generatedTestStep.Type == SenseAI.Domain.WorkflowModel.WorkflowType.Manual && !generatedTestStep.TypicalInstance.Attributes.Any())
                {
                    testData.AddAttribute("ID", "No Value");
                }

                foreach (var attribute in generatedTestStep.TypicalInstance.Attributes)
                {
                    testData.AddAttribute(attribute.Name, attribute.Value);
                }
            }

            return testData;
        }

        private ValidataRecord ExpectedResultRecord(TestStepJson generatedTestStep)
        {
            if (generatedTestStep.IsNegativeStep && !string.IsNullOrEmpty(generatedTestStep.ExpectedError))
            {
                var expectedResultRecord = new ValidataRecord
                {
                    CatalogName = "NavigatorCatalog",
                    TypicalName = "AppError"
                };

                expectedResultRecord.AddAttribute(attrERROR_MESSAGE, generatedTestStep.ExpectedError);

                return expectedResultRecord;
            }

            return null;
        }

        /// <summary>
        ///     Genetartes a list of Filters
        /// </summary>
        /// <param name="generatedTestStep">The generated TestStep</param>
        /// <returns>Quality Suite list of scripted Filters</returns>
        private List<object> FiltersFactory(TestStepJson generatedTestStep)
        {
            List<object> filters = new List<object>();

            generatedTestStep.Filters?.Where(f => !string.IsNullOrEmpty(f.OperatorName))
                .ToList()
                .ForEach(generatedFilter =>
                {
                    var filter = new External.QualitySuite.ApiModel.Filter()
                    {
                        AttributeName = generatedFilter.AttributeName,
                        Value = generatedFilter.AttributeValue,
                        Type = (External.QualitySuite.ApiModel.FilterType)Enum.Parse(typeof(External.QualitySuite.ApiModel.FilterType), generatedFilter.OperatorName),
                        IsMask = false,
                        IsPostFilter = false,
                        AutomaticCalculations = AutomaticCalculationFactory(new List<DynamicData>() { generatedFilter.DynamicData })
                    };
                    if (_atsTestStep != null && _atsTestStep.Filters != null)
                    {
                        var existingFilter = _atsTestStep.Filters.FirstOrDefault(w => w.Id ==
                        filter.AttributeName.ToLower() + filter.Value.ToLower());

                    }
                    filters.Add(filter);
                });

            generatedTestStep.PostFilters?.Where(f => !string.IsNullOrEmpty(f.OperatorName))
                .ToList()
                .ForEach(generatedFilter =>
                {
                    if (!(generatedFilter.DynamicData == null && generatedFilter.AttributeValue == null))
                    {
                        var postFilter = new External.QualitySuite.ApiModel.Filter()
                        {
                            AttributeName = generatedFilter.AttributeName,
                            Value = generatedFilter.AttributeValue,
                            Type = (External.QualitySuite.ApiModel.FilterType)Enum.Parse(typeof(External.QualitySuite.ApiModel.FilterType), generatedFilter.OperatorName),
                            IsMask = false,
                            IsPostFilter = true,
                            AutomaticCalculations = AutomaticCalculationFactory(new List<DynamicData>() { generatedFilter.DynamicData })
                        };


                        if (_atsTestStep != null && _atsTestStep.PostFilters != null)
                        {
                            var existingPostFilter = _atsTestStep.PostFilters.FirstOrDefault(w => w.Id ==
                            postFilter.AttributeName.ToLower() + postFilter.Value.ToLower());

                        }
                        filters.Add(postFilter);

                    }
                });

            return filters;
        }

        #region [ Automatic Calculation ]

        private List<object> AutomaticCalculationFactory(List<DynamicData> generatedAutomaticCalculations)
        {
            var automaticCalculations = new List<object>();
            try
            {
                if (generatedAutomaticCalculations != null)
                {
                    generatedAutomaticCalculations.ToList().ForEach(automaticCalculation =>
                    {
                        if (automaticCalculation != null)
                        {
                            var scriptedAutomaticCalculation = new AutomaticCalculation
                            {
                                Noderef = 0,
                                AttributeName = automaticCalculation.TargetAttributeName,
                                PlainTextFormula = automaticCalculation.TestStepId.IsNullOrDefault()
                                                ? Encoding.UTF8.GetString(automaticCalculation.PlainTextFormula, 0, automaticCalculation.PlainTextFormula.Length)
                                                : string.Empty,
                                Items = automaticCalculation.TestStepId.IsNullOrDefault()
                                       ? new List<object>()
                                       : AutomaticCalculationItemFactory(automaticCalculation)
                            };

                            if (!automaticCalculation.TestStepId.IsNullOrDefault() && scriptedAutomaticCalculation.Items.FirstOrDefault() != null)
                                scriptedAutomaticCalculation.PlainTextFormula = ((AutomaticCalculationItem)scriptedAutomaticCalculation.Items.First()).VariableName;

                            automaticCalculations.Add(scriptedAutomaticCalculation);
                            if (_atsTestStep != null && _atsTestStep.DynamicDatas != null)
                            {
                                var existingDD = _atsTestStep.DynamicDatas.FirstOrDefault(w => w.Id ==
                                automaticCalculation.SourceAttributeName.ToLower() + automaticCalculation.TargetAttributeName.ToLower() + scriptedAutomaticCalculation.PlainTextFormula.ToLower());
                                if (existingDD != null)
                                    scriptedAutomaticCalculation.Noderef = existingDD.Noderef;
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                //
            }
            return automaticCalculations;
        }

        /// <summary>
        ///     Genetarte AC reference item
        /// </summary>
        /// <param name="generatedAutomaticCalculationItem">The generated AC item</param>
        /// <returns>Quality Suite list of scripted AC reference items</returns>
        private List<object> AutomaticCalculationItemFactory(DynamicData generatedAutomaticCalculationItem)
        {
            List<object> result = new List<object>();

            if (generatedAutomaticCalculationItem != null)
            {
                if (_testSteps.Any(items => items.TestStepId == generatedAutomaticCalculationItem.TestStepId))
                {
                    var testStep = _testSteps.First(items => items.TestStepId == generatedAutomaticCalculationItem.TestStepId);

                    var acItem = new AutomaticCalculationItem
                    {
                        CatalogName = _generatedTestSteps[testStep.TSIndex].CatalogName,
                        TypicalName = GetTestStepTypical(_generatedTestSteps[testStep.TSIndex]),
                        AttributeName = generatedAutomaticCalculationItem.SourceAttributeName,
                        CollectFrom = CollectionSource.Response,
                        VariableName = $"r_{generatedAutomaticCalculationItem.SourceAttributeName.Replace("@", "_").Replace("{", "_").Replace("}", "_").Replace(".", "_").Replace(" ", "_").Replace("-", "_").Replace("~", "_")}",
                        SourceTestStepID = testStep.TestStepId.ToString()
                    };

                    var atsStep = _atsTC?.Steps?.FirstOrDefault(w => w.TestStepId == testStep.TestStepId);
                    if (_atsTC != null && atsStep != null)
                        acItem.SourceTestStepID = atsStep.UId;

                    result.Add(acItem);
                }
                else if (_testStepsIdMapps.Any(item => item.Key.EndsWith(generatedAutomaticCalculationItem.TestStepId.ToString())) || _lstAtsTCs.Any(a => a.Steps.Any(a1 => a1.TestStepId == generatedAutomaticCalculationItem.TestStepId)))
                {
                    string noderef = "0";
                    _lstAtsTCs.ForEach(f =>
                    {
                        var step = f.Steps.FirstOrDefault(w => w.TestStepId == generatedAutomaticCalculationItem.TestStepId);
                        if (step != null)
                        {
                            noderef = step.UId;
                            return;
                        }
                    });
                    var item = _testStepsIdMapps.FirstOrDefault(items => items.Key.EndsWith(generatedAutomaticCalculationItem.TestStepId.ToString()));
                    if (item.IsNullOrDefault() && noderef != "0")
                        item = _testStepsIdMapps.FirstOrDefault(items => items.Key.EndsWith(noderef));
                    var generatedTestStep = item.Value.Value;

                    result.Add(new AutomaticCalculationItem
                    {
                        CatalogName = ((ValidataRecord)generatedTestStep.TestData).CatalogName,
                        TypicalName = ((ValidataRecord)generatedTestStep.TestData).TypicalName,
                        AttributeName = generatedAutomaticCalculationItem.SourceAttributeName,
                        CollectFrom = CollectionSource.Response,
                        VariableName = $"r_{generatedAutomaticCalculationItem.SourceAttributeName.Replace("@", "_").Replace("{", "_").Replace("}", "_").Replace(".", "_").Replace(" ", "_").Replace("-", "_").Replace("~", "_")}",
                        SourceTestStepID = generatedTestStep.UID
                    });
                }
                else
                {
                    throw new NullReferenceException($"DataModel error, the referenced test step with id '{generatedAutomaticCalculationItem.TestStepId}' is not present in test case!");
                }
            }

            return result;
        }

        #endregion [ Automatic Calculation ]

        #endregion [ TestStep ]

        #region [ Data Variations ]

        private void DoCartesianProductForDataPools()
        {
            var count = 1;

            if (_atsTestCase.DataPools != null)
            {
                var dataPool = ((DataPool)_atsTestCase.DataPools.First());
                if (_atsTC != null)
                    dataPool.Noderef = _atsTC.DataSetNoderef;
                dataPool.DataPoolItems.ForEach(datapoolItem =>
            {
                var dataPoolItemCount = ((DataPoolItem)datapoolItem).FinancialObjects.Count;

                if (dataPoolItemCount > 0)
                {
                    count *= ((DataPoolItem)datapoolItem).FinancialObjects.Count;
                }
            });

                dataPool.DataPoolItems.ForEach(datapoolItem =>
                {
                    var datapoolItemCasted = ((DataPoolItem)datapoolItem);
                    var originalCollection = datapoolItemCasted.FinancialObjects.Select(fo => CloneTypicalInstance((ValidataRecord)fo)).ToList();

                    if (originalCollection.Count > 0)
                    {
                        var noOfCycles = count / originalCollection.Count - 1;

                        for (int i = 0; i < noOfCycles; i++)
                        {
                            for (var j = 0; j < originalCollection.Count; j++)
                                datapoolItemCasted.FinancialObjects.Add(CloneTypicalInstance(originalCollection[j]));
                        }
                    }
                });
            }
        }

        private ValidataRecord CloneTypicalInstance(ValidataRecord instance)
        {
            var dataRecord = new ValidataRecord
            {
                CatalogName = instance.CatalogName,
                TypicalName = instance.TypicalName
            };

            foreach (var attribute in instance.Attributes)
            {
                dataRecord.AddAttribute(attribute.Name, attribute.Value);
            }

            return dataRecord;
        }

        #endregion [ Data Variations ]

        #endregion [ Parser ]
    }
}