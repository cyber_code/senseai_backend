﻿using External.QualitySuite;
using External.QualitySuite.ApiModel;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Persistence.AdminModel;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using Commands.DesignModule.QualitySuite;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;
using SenseAI.Domain;
using SenseAI.Domain.TestCaseModel;
using Persistence.TestCaseModel;
using TestCase = External.QualitySuite.ApiModel.TestCase;

namespace Application.Commands.DesignModule.QualitySuite
{
    public sealed class ExportWorkflowToQualitySuiteHandler : ICommandHandler<ExportWorkflowToQualitySuite, ExportWorkflowToQualitySuiteResult>
    {
        private readonly IQualitySuiteClient _service;
        private readonly IDbContext _dbContext;
        private readonly Credentials _credentials;
        IExportedTestCaseRepository _exportedTestCaseRepository;
        IATSTestCasesRepository _atsTestCasesRepository;
        IWorkflowExportedTestCases _workflowExportedTestCases;
        int versionId = 0;
        private List<ATSTestCases> _atsTestCases = new List<ATSTestCases>();

        public ExportWorkflowToQualitySuiteHandler(IQualitySuiteClient service,
                                                 IDbContext dbContext,
                                                 IExportedTestCaseRepository exportedTestCaseRepository,
                                                 IWorkflowExportedTestCases workflowExportedTestCases,
                                                 IATSTestCasesRepository atsTestCasesRepository)
        {
            _dbContext = dbContext;
            _service = service;
            _exportedTestCaseRepository = exportedTestCaseRepository;
            _workflowExportedTestCases = workflowExportedTestCases;
            _atsTestCasesRepository = atsTestCasesRepository;

            _credentials = new Credentials()
            {
                Password = service.BindingConfiguration.Password,
                DisciplineNoderef = service.BindingConfiguration.DisciplineNoderef,
                RoleNoderef = service.BindingConfiguration.RoleNoderef,
                LicenseType = service.BindingConfiguration.LicenseType,
                User = service.BindingConfiguration.User
            };

            QualitySuiteTestCaseFactory.RegisterDataPoolFunction(GetDataPool);
            QualitySuiteTestCaseFactory.RegisterGetTypicalType(GetTypicalTypeByCatalogName);
        }

        public List<string> Errors { get; set; }

        public CommandResponse<ExportWorkflowToQualitySuiteResult> Handle(ExportWorkflowToQualitySuite command)
        {
            var handlerResponse = new ExportWorkflowToQualitySuiteResult(0, 0, new List<string>());
            var testStepsIdMapps = new Dictionary<string, KeyValuePair<string, External.QualitySuite.ApiModel.TestStep>>();
            try
            {
                // get data from db
                var atsHeader = GetAtsHeader(command.WorkflowId);
                var testCases = GetGeneratedTestCases(command);

                if (testCases.Count() == 0)
                    throw new Exception(string.Format("Number of test cases for workflow id:'{0}'", command.WorkflowId));
                handlerResponse.TotlalTestCases = testCases.Count();

                var clientService = _service.CreateClient(_credentials, command.ProjectId, command.SubProjectId);
                var atsSystems = clientService.GetSystemsList();
                var atsAdapters = clientService.GetAdapters(command.SubProjectId);
                Dictionary<string, List<ValidataItem>> atsAdaptersPerSystem = clientService.GetAdaptersPerSystems(command.SubProjectId);

                var atsFolderId = clientService.GetTestFolderId($"{atsHeader.FolderTitle}", atsHeader.FolderDescription);
                var atsWorfklowFolderId = clientService.GetTestSubFolderId(atsFolderId, $"{atsHeader.WorkflowTitle}", atsHeader.WorkflowDescription);
                var atsSubFolderId = clientService.GetTestSubFolderId(atsWorfklowFolderId, $"{"Version "}{versionId}", atsHeader.FolderDescription);

                var reqirementId = clientService.SaveRequirement(atsHeader.WorkflowTitle, atsHeader.WorkflowDescription);

                List<WorkflowTestCases> senseAITestCases = new List<WorkflowTestCases>();
                List<External.QualitySuite.ApiModel.TestCase> atsTestCases = new List<External.QualitySuite.ApiModel.TestCase>();

                foreach (var testCase in testCases)
                {
                    int index = 0;
                    var teststeps = testCase.OrderBy(ct => ct.TSIndex).ToList().Select(testCaseData =>
                    new WorkflowTestCases(
                                            testCaseData.Id,
                                            testCaseData.WorkflowId,
                                            testCaseData.VersionId,
                                            testCaseData.WorkflowPathId,
                                            testCaseData.TestCaseId,
                                            testCaseData.TestCaseTitle,
                                            testCaseData.TCIndex,
                                            testCaseData.TestStepId,
                                            testCaseData.TestStepTitle,
                                            testCaseData.TestStepJson,
                                            testCaseData.TypicalId,
                                            testCaseData.TypicalName,
                                            index++,
                                            testCaseData.TestStepType)).ToList();

                    senseAITestCases.AddRange(teststeps);

                    var atsTestCase = QualitySuiteTestCaseFactory.TestCaseFactory(atsSystems, atsAdapters, atsAdaptersPerSystem, teststeps, command.Catalogs.ToList(),
                        command.Systems.ToList(), testStepsIdMapps, _atsTestCases.FirstOrDefault(w => w.TestCaseId == testCase.Key), _atsTestCases);
                    atsTestCase.ExternalSystemId = atsHeader.WorkflowId.ToString() + "_" + versionId.ToString() + "_" + teststeps.FirstOrDefault().WorkflowPathId;

                    SetPreviousTestCases(testCase.FirstOrDefault(), atsTestCase);

                    atsTestCases.Add(atsTestCase);

                    try
                    {
                        var serviceResponse = clientService.SaveTestCase(atsSubFolderId, atsTestCase);

                        if (serviceResponse.stepsWithErrorsCount == 0 && serviceResponse.SaveTestCaseResult?.Noderef != null)
                        {
                            long testCaseId;
                            if (long.TryParse(serviceResponse.SaveTestCaseResult.ID, out testCaseId))
                            {
                                _exportedTestCaseRepository.StoreExportedTestCaseReference(atsHeader.WorkflowId, testCaseId);
                            }

                            handlerResponse.SuccessfullySaved++;
                            var mappedIds = QualitySuiteTestCaseFactory.GetExportMapping(serviceResponse.SaveTestCaseResult, atsTestCase, testCase.Key);

                            testStepsIdMapps = testStepsIdMapps.Union(mappedIds).GroupBy(kvp => kvp).Select(grp => grp.First())
                                                               .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);


                            _atsTestCasesRepository.Add(GetTSTestCase(serviceResponse, testCase.ToList(), testStepsIdMapps, atsTestCase, command.ProjectId, command.SubProjectId));

                            clientService.AssociateRequirementToTestCase(reqirementId, serviceResponse.SaveTestCaseResult.Noderef);
                        }

                        clientService.GetError().ForEach(error =>
                        {
                            if (!string.IsNullOrEmpty(error.ExceptionMessage))
                            {
                                if (!handlerResponse.Errors.Contains(error.ExceptionMessage))
                                    handlerResponse.Errors.Add(error.ExceptionMessage);
                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        handlerResponse.Errors.Add(ex.Message);
                    }
                }
                SaveWorkflowExportedTestCases(senseAITestCases, atsTestCases);
            }
            catch (Exception e)
            {
                handlerResponse.Errors.Add(e.Message);
            }
            CommandResponseMessage commandResponseMessage = new CommandResponseMessage((int)MessageType.Successful, "");
            if (handlerResponse.Errors.Any())
                commandResponseMessage = new CommandResponseMessage((int)MessageType.Failed, string.Join("\n", handlerResponse.Errors.ToArray()));
            return new CommandResponse<ExportWorkflowToQualitySuiteResult>(handlerResponse, commandResponseMessage);
        }

        public bool Validate(ExportWorkflowToQualitySuite query)
        {
            if (query.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            if (query.ProjectId == 0)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (query.SubProjectId == 0)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (query.Catalogs.Count() == 0)
            {
                Errors.Add("Catalogs mapping cannot be empty.");
                return false;
            }

            if (query.Systems.Count() == 0)
            {
                Errors.Add("Systems mapping cannot be empty.");
                return false;
            }
            return true;
        }

        private List<IGrouping<Guid, WorkflowTestCases>> GetGeneratedTestCases(ExportWorkflowToQualitySuite command)
        {
            if (command.WorkflowPathId != Guid.Empty)
                versionId = _dbContext.Set<WorkflowVersionPathsData>().AsNoTracking().FirstOrDefault(w => w.Id == command.WorkflowPathId).VersionId;
            else
                versionId = _dbContext.Set<WorkflowVersionData>().AsNoTracking().FirstOrDefault(w => w.WorkflowId == command.WorkflowId && w.IsLast).VersionId;

            _atsTestCases = (from testCase in _dbContext.Set<ATSTestCasesData>().AsNoTracking()
                             where testCase.WorkflowId == command.WorkflowId && testCase.VersionId == versionId
                             select new ATSTestCases(testCase.Noderef, testCase.UId, testCase.WorkflowId, testCase.WorkflowPathId,
                             testCase.VersionId, testCase.TestCaseId, testCase.DataSetId, testCase.DataSetNoderef, command.ProjectId, command.SubProjectId)).ToList();
            if (command.WorkflowPathId != Guid.Empty)
                _atsTestCases = _atsTestCases.Where(w => w.WorkflowPathId == command.WorkflowPathId).ToList();

            _atsTestCases.ForEach(f =>
            {
                f.AddATSTestSteps(_dbContext.Set<ATSTestStepsData>().AsNoTracking().Where(w => w.TestCaseNoderef == f.Noderef).
                    Select(s => JsonConvert.DeserializeObject<ATSTestSteps>(s.TestStepJson)).ToArray());

            });

            var testCases =
             from workflowTestCase in _dbContext.Set<WorkflowTestCasesData>().AsNoTracking()
             where workflowTestCase.WorkflowId == command.WorkflowId && workflowTestCase.VersionId == versionId &&
             (workflowTestCase.TestStepType == 1 || (command.MaintainManualSteps && workflowTestCase.TestStepType == 4))
             orderby workflowTestCase.TCIndex
             select new WorkflowTestCases
                (workflowTestCase.Id, workflowTestCase.WorkflowId, workflowTestCase.VersionId, workflowTestCase.WorkflowPathId,
             workflowTestCase.TestCaseId, workflowTestCase.TestCaseTitle, workflowTestCase.TCIndex, workflowTestCase.TestStepId,
             workflowTestCase.TestStepTitle, workflowTestCase.TestStepJson, workflowTestCase.TypicalId, workflowTestCase.TypicalName,
             workflowTestCase.TSIndex, workflowTestCase.TestStepType);

            if (command.WorkflowPathId != Guid.Empty)
                testCases = testCases.Where(u => u.WorkflowPathId == command.WorkflowPathId);
            return testCases.GroupBy(ct => ct.TestCaseId).ToList(); ;
        }

        private ATSHeader GetAtsHeader(Guid workflowId)
        {
            return
                 (
                 from workflowData in _dbContext.Set<WorkflowData>().AsNoTracking()
                 join folderData in _dbContext.Set<FolderData>().AsNoTracking() on workflowData.FolderId equals folderData.Id
                 join subProjectData in _dbContext.Set<SubProjectData>().AsNoTracking() on folderData.SubProjectId equals subProjectData.Id
                 join projectData in _dbContext.Set<ProjectData>().AsNoTracking() on subProjectData.ProjectId equals projectData.Id
                 where workflowData.Id == workflowId
                 select new ATSHeader(workflowData.Id, workflowData.Title, workflowData.Description, projectData.Title, subProjectData.Title,
                            folderData.Title, folderData.Description)
                 ).FirstOrDefault();
        }

        private List<TypicalInstance> GetDataPool(Guid datasetId)
        {
            var rows = from dataSetItem in _dbContext.Set<DataSetItemData>()
                       where dataSetItem.DataSetId.Equals(datasetId)
                       select dataSetItem.Row;

            string row = "[" + string.Join(",", rows.Select(ct => "{\"Attributes\":" + ct + "}")) + "]";
            return JsonConvert.DeserializeObject<List<TypicalInstance>>(row);
        }

        private int GetTypicalTypeByCatalogName(string catalogName)
        {
            var scriptedGropus = from catalog in _dbContext.Set<Persistence.CatalogModel.CatalogData>()
                                 where catalog.Title.Equals(catalogName)
                                 select catalog.Type;

            return scriptedGropus.FirstOrDefault();
        }

        private void SaveWorkflowExportedTestCases(List<WorkflowTestCases> senseAiTestCases,
            List<External.QualitySuite.ApiModel.TestCase> atsTestCases)
        {
            if (senseAiTestCases.Count == 0 || atsTestCases.Count == 0)
                return;

            var senseAITestCasesJson = JsonConvert.SerializeObject(senseAiTestCases);
            var atsTestCasesJson = JsonConvert.SerializeObject(atsTestCases);

            WorkflowExportedTestCases tcToExport = new WorkflowExportedTestCases
            {
                WorkflowId = senseAiTestCases[0].WorkflowId,
                VersionId = 1,
                IsLast = true,
                SenseAITestCases = senseAITestCasesJson,
                AtsTestCases = atsTestCasesJson
            };

            _workflowExportedTestCases.Add(tcToExport);
        }

        private ATSTestCases GetTSTestCase(SaveTestCaseResponse serviceResponse, List<WorkflowTestCases> testCases,
                                           Dictionary<string, KeyValuePair<string, External.QualitySuite.ApiModel.TestStep>> testStepsIdMapps,
                                           External.QualitySuite.ApiModel.TestCase atsTestCase, ulong projectId, ulong subProjectId)
        {
            ATSTestCases atsTC = new ATSTestCases(ulong.Parse(serviceResponse.SaveTestCaseResult.Noderef), serviceResponse.SaveTestCaseResult.ID,
                                  testCases.FirstOrDefault().WorkflowId, testCases.FirstOrDefault().WorkflowPathId, testCases.FirstOrDefault().VersionId,
                                  testCases.FirstOrDefault().TestCaseId, Guid.NewGuid(), 0, projectId, subProjectId);

            testCases.ToList().ForEach(f =>
            {
                ATSTestSteps atsTS = null;
                if (_atsTestCases.Count() > 0 && _atsTestCases.FirstOrDefault(w => w.TestCaseId == f.TestCaseId) != null)
                    atsTS = _atsTestCases.FirstOrDefault(w => w.TestCaseId == f.TestCaseId).Steps?.FirstOrDefault(w1 => w1.TestStepId == f.TestStepId);
                var step = testStepsIdMapps.FirstOrDefault(w => w.Key == f.TestCaseId + f.TestStepId.ToString() || (atsTS != null && f.TestCaseId + atsTS.UId == w.Key)).Value.Value;
                var atsStep = new ATSTestSteps(step.Noderef, step.UID, atsTC.Noderef, f.TestStepId);

                step.AutomaticCalculations.ForEach(f1 =>
                {
                    AutomaticCalculation ac = (AutomaticCalculation)f1;
                    AutomaticCalculationItem automaticCalculationItem = (AutomaticCalculationItem)ac.Items?.FirstOrDefault();
                    atsStep.AddATSDynamicData(automaticCalculationItem?.AttributeName.ToLower() + ac.AttributeName.ToLower() + ac.PlainTextFormula.ToLower(),
                        ac.Noderef);
                });

                External.QualitySuite.ApiModel.TestStep sendStep = (External.QualitySuite.ApiModel.TestStep)atsTestCase.Steps.FirstOrDefault(w => ((External.QualitySuite.ApiModel.TestStep)w).Noderef == step.Noderef);

                sendStep?.AutomaticCalculations.Where(w => !atsStep.DynamicDatas.Select(s => s.Noderef).Contains(((AutomaticCalculation)w).Noderef)).ToList().ForEach(f1 =>
                {
                    AutomaticCalculation ac = (AutomaticCalculation)f1;
                    AutomaticCalculationItem automaticCalculationItem = (AutomaticCalculationItem)ac.Items?.FirstOrDefault();
                    atsStep.AddATSDynamicData(automaticCalculationItem?.AttributeName.ToLower() + ac.AttributeName.ToLower() + ac.PlainTextFormula.ToLower(),
                        ac.Noderef);
                });

                step.Filters.Where(w => ((External.QualitySuite.ApiModel.Filter)w).IsPostFilter).ToList().ForEach(f1 =>
                {
                    External.QualitySuite.ApiModel.Filter postfilter = (External.QualitySuite.ApiModel.Filter)f1;
                    ATSDynamicData atsDynamicData = null;
                    AutomaticCalculation ac = (AutomaticCalculation)postfilter.AutomaticCalculations.FirstOrDefault();
                    if (ac != null && ac.Items.FirstOrDefault() != null)
                    {
                        AutomaticCalculationItem automaticCalculationItem = (AutomaticCalculationItem)ac.Items.FirstOrDefault();
                        atsDynamicData = new ATSDynamicData(automaticCalculationItem.AttributeName.ToLower() + ac.AttributeName.ToLower() + ac.PlainTextFormula.ToLower(),
                           ac.Noderef);
                    }
                    atsStep.AddATSPostFilters(postfilter.AttributeName + postfilter.Value, atsDynamicData);
                });

                step.Filters.Where(w => !((External.QualitySuite.ApiModel.Filter)w).IsPostFilter).ToList().ForEach(f1 =>
                {
                    External.QualitySuite.ApiModel.Filter filter = (External.QualitySuite.ApiModel.Filter)f1;
                    ATSDynamicData atsDynamicData = null;
                    AutomaticCalculation ac = (AutomaticCalculation)filter.AutomaticCalculations.FirstOrDefault();
                    if (ac != null && ac.Items.FirstOrDefault() != null)
                    {
                        AutomaticCalculationItem automaticCalculationItem = (AutomaticCalculationItem)ac.Items.FirstOrDefault();
                        atsDynamicData = new ATSDynamicData(automaticCalculationItem.AttributeName.ToLower() + ac.AttributeName.ToLower() + ac.PlainTextFormula.ToLower(),
                           ac.Noderef);
                    }
                    atsStep.AddATSFilters(filter.AttributeName + filter.Value, atsDynamicData);
                });

                atsTC.AddATSTestStep(atsStep);
            });
            return atsTC;
        }

        private void SetPreviousTestCases(WorkflowTestCases workflowTestCase, TestCase atsTestCase)
        {
            if (workflowTestCase.VersionId > 0)
            {
                var atsprevTestCaseNoderefs = _dbContext.Set<ATSTestCasesData>().AsNoTracking().Where(w => w.WorkflowId == workflowTestCase.WorkflowId &&
                    w.VersionId == workflowTestCase.VersionId - 1).Select(s => s.Noderef).ToList();
                if (atsprevTestCaseNoderefs.Count() > 0)
                {
                    var atsTestSteps = _dbContext.Set<ATSTestStepsData>().AsNoTracking().Where(w => atsprevTestCaseNoderefs.Contains(w.TestCaseNoderef));

                    atsTestCase.StepsIDsMap = atsTestSteps.Count() == 0 ? null : atsTestSteps.ToDictionary(d => d.TestStepId.ToString(),
                         d1 => (object)new External.QualitySuite.ApiModel.TestStep
                         {
                             UID = d1.UId,
                             Noderef = d1.Noderef,
                             Type = EnumsTestStepType.Action,
                             ManualOrAuto = EnumsExecutionType.Automatic,
                             Priority = EnumsTestStepPriority.High
                         });
                }
            }
        }
    }
}