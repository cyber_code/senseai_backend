﻿using System;

namespace Application.Commands.DesignModule.QualitySuite
{
    public sealed class ATSHeader
    {
        public ATSHeader(Guid workflowId, string workflowTitle, string workflowDescription, string projectTitle, 
            string subProjectTitle, string folderTitle, string folderDescription)
        {
            WorkflowId = workflowId;
            WorkflowTitle = workflowTitle;
            WorkflowDescription = workflowDescription;
            ProjectTitle = projectTitle;
            SubProjectTitle = subProjectTitle;
            FolderTitle = folderTitle;
            FolderDescription = folderDescription;
        }

        public Guid WorkflowId { get; private set; }
         
        public string WorkflowTitle { get; private set; }

        public string WorkflowDescription { get; private set; }

        public string ProjectTitle { get; private set; }

        public string SubProjectTitle { get; private set; }

        public string FolderTitle { get; private set; }

        public string FolderDescription { get; private set; }
    }
}