﻿using Commands.DesignModule.Folder;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System.Collections.Generic;
using System;

namespace Application.Commands.DesignModule.FolderModel
{
    public sealed class AddFolderHandler : ICommandHandler<AddFolder, AddFolderResult>
    {
        private readonly IFolderRepository _repository;

        public AddFolderHandler(IFolderRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddFolderResult> Handle(AddFolder command)
        {
            Folder folder = new Folder(command.Title, command.Description, command.SubProjectId);

            _repository.Add(folder);

            return new CommandResponse<AddFolderResult>(
                new AddFolderResult(folder.Id)
            );
        }

        public bool Validate(AddFolder command)
        {
            if (command.SubProjectId==null || command.SubProjectId== Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}