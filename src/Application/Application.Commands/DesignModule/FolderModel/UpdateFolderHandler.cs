﻿using Commands.DesignModule.Folder;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.DesignModule.FolderModel
{
    public class UpdateFolderHandler : ICommandHandler<UpdateFolder, bool>
    {
        private readonly IFolderRepository _repository;

        public UpdateFolderHandler(IFolderRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateFolder command)
        {
            Folder folder = new Folder(command.Id, command.Title, command.Description);

            bool result = false;
            _repository.Update(folder);
            result = true;
            return new CommandResponse<bool>(
                result
            );
        }

        public bool Validate(UpdateFolder command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("FolderId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}