﻿using Commands.DesignModule.Folder;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.DesignModule.FolderModel
{
    public sealed class PasteFolderHandler : ICommandHandler<PasteFolder, PastFolderResult>
    {
        private readonly IFolderRepository _repository;

        public PasteFolderHandler(IFolderRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<PastFolderResult> Handle(PasteFolder command)
        {
            _repository.Paste(command.FolderId);

            return new CommandResponse<PastFolderResult>(
                new PastFolderResult(command.FolderId)
            );
        }

        public bool Validate(PasteFolder command)
        {
            if (command.FolderId == Guid.Empty)
            {
                Errors.Add("FolderId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}