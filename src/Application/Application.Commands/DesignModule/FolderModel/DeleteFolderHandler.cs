﻿using Commands.DesignModule.Folder;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.DesignModule.FolderModel
{
    public sealed class DeleteFolderHandler : ICommandHandler<DeleteFolder, bool>
    {
        private readonly IFolderRepository _repository;

        public DeleteFolderHandler(IFolderRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteFolder command)
        {
            bool tmpResult = false;
            Folder folder = new Folder(command.Id);
            _repository.Delete(command.Id);
            tmpResult = true;
            return new CommandResponse<bool>(
                 tmpResult
            );
        }

        public bool Validate(DeleteFolder command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("FolderId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}