﻿using Newtonsoft.Json;
using Commands.DataModule.DataSets;
using SenseAI.Domain.DataSetsModel;
using Messaging.Commands;
using System;
using System.Linq;
using System.Collections.Generic;
using NeuralEngine;
using CommandsCombinations = Commands.DataModule.DataSets.Combinations;

namespace Application.Commands.DataModule.DataSets
{
    public sealed class UpdateDataSetHandler : ICommandHandler<UpdateDataSet, UpdateDataSetResult>
    {
        private readonly IDataSetRepository _repository;
        private readonly INeuralEngineClient _neuralEngineClient;

        public UpdateDataSetHandler(IDataSetRepository repository, INeuralEngineClient neuralEngineClient)
        {
            _repository = repository;
            _neuralEngineClient = neuralEngineClient;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<UpdateDataSetResult> Handle(UpdateDataSet command)
        {
            DataSetItem[] dataSetItems = command.Rows.Select(ct => new DataSetItem(Guid.NewGuid(), JsonConvert.SerializeObject(ct.Attributes))).ToArray();
            var attributes = "";
            var combinations = GenerateCombinations(command.Rows);
            if (combinations.Count() > 0)
                attributes = JsonConvert.SerializeObject(combinations);

            DataSet dataSet = new DataSet(command.Id, command.SubProjectId, command.CatalogId, command.SystemId, command.TypicalId, command.Title,
                GetCoverage(command, combinations), attributes, dataSetItems);

            _repository.Update(dataSet);

            return new CommandResponse<UpdateDataSetResult>(new UpdateDataSetResult(dataSet.Id));
        }

        private decimal GetCoverage(UpdateDataSet command, List<CommandsCombinations> combinations)
        {
            decimal coverage = 0;
            var request = new NeuralEngine.DataSet.GetDataSetCoverageRequest
            {
                TenantId = command.TenantId,
                ProjectId = command.ProjectId,
                SubProjectId = command.SubProjectId,
                CatalogId = command.CatalogId,
                TypicalId = command.TypicalId,
                TypicalName = command.TypicalName,
                TypicalType = command.TypicalType,
                Combinations = combinations.Select(s => new NeuralEngine.DataSet.Combinations { Attribute = s.Attribute, Values = s.Values.ToArray() }).ToArray()
            };
            try
            {
                var response = _neuralEngineClient.GetDataSetCoverage(request);
                coverage = response.Coverage;
            }
            catch (Exception ex)
            { }

            return coverage;
        }

        private List<CommandsCombinations> GenerateCombinations(Rows[] rows)
        {
            List<CommandsCombinations> combinations = new List<CommandsCombinations>();
            if (rows.Count() > 0)
            {
                List<Attributes> attributes = new List<Attributes>();
                foreach (var row in rows)
                    attributes.AddRange(row.Attributes.Select(ct => new Attributes(ct.Name, ct.Value)).ToList());

                var grAttributes = attributes.GroupBy(ct => ct.Name);

                combinations = grAttributes.Select(att => new CommandsCombinations { Attribute = att.Key, Values = att.Select(val => val.Value).Distinct().ToList() }).ToList();
            }
            return combinations;
        }

        public bool Validate(UpdateDataSet command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }
            if (command.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId be empty.");
                return false;
            }
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (command.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.TypicalName))
            {
                Errors.Add("TypicalName cannot be empty.");
                return false;
            }
            return true;
        }
    }
}