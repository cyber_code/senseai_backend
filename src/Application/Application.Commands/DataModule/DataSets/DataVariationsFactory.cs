﻿using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.DataModule.DataSets
{
    /// <summary>
    ///     Generator for data variations
    /// </summary>
    public static class DataVariationsFactory
    {
        /// <summary>
        ///      Multiples the instance template by combining in cartesian product the equivalence sets
        /// </summary>
        /// <param name="instanceTemplate">The instance template</param>
        /// <param name="equivalenceSets">The equivalence sets</param>
        /// <returns>Typical instance variations</returns>
        public static List<TypicalInstance> VariationsFactory(TypicalInstance instanceTemplate, List<TypicalAttributes> attributesMetadata, Dictionary<string, List<string>> equivalenceSets)
        {
            if (instanceTemplate == null)
            {
                var attributes = equivalenceSets.Select(ct => new TypicalAttributes { Name = ct.Key, DefValue = "No Value" }).ToList();
                instanceTemplate = AutoGeneratdTemplateVariationsFactory(attributes);
            }
            var result = new List<TypicalInstance>();
            var equivalencesList = new List<List<KeyValuePair<string, string>>>();

            foreach (var equivalenceKey in equivalenceSets.Keys)
            {
                var subSet = new List<KeyValuePair<string, string>>();

                equivalenceSets[equivalenceKey].ForEach(item => subSet.Add(new KeyValuePair<string, string>(equivalenceKey, item)));
                equivalencesList.Add(subSet);
            }
            if (equivalencesList.Any())
            {
                var cartesianProduct = Cartesiant(equivalencesList);

                foreach (var subSet in cartesianProduct)
                {
                    var dataRowInstance = (TypicalInstance)instanceTemplate.Clone();

                    foreach (KeyValuePair<string, string> item in (IEnumerable)subSet)
                    {
                        var dataRowInstanceAttribute = dataRowInstance.Attributes.First(attribute => attribute.Name.Equals(item.Key));

                        if (dataRowInstanceAttribute != null)
                        {
                            dataRowInstanceAttribute.Value = item.Value;
                        }
                        else
                        {
                            throw new AggregateException($"Can't find the equivalence attribute {item.Key} in the typical attributes list!");
                        }
                    }

                    result.Add(dataRowInstance);
                }
            }

            return result;
        }

        /// <summary>
        ///     Creates a random instance template from the attributes metadata list and multiples the instance template by combining in cartesian product the equivalence sets
        /// </summary>
        /// <param name="attributesMetadata">The attributes metadata list</param>
        /// <param name="equivalenceSets">The equivalence sets</param>
        /// <returns>Random typical instance variations</returns>
        private static TypicalInstance AutoGeneratdTemplateVariationsFactory(List<TypicalAttributes> attributesMetadata)
        {
            TypicalInstance instanceTemplate = new TypicalInstance();

            attributesMetadata.ForEach(attrbuteMetaData =>
            {
                var attribute = new SenseAI.Domain.WorkflowModel.Attribute { Name = attrbuteMetaData.Name };

                if (!string.IsNullOrWhiteSpace(attrbuteMetaData.DefValue))
                {
                    attribute.Value = attrbuteMetaData.DefValue;
                }
                else
                {
                    switch (attrbuteMetaData.DataType)
                    {
                        case "string":
                            attribute.Value = "No Value";// GenerateRandomString(attrbuteMetaData.Length);
                            break;

                        case "integer":
                            attribute.Value = "No Value";// GenerateRandomInteger(attrbuteMetaData.MinValue, attrbuteMetaData.MaxValue, attrbuteMetaData.Length);
                            break;

                        case "real":
                            attribute.Value = "No Value";// GenerateRandomDecimal(attrbuteMetaData.MinValue, attrbuteMetaData.MaxValue, attrbuteMetaData.Length);
                            break;

                        case "boolean":
                            attribute.Value = "No Value";// GenerateRandomInteger(0.ToString(), 1.ToString(), attrbuteMetaData.Length);
                            break;

                        case "currency":
                            attribute.Value = "No Value";// GenerateRandomCurrency();
                            break;

                        case "datetime":
                            attribute.Value = "No Value";// GenerateRandomDateTime();
                            break;

                        case "blob":
                            attribute.Value = "No Value";// GenerateRandomBlob();
                            break;

                        case "measure":
                            attribute.Value = "No Value";// GenerateRandomMeasure();
                            break;
                    }
                }

                instanceTemplate.Attributes.Add(attribute);
            });

            return instanceTemplate;
        }

        /// <summary>
        ///     Produces the cartesian product for the given sets
        /// </summary>
        /// <param name="sets">The list of eqiovalences</param>
        /// <returns>The cartesian product for the sets</returns>
        private static IEnumerable Cartesiant(this IEnumerable<IEnumerable> sets)
        {
            var slots = sets
                .Select(x => x.GetEnumerator())
                .Where(x => x.MoveNext())
                .ToArray();

            while (true)
            {
                yield return slots.Select(x => x.Current);

                foreach (var slot in slots)
                {
                    // reset the slot if it couldn't move next
                    if (!slot.MoveNext())
                    {
                        // stop when the last enumerator resets
                        if (Equals(slot, slots.Last())) { yield break; }
                        slot.Reset();
                        slot.MoveNext();

                        continue;
                    }
                    // we could increase the current enumerator without reset so stop here
                    break;
                }
            }
        }

        #region [ Random data ]

        /// <summary>
        ///      Generates a random string
        /// </summary>
        /// <param name="length">The length of the string to be returned</param>
        /// <returns></returns>
        private static string GenerateRandomString(string length)
        {
            Random random = new Random();

            if (!int.TryParse(length, out var returnValueLength))
            {
                returnValueLength = random.Next(5);
            }

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789";
            return new string(Enumerable.Repeat(chars, returnValueLength).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static string GenerateRandomInteger(string attributeMinValue, string attributeMaxValue, string attributeValueLength)
        {
            Random random = new Random();

            if (int.TryParse(attributeMinValue, out var minValue) && int.TryParse(attributeMaxValue, out var maxValue))
                return random.Next(minValue, maxValue).ToString();
            else
            {
                var val = new Random().Next(0, int.MaxValue).ToString();
                return int.TryParse(attributeValueLength, out var returnValueLength) ? val.Substring(0, returnValueLength) : val;
            }
        }

        private static string GenerateRandomDecimal(string attributeMinValue, string attributeMaxValue, string attributeValueLength)
        {
            Random random = new Random();
            string returnValue;
            if (int.TryParse(attributeMinValue, out var minValue) && int.TryParse(attributeMaxValue, out var maxValue))
            {
                returnValue = string.Format("{0:0.00}", random.NextDouble() * Math.Abs(maxValue - minValue) + minValue);
            }
            else
            {
                var generatedValue = random.NextDouble();
                returnValue = string.Format("{0:0.00}", generatedValue);

                if (!int.TryParse(attributeValueLength, out var returnValueLength))
                {
                    returnValueLength = 5;
                }
                else
                {
                    if (returnValue.Length < returnValueLength)
                        returnValueLength = returnValue.Length;
                }

                returnValue = returnValue.Substring(returnValueLength - returnValue.Length, returnValueLength);
            }

            return returnValue;
        }

        private static string GenerateRandomCurrency()
        {
            var currencies = new string[] { "USD", "EUR", "JPY", "GBP", "AUD", "CAD", "CHF", "CNH", "SEK", "NZD" };
            return currencies[new Random().Next(0, 9)];
        }

        private static string GenerateRandomDateTime()
        {
            Random r = new Random();
            return new DateTime(r.Next(1950, 2010), r.Next(1, 12), r.Next(1, 28)).ToShortDateString();
        }

        private static string GenerateRandomBlob()
        {
            return GenerateRandomString(100.ToString());
        }

        private static string GenerateRandomMeasure()
        {
            return GenerateRandomInteger(10.ToString(), 100.ToString(), 3.ToString());
        }

        #endregion [ Random data ]
    }
}