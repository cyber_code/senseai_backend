﻿using Newtonsoft.Json;
using Commands.DataModule.DataSets;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using System;
using System.Linq;
using System.Collections.Generic;
using NeuralEngine;

namespace Application.Commands.DataModule.DataSets
{
    public sealed class AddManualDataSetHandler : ICommandHandler<AddManualDataSet, AddManualDataSetResult>
    {
        private readonly IDataSetRepository _repository;
        private readonly ITypicalInstanceRepository _typicalInstanceRepository;
        private readonly ITypicalRepository _typicalRepository;
        private readonly INeuralEngineClient _neuralEngineClient;

        public AddManualDataSetHandler(IDataSetRepository repository, ITypicalInstanceRepository typicalInstanceRepository, 
            ITypicalRepository typicalRepository, INeuralEngineClient neuralEngineClient)
        {
            _repository = repository;
            _typicalInstanceRepository = typicalInstanceRepository;
            _typicalRepository = typicalRepository;
            _neuralEngineClient = neuralEngineClient;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddManualDataSetResult> Handle(AddManualDataSet command)
        {
            var attributes = _typicalInstanceRepository.Get(command.TypicalId);
            var typical = _typicalRepository.GetTypical(command.TypicalId);

            TypicalInstance typicalInstance = null;
            if (!string.IsNullOrWhiteSpace(attributes))
            {
                typicalInstance = JsonConvert.DeserializeObject<TypicalInstance>(attributes);
            }
            TypicalAttributeMetadata typicalAttributeMetadata = JsonConvert.DeserializeObject<TypicalAttributeMetadata>(typical.Json);

            var skipEmptyValues = command.Combinations.Where(w => w.Values.Any(w1 => w1.Trim() != ""));
            
            var rows = DataVariationsFactory.VariationsFactory(typicalInstance, typicalAttributeMetadata.Attributes.ToList(), skipEmptyValues.ToDictionary(ct => ct.Attribute, ct => ct.Values));
            DataSetItem[] dataSetItems = rows.Select(ct => new DataSetItem(Guid.NewGuid(), JsonConvert.SerializeObject(ct.Attributes))).ToArray();

            DataSet dataSet = new DataSet(command.SubProjectId, command.CatalogId, command.SystemId, command.TypicalId, command.Title, GetCoverage(command), JsonConvert.SerializeObject(skipEmptyValues), dataSetItems);

            _repository.Add(dataSet);

            return new CommandResponse<AddManualDataSetResult>(new AddManualDataSetResult(dataSet.Id));
        }

        private decimal GetCoverage(AddManualDataSet command)
        {
            decimal coverage = 0;
            var request = new NeuralEngine.DataSet.GetDataSetCoverageRequest
            {
                TenantId = command.TenantId,
                ProjectId = command.ProjectId,
                SubProjectId = command.SubProjectId,
                CatalogId = command.CatalogId,              
                TypicalId = command.TypicalId,
                TypicalName = command.TypicalName,
                TypicalType = command.TypicalType,
                Combinations = command.Combinations.Select(s => new NeuralEngine.DataSet.Combinations { Attribute = s.Attribute, Values = s.Values.ToArray() }).ToArray()
            };
            try
            {
                var response = _neuralEngineClient.GetDataSetCoverage(request);
                coverage = response.Coverage;
            }
            catch (Exception ex)
            { }

            return coverage;
        }

        public bool Validate(AddManualDataSet command)
        {
            if (command.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }

            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (command.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.TypicalName))
            {
                Errors.Add("TypicalName cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}