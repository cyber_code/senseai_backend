﻿using Newtonsoft.Json;
using Commands.DataModule.DataSets;
using SenseAI.Domain.DataSetsModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.DataModule.DataSets
{
    public sealed class UpdateDataSetItemHandler : ICommandHandler<UpdateDataSetItem, UpdateDataSetItemResult>
    {
        private readonly IDataSetRepository _repository;

        public UpdateDataSetItemHandler(IDataSetRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<UpdateDataSetItemResult> Handle(UpdateDataSetItem command)
        {
            var dataSetItem = new DataSetItem(command.Id, command.DataSetId, JsonConvert.SerializeObject(command.Row.Attributes));

            _repository.UpdateItem(dataSetItem);

            return new CommandResponse<UpdateDataSetItemResult>(new UpdateDataSetItemResult(dataSetItem.Id));
        }

        public bool Validate(UpdateDataSetItem command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Data set itemId cannot be empty.");
                return false;
            }
            if (command.Row == null)
            {
                Errors.Add("Row cannot be empty.");
                return false;
            }
            return true;
        }
    }
}