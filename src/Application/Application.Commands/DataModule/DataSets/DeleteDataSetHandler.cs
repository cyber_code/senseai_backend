﻿using Commands.DataModule.DataSets;
using SenseAI.Domain.DataSetsModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.DataModule.DataSets
{
    public sealed class DeleteDataSetHandler : ICommandHandler<DeleteDataSet, DeleteDataSetResult>
    {
        private readonly IDataSetRepository _repository;

        public DeleteDataSetHandler(IDataSetRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<DeleteDataSetResult> Handle(DeleteDataSet command)
        {
            _repository.Delete(command.Id);

            return new CommandResponse<DeleteDataSetResult>(new DeleteDataSetResult(command.Id));
        }

        public bool Validate(DeleteDataSet command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("DataSetId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}