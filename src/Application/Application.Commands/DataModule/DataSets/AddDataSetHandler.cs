﻿using Newtonsoft.Json;
using Commands.DataModule.DataSets;
using SenseAI.Domain.DataSetsModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using CommandsCombinations = Commands.DataModule.DataSets.Combinations;

namespace Application.Commands.DataModule.DataSets
{
    public sealed class AddDataSetHandler : ICommandHandler<AddDataSet, AddDataSetResult>
    {
        private readonly IDataSetRepository _repository;

        public AddDataSetHandler(IDataSetRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddDataSetResult> Handle(AddDataSet command)
        {
            DataSetItem[] dataSetItems = command.Rows.Select(ct => new DataSetItem(Guid.NewGuid(), JsonConvert.SerializeObject(ct.Attributes))).ToArray();
            DataSet dataSet = new DataSet(command.SubProjectId, command.CatalogId, command.SystemId, command.TypicalId, command.Title, command.Coverage, GenerateCombinations(command.Rows), dataSetItems);

            _repository.Add(dataSet);

            return new CommandResponse<AddDataSetResult>(new AddDataSetResult(dataSet.Id));
        }

        public bool Validate(AddDataSet command)
        {
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (command.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            if (command.Rows.Length == 0)
            {
                Errors.Add("Nr of rows cannot be empty.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Generate combinations based on the rows suggested from AI
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        private string GenerateCombinations(Rows[] rows)
        {
            string combinations = string.Empty;
            if (rows.Count() > 0)
            {
                List<Attributes> attributes = new List<Attributes>();
                foreach (var row in rows)
                    attributes.AddRange(row.Attributes.Select(ct => new Attributes(ct.Name, ct.Value)).ToList());

                var grAttributes = attributes.GroupBy(ct => ct.Name);

                var listOfCombinations = grAttributes.Select(att => new CommandsCombinations { Attribute = att.Key, Values = att.Select(val => val.Value).Distinct().ToList() });
                if (listOfCombinations.Count() > 0)
                    combinations = JsonConvert.SerializeObject(listOfCombinations);
            }
            return combinations;
        }
    }
}