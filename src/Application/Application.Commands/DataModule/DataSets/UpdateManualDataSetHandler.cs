﻿using Newtonsoft.Json;
using Commands.DataModule.DataSets;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using System;
using System.Linq;
using System.Collections.Generic;
using NeuralEngine;

namespace Application.Commands.DataModule.DataSets
{
    public sealed class UpdateManualDataSetHandler : ICommandHandler<UpdateManualDataSet, UpdateManualDataSetResult>
    {
        private readonly IDataSetRepository _repository;
        private readonly IWorkflowVersionPathsRepository _workflowVersionPathsRepository;
        private readonly ITypicalInstanceRepository _typicalInstanceRepository;
        private readonly ITypicalRepository _typicalRepository;
        private readonly INeuralEngineClient _neuralEngineClient;

        public UpdateManualDataSetHandler(IDataSetRepository repository, ITypicalInstanceRepository typicalInstanceRepository,
            ITypicalRepository typicalRepository, INeuralEngineClient neuralEngineClient, IWorkflowVersionPathsRepository workflowVersionPathsRepository)
        {
            _repository = repository;
            _typicalInstanceRepository = typicalInstanceRepository;
            _typicalRepository = typicalRepository;
            _neuralEngineClient = neuralEngineClient;
            _workflowVersionPathsRepository = workflowVersionPathsRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<UpdateManualDataSetResult> Handle(UpdateManualDataSet command)
        {
            if (command.UpdateStatus == UpdateStatus.NoAction && _workflowVersionPathsRepository.CheckDataSetOnLastVersions(command.WorkflowId, command.Id))
                return new CommandResponse<UpdateManualDataSetResult>(new UpdateManualDataSetResult(command.Id, true, false));
            if (command.UpdateStatus == UpdateStatus.NoAction && _workflowVersionPathsRepository.CheckDataSetOnNotLastVersions(command.WorkflowId, command.Id))
                return new CommandResponse<UpdateManualDataSetResult>(new UpdateManualDataSetResult(command.Id, false, true));

            var attributes = _typicalInstanceRepository.Get(command.TypicalId);
            var typical = _typicalRepository.GetTypical(command.TypicalId);

            TypicalInstance typicalInstance = null;
            if (!string.IsNullOrWhiteSpace(attributes))
            {
                typicalInstance = JsonConvert.DeserializeObject<TypicalInstance>(attributes);
            }

            TypicalAttributeMetadata typicalAttributeMetadata = JsonConvert.DeserializeObject<TypicalAttributeMetadata>(typical.Json);

            var skipEmptyValues = command.Combinations.Where(w => w.Values.Any(w1 => w1.Trim() != ""));

            var rows = DataVariationsFactory.VariationsFactory(typicalInstance, typicalAttributeMetadata.Attributes.ToList(), skipEmptyValues.ToDictionary(ct => ct.Attribute, ct => ct.Values));
            DataSetItem[] dataSetItems = rows.Select(ct => new DataSetItem(Guid.NewGuid(), JsonConvert.SerializeObject(ct.Attributes))).ToArray();

            if (dataSetItems.Count() == 1)
            {
                if (String.IsNullOrEmpty(rows[0].Attributes[0].Value))
                    dataSetItems = new DataSetItem[0];

            }

            DataSet dataSet = new DataSet(command.Id, command.Title, JsonConvert.SerializeObject(skipEmptyValues), GetCoverage(command), dataSetItems);
            DataSet existingDataSet = _repository.Get(command.Id);
            bool update = dataSet.Combinations.ToLower() == existingDataSet.Combinations.ToLower();


            if (!update && command.UpdateStatus == UpdateStatus.LastVersionUpdate)
            {
                _repository.Update(dataSet);
                _workflowVersionPathsRepository.UpdateOnLastVersion(command.Id, dataSet.Id);
            }
            else if (!update && (command.UpdateStatus == UpdateStatus.LastVersionCreateNew || command.UpdateStatus == UpdateStatus.NotLastVersionCreateNew))
            {
                int number = _repository.GetNumberOfSameDatasets(command.Id);
                string title = $"{command.Title}_{++number}";

                dataSet = new DataSet(command.SubProjectId, command.CatalogId, command.SystemId, command.TypicalId, title, GetCoverage(command), JsonConvert.SerializeObject(skipEmptyValues), dataSetItems, command.Id);
                _repository.Add(dataSet);
                _workflowVersionPathsRepository.Update(command.WorkflowId, command.WorkflowPathId, command.WorkflowPathItemId, command.Index, dataSet.Id);
            }
            else
            {
                _repository.Update(dataSet);
            }

            return new CommandResponse<UpdateManualDataSetResult>(new UpdateManualDataSetResult(dataSet.Id, false, false));
        }

        private decimal GetCoverage(UpdateManualDataSet command)
        {
            decimal coverage = 0;
            var request = new NeuralEngine.DataSet.GetDataSetCoverageRequest
            {
                TenantId = command.TenantId,
                ProjectId = command.ProjectId,
                SubProjectId = command.SubProjectId,
                CatalogId = command.CatalogId,
                TypicalId = command.TypicalId,
                TypicalName = command.TypicalName,
                TypicalType = command.TypicalType,
                Combinations = command.Combinations.Select(s => new NeuralEngine.DataSet.Combinations { Attribute = s.Attribute, Values = s.Values.ToArray() }).ToArray()
            };
            try
            {
                var response = _neuralEngineClient.GetDataSetCoverage(request);
                coverage = response.Coverage;
            }
            catch (Exception ex)
            { }

            return coverage;
        }

        public bool Validate(UpdateManualDataSet command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("DataSetId cannot be empty.");
                return false;
            }

            if (command.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }

            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }

            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (command.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            if (command.WorkflowPathId == Guid.Empty)
            {
                Errors.Add("WorkflowPathId cannot be empty.");
                return false;
            }
            if (command.WorkflowPathItemId == Guid.Empty)
            {
                Errors.Add("WorkflowPathItemId cannot be empty.");
                return false;
            }
            if (command.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.TypicalName))
            {
                Errors.Add("TypicalName cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            if (command.Combinations.Length == 0)
            {
                Errors.Add("Combinations cannot be empty.");
                return false;
            }
            return true;
        }
    }
}