﻿using Commands.DataModule.DataSets;
using SenseAI.Domain.DataSetsModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using SenseAI.Domain.WorkflowModel;
using System.Linq;

namespace Application.Commands.DataModule.DataSets
{
    public sealed class DeleteDataSetItemHandler : ICommandHandler<DeleteDataSetItem, DeleteDataSetItemResult>
    {
        private readonly IDataSetRepository _repository;
        private readonly IWorkflowVersionPathsRepository _workflowVersionPathsRepository;

        public DeleteDataSetItemHandler(IDataSetRepository repository,IWorkflowVersionPathsRepository workflowVersionPathsRepository)
        {
            _repository = repository;
            _workflowVersionPathsRepository = workflowVersionPathsRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<DeleteDataSetItemResult> Handle(DeleteDataSetItem command)
        {
            if (command.DeleteStatus == DeleteStatus.NoAction && _workflowVersionPathsRepository.CheckDataSetOnLastVersions(command.WorkflowId, command.DataSetId))
                return new CommandResponse<DeleteDataSetItemResult>(new DeleteDataSetItemResult(command.Id, true, false));
            if (command.DeleteStatus == DeleteStatus.NoAction && _workflowVersionPathsRepository.CheckDataSetOnNotLastVersions(command.WorkflowId, command.DataSetId))
                return new CommandResponse<DeleteDataSetItemResult>(new DeleteDataSetItemResult(command.Id, false, true));

            Guid newDataSetId = Guid.NewGuid();

            if (command.DeleteStatus == DeleteStatus.LastVersionCreateNew || command.DeleteStatus == DeleteStatus.NotLastVersionCreateNew)
            {
                var oldDataSet = _repository.GetDataSet(command.DataSetId);

                int number = _repository.GetNumberOfSameDatasets(oldDataSet.Id);
                string title = $"{oldDataSet.Title}_{++number}";

                var newDataSetItems = oldDataSet.DataSetItems.Where(u => u.Id != command.Id).Select(w => new DataSetItem(Guid.NewGuid(), newDataSetId, w.Row));

                var newDataSet = new DataSet(newDataSetId, oldDataSet.SubProjectId, oldDataSet.CatalogId, oldDataSet.SystemId, oldDataSet.TypicalId, title, oldDataSet.Coverage, oldDataSet.Combinations, newDataSetItems.ToArray(), oldDataSet.Id);
                _repository.Add(newDataSet);
                _workflowVersionPathsRepository.Update(command.WorkflowId, command.WorkflowPathId, command.WorkflowPathItemId, command.Index, newDataSet.Id);
            }
            else
            {
                _repository.DeleteItem(command.Id);
            }

            return new CommandResponse<DeleteDataSetItemResult>(new DeleteDataSetItemResult(newDataSetId, false, false));
        }

        public bool Validate(DeleteDataSetItem command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Data set itemId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}