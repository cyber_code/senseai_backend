﻿using Newtonsoft.Json;
using Commands.DataModule.TestCases;
using SenseAI.Domain;
using SenseAI.Domain.SystemModel;
using SenseAI.Domain.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using System;
using System.Linq;
using System.Collections.Generic;
using Core.Abstractions;
using Persistence.Internal;
using Persistence.WorkflowModel;
using NeuralEngine;
using Persistence.DataSetsModel;
using Microsoft.EntityFrameworkCore;

namespace Application.Commands.DataModule.TestCases
{
    public sealed class SetDataSetForPathItemHandler : ICommandHandler<SetDataSetForPathItem, bool>
    {
        private readonly IWorkflowVersionPathsRepository _workflowPathsRepository;
        private readonly ICommandHandler<GenerateTestCases, GenerateTestCasesResult> _generateTestCasesCommand;

        public SetDataSetForPathItemHandler(IWorkflowVersionPathsRepository workflowPathsRepository, ICommandHandler<GenerateTestCases, GenerateTestCasesResult> commandHandler)
        {
            _workflowPathsRepository = workflowPathsRepository;
            _generateTestCasesCommand = commandHandler;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(SetDataSetForPathItem command)
        {
            _workflowPathsRepository.Update(command.WorkflowId, command.WorkflowPathId, command.WorkflowPathItemId,  command.Index, command.DataSetId);

            GenerateTestCases generateTestCases = new GenerateTestCases
            {
                TenantId = command.TenantId,
                ProjectId = command.ProjectId,
                SubProjectId = command.SubProjectId,
                CatalogId = command.CatalogId,
                SystemId = command.SystemId,
                WorkflowId = command.WorkflowId,
                WorkflowPathId = command.WorkflowPathId,
                SkipStatus = command.SkipStatus
            };

            var response = _generateTestCasesCommand.Handle(generateTestCases);

            return new CommandResponse<bool>(true, response.Response);
        }

        public bool Validate(SetDataSetForPathItem command)
        {
            if (command.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }

            if (command.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            if (command.WorkflowPathId == Guid.Empty)
            {
                Errors.Add("WorkflowPathId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}