﻿using Newtonsoft.Json;
using Commands.DataModule.TestCases;
using SenseAI.Domain;
using SenseAI.Domain.SystemModel;
using SenseAI.Domain.WorkflowModel; 
using Messaging.Commands;
using System;
using System.Linq;
using System.Collections.Generic;
using Core.Abstractions;
using Persistence.Internal;
using Persistence.WorkflowModel;
using NeuralEngine;
using Persistence.DataSetsModel;
using Microsoft.EntityFrameworkCore;
using ActionType = SenseAI.Domain.ActionType;

namespace Application.Commands.DataModule.TestCases
{
    public sealed class GenerateTestCasesHandler : ICommandHandler<GenerateTestCases, GenerateTestCasesResult>
    {
        private readonly IWorkflowRepository _workflowRepository;
        private readonly INeuralEngineClient _neuralEngineClient;
        private readonly IWorkflowVersionPathsRepository _workflowPathsRepository;
        private readonly IWorkflowTestCasesRepository _generatedTestCasesRepository;
        private readonly IWorkContext _workContext;
        private readonly ISystemRepository _systemRepository;
        private readonly IDbContext _dbContext;

        public GenerateTestCasesHandler(IWorkflowRepository workflowRepository, IWorkflowVersionPathsRepository workflowPathsRepository,
            IWorkflowTestCasesRepository generatedTestCasesRepository, ISystemRepository systemRepository, IWorkContext workContext,
            IDbContext dbContext, INeuralEngineClient neuralEngineClient)
        {
            _workflowRepository = workflowRepository;
            _workflowPathsRepository = workflowPathsRepository;
            _generatedTestCasesRepository = generatedTestCasesRepository;
            _systemRepository = systemRepository;
            _workContext = workContext;
            _dbContext = dbContext;
            _neuralEngineClient = neuralEngineClient;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<GenerateTestCasesResult> Handle(GenerateTestCases command)
        {
            WorkflowVersionPaths workflowPaths = _workflowPathsRepository.Get(command.WorkflowId, command.WorkflowPathId);
            if (workflowPaths.Items.Any(a => a.ActionType == (int)ActionType.Input && a.DataSetId == Guid.Empty))
                return new CommandResponse<GenerateTestCasesResult>(new GenerateTestCasesResult(command.WorkflowPathId), new CommandResponseMessage((int)MessageType.Warning, "All the data sets need to be selected."));

            MessageType messageType = MessageType.Successful;
            string message = string.Empty;

            var userModifiedId = _dbContext.Set<WorkflowData>().Where(u => u.Id == command.WorkflowId).Select(u => u.UserModified).FirstOrDefault();

            //Get workflow status
            int workflowStatus = _workflowRepository.GetWorkflowStatus(command.WorkflowId);
            if (command.SkipStatus == false && userModifiedId != _workContext.UserId)
            {
                if (workflowStatus != (int)WorkflowStatus.ReadyToGenerate)
                {
                    //can't generate test case if the status is not "ReadyToGenerate"
                    //this action will stop generating test cases
                    messageType = MessageType.Information;
                    var workflowDetails = _workflowRepository.GetWorkflowDetails(command.WorkflowId);
                    message = $"This workflow is currently in progress by another user. If you continue they will lose any unsaved progress.";
                    return new CommandResponse<GenerateTestCasesResult>(new GenerateTestCasesResult(command.WorkflowPathId), new CommandResponseMessage((int)messageType, message));
                }
            }
            else
                _workflowRepository.UpdateStatus(command.WorkflowId, WorkflowStatus.GeneratingInProcess);
            try
            {

                if (workflowPaths == null || workflowPaths.Items.Count() == 0)
                    throw new Exception("The path does not have any path items.");

                Workflow workflow = _workflowRepository.GetLastVersion(command.WorkflowId);
                if (workflow.Items.Any(ct => ct is LinkedWorkflowItem))
                {
                    foreach (LinkedWorkflowItem lkWorkflow in workflow.Items.Where(ct => ct is LinkedWorkflowItem))
                    {
                        try
                        {
                            var innerWorkflow = _workflowRepository.GetWorkflowByVersion(lkWorkflow.WorkflowId, lkWorkflow.WorkflowVersionId);
                            lkWorkflow.SetTitle(lkWorkflow.Title + ": " + innerWorkflow.Title);
                            innerWorkflow.SetParentIds(lkWorkflow.Id);

                            innerWorkflow.SetParentIds(lkWorkflow.Id);
                            innerWorkflow.SetUniqueIds(lkWorkflow.Id.ToString());
                            lkWorkflow.SetUniqueId(Guid.Empty.ToString());
                            AppendRecursivelyLinkWorkflow(innerWorkflow, lkWorkflow.Id.ToString());

                            workflow.AppendLinkWorkflow(lkWorkflow, innerWorkflow, lkWorkflow.Id.ToString());
                            workflow.AddItems(innerWorkflow.Items.ToList());
                            workflow.AddLinks(innerWorkflow.ItemLinks.ToList());
                        }
                        catch (Exception ex)
                        {
                            var workflowTitle = _dbContext.Set<WorkflowData>().Where(u => u.Id == lkWorkflow.WorkflowId).Select(u => u.Title).FirstOrDefault();
                            message = $"There is linked workflow with title \" {workflowTitle} \" that is not issued! Test Cases are not generated!";
                            throw new Exception(message);
                        }
                    }
                }

                TestCaseGenerator testCaseGenerator = new TestCaseGenerator(workflow, _systemRepository.GetSystems());
                var testCases = testCaseGenerator.Generate(workflowPaths);

                var generatedTestCases = testCases.SelectMany(
                    testCase => testCase.TestSteps.Select(
                        testStep => new WorkflowTestCases(
                           Guid.NewGuid(), command.WorkflowId, workflowPaths.VersionId, command.WorkflowPathId,
                            testCase.Id, testCase.Title, testCases.IndexOf(testCase), testStep.Id, testStep.Title,
                            JsonConvert.SerializeObject(testStep), testStep.TypicalId, testStep.TypicalName, testCase.TestSteps.ToList().IndexOf(testStep),
                            int.Parse(testStep.Type)
                        )
                    )
                ).ToArray();

                _generatedTestCasesRepository.Add(generatedTestCases);
                _workflowRepository.UpdateStatus(command.WorkflowId, WorkflowStatus.ReadyToExport);
                _workflowRepository.SetIsTestCasegenerated(command.WorkflowId);

                if (!SaveWorkflowDataCoverage(workflowPaths.Items.Select(s => s.DataSetId).Where(w => w != Guid.Empty).ToArray(), command))
                    return new CommandResponse<GenerateTestCasesResult>(new GenerateTestCasesResult(command.WorkflowPathId), new CommandResponseMessage((int)MessageType.Warning, "Workflow data coverage are not saved on ML."));

                return new CommandResponse<GenerateTestCasesResult>(new GenerateTestCasesResult(command.WorkflowPathId));
            }
            catch (Exception ex)
            {
                // if test case generation fail update status to ReadyToGenerate
                messageType = MessageType.Failed;

                _workflowRepository.UpdateStatus(command.WorkflowId, WorkflowStatus.ReadyToGenerate);
                return new CommandResponse<GenerateTestCasesResult>(new GenerateTestCasesResult(command.WorkflowPathId), new CommandResponseMessage((int)messageType, message));
            }
        }

        public bool Validate(GenerateTestCases command)
        {
            if (command.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }

            if (command.WorkflowId == Guid.Empty)
            {
                Errors.Add("WorkflowId cannot be empty.");
                return false;
            }
            if (command.WorkflowPathId == Guid.Empty)
            {
                Errors.Add("WorkflowPathId cannot be empty.");
                return false;
            }
            return true;
        }

        private void AppendRecursivelyLinkWorkflow(Workflow workflow, string uniqueId)
        {
            if (workflow.Items.Where(ct => ct is LinkedWorkflowItem).Count() == 0)
                return;
            foreach (LinkedWorkflowItem lkWorkflow in workflow.Items.Where(ct => ct is LinkedWorkflowItem))
            {
                var innerWorkflow = _workflowRepository.GetWorkflowByVersion(lkWorkflow.WorkflowId, lkWorkflow.WorkflowVersionId);
                lkWorkflow.SetTitle(lkWorkflow.Title + ": " + innerWorkflow.Title);

                string innweUniqueId = uniqueId + "" + lkWorkflow.Id.ToString();
                innerWorkflow.SetParentIds(lkWorkflow.Id);
                innerWorkflow.SetUniqueIds(innweUniqueId);

                workflow.AppendLinkWorkflow(lkWorkflow, innerWorkflow, innweUniqueId);
                AppendRecursivelyLinkWorkflow(innerWorkflow, innweUniqueId);

                workflow.AddItems(innerWorkflow.Items.ToList());
                workflow.AddLinks(innerWorkflow.ItemLinks.ToList());
            }
        }

        private bool SaveWorkflowDataCoverage(Guid[] dataSetId, GenerateTestCases command)
        {
            var dataSets = _dbContext.Set<DataSetData>().AsNoTracking().Where(w => dataSetId.FirstOrDefault(w1 => w1 == w.Id) != null);
            if (dataSets != null)
            {
                decimal coverage = dataSets.Sum(s => s.Coverage) / dataSets.Count();
                try
                {
                    var request = new NeuralEngine.DataSet.SaveWorkflowDataCoverageRequest
                    {
                        TenantId = command.TenantId,
                        ProjectId = command.ProjectId,
                        SubProjectId = command.SubProjectId,
                        CatalogId = command.CatalogId,
                        WorkflowId = command.WorkflowId,
                        Coverage = coverage
                    };
                    var result = _neuralEngineClient.SaveWorkflowDataCoverage(request);
                    return true;
                }
                catch (Exception ex)
                {
                }
            }
            return false;
        }
    }
}