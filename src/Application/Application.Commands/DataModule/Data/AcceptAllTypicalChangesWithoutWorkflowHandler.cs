﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Commands.DataModule.Data;
using Commands.DataModule.DataSets;
using DataExtraction;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using Persistence.DataModel;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CommandsCombinations = Commands.DataModule.DataSets.Combinations;

namespace Application.Commands.DataModule.Data
{
    public sealed class AcceptAllTypicalChangesWithoutWorkflowHandler : ICommandHandler<AcceptAllTypicalChangesWithoutWorkflow, AcceptAllTypicalChangesWithoutWorkflowResult>
    {
        private readonly SenseAIObjectContext _dbContext;
        private readonly ITypicalRepository _repository;
        private readonly IWorkflowRepository _wfRepository;
        private readonly ITypicalChangesRepository _typicalChangesRepository;
        private readonly IAcceptedTypicalChangesRepository _acceptedTypicalChangesRepository;
        private readonly IDataSetRepository _dataSetRepository;
        private readonly IDataExtractionClient _service;

        public AcceptAllTypicalChangesWithoutWorkflowHandler(ITypicalRepository repository, SenseAIObjectContext dbContext, IWorkflowRepository wfRepository, ITypicalChangesRepository typicalChangesRepository, IAcceptedTypicalChangesRepository acceptedTypicalChangesRepository, IDataSetRepository dataSetRepository, IDataExtractionClient service)
        {
            _repository = repository;
            _dbContext = dbContext;
            _wfRepository = wfRepository;
            _typicalChangesRepository = typicalChangesRepository;
            _acceptedTypicalChangesRepository = acceptedTypicalChangesRepository;
            _dataSetRepository = dataSetRepository;
            _service = service;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AcceptAllTypicalChangesWithoutWorkflowResult> Handle(AcceptAllTypicalChangesWithoutWorkflow command)
        {
            int count = Execute(command);

            return new CommandResponse<AcceptAllTypicalChangesWithoutWorkflowResult>(
                new AcceptAllTypicalChangesWithoutWorkflowResult(count)
            );
        }

        public bool Validate(AcceptAllTypicalChangesWithoutWorkflow command)
        {
            return true;
        }

        private int Execute(AcceptAllTypicalChangesWithoutWorkflow command)
        {
            var typicalChanges = (from typical in _dbContext.Set<TypicalChangesData>()

                                  select typical).AsNoTracking().ToList();
            int count = 0;
            foreach (var typicalChange in typicalChanges)
            {
                var actualTypical = (from typical in _dbContext.Set<TypicalData>()
                                     where typical.Title == typicalChange.Title
                                     && typical.CatalogId == typicalChange.CatalogId
                                     select typical).AsNoTracking().FirstOrDefault();

                var typicalChangeMetadata = JsonConvert.DeserializeObject<TypicalChangesMetadata>(typicalChange.Json);
                var typicalChangeAttributes = typicalChangeMetadata.Attributes;
                var typicalAttributes = JsonConvert.DeserializeObject<TypicalChangesMetadata>(actualTypical.Json).Attributes;

                var newAttributes = typicalChangeAttributes.Except(typicalAttributes, new AttributeNameComparer());
                var deletedAttributes = typicalAttributes.Except(typicalChangeAttributes, new AttributeNameComparer());
                var changedAttributes = typicalChangeAttributes.Except(typicalAttributes, new AttributeComparer())
                                                               .Except(newAttributes, new AttributeNameComparer());
                var listOfAffectedWorkflowsQuery = (from workflow in _dbContext.Set<WorkflowData>()
                                                    join workflowitem in _dbContext.Set<WorkflowItemData>() on workflow.Id equals workflowitem.WorkflowId
                                                    where workflowitem.TypicalTitle == typicalChange.Title
                                                    select workflow).AsNoTracking().Distinct().ToList();
                if (listOfAffectedWorkflowsQuery.Count() == 0)
                {
                    updateDataSetsObject(actualTypical.Id, deletedAttributes);

                    var updatedtypical = updateTypicalAndTypicalChanges(actualTypical, typicalChange);

                    callingDataExtration(typicalChangeMetadata);
                    count++;
                }
            }

            return count;
        }

        private void callingDataExtration(TypicalChangesMetadata typicalChangeMetadata)
        {
            DataExtraction.AcceptTypicalChangesModel.AcceptTypicalChangesModel acceptTypicalModelRequest = new DataExtraction.AcceptTypicalChangesModel.AcceptTypicalChangesModel
            {
                TenantId = typicalChangeMetadata.TenantId,
                ProjectId = typicalChangeMetadata.ProjectId,
                SubProjectId = typicalChangeMetadata.SubProjectId,
                CatalogId = typicalChangeMetadata.CatalogId,
                SystemId = typicalChangeMetadata.SystemId, 

                Id = typicalChangeMetadata.Id,
                Type = typicalChangeMetadata.Type,
                Title = typicalChangeMetadata.Title,
                HasAutomaticId = typicalChangeMetadata.HasAutomaticId,
                IsConfiguration = typicalChangeMetadata.IsConfiguration,
                Attributes = typicalChangeMetadata.Attributes
            };

            try
            {
                var result = _service.AcceptedTypicalChanges(acceptTypicalModelRequest).Result;
            }
            catch (Exception ex)
            {
                throw new Exception($"Could not send the typical changes to ML service! error: {ex.Message}");
            }
        }

        private void updateDataSetsObject(Guid actualTypicalId, IEnumerable<TypicalChangesAttributes> deletedAttributes)
        {
            // get dataset that use this typical
            var affectedDataset = (from dataset in _dbContext.Set<DataSetData>()
                                   where dataset.TypicalId == actualTypicalId
                                   select dataset).AsNoTracking().ToList();
            foreach (var ds in affectedDataset)
            {
                // get the items of the dataset  and modify the row if the attribute is deleted
                var datasetItems = (from dsitem in _dbContext.Set<DataSetItemData>()
                                    where dsitem.DataSetId == ds.Id
                                    select dsitem).AsNoTracking().ToList();
                ArrayList existingJsonRows = new ArrayList();
                foreach (var dsi in datasetItems)
                {
                    if (!string.IsNullOrWhiteSpace(dsi.Row))
                    {
                        var rowValues = JsonConvert.DeserializeObject<List<Attributes>>(dsi.Row);

                        var deleteditems = rowValues.Where(attribute => deletedAttributes.FirstOrDefault(deletedAttr => deletedAttr.Name == attribute.Name) != null);
                        List<Attributes> rowValuesArrayList = new List<Attributes>(rowValues);
                        foreach (var item in deleteditems)
                        {
                            rowValuesArrayList.Remove(item);
                        }
                        var jsonRow = JsonConvert.SerializeObject(rowValuesArrayList);
                        DataSetItem datasetItem = new DataSetItem(dsi.Id, dsi.DataSetId, jsonRow);
                        if (!existingJsonRows.Contains(jsonRow))
                        {
                            _dataSetRepository.UpdateItem(datasetItem);
                            existingJsonRows.Add(jsonRow);
                        }
                        else
                        {
                            _dataSetRepository.DeleteItem(datasetItem.Id);
                        }
                    }
                }
                // update the combination if the attribute is deleted
                if (!string.IsNullOrWhiteSpace(ds.Combinations))
                {
                    var combinations = JsonConvert.DeserializeObject<List<CommandsCombinations>>(ds.Combinations);
                    List<CommandsCombinations> combinationsArrayList = new List<CommandsCombinations>(combinations);
                    var deletedcombinations = combinations.Where(attribute => deletedAttributes.FirstOrDefault(deletedAttr => deletedAttr.Name == attribute.Attribute) != null);
                    foreach (var item in deletedcombinations)
                    {
                        combinationsArrayList.Remove(item);
                    }

                    _dataSetRepository.UpdateCombinations(ds.Id, JsonConvert.SerializeObject(combinationsArrayList));
                }
            }
        }

        private Typical updateTypicalAndTypicalChanges(TypicalData actualTypical, TypicalChangesData typicalChange)
        {
            // make the changes to the typical table
            var updatedtypical = new Typical(actualTypical.Id,
                                         typicalChange.CatalogId,
                                         typicalChange.Title,
                                         typicalChange.Json,
                                         typicalChange.Xml,
                                         typicalChange.XmlHash,
                                         typicalChange.Type);

            _repository.Update(updatedtypical);
            // add the new row to accepted typical change
            _acceptedTypicalChangesRepository.Add(new AcceptedTypicalChanges(actualTypical.Id,
                                                                    actualTypical.Title,
                                                                    actualTypical.Xml,
                                                                    typicalChange.Xml,
                                                                    actualTypical.XmlHash,
                                                                    typicalChange.XmlHash,
                                                                    1,
                                                                    actualTypical.Json,
                                                                    typicalChange.Json));
            // delete the row from typical change
            _typicalChangesRepository.Delete(typicalChange.Id);
            return updatedtypical;
        }
    }
}