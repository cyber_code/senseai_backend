﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Commands.DataModule.Data;
using Commands.DataModule.DataSets;
using DataExtraction;
using SenseAI.Domain.DataModel;
using SenseAI.Domain.DataSetsModel;
using SenseAI.Domain.WorkflowModel;
using Messaging.Commands;
using Persistence.CatalogModel;
using Persistence.DataModel;
using Persistence.DataSetsModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CommandsCombinations = Commands.DataModule.DataSets.Combinations;

namespace Application.Commands.DataModule.Data
{
    public sealed class AcceptTypicalChangesHandler : ICommandHandler<AcceptTypicalChanges, AcceptTypicalChangesResult>
    {
        private readonly SenseAIObjectContext _dbContext;
        private readonly ITypicalRepository _repository;
        private readonly IWorkflowRepository _wfRepository;
        private readonly ITypicalChangesRepository _typicalChangesRepository;
        private readonly IAcceptedTypicalChangesRepository _acceptedTypicalChangesRepository;
        private readonly IDataSetRepository _dataSetRepository;
        private readonly IDataExtractionClient _service;

        public AcceptTypicalChangesHandler(ITypicalRepository repository, SenseAIObjectContext dbContext, IWorkflowRepository wfRepository, ITypicalChangesRepository typicalChangesRepository, IAcceptedTypicalChangesRepository acceptedTypicalChangesRepository, IDataSetRepository dataSetRepository, IDataExtractionClient service)
        {
            _repository = repository;
            _dbContext = dbContext;
            _wfRepository = wfRepository;
            _typicalChangesRepository = typicalChangesRepository;
            _acceptedTypicalChangesRepository = acceptedTypicalChangesRepository;
            _dataSetRepository = dataSetRepository;
            _service = service;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AcceptTypicalChangesResult> Handle(AcceptTypicalChanges command)
        {
            var typicalChangesMetadatas = new List<TypicalChangesMetadata>();

            foreach (var typicalId in command.Ids)
            {
                var typicalChangesMetadata = Execute(typicalId, command.MarkAsInvalid);
                if (typicalChangesMetadata.Id != Guid.Empty)
                    typicalChangesMetadatas.Add(typicalChangesMetadata);
            }
            bool dataextractionError = false;
            string message = "";
            var lists = splitList<TypicalChangesMetadata>(typicalChangesMetadatas);
            foreach (var list in lists)
            {
                try
                {
                    SendToDataExtration(typicalChangesMetadatas);
                }
                catch (Exception ex)
                {
                    dataextractionError = true;
                    message +=" "+ ex.Message;
                    
                }
            }
            if(dataextractionError)
                throw new Exception($"Could not send the typical changes to ML service! error: {message}");
            return new CommandResponse<AcceptTypicalChangesResult>(
                new AcceptTypicalChangesResult(Guid.Empty)
            );
        }
        public static IEnumerable<List<T>> splitList<T>(List<T> locations, int nSize = 100)
        {
            for (int i = 0; i < locations.Count; i += nSize)
            {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }
        public bool Validate(AcceptTypicalChanges command)
        {
            if (command.Ids == null || !command.Ids.Any() || command.Ids.Any(x => x == Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }

        private static void UpdateDynamicData(TypicalData actualTypical, IEnumerable<TypicalChangesAttributes> deletedAttributes, List<Cell> cellsParsed, WorkflowDesignerCellItem parsedCustomData)
        {
            var dynamicDatas = parsedCustomData.DynamicDatas;
            if (dynamicDatas != null && dynamicDatas.Length > 0)
            {
                //if the target typical of the dynamic data is the typical that is changing we see if it use any of the deleted attribute and remove it
                List<WorkflowDesignerCellItemDynamicDatas> dynamicDatasArrayList = new List<WorkflowDesignerCellItemDynamicDatas>(dynamicDatas);
                if (parsedCustomData.TypicalId == actualTypical.Id)
                {
                    var deletedDynamicData = dynamicDatas.Where(attribute => deletedAttributes.FirstOrDefault(deletedAttr => deletedAttr.Name == attribute.TargetAttributeName) != null);

                    foreach (var item in deletedDynamicData)
                    {
                        dynamicDatasArrayList.Remove(item);
                    }
                }
                List<WorkflowDesignerCellItemDynamicDatas> itemToBeDeleted = new List<WorkflowDesignerCellItemDynamicDatas>();
                foreach (var dynamicdata in dynamicDatasArrayList)
                {//if the source typical of the dynamic data is the typical that is changing we see if it use any of the deleted attribute and remove it
                    var linkeditem = cellsParsed.FirstOrDefault(x => x.CustomData?.Id == dynamicdata.SourceWorkflowItemId);
                    if (linkeditem != null && linkeditem.CustomData.TypicalName == actualTypical.Title)
                    {
                        if (deletedAttributes.FirstOrDefault(x => x.Name == dynamicdata.SourceAttributeName) != null)
                        {
                            itemToBeDeleted.Add(dynamicdata);
                        }
                    }
                }
                foreach (var item in itemToBeDeleted)
                {
                    dynamicDatasArrayList.Remove(item);
                }
                parsedCustomData.DynamicDatas = dynamicDatasArrayList.ToArray();
            }
        }

        private static void UpdatePostFilters(TypicalData actualTypical, IEnumerable<TypicalChangesAttributes> deletedAttributes, List<Cell> cellsParsed, WorkflowDesignerCellItem parsedCustomData)
        {
            var postfilters = parsedCustomData.PostConstraints;
            if (postfilters != null && postfilters.Length > 0)
            {     //if the dynamic data of the postfilter uses a deleted attribute we remove it
                var postfiltersWithDynamicData = postfilters.Where(att => !string.IsNullOrWhiteSpace(att.DynamicData?.Path)).ToList();
                foreach (var postfilter in postfiltersWithDynamicData)
                {
                    if (cellsParsed.Where(x => x.CustomData?.Id == postfilter.DynamicData.SourceWorkflowItemId).FirstOrDefault()?.CustomData.TypicalName == actualTypical.Title)
                        if (deletedAttributes.FirstOrDefault(x => x.Name == postfilter.DynamicData.SourceAttributeName) != null)
                        {
                            postfilter.DynamicData = new WorkflowDesignerCellItemDynamicDatas();
                        }
                }

                if (parsedCustomData.TypicalId == actualTypical.Id)
                {   //if the postfilter is using a deleted attribute we remove it
                    var deletedpostfilters = postfilters.Where(attribute => deletedAttributes.FirstOrDefault(deletedAttr => deletedAttr.Name == attribute.AttributeName) != null);
                    List<WorkflowDesignerCellItemConstraint> postfiltersArrayList = new List<WorkflowDesignerCellItemConstraint>(postfilters);
                    foreach (var item in deletedpostfilters)
                    {
                        postfiltersArrayList.Remove(item);
                    }
                    parsedCustomData.PostConstraints = postfiltersArrayList.ToArray();
                }
            }
        }

        private static void UpdateFilters(TypicalData actualTypical, IEnumerable<TypicalChangesAttributes> deletedAttributes, List<Cell> cellsParsed, WorkflowDesignerCellItem parsedCustomData)
        {
            var filters = parsedCustomData.Constraints;
            if (filters != null && filters.Length > 0)
            {
                //if the dynamic data of the filter uses a deleted attribute we remove it
                var filtersWithDynamicData = filters.Where(att => !string.IsNullOrWhiteSpace(att.DynamicData?.Path)).ToList();
                foreach (var filter in filtersWithDynamicData)
                {
                    if (cellsParsed.Where(x => x.CustomData?.Id == filter.DynamicData.SourceWorkflowItemId).FirstOrDefault()?.CustomData.TypicalName == actualTypical.Title)
                        if (deletedAttributes.FirstOrDefault(x => x.Name == filter.DynamicData.SourceAttributeName) != null)
                        {
                            filter.DynamicData = new WorkflowDesignerCellItemDynamicDatas();
                        }
                }
                if (parsedCustomData.TypicalId == actualTypical.Id)
                {
                    //if the filter is using a deleted attribute we remove it
                    var deletedfilters = filters.Where(attribute => deletedAttributes.FirstOrDefault(deletedAttr => deletedAttr.Name == attribute.AttributeName) != null);

                    List<WorkflowDesignerCellItemConstraint> filtersArrayList = new List<WorkflowDesignerCellItemConstraint>(filters);
                    foreach (var item in deletedfilters)
                    {
                        filtersArrayList.Remove(item);
                    }

                    parsedCustomData.Constraints = filtersArrayList.ToArray();
                }
            }
        }

        private TypicalChangesMetadata Execute(Guid id, bool markAsInvalid)
        {
            var typicalChange = (from typical in _dbContext.Set<TypicalChangesData>()
                                 where typical.Id == id
                                 select typical).AsNoTracking().FirstOrDefault();
            if (typicalChange == null)
            {
                return new TypicalChangesMetadata();
            }
            var actualTypical = (from typical in _dbContext.Set<TypicalData>()
                                 where typical.Title == typicalChange.Title
                                 && typical.CatalogId == typicalChange.CatalogId
                                 select typical).AsNoTracking().FirstOrDefault();

            var actualCatalog = (from catalog in _dbContext.Set<CatalogData>()
                                 where catalog.Id == typicalChange.CatalogId
                                 select catalog).AsNoTracking().FirstOrDefault();

            var typicalChangeMetadata = JsonConvert.DeserializeObject<TypicalChangesMetadata>(typicalChange.Json);
            var typicalChangeAttributes = typicalChangeMetadata.Attributes;
            var typicalAttributes = JsonConvert.DeserializeObject<TypicalChangesMetadata>(actualTypical.Json).Attributes;

            var newAttributes = (typicalAttributes == null) ? typicalChangeAttributes : typicalChangeAttributes.Except(typicalAttributes, new AttributeNameComparer());
            var deletedAttributes = (typicalAttributes == null) ? new List<TypicalChangesAttributes>() : typicalAttributes.Except(typicalChangeAttributes, new AttributeNameComparer());
            var changedAttributes = (typicalAttributes == null) ? new List<TypicalChangesAttributes>() : typicalChangeAttributes.Except(typicalAttributes, new AttributeComparer())
                                                           .Except(newAttributes, new AttributeNameComparer());

            updateAffectedWorkflow(markAsInvalid, typicalChange, actualTypical, actualCatalog, deletedAttributes);

            updateDataSetsObject(actualTypical.Id, deletedAttributes);

            var updatedtypical = updateTypicalAndTypicalChanges(actualTypical, typicalChange);

            return typicalChangeMetadata;
        }

        private void SendToDataExtration(IEnumerable<TypicalChangesMetadata> typicalChangesMetadatas)
        {

            var acceptTypicalModelRequest = typicalChangesMetadatas.Select(x =>
                                            new DataExtraction.AcceptTypicalChangesModel.AcceptTypicalChangesModel
                                            {
                                                TenantId = x.TenantId,
                                                ProjectId = x.ProjectId,
                                                SubProjectId = x.SubProjectId,
                                                CatalogId = x.CatalogId,
                                                SystemId = x.SystemId,

                                                Id = x.Id,
                                                Type = x.Type,
                                                Title = x.Title,
                                                HasAutomaticId = x.HasAutomaticId,
                                                IsConfiguration = x.IsConfiguration,
                                                Attributes = x.Attributes 
                                            });

            try
            {
                var result = _service.AcceptedTypicalChanges(acceptTypicalModelRequest).Result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void updateAffectedWorkflow(bool markAsInvalid, TypicalChangesData typicalChange, TypicalData actualTypical, CatalogData actualCatalog, IEnumerable<TypicalChangesAttributes> deletedAttributes)
        {
            var listOfAffectedWorkflowsQuery = (from workflow in _dbContext.Set<WorkflowData>()
                                                join workflowitem in _dbContext.Set<WorkflowItemData>() on workflow.Id equals workflowitem.WorkflowId
                                                where workflowitem.TypicalTitle == typicalChange.Title
                                                && workflowitem.CatalogTitle == actualCatalog.Title
                                                select workflow)
                                                .AsNoTracking()
                                                .Distinct()
                                                .ToList();

            foreach (var wf in listOfAffectedWorkflowsQuery)
            {
                var designerJson = JObject.Parse(wf.DesignerJson);
                var cells = (JArray)designerJson["cells"];
                if (cells != null)
                {
                    var cellsParsed = cells.ToObject<List<Cell>>();

                    foreach (var cell in cells)
                    {
                        var customData = cell.SelectToken("customData");
                        if (customData != null)
                        {
                            var parsedCustomData = customData.ToObject<WorkflowDesignerCellItem>();

                            //filters
                            UpdateFilters(actualTypical, deletedAttributes, cellsParsed, parsedCustomData);
                            //postfilters
                            UpdatePostFilters(actualTypical, deletedAttributes, cellsParsed, parsedCustomData);
                            //dynamic data
                            UpdateDynamicData(actualTypical, deletedAttributes, cellsParsed, parsedCustomData);
                            cell["customData"] = JObject.Parse(JsonConvert.SerializeObject(parsedCustomData));
                        }
                    }
                }
                _wfRepository.Update(wf.Id, designerJson.ToString(), markAsInvalid);
            }
        }

        private void updateDataSetsObject(Guid actualTypicalId, IEnumerable<TypicalChangesAttributes> deletedAttributes)
        {
            // get dataset that use this typical
            var affectedDataset = (from dataset in _dbContext.Set<DataSetData>()
                                   where dataset.TypicalId == actualTypicalId
                                   select dataset).AsNoTracking().ToList();
            foreach (var ds in affectedDataset)
            {
                // get the items of the dataset  and modify the row if the attribute is deleted
                var datasetItems = (from dsitem in _dbContext.Set<DataSetItemData>()
                                    where dsitem.DataSetId == ds.Id
                                    select dsitem).AsNoTracking().ToList();
                ArrayList existingJsonRows = new ArrayList();
                foreach (var dsi in datasetItems)
                {
                    if (!string.IsNullOrWhiteSpace(dsi.Row))
                    {
                        var rowValues = JsonConvert.DeserializeObject<List<Attributes>>(dsi.Row);

                        var deleteditems = rowValues.Where(attribute => deletedAttributes.FirstOrDefault(deletedAttr => deletedAttr.Name == attribute.Name) != null);
                        List<Attributes> rowValuesArrayList = new List<Attributes>(rowValues);
                        foreach (var item in deleteditems)
                        {
                            rowValuesArrayList.Remove(item);
                        }
                        var jsonRow = JsonConvert.SerializeObject(rowValuesArrayList);
                        DataSetItem datasetItem = new DataSetItem(dsi.Id, dsi.DataSetId, jsonRow);
                        if (!existingJsonRows.Contains(jsonRow))
                        {
                            _dataSetRepository.UpdateItem(datasetItem);
                            existingJsonRows.Add(jsonRow);
                        }
                        else
                        {
                            _dataSetRepository.DeleteItem(datasetItem.Id);
                        }
                    }
                }
                // update the combination if the attribute is deleted
                if (!string.IsNullOrWhiteSpace(ds.Combinations))
                {
                    var combinations = JsonConvert.DeserializeObject<List<CommandsCombinations>>(ds.Combinations);
                    List<CommandsCombinations> combinationsArrayList = new List<CommandsCombinations>(combinations);
                    var deletedcombinations = combinations.Where(attribute => deletedAttributes.FirstOrDefault(deletedAttr => deletedAttr.Name == attribute.Attribute) != null);
                    foreach (var item in deletedcombinations)
                    {
                        combinationsArrayList.Remove(item);
                    }

                    _dataSetRepository.UpdateCombinations(ds.Id, JsonConvert.SerializeObject(combinationsArrayList));
                }
            }
        }

        private Typical updateTypicalAndTypicalChanges(TypicalData actualTypical, TypicalChangesData typicalChange)
        {
            // make the changes to the typical table
            var updatedtypical = new Typical(actualTypical.Id,
                                         typicalChange.CatalogId,
                                         typicalChange.Title,
                                         typicalChange.Json,
                                         typicalChange.Xml,
                                         typicalChange.XmlHash,
                                         typicalChange.Type);

            _repository.Update(updatedtypical);
            // add the new row to accepted typical change
            _acceptedTypicalChangesRepository.Add(new AcceptedTypicalChanges(actualTypical.Id,
                                                                    actualTypical.Title,
                                                                    actualTypical.Xml,
                                                                    typicalChange.Xml,
                                                                    actualTypical.XmlHash,
                                                                    typicalChange.XmlHash,
                                                                    1,
                                                                    actualTypical.Json,
                                                                    typicalChange.Json));
            // delete the row from typical change
            _typicalChangesRepository.Delete(typicalChange.Id);
            return updatedtypical;
        }
    }
}