﻿using Commands.DataModule.Data;
using Messaging.Commands;
using Newtonsoft.Json;
using SenseAI.Domain;
using SenseAI.Domain.DataModel;
using System;
using System.Collections.Generic; 
using System.Linq;

namespace Application.Commands.DataModule.Data
{
    public sealed class AddTypicalAttributeManuallyHandler : ICommandHandler<AddTypicalAttributeManually, AddTypicalAttributeManuallyResult>
    {
        private readonly ITypicalRepository _typicalRepository;

        public AddTypicalAttributeManuallyHandler(ITypicalRepository typicalRepository)
        {
            _typicalRepository = typicalRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddTypicalAttributeManuallyResult> Handle(AddTypicalAttributeManually command)
        {
            // get existing typical
            var existingtypical = _typicalRepository.GetTypical(command.TypicalId);
            var typicalChangeMetadata = JsonConvert.DeserializeObject<TypicalChangesMetadata>(existingtypical.Json);

            //create new attribute
            var newAttribute = new TypicalChangesAttributes(command.Name, command.IsMultiValue, command.PossibleValues, command.IsRequired, command.IsNoChange, command.IsExternal,
                command.MinimumLength, command.MaximumLength, command.RelatedApplicationName, command.ExtraData, command.RelatedApplicationIsConfiguration);

            List<TypicalChangesAttributes> typicalAttributes = new List<TypicalChangesAttributes>(typicalChangeMetadata.Attributes ?? new TypicalChangesAttributes[] { });

            //Check if attribute name exists
            if (!typicalAttributes.Any(t => t.Name == newAttribute.Name))
            {
                typicalAttributes.Add(newAttribute);
                typicalChangeMetadata.Attributes = typicalAttributes.ToArray();

                var jsonRow = JsonConvert.SerializeObject(typicalChangeMetadata);
                var xml = JsonConvert.DeserializeXmlNode(jsonRow, "row");
                string xmlHash = new MD5HashManager().ComputeStringHash(xml.OuterXml);

                Typical typical = new Typical(existingtypical.Id, existingtypical.CatalogId, existingtypical.Title, jsonRow, xml.OuterXml, xmlHash, existingtypical.Type);
                _typicalRepository.Update(typical);
            }

            return new CommandResponse<AddTypicalAttributeManuallyResult>(
                new AddTypicalAttributeManuallyResult(existingtypical.Id)
            );
        }

        public bool Validate(AddTypicalAttributeManually command)
        {
            if (command.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Name))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
