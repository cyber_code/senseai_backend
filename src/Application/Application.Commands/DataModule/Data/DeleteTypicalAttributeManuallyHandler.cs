﻿using Commands.DataModule.Data;
using Messaging.Commands;
using Newtonsoft.Json;
using SenseAI.Domain;
using SenseAI.Domain.DataModel;
using System;
using System.Collections.Generic;
using System.Linq; 

namespace Application.Commands.DataModule.Data
{
    public sealed class DeleteTypicalAttributeManuallyHandler : ICommandHandler<DeleteTypicalAttributeManually, DeleteTypicalAttributeManuallyResult>
    {
        private readonly ITypicalRepository _typicalRepository;
        private readonly ITypicalChangesRepository _typicalChangesRepository;

        public DeleteTypicalAttributeManuallyHandler(ITypicalRepository typicalRepository, ITypicalChangesRepository typicalChangesRepository)
        {
            _typicalRepository = typicalRepository;
            _typicalChangesRepository = typicalChangesRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<DeleteTypicalAttributeManuallyResult> Handle(DeleteTypicalAttributeManually command)
        {
            // get existing typical
            var existingTypical = _typicalRepository.GetTypical(command.TypicalId);
            var typicalChangeMetadata = JsonConvert.DeserializeObject<TypicalChangesMetadata>(existingTypical.Json);

            //get attribute by name
            List<TypicalChangesAttributes> typicalChangeAttributes = new List<TypicalChangesAttributes>(typicalChangeMetadata.Attributes);
            var deletedAttribute = typicalChangeAttributes.FirstOrDefault(t => t.Name == command.Name);

            if (deletedAttribute != null && typicalChangeAttributes.Remove(deletedAttribute))
            {                
                typicalChangeMetadata.Attributes = typicalChangeAttributes.ToArray();

                var jsonRow = JsonConvert.SerializeObject(typicalChangeMetadata);
                var xml = JsonConvert.DeserializeXmlNode(jsonRow, "row");
                var xmlHash = new MD5HashManager().ComputeStringHash(xml.OuterXml);

                if (command.IsTypicalChange)
                {
                    TypicalChanges typicalChange = new TypicalChanges(existingTypical.Id, existingTypical.CatalogId, existingTypical.Title, jsonRow, xml.OuterXml, xmlHash, existingTypical.Type, 1);
                    _typicalChangesRepository.Add(typicalChange);
                }
                else
                {
                    Typical typical = new Typical(existingTypical.Id, existingTypical.CatalogId, existingTypical.Title, jsonRow, xml.OuterXml, xmlHash, existingTypical.Type);
                    _typicalRepository.Update(typical);
                }
            }

            return new CommandResponse<DeleteTypicalAttributeManuallyResult>(
                 new DeleteTypicalAttributeManuallyResult(command.TypicalId)
            );
        }

        public bool Validate(DeleteTypicalAttributeManually command)
        {
            if (command.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Name))
            {
                Errors.Add("Name cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
