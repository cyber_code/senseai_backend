﻿using Commands.DataModule.Data;
using Messaging.Commands;
using SenseAI.Domain;
using SenseAI.Domain.DataModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.DataModule.Data
{
    public sealed class UpdateTypicalManuallyHandler : ICommandHandler<UpdateTypicalManually, bool>
    {
        private readonly ITypicalRepository _typicalRepository;
        public UpdateTypicalManuallyHandler(ITypicalRepository typicalRepository)
        {
            _typicalRepository = typicalRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateTypicalManually command)
        {
            var typical = _typicalRepository.GetTypical(command.Id);

            if (_typicalRepository.CheckIfTypicalExist(typical.CatalogId, command.Title))
            {
                string message = "Typical title already exist in this catalog! Please choose a different one!";
                return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Failed, message));
            }

            typical.SetTitle(command.Title);
            _typicalRepository.Update(typical);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateTypicalManually command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
