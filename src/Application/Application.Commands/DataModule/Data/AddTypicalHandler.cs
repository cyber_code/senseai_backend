﻿using Commands.DataModule.Data;
using SenseAI.Domain.DataModel;
using Messaging.Commands;
using System.Collections.Generic;

namespace Application.Commands.DataModule.Data
{
    public sealed class AddTypicalHandler : ICommandHandler<AddTypical, AddTypicalResult>
    {
        private readonly ITypicalRepository _repository;

        public AddTypicalHandler(ITypicalRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddTypicalResult> Handle(AddTypical command)
        {
            Typical typical = new Typical(command.CatalogId, command.Title, command.Json, command.Xml, command.XmlHash, command.Type);

            _repository.Add(typical);

            return new CommandResponse<AddTypicalResult>(
                new AddTypicalResult(typical.Id)
            );
        }

        public bool Validate(AddTypical command)
        {
            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.Json))
            {
                Errors.Add("Json cannot be empty.");
                return false;
            }

            return true;
        }
    }
}