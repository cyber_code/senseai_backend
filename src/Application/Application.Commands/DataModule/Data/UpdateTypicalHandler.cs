﻿using Commands.DataModule.Data;
using SenseAI.Domain.DataModel;
using Messaging.Commands;
using System.Collections.Generic;

namespace Application.Commands.DataModule.Data
{
    public class UpdateTypicalHandler : ICommandHandler<UpdateTypical, UpdateTypicalResult>
    {
        private readonly ITypicalRepository _repository;

        public UpdateTypicalHandler(ITypicalRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<UpdateTypicalResult> Handle(UpdateTypical command)
        {
            var existingtypical = _repository.GetTypical(command.Id);
            Typical typical = new Typical(command.Id, existingtypical.CatalogId, command.Title, command.Json, command.Xml, command.XmlHash, command.Type);
            _repository.Update(typical);
            return new CommandResponse<UpdateTypicalResult>(
                   new UpdateTypicalResult(command.Id)
               );
        }

        public bool Validate(UpdateTypical command)
        {
            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.Json))
            {
                Errors.Add("Json cannot be empty.");
                return false;
            }
            return true;
        }
    }
}