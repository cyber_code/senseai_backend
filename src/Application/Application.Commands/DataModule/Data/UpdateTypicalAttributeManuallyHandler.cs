﻿using Commands.DataModule.Data;
using Messaging.Commands;
using Newtonsoft.Json;
using SenseAI.Domain;
using SenseAI.Domain.DataModel;
using System;
using System.Collections.Generic;
using System.Linq; 

namespace Application.Commands.DataModule.Data
{
    public sealed class UpdateTypicalAttributeManuallyHandler : ICommandHandler<UpdateTypicalAttributeManually, UpdateTypicalAttributeManuallyResult>
    {
        private readonly ITypicalRepository _typicalRepository;
        private readonly ITypicalChangesRepository _typicalChangesRepository;

        public UpdateTypicalAttributeManuallyHandler(ITypicalRepository typicalRepository, ITypicalChangesRepository typicalChangesRepository)
        {
            _typicalRepository = typicalRepository;
            _typicalChangesRepository = typicalChangesRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<UpdateTypicalAttributeManuallyResult> Handle(UpdateTypicalAttributeManually command)
        {
            // get existing typical
            var existingTypical = _typicalRepository.GetTypical(command.TypicalId);
            var typicalChangeMetadata = JsonConvert.DeserializeObject<TypicalChangesMetadata>(existingTypical.Json);

            //get attribute by name
            var typicalChangeAttributes = typicalChangeMetadata.Attributes;

            var changedAttribute = typicalChangeAttributes.FirstOrDefault(t => t.Name == command.Name);
            if(changedAttribute != null)
            {
                changedAttribute.Name = command.NewName;
                changedAttribute.IsMultiValue = command.IsMultiValue;
                changedAttribute.PossibleValues = command.PossibleValues;
                changedAttribute.IsRequired = command.IsRequired;
                changedAttribute.IsNoChange = command.IsNoChange;
                changedAttribute.IsExternal = command.IsExternal;
                changedAttribute.MinimumLength = command.MinimumLength;
                changedAttribute.MaximumLength = command.MaximumLength;
                changedAttribute.RelatedApplicationName = command.RelatedApplicationName;
                changedAttribute.ExtraData = command.ExtraData;
                changedAttribute.RelatedApplicationIsConfiguration = command.RelatedApplicationIsConfiguration;

                var jsonRow = JsonConvert.SerializeObject(typicalChangeMetadata);
                var xml = JsonConvert.DeserializeXmlNode(jsonRow, "row");
                var xmlHash = new MD5HashManager().ComputeStringHash(xml.OuterXml);

                if(command.IsTypicalChange)
                {
                    TypicalChanges typicalChange = new TypicalChanges(existingTypical.Id, existingTypical.CatalogId, existingTypical.Title, jsonRow, xml.OuterXml, xmlHash, existingTypical.Type, 1);
                    _typicalChangesRepository.Add(typicalChange);
                }
                else
                {
                    Typical typical = new Typical(existingTypical.Id, existingTypical.CatalogId, existingTypical.Title, jsonRow, xml.OuterXml, xmlHash, existingTypical.Type);
                    _typicalRepository.Update(typical);
                }
            }

            return new CommandResponse<UpdateTypicalAttributeManuallyResult>(
                new UpdateTypicalAttributeManuallyResult(existingTypical.Id)
            );
        }

        public bool Validate(UpdateTypicalAttributeManually command)
        {
            if (command.TypicalId == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Name))
            {
                Errors.Add("Attribute name cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
