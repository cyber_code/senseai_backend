﻿using Commands.DataModule.Data;
using Core.Abstractions;
using Messaging.Commands;
using Newtonsoft.Json;
using SenseAI.Domain;
using SenseAI.Domain.DataModel;
using System;
using System.Collections.Generic; 

namespace Application.Commands.DataModule.Data
{
    public sealed class AddTypicalManuallyHandler : ICommandHandler<AddTypicalManually, AddTypicalManuallyResult>
    {
        private readonly ITypicalRepository _typicalRepository;

        public AddTypicalManuallyHandler(ITypicalRepository typicalRepository)
        {
            _typicalRepository = typicalRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddTypicalManuallyResult> Handle(AddTypicalManually command)
        {
            if (_typicalRepository.CheckIfTypicalExist(command.CatalogId, command.Title))
            {
                string message = "Typical already exist in this catalog!";
                return new CommandResponse<AddTypicalManuallyResult>(new AddTypicalManuallyResult(Guid.Empty),
                    new CommandResponseMessage((int)MessageType.Failed, message));
            }

            var jsonRow = JsonConvert.SerializeObject(command);
            var xml = JsonConvert.DeserializeXmlNode(jsonRow, "row");
            var xmlHash = new MD5HashManager().ComputeStringHash(xml.OuterXml);

            Typical newTypical = new Typical(command.CatalogId, command.Title, jsonRow, xml.OuterXml, xmlHash, command.Type);
            _typicalRepository.Add(newTypical);

            return new CommandResponse<AddTypicalManuallyResult>(new AddTypicalManuallyResult(newTypical.Id));
        }

        public bool Validate(AddTypicalManually command)
        {
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }

            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }

            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("Catalog cannot be empty.");
                return false;
            }

            if (command.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;

            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("Catalog cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
