﻿using Commands.DataModule.Data;
using SenseAI.Domain.DataModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.DataModule.Data
{
    public sealed class DeleteTypicalHandler : ICommandHandler<DeleteTypical, DeleteTypicalResult>
    {
        private readonly ITypicalRepository _repository;

        public DeleteTypicalHandler(ITypicalRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<DeleteTypicalResult> Handle(DeleteTypical command)
        {
            _repository.Delete(command.Id);
            return new CommandResponse<DeleteTypicalResult>(
                 new DeleteTypicalResult(command.Id)

            );
        }

        public bool Validate(DeleteTypical command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}