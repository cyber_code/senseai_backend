﻿using Commands.DataModule.Data;
using Messaging.Commands;
using Newtonsoft.Json;
using Persistence.DataModel;
using Persistence.Internal;
using Persistence.WorkflowModel;
using SenseAI.Domain;
using SenseAI.Domain.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Commands.DataModule.Data
{
    public sealed class DeleteTypicalManuallyHandler : ICommandHandler<DeleteTypicalManually, bool>
    {
        private readonly ITypicalRepository _typicalRepository;
        private readonly ITypicalChangesRepository _typicalRepositoryChanges;
        private readonly SenseAIObjectContext _context;
        public DeleteTypicalManuallyHandler(ITypicalRepository typicalRepository, ITypicalChangesRepository typicalChangesRepository, SenseAIObjectContext context)
        {
            _typicalRepository = typicalRepository;
            _typicalRepositoryChanges = typicalChangesRepository;
            _context = context;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteTypicalManually command)
        {

            var typical = _typicalRepository.GetTypical(command.Id);            
            var workflowItem = _context.Set<WorkflowItemData>().FirstOrDefault(w => w.TypicalTitle == typical.Title);

            if (workflowItem != null)
                return new CommandResponse<bool>(false, new CommandResponseMessage((int)MessageType.Failed, $"The typical: {typical.Title} is used on workflows, so it cannot be deleted."));
            _typicalRepository.Delete(command.Id);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteTypicalManually command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("TypicalId cannot be empty.");
                return false;
            }
            return true;
        }

    }
}
