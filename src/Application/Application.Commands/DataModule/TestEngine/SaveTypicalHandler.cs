﻿using SenseAI.Domain.DataModel;
using Messaging.Commands;
using System.Collections.Generic;
using Commands.DataModule.TestEngine;
using System;
using Newtonsoft.Json;
using SenseAI.Domain;
using DataExtraction;
using DataExtraction.AcceptTypicalChangesModel;
using System.Linq;
using SenseAI.Domain.CatalogModel;

namespace Application.Commands.DataModule.TestEngine
{
    public sealed class SaveTypicalHandler : ICommandHandler<SaveTypical, bool>
    {
        private readonly ITypicalRepository _typicalRepository;
        private readonly ITypicalChangesRepository _typicalChangesRepository;
        private readonly ICatalogRepository _catalogRepository;
        private readonly IDataExtractionClient _service;

        public SaveTypicalHandler(ITypicalRepository typicalRepository, ITypicalChangesRepository typicalChangesRepository, ICatalogRepository catalogRepository, IDataExtractionClient service)
        {
            _typicalRepository = typicalRepository;
            _typicalChangesRepository = typicalChangesRepository;
            _catalogRepository = catalogRepository;
            _service = service;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(SaveTypical command)
        {
            var jsonRow = JsonConvert.SerializeObject(command);
            var xml = JsonConvert.DeserializeXmlNode(jsonRow, "row");
            var xmlHash = new MD5HashManager().ComputeStringHash(xml.OuterXml);
            int typicalType = command.TypicalType;
            if (typicalType == (int)TypicalType.Version)
                typicalType = (int)TypicalType.Application;
            var catalog = _catalogRepository.GetCatalogs(command.SubProjectId, command.SystemId).FirstOrDefault(w => w.Type == typicalType);
            command.CatalogId = catalog.Id;
            Typical typical = _typicalRepository.GetTypical(catalog.Id, command.TypicalName, (TypicalType)command.TypicalType);

            var typicalChangeAttributes = JsonConvert.DeserializeObject<TypicalChangesMetadata>(jsonRow).Attributes;
            TypicalChangesMetadata currentTypical = null;
            if (typical != null)
                currentTypical = JsonConvert.DeserializeObject<TypicalChangesMetadata>(typical.Json);
            var typicalAttributes = currentTypical == null ? null : currentTypical.Attributes;

            List<TypicalChangesAttributes> newAttributes = new List<TypicalChangesAttributes>();
            List<TypicalChangesAttributes> deleteAttributes = new List<TypicalChangesAttributes>();

            if (typicalChangeAttributes != null && typicalAttributes != null)
                newAttributes = typicalChangeAttributes.Except(typicalAttributes, new AttributeNameComparer()).ToList();

            var newTypical = new Typical(command.CatalogId, command.TypicalName, jsonRow, xml.OuterXml, xmlHash, (TypicalType)command.TypicalType);

            AcceptTypicalChangesModel acceptTypicalModelRequest = new AcceptTypicalChangesModel
            {
                TenantId = command.TenantId,
                ProjectId = command.ProjectId,
                SubProjectId = command.SubProjectId,
                CatalogId = command.CatalogId,
                SystemId = command.SystemId, 

                Id = newTypical.Id,
                Type = (int)newTypical.Type,
                Title = newTypical.Title,
                HasAutomaticId = false,
                IsConfiguration = false,
                Attributes = command.Attributes.Select(s => new TypicalChangesAttributes(s.Name, s.IsMultiValue, s.PossibleValues, s.IsRequired,
                        s.IsNoChange, s.IsExternal, s.MinimumLength, s.MaximumLength, s.RelatedApplicationName, s.ExtraData, s.RelatedApplicationIsConfiguration)
               ).ToArray()
            };


            if (typical != null && newAttributes.Count() > 0)
            {
                var attributes = currentTypical.Attributes.ToList();

                newAttributes.ForEach(f =>
                {
                    attributes.Add(f);
                });

                currentTypical.Attributes = attributes.ToArray();
                jsonRow = JsonConvert.SerializeObject(currentTypical);
                xml = JsonConvert.DeserializeXmlNode(jsonRow, "row");
                xmlHash = new MD5HashManager().ComputeStringHash(xml.OuterXml);

                typical = new Typical(typical.Id, command.CatalogId, command.TypicalName, jsonRow, xml.OuterXml, xmlHash, (TypicalType)command.TypicalType);
                _typicalRepository.Update(typical);
                _service.AcceptedTypicalChanges(acceptTypicalModelRequest);
            }

            if (typical == null)
            {
                _typicalRepository.Add(newTypical);
                _service.AcceptedTypicalChanges(acceptTypicalModelRequest);
            }

            return new CommandResponse<bool>(true);
        }

        public bool Validate(SaveTypical command)
        {
            if (string.IsNullOrWhiteSpace(command.TypicalName))
            {
                Errors.Add("TypicalName cannot be empty.");
                return false;
            }

            return true;
        }
    }
}