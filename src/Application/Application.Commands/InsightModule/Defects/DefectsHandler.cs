﻿using Microsoft.AspNetCore.Http;
using DataExtraction;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.InsightModule.Defects
{
    public sealed class DefectsHandler : ICommandHandler<DefectsModel, bool>
    {
        private readonly IDataExtractionClient _service;

        public DefectsHandler(IDataExtractionClient service)
        {
            _service = service;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DefectsModel command)
        {
            var respond = false;
            var mapp = new List<DataExtraction.Defects.ColumnMappingModel>();
            mapp = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DataExtraction.Defects.ColumnMappingModel>>(command.Mapping);
            DataExtraction.Defects.DefectsModel defectModelRequest = new DataExtraction.Defects.DefectsModel
            {
                TenantId = command.TenantId,
                ProjectId = command.ProjectId,
                SubProjectId = command.SubProjectId,
                CatalogId = command.CatalogId,
                Files = command.Files,
                Mapping = mapp,
                Lang = command.Lang
            };
            respond = _service.SaveDefects(defectModelRequest).Result;

            return new CommandResponse<bool>(respond);
        }

        public bool Validate(DefectsModel command)
        {
            if (command.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            return true;
        }
    }

    public sealed class DefectsModel : ICommand<bool>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public IFormFile Files { get; set; }

        public String Mapping { get; set; }

        public string Lang { get; set; }
    }

    public class ColumnMappingModel
    {
        public string OriginalField { get; set; }

        public string CsvField { get; set; }

        public int Index { get; set; }
    }
}