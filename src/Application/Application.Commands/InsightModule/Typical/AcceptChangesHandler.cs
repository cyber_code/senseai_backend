﻿using Microsoft.AspNetCore.Http;
using DataExtraction;
using DataExtraction.AcceptTypicalChangesModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.InsightModule.Defects
{
    public sealed class AcceptedChangesHandler : ICommandHandler<AcceptTypicalChangesModel, bool>
    {
        private readonly IDataExtractionClient _service;

        public AcceptedChangesHandler(IDataExtractionClient service)
        {
            _service = service;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(AcceptTypicalChangesModel command)
        {
            DataExtraction.AcceptTypicalChangesModel.AcceptTypicalChangesModel acceptTypicalModelRequest = new DataExtraction.AcceptTypicalChangesModel.AcceptTypicalChangesModel
            {
                TenantId = command.TenantId,
                ProjectId = command.ProjectId,
                SubProjectId = command.SubProjectId,
                CatalogId = command.CatalogId, 
                Title = command.Title,
                Id = command.Id,
                Type = command.Type,
                Attributes = command.Attributes.Select(s => new SenseAI.Domain.DataModel.TypicalChangesAttributes(
                    s.Name, s.IsMultiValue, s.PossibleValues, s.IsRequired, s.IsNoChange, s.IsExternal, s.MinimumLength,
                    s.MaximumLength, s.RelatedApplicationName, s.ExtraData, s.RelatedApplicationIsConfiguration

                    )).ToArray()
            };
            bool respond = _service.AcceptedTypicalChanges(acceptTypicalModelRequest).Result;
            return new CommandResponse<bool>(respond);
        }

        public bool Validate(AcceptTypicalChangesModel command)
        {
            if (command.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            return true;
        }
    }

    public class AcceptTypicalChangesModel : ICommand<bool>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid Id { get; set; }

        public int Type { get; set; }

        public string Title { get; set; }

        public bool HasAutomaticId { get; set; }

        public bool IsConfiguration { get; set; }

        public TypicalChangesAttributes[] Attributes { get; set; }
    }

    public sealed class TypicalChangesAttributes
    {
        public string Name { get; set; }

        public bool IsMultiValue { get; set; }

        public string[] PossibleValues { get; set; }

        public bool IsRequired { get; set; }

        public bool IsNoInput { get; set; }

        public bool IsNoChange { get; set; }

        public bool IsExternal { get; set; }

        public int MinimumLength { get; set; }

        public int MaximumLength { get; set; }

        public string RelatedApplicationName { get; set; }

        public string ExtraData { get; set; }

        public bool RelatedApplicationIsConfiguration { get; set; }
    }
}