﻿using Microsoft.AspNetCore.Http;
using DataExtraction;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Persistence.Internal;
using Persistence.Process.SprintsModel;
using System.Linq;
using DataExtraction.ExecutionLogsModel;

namespace Application.Commands.InsightModule.ExecutionLogs
{
    public sealed class ExecutionLogsHandler : ICommandHandler<ExecutionsModel, bool>
    {
        private readonly IDataExtractionClient _service;
        private readonly IDbContext _dbContext;

        public ExecutionLogsHandler(IDataExtractionClient service, IDbContext dbContext)
        {
            _service = service;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(ExecutionsModel command)
        {
            var respond = false;
            var sprintId = _dbContext.Set<SprintsData>().Where(sd => sd.SubProjectId == command.SubProjectId & sd.Status == 1).Select(sd => sd.Id).FirstOrDefault();
            if (sprintId == null || sprintId == Guid.Empty)
            {
                return new CommandResponse<bool>(respond, new CommandResponseMessage(3, "There is no active sprint for this subproject"));
            }

           ExecutionLogsModel defectModelRequest = new ExecutionLogsModel
            {
                TenantId = command.TenantId,
                ProjectId = command.ProjectId,
                SubProjectId = command.SubProjectId,
                CatalogId = command.CatalogId,
                WorkflowId = command.WorkflowId,
                SprintId = sprintId,
                Files = command.Files
            };

            var response =  _service.ExecutionLogs(defectModelRequest).Result;
            return new CommandResponse<bool>(response.Successful);
        }

        public bool Validate(ExecutionsModel command)
        {
            if (command.TenantId == Guid.Empty)
            {
                Errors.Add("TenantId cannot be empty.");
                return false;
            }
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            if (command.Files == null)
            {
                Errors.Add("Files cannot be empty.");
                return false;
            }
            return true;
        }
    }

    public sealed class ExecutionsModel : ICommand<bool>
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get; set; }

        public Guid CatalogId { get; set; }

        public Guid WorkflowId { get; set; }

        public string HashId
        {
            get
            {
                return BitConverter.ToString(MD5.Create()
                    .ComputeHash(UTF8Encoding.UTF8.GetBytes(
                        TenantId.ToString() + "|" +
                        ProjectId.ToString() + "|" +
                        SubProjectId.ToString() + "|" +
                        SystemId.ToString() + "|" +
                        CatalogId.ToString()))).Replace("-", string.Empty);
            }
        }

        public IFormFileCollection Files { get; set; }
    }
   
}