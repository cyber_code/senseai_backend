﻿using Commands.AdministrationModule.SystemCatalogs;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.SystemCatalogModel;
using SenseAI.Domain;
using SenseAI.Domain.AdminModel;
using SenseAI.Domain.SystemCatalogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.AdministrationModule.SystemCatalogs
{
    public sealed class UpdateSystemCatalogsHandler : ICommandHandler<UpdateSystemCatalog, bool>
    {
        private readonly ISystemCatalogRepository _repository;
        private readonly IDbContext _dbContext;

        public UpdateSystemCatalogsHandler(ISystemCatalogRepository repository, IDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateSystemCatalog command)
        {
            var query = _dbContext.Set<SystemCatalogData>().AsNoTracking().FirstOrDefault(ct => ct.SubprojectId == command.SubProjectId
          && ct.SystemId == command.SystemId && ct.CatalogId == command.CatalogId);//TODO: && ct.CatalogId == command.CatalogId when we activate single catalog remove this condition

            if (query != null)
            {
                return new CommandResponse<bool>(false,
                     new CommandResponseMessage((int)MessageType.Failed, $"This system is already defined in this Catalog"));
            }

            SystemCatalog systemCatalogs = new SystemCatalog(command.Id, command.SubProjectId, command.SystemId, command.CatalogId);
            _repository.Update(systemCatalogs);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateSystemCatalog command)
        {
            if (command.Id ==  null|| command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            if (command.SubProjectId == null || command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            if (command.CatalogId == null || command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty!");
                return false;
            }
            if (command.SystemId == null || command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}