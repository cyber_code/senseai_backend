﻿using Commands.AdministrationModule.SystemCatalogs;
using Messaging.Commands;
using SenseAI.Domain.SystemCatalogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.AdministrationModule.SystemCatalogs
{
    class DeleteSystemCatalogsHandler : ICommandHandler<DeleteSystemCatalog, bool>
    {
        private readonly ISystemCatalogRepository _repository;
        public List<string> Errors { get; set; }
        public DeleteSystemCatalogsHandler(ISystemCatalogRepository repository)
        {
            _repository = repository;
        } 
        public CommandResponse<bool> Handle(DeleteSystemCatalog command)
        {
            bool tmpResult = false; 
            _repository.Delete(command.Id);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(DeleteSystemCatalog command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }
            return true;
        }
    }
}