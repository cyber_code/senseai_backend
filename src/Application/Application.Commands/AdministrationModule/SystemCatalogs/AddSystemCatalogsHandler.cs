﻿using Commands.AdministrationModule.SystemCatalogs;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.Internal;
using Persistence.SystemCatalogModel;
using SenseAI.Domain;
using SenseAI.Domain.SystemCatalogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.AdministrationModule.SystemCatalogs
{
    internal class AddSystemCatalogsHandler : ICommandHandler<AddSystemCatalog, AddSystemCatalogResult>
    {
        private readonly ISystemCatalogRepository _repository;
        private readonly IDbContext _dbContext;

        public AddSystemCatalogsHandler(ISystemCatalogRepository repository, IDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddSystemCatalogResult> Handle(AddSystemCatalog command)
        {
            var query = _dbContext.Set<SystemCatalogData>().AsNoTracking().FirstOrDefault(ct => ct.SubprojectId == command.SubProjectId
           &&  ct.SystemId == command.SystemId && ct.CatalogId == command.CatalogId);//TODO: && ct.CatalogId == command.CatalogId when we activate single catalog remove this condition

            if (query != null)
            {
                return new CommandResponse<AddSystemCatalogResult>(null,
                    new CommandResponseMessage((int)MessageType.Failed, $"The system is already defined on another catalog in this subproject."));
            }

            SystemCatalog systemCatalog = new SystemCatalog(command.SubProjectId, command.SystemId, command.CatalogId);

            _repository.Add(systemCatalog);

            return new CommandResponse<AddSystemCatalogResult>(new AddSystemCatalogResult(systemCatalog.Id));
        }

        public bool Validate(AddSystemCatalog command)
        {
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty!");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty!");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty!");
                return false;
            }
            return true;
        }
    }
}