﻿using System;
using System.Collections.Generic;
using Messaging.Commands;
using Commands.AdministrationModule.SystemTags;
using SenseAI.Domain.SystemTags;

namespace Application.Commands.AdministrationModule.SystemTags
{
    public sealed class DeleteSystemTagsHandler : ICommandHandler<DeleteSystemTag, bool>
    {
        private readonly ISystemTagRepository _repository;
        public List<string> Errors { get; set; }

        public DeleteSystemTagsHandler(ISystemTagRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<bool> Handle(DeleteSystemTag command)
        {
            _repository.Delete(command.Id);
           
            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteSystemTag command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            return true;
        }
    }
}