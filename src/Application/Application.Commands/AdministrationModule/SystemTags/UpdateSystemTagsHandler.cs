﻿using System;
using System.Collections.Generic;
using Commands.AdministrationModule.SystemTags;
using Messaging.Commands;
using SenseAI.Domain.SystemTags;

namespace Application.Commands.AdministrationModule.SystemTags
{
    public sealed class UpdateSystemTagsHandler : ICommandHandler<UpdateSystemTag, bool>
    {
        private readonly ISystemTagRepository _repository;
        public List<string> Errors { get; set; }

        public UpdateSystemTagsHandler(ISystemTagRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<bool> Handle(UpdateSystemTag command)
        {
            SystemTag systemTag = new SystemTag(command.Id, command.SystemId, command.Title, command.Description);
            _repository.Update(systemTag);

            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateSystemTag command)
        {
            if (command.Id ==null || command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }

            if (command.SystemId == null || command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty!");
                return false;
            }

            if (string.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }

            return true;
        }
    }
}