﻿using System;
using System.Collections.Generic;
using Commands.AdministrationModule.SystemTags;
using Messaging.Commands;
using SenseAI.Domain.SystemTags;

namespace Application.Commands.AdministrationModule.SystemTags
{
    public sealed class AddSystemTagsHandler : ICommandHandler<AddSystemTag, AddSystemTagResult>
    {
        private readonly ISystemTagRepository _repository;
        public List<string> Errors { get; set; }

        public AddSystemTagsHandler(ISystemTagRepository repository)
        {
            _repository = repository;
        }

        public CommandResponse<AddSystemTagResult> Handle(AddSystemTag command)
        {
            SystemTag systemTag = new SystemTag(command.SystemId, command.Title, command.Description);
            _repository.Add(systemTag);

            return new CommandResponse<AddSystemTagResult>(new AddSystemTagResult(systemTag.Id));
        }

        public bool Validate(AddSystemTag command)
        {
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty!");
                return false;
            }

            if (string.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }

            if (string.IsNullOrEmpty(command.Description))
            {
                Errors.Add("Description cannot be empty");
                return false;
            }

            return true;
        }
    }
}