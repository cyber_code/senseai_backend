﻿using Commands.AdministrationModule.Systems;
using Messaging.Commands;
using Persistence.Internal;
using Persistence.SystemModel;
using SenseAI.Domain.SystemModel;
using System;
using System.Collections.Generic;
using System.Linq; 

namespace Application.Commands.AdministrationModule.Systems
{
    public sealed class AddSystemsHandler : ICommandHandler<AddSystem, AddSystemResult>
    {
        private readonly ISystemRepository _repository;
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }
        public AddSystemsHandler(IDbContext dbContext,ISystemRepository repository)
        {
            _dbContext = dbContext;
            _repository = repository;
        }
        public CommandResponse<AddSystemResult> Handle(AddSystem command)
        {
            SenseAI.Domain.SystemModel.System system = new SenseAI.Domain.SystemModel.System(command.Title, command.AdapterName);

            _repository.Add(system);

            return new CommandResponse<AddSystemResult>(new AddSystemResult(system.Id));
        }

        public bool Validate(AddSystem command)
        { 
            if (String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }
            if (String.IsNullOrEmpty(command.AdapterName))
            {
                Errors.Add("AdapterName cannot be empty!");
                return false;
            }

            //The code commented below should be activated, it is temporarly commented

            //var query = from system in _dbContext.Set<SystemData>()
            //            where system.AdapterName == command.AdapterName
            //            select new
            //            {
            //                system.Id,
            //                system.Title,
            //                system.AdapterName
            //            };
            //if (query.Any())
            //{
            //    Errors.Add("AdapterName already exists!");
            //    return false;
            //}
            return true;
        }
    }
}
