﻿using Commands.AdministrationModule.Systems;
using Messaging.Commands;
using SenseAI.Domain.SystemModel;
using System;
using System.Collections.Generic;

namespace Application.Commands.AdministrationModule.Systems
{
    public sealed class DeleteSystemsHandler : ICommandHandler<DeleteSystem, bool>
    {
        private readonly ISystemRepository _repository;
        public List<string> Errors { get; set; }
        public DeleteSystemsHandler(ISystemRepository repository)
        {
            _repository = repository;
        }
        public CommandResponse<bool> Handle(DeleteSystem command)
        {
            bool tmpResult = false;
            _repository.Delete(command.Id);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(DeleteSystem command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }
            return true;
        }
    }
}