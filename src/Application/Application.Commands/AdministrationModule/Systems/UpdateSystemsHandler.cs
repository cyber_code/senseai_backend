﻿using Commands.AdministrationModule.Systems;
using Messaging.Commands;
using Persistence.Internal;
using Persistence.SystemModel;
using SenseAI.Domain.SystemModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.AdministrationModule.Systems
{
    public sealed class UpdateSystemsHandler : ICommandHandler<UpdateSystem, bool>
    {
        private readonly ISystemRepository _repository;
        private readonly IDbContext _dbContext;
        public List<string> Errors { get; set; }
        public UpdateSystemsHandler(IDbContext dbContext, ISystemRepository repository)
        {
            _dbContext = dbContext;
            _repository = repository;
        }

        public CommandResponse<bool> Handle(UpdateSystem command)
        {
            SenseAI.Domain.SystemModel.System system = new SenseAI.Domain.SystemModel.System(command.Id, command.Title, command.AdapterName);
            _repository.Update(system);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateSystem command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty!");
                return false;
            }
            if (String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty!");
                return false;
            }
            if (String.IsNullOrEmpty(command.AdapterName))
            {
                Errors.Add("AdapterName cannot be empty!");
                return false;
            }

            var query = from system in _dbContext.Set<SystemData>()
                        where system.AdapterName == command.AdapterName && command.Id != system.Id
                        select new
                        {
                            system.Id,
                            system.Title,
                            system.AdapterName
                        };
            if (query.Any())
            {
                Errors.Add("AdapterName already exists!");
                return false;
            }
            return true;
        }
    }
}