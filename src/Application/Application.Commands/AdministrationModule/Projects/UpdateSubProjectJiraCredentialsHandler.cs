﻿using Commands.AdministrationModule.Projects;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using Persistence.AdminModel;
using Persistence.Internal;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Application.Commands.AdministrationModule.Projects
{
    public sealed class UpdateSubProjectJiraCredentialsHandler : ICommandHandler<UpdateSubProjectJiraCredentials, bool>
    {
        private readonly ISubProjectRepository _repository;
        private readonly IDbContext _dbContext;

        public UpdateSubProjectJiraCredentialsHandler(ISubProjectRepository repository, IDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateSubProjectJiraCredentials command)
        {
            bool tmpResult = false;
            var query = (from project in _dbContext.Set<SubProjectData>().AsNoTracking()
                         where project.Id == command.Id
                         select new
                         {
                             project.Id,
                             project.ProjectId,
                             project.Title,
                             project.Description,
                             project.JiraLink,
                             project.IsJiraIntegrationEnabled
                         }).FirstOrDefault();
            SubProject subProject = new SubProject(command.Id, query.ProjectId, query.Title,
                query.Description, command.IsJiraIntegrationEnabled, query.JiraLink);
            _repository.Update(subProject);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(UpdateSubProjectJiraCredentials command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}