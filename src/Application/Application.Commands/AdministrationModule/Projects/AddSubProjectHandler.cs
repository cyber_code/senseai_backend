﻿using Commands.AdministrationModule.Projects;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using SenseAI.Domain.Process.IssuesModel;

namespace Application.Commands.AdministrationModule.Projects
{
    public sealed class AddSubProjectHandler : ICommandHandler<AddSubProject, AddSubProjectResult>
    {
        private readonly ISubProjectRepository _repository;
        private readonly IIssueTypeRepository _issueTypesRepository;
        private readonly IIssueTypeFieldsRepository _fieldsRepository;

        public AddSubProjectHandler(ISubProjectRepository repository, IIssueTypeRepository issueTypesRepository, IIssueTypeFieldsRepository fieldsRepository)
        {
            _repository = repository;
            _issueTypesRepository = issueTypesRepository;
            _fieldsRepository = fieldsRepository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddSubProjectResult> Handle(AddSubProject command)
        {
            SubProject subProject = new SubProject(command.ProjectId, command.Title, command.Description, false, "");

            _repository.Add(subProject);
            AddDefaultIssueTypes(subProject);
            return new CommandResponse<AddSubProjectResult>(new AddSubProjectResult(subProject.Id));
        }

        public bool Validate(AddSubProject command)
        {
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }

        private void AddDefaultIssueTypes(SubProject subProject)
        {
            IssueType issue1 = new IssueType(subProject.Id, "Defect", "Defect", "Story points", true);
            IssueType issue2 = new IssueType(subProject.Id, "Technical CR", "Technical Change Request", "hours", true);
            IssueType issue3 = new IssueType(subProject.Id, "Business CR", "Business Change Request", "hours", true);
            IssueType issue4 = new IssueType(subProject.Id, "Task QA", "Task QA", "Days", true);
            _issueTypesRepository.Add(issue1);
            _issueTypesRepository.Add(issue2);
            _issueTypesRepository.Add(issue3);
            _issueTypesRepository.Add(issue4);
            //Defect
            AddDefectFields(issue1);
            AddBusinessCrFields(issue3);
            AddTechnicalCrFields(issue2);
            AddTaskFields(issue4);
        }

        private void AddBusinessCrFields(IssueType issue)
        {
            //Business CR
        
            IssueTypeFields issueTypeFieldsPriority = new IssueTypeFields(issue.Id, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), false);
            IssueTypeFields issueTypeFieldsSeverity = new IssueTypeFields(issue.Id, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), false);
  
            IssueTypeFields issueTypeFieldsApprovalDate = new IssueTypeFields(issue.Id, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), true);
 
            IssueTypeFields issueTypeFieldsApprovedBy = new IssueTypeFields(issue.Id, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), true);
            IssueTypeFields issueTypeFieldsIdentifiedBy = new IssueTypeFields(issue.Id, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), true);
            IssueTypeFields issueTypeFieldsWorkflow = new IssueTypeFields(issue.Id, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), false);
            IssueTypeFields issueTypeFieldsChangeStatus = new IssueTypeFields(issue.Id, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), true);
            IssueTypeFields issueTypeFieldsApprovedForInvestigation = new IssueTypeFields(issue.Id, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), true);
            IssueTypeFields issueTypeFieldsAttribute = new IssueTypeFields(issue.Id, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), true);
            IssueTypeFields issueTypeFieldsApplication = new IssueTypeFields(issue.Id, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), true);
            IssueTypeFields issueTypeFieldsComment = new IssueTypeFields(issue.Id, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), true);
            IssueTypeFields issueTypeFieldsImplication = new IssueTypeFields(issue.Id, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), true);
            IssueTypeFields issueTypeFieldsReason = new IssueTypeFields(issue.Id, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), true);
            IssueTypeFields issueTypeFieldsEstimate = new IssueTypeFields(issue.Id, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), true);
            IssueTypeFields issueTypeFieldsStatus = new IssueTypeFields(issue.Id, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), true);
            IssueTypeFields issueTypeFieldsInvestigationStatus = new IssueTypeFields(issue.Id, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), true);
            IssueTypeFields issueTypeFieldsDefectType = new IssueTypeFields(issue.Id, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), false);

            _fieldsRepository.Add(issueTypeFieldsPriority);
            _fieldsRepository.Add(issueTypeFieldsSeverity);
            _fieldsRepository.Add(issueTypeFieldsApprovalDate);
            _fieldsRepository.Add(issueTypeFieldsApprovedBy);
            _fieldsRepository.Add(issueTypeFieldsIdentifiedBy);
            _fieldsRepository.Add(issueTypeFieldsWorkflow);
            _fieldsRepository.Add(issueTypeFieldsChangeStatus);
            _fieldsRepository.Add(issueTypeFieldsApprovedForInvestigation);
            _fieldsRepository.Add(issueTypeFieldsAttribute);
            _fieldsRepository.Add(issueTypeFieldsApplication);
            _fieldsRepository.Add(issueTypeFieldsComment);
            _fieldsRepository.Add(issueTypeFieldsImplication);
            _fieldsRepository.Add(issueTypeFieldsReason);
            _fieldsRepository.Add(issueTypeFieldsEstimate);
            _fieldsRepository.Add(issueTypeFieldsStatus);
            _fieldsRepository.Add(issueTypeFieldsInvestigationStatus);
            _fieldsRepository.Add(issueTypeFieldsDefectType);
        }

        private void AddTechnicalCrFields(IssueType issue)
        {
            //Business CR

            IssueTypeFields issueTypeFieldsPriority = new IssueTypeFields(issue.Id, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), false);
            IssueTypeFields issueTypeFieldsSeverity = new IssueTypeFields(issue.Id, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), false);
            IssueTypeFields issueTypeFieldsApprovalDate = new IssueTypeFields(issue.Id, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), true);
            IssueTypeFields issueTypeFieldsApprovedBy = new IssueTypeFields(issue.Id, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), true);
            IssueTypeFields issueTypeFieldsIdentifiedBy = new IssueTypeFields(issue.Id, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), true);
            IssueTypeFields issueTypeFieldsWorkflow = new IssueTypeFields(issue.Id, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), false);
            IssueTypeFields issueTypeFieldsChangeStatus = new IssueTypeFields(issue.Id, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), true);
            IssueTypeFields issueTypeFieldsApprovedForInvestigation = new IssueTypeFields(issue.Id, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), true);
            IssueTypeFields issueTypeFieldsAttribute = new IssueTypeFields(issue.Id, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), true);
            IssueTypeFields issueTypeFieldsApplication = new IssueTypeFields(issue.Id, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), true);
            IssueTypeFields issueTypeFieldsComment = new IssueTypeFields(issue.Id, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), true);
            IssueTypeFields issueTypeFieldsImplication = new IssueTypeFields(issue.Id, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), true);
            IssueTypeFields issueTypeFieldsReason = new IssueTypeFields(issue.Id, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), true);
            IssueTypeFields issueTypeFieldsEstimate = new IssueTypeFields(issue.Id, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), true);
            IssueTypeFields issueTypeFieldsStatus = new IssueTypeFields(issue.Id, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), true);
            IssueTypeFields issueTypeFieldsInvestigationStatus = new IssueTypeFields(issue.Id, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), true);
            IssueTypeFields issueTypeFieldsDefectType = new IssueTypeFields(issue.Id, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), false);

            _fieldsRepository.Add(issueTypeFieldsPriority);
            _fieldsRepository.Add(issueTypeFieldsSeverity);
            _fieldsRepository.Add(issueTypeFieldsApprovalDate);
            _fieldsRepository.Add(issueTypeFieldsApprovedBy);
            _fieldsRepository.Add(issueTypeFieldsIdentifiedBy);
            _fieldsRepository.Add(issueTypeFieldsWorkflow);
            _fieldsRepository.Add(issueTypeFieldsChangeStatus);
            _fieldsRepository.Add(issueTypeFieldsApprovedForInvestigation);
            _fieldsRepository.Add(issueTypeFieldsAttribute);
            _fieldsRepository.Add(issueTypeFieldsApplication);
            _fieldsRepository.Add(issueTypeFieldsComment);
            _fieldsRepository.Add(issueTypeFieldsImplication);
            _fieldsRepository.Add(issueTypeFieldsReason);
            _fieldsRepository.Add(issueTypeFieldsEstimate);
            _fieldsRepository.Add(issueTypeFieldsStatus);
            _fieldsRepository.Add(issueTypeFieldsInvestigationStatus);
            _fieldsRepository.Add(issueTypeFieldsDefectType);
        }

        private void AddDefectFields(IssueType issue)
        {

            IssueTypeFields issueTypeFieldsPriority = new IssueTypeFields(issue.Id, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), true);
            IssueTypeFields issueTypeFieldsSeverity = new IssueTypeFields(issue.Id, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), true);
            IssueTypeFields issueTypeFieldsApprovalDate = new IssueTypeFields(issue.Id, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), false);
            IssueTypeFields issueTypeFieldsApprovedBy = new IssueTypeFields(issue.Id, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), false);
            IssueTypeFields issueTypeFieldsIdentifiedBy = new IssueTypeFields(issue.Id, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), false);
            IssueTypeFields issueTypeFieldsWorkflow = new IssueTypeFields(issue.Id, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), true);
            IssueTypeFields issueTypeFieldsChangeStatus = new IssueTypeFields(issue.Id, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), false);
            IssueTypeFields issueTypeFieldsApprovedForInvestigation = new IssueTypeFields(issue.Id, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), false);
            IssueTypeFields issueTypeFieldsAttribute = new IssueTypeFields(issue.Id, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), false);
            IssueTypeFields issueTypeFieldsApplication = new IssueTypeFields(issue.Id, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), false);
            IssueTypeFields issueTypeFieldsComment = new IssueTypeFields(issue.Id, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), false);
            IssueTypeFields issueTypeFieldsImplication = new IssueTypeFields(issue.Id, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), false);
            IssueTypeFields issueTypeFieldsReason = new IssueTypeFields(issue.Id, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), false);
            IssueTypeFields issueTypeFieldsEstimate = new IssueTypeFields(issue.Id, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), true);
            IssueTypeFields issueTypeFieldsStatus = new IssueTypeFields(issue.Id, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), true);
            IssueTypeFields issueTypeFieldsInvestigationStatus = new IssueTypeFields(issue.Id, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), false);
            IssueTypeFields issueTypeFieldsDefectType = new IssueTypeFields(issue.Id, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), true);
            IssueTypeFields issueTypeFieldsTestCaseId = new IssueTypeFields(issue.Id, new Guid("8AD0BE32-31E0-43EE-8C4F-C15C145B026F"), true);

            _fieldsRepository.Add(issueTypeFieldsPriority);
            _fieldsRepository.Add(issueTypeFieldsSeverity);
            _fieldsRepository.Add(issueTypeFieldsApprovalDate);
            _fieldsRepository.Add(issueTypeFieldsApprovedBy);
            _fieldsRepository.Add(issueTypeFieldsIdentifiedBy);
            _fieldsRepository.Add(issueTypeFieldsWorkflow);
            _fieldsRepository.Add(issueTypeFieldsChangeStatus);
            _fieldsRepository.Add(issueTypeFieldsApprovedForInvestigation);
            _fieldsRepository.Add(issueTypeFieldsAttribute);
            _fieldsRepository.Add(issueTypeFieldsApplication);
            _fieldsRepository.Add(issueTypeFieldsComment);
            _fieldsRepository.Add(issueTypeFieldsImplication);
            _fieldsRepository.Add(issueTypeFieldsReason);
            _fieldsRepository.Add(issueTypeFieldsEstimate);
            _fieldsRepository.Add(issueTypeFieldsStatus);
            _fieldsRepository.Add(issueTypeFieldsInvestigationStatus);
            _fieldsRepository.Add(issueTypeFieldsDefectType);
            _fieldsRepository.Add(issueTypeFieldsTestCaseId);
        }

        private void AddTaskFields(IssueType issue)
        {
            IssueTypeFields issueTypeFieldsPriority = new IssueTypeFields(issue.Id, new Guid("0005619f-4bba-4506-9a5f-4c952d954936"), true);
            IssueTypeFields issueTypeFieldsSeverity = new IssueTypeFields(issue.Id, new Guid("9eb4717c-1d26-4044-a59a-c1e46740a5ae"), false);
            IssueTypeFields issueTypeFieldsApprovalDate = new IssueTypeFields(issue.Id, new Guid("68c49518-35b2-4691-9524-95be5ba93723"), false);
            IssueTypeFields issueTypeFieldsApprovedBy = new IssueTypeFields(issue.Id, new Guid("594f6ee2-c656-4827-bd4b-62b1bd194e1e"), false);
            IssueTypeFields issueTypeFieldsIdentifiedBy = new IssueTypeFields(issue.Id, new Guid("ce9bf5b5-bb29-45dc-9253-07659c41c033"), true);
            IssueTypeFields issueTypeFieldsWorkflow = new IssueTypeFields(issue.Id, new Guid("da904b45-2a58-4ee8-a6d7-eea7583d84a5"), true);
            IssueTypeFields issueTypeFieldsChangeStatus = new IssueTypeFields(issue.Id, new Guid("c93a3a44-df75-4a8c-8846-8aded31a8132"), false);
            IssueTypeFields issueTypeFieldsApprovedForInvestigation = new IssueTypeFields(issue.Id, new Guid("ada7c874-1922-44ab-981d-adf596beece2"), false);
            IssueTypeFields issueTypeFieldsAttribute = new IssueTypeFields(issue.Id, new Guid("0dfcdeb0-ea88-435a-87e7-3352d9eee7cc"), false);
            IssueTypeFields issueTypeFieldsApplication = new IssueTypeFields(issue.Id, new Guid("f2389062-c1f7-4f66-b3ab-5abe54195fcf"), false);
            IssueTypeFields issueTypeFieldsComment = new IssueTypeFields(issue.Id, new Guid("0072c3ed-37b6-44d9-8df5-a59acfd5b662"), false);
            IssueTypeFields issueTypeFieldsImplication = new IssueTypeFields(issue.Id, new Guid("33d9d185-cfcf-4945-8cd3-e9cf8d4769b9"), false);
            IssueTypeFields issueTypeFieldsReason = new IssueTypeFields(issue.Id, new Guid("c8be293a-c93f-4806-99cc-02d56322ad3d"), false);
            IssueTypeFields issueTypeFieldsEstimate = new IssueTypeFields(issue.Id, new Guid("d1e14aa7-80dd-4223-aba1-ba47dd9bb660"), true);
            IssueTypeFields issueTypeFieldsStatus = new IssueTypeFields(issue.Id, new Guid("e62f1ece-f623-4aaf-ae29-92fe70767706"), true);
            IssueTypeFields issueTypeFieldsInvestigationStatus = new IssueTypeFields(issue.Id, new Guid("da465c04-07d8-4ccd-b9ab-420449539999"), false);
            IssueTypeFields issueTypeFieldsDefectType = new IssueTypeFields(issue.Id, new Guid("401518ad-59a4-4607-b53c-61a232adca62"), false);

            _fieldsRepository.Add(issueTypeFieldsPriority);
            _fieldsRepository.Add(issueTypeFieldsSeverity);
            _fieldsRepository.Add(issueTypeFieldsApprovalDate);
            _fieldsRepository.Add(issueTypeFieldsApprovedBy);
            _fieldsRepository.Add(issueTypeFieldsIdentifiedBy);
            _fieldsRepository.Add(issueTypeFieldsWorkflow);
            _fieldsRepository.Add(issueTypeFieldsChangeStatus);
            _fieldsRepository.Add(issueTypeFieldsApprovedForInvestigation);
            _fieldsRepository.Add(issueTypeFieldsAttribute);
            _fieldsRepository.Add(issueTypeFieldsApplication);
            _fieldsRepository.Add(issueTypeFieldsComment);
            _fieldsRepository.Add(issueTypeFieldsImplication);
            _fieldsRepository.Add(issueTypeFieldsReason);
            _fieldsRepository.Add(issueTypeFieldsEstimate);
            _fieldsRepository.Add(issueTypeFieldsStatus);
            _fieldsRepository.Add(issueTypeFieldsInvestigationStatus);
            _fieldsRepository.Add(issueTypeFieldsDefectType);
        }
    }
}