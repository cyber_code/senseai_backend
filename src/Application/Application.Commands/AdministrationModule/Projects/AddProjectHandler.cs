﻿using Commands.AdministrationModule.Projects;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System.Collections.Generic;
using SignalR;
using Core.Abstractions;
using System;

namespace Application.Commands.AdministrationModule.Projects
{
    public sealed class AddProjectHandler : ICommandHandler<AddProject, AddProjectResult>
    {
        private readonly IProjectRepository _repository;
        private readonly SignalR.ISignalRClient _signalRClient;
        private readonly IWorkContext _workContext;

        public AddProjectHandler(IProjectRepository repository, ISignalRClient signalRClient, IWorkContext workContext)
        {
            _repository = repository;
            _signalRClient = signalRClient;
            _workContext = workContext;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddProjectResult> Handle(AddProject command)
        {
            _signalRClient.SendAsync(new SignalRCommand
            {
                Method = "SaveRecordedWorkflow",
                Group = _workContext.FullName,
                Value = Guid.NewGuid()
            });
            Project project = new Project(command.Title, command.Description);

            _repository.Add(project);

            return new CommandResponse<AddProjectResult>(new AddProjectResult(project.Id));
        }

        public bool Validate(AddProject command)
        {
            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}