﻿using Commands.AdministrationModule.Settings;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands
{
    public sealed class UpdateSettingsHandler : ICommandHandler<UpdateSettings, bool>
    {
        private readonly ISettingsRepository _repository;

        public UpdateSettingsHandler(ISettingsRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateSettings command)
        {
            Settings settings = new Settings(command.Id, command.ProjectId, command.SubProjectId, command.SystemId, command.CatalogId);
            _repository.Update(settings);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateSettings command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}