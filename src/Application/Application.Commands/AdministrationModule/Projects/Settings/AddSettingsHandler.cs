﻿using Commands.AdministrationModule.Settings;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.AdministrationModule
{
    public class AddSettingsHandler : ICommandHandler<AddSettings, AddSettingsResult>
    {
        private readonly ISettingsRepository _repository;

        public AddSettingsHandler(ISettingsRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<AddSettingsResult> Handle(AddSettings command)
        {
            Settings settings = new Settings(command.ProjectId, command.SubProjectId, command.SystemId, command.CatalogId);
            _repository.Add(settings);
            return new CommandResponse<AddSettingsResult>(new AddSettingsResult(settings.Id));
        }

        public bool Validate(AddSettings command)
        {
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.SystemId == Guid.Empty)
            {
                Errors.Add("SystemId cannot be empty.");
                return false;
            }
            if (command.CatalogId == Guid.Empty)
            {
                Errors.Add("CatalogId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}