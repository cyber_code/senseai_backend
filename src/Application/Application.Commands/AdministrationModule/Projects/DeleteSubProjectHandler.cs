﻿using Commands.AdministrationModule.Projects;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.AdministrationModule.Projects
{
    public sealed class DeleteSubProjectHandler : ICommandHandler<DeleteSubProject, bool>
    {
        private readonly ISubProjectRepository _repository;

        public DeleteSubProjectHandler(ISubProjectRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteSubProject command)
        {
            bool tmpResult = false;

            _repository.Delete(command.Id);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(DeleteSubProject command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}