﻿using Commands.AdministrationModule.Projects;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.AdministrationModule.Projects
{
    public sealed class DeleteProjectHandler : ICommandHandler<DeleteProject, bool>
    {
        private readonly IProjectRepository _repository;

        public DeleteProjectHandler(IProjectRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(DeleteProject command)
        {
            bool tmpResult = false;

            _repository.Delete(command.Id);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(DeleteProject command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            return true;
        }
    }
}