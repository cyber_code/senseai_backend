﻿using Commands.AdministrationModule.Projects;
using SenseAI.Domain.AdminModel;
using Messaging.Commands;
using System;
using System.Collections.Generic;

namespace Application.Commands.AdministrationModule.Projects
{
    public sealed class UpdateSubProjectHandler : ICommandHandler<UpdateSubProject, bool>
    {
        private readonly ISubProjectRepository _repository;

        public UpdateSubProjectHandler(ISubProjectRepository repository)
        {
            _repository = repository;
        }

        public List<string> Errors { get; set; }

        public CommandResponse<bool> Handle(UpdateSubProject command)
        {
            bool tmpResult = false;
            SubProject subProject = new SubProject(command.Id, command.ProjectId, command.Title, command.Description, false, null);

            _repository.Update(subProject);
            tmpResult = true;
            return new CommandResponse<bool>(tmpResult);
        }

        public bool Validate(UpdateSubProject command)
        {
            if (command.Id == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            return true;
        }
    }
}