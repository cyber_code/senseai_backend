﻿using Commands.AdministrationModule.Catalogs;
using Commands.AdministrationModule.SystemCatalogs;
using Messaging.Commands;
using SenseAI.Domain.CatalogModel;
using SenseAI.Domain.SystemCatalogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.AdministrationModule.Catalogs
{
    class AddSubProjectCatalogHandler : ICommandHandler<AddSubProjectCatalog, AddSubProjectCatalogResult>
    {
        private readonly ICatalogRepository _repository;
        public List<string> Errors { get; set; }
        public AddSubProjectCatalogHandler(ICatalogRepository repository)
        {
            _repository = repository;
        }
        public CommandResponse<AddSubProjectCatalogResult> Handle(AddSubProjectCatalog command)
        {
            Catalog catalog = new Catalog(command.ProjectId, command.SubProjectId, command.Title, command.Description, command.Type);
            _repository.Add(catalog);
            return new CommandResponse<AddSubProjectCatalogResult>(new AddSubProjectCatalogResult(catalog.Id));
        }

        public bool Validate(AddSubProjectCatalog command)
        {
            if (command.ProjectId == Guid.Empty)
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }

            if (command.SubProjectId == Guid.Empty)
            {
                Errors.Add("SubProjectId cannot be empty.");
                return false;
            }

            if (String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }
            return true;
        }
    }
}
