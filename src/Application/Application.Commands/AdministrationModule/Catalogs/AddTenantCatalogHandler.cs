﻿using Commands.AdministrationModule.Catalogs;
using Commands.AdministrationModule.SystemCatalogs;
using Messaging.Commands;
using SenseAI.Domain.CatalogModel;
using SenseAI.Domain.SystemCatalogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.AdministrationModule.Catalogs
{
    class AddTenantCatalogHandler : ICommandHandler<AddTenantCatalog, AddTenantCatalogResult>
    {
        private readonly ICatalogRepository _repository;
        public List<string> Errors { get; set; }
        public AddTenantCatalogHandler(ICatalogRepository repository)
        {
            _repository = repository;
        }
        public CommandResponse<AddTenantCatalogResult> Handle(AddTenantCatalog command)
        {
            Catalog catalog = new Catalog(Guid.Empty, Guid.Empty, command.Title, command.Description, command.Type);
            _repository.Add(catalog);
            return new CommandResponse<AddTenantCatalogResult>(new AddTenantCatalogResult(catalog.Id));
        }

        public bool Validate(AddTenantCatalog command)
        {
            if(String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
