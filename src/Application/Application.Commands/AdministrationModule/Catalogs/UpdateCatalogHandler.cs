﻿using Commands.AdministrationModule.Catalogs;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.CatalogModel;
using Persistence.WorkflowModel;
using Persistence.Internal;
using SenseAI.Domain.CatalogModel;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using SenseAI.Domain.WorkflowModel;
using Core.Extensions;

namespace Application.Commands.AdministrationModule.Catalogs
{
    class UpdateCatalogHandler : ICommandHandler<UpdateCatalog, bool>
    {
        private readonly ICatalogRepository _repository;
        private readonly IWorkflowTestCasesRepository _workflowTestCasesRepository;
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IDbContext _dbContext;


        public List<string> Errors { get; set; }
        public UpdateCatalogHandler(IDbContext dbContext, ICatalogRepository repository, IWorkflowTestCasesRepository workflowTestCasesRepository, IWorkflowRepository workflowRepository)
        {
            _dbContext = dbContext;
            _repository = repository;
            _workflowTestCasesRepository = workflowTestCasesRepository;
            _workflowRepository = workflowRepository;
        }
        public CommandResponse<bool> Handle(UpdateCatalog command)
        {
            var catalog = _dbContext.Set<CatalogData>().AsNoTracking()
                .Where(c => c.Id == command.Id).First();

            Catalog updatedCatalog = new Catalog(
                catalog.Id,
                catalog.ProjectId,
                catalog.SubProjectId,
                command.Title,
                command.Description,
                command.Type);
            _repository.Update(updatedCatalog);

           var workflowTestCase = _dbContext.Set<WorkflowTestCasesData>().AsNoTracking().ToList();
            var tc2Add = new List<WorkflowTestCases>();
            foreach (var workflowData in workflowTestCase)
            {
                try
                {
                    JObject jObject = JObject.Parse(workflowData.TestStepJson);
                    var testStepJson = JsonConvert.DeserializeObject<TestStepJson>(jObject.ToString());
                    if (testStepJson.CatalogId != command.Id)
                        continue;
                    testStepJson.CatalogName = command.Title;
                    workflowData.TestStepJson = JsonConvert.SerializeObject(testStepJson);
                    tc2Add.Add(new WorkflowTestCases(
                          workflowData.Id, workflowData.WorkflowId, workflowData.VersionId, workflowData.WorkflowPathId,
                           workflowData.TestCaseId, workflowData.TestCaseTitle, workflowData.TCIndex, workflowData.TestStepId, workflowData.TestStepTitle,
                            workflowData.TestStepJson, workflowData.TypicalId, workflowData.TypicalName, workflowData.TSIndex,
                            workflowData.TestStepType));

                }
                catch (Exception ex)
                {
                }
            }
        
            _workflowTestCasesRepository.Update(tc2Add.ToArray());

            var workflows = _dbContext.Set<WorkflowData>().AsNoTracking().ToList();
            
            foreach (var workflowData in workflows)
            {
                try
                {
                    var isChange = false;
                    JObject jObject = JObject.Parse(workflowData.DesignerJson);
                    
                    var cells = jObject["cells"];
                    var i = 0;
                    foreach (var item in cells)
                    {
                        
                        if (item["customData"] == null)
                            continue;
                        var testStepJson = JsonConvert.DeserializeObject<TestStepJson>(item["customData"].ToString());
                        if (testStepJson.CatalogId == command.Id)
                        {
                            jObject["cells"][i]["customData"]["CatalogName"] = command.Title;
                            isChange = true;
                        }
                                
                        i++;

                    }
                    if (isChange)
                        _workflowRepository.Update(workflowData.Id, JsonConvert.SerializeObject(jObject), false);
                }
                catch (Exception ex)
                {
                }
            }


            return new CommandResponse<bool>(true);
        }

        public bool Validate(UpdateCatalog command)
        {
            if (command.Id == null || command.Id == Guid.Empty)
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            if (String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
