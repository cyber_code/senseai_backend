﻿using Commands.AdministrationModule.Catalogs;
using Messaging.Commands;
using Microsoft.EntityFrameworkCore;
using Persistence.CatalogModel;
using Persistence.Internal;
using SenseAI.Domain.CatalogModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Commands.AdministrationModule.Catalogs
{
    class DeleteCatalogHandler : ICommandHandler<DeleteCatalog, bool>
    {
        private readonly ICatalogRepository _repository;

        public List<string> Errors { get; set; }
        public DeleteCatalogHandler(ICatalogRepository repository)
        {
            _repository = repository;
        }
        public CommandResponse<bool> Handle(DeleteCatalog command)
        {
            _repository.Delete(command.Id);
            return new CommandResponse<bool>(true);
        }

        public bool Validate(DeleteCatalog command)
        {
            if(command.Id == null || command.Id.Equals(Guid.Empty))
            {
                Errors.Add("Id cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
