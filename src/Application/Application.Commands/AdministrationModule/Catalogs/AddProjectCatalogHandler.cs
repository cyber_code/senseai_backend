﻿using Commands.AdministrationModule.Catalogs;
using Commands.AdministrationModule.SystemCatalogs;
using Messaging.Commands;
using SenseAI.Domain.CatalogModel;
using SenseAI.Domain.SystemCatalogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.AdministrationModule.Catalogs
{
    class AddProjectCatalogHandler : ICommandHandler<AddProjectCatalog, AddProjectCatalogResult>
    {
        private readonly ICatalogRepository _repository;
        public List<string> Errors { get; set; }
        public AddProjectCatalogHandler(ICatalogRepository repository)
        {
            _repository = repository;
        }
        public CommandResponse<AddProjectCatalogResult> Handle(AddProjectCatalog command)
        {
            Catalog catalog = new Catalog(command.ProjectId, Guid.Empty, command.Title, command.Description, command.Type);
            _repository.Add(catalog);
            return new CommandResponse<AddProjectCatalogResult>(new AddProjectCatalogResult(catalog.Id));
        }

        public bool Validate(AddProjectCatalog command)
        {
            if(String.IsNullOrEmpty(command.Title))
            {
                Errors.Add("Title cannot be empty.");
                return false;
            }

            if(command.ProjectId == null || command.ProjectId.Equals(Guid.Empty))
            {
                Errors.Add("ProjectId cannot be empty.");
                return false;
            }

            return true;
        }
    }
}
