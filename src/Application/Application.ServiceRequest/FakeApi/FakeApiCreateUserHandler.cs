﻿using ExternalServices.FakeApiModule;
using Hub.Contracts;
using Messaging.ExternalServices;
using System.Threading.Tasks;

namespace Application.ExternalServices.FakeApi
{
    internal class FakeApiCreateUserHandler
        : IServiceRequestHandler<FakeApiCreateUserRequest, FakeApiCreateUserResponse>
    {
        private readonly IFakeApiService _service;

        public FakeApiCreateUserHandler(IFakeApiService service)
        {
            _service = service;
        }

        public async Task<ServiceResponse<FakeApiCreateUserResponse>> Handle(FakeApiCreateUserRequest request)
        {
            var result = await _service.FakeApiCreateUserRequest(request);

            return new ServiceResponse<FakeApiCreateUserResponse>(result);
        }

        public bool Validate(FakeApiCreateUserRequest request)
        {
            //TODO: Harold
            return true;
        }
    }
}