﻿using ExternalServices.NeuralEngineModule;
using Hub.Contracts;
using Messaging.ExternalServices;
using System.Threading.Tasks;

namespace Application.ExternalServices.MachineLearningModule
{
    public sealed class GetSuggestionHandler
        : IServiceRequestHandler<GetSuggestion, GetSuggestionResponse>
    {
        private readonly INeuralEngineService _service;

        public GetSuggestionHandler(INeuralEngineService service)
        {
            _service = service;
        }

        public async Task<ServiceResponse<GetSuggestionResponse>> Handle(GetSuggestion request)
        {
            var result = await _service.GetSuggestion(request);

            return new ServiceResponse<GetSuggestionResponse>(result);
        }

        public bool Validate(GetSuggestion request)
        {
            return true;
        }
    }
}