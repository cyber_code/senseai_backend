﻿using NeuralEngine.CheckServiceModel;
using NeuralEngine.DataSet;
using NeuralEngine.DefectsModel;
using NeuralEngine.Suggestion;
using System.Collections.Generic;

namespace NeuralEngine
{
    public interface INeuralEngineClient
    {
        GetSuggestionsResponse GetSuggestions(GetSuggestionsRequest request);

        GetPathsResponse GetPaths(GetPathsRequest request);

        GetDataSetsItemsResponse GetDataSetItems(GetDataSetsItemsRequest request);

        bool PostIssueWorkflow(IssueWorkflowRequest request);

        SaveWorkflowDataCoverageResponse SaveWorkflowDataCoverage(SaveWorkflowDataCoverageRequest request);

        List<ServiceInfo> CheckServices();

        WorkflowCoverageResponse GetWorkflowCoverage(WorkflowCoverageRequest request);

        GetDataSetCoverageResponse GetDataSetCoverage(GetDataSetCoverageRequest request);

        GetComputerGeneratedWorkflowsResponse GetWorkflows(GetComputerGeneratedWorkflowsRequest request);

        DefectResponse SaveDefects(DefectPostModel model);

        DefectResponse DeleteDefects(string HashId, string DefectId);
    }
}