﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace NeuralEngine.DefectsModel
{
    public sealed class DefectPostModel
    {
        private string _hashId;

        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get { return Guid.Parse("b5bcc43f-afc8-4f67-8eb1-b633994ce363"); } }

        public Guid CatalogId { get; set; }

        public string HashId
        {
            get
            {
                if (_hashId == null)
                {
                    _hashId = BitConverter.ToString(
                        MD5.Create().ComputeHash(
                            Encoding.UTF8.GetBytes(
                                TenantId + "|" +
                                ProjectId + "|" +
                                SubProjectId + "|" +
                                SystemId + "|" +
                                CatalogId
                            )
                        )
                    );
                }

                return _hashId.Replace("-", string.Empty);
            }
        }

        public DefectPostModelItem[] Items { get; set; }

        public string Lang { get; set; }
    }

    public sealed class DefectPostModelItem
    {
        public string DefectId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Component { get; set; }

        public string Priority { get; set; }

        public string Severity { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

        public string AssignedTo { get; set; }

        public string StartDate { get; set; }

        public string ModifiedDate { get; set; }

        public string TargetDate { get; set; }

        public string TestCaseId { get; set; }

        public string IssueType { get; set; }
    }

    public sealed class DefectResponse
    {
        public bool Successful { get; set; }

        public string Description { get; set; }
    }
}
