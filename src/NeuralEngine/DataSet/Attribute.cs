﻿namespace NeuralEngine.DataSet
{
    public sealed class Attribute
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}