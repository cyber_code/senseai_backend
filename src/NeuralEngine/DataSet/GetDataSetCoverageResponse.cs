﻿namespace NeuralEngine.DataSet
{
    public class GetDataSetCoverageResponse
    {
        public decimal Coverage { get; set; }
    }
}
