﻿namespace NeuralEngine.DataSet
{
    public class Combinations
    {
        public string Attribute { get; set; }

        public string[] Values { get; set; }
    }
}
