﻿namespace NeuralEngine.DataSet
{
    public sealed class Rows
    {
        public Attribute[] Attributes { get; set; }
    }
}