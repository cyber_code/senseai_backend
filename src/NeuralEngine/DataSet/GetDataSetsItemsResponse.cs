﻿using System;

namespace NeuralEngine.DataSet
{
    public class GetDataSetsItemsResponse
    {
        public DataSet[] DataSets { get; set; }
    }

    public sealed class DataSet
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public decimal Coverage { get; set; }

        public Rows[] Items { get; set; }

        /*This is temperory only for dummy data*/
        //haxhi mos e hek ? se spo bojn dataset-at
        public Guid? TypicalId { get; set; }
    }
}