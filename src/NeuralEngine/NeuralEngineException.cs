﻿using System;
using System.Runtime.Serialization;

namespace NeuralEngine
{
    [Serializable]
    public sealed class NeuralEngineException : Exception
    {
        public NeuralEngineException()
        {
        }

        public NeuralEngineException(string message) : base(message)
        {
        }

        public NeuralEngineException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public NeuralEngineException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}