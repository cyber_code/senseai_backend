﻿using NeuralEngine.DataSet;
using NeuralEngine.Suggestion;
using System;
using System.Collections.Generic;
using Attribute = NeuralEngine.DataSet.Attribute;

namespace NeuralEngine
{
    internal class DummyData
    {
        public static GetPathsResponse GetPaths()
        {
            var listPaths = new List<WorkflowPath>();
            var listIds = new List<Guid>()
            {
                Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid()
            };
            var wPath = new WorkflowPath()
            {
                Coverage = 90,
                IsBestPath = true,
                ItemIds = listIds.ToArray(),
                Number = 1
            };
            listPaths.Add(wPath);
            listIds = new List<Guid>()
            {
                Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid()
            };
            wPath = new WorkflowPath()
            {
                Coverage = 76,
                IsBestPath = false,
                ItemIds = listIds.ToArray(),
                Number = 2
            };
            listPaths.Add(wPath);
            var getPaths = new GetPathsResponse()
            {
                WorkflowPaths = listPaths.ToArray()
            };
            return getPaths;
        }

        public static WorkflowCoverageResponse GetWorkflowCoverage()
        {
            var getCoverage = new WorkflowCoverageResponse()
            {
                Status = "successful",
                WorkflowId = Guid.NewGuid(),
                WorkflowCoverage = 85,
                DataCoverage = 43
            };
            return getCoverage;
        }

        public static List<GetComputerGeneratedWorkflowsResponse> GetComputerGeneratedWorkflows()
        {
            List<GetComputerGeneratedWorkflowsResponse> workflowsList = new List<GetComputerGeneratedWorkflowsResponse>();
            GetComputerGeneratedWorkflowsResponse workflows = new GetComputerGeneratedWorkflowsResponse();
            List<WorkflowItem> WorkflowItem;

            List<WorkflowItemLink> WorkflowItemLink;

            List<WorkflowPath> WorkflowPath;

            return null;
        }

        public static List<GetSuggestionsResponse> GetSuggestions()
        {
            var list = new List<GetSuggestionsResponse>();
            var preList = new List<GetSuggestionsResponseItem>();
            var dyndatalist = new List<DynamicData>();
            var suggestionId = Guid.Parse("3ED2A98B-494F-42D4-B440-96DCF82A2847");
            var dynamicData = new DynamicData
            {
                FormulaText = "Formula Text",
                SourceAttributeTitle = "ID",
                SourceWorkflowItemId = Guid.NewGuid(),
                TargetAttributeTitle = "CustomerID"
            };
            dyndatalist.Add(dynamicData);
            var workflowItem = new WorkflowItem
            {
                Id = Guid.NewGuid(),
                CatalogTitle = "R17 Applications",
                TypicalTitle = "CUSTOMER",
                SystemId = Guid.Parse("3ED2A98B-494F-42D4-B440-96DCF82A2847"),
                //SystemName = "T24WebAdapter",
                ActionType = "Input",
                Description = "Customer description",
                DynamicData = dyndatalist.ToArray(),
                //Parameters = "",
                Title = "Customer",
                Type = "Action",
                IsNegativeStep = false,
                ExpectedError = ""
            };

            var workflowItem1 = new WorkflowItem
            {
                Id = Guid.NewGuid(),
                CatalogTitle = "R17 Applications",
                TypicalTitle = "CUSTOMER",
                SystemId = Guid.Parse("3ED2A98B-494F-42D4-B440-96DCF82A2847"),
                //SystemName = "T24WebAdapter",
                ActionType = "Delete",
                Description = "Customer description",
                DynamicData = dyndatalist.ToArray(),
                //Parameters = "",
                Title = "Customer",
                Type = "Condition",
                IsNegativeStep = true,
                ExpectedError = "error deleting"
            };

            var workflowItemLink = new WorkflowItemLink
            {
                SourceId = workflowItem.Id,
                TargetId = workflowItem1.Id,
                State = (WorkflowItemLinkState)2
            };
            var pre = new GetSuggestionsResponseItem
            {
                WorkflowItem = workflowItem,
                WorkflowItemLink = workflowItemLink
            };
            preList.Add(pre);

            pre = new GetSuggestionsResponseItem
            {
                WorkflowItem = workflowItem1,
                WorkflowItemLink = workflowItemLink
            };
            preList.Add(pre);
            var currentItemId = Guid.Parse("3ED2A98B-494F-42D4-B440-96DCF82A2847");
            var getSuggestionsResponse = new GetSuggestionsResponse
            {
                CurrentItemId = currentItemId,
                PreItems = preList.ToArray(),
                PostItems = preList.ToArray()
            };

            list.Add(getSuggestionsResponse);

            preList = new List<GetSuggestionsResponseItem>();

            pre = new GetSuggestionsResponseItem
            {
                WorkflowItem = workflowItem,
                WorkflowItemLink = workflowItemLink
            };
            preList.Add(pre);

            pre = new GetSuggestionsResponseItem
            {
                WorkflowItem = workflowItem1,
                WorkflowItemLink = workflowItemLink
            };
            preList.Add(pre);
            currentItemId = Guid.Parse("3ED2A98B-494F-42D4-B440-96DCF82A2840");
            getSuggestionsResponse = new GetSuggestionsResponse
            {
                CurrentItemId = currentItemId,
                PreItems = preList.ToArray(),
                PostItems = preList.ToArray()
            };

            list.Add(getSuggestionsResponse);

            return list;
        }

        public static GetDataSetsItemsResponse GetAllDataSetItems()
        {
            GetDataSetsItemsResponse getDataSetsItem = new GetDataSetsItemsResponse();
            List<DataSet.DataSet> dataSets = new List<DataSet.DataSet>();
            var typicalId = Guid.Parse("CB6FCD12-D089-4C0C-8158-29779D449384");// Guid.NewGuid();
            var attributes = new List<Attribute>();
            var attribute = new Attribute
            { Name = "ID", Value = "10" };
            attributes.Add(attribute);

            attribute = new Attribute
            { Name = "SECTOR", Value = "30" };
            attributes.Add(attribute);

            attribute = new Attribute
            { Name = "GENDER", Value = "F" };
            attributes.Add(attribute);

            Rows row = new Rows();
            row.Attributes = attributes.ToArray();
            List<Rows> rows = new List<Rows>();
            rows.Add(row);

            attributes = new List<Attribute>();
            attribute = new Attribute
            { Name = "ID", Value = "30" };
            attributes.Add(attribute);

            attribute = new Attribute
            { Name = "SECTOR", Value = "20" };
            attributes.Add(attribute);

            attribute = new Attribute
            { Name = "GENDER", Value = "M" };
            attributes.Add(attribute);

            row = new Rows();
            row.Attributes = attributes.ToArray();
            rows.Add(row);

            DataSet.DataSet dataSet = new DataSet.DataSet
            {
                Id = Guid.NewGuid(),
                TypicalId = typicalId,
                Title = "Data set 1 - 100%",
                Coverage = 100,
                Items = rows.ToArray()
            };

            dataSets.Add(dataSet);

            typicalId = Guid.Parse("ABC03A9B-FBAF-4131-805E-48818A5DE03B");
            attributes = new List<Attribute>();
            attribute = new Attribute
            { Name = "ID", Value = "10" };
            attributes.Add(attribute);
            attribute = new Attribute
            { Name = "SECTOR", Value = "1000" };
            attributes.Add(attribute);
            attribute = new Attribute
            { Name = "INDUSTRY", Value = "30" };
            attributes.Add(attribute);
            row = new Rows();
            row.Attributes = attributes.ToArray();
            rows = new List<Rows>();
            rows.Add(row);

            typicalId = Guid.Parse("ABC03A9B-FBAF-4131-805E-48818A5DE03B");
            attributes = new List<Attribute>();
            attribute = new Attribute
            { Name = "ID", Value = "11" };
            attributes.Add(attribute);
            attribute = new Attribute
            { Name = "SECTOR", Value = "1001" };
            attributes.Add(attribute);
            attribute = new Attribute
            { Name = "INDUSTRY", Value = "35" };
            attributes.Add(attribute);
            row = new Rows();
            row.Attributes = attributes.ToArray();
            rows.Add(row);

            dataSet = new DataSet.DataSet
            {
                Id = Guid.NewGuid(),
                TypicalId = typicalId,
                Title = "Data set 1 - 100%",
                Coverage = 100,
                Items = rows.ToArray()
            };

            dataSets.Add(dataSet);

            typicalId = Guid.Parse("830B7AF9-AC5E-43BC-9858-7760199577AC");
            attributes = new List<Attribute>();
            attribute = new Attribute
            { Name = "ID", Value = "10" };
            attributes.Add(attribute);
            attribute = new Attribute
            { Name = "Name", Value = "test" };
            attributes.Add(attribute);
            attribute = new Attribute
            { Name = "Age", Value = "30" };
            attributes.Add(attribute);
            row = new Rows();
            row.Attributes = attributes.ToArray();
            rows = new List<Rows>();
            rows.Add(row);

            typicalId = Guid.Parse("85434986-25de-4f9b-b2ef-000171b2ba58");
            attributes = new List<Attribute>();
            attribute = new Attribute
            { Name = "ID", Value = "16" };
            attributes.Add(attribute);
            attribute = new Attribute
            { Name = "Name", Value = "test test3" };
            attributes.Add(attribute);
            attribute = new Attribute
            { Name = "Age", Value = "33" };
            attributes.Add(attribute);
            row = new Rows();
            row.Attributes = attributes.ToArray();

            dataSet = new DataSet.DataSet
            {
                Id = Guid.NewGuid(),
                TypicalId = typicalId,
                Title = "Data set 1 - 100%",
                Coverage = 100,
                Items = rows.ToArray()
            };

            dataSets.Add(dataSet);
            getDataSetsItem.DataSets = dataSets.ToArray();

            return getDataSetsItem;
        }
    }
}