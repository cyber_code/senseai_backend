﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralEngine
{
    public enum ActionType
    {
        Menu,
        See,
        Delete,
        Authorize,
        Reverse,
        SingOff,
        Tab,
        Input,
        EnquiryAction,
        EnquiryResult,
        Verify
    }

    public enum TypicalType
    {
        Application = 1,
        Version = 2,
        AA = 3,
        Enquiry = 4,
        ProjectResourcing = 5
    }
}