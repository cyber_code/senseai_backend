﻿using Newtonsoft.Json;
using Core.Abstractions.Bindings;
using Core.Configuration;
using NeuralEngine.CheckServiceModel;
using NeuralEngine.DataSet;
using NeuralEngine.Suggestion;
using System;
using System.Collections.Generic;
using System.Linq;
using NeuralEngine.DefectsModel;

namespace NeuralEngine
{
    public class NeuralEngineClient : RestClient, INeuralEngineClient
    {
        private SenseAIConfig _configuration;

        public NeuralEngineClient(INeuralEngineBindingConfiguration bindingConfiguration, SenseAIConfig configuration)
            : base(bindingConfiguration)
        {
            _configuration = configuration;
        }

        private INeuralEngineBindingConfiguration BindingConfiguration => (INeuralEngineBindingConfiguration)_bindingConfiguration;

        public GetSuggestionsResponse GetSuggestions(GetSuggestionsRequest request)
        {
            return !_configuration.EnableNeuralEngine ? notNeural() : neural();
            GetSuggestionsResponse neural()
            {
                /*Call the http ML*/
                return PostAsJsonAsync<GetSuggestionsResponse>(request, $"{BindingConfiguration.Client.Endpoint}{"/suggestions"}").Result;
            }
            GetSuggestionsResponse notNeural()
            {
                /*Call the DummyData */
                //return DummyData.GetSuggestions().Where(ct => ct.CurrentItemId == request.CurrentItemId).FirstOrDefault();
                return DummyData.GetSuggestions().FirstOrDefault();
            }
        }

        public WorkflowCoverageResponse GetWorkflowCoverage(WorkflowCoverageRequest request)
        {
            return !_configuration.EnableNeuralEngine ? notNeural() : neural();
            WorkflowCoverageResponse neural()
            {
                /*Call the http ML*/
                return PostAsJsonAsync<WorkflowCoverageResponse>(request, $"{BindingConfiguration.Client.Endpoint}{"/getCoverage"}").Result;
            }
            WorkflowCoverageResponse notNeural()
            {
                return DummyData.GetWorkflowCoverage();
            }
        }


        public GetDataSetCoverageResponse GetDataSetCoverage(GetDataSetCoverageRequest request)
        {
            return !_configuration.EnableNeuralEngine ? notNeural() : neural();
            GetDataSetCoverageResponse neural()
            {
                /*Call the http ML*/
                return PostAsJsonAsync<GetDataSetCoverageResponse>(request, $"{BindingConfiguration.Client.Endpoint}{"/getDataCoverageforDataSet"}").Result;
            }
            GetDataSetCoverageResponse notNeural()
            {
                return new GetDataSetCoverageResponse() { Coverage = 0};
            }
        }
        public GetDataSetsItemsResponse GetDataSetItems(GetDataSetsItemsRequest request)
        {
            return !_configuration.EnableNeuralEngine ? notNeural() : neural();
            GetDataSetsItemsResponse notNeural()
            {
                /*Call the DummyData*/
                GetDataSetsItemsResponse getDataSetsItemsResponse = new GetDataSetsItemsResponse();
                getDataSetsItemsResponse.DataSets = DummyData.GetAllDataSetItems().DataSets.Where(ct => ct.TypicalId == request.TypicalId).ToArray();
                return getDataSetsItemsResponse;
            }
            GetDataSetsItemsResponse neural()
            {
                /*Call the http ML*/
                return PostAsJsonAsync<GetDataSetsItemsResponse>(request, $"{BindingConfiguration.Client.Endpoint}{"/datasets"}").Result;
            }
        }

        public GetPathsResponse GetPaths(GetPathsRequest request)
        {
            GetPathsResponse response = new GetPathsResponse();
            try
            {
                response = PostAsJsonAsync<GetPathsResponse>(request, $"{BindingConfiguration.Client.Endpoint}{"/paths"}").Result;
            }
            catch
            {
            }
            return response;
        }

        public bool PostIssueWorkflow(IssueWorkflowRequest request)
        {
            return !_configuration.EnableNeuralEngine ? true : Neural();

            bool Neural()
            {
                return PostAsJsonAsync<bool>(request, $"{BindingConfiguration.Client.Endpoint}{"/addWorkflow"}").Result;
            }
        }

        public SaveWorkflowDataCoverageResponse SaveWorkflowDataCoverage(SaveWorkflowDataCoverageRequest request)
        {
            return !_configuration.EnableNeuralEngine ? new SaveWorkflowDataCoverageResponse { Result = false } : Neural();

            SaveWorkflowDataCoverageResponse Neural()
            {
                return PostAsJsonAsync<SaveWorkflowDataCoverageResponse>(request, $"{BindingConfiguration.Client.Endpoint}{"/SaveWorkflowDataCoverage"}").Result;
            }
        }

        public List<ServiceInfo> CheckServices()
        {
            var result = GetAsync<IDictionary<string, bool>>($"{BindingConfiguration.Client.Endpoint}{"/check"}").Result;

            List<ServiceInfo> services = new List<ServiceInfo>();
            services.Add(new ServiceInfo { Title = "ML Api Running", Active = result["serverRunning"], Info = BindingConfiguration.Client.Endpoint });
            services.Add(new ServiceInfo { Title = "ML DB Running", Active = result["databaseRunning"], Info = BindingConfiguration.Client.Endpoint });

            return services;
        }

        public GetComputerGeneratedWorkflowsResponse GetWorkflows(GetComputerGeneratedWorkflowsRequest request)
        {
            return !_configuration.EnableNeuralEngine ? notNeural() : neural();
            GetComputerGeneratedWorkflowsResponse notNeural()
            {
                /*Call the DummyData*/
                //GetDataSetsItemsResponse getDataSetsItemsResponse = new GetDataSetsItemsResponse();
                //getDataSetsItemsResponse.DataSets = DummyData.GetAllDataSetItems().DataSets.Where(ct => ct.TypicalId == request.TypicalId).ToArray();
                return null;
            }
            GetComputerGeneratedWorkflowsResponse neural()
            {
                /*Call the http ML*/
                return PostAsJsonAsync<GetComputerGeneratedWorkflowsResponse>(request, $"{BindingConfiguration.Client.Endpoint}{"/getworkflows"}").Result;
            }
        }

        public DefectResponse SaveDefects(DefectPostModel model)
        {
            return !_configuration.EnableNeuralEngine ? notNeural() : neural();
            DefectResponse notNeural()
            {
                return new DefectResponse { Successful = false, Description = "Neural engine integration not enabled!" };
            }
            DefectResponse neural()
            {
                return PostAsJsonAsync<DefectResponse>(model, $"{BindingConfiguration.Client.Endpoint}/addDefects").Result;
            }
        }

        public DefectResponse DeleteDefects(string HashId, string DefectId)
        {
            return !_configuration.EnableNeuralEngine ? notNeural() : neural();
            DefectResponse notNeural()
            {
                return new DefectResponse { Successful = false, Description = "Neural engine integration not enabled!" };
            }
            DefectResponse neural()
            {
                return GetAsync<DefectResponse>($"{BindingConfiguration.Client.Endpoint}/deleteDefectById/hashId/{HashId}/defectId/{DefectId}").Result;
            }
        }
    }
}