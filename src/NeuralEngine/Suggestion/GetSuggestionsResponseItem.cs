﻿namespace NeuralEngine.Suggestion
{
    public class GetSuggestionsResponseItem
    {
        public WorkflowItem WorkflowItem { get; set; }

        public WorkflowItemLink WorkflowItemLink { get; set; }
    }
}