﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace NeuralEngine.Suggestion
{
    public sealed class GetPathsRequest : IBaseId
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get { return Guid.Parse("b5bcc43f-afc8-4f67-8eb1-b633994ce363"); } }

        public Guid CatalogId { get; set; }

        public int VersionId { get; set; }

        public string HashId
        {
            get
            {
                return BitConverter.ToString(MD5.Create()
                    .ComputeHash(UTF8Encoding.UTF8.GetBytes(
                        TenantId.ToString() + "|" +
                        ProjectId.ToString() + "|" +
                        SubProjectId.ToString() + "|" +
                        SystemId.ToString() + "|" +
                        CatalogId.ToString()))).Replace("-", string.Empty);
            }
        }

        public WorkflowItem[] WorkflowItems { get; set; }

        public WorkflowItemLink[] WorkflowItemLinks { get; set; }
    }
}