﻿using System;

namespace NeuralEngine.Suggestion
{
    public enum WorkflowItemLinkState
    {
        None,
        Yes,
        No
    }

    public sealed class WorkflowItemLink
    {
        public Guid SourceId { get; set; }

        public Guid TargetId { get; set; }

        public WorkflowItemLinkState State { get; set; }
    }
}