﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace NeuralEngine.Suggestion
{
    public sealed class GetSuggestionsRequest : IBaseId
    {
        public Guid TenantId { get; set; }

        public Guid ProjectId { get; set; }

        public Guid SubProjectId { get; set; }

        public Guid SystemId { get { return Guid.Parse("b5bcc43f-afc8-4f67-8eb1-b633994ce363"); } }

        public Guid CatalogId { get; set; }

        public int VersionId { get; set; }

        public string HashId
        {
            get
            {
                return BitConverter.ToString(MD5.Create()
                    .ComputeHash(UTF8Encoding.UTF8.GetBytes(
                        TenantId.ToString() + "|" +
                        ProjectId.ToString() + "|" +
                        SubProjectId.ToString() + "|" +
                        SystemId.ToString() + "|" +
                        CatalogId.ToString()))).Replace("-", string.Empty);
            }
        }

        public Guid CurrentItemId { get; set; }

        public WorkflowItem[] WorkflowItems { get; set; }

        public WorkflowItemLink[] WorkflowItemLinks { get; set; }

        public void AppendLinkWorkflow(WorkflowItem linkedWorkflowItem, GetSuggestionsRequest linkedWorkflow)
        {
            List<WorkflowItem> _items = this.WorkflowItems.ToList();
            List<WorkflowItemLink> _itemLinks = this.WorkflowItemLinks.ToList();
            var lkFirstWF = linkedWorkflow.WorkflowItems.FirstOrDefault(ct => ct.Type == "Start" && linkedWorkflow.WorkflowItemLinks.FirstOrDefault(wf => wf.TargetId == ct.Id) == null);
            lkFirstWF.Title = "Switch System";
            var lkLastWf = linkedWorkflow.WorkflowItems.Where(ct => linkedWorkflow.WorkflowItemLinks.FirstOrDefault(wf => wf.SourceId == ct.Id) == null);

            var nextItem = this.WorkflowItemLinks.FirstOrDefault(ct => ct.SourceId == linkedWorkflowItem.Id);

            var itemF = new WorkflowItemLink
            {
                SourceId = linkedWorkflowItem.Id,
                TargetId = lkFirstWF.Id,
                State = WorkflowItemLinkState.Yes
            };

            _itemLinks.Add(itemF);

            foreach (var item in linkedWorkflow.WorkflowItems)
            {
                if (item.Type == "Start" && item.Title.ToLower() == "start")
                    item.Title = "Switch System";

                _items.Add(item);
            }

            foreach (var itemLink in linkedWorkflow.WorkflowItemLinks)
            {
                _itemLinks.Add(itemLink);
            }
            if (nextItem != null)
            {
                foreach (var wfi in lkLastWf)
                {
                    var itemL = new WorkflowItemLink { SourceId = wfi.Id, TargetId = nextItem.TargetId, State = WorkflowItemLinkState.Yes };
                    _itemLinks.Add(itemL);
                }
                _itemLinks.Remove(nextItem);
            }
            this.WorkflowItems = _items.ToArray();
            this.WorkflowItemLinks = _itemLinks.ToArray();
        }
    }
}