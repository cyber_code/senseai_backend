﻿using System;

namespace NeuralEngine.Suggestion
{
    public sealed class WorkflowItem
    {
        public string ActionType { get; set; }

        public Guid CatalogId { get; set; }

        public string CatalogTitle { get; set; }

        public string Description { get; set; }

        public DynamicData[] DynamicData { get; set; }

        public Guid Id { get; set; }

        public Guid SystemId { get; set; }

        public string Title { get; set; }

        public string Type { get; set; }

        public Guid TypicalId { get; set; }

        public string TypicalTitle { get; set; }

        public string TypicalType { get; set; }

        public Guid WorkflowId { get; set; }

        public bool IsNegativeStep { get; set; }

        public string ExpectedError { get; set; }
    }
}