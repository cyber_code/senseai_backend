﻿using System;

namespace NeuralEngine.Suggestion
{
    public sealed class WorkflowCoverageResponse
    {
        public Guid WorkflowId { get; set; }
        public decimal WorkflowCoverage { get; set; }
        public decimal DataCoverage { get; set; }
        public string Status { get; set; }
    }

   
}