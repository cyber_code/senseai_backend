﻿using System.Collections.Generic;

namespace NeuralEngine.Suggestion
{
    public sealed class Workflows
    {
        public List<WorkflowItem> WorkflowItem { get; set; }

        public List<WorkflowItemLink> WorkflowItemLink { get; set; }

        public List<WorkflowPath> WorkflowPath { get; set; }
    }
}