﻿using System.Collections.Generic;

namespace NeuralEngine.Suggestion
{
    public sealed class GetComputerGeneratedWorkflowsResponse
    {
        public List<Workflows> Workflows { get; set; }
    }
}