﻿using System;

namespace NeuralEngine.Suggestion
{
    public sealed class GetPathsResponse
    {
        public WorkflowPath[] WorkflowPaths { get; set; }
    }

    public sealed class WorkflowPath
    {
        public int Number { get; set; }

        public decimal Coverage { get; set; }

        public bool IsBestPath { get; set; }

        public Guid[] ItemIds { get; set; }
    }
}