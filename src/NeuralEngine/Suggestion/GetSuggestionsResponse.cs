﻿using System;

namespace NeuralEngine.Suggestion
{
    public sealed class GetSuggestionsResponse
    {
        public Guid CurrentItemId { get; set; }

        public GetSuggestionsResponseItem[] PreItems { get; set; }

        public GetSuggestionsResponseItem[] PostItems { get; set; }
    }
}