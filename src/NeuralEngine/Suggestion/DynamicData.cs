﻿using System;

namespace NeuralEngine.Suggestion
{
    public sealed class DynamicData
    {
        public Guid SourceWorkflowItemId { get; set; }

        public string SourceAttributeTitle { get; set; }

        public string TargetAttributeTitle { get; set; }

        public string FormulaText { get; set; }
    }
}