﻿using Application.Services;
using Commands.ProcessModule.Issues;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.ProcessModule.Issues;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class ConfigureControllerIssueTypesTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _issueTypeId;
        private static Guid _issueTypeFieldId;

        public ConfigureControllerIssueTypesTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddIssueType_should_success()
        {
            var issuefield = new IssueTypeFields()
            {
                IssueTypeFieldsNameId = Variables.IssueTypeFieldsNameId,
                Checked = true
            };
            var issuefields = new IssueTypeFields[1];
            issuefields[0] = issuefield;
            //Arrange
            var issueType = new AddIssueType
            {
                SubProjectId = Variables.SubProjectId,
                Title = "Title test 3",
                Description = "Description test 3",
                IssueTypeFields =issuefields

            };

            //Act
            var response = await PostAsync("api/Process/Configure/AddIssueType", issueType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddIssueTypeResult>();
            _issueTypeId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateIssueType_should_success()
        {
            var issuefield = new IssueTypeFields()
            {
                IssueTypeFieldsNameId = Variables.IssueTypeFieldsNameId,
                Checked = true
            };
            var issuefields = new IssueTypeFields[1];
            issuefields[0] = issuefield;
            //Arrange
            bool expected = true;
            UpdateIssueType updateIssueType = new UpdateIssueType
            {
                Id = _issueTypeId,
                SubProjectId = Variables.SubProjectId,
                Title = "Title updated from test",
                Description = "Description updated from test",
                IssueTypeFields = issuefields
            };

            //Act
            var response = await PutAsync("api/Process/Configure/UpdateIssueType", updateIssueType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetIssuesTypes_should_success()
        {
            //Arrange
            int expected = 4;

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypes?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssueTypesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetIssueType_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueType?Id={_issueTypeId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetIssueTypeResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }
        [Fact, TestPriority(2)]
        public async Task GetIssuesTypeFieldsNames_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypeFieldsNames");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssueTypeFieldsNamesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetIssueTypeFieldsName_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypeFieldsName?Id={Variables.IssueTypeFieldsNameId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetIssueTypeFieldsNameResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }
        [Fact, TestPriority(2)]
        public async Task GetIssuesTypeFields_should_success()
        {
            //Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypeFields?IssueTypeId={_issueTypeId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssueTypesResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual.Count);
            _issueTypeFieldId = actual[0].Id;
        }

        [Fact, TestPriority(2)]
        public async Task GetIssueTypeField_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypeField?Id={_issueTypeFieldId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetIssueTypeResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }
        [Fact, TestPriority(3)]
        public async Task DeleteissueType_should_sucess()
        {
            //Arrange
            bool expected = true;

            //Act
            var response = await DeleteAsync($"api/Process/Configure/DeleteIssueType?Id={_issueTypeId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetIssuesTypes_after_delete_should_success()
        {
            //Arrange
            int expected = 3;

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypes?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssueTypesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task AddIssueType_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";
            var issueType = new AddIssueType
            {
                SubProjectId = Guid.Empty,
                Title = null,
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Process/Configure/AddIssueType", issueType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddIssueType_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            var issueType = new AddIssueType
            {
                SubProjectId = Guid.NewGuid(),
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Process/Configure/AddIssueType", issueType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateIssueType_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            UpdateIssueType updateIssueType = new UpdateIssueType
            {
                SubProjectId = Guid.NewGuid(),
                Title = "test"
            };

            //Act
            var response = await PutAsync("api/Process/Configure/UpdateIssueType", updateIssueType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateIssueType_empty_subProjectId_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";
            UpdateIssueType updateIssueType = new UpdateIssueType
            {
                Id = Guid.NewGuid(),
                Title = "test"
            };

            //Act
            var response = await PutAsync("api/Process/Configure/UpdateIssueType", updateIssueType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateIssueType_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            UpdateIssueType updateIssueType = new UpdateIssueType
            {
                Id = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
            };

            //Act
            var response = await PutAsync("api/Process/Configure/UpdateIssueType", updateIssueType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeleteissueType_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty!";

            //Act
            var response = await DeleteAsync($"api/Process/Configure/DeleteIssueType?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueType_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty!";

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueType?Id={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueType_empty_result_should_fail()
        {
            //Arrange
            string expected = "No issue type found.";

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueType?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueTypes_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty!";

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypes?SubProjectId={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueTypes_empty_result_should_fail()
        {
            //Arrange
            string expected = "No issue types found in sub project.";

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypes?SubProjectId={ Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueTypeField_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty!";

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypeField?Id={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueTypeField_empty_result_should_fail()
        {
            //Arrange
            string expected = "No issue type fields found.";

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypeField?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueTypeFields_should_fail()
        {
            //Arrange
            string expected = "IssueTypeId cannot be empty!";

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypeFields?IssueTypeId={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueTypeFields_empty_result_should_fail()
        {
            //Arrange
            string expected = "No issue types fields found in issue type.";

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypeFields?IssueTypeId={ Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueTypeFieldsName_empty_result_should_fail()
        {
            //Arrange
            string expected = "No issue type fields name found.";

            //Act
            var response = await GetAsync($"api/Process/Configure/GetIssueTypeFieldsName?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        
    }
}
