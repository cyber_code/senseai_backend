﻿using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using WebApi.IntegrationTest.Infrastructure;
using Commands.ProcessModule.Processes;
using Queries.ProcessModule.Processes;
using SenseAI.Domain;
using Application.Services;
using Messaging.Commands;
using Messaging.Queries;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerProcessTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _processId;
        private static Guid _pastProcessId;

        public BrowseControllerProcessTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddProcess_should_success()
        {
            // Arrange
            var process = new AddProcess
            {
                SubProjectId = Variables.SubProjectId,
                HierarchyId = Variables.HierarchyId,
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Title = "Process Title 1",
                Description = "Process Description 1",
                RequirementPriorityId = Variables.RequirementPriorityId,
                RequirementTypeId = Variables.RequirementTypeId,
                TypicalId = Guid.NewGuid()
            };

            // Act
            var response = await PostAsync("api/Process/Browse/AddProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddProcessResult>();
            _processId = actual.Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateProcess_should_success()
        {
            // Arrange
            bool expected = true;
            UpdateProcess updateProcess = new UpdateProcess
            {
                Id = _processId,
                SubProjectId = Variables.SubProjectId,
                HierarchyId = Variables.HierarchyId,
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Title = "Process Title 1 update",
                Description = "Process Description 1 update",
                RequirementPriorityId = Variables.RequirementPriorityId,
                RequirementTypeId = Variables.RequirementTypeId,
                TypicalId = Guid.NewGuid()
            };

            // Act
            var response = await PutAsync($"api/Process/Browse/UpdateProcess", updateProcess);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task RenameProcessTitle_should_success()
        {
            // Arrange
            bool expected = true;
            RenameProcessTitle renameProcessTitle = new RenameProcessTitle
            {
                ProcessId = _processId,
                Title = "Title modified"
            };

            // Act
            var response = await PutAsync("api/Process/Browse/RenameProcessTitle", renameProcessTitle);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task EditProcessDescription_should_success()
        {
            // Arrange
            bool expected = true;
            EditProcessDescription editProcessDescription = new EditProcessDescription
            {
                ProcessId = _processId,
                Description = "Description modified"
            };

            // Act
            var response = await PutAsync("api/Process/Browse/EditProcessDescription", editProcessDescription);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetProcess_should_success()
        {
            // Arrange

            // Act
            var response = await GetAsync($"api/Process/Browse/GetProcess?Id={_processId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetProcessResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }


        [Fact, TestPriority(2)]
        public async Task GetProcesses_should_success()
        {
            // Arrange
            int expected = 2;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetProcesses?hierarchyId={Variables.HierarchyId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetProcessesResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetProcessesBySubProject_should_success()
        {
            // Arrange
            int expected = 2;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetProcessesBySubProject?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetProcessesBySubProjectResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetAllProcessWorkflowsBySubProject_should_success()
        {
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetAllProcessWorkflowsBySubProject?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetAllProcessWorkflowsBySubProjectResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetAllProcessTestCasesBySubProject_should_success()
        {
            // Arrange
            int expected = 0;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetAllProcessTestCasesBySubProject?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetAllProcessWorkflowsBySubProjectResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetAllProcessOpenIssuesBySubProject_should_success()
        {
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetAllProcessOpenIssuesBySubProject?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetAllProcessWorkflowsBySubProjectResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }


        [Fact, TestPriority(3)]
        public async Task PasteProcess_should_success()
        {
            // Arrange
            PasteProcess pasteProcess = new PasteProcess
            {
                ProcessId = _processId,
                HierarchyId = Variables.HierarchyId,
                Type = SenseAI.Domain.PasteType.Copy
            };

            //Act
            var response = await PostAsync("api/process/browse/PasteProcess", pasteProcess);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<PasteProcess>();
            _pastProcessId = actual.ProcessId;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.ProcessId.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(4)]
        public async Task DeleteProcess_should_success()
        {
            // Arrange
            bool expected = true;

            // Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteProcess?Id={_processId}&ForceDelete=true");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }


        [Fact, TestPriority(5)]
        public async Task GetProcesses_after_copy_past_should_success()
        {
            // Arrange
            int expected = 2;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetProcesses?hierarchyId={Variables.HierarchyId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetProcessesResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(6)]
        public async Task ChangeProcessHierarchy_should_success()
        {
            // Arrange
            bool expected = true;
            ChangeProcessHierarchy changeProcessHierarchy = new ChangeProcessHierarchy
            {
                ProcessId = _pastProcessId,
                HierarchyId = Guid.NewGuid()
            };

            // Act
            var response = await PutAsync("api/Process/Browse/ChangeProcessHierarchy", changeProcessHierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(7)]
        public async Task DeleteProcess_after_copy_past_should_success()
        {
            // Arrange
            bool expected = true;

            // Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteProcess?Id={_pastProcessId}&ForceDelete=true");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(8)]
        public async Task GetProcesses_after_delete_should_success()
        {
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetProcesses?hierarchyId={Variables.HierarchyId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetProcessesResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(8)]
        public async Task AddProcess_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";
            var process = new AddProcess
            {
                SubProjectId = Guid.Empty,
                HierarchyId = Guid.NewGuid(),
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Title = "Process Title 1",
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task AddProcess_empty_hierarchyId_should_fail()
        {
            // Arrange
            string expected = "HierarchyId cannot be empty!";
            var process = new AddProcess
            {
                SubProjectId = Guid.NewGuid(),
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Title = "Process Title 1",
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task AddProcess_empty_createdId_should_fail()
        {
            // Arrange
            string expected = "CreatedId cannot be empty!";
            var process = new AddProcess
            {
                SubProjectId = Guid.NewGuid(),
                HierarchyId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Title = "Process Title 1",
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        //[Fact, TestPriority(8)]
        //public async Task AddProcess_empty_ownerId_should_fail()
        //{
        //    // Arrange
        //    string expected = "OwnerId cannot be empty!";
        //    var process = new AddProcess
        //    {
        //        SubProjectId = Guid.NewGuid(),
        //        HierarchyId = Guid.NewGuid(),
        //        CreatedId = Guid.NewGuid(),
        //        ProcessType = ProcessType.Generic,
        //        Title = "Process Title 1",
        //        Description = "Process Description 1"
        //    };

        //    //Act
        //    var response = await PostAsync("api/Process/Browse/AddProcess", process);
        //    response.EnsureSuccessStatusCode();
        //    var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

        //    //Assert
        //    Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        //}

        [Fact, TestPriority(8)]
        public async Task AddProcess_empty_not_specified_process_type_should_fail()
        {
            // Arrange
            string expected = "ProcessType must be either Generic or Aris Type!";
            var process = new AddProcess
            {
                SubProjectId = Guid.NewGuid(),
                HierarchyId = Guid.NewGuid(),
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                Title = "Process Title 1",
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task AddProcess_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";
            var process = new AddProcess
            {
                SubProjectId = Guid.NewGuid(),
                HierarchyId = Guid.NewGuid(),
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task UpdateProcess_should_fail()
        {
            // Arrange
            string expected = "ProcessId cannot be empty!";
            var process = new UpdateProcess
            {
                SubProjectId = Guid.NewGuid(),
                HierarchyId = Guid.NewGuid(),
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Title = "Process Title 1",
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task UpdateProcess_empty_subProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";
            var process = new UpdateProcess
            {
                Id = Guid.NewGuid(),
                SubProjectId = Guid.Empty,
                HierarchyId = Guid.NewGuid(),
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Title = "Process Title 1",
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task UpdateProcess_empty_hierarchyId_should_fail()
        {
            // Arrange
            string expected = "HierarchyId cannot be empty!";
            var process = new UpdateProcess
            {
                Id = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Title = "Process Title 1",
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task UpdateProcess_empty_createdId_should_fail()
        {
            // Arrange
            string expected = "CreatedId cannot be empty!";
            var process = new UpdateProcess
            {
                Id = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                HierarchyId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Title = "Process Title 1",
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        //[Fact, TestPriority(8)]
        //public async Task UpdateProcess_empty_ownerId_should_fail()
        //{
        //    // Arrange
        //    string expected = "OwnerId cannot be empty!";
        //    var process = new UpdateProcess
        //    {
        //        Id = Guid.NewGuid(),
        //        SubProjectId = Guid.NewGuid(),
        //        HierarchyId = Guid.NewGuid(),
        //        CreatedId = Guid.NewGuid(),
        //        ProcessType = ProcessType.Generic,
        //        Title = "Process Title 1",
        //        Description = "Process Description 1"
        //    };

        //    //Act
        //    var response = await PutAsync("api/Process/Browse/UpdateProcess", process);
        //    response.EnsureSuccessStatusCode();
        //    var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

        //    //Assert
        //    Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        //}

        [Fact, TestPriority(8)]
        public async Task UpdateProcess_empty_not_specified_process_type_should_fail()
        {
            // Arrange
            string expected = "ProcessType must be either Generic or Aris Type!";
            var process = new UpdateProcess
            {
                Id = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                HierarchyId = Guid.NewGuid(),
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                Title = "Process Title 1",
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task UpdateProcess_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";
            var process = new UpdateProcess
            {
                Id = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                HierarchyId = Guid.NewGuid(),
                CreatedId = Guid.NewGuid(),
                OwnerId = Guid.NewGuid(),
                ProcessType = ProcessType.Generic,
                Description = "Process Description 1",
                TypicalId = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateProcess", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }


        [Fact, TestPriority(8)]
        public async Task DeleteProcess_should_fail()
        {
            //Arrange
            string expected = "ProcessId cannot be empty!";

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteProcess?Id={Guid.Empty}&ForceDelete=true");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task PasteProcess_should_fail()
        {
            //Arrange
            string expected = "ProcessId cannot be empty!";
            var prcPaste = new PasteProcess
            {
                ProcessId = Guid.Empty,
                HierarchyId = Guid.Empty,
                Type = SenseAI.Domain.PasteType.Copy
            };

            //Act
            var response = await PostAsync("api/process/browse/PasteProcess", prcPaste);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task PasteProcess_empty_hierarchyId_should_fail()
        {
            //Arrange
            string expected = "HierarchyId cannot be empty!";
            var prcPaste = new PasteProcess
            {
                ProcessId = Guid.NewGuid(),
                Type = SenseAI.Domain.PasteType.Copy
            };

            //Act
            var response = await PostAsync("api/process/browse/PasteProcess", prcPaste);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }


        [Fact, TestPriority(8)]
        public async Task GetProcess_should_fail()
        {
            //Arrange
            string expected = "processid cannot be empty!";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcess?Id={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetProcess_empty_result_should_fail()
        {
            //Arrange
            string expected = "No Process found.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcess?Id={ Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetProcesses_should_fail()
        {
            //Arrange
            string expected = "subprojectid cannot be empty!";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcesses?SubProjectId={Guid.Empty}&hierarchyId={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetProcesses_empty_hierarchyId_should_fail()
        {
            //Arrange
            string expected = "hierarchyId cannot be empty!";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcesses?SubProjectId={Guid.NewGuid()}&hierarchyId={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }


        [Fact, TestPriority(8)]
        public async Task GetProcesses_empty_result_should_fail()
        {
            //Arrange
            string expected = "No Processes found in sub projectId.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcesses?SubProjectId={Guid.NewGuid()}&hierarchyId={ Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task RenameProcessTitle_should_fail()
        {
            // Arrange
            string expected = "ProcessId cannot be empty!";
            RenameProcessTitle rPt = new RenameProcessTitle()
            {
                Title = String.Empty
            };

            // Act
            var response = await PutAsync("api/Process/Browse/RenameProcessTitle", rPt);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task RenameProcessTitle_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";
            RenameProcessTitle rPt = new RenameProcessTitle()
            {
                ProcessId = Guid.NewGuid()
            };

            // Act
            var response = await PutAsync("api/Process/Browse/RenameProcessTitle", rPt);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task EditProcessDescription_should_fail()
        {
            // Arrange
            string expected = "ProcessId cannot be empty!";
            EditProcessDescription ePd = new EditProcessDescription()
            {
                Description = String.Empty
            };

            // Act
            var response = await PutAsync("api/Process/Browse/EditProcessDescription", ePd);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task ChangeProcessHierarchy_should_fail()
        {
            // Arrange
            string expected = "ProcessId cannot be empty!";
            ChangeProcessHierarchy cPh = new ChangeProcessHierarchy()
            {
                HierarchyId = Guid.Empty
            };

            // Act
            var response = await PutAsync("api/Process/Browse/ChangeProcessHierarchy", cPh);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task ChangeProcessHierarchy_empty_hierarchyId_should_fail()
        {
            // Arrange
            string expected = "HierarchyId cannot be empty!";
            ChangeProcessHierarchy cPh = new ChangeProcessHierarchy()
            {
                ProcessId = Guid.NewGuid()
            };

            // Act
            var response = await PutAsync("api/Process/Browse/ChangeProcessHierarchy", cPh);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetProcesessBySubProject_should_fail()
        {
            //Arrange
            string expected = "subprojectid cannot be empty!";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessesBySubProject?SubProjectId={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetProcesessBySubProject_empty_result_should_fail()
        {
            //Arrange
            string expected = "No Processes found in sub project.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessesBySubProject?SubProjectId={ Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetAllProcessWorkflowsBySubProject_should_fail()
        {
            //Arrange
            string expected = "subprojectid cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetAllProcessWorkflowsBySubProject?SubProjectId={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetAllProcessWorkflowsBySubProject_empty_result_should_fail()
        {
            //Arrange
            string expected = "No Workflows found in sub project.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetAllProcessWorkflowsBySubProject?SubProjectId={ Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetAllProcessTestCasesBySubProject_should_fail()
        {
            //Arrange
            string expected = "subprojectid cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetAllProcessTestCasesBySubProject?SubProjectId={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetAllProcessTestCasesBySubProject_empty_result_should_fail()
        {
            //Arrange
            string expected = "No Testcase found in sub project.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetAllProcessTestCasesBySubProject?SubProjectId={ Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetAllProcessOpenIssuesBySubProject_should_fail()
        {
            //Arrange
            string expected = "subprojectid cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetAllProcessOpenIssuesBySubProject?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetAllProcessOpenIssuesBySubProject_empty_result_should_fail()
        {
            //Arrange
            string expected = "No Issues found in sub project.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetAllProcessOpenIssuesBySubProject?SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}