﻿using FluentAssertions;
using WebApi.IntegrationTest.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Queries.DesignModule.QualitySuite;
using Messaging.Queries;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class PlanControllerATSTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static int _projectNoderef;
        private static int _productNoderef;
        private static int _featureNoderef;
        private static int _useCaseNoderef;

        public PlanControllerATSTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task GetATSProjects_should_succeed()
        {
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync($"api/Plan/GetATSProjects");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetATSProjectResult>();
            _projectNoderef = int.Parse(actual.FirstOrDefault().NoderefField);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Count.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(1)]
        public async Task GetATSSubprojects_should_succeed()
        {
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync($"api/Plan/GetATSSubProjects?NoderefField={_projectNoderef}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetATSProjectResult>();

            //Asserts
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Count.Should().BeGreaterOrEqualTo(expected);
        }
         
        [Fact, TestPriority(2)]
        public async Task GetATSProducts_should_succeed()
        {
            //Arrange
            int expected = 0;

            //Act
            var response = await GetAsync($"api/Plan/GetATSProducts?ProjectId={_projectNoderef}&SubProjectId={_projectNoderef}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetATSProductsResult>();
            if(actual != null && actual.Any())
            int.TryParse(actual?.FirstOrDefault().NoderefField, out _productNoderef);
            
            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual?.Count.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(3)]
        public async Task GetATSFeatures_should_succeed()
        {
            // Arrange
            int expected = 0;

            // Act
            var response = await GetAsync($"api/Plan/GetATSFeatures?ProjectId={_projectNoderef}&SubProjectId={_projectNoderef}&ProductId={_productNoderef}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetATSFeaturesResult>(); 

            int.TryParse(actual?.FirstOrDefault().NoderefField, out _featureNoderef);
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual?.Count.Should().BeGreaterOrEqualTo(expected);
        }


        [Fact, TestPriority(4)]
        public async Task GetATSUsecases_should_succeed()
        {
            if (_featureNoderef == 0)//In case no use cases found
            {
                Assert.True(true);
                return;
            }
            // Arrange
            int expected = 0;

            // Act
            var response = await GetAsync($"api/Plan/GetATSUsecases?ProjectId={_projectNoderef}&SubProjectId={_projectNoderef}&FeatureId={_featureNoderef}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetATSUsecasesResult>();
            if (actual.Count > 0)
                _useCaseNoderef = int.Parse(actual.FirstOrDefault().NoderefField);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual?.Count.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(5)]
        public async Task GetATSRequirements_should_succeed()
        {
            if (_useCaseNoderef == 0)//In case no use cases found
            {
                Assert.True(true);
                return;
            }
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync($"api/Plan/GetATSRequirements?ProjectId={_projectNoderef}&SubProjectId={_projectNoderef}&UseCaseId={_useCaseNoderef}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetATSRequirementsResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual?.Count.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(6)]
        public async Task GetATSSystems_should_succeed()
        {
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync($"api/Plan/GetATSSystems?ProjectId=1&SubProjectId=1");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var actual = response.GetCollection<GetATSSystemsResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Count.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(6)]
        public async Task GetATSCatalogs_should_succeed()
        {
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync("api/Plan/GetATSCatalogs?ProjectId=1&SubProjectId=1");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var actual = response.GetCollection<GetATSCatalogsResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Count.Should().BeGreaterOrEqualTo(expected);
        }
         
        [Fact, TestPriority(6)]
        public async Task GetATSReleaseSummary_should_succeed()
        {
            // Arrange
            ulong releaseIterationId = 1717441;
            int expected = 0;

            // Act
            var response = await GetAsync($"api/Plan/GetATSReleaseSummary?releaseIterationId={releaseIterationId}&ProjectId={1}&SubProjectId={1}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetATSReleaseSummaryResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual?.TotalTestCaseExecutions.Should().BeGreaterOrEqualTo(expected);
        }


        [Fact, TestPriority(7)]
        public async Task GetATSSubprojects_should_fail()
        {
            // Arrange
            string expected = "ProjectId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSSubprojects?NoderefField=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSSubprojects_empty_result_should_fail()
        {
            // Arrange
            string expected = "No sub project found in ATS, in project.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSSubprojects?NoderefField=9991");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSProducts_should_fail()
        {
            // Arrange
            string expected = "ProjectId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSProducts?ProjectId=0&SubProjectId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSProducts_empty_subProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSProducts?ProjectId=1&SubProjectId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }


        [Fact, TestPriority(7)]
        public async Task GetATSProducts_empty_result_should_fail()
        {
            // Arrange
            string expected = "The given subproject does not have any Product.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSProducts?ProjectId={ulong.MaxValue}&SubProjectId={ulong.MaxValue}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSFeatures_should_fail()
        {
            // Arrange
            string expected = "ProjectId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSFeatures?ProjectId=0&SubProjectId=0&ProductId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSFeatures_empty_subProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSFeatures?ProjectId=1&SubProjectId=0&ProductId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSFeatures_empty_productId_should_fail()
        {
            // Arrange
            string expected = "ProductId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSFeatures?ProjectId=1&SubProjectId=1&ProductId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSFeatures_empty_result_should_fail()
        {
            // Arrange
            string expected = "The given product does not have any Features.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSFeatures?ProjectId={ulong.MaxValue}&SubProjectId={ulong.MaxValue}&ProductId={ulong.MaxValue}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSUsecases_should_fail()
        {
            // Arrange
            string expected = "ProjectId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSUsecases?ProjectId=0&SubProjectId=0&FeatureId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSUsecases_empty_subProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSUsecases?ProjectId=1&SubProjectId=0&FeatureId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSUsecases_empty_featureId_should_fail()
        {
            // Arrange
            string expected = "FeatureId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSUsecases?ProjectId=1&SubProjectId=1&FeatureId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSUsecases_empty_result_should_fail()
        {
            // Arrange
            string expected = "The given feature does not have any use case.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSUsecases?ProjectId={ulong.MaxValue}&SubProjectId={ulong.MaxValue}&FeatureId={ulong.MaxValue}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSRequirements_should_fail()
        {
            // Arrange
            string expected = "ProjectId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSRequirements?ProjectId=0&SubProjectId=0&UseCaseId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSRequirements_emtpy_subprojectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSRequirements?ProjectId=1&SubProjectId=0&UseCaseId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSRequirements_emtpy_useCaseId_should_fail()
        {
            // Arrange
            string expected = "UseCaseId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSRequirements?ProjectId=1&SubProjectId=1&UseCaseId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetATSRequirements_emtpy_result_should_fail()
        {
            // Arrange
            string expected = "The given usecase does not have any requirements.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSRequirements?ProjectId={ulong.MaxValue}&SubProjectId={ulong.MaxValue}&UseCaseId={ulong.MaxValue}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }


        [Fact, TestPriority(7)]
        public async Task GetATSReleaseSummary_should_not_succeed()
        {
            // Arrange
            string expected = "ReleaseIterationId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Plan/GetATSReleaseSummary?releaseIterationId=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

    }
}