﻿using FluentAssertions;
using Queries.DesignModule;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Application.Services;
using Messaging.Queries;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class DesignControllerSearchTypicalsAndAttributesTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _typicalId;
        public DesignControllerSearchTypicalsAndAttributesTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }


        [Fact, TestPriority(0)]
        public async Task GetTypicals_should_succeed()
        {
            // Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Design/GetTypicals?CatalogId={Variables.CatalogId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetTypicalsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(0)]
        public async Task SearchTypicals_should_succeed()
        {
            // Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Design/SearchTypicals?searchString=customer&CatalogId={Variables.CatalogId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetTypicalsResult>();
            _typicalId = actual.FirstOrDefault().Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Count.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(1)]
        public async Task SearchAttributes_should_succeed()
        {
            // Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Design/SearchAttributes?SearchString=C&TypicalId={Variables.TypicalId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetTypicalsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(2)]
        public async Task GetTypicals_should_fail()
        {
            // Arrange
            string expected = "CatalogId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Design/GetTypicals?CatalogId={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(2)]
        public async Task GetTypicals_empty_result_should_fail()
        {
            // Arrange
            string expected = "No typicals found in catalog.";

            //Act
            var response = await GetAsync($"api/Design/GetTypicals?CatalogId={ Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(2)]
        public async Task SearchTypicals_should_fail()
        {
            // Arrange
            string expected = "CatalogId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Design/SearchTypicals?searchString=b&CatalogId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(2)]
        public async Task SearchTypicals_empty_result_should_fail()
        {
            // Arrange
            string expected = "No typicals found in catalog.";

            //Act
            var response = await GetAsync($"api/Design/SearchTypicals?searchString=b&CatalogId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(2)]
        public async Task SearchAttributes_shouldFail()
        {
            // Arrange
            string expected = "TypicalId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Design/SearchAttributes?searchString=test&TypicalId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(2)]
        public async Task SearchAttributes_empty_result_shouldFail()
        {
            // Arrange
            string expected = "No attributes found in typical.";

            //Act
            var response = await GetAsync($"api/Design/SearchAttributes?searchString=test&TypicalId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}