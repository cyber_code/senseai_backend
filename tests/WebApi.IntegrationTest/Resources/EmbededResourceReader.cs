﻿using System.IO;
using System.Reflection;

namespace WebApi.IntegrationTest.Resources
{
    internal sealed class EmbededResourceReader
    {
        public static TextReader GetTextReader(string resourceName)
        {
            return new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName));
        }

        public static Stream GetStream(string resourceName)
        {
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
        }
    }
}