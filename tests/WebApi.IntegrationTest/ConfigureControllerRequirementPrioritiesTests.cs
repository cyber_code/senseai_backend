﻿using System;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using WebApi.IntegrationTest.Infrastructure;
using Application.Services;
using Messaging.Commands;
using Messaging.Queries;
using Commands.ProcessModule.RequirementPriorities;
using Queries.ProcessModule.RequirementPriorities;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class ConfigureControllerRequirementPrioritiesTests : ControllerTestBase
    {
        private static readonly Random random = new Random();
        private static Guid _requirementPriorityId;
        private readonly TestServerFixture _fixture;

        public ConfigureControllerRequirementPrioritiesTests(TestServerFixture fixture) : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddRequirementPriority_should_success()
        {
            // Arrange
            AddRequirementPriority RequirementPriority = new AddRequirementPriority()
            {
                SubProjectId = Variables.SubProjectId,
                Title = GenerateRandomString(20),
                Code = GenerateRandomString(20),
            };

            // Act
            var response = await PostAsync("api/Process/Configure/AddRequirementPriority", RequirementPriority);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddRequirementPriorityResult>();
            _requirementPriorityId = actual.Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateRequirementPriority_should_success()
        {
            // Arrange
            bool expected = true;
            UpdateRequirementPriority updateRequirementPriority = new UpdateRequirementPriority()
            {
                Id = _requirementPriorityId,
                SubProjectId = Variables.SubProjectId,
                Title = GenerateRandomString(20),
                Code = GenerateRandomString(20),
            };

            // Act
            var response = await PutAsync("api/Process/Configure/UpdateRequirementPriority", updateRequirementPriority);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetRequirementPriority_should_success()
        {
            // Arrange

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementPriority?Id={_requirementPriorityId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetRequirementPriorityResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetRequirementPriorities_should_success()
        {
            // Arrange
            int expected = 4;

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementPriorities?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetRequirementPrioritiesResult>().Result.Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(2)]
        public async Task UpdateIndexesRequirementPriority_should_success()
        {
            // Arrange
            bool expected = true;
            var response = await GetAsync($"api/Process/Configure/GetRequirementPriorities?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetRequirementPrioritiesResult>().Result.ToArray();
            UpdateIndexesRequirementPriority newPriorities = new UpdateIndexesRequirementPriority();
            newPriorities.RequirementPriorities = new RequirementPriorities[actual.Length];
            for(int i=1;i<actual.Length;i++)
            {
                newPriorities.RequirementPriorities[i - 1] = new RequirementPriorities {
                    Id = actual[i].Id,
                    Code = actual[i].Code,
                    Title = actual[i].Title,
                     SubProjectId=actual[i].SubProjectId
             };
            }
            newPriorities.RequirementPriorities[actual.Length - 1]= new RequirementPriorities
            {
                Id = actual[0].Id,
                Code = actual[0].Code,
                Title = actual[0].Title,
                SubProjectId = actual[0].SubProjectId
            };
           
            // Act
            var responseupd = await PutAsync("api/Process/Configure/UpdateIndexesRequirementPriority", newPriorities);
            responseupd.EnsureSuccessStatusCode();
            var actualupd = responseupd.GetObject<bool>();

            // Assert
            responseupd.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actualupd);

             response = await GetAsync($"api/Process/Configure/GetRequirementPriorities?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
             actual = response.GetCollectionAsync<GetRequirementPrioritiesResult>().Result.ToArray();
            Assert.Equal(actual[actual.Length - 2].Id, _requirementPriorityId);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteRequirementPriority_should_success()
        {
            // Arrange
            bool expected = true;

            // Act
            var response = await DeleteAsync($"api/Process/Configure/DeleteRequirementPriority?Id={_requirementPriorityId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Arrange
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetRequirementPriorities_after_delete_should_success()
        {
            // Arrange
            int expected = 3;

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementPriorities?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetRequirementPrioritiesResult>().Result.Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(9)]
        public async Task AddRequirementPriority_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            AddRequirementPriority RequirementPriority = new AddRequirementPriority()
            {
                SubProjectId = Guid.Empty,
                Title = GenerateRandomString(20),
                Code = GenerateRandomString(20)
            };

            // Act
            var response = await PostAsync("api/Process/Configure/AddRequirementPriority", RequirementPriority);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task AddRequirementPriority_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";

            AddRequirementPriority RequirementPriority = new AddRequirementPriority()
            {
                SubProjectId = Variables.SubProjectId,
                Code = GenerateRandomString(20)
            };


            // Act
            var response = await PostAsync("api/Process/Configure/AddRequirementPriority", RequirementPriority);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateRequirementPriority_should_fail()
        {
            // Arrange
            string expected = "RequirementPriorityId cannot be empty!";

            // Act
            UpdateRequirementPriority updateRequirementPriority = new UpdateRequirementPriority()
            {
                SubProjectId = Variables.SubProjectId,
                Title = GenerateRandomString(20),
                Code = GenerateRandomString(20)
            };

            var response = await PutAsync("api/Process/Configure/UpdateRequirementPriority", updateRequirementPriority);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateRequirementPriority_empty_SubProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            // Act
            UpdateRequirementPriority updateRequirementPriority = new UpdateRequirementPriority()
            {
                Id = Guid.NewGuid(),
                Title = GenerateRandomString(20),
                Code = GenerateRandomString(20)
            };

            var response = await PutAsync("api/Process/Configure/UpdateRequirementPriority", updateRequirementPriority);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateRequirementPriority_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";

            // Act
            UpdateRequirementPriority updateRequirementPriority = new UpdateRequirementPriority()
            {
                Id = Guid.NewGuid(),
                SubProjectId = Variables.SubProjectId,
                Code = GenerateRandomString(20)
            };

            var response = await PutAsync("api/Process/Configure/UpdateRequirementPriority", updateRequirementPriority);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task DeleteRequirementPriority_should_fail()
        {
            // Arrange
            string expected = "RequirementPriorityId cannot be empty!";

            // Act
            var response = await DeleteAsync($"api/Process/Configure/DeleteRequirementPriority?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetRequirementPriority_should_fail()
        {
            // Arrange
            string expected = "RequirementPriorityId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementPriority?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetRequirementPriority_empty_result_should_fail()
        {
            // Arrange
            string expected = "No Requirement Priority found!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementPriority?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetRequirementPriorities_empty_SubProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementPriorities?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetRequirementPriorities_empty_result_should_fail()
        {
            // Arrange
            string expected = "No Requirement Priorities found in project!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementPriorities?&SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        private static string GenerateRandomString(int strLength)
        {
            const string alphanumericChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            return new string(Enumerable.Repeat(alphanumericChar, strLength)
              .Select(str => str[random.Next(str.Length)])
              .ToArray());
        }
    }
}