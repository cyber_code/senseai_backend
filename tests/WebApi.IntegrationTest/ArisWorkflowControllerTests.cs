﻿using Commands.ProcessModule.ArisWorkflows;
using FluentAssertions;
using Messaging.Queries;
using Queries.ProcessModule.ArisWorkflows;
using Queries.ProcessModule.Processes;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using WebApi.IntegrationTest.Resources;
using Xunit;
using Application.Services;
using Messaging.Commands;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class ArisWorkflowControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _arisWorkflowId;

        public ArisWorkflowControllerTests(TestServerFixture testServerFixture) : base(testServerFixture)
        {
            _fixture = testServerFixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddArisWorkflow_should_success()
        {
            //Arrange
            var arisWorkflow = new AddArisWorkflow
            {
                ProcessId = Variables.ProcessId,
                Title = "title 1",
                Description = "Description 1",
            };

            //Act
            var response = await PostAsync("api/Process/ArisWorkflow/AddArisWorkflow", arisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddArisWorkflowResult>();
            _arisWorkflowId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task SaveArisWorkflow_should_success()
        {
            //Arrange
            string jsonContent = "";
            using (var jsonReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.ArisWorkflow.UIDesignerJson.json"))
            {
                jsonContent = jsonReader.ReadToEnd();
            }

            var saveArisWorkflow = new SaveArisWorkflow
            {
                Id = _arisWorkflowId,
                DesignerJson = jsonContent,
            };

            //Act
            var response = await PutAsync("api/Process/ArisWorkflow/SaveArisWorkflow", saveArisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<SaveArisWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
        }

        [Fact, TestPriority(2)]
        public async Task RollbackArisWorkflowVersion_with_noversion_should_fail()
        {
            //Arrange
            string expected = "The aris workflow does not have any version.";
            var rollbackArisWorkflowVersion = new RollbackArisWorkflowVersion
            {
                ArisWorkflowId = _arisWorkflowId,
                Version = 0,
            };

            //Act
            var response = await PutAsync("api/Process/ArisWorkflow/RollbackArisWorkflow", rollbackArisWorkflowVersion);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(2)]
        public async Task RenameArisWorkflow_should_success()
        {
            //Arrange
            var renameArisWorkflow = new RenameArisWorkflow
            {
                Id = _arisWorkflowId,
                Title = " Tile is updated."
            };

            //Act
            var response = await PutAsync("api/Process/ArisWorkflow/RenameArisWorkflow", renameArisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetArisWorkflowsResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetArisWorkflows_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/ArisWorkflow/GetArisWorkflows?ProcessId={Variables.ProcessId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetArisWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetArisWorkflow_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync("api/Process/ArisWorkflow/GetArisWorkflow?Id=" + _arisWorkflowId);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetArisWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(3)]
        public async Task IssueArisWorkflow_versions_0_should_success()
        {
            //Arrange
            var issueArisWorkflow = new IssueArisWorkflow
            {
                ArisWorkflowId = _arisWorkflowId,
                NewVersion = true,
            };

            //Act
            var response = await PostAsync("api/Process/ArisWorkflow/IssueArisWorkflow", issueArisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<IssueArisWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(_arisWorkflowId, actual.ArisWorkflowId);
        }

        [Fact, TestPriority(4)]
        public async Task RollbackArisWorkflowVersion_with_noitems_should_success()
        {
            //Arrange
            var rollbackArisWorkflowVersion = new RollbackArisWorkflowVersion
            {
                ArisWorkflowId = _arisWorkflowId,
                Version = 0,
            };

            //Act
            var response = await PutAsync("api/Process/ArisWorkflow/RollbackArisWorkflow", rollbackArisWorkflowVersion);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<RollbackArisWorkflowVersionResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
        }

        [Fact, TestPriority(5)]
        public async Task IssueArisWorkflow_version1_should_success()
        {
            //Arrange
            var issueArisWorkflow = new IssueArisWorkflow
            {
                ArisWorkflowId = _arisWorkflowId,
                NewVersion = true,
            };

            //Act
            var response = await PostAsync("api/Process/ArisWorkflow/IssueArisWorkflow", issueArisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<IssueArisWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(_arisWorkflowId, actual.ArisWorkflowId);
        }

        [Fact, TestPriority(6)]
        public async Task RollbackArisWorkflowVersion_should_success()
        {
            //Arrange
            var rollbackArisWorkflowVersion = new RollbackArisWorkflowVersion
            {
                ArisWorkflowId = _arisWorkflowId,
                Version = 0,
            };

            //Act
            var response = await PutAsync("api/Process/ArisWorkflow/RollbackArisWorkflow", rollbackArisWorkflowVersion);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<RollbackArisWorkflowVersionResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
        }

        [Fact, TestPriority(7)]
        public async Task GetArisWorkflowVersions_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/ArisWorkflow/GetArisWorkflowVersions?ArisWorkflowId={_arisWorkflowId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetArisWorkflowVersionsResult>().Result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(7)]
        public async Task GetArisWorkflowVersionByDate_should_succeed()
        {
            // Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/ArisWorkflow/GetArisWorkflowVersionsByDate?ArisWorkflowId={_arisWorkflowId}&DateFrom={DateTime.Now.AddYears(-1)}&DateTo={DateTime.Now.AddDays(1)}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetArisWorkflowVersionsByDateResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(7)]
        public async Task ExportArisWorkflow2WorkflowDesinger_should_success()
        {
            //Arrange            
            bool expected = true;
            var exAris = new ExportArisWorkflow2WorkflowDesinger
            {
                SubprojectId = Variables.SubProjectId,
                SystemId = Variables.SystemId,
                CatalogId = Variables.CatalogId,
                ArisWorkflowId = _arisWorkflowId
            };

            //Act
            var response = await PostAsync($"api/Process/ArisWorkflow/ExportArisWorkflow2WorkflowDesinger?SubprojectId={exAris.SubprojectId}" +
                $"&SystemId={exAris.SystemId}&CatalogId={exAris.CatalogId}&ArisWorkflowId={_arisWorkflowId}", exAris);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(8)]
        public async Task DeleteArisWorkflow_should_success()
        {
            //Arrange
            bool expected = true;
            var arisWorkflow = new DeleteArisWorkflow
            {
                Id = _arisWorkflowId
            };

            //Act
            var response = await DeleteAsync("api/Process/ArisWorkflow/DeleteArisWorkflow", arisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(9)]
        public async Task ImportArisWorkflow2ArisDesigner_should_success()
        {
            int expected = 8;
            string xmlContent = "";
            var xmlreader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.ArisWorkflow.aris.xml");
            {
                xmlContent = xmlreader.ReadToEnd();
            }

            var surveyBytes = Encoding.UTF8.GetBytes(xmlContent);
            var byteArrayContent = new ByteArrayContent(surveyBytes);
            byteArrayContent.Headers.ContentType = MediaTypeHeaderValue.Parse("text/xml");
            var form = new MultipartFormDataContent
            {
                {byteArrayContent, "\"file\"", "\"aris.xml\""}
            };

            //Act
            var response = await PostFileAsync($"api/Process/ArisWorkflow/ImportArisWorkflow2ArisDesigner?processId={Variables.ProcessId}", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<ImportArisWorkflow2ArisDesignerResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(10)]
        public async Task ImportArisWorkflows_should_success()
        {
            //Arrange           
            int expected = 8;
            string xmlContent = "";
            var xmlreader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.ArisWorkflow.aris.xml");
            {
                xmlContent = xmlreader.ReadToEnd();
            }

            var surveyBytes = Encoding.UTF8.GetBytes(xmlContent);
            var byteArrayContent = new ByteArrayContent(surveyBytes);
            byteArrayContent.Headers.ContentType = MediaTypeHeaderValue.Parse("text/xml");
            var form = new MultipartFormDataContent
            {
                {byteArrayContent, "\"file\"", "\"aris.xml\""}
            };

            //Act
            var response = await PostFileAsync($"api/Process/ArisWorkflow/ImportArisWorkflows?subProjectId={Guid.NewGuid()}&hierarchyId={Guid.NewGuid()}", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<ImportArisWorkflowsResult>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
            Assert.Equal(expected, actual.Count);
        }

        [Fact, TestPriority(12)]
        public async Task AddArisWorkflow_should_failed()
        {
            //Arrange           
            string expected = "ProcessId cannot be empty.";
            var arisWorkflow = new AddArisWorkflow
            {
                ProcessId = Guid.Empty,
                Title = "title 1",
                Description = "Description 1",
            };

            //Act
            var response = await PostAsync("api/Process/ArisWorkflow/AddArisWorkflow", arisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task AddArisWorkflow_empty_title_should_failed()
        {
            //Arrange           
            string expected = "Title cannot be empty.";
            var arisWorkflow = new AddArisWorkflow
            {
                ProcessId = Guid.NewGuid(),
                Description = "Description 1",
            };

            //Act
            var response = await PostAsync("api/Process/ArisWorkflow/AddArisWorkflow", arisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task SaveArisWorkflow_should_failed()
        {
            //Arrange       
            string expected = "Id cannot be empty.";
            var arisWorkflow = new SaveArisWorkflow
            {
                Id = Guid.Empty,
                DesignerJson = null,
            };

            //Act
            var response = await PutAsync("api/Process/ArisWorkflow/SaveArisWorkflow", arisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task SaveArisWorkflow_empty_designerJson_should_failed()
        {
            //Arrange       
            string expected = "DesignerJson cannot be empty.";
            var arisWorkflow = new SaveArisWorkflow
            {
                Id = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Process/ArisWorkflow/SaveArisWorkflow", arisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task RenameArisWorkflow_should_failed()
        {
            //Arrange           
            string expected = "Id cannot be empty.";
            var arisWorkflow = new RenameArisWorkflow
            {
                Id = Guid.Empty,
                Title = "update tile",
            };

            //Act
            var response = await PutAsync("api/Process/ArisWorkflow/RenameArisWorkflow", arisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task RenameArisWorkflow_empty_title_should_failed()
        {
            //Arrange           
            string expected = "Title cannot be empty.";
            var arisWorkflow = new RenameArisWorkflow
            {
                Id = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Process/ArisWorkflow/RenameArisWorkflow", arisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task GetArisWorkflow_should_failed()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/ArisWorkflow/GetArisWorkflow?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task GetArisWorkflowVersions_should_failed()
        {
            //Arrange
            string expected = "ArisWorkflowId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/ArisWorkflow/GetArisWorkflowVersions?ProcessId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(12)]
        public async Task GetArisWorkflowVersionByDate_should_fail()
        {
            //Arrange
            string expected = "ArisWorkflowId cannot be empty.";


            //Act
            var response = await GetAsync($"api/Process/ArisWorkflow/GetArisWorkflowVersionsByDate?ArisWorkflowId={Guid.Empty}&DateFrom={DateTime.Now.AddYears(-1)}&DateTo={DateTime.Now.AddDays(1)}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(12)]
        public async Task GetArisWorkflows_should_failed()
        {
            //Arrange
            string expected = "ProcessId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/ArisWorkflow/GetArisWorkflows?ProcessId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task GetArisWorkflows_empty_result_should_failed()
        {
            //Arrange
            string expected = "Aris workflow not found in the process.";

            //Act
            var response = await GetAsync($"api/Process/ArisWorkflow/GetArisWorkflows?ProcessId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task DeleteArisWorkflow_should_failed()
        {
            //Arrange     
            string expected = "Id cannot be empty.";
            var arisWorkflow = new DeleteArisWorkflow
            {
                Id = Guid.Empty
            };

            //Act
            var response = await DeleteAsync("api/Process/ArisWorkflow/DeleteArisWorkflow", arisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ImportArisWorkflow2ArisDesigner_should_fail()
        {
            //Arrange
            string expected = "ProcessId cannot be empty.";
            var form = new MultipartFormDataContent
            {
                { new StringContent(""),"file","file.xml" }
            };

            //Act
            var response = await PostFileAsync($"api/Process/ArisWorkflow/ImportArisWorkflow2ArisDesigner?ProcessId={Guid.Empty}", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ImportArisWorkflow2ArisDesigner_empty_filecontent_should_fail()
        {
            //Arrange
            string expected = "FileContent cannot be empty.";
            var form = new MultipartFormDataContent
            {
                { new StringContent(""),"file","file.xml" }
            };

            //Act
            var response = await PostFileAsync($"api/Process/ArisWorkflow/ImportArisWorkflow2ArisDesigner?ProcessId={Guid.NewGuid()}", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ImportArisWorkflow2ArisDesigner_empty_file_extension_should_fail()
        {
            //Arrange
            string expected = "File extension not supported!";
            var form = new MultipartFormDataContent
            {
                { new StringContent(""),"file","file.json" }
            };

            //Act
            var response = await PostFileAsync($"api/Process/ArisWorkflow/ImportArisWorkflow2ArisDesigner?ProcessId={Guid.NewGuid()}", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ImportArisWorkflows_should_fail()
        {
            //Arrange
            string expected = "filecontent cannot be empty.";
            var form = new MultipartFormDataContent
            {
                { new StringContent(""),"file","file.xml" }
            };

            //Act
            var response = await PostFileAsync($"api/Process/ArisWorkflow/ImportArisWorkflows?subProjectId={Guid.NewGuid()}&hierarchyId={Guid.NewGuid()}", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ImportArisWorkflows_empty_subprojectId_should_fail()
        {
            // Arrange
            string expected = "SubPojectId cannot be empty.";
            var form = new MultipartFormDataContent
            {
                { new StringContent("test test"),"file","file" }
            };

            //Act
            var response = await PostFileAsync($"api/Process/ArisWorkflow/ImportArisWorkflows?SubProjectId={Guid.Empty}&hierarchyId={Guid.NewGuid()}", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ImportArisWorkflows_empty_hierarchyId_should_fail()
        {
            // Arrange
            string expected = "hierarchyId cannot be empty.";
            var form = new MultipartFormDataContent
            {
                { new StringContent("test test"),"file","file" }
            };

            //Act
            var response = await PostFileAsync($"api/Process/ArisWorkflow/ImportArisWorkflows?SubProjectId={Guid.NewGuid()}&HierarchyId={Guid.Empty}", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ImportArisWorkflows_file_bad_xml_should_fail()
        {
            //Arrange
            string expected = "xml is not valid.";
            var form = new MultipartFormDataContent
            {
                { new StringContent("test test"),"file","file.xml" }
};

            //Act
            var response = await PostFileAsync($"api/Process/ArisWorkflow/ImportArisWorkflow2ArisDesigner?processId={Guid.NewGuid()}", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ExportArisWorkflow2WorkflowDesinger_should_fail()
        {
            // Arrange
            string expected = "subProjectId cannot be empty.";
            var exAris = new ExportArisWorkflow2WorkflowDesinger
            {
                SubprojectId = Guid.Empty,
                SystemId = Variables.SystemId,
                CatalogId = Variables.CatalogId,
                ArisWorkflowId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync($"api/Process/ArisWorkflow/ExportArisWorkflow2WorkflowDesinger?SubprojectId={exAris.SubprojectId}&" +
                $"SystemId={exAris.SystemId}&CatalogId={exAris.CatalogId}&ArisWorkflowId={exAris.ArisWorkflowId}", exAris);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ExportArisWorkflow2WorkflowDesinger_empty_systemId_should_fail()
        {
            // Arrange
            string expected = "SystemId cannot be empty.";
            var exAris = new ExportArisWorkflow2WorkflowDesinger
            {
                SubprojectId = Variables.SubProjectId,
                SystemId = Guid.Empty,
                CatalogId = Variables.CatalogId,
                ArisWorkflowId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync($"api/Process/ArisWorkflow/ExportArisWorkflow2WorkflowDesinger?SubprojectId={exAris.SubprojectId}&" +
                $"SystemId={exAris.SystemId}&CatalogId={exAris.CatalogId}&ArisWorkflowId={exAris.ArisWorkflowId}", exAris);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ExportArisWorkflow2WorkflowDesinger_empty_catalogId_should_fail()
        {
            // Arrange
            string expected = "CatalogId cannot be empty.";
            var exAris = new ExportArisWorkflow2WorkflowDesinger
            {
                SubprojectId = Variables.SubProjectId,
                SystemId = Variables.SystemId,
                ArisWorkflowId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync($"api/Process/ArisWorkflow/ExportArisWorkflow2WorkflowDesinger?SubprojectId={exAris.SubprojectId}&" +
                $"SystemId={exAris.SystemId}&CatalogId={exAris.CatalogId}&ArisWorkflowId={exAris.ArisWorkflowId}", exAris);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task ExportArisWorkflow2WorkflowDesinger_empty_arisworkflowId_should_fail()
        {
            // Arrange
            string expected = "ArisWorkflowId cannot be empty.";
            var exAris = new ExportArisWorkflow2WorkflowDesinger
            {
                SubprojectId = Variables.SubProjectId,
                SystemId = Variables.SystemId,
                CatalogId = Variables.CatalogId
            };

            //Act
            var response = await PostAsync($"api/Process/ArisWorkflow/ExportArisWorkflow2WorkflowDesinger?SubprojectId={exAris.SubprojectId}&" +
                $"SystemId={exAris.SystemId}&CatalogId={exAris.CatalogId}&ArisWorkflowId={exAris.ArisWorkflowId}", exAris);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task IssueArisWorkflow_should_failed()
        {
            // Arrange
            string expected = "ArisWorkflowId cannot be empty.";
            var issueArisWorkflow = new IssueArisWorkflow
            {
                ArisWorkflowId = Guid.Empty,
                NewVersion = true,
            };

            //Act
            var response = await PostAsync("api/Process/ArisWorkflow/IssueArisWorkflow", issueArisWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task RollbackArisWorkflowVersion_parameters_should_failed()
        {
            //Arrange   
            string expected = "ArisWorkflowId cannot be empty.";
            var rollbackArisWorkflowVersion = new RollbackArisWorkflowVersion
            {
                ArisWorkflowId = Guid.Empty,
                Version = 0,
            };

            //Act
            var response = await PutAsync("api/Process/ArisWorkflow/RollbackArisWorkflow", rollbackArisWorkflowVersion);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
