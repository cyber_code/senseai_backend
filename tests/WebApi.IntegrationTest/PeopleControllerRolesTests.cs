﻿using Application.Services;
using Commands.PeopleModule;
using Commands.ProcessModule.Sprints;
using FluentAssertions;
using Messaging.Commands;
using Queries.PeopleModule;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class PeopleControllerRolesTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _roleId;

        public PeopleControllerRolesTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddRole_should_succeed()
        {
            // Arrange
            AddRole addRole = new AddRole
            {
                Title = "Role Title",
                Description = "Role description",
                SubProjectId = Variables.SubProjectId
            };

            //Act
            var response = await PostAsync("api/People/AddRole", addRole);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddRoleResult>();
            _roleId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }
        
        [Fact, TestPriority(1)]
        public async Task UpdateRole_should_succeed()
        {
            // Arrange
            bool expected = true;
            UpdateRole updateRole = new UpdateRole
            {
                Id = _roleId,
                Title = "Role Title",
                Description = "Role description",
                SubProjectId = Variables.SubProjectId
            };

            //Act
            var response = await PutAsync($"api/People/UpdateRole", updateRole);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetRole_should_succeed()
        {
            // Arrange

            //Act
            var response = await GetAsync($"api/People/GetRole?Id={_roleId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetRoleResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetRoles_should_succeed()
        {
            // Arrange
            int expected = 3;

            //Act
            var response = await GetAsync($"api/People/GetRoles?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetRolesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteRole_should_succeed()
        {
            // Arrange
            bool expected = true;
            DeleteRole deleteRole = new DeleteRole
            {
                Id = _roleId
            };

            //Act
            var response = await DeleteAsync("api/People/DeleteRole", deleteRole);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetRoles_after_delete_should_succeed()
        {
            // Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/People/GetRoles?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetRolesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task AddRole_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            var role = new AddRole
            {
                Title = null,
                Description = null,
                SubProjectId = Variables.SubProjectId
            };

            //Act
            var response = await PostAsync("api/People/AddRole", role).ConfigureAwait(false);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact]
        public async Task UpdateRole_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            var role = new UpdateRole
            {
                Title = null,
            };

            //Act
            var response = await PutAsync($"api/People/UpdateRole", role);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact]
        public async Task UpdateRole_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            var role = new UpdateRole
            {
                Id = Guid.NewGuid(),
                Title = null,
                SubProjectId = Variables.SubProjectId
            };

            //Act
            var response = await PutAsync($"api/People/UpdateRole", role);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact]
        public async Task DeleteRole_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            DeleteRole deleteRole = new DeleteRole
            {
                Id = Guid.Empty
            };
           
            //Act
            var response = await DeleteAsync("api/People/DeleteRole", deleteRole);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact]
        public async Task GetRole_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await GetAsync($"api/People/GetRole?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact]
        public async Task GetRole_empty_result_should_fail()
        {
            //Arrange
            string expected = "No role found.";

            //Act
            var response = await GetAsync($"api/People/GetRole?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
