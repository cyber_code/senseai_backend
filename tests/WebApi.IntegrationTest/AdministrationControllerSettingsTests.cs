﻿using FluentAssertions;
using Commands.AdministrationModule.Settings;
using Queries.AdministrationModule.Settings;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Messaging.Queries;
using Messaging.Commands;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class AdministrationControllerSettingsTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _settingId;

        public AdministrationControllerSettingsTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddSettings_should_success()
        {
            //Arrange
            var setting = new AddSettings
            {
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                SystemId = Guid.NewGuid(),
                CatalogId = Guid.NewGuid(),
            };

            //Act
            var response = await PostAsync("api/Administration/AddSettings", setting);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddSettingsResult>();
            _settingId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateSettings_should_success()
        {
            //Arrange
            bool expected = true;
            UpdateSettings updateSettings = new UpdateSettings
            {
                Id = _settingId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSettings", updateSettings);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().Be(expected);
        }

        [Fact, TestPriority(2)]
        public async Task GetSetting_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Administration/GetSetting?ProjectId={Variables.ProjectId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetSettingResult>();

            //Assert
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(3)]
        public async Task AddSettings_should_fail()
        {
            //Arrange
            string expected = "ProjectId cannot be empty.";
            var setting = new AddSettings
            {
                ProjectId = Guid.Empty,
                SubProjectId = Guid.Empty,
                SystemId = Guid.Empty,
                CatalogId = Guid.Empty,
            };

            //Act
            var response = await PostAsync("api/Administration/AddSettings", setting);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task AddSettings_empty_subProjectId_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";
            var setting = new AddSettings
            {
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.Empty,
                SystemId = Guid.Empty,
                CatalogId = Guid.Empty,
            };

            //Act
            var response = await PostAsync("api/Administration/AddSettings", setting);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task AddSettings_empty_systemId_should_fail()
        {
            //Arrange
            string expected = "SystemId cannot be empty.";
            var setting = new AddSettings
            {
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                SystemId = Guid.Empty,
                CatalogId = Guid.Empty,
            };

            //Act
            var response = await PostAsync("api/Administration/AddSettings", setting);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task AddSettings_empty_catalogId_should_fail()
        {
            //Arrange
            string expected = "CatalogId cannot be empty.";
            var setting = new AddSettings
            {
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                SystemId = Guid.NewGuid(),
                CatalogId = Guid.Empty,
            };

            //Act
            var response = await PostAsync("api/Administration/AddSettings", setting);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task UpdateSettings_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            var updateSetting = new UpdateSettings
            {
                Id = Guid.Empty,
                ProjectId = Guid.Empty,
                SubProjectId = Guid.Empty,
                SystemId = Guid.Empty,
                CatalogId = Guid.Empty,
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSettings", updateSetting);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task UpdateSettings_empty_projectId_should_fail()
        {
            //Arrange
            string expected = "ProjectId cannot be empty.";
            var updateSetting = new UpdateSettings
            {
                Id = Guid.NewGuid(),
                ProjectId = Guid.Empty,
                SubProjectId = Guid.Empty,
                SystemId = Guid.Empty,
                CatalogId = Guid.Empty,
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSettings", updateSetting);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task UpdateSettings_empty_subProjectId_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";
            var updateSetting = new UpdateSettings
            {
                Id = Guid.NewGuid(),
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.Empty,
                SystemId = Guid.Empty,
                CatalogId = Guid.Empty,
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSettings", updateSetting);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task UpdateSettings_empty_catalogId_should_fail()
        {
            //Arrange
            string expected = "CatalogId cannot be empty.";
            var updateSetting = new UpdateSettings
            {
                Id = Guid.NewGuid(),
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                SystemId = Guid.NewGuid(),
                CatalogId = Guid.Empty,
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSettings", updateSetting);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task UpdateSettings_empty_systemId_should_fail()
        {
            //Arrange
            string expected = "SystemId cannot be empty.";
            var updateSetting = new UpdateSettings
            {
                Id = Guid.NewGuid(),
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                SystemId = Guid.Empty,
                CatalogId = Guid.Empty,
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSettings", updateSetting);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task GetSetting_should_fail()
        {
            //Arrange
            string expected = "ProjectId cannot be empty.";

            //Act
            var response = await GetAsync("api/Administration/GetSetting?ProjectId=" + Guid.Empty + "&SubProjectId=" + Guid.Empty);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task GetSetting_should_empty_subprojectId_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await GetAsync("api/Administration/GetSetting?ProjectId=" + Guid.NewGuid() + "&SubProjectId=" + Guid.Empty);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task GetSetting_should_empty_result_fail()
        {
            //Arrange
            string expected = "No setting found for project and subproject!";

            //Act
            var response = await GetAsync("api/Administration/GetSetting?ProjectId=" + Guid.NewGuid() + "&SubProjectId=" + Guid.NewGuid());
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}