﻿using FluentAssertions;
using FluentAssertions.Json;
using Commands.DesignModule.WorkflowModel;
using Queries.DesignModule;
using WebApi.IntegrationTest.Infrastructure;
using WebApi.IntegrationTest.Resources;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Application.Services;
using Messaging.Commands;
using Messaging.Queries;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class DesignControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _workflowId;

        public DesignControllerTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddWorkflow_should_succeed()
        {
            // Arrange
            AddWorkflow addWorkflow = new AddWorkflow
            {
                Title = "SenseAI Workflow",
                Description = "SenseAI Workflow notes",
                FolderId = Variables.FolderId
            };

            //Act
            var response = await PostAsync("api/Design/AddWorkflow", addWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddWorkflowResult>();
            _workflowId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task SaveWorkflow_should_succeed()
        {
            // Arrange
            string workflowPathsJson = "";
            using (var expectedReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.Design.WorkflowPath.json"))
            {
                workflowPathsJson = expectedReader.ReadToEnd();
            }
            string designerJson = "";
            using (var expectedReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.Design.UIDesignerJson.json"))
            {
                designerJson = expectedReader.ReadToEnd();
            }

            SaveWorkflow saveWorkflow = new SaveWorkflow
            {
                Id = _workflowId,
                Json = designerJson,
                WorkflowPathsJson = workflowPathsJson
            };

            //Act
            var response = await PutAsync("api/Design/SaveWorkflow", saveWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<SaveWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task RenameWorkflow_should_succeed()
        {
            // Arrange
            bool expected = true;
            RenameWorkflow renameWorkflow = new RenameWorkflow
            {
                WorkflowId = _workflowId,
                Title = "SenseAI Workflow - renamed"
            };

            //Act
            var response = await PutAsync("api/Design/RenameWorkflow", renameWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task IssueWorkflow_should_succeed()
        {
            // Arrange
            string image = "";
            using (var expectedReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.Design.image.txt"))
            {
                image = expectedReader.ReadToEnd();
            }

            IssueWorkflow issueWorkflow = new IssueWorkflow
            {
                TenantId = Variables.TenantId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                WorkflowId =_workflowId,
                WorkflowImage = image,
                NewVersion = true,
                SkipStatus = true,
                VersionStatus = IssueVersioning.NoAction,
            };

            //Act
            var response = await PostAsync("api/Design/IssueWorkflow", issueWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<IssueWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.WorkflowId.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(3)]
        private async Task GetWorkflowVersions_should_succeed()
        {
            // Arrange
            int expected = 1;

            var response = await GetAsync($"api/Design/GetWorkflowVersions?workflowId={_workflowId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetWorkflowVersionsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual); 
        }

        [Fact, TestPriority(3)]
        private async Task GetWorkflowVersion_should_succeed()
        {
            // Arrange

            //Act
            var response = await GetAsync($"api/Design/GetWorkflowVersion?workflowId={_workflowId}&version={0}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetWorkflowVersionResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.WorkflowId.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(4)]
        public async Task IssueWorkflow_version2_should_succeed()
        {
            // Arrange
            string image = "";
            using (var expectedReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.Design.image.txt"))
            {
                image = expectedReader.ReadToEnd();
            }

            IssueWorkflow issueWorkflow = new IssueWorkflow
            {
                TenantId = Variables.TenantId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                WorkflowId = _workflowId,
                WorkflowImage = image,
                VersionStatus = IssueVersioning.NoAction,
                NewVersion = true,
                SkipStatus = true
            };

            //Act
            var response = await PostAsync("api/Design/IssueWorkflow", issueWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<IssueWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.WorkflowId.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(5)]
        private async Task GetWorkflowVersions_after_version2_should_succeed()
        {
            // Arrange
            int expected = 2;

            var response = await GetAsync($"api/Design/GetWorkflowVersions?workflowId={_workflowId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetWorkflowVersionsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(6)]
        public async Task RollbackWorkflow_should_succeed()
        {
            // Arrange
            RollbackWorkflowVersion rollbackWorkflow = new RollbackWorkflowVersion
            {
                WorkflowId  = _workflowId,
                Version = (int)0,
                SkipStatus = true,
                TenantId = Variables.TenantId,
                CatalogId = Variables.CatalogId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                SystemId = Variables.SystemId
            };

            //Act
            var response = await PutAsync("api/Design/RollbackWorkflow", rollbackWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<RollbackWorkflowVersionResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.WorkflowId.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(7)]
        private async Task GetWorkflowVersion_after_rollback_should_succeed()
        {
            // Arrange
            bool expected = true;

            //Act
            var response = await GetAsync($"api/Design/GetWorkflowVersion?workflowId={_workflowId}&version={0}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetWorkflowVersionResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual != null);
        }

        [Fact, TestPriority(7)]
        public async Task GetWorkflows_should_succeed()
        {
            // Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Design/GetWorkflows?FolderId={Variables.FolderId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetWorkflowsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(7)]
        private async Task GetWorkflow_should_succeed()
        {
            // Arrange

            //Act
            var response = await GetAsync($"api/Design/GetWorkflow?workflowId={_workflowId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(7)]
        public async Task SearchWorkflows_should_succeed()
        {
            // Arrange
            int expected = 2;
            
            //Act
            var response = await GetAsync($"api/Design/SearchWorkflows?SearchString=fold&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<SearchWorkflowsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(7)]
        public async Task GetWorkflowByTitle_should_succeed()
        {
            // Arrange

            //Act
            var response = await GetAsync($"api/Design/GetWorkflowByTitle?TenantId={Guid.NewGuid()}&ProjectId={Variables.ProjectId}&SubProjectId=" +
                $"{Variables.SubProjectId}&SystemId={Variables.SystemId}&CatalogId={Variables.CatalogId}&Title=Workflow1");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetWorkflowsResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(7)]
        public async Task 
        ByDate_should_succeed()
        {
            // Arrange
            int expected = 3;

            //Act
            var response = await GetAsync($"api/Design/GetWorkflowVersions?WorkflowId={_workflowId}&DateFrom={DateTime.Now.AddYears(-1)}&DateTo={DateTime.Now.AddDays(1)}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetWorkflowsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(8)]
        public async Task PasteWorkflow_should_succeed()
        {
            // Arrange
            var workf = new PasteWorkflow
            {
                FolderId = Variables.FolderId,
                WorkflowId = _workflowId,
                Type = SenseAI.Domain.PasteType.Copy
            };

            //Act
            var response = await PostAsync("api/Design/PasteWorkflow", workf);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<PasteWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.WorkflowId.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(9)]
        public async Task GetWorkflows_after_copy_should_succeed()
        {
            // Arrange
            int expected = 3;

            //Act
            var response = await GetAsync($"api/Design/GetWorkflows?FolderId={Variables.FolderId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetWorkflowsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(10)]
        public async Task DeleteWorkflow_should_succeed()
        {
            // Arrange
            bool expected = true;
            DeleteWorkflow deleteWorkflow = new DeleteWorkflow
            {
                Id = _workflowId
            };

            //Act
            var response = await DeleteAsync("api/Design/DeleteWorkflow", deleteWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(11)]
        public async Task GetWorkflows_after_delete_should_succeed()
        {
            // Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Design/GetWorkflows?FolderId={Variables.FolderId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetWorkflowsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(11)]
        public async Task ImportArisWorkflow_should_success()
        {
            //Arrange
            var expected = true;
            string xmlContent = "";
            var xmlreader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.ArisWorkflow.aris.xml");
            {
                xmlContent = xmlreader.ReadToEnd();
            }

            var surveyBytes = Encoding.UTF8.GetBytes(xmlContent);
            var byteArrayContent = new ByteArrayContent(surveyBytes);
            byteArrayContent.Headers.ContentType = MediaTypeHeaderValue.Parse("text/xml");
            var form = new MultipartFormDataContent
            {
                {byteArrayContent, "\"file\"", "\"aris.xml\""}
            };

            //Act
            var response = await PostFileAsync($"api/Design/ImportArisWorkflow?folderId={Variables.ImportFolderId}&systemId={Variables.SystemId}&catalogId={Variables.CatalogId}", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
           // Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(12)]
        public async Task GetWorkflows_after_import_should_succeed()
        {
            // Arrange
            int expected = 4;

            //Act
            var response = await GetAsync($"api/Design/GetWorkflows?FolderId={Variables.ImportFolderId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetWorkflowsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(13)]
        public async Task AddWorkflow_should_fail()
        {
            //Arrange
            string expected = "FolderId cannot be empty.";
            AddWorkflow addWorkflow = new AddWorkflow
            {
                Title = "SenseAI Workflow",
                Description = "SenseAI Workflow notes",
            };

            //Act
            var response = await PostAsync("api/Design/AddWorkflow", addWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task AddWorkflow_should_empty_title_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            AddWorkflow addWorkflow = new AddWorkflow
            {
                FolderId = Guid.NewGuid(),
                Description = "SenseAI Workflow notes",
            };

            //Act
            var response = await PostAsync("api/Design/AddWorkflow", addWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task SaveWorkflow_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";
            SaveWorkflow saveWorkflow = new SaveWorkflow
            {
                Json = "",
                WorkflowPathsJson = ""
            };

            //Act
            var response = await PutAsync("api/Design/SaveWorkflow", saveWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task SaveWorkflow_empty_json_should_fail()
        {
            // Arrange
            string expected = "Json cannot be empty.";
            SaveWorkflow saveWorkflow = new SaveWorkflow
            {
                Id = Guid.NewGuid(),
                WorkflowPathsJson = ""
            };

            //Act
            var response = await PutAsync("api/Design/SaveWorkflow", saveWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task RenameWorkflow_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";
            RenameWorkflow renameWorkflow = new RenameWorkflow
            {
                Title = "SenseAI Workflow - renamed"
            };

            //Act
            var response = await PutAsync("api/Design/RenameWorkflow", renameWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task RenameWorkflow_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty.";
            RenameWorkflow renameWorkflow = new RenameWorkflow
            {
                WorkflowId = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Design/RenameWorkflow", renameWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task IssueWorkflow_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";
            IssueWorkflow issueWorkflow = new IssueWorkflow
            {
                TenantId = Variables.TenantId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                NewVersion = true,
                SkipStatus = true
            };

            //Act
            var response = await PostAsync("api/Design/IssueWorkflow", issueWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        private async Task GetWorkflowVersions_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";

            var response = await GetAsync($"api/Design/GetWorkflowVersions?workflowId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        private async Task GetWorkflowVersions_empty_result_should_fail()
        {
            // Arrange
            string expected = "No workflow versions found.";

            var response = await GetAsync($"api/Design/GetWorkflowVersions?workflowId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        private async Task GetWorkflowVersion_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";

            var response = await GetAsync($"api/Design/GetWorkflowVersion?workflowId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        private async Task GetWorkflowVersion_empty_result_should_fail()
        {
            // Arrange
            string expected = "No workflow version found.";

            var response = await GetAsync($"api/Design/GetWorkflowVersion?workflowId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task RollbackWorkflow_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";
            RollbackWorkflowVersion rollbackWorkflow = new RollbackWorkflowVersion
            {
                Version = (int)0,
                SkipStatus = true
            };

            //Act
            var response = await PutAsync("api/Design/RollbackWorkflow", rollbackWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task GetWorkflows_should_fail()
        {
            // Arrange
            string expected = "FolderId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Design/GetWorkflows?FolderId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task GetWorkflows_empty_result_should_fail()
        {
            // Arrange
            string expected = "No workflows found in folder.";

            //Act
            var response = await GetAsync($"api/Design/GetWorkflows?FolderId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task GetWorkflow_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await GetAsync($"api/Design/GetWorkflow?FolderId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(13)]
        public async Task GetWorkflow_empty_result_should_fail()
        {
            // Arrange
            string expected = "No workflow found.";

            //Act
            var response = await GetAsync($"api/Design/GetWorkflow?workflowId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task SearchWorkflows_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Design/SearchWorkflows?SearchString=fold&SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task SearchWorkflows_empty_result_should_fail()
        {
            // Arrange
            string expected = "No workflows found in SubProject.";

            //Act
            var response = await GetAsync($"api/Design/SearchWorkflows?SearchString=fold&SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}