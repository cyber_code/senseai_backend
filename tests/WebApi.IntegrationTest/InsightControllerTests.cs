﻿using FluentAssertions;
using FluentAssertions.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.StaticFiles;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Application.Commands.InsightModule.Defects;
using Application.Commands.InsightModule.ExecutionLogs;
using Core.Abstractions.Bindings;
using Queries.InsightModule;
using WebApi.IntegrationTest.Infrastructure;
using WebApi.IntegrationTest.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class InsightControllerTests : ControllerTestBase
    {
        protected readonly IBindingConfiguration _bindingConfiguration;
        private readonly TestServerFixture _fixture;
        private HttpClient _client;
        private HttpClientHandler handler = new HttpClientHandler();

        public InsightControllerTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        private HttpClient Client => _client ?? (_client = new HttpClient(handler));

        [Fact, TestPriority(2)]
        public async Task GetDefectsColumnsMapping()
        {
            var response = await GetAsync("api/Insight/GetDefectsColumnMapping");
            Assert.True(response.StatusCode == HttpStatusCode.OK, "Data Extraction API is not accessible");
            response.GetObject<List<string>>();
            Assert.NotNull(response);
        }

        public string GetMime(string fileName)
        {
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(fileName, out string contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }

        [Fact, TestPriority(0)]
        public async Task UploadExecutionLogs()
        {
            //Arrange
            string dir = Directory.GetCurrentDirectory() + @"\Resources\Insight\ExecutionLogs\";
            string[] filePaths = Directory.GetFiles(dir);
            var formFiles = new FormFileCollection();

            using (var formDataContent = new MultipartFormDataContent())
            {
                foreach (var file in filePaths)
                {
                    var fileContent = new StreamContent(File.OpenRead(file))
                    {
                        Headers =
                        {
                            ContentLength = file.Length,
                            ContentType = new MediaTypeHeaderValue(GetMime(file))
                        }
                    };
                    formDataContent.Add(fileContent, "files", Path.GetFileName(file));
                }
                formDataContent.Add(new StringContent("0b450e5c-9b76-4851-b341-a42fdc129d68"), "TenantId");
                formDataContent.Add(new StringContent("0b450e5c-9b76-4851-b341-a42fdc129d68"), "ProjectId");
                formDataContent.Add(new StringContent("0b450e5c-9b76-4851-b341-a42fdc129d68"), "SubProjectId");
                formDataContent.Add(new StringContent("0b450e5c-9b76-4851-b341-a42fdc129d68"), "SystemId");
                formDataContent.Add(new StringContent("0b450e5c-9b76-4851-b341-a42fdc129d68"), "CatalogId");

                //Act
                // Check  https://github.com/aspnet/Hosting/issues/1116
                var response = await _fixture.Client.PostAsync("api/Insight/UploadExecutionLogs", formDataContent);
                response.EnsureSuccessStatusCode();

                //Assert
                Assert.NotNull(response);
                response.StatusCode.Should().Be(HttpStatusCode.OK);
                bool success = response.Content.ReadAsStringAsync().IsCompletedSuccessfully;
                success.Should().Be(true);

                var responseResult = await response.Content.ReadAsStringAsync();
                JObject jObject = JObject.Parse(responseResult);
                string result = jObject["result"].ToString();
                //error comunication from data extraction to ML.
               // Assert.True(bool.Parse(result), "Failed to communicate with ML API or DataExtraction API");
            }
        }

        [Fact, TestPriority(0)]
        public async Task AcceptChanges()
        {
            // check again
            var attributesList = new List<TypicalChangesAttributes>();
            var attribute = new TypicalChangesAttributes
            {
                ExtraData = "extract data",
                IsExternal = true,
                IsMultiValue = false,
                IsNoChange = false,
                IsNoInput = false,
                IsRequired = true,
                MaximumLength = 100,
                MinimumLength = 1,
                Name = "Name",
                PossibleValues = new string[] { "value1", "value2" },
                RelatedApplicationIsConfiguration = false,
                RelatedApplicationName = "RelatedApplicationName"
            };
            attributesList.Add(attribute);

            var acceptTypicalChangesModel = new AcceptTypicalChangesModel
            {
                Id = Guid.Parse("BD0CB2DF-77EE-4C57-8FBE-0C1A0F3A64DB"),
                CatalogId = Guid.Parse("0b450e5c-9b76-4851-b341-a42fdc129d68"),
                TenantId = Guid.Parse("0b450e5c-9b76-4851-b341-a42fdc129d68"),
                ProjectId = Guid.Parse("0b450e5c-9b76-4851-b341-a42fdc129d68"),
                SubProjectId = Guid.Parse("0b450e5c-9b76-4851-b341-a42fdc129d68"),
                SystemId = Guid.Parse("0b450e5c-9b76-4851-b341-a42fdc129d68"),
                Title = "Title",
                IsConfiguration = true,
                Type = 1,
                HasAutomaticId = true,
                Attributes = attributesList.ToArray()
            };
            //Act
            var response = await PostAsync("api/Insight/AcceptChanges", acceptTypicalChangesModel);
            response.EnsureSuccessStatusCode();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);  
        }
        /*
        [Fact, TestPriority(0)]
        public async Task SaveDefects()
        {
            //Arrange
            string mapping = "";
            using (var expectedReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.Insight.Defects.mapping.json"))
            {
                mapping = expectedReader.ReadToEnd();
            }
            string dir = Directory.GetCurrentDirectory() + @"\Resources\Insight\Defects\";
            string[] filePaths = Directory.GetFiles(dir);
            var formFiles = new FormFileCollection();

            var formDataContent = new System.Net.Http.MultipartFormDataContent();

            foreach (var file in filePaths)
            {
                if (file.EndsWith(".csv"))
                {
                    var fileContent = new StreamContent(File.OpenRead(file))
                    {
                        Headers =
                    {
                        ContentLength = file.Length,
                        ContentType = new MediaTypeHeaderValue(GetMime(file))
                    }
                    };
                    formDataContent.Add(fileContent, "files", Path.GetFileName(file));
                }
            }
            formDataContent.Add(new StringContent(Variables.TenantId.ToString()), "TenantId");
            formDataContent.Add(new StringContent(Variables.ProjectId.ToString()), "ProjectId");
            formDataContent.Add(new StringContent(Variables.SubProjectId.ToString()), "SubProjectId");
            formDataContent.Add(new StringContent(Variables.SystemId.ToString()), "SystemId");
            formDataContent.Add(new StringContent(Variables.CatalogId.ToString()), "CatalogId");
            formDataContent.Add(new StringContent(mapping), "Mapping");
            formDataContent.Add(new StringContent("en"), "Lang");
            //Act
            var response = await _fixture.Client.PostAsync("api/Insight/SaveDefects", formDataContent);
            response.EnsureSuccessStatusCode();

            //Assert
            Assert.NotNull(response);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            bool success = response.Content.ReadAsStringAsync().IsCompletedSuccessfully;
            success.Should().Be(true);
            var responseResult = await response.Content.ReadAsStringAsync();
            JObject jObject = JObject.Parse(responseResult);
            string result = jObject["result"].ToString();
            //var responseResult = response.GetObjectAsync<bool>().Result;
            Assert.True(bool.Parse(result) == true, "ML API or DataExtraction API is not accessible");
        }*/
    }
}