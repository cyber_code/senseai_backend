﻿using Application.Services;
using Commands.ProcessModule.GenericWorkflows;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.DesignModule;
using Queries.ProcessModule.GenericWorkflows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using WebApi.IntegrationTest.Resources;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class GenericWorkflowControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _genericWorkflowId;
        public GenericWorkflowControllerTests(TestServerFixture testServerFixture) : base(testServerFixture)
        {
            _fixture = testServerFixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddGenericWorkflow_should_success()
        {
            //Arrange
            var genericWorkflow = new AddGenericWorkflow
            {
                ProcessId = Variables.ProcessId,
                Title = "title 1",
                Description = "Description 1",
            };

            //Act
            var response = await PostAsync("api/Process/GenericWorkflow/AddGenericWorkflow", genericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddGenericWorkflowResult>();
            _genericWorkflowId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task SaveGenericWorkflow_should_success()
        {
            //Arrange
            string jsonContent = "";
            using (var jsonReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.GenericWorkflow.UIDesignerJson.json"))
            {
                jsonContent = jsonReader.ReadToEnd();
            }

            var saveGenericWorkflow = new SaveGenericWorkflow
            {
                Id = _genericWorkflowId,
                DesignerJson = jsonContent,
            };

            //Act
            var response = await PutAsync("api/Process/GenericWorkflow/SaveGenericWorkflow", saveGenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<SaveGenericWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
        }

        [Fact, TestPriority(2)]
        public async Task RollbackGenericWorkflowVersion_with_noversion_should_fail()
        {
            //Arrange
            string expected = "The Generic workflow does not have any version.";
            var rollbackGenericWorkflowVersion = new RollbackGenericWorkflowVersion
            {
                GenericWorkflowId = _genericWorkflowId,
                Version = 0,
            };

            //Act
            var response = await PutAsync("api/Process/GenericWorkflow/RollbackGenericWorkflow", rollbackGenericWorkflowVersion);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(2)]
        public async Task RenameGenericWorkflow_should_success()
        {
            //Arrange
            var renameGenericWorkflow = new RenameGenericWorkflow
            {
                Id = _genericWorkflowId,
                Title = " Tile is updated."
            };

            //Act
            var response = await PutAsync("api/Process/GenericWorkflow/RenameGenericWorkflow", renameGenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetGenericWorkflowsResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetGenericWorkflows_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/GenericWorkflow/GetGenericWorkflows?ProcessId={Variables.ProcessId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetGenericWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetGenericWorkflow_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync("api/Process/GenericWorkflow/GetGenericWorkflow?Id=" + _genericWorkflowId);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetGenericWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(3)]
        public async Task IssueGenericWorkflow_versions_0_should_success()
        {
            //Arrange
            var issueGenericWorkflow = new IssueGenericWorkflow
            {
                GenericWorkflowId = _genericWorkflowId,
                NewVersion = true,
            };

            //Act
            var response = await PostAsync("api/Process/GenericWorkflow/IssueGenericWorkflow", issueGenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<IssueGenericWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(_genericWorkflowId, actual.GenericWorkflowId);
        }

        [Fact, TestPriority(4)]
        public async Task RollbackGenericWorkflowVersion_with_noitems_should_success()
        {
            //Arrange
            var rollbackGenericWorkflowVersion = new RollbackGenericWorkflowVersion
            {
                GenericWorkflowId = _genericWorkflowId,
                Version = 0,
            };

            //Act
            var response = await PutAsync("api/Process/GenericWorkflow/RollbackGenericWorkflow", rollbackGenericWorkflowVersion);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<RollbackGenericWorkflowVersionResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
        }

        [Fact, TestPriority(5)]
        public async Task IssueGenericWorkflow_version1_should_success()
        {
            //Arrange
            var issueGenericWorkflow = new IssueGenericWorkflow
            {
                GenericWorkflowId = _genericWorkflowId,
                NewVersion = true,
            };

            //Act
            var response = await PostAsync("api/Process/GenericWorkflow/IssueGenericWorkflow", issueGenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<IssueGenericWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(_genericWorkflowId, actual.GenericWorkflowId);
        }

        [Fact, TestPriority(6)]
        public async Task RollbackGenericWorkflowVersion_should_success()
        {
            //Arrange
            var rollbackGenericWorkflowVersion = new RollbackGenericWorkflowVersion
            {
                GenericWorkflowId = _genericWorkflowId,
                Version = 0,
            };

            //Act
            var response = await PutAsync("api/Process/GenericWorkflow/RollbackGenericWorkflow", rollbackGenericWorkflowVersion);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<RollbackGenericWorkflowVersionResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
        }

        [Fact, TestPriority(7)]
        public async Task GetGenericWorkflowVersions_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/GenericWorkflow/GetGenericWorkflowVersions?GenericWorkflowId={_genericWorkflowId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetGenericWorkflowVersionsResult>().Result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(7)]
        public async Task GetGenericWorkflowVersionByDate_should_succeed()
        {
            // Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/GenericWorkflow/GetGenericWorkflowVersionsByDate?GenericWorkflowId={_genericWorkflowId}&DateFrom={DateTime.Now.AddYears(-1)}&DateTo={DateTime.Now.AddDays(1)}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetGenericWorkflowVersionsByDateResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(8)]
        public async Task DeleteGenericWorkflow_should_success()
        {
            //Arrange
            bool expected = true;
            var GenericWorkflow = new DeleteGenericWorkflow
            {
                Id = _genericWorkflowId
            };

            //Act
            var response = await DeleteAsync("api/Process/GenericWorkflow/DeleteGenericWorkflow", GenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(12)]
        public async Task AddGenericWorkflow_should_failed()
        {
            //Arrange           
            string expected = "ProcessId cannot be empty.";
            var GenericWorkflow = new AddGenericWorkflow
            {
                ProcessId = Guid.Empty,
                Title = "title 1",
                Description = "Description 1",
            };

            //Act
            var response = await PostAsync("api/Process/GenericWorkflow/AddGenericWorkflow", GenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task AddGenericWorkflow_empty_title_should_failed()
        {
            //Arrange           
            string expected = "Title cannot be empty.";
            var GenericWorkflow = new AddGenericWorkflow
            {
                ProcessId = Guid.NewGuid(),
                Description = "Description 1",
            };

            //Act
            var response = await PostAsync("api/Process/GenericWorkflow/AddGenericWorkflow", GenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task SaveGenericWorkflow_should_failed()
        {
            //Arrange       
            string expected = "Id cannot be empty.";
            var GenericWorkflow = new SaveGenericWorkflow
            {
                Id = Guid.Empty,
                DesignerJson = null,
            };

            //Act
            var response = await PutAsync("api/Process/GenericWorkflow/SaveGenericWorkflow", GenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task SaveGenericWorkflow_empty_designerJson_should_failed()
        {
            //Arrange       
            string expected = "DesignerJson cannot be empty.";
            var GenericWorkflow = new SaveGenericWorkflow
            {
                Id = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Process/GenericWorkflow/SaveGenericWorkflow", GenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task RenameGenericWorkflow_should_failed()
        {
            //Arrange           
            string expected = "Id cannot be empty.";
            var GenericWorkflow = new RenameGenericWorkflow
            {
                Id = Guid.Empty,
                Title = "update tile",
            };

            //Act
            var response = await PutAsync("api/Process/GenericWorkflow/RenameGenericWorkflow", GenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task RenameGenericWorkflow_empty_title_should_failed()
        {
            //Arrange           
            string expected = "Title cannot be empty.";
            var GenericWorkflow = new RenameGenericWorkflow
            {
                Id = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync("api/Process/GenericWorkflow/RenameGenericWorkflow", GenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task GetGenericWorkflow_should_failed()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/GenericWorkflow/GetGenericWorkflow?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(12)]
        public async Task GetGenericWorkflowWithoutRecords_should_failed()
        {
            //Arrange
            string expected = "Generic workflow not found in the process.";

            //Act
            var response = await GetAsync($"api/Process/GenericWorkflow/GetGenericWorkflow?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(12)]
        public async Task GetGenericWorkflowVersionsWithoutRecords_should_failed()
        {
            //Arrange
            string expected = "No GenericWorkflow Versions found.";

            //Act
            var response = await GetAsync($"api/Process/GenericWorkflow/GetGenericWorkflowVersions?GenericWorkflowId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(12)]
        public async Task GetGenericWorkflowVersionByDate_should_fail()
        {
            //Arrange
            string expected = "GenericWorkflowId cannot be empty.";


            //Act
            var response = await GetAsync($"api/Process/GenericWorkflow/GetGenericWorkflowVersionsByDate?GenericWorkflowId={Guid.Empty}&DateFrom={DateTime.Now.AddYears(-1)}&DateTo={DateTime.Now.AddDays(1)}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(12)]
        public async Task GetGenericWorkflows_should_failed()
        {
            //Arrange
            string expected = "ProcessId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/GenericWorkflow/GetGenericWorkflows?ProcessId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task GetGenericWorkflows_empty_result_should_failed()
        {
            //Arrange
            string expected = "Generic workflow not found in the process.";

            //Act
            var response = await GetAsync($"api/Process/GenericWorkflow/GetGenericWorkflows?ProcessId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
      
        [Fact, TestPriority(12)]
        public async Task DeleteGenericWorkflow_should_failed()
        {
            //Arrange     
            string expected = "Id cannot be empty.";
            var GenericWorkflow = new DeleteGenericWorkflow
            {
                Id = Guid.Empty
            };

            //Act
            var response = await DeleteAsync("api/Process/GenericWorkflow/DeleteGenericWorkflow", GenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task IssueGenericWorkflow_should_failed()
        {
            // Arrange
            string expected = "GenericWorkflowId cannot be empty.";
            var issueGenericWorkflow = new IssueGenericWorkflow
            {
                GenericWorkflowId = Guid.Empty,
                NewVersion = true,
            };

            //Act
            var response = await PostAsync("api/Process/GenericWorkflow/IssueGenericWorkflow", issueGenericWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task RollbackGenericWorkflowVersion_parameters_should_failed()
        {
            //Arrange   
            string expected = "GenericWorkflowId cannot be empty.";
            var rollbackGenericWorkflowVersion = new RollbackGenericWorkflowVersion
            {
                GenericWorkflowId = Guid.Empty,
                Version = 0,
            };

            //Act
            var response = await PutAsync("api/Process/GenericWorkflow/RollbackGenericWorkflow", rollbackGenericWorkflowVersion);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }


        private async Task<GetGenericWorkflowsResult> ImportGenericWorkflow2GenericDesigner()
        {
            //Arrange
            var processId = new Guid("C50C7A2C-6B77-4600-9650-F78EAF29CACE");
            var gtResponse = await GetAsync("api/Process/GenericWorkflow/GetGenericWorkflows?ProcessId=" + processId);
            gtResponse.EnsureSuccessStatusCode();
            var lstGenericWorkflows = gtResponse.GetCollectionAsync<GetGenericWorkflowsResult>().Result;

            string xmlContent = "";
            var xmlreader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.GenericWorkflow.generic.xml");
            {
                xmlContent = xmlreader.ReadToEnd();
            }
            var surveyBytes = Encoding.UTF8.GetBytes(xmlContent);
            var byteArrayContent = new ByteArrayContent(surveyBytes);
            byteArrayContent.Headers.ContentType = MediaTypeHeaderValue.Parse("text/xml");
            var form = new MultipartFormDataContent
            {
                {byteArrayContent, "\"file\"", "\"generic.xml\""}
            };

            //Act
            var response = await PostFileAsync("api/Process/GenericWorkflow/ImportGenericWorkflow2GenericDesigner?processId=" + processId, form);
            var actual = response.GetObject<bool>();


            gtResponse = await GetAsync("api/Process/GenericWorkflow/GetGenericWorkflows?ProcessId=" + processId);
            gtResponse.EnsureSuccessStatusCode();
            var result = gtResponse.GetCollectionAsync<GetGenericWorkflowsResult>().Result;
            var genericWokflow = result.Where(w => !lstGenericWorkflows.Select(s => s.Id).Contains(w.Id)).FirstOrDefault();

            /*update the desinger json*/
            string jsonContent = "";
            using (var jsonReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.GenericWorkflow.UIDesignerJson.json"))
            {
                jsonContent = jsonReader.ReadToEnd();
            }

            var saveGenericWorkflow = new SaveGenericWorkflow
            {
                Id = genericWokflow.Id,
                DesignerJson = jsonContent,
            };

            //Act
            response = await PutAsync("api/Process/GenericWorkflow/SaveGenericWorkflow", saveGenericWorkflow);
            response.EnsureSuccessStatusCode();


            return genericWokflow;
        }
    }
}
