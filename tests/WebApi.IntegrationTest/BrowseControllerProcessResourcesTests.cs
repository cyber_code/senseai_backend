﻿using Application.Services;
using Commands.ProcessModule.ProcessResources;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.ProcessModule.ProcessResources;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using WebApi.IntegrationTest.Resources;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerProcessResourcesTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _processResourceId;

        public BrowseControllerProcessResourcesTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddProcessResources_should_success()
        {
            // Arrange
            string fileContent = "";
            using (var reader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.ProcessResources.resource1.txt"))
            {
                fileContent = reader.ReadToEnd();
            }

            ByteArrayContent bytes = new ByteArrayContent(Encoding.UTF8.GetBytes(fileContent));

            var form = new MultipartFormDataContent
            {
                {bytes, "FileCollection", "resource1.txt"}
            };

            form.Add(new StringContent(Variables.ProcessId.ToString()), "ProcessId");

            //Act
            var response = await PostFileAsync($"api/Process/Browse/AddProcessResources", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<AddProcessResourcesResult>()[0];
            _processResourceId = actual.Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateProcessResources_should_success()
        {
            // Arrange
            bool expected = true;
            string fileContent = "";
            using (var reader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.ProcessResources.resource2.txt"))
            {
                fileContent = reader.ReadToEnd();
            }

            ByteArrayContent bytes = new ByteArrayContent(Encoding.UTF8.GetBytes(fileContent));

            var form = new MultipartFormDataContent
            {
                {bytes, "File", "resource2.txt"}
            };

            form.Add(new StringContent(_processResourceId.ToString()), "Id");
            form.Add(new StringContent(Variables.ProcessId.ToString()), "ProcessId");

            //Act
            var response = await PutFileAsync($"api/Process/Browse/UpdateProcessResources", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetProcessResource_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessResource?Id={_processResourceId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetProcessResourceResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }
        [Fact, TestPriority(2)]
        public async Task DownloadProcessResource_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/Browse/DownloadProcessResource?Id={_processResourceId}");
            response.EnsureSuccessStatusCode();
           
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
  
         
        }
        [Fact, TestPriority(2)]
        public async Task GetProcesResources_should_success()
        {
            //Arrange
            int expected = 3;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessResources?ProcessId={Variables.ProcessId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetProcessResourceResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact,TestPriority(3)]
        public async Task DeleteProcessResource_should_success()
        {
            //Arrange
            bool expected = true;

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteProcessResources?Id={_processResourceId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetProcesResources_after_delete_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessResources?ProcessId={Variables.ProcessId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetProcessResourceResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task AddProcessResources_should_fail()
        {
            //Arrange
            string expected = "ProcessId cannot be empty.";

            var process = new AddProcessResources
            {
                FileCollection = null
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddProcessResources", process);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddProcessResources_empty_filename_should_fail()
        {
            //Arrange
            string expected = "File Content cannot be empty.";

            var form = new MultipartFormDataContent
            {
            };

            form.Add(new StringContent(Variables.ProcessId.ToString()), "ProcessId");

            //Act
            var response = await PostFileAsync($"api/Process/Browse/AddProcessResources", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateProcesssResource_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            var form = new MultipartFormDataContent
            {
            };

            form.Add(new StringContent(Guid.NewGuid().ToString()), "ProcessId");

            //Act
            var response = await PutFileAsync($"api/Process/Browse/UpdateProcessResources", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateProcesssResource_empty_processId_should_fail()
        {
            //Arrange
            string expected = "ProcessId cannot be empty.";
            
            var form = new MultipartFormDataContent
            {
            };

            form.Add(new StringContent(Guid.NewGuid().ToString()), "Id");

            //Act
            var response = await PutFileAsync($"api/Process/Browse/UpdateProcessResources", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateProcesssResource_empty_filename_should_fail()
        {
            //Arrange
            string expected = "File cannot be empty.";
            var form = new MultipartFormDataContent
            {
            };

            form.Add(new StringContent(Guid.NewGuid().ToString()), "Id");
            form.Add(new StringContent(Guid.NewGuid().ToString()), "ProcessId");


            //Act
            var response = await PutFileAsync($"api/Process/Browse/UpdateProcessResources", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeleteProcessResources_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteProcessResources?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetProcessResource_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessResource?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetProcessResource_empty_result_should_fail()
        {
            //Arrange
            string expected = "No process resource found.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessResource?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetProcesResources_should_fail()
        {
            //Arrange
            string expected = "process id cannot be empty!";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessResources?ProcessId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetProcesResources_empty_result_should_fail()
        {
            //Arrange
            string expected = "No process resources found.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessResources?ProcessId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}