﻿using System;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using Commands.ProcessModule.Hierarchies;
using Queries.ProcessModule.Hierarchies;
using SenseAI.Domain;
using WebApi.IntegrationTest.Infrastructure;
using System.IO;
using Microsoft.AspNetCore.Http.Internal;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json.Linq;
using Application.Services;
using Messaging.Commands;
using Messaging.Queries;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerHierarchyTests : ControllerTestBase
    {
        private static readonly Random random = new Random();
        private static Guid _hierarchyId;
        private readonly TestServerFixture _fixture;

        public BrowseControllerHierarchyTests(TestServerFixture fixture) : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddHierarchy_should_success()
        {
            // Arrange
            AddHierarchy hierarchy = new AddHierarchy()
            {
                SubProjectId = Variables.SubProjectId,
                HierarchyTypeId = Variables.HierarchyTypeId.ToString(),
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            // Act
            var response = await PostAsync("api/Process/Browse/AddHierarchy", hierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddHierarchyResult>();
            _hierarchyId = actual.Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateHierarchy_should_success()
        {
            // Arrange
            bool expected = true;
            UpdateHierarchy uhierarchy = new UpdateHierarchy()
            {
                Id = _hierarchyId,
                ParentId = Guid.Empty.ToString(),
                SubProjectId = Variables.SubProjectId,
                HierarchyTypeId = Variables.HierarchyTypeId,
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            // Act
            var response = await PutAsync("api/Process/Browse/UpdateHierarchy", uhierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetHierarchy_should_success()
        {
            // Arrange

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchy?Id={_hierarchyId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetHierarchyResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetHierarchies_should_success()
        {
            // Arrange
            int expected = 4;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchies?HierarchyTypeId={Variables.HierarchyTypeId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetHierarchiesResult>().Result.Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetHierarchiesByParent_should_success()
        {
            // Arrange
            int expected = 4;

            // Arrange
            var response = await GetAsync($"api/Process/Browse/GetHierarchiesByParent?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetHierarchiesResult>().Result.Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteHierarchy_should_success()
        {
            // Arrange
            bool expected = true;

            // Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteHierarchy?Id={_hierarchyId.ToString()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Arrange
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetHierarchies_after_delete_should_success()
        {
            // Arrange
            int expected = 3;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchies?HierarchyTypeId={Variables.HierarchyTypeId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetHierarchiesResult>().Result.Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task PasteHierarchy_should_success()
        {
            // Arrange
            PasteHierarchy pasteHierarchy = new PasteHierarchy
            {
                HierarchyId = Variables.HierarchyId,
                ParentId = Guid.Empty,
                PasteType = PasteType.Copy
            };

            // Act
            var response = await PostAsync("api/Process/Browse/PasteHierarchy", pasteHierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<PasteHierarchyResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.HierarchyId.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(6)]
        public async Task GetHierarchies_after_copy_past_should_success()
        {
            // Arrange
            int expected = 4;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchies?HierarchyTypeId={Variables.HierarchyTypeId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetHierarchiesResult>().Result.Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(7)]
        public async Task SearchHierarchy_should_success()
        {
            // Arrange
            int expected = 4;

            // Act
            var response = await GetAsync($"api/Process/Browse/SearchHierarchies?SubProjectId={Variables.SubProjectId}&ParentId={Guid.Empty}&SearchCriteria=title");
            response.EnsureSuccessStatusCode();

            var actual = response.GetObject<SearchHierarchiesResult>().ListOfHierarchies.Length;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(8)]
        public async Task ImportHierarchy_should_success()
        {
            //Arrange
            string file = Path.Combine(Directory.GetCurrentDirectory(), @"..\..\..\") + @"\Resources\Hierarchy\TEMENOS_Reference_Processes_Inventory_R19 - OnePage.xlsm";
            //string[] filePaths = Directory.GetFiles(dir);
            var formFiles = new FormFileCollection();

            using (var formDataContent = new MultipartFormDataContent())
            {
                var fileContent = new StreamContent(File.OpenRead(file))
                {
                    Headers =
                           {
                               ContentLength = file.Length,
                               ContentType = new MediaTypeHeaderValue(GetMime(file))
                           }
                };
                formDataContent.Add(fileContent, "File", Path.GetFileName(file));
                formDataContent.Add(new StringContent(Guid.NewGuid().ToString()), "SubProjectId");

                //Act
                var response = await _fixture.Client.PostAsync("api/Process/Browse/ImportHierarchy", formDataContent);
                response.EnsureSuccessStatusCode();

                //Assert
                Assert.NotNull(response);
                response.StatusCode.Should().Be(HttpStatusCode.OK);
                bool success = response.Content.ReadAsStringAsync().IsCompletedSuccessfully;
                success.Should().Be(true);

                var responseResult = await response.Content.ReadAsStringAsync();
                JObject jObject = JObject.Parse(responseResult);
                string result = jObject["result"].ToString();
                Assert.True(bool.Parse(result), "Failed to import Hierarchies");
            }
            //takes to much time to be executed.
        }

        [Fact, TestPriority(9)]
        public async Task AddHierarchy_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            AddHierarchy hierarchy = new AddHierarchy()
            {
                SubProjectId = Guid.Empty,
                HierarchyTypeId = null,
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            // Act
            var response = await PostAsync("api/Process/Browse/AddHierarchy", hierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task AddHierarchy_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";

            AddHierarchy hierarchy = new AddHierarchy()
            {
                SubProjectId = Guid.NewGuid(),
                Description = GenerateRandomString(30)
            };

            // Act
            var response = await PostAsync("api/Process/Browse/AddHierarchy", hierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateHierarchy_should_fail()
        {
            // Arrange
            string expected = "HierarchyId cannot be empty!";

            // Act
            UpdateHierarchy uhierarchy = new UpdateHierarchy()
            {
                ParentId = Guid.Empty.ToString(),
                SubProjectId = Guid.Empty,
                HierarchyTypeId = Guid.Empty,
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            var response = await PutAsync("api/Process/Browse/UpdateHierarchy", uhierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateHierarchy_empty_parentId_should_fail()
        {
            // Arrange
            string expected = "ParentId cannot be empty!";

            // Act
            UpdateHierarchy uhierarchy = new UpdateHierarchy()
            {
                Id = Guid.NewGuid(),
                SubProjectId = Guid.Empty,
                HierarchyTypeId = Guid.Empty,
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            var response = await PutAsync("api/Process/Browse/UpdateHierarchy", uhierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateHierarchy_empty_subProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            // Act
            UpdateHierarchy uhierarchy = new UpdateHierarchy()
            {
                Id = Guid.NewGuid(),
                ParentId = Guid.NewGuid().ToString(),
                HierarchyTypeId = Guid.NewGuid(),
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            var response = await PutAsync("api/Process/Browse/UpdateHierarchy", uhierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateHierarchy_empty_hierarchyTypeId_should_fail()
        {
            // Arrange
            string expected = "HierarchyTypeId cannot be empty!";

            // Act
            UpdateHierarchy uhierarchy = new UpdateHierarchy()
            {
                Id = Guid.NewGuid(),
                ParentId = Guid.NewGuid().ToString(),
                SubProjectId = Guid.NewGuid(),
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            var response = await PutAsync("api/Process/Browse/UpdateHierarchy", uhierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateHierarchy_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";

            // Act
            UpdateHierarchy uhierarchy = new UpdateHierarchy()
            {
                Id = Guid.NewGuid(),
                ParentId = Guid.NewGuid().ToString(),
                SubProjectId = Guid.NewGuid(),
                HierarchyTypeId = Guid.NewGuid(),
                Description = GenerateRandomString(30)
            };

            var response = await PutAsync("api/Process/Browse/UpdateHierarchy", uhierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task DeleteHierarchy_should_fail()
        {
            // Arrange
            string expected = "HierarchyId cannot be empty!";

            // Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteHierarchy?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetHierarchy_should_fail()
        {
            // Arrange
            string expected = "HierarchyId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchy?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetHierarchy_empty_result_should_fail()
        {
            // Arrange
            string expected = "No Hierarchy found.";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchy?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetHierarchies_should_fail()
        {
            // Arrange
            string expected = "HierarchyTypeId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchies?HierarchyTypeId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetHierarchies_empty_subProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchies?HierarchyTypeId={Guid.NewGuid()}&SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetHierarchies_empty_result_should_fail()
        {
            // Arrange
            string expected = "No Hierarchies found in subProject.";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchies?HierarchyTypeId={Guid.NewGuid()}&SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetHierarchiesByParent_should_fail()
        {
            // Arrange
            string expected = "SubProjectId must be provided!";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchiesByParent?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetHierarchiesByParent_empty_result_should_fail()
        {
            // Arrange
            string expected = "No hierarchy type found.";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchiesByParent?SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task PasteHierarchy_should_fail()
        {
            // Arrange
            string expected = "HierarchyId cannot be empty!";
            PasteHierarchy pasteHierarchy = new PasteHierarchy()
            {
                HierarchyId = Guid.Empty,
                ParentId = Guid.NewGuid(),
                PasteType = PasteType.Copy
            };

            // Act
            var response = await PostAsync("api/Process/Browse/PasteHierarchy", pasteHierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task ImportHierarchy_should_fail()
        {
            //Arrange
            string file = Path.Combine( Directory.GetCurrentDirectory() , @"..\..\..\") + @"\Resources\Hierarchy\TEMENOS_Reference_Processes_Inventory_R19 - OnePage.xlsm";
            //string file = @"C:\Users\valmir.muqkurtaj\source\repos\validata-ai\tests\WebApi.IntegrationTest\Resources\Hierarchy\TEMENOS_Reference_Processes_Inventory_R19 - OnePage.xlsm";
            var formFiles = new FormFileCollection();

            using (var formDataContent = new MultipartFormDataContent())
            {
                var fileContent = new StreamContent(File.OpenRead(file))
                {
                    Headers =
                        {
                            ContentLength = file.Length,
                            ContentType = new MediaTypeHeaderValue(GetMime(file))
                        }
                };
                formDataContent.Add(fileContent, "File", Path.GetFileName(file));
                formDataContent.Add(new StringContent("00000000-0000-0000-0000-000000000000"), "SubProjectId");

                //Act
                var response = await _fixture.Client.PostAsync("api/Process/Browse/ImportHierarchy", formDataContent);
                response.EnsureSuccessStatusCode();

                //Assert
                Assert.NotNull(response);
                response.StatusCode.Should().Be(HttpStatusCode.OK);
                bool success = response.Content.ReadAsStringAsync().IsCompletedSuccessfully;
                success.Should().Be(true);

                var responseResult = await response.Content.ReadAsStringAsync();
                JObject jObject = JObject.Parse(responseResult);
                string result = jObject["result"].ToString();
                Assert.False(bool.Parse(result), "Failed to import Hierarchies");
            }
        }

        [Fact, TestPriority(9)]
        public async Task SearchHierarchy_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";
            SearchHierarchies searchHierarchy = new SearchHierarchies()
            {
                SearchCriteria = "title"
            };

            // Act
            var response = await GetAsync($"api/Process/Browse/SearchHierarchies?SubProjectId={searchHierarchy.SubProjectId}&ParentId={searchHierarchy.ParentId}&SearchCriteria={searchHierarchy.SearchCriteria}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task SearchHierarchy_empty_searchCriteria_should_fail()
        {
            // Arrange
            string expected = "Search criteria cannot be empty!";
            SearchHierarchies searchHierarchy = new SearchHierarchies()
            {
                SubProjectId = Guid.NewGuid()
            };

            // Act
            var response = await GetAsync($"api/Process/Browse/SearchHierarchies?SubProjectId={searchHierarchy.SubProjectId}&ParentId={searchHierarchy.ParentId}&SearchCriteria={searchHierarchy.SearchCriteria}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task SearchHierarchy_empty_result_should_fail()
        {
            // Arrange
            string expected = "No HierarchyTypes found in subProject.";
            SearchHierarchies searchHierarchy = new SearchHierarchies()
            {
                SubProjectId = Guid.NewGuid(),
                SearchCriteria = "title"
            };

            // Act
            var response = await GetAsync($"api/Process/Browse/SearchHierarchies?SubProjectId={searchHierarchy.SubProjectId}&ParentId={searchHierarchy.ParentId}&SearchCriteria={searchHierarchy.SearchCriteria}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(10)]
        public async Task MoveHierarchy_should_success()
        {
            // Arrange
            bool expected = true;
            MoveHierarchy moveHierarchy = new MoveHierarchy
            {
                FromHierarchyId = Variables.FromHierarchyId,
                ToHierarchyId = Variables.ToHierarchyId
            };

            // Act
            var response = await PutAsync("api/Process/Browse/MoveHierarchy", moveHierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Arrange
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(10)]
        public async Task MoveHierarchy_empty_FromHierarchyId_should_fail()
        {
            //Arrange
            string expected = "FromHierarchyId cannot be empty!";
            MoveHierarchy moveHierarchy = new MoveHierarchy
            {
                FromHierarchyId = Guid.Empty,
                ToHierarchyId = Variables.ToHierarchyId
            };

            //Act
            var response = await PutAsync("api/Process/Browse/MoveHierarchy", moveHierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Contains(actual.Message.ToLower(), expected.ToLower());
        }

        [Fact, TestPriority(10)]
        public async Task MoveHierarchy_empty_ToHierarchyId_should_fail()
        {
            //Arrange
            string expected = "ToHierarchyId cannot be empty!";
            MoveHierarchy moveHierarchy = new MoveHierarchy
            {
                FromHierarchyId = Variables.FromHierarchyId,
                ToHierarchyId = Guid.Empty
            };

            //Act
            var response = await PutAsync("api/Process/Browse/MoveHierarchy", moveHierarchy);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Contains(actual.Message.ToLower(), expected.ToLower());
        }


        public string GetMime(string fileName)
        {
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(fileName, out string contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }

        private static string GenerateRandomString(int strLength)
        {
            const string alphanumericChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            return new string(Enumerable.Repeat(alphanumericChar, strLength)
              .Select(str => str[random.Next(str.Length)])
              .ToArray());
        }
    }
}