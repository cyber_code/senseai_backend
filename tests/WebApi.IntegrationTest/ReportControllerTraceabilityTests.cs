﻿using System;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using Commands.ProcessModule.Hierarchies;
using Queries.ProcessModule.Hierarchies;
using SenseAI.Domain;
using WebApi.IntegrationTest.Infrastructure;
using System.IO;
using Microsoft.AspNetCore.Http.Internal;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json.Linq;
using Application.Services;
using Messaging.Commands;
using Messaging.Queries;
using Queries.ReportModule.TraceabilityReport;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class ReportControllerTraceabilityTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public ReportControllerTraceabilityTests(TestServerFixture fixture) : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task TraceabilityReportLine_Zero_Level_should_success()
        {
            
            // Act
            var response = await GetAsync($"api/Process/Reports/GetTraceabilityReportLine?SubProjectId={Variables.SubProjectId}&Level=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<GetTraceabilityReportResult>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(TraceablityReportLevel.Hierarchy, actual.NextLevel);
        }

        [Fact, TestPriority(0)]
        public async Task TraceabilityReportLine_Level_One_should_success()
        {

            // Act
            var response = await GetAsync($"api/Process/Reports/GetTraceabilityReportLine?SubProjectId={Variables.SubProjectId}&Level=1");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<GetTraceabilityReportResult>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(TraceablityReportLevel.Hierarchy, actual.NextLevel);
        }

        [Fact, TestPriority(0)]
        public async Task TraceabilityReportLine_Level_Two_should_success()
        {

            // Act
            var response = await GetAsync($"api/Process/Reports/GetTraceabilityReportLine?SubProjectId={Variables.SubProjectId}&Level=2");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<GetTraceabilityReportResult>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(TraceablityReportLevel.Hierarchy, actual.NextLevel);
        }

        [Fact, TestPriority(1)]
        public async Task TraceabilityReportLine_empty_subProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Reports/GetTraceabilityReportLine?SubProjectId={Guid.Empty}&Level=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        /*
        [Fact, TestPriority(1)]
        public async Task TraceabilityReportLine_Level_Not_Set_should_fail()
        {
            // Arrange
            string expected = "Level cannot be less then zero!";

            // Act
            var response = await GetAsync($"api/Process/Reports/GetTraceabilityReportLine?SubProjectId={Variables.SubProjectId}&Level=-1");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        */
        [Fact, TestPriority(1)]
        public async Task TraceabilityReportDetail_empty_subProjectId_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Reports/GetTraceabilityReportDetail?Id={Guid.Empty}&Level=0");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        /*
        [Fact, TestPriority(1)]
        public async Task TraceabilityReportDetail_Level_Not_Set_should_fail()
        {
            // Arrange
            string expected = "Level cannot be less then zero!";

            // Act
            var response = await GetAsync($"api/Process/Reports/GetTraceabilityReportDetail?Id={Variables.SubProjectId}&Level=-1");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        */


    }
}