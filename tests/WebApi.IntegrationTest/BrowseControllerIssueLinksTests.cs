﻿using Commands.ProcessModule.Issues;
using FluentAssertions;
using Queries.ProcessModule.Issues;
using System;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;
using Application.Services;
using Messaging.Commands;
using Commands.ProcessModule.Processes;
using SenseAI.Domain;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerIssueLinksTests : ControllerTestBase
    {
        private static Guid _businessChangeRequestIssueId;
        private static Guid _technicalChangeRequestIssueId;
        private static Guid _randomTypeIssue1Id;
        private static Guid _randomTypeIssue2Id;
        private readonly TestServerFixture _fixture;

        public BrowseControllerIssueLinksTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task LinkIssue_should_succeed()
        {
            _businessChangeRequestIssueId = AddBusinessChangeRequest().Result;
            _technicalChangeRequestIssueId = AddTechnicalChangeRequest().Result;

            // Arrange
            var expected = true;
            LinkIssue linkIssue = new LinkIssue
            {
                SrcIssueId = _businessChangeRequestIssueId,
                LinkType = IssueLinkType.DefaultLinkType,
                DstIssueId = _technicalChangeRequestIssueId,
            };

            // Act
            var response = await PostAsync("api/Process/Browse/LinkIssue", linkIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(0)]
        public async Task LinkIssue_restricted_issue_types_should_fail()
        {
            // Arrange
            _randomTypeIssue1Id = AddRandomTypeIssue().Result;
            _randomTypeIssue2Id = AddRandomTypeIssue().Result;
            var expected = "issue not found";

            LinkIssue linkIssue = new LinkIssue
            {
                SrcIssueId = _randomTypeIssue1Id,
                LinkType = IssueLinkType.DefaultLinkType,
                DstIssueId = _randomTypeIssue2Id,
            };

            // Act
            var response = await PostAsync("api/Process/Browse/LinkIssue", linkIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Contains(expected.ToLower(), actual.Message.ToLower());
        }

        /*
        [Fact, TestPriority(0)]
        public async Task LinkIssue_nonexistent_issue_should_fail()
        {
            // Arrange
            var expected = new string[] {
                "source issue does not exist",
                "destination issue does not exist",
            };

            LinkIssue linkIssue = new LinkIssue
            {
                SrcIssueId = Guid.NewGuid(),
                LinkType = IssueLinkType.IsLinkedTo,
                DstIssueId = Guid.NewGuid(),
            };

            // Act
            var response = await PostAsync("api/Process/Browse/LinkIssue", linkIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Contains(expected, e => actual.Message.ToLower().Contains(e.ToLower()));
        }
        */

        [Fact, TestPriority(0)]
        public async Task LinkIssue_empty_issueid_should_fail()
        {
            // Arrange
            var expected = new string[] {
                "SrcIssueId cannot be empty",
                "DstIssueId cannot be empty",
            };

            LinkIssue linkIssue = new LinkIssue
            {
                SrcIssueId = Guid.Empty,
                LinkType = IssueLinkType.DefaultLinkType,
                DstIssueId = Guid.Empty,
            };

            // Act
            var response = await PostAsync("api/Process/Browse/LinkIssue", linkIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Contains(expected, e => actual.Message.ToLower().Contains(e.ToLower()));
        }

        [Fact, TestPriority(1)]
        public async Task GetLinkedIssues_should_succeed()
        {
            // Arrange
            var expectedCount = 1;
            var expected = new GetLinkedIssuesResult()
            {
                IssueId = _technicalChangeRequestIssueId,
                LinkType = IssueLinkType.DefaultLinkType,
                IsDestination = false,
                Title = "Title of issue 1"
            };

            // Act
            var response = await GetAsync($"api/Process/Browse/GetLinkedIssues?IssueId={_businessChangeRequestIssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetLinkedIssuesResult>();

            // Assert
            actual.Count.Should().Be(expectedCount);
            actual[0].Should().BeEquivalentTo(expected);
        }

        /*
        [Fact, TestPriority(1)]
        public async Task GetLinkedIssues_nonexistent_issue_should_fail()
        {
            Assert.True(false);
        }
        */

        [Fact, TestPriority(1)]
        public async Task GetLinkedIssues_empty_issueid_should_fail()
        {
            // Arrange
            var expected = "IssueId cannot be empty";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetLinkedIssues?IssueId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Contains(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(2)]
        public async Task UnlinkIssue_should_succeed()
        {
            // Arrange
            var expected = true;
            UnlinkIssue unlinkIssue = new UnlinkIssue
            {
                SrcIssueId = _businessChangeRequestIssueId,
                DstIssueId = _technicalChangeRequestIssueId,
            };

            // Act
            var response = await DeleteAsync("api/Process/Browse/UnlinkIssue", unlinkIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            Assert.Equal(expected, actual);
        }

        /*
        [Fact, TestPriority(2)]
        public async Task UnlinkIssue_nonexistent_link_should_fail()
        {
            Assert.True(false);
        }
        */

        [Fact, TestPriority(2)]
        public async Task UnlinkIssue_empty_issueid_should_fail()
        {
            // Arrange
            var expected = new string[] {
                "SrcIssueId cannot be empty",
                "DstIssueId cannot be empty",
            };

            UnlinkIssue unlinkIssue = new UnlinkIssue
            {
                SrcIssueId = Guid.Empty,
                DstIssueId = Guid.Empty,
            };

            // Act
            var response = await DeleteAsync("api/Process/Browse/UnlinkIssue", unlinkIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Contains(expected, e => actual.Message.ToLower().Contains(e.ToLower()));
        }

        [Fact, TestPriority(3)]
        public async Task GetLinkedIssues_after_unlink_should_succeed()
        {
            // Arrange
            var expectedCount = 0;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetLinkedIssues?IssueId={_businessChangeRequestIssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetLinkedIssuesResult>();

            // Assert
            actual.Count.Should().Be(expectedCount);

            // cleanup
            DeleteIssue(_businessChangeRequestIssueId);
            DeleteIssue(_technicalChangeRequestIssueId);
            DeleteIssue(_randomTypeIssue1Id);
            DeleteIssue(_randomTypeIssue2Id);
        }

        #region Issue helpers

        private async Task<Guid> AddBusinessChangeRequest()
        {
            return AddIssue(Variables.BusinessChangeRequestIssueTypeId).Result;
        }

        private async Task<Guid> AddTechnicalChangeRequest()
        {
            return AddIssue(Variables.TechnicalChangeRequestIssueTypeId).Result;
        }

        private async Task<Guid> AddRandomTypeIssue()
        {
            return AddIssue(Guid.NewGuid()).Result;
        }

        private async Task<Guid> AddIssue(Guid issueTypeId)
        {
            AddIssue addIssue = new AddIssue
            {
                ProcessId = Variables.ProcessId,
                SubProjectId = Variables.SubProjectId,
                IssueTypeId = issueTypeId,
                AssignedId = Guid.NewGuid(),
                Title = "Title of issue 1",
                Description = "Issue description",
                Status = StatusType.ToDo,
                Estimate = 0
            };

            var response = await PostAsync("api/Process/Browse/AddIssue", addIssue);
            return response.GetObject<AddIssueResult>().Id;
        }

        private async Task DeleteIssue(Guid issueId)
        {
            await DeleteAsync($"api/Process/Browse/DeleteIssue?Id={issueId}");
        }
    }

    #endregion Issue helpers
}