﻿using Commands.AdministrationModule.Systems;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.AdministrationModule.Folder;
using Queries.AdministrationModule.Systems;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class AdministrationControllerSystemTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _systemId;

        public AdministrationControllerSystemTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddSystem_should_success()
        {
            //Arrange
            var system = new AddSystem
            {
                Title = RandomString(9),
                AdapterName = RandomString(10),
            };

            //Act
            var response = await PostAsync("api/Administration/AddSystem", system);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddSystemResult>();
            _systemId = actual.Id;

            //Assert
            actual.Id.Should().Be(actual.Id);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateSystem_should_success()
        {
            //Arrange
            var expected = true;
            var updateSystem = new UpdateSystem()
            {
                Id = _systemId,
                Title = "Title Updated",
                AdapterName = RandomString(8)
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSystem", updateSystem);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().Be(expected);
        }

        [Fact, TestPriority(2)]
        public async Task GetSystems_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Design/GetSystems");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<Queries.DesignModule.System.GetSystemsResult>().Result;
            int actual = result.Count;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteSystem_should_success()
        {
            //Arrange
            var expected = true;

            //Act
            var response = await DeleteAsync($"api/Administration/DeleteSystem?Id={_systemId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().Be(expected);
        }

        [Fact, TestPriority(4)]
        public async Task GetSystems_after_delete_should_success()
        {
            //Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Design/GetSystems");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<Queries.DesignModule.System.GetSystemsResult>().Result;
            int actual = result.Count;

            //Assert
            actual.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(4)]
        public async Task GetSystem_should_success()
        {
            //Arrange
            GetSystems getSystem = new GetSystems
            {
                Title = "T24",
                AdapterName = "T24Webadapter"
            };

            //Act
            var response = await GetAsync($"api/Administration/GetSystem?Title={getSystem.Title}&AdapterName={getSystem.AdapterName}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetSystemsResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
            Assert.Equal(getSystem.AdapterName.ToLower(), actual.AdapterName.ToLower());
        }

        //[Fact, TestPriority(5)]
        //public async Task AddSystem_should_fail()
        //{
        //    //Arrange
        //    var expected = "AdapterName already exists!";
        //    var system = new AddSystem
        //    {
        //        Title = "T24",
        //        AdapterName = "T24Webadapter",
        //    };

        //    //Arrange
        //    var response = await PostAsync("api/Administration/AddSystem", system);
        //    response.EnsureSuccessStatusCode();
        //    var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

        //    //Assert
        //    Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        //}

        [Fact, TestPriority(5)]
        public async Task AddSystem_empty_title_should_fail()
        {
            //Arrange
            var expected = "Title cannot be empty!";
            var system = new AddSystem
            {
                Title = null,
                AdapterName = "T24Webadapter",
            };

            //Arrange
            var response = await PostAsync("api/Administration/AddSystem", system);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddSystem_empty_adapter_should_fail()
        {
            //Arrange
            var expected = "AdapterName cannot be empty!";
            var system = new AddSystem
            {
                Title = "test",
                AdapterName = null,
            };

            //Arrange
            var response = await PostAsync("api/Administration/AddSystem", system);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        //[Fact, TestPriority(5)]
        //public async Task AddSystem_adaptername_exists_should_fail()
        //{
        //    //Arrange
        //    var expected = "AdapterName already exists!";
        //    var system = new AddSystem
        //    {
        //        Title = "test",
        //        AdapterName = "T24Webadapter",
        //    };

        //    //Arrange
        //    var response = await PostAsync("api/Administration/AddSystem", system);
        //    response.EnsureSuccessStatusCode();
        //   var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

        //    //Assert
        //    Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        //}

        [Fact, TestPriority(5)]
        public async Task UpdateSystem_should_fail()
        {
            var expected = "Id cannot be empty!";
            var updateSystem = new UpdateSystem()
            {
                Title = "Title Updated",
                AdapterName = RandomString(8)
            };
            //Act
            var response = await PutAsync($"api/Administration/UpdateSystem", updateSystem);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSystem_empty_title_should_fail()
        {
            var expected = "Title cannot be empty!";
            var updateSystem = new UpdateSystem()
            {
                Id = Guid.NewGuid(),
                AdapterName = RandomString(8)
            };
            //Act
            var response = await PutAsync($"api/Administration/UpdateSystem", updateSystem);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSystem_empty_adaptername_should_fail()
        {
            var expected = "AdapterName cannot be empty!";
            var updateSystem = new UpdateSystem()
            {
                Id = Guid.NewGuid(),
                Title = RandomString(8)
            };
            //Act
            var response = await PutAsync($"api/Administration/UpdateSystem", updateSystem);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSystem_adaptername_exists_should_fail()
        {
            var expected = "AdapterName already exists!";
            var updateSystem = new UpdateSystem()
            {
                Id = Guid.NewGuid(),
                Title = RandomString(8),
                AdapterName = "T24Webadapter"
            };
            //Act
            var response = await PutAsync($"api/Administration/UpdateSystem", updateSystem);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSystem_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty!";
            GetSystems getSystem = new GetSystems
            {
                Title = null,
                AdapterName = "T24Webadapter"
            };

            //Act
            var response = await GetAsync($"api/Administration/GetSystem?Title={getSystem.Title}&AdapterName={getSystem.AdapterName}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSystem_should_fail()
        {
            //Arrange
            string expected = "AdapterName cannot be empty!";
            GetSystems getSystem = new GetSystems
            {
                Title = "t24",
                AdapterName = null
            };

            //Act
            var response = await GetAsync($"api/Administration/GetSystem?Title={getSystem.Title}&AdapterName={getSystem.AdapterName}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSystem_empty_result_should_fail()
        {
            //Arrange
            string expected = "No system found.";
            GetSystems getSystem = new GetSystems
            {
                Title = "t24",
                AdapterName = null
            };

            //Act
            var response = await GetAsync($"api/Administration/GetSystem?Title=test&AdapterName=test");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeleteSystem_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty!";

            //Act
            var response = await DeleteAsync($"api/Administration/DeleteSystem?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        private Random random = new Random();
        public string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        [Fact, TestPriority(6)]
        public async Task GetAdapters_should_success()
        {
            //Arrange
            int expected = 1;

            //Act
            var response = await GetAsync("api/Administration/GetAdapters");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetAdaptersResult>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
    }
}
