﻿using System;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using Commands.ProcessModule.Hierarchies;
using Queries.ProcessModule.Hierarchies;
using WebApi.IntegrationTest.Infrastructure;
using Application.Services;
using Messaging.Commands;
using Messaging.Queries;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class ConfigureControllerHierarchyTypesTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _hierarchyTypeId;

        private static readonly Random random = new Random();

        public ConfigureControllerHierarchyTypesTests(TestServerFixture fixture) : base(fixture)
        {
            _fixture = fixture;
        }

        private static string GenerateRandomString(int strLength)
        {
            const string alphanumericChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            return new string(Enumerable.Repeat(alphanumericChar, strLength)
              .Select(str => str[random.Next(str.Length)])
              .ToArray());
        }

        [Fact, TestPriority(0)]
        public async Task AddHierarchyType_should_success()
        {
            // Arrange
            AddHierarchyType hierarchyType = new AddHierarchyType()
            {
                ParentId = Variables.HierarchyTypeId2.ToString(),
                SubProjectId = Variables.SubProjectId,
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30),
                ForceDelete=true
            };

            // Act
            var response = await PostAsync("api/Process/Configure/AddHierarchyType", hierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddHierarchyTypeResult>();
            _hierarchyTypeId = actual.Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateHierarchyType_should_success()
        {
            // Arrange
            bool expected = true;
            UpdateHierarchyType updateHierarchyType = new UpdateHierarchyType()
            {
                Id = _hierarchyTypeId,
                ParentId = Variables.DelHierarchyTypeId,
                SubProjectId = Variables.SubProjectId,
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            // Act
            var response = await PutAsync("api/Process/Configure/UpdateHierarchyType", updateHierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetHierarchyType_should_success()
        {
            // Arrange

            // Act
            var response = await GetAsync($"api/Process/Configure/GetHierarchyType?Id={_hierarchyTypeId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetHierarchyTypeResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetLastHierarchyType_should_success()
        {
            // Arrange

            //Act
            var response = await GetAsync($"api/Process/Configure/GetLastHierarchyType?Id={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetLastHierarchyTypeResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetHierarchyTypes_should_success()
        {
            // Arrange
            int expected = 3;

            //Act
            var response = await GetAsync($"api/Process/Configure/GetHierarchyTypes?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetHierarchyTypesResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task AddHierarchyType_for_move_should_success()
        {
            // Arrange
            AddHierarchyType hierarchyType = new AddHierarchyType()
            {
                ParentId = _hierarchyTypeId.ToString(),
                SubProjectId = Variables.SubProjectId,
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            // Act
            var response = await PostAsync("api/Process/Configure/AddHierarchyType", hierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddHierarchyTypeResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(4)]
        public async Task MoveHierarchyType_should_success()
        {
            // Arrange
            bool expected = true;
            MoveHierarchyType moveHierarchyType = new MoveHierarchyType
            {
                Id = _hierarchyTypeId
            };

            // Act
            var response = await PutAsync("api/Process/Configure/MoveHierarchyType", moveHierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Arrange
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task AddHierarchyType_after_move_should_success()
        {
            // Arrange
            AddHierarchyType hierarchyType = new AddHierarchyType()
            {
                ParentId = Variables.HierarchyTypeId2.ToString(),
                SubProjectId = Variables.SubProjectId,
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30),
                ForceDelete=true
            };

            // Act
            var response = await PostAsync("api/Process/Configure/AddHierarchyType", hierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddHierarchyTypeResult>();
            _hierarchyTypeId = actual.Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(6)]
        public async Task DeleteHierarchyType_should_success()
        {
            // Arrange
            bool expected = true;

            // Act
            var response = await DeleteAsync($"api/Process/Configure/DeleteHierarchyType?Id={_hierarchyTypeId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Arrange
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetHierarchyTypes_after_delete_should_success()
        {
            // Arrange
            int expected = 3;

            //Act
            var response = await GetAsync($"api/Process/Configure/GetHierarchyTypes?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetHierarchyTypesResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(8)]
        public async Task AddHierarchyType_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";
            AddHierarchyType hierarchyType = new AddHierarchyType()
            {
                SubProjectId = Guid.Empty,
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            // Act
            var response = await PostAsync("api/Process/Configure/AddHierarchyType", hierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task AddHierarchyType_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";
            AddHierarchyType hierarchyType = new AddHierarchyType()
            {
                SubProjectId = Guid.NewGuid(),
                Description = GenerateRandomString(30)
            };

            // Act
            var response = await PostAsync("api/Process/Configure/AddHierarchyType", hierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task UpdateHierarchyType_should_fail()
        {
            // Arrange
            string expected = "HierarchyTypeId cannot be empty!";
            UpdateHierarchyType updateHierarchyType = new UpdateHierarchyType()
            {
                ParentId = Guid.Empty,
                SubProjectId = Guid.Empty,
                Title = GenerateRandomString(20),
                Description = GenerateRandomString(30)
            };

            // Act
            var response = await PutAsync("api/Process/Configure/UpdateHierarchyType", updateHierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        //[Fact, TestPriority(8)]
        //public async Task UpdateHierarchyType_empty_parentId_should_fail()
        //{
        //    // Arrange
        //    string expected = "ParentId cannot be empty!";
        //    UpdateHierarchyType updateHierarchyType = new UpdateHierarchyType()
        //    {
        //        Id = Guid.NewGuid(),
        //        SubProjectId = Guid.NewGuid(),
        //        Title = GenerateRandomString(20),
        //        ParentId = Guid.Empty
        //    };

        //    // Act
        //    var response = await PutAsync("api/Process/Configure/UpdateHierarchyType", updateHierarchyType);
        //    response.EnsureSuccessStatusCode();
        //    var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

        //    //Assert
        //    Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        //}

        [Fact, TestPriority(8)]
        public async Task UpdateHierarchyType_empty_subProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";
            UpdateHierarchyType updateHierarchyType = new UpdateHierarchyType()
            {
                Id = Guid.NewGuid(),
                ParentId = Guid.NewGuid(),
                Title = GenerateRandomString(20)
            };

            // Act
            var response = await PutAsync("api/Process/Configure/UpdateHierarchyType", updateHierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task UpdateHierarchyType_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";
            UpdateHierarchyType updateHierarchyType = new UpdateHierarchyType()
            {
                Id = Guid.NewGuid(),
                ParentId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid()
            };

            // Act
            var response = await PutAsync("api/Process/Configure/UpdateHierarchyType", updateHierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task DeleteHierarchyType_should_fail()
        {
            // Arrange
            string expected = "HierarchyTypeId cannot be empty!";

            // Act
            var response = await DeleteAsync($"api/Process/Configure/DeleteHierarchyType?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetHierarchyType_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetHierarchyType?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetHierarchyType_empty_result_should_fail()
        {
            // Arrange
            string expected = "No HierarchyType found.";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetHierarchyType?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetLastHierarchyType_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetLastHierarchyType?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetLastHierarchyType_empty_result_should_fail()
        {
            // Arrange
            string expected = "No HierarchyType found in subProject.";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetLastHierarchyType?SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetHierarchyTypes_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetHierarchyTypes?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task GetHierarchyTypes_empty_result_should_fail()
        {
            // Arrange
            string expected = "No HierarchyTypes found with in subProject.";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetHierarchyTypes?SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(8)]
        public async Task MoveHierarchyType_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty!";
            MoveHierarchyType moveHierarchyType = new MoveHierarchyType
            {
                Id = Guid.Empty
            };

            //Act
            var response = await PutAsync("api/Process/Configure/MoveHierarchyType", moveHierarchyType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}