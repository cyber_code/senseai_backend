﻿using Application.Services;
using Commands.ProcessModule.Sprints;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.ProcessModule.Issues;
using Queries.ProcessModule.Sprints;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class SprintControllerIssuesTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _sprintIssueId;

        public SprintControllerIssuesTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddSprintIssue_should_succeed()
        {
            // Arrange
            AddSprintIssues addSprintIssue = new AddSprintIssues
            {
                SprintId = Variables.SprintId,
                IssueId = Guid.NewGuid(),
                Status = 1
            };

            //Act
            var response = await PostAsync("api/Process/Sprint/AddSprintIssue", addSprintIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddSprintIssuesResult>();
            _sprintIssueId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateSprintIssue_should_succeed()
        {
            // Arrange
            bool expected = true;
            UpdateSprintIssues updateSprintIssue = new UpdateSprintIssues
            {
                Id = _sprintIssueId,
                SprintId = Variables.SprintId,
                IssueId = Variables.IssueId,
                Status = 2
            };

            //Act
            var response = await PutAsync($"api/Process/Sprint/UpdateSprintIssue", updateSprintIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetSprintIssues_should_succeed()
        {
            // Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintIssues?SprintId={Variables.SprintId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSprintIssuesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetSprintIssuesWorkflow_should_succeed()
        {
            // Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetSprintIssuesWorkflow?SprintId={Variables.SprintId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSprintIssuesWorkflowResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetSprintIssuesTestCase_should_succeed()
        {
            // Arrange
            int expected = 0;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetSprintIssuesTestCase?SprintId={Variables.SprintId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSprintIssuesTestCaseResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteSprintIssue_should_succeed()
        {
            // Arrange
            bool expected = true;
            DeleteSprintIssues deleteSprintIssue = new DeleteSprintIssues
            {
                Id = _sprintIssueId
            };

            //Act
            var response = await DeleteAsync("api/Process/Sprint/DeleteSprintIssue", deleteSprintIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetSprintIssues_after_delete_should_succeed()
        {
            // Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintIssues?SprintId={Variables.SprintId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSprintIssuesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task AddSprintIssue_should_fail()
        {
            //Arrange
            string expected = "SprintId cannot be empty.";
            AddSprintIssues addSprintIssue = new AddSprintIssues
            {
                SprintId = Guid.Empty,
                IssueId = Guid.Empty,
            };

            //Act
            var response = await PostAsync("api/Process/Sprint/AddSprintIssue", addSprintIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddSprintIssue_empty_issueId_should_fail()
        {
            //Arrange
            string expected = "IssueId cannot be empty.";
            AddSprintIssues addSprintIssue = new AddSprintIssues
            {
                SprintId = Guid.NewGuid(),
                IssueId = Guid.Empty,
            };

            //Act
            var response = await PostAsync("api/Process/Sprint/AddSprintIssue", addSprintIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSprintIssue_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            UpdateSprintIssues updateSprintIssue = new UpdateSprintIssues
            {
                SprintId = Guid.Empty,
                IssueId = Guid.Empty,
            };

            //Act
            var response = await PutAsync($"api/Process/Sprint/UpdateSprintIssue", updateSprintIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSprintIssue_empty_sprintId_should_fail()
        {
            //Arrange
            string expected = "SprintId cannot be empty.";
            UpdateSprintIssues updateSprintIssue = new UpdateSprintIssues
            {
                Id = Guid.NewGuid(),
                SprintId = Guid.Empty,
                IssueId = Guid.Empty,
            };

            //Act
            var response = await PutAsync($"api/Process/Sprint/UpdateSprintIssue", updateSprintIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSprintIssue_empty_issueId_should_fail()
        {
            //Arrange
            string expected = "IssueId cannot be empty.";
            UpdateSprintIssues updateSprintIssue = new UpdateSprintIssues
            {
                Id = Guid.NewGuid(),
                SprintId = Guid.NewGuid(),
                IssueId = Guid.Empty,
            };

            //Act
            var response = await PutAsync($"api/Process/Sprint/UpdateSprintIssue", updateSprintIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeleteSprintIssue_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            DeleteSprintIssues deleteSprintIssue = new DeleteSprintIssues
            {
                Id = Guid.Empty
            };
            
            //Act
            var response = await DeleteAsync("api/Process/Sprint/DeleteSprintIssue", deleteSprintIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSprintIssuesWorkflow_should_fail()
        {
            //Arrange
            string expected = "SprintId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetSprintIssuesWorkflow?SprintId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSprintIssuesWorkflow_empty_result_should_fail()
        {
            //Arrange
            string expected = "No workflow found linked with sprint.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetSprintIssuesWorkflow?SprintId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSprintIssuesTestCase_should_fail()
        {
            //Arrange
            string expected = "SprintId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetSprintIssuesTestCase?SprintId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSprintIssuesTestCase_empty_result_should_fail()
        {
            //Arrange
            string expected = "No testCase found linked with sprint.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetSprintIssuesTestCase?SprintId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSprintIssues_should_fail()
        {
            //Arrange
            string expected = "SprintId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintIssues?SprintId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSprintIssues_empty_result_should_fail()
        {
            //Arrange
            string expected = "No Issues found.";

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintIssues?SprintId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
