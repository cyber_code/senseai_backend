﻿using FluentAssertions;
using Commands.DesignModule.Folder;
using Queries.AdministrationModule.Folder;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Messaging.Queries;
using Messaging.Commands;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class AdministrationControllerFoldersTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _folderId;

        public AdministrationControllerFoldersTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }


        [Fact, TestPriority(0)]
        public async Task AddFolder_should_success()
        {
            //Arrange
            var folder = new AddFolder
            {
                SubProjectId = Variables.SubProjectId,
                Title = "title 1",
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddFolder", folder);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddFolderResult>();
            _folderId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateFolder_should_success()
        {
            //Arrange
            bool expected = true;

            var updateFolder = new UpdateFolder
            {
                Id = _folderId,
                Title = "Folder updated",
                Description = "Description updated"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateFolder", updateFolder);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task PasteFolder_should_success()
        {
            //Arrange
            var firstFolder = new PasteFolder
            {
                FolderId = _folderId
            };

            //Act
            var response = await PostAsync($"api/Administration/PasteFolder", firstFolder);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<PastFolderResult>();

            //Assert
            actual.FolderId.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(3)]
        public async Task GetFolders_should_success()
        {
            //Arrange
            int expected = 5;

            //Act
            var response = await GetAsync($"api/Design/GetFolders?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetFolders>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task DeleteFolder_should_success()
        {
            //Arrange
            var expected = true;

            //Act
            var response = await DeleteAsync($"api/Administration/DeleteFolder?Id={_folderId}");
            response.EnsureSuccessStatusCode();
            bool actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task GetFolders_after_delete_should_success()
        {
            //Arrange
            int expected = 4;

            //Act
            var response = await GetAsync($"api/Design/GetFolders?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetFolders>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task GetFolder_should_success()
        {
            //Arrange 

            //Act
            var response = await GetAsync($"api/Administration/GetFolder?folderId={Variables.FolderId}");
            response.EnsureSuccessStatusCode();
            var result = response.GetObject<GetFolderResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            result.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(6)]
        public async Task DeleteFolderAndEverythingUnderFolder_should_success()
        {
            //Arrange
            var expected = true;

            ///Act
            var response = await DeleteAsync($"api/Administration/DeleteFolder?Id={Variables.DelFolderId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(7)]
        public async Task AddFolder_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";
            var folder = new AddFolder
            {
                SubProjectId = Guid.Empty,
                Title = null,
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddFolder", folder);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task AddFolder_should_fail_empty_title()
        {
            //Arrange
            string expected = "title cannot be empty.";
            var folder = new AddFolder
            {
                SubProjectId = Guid.NewGuid(),
                Title = null,
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddFolder", folder);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task UpdateFolder_should_fail()
        {
            //Arrange
            string expected = "FolderId cannot be empty.";
            UpdateFolder updateFolder = new UpdateFolder
            {
                Id = Guid.Empty,
                Title = null,
                Description = "test test"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateFolder", updateFolder);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task UpdateFolder_empty_title_should_fail()
        {
            //Arrange
            string expected = "title cannot be empty.";
            UpdateFolder updateFolder = new UpdateFolder
            {
                Id = Guid.NewGuid(),
                Title = null,
                Description = "test test"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateFolder", updateFolder);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task DeleteFolder_should_fail()
        {
            //Arrange
            string expected = "FolderId cannot be empty.";

            //Act
            var response = await DeleteAsync($"api/Administration/DeleteFolder?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetFolders_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Design/GetFolders?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetFolders_empty_result_should_fail()
        {
            //Arrange
            var id = Guid.NewGuid();
            string expected = string.Format("No Folders found.");

            //Act
            var response = await GetAsync($"api/Design/GetFolders?SubProjectId={id}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetFolder_should_fail()
        {
            //Arrange
            string expected = "FolderId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Administration/GetFolder?folderId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task GetFolder_empty_result_should_fail()
        {
            //Arrange
            var id = Guid.NewGuid();
            string expected = "No Folder found.";

            //Act
            var response = await GetAsync($"api/Administration/GetFolder?folderId={id}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(7)]
        public async Task PasteFolder_should_fail()
        {
            //Arrange
            string expected = "FolderId cannot be empty.";
            var folder = new PasteFolder
            {
                FolderId = Guid.Empty
            };

            //Act
            var response = await PostAsync($"api/Administration/PasteFolder", folder);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}