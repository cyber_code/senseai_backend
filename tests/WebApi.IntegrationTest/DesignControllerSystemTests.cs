﻿using FluentAssertions;
using Queries.DesignModule.Systems;
using WebApi.IntegrationTest.Infrastructure;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Messaging.Queries;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class DesignControllerSystemTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public DesignControllerSystemTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        
        [Fact, TestPriority(0)]
        public async Task GetConditionItem_should_succeed()
        {
            // Arrange
            int expected = 1;

            //Act
            var response = await GetAsync("api/Design/GetConditionItem?AdapterName=T24WebAdapter");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetConditionItemsResult>().Operators.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(0)]
        public async Task GetActionItem_should_succeed()
        {
            // Arrange
            int expected = 1;

            //Act
            var response = await GetAsync("api/Design/GetActionItem?AdapterName=T24WebAdapter");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetActionItemsResult>().ActionItems.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(0)]
        public async Task GetConditionItem_should_fail()
        {
            // Arrange
            string expected = "AdapterName cannot be empty.";

            //Act
            var response = await GetAsync("api/Design/GetConditionItem?AdapterName=");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(0)]
        public async Task GetConditionItem_empty_result_should_fail()
        {
            // Arrange
            string expected = "Adapter is not implemented.";

            //Act
            var response = await GetAsync("api/Design/GetConditionItem?AdapterName=test");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(0)]
        public async Task GetActionItem_should_fail()
        {
            // Arrange
            string expected = "AdapterName cannot be empty.";

            //Act
            var response = await GetAsync("api/Design/GetActionItem?AdapterName=");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(0)]
        public async Task GetActionItem_empty_result_should_fail()
        {
            // Arrange
            string expected = "Adapter is not implemented.";

            //Act
            var response = await GetAsync("api/Design/GetActionItem?AdapterName=test");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}