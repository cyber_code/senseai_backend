﻿using Newtonsoft.Json;
using Queries.DataModule.Data.DataSets;
using Queries.DesignModule.Suggestions;
using WebApi.IntegrationTest.Infrastructure;
using WebApi.IntegrationTest.Resources;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class NeuralEngineControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private readonly Uri neuralEngineAPI = new Uri("http://192.168.1.199:7100/");

        public NeuralEngineControllerTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(3)]
        public async Task GetSuggestions_should_success()
        {
            //Arrange

            //Act
            using (var jsonReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.Design.UIDesignerJson.json"))
            {
                var jsonContent = jsonReader.ReadToEnd();
                var getSuggestion = new GetSuggestions
                {
                    TenantId = Variables.TenantId,
                    ProjectId = Variables.ProjectId,
                    SubProjectId = Variables.SubProjectId,
                    CatalogId = Variables.CatalogId,
                    SystemId = Variables.SystemId,
                    CurrentItemId = Variables.SuggestionCurrentItemId,
                    Json = jsonContent
                };
                var response = await PostAsync("api/Design/GetSuggestions", getSuggestion);
                response.EnsureSuccessStatusCode();
                var actual = response.GetObjectAsync<GetSuggestionsResult>().Result;

                //Assert 
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
                //Assert.NotNull(actual);
            }
        }

        [Fact, TestPriority(3)]
        public async Task GetComputerGeneratedWorkflows()
        {
            //Arrange

            //Act 
            var getComputerGenWorkflows = new GetComputerGeneratedWorkflows
            {
                TenantId = Variables.TenantId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                TypicalId = Variables.TypicalId,
                TypicalTitle = "FUNDS.TRANSFER,ACTR",
                WorkflowTitle = "Customer"
            };
            var response = await PostAsync("api/Design/GetComputerGeneratedWorkflows", getComputerGenWorkflows);
            var actual = response.GetObjectAsync<GetComputerGeneratedWorkflowsResult>().Result;

            //Assert
            response.EnsureSuccessStatusCode();
            //Assert.NotNull(actual);
        }
        [Fact, TestPriority(3)]
        public async Task GetWorkflowCoverage()
        {
            //Arrange

            //Act

            var getCoverage = new GetWorkflowCoverage
            {
                TenantId = Variables.TenantId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                WorkflowId = Variables.WorkflowId,
            };
            var response = await PostAsync("api/Design/GetWorkflowCoverage", getCoverage);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectAsync<GetWorkflowCoverageResult>().Result;

            //Assert
            Assert.NotNull(actual);
        }
        [Fact, TestPriority(3)]
        public async Task GetPaths()
        {
            //Arrange

            //Act
            using (var jsonReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.Design.UIDesignerJson.json"))
            {
                var jsonContent = jsonReader.ReadToEnd();
                var getPaths = new GetPaths
                {
                    TenantId = Variables.TenantId,
                    ProjectId = Variables.ProjectId,
                    SubProjectId = Variables.SubProjectId,
                    CatalogId = Variables.CatalogId,
                    SystemId = Variables.SystemId,
                    Json = jsonContent
                };
                var response = await PostAsync("api/Design/GetPaths", getPaths);
                response.EnsureSuccessStatusCode();
                var actual = response.GetObjectAsync<GetPathsResult>().Result;
                //Assert 
                Assert.NotNull(actual);
            }
        }
        
        [Fact, TestPriority(3)]
        public async Task GetWorkflowCoverageFromNeuralEngine()
        {
            //Arrange

            //Act

            var getCoverage = new GetWorkflowCoverage
            {
                TenantId = Variables.TenantId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,

                WorkflowId = Variables.WorkflowId,
            };
            var myContent = JsonConvert.SerializeObject(getCoverage);
            var stringContent = new StringContent(myContent);
            using (var client = new HttpClient())
            {
                client.BaseAddress = neuralEngineAPI;
                var response = await client.PostAsync("getCoverage", stringContent);

                //Assert
                string resultContent = response.Content.ReadAsStringAsync().Result;
                response.EnsureSuccessStatusCode();  
            }
        }
        [Fact, TestPriority(3)]
        public async Task GetPathsFromNeuralEngine()
        {
            //Arrange

            //Act
            using (var jsonReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.Design.UIDesignerJson.json"))
            {
                var jsonContent = jsonReader.ReadToEnd();
                var getPaths = new GetPaths
                {
                    TenantId = Variables.TenantId,
                    ProjectId = Variables.ProjectId,
                    SubProjectId = Variables.SubProjectId,
                    CatalogId = Variables.CatalogId,
                    SystemId = Variables.SystemId,
                    Json = jsonContent
                };
                var myContent = JsonConvert.SerializeObject(getPaths);
                var stringContent = new StringContent(myContent);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = neuralEngineAPI;
                    var response = await client.PostAsync("paths", stringContent);

                    //Assert
                    string resultContent = response.Content.ReadAsStringAsync().Result;
                    response.EnsureSuccessStatusCode();
                    Assert.Equal(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        [Fact,TestPriority(0)]
        public async Task GetDataSetsFromNeuralEngine()
        {
            //Arrange

            //Act
            var getSuggestedDataSets = new GetSuggestedDataSets
            {
                TenantId = Variables.TenantId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                SystemId = Variables.SystemId,
                CatalogId = Variables.CatalogId,
                TypicalName = Variables.TypicalName,
                PathItemId = Guid.Empty
            };
            var myContent = JsonConvert.SerializeObject(getSuggestedDataSets);
            var stringContent = new StringContent(myContent);
            using (var client = new HttpClient())
            {
                client.BaseAddress = neuralEngineAPI;
                var response = await client.PostAsync("datasets", stringContent); 
                //Assert
                response.EnsureSuccessStatusCode();
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            }
        }
    }
}