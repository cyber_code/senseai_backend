﻿using Application.Services;
using FluentAssertions;
using Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerDocumentGenerationTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public BrowseControllerDocumentGenerationTests(TestServerFixture fixture) : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task GenerateHierarchyDocsByDate_Should_Success()
        {
            //Arrange
            var date = DateTime.Now;

            //Act
            var response = await GetAsync($"api/Process/Browse/GenerateHierarchyDocsByDate?HierarchyId={Variables.HierarchyId}&Date={date}");
            response.EnsureSuccessStatusCode();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task GenerateHierarchyDocsByDate_HierarchyId_Null_Should_Fail()
        {
            //Arrange
            var date = DateTime.Now;
            var expected = "HierarchyId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GenerateHierarchyDocsByDate?HierarchyId={Guid.Empty}&Date={date}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Contains(actual.Message.ToLower(), expected.ToLower());
        }

        [Fact]
        public async Task GenerateHierarchyDocsByDate_Date_Null_Should_Fail()
        {
            //Arrange
            var expected = "Date cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GenerateHierarchyDocsByDate?HierarchyId={Variables.HierarchyId}&Date=");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Contains(actual.Message.ToLower(), expected.ToLower());
        }

        [Fact]
        public async Task GenerateHierarchyDocs_Should_Success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/Browse/GenerateHierarchyDocs?HierarchyId={Variables.HierarchyId}");
            response.EnsureSuccessStatusCode();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task GenerateHierarchyDocs_Should_Fail()
        {
            //Arrange
            var expected = "HierarchyId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GenerateHierarchyDocs?HierarchyId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Contains(actual.Message.ToLower(), expected.ToLower());
        }

        [Fact]
        public async Task GenerateRequirementDocsByDate_Should_Success()
        {
            //Arrange
            var date = DateTime.Now;

            //Act
            var response = await GetAsync($"api/Process/Browse/GenerateRequirementDocsByDate?ProcessId={Variables.ProcessId}&Date={date}");
            response.EnsureSuccessStatusCode();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task GenerateRequirementDocsByDate_ProcessId_Null_Should_Fail()
        {
            //Arrange
            var date = DateTime.Now;
            var expected = "ProcessId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GenerateRequirementDocsByDate?ProcessId={Guid.Empty}&Date={date}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Contains(actual.Message.ToLower(), expected.ToLower());
        }

        [Fact]
        public async Task GenerateRequirementDocsByDate_Date_Null_Should_Fail()
        {
            //Arrange
            var expected = "Date cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GenerateRequirementDocsByDate?ProcessId={Variables.ProcessId}&Date=");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Contains(actual.Message.ToLower(), expected.ToLower());
        }

        [Fact]
        public async Task GenerateRequirementDocs_Should_Success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/Browse/GenerateRequirementDocs?ProcessId={Variables.ProcessId}");
            response.EnsureSuccessStatusCode();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task GenerateRequirementDocs_ProcessId_Empty_Should_Fail()
        {
            //Arrange
            var expected = "ProcessId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GenerateRequirementDocs?ProcessId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Contains(actual.Message.ToLower(), expected.ToLower());
        }
    }
}
