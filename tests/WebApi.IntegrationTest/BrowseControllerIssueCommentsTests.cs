﻿using Application.Services;
using Commands.ProcessModule.Issues;
using FluentAssertions;
using Messaging.Commands;
using Queries.ProcessModule.Issues;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerIssueCommentsTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _issueCommentId;


        public BrowseControllerIssueCommentsTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddIssueComment_should_success()
        {
            //Arrange
            var issue = new AddIssueComment
            {
                IssueId = Variables.IssueId,
                Comment = "Test Comment",
                ParentId = Guid.Empty,
                CommenterId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssueComment", issue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddIssueCommentResult>();
            _issueCommentId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task GetIssueComments_should_succes()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssueComments?IssueId={Variables.IssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssueCommentsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task DeleteIssueComment_should_sucess()
        {
            //Arrange
            bool expected = true;

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteIssueComment?Id={_issueCommentId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task GetIssueComments_after_delete_should_succes()
        {
            //Arrange
            int expected = 1;


            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssueComments?IssueId={Variables.IssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssueCommentsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task AddIssueComment_empty_comment_should_fail()
        {
            //Arrange
            string expected = "Comment cannot be empty.";
            var issue = new AddIssueComment
            {
                IssueId = Guid.NewGuid(),
                Comment = null,
                ParentId = Guid.NewGuid(),
                CommenterId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssueComment", issue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task AddIssueComment_empty_commenterId_should_fail()
        {
            //Arrange
            string expected = "CommenterId cannot be empty.";
            var issue = new AddIssueComment
            {
                IssueId = Guid.NewGuid(),
                Comment = "Test Comment"
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssueComment", issue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task AddIssueComment_should_fail()
        {
            //Arrange
            string expected = "IssueId cannot be empty.";
            var issue = new AddIssueComment
            {
                IssueId = Guid.Empty,
                Comment = "Test Comment",
                ParentId = Guid.NewGuid(),
                CommenterId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssueComment", issue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task DeleteIssueComment_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteIssueComment?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetIssueComments_should_fail()
        {
            //Arrange
            string expected = "IssueId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssueComments?IssueId={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetIssueComments_empty_result_should_fail()
        {
            //Arrange
            string expected = "No issue comments found.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssueComments?IssueId={ Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }


    }
}
