﻿using Application.Services;
using FluentAssertions;
using Messaging.Commands;
using Queries.ProcessModule.Hierarchies;
using Queries.ProcessModule.Processes;
using Queries.ReportModule.RequirementPrioritizationReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class ReportControllerRequirementPrioritizationReportTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public ReportControllerRequirementPrioritizationReportTests(TestServerFixture fixture) : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task GetRequirementPrioritizationReport_should_succeed()
        {
            // Arrange

            // columns should be all the hierarchy types in order plus requirement title plus requirement priority
            var expectedNumberOfColumns = 1 + 1 + 1;

            var expectedNumberOfProcesses = GetProcessesBySubProject(Variables.SubProjectId).Result.Count;

            // Act
            var response = await GetAsync($"api/Process/Reports/GetRequirementPrioritizationReport?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetRequirementPrioritizationReportResult>();

            // Assert

            // assert correct number of columns in column names
            actual.ColumnNames.Length.Should().Be(expectedNumberOfColumns);

            // assert correct number of rows
            actual.ReportData.Length.Should().Be(expectedNumberOfProcesses);

            // assert correct number of columns in each row
            foreach(var row in actual.ReportData)
            {
                row.Length.Should().Be(expectedNumberOfColumns);
            }
        }

        [Fact, TestPriority(0)]
        public async Task GetRequirementPrioritizationReport_empty_SubProject_should_fail()
        {
            // Arrange
            var expected = "subprojectid cannot be empty";

            // Act
            var response = await GetAsync($"api/Process/Reports/GetRequirementPrioritizationReport?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Contains(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(0)]
        public async Task GetRequirementPrioritizationReport_empty_result_should_fail()
        {
            // Arrange
            var expected = "no processes found in subproject";

            // Act
            var response = await GetAsync($"api/Process/Reports/GetRequirementPrioritizationReport?SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Contains(expected.ToLower(), actual.Message.ToLower());
        }

        private async Task<List<GetProcessesBySubProjectResult>> GetProcessesBySubProject(Guid subProjectId)
        {
            var response = await GetAsync($"api/Process/Browse/GetProcessesBySubProject?SubProjectId={subProjectId}");
            return response.GetCollection<GetProcessesBySubProjectResult>();
        }
    }
}
