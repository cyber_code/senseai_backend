﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.PlatformAbstractions;
using Application.Services;
using Core;
using System;
using System.IO;
using System.Net.Http;

namespace WebApi.IntegrationTest.Infrastructure
{
    public sealed class TestServerFixture : IDisposable
    {
        private static readonly TestServer TestServer; 
        private static bool isCreated;
        static TestServerFixture()
        {
            var builder = new WebHostBuilder()
                .UseContentRoot(GetContentRootPath())
                .UseEnvironment("Testing")  // environment should be Testing in order to execute all test cases against in memory db, unless overriden by environment variable
                .UseStartup<Startup>();

            TestServer = new TestServer(builder);



        }

        private static void installDB()
        {
            var installService = EngineContext.Current.Resolve<IInstallationService>();
            lock (installService)
            {
                if (!isCreated)
                    installService.InstallData();
                isCreated = true;

            }
        }

        public TestServerFixture()
        {

            Client = TestServer.CreateClient();
            if (!isCreated)
                installDB();

        }

        public HttpClient Client { get; }

        public void Dispose()
        {
            Client.Dispose();
        }

        private static string GetContentRootPath()
        {
            var testProjectPath = PlatformServices.Default.Application.ApplicationBasePath;
            var relativePathToHostProject = @"..\..\..\..\..\src\WebApi";
            return Path.Combine(testProjectPath, relativePathToHostProject);
        }
    }
}