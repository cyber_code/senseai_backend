﻿using FluentAssertions;
using Queries.DataModule.Data;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class DataControllerProductionDataTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public DataControllerProductionDataTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        #region ProductionData Tests

        [Fact, TestPriority(0)]
        public async Task GetAllProductionData_should_success()
        {
            //Arrange
            int expected = 1;
            //Act
            var response = await GetAsync("api/Data/GetAllProductionDatas");
            response.EnsureSuccessStatusCode(); 
            var actual = response.GetObjectAsync<GetProductionDatasResult>(); 
            var productionDatas = response.GetCollection<GetProductionDataResult>();
            //Assert
            Assert.True(productionDatas.Count >= expected);
            actual.Id.Should().NotBe(null);
        } 
        [Fact, TestPriority(0)]
        public async Task GetProductionDatabyId_should_success()
        {
            //Act
            var productionData = GetProductionData().Result; 
            var response = await GetAsync($"api/Data/GetProductionData?Id={productionData.Id}");
            response.EnsureSuccessStatusCode();  
            var actual = response.GetObject<GetProductionDataResult>();
            //Assert
            response.GetObjectAsync<GetProductionDataResult>().Should().NotBeNull();
            actual.Id.Should().NotBe(0);
        }
        [Fact, TestPriority(0)]
        public async Task GetProductionDataById_should_fail()
        {
            //Act
            string excepted = "Id cannot be empty."; 
            var response = await GetAsync($"api/Data/GetProductionData?Id={0}");
            response.EnsureSuccessStatusCode(); 
            //Assert
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(0)]
        public async Task GetProductionDataByIdWithoutRecords_should_fail()
        {
            //Act
            string excepted = "No Production Data found.";
            var response = await GetAsync($"api/Data/GetProductionData?Id={-1}");
            response.EnsureSuccessStatusCode();
            //Assert
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        
        private async Task<GetProductionDatasResult> GetProductionData()
        {
            var response = await GetAsync("api/Data/GetAllProductionDatas");
            return response.GetCollection<GetProductionDatasResult>().FirstOrDefault();
        }

        #endregion ProductionData Tests
    }
}