﻿using Commands.ProcessModule.Issues;
using FluentAssertions;
using Queries.ProcessModule.Issues;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;
using Application.Services;
using Messaging.Commands;
using Messaging.Queries;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerWorkflowIssuesTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _workflowIssueId;
        public BrowseControllerWorkflowIssuesTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddWorkflowIssue_should_succeed()
        {
            // Arrange
            var workflowIssue = new AddWorkflowIssue
            {
                IssueId = Variables.IssueId,
                WorkflowId = Guid.NewGuid()
            };

            // Act
            var response = await PostAsync("api/Process/Browse/AddWorkflowIssue", workflowIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddWorkflowIssueResult>();
            _workflowIssueId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateWorkflowIssue_should_succeed()
        {
            // Arrange
            bool expected = true;
            UpdateWorkflowIssue updateWorkflowIssue = new UpdateWorkflowIssue
            {
                Id = _workflowIssueId,
                IssueId = Variables.IssueId,
                WorkflowId = Guid.NewGuid()
            };

            // Act
            var response = await PutAsync("api/Process/Browse/UpdateWorkflowIssue", updateWorkflowIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetWorkflowIssue_should_succeed()
        {
            // Arrange

            // Act
            var response = await GetAsync($"api/Process/Browse/GetWorkflowIssue?Id={ _workflowIssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetWorkflowIssueResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBeEmpty();
        }

        [Fact, TestPriority(2)]
        public async Task GetWorkflowIssues_should_succeed()
        {
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetWorkflowIssues?WorkflowId={Variables.WorkflowId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetWorkflowIssuesResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetIssueWorkflows_should_succeed()
        {
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetIssueWorkflows?IssueId={Variables.IssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssueWorkflowsResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteWorkflowIssue_should_succeed()
        {
            // Arrange
            bool expected = true;

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteWorkflowIssue?Id={_workflowIssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task UnlinkWorkflowIssue_should_succeed()
        {
            // Arrange
            var workflowIssue = new AddWorkflowIssue
            {
                IssueId = Guid.NewGuid(),
                WorkflowId = Guid.NewGuid()
            };

            // Act
            var addWorkflowIssueResponse = await PostAsync("api/Process/Browse/AddWorkflowIssue", workflowIssue);
            addWorkflowIssueResponse.EnsureSuccessStatusCode();
            var workflowIssueId = addWorkflowIssueResponse.GetObject<AddWorkflowIssueResult>().Id;

            bool expected = true;
            var response = await DeleteAsync($"api/Process/Browse/UnlinkWorkflowIssue?WorkflowId={workflowIssue.WorkflowId}&IssueId={workflowIssue.IssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);

            var getWorkflowIssueResponse = await GetAsync($"api/Process/Browse/GetWorkflowIssue?Id={workflowIssueId}");
            getWorkflowIssueResponse.EnsureSuccessStatusCode();
            var actualWfIssueMsg = getWorkflowIssueResponse.GetObjectCmdResponseAsync<QueryResponseMessage>().Result.Message;

            //Assert
            Assert.Equal("no workflow issue found.", actualWfIssueMsg.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetIssueWorkflows_after_delete_should_succeed()
        {
            // Arrange
            int expected = 1;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetIssueWorkflows?IssueId={Variables.IssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssueWorkflowsResult>().Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task AddWorkflowIssue_should_fail()
        {
            // Arrange
            string expected = "WorkflowId cannot be empty.";
            AddWorkflowIssue  addWorkflowIssue    = new AddWorkflowIssue
            {
                WorkflowId = Guid.Empty,
                IssueId = Guid.Empty,
            };

            // Act
            var response = await PostAsync("api/Process/Browse/AddWorkflowIssue", addWorkflowIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddWorkflowIssue_empty_issueId_should_fail()
        {
            // Arrange
            string expected = "IssueId cannot be empty.";
            AddWorkflowIssue addWorkflowIssue = new AddWorkflowIssue
            {
                WorkflowId = Guid.NewGuid(),
                IssueId = Guid.Empty,
            };

            // Act
            var response = await PostAsync("api/Process/Browse/AddWorkflowIssue", addWorkflowIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateWorkflowIssue_should_fail()
        {
            // Arrange
            // Arrange
            string expected = "Id cannot be empty.";
            UpdateWorkflowIssue updateWorkflowIssue = new UpdateWorkflowIssue
            {
                WorkflowId = Guid.NewGuid(),
                IssueId = Guid.NewGuid(),
            };

            // Act
            var response = await PutAsync("api/Process/Browse/UpdateWorkflowIssue", updateWorkflowIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateWorkflowIssue_empty_workflowId_should_fail()
        {
            // Arrange
            // Arrange
            string expected = "WorkflowId cannot be empty.";
            UpdateWorkflowIssue updateWorkflowIssue = new UpdateWorkflowIssue
            {
                Id = Guid.NewGuid(),
                IssueId = Guid.NewGuid(),
            };

            // Act
            var response = await PutAsync("api/Process/Browse/UpdateWorkflowIssue", updateWorkflowIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateWorkflowIssue_empty_issueId_should_fail()
        {
            // Arrange
            // Arrange
            string expected = "issueId cannot be empty.";
            UpdateWorkflowIssue updateWorkflowIssue = new UpdateWorkflowIssue
            {
                Id = Guid.NewGuid(),
                WorkflowId = Guid.NewGuid(),
            };

            // Act
            var response = await PutAsync("api/Process/Browse/UpdateWorkflowIssue", updateWorkflowIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeleteWorkflowIssue_should_fail()
        {
            // Arrange
            bool expected = false;

            // Act
            var response = await DeleteAsync("api/Process/Browse/DeleteWorkflowIssue?Id=" + Guid.Empty);
            bool actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().Be(expected);
        }


        [Fact, TestPriority(5)]
        public async Task GetWorkflowIssue_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetWorkflowIssue?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetWorkflowIssue_empty_result_should_fail()
        {
            // Arrange
            string expected = "No workflow issue found.";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetWorkflowIssue?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetWorkflowIssues_should_fail()
        {
            // Arrange
            string expected = "WorkflowId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetWorkflowIssues?WorkflowId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetWorkflowIssues_empty_result_should_fail()
        {
            // Arrange
            string expected = "No Issue found linked with workflow.";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetWorkflowIssues?WorkflowId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueWorkflows_should_fail()
        {
            // Arrange
            string expected = "IssueId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetIssueWorkflows?IssueId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssueWorkflows_empty_result_should_fail()
        {
            // Arrange
            string expected = "No Workflow found linked with issue.";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetIssueWorkflows?IssueId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
