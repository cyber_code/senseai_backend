﻿using FluentAssertions;
using Newtonsoft.Json;
using Commands.DataModule.DataSets;
using Queries.DataModule.Data;
using Queries.DataModule.Data.DataSets;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Application.Services;
using Messaging.Commands;
using SenseAI.Domain;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class DataControllerDataSetsTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _dataSetId;
        private static Guid _manualDataSetId;
        public DataControllerDataSetsTests(TestServerFixture fixture)
                    : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddDataSet_should_success()
        {
            // Arrange
            List<Commands.DataModule.DataSets.Rows> rows = new List<Commands.DataModule.DataSets.Rows>();
            Commands.DataModule.DataSets.Rows row = new Commands.DataModule.DataSets.Rows();
            List<Commands.DataModule.DataSets.Attributes> attributes = new List<Commands.DataModule.DataSets.Attributes>();
            Commands.DataModule.DataSets.Attributes attribute = new Commands.DataModule.DataSets.Attributes("Name", "Test test");
            attributes.Add(attribute);

            attribute = new Commands.DataModule.DataSets.Attributes("Name", "10");
            attributes.Add(attribute);
            row.Attributes = new List<Commands.DataModule.DataSets.Attributes>();
            row.Attributes.AddRange(attributes);
            rows.Add(row);

            var dataset = new AddDataSet
            {
                Title = "Data set 1 from IT",
                SubProjectId = Variables.SubProjectId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                TypicalId = Variables.DataSetTypicalId,
                Rows = rows.ToArray()
            };

            // Act  
            var response = await PostAsync("api/Data/AddDataSet", dataset);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddDataSetResult>();
            _dataSetId = actual.Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }
        [Fact, TestPriority(9)] 
        public async Task AddDataSet_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Rows> rows = new List<Commands.DataModule.DataSets.Rows>();
            Commands.DataModule.DataSets.Rows row = new Commands.DataModule.DataSets.Rows();
            List<Commands.DataModule.DataSets.Attributes> attributes = new List<Commands.DataModule.DataSets.Attributes>();
            Commands.DataModule.DataSets.Attributes attribute = new Commands.DataModule.DataSets.Attributes("Name", "Test test");
            attributes.Add(attribute);

            attribute = new Commands.DataModule.DataSets.Attributes("Age", "10");
            attributes.Add(attribute);
            row.Attributes = new List<Commands.DataModule.DataSets.Attributes>();
            row.Attributes.AddRange(attributes);
            rows.Add(row); 
            var dataset = new AddDataSet
            {
                Title =null,
                SubProjectId = Guid.Empty,
                CatalogId = Guid.Empty,
                SystemId = Guid.Empty,
                TypicalId = Guid.Empty,
                Rows = rows.ToArray()
            };

            var response = await PostAsync("api/Data/AddDataSet", dataset);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower()); 
        }
        [Fact, TestPriority(9)]
        public async Task AddDataSetWithoutCatalogId_should_fail()
        {
            // Arrange
            string expected = "CatalogId cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Rows> rows = new List<Commands.DataModule.DataSets.Rows>();
            Commands.DataModule.DataSets.Rows row = new Commands.DataModule.DataSets.Rows();
            List<Commands.DataModule.DataSets.Attributes> attributes = new List<Commands.DataModule.DataSets.Attributes>();
            Commands.DataModule.DataSets.Attributes attribute = new Commands.DataModule.DataSets.Attributes("Name", "Test test");
            attributes.Add(attribute);

            attribute = new Commands.DataModule.DataSets.Attributes("Age", "10");
            attributes.Add(attribute);
            row.Attributes = new List<Commands.DataModule.DataSets.Attributes>();
            row.Attributes.AddRange(attributes);
            rows.Add(row);
            var dataset = new AddDataSet
            {
                Title = "Test title",
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.Empty,
                SystemId = Guid.NewGuid(),
                TypicalId = Guid.NewGuid(),
                Rows = rows.ToArray()
            };

            var response = await PostAsync("api/Data/AddDataSet", dataset);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(9)]
        public async Task AddDataSetWithoutSystemId_should_fail()
        {
            // Arrange
            string expected = "SystemId cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Rows> rows = new List<Commands.DataModule.DataSets.Rows>();
            Commands.DataModule.DataSets.Rows row = new Commands.DataModule.DataSets.Rows();
            List<Commands.DataModule.DataSets.Attributes> attributes = new List<Commands.DataModule.DataSets.Attributes>();
            Commands.DataModule.DataSets.Attributes attribute = new Commands.DataModule.DataSets.Attributes("Name", "Test test");
            attributes.Add(attribute);

            attribute = new Commands.DataModule.DataSets.Attributes("Age", "10");
            attributes.Add(attribute);
            row.Attributes = new List<Commands.DataModule.DataSets.Attributes>();
            row.Attributes.AddRange(attributes);
            rows.Add(row);
            var dataset = new AddDataSet
            {
                Title = "Test title",
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.Empty,
                TypicalId = Guid.NewGuid(),
                Rows = rows.ToArray()
            };

            var response = await PostAsync("api/Data/AddDataSet", dataset);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(9)]
        public async Task AddDataSetWithoutTypicalId_should_fail()
        {
            // Arrange
            string expected = "TypicalId cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Rows> rows = new List<Commands.DataModule.DataSets.Rows>();
            Commands.DataModule.DataSets.Rows row = new Commands.DataModule.DataSets.Rows();
            List<Commands.DataModule.DataSets.Attributes> attributes = new List<Commands.DataModule.DataSets.Attributes>();
            Commands.DataModule.DataSets.Attributes attribute = new Commands.DataModule.DataSets.Attributes("Name", "Test test");
            attributes.Add(attribute);

            attribute = new Commands.DataModule.DataSets.Attributes("Age", "10");
            attributes.Add(attribute);
            row.Attributes = new List<Commands.DataModule.DataSets.Attributes>();
            row.Attributes.AddRange(attributes);
            rows.Add(row);
            var dataset = new AddDataSet
            {
                Title = "Test title",
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.NewGuid(),
                TypicalId = Guid.Empty,
                Rows = rows.ToArray()
            };

            var response = await PostAsync("api/Data/AddDataSet", dataset);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(9)]
        public async Task AddDataSetWithoutTitle_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Rows> rows = new List<Commands.DataModule.DataSets.Rows>();
            Commands.DataModule.DataSets.Rows row = new Commands.DataModule.DataSets.Rows();
            List<Commands.DataModule.DataSets.Attributes> attributes = new List<Commands.DataModule.DataSets.Attributes>();
            Commands.DataModule.DataSets.Attributes attribute = new Commands.DataModule.DataSets.Attributes("Name", "Test test");
            attributes.Add(attribute);

            attribute = new Commands.DataModule.DataSets.Attributes("Age", "10");
            attributes.Add(attribute);
            row.Attributes = new List<Commands.DataModule.DataSets.Attributes>();
            row.Attributes.AddRange(attributes);
            rows.Add(row);
            var dataset = new AddDataSet
            {
                Title = null,
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.NewGuid(),
                TypicalId = Guid.NewGuid(),
                Rows = rows.ToArray()
            };

            var response = await PostAsync("api/Data/AddDataSet", dataset);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(9)]
        public async Task AddDataSetWithoutRows_should_fail()
        {
            // Arrange
            string expected = "Nr of rows cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Rows> rows = new List<Commands.DataModule.DataSets.Rows>();
            Commands.DataModule.DataSets.Rows row = new Commands.DataModule.DataSets.Rows();
            List<Commands.DataModule.DataSets.Attributes> attributes = new List<Commands.DataModule.DataSets.Attributes>();
            Commands.DataModule.DataSets.Attributes attribute = new Commands.DataModule.DataSets.Attributes("Name", "Test test");
            attributes.Add(attribute);

            attribute = new Commands.DataModule.DataSets.Attributes("Age", "10");
            attributes.Add(attribute);
            row.Attributes = new List<Commands.DataModule.DataSets.Attributes>();
            row.Attributes.AddRange(attributes);
            //rows.Add(row);
            var dataset = new AddDataSet
            {
                Title = "Test Title",
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.NewGuid(),
                TypicalId = Guid.NewGuid(),
                Rows = rows.ToArray()
            };

            var response = await PostAsync("api/Data/AddDataSet", dataset);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(9)] 
        public async Task UpdateDataSet_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Rows> rows = new List<Commands.DataModule.DataSets.Rows>();
            Commands.DataModule.DataSets.Rows row = new Commands.DataModule.DataSets.Rows();
            List<Commands.DataModule.DataSets.Attributes> attributes = new List<Commands.DataModule.DataSets.Attributes>();
            Commands.DataModule.DataSets.Attributes attribute = new Commands.DataModule.DataSets.Attributes("Name", "Test test");
            attributes.Add(attribute);

            attribute = new Commands.DataModule.DataSets.Attributes("Age", "10");
            attributes.Add(attribute);
            row.Attributes = new List<Commands.DataModule.DataSets.Attributes>();
            row.Attributes.AddRange(attributes);
            rows.Add(row);

            var dataset = new UpdateDataSet
            {
                Id = Guid.Empty,
                Title = null,
                SubProjectId = Guid.Empty,
                CatalogId = Guid.Empty,
                SystemId = Guid.Empty,
                TypicalId = Guid.Empty,
                Rows = rows.ToArray()
            };
            var response = await PutAsync("api/Data/UpdateDataSet", dataset);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result; 
            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());

        }
        [Fact, TestPriority(3)] 
        public async Task DeleteDataSet_should_success()
        {
            // Arrange
            bool expected = true;
            // Act
            var response = await DeleteAsync($"api/Data/DeleteDataSet?Id={_dataSetId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<DeleteDataSetResult>();

            // Assert
            Assert.True(actual != null && actual.Id != Guid.Empty); 


        }
        [Fact, TestPriority(9)]
        
        public async Task DeleteDataSet_should_fail()
        {
            // Arrange
            string excepted = "DataSetId cannot be empty.";
            // Act
            var response = await DeleteAsync("api/Data/DeleteDataSet?Id=" + Guid.Empty);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);


        }
        [Fact, TestPriority(0)] 
        public async Task AddManualDataSet_should_success()
        {
            // Arrange

            // Act
            List<Commands.DataModule.DataSets.Combinations> list = new List<Commands.DataModule.DataSets.Combinations>();
            Commands.DataModule.DataSets.Combinations combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Gender";
            combination.Values = new List<string>();
            combination.Values.Add("M");
            combination.Values.Add("F");
            list.Add(combination);

            combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Sector";
            combination.Values = new List<string>();
            combination.Values.Add("1000");
            combination.Values.Add("1001");
            combination.Values.Add("1002");
            list.Add(combination);

            var dataset = new AddManualDataSet
            {
                Title = "Data set 2 from IT - Manula",
                TenantId = Guid.NewGuid(),
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                TypicalId = Variables.DataSetTypicalId,
                TypicalName = Variables.TypicalName,
                Combinations = list.ToArray()
            };

            var response = await PostAsync("api/Data/AddManualDataSet", dataset);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddManualDataSetResult>();
            _manualDataSetId = actual.Id;
            // Assert
            Assert.True(actual != null && actual.Id != Guid.Empty);
        }

        [Fact, TestPriority(9)]  
        public async Task AddManualDataSet_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Combinations> list = new List<Commands.DataModule.DataSets.Combinations>();
            Commands.DataModule.DataSets.Combinations combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Gender";
            combination.Values = new List<string>();
            combination.Values.Add("M");
            combination.Values.Add("F");
            list.Add(combination);

            combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Sector";
            combination.Values = new List<string>();
            combination.Values.Add("1000");
            combination.Values.Add("1001");
            combination.Values.Add("1002");
            list.Add(combination);

            var dataset = new AddManualDataSet
            {
                Title = null,
                TenantId = Guid.NewGuid(),
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.Empty,
                CatalogId = Guid.Empty,
                SystemId = Guid.Empty,
                TypicalId = Guid.Empty,
                TypicalName = "CUSTOMER",
                Combinations = list.ToArray()
            };

            var response = await PostAsync("api/Data/AddManualDataSet", dataset);
            response.EnsureSuccessStatusCode();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(expected);
        }
        [Fact, TestPriority(9)] 
        public async Task AddManualDataSetWithoutCatalogId_should_fail()
        {
            // Arrange
            string expected = "CatalogId cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Combinations> list = new List<Commands.DataModule.DataSets.Combinations>();
            Commands.DataModule.DataSets.Combinations combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Gender";
            combination.Values = new List<string>();
            combination.Values.Add("M");
            combination.Values.Add("F");
            list.Add(combination);

            combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Sector";
            combination.Values = new List<string>();
            combination.Values.Add("1000");
            combination.Values.Add("1001");
            combination.Values.Add("1002");
            list.Add(combination);

            var dataset = new AddManualDataSet
            {
                Title = "title test",
                ProjectId = Guid.NewGuid(),
                TenantId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.Empty,
                SystemId = Guid.NewGuid(),
                TypicalId = Guid.NewGuid(),
                Combinations = list.ToArray()
            };

            var response = await PostAsync("api/Data/AddManualDataSet", dataset);
            response.EnsureSuccessStatusCode();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(expected);
        }
        [Fact, TestPriority(9)]
        public async Task AddManualDataSetWithoutSystemId_should_fail()
        {
            // Arrange
            string expected = "SystemId cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Combinations> list = new List<Commands.DataModule.DataSets.Combinations>();
            Commands.DataModule.DataSets.Combinations combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Gender";
            combination.Values = new List<string>();
            combination.Values.Add("M");
            combination.Values.Add("F");
            list.Add(combination);

            combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Sector";
            combination.Values = new List<string>();
            combination.Values.Add("1000");
            combination.Values.Add("1001");
            combination.Values.Add("1002");
            list.Add(combination);

            var dataset = new AddManualDataSet
            {
                Title = "title test",
                TenantId = Guid.NewGuid(),
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.Empty,
                TypicalId = Guid.NewGuid(),
                TypicalName = "CUSTOMER",
                Combinations = list.ToArray()
            };

            var response = await PostAsync("api/Data/AddManualDataSet", dataset);
            response.EnsureSuccessStatusCode();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(expected);
        }
        [Fact, TestPriority(9)]
        public async Task AddManualDataSetWithoutTypicalId_should_fail()
        {
            // Arrange
            string expected = "TypicalId cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Combinations> list = new List<Commands.DataModule.DataSets.Combinations>();
            Commands.DataModule.DataSets.Combinations combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Gender";
            combination.Values = new List<string>();
            combination.Values.Add("M");
            combination.Values.Add("F");
            list.Add(combination);

            combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Sector";
            combination.Values = new List<string>();
            combination.Values.Add("1000");
            combination.Values.Add("1001");
            combination.Values.Add("1002");
            list.Add(combination);

            var dataset = new AddManualDataSet
            {
                Title = "title test",
                TenantId = Guid.NewGuid(),
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.NewGuid(),
                TypicalId = Guid.Empty,
                TypicalName = "CUSTOMER",
                Combinations = list.ToArray()
            };

            var response = await PostAsync("api/Data/AddManualDataSet", dataset);
            response.EnsureSuccessStatusCode();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(expected);
        }
        [Fact, TestPriority(9)]
        public async Task AddManualDataSetWithoutTitle_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty.";

            // Act
            List<Commands.DataModule.DataSets.Combinations> list = new List<Commands.DataModule.DataSets.Combinations>();
            Commands.DataModule.DataSets.Combinations combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Gender";
            combination.Values = new List<string>();
            combination.Values.Add("M");
            combination.Values.Add("F");
            list.Add(combination);

            combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Sector";
            combination.Values = new List<string>();
            combination.Values.Add("1000");
            combination.Values.Add("1001");
            combination.Values.Add("1002");
            list.Add(combination);

            var dataset = new AddManualDataSet
            {
                Title = null,
                TenantId = Guid.NewGuid(),
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.NewGuid(),
                TypicalId = Guid.NewGuid(),
                TypicalName = "CUSTOMER",
                Combinations = list.ToArray()
            };

            var response = await PostAsync("api/Data/AddManualDataSet", dataset);
            response.EnsureSuccessStatusCode();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(expected);
        }
        [Fact, TestPriority(1)] 
        public async Task UpdateManualDataSet_should_succes()
        {
            // Arrange

            // Act
            List<Commands.DataModule.DataSets.Combinations> list = new List<Commands.DataModule.DataSets.Combinations>();
            Commands.DataModule.DataSets.Combinations combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Gender";
            combination.Values = new List<string>();
            combination.Values.Add("M");
            combination.Values.Add("F");
            list.Add(combination);

            combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Sector";
            combination.Values = new List<string>();
            combination.Values.Add("1000");
            combination.Values.Add("1001");
            combination.Values.Add("1002");
            combination.Values.Add("1003");
            list.Add(combination);

            var dataset = new UpdateManualDataSet
            {
                Id = _manualDataSetId,
                Title = "Data set 2 from IT - Manula - update",
                TenantId = Guid.NewGuid(),
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                WorkflowId = Variables.WorkflowId,
                WorkflowPathId = Guid.NewGuid(),
                WorkflowPathItemId = Guid.NewGuid(),
                TypicalType = ((int)TypicalType.Application).ToString(),
                TypicalId = Variables.DataSetTypicalId,

                TypicalName = Variables.TypicalName,
                Combinations = list.ToArray(),
                UpdateStatus = UpdateStatus.NoAction,
               
            };

            var response = await PutAsync("api/Data/UpdateManualDataSet", dataset);
            response.EnsureSuccessStatusCode();
            var result = response.GetObject<UpdateManualDataSetResult>();

            // Assert
            Assert.True(result != null && result.Id != Guid.Empty);
        }

        [Fact, TestPriority(9)] 
        public async Task UpdateManualDataSet_should_fail()
        {
            // Arrange
            string excepted = "DataSetId cannot be empty.";
            // Act
            List<Commands.DataModule.DataSets.Combinations> list = new List<Commands.DataModule.DataSets.Combinations>();
            Commands.DataModule.DataSets.Combinations combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Gender";
            combination.Values = new List<string>();
            combination.Values.Add("M");
            combination.Values.Add("F");
            list.Add(combination);

            combination = new Commands.DataModule.DataSets.Combinations();
            combination.Attribute = "Sector";
            combination.Values = new List<string>();
            combination.Values.Add("1000");
            combination.Values.Add("1001");
            combination.Values.Add("1002");
            combination.Values.Add("1003");
            list.Add(combination);

            var dataset = new UpdateManualDataSet
            {
                Id = Guid.Empty,
                Title = null,
                SubProjectId = Guid.Empty,
                CatalogId = Guid.Empty,
                SystemId = Guid.Empty,
                TypicalId = Guid.Empty,
                Combinations = list.ToArray()
            };

            var response = await PutAsync("api/Data/UpdateManualDataSet", dataset);
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }

        [Fact, TestPriority(5)]
        public async Task GetSuggestedDataSets_should_success()
        {
            // Arrange
            int excepted = 0;
            // Act

            //var response = await GetAsync("api/Data/GetSuggestedDataSets?TenantId=" + TenantId + "&ProjectId=" + projectId + "&SubprojectId=" + subprojectId +
            //    "&CatalogId=" + catalogId + "&SystemId=" + systemId + "&TypicalName=" + typicalName+ "&TypicalId="+GetTypical(typicalName).Result.Id+"&WorkflowId="+workflowid +"&PathItemId="+pathitemid);
            var response = await GetAsync($"api/Data/GetSuggestedDataSets?TenantId={Variables.TenantId}&ProjectId={Variables.ProjectId}&SubprojectId={Variables.SubProjectId}&CatalogId={Variables.CatalogId}&SystemId={Variables.SystemId}&TypicalName={Variables.TypicalName}&TypicalId={Variables.TypicalId}&WorkflowId={Variables.WorkflowId}&PathItemId={Variables.DataSetPathItemId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSuggestedDataSetsResult>();

            // Assert
            Assert.True(actual.Count >= excepted);
        }
        [Fact, TestPriority(5)]
        public async Task GetSuggestedDataSetsWithoutRecords_should_fail()
        {
            // Arrange
            string excepted = "Workflow is not found.";
            // Act

            //var response = await GetAsync("api/Data/GetSuggestedDataSets?TenantId=" + TenantId + "&ProjectId=" + projectId + "&SubprojectId=" + subprojectId +
            //    "&CatalogId=" + catalogId + "&SystemId=" + systemId + "&TypicalName=" + typicalName+ "&TypicalId="+GetTypical(typicalName).Result.Id+"&WorkflowId="+workflowid +"&PathItemId="+pathitemid);
            var response = await GetAsync($"api/Data/GetSuggestedDataSets?TenantId={Variables.TenantId}&ProjectId={Variables.ProjectId}&SubprojectId={Variables.SubProjectId}&CatalogId={Variables.CatalogId}&SystemId={Variables.SystemId}&TypicalName={Variables.TypicalName}&TypicalId={Variables.TypicalId}&WorkflowId={Guid.NewGuid()}&PathItemId={Variables.DataSetPathItemId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSuggestedDataSetsResult>();

            // Assert
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }

        [Fact, TestPriority(4)]
        public async Task DeleteDataSetItem_should_success()
        { 
            // Arrange
            //var id = "dbf08e7b-846c-4214-b056-006e80f139d1";
            // Act 
            var response = await DeleteAsync($"api/Data/DeleteDataSetItem?Id={Variables.DataSetItemId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<DeleteDataSetItemResult>();
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            // Assert
            Assert.True(response != null);
            Assert.True(actual != null && actual.Id != Guid.Empty);
        }

        [Fact, TestPriority(9)] 
        public async Task DeleteDataSetItem_should_fail()
        {
            // Arrange
            string excepted = "Data set itemId cannot be empty.";
            // Act

            var response = await DeleteAsync("api/Data/DeleteDataSetItem?Id=" + Guid.Empty);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        
        [Fact, TestPriority(9)]
        public async Task UpdateDataSetItem_should_fail()
        {
            string excepted = "Data set itemId cannot be empty.";
            Commands.DataModule.DataSets.Rows row = new Commands.DataModule.DataSets.Rows();
            List<Attributes> attributes = new List<Attributes>();
            Attributes attribute = new Attributes("Name", "Test test");
            attributes.Add(attribute);

            attribute = new Attributes("Age", "10");
            attributes.Add(attribute);
            row.Attributes = new List<Attributes>();
            row.Attributes.AddRange(attributes);

            var updateDataSetItem = new UpdateDataSetItem()
            {
                Id = Guid.Empty,
                DataSetId = Guid.Empty,
                Row = row
            };
            // Act

            var response = await PutAsync("api/Data/UpdateDataSetItem", updateDataSetItem);
            response.EnsureSuccessStatusCode();
            var actual = response.Content.ReadAsStringAsync().Result;
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            //Assert
            actual.Should().Contain(excepted);
        }
        [Fact, TestPriority(1)]
        public async Task GetDataSets_should_success()
        {
            // Arrange
            int excepted = 1;

            // Act
            var response = await GetAsync("api/Data/GetDataSets?SubprojectId=" + Variables.SubProjectId +
                "&CatalogId=" + Variables.CatalogId + "&SystemId=" + Variables.SystemId + "&TypicalName=" + Variables.TypicalName +"&TypicalId=" + Variables.TypicalId);
            var actual = response.GetCollection<GetDataSetsResult>();

            // Assert
            Assert.True(actual.Count >= excepted);
        }
        [Fact,  TestPriority(9)] 
        public async Task GetDataSets_should_fail()
        {
            // Arrange
            string excepted = "SubProjectId cannot be empty.";

            // Act
            var response = await GetAsync("api/Data/GetDataSets?SubprojectId=" + Guid.Empty +
                "&CatalogId=" + Guid.Empty + "&SystemId=" + Guid.Empty + "&TypicalName=" + Variables.TypicalName + "&TypicalId=" + Variables.TypicalId);
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetDataSetsResult>();

            // Assert 
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(9)]
        public async Task GetDataSetsWithoutRecords_should_fail()
        {
            // Arrange
            string excepted = "No Data Sets found.";

            // Act
            var response = await GetAsync("api/Data/GetDataSets?SubprojectId=" + Guid.NewGuid() +
                "&CatalogId=" + Guid.NewGuid() + "&SystemId=" + Guid.NewGuid() + "&TypicalName=" + Variables.TypicalName + "&TypicalId=" + Variables.TypicalId);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetDataSetsResult>();

            // Assert 
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(1)] 
        public async Task GetDataSet_should_success()
        {
            // Arrange 
            // Act
            var response = await GetAsync("api/Data/GetDataSet?Id=" + _dataSetId +
                "&Mandatory=" + 1 );
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetDataSetsResult>();

            // Assert
            Assert.True(actual.Id != null);
        }
        [Fact, TestPriority(9)] 
        public async Task GetDataSet_should_fail()
        {
            // Arrange
            string excepted = "DataSetId cannot be empty.";

            // Act
            var response = await GetAsync("api/Data/GetDataSet?Id=" + Guid.Empty +
                "&Mandatory=" + 0);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetDataSetsResult>();

            // Assert
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(9)]
        public async Task GetDataSetWithoutRecords_should_fail()
        {
            // Arrange
            string excepted = "No Data Set found.";

            // Act
            var response = await GetAsync("api/Data/GetDataSet?Id=" + Guid.NewGuid() +
                "&Mandatory=" + 0);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetDataSetsResult>();

            // Assert
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }

        [Fact, TestPriority(6)] 
        public async Task GetDataSetItems_should_success()
        {
            // Arrange
            int excepted = 1;

            // Act

            List<Guid> list = new List<Guid>();
            //list.Add(_dataSetId);
            list.Add(Variables.DatasetId);
            GetDataSetItems getDataSetItem = new GetDataSetItems();
            getDataSetItem.DataSetIds = list.ToArray();

            var response = await PostAsync("api/Data/GetDataSetItems", getDataSetItem);
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetDataSetItemsResult>();

            // Assert
            Assert.True(actual.Count >= excepted);
        }
        [Fact, TestPriority(9)]

        public async Task GetDataSetItems_should_fail()
        {
            // Arrange
            string excepted = "DataSetIds cannot be empty.";

            // Act

            List<Guid> list = new List<Guid>();
            list.Add(Guid.Empty);
            list.Add(Guid.Empty);
            GetDataSetItems getDataSetItem = new GetDataSetItems();
            getDataSetItem.DataSetIds = list.ToArray();

            var response = await PostAsync("api/Data/GetDataSetItems", getDataSetItem);
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetDataSetItemsResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(9)] 
        public async Task GetDataSetItemsWithoutRecords_should_fail()
        {
            // Arrange
            string excepted = "No Data-Set-Items found.";

            // Act

            List<Guid> list = new List<Guid>();
            list.Add(Guid.NewGuid());
            list.Add(Guid.NewGuid());
            GetDataSetItems getDataSetItem = new GetDataSetItems();
            getDataSetItem.DataSetIds = list.ToArray();

            var response = await PostAsync("api/Data/GetDataSetItems", getDataSetItem);
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetDataSetItemsResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(6)] 
        public async Task GetDataSetItem_should_success()
        {
            // Arrange
            int excepted = 1;
            // Act

            var response = await GetAsync("api/Data/GetDataSetItem?DataSetId=" + Variables.DatasetId);
            response.EnsureSuccessStatusCode();
            var actual =response.GetObject<GetDataSetItemResult>().Rows;

            // Assert
            Assert.True(actual.Length >= excepted);
        }
        [Fact, TestPriority(9)]
     
        public async Task GetDataSetItem_should_fail()
        {
            // Arrange
         
            string excepted = "DataSetId cannot be empty.";
            // Act

            var response = await GetAsync("api/Data/GetDataSetItem?DataSetId=" + Guid.Empty);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetDataSetItemResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(9)] 
        public async Task GetDataSetItemWithNoRecords_should_fail()
        {
            // Arrange

            string excepted = "No Data-Set-Items found.";
            // Act

            var response = await GetAsync("api/Data/GetDataSetItem?DataSetId=" + Guid.NewGuid());
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetDataSetItemResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
       
        [Fact, TestPriority(5)]
        public async Task GetTypicalAttributes_should_success()
        {
            // Arrange
            int excepted = 0;
            // Act

            //var response = await GetAsync("api/Data/GetSuggestedDataSets?TenantId=" + TenantId + "&ProjectId=" + projectId + "&SubprojectId=" + subprojectId +
            //    "&CatalogId=" + catalogId + "&SystemId=" + systemId + "&TypicalName=" + typicalName+ "&TypicalId="+GetTypical(typicalName).Result.Id+"&WorkflowId="+workflowid +"&PathItemId="+pathitemid);
            var response = await GetAsync($"api/Data/GetTypicalAttributes?TypicalId={Variables.TypicalId}&Mandatory=2");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<Queries.DataModule.DataSets.GetTypicalAttributesResult>();

            //// Assert
            Assert.True(actual.Id != Guid.Empty);
        }
        [Fact, TestPriority(9)]
        public async Task GetTypicalAttributesWithoutRecords_should_success()
        {
            // Arrange
            string excepted = "No Typical found.";
            // Act

            //var response = await GetAsync("api/Data/GetSuggestedDataSets?TenantId=" + TenantId + "&ProjectId=" + projectId + "&SubprojectId=" + subprojectId +
            //    "&CatalogId=" + catalogId + "&SystemId=" + systemId + "&TypicalName=" + typicalName+ "&TypicalId="+GetTypical(typicalName).Result.Id+"&WorkflowId="+workflowid +"&PathItemId="+pathitemid);
            var response = await GetAsync($"api/Data/GetTypicalAttributes?TypicalId={Guid.NewGuid()}&Mandatory=1");
            response.EnsureSuccessStatusCode();
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);

            //// Assert 
        }

    }
}