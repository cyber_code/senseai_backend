﻿using Application.Services;
using FluentAssertions;
using Messaging.Queries;
using Queries.ProcessModule.Sprints;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class SprintControllerStatistics : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public SprintControllerStatistics(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }


        [Fact, TestPriority(0)]
        public async Task GetSprintStatistics_should_succeed()
        {
            // Arrange
            int nrExpectedWorkflows = 1;
            int nrExpectedOpenIssues = 0;
            int nrExpectedTotalIssues = 1;
            int nrExpectedTotalTestCases = 1;

            // Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintStatistics?SprintId={Variables.SprintId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSprintStatisticsResult>()[0];

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(nrExpectedWorkflows, actual.NrWorkflows);
            Assert.Equal(nrExpectedOpenIssues, actual.NrOpenIssues);
            Assert.Equal(nrExpectedTotalIssues, actual.NrTotalIssues);
            Assert.Equal(nrExpectedTotalTestCases, actual.NrTestCases);
        }

        [Fact, TestPriority(1)]
        public async Task GetSprintStatistics_should_fail()
        {
            // Arrange
            string expected = "SprintId cannot be empty.";

            // Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintStatistics?SprintId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(1)]
        public async Task GetSprintStatistics_empty_result_should_fail()
        {
            // Arrange
            string expected = "No process items found for sprint.";

            // Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintStatistics?SprintId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
