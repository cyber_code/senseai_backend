﻿using Commands.ProcessModule.Issues;
using FluentAssertions;
using Queries.ProcessModule.Issues;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;
using Application.Services;
using Messaging.Commands;
using Commands.ProcessModule.Processes;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerIssuesTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _issueId;

        public BrowseControllerIssuesTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddIssue_should_success()
        {
            //Arrange
            AddIssue addIssue = new AddIssue
            {
                ProcessId = Variables.ProcessId,
                SubProjectId = Variables.SubProjectId,
                IssueTypeId = Variables.IssueTypeId,
                AssignedId = Guid.NewGuid(),
                Title = "Title of issue 1",
                Description = "Issue description",
                Status = SenseAI.Domain.StatusType.ToDo,
                Estimate = 0
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssue", addIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddIssueResult>();
            _issueId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateIssue_should_success()
        {
            //Arrange
            bool expected = true;
            UpdateIssue updateIssue = new UpdateIssue
            {
                Id = _issueId,
                ProcessId = Variables.ProcessId,
                SubProjectId = Variables.SubProjectId,
                IssueTypeId = Variables.IssueTypeId,
                AssignedId = Guid.NewGuid(),
                Title = "Title of issue 1 update",
                Description = "Issue description update",
                Status = SenseAI.Domain.StatusType.ToDo,
                Estimate = 0
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateIssue", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(1)]
        public async Task UnlinkIssue_should_success()
        {
            //Arrange
            bool expected = true;
            UnlinkIssueFromProcess updateIssue = new UnlinkIssueFromProcess
            {
                IssueId = _issueId,
                
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UnlinkIssueFromProcess", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(1)]
        public async Task LinkIssueToProcess_should_success()
        {
            //Arrange
            bool expected = true;
            LinkIssueToProcess updateIssue = new LinkIssueToProcess
            {
                IssueId = _issueId,
                ProcessId=Variables.ProcessId

            };

            //Act
            var response = await PutAsync("api/Process/Browse/LinkIssueToProcess", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(2)]
        public async Task GetIssues_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssues?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssuesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(2)]
        public async Task GetIssuesWithDetails_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssuesWithDetails?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssuesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(2)]
        public async Task GetIssuesByIssueType_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssuesByIssueType?SubProjectId={Variables.SubProjectId}&IssueTypeId={ Variables.IssueTypeId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssuesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(2)]
        public async Task GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType?SubProjectId={Variables.SubProjectId}&IssueTypeId={ Variables.IssueTypeId}&ProcessId={Variables.ProcessId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssuesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(2)]
        public async Task GetIssue_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssue?Id={_issueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetIssueTypeResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteIssue_should_sucess()
        {
            //Arrange
            bool expected = true;

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteIssue?Id={_issueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }


        [Fact, TestPriority(4)]
        public async Task GetIssues_after_delete_should_success()
        {
            //Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssues?ProcessId={Variables.ProcessId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssuesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetIssuesWithDetails_after_delete_should_success()
        {
            //Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssuesWithDetails?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssuesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task AddIssue_should_fail()
        {
            //Arrange
            string expected = "subprojectid cannot be empty.";
            var issue = new AddIssue
            {
                ProcessId = Guid.Empty,
                SubProjectId = Guid.Empty,
                IssueTypeId = Guid.Empty,
                AssignedId = Guid.Empty,
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssue", issue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddIssue_empty_subProjectId_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";
            var issue = new AddIssue
            {
                ProcessId = Guid.NewGuid(),
                SubProjectId = Guid.Empty,
                IssueTypeId = Guid.Empty,
                AssignedId = Guid.Empty,
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssue", issue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddIssue_empty_issueTypeId_should_fail()
        {
            //Arrange
            string expected = "IssueTypeId cannot be empty.";
            var issue = new AddIssue
            {
                ProcessId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                IssueTypeId = Guid.Empty,
                AssignedId = Guid.Empty,
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssue", issue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddIssue_empty_assignedId_should_fail()
        {
            //Arrange
            string expected = "assignedId cannot be empty.";
            var issue = new AddIssue
            {
                ProcessId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                IssueTypeId = Guid.NewGuid(),
                AssignedId = Guid.Empty,
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssue", issue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }


        [Fact, TestPriority(5)]
        public async Task AddIssue_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            var issue = new AddIssue
            {
                ProcessId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                IssueTypeId = Guid.NewGuid(),
                AssignedId = Guid.NewGuid(),
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssue", issue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateIssue_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            UpdateIssue updateIssue = new UpdateIssue
            {
                Id = Guid.Empty,
                ProcessId = Guid.Empty,
                SubProjectId = Guid.Empty,
                IssueTypeId = Guid.Empty,
                AssignedId = Guid.Empty,
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateIssue", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(5)]
        public async Task UnlinkIssue_should_fail()
        {
            //Arrange
            string expected = "issueid cannot be empty.";
            UnlinkIssueFromProcess updateIssue = new UnlinkIssueFromProcess
            {
                IssueId = Guid.Empty,
               
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UnlinkIssueFromProcess", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(5)]
        public async Task LinkIssueToProcess_should_fail()
        {
            //Arrange
            string expected = "issueid cannot be empty.";
            LinkIssueToProcess updateIssue = new LinkIssueToProcess
            {
                IssueId = Guid.Empty,

            };

            //Act
            var response = await PutAsync("api/Process/Browse/LinkIssueToProcess", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(5)]
        public async Task LinkIssueToProcess_with_empty_process_should_fail()
        {
            //Arrange
            string expected = "processid cannot be empty.";
            LinkIssueToProcess updateIssue = new LinkIssueToProcess
            {
                IssueId = _issueId,
                ProcessId=Guid.Empty
            };

            //Act
            var response = await PutAsync("api/Process/Browse/LinkIssueToProcess", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(5)]
        public async Task UpdateIssue_empty_processId_should_fail()
        {
            //Arrange
            string expected = "subprojectid cannot be empty.";
            UpdateIssue updateIssue = new UpdateIssue
            {
                Id = Guid.NewGuid(),
                ProcessId = Guid.Empty,
                SubProjectId = Guid.Empty,
                IssueTypeId = Guid.Empty,
                AssignedId = Guid.Empty,
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateIssue", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateIssue_empty_subProjectId_should_fail()
        {
            //Arrange
            string expected = "subProjectId cannot be empty.";
            UpdateIssue updateIssue = new UpdateIssue
            {
                Id = Guid.NewGuid(),
                ProcessId = Guid.NewGuid(),
                SubProjectId = Guid.Empty,
                IssueTypeId = Guid.Empty,
                AssignedId = Guid.Empty,
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateIssue", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateIssue_empty_issueTypeId_should_fail()
        {
            //Arrange
            string expected = "issueTypeId cannot be empty.";
            UpdateIssue updateIssue = new UpdateIssue
            {
                Id = Guid.NewGuid(),
                ProcessId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                IssueTypeId = Guid.Empty,
                AssignedId = Guid.Empty,
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateIssue", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateIssue_empty_assignedId_should_fail()
        {
            //Arrange
            string expected = "assignedId cannot be empty.";
            UpdateIssue updateIssue = new UpdateIssue
            {
                Id = Guid.NewGuid(),
                ProcessId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                IssueTypeId = Guid.NewGuid(),
                AssignedId = Guid.Empty,
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateIssue", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact]
        public async Task GetIssuesForSprintPlannerWithFilters_should_success()
        {
            var hierarchy = Guid.NewGuid();
            var subProjectId = Guid.NewGuid();

            //Arrange
            var process = new AddProcess
            {
                Title = "Process Title",
                HierarchyId = hierarchy,
                SubProjectId = subProjectId,
                CreatedId = subProjectId,
                OwnerId = subProjectId,
                Description = "Process",
                ProcessType = SenseAI.Domain.ProcessType.Generic
            };

            //Act
            var processResponse = await PostAsync("api/Process/Browse/AddProcess", process);
            processResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            var createdProcess = processResponse.GetObject<AddProcessResult>();


            //Assert
            createdProcess.Should().NotBeNull();
            createdProcess.Id.Should().NotBe(Guid.Empty);

            //Arrange
            var issue = new AddIssue
            {
                ProcessId = createdProcess.Id,
                SubProjectId = subProjectId,
                IssueTypeId = Guid.NewGuid(),
                AssignedId = Guid.NewGuid(),
                Title = "Title of issue 1",
                Description = "Issue description",
                Status = SenseAI.Domain.StatusType.ToDo,
                Estimate = 0
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddIssue", issue);
            response.EnsureSuccessStatusCode();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var createdIssue = response.GetObject<AddIssueResult>();
            createdIssue.Id.Should().NotBe(Guid.Empty);

            var filters = new GetIssuesForSprintPlannerWithFilters
            {
                ProcessId = createdProcess.Id,
                SubProjectId = subProjectId,
                SearchText = "title",
                AssigneeIds =new Guid[0]

            };
            //Arrange
            var getIssuesResponse = await PostAsync("api/Process/Browse/GetIssuesForSprintPlannerWithFilters",filters);
            getIssuesResponse.EnsureSuccessStatusCode();
            var result = getIssuesResponse.GetObject<GetIssuesForSprintPlannerWithFiltersResult>();
      
            //Assert
            getIssuesResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            result.BacklogIssues.Length .Should().BeGreaterThan(0);
        }

        [Fact]
        public async Task GetIssuesForSprintPlannerWithFilters_should_fail()
        {
            //Arrange
            string excepted = "subprojectid cannot be empty.";
            var filters = new GetIssuesForSprintPlannerWithFilters
            {
                ProcessId = Guid.Empty,
                SubProjectId = Guid.Empty,
                SearchText = "title",
                AssigneeIds = new Guid[0]

            };
            //Act
            var response = await PostAsync("api/Process/Browse/GetIssuesForSprintPlannerWithFilters", filters);

            //Assert
            response.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateIssue_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            UpdateIssue updateIssue = new UpdateIssue
            {
                Id = Guid.NewGuid(),
                ProcessId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                IssueTypeId = Guid.NewGuid(),
                AssignedId = Guid.NewGuid(),
                Title = null,
                Description = "Description 1",
                Estimate = 0
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateIssue", updateIssue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeleteIssue_should_fail()
        {   
            //Arrange
            string expected = "Id cannot be empty!";

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteIssue?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssue_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssue?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssue_empty_result_should_fail()
        {
            //Arrange
            string expected = "No issue found.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssue?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssues_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssues?SubProjectId{Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(5)]
        public async Task GetIssuesWithDetails_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssuesWithDetails?SubProjectId{Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(5)]
        public async Task GetIssuesByIssueType_should_fail()
        {
            //Arrange
            string expected = "IssueTypeId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssuesByIssueType?SubProjectId={Variables.SubProjectId}&IssueTypeId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(5)]
        public async Task GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType_should_fail()
        {
            //Arrange
            string expected = "ProcessId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType?SubProjectId={Variables.SubProjectId}&IssueTypeId={Variables.IssueTypeId}&ProcessId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(5)]
        public async Task GetIssuesWithDetails_empty_subProjectId_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssuesWithDetails?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(5)]
        public async Task GetIssues_empty_subProjectId_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssues?ProcessId={Guid.NewGuid()}&SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetIssues_empty_result_should_fail()
        {
            //Arrange
            string expected = "No issues were found in process.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssues?ProcessId={Guid.NewGuid()}&SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        [Fact, TestPriority(5)]
        public async Task GetIssuesWithDetails_empty_result_should_fail()
        {
            //Arrange
            string expected = "No issues were found in subproject.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssuesWithDetails?SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
