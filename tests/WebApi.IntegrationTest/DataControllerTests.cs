﻿using FluentAssertions;
using FluentAssertions.Json;
using Newtonsoft.Json.Linq;
using Commands.DataModule.TestCases;
using Messaging.Queries;
using Queries.AdministrationModule.Projects;
using Queries.DataModule.Data;
using Queries.DataModule.Visualization;
using Queries.DesignModule;
using Queries.TestGenerationModule;
using WebApi.IntegrationTest.Infrastructure;
using WebApi.IntegrationTest.Resources;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class DataControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public DataControllerTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }


        [Fact, TestPriority(1)]
        public async Task Get_all_workflow_paths_with_success()
        {
 //           var workflowId = "9e90b265-e7b3-4dbb-8e81-089d71cf7443";
 //           var response = await GetAsync("api/Data/GetWorkflowPaths?WorkflowId=" + workflowId.ToString());
 //response.StatusCode.Should().Be(HttpStatusCode.OK);
 //           response.GetObjectAsync<GetWorkflowPathsResult>().Should().NotBeNull();

 //           var workflowpaths = response.GetCollection<GetWorkflowPathsResult>();
 //           workflowpaths.Count.Should().BeGreaterThan(0);
        }
        [Fact, TestPriority(1)]
        public async Task Get_all_workflow_paths_should_Fail()
        {
            string excepted = "WorkflowId cannot be empty.";

            var response = await GetAsync("api/Data/GetWorkflowPaths?WorkflowId=" + Guid.Empty);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(1)]
        public async Task Get_all_workflow_path_items_with_success()
        {

            //var workflowpathId = "6519fd8d-6016-44d2-9a15-0660a75b9fbc";
            //var response = await GetAsync("api/Data/GetWorkflowPathItems?WorkflowPathId=" + workflowpathId);

            ////response.StatusCode.Should().Be(HttpStatusCode.OK);
            ////response.GetObjectAsync<GetWorkflowPathItemsResult>().Should().NotBeNull();

            //var workflowpathitems = response.GetCollection<GetWorkflowPathItemsResult>();
            //workflowpathitems.Count.Should().BeGreaterThan(0);
        }
        [Fact, TestPriority(1)]
        public async Task Get_all_workflow_path_items_should_fail()
        {
            string excepted = "WorkflowPathId cannot be empty.";

            var response = await GetAsync("api/Data/GetWorkflowPathItems?WorkflowPathId=" + Guid.Empty);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }

        [Fact, TestPriority(1)]
        public async Task Get_all_workflow_path_items_ids_with_success()
        {
            //var pathid = "6519fd8d-6016-44d2-9a15-0660a75b9fbc";
            //var response = await GetAsync("api/Data/GetWorkflowPathItemIds?WorkflowPathId=" + pathid);

            ////response.StatusCode.Should().Be(HttpStatusCode.OK);
            ////response.GetObjectAsync<QueryResponse<Guid[]>>().Should().NotBeNull();

            //var workflowPathItemIds = response.GetCollection<Guid>();
            //workflowPathItemIds.Count.Should().BeGreaterThan(0);
        }
        [Fact, TestPriority(1)]
        public async Task Get_all_workflow_path_items_ids_should_fail()
        {
            string excepted = "WorkflowPathId cannot be empty.";
            var response = await GetAsync("api/Data/GetWorkflowPathItemIds?WorkflowPathId=" + Guid.Empty);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(1)]
        public async Task Test_case_generation()
        {
            Guid wfid = Guid.Parse("9e90b265-e7b3-4dbb-8e81-089d71cf7443");
            //Guid wfpid = Guid.Parse("6519fd8d-6016-44d2-9a15-0660a75b9fbc");
            //string datasetsJson = "";
            //using (var expectedReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.Data.DataSets.json"))
            //{
            //    datasetsJson = expectedReader.ReadToEnd();
            //}
            //DataSets[] dataSetsIds = 
            //    Newtonsoft.Json.JsonConvert.DeserializeObject<GenerateTestCases>(datasetsJson).DataSetsIds;
            //GenerateTestCases generateTestCases = new GenerateTestCases
            //{
            //    WorkflowId = wfid,
            //    WorkflowPathId = wfpid,
            //    DataSetsIds = dataSetsIds
            //};
            //var response = await PostAsync("api/Data/GenerateTestCases", generateTestCases);

            //var result = response.GetObject<GenerateTestCasesResult>();
            //result.Should().NotBeNull();
        }

        [Fact, TestPriority(1)]
        public async Task Test_case_generation_should_fail()
        {
            string excepted = "WorkflowId cannot be empty.";

            GenerateTestCases generateTestCases = new GenerateTestCases
            {
                TenantId = Guid.NewGuid(),
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.NewGuid(),
                WorkflowId = Guid.Empty,
                WorkflowPathId = Guid.Empty,
               
            };
            var response = await PostAsync("api/Data/GenerateTestCases", generateTestCases);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }

        private async Task<GetProjectsResult> GetProjects()
        {
            var response = await GetAsync("api/Administration/GetProjects");
            return response.GetCollection<GetProjectsResult>().FirstOrDefault();
        }

        private async Task<GetSubProjectsResult> GetSubProjects()
        {
            var project = GetProjects().Result;
            var response = await GetAsync("api/Administration/GetSubProjects?ProjectId=" + project.Id);
            return response.GetCollection<GetSubProjectsResult>().FirstOrDefault();
        }

        private async Task<GetFoldersResult> GetFolder()
        {
            var subProject = GetSubProjects().Result;
            var response = await GetAsync("api/Design/GetFolders?SubProjectId=" + subProject.Id);
            return response.GetCollection<GetFoldersResult>().LastOrDefault();
        }

        private async Task<GetWorkflowsResult> GetWorkflow()
        {
            var folder = GetFolder().Result;
            var response = await GetAsync("api/Design/GetWorkflows?FolderId=" + folder.Id);

            return response.GetCollection<GetWorkflowsResult>().FirstOrDefault();
        }

        [Fact, TestPriority(1)]
        private async Task GetModelVisualization()
        {
            GetModelVisualization getVisualization = new GetModelVisualization
            {
                TypicalName = "CUSTOMER",
                Level = 1
            };
            var response = await GetAsync("api/Data/GetModelVisualization?TypicalName=" + getVisualization.TypicalName + "&Level=" + getVisualization.Level.ToString());
            var result = await response.GetObjectAsync<GetModelVisualizationResult>();

            result.Should().NotBeNull();
            result.Items.Should().NotBeNull();
            result.Links.Should().NotBeNull();
        }
        [Fact, TestPriority(1)]
        private async Task GetModelVisualization_should_fail()
        {
            string excepted = "typical name cannot be empty.";
            GetModelVisualization getVisualization = new GetModelVisualization
            {
                TypicalName = null,
                Level = 1
            };
            var response = await GetAsync("api/Data/GetModelVisualization?TypicalName=" + getVisualization.TypicalName + "&Level=" + getVisualization.Level.ToString());
            //response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted);
        }
        [Fact, TestPriority(2)]
        public async Task Get_testSuite_with_success()
        {
            var workflowId = "9e90b265-e7b3-4dbb-8e81-089d71cf7442";
            var pathid = "6519fd8d-6016-44d2-9a15-0660a75b9fba";
            var responsets = await GetAsync("api/Data/GetTestSuite?WorkflowId=" + workflowId + "&WorkflowPathId="+pathid);

            responsets.StatusCode.Should().Be(HttpStatusCode.OK);
            responsets.GetObjectAsync<GetTestSuiteResult>().Should().NotBeNull();
        }
        [Fact, TestPriority(1)]
        public async Task Get_testsuite_should_Fail()
        {
            string excepted = "WorkflowId cannot be empty.";

            var response = await GetAsync("api/Data/GetTestSuite?WorkflowId=" +Guid.Empty + "&WorkflowPathId=" + Guid.Empty);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
    }
}