﻿using FluentAssertions;
using Commands.AdministrationModule.Projects;
using Queries.AdministrationModule.Projects;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Messaging.Commands;
using Messaging.Queries;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class AdministrationControllerProjectsTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _projectId;

        public AdministrationControllerProjectsTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddProject_should_success()
        {
            //Arrange
            var project = new AddProject
            {
                Title = "title 1",
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddProject", project);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddProjectResult>();
            _projectId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateProject_should_success()
        {
            //Arrange
            bool expected = true;
            var updateProject = new UpdateProject
            {
                Id = _projectId,
                Title = "Title updated",
                Description = "Description updated"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateProject", updateProject);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetProjects_should_success()
        {
            //Arrange
            int expected = 3;

            //Act
            var response = await GetAsync("api/Administration/GetProjects");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetProjectsResult>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteProject_should_success()
        {
            //Arrange
            bool expected = true;

            //Act
            var response = await DeleteAsync($"api/Administration/DeleteProject?Id={_projectId}");
            response.EnsureSuccessStatusCode();
            bool actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(10)]
        public async Task DeleteProjectAndEverythingUnderProject_should_success()
        {
            // Arrange
            //Add Project with SubProject in order to test the delete with everything under project
            var project = new AddProject
            {
                Title = "title 1",
                Description = "Description 1"
            };

            //Act
            var responseProject = await PostAsync("api/Administration/AddProject", project);
            responseProject.EnsureSuccessStatusCode();
            var actualProject = responseProject.GetObject<AddProjectResult>();

            var subProject = new AddSubProject
            {
                ProjectId = actualProject.Id,
                Title = "title 1",
                Description = "Description 1"
            };

            //Act
            var responseSubProject = await PostAsync("api/Administration/AddSubProject", subProject);
            responseSubProject.EnsureSuccessStatusCode();
            var actualSubproject = responseSubProject.GetObject<AddSubProjectResult>();


            //Arrange
            //Delete Project and subproject
            bool expected = true;

            //Act
            var response = await DeleteAsync($"api/Administration/DeleteProject?Id={actualProject.Id}");
            response.EnsureSuccessStatusCode();
            bool actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(4)]
        public async Task GetProjects_after_delete_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync("api/Administration/GetProjects");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetProjectsResult>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetProject_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Administration/GetProject?Id={Variables.ProjectId}");
            response.EnsureSuccessStatusCode();
            var getProject = response.GetObject<GetProjectResult>();

            //Assert
            getProject.Should().NotBeNull();
            getProject.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(5)]
        public async Task AddProject_should_fail()
        {
            //Arrange
            string expected = "title cannot be empty.";
            var project = new AddProject
            {
                Title = null,
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddProject", project);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateProject_should_fail()
        {
            //Arrange
            string expected = "projectId cannot be empty.";
            UpdateProject updateProject = new UpdateProject
            {
                Id = Guid.Empty,
                Title = null,
                Description = "test test"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateProject", updateProject);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateProject_empty_title_should_fail()
        {
            //Arrange
            string expected = "title cannot be empty.";
            UpdateProject updateProject = new UpdateProject
            {
                Id = Guid.NewGuid(),
                Title = null,
                Description = "test test"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateProject", updateProject);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeleteProject_should_fail()
        {
            //Arrange
            string expected = "projectId cannot be empty.";

            //Act
            var response = await DeleteAsync($"api/Administration/DeleteProject?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetProject_should_fail()
        {
            //Arrange
            string expected = "projectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Administration/GetProject?Id={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetProject_empty_result_should_fail()
        {
            //Arrange
            var id = Guid.NewGuid();
            string expected = "No Project found.";

            //Act
            var response = await GetAsync($"api/Administration/GetProject?Id={id}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}