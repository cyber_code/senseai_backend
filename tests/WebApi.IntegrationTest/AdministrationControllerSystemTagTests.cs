﻿using System;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using Commands.AdministrationModule.SystemTags;
using Queries.AdministrationModule.SystemTags;
using WebApi.IntegrationTest.Infrastructure;
using Messaging.Queries;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class AdministrationControllerSystemTagTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _systemTagId;

        private static readonly Random random = new Random();

        public AdministrationControllerSystemTagTests(TestServerFixture fixture) : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddSystemTag_should_success()
        {
            // Arrange 
            AddSystemTag systemTag = new AddSystemTag
            {
                SystemId = Variables.SystemId,
                Title = GenerateRandomString(10),
                Description = GenerateRandomString(20)
            };

            //Act
            var response = await PostAsync($"api/Administration/AddSystemTag", systemTag);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddSystemTagResult>();
            _systemTagId = actual.Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        } 

        [Fact, TestPriority(1)]
        public async Task UpdateSystemTag_should_success()
        {
            // Arrange
            bool expected = true;
            UpdateSystemTag updateSystemTag = new UpdateSystemTag()
            {
                Id = _systemTagId,
                SystemId = Variables.SystemId,
                Title = GenerateRandomString(10),
                Description = GenerateRandomString(20)
            };

            // Act
            var response = await PutAsync($"api/Administration/UpdateSystemTag", updateSystemTag);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetSystemTags_should_success()
        {
            //Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Administration/GetSystemTags?SystemId={Variables.SystemId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSystemTagsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(2)]
        public async Task GetSystemTag_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Administration/GetSystemTag?Id={_systemTagId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetSystemTagResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteSystemTag_should_success()
        {
            // Arrange
            bool expected = true;

            // Act 
            var response = await DeleteAsync($"api/Administration/DeleteSystemTag?Id={_systemTagId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetSystemTags_after_delete_should_success()
        {
            //Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Administration/GetSystemTags?SystemId={Variables.SystemId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSystemTagsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(5)]
        public async Task AddSystemTag_should_fail()
        {
            // Arrange
            string expected = "SystemId cannot be empty!";

            AddSystemTag systemTag = new AddSystemTag
            {
                SystemId = Guid.Empty,
                Title = GenerateRandomString(10),
                Description = GenerateRandomString(20)
            };

            var response = await PostAsync("api/Administration/AddSystemTag", systemTag);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddSystemTag_should_title_empty_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";

            AddSystemTag systemTag = new AddSystemTag
            {
                SystemId = Guid.NewGuid(),
                Description = GenerateRandomString(20)
            };

            var response = await PostAsync("api/Administration/AddSystemTag", systemTag);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSystemTag_should_fail()
        {
            string expected = "Id cannot be empty!";
            var updateSystemTag = new UpdateSystemTag()
            {
                Id = Guid.Empty,
                SystemId = Guid.Empty,
                Title = string.Empty,
                Description = string.Empty
            };

            // Act
            var response = await PutAsync($"api/Administration/UpdateSystemTag", updateSystemTag);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSystemTag_empty_systemId_should_fail()
        {
            string expected = "SystemId cannot be empty!";
            var updateSystemTag = new UpdateSystemTag()
            {
                Id = Guid.NewGuid(),
                SystemId = Guid.Empty,
                Title = string.Empty,
                Description = string.Empty
            };

            // Act
            var response = await PutAsync($"api/Administration/UpdateSystemTag", updateSystemTag);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSystemTag_empty_title_should_fail()
        {
            string expected = "Title cannot be empty!";
            var updateSystemTag = new UpdateSystemTag()
            {
                Id = Guid.NewGuid(),
                SystemId = Guid.NewGuid(),
                Title = string.Empty,
                Description = string.Empty
            };

            // Act
            var response = await PutAsync($"api/Administration/UpdateSystemTag", updateSystemTag);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeleteSystemTag_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty!";

            // Act
            var response = await DeleteAsync("api/Administration/DeleteSystemTag?Id = " + Guid.Empty.ToString());
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSystemTags_should_fail()
        {
            //Arrange
            string expected = "SystemId cannot be empty!";

            //Act
            var response = await GetAsync($"api/Administration/GetSystemTags?SystemId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSystemTags_empty_result_should_fail()
        {
            //Arrange
            string expected = "No SystemTags found.";

            //Act
            var response = await GetAsync($"api/Administration/GetSystemTags?SystemId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSystemTag_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty!";

            //Act
            var response = await GetAsync($"api/Administration/GetSystemTag?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSystemTag_empty_result_should_fail()
        {
            //Arrange
            string expected = "No SystemTag found.";

            //Act
            var response = await GetAsync($"api/Administration/GetSystemTag?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        public static string GenerateRandomString(int strLength)
        {
            const string alphanumericChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            return new string(Enumerable.Repeat(alphanumericChar, strLength)
              .Select(str => str[random.Next(str.Length)])
              .ToArray());
        }
    }
}