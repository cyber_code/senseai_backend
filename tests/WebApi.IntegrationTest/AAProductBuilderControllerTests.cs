﻿using FluentAssertions;
using Commands.AAProductBuilderModule;
using Queries.AAProductBuilderModule;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit; 

namespace WebApi.IntegrationTest
{
    public class AAProductBuilderControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public AAProductBuilderControllerTests(TestServerFixture testServerFixture) : base(testServerFixture)
        {
            _fixture = testServerFixture;
        }

        //[Fact]
        //public async Task GetParameters()
        //{
        //    var response = await GetAsync("api/AAProductBuilder/GetParameters");

        //    response.StatusCode.Should().Be(HttpStatusCode.OK);
        //    response.GetObjectAsync<List<string>>().Should().NotBeNull();

        //    var parameters = response.GetCollection<string>();
        //    parameters.Count.Should().BeGreaterThan(0);
        //}

        //[Fact]
        //public async Task GetQueries()
        //{
        //    var response = await GetAsync("api/AAProductBuilder/GetQueries");

        //    response.StatusCode.Should().Be(HttpStatusCode.OK);
        //    response.GetObjectAsync<ListQueriesResult>().Should().NotBeNull();

        //    var products = response.GetCollection<ListQueriesResult>();
        //    products.Count.Should().BeGreaterThan(0);
        //}

        //[Fact]
        //public async Task CompleteSection()
        //{
        //    var productsResponse = await GetAsync("api/AAProductBuilder/GetProducts");
        //    var product = productsResponse.GetCollection<ListProductGroupsResult>().First();


        //    var request = new CompleteSection
        //    {
        //        SessionId = Guid.Parse("6F49C73E-EC09-41E9-833A-9998BD56C7D0"),
        //        ProductId = product.Id,
        //        SectionId = product.SectionIds.First()
        //    };

        //    var response = await PostAsync("api/AAProductBuilder/CompleteSection", request);
        //    response.StatusCode.Should().Be(HttpStatusCode.OK);
        //    response.GetObjectAsync<bool>().Result.Should().Be(true);
        //}

        //[Fact]
        //public async Task CompleteSectionWithUnexistingSessionId()
        //{
        //    var productsResponse = await GetAsync("api/AAProductBuilder/GetProducts");
        //    var product = productsResponse.GetCollection<GetProductsResult>().First();

        //    var request = new CompleteSection
        //    {
        //        SessionId = Guid.Parse("6F49C73E-EC09-41E9-833A-9998BD56C7D9"),
        //        ProductId = product.Id,
        //        SectionId = product.SectionIds.First()
        //    };

        //    var response = await PostAsync("api/AAProductBuilder/CompleteSection", request);
        //    response.StatusCode.Should().Be(HttpStatusCode.OK);
        //    response.GetObjectAsync<bool>().Result.Should().Be(false);
        //}

        //[Fact]
        //public async Task CompleteProduct()
        //{
        //    var productsResponse = await GetAsync("api/AAProductBuilder/GetProducts");
        //    var product = productsResponse.GetCollection<GetProductsResult>().First();

        //    var request = new CompleteProduct
        //    {
        //        SessionId = Guid.Parse("6F49C73E-EC09-41E9-833A-9998BD56C7D0"),
        //        ProductId = product.Id
        //    };

        //    var response = await PostAsync("api/AAProductBuilder/CompleteProduct", request);
        //    response.StatusCode.Should().Be(HttpStatusCode.OK);
        //    response.GetObjectAsync<bool>().Result.Should().Be(true);
        //}

        [Fact]
        public async Task ResetSession()
        {
            var request = new ResetSession
            {
                SessionId = Guid.Parse("6F49C73E-EC09-41E9-833A-9998BD56C7D0")
            };

            var response = await PostAsync("api/AAProductBuilder/ResetSession", request);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
           // response.GetObjectAsync<bool>().Result.Should().Be(true);
        }
    }
}
