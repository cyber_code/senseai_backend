﻿using Application.Commands.DesignModule.TestEngineModel;
using Application.Services;
using Commands.DataModule.TestEngine;
using Commands.DesignModule.WorkflowModel;
using Core.Configuration;
using FluentAssertions;
using Messaging.Commands;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using WebApi.IntegrationTest.Resources;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class Z_DesignControllerTestEngineTests : ControllerTestBase
    {
        private static Guid workflowId;
        public Z_DesignControllerTestEngineTests(TestServerFixture fixture)
            : base(fixture)
        {
        }

        [Fact, TestPriority(0)]
        public async Task SaveVideo_should_success()
        {
            //arrange
            workflowId = Variables.WorkflowId;

            using (var videoReader = EmbededResourceReader.GetStream("WebApi.IntegrationTest.Resources.Design.RecordedVideo.mp4"))
            using (var streamContent = new StreamContent(videoReader))
            {
                streamContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("video/mp4");

                var form = new MultipartFormDataContent
                {
                    { streamContent, "\"videoFile\"", "\"RecordedVideo.mp4\"" }
                };

                //act
                var response = await PostFileAsync($"api/Design/SaveVideo?WorkflowId={ workflowId }", form);
                response.EnsureSuccessStatusCode();
                var actual = response.GetObject<bool>();

                //Assert
                response.StatusCode.Should().Be(HttpStatusCode.OK);
                Assert.True(actual);
            }
        }

        [Fact, TestPriority(2)]
        public async Task DownloadVideo_should_success()
        {
            //arrange 

            using (var videoReader = EmbededResourceReader.GetStream("WebApi.IntegrationTest.Resources.Design.RecordedVideo.mp4"))
            using (var streamContent = new StreamContent(videoReader))
            {
                streamContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("video/mp4");

                var form = new MultipartFormDataContent
                {
                    { streamContent, "\"VideoFile\"", "\"RecordedVideo.mp4\"" }
                };

                //act
                var responsePost = await PostFileAsync($"api/Design/SaveVideo?WorkflowId={ workflowId }", form);
                responsePost.EnsureSuccessStatusCode();
                var actualPost = responsePost.GetObject<bool>();

                //Assert
                responsePost.StatusCode.Should().Be(HttpStatusCode.OK);
                Assert.True(actualPost);

                //act
                var responseGet = await GetAsync($"api/Design/DownloadVideo?WorkflowId={ workflowId }");

                //assert
                Assert.Equal(responseGet.Content.Headers.ContentLength, streamContent.Headers.ContentLength);
                Assert.Equal(responseGet.Content.Headers.ContentMD5, streamContent.Headers.ContentMD5);

            }
        }


        [Fact, TestPriority(1)]
        private async Task HasVideo_should_succeed()
        {
            // Arrange

            //Act
            var response = await GetAsync($"api/Design/HasVideo?workflowId={workflowId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<HasVideoModelResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.HasVideo.Should().BeTrue();
        }

        
        [Fact, TestPriority(3)]
        public async Task SaveRecordedWorkflow_should_succeed()
        {
            // Arrange
            bool expected = true;

            string json = "";
            using (var expectedReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.TestEngine.TestEngine.json"))
            {
                json = expectedReader.ReadToEnd();
            }
            SaveRecordedWorkflow saveRecordedWorkflow = Newtonsoft.Json.JsonConvert.DeserializeObject<SaveRecordedWorkflow>(json);
            saveRecordedWorkflow.WorkflowId = Variables.WorkflowId;
            saveRecordedWorkflow.Header = new Header
            {
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                FolderId = Variables.FolderId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                TenantId = Guid.NewGuid()
            };
            saveRecordedWorkflow.DataSet = null;

            //Act
            var response = await PostAsync("api/Design/SaveRecordedWorkflow", saveRecordedWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            Assert.Equal(expected, actual);
        }
        

        [Fact, TestPriority(5)]
        public async Task SaveRecordedWorkflow_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";
            SaveRecordedWorkflow saveRecordedWorkflow = new SaveRecordedWorkflow
            {
                Title = "test"
            };

            //Act
            var response = await PostAsync("api/Design/SaveRecordedWorkflow", saveRecordedWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task SaveRecordedWorkflow_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty.";
            SaveRecordedWorkflow saveRecordedWorkflow = new SaveRecordedWorkflow
            {
                WorkflowId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Design/SaveRecordedWorkflow", saveRecordedWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task SaveRecordedWorkflow_empty_workflow_items_should_fail()
        {
            // Arrange
            string expected = "No workflow items attached to workflow.";

            string json = "";
            using (var expectedReader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.TestEngine.TestEngine.json"))
            {
                json = expectedReader.ReadToEnd();
            }
            SaveRecordedWorkflow saveRecordedWorkflow = Newtonsoft.Json.JsonConvert.DeserializeObject<SaveRecordedWorkflow>(json);
            saveRecordedWorkflow.WorkflowId = Variables.WorkflowId;
            saveRecordedWorkflow.Header = new Header
            {
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                FolderId = Variables.FolderId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                TenantId = Guid.NewGuid()
            };
            saveRecordedWorkflow.DataSet = null;
            saveRecordedWorkflow.Items = null;

            //Act
            var response = await PostAsync("api/Design/SaveRecordedWorkflow", saveRecordedWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task AddVEnquiry_should_fail()
        {
            // Arrange
            string expected = "TypicalName cannot be empty.";

            SaveTypical addVEnquiry = new SaveTypical
            { 
                
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                CatalogId = Variables.CatalogId,
                SystemId = Variables.SystemId,
                TenantId = Guid.NewGuid(), 
            };

            //Act
            var response = await PostAsync("api/Data/SaveTypical", addVEnquiry);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
