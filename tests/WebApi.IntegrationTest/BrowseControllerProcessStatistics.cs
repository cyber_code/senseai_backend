﻿using Application.Services;
using FluentAssertions;
using Messaging.Queries;
using Queries.ProcessModule.Processes;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerProcessStatistics: ControllerTestBase
    {

        private readonly TestServerFixture _fixture;

        public BrowseControllerProcessStatistics(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task ProcessStatistics_should_success()
        {
            // Arrange
            int nrExpectedWorkflows = 2;
            int nrExpectedOpenIssues = 1;
            int nrExpectedTotalIssues = 1;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetProcessStatistics?ProcessId={Variables.ProcessId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetProcessStatisticsResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(nrExpectedWorkflows, actual.NrWorkflows);
            Assert.Equal(nrExpectedOpenIssues, actual.NrOpenIssues);
            Assert.Equal(nrExpectedTotalIssues, actual.NrTotalIssues);
        }

        [Fact, TestPriority(1)]
        public async Task GetProcessStatistics_should_fail()
        {
            // Arrange
            string expected = "ProcessId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetProcessStatistics?ProcessId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(1)]
        public async Task ProcessStatistics_empty_result_should_fail()
        {
            // Arrange
            int nrExpectedWorkflows = 0;
            int nrExpectedOpenIssues = 0;
            int nrExpectedTotalIssues = 0;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetProcessStatistics?ProcessId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetProcessStatisticsResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(nrExpectedWorkflows, actual.NrWorkflows);
            Assert.Equal(nrExpectedOpenIssues, actual.NrOpenIssues);
            Assert.Equal(nrExpectedTotalIssues, actual.NrTotalIssues);
        }
    }
}
