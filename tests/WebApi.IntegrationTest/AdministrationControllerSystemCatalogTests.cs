﻿using Commands.AdministrationModule.SystemCatalogs;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.AdministrationModule.SystemCatalogs;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class AdministrationControllerSystemCatalogTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _systemCatalogId;

        public AdministrationControllerSystemCatalogTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }


        [Fact, TestPriority(0)]
        public async Task AddSystemCatalog_should_success()
        {
            //Arrange
            var systemCatalog = new AddSystemCatalog
            {
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Administration/AddSystemCatalog", systemCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddSystemCatalogResult>();
            _systemCatalogId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }


        [Fact, TestPriority(1)]
        public async Task UpdateSystemCatalog_should_success()
        {
            //Arrange
            bool expected = true;
            var updateSystemCatalog = new UpdateSystemCatalog()
            {
                Id = _systemCatalogId,
                SystemId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                CatalogId = Guid.NewGuid()
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSystemCatalog", updateSystemCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task DeleteSystemCatalog_should_success()
        {
            //Arrange
            bool expected = true;
            
            //Act
            var response = await DeleteAsync($"api/Administration/DeleteSystemCatalog?Id={_systemCatalogId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task GetCatalogSystem_should_succes()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Administration/GetCatalogSystem?CatalogId={Variables.CatalogId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetCatalogSystem>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBe(Guid.Empty);
        }


        [Fact, TestPriority(3)]
        public async Task GetSystemCatalog_should_succes()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Administration/GetSystemCatalog?CatalogId={Variables.CatalogId}&SubProjectId={Variables.SubProjectId}&SystemId={Variables.SystemId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetCatalogSystem>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(4)]
        public async Task AddSystemCatalog_should_fail()
        {
            //Arrange 
            string expected = "SubProjectId cannot be empty!";
            var systemCatalog = new AddSystemCatalog
            {
                CatalogId = Guid.Empty,
                SystemId = Guid.Empty,
                SubProjectId = Guid.Empty
            };

            //Act
            var response = await PostAsync("api/Administration/AddSystemCatalog", systemCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task AddSystemCatalog_empty_catalogId_should_fail()
        {
            //Arrange 
            string expected = "CatalogId cannot be empty!";
            var systemCatalog = new AddSystemCatalog
            {
                CatalogId = Guid.Empty,
                SystemId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Administration/AddSystemCatalog", systemCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task AddSystemCatalog_empty_systemId_should_fail()
        {
            //Arrange 
            string expected = "SystemId cannot be empty!";
            var systemCatalog = new AddSystemCatalog
            {
                CatalogId = Guid.NewGuid(),
                SystemId = Guid.Empty,
                SubProjectId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/Administration/AddSystemCatalog", systemCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task UpdateSystemCatalog_should_fail()
        {
            string expected = "Id cannot be empty!";
            var updateSystemCatalog = new UpdateSystemCatalog()
            {
                CatalogId = Guid.Empty,
                SystemId = Guid.Empty,
                SubProjectId = Guid.Empty
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSystemCatalog", updateSystemCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task UpdateSystemCatalog_empty_subprojectId_should_fail()
        {
            string expected = "SubProjectId cannot be empty!";
            var updateSystemCatalog = new UpdateSystemCatalog()
            {
                Id = Guid.NewGuid(),
                CatalogId = Guid.Empty,
                SystemId = Guid.Empty,
                SubProjectId = Guid.Empty
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSystemCatalog", updateSystemCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task UpdateSystemCatalog_empty_catalogId_should_fail()
        {
            string expected = "CatalogId cannot be empty!";
            var updateSystemCatalog = new UpdateSystemCatalog()
            {
                Id = Guid.NewGuid(),
                CatalogId = Guid.Empty,
                SystemId = Variables.SystemId,
                SubProjectId = Variables.SubProjectId
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSystemCatalog", updateSystemCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task UpdateSystemCatalog_empty_systemId_should_fail()
        {
            string expected = "SystemId cannot be empty!";
            var updateSystemCatalog = new UpdateSystemCatalog()
            {
                Id = Guid.NewGuid(),
                CatalogId = Variables.CatalogId,
                SystemId = Guid.Empty,
                SubProjectId = Variables.SubProjectId
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSystemCatalog", updateSystemCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task DeleteSystemCatalog_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty!";

            //Act
            var response = await DeleteAsync($"api/Administration/DeleteSystemCatalog?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }


        [Fact, TestPriority(4)]
        public async Task GetSystemCatalog_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty!";
            //Act
            var response = await GetAsync($"api/Administration/GetSystemCatalog?CatalogId={Variables.CatalogId}&SubProjectId={Guid.Empty}&SystemId={Variables.SystemId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetSystemCatalog_empty_systemId_should_fail()
        {
            //Arrange
            string expected = "SystemId cannot be empty!";
            //Act
            var response = await GetAsync($"api/Administration/GetSystemCatalog?CatalogId={Variables.CatalogId}&SubProjectId={Variables.SubProjectId}&SystemId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetSystemCatalog_empty_catalogId_should_fail()
        {
            //Arrange
            string expected = "CatalogId cannot be empty!";
            //Act
            var response = await GetAsync($"api/Administration/GetSystemCatalog?CatalogId={Guid.Empty}&SubProjectId={Variables.SubProjectId}&SystemId={Variables.SystemId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetSystemCatalog_empty_result_should_fail()
        {
            //Arrange
            string expected = "No relation for subproject, system and catalog!";
            //Act
            var response = await GetAsync($"api/Administration/GetSystemCatalog?CatalogId={Guid.NewGuid()}&SubProjectId={Guid.NewGuid()}&SystemId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetCatalogSystem_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty!";
            //Act
            var response = await GetAsync($"api/Administration/GetCatalogSystem?CatalogId={Variables.CatalogId}&SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetCatalogSystem_empty_catalogId_should_fail()
        {
            //Arrange
            string expected = "CatalogId cannot be empty!";
            //Act
            var response = await GetAsync($"api/Administration/GetCatalogSystem?CatalogId={Guid.Empty}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetCatalogSystem_empty_resuld_should_fail()
        {
            //Arrange
            string expected = "No system specified for catalog!";
            //Act
            var response = await GetAsync($"api/Administration/GetCatalogSystem?CatalogId={Guid.NewGuid()}&SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
