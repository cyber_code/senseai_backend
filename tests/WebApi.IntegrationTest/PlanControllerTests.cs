﻿using FluentAssertions;
using Newtonsoft.Json.Linq;
using Queries.AdministrationModule.Projects;
using Queries.DataModule.Data;
using Queries.DesignModule;
using Queries.DesignModule.QualitySuite;
using Queries.TestGenerationModule;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Commands.DesignModule.QualitySuite;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class PlanControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public PlanControllerTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task SavePlanToQualitySuite_should_succes()
        {
            Catalogs catalogs = new Catalogs { From = "", To = "" };
            Catalogs[] arrayCatalog = new Catalogs[1];
            arrayCatalog[0] = catalogs;

            Systems system = new Systems { From = "", To = "" };
            Systems[] arraySystems = new Systems[1];
            arraySystems[0] = system;


            ExportWorkflowToQualitySuite plan = new ExportWorkflowToQualitySuite
            {
                MaintainManualSteps = true,
                ProjectId = 652,
                SubProjectId = 653,
                WorkflowId =Variables.WorkflowId,
                Catalogs = arrayCatalog,
                Systems = arraySystems
            };
            var response = await PostAsync($"api/Plan/ExportWorkflowToQualitySuite", plan);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact, TestPriority(0)]
        public async Task SaveWorkflowToQualitySuite_From_Mock_should_success()
        {
            ExportWorkflowToQualitySuite plan = new ExportWorkflowToQualitySuite
            {
                WorkflowId = Variables.WorkflowId,
                ProjectId = 1,
                SubProjectId = 1,
                MaintainManualSteps = false 
            };
            Catalogs catalogs = new Catalogs { From = "", To = "" };
            Catalogs[] arrayCatalog = new Catalogs[1];
            arrayCatalog[0] = catalogs;

            Systems system = new Systems { From = "", To = "" };
            Systems[] arraySystems = new Systems[1];
            arraySystems[0] = system;

            plan.Catalogs = arrayCatalog;
            plan.Systems = arraySystems;
            var response = await PostAsync("api/Plan/ExportWorkflowToQualitySuite?WorkflowId", plan);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectAsync<ExportWorkflowToQualitySuiteResult>().Result;
            Assert.NotNull(actual);
        }

        [Fact, TestPriority(0)]
        public async Task SaveWorkflowToQualitySuite_Wrong_Id_should_fail()
        {
            Catalogs catalogs = new Catalogs { From = "", To = "" };
            Catalogs[] arrayCatalog = new Catalogs[1];
            arrayCatalog[0] = catalogs;

            Systems system = new Systems { From = "", To = "" };
            Systems[] arraySystems = new Systems[1];
            arraySystems[0] = system;
            ExportWorkflowToQualitySuite plan = new ExportWorkflowToQualitySuite
            {
                MaintainManualSteps = true,
                ProjectId = 652,
                SubProjectId = 653,
                Catalogs = arrayCatalog,
                Systems = arraySystems,
                WorkflowId = Guid.NewGuid()
            };
            var response = await PostAsync("api/Plan/ExportWorkflowToQualitySuite?workflowId", plan);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var saveReposne = response.GetObject<ExportWorkflowToQualitySuiteResult>();

            saveReposne.Errors.Count.Should().Be(1);
        }
        
        private async Task<GetFoldersResult> GetFolder()
        {
            var response = await GetAsync("api/Administration/GetFolders");
            return response.GetCollection<GetFoldersResult>().FirstOrDefault();
        }

        private async Task<GetSubProjectsResult> GetSubProject()
        {
            var projectRespond = await GetAsync("api/Administration/GetProjects");
            var project = projectRespond.GetCollection<GetSubProjectsResult>().FirstOrDefault();

            var subProjectRespond = await GetAsync("api/Administration/GetSubProjects?SubProjectId=" + project.Id);

            var subProject = subProjectRespond.GetCollection<GetSubProjectsResult>().FirstOrDefault();
            return subProject;
        }
        [Fact, TestPriority(2)]
        private async Task GetAdapters_should_success()
        {
            string projectid = "1";
            string subprojectid = "3";
            var adapterRespond = await GetAsync($"api/Plan/GetATSAdapters?ProjectId={projectid}&SubProjectId={subprojectid}");
            var adapters=adapterRespond.GetCollectionAsync<GetATSAdaptersResult>().Result;
            adapters.Should().NotBeNull();
            adapters.Count().Should().BeGreaterThan(1);
        }
        [Fact, TestPriority(2)]
        private async Task GetATSAdaptersPerSystems_should_success()
        { 
            var adapterRespond = await GetAsync("api/Plan/GetATSAdaptersPerSystems?ProjectId=" + 1 + "&SubProjectId=" + 1);
            var adapters = adapterRespond.GetCollectionAsync<GetATSAdaptersPerSystemsResult>().Result;
            adapters.Should().NotBeNull();
            adapters.Count().Should().BeGreaterThan(1);
        }
    }
}