﻿using Application.Services;
using Commands.AdministrationModule.Catalogs;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.AdministrationModule.Systems;
using Queries.DesignModule;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;
namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class AdministrationControllerCatalogTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _tenantCatalogId;
        private static Guid _projectCatalogId;
        private static Guid _catalogId;

        public AdministrationControllerCatalogTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }
       
        [Fact, TestPriority(1)]
        public async Task AddTenantCatalog_should_succeed()
        {
            // Arrange
            var addTenantCatalog = new AddTenantCatalog
            {
                Title = "TenantCatalog",
                Description = "Tenant Level Catalog",
                Type = 1
            };

            // Act 
            var response = await PostAsync("api/Administration/AddTenantCatalog", addTenantCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddTenantCatalogResult>();
            _tenantCatalogId = actual.Id;

            // Assert
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task AddProjectCatalog_should_succeed()
        {
            // Arrange
            var addProjectCatalog = new AddProjectCatalog
            {
                ProjectId = Variables.ProjectId,
                Title = "ProjectCatalog",
                Description = "Project Level Catalog",
                Type = 1
            };

            // Act 
            var response = await PostAsync("api/Administration/AddProjectCatalog", addProjectCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddProjectCatalogResult>();
            _projectCatalogId = actual.Id;

            // Assert
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task AddSubProjectCatalog_should_succeed()
        {
            // Arrange
            var addSubProjectCatalog = new AddSubProjectCatalog
            {
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                Title = "SubProjectCatalog",
                Description = "SubProject Level Catalog",
                Type = 1
            };

            // Act 
            var response = await PostAsync("api/Administration/AddSubProjectCatalog", addSubProjectCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddSubProjectCatalogResult>();
            _catalogId = actual.Id;

            // Assert
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task UpdateCatalog_should_succeed()
        {
            // Arrange
            bool expected = true;            
            var updateCatalog = new UpdateCatalog
            {
                Id = _catalogId,
                Title = "Updated Title",
                Description = "Updated Description",
                Type = 2
            };

            // Act 
            var response = await PutAsync("api/Administration/UpdateCatalog", updateCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task GetTenantCatalogs_by_subprojectId_should_success()
        {
            //Arrange
            int expected = 6;

            //Act
            var response = await GetAsync($"api/Administration/GetTenantCatalogs?ProjectId={Variables.ProjectId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetTenantCatalogs>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task DeleteCatalog_should_succeed()
        {
            // Arrange
            bool expected = true;
            
            // Act
            var response = await DeleteAsync($"api/Administration/DeleteCatalog?Id={_catalogId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task GetTenantCatalogs_by_subprojectId_after_delete_should_success()
        {
            //Arrange
            int expected = 5;

            //Act
            var response = await GetAsync($"api/Administration/GetTenantCatalogs?ProjectId={Variables.ProjectId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetTenantCatalogs>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task GetTenantCatalogs_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Administration/GetTenantCatalogs?ProjectId={Guid.Empty}&SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetTenantCatalogs>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(5)]
        public async Task GetTenantCatalogs_by_projectId_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Administration/GetTenantCatalogs?ProjectId={Variables.ProjectId}");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetTenantCatalogs>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(5)]
        public async Task GetCatalogs_should_success()
        {
            //Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Design/GetCatalogs?ProjectId={Variables.ProjectId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetCatalogsResult>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().BeGreaterOrEqualTo(expected);
        }

        [Fact, TestPriority(5)]
        public async Task GetCatalog_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Design/GetCatalog?catalogId={Variables.CatalogId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetCatalogResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(6)]
        public async Task AddTenantCatalog_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            AddTenantCatalog addTenantCatalog = new AddTenantCatalog
            {
                Title = null,
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddTenantCatalog", addTenantCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task AddProjectCatalog_should_fail()
        {
            //Arrange
            string expected = "ProjectId cannot be empty.";
            AddProjectCatalog addProjectCatalog = new AddProjectCatalog
            {
                ProjectId = Guid.Empty,
                Title = "test",
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddProjectCatalog", addProjectCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task AddProjectCatalog_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            AddProjectCatalog addProjectCatalog = new AddProjectCatalog
            {
                ProjectId = Guid.NewGuid(),
                Title = null,
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddProjectCatalog", addProjectCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task AddSubProjectCatalog_should_fail()
        {
            //Arrange
            string expected = "ProjectId cannot be empty.";
            AddSubProjectCatalog addSubProjectCatalog = new AddSubProjectCatalog
            {
                ProjectId = Guid.Empty,
                SubProjectId = Guid.NewGuid(),
                Title = "test",
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddSubProjectCatalog", addSubProjectCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task AddSubProjectCatalog_subprojectId_empty__should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";
            AddSubProjectCatalog addSubProjectCatalog = new AddSubProjectCatalog
            {
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.Empty,
                Title = "test",
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddSubProjectCatalog", addSubProjectCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task AddSubProjectCatalog_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            AddSubProjectCatalog addSubProjectCatalog = new AddSubProjectCatalog
            {
                ProjectId = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                Title = null,
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddSubProjectCatalog", addSubProjectCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task UpdateCatalog_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            UpdateCatalog updateCatalog = new UpdateCatalog
            {
                Id = Guid.Empty,
                Title = null,
                Description = "test test"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateCatalog", updateCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task UpdateCatalog_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            UpdateCatalog updateCatalog = new UpdateCatalog
            {
                Id = Guid.NewGuid(),
                Title = null,
                Description = "test test"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateCatalog", updateCatalog);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task DeleteCatalog_should_fail()
        {
            // Arrange
            string expected = "Id cannot be empty.";

            // Act
            var response = await DeleteAsync($"api/Administration/DeleteCatalog?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task GetCatalog_should_fail()
        {
            //Arrange
            string expected = "CatalogId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Design/GetCatalog?catalogId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task GetCatalog_empty_result_should_fail()
        {
            //Arrange
            var id = Guid.NewGuid();
            string expected = "No catalog found.";

            //Act
            var response = await GetAsync($"api/Design/GetCatalog?catalogId={id}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task GetCatalogs_empty_result_should_success()
        {
            //Arrange
            string expected = "No catalogs found.";

            //Act
            var response = await GetAsync($"api/Design/GetCatalogs?ProjectId={Guid.NewGuid()}&SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

    }
}
