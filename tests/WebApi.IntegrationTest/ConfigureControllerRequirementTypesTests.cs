﻿using System;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using WebApi.IntegrationTest.Infrastructure;
using Application.Services;
using Messaging.Commands;
using Messaging.Queries;
using Commands.ProcessModule.RequirementTypes;
using Queries.ProcessModule.RequirementTypes;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class ConfigureControllerRequirementTypesTests : ControllerTestBase
    {
        private static readonly Random random = new Random();
        private static Guid _requirementTypeId;
        private readonly TestServerFixture _fixture;

        public ConfigureControllerRequirementTypesTests(TestServerFixture fixture) : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddRequirementType_should_success()
        {
            // Arrange
            AddRequirementType requirementType = new AddRequirementType()
            {
                SubProjectId = Variables.SubProjectId,
                Title = GenerateRandomString(20),
                Code = GenerateRandomString(20),
            };

            // Act
            var response = await PostAsync("api/Process/Configure/AddRequirementType", requirementType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddRequirementTypeResult>();
            _requirementTypeId = actual.Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateRequirementType_should_success()
        {
            // Arrange
            bool expected = true;
            UpdateRequirementType updateRequirementType = new UpdateRequirementType()
            {
                Id = _requirementTypeId,
                SubProjectId = Variables.SubProjectId,
                Title = GenerateRandomString(20),
                Code = GenerateRandomString(20),
            };

            // Act
            var response = await PutAsync("api/Process/Configure/UpdateRequirementType", updateRequirementType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetRequirementType_should_success()
        {
            // Arrange

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementType?Id={_requirementTypeId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetRequirementTypeResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetRequirementTypes_should_success()
        {
            // Arrange
            int expected = 4;

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementTypes?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetRequirementTypesResult>().Result.Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteRequirementType_should_success()
        {
            // Arrange
            bool expected = true;

            // Act
            var response = await DeleteAsync($"api/Process/Configure/DeleteRequirementType?Id={_requirementTypeId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            // Arrange
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetRequirementTypes_after_delete_should_success()
        {
            // Arrange
            int expected = 3;

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementTypes?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollectionAsync<GetRequirementTypesResult>().Result.Count;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(9)]
        public async Task AddRequirementType_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            AddRequirementType requirementType = new AddRequirementType()
            {
                SubProjectId = Guid.Empty,
                Title = GenerateRandomString(20),
                Code = GenerateRandomString(20)
            };

            // Act
            var response = await PostAsync("api/Process/Configure/AddRequirementType", requirementType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task AddRequirementType_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";

            AddRequirementType requirementType = new AddRequirementType()
            {
                SubProjectId = Variables.SubProjectId,
                Code = GenerateRandomString(20)
            };


            // Act
            var response = await PostAsync("api/Process/Configure/AddRequirementType", requirementType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateRequirementType_should_fail()
        {
            // Arrange
            string expected = "RequirementTypeId cannot be empty!";

            // Act
            UpdateRequirementType updateRequirementType = new UpdateRequirementType()
            {
                SubProjectId = Variables.SubProjectId,
                Title = GenerateRandomString(20),
                Code = GenerateRandomString(20)
            };

            var response = await PutAsync("api/Process/Configure/UpdateRequirementType", updateRequirementType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateRequirementType_empty_SubProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            // Act
            UpdateRequirementType updateRequirementType = new UpdateRequirementType()
            {
                Id = Guid.NewGuid(),
                Title = GenerateRandomString(20),
                Code = GenerateRandomString(20)
            };

            var response = await PutAsync("api/Process/Configure/UpdateRequirementType", updateRequirementType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task UpdateRequirementType_empty_title_should_fail()
        {
            // Arrange
            string expected = "Title cannot be empty!";

            // Act
            UpdateRequirementType updateRequirementType = new UpdateRequirementType()
            {
                Id = Guid.NewGuid(),
                SubProjectId = Variables.SubProjectId,
                Code = GenerateRandomString(20)
            };

            var response = await PutAsync("api/Process/Configure/UpdateRequirementType", updateRequirementType);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task DeleteRequirementType_should_fail()
        {
            // Arrange
            string expected = "RequirementTypeId cannot be empty!";

            // Act
            var response = await DeleteAsync($"api/Process/Configure/DeleteRequirementType?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetRequirementType_should_fail()
        {
            // Arrange
            string expected = "RequirementTypeId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementType?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetRequirementType_empty_result_should_fail()
        {
            // Arrange
            string expected = "No Requirement Type found!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementType?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetRequirementTypes_empty_SubProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementTypes?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task GetRequirementTypes_empty_result_should_fail()
        {
            // Arrange
            string expected = "No Requirement Types found in project!";

            // Act
            var response = await GetAsync($"api/Process/Configure/GetRequirementTypes?&SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            // Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        private static string GenerateRandomString(int strLength)
        {
            const string alphanumericChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            return new string(Enumerable.Repeat(alphanumericChar, strLength)
              .Select(str => str[random.Next(str.Length)])
              .ToArray());
        }
    }
}