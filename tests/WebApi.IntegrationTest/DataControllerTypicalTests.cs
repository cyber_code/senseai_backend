﻿using FluentAssertions;
using Commands.DataModule.Data;
using Queries.AdministrationModule.Projects;
using Queries.DataModule.Data;
using Queries.DataModule.DataSets;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using SenseAI.Domain;
using Queries.AdministrationModule.SystemCatalogs;
using Queries.DesignModule;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class DataControllerTypicalTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _typicalId;
        private static Guid _typicalManulId;
        private static string _typicalTitle;
        private static TypicalType _typicalType;
        public DataControllerTypicalTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        #region Typical Tests 
        [Fact, TestPriority(0)]
        public async Task AddTypical_should_success()
        { 
            //add Typical
            var typical = new AddTypical
            {
                CatalogId = Variables.CatalogId,
                Title = "Typical Title",
                Json = "{}",
                Xml = "",
                Type = SenseAI.Domain.TypicalType.Application,
                XmlHash = ""
            };
            var response = await PostAsync("api/Data/AddTypical", typical);
            response.EnsureSuccessStatusCode(); 
            var actual = response.GetObject<AddTypicalResult>();
            _typicalId = actual.Id;
            _typicalTitle = typical.Title;
            _typicalType = typical.Type;
            //Assert
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact,TestPriority(9)]
        public async Task AddTypicalWithNoTitle_should_fail()
        {
            //Arrange
            string excepted = "Title cannot be empty.";
            var typical = new AddTypical
            {
                CatalogId = Guid.Empty,
                Title = null,
                Json = "{}",
                Xml = "",
                Type = SenseAI.Domain.TypicalType.Application,
                XmlHash = ""
            };

            //Act
            var typicalResponse = await PostAsync("api/Data/AddTypical", typical);

            //Assert
            typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            typicalResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(9)]
        public async Task AddTypicalWithNoJson_should_fail()
        {
            //Arrange
            string excepted = "Json cannot be empty.";
            var typical = new AddTypical
            {
                CatalogId = Guid.NewGuid(),
                Title = "title",
                Json = "",
                Xml = "",
                Type = SenseAI.Domain.TypicalType.Application,
                XmlHash = ""
            };

            //Act
            var typicalResponse = await PostAsync("api/Data/AddTypical", typical);

            //Assert
            typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            typicalResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }

        [Fact,TestPriority(10)]
        public async Task UpdateTypical_should_success()
        { 
            //Act
            var updatedTypical = new GetTypicalResult(_typicalId, "Typical Title updated", "{}", SenseAI.Domain.TypicalType.Application);
            var putResponse = await PutAsync($"api/Data/UpdateTypical", updatedTypical);
            putResponse.EnsureSuccessStatusCode();
            var responseLast = await GetAsync($"api/Data/GetTypical?Id={_typicalId}");
            var actual = responseLast.GetObject<GetTypicalResult>();

            //Assert
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty); 
            updatedTypical.Title.Should().Be(actual.Title);
        }

        [Fact, TestPriority(9)]
        public async Task UpdateTypical_should_fail()
        {
            //Arrange
            string excepted = "Title cannot be empty.";
            var typical = new UpdateTypical
            {
                Id = Guid.NewGuid(),

                Title = null,
                Json = "{}",
                Xml = "",
                Type = SenseAI.Domain.TypicalType.Application,
                XmlHash = ""
            };

            //Act
            var putResponse = await PutAsync($"api/Data/UpdateTypical", typical);

            //Assert
            putResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            putResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }

        [Fact, TestPriority(12)]
        public async Task DeleteTypical_should_success()
        {
            //delete Typical 
            var response = await DeleteAsync("api/Data/DeleteTypical?Id=" + Variables.DelTypicalId);

            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<DeleteTypicalResult>();

            actual.Id.Should().NotBe(Guid.Empty); 
        }

       

        [Fact, TestPriority(9)]
        public async Task DeleteTypical_should_fail()
        {
            //Arrange
            string excepted = "TypicalId cannot be empty."; 
            //Act
            var deleteResponse = await DeleteAsync("api/Data/DeleteTypical?Id=" + Guid.Empty.ToString());

            //Assert
            deleteResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            deleteResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }


        [Fact, TestPriority(0)]
        public async Task AddTypicalManually_should_success()
        {
            var catalog = Variables.CatalogId;
            var systemId = Guid.NewGuid();
            var tenantId = Guid.NewGuid();

            //add Typical Manually
            var typical = new AddTypicalManually
            {
                CatalogId = Variables.CatalogId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                SystemId = systemId,
                TenantId = tenantId,
                Type = TypicalType.Application,
                Title = "Typical Title 2",
                HasAutomaticId = false,
                IsConfiguration = false
            };

            var response = await PostAsync("api/Data/AddTypicalManually", typical);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddTypicalManuallyResult>();
            _typicalManulId = actual.Id;

            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact,TestPriority(3)]
        public async Task AddTypicalManuallyWithNoProjectId_should_fail()
        { 
            string excepted = "ProjectId cannot be empty.";

            //add Typical Manually
            var typical = new AddTypicalManually
            {
                CatalogId = Variables.CatalogId,
                ProjectId = Guid.Empty,
                SubProjectId = Variables.SubProjectId,
                SystemId = Variables.SystemId,
                TenantId = Variables.TenantId,
                Type = TypicalType.Application,
                Title = "Title",
                HasAutomaticId = false,
                IsConfiguration = false
            };

            //Act
            var typicalResponse = await PostAsync("api/Data/AddTypicalManually", typical);

            //Assert
            typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            typicalResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(3)]
        public async Task AddTypicalManuallyWithNoSubProjectId_should_fail()
        {
            string excepted = "SubProjectId cannot be empty.";

            //add Typical Manually
            var typical = new AddTypicalManually
            {
                CatalogId = Variables.CatalogId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Guid.Empty,
                SystemId = Variables.SystemId,
                TenantId = Variables.TenantId,
                Type = TypicalType.Application,
                Title = "Title",
                HasAutomaticId = false,
                IsConfiguration = false
            };

            //Act
            var typicalResponse = await PostAsync("api/Data/AddTypicalManually", typical);

            //Assert
            typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            typicalResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(3)]
        public async Task AddTypicalManuallyWithNoSystemId_should_fail()
        {
            string excepted = "SystemId cannot be empty.";

            //add Typical Manually
            var typical = new AddTypicalManually
            {
                CatalogId = Variables.CatalogId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                SystemId = Guid.Empty,
                TenantId = Variables.TenantId,
                Type = TypicalType.Application,
                Title = "Title",
                HasAutomaticId = false,
                IsConfiguration = false
            };

            //Act
            var typicalResponse = await PostAsync("api/Data/AddTypicalManually", typical);

            //Assert
            typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            typicalResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(3)]
        public async Task AddTypicalManuallyWithNoCatalogId_should_fail()
        {
            string excepted = "Catalog cannot be empty.";

            //add Typical Manually
            var typical = new AddTypicalManually
            {
                CatalogId = Guid.Empty,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                SystemId = Variables.SystemId,
                TenantId = Variables.TenantId,
                Type = TypicalType.Application,
                Title = "Title",
                HasAutomaticId = false,
                IsConfiguration = false
            };

            //Act
            var typicalResponse = await PostAsync("api/Data/AddTypicalManually", typical);

            //Assert
            typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            typicalResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(3)]
        public async Task AddTypicalManuallyWithNoTenantId_should_fail()
        {
            string excepted = "TenantId cannot be empty.";

            //add Typical Manually
            var typical = new AddTypicalManually
            {
                CatalogId = Variables.CatalogId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                SystemId = Variables.SystemId,
                TenantId = Guid.Empty,
                Type = TypicalType.Application,
                Title = "Title",
                HasAutomaticId = false,
                IsConfiguration = false
            };

            //Act
            var typicalResponse = await PostAsync("api/Data/AddTypicalManually", typical);

            //Assert
            typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            typicalResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(3)]
        public async Task AddTypicalManuallyWithNoTitle_should_fail()
        {
            string excepted = "Title cannot be empty.";

            //add Typical Manually
            var typical = new AddTypicalManually
            {
                CatalogId = Variables.CatalogId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                SystemId = Variables.SystemId,
                TenantId = Variables.TenantId,
                Type = TypicalType.Application,
                Title = "",
                HasAutomaticId = false,
                IsConfiguration = false
            };

            //Act
            var typicalResponse = await PostAsync("api/Data/AddTypicalManually", typical);

            //Assert
            typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            typicalResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }

        [Fact, TestPriority(3)]
        public async Task UpdateTypicalManually_should_success()
        { 
            //Update same Typical
            var updatedTypical = new UpdateTypicalManually() { Id = _typicalManulId, Title = "Typical Updated Title" /* , IsTypicalChange = false */ };
            var putResponse = await PutAsync($"api/Data/UpdateTypicalManually", updatedTypical);

            //Get again same Typical
            var responseLast = await GetAsync("api/Data/GetTypical?Id=" + _typicalManulId);
            var newVersionTypical = responseLast.GetObject<GetTypicalResult>();

            newVersionTypical.Should().NotBeNull();
            newVersionTypical.Id.Should().NotBe(Guid.Empty);

            //Check if updated
            updatedTypical.Title.Should().Be(newVersionTypical.Title);
        }

        [Fact, TestPriority(9)]
        public async Task UpdateTypicalManually_should_fail()
        {
            //Arrange
            string excepted = "title cannot be empty.";
            var typical = new UpdateTypicalManually
            {
                Id = Guid.NewGuid(),
                Title = null,
                /* IsTypicalChange = false */
            }; 
            //Act
            var putResponse = await PutAsync($"api/Data/UpdateTypicalManually", typical); 
            //Assert
            putResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            putResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted);
        }

        [Fact, TestPriority(11)]
        public async Task DeleteTypicalManually_should_success()
        {

            //delete Typical
            var typicalDeleteResponse = await DeleteAsync("api/Data/DeleteTypicalManually?Id="+ _typicalManulId);
            typicalDeleteResponse.EnsureSuccessStatusCode();
            var actual = typicalDeleteResponse.GetObject<bool>();

            actual.Should().Be(true);
        }

        [Fact, TestPriority(9)]
        public async Task DeleteTypicalManually_should_fail()
        {
            //Arrange
            string excepted = "TypicalId cannot be empty.";

            //Act
            var deleteResponse = await DeleteAsync("api/Data/DeleteTypicalManually?Id=" + Guid.Empty.ToString());

            //Assert
            deleteResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            deleteResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }

        [Fact, TestPriority(0)]
        public async Task AddTypicalAttributeManually()
        { 
            //add Typical Manually
            var typical = new AddTypicalManually
            {
                CatalogId = Variables.CatalogId,
                ProjectId = Variables.ProjectId,
                SubProjectId = Variables.SubProjectId,
                SystemId = Variables.SystemId,
                TenantId = Variables.TenantId,
                Type = TypicalType.Application,
                Title = "Typical Title",
                HasAutomaticId = false,
                IsConfiguration = false
            };
            var typicalResponse = await PostAsync("api/Data/AddTypicalManually", typical);
            typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            var createdTypical = typicalResponse.GetObject<AddTypicalManuallyResult>();
            createdTypical.Should().NotBeNull();
            createdTypical.Id.Should().NotBe(Guid.Empty);

            //add attribute
            var attribute = new AddTypicalAttributeManually
            {
                TypicalId = createdTypical.Id,
                Name = "Typical Attribute",
                IsMultiValue = false,
                PossibleValues = new string[] { },
                IsRequired = false,
                IsNoChange = false,
                IsNoInput = false,
                IsExternal = false,
                MinimumLength = 0,
                MaximumLength = 10,
                RelatedApplicationName = string.Empty,
                ExtraData = string.Empty,
                RelatedApplicationIsConfiguration = false,
            };

            var attributeResponse = await PostAsync("api/Data/AddTypicalAttributeManually", attribute);
            attributeResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            var createdAttribute = attributeResponse.GetObject<AddTypicalAttributeManuallyResult>();
            createdAttribute.Should().NotBeNull();
            createdAttribute.Id.Should().NotBe(Guid.Empty);
            
        }

        [Fact, TestPriority(9)]
        public async Task AddTypicalAttributeManually_should_fail()
        {
            //var catalog = GetCatalog().Result;
            //var systemId = Guid.NewGuid();
            //var tenantId = Guid.NewGuid();
            ////Arrange
            //string excepted = "TypicalId cannot be empty.";

            ////add Typical Manually
            //var typical = new AddTypicalManually
            //{
            //    CatalogId = catalog.Id,
            //    ProjectId = catalog.ProjectId,
            //    SubProjectId = catalog.SubProjectId,
            //    SystemId = systemId,
            //    TenantId = tenantId,
            //    Type = TypicalType.Application,
            //    Title = "Typical Title",
            //    HasAutomaticId = false,
            //    IsConfiguration = false
            //};
            //var typicalResponse = await PostAsync("api/Data/AddTypicalManually", typical);
            //typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            //var createdTypical = typicalResponse.GetObject<AddTypicalManuallyResult>();
            //createdTypical.Should().NotBeNull();
            //createdTypical.Id.Should().NotBe(Guid.Empty);

            ////add attribute
            //var attribute = new AddTypicalAttributeManually
            //{
            //    //TypicalId = createdTypical.Id,
            //    Name = "Typical Attribute",
            //    IsMultiValue = false,
            //    PossibleValues = new string[] { },
            //    IsRequired = false,
            //    IsNoChange = false,
            //    IsNoInput = false,
            //    IsExternal = false,
            //    MinimumLength = 0,
            //    MaximumLength = 10,
            //    RelatedApplicationName = string.Empty,
            //    ExtraData = string.Empty,
            //    RelatedApplicationIsConfiguration = false,
            //};

            //var attributeResponse = await PostAsync("api/Data/AddTypicalAttributeManually", attribute);
            //attributeResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            //attributeResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }

        [Fact]
        public async Task UpdateTypicalAttributeManually()
        {
            //Add Typical
            //var catalog = GetCatalog().Result;
            //var systemId = Guid.NewGuid();
            //var tenantId = Guid.NewGuid();

            ////add Typical Manually
            //var typical = new AddTypicalManually
            //{
            //    CatalogId = catalog.Id,
            //    ProjectId = catalog.ProjectId,
            //    SubProjectId = catalog.SubProjectId,
            //    SystemId = systemId,
            //    TenantId = tenantId,
            //    Type = TypicalType.Application,
            //    Title = "Typical Title",
            //    HasAutomaticId = false,
            //    IsConfiguration = false
            //};
            //var typicalResponse = await PostAsync("api/Data/AddTypicalManually", typical);
            //typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            //var createdTypical = typicalResponse.GetObject<AddTypicalManuallyResult>();

            //createdTypical.Should().NotBeNull();
            //createdTypical.Id.Should().NotBe(Guid.Empty);

            ////add attribute
            //var attribute = new AddTypicalAttributeManually
            //{
            //    TypicalId = createdTypical.Id,
            //    Name = "Typical",
            //    IsMultiValue = false,
            //    PossibleValues = new string[] { },
            //    IsRequired = false,
            //    IsNoChange = false,
            //    IsNoInput = false,
            //    IsExternal = false,
            //    MinimumLength = 0,
            //    MaximumLength = 10,
            //    RelatedApplicationName = string.Empty,
            //    ExtraData = string.Empty,
            //    RelatedApplicationIsConfiguration = false,
            //};

            //var attributeResponse = await PostAsync("api/Data/AddTypicalAttributeManually", attribute);
            //attributeResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            //var createdAttribute = attributeResponse.GetObject<AddTypicalAttributeManuallyResult>();
            //createdAttribute.Should().NotBeNull();
            //createdAttribute.Id.Should().NotBe(Guid.Empty);

            //// Get same Typical Attribute
            ////var responseU = await GetAsync($"api/Data/GetTypicalAttribute?TypicalId={createdTypical.Id}&Name={attribute.Name}");
            //var responseU = await GetAsync("api/Data/GetTypicalAttribute?TypicalId=" + attribute.TypicalId + "&Name=" + attribute.Name);
            ////var responseU = await GetAsync("api/Data/GetTypicalAttribute?Id=" + createdTypical.Id+"&" +attribute.Name);
            //var responseResult = responseU.GetObject<GetTypicalAttributeResult>();

            ////Update same Typical Attribute
            //var updatedTypicalAttribute = new UpdateTypicalAttributeManually()
            //{
            //    TypicalId = responseResult.TypicalId,
            //    Name = "Updated",
            //    IsTypicalChange = false,
            //    IsMultiValue = false,
            //    PossibleValues = new string[] { },
            //    IsRequired = false,
            //    IsNoChange = false,
            //    IsNoInput = false,
            //    IsExternal = false,
            //    MinimumLength = 0,
            //    MaximumLength = 10,
            //    RelatedApplicationName = string.Empty,
            //    ExtraData = string.Empty,
            //    RelatedApplicationIsConfiguration = false,
            //};
            //var putResponse = await PutAsync($"api/Data/UpdateTypicalAttributeManually", updatedTypicalAttribute);

            ////Get again same Typical Attribute
            //var responseLast = await GetAsync($"api/Data/GetTypicalAttribute?TypicalId={createdTypical.Id}&Name={attribute.Name}");
            //var newVersionTypicalAttribute = responseLast.GetObject<GetTypicalAttributeResult>();

            //newVersionTypicalAttribute.Should().NotBeNull();
            //newVersionTypicalAttribute.TypicalId.Should().NotBe(Guid.Empty);

        }

        [Fact]
        public async Task UpdateTypicalAttributeManually_should_fail()
        {
            ////Add Typical
            //var catalog = GetCatalog().Result;
            //var systemId = Guid.NewGuid();
            //var tenantId = Guid.NewGuid();
            //string excepted = "TypicalId cannot be empty.";

            ////add Typical Manually
            //var typical = new AddTypicalManually
            //{
            //    CatalogId = catalog.Id,
            //    ProjectId = catalog.ProjectId,
            //    SubProjectId = catalog.SubProjectId,
            //    SystemId = systemId,
            //    TenantId = tenantId,
            //    Type = TypicalType.Application,
            //    Title = "Typical Title",
            //    HasAutomaticId = false,
            //    IsConfiguration = false
            //};
            //var typicalResponse = await PostAsync("api/Data/AddTypicalManually", typical);
            //typicalResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            //var createdTypical = typicalResponse.GetObject<AddTypicalManuallyResult>();

            //createdTypical.Should().NotBeNull();
            //createdTypical.Id.Should().NotBe(Guid.Empty);

            ////add attribute
            //var attribute = new AddTypicalAttributeManually
            //{
            //    TypicalId = createdTypical.Id,
            //    Name = "Typical Attribute",
            //    IsMultiValue = false,
            //    PossibleValues = new string[] { },
            //    IsRequired = false,
            //    IsNoChange = false,
            //    IsNoInput = false,
            //    IsExternal = false,
            //    MinimumLength = 0,
            //    MaximumLength = 10,
            //    RelatedApplicationName = string.Empty,
            //    ExtraData = string.Empty,
            //    RelatedApplicationIsConfiguration = false,
            //};

            //var attributeResponse = await PostAsync("api/Data/AddTypicalAttributeManually", attribute);
            //attributeResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            //var createdAttribute = attributeResponse.GetObject<AddTypicalAttributeManuallyResult>();
            //createdAttribute.Should().NotBeNull();
            //createdAttribute.Id.Should().NotBe(Guid.Empty);

            //// Get same Typical Attribute
            //var responseU = await GetAsync($"api/Data/GetTypicalAttribute?Id=" + createdTypical.Id + "&" + attribute.Name);
            //var responseResult = responseU.GetObject<GetTypicalAttributeResult>();

            ////Update same Typical Attribute
            //var updatedTypicalAttribute = new UpdateTypicalAttributeManually()
            //{
            //    //TypicalId = responseResult.TypicalId,
            //    Name = "Updated Typical Attribute",
            //    IsTypicalChange = false,
            //    IsMultiValue = false,
            //    PossibleValues = new string[] { },
            //    IsRequired = false,
            //    IsNoChange = false,
            //    IsNoInput = false,
            //    IsExternal = false,
            //    MinimumLength = 0,
            //    MaximumLength = 10,
            //    RelatedApplicationName = string.Empty,
            //    ExtraData = string.Empty,
            //    RelatedApplicationIsConfiguration = false,
            //};
            //var putResponse = await PutAsync($"api/Data/UpdateTypicalAttributeManually", updatedTypicalAttribute);

            ////Get again same Typical Attribute
            //putResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            //putResponse.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());

        }
        
        
        [Fact, TestPriority(3)]
        public async Task GetTypicalById_should_success()
        { 
            //Get same Typical
            var response = await GetAsync("api/Data/GetTypical?Id=" + _typicalId);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.GetObjectAsync<GetTypicalResult>().Should().NotBeNull();
            var gTypical = response.GetObject<GetTypicalResult>();
            //Assert
            gTypical.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(9)]
        public async Task GeTypicalByID_should_fail()
        {
            //Arrange
            string excepted = "TypicalId cannot be empty.";

            //Act
            var response = await GetAsync("api/Data/GetTypical?Id=" + Guid.Empty);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(9)]
        public async Task GeTypicalByIDWithoutRecords_should_fail()
        {
            //Arrange
            string excepted = "No Typical found.";

            //Act
            var response = await GetAsync("api/Data/GetTypical?Id=" + Guid.NewGuid());

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(3)]
        public async Task GetTypicalsByType_should_success()
        {
            //Arrange
            int expected = 1;
            //Act
            var response = await GetAsync("api/Data/GetTypicalsByType?Type=" + _typicalType);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var typicals = response.GetCollection<GetTypicalResult>();
            //Assert
            Assert.True(typicals.Count >= expected); 
        }

       
       
        [Fact, TestPriority(3)]
        public async Task GetTypicalsByName_should_success()
        {
            // Arrange

            // Act
            var response = await GetAsync("api/Data/GetTypicalByName?Name=" + _typicalTitle + "&Type=" + (int)_typicalType);
            response.EnsureSuccessStatusCode();
            var result = response.GetObject<GetTypicalResult>();

            // Assert
            Assert.True(result != null && result.Id != Guid.Empty);
        }

        [Fact, TestPriority(9)]
        public async Task GetTypicalByName_should_fail()
        {
            //Arrange
            string excepted = "Name cannot be empty.";

            //Act
            var response = await GetAsync("api/Data/GetTypicalByName?Name=" + null + "&Type=" + 1);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(9)]
        public async Task GetTypicalByNameWithoutRecords_should_fail()
        {
            //Arrange
            string excepted = "No Typical found.";

            //Act
            var response = await GetAsync("api/Data/GetTypicalByName?Name=TttUU&Type=" + 1);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        

        [Fact, TestPriority(9)]
        public async Task GetTypicalAttribute_should_fail()
        {
            string excepted = "TypicalId cannot be empty.";

            var response = await GetAsync("api/Data/GetTypicalAttributes?TypicalId=" + Guid.Empty + "&Mandatory=" + 0);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(9)]
        public async Task GetTypicalAttributeWithoutRecords_should_fail()
        {
            string excepted = "No Typical found.";

            var response = await GetAsync("api/Data/GetTypicalAttributes?TypicalId=" + Guid.NewGuid() + "&Mandatory=" + 0);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
      
        [Fact, TestPriority(9)]
        public async Task GetNumberOfTypicalChanges_should_success()
        {
            //Get same Typical
            var response = await GetAsync("/api/Data/GetNumberOfTypicalChanges");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.GetObjectAsync<int>().Should().NotBeNull();
            var gTypical = response.GetObject<int>();
            gTypical.Should().BeGreaterOrEqualTo(0);
        }

        [Fact, TestPriority(9)]
        public async Task GetTypicalChanges_should_success()
        {
            //Get same Typical
            var response = await GetAsync("/api/Data/GetTypicalChanges");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.GetCollectionAsync<GetTypicalChangesResult>().Should().NotBeNull();
            var gTypical = response.GetCollection<GetTypicalChangesResult>();
            gTypical.Count.Should().BeGreaterOrEqualTo(0);
        }

        [Fact, TestPriority(9)]
        public async Task GetAcceptedTypicalChanges_should_success()
        {
            //Get same Typical
            var response = await GetAsync("/api/Data/GetAcceptedTypicalChanges");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.GetCollectionAsync<GetAcceptedTypicalChangesResult>().Should().NotBeNull();
            var gTypical = response.GetCollection<GetAcceptedTypicalChangesResult>();
            gTypical.Count.Should().BeGreaterOrEqualTo(0);
        }

        [Fact, TestPriority(6)]
        public async Task GetTypicalAttributeChangesById_should_success()
        {
            //Get same Typical
            var response = await GetAsync("/api/Data/GetTypicalChanges");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.GetCollectionAsync<GetTypicalChangesResult>().Should().NotBeNull();
            var gTypical = response.GetCollection<GetTypicalChangesResult>().FirstOrDefault();
            var responsets = await GetAsync("/api/Data/GetTypicalAttributeChangesById?Id=" + gTypical.Id);

            responsets.StatusCode.Should().Be(HttpStatusCode.OK);
            responsets.GetObjectAsync<GetTypicalAttributeChangesResult>().Should().NotBeNull();
        }

        [Fact, TestPriority(9)]
        public async Task GetTypicalAttributeChangesById_should_fail()
        {
            string excepted = "typicalid cannot be empty.";

            var responsets = await GetAsync("/api/Data/GetTypicalAttributeChangesById?Id=" + Guid.Empty);

            responsets.StatusCode.Should().Be(HttpStatusCode.OK);
            responsets.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(9)]
        public async Task GetTypicalAttributeChangesByIdWithoutRecords_should_fail()
        {
            string excepted = "No Typical-Change found.";

            var responsets = await GetAsync("/api/Data/GetTypicalAttributeChangesById?Id=" + Guid.NewGuid());

            responsets.StatusCode.Should().Be(HttpStatusCode.OK);
            responsets.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }
        [Fact, TestPriority(9)]
        public async Task GetAcceptedTypicalAttributeChangesById_should_success()
        {
            //Get same Typical
            var response = await GetAsync("/api/Data/GetAcceptedTypicalChanges");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.GetCollectionAsync<GetAcceptedTypicalChangesResult>().Should().NotBeNull();
            var gTypical = response.GetCollection<GetAcceptedTypicalChangesResult>().FirstOrDefault();
            var responsets = await GetAsync("/api/Data/GetAcceptedTypicalAttributeChangesById?Id=" + gTypical.Id);

            responsets.StatusCode.Should().Be(HttpStatusCode.OK);
            responsets.GetObjectAsync<GetAcceptedTypicalAttributeChangesResult>().Should().NotBeNull();
        }

        [Fact,TestPriority(9)]
        public async Task GetAcceptedTypicalAttributeChangesById_should_fail()
        {
            string excepted = "Id cannot be empty.";

            var responsets = await GetAsync("/api/Data/GetAcceptedTypicalAttributeChangesById?Id=" + Guid.Empty);

            responsets.StatusCode.Should().Be(HttpStatusCode.OK);
            responsets.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task AcceptChanges_should_success()
        {
            //Get same Typical
            var response = await GetAsync("/api/Data/GetTypicalChanges");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.GetCollectionAsync<GetTypicalChangesResult>().Should().NotBeNull();
            var gTypical = response.GetCollection<GetTypicalChangesResult>().FirstOrDefault();
            var typical = new AcceptTypicalChanges()
            {
                Ids = new[] { gTypical.Id },
                MarkAsInvalid = true
            };
            var responsets = await PostAsync("/api/Data/AcceptChanges", typical);

            responsets.StatusCode.Should().Be(HttpStatusCode.OK);
            responsets.GetObjectAsync<AcceptTypicalChangesResult>().Should().NotBeNull();
        }

        [Fact, TestPriority(9)]
        public async Task AcceptChanges_should_fail()
        {
            string excepted = "Id cannot be empty.";
            var typical = new AcceptTypicalChanges()
            {
                Ids = new[] { Guid.Empty },
                MarkAsInvalid = true
            };
            var responsets = await PostAsync("/api/Data/AcceptChanges", typical);

            responsets.StatusCode.Should().Be(HttpStatusCode.OK);
            responsets.Content.ReadAsStringAsync().Result.ToLower().Should().Contain(excepted.ToLower());
        }

        [Fact, TestPriority(9)]
        public async Task AcceptAllChangesWithoutWorkflow_should_success()
        {
            //Get same Typical

            var typical = new AcceptAllTypicalChangesWithoutWorkflow();

            var responsets = await PostAsync("/api/Data/AcceptAllChangesWithoutWorkflow", typical);

            responsets.StatusCode.Should().Be(HttpStatusCode.OK);
            responsets.GetObjectAsync<AcceptAllTypicalChangesWithoutWorkflowResult>().Should().NotBeNull();
        }

        private async Task<GetProjectsResult> GetProject()
        {
            var response = await GetAsync("api/Administration/GetProjects");
            return response.GetCollectionAsync<GetProjectsResult>().Result.FirstOrDefault();
        }

        #endregion Typical Tests
    }
}