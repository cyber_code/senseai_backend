﻿using Application.Services;
using Commands.ProcessModule.Sprints;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.ProcessModule.Sprints;
using SenseAI.Domain;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class SprintControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _sprintId;

        public SprintControllerTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddSprint_should_succeed()
        {
            // Arrange
            AddSprints addSprint = new AddSprints
            {
                SubProjectId = Variables.SubProjectId,
                Title = "Sprint Title",
                Description = "Sprint description",
                Duration = 0,
                DurationUnit = DurationUnit.Weeks
            };

            //Act
            var response = await PostAsync("api/Process/Sprint/AddSprint", addSprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddSprintsResult>();
            _sprintId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateSprint_should_succeed()
        {
            // Arrange
            bool expected = true;
            UpdateSprints updateSprint = new UpdateSprints
            {
                Id = _sprintId,
                SubProjectId = Variables.SubProjectId,
                Title = "Sprint Title",
                Description = "Sprint description",
                Start = DateTime.Now,
                End = DateTime.Now.AddDays(2),
                Duration = 0,
                DurationUnit = DurationUnit.Weeks
            };

            //Act
            var response = await PutAsync($"api/Process/Sprint/UpdateSprint", updateSprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(1)]
        public async Task StartSprint_should_succeed()
        {
            // Arrange
            bool expected = true;
            StartSprints  startSprint  = new StartSprints
            {
                Id = _sprintId
            };

            //Act
            var response = await PutAsync($"api/Process/Sprint/StartSprint?Id={_sprintId}", startSprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(1)]
        public async Task EndSprint_should_succeed()
        {
            // Arrange
            bool expected = true;
            EndSprints endSprint = new EndSprints
            {
                Id = _sprintId
            };

            //Act
            var response = await PutAsync($"api/Process/Sprint/EndSprint?Id={_sprintId}", endSprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetSprint_should_succeed()
        {
            // Arrange

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprint?Id={_sprintId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetSprintResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetSprints_should_succeed()
        {
            // Arrange
            int expected = 2;
            
            //Act
            var response = await GetAsync("api/Process/Sprint/GetSprints");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSprintsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetSprintsBySubProject_should_succeed()
        {
            // Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintsBySubProject?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSprintsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);

        }

        [Fact, TestPriority(2)]
        public async Task GetOpenSprintsBySubProject_should_succeed()
        {
            // Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetOpenSprintBySubProject?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSprintsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteSprint_should_succeed()
        {
            // Arrange
            bool expected = true;
            DeleteSprints deleteSprint = new DeleteSprints
            {
                Id = _sprintId
            };

            //Act
            var response = await DeleteAsync("api/Process/Sprint/DeleteSprint", deleteSprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetSprintsBySubProject_after_delete_should_succeed()
        {
            // Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintsBySubProject?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetSprintsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);

        }

        [Fact, TestPriority(5)]
        public async Task AddSprint_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";
            var sprint = new AddSprints
            {
                Title = null,
                Description = null
            };

            //Act
            var response = await PostAsync("api/Process/Sprint/AddSprint", sprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddSprint_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            var sprint = new AddSprints
            {
                SubProjectId = Variables.SubProjectId,
                Description = null
            };

            //Act
            var response = await PostAsync("api/Process/Sprint/AddSprint", sprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSprint_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            var sprint = new UpdateSprints
            {
                Title = null,
            };

            //Act
            var response = await PutAsync($"api/Process/Sprint/UpdateSprint", sprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSprint_empty_subProjectId_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";
            var sprint = new UpdateSprints
            {
                Id = Guid.NewGuid(),
                Title = null,
            };

            //Act
            var response = await PutAsync($"api/Process/Sprint/UpdateSprint", sprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSprint_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            var sprint = new UpdateSprints
            {
                Id = Guid.NewGuid(),
                SubProjectId = Guid.NewGuid(),
                Title = null,
            };

            //Act
            var response = await PutAsync($"api/Process/Sprint/UpdateSprint", sprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeleteSprint_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            var sprint = new DeleteSprints
            {
                Id = Guid.Empty
            };

            //Act
            var response = await DeleteAsync("api/Process/Sprint/DeleteSprint", sprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        
        [Fact, TestPriority(5)]
        public async Task StartSprint_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            var sprint = new DeleteSprints
            {
                Id = Guid.Empty
            };
           
            //Act
            var response = await PutAsync("api/Process/Sprint/StartSprint", sprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        
        [Fact, TestPriority(5)]
        public async Task EndSprint_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            var sprint = new DeleteSprints
            {
                Id = Guid.Empty
            };
           
            //Act
            var response = await PutAsync("api/Process/Sprint/EndSprint", sprint);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
        
        [Fact, TestPriority(5)]
        public async Task GetSprint_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprint?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSprint_empty_result_should_fail()
        {
            //Arrange
            string expected = "No sprint found.";

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprint?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSprintsBySubProject_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintsBySubProject?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSprintsBySubProject_empty_result_should_fail()
        {
            //Arrange
            string expected = "No Sprints found.";

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetSprintsBySubProject?SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetOpenSprintsBySubProject_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetOpenSprintBySubProject?SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetOpenSprintsBySubProject_empty_result_should_fail()
        {
            //Arrange
            string expected = "No open sprints found.";

            //Act
            var response = await GetAsync($"api/Process/Sprint/GetOpenSprintBySubProject?SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
