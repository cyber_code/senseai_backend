﻿using FluentAssertions;
using Queries.AdministrationModule.Projects;
using Queries.DataModule.Data;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class DataControllerNavigationTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public DataControllerNavigationTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        #region Navigation Tests

        [Fact, TestPriority(0)]
        public async Task GetAllNavigations_should_success()
        {
            //Arrange
            int expected = 1;
            //Act
            var response = await GetAsync($"api/Data/GetAllNavigations?SubProjectId={Variables.SubProjectId}&SystemId={Variables.SystemId}");
            response.EnsureSuccessStatusCode(); 
            var actual = response.GetCollection<GetNavigationsResult>();

            //Assert
            Assert.True(actual.Count >= expected);
        }

        [Fact, TestPriority(0)]
        public async Task GetAllNavigationsWhenSubprojectIdIsEmpty_should_fail()
        { 
            string excepted = "SubProjectId cannot be empty!";
            var response = await GetAsync($"api/Data/GetAllNavigations?SubProjectId={Guid.Empty}&SystemId={Variables.SystemId}");
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }

        [Fact, TestPriority(0)]
        public async Task GetAllNavigationsWhenSystemIdIsEmpty_should_fail()
        { 
            string excepted = "SystemId cannot be empty!";
            var response = await GetAsync($"api/Data/GetAllNavigations?SubProjectId={Variables.SubProjectId}&SystemId={Guid.Empty}");
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(0)]
        public async Task GetAllNavigationsWithoutRecords_should_fail()
        {
            string excepted = "No Catalog Data found.";
            var response = await GetAsync($"api/Data/GetAllNavigations?SubProjectId={Variables.SubProjectId}&SystemId={Guid.NewGuid()}");
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }

        [Fact, TestPriority(0)]
        public async Task GetNavigationById_should_success()
        {
            //Act
            var response = await GetAsync($"api/Data/GetNavigation?Id={Variables.NavigationId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetNavigationResult>(); 
            //Assert
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(0)]
        public async Task GetNavigationById_should_fail()
        {
            string excepted = "Id cannot be empty."; 
            var response = await GetAsync($"api/Data/GetNavigation?Id={Guid.Empty}");

            //Assert
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(0)]
        public async Task GetNavigationByIdWithoutRecords_should_fail()
        {
            string excepted = "No Navigation found.";
            var response = await GetAsync($"api/Data/GetNavigation?Id={Guid.NewGuid()}");

            //Assert
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }
        [Fact, TestPriority(0)]
        public async Task GetNavigationByParent_should_success()
        {
            //Arrange
            int expected = 1;
            //Act
            var response = await GetAsync($"api/Data/GetNavigationsByParent?ParentId={Variables.NavigationParentId}"); 
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetNavigationsByParentResult>();
            //Assert
            Assert.True(actual.Count >= expected);
        }
        [Fact, TestPriority(0)]
        public async Task GetNavigationByParent_should_fail()
        {
            string excepted = "ParentId cannot be empty."; 
            var response = await GetAsync($"api/Data/GetNavigationsByParent?ParentId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            //Assert
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }

        [Fact, TestPriority(0)]
        public async Task GetNavigationByParentWithoutRecords_should_fail()
        {
            string excepted = "No Navigations found.";
            var response = await GetAsync($"api/Data/GetNavigationsByParent?ParentId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            //Assert
            response.Content.ReadAsStringAsync().Result.Should().Contain(excepted);
        }

        #endregion Navigation Tests

    }
}