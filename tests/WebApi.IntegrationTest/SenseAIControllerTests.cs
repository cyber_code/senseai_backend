﻿using WebApi.IntegrationTest.Infrastructure;

namespace WebApi.IntegrationTest
{
    public class SenseAIControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public SenseAIControllerTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }
    }
}