﻿using Application.Services;
using Commands.PeopleModule;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.PeopleModule;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class PeopleControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _peopleId;

        public PeopleControllerTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddPeople_should_succeed()
        {
            // Arrange
            var people = new AddPeople
            {
                RoleId = Variables.RoleId,
                Name = "John",
                Surname = "Smith",
                UserIdFromAdminPanel = 5,
                SubProjectId = Variables.SubProjectId,
                Color = "#5cb85c"
            };

            //Act
            var response = await PostAsync("api/People/AddPeople", people);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddPeopleResult>();
            _peopleId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdatePeople_should_succeed()
        {
            // Arrange    
            bool expected = true;
            UpdatePeople updatePeople = new UpdatePeople
            {
                Id = _peopleId,
                RoleId = Variables.RoleId,
                Name = "John",
                Surname = "Smith2",
                UserIdFromAdminPanel = 5,
                SubProjectId = Variables.SubProjectId,
                Color = "#5cb85c"
            };

            //Act
            var response = await PutAsync($"api/People/UpdatePeople", updatePeople);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetPeoplesByRole_should_succeed()
        {
            // Arrange    
            int expected = 2;

            //Act
            var response = await GetAsync($"api/People/GetPeoplesByRole?RoleId={Variables.RoleId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetPeoplesByRoleResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetPeoples_should_succeed()
        {
            // Arrange    
            int expected = 3;

            //Act
            var response = await GetAsync($"api/People/GetPeoples?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetPeoplesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(2)]
        public async Task GetPeoplesByIds_should_succeed()
        {
            // Arrange    
            int expected = 1;
            Guid[] ids = new Guid[1];
            ids[0] = _peopleId;
            GetPeoplesByIds getPeoplesByIds = new GetPeoplesByIds
            {
                Ids = ids
            };
            //Act
            var response = await PostAsync("api/People/GetPeoplesByIds",getPeoplesByIds);
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetPeoplesByIdsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(2)]
        public async Task GetPeople_should_succeed()
        {
            // Arrange    

            //Act
            var response = await GetAsync($"api/People/GetPeople?Id={_peopleId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetPeopleResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }
        [Fact, TestPriority(2)]
        public async Task GetPeopleByUserFromAdmin_should_succeed()
        {
            // Arrange    

            //Act
            var response = await GetAsync($"api/People/GetPeopleByUserFromAdmin?UserIdFromAdminPanel={5}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetPeopleResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(3)]
        public async Task DeletePeople_should_succeed()
        {
            // Arrange    
            bool expected = true;
            DeletePeople deletePeople = new DeletePeople
            {
                Id = _peopleId
            };

            //Act
            var response = await DeleteAsync("api/People/DeletePeople", deletePeople);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetPeoples_after_delete_should_succeed()
        {
            // Arrange    
            int expected = 2;

            //Act
            var response = await GetAsync($"api/People/GetPeoples?SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetPeoplesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task AddPeople_should_fail()
        {
            //Arrange
            string expected = "RoleId cannot be empty!";
            var people = new AddPeople
            {
                RoleId = Guid.Empty,
                Name = null,
            };

            //Act
            var response = await PostAsync("api/People/AddPeople", people);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddPeople_empty_name_should_fail()
        {
            //Arrange
            string expected = "Name cannot be empty!";
            var people = new AddPeople
            {
                RoleId = Guid.NewGuid(),
                Name = null,
                SubProjectId = Guid.NewGuid()
            };

            //Act
            var response = await PostAsync("api/People/AddPeople", people);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdatePeople_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            var people = new UpdatePeople
            {
                Id = Guid.Empty,
                RoleId = Guid.Empty,
                Name = null,
            };

            //Act
            var response = await PutAsync($"api/People/UpdatePeople", people);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdatePeople_empty_roleId_should_fail()
        {
            //Arrange
            string expected = "RoleId cannot be empty.";
            var people = new UpdatePeople
            {
                Id = Guid.NewGuid(),
                RoleId = Guid.Empty,
                Name = null,
            };

            //Act
            var response = await PutAsync($"api/People/UpdatePeople", people);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdatePeople_empty_name_should_fail()
        {
            //Arrange
            string expected = "Name cannot be empty.";
            var people = new UpdatePeople
            {
                Id = Guid.NewGuid(),
                RoleId = Guid.NewGuid(),
                Name = null,
            };

            //Act
            var response = await PutAsync($"api/People/UpdatePeople", people);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeletePeople_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            var people = new DeletePeople
            {
                Id = Guid.Empty
            };

            //Act
            var response = await DeleteAsync("api/People/DeletePeople", people);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetPeople_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await GetAsync($"api/People/GetPeople?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetPeople_empty_result_should_fail()
        {
            //Arrange
            string expected = "No people found.";

            //Act
            var response = await GetAsync($"api/People/GetPeople?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetPeoplesByRole_should_fail()
        {
            //Arrange
            string expected = "RoleId cannot be empty.";

            //Act
            var response = await GetAsync($"api/People/GetPeoplesByRole?RoleId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetPeoplesByRole_empty_result_should_fail()
        {
            //Arrange
            string expected = "No people found.";

            //Act
            var response = await GetAsync($"api/People/GetPeoplesByRole?RoleId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
