﻿using Application.Services;
using Commands.ProcessModule.Issues;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.ProcessModule.Issues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using WebApi.IntegrationTest.Resources;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerIssueResourcesTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _issueResourceId;


        public BrowseControllerIssueResourcesTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AttachIssueResource_should_success()
        {
            string fileContent = "";
            using (var reader = EmbededResourceReader.GetTextReader("WebApi.IntegrationTest.Resources.ProcessResources.resource1.txt"))
            {
                fileContent = reader.ReadToEnd();
            }

            ByteArrayContent bytes = new ByteArrayContent(Encoding.UTF8.GetBytes(fileContent));

            var form = new MultipartFormDataContent
            {
                {bytes, "FileCollection", "resource1.txt"}
            };

            form.Add(new StringContent(Variables.IssueId.ToString()), "IssueId");

            //Act
            var response = await PostFileAsync($"api/Process/Browse/AttachIssueResource", form);
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<AttachIssueResourceResult>()[0];
            _issueResourceId = actual.Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);

           
        }

        [Fact, TestPriority(1)]
        public async Task GetIssueResource_should_succes()
        {
            //Arrange
            int expected = 3;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssueResources?IssueId={Variables.IssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssueResourcesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }
        [Fact, TestPriority(1)]
        public async Task DownloadIssueResource_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/Browse/DownloadIssueResource?Id={_issueResourceId}");
            response.EnsureSuccessStatusCode();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);


        }
        [Fact, TestPriority(2)]
        public async Task DeleteIssueResource_should_sucess()
        {
            //Arrange
            bool expected = true;

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteIssueResource?Id={_issueResourceId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task GetIssueResource_after_delete_should_succes()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssueResources?IssueId={Variables.IssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetIssueResourcesResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task AttachIssueResource_should_fail()
        {
            //Arrange
            string expected = "IssueId cannot be empty.";
            var issue = new AttachIssueResource
            {
                IssueId = Guid.Empty,
               FileCollection=null
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AttachIssueResource", issue);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

       

      

        [Fact, TestPriority(4)]
        public async Task DeleteIssueResource_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteIssueResource?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetIssueResource_should_fail()
        {
            //Arrange
            string expected = "IssueId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssueResources?IssueId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(4)]
        public async Task GetIssueResource_empty_result_should_fail()
        {
            //Arrange
            string expected = "No issue resources found.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetIssueResources?IssueId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

    }
}
