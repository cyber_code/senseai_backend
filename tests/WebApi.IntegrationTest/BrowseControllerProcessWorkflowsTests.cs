﻿using Application.Services;
using Commands.ProcessModule.Processes;
using FluentAssertions;
using Messaging.Commands;
using Messaging.Queries;
using Queries.ProcessModule.Processes;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerProcessWorkflowsTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _processIssueId;
        public BrowseControllerProcessWorkflowsTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddProcessWorkflow_should_success()
        {
            //Arrange
            var processWorkflow = new AddProcessWorkflow
            {
                ProcessId = Variables.ProcessId,
                WorkflowId = Variables.WorkflowId
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddProcessWorkflow", processWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddProcessWorkflowResult>();
            _processIssueId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(1)]
        public async Task UpdateProcessWorkflow_should_success()
        {
            //Arrange
            bool expected = true;
            UpdateProcessWorkflow updateProcessWorkflow = new UpdateProcessWorkflow
            {
                Id = _processIssueId,
                ProcessId = Variables.ProcessId,
                WorkflowId = Variables.WorkflowId
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateProcessWorkflow", updateProcessWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetProcessWorkflow_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessWorkflow?Id={_processIssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetProcessWorkflowResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Should().NotBeNull();
            actual.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(2)]
        public async Task GetProcessWorkflows_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessWorkflows?ProcessId={Variables.ProcessId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetProcessWorkflowsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteProcessWorkflow_should_sucess()
        {
            //Arrange
            bool expected = true;

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteProcessWorkflow?Id={_processIssueId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetProcessWorkflows_after_delete_should_success()
        {
            //Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessWorkflows?ProcessId={Variables.ProcessId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetCollection<GetProcessWorkflowsResult>().Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public async Task AddProcessWorkflow_should_fail()
        {
            //Arrange
            string expected = "ProcessId cannot be empty.";
            AddProcessWorkflow addProcessWorkflow = new AddProcessWorkflow
            {
                ProcessId = Guid.Empty,
                WorkflowId = Guid.Empty
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddProcessWorkflow", addProcessWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddProcessWorkflow_empty_workflowId_should_fail()
        {
            //Arrange
            string expected = "WorkflowId cannot be empty.";
            AddProcessWorkflow addProcessWorkflow = new AddProcessWorkflow
            {
                ProcessId = Guid.NewGuid(),
                WorkflowId = Guid.Empty
            };

            //Act
            var response = await PostAsync("api/Process/Browse/AddProcessWorkflow", addProcessWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateProcessWorkflow_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";
            UpdateProcessWorkflow updateProcessWorkflow = new UpdateProcessWorkflow
            {
                Id = Guid.Empty,
                ProcessId = Guid.Empty,
                WorkflowId = Guid.Empty,
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateProcessWorkflow", updateProcessWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateProcessWorkflow_empty_processId_should_fail()
        {
            //Arrange
            string expected = "ProcessId cannot be empty.";
            UpdateProcessWorkflow updateProcessWorkflow = new UpdateProcessWorkflow
            {
                Id = Guid.NewGuid(),
                ProcessId = Guid.Empty,
                WorkflowId = Guid.Empty,
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateProcessWorkflow", updateProcessWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateProcessWorkflow_empty_workflowId_should_fail()
        {
            //Arrange
            string expected = "WorkflowId cannot be empty.";
            UpdateProcessWorkflow updateProcessWorkflow = new UpdateProcessWorkflow
            {
                Id = Guid.NewGuid(),
                ProcessId = Guid.NewGuid(),
                WorkflowId = Guid.Empty,
            };

            //Act
            var response = await PutAsync("api/Process/Browse/UpdateProcessWorkflow", updateProcessWorkflow);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }


        [Fact, TestPriority(5)]
        public async Task DeleteProcessWorkflow_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await DeleteAsync($"api/Process/Browse/DeleteProcessWorkflow?Id={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetProcessWorkflow_should_fail()
        {
            //Arrange
            string expected = "Id cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessWorkflow?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetProcessWorkflow_empty_result_should_fail()
        {
            //Arrange
            string expected = "No process workflow is found.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessWorkflow?Id={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetProcessWorkflows_should_fail()
        {
            //Arrange
            string expected = "ProcessId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessWorkflows?ProcessId={ Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetProcessWorkflows_empty_result_should_fail()
        {
            //Arrange
            string expected = "No workflows found in process.";

            //Act
            var response = await GetAsync($"api/Process/Browse/GetProcessWorkflows?ProcessId={ Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }
    }
}
