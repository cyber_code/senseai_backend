﻿using Application.Services;
using FluentAssertions;
using Messaging.Queries;
using Queries.ProcessModule.Hierarchies;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.IntegrationTest.Infrastructure;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class BrowseControllerHierarchyStatistics : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        
        public BrowseControllerHierarchyStatistics(TestServerFixture fixture) : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task HierarchyStatistics_should_success()
        {  
            // Arrange
            int nrExpectedProcesses = 1;
            int nrExpectedWorkflows = 2;
            int nrExpectedOpenIssues = 1;
            int nrExpectedTotalIssues = 1;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchyStatistics?HierarchyId={Variables.HierarchyId}&SubProjectId={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetHierarchyStatisticsResult>();

            // Assert
           response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(nrExpectedProcesses, actual.NrProcesses);
            Assert.Equal(nrExpectedWorkflows, actual.NrWorkflows);
            Assert.Equal(nrExpectedOpenIssues, actual.NrOpenIssues);
            Assert.Equal(nrExpectedTotalIssues, actual.NrTotalIssues);
        }

        [Fact, TestPriority(1)]
        public async Task HierarchyStatistics_should_fail()
        {
            // Arrange
            string expected = "HierarchyId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchyStatistics?HierarchyId={Guid.Empty}&SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(1)]
        public async Task HierarchyStatistics_empty_subProjectId_should_fail()
        {
            // Arrange
            string expected = "SubProjectId cannot be empty!";

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchyStatistics?HierarchyId={Guid.NewGuid()}&SubProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(1)]
        public async Task HierarchyStatistics_empty_result_should_success()
        {
            // Arrange
            int nrExpectedProcesses = 0;
            int nrExpectedWorkflows = 0;
            int nrExpectedOpenIssues = 0;
            int nrExpectedTotalIssues = 0;

            // Act
            var response = await GetAsync($"api/Process/Browse/GetHierarchyStatistics?HierarchyId={Guid.NewGuid()}&SubProjectId={Guid.NewGuid()}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<GetHierarchyStatisticsResult>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(nrExpectedProcesses, actual.NrProcesses);
            Assert.Equal(nrExpectedWorkflows, actual.NrWorkflows);
            Assert.Equal(nrExpectedOpenIssues, actual.NrOpenIssues);
            Assert.Equal(nrExpectedTotalIssues, actual.NrTotalIssues);
        }
    }
}
