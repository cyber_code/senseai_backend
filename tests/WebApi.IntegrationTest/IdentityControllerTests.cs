﻿using FluentAssertions;
using Newtonsoft.Json.Linq;
using Commands.Identity;
using WebApi.IntegrationTest.Infrastructure;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class IdentityControllerTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;

        public IdentityControllerTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        private async Task<HttpResponseMessage> DoSignIn(string userName, string password, string returnURL)
        {
            var signIn = new SignIn { UserName = userName, Password = password, ReturnUrl=returnURL };
            return await PostAsync("api/Identity/signin", signIn);
        }

        private string GetAccesTokenFromResponse(string responseString)
        {
            var resource = JObject.Parse(responseString);

            foreach (var jToken in resource.Children())
            {
                var token = (JProperty)jToken;
                if (token.Name.Equals("access_token"))
                {
                    return token.Value.ToString();
                }
            }

            return null;
        }

        #region [ Test Sign In Action ]

        [Fact, TestPriority(0)]
        private async Task SignInWithCorrectUserCredential()
        {
            var response = await DoSignIn("valmir.muqkurtaj", "manager","");

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var responseString = response.Content.ReadAsStringAsync().Result;
            var accesToken = GetAccesTokenFromResponse(responseString);

            accesToken.Should().NotBeNullOrEmpty();
        }

        [Fact, TestPriority(1)]
        private async Task SignInNegativeTest()
        {
            var response = await DoSignIn("valmir.muqkurtaj", "password123","");

            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
            var responseString = response.Content.ReadAsStringAsync().Result;

            responseString.Should().Be("Incorrect password");
        }

        #endregion [ Test Sign In Action ]

        #region [ Test Sign out Action ]

        [Fact, TestPriority(3)]
        private async Task SignOut()
        {
            var signInResponse = await DoSignIn("valmir.muqkurtaj", "manager","");
            var responseString = signInResponse.Content.ReadAsStringAsync().Result;

            var accesToken = GetAccesTokenFromResponse(responseString);
            var signOutresponse = await GetWithHeadersAsync("api/Identity/signout", new Dictionary<string, string>() { { "Authorization", $"Bearer {accesToken}" } }, accesToken);

            signOutresponse.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        #endregion [ Test Sign out Action ]

        #region [ Test Permissions out Action ]

        [Fact, TestPriority(4)]
        private async Task HavingPermisionsOnAction()
        {
            var signInResponse = await DoSignIn("valmir.muqkurtaj", "manager", "");
            var responseString = signInResponse.Content.ReadAsStringAsync().Result;

            var accesToken = GetAccesTokenFromResponse(responseString);
            //var response = await GetAsync("api/Administration/GetProjects");
            var getAllProjectsResponseMessage = await GetWithHeadersAsync("api/Administration/GetProjects", new Dictionary<string, string>() { { "Authorization", $"Bearer {accesToken}" } }, accesToken);
            getAllProjectsResponseMessage.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        //[Fact]
        //private async Task NotHavingPermisionsOnAction()
        //{
        //    var signInResponse = await DoSignIn("admin", "123456", "");
        //    var responseString = signInResponse.Content.ReadAsStringAsync().Result;

        //    var accesToken = GetAccesTokenFromResponse(responseString);
        //    var getAllFoldersreResponseMessage = await GetWithHeadersAsync("api/administration/getallfolders", new Dictionary<string, string>() { { "Authorization", $"Bearer {accesToken}" } }, accesToken);

        //    getAllFoldersreResponseMessage.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        //}

        #endregion [ Test Permissions out Action ]
    }
}