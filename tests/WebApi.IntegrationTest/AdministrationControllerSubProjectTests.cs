﻿using FluentAssertions;
using Commands.AdministrationModule.Projects;
using Queries.AdministrationModule.Projects;
using WebApi.IntegrationTest.Infrastructure;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Messaging.Commands;
using Messaging.Queries;
using Application.Services;

namespace WebApi.IntegrationTest
{
    [TestCaseOrderer("WebApi.IntegrationTest.Infrastructure.PriorityOrderer", "WebApi.IntegrationTest")]
    public class AdministrationControllerSubProjectTests : ControllerTestBase
    {
        private readonly TestServerFixture _fixture;
        private static Guid _subProjectId;

        public AdministrationControllerSubProjectTests(TestServerFixture fixture)
            : base(fixture)
        {
            _fixture = fixture;
        }

        [Fact, TestPriority(0)]
        public async Task AddSubProject_should_success()
        {
            //Arrange
            var subProject = new AddSubProject
            {
                ProjectId = Variables.ProjectId,
                Title = "title 1",
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddSubProject", subProject);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<AddSubProjectResult>();
            _subProjectId = actual.Id;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            actual.Id.Should().NotBe(Guid.Empty);

        }

        [Fact, TestPriority(1)]
        public async Task UpdateSubProject_should_success()
        {
            //Arrange
            bool expected = true;
            var updateSubProject = new UpdateSubProject
            {
                Id = _subProjectId,
                ProjectId = Variables.ProjectId,
                Title = "Test SubProject 2 updated",
                Description = "Test SubProject 2 updated"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSubProject", updateSubProject);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(2)]
        public async Task GetSubProjects_should_success()
        {
            //Arrange
            int expected = 2;

            //Act
            var response = await GetAsync($"api/Administration/GetSubProjects?ProjectId={Variables.ProjectId}");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetSubProjectsResult>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public async Task DeleteSubProject_should_success()
        {
            //Arrange

            bool expected = true;

            //Act
            var response = await DeleteAsync($"api/Administration/DeleteSubProject?Id={_subProjectId}");
            response.EnsureSuccessStatusCode();

            bool actual = response.GetObject<bool>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            expected.Should().Be(actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetSubProjects_after_delete_should_success()
        {
            //Arrange
            int expected = 1;

            //Act
            var response = await GetAsync($"api/Administration/GetSubProjects?ProjectId={Variables.ProjectId}");
            response.EnsureSuccessStatusCode();
            var result = response.GetCollectionAsync<GetSubProjectsResult>().Result;
            int actual = result.Count;

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(4)]
        public async Task GetSubProject_should_success()
        {
            //Arrange

            //Act
            var response = await GetAsync($"api/Administration/GetSubProject?Id={Variables.SubProjectId}");
            response.EnsureSuccessStatusCode();
            var result = response.GetObject<GetSubProjectResult>();

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            result.Id.Should().NotBe(Guid.Empty);
        }

        [Fact, TestPriority(5)]
        public async Task AddSubProject_should_fail()
        {
            //Arrange
            string expected = "ProjectId cannot be empty.";
            var subProject = new AddSubProject
            {
                ProjectId = Guid.Empty,
                Title = null,
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddSubProject", subProject);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task AddSubProject_empty_title_should_fail()
        {
            //Arrange
            string expected = "title cannot be empty.";
            var subProject = new AddSubProject
            {
                ProjectId = Guid.NewGuid(),
                Title = null,
                Description = "Description 1"
            };

            //Act
            var response = await PostAsync("api/Administration/AddSubProject", subProject);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSubProject_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";
            UpdateSubProject updateProject = new UpdateSubProject
            {
                Id = Guid.Empty,
                ProjectId = Guid.Empty,
                Title = null,
                Description = "test test"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSubProject", updateProject);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSubProject_empty_projectId_should_fail()
        {
            //Arrange
            string expected = "ProjectId cannot be empty.";
            UpdateSubProject updateProject = new UpdateSubProject
            {
                Id = Guid.NewGuid(),
                ProjectId = Guid.Empty,
                Title = null,
                Description = "test test"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSubProject", updateProject);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task UpdateSubProject_empty_title_should_fail()
        {
            //Arrange
            string expected = "Title cannot be empty.";
            UpdateSubProject updateProject = new UpdateSubProject
            {
                Id = Guid.NewGuid(),
                ProjectId = Guid.NewGuid(),
                Title = null,
                Description = "test test"
            };

            //Act
            var response = await PutAsync($"api/Administration/UpdateSubProject", updateProject);
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task DeleteSubProject_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await DeleteAsync($"api/Administration/DeleteSubProject?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<CommandResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSubProjects_should_fail()
        {
            //Arrange
            string expected = "ProjectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Administration/GetSubProjects?ProjectId={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSubProject_should_fail()
        {
            //Arrange
            string expected = "SubProjectId cannot be empty.";

            //Act
            var response = await GetAsync($"api/Administration/GetSubProject?Id={Guid.Empty}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(5)]
        public async Task GetSubProject_empty_result_should_fail()
        {
            //Arrange
            var id = Guid.NewGuid();
            string expected = "No Sub-Project found.";

            //Act
            var response = await GetAsync($"api/Administration/GetSubProject?Id={id}");
            response.EnsureSuccessStatusCode();
            var actual = response.GetObjectCmdResponseAsync<QueryResponseMessage>().Result;

            //Assert
            Assert.Equal(expected.ToLower(), actual.Message.ToLower());
        }

        [Fact, TestPriority(6)]
        public async Task DeleteSubProject_AndEverithingUnder_shoud_success()
        {
            //delete Process
            var deleteFolder = await DeleteAsync($"api/Administration/DeleteSubProject?Id={Variables.DelSubProjectId}");
            var actual = deleteFolder.GetObject<bool>();
            actual.Should().BeTrue();
        }

        [Fact, TestPriority(6)]
        public async Task DeleteSubProject_AndEverithingUnder_shoud_fail()
        {
            //delete Process
            var deleteFolder = await DeleteAsync($"api/Administration/DeleteSubProject?Id={Guid.NewGuid()}");
            var actual = deleteFolder.GetObject<bool>();
            actual.Should().BeFalse();
        }


    }
}