﻿using System;
using AXMLEngine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using OFSCommonMethods.IO.Telnet;
using ValidataT24Deployment;
using T24TelnetAdapter;
using ValidataCommon;

namespace TestT24DeploymentDotNet45
{
    [TestClass]
    public class T24DeploymentTests
    {
        [TestMethod]
        public void TestConsoleClass()
        {
            string telnetSetName = "67-abc-orig";


            var aadesigner = new AAProductDesigner();
            aadesigner.ID = $"NEWBOT.{getThreeCaps()}-20190702";
            aadesigner.DESCRIPTION = "New account";
            aadesigner.PRODUCT_GROUP = "CURRENT.ACCOUNTS";
            aadesigner.CURRENCY.Add(new MVField { MVIndex = 1, SVIndex = 1, Value = "EUR" });
            aadesigner.PROPERTY_CONDITIONS.Add(new PropertyConditions(1, "ACCOUNTING", "ACCOUNTS", "TRACKING"));
            aadesigner.PROPERTY_CONDITIONS.Add(new PropertyConditions(2, "ACTIVITY.MAPPING", "ACCOUNTS", "TRACKING"));
            aadesigner.CALCULATION_SOURCE.Add(new CalculationsSource(1, "CRINTEREST", "CR.DAILY", "CURBALANCE"));
            aadesigner.CALCULATION_SOURCE.Add(new CalculationsSource(2, "DRINTEREST", "DR.MAXIMUM", "CURBALANCE"));


            var errText = string.Empty;
            RequestManagerWrapper t24Deployer = new RequestManagerWrapper(telnetSetName);
            DeploymentOutput result = t24Deployer.DeployProofPublishAAProd(aadesigner, out errText);

            bool prodPublished = result.Outcome == DeploymentOutcome.Successful && t24Deployer.CheckProductPublished(aadesigner.ID);

            Assert.IsTrue(prodPublished);

        }

        [TestMethod]
        public void TestEndToEnd()
        {
            //{
            //  "ID": "NEWBOT.NNF-20190702",
            //  "DESCRIPTION": "New account",
            //  "PRODUCT_GROUP": "CURRENT.ACCOUNTS",
            //  "PARENT_PRODUCT":" "CURRENT.PARENT.PREF",
            //  "CURRENCY": [
            //    {
            //      "MVIndex": 1,
            //      "SVIndex": 1,
            //      "Value": "USD"
            //    }
            //  ],
            //  "PROPERTY_CONDITIONS": [
            //    {
            //      "PROPERTY": {
            //        "MVIndex": 1,
            //        "SVIndex": 1,
            //        "Value": "BALANCE"
            //      },
            //      "PRD_PROPERTY": {
            //        "MVIndex": 1,
            //        "SVIndex": 1,
            //        "Value": "CURRENT.ACCOUNT.PREF"
            //      },
            //      "ARR_LINK": {
            //        "MVIndex": 1,
            //        "SVIndex": 1,
            //        "Value": "NON.TRACKING"
            //      }
            //    }
            //  ],
            //  "CALCULATION_SOURCE": [
            //    {
            //      "CALC_PROPERTY": {
            //        "MVIndex": 1,
            //        "SVIndex": 1,
            //        "Value": "BALANCE"
            //      },
            //      "SOURCE_TYPE": {
            //        "MVIndex": 1,
            //        "SVIndex": 1,
            //        "Value": "BALANCE"
            //      },
            //      "SOURCE_BALANCE": {
            //        "MVIndex": 1,
            //        "SVIndex": 1,
            //        "Value": "ACCBONUS"
            //      }
            //    }
            //  ]
            //}

            var randID = $"NEWBOT.{getThreeCaps()}";
            string designerJson = "{\"ID\":\""+ randID + "\",\"DESCRIPTION\":\"New account\",\"PRODUCT_GROUP\":\"CURRENT.ACCOUNTS\", \"PARENT_PRODUCT\":\"CURRENT.PARENT.PREF\",\"CURRENCY\":[{\"MVIndex\":1,\"SVIndex\":1,\"Value\":\"USD\"}],\"PROPERTY_CONDITIONS\":[{\"PROPERTY\":{\"MVIndex\":1,\"SVIndex\":1,\"Value\":\"BALANCE\"},\"PRD_PROPERTY\":{\"MVIndex\":1,\"SVIndex\":1,\"Value\":\"CURRENT.ACCOUNT.PREF\"},\"ARR_LINK\":{\"MVIndex\":1,\"SVIndex\":1,\"Value\":\"NON.TRACKING\"}}],\"CALCULATION_SOURCE\":[{\"CALC_PROPERTY\":{\"MVIndex\":1,\"SVIndex\":1,\"Value\":\"BALANCE\"},\"SOURCE_TYPE\":{\"MVIndex\":1,\"SVIndex\":1,\"Value\":\"BALANCE\"},\"SOURCE_BALANCE\":{\"MVIndex\":1,\"SVIndex\":1,\"Value\":\"ACCBONUS\"}}]}";
            string telnetSetName = "67-abc-orig";

            // override if needed
            //ExecutionStepContainer.TelnetDefFileName = "";

            AAProductDesigner aadesigner = JsonConvert.DeserializeObject<AAProductDesigner>(designerJson);
            RequestManagerWrapper t24Deployer = new RequestManagerWrapper(telnetSetName);

            string errText;
            t24Deployer.DeployAAProdFull(aadesigner, null, null, out errText);

        }

        [TestMethod]
        public void TestDeploymentABB()
        {
            string telnetSetName = "67-abc-orig";


            // the instance
            var atID = "TEST.ABB";
            var typical = "ABBREVIATION";

            AXMLEngine.Instance inst = new AXMLEngine.Instance(typical, "dummyCatalog");
            string val = $"VAL{getThreeCaps()}";
            inst.AddAttribute("ORIGINAL.TEXT", val);
            inst.AddAttribute("@ID", atID);

            RequestManager reqman = new RequestManager(
                T24TelnetAdapter.OperatingSystem.Win,
                telnetSetName,
                "",
                "",
                "",
                "",
                true
                );

           

            var result = reqman.DeployRecord(new DeploymentRecord()
            {
                RecordInstance = inst,
                RecordName = inst.GetAttributeValue("@ID"),
                DeploymentPack = "Dummy Packet",
                Company = "",
                DeploymentMethod = DeploymentMethod.Record_IA,
                SkipValueDeletions = true,
                InputSameValues = true,
                UseReplaceOption = false

            }, true, false);

            reqman.Release();

            Assert.AreEqual(result.UpdatedInstanceOnT24.AttributeByName("ORIGINAL.TEXT").Value, val);


        }

        [TestMethod]
        public void TestDeploymentWrapperABB()
        {
            string telnetSetName = "67-abc-orig";

            // the instance
            var atID = "TEST.ABB";
            var typical = "ABBREVIATION";

            AXMLEngine.Instance inst = new AXMLEngine.Instance(typical, "dummyCatalog");
            string val = $"VAL{getThreeCaps()}";
            inst.AddAttribute("ORIGINAL.TEXT", val);
            inst.AddAttribute("@ID", atID);

            var errText = string.Empty;
            RequestManagerWrapper t24Deployer = new RequestManagerWrapper(telnetSetName);
            DeploymentOutput result = t24Deployer.DeployAnyInstance(inst, out errText);

            Assert.AreEqual(result.Outcome, DeploymentOutcome.Successful);

        }


        [TestMethod]
        public void TestDeploymentWrapperAAPD()
        {
            string telnetSetName = "67-abc-orig";

            // the instance
            var atID = $"BOT.{getThreeCaps()}";
            var typical = "AA.PRODUCT.DESIGNER";

            AXMLEngine.Instance inst = new AXMLEngine.Instance(typical, "dummyCatalog");
            inst.AddAttribute("@ID", atID);

            inst.AddAttribute("DESCRIPTION",      "AA new prod.");
            inst.AddAttribute("PRODUCT.GROUP-1~1", "CURRENT.ACCOUNTS");
            inst.AddAttribute("PARENT.PRODUCT-1~1", "CURRENT.PARENT.PREF");
            inst.AddAttribute("CURRENCY-1~1", "USD");
            inst.AddAttribute("PROPERTY-1~1", "BALANCE");
            inst.AddAttribute("PRD.PROPERTY-1~1", "CURRENT.ACCOUNT.PREF");
            inst.AddAttribute("ARR.LINK-1~1", "NON.TRACKING");
            inst.AddAttribute("CALC.PROPERTY-1~1", "BALANCE");
            inst.AddAttribute("SOURCE.TYPE-1~1", "BALANCE");
            inst.AddAttribute("SOURCE.BALANCE-1~1", "ACCBONUS");

            var errText = string.Empty;
            RequestManagerWrapper t24Deployer = new RequestManagerWrapper(telnetSetName);
            DeploymentOutput result = t24Deployer.DeployAnyInstance(inst, out errText);

        }

        [TestMethod]
        public void TestWrapperFull()
        {
            string telnetSetName = "67-abc-orig";

            // the instance
            var atID = $"BOT.{getThreeCaps()}";
            var typical = "AA.PRODUCT.DESIGNER";

            AXMLEngine.Instance inst = new AXMLEngine.Instance(typical, "dummyCatalog");
            inst.AddAttribute("@ID", atID);

            inst.AddAttribute("DESCRIPTION", "AA new prod.");
            inst.AddAttribute("PRODUCT.GROUP-1~1", "CURRENT.ACCOUNTS");
            inst.AddAttribute("PARENT.PRODUCT-1~1", "CURRENT.PARENT.PREF");
            inst.AddAttribute("CURRENCY-1~1", "USD");
            inst.AddAttribute("PROPERTY-1~1", "BALANCE");
            inst.AddAttribute("PRD.PROPERTY-1~1", "CURRENT.ACCOUNT.PREF");
            inst.AddAttribute("ARR.LINK-1~1", "NON.TRACKING");
            inst.AddAttribute("CALC.PROPERTY-1~1", "BALANCE");
            inst.AddAttribute("SOURCE.TYPE-1~1", "BALANCE");
            inst.AddAttribute("SOURCE.BALANCE-1~1", "ACCBONUS");



            var errText = string.Empty;
            RequestManagerWrapper t24Deployer = new RequestManagerWrapper(telnetSetName);
            DeploymentOutput result = t24Deployer.DeployProofPublishAAProd(inst, out errText);

            bool prodPublished = t24Deployer.CheckProductPublished(atID);

            Assert.IsTrue(prodPublished);

        }



        private string getThreeCaps()
        {
            Random _random = new Random();
            return $"{(char)('A' + _random.Next(0, 26))}{(char)('A' + _random.Next(0, 26))}{(char)('A' + _random.Next(0, 26))}";
        }
    }
}
