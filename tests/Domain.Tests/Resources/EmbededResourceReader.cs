﻿using System.IO;
using System.Reflection;

namespace SenseAI.Domain.Tests.Resources
{
    internal sealed class EmbededResourceReader
    {
        public static TextReader GetTextReader(string resourceName)
        {
            return new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName));
        }
    }
}