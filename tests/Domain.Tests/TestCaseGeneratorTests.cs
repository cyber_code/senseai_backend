﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SenseAI.Domain.WorkflowModel;
using SenseAI.Domain.Tests.Resources;
using SenseAI.Domain.WorkflowModel;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SenseAI.Domain.Tests
{
    public class TestCaseGeneratorTests
    {
        //[Theory]
        //[MemberData(nameof(Init_Parameters))]
        //public void Generate_ShouldThrowsExceptionForInitParametes(Workflow workflow, WorkflowVersionPaths workflowPaths, string parameter)
        //{
        //    //Arrange

        //    //Act
        //    TestCaseGenerator testCaseGenerator = new TestCaseGenerator(workflow, new List<SystemModel.System>());

        //    //Assert
        //    //Assert.Throws<ArgumentException>(parameter, () => testCaseGenerator.Generate(workflowPaths));
        //}

        //[Theory]
        //[MemberData(nameof(NoLinks))]
        //[MemberData(nameof(NoStartElement))]
        //public void Generate_ShouldThrowsExceptionForNoStartElementAndNoLink(Workflow workflow, WorkflowVersionPaths workflowPaths)
        //{
        //    //Arrange

        //    //Act
        //    TestCaseGenerator testCaseGenerator = new TestCaseGenerator(workflow, new List<SystemModel.System>());

        //    //Assert
        //    //Assert.Throws<Exception>(() => testCaseGenerator.Generate(workflowPaths));
        //}

        //[Theory]
        //[MemberData(nameof(Data))]
        //public void Generate_ShouldWork(Workflow workflow, WorkflowVersionPaths workflowPaths)
        //{
        //    //Arrange
        //    int expectedTC = 1;
        //    int expectedTS = 3;

        //    //Act
        //    TestCaseGenerator testCaseGenerator = new TestCaseGenerator(workflow, new List<SystemModel.System>());
        //    List<TestCase> result = testCaseGenerator.Generate(workflowPaths).ToList();
        //    int actualTC = result.Count;
        //    int actualTS = result.Select(ts => ts.TestSteps.Count).Sum();

        //    //Assert
        //    //Assert.Equal(expectedTC, actualTC);
        //    //Assert.Equal(expectedTS, actualTS);
        //}

        //[Theory]
        //[MemberData(nameof(Data))]
        //public void Generate_ShouldWork_with_json(Workflow workflow, WorkflowVersionPaths workflowPaths)
        //{
        //    //Arrange
        //    int expectedTC = 1;
        //    int expectedTS = 3;

        //    //Act
        //    TestCaseGenerator testCaseGenerator = new TestCaseGenerator(workflow, new List<SystemModel.System>());
        //    List<TestCase> result = testCaseGenerator.Generate(workflowPaths).ToList();
        //    int actualTC = result.Count;
        //    int actualTS = result.Select(ts => ts.TestSteps.Count).Sum();

        //    //Assert
        //    //Assert.Equal(expectedTC, actualTC);
        //    //Assert.Equal(expectedTS, actualTS);
        //}

        #region Input Data

        public static IEnumerable<object[]> Init_Parameters =>
           new List<object[]>
           {
                        new object[] { null,null,null,"workflow" },
                        new object[] { new Workflow(Guid.NewGuid()), null, null ,"workflowPaths"},
                        new object[] { new Workflow(Guid.NewGuid()), new WorkflowVersionPaths[2], null,"dataSetsIds" },
           };

        public static IEnumerable<object[]> NoStartElement => Workflow_withoutstart_element();

        public static IEnumerable<object[]> NoLinks => Workflow_withoutlink_element();

        public static IEnumerable<object[]> Data => Workflow_with_all_ements();

        public static IEnumerable<object[]> DataFromJson => Workflow_with_all_ements_from_json();

        private static IEnumerable<object[]> Workflow_withoutstart_element()
        {
            WorkflowItemConstraint[] constraint = new WorkflowItemConstraint[0];
            WorkflowItemDynamicData[] dds = new WorkflowItemDynamicData[0];
            Workflow workflow = new Workflow(Guid.NewGuid(), "Workflow 1", "", Guid.Empty, true, true);
            Guid from = Guid.NewGuid();
            workflow.AddActionItem(from, "Create customer", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "CUSTOMER", "1", (int)ActionType.Input, "", constraint, constraint,
                dds, false, "");
            Guid to = Guid.NewGuid();
            workflow.AddActionItem(to, "Create Account", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "ACCOUNT", "1", (int)ActionType.Input, "", constraint, constraint,
               dds, false, "");

            workflow.AddItemLink(from, to, WorkflowItemLinkState.Yes);

            List<WorkflowVersionPaths> workflowPaths = new List<WorkflowVersionPaths>();
            Guid wfpid = Guid.NewGuid();
            WorkflowVersionPaths workflowVersionPath = new WorkflowVersionPaths(wfpid, workflow.Id, 0, "Path1", true, 90, 0, true);
            workflowVersionPath.AddItem(from, workflow.Id, 0, wfpid, "Create customer", 1, (int)ActionType.Input, Guid.NewGuid(), Guid.NewGuid(), "CUSTOMER", "1", Guid.Empty, "", 0);
            workflowVersionPath.AddItem(to, workflow.Id, 0, wfpid, "Create Account", 1, (int)ActionType.Input, Guid.NewGuid(), Guid.NewGuid(), "ACCOUNT", "1", Guid.Empty, "", 0);
            workflowPaths.Add(workflowVersionPath);


            DataSetsIds[] dataSetsIdS = new DataSetsIds[0];
            return new List<object[]>
        {
                                        new object[] { workflow, workflowPaths.ToArray(), dataSetsIdS },
        };
        }

        private static IEnumerable<object[]> Workflow_withoutlink_element()
        {
            WorkflowItemConstraint[] constraint = new WorkflowItemConstraint[0];
            Guid[] guid = new Guid[0];
            WorkflowItemDynamicData[] dds = new WorkflowItemDynamicData[0];
            Workflow workflow = new Workflow(Guid.NewGuid(), "Workflow 1", "", Guid.Empty,true, true);
            Guid from = Guid.NewGuid();
            workflow.AddActionItem(from, "Create customer", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "CUSTOMER", "1", (int)ActionType.Input, "", constraint, constraint,
                dds, false, "");
            Guid to = Guid.NewGuid();
            workflow.AddActionItem(to, "Create Account", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "ACCOUNT", "1", (int)ActionType.Input, "", constraint, constraint,
               dds, false, "");

            List<WorkflowVersionPaths> workflowPaths = new List<WorkflowVersionPaths>();
            Guid wfpid = Guid.NewGuid();
            WorkflowVersionPaths workflowVersionPath = new WorkflowVersionPaths(wfpid, workflow.Id, 0, "Path1", true, 90, 0, true);
            workflowVersionPath.AddItem(from, workflow.Id, 0, wfpid, "Create customer", 1, (int)ActionType.Input, Guid.NewGuid(), Guid.NewGuid(), "CUSTOMER", "1", Guid.Empty, "", 0);
            workflowVersionPath.AddItem(to, workflow.Id, 0, wfpid, "Create Account", 1, (int)ActionType.Input, Guid.NewGuid(), Guid.NewGuid(), "ACCOUNT", "1", Guid.Empty, "", 0);
            workflowPaths.Add(workflowVersionPath);

            DataSetsIds[] dataSetsIdS = new DataSetsIds[0];
            return new List<object[]>
        {
                                        new object[] { workflow, workflowPaths.ToArray(), dataSetsIdS },
        };
        }

        private static IEnumerable<object[]> Workflow_with_all_ements()
        {
            WorkflowItemConstraint[] constraint = new WorkflowItemConstraint[0];
            Guid[] guid = new Guid[0];
            WorkflowItemDynamicData[] dds = new WorkflowItemDynamicData[0];
            Workflow workflow = new Workflow(Guid.NewGuid(), "Workflow 1", "", Guid.Empty, true, true);
            List<WorkflowVersionPaths> workflowPaths = new List<WorkflowVersionPaths>();
            Guid wfpid = Guid.NewGuid();

            /*Item 1 and 2*/
            Guid fromS = Guid.NewGuid();
            workflow.AddStartItem(fromS, Guid.NewGuid(), "Start", "", Guid.NewGuid(), "T24WebAdapter", "R16", Guid.NewGuid(), "CUSTOMER", "1", (int)ActionType.Input, "");

            Guid toI1 = Guid.NewGuid();

            workflow.AddActionItem(toI1, "Create customer", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "CUSTOMER", "1", (int)ActionType.Input, "", constraint, constraint,
                dds, false, "");

            workflow.AddItemLink(fromS, toI1, WorkflowItemLinkState.Yes);
            WorkflowVersionPaths workflowVersionPath = new WorkflowVersionPaths(wfpid, workflow.Id, 0, "Path1", true, 90, 0, true);
            workflowVersionPath.AddItem(fromS, workflow.Id, 0, wfpid, "Create customer", 1, (int)ActionType.Input, Guid.NewGuid(), Guid.NewGuid(), "CUSTOMER", "1", Guid.Empty, "", 0);
            workflowVersionPath.AddItem(toI1, workflow.Id, 0, wfpid, "Create Account", 1, (int)ActionType.Input, Guid.NewGuid(), Guid.NewGuid(), "ACCOUNT", "1", Guid.Empty, "", 0);
            workflowPaths.Add(workflowVersionPath);

            /*Item 3 and 4*/
            Guid fromI3 = Guid.NewGuid();
            workflow.AddActionItem(fromI3, "Create Account", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "ACCOUNT", "1", (int)ActionType.Input, "", constraint, constraint,
               dds, false, "");

            workflow.AddItemLink(toI1, fromI3, WorkflowItemLinkState.Yes);

            /*Item 4 and 5*/
            Guid toi4 = Guid.NewGuid();

            workflow.AddActionItem(toi4, "Create customer 2", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "CUSTOMER", "1", (int)ActionType.Input, "", constraint, constraint,
                dds, false, "");

            workflow.AddItemLink(fromI3, toi4, WorkflowItemLinkState.Yes);
            workflowVersionPath = new WorkflowVersionPaths(wfpid, workflow.Id, 0, "Path1", true, 90, 0, true);
            workflowVersionPath.AddItem(fromI3, workflow.Id, 0, wfpid, "Create customer", 1, (int)ActionType.Input, Guid.NewGuid(), Guid.NewGuid(), "CUSTOMER", "1", Guid.Empty, "", 0);
            workflowVersionPath.AddItem(toi4, workflow.Id, 0, wfpid, "Create Account", 1, (int)ActionType.Input, Guid.NewGuid(), Guid.NewGuid(), "ACCOUNT", "1", Guid.Empty, "", 0);
            workflowPaths.Add(workflowVersionPath);


            DataSetsIds[] dataSetsIdS = new DataSetsIds[0];
            return new List<object[]>
        {
                                        new object[] { workflow, workflowPaths.ToArray(), dataSetsIdS},
        };
        }

        private static IEnumerable<object[]> Workflow_with_all_ements_from_json()
        {
            string workflowJson = "";
            using (var expectedReader = EmbededResourceReader.GetTextReader("Domain.Tests.Resources.TestGeneration.UIDesignerJson.json"))
            {
                workflowJson = expectedReader.ReadToEnd();
            }

            var workflow = WorkflowFactory.Create(Guid.NewGuid(), workflowJson);

            string workflowPathsJson = "";
            using (var expectedReader = EmbededResourceReader.GetTextReader("Domain.Tests.Resources.TestGeneration.WorkflowPath.json"))
            {
                workflowPathsJson = expectedReader.ReadToEnd();
            }

            var json = JObject.Parse(workflowPathsJson);
            WorkflowPathDesinger workflowPathDesinger = JsonConvert.DeserializeObject<WorkflowPathDesinger>(json.ToString());

            WorkflowVersionPaths workflowPaths = null;
            // CHANGE
            // workflowPathDesinger.Paths.SelectMany(
            //    workflowPath => workflowPath.ItemIds.Select(
            //        itemId => new WorkflowVersionPaths(
            //            workflow.Id,
            //            workflowPath.PathId,
            //            workflowPath.Title,
            //            workflowPathDesinger.Paths.ToList().IndexOf(workflowPath),
            //            itemId,
            //            workflow.Items.FirstOrDefault(ct => ct.Id == itemId).Title,
            //            (workflow.Items.FirstOrDefault(ct => ct.Id == itemId) is ActionWorkflowItem) ? 1 :
            //            (workflow.Items.FirstOrDefault(ct => ct.Id == itemId) is ConditionWorkflowItem) ? 2 : 0,
            //            (workflow.Items.FirstOrDefault(ct => ct.Id == itemId) is ActionWorkflowItem) ? workflow.Items.FirstOrDefault(ct => ct.Id == itemId).ActionType : -1,
            //            workflowPathsJson,
            //            workflow.Items.FirstOrDefault(ct => ct.Id == itemId).CatalogId,
            //            workflow.Items.FirstOrDefault(ct => ct.Id == itemId).TypicalName,
            //            workflow.Items.FirstOrDefault(ct => ct.Id == itemId).TypicalType,
            //            workflow.Items.FirstOrDefault(ct => ct.Id == itemId).TypicalId,
            //            Guid.Empty, workflowPath.Coverage.ToLower() == "n/a" ? 0 : decimal.Parse(workflowPath.Coverage),
            //            workflowPath.Selected,
            //            workflowPath.ItemIds.ToList().IndexOf(itemId),
            //            true
            //        )
            //    )
            //).ToArray();

            string dataSetsJson;
            using (var expectedReader = EmbededResourceReader.GetTextReader("Domain.Tests.Resources.TestGeneration.DataSets.json"))
            {
                dataSetsJson = expectedReader.ReadToEnd();
            }

            var dataSetsIdS = JsonConvert.DeserializeObject<DataSetsIds[]>(dataSetsJson);

            return new List<object[]>
            {
                  new object[] { workflow, workflowPaths, dataSetsIdS},
            };
        }

        #endregion Input Data
    }
}