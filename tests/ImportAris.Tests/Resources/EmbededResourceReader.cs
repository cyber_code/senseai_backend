﻿using System;
using System.IO;
using System.Reflection;

namespace ImportAris.Tests.Resources
{
    internal sealed class EmbededResourceReader
    {
        public static TextReader GetTextReader(string resourceName)
        {
            try
            {
                return new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName));
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}