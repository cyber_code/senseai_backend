using System;
using Xunit;
using ImportAris.Tests.Resources;

namespace ImportAris.Tests
{
    public class ImportArisWorkflowTests
    {
        [Fact()]
        public void XmlToArisWorkflow_ShouldThrowsExceptionForInitParametefileContent()
        {
            //Arrange

            //Act
            ImportArisWorkflow importAris = new ImportArisWorkflow();

            //Assert
            Assert.Throws<ArgumentException>("fileContent", () => importAris.XmlToArisWorkflow(null));
        } 

        [Fact()]
        public void XmlToArisWorkflow_ShouldThrowsExceptionForInitParameteInvalidXml()
        {
            //Arrange
            string expected = "Xml is not valid.";

            //Act
            ImportArisWorkflow importAris = new ImportArisWorkflow();

            //Assert
            var ex = Assert.Throws<Exception>(() => importAris.XmlToArisWorkflow("xml"));
            Assert.Equal(expected, ex.Message);
        }

        [Fact()]
        public void XmlToArisWorkflow_ShouldWork()
        {
            //Arrange
            int expected = 8;

            //Act
            ImportArisWorkflow importAris = new ImportArisWorkflow();
            string fileContent = null;
            using (var reader = EmbededResourceReader.GetTextReader("ImportAris.Tests.Resources.Aris.aris.xml"))
            {
                fileContent = reader.ReadToEnd();
            }
            var arisWorkflows = importAris.XmlToArisWorkflow(fileContent);
            int actual = arisWorkflows.Count;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact()]
        public void XmlToWorkflow_ShouldWork()
        {
            //Arrange
            int expected = 8;

            //Act
            ImportArisWorkflow importAris = new ImportArisWorkflow();
            string fileContent = null;
            using (var reader = EmbededResourceReader.GetTextReader("ImportAris.Tests.Resources.Aris.aris.xml"))
            {
                fileContent = reader.ReadToEnd();
            }
            var workflows = importAris.XmlToWorkflow(fileContent, Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "", null);
            int actual = workflows.Count;

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}