﻿using SenseAI.Domain;
using SenseAI.Domain.WorkflowModel;
using SenseAI.Domain.WorkflowModel;
using PathGenerations.Tests.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PathGenerations.Tests
{
    public class GeneratePathsTests
    {
        [Fact()]
        public void Generate_ShouldThrowsExceptionForInitParametes()
        {
            //Arrange

            //Act
            GeneratePaths path = new GeneratePaths();

            //Assert
            Assert.Throws<ArgumentException>("workflow", () => path.Generate(null));
        }

        [Theory]
        [MemberData(nameof(No_Links))]
        [MemberData(nameof(No_StartElement))]
        public void Generate_ShouldThrowsExceptionForNoStartElementAndNoLink(Workflow workflow)
        {
            //Arrange

            //Act
            GeneratePaths path = new GeneratePaths();

            //Assert
            Assert.Throws<Exception>(() => path.Generate(workflow));
        }

        [Theory]
        [MemberData(nameof(Data))]
        public void Generate_ShouldWork(Workflow workflow)
        {
            //Arrange
            int expectedPaths = 1;

            //Act
            GeneratePaths path = new GeneratePaths();
            List<Path> result = path.Generate(workflow);
            int actualPath = result.Count;

            //Assert
            Assert.Equal(expectedPaths, actualPath);
        }

        [Theory]
        [MemberData(nameof(DataFromJson))]
        public void Generate_ShouldWork_with_json(Workflow workflow)
        {
            //Arrange
            int expectedPaths = 2;

            //Act
            GeneratePaths path = new GeneratePaths();
            List<Path> result = path.Generate(workflow);
            int actualPath = result.Count;

            //Assert
            Assert.Equal(expectedPaths, actualPath);
        }

        #region Input Data

        public static IEnumerable<object[]> No_StartElement =>
            workflow_withoutstart_element();

        public static IEnumerable<object[]> No_Links =>
            workflow_withoutlink_element();

        public static IEnumerable<object[]> Data =>
            workflow_with_all_ements();

        public static IEnumerable<object[]> DataFromJson =>
            workflow_with_all_ements_from_json();

        private static IEnumerable<object[]> workflow_withoutstart_element()
        {
            WorkflowItemConstraint[] constraint = new WorkflowItemConstraint[0];
            WorkflowItemDynamicData[] dds = new WorkflowItemDynamicData[0];
            Workflow workflow = new Workflow(Guid.NewGuid(), "Workflow 1", "", Guid.Empty, true, true);
            Guid from = Guid.NewGuid();
            workflow.AddActionItem(from, "Create customer", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "CUSTOMER", "1", (int)ActionType.Input, "", constraint, constraint,
                dds, false, "");
            Guid to = Guid.NewGuid();
            workflow.AddActionItem(to, "Create Account", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "ACCOUNT", "1", (int)ActionType.Input, "", constraint, constraint,
               dds, false, "");

            workflow.AddItemLink(from, to, WorkflowItemLinkState.Yes);

            return new List<object[]>
                {
                    new object[] { workflow },
                };
        }

        private static IEnumerable<object[]> workflow_withoutlink_element()
        {
            WorkflowItemConstraint[] constraint = new WorkflowItemConstraint[0];
            Guid[] guid = new Guid[0];
            WorkflowItemDynamicData[] dds = new WorkflowItemDynamicData[0];
            Workflow workflow = new Workflow(Guid.NewGuid(), "Workflow 1", "", Guid.Empty, true, true);
            Guid from = Guid.NewGuid();
            workflow.AddActionItem(from, "Create customer", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "CUSTOMER", "1", (int)ActionType.Input, "", constraint, constraint,
                dds, false, "");
            Guid to = Guid.NewGuid();
            workflow.AddActionItem(to, "Create Account", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "ACCOUNT", "1", (int)ActionType.Input, "", constraint, constraint,
               dds, false, "");

            return new List<object[]>
            {
                  new object[] { workflow },
            };
        }

        private static IEnumerable<object[]> workflow_with_all_ements()
        {
            WorkflowItemConstraint[] constraint = new WorkflowItemConstraint[0];
            Guid[] guid = new Guid[0];
            WorkflowItemDynamicData[] dds = new WorkflowItemDynamicData[0];
            Workflow workflow = new Workflow(Guid.NewGuid(), "Workflow 1", "", Guid.Empty, true, true);

            Guid wfpid = Guid.NewGuid();

            /*Item 1 and 2*/
            Guid fromS = Guid.NewGuid();
            workflow.AddStartItem(fromS, Guid.NewGuid(), "Start", "", Guid.NewGuid(), "T24WebAdapter", "R16", Guid.NewGuid(), "CUSTOMER", "1", (int)ActionType.Input, "");

            Guid toI1 = Guid.NewGuid();

            workflow.AddActionItem(toI1, "Create customer", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "CUSTOMER", "1", (int)ActionType.Input, "", constraint, constraint,
                dds, false, "");

            workflow.AddItemLink(fromS, toI1, WorkflowItemLinkState.Yes);

            /*Item 3 and 4*/
            Guid fromI3 = Guid.NewGuid();
            workflow.AddActionItem(fromI3, "Create Account", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "ACCOUNT", "1", (int)ActionType.Input, "", constraint, constraint,
               dds, false, "");

            workflow.AddItemLink(toI1, fromI3, WorkflowItemLinkState.Yes);

            /*Item 4 and 5*/
            Guid toi4 = Guid.NewGuid();

            workflow.AddActionItem(toi4, "Create customer 2", "", Guid.NewGuid(), "R16", Guid.NewGuid(), "CUSTOMER", "1", (int)ActionType.Input, "", constraint, constraint,
                dds, false, "");

            workflow.AddItemLink(fromI3, toi4, WorkflowItemLinkState.Yes);

            return new List<object[]> { new object[] { workflow } };
        }

        private static IEnumerable<object[]> workflow_with_all_ements_from_json()
        {
            string workflowJson = "";
            using (var expectedReader = EmbededResourceReader.GetTextReader("PathGenerations.Tests.Resources.PathGeneration.UIDesignerJson.josn"))
            {
                workflowJson = expectedReader.ReadToEnd();
            }

            var workflow = WorkflowFactory.Create(Guid.NewGuid(), workflowJson);

            return new List<object[]> { new object[] { workflow } };
        }

        #endregion Input Data
    }
}