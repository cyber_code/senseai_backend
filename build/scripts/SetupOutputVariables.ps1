##-----------------------------------------------------------------------
## <copyright file="SetupOutputVariables.ps1">(c) Validata Group. Author: Edmond Shtogu. All rights reserved.</copyright>
##-----------------------------------------------------------------------

# Enable -Verbose option
[CmdletBinding()]
param()

#region Functions
function Set-VstsVariable {
    param([string]$var, [string]$value)
    Write-Output ("##vso[task.setvariable variable=$var;]$value")
    Write-Host -ForegroundColor Cyan "New variable is set up: `$$var=$value"
}
#endregion

# Make sure path to source code directory is available
if (-not $Env:BUILD_SOURCESDIRECTORY)
{
    Write-Error ("BUILD_SOURCESDIRECTORY environment variable is missing.")
    exit 1
}
elseif (-not (Test-Path $Env:BUILD_SOURCESDIRECTORY))
{
    Write-Error "BUILD_SOURCESDIRECTORY does not exist: $Env:BUILD_SOURCESDIRECTORY"
    exit 1
}
Write-Verbose "BUILD_SOURCESDIRECTORY: $Env:BUILD_SOURCESDIRECTORY"

# Make sure there is a build number
if (-not $Env:BUILD_BUILDNUMBER)
{
    Write-Error ("BUILD_BUILDNUMBER environment variable is missing.")
    exit 1
}
Write-Verbose "BUILD_BUILDNUMBER: $Env:BUILD_BUILDNUMBER"

# Make sure there is a Build.ArtifactsStagingDirectory
if (-not $Env:BUILD_ARTIFACTSTAGINGDIRECTORY)
{
    Write-Error ("BUILD_ARTIFACTSTAGINGDIRECTORY environment variable is missing.")
    exit 1
}
Write-Verbose "BUILD_ARTIFACTSTAGINGDIRECTORY: $Env:BUILD_ARTIFACTSTAGINGDIRECTORY"

#Inicialize ValidataRebuildLogFile variable
$validataRebuildLogFile = Join-Path $Env:BUILD_ARTIFACTSTAGINGDIRECTORY "ValidataRebuildLog.log"
New-Item $validataRebuildLogFile -Force -ItemType File
Set-VstsVariable -var "ValidataRebuildLogFile" -value $validataRebuildLogFile

#Inicialize ValidataRebuildParsedErrorsLogFile variable
$parsedOutputPath = Join-Path $Env:BUILD_ARTIFACTSTAGINGDIRECTORY "ValidataRebuildParsedErrorsLog.log"
New-Item $parsedOutputPath -Force -ItemType File
Set-VstsVariable -var "ValidataRebuildParsedErrorsLogFile" -value $parsedOutputPath

