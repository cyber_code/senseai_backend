##-----------------------------------------------------------------------
## <copyright file="CheckSolutionBuildFailure.ps1">(c) Validata Group. Author: Edmond Shtogu. All rights reserved.</copyright>
##-----------------------------------------------------------------------

# Enable -Verbose option
[CmdletBinding()]
param()

#Get last line of the $Env:VALIDATAREBUILDLOGFILE
[string]$buildStatus = (Get-Content $Env:VALIDATAREBUILDLOGFILE -Tail 2);

$status = $buildStatus.Replace("Rebuild All:", "").Replace("succeeded", "").Replace("failed", "").Replace("skipped", "").Replace(" ", "").Replace("=", "").Split(",");

Write-Verbose $buildStatus
if (!$status[1].Equals("0")) {    
    #Parse the msbuild output to locate the exact lines with reason for failing compilation
    $hash = @{} #define a new empty hash table
    Get-Content $Env:VALIDATAREBUILDLOGFILE | Where-Object {$_ -like "*: error*" -or $_ -like "*= Rebuild All:*"} | 
    ForEach-Object{# For each object in the pipeline...    
        $k = $_-replace "(?<=\.(?:[a-z][a-z]+)\().+(?=\): error)", "0, 0"
        if (!($hash.ContainsKey($k))) { 
            $hash.Add($k, 1)
            $_ #if that line is not a key in our hashtable send that line further along the pipe
        };		
    } > $Env:VALIDATAREBUILDPARSEDERRORSLOGFILE

    (Get-Content $Env:VALIDATAREBUILDPARSEDERRORSLOGFILE) | ForEach-Object { Write-Host $_ ; }
}