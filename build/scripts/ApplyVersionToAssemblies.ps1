##-----------------------------------------------------------------------
## <copyright file="ApplyVersionToAssemblies.ps1">(c) Validata Group. Author: Edmond Shtogu. All rights reserved.</copyright>
##-----------------------------------------------------------------------

# Enable -Verbose option
[CmdletBinding()]
param()

#Update assembly file version
Write-Host -ForegroundColor Cyan "Updating Assemblies File Version..."

# Apply the version to the assembly property files
$files = Get-ChildItem $(Join-Path $Env:BUILD_SOURCESDIRECTORY "src\T24Deployment") -Recurse -Filter AssemblyInfo.cs

if($files)
{
    Write-Verbose "Will apply $Env:BUILD_BUILDNUMBER to $($files.count) files."

    foreach ($file in $files) {
        $pathToFile = Join-Path $file.Directory.ToString() $file.Name
        #load the file and process the lines
        $newFile = Get-Content $pathToFile | Foreach-Object {
            if ($_.StartsWith("[assembly: AssemblyFileVersion") -or $_.StartsWith("[assembly: AssemblyVersion")) {
                # Get original file assembly version
                $verStart = $_.IndexOf("(")
                $verEnd = $_.IndexOf(")", $verStart)
                $origVersion = $_.SubString($verStart+2, $verEnd-$verStart-3)
                $origVersion = $origVersion.TrimStart(' ','"')
                
                Write-Verbose "$pathToFile - version applied"

                $_.Replace($origVersion, $Env:BUILD_BUILDNUMBER)
            }  
            else {
                $_
            }
        }
        
        $newfile | Set-Content $pathToFile
    }

    Write-Host "Assemblies File Version updated successfully!"
}
else
{
    Write-Warning "Found no files."
}
