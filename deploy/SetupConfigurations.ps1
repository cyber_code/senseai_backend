##-----------------------------------------------------------------------
## <copyright file="SetupConfigurations.ps1">(c) Validata Group. Author: Edmond Shtogu. All rights reserved.</copyright>
##-----------------------------------------------------------------------

# Enable -Verbose option
[CmdletBinding()]
param()

# Make sure path to agent release directory is available
if (-not $Env:AGENT_RELEASEDIRECTORY)
{
    Write-Error ("AGENT_RELEASEDIRECTORY environment variable is missing.")
    exit 1
}
elseif (-not (Test-Path $Env:AGENT_RELEASEDIRECTORY))
{
    Write-Error "AGENT_RELEASEDIRECTORY does not exist: $Env:AGENT_RELEASEDIRECTORY"
    exit 1
}
Write-Verbose "AGENT_RELEASEDIRECTORY: $Env:AGENT_RELEASEDIRECTORY"

# Make sure there is release triggering artifact alias
if (-not $Env:RELEASE_PRIMARYARTIFACTSOURCEALIAS)
{
    Write-Error ("RELEASE_PRIMARYARTIFACTSOURCEALIAS environment variable is missing.")
    exit 1
}
Write-Verbose "RELEASE_PRIMARYARTIFACTSOURCEALIAS: $Env:RELEASE_PRIMARYARTIFACTSOURCEALIAS"

# Make sure there is agent machine name
if (-not $Env:AGENT_MACHINENAME)
{
    Write-Error ("AGENT_MACHINENAME environment variable is missing.")
    exit 1
}
Write-Verbose "AGENT_MACHINENAME: $Env:AGENT_MACHINENAME"

# Make sure there is agent os architecture
if (-not $Env:AGENT_OSARCHITECTURE)
{
    Write-Error ("AGENT_OSARCHITECTURE environment variable is missing.")
    exit 1
}
Write-Verbose "AGENT_OSARCHITECTURE: $Env:AGENT_OSARCHITECTURE"

# Make sure there is an install config file for this machine
$configFilePath = "$Env:AGENT_RELEASEDIRECTORY\$Env:RELEASE_PRIMARYARTIFACTSOURCEALIAS\scripts\Configurations\$($Env:AGENT_MACHINENAME.ToLower())-config.json"
if (-not (Test-Path $configFilePath))
{
    Write-Error "The installation file config for $Env:AGENT_MACHINENAME does not exist: $configFilePath"
    exit 1
}
Write-Verbose "The installation file config: $configFilePath"

$appPath = "C:\inetpub\wwwroot\ValidataRPA\ai"
# Make sure there is a disk for installation as into install config file for this machine
$jsonConfigs = Get-Content $configFilePath -Raw | ConvertFrom-Json

$dataSettings = $jsonConfigs.datasettings
$dataSettings | ConvertTo-Json -depth 100 | Out-File "$appPath\App_Data\dataSettings.json"

$appSettings = $jsonConfigs.appsettings
$appSettings | ConvertTo-Json -depth 100 | Out-File "$appPath\appSettings.json"

$webconfig = [xml]($jsonConfigs.webconfig)
$webconfig.Save("$appPath\web.config")